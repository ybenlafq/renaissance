      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*00000010
      * COMMAREA TD00: TRAITEMENT DOSSIER                              *00000020
      *----------------------------------------------------------------*00000010
      *-MAINTENANCES-                                                  *00000010
      *----------------------------------------------------------------*00000010
      * 210104 : TD01-DENVOI AJOUTEE MARQUE AL2101                     *00000010
      *----------------------------------------------------------------*00000010
      * 111004 : AJOUT DE LA ZONE COMM-TD00-CDOSSIER SUITE MODIF       *00000010
      *          ECRAN DE CHOIX                                        *00000010
      *----------------------------------------------------------------*00000010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-TD00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00000050
      *--                                                                       
       01  COM-TD00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
       01  Z-FILLER.                                                    00000070
      * ZONES RESERVEES AIDA ------------------------------------- 100  00000090
          02 FILLER-COM-AIDA      PIC  X(100).                          00000091
      * ZONES RESERVEES CICS ------------------------------------- 020  00000093
          02 FILLER               PIC  X(8).                            00000094
          02 FILLER               PIC  X(8).                            00000095
          02 FILLER               PIC  X(4).                            00000096
      * DATE DU JOUR --------------------------------------------- 100  00000098
          02 FILLER               PIC  99.                              00000099
          02 FILLER               PIC  99.                              00000100
          02 FILLER               PIC  99.                              00000110
          02 FILLER               PIC  99.                              00000120
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000130
          02 FILLER               PIC  999.                             00000140
          02 FILLER               PIC  99999.                           00000150
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000160
          02 FILLER               PIC  9.                               00000170
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00000180
          02 FILLER               PIC  9.                               00000190
      *   LIBELLES DU JOUR COURT - LONG                                 00000191
          02 FILLER               PIC  XXX.                             00000192
          02 FILLER               PIC  X(8).                            00000193
      *   LIBELLES DU MOIS COURT - LONG                                 00000194
          02 FILLER               PIC  XXX.                             00000195
          02 FILLER               PIC  X(8).                            00000196
      *   DIFFERENTES FORMES DE DATE                                    00000197
          02 FILLER               PIC  X(8).                            00000198
          02 FILLER               PIC  X(6).                            00000199
          02 FILLER               PIC  X(8).                            00000200
          02 FILLER               PIC  X(6).                            00000210
          02 FILLER               PIC  X(8).                            00000220
          02 FILLER               PIC  X(10).                           00000230
      *   TRAITEMENT NUMERO DE SEMAINE                                  00000240
          02 COMM-DATE-WEEK.                                            00000250
             05 COMM-DATE-SEMSS   PIC  99.                              00000260
             05 COMM-DATE-SEMAA   PIC  99.                              00000270
             05 COMM-DATE-SEMNU   PIC  99.                              00000280
          02 COMM-DATE-FILLER     PIC  X(08).                           00000290
      * ATTRIBUTS BMS ------------------------------------------------  00000292
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 FILLER               PIC S9(4) COMP VALUE -1.              00000293
      *--                                                                       
          02 FILLER               PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-TD00-ZMAP       PIC  X(1).                            00000294
          02 COMM-TD00-REDEFINES  PIC  X(3000).                         00000302
          02 COMM-TD00 REDEFINES COMM-TD00-REDEFINES.                   00000302
      * ZONES APPLICATIVES MDT05 -------------------------------------  00000296
             03 COMM-MTD05           PIC X(200).                        00000303
             03 COMM-MTD05-R REDEFINES COMM-MTD05.                      00000303
                05 COMM-MTD05-ERREUR    PIC X(70).                      00000303
                05 COMM-MTD05-SELECTION PIC X(10).                      00000303
                05 COMM-MTD05-LIBSEL    PIC X(20).                      00000303
                05 COMM-MTD05-NVENTE    PIC X(7).                       00000303
                05 COMM-MTD05-PARAM     PIC X(20).                      00000303
                05 COMM-MTD05-NDOSSIER  PIC S9(5) USAGE COMP-3.                 
      * ZONES APPLICATIVES TD00 --------------------------------------  00000296
             03 COMM-ETD00-R         PIC X(300).                                
             03 COMM-ETD00  REDEFINES COMM-ETD00-R.                             
                05 COMM-TD00-NSOCIETE   PIC X(3).                          00000
                05 COMM-TD00-LSOCIETE   PIC X(20).                         00000
                05 COMM-TD00-NLIEU      PIC X(3).                          00000
                05 COMM-TD00-LLIEU      PIC X(20).                         00000
                05 COMM-TD00-NVENTE     PIC X(7).                               
                05 COMM-TD00-NDOSSIER   PIC S9(5) USAGE COMP-3.                 
                05 COMM-TD00-CTYPE      PIC X(5).                          00000
                05 COMM-TD00-INITIALES  PIC X(2).                          00000
                05 COMM-TD00-CMARQ      PIC X(5).                          00000
                05 COMM-TD00-LNOM       PIC X(25).                              
                05 COMM-TD00-DCREATION  PIC X(8).                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD00-NBL        PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD00-NBL        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD00-NBTS       PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD00-NBTS       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD00-NBLDTS     PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD00-NBLDTS     PIC S9(4) COMP-5.                       
      *}                                                                        
                05 COMM-TD00-IMPRIM     PIC X(4).                          00000
                05 COMM-TD00-SIEGE      PIC X.                             00000
                05 COMM-TD00-PROGRAMME  PIC X(5).                          00000
                05 COMM-TD00-CODRECH    PIC X(5).                          00000
                05 COMM-TD00-VALRECH    PIC X(30).                              
                05 COMM-TD00-DATDEB     PIC X(8).                          00000
                05 COMM-TD00-DATFIN     PIC X(8).                          00000
                05 COMM-TD00-TYPE       PIC X(4).                          00000
                05 COMM-TD00-NOPTION    PIC X(2).                          00000
                05 COMM-TD00-LOPTION    PIC X(20).                         00000
                05 COMM-TD00-PF5        PIC X(1).                          00000
                05 COMM-TD00-PF6        PIC X(1).                          00000
                05 COMM-TD00-J-1M       PIC X(8).                               
                05 COMM-TD00-TYPDOSS    PIC X(5).                               
AL1110          05 COMM-TD00-CDOSSIER   PIC X(05).                              
      * ZONES APPLICATIVES EDT01 -------------------------------------  00000296
             03 COMM-ETD01-R         PIC X(200).                                
             03 COMM-ETD01  REDEFINES COMM-ETD01-R.                             
                05 COMM-TD01-PROGRAMME  PIC X(5).                          00000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD01-NBDOSSIER  PIC S9(4) BINARY.                       
      *--                                                                       
                05 COMM-TD01-NBDOSSIER  PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD01-PAGEA      PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD01-PAGEA      PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD01-NBP        PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD01-NBP        PIC S9(4) COMP-5.                       
      *}                                                                        
                05 COMM-TD01-NVENTE     PIC X(7).                               
                05 COMM-TD01-NDOSSIER   PIC S9(5) USAGE COMP-3.                 
                05 COMM-TD01-CMARQ      PIC X(5).                               
                05 COMM-TD01-CTYPE      PIC X(5).                               
                05 COMM-TD01-DCREATION  PIC X(8).                               
                05 COMM-TD01-LNOM       PIC X(25).                              
                05 COMM-TD01-CDOSSIER   PIC X(5).                               
                05 COMM-TD01-NLIEU      PIC X(3).                               
                05 COMM-TD01-LLIEU      PIC X(20).                         00000
                05 COMM-TD01-DANNUL     PIC X(8).                               
AL2101          05 COMM-TD01-DENVOI     PIC X(8).                               
                05 COMM-TD01-WARI       PIC X(5).                               
                05 COMM-TD01-DARI       PIC X(8).                               
                05 COMM-TD01-WARO       PIC X(5).                               
                05 COMM-TD01-DARO       PIC X(8).                               
                05 COMM-TD01-INITIALES  PIC X(2).                               
                05 COMM-TD01-ORIGINE    PIC X(1).                               
      * ZONES APPLICATIVES ETD02 -------------------------------------  00000296
             03 COMM-ETD02-R         PIC X(200).                                
             03 COMM-ETD02  REDEFINES COMM-ETD02-R.                             
                05 COMM-TD02-PROGRAMME  PIC X(5).                          00000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD02-NBL        PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD02-NBL        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD02-NBTS       PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD02-NBTS       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD02-NBLDTS     PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD02-NBLDTS     PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD02-PAGEA      PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD02-PAGEA      PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD02-NBP        PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD02-NBP        PIC S9(4) COMP-5.                       
      *}                                                                        
                05 COMM-TD02-NOPTION    PIC X(2).                          00000
      * ZONES APPLICATIVES EDT03 -------------------------------------  00000296
             03 COMM-ETD03-R         PIC X(200).                                
             03 COMM-ETD03  REDEFINES COMM-ETD03-R.                             
                05 COMM-TD03-PROGRAMME  PIC X(5).                          00000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD03-NBL        PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD03-NBL        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD03-NBTS       PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD03-NBTS       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD03-NBLDTS     PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD03-NBLDTS     PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD03-PAGEA      PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD03-PAGEA      PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD03-NBP        PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD03-NBP        PIC S9(4) COMP-5.                       
      *}                                                                        
                05 COMM-TD03-NOPTION    PIC X(2).                          00000
      * ZONES APPLICATIVES EDT04 -------------------------------------  00000296
             03 COMM-ETD04-R         PIC X(200).                                
             03 COMM-ETD04  REDEFINES COMM-ETD04-R.                             
                05 COMM-TD04-PROGRAMME  PIC X(5).                          00000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD04-NBL        PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD04-NBL        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD04-NBTS       PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD04-NBTS       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD04-NBLDTS     PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD04-NBLDTS     PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD04-PAGEA      PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD04-PAGEA      PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD04-NBP        PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD04-NBP        PIC S9(4) COMP-5.                       
      *}                                                                        
      * ZONES APPLICATIVES EDT05 -------------------------------------  00000296
             03 COMM-ETD05-R         PIC X(200).                                
             03 COMM-ETD05  REDEFINES COMM-ETD05-R.                             
                05 COMM-TD05-PROGRAMME  PIC X(5).                          00000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD05-NBL        PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD05-NBL        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD05-NBTS       PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD05-NBTS       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD05-NBLDTS     PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD05-NBLDTS     PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD05-PAGEA      PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD05-PAGEA      PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD05-NBP        PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD05-NBP        PIC S9(4) COMP-5.                       
      *}                                                                        
                05 COMM-TD05-DATE       PIC X(08).                         00000
      * ZONES APPLICATIVES EDT06 -------------------------------------  00000296
             03 COMM-ETD06-R         PIC X(200).                                
             03 COMM-ETD06  REDEFINES COMM-ETD06-R.                             
                05 COMM-TD06-PROGRAMME  PIC X(5).                          00000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD06-NBL        PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD06-NBL        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD06-NBTS       PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD06-NBTS       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD06-NBLDTS     PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD06-NBLDTS     PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD06-PAGEA      PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD06-PAGEA      PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD06-NBP        PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD06-NBP        PIC S9(4) COMP-5.                       
      *}                                                                        
      * ZONES APPLICATIVES EDT07 -------------------------------------  00000296
             03 COMM-ETD07-R         PIC X(200).                                
             03 COMM-ETD07  REDEFINES COMM-ETD07-R.                             
                05 COMM-TD07-PROGRAMME  PIC X(5).                          00000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD07-NBL        PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD07-NBL        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD07-NBTS       PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD07-NBTS       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD07-NBLDTS     PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD07-NBLDTS     PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD07-PAGEA      PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD07-PAGEA      PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-TD07-NBP        PIC S9(4) BINARY.                  00000
      *--                                                                       
                05 COMM-TD07-NBP        PIC S9(4) COMP-5.                       
      *}                                                                        
      * ZONES APPLICATIVES MDT02 -------------------------------------  00000296
             03 COMM-MTD02           PIC X(200).                        00000303
             03 COMM-MTD02-R REDEFINES COMM-MTD02.                      00000303
                05 COMM-MTD02-ERREUR      PIC X(70).                    00000303
                05 COMM-MTD02-CCTRL       PIC X(05).                    00000303
                05 COMM-MTD02-FCTRL       PIC X(01).                    00000303
                05 COMM-MTD02-E-VALCTRL   PIC X(50).                    00000303
                05 COMM-MTD02-S-VALCTRL   PIC X(50).                    00000303
                05 COMM-MTD02-QLONG-N     PIC 9(02).                    00000303
                05 COMM-MTD02-QLONGDEC-N  PIC 9(02).                    00000303
      * ZONES APPLICATIVES MDT03 -------------------------------------  00000296
             03 COMM-MTD03           PIC X(200).                        00000303
             03 COMM-MTD03-R REDEFINES COMM-MTD03.                      00000303
                05 COMM-MTD03-CODERR      PIC X(02).                            
                05 COMM-MTD03-ERREUR      PIC X(70).                    00000303
                05 COMM-MTD03-OPTION      PIC X(01).                    00000303
       01  FILLER                         PIC X(864).                   00000050
                                                                                
