      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVPO0100                      *        
      ******************************************************************        
       01  RVPO0100.                                                            
           10 PO01-NUECH                PIC S9(9)V USAGE COMP-3.                
           10 PO01-PRFDOS               PIC X(3).                               
           10 PO01-NUDOS                PIC S9(9)V USAGE COMP-3.                
           10 PO01-DATREC               PIC X(8).                               
           10 PO01-NSOCD                PIC X(3).                               
           10 PO01-NLIEUD               PIC X(3).                               
           10 PO01-NSOCV                PIC X(3).                               
           10 PO01-NLIEUV               PIC X(3).                               
           10 PO01-NVENTE               PIC X(7).                               
           10 PO01-NCODIC               PIC X(7).                               
           10 PO01-NSEQNQ               PIC S9(5)V USAGE COMP-3.                
           10 PO01-PACHAT               PIC S9(7)V9(2) USAGE COMP-3.            
           10 PO01-DTFGAR               PIC X(8).                               
           10 PO01-NSOCE                PIC X(3).                               
           10 PO01-NLIEUE               PIC X(3).                               
           10 PO01-NSOCR                PIC X(3).                               
           10 PO01-NLIEUR               PIC X(3).                               
           10 PO01-SLIEUS               PIC X(3).                               
           10 PO01-PECHA                PIC S9(7)V9(2) USAGE COMP-3.            
           10 PO01-RESECH               PIC X(1).                               
           10 PO01-NNSOC                PIC X(3).                               
           10 PO01-NNLIEU               PIC X(3).                               
           10 PO01-NNVENTE              PIC X(7).                               
           10 PO01-NNCODIC              PIC X(7).                               
           10 PO01-NNSEQNQ              PIC S9(5)V USAGE COMP-3.                
           10 PO01-ETATECH              PIC X(1).                               
           10 PO01-ACID                 PIC X(10).                              
           10 PO01-DSYST                PIC S9(13)V USAGE COMP-3.               
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVPO0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-NUECH-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-NUECH-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-PRFDOS-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-PRFDOS-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-NUDOS-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-NUDOS-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-DATREC-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-DATREC-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-NSOCD-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-NSOCD-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-NLIEUD-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-NLIEUD-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-NSOCV-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-NSOCV-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-NLIEUV-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-NLIEUV-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-NVENTE-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-NVENTE-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-NCODIC-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-NCODIC-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-NSEQNQ-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-NSEQNQ-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-PACHAT-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-PACHAT-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-DTFGAR-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-DTFGAR-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-NSOCE-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-NSOCE-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-NLIEUE-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-NLIEUE-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-NSOCR-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-NSOCR-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-NLIEUR-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-NLIEUR-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-SLIEUS-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-SLIEUS-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-PECHA-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-PECHA-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-RESECH-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-RESECH-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-NNSOC-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-NNSOC-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-NNLIEU-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-NNLIEU-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-NNVENTE-F            PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-NNVENTE-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-NNCODIC-F            PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-NNCODIC-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-NNSEQNQ-F            PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-NNSEQNQ-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-ETATECH-F            PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-ETATECH-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-ACID-F               PIC S9(4) COMP.                         
      *--                                                                       
           10 PO01-ACID-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 PO01-DSYST-F              PIC S9(4) COMP.                         
      *                                                                         
      *--                                                                       
           10 PO01-DSYST-F              PIC S9(4) COMP-5.                       
                                                                                
      *}                                                                        
