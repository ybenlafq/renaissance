      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESL23   ESL23                                              00000020
      ***************************************************************** 00000030
       01   ESL23I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPAGESI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOCCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUCL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNLIEUCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEUCF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNLIEUCI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPDOCL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MTYPDOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPDOCF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MTYPDOCI  PIC X(2).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMDOCL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNUMDOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUMDOCF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNUMDOCI  PIC X(7).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMSL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCFAMSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAMSF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCFAMSI   PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQSL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCMARQSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQSF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCMARQSI  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICSL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MNCODICSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODICSF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNCODICSI      PIC X(7).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEBUTL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDDEBUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDEBUTF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDDEBUTI  PIC X(6).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDFINL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDFINF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDFINI    PIC X(6).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCOL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MSOCOL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCOF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MSOCOI    PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUOL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLIEUOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEUOF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLIEUOI   PIC X(3).                                       00000650
           02 MNSOCD OCCURS   13 TIMES .                                00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNSOCI  PIC X(3).                                       00000700
           02 MNLIEUD OCCURS   13 TIMES .                               00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000720
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000730
             03 FILLER  PIC X(4).                                       00000740
             03 MNLIEUI      PIC X(3).                                  00000750
           02 MTYPED OCCURS   13 TIMES .                                00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPEL  COMP PIC S9(4).                                 00000770
      *--                                                                       
             03 MTYPEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTYPEF  PIC X.                                          00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MTYPEI  PIC X(2).                                       00000800
           02 MNUMD OCCURS   13 TIMES .                                 00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNUML   COMP PIC S9(4).                                 00000820
      *--                                                                       
             03 MNUML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNUMF   PIC X.                                          00000830
             03 FILLER  PIC X(4).                                       00000840
             03 MNUMI   PIC X(7).                                       00000850
           02 MCFAMD OCCURS   13 TIMES .                                00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MCFAMI  PIC X(5).                                       00000900
           02 MCMARQD OCCURS   13 TIMES .                               00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000920
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000930
             03 FILLER  PIC X(4).                                       00000940
             03 MCMARQI      PIC X(5).                                  00000950
           02 MNCODICD OCCURS   13 TIMES .                              00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000970
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000980
             03 FILLER  PIC X(4).                                       00000990
             03 MNCODICI     PIC X(7).                                  00001000
           02 MNSOCOD OCCURS   13 TIMES .                               00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCOL      COMP PIC S9(4).                            00001020
      *--                                                                       
             03 MNSOCOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNSOCOF      PIC X.                                     00001030
             03 FILLER  PIC X(4).                                       00001040
             03 MNSOCOI      PIC X(3).                                  00001050
           02 MNLIEUOD OCCURS   13 TIMES .                              00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUOL     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MNLIEUOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLIEUOF     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MNLIEUOI     PIC X(3).                                  00001100
           02 MNSOCDD OCCURS   13 TIMES .                               00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCDL      COMP PIC S9(4).                            00001120
      *--                                                                       
             03 MNSOCDL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNSOCDF      PIC X.                                     00001130
             03 FILLER  PIC X(4).                                       00001140
             03 MNSOCDI      PIC X(3).                                  00001150
           02 MNLIEUDD OCCURS   13 TIMES .                              00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUDL     COMP PIC S9(4).                            00001170
      *--                                                                       
             03 MNLIEUDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLIEUDF     PIC X.                                     00001180
             03 FILLER  PIC X(4).                                       00001190
             03 MNLIEUDI     PIC X(3).                                  00001200
           02 MQEXPD OCCURS   13 TIMES .                                00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPL  COMP PIC S9(4).                                 00001220
      *--                                                                       
             03 MQEXPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQEXPF  PIC X.                                          00001230
             03 FILLER  PIC X(4).                                       00001240
             03 MQEXPI  PIC X(5).                                       00001250
           02 MQRECD OCCURS   13 TIMES .                                00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQRECL  COMP PIC S9(4).                                 00001270
      *--                                                                       
             03 MQRECL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQRECF  PIC X.                                          00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MQRECI  PIC X(5).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQEXPTOTL      COMP PIC S9(4).                            00001310
      *--                                                                       
           02 MQEXPTOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQEXPTOTF      PIC X.                                     00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MQEXPTOTI      PIC X(5).                                  00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQRECTOTL      COMP PIC S9(4).                            00001350
      *--                                                                       
           02 MQRECTOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQRECTOTF      PIC X.                                     00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MQRECTOTI      PIC X(5).                                  00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MLIBERRI  PIC X(78).                                      00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MCODTRAI  PIC X(4).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MCICSI    PIC X(5).                                       00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MNETNAMI  PIC X(8).                                       00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MSCREENI  PIC X(4).                                       00001580
      ***************************************************************** 00001590
      * SDF: ESL23   ESL23                                              00001600
      ***************************************************************** 00001610
       01   ESL23O REDEFINES ESL23I.                                    00001620
           02 FILLER    PIC X(12).                                      00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MDATJOUA  PIC X.                                          00001650
           02 MDATJOUC  PIC X.                                          00001660
           02 MDATJOUP  PIC X.                                          00001670
           02 MDATJOUH  PIC X.                                          00001680
           02 MDATJOUV  PIC X.                                          00001690
           02 MDATJOUO  PIC X(10).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MTIMJOUA  PIC X.                                          00001720
           02 MTIMJOUC  PIC X.                                          00001730
           02 MTIMJOUP  PIC X.                                          00001740
           02 MTIMJOUH  PIC X.                                          00001750
           02 MTIMJOUV  PIC X.                                          00001760
           02 MTIMJOUO  PIC X(5).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MNPAGEA   PIC X.                                          00001790
           02 MNPAGEC   PIC X.                                          00001800
           02 MNPAGEP   PIC X.                                          00001810
           02 MNPAGEH   PIC X.                                          00001820
           02 MNPAGEV   PIC X.                                          00001830
           02 MNPAGEO   PIC X(3).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MNBPAGESA      PIC X.                                     00001860
           02 MNBPAGESC PIC X.                                          00001870
           02 MNBPAGESP PIC X.                                          00001880
           02 MNBPAGESH PIC X.                                          00001890
           02 MNBPAGESV PIC X.                                          00001900
           02 MNBPAGESO      PIC X(3).                                  00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MNSOCCA   PIC X.                                          00001930
           02 MNSOCCC   PIC X.                                          00001940
           02 MNSOCCP   PIC X.                                          00001950
           02 MNSOCCH   PIC X.                                          00001960
           02 MNSOCCV   PIC X.                                          00001970
           02 MNSOCCO   PIC X(3).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MNLIEUCA  PIC X.                                          00002000
           02 MNLIEUCC  PIC X.                                          00002010
           02 MNLIEUCP  PIC X.                                          00002020
           02 MNLIEUCH  PIC X.                                          00002030
           02 MNLIEUCV  PIC X.                                          00002040
           02 MNLIEUCO  PIC X(3).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MTYPDOCA  PIC X.                                          00002070
           02 MTYPDOCC  PIC X.                                          00002080
           02 MTYPDOCP  PIC X.                                          00002090
           02 MTYPDOCH  PIC X.                                          00002100
           02 MTYPDOCV  PIC X.                                          00002110
           02 MTYPDOCO  PIC X(2).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MNUMDOCA  PIC X.                                          00002140
           02 MNUMDOCC  PIC X.                                          00002150
           02 MNUMDOCP  PIC X.                                          00002160
           02 MNUMDOCH  PIC X.                                          00002170
           02 MNUMDOCV  PIC X.                                          00002180
           02 MNUMDOCO  PIC X(7).                                       00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MCFAMSA   PIC X.                                          00002210
           02 MCFAMSC   PIC X.                                          00002220
           02 MCFAMSP   PIC X.                                          00002230
           02 MCFAMSH   PIC X.                                          00002240
           02 MCFAMSV   PIC X.                                          00002250
           02 MCFAMSO   PIC X(5).                                       00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCMARQSA  PIC X.                                          00002280
           02 MCMARQSC  PIC X.                                          00002290
           02 MCMARQSP  PIC X.                                          00002300
           02 MCMARQSH  PIC X.                                          00002310
           02 MCMARQSV  PIC X.                                          00002320
           02 MCMARQSO  PIC X(5).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MNCODICSA      PIC X.                                     00002350
           02 MNCODICSC PIC X.                                          00002360
           02 MNCODICSP PIC X.                                          00002370
           02 MNCODICSH PIC X.                                          00002380
           02 MNCODICSV PIC X.                                          00002390
           02 MNCODICSO      PIC X(7).                                  00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MDDEBUTA  PIC X.                                          00002420
           02 MDDEBUTC  PIC X.                                          00002430
           02 MDDEBUTP  PIC X.                                          00002440
           02 MDDEBUTH  PIC X.                                          00002450
           02 MDDEBUTV  PIC X.                                          00002460
           02 MDDEBUTO  PIC X(6).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MDFINA    PIC X.                                          00002490
           02 MDFINC    PIC X.                                          00002500
           02 MDFINP    PIC X.                                          00002510
           02 MDFINH    PIC X.                                          00002520
           02 MDFINV    PIC X.                                          00002530
           02 MDFINO    PIC X(6).                                       00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MSOCOA    PIC X.                                          00002560
           02 MSOCOC    PIC X.                                          00002570
           02 MSOCOP    PIC X.                                          00002580
           02 MSOCOH    PIC X.                                          00002590
           02 MSOCOV    PIC X.                                          00002600
           02 MSOCOO    PIC X(3).                                       00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MLIEUOA   PIC X.                                          00002630
           02 MLIEUOC   PIC X.                                          00002640
           02 MLIEUOP   PIC X.                                          00002650
           02 MLIEUOH   PIC X.                                          00002660
           02 MLIEUOV   PIC X.                                          00002670
           02 MLIEUOO   PIC X(3).                                       00002680
           02 DFHMS1 OCCURS   13 TIMES .                                00002690
             03 FILLER       PIC X(2).                                  00002700
             03 MNSOCA  PIC X.                                          00002710
             03 MNSOCC  PIC X.                                          00002720
             03 MNSOCP  PIC X.                                          00002730
             03 MNSOCH  PIC X.                                          00002740
             03 MNSOCV  PIC X.                                          00002750
             03 MNSOCO  PIC X(3).                                       00002760
           02 DFHMS2 OCCURS   13 TIMES .                                00002770
             03 FILLER       PIC X(2).                                  00002780
             03 MNLIEUA      PIC X.                                     00002790
             03 MNLIEUC PIC X.                                          00002800
             03 MNLIEUP PIC X.                                          00002810
             03 MNLIEUH PIC X.                                          00002820
             03 MNLIEUV PIC X.                                          00002830
             03 MNLIEUO      PIC X(3).                                  00002840
           02 DFHMS3 OCCURS   13 TIMES .                                00002850
             03 FILLER       PIC X(2).                                  00002860
             03 MTYPEA  PIC X.                                          00002870
             03 MTYPEC  PIC X.                                          00002880
             03 MTYPEP  PIC X.                                          00002890
             03 MTYPEH  PIC X.                                          00002900
             03 MTYPEV  PIC X.                                          00002910
             03 MTYPEO  PIC X(2).                                       00002920
           02 DFHMS4 OCCURS   13 TIMES .                                00002930
             03 FILLER       PIC X(2).                                  00002940
             03 MNUMA   PIC X.                                          00002950
             03 MNUMC   PIC X.                                          00002960
             03 MNUMP   PIC X.                                          00002970
             03 MNUMH   PIC X.                                          00002980
             03 MNUMV   PIC X.                                          00002990
             03 MNUMO   PIC X(7).                                       00003000
           02 DFHMS5 OCCURS   13 TIMES .                                00003010
             03 FILLER       PIC X(2).                                  00003020
             03 MCFAMA  PIC X.                                          00003030
             03 MCFAMC  PIC X.                                          00003040
             03 MCFAMP  PIC X.                                          00003050
             03 MCFAMH  PIC X.                                          00003060
             03 MCFAMV  PIC X.                                          00003070
             03 MCFAMO  PIC X(5).                                       00003080
           02 DFHMS6 OCCURS   13 TIMES .                                00003090
             03 FILLER       PIC X(2).                                  00003100
             03 MCMARQA      PIC X.                                     00003110
             03 MCMARQC PIC X.                                          00003120
             03 MCMARQP PIC X.                                          00003130
             03 MCMARQH PIC X.                                          00003140
             03 MCMARQV PIC X.                                          00003150
             03 MCMARQO      PIC X(5).                                  00003160
           02 DFHMS7 OCCURS   13 TIMES .                                00003170
             03 FILLER       PIC X(2).                                  00003180
             03 MNCODICA     PIC X.                                     00003190
             03 MNCODICC     PIC X.                                     00003200
             03 MNCODICP     PIC X.                                     00003210
             03 MNCODICH     PIC X.                                     00003220
             03 MNCODICV     PIC X.                                     00003230
             03 MNCODICO     PIC X(7).                                  00003240
           02 DFHMS8 OCCURS   13 TIMES .                                00003250
             03 FILLER       PIC X(2).                                  00003260
             03 MNSOCOA      PIC X.                                     00003270
             03 MNSOCOC PIC X.                                          00003280
             03 MNSOCOP PIC X.                                          00003290
             03 MNSOCOH PIC X.                                          00003300
             03 MNSOCOV PIC X.                                          00003310
             03 MNSOCOO      PIC X(3).                                  00003320
           02 DFHMS9 OCCURS   13 TIMES .                                00003330
             03 FILLER       PIC X(2).                                  00003340
             03 MNLIEUOA     PIC X.                                     00003350
             03 MNLIEUOC     PIC X.                                     00003360
             03 MNLIEUOP     PIC X.                                     00003370
             03 MNLIEUOH     PIC X.                                     00003380
             03 MNLIEUOV     PIC X.                                     00003390
             03 MNLIEUOO     PIC X(3).                                  00003400
           02 DFHMS10 OCCURS   13 TIMES .                               00003410
             03 FILLER       PIC X(2).                                  00003420
             03 MNSOCDA      PIC X.                                     00003430
             03 MNSOCDC PIC X.                                          00003440
             03 MNSOCDP PIC X.                                          00003450
             03 MNSOCDH PIC X.                                          00003460
             03 MNSOCDV PIC X.                                          00003470
             03 MNSOCDO      PIC X(3).                                  00003480
           02 DFHMS11 OCCURS   13 TIMES .                               00003490
             03 FILLER       PIC X(2).                                  00003500
             03 MNLIEUDA     PIC X.                                     00003510
             03 MNLIEUDC     PIC X.                                     00003520
             03 MNLIEUDP     PIC X.                                     00003530
             03 MNLIEUDH     PIC X.                                     00003540
             03 MNLIEUDV     PIC X.                                     00003550
             03 MNLIEUDO     PIC X(3).                                  00003560
           02 DFHMS12 OCCURS   13 TIMES .                               00003570
             03 FILLER       PIC X(2).                                  00003580
             03 MQEXPA  PIC X.                                          00003590
             03 MQEXPC  PIC X.                                          00003600
             03 MQEXPP  PIC X.                                          00003610
             03 MQEXPH  PIC X.                                          00003620
             03 MQEXPV  PIC X.                                          00003630
             03 MQEXPO  PIC X(5).                                       00003640
           02 DFHMS13 OCCURS   13 TIMES .                               00003650
             03 FILLER       PIC X(2).                                  00003660
             03 MQRECA  PIC X.                                          00003670
             03 MQRECC  PIC X.                                          00003680
             03 MQRECP  PIC X.                                          00003690
             03 MQRECH  PIC X.                                          00003700
             03 MQRECV  PIC X.                                          00003710
             03 MQRECO  PIC X(5).                                       00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MQEXPTOTA      PIC X.                                     00003740
           02 MQEXPTOTC PIC X.                                          00003750
           02 MQEXPTOTP PIC X.                                          00003760
           02 MQEXPTOTH PIC X.                                          00003770
           02 MQEXPTOTV PIC X.                                          00003780
           02 MQEXPTOTO      PIC X(5).                                  00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MQRECTOTA      PIC X.                                     00003810
           02 MQRECTOTC PIC X.                                          00003820
           02 MQRECTOTP PIC X.                                          00003830
           02 MQRECTOTH PIC X.                                          00003840
           02 MQRECTOTV PIC X.                                          00003850
           02 MQRECTOTO      PIC X(5).                                  00003860
           02 FILLER    PIC X(2).                                       00003870
           02 MLIBERRA  PIC X.                                          00003880
           02 MLIBERRC  PIC X.                                          00003890
           02 MLIBERRP  PIC X.                                          00003900
           02 MLIBERRH  PIC X.                                          00003910
           02 MLIBERRV  PIC X.                                          00003920
           02 MLIBERRO  PIC X(78).                                      00003930
           02 FILLER    PIC X(2).                                       00003940
           02 MCODTRAA  PIC X.                                          00003950
           02 MCODTRAC  PIC X.                                          00003960
           02 MCODTRAP  PIC X.                                          00003970
           02 MCODTRAH  PIC X.                                          00003980
           02 MCODTRAV  PIC X.                                          00003990
           02 MCODTRAO  PIC X(4).                                       00004000
           02 FILLER    PIC X(2).                                       00004010
           02 MCICSA    PIC X.                                          00004020
           02 MCICSC    PIC X.                                          00004030
           02 MCICSP    PIC X.                                          00004040
           02 MCICSH    PIC X.                                          00004050
           02 MCICSV    PIC X.                                          00004060
           02 MCICSO    PIC X(5).                                       00004070
           02 FILLER    PIC X(2).                                       00004080
           02 MNETNAMA  PIC X.                                          00004090
           02 MNETNAMC  PIC X.                                          00004100
           02 MNETNAMP  PIC X.                                          00004110
           02 MNETNAMH  PIC X.                                          00004120
           02 MNETNAMV  PIC X.                                          00004130
           02 MNETNAMO  PIC X(8).                                       00004140
           02 FILLER    PIC X(2).                                       00004150
           02 MSCREENA  PIC X.                                          00004160
           02 MSCREENC  PIC X.                                          00004170
           02 MSCREENP  PIC X.                                          00004180
           02 MSCREENH  PIC X.                                          00004190
           02 MSCREENV  PIC X.                                          00004200
           02 MSCREENO  PIC X(4).                                       00004210
                                                                                
