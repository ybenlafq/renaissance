      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: Eem49   Eem49                                              00000020
      ***************************************************************** 00000030
       01   EEM49I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * Date systeme du jour                                            00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * Heure systeme du jour                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * Numero de page                                                  00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOPAGEL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MNOPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOPAGEF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MNOPAGEI  PIC X(3).                                       00000200
      * Nombre de pages totales                                         00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGEL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPAGEF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBPAGEI  PIC X(3).                                       00000250
      * Numero societe                                                  00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MNSOCI    PIC X(3).                                       00000300
      * Numero magasin                                                  00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MNMAGI    PIC X(3).                                       00000350
      * Numero caisse                                                   00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCAISL   COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MNCAISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCAISF   PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MNCAISI   PIC X(3).                                       00000400
      * Date de transaction                                             00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDTRANSL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDTRANSF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDTRANSI  PIC X(10).                                      00000450
      * Identification operateur                                        00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOPERL   COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MNOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNOPERF   PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MNOPERI   PIC X(7).                                       00000500
      * Numero de transaction                                           00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTRANSL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MNTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNTRANSF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MNTRANSI  PIC X(8).                                       00000550
      * Heure de la transaction                                         00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDHTRANSL      COMP PIC S9(4).                            00000570
      *--                                                                       
           02 MDHTRANSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDHTRANSF      PIC X.                                     00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MDHTRANSI      PIC X(5).                                  00000600
      * Type de transaction                                             00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTTRANSL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MTTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTTRANSF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MTTRANSI  PIC X(3).                                       00000650
      * Libelle type trans. si select.                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETATL   COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLETATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLETATF   PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLETATI   PIC X(25).                                      00000700
      * Libelle colonne / type trans.                                   00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTADML   COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MLTADML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLTADMF   PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MLTADMI   PIC X(57).                                      00000750
      * Libelle type de transaction                                     00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTTRANSL      COMP PIC S9(4).                            00000770
      *--                                                                       
           02 MLTTRANSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLTTRANSF      PIC X.                                     00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MLTTRANSI      PIC X(19).                                 00000800
      * Montant de la transaction                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMONTRSL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MMONTRSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMONTRSF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MMONTRSI  PIC X(10).                                      00000850
      * Code de la devise/transaction                                   00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00000870
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCDEVISEI      PIC X.                                     00000900
           02 MLCTADMI OCCURS   12 TIMES .                              00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTADML      COMP PIC S9(4).                            00000920
      *--                                                                       
             03 MCTADML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCTADMF      PIC X.                                     00000930
             03 FILLER  PIC X(4).                                       00000940
             03 MCTADMI      PIC X(41).                                 00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MZONCMDI  PIC X(15).                                      00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MLIBERRI  PIC X(58).                                      00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MCODTRAI  PIC X(4).                                       00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MCICSI    PIC X(5).                                       00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MNETNAMI  PIC X(8).                                       00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001160
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001170
           02 FILLER    PIC X(4).                                       00001180
           02 MSCREENI  PIC X(4).                                       00001190
      ***************************************************************** 00001200
      * SDF: Eem49   Eem49                                              00001210
      ***************************************************************** 00001220
       01   EEM49O REDEFINES EEM49I.                                    00001230
           02 FILLER    PIC X(12).                                      00001240
      * Date systeme du jour                                            00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MDATJOUA  PIC X.                                          00001270
           02 MDATJOUC  PIC X.                                          00001280
           02 MDATJOUP  PIC X.                                          00001290
           02 MDATJOUH  PIC X.                                          00001300
           02 MDATJOUV  PIC X.                                          00001310
           02 MDATJOUO  PIC X(10).                                      00001320
      * Heure systeme du jour                                           00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MTIMJOUA  PIC X.                                          00001350
           02 MTIMJOUC  PIC X.                                          00001360
           02 MTIMJOUP  PIC X.                                          00001370
           02 MTIMJOUH  PIC X.                                          00001380
           02 MTIMJOUV  PIC X.                                          00001390
           02 MTIMJOUO  PIC X(5).                                       00001400
      * Numero de page                                                  00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNOPAGEA  PIC X.                                          00001430
           02 MNOPAGEC  PIC X.                                          00001440
           02 MNOPAGEP  PIC X.                                          00001450
           02 MNOPAGEH  PIC X.                                          00001460
           02 MNOPAGEV  PIC X.                                          00001470
           02 MNOPAGEO  PIC X(3).                                       00001480
      * Nombre de pages totales                                         00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MNBPAGEA  PIC X.                                          00001510
           02 MNBPAGEC  PIC X.                                          00001520
           02 MNBPAGEP  PIC X.                                          00001530
           02 MNBPAGEH  PIC X.                                          00001540
           02 MNBPAGEV  PIC X.                                          00001550
           02 MNBPAGEO  PIC X(3).                                       00001560
      * Numero societe                                                  00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MNSOCA    PIC X.                                          00001590
           02 MNSOCC    PIC X.                                          00001600
           02 MNSOCP    PIC X.                                          00001610
           02 MNSOCH    PIC X.                                          00001620
           02 MNSOCV    PIC X.                                          00001630
           02 MNSOCO    PIC X(3).                                       00001640
      * Numero magasin                                                  00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MNMAGA    PIC X.                                          00001670
           02 MNMAGC    PIC X.                                          00001680
           02 MNMAGP    PIC X.                                          00001690
           02 MNMAGH    PIC X.                                          00001700
           02 MNMAGV    PIC X.                                          00001710
           02 MNMAGO    PIC X(3).                                       00001720
      * Numero caisse                                                   00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MNCAISA   PIC X.                                          00001750
           02 MNCAISC   PIC X.                                          00001760
           02 MNCAISP   PIC X.                                          00001770
           02 MNCAISH   PIC X.                                          00001780
           02 MNCAISV   PIC X.                                          00001790
           02 MNCAISO   PIC X(3).                                       00001800
      * Date de transaction                                             00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MDTRANSA  PIC X.                                          00001830
           02 MDTRANSC  PIC X.                                          00001840
           02 MDTRANSP  PIC X.                                          00001850
           02 MDTRANSH  PIC X.                                          00001860
           02 MDTRANSV  PIC X.                                          00001870
           02 MDTRANSO  PIC X(10).                                      00001880
      * Identification operateur                                        00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MNOPERA   PIC X.                                          00001910
           02 MNOPERC   PIC X.                                          00001920
           02 MNOPERP   PIC X.                                          00001930
           02 MNOPERH   PIC X.                                          00001940
           02 MNOPERV   PIC X.                                          00001950
           02 MNOPERO   PIC X(7).                                       00001960
      * Numero de transaction                                           00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MNTRANSA  PIC X.                                          00001990
           02 MNTRANSC  PIC X.                                          00002000
           02 MNTRANSP  PIC X.                                          00002010
           02 MNTRANSH  PIC X.                                          00002020
           02 MNTRANSV  PIC X.                                          00002030
           02 MNTRANSO  PIC X(8).                                       00002040
      * Heure de la transaction                                         00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MDHTRANSA      PIC X.                                     00002070
           02 MDHTRANSC PIC X.                                          00002080
           02 MDHTRANSP PIC X.                                          00002090
           02 MDHTRANSH PIC X.                                          00002100
           02 MDHTRANSV PIC X.                                          00002110
           02 MDHTRANSO      PIC X(5).                                  00002120
      * Type de transaction                                             00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MTTRANSA  PIC X.                                          00002150
           02 MTTRANSC  PIC X.                                          00002160
           02 MTTRANSP  PIC X.                                          00002170
           02 MTTRANSH  PIC X.                                          00002180
           02 MTTRANSV  PIC X.                                          00002190
           02 MTTRANSO  PIC X(3).                                       00002200
      * Libelle type trans. si select.                                  00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MLETATA   PIC X.                                          00002230
           02 MLETATC   PIC X.                                          00002240
           02 MLETATP   PIC X.                                          00002250
           02 MLETATH   PIC X.                                          00002260
           02 MLETATV   PIC X.                                          00002270
           02 MLETATO   PIC X(25).                                      00002280
      * Libelle colonne / type trans.                                   00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MLTADMA   PIC X.                                          00002310
           02 MLTADMC   PIC X.                                          00002320
           02 MLTADMP   PIC X.                                          00002330
           02 MLTADMH   PIC X.                                          00002340
           02 MLTADMV   PIC X.                                          00002350
           02 MLTADMO   PIC X(57).                                      00002360
      * Libelle type de transaction                                     00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MLTTRANSA      PIC X.                                     00002390
           02 MLTTRANSC PIC X.                                          00002400
           02 MLTTRANSP PIC X.                                          00002410
           02 MLTTRANSH PIC X.                                          00002420
           02 MLTTRANSV PIC X.                                          00002430
           02 MLTTRANSO      PIC X(19).                                 00002440
      * Montant de la transaction                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MMONTRSA  PIC X.                                          00002470
           02 MMONTRSC  PIC X.                                          00002480
           02 MMONTRSP  PIC X.                                          00002490
           02 MMONTRSH  PIC X.                                          00002500
           02 MMONTRSV  PIC X.                                          00002510
           02 MMONTRSO  PIC X(10).                                      00002520
      * Code de la devise/transaction                                   00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MCDEVISEA      PIC X.                                     00002550
           02 MCDEVISEC PIC X.                                          00002560
           02 MCDEVISEP PIC X.                                          00002570
           02 MCDEVISEH PIC X.                                          00002580
           02 MCDEVISEV PIC X.                                          00002590
           02 MCDEVISEO      PIC X.                                     00002600
           02 MLCTADMO OCCURS   12 TIMES .                              00002610
             03 FILLER       PIC X(2).                                  00002620
             03 MCTADMA      PIC X.                                     00002630
             03 MCTADMC PIC X.                                          00002640
             03 MCTADMP PIC X.                                          00002650
             03 MCTADMH PIC X.                                          00002660
             03 MCTADMV PIC X.                                          00002670
             03 MCTADMO      PIC X(41).                                 00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MZONCMDA  PIC X.                                          00002700
           02 MZONCMDC  PIC X.                                          00002710
           02 MZONCMDP  PIC X.                                          00002720
           02 MZONCMDH  PIC X.                                          00002730
           02 MZONCMDV  PIC X.                                          00002740
           02 MZONCMDO  PIC X(15).                                      00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MLIBERRA  PIC X.                                          00002770
           02 MLIBERRC  PIC X.                                          00002780
           02 MLIBERRP  PIC X.                                          00002790
           02 MLIBERRH  PIC X.                                          00002800
           02 MLIBERRV  PIC X.                                          00002810
           02 MLIBERRO  PIC X(58).                                      00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MCODTRAA  PIC X.                                          00002840
           02 MCODTRAC  PIC X.                                          00002850
           02 MCODTRAP  PIC X.                                          00002860
           02 MCODTRAH  PIC X.                                          00002870
           02 MCODTRAV  PIC X.                                          00002880
           02 MCODTRAO  PIC X(4).                                       00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MCICSA    PIC X.                                          00002910
           02 MCICSC    PIC X.                                          00002920
           02 MCICSP    PIC X.                                          00002930
           02 MCICSH    PIC X.                                          00002940
           02 MCICSV    PIC X.                                          00002950
           02 MCICSO    PIC X(5).                                       00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MNETNAMA  PIC X.                                          00002980
           02 MNETNAMC  PIC X.                                          00002990
           02 MNETNAMP  PIC X.                                          00003000
           02 MNETNAMH  PIC X.                                          00003010
           02 MNETNAMV  PIC X.                                          00003020
           02 MNETNAMO  PIC X(8).                                       00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MSCREENA  PIC X.                                          00003050
           02 MSCREENC  PIC X.                                          00003060
           02 MSCREENP  PIC X.                                          00003070
           02 MSCREENH  PIC X.                                          00003080
           02 MSCREENV  PIC X.                                          00003090
           02 MSCREENO  PIC X(4).                                       00003100
                                                                                
