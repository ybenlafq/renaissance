      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:36 >
      *----------------------------------------------------------------*
      *    VUE DE LA SOUS-TABLE EC006 DE : QUOTAS SUP / PTF, ZON LIV   *
      *----------------------------------------------------------------*
       01  RVEC006.
           05  EC006-CTABLEG2    PIC X(15).
           05  EC006-CTABLEG2-REDEF REDEFINES EC006-CTABLEG2.
               10  EC006-NSOC            PIC X(03).
               10  EC006-NLIEU           PIC X(03).
               10  EC006-CZONLIV         PIC X(05).
               10  EC006-CPLAGE          PIC X(02).
               10  EC006-NJOUR           PIC X(01).
           05  EC006-WTABLEG     PIC X(80).
           05  EC006-WTABLEG-REDEF  REDEFINES EC006-WTABLEG.
               10  EC006-QQUOTA          PIC X(03).
               10  EC006-QQUOTA-N       REDEFINES EC006-QQUOTA
                                         PIC 9(03).
      *----------------------------------------------------------------*
      *    FLAGS DE LA VUE                                             *
      *----------------------------------------------------------------*
       01  RVEC006-FLAGS.
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    05  EC006-CTABLEG2-F  PIC S9(4)  COMP.
      *--
           05  EC006-CTABLEG2-F  PIC S9(4) COMP-5.
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    05  EC006-WTABLEG-F   PIC S9(4)  COMP.
      *
      *--
           05  EC006-WTABLEG-F   PIC S9(4) COMP-5.
      
      *}
