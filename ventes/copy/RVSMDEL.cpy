      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SMDEL TRANSCO MODE DELIV SIEBEL-NCG    *        
      *----------------------------------------------------------------*        
       01  RVSMDEL .                                                            
           05  SMDEL-CTABLEG2    PIC X(15).                                     
           05  SMDEL-CTABLEG2-REDEF REDEFINES SMDEL-CTABLEG2.                   
               10  SMDEL-CSIEB           PIC X(10).                             
           05  SMDEL-WTABLEG     PIC X(80).                                     
           05  SMDEL-WTABLEG-REDEF  REDEFINES SMDEL-WTABLEG.                    
               10  SMDEL-CMODDEL         PIC X(03).                             
               10  SMDEL-WEMPORTE        PIC X(01).                             
               10  SMDEL-LIBELLE         PIC X(30).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVSMDEL-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SMDEL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SMDEL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SMDEL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SMDEL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
