      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000010
      * >>>>>>>>>>>>>>>>> DESCRIPTION DE LA TSGV92 <<<<<<<<<<<<<<<<<< * 00000020
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000030
      *                                                               * 00000040
       01  TSGV92-IDENTIFICATEUR.                                       00000050
           05  TSGV92-TRANSID               PIC X(04) VALUE 'GV92'.     00000060
           05  TSGV92-NUMERO-DE-TERMINAL    PIC X(04) VALUE SPACES.     00000070
      *                                                               * 00000080
       01  TSGV92-ITEM.                                                 00000090
           05  TSGV92-LONGUEUR              PIC S9(4) VALUE +630.       00000100
           05  TSGV92-DATAS.                                            00000110
               10  TSGV92-LIGNE             OCCURS 10.                  00000120
                   15  TSGV92-NCODIC        PIC X(07).                  00000130
                   15  TSGV92-CFAM          PIC X(05).                  00000130
                   15  TSGV92-CMARQ         PIC X(05).                  00000140
                   15  TSGV92-LREF          PIC X(20).                  00000150
                   15  TSGV92-DCREA         PIC X(10).                  00000160
                   15  TSGV92-CODE          PIC X(03).                  00000170
                   15  TSGV92-DURE          PIC X(03).                  00000180
                   15  TSGV92-DATE          PIC X(10).                  00000190
      *                                                               * 00000200
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000210
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< * 00000220
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000230
                                                                                
