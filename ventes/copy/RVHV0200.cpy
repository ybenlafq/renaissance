      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVHV0200                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV0200                         
      **********************************************************                
       01  RVHV0200.                                                            
           02  HV02-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV02-NCODIC                                                      
               PIC X(0007).                                                     
           02  HV02-DVENTECIALE                                                 
               PIC X(0008).                                                     
           02  HV02-QPIECES                                                     
               PIC S9(5) COMP-3.                                                
           02  HV02-PCA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV02-PMTACHATS                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV02-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVHV0200                                  
      **********************************************************                
       01  RVHV0200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV02-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV02-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV02-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV02-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV02-DVENTECIALE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV02-DVENTECIALE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV02-QPIECES-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV02-QPIECES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV02-PCA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV02-PCA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV02-PMTACHATS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV02-PMTACHATS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV02-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  HV02-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
      *}                                                                        
