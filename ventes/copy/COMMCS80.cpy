      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : EDITION DES ETIQUETTES DE CREDIT                 *        
      *  TRANSACTION: CS80                                             *        
      *  TITRE      : COMMAREA DE LA TRANSACTION CS80                  *        
      *  LONGUEUR   : 4096                                             *        
      *                                                                *        
      * Z-COMMAREA, D' UNE LONGUEUR DE 4096 C, COMPRENANT :            *        
      * 1 - LES ZONES RESERVEES A AIDA                           100 C *        
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS             20 C *        
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT          100 C *        
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP               152 C *        
      * 5 - LES ZONES RESERVEES APPLICATIVES                    3724 C *        
      *                                                        -----   *        
      * COM-CS80-LONG-COMMAREA A UNE VALUE DE                   4096 C *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-CS80-LONG-COMMAREA       PIC S9(4) COMP   VALUE +4096.           
      *--                                                                       
       01  COM-CS80-LONG-COMMAREA       PIC S9(4) COMP-5   VALUE +4096.         
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *--- ZONES RESERVEES A AIDA -------------------------------- 100          
           02 FILLER-COM-AIDA           PIC X(100).                             
      *--- ZONES RESERVEES EN PROVENANCE DE CICS ----------------- 020          
           02 COMM-CICS-APPLID          PIC X(08).                              
           02 COMM-CICS-NETNAM          PIC X(08).                              
           02 COMM-CICS-TRANSA          PIC X(04).                              
      *--- ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION --- 100          
           02 COMM-DATE-SIECLE          PIC X(02).                              
           02 COMM-DATE-ANNEE           PIC X(02).                              
           02 COMM-DATE-MOIS            PIC X(02).                              
           02 COMM-DATE-JOUR            PIC 9(02).                              
           02 COMM-DATE-QNTA            PIC 9(03).                              
           02 COMM-DATE-QNT0            PIC 9(05).                              
           02 COMM-DATE-BISX            PIC 9(01).                              
           02 COMM-DATE-JSM             PIC 9(01).                              
           02 COMM-DATE-JSM-LC          PIC X(03).                              
           02 COMM-DATE-JSM-LL          PIC X(08).                              
           02 COMM-DATE-MOIS-LC         PIC X(03).                              
           02 COMM-DATE-MOIS-LL         PIC X(09).                              
           02 COMM-DATE-SSAAMMJJ        PIC X(08).                              
           02 COMM-DATE-AAMMJJ          PIC X(06).                              
           02 COMM-DATE-JJMMSSAA        PIC X(08).                              
           02 COMM-DATE-JJMMAA          PIC X(06).                              
           02 COMM-DATE-JJ-MM-AA        PIC X(08).                              
           02 COMM-DATE-JJ-MM-SSAA      PIC X(10).                              
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS           PIC 9(02).                           
              05 COMM-DATE-SEMAA           PIC 9(02).                           
              05 COMM-DATE-SEMNU           PIC 9(02).                           
           02 COMM-DATE-FILLER          PIC X(07).                              
      *--- ZONES RESERVEES TRAITEMENT DU SWAP -------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS            PIC S9(4) COMP VALUE -1.                
      *--                                                                       
           02 COMM-SWAP-CURS            PIC S9(4) COMP-5 VALUE -1.              
      *}                                                                        
           02 COMM-SWAP-ATTR            OCCURS 150 PIC X(1).                    
      *                                                                         
      ******************************************************************        
      *--- ZONES RESERVEES APPLICATIVES DE LA TRANSACTION CS80 - 3724 C*        
      ******************************************************************        
      *                                                                         
           02 COMM-CS80-APPLI.                                                  
      *------ ZONES APPLICATIVES ISSUES DU USER ------------------ 46 C         
              05 COMM-CS80-ZONES-USER.                                          
                 10 COMM-CS80-MAGASIN.                                          
                    15 COMM-CS80-NSOCIETE        PIC X(03).                     
                    15 COMM-CS80-NLIEU           PIC X(03).                     
                 10 COMM-CS80-LIBELLES.                                         
                    15 COMM-CS80-LLIEU           PIC X(20).                     
                    15 COMM-CS80-LCRED           PIC X(20).                     
      *------ ZONES APPLICATIVES ISSUES DE LA SAISIE  ------------ 15 C         
              05 COMM-CS80-ZONES-SAISIES.                                       
                 10 COMM-CS80-CIMPRI          PIC X(04).                        
                 10 COMM-CS80-FORMAT.                                           
                    15 COMM-CS80-CFORMAT      PIC X(01).                        
                    15 COMM-CS80-NOMETAT      PIC X(08).                        
                 10 COMM-CS80-STARTCODE       PIC X(02).                        
      *------ ZONES DISPONIBLES -------------------------------- 3663 C         
              05 COMM-CS80-FILLER          PIC X(3663).                         
      *                                                                         
                                                                                
