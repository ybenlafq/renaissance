      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVHV7000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV7000                         
      **********************************************************                
       01  RVHV7000.                                                            
           02  HV70-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV70-DMOIS                                                       
               PIC X(0006).                                                     
           02  HV70-NCODIC                                                      
               PIC X(0007).                                                     
           02  HV70-QACHETEE                                                    
               PIC S9(11) COMP-3.                                               
           02  HV70-PACHETEE                                                    
               PIC S9(13)V9(0002) COMP-3.                                       
           02  HV70-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  HV70-NENTCDE                                                     
               PIC X(5).                                                        
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVHV7000                                  
      **********************************************************                
       01  RVHV7000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV70-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV70-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV70-DMOIS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV70-DMOIS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV70-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV70-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV70-QACHETEE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV70-QACHETEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV70-PACHETEE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV70-PACHETEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV70-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV70-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV70-NENTCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  HV70-NENTCDE-F                                                   
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
