      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EPP30   EPP30                                              00000020
      ***************************************************************** 00000030
       01   EPP30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPTIONL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MLOPTIONL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLOPTIONF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MLOPTIONI      PIC X(20).                                 00000170
           02 M1TABI OCCURS   10 TIMES .                                00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1NRUBL      COMP PIC S9(4).                            00000190
      *--                                                                       
             03 M1NRUBL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 M1NRUBF      PIC X.                                     00000200
             03 FILLER  PIC X(4).                                       00000210
             03 M1NRUBI      PIC X(2).                                  00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1LRUBL      COMP PIC S9(4).                            00000230
      *--                                                                       
             03 M1LRUBL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 M1LRUBF      PIC X.                                     00000240
             03 FILLER  PIC X(4).                                       00000250
             03 M1LRUBI      PIC X(19).                                 00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1CORIGZINL  COMP PIC S9(4).                            00000270
      *--                                                                       
             03 M1CORIGZINL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 M1CORIGZINF  PIC X.                                     00000280
             03 FILLER  PIC X(4).                                       00000290
             03 M1CORIGZINI  PIC X(2).                                  00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1CPARENT1L  COMP PIC S9(4).                            00000310
      *--                                                                       
             03 M1CPARENT1L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 M1CPARENT1F  PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 M1CPARENT1I  PIC X.                                     00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1CORIGZ1L   COMP PIC S9(4).                            00000350
      *--                                                                       
             03 M1CORIGZ1L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M1CORIGZ1F   PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 M1CORIGZ1I   PIC X(2).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1PMONTZ1L   COMP PIC S9(4).                            00000390
      *--                                                                       
             03 M1PMONTZ1L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M1PMONTZ1F   PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 M1PMONTZ1I   PIC X(9).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1COPER1L    COMP PIC S9(4).                            00000430
      *--                                                                       
             03 M1COPER1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 M1COPER1F    PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 M1COPER1I    PIC X.                                     00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1CPARENT2L  COMP PIC S9(4).                            00000470
      *--                                                                       
             03 M1CPARENT2L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 M1CPARENT2F  PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 M1CPARENT2I  PIC X.                                     00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1CORIGZ2L   COMP PIC S9(4).                            00000510
      *--                                                                       
             03 M1CORIGZ2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M1CORIGZ2F   PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 M1CORIGZ2I   PIC X(2).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1PMONTZ2L   COMP PIC S9(4).                            00000550
      *--                                                                       
             03 M1PMONTZ2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M1PMONTZ2F   PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 M1PMONTZ2I   PIC X(6).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1CPARENT3L  COMP PIC S9(4).                            00000590
      *--                                                                       
             03 M1CPARENT3L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 M1CPARENT3F  PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 M1CPARENT3I  PIC X.                                     00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1COPER2L    COMP PIC S9(4).                            00000630
      *--                                                                       
             03 M1COPER2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 M1COPER2F    PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 M1COPER2I    PIC X.                                     00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1CORIGZ3L   COMP PIC S9(4).                            00000670
      *--                                                                       
             03 M1CORIGZ3L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M1CORIGZ3F   PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 M1CORIGZ3I   PIC X(2).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1PMONTZ3L   COMP PIC S9(4).                            00000710
      *--                                                                       
             03 M1PMONTZ3L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M1PMONTZ3F   PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 M1PMONTZ3I   PIC X(6).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1CPARENT4L  COMP PIC S9(4).                            00000750
      *--                                                                       
             03 M1CPARENT4L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 M1CPARENT4F  PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 M1CPARENT4I  PIC X.                                     00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1CCALCULL   COMP PIC S9(4).                            00000790
      *--                                                                       
             03 M1CCALCULL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M1CCALCULF   PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 M1CCALCULI   PIC X.                                     00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M4COPERL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 M4COPERL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 M4COPERF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 M4COPERI  PIC X.                                          00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M4PSEUILL      COMP PIC S9(4).                            00000870
      *--                                                                       
           02 M4PSEUILL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 M4PSEUILF      PIC X.                                     00000880
           02 FILLER    PIC X(4).                                       00000890
           02 M4PSEUILI      PIC X(11).                                 00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M1LTYPRUL      COMP PIC S9(4).                            00000910
      *--                                                                       
           02 M1LTYPRUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 M1LTYPRUF      PIC X.                                     00000920
           02 FILLER    PIC X(4).                                       00000930
           02 M1LTYPRUI      PIC X(30).                                 00000940
           02 M2LRUBD OCCURS   2 TIMES .                                00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M2LRUBL      COMP PIC S9(4).                            00000960
      *--                                                                       
             03 M2LRUBL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 M2LRUBF      PIC X.                                     00000970
             03 FILLER  PIC X(4).                                       00000980
             03 M2LRUBI      PIC X(30).                                 00000990
           02 M2CORIGZ1D OCCURS   2 TIMES .                             00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M2CORIGZ1L   COMP PIC S9(4).                            00001010
      *--                                                                       
             03 M2CORIGZ1L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M2CORIGZ1F   PIC X.                                     00001020
             03 FILLER  PIC X(4).                                       00001030
             03 M2CORIGZ1I   PIC X(2).                                  00001040
           02 M2PMONTZ1D OCCURS   2 TIMES .                             00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M2PMONTZ1L   COMP PIC S9(4).                            00001060
      *--                                                                       
             03 M2PMONTZ1L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M2PMONTZ1F   PIC X.                                     00001070
             03 FILLER  PIC X(4).                                       00001080
             03 M2PMONTZ1I   PIC X(11).                                 00001090
           02 M2COPER1D OCCURS   2 TIMES .                              00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M2COPER1L    COMP PIC S9(4).                            00001110
      *--                                                                       
             03 M2COPER1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 M2COPER1F    PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 M2COPER1I    PIC X.                                     00001140
           02 M2CORIGZ2D OCCURS   2 TIMES .                             00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M2CORIGZ2L   COMP PIC S9(4).                            00001160
      *--                                                                       
             03 M2CORIGZ2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M2CORIGZ2F   PIC X.                                     00001170
             03 FILLER  PIC X(4).                                       00001180
             03 M2CORIGZ2I   PIC X(2).                                  00001190
           02 M2PMONTZ2D OCCURS   2 TIMES .                             00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M2PMONTZ2L   COMP PIC S9(4).                            00001210
      *--                                                                       
             03 M2PMONTZ2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M2PMONTZ2F   PIC X.                                     00001220
             03 FILLER  PIC X(4).                                       00001230
             03 M2PMONTZ2I   PIC X(6).                                  00001240
           02 M2CCALCULD OCCURS   2 TIMES .                             00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M2CCALCULL   COMP PIC S9(4).                            00001260
      *--                                                                       
             03 M2CCALCULL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M2CCALCULF   PIC X.                                     00001270
             03 FILLER  PIC X(4).                                       00001280
             03 M2CCALCULI   PIC X.                                     00001290
           02 M3TABI OCCURS   2 TIMES .                                 00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3NRUBL      COMP PIC S9(4).                            00001310
      *--                                                                       
             03 M3NRUBL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 M3NRUBF      PIC X.                                     00001320
             03 FILLER  PIC X(4).                                       00001330
             03 M3NRUBI      PIC X(2).                                  00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3LRUBL      COMP PIC S9(4).                            00001350
      *--                                                                       
             03 M3LRUBL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 M3LRUBF      PIC X.                                     00001360
             03 FILLER  PIC X(4).                                       00001370
             03 M3LRUBI      PIC X(19).                                 00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3CORIGZINL  COMP PIC S9(4).                            00001390
      *--                                                                       
             03 M3CORIGZINL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 M3CORIGZINF  PIC X.                                     00001400
             03 FILLER  PIC X(4).                                       00001410
             03 M3CORIGZINI  PIC X(2).                                  00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3CPARENT1L  COMP PIC S9(4).                            00001430
      *--                                                                       
             03 M3CPARENT1L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 M3CPARENT1F  PIC X.                                     00001440
             03 FILLER  PIC X(4).                                       00001450
             03 M3CPARENT1I  PIC X.                                     00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3CORIGZ1L   COMP PIC S9(4).                            00001470
      *--                                                                       
             03 M3CORIGZ1L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M3CORIGZ1F   PIC X.                                     00001480
             03 FILLER  PIC X(4).                                       00001490
             03 M3CORIGZ1I   PIC X(2).                                  00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3PMONTZ1L   COMP PIC S9(4).                            00001510
      *--                                                                       
             03 M3PMONTZ1L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M3PMONTZ1F   PIC X.                                     00001520
             03 FILLER  PIC X(4).                                       00001530
             03 M3PMONTZ1I   PIC X(9).                                  00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3COPER1L    COMP PIC S9(4).                            00001550
      *--                                                                       
             03 M3COPER1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 M3COPER1F    PIC X.                                     00001560
             03 FILLER  PIC X(4).                                       00001570
             03 M3COPER1I    PIC X.                                     00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3CPARENT2L  COMP PIC S9(4).                            00001590
      *--                                                                       
             03 M3CPARENT2L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 M3CPARENT2F  PIC X.                                     00001600
             03 FILLER  PIC X(4).                                       00001610
             03 M3CPARENT2I  PIC X.                                     00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3CORIGZ2L   COMP PIC S9(4).                            00001630
      *--                                                                       
             03 M3CORIGZ2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M3CORIGZ2F   PIC X.                                     00001640
             03 FILLER  PIC X(4).                                       00001650
             03 M3CORIGZ2I   PIC X(2).                                  00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3PMONTZ2L   COMP PIC S9(4).                            00001670
      *--                                                                       
             03 M3PMONTZ2L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M3PMONTZ2F   PIC X.                                     00001680
             03 FILLER  PIC X(4).                                       00001690
             03 M3PMONTZ2I   PIC X(6).                                  00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3CPARENT3L  COMP PIC S9(4).                            00001710
      *--                                                                       
             03 M3CPARENT3L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 M3CPARENT3F  PIC X.                                     00001720
             03 FILLER  PIC X(4).                                       00001730
             03 M3CPARENT3I  PIC X.                                     00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3COPER2L    COMP PIC S9(4).                            00001750
      *--                                                                       
             03 M3COPER2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 M3COPER2F    PIC X.                                     00001760
             03 FILLER  PIC X(4).                                       00001770
             03 M3COPER2I    PIC X.                                     00001780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3CORIGZ3L   COMP PIC S9(4).                            00001790
      *--                                                                       
             03 M3CORIGZ3L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M3CORIGZ3F   PIC X.                                     00001800
             03 FILLER  PIC X(4).                                       00001810
             03 M3CORIGZ3I   PIC X(2).                                  00001820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3PMONTZ3L   COMP PIC S9(4).                            00001830
      *--                                                                       
             03 M3PMONTZ3L COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M3PMONTZ3F   PIC X.                                     00001840
             03 FILLER  PIC X(4).                                       00001850
             03 M3PMONTZ3I   PIC X(6).                                  00001860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3CPARENT4L  COMP PIC S9(4).                            00001870
      *--                                                                       
             03 M3CPARENT4L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 M3CPARENT4F  PIC X.                                     00001880
             03 FILLER  PIC X(4).                                       00001890
             03 M3CPARENT4I  PIC X.                                     00001900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3CCALCULL   COMP PIC S9(4).                            00001910
      *--                                                                       
             03 M3CCALCULL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 M3CCALCULF   PIC X.                                     00001920
             03 FILLER  PIC X(4).                                       00001930
             03 M3CCALCULI   PIC X.                                     00001940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M3LTYPRUL      COMP PIC S9(4).                            00001950
      *--                                                                       
           02 M3LTYPRUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 M3LTYPRUF      PIC X.                                     00001960
           02 FILLER    PIC X(4).                                       00001970
           02 M3LTYPRUI      PIC X(30).                                 00001980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001990
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00002000
           02 FILLER    PIC X(4).                                       00002010
           02 MZONCMDI  PIC X(15).                                      00002020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00002030
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00002040
           02 FILLER    PIC X(4).                                       00002050
           02 MLIBERRI  PIC X(58).                                      00002060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00002070
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00002080
           02 FILLER    PIC X(4).                                       00002090
           02 MCODTRAI  PIC X(4).                                       00002100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00002110
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00002120
           02 FILLER    PIC X(4).                                       00002130
           02 MCICSI    PIC X(5).                                       00002140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00002150
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002160
           02 FILLER    PIC X(4).                                       00002170
           02 MNETNAMI  PIC X(8).                                       00002180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002190
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002200
           02 FILLER    PIC X(4).                                       00002210
           02 MSCREENI  PIC X(4).                                       00002220
      ***************************************************************** 00002230
      * SDF: EPP30   EPP30                                              00002240
      ***************************************************************** 00002250
       01   EPP30O REDEFINES EPP30I.                                    00002260
           02 FILLER    PIC X(12).                                      00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MDATJOUA  PIC X.                                          00002290
           02 MDATJOUC  PIC X.                                          00002300
           02 MDATJOUP  PIC X.                                          00002310
           02 MDATJOUH  PIC X.                                          00002320
           02 MDATJOUV  PIC X.                                          00002330
           02 MDATJOUO  PIC X(10).                                      00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MTIMJOUA  PIC X.                                          00002360
           02 MTIMJOUC  PIC X.                                          00002370
           02 MTIMJOUP  PIC X.                                          00002380
           02 MTIMJOUH  PIC X.                                          00002390
           02 MTIMJOUV  PIC X.                                          00002400
           02 MTIMJOUO  PIC X(5).                                       00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLOPTIONA      PIC X.                                     00002430
           02 MLOPTIONC PIC X.                                          00002440
           02 MLOPTIONP PIC X.                                          00002450
           02 MLOPTIONH PIC X.                                          00002460
           02 MLOPTIONV PIC X.                                          00002470
           02 MLOPTIONO      PIC X(20).                                 00002480
           02 M1TABO OCCURS   10 TIMES .                                00002490
             03 FILLER       PIC X(2).                                  00002500
             03 M1NRUBA      PIC X.                                     00002510
             03 M1NRUBC PIC X.                                          00002520
             03 M1NRUBP PIC X.                                          00002530
             03 M1NRUBH PIC X.                                          00002540
             03 M1NRUBV PIC X.                                          00002550
             03 M1NRUBO      PIC X(2).                                  00002560
             03 FILLER       PIC X(2).                                  00002570
             03 M1LRUBA      PIC X.                                     00002580
             03 M1LRUBC PIC X.                                          00002590
             03 M1LRUBP PIC X.                                          00002600
             03 M1LRUBH PIC X.                                          00002610
             03 M1LRUBV PIC X.                                          00002620
             03 M1LRUBO      PIC X(19).                                 00002630
             03 FILLER       PIC X(2).                                  00002640
             03 M1CORIGZINA  PIC X.                                     00002650
             03 M1CORIGZINC  PIC X.                                     00002660
             03 M1CORIGZINP  PIC X.                                     00002670
             03 M1CORIGZINH  PIC X.                                     00002680
             03 M1CORIGZINV  PIC X.                                     00002690
             03 M1CORIGZINO  PIC X(2).                                  00002700
             03 FILLER       PIC X(2).                                  00002710
             03 M1CPARENT1A  PIC X.                                     00002720
             03 M1CPARENT1C  PIC X.                                     00002730
             03 M1CPARENT1P  PIC X.                                     00002740
             03 M1CPARENT1H  PIC X.                                     00002750
             03 M1CPARENT1V  PIC X.                                     00002760
             03 M1CPARENT1O  PIC X.                                     00002770
             03 FILLER       PIC X(2).                                  00002780
             03 M1CORIGZ1A   PIC X.                                     00002790
             03 M1CORIGZ1C   PIC X.                                     00002800
             03 M1CORIGZ1P   PIC X.                                     00002810
             03 M1CORIGZ1H   PIC X.                                     00002820
             03 M1CORIGZ1V   PIC X.                                     00002830
             03 M1CORIGZ1O   PIC X(2).                                  00002840
             03 FILLER       PIC X(2).                                  00002850
             03 M1PMONTZ1A   PIC X.                                     00002860
             03 M1PMONTZ1C   PIC X.                                     00002870
             03 M1PMONTZ1P   PIC X.                                     00002880
             03 M1PMONTZ1H   PIC X.                                     00002890
             03 M1PMONTZ1V   PIC X.                                     00002900
             03 M1PMONTZ1O   PIC X(9).                                  00002910
             03 FILLER       PIC X(2).                                  00002920
             03 M1COPER1A    PIC X.                                     00002930
             03 M1COPER1C    PIC X.                                     00002940
             03 M1COPER1P    PIC X.                                     00002950
             03 M1COPER1H    PIC X.                                     00002960
             03 M1COPER1V    PIC X.                                     00002970
             03 M1COPER1O    PIC X.                                     00002980
             03 FILLER       PIC X(2).                                  00002990
             03 M1CPARENT2A  PIC X.                                     00003000
             03 M1CPARENT2C  PIC X.                                     00003010
             03 M1CPARENT2P  PIC X.                                     00003020
             03 M1CPARENT2H  PIC X.                                     00003030
             03 M1CPARENT2V  PIC X.                                     00003040
             03 M1CPARENT2O  PIC X.                                     00003050
             03 FILLER       PIC X(2).                                  00003060
             03 M1CORIGZ2A   PIC X.                                     00003070
             03 M1CORIGZ2C   PIC X.                                     00003080
             03 M1CORIGZ2P   PIC X.                                     00003090
             03 M1CORIGZ2H   PIC X.                                     00003100
             03 M1CORIGZ2V   PIC X.                                     00003110
             03 M1CORIGZ2O   PIC X(2).                                  00003120
             03 FILLER       PIC X(2).                                  00003130
             03 M1PMONTZ2A   PIC X.                                     00003140
             03 M1PMONTZ2C   PIC X.                                     00003150
             03 M1PMONTZ2P   PIC X.                                     00003160
             03 M1PMONTZ2H   PIC X.                                     00003170
             03 M1PMONTZ2V   PIC X.                                     00003180
             03 M1PMONTZ2O   PIC X(6).                                  00003190
             03 FILLER       PIC X(2).                                  00003200
             03 M1CPARENT3A  PIC X.                                     00003210
             03 M1CPARENT3C  PIC X.                                     00003220
             03 M1CPARENT3P  PIC X.                                     00003230
             03 M1CPARENT3H  PIC X.                                     00003240
             03 M1CPARENT3V  PIC X.                                     00003250
             03 M1CPARENT3O  PIC X.                                     00003260
             03 FILLER       PIC X(2).                                  00003270
             03 M1COPER2A    PIC X.                                     00003280
             03 M1COPER2C    PIC X.                                     00003290
             03 M1COPER2P    PIC X.                                     00003300
             03 M1COPER2H    PIC X.                                     00003310
             03 M1COPER2V    PIC X.                                     00003320
             03 M1COPER2O    PIC X.                                     00003330
             03 FILLER       PIC X(2).                                  00003340
             03 M1CORIGZ3A   PIC X.                                     00003350
             03 M1CORIGZ3C   PIC X.                                     00003360
             03 M1CORIGZ3P   PIC X.                                     00003370
             03 M1CORIGZ3H   PIC X.                                     00003380
             03 M1CORIGZ3V   PIC X.                                     00003390
             03 M1CORIGZ3O   PIC X(2).                                  00003400
             03 FILLER       PIC X(2).                                  00003410
             03 M1PMONTZ3A   PIC X.                                     00003420
             03 M1PMONTZ3C   PIC X.                                     00003430
             03 M1PMONTZ3P   PIC X.                                     00003440
             03 M1PMONTZ3H   PIC X.                                     00003450
             03 M1PMONTZ3V   PIC X.                                     00003460
             03 M1PMONTZ3O   PIC X(6).                                  00003470
             03 FILLER       PIC X(2).                                  00003480
             03 M1CPARENT4A  PIC X.                                     00003490
             03 M1CPARENT4C  PIC X.                                     00003500
             03 M1CPARENT4P  PIC X.                                     00003510
             03 M1CPARENT4H  PIC X.                                     00003520
             03 M1CPARENT4V  PIC X.                                     00003530
             03 M1CPARENT4O  PIC X.                                     00003540
             03 FILLER       PIC X(2).                                  00003550
             03 M1CCALCULA   PIC X.                                     00003560
             03 M1CCALCULC   PIC X.                                     00003570
             03 M1CCALCULP   PIC X.                                     00003580
             03 M1CCALCULH   PIC X.                                     00003590
             03 M1CCALCULV   PIC X.                                     00003600
             03 M1CCALCULO   PIC X.                                     00003610
           02 FILLER    PIC X(2).                                       00003620
           02 M4COPERA  PIC X.                                          00003630
           02 M4COPERC  PIC X.                                          00003640
           02 M4COPERP  PIC X.                                          00003650
           02 M4COPERH  PIC X.                                          00003660
           02 M4COPERV  PIC X.                                          00003670
           02 M4COPERO  PIC X.                                          00003680
           02 FILLER    PIC X(2).                                       00003690
           02 M4PSEUILA      PIC X.                                     00003700
           02 M4PSEUILC PIC X.                                          00003710
           02 M4PSEUILP PIC X.                                          00003720
           02 M4PSEUILH PIC X.                                          00003730
           02 M4PSEUILV PIC X.                                          00003740
           02 M4PSEUILO      PIC X(11).                                 00003750
           02 FILLER    PIC X(2).                                       00003760
           02 M1LTYPRUA      PIC X.                                     00003770
           02 M1LTYPRUC PIC X.                                          00003780
           02 M1LTYPRUP PIC X.                                          00003790
           02 M1LTYPRUH PIC X.                                          00003800
           02 M1LTYPRUV PIC X.                                          00003810
           02 M1LTYPRUO      PIC X(30).                                 00003820
           02 DFHMS1 OCCURS   2 TIMES .                                 00003830
             03 FILLER       PIC X(2).                                  00003840
             03 M2LRUBA      PIC X.                                     00003850
             03 M2LRUBC PIC X.                                          00003860
             03 M2LRUBP PIC X.                                          00003870
             03 M2LRUBH PIC X.                                          00003880
             03 M2LRUBV PIC X.                                          00003890
             03 M2LRUBO      PIC X(30).                                 00003900
           02 DFHMS2 OCCURS   2 TIMES .                                 00003910
             03 FILLER       PIC X(2).                                  00003920
             03 M2CORIGZ1A   PIC X.                                     00003930
             03 M2CORIGZ1C   PIC X.                                     00003940
             03 M2CORIGZ1P   PIC X.                                     00003950
             03 M2CORIGZ1H   PIC X.                                     00003960
             03 M2CORIGZ1V   PIC X.                                     00003970
             03 M2CORIGZ1O   PIC X(2).                                  00003980
           02 DFHMS3 OCCURS   2 TIMES .                                 00003990
             03 FILLER       PIC X(2).                                  00004000
             03 M2PMONTZ1A   PIC X.                                     00004010
             03 M2PMONTZ1C   PIC X.                                     00004020
             03 M2PMONTZ1P   PIC X.                                     00004030
             03 M2PMONTZ1H   PIC X.                                     00004040
             03 M2PMONTZ1V   PIC X.                                     00004050
             03 M2PMONTZ1O   PIC X(11).                                 00004060
           02 DFHMS4 OCCURS   2 TIMES .                                 00004070
             03 FILLER       PIC X(2).                                  00004080
             03 M2COPER1A    PIC X.                                     00004090
             03 M2COPER1C    PIC X.                                     00004100
             03 M2COPER1P    PIC X.                                     00004110
             03 M2COPER1H    PIC X.                                     00004120
             03 M2COPER1V    PIC X.                                     00004130
             03 M2COPER1O    PIC X.                                     00004140
           02 DFHMS5 OCCURS   2 TIMES .                                 00004150
             03 FILLER       PIC X(2).                                  00004160
             03 M2CORIGZ2A   PIC X.                                     00004170
             03 M2CORIGZ2C   PIC X.                                     00004180
             03 M2CORIGZ2P   PIC X.                                     00004190
             03 M2CORIGZ2H   PIC X.                                     00004200
             03 M2CORIGZ2V   PIC X.                                     00004210
             03 M2CORIGZ2O   PIC X(2).                                  00004220
           02 DFHMS6 OCCURS   2 TIMES .                                 00004230
             03 FILLER       PIC X(2).                                  00004240
             03 M2PMONTZ2A   PIC X.                                     00004250
             03 M2PMONTZ2C   PIC X.                                     00004260
             03 M2PMONTZ2P   PIC X.                                     00004270
             03 M2PMONTZ2H   PIC X.                                     00004280
             03 M2PMONTZ2V   PIC X.                                     00004290
             03 M2PMONTZ2O   PIC X(6).                                  00004300
           02 DFHMS7 OCCURS   2 TIMES .                                 00004310
             03 FILLER       PIC X(2).                                  00004320
             03 M2CCALCULA   PIC X.                                     00004330
             03 M2CCALCULC   PIC X.                                     00004340
             03 M2CCALCULP   PIC X.                                     00004350
             03 M2CCALCULH   PIC X.                                     00004360
             03 M2CCALCULV   PIC X.                                     00004370
             03 M2CCALCULO   PIC X.                                     00004380
           02 M3TABO OCCURS   2 TIMES .                                 00004390
             03 FILLER       PIC X(2).                                  00004400
             03 M3NRUBA      PIC X.                                     00004410
             03 M3NRUBC PIC X.                                          00004420
             03 M3NRUBP PIC X.                                          00004430
             03 M3NRUBH PIC X.                                          00004440
             03 M3NRUBV PIC X.                                          00004450
             03 M3NRUBO      PIC X(2).                                  00004460
             03 FILLER       PIC X(2).                                  00004470
             03 M3LRUBA      PIC X.                                     00004480
             03 M3LRUBC PIC X.                                          00004490
             03 M3LRUBP PIC X.                                          00004500
             03 M3LRUBH PIC X.                                          00004510
             03 M3LRUBV PIC X.                                          00004520
             03 M3LRUBO      PIC X(19).                                 00004530
             03 FILLER       PIC X(2).                                  00004540
             03 M3CORIGZINA  PIC X.                                     00004550
             03 M3CORIGZINC  PIC X.                                     00004560
             03 M3CORIGZINP  PIC X.                                     00004570
             03 M3CORIGZINH  PIC X.                                     00004580
             03 M3CORIGZINV  PIC X.                                     00004590
             03 M3CORIGZINO  PIC X(2).                                  00004600
             03 FILLER       PIC X(2).                                  00004610
             03 M3CPARENT1A  PIC X.                                     00004620
             03 M3CPARENT1C  PIC X.                                     00004630
             03 M3CPARENT1P  PIC X.                                     00004640
             03 M3CPARENT1H  PIC X.                                     00004650
             03 M3CPARENT1V  PIC X.                                     00004660
             03 M3CPARENT1O  PIC X.                                     00004670
             03 FILLER       PIC X(2).                                  00004680
             03 M3CORIGZ1A   PIC X.                                     00004690
             03 M3CORIGZ1C   PIC X.                                     00004700
             03 M3CORIGZ1P   PIC X.                                     00004710
             03 M3CORIGZ1H   PIC X.                                     00004720
             03 M3CORIGZ1V   PIC X.                                     00004730
             03 M3CORIGZ1O   PIC X(2).                                  00004740
             03 FILLER       PIC X(2).                                  00004750
             03 M3PMONTZ1A   PIC X.                                     00004760
             03 M3PMONTZ1C   PIC X.                                     00004770
             03 M3PMONTZ1P   PIC X.                                     00004780
             03 M3PMONTZ1H   PIC X.                                     00004790
             03 M3PMONTZ1V   PIC X.                                     00004800
             03 M3PMONTZ1O   PIC X(9).                                  00004810
             03 FILLER       PIC X(2).                                  00004820
             03 M3COPER1A    PIC X.                                     00004830
             03 M3COPER1C    PIC X.                                     00004840
             03 M3COPER1P    PIC X.                                     00004850
             03 M3COPER1H    PIC X.                                     00004860
             03 M3COPER1V    PIC X.                                     00004870
             03 M3COPER1O    PIC X.                                     00004880
             03 FILLER       PIC X(2).                                  00004890
             03 M3CPARENT2A  PIC X.                                     00004900
             03 M3CPARENT2C  PIC X.                                     00004910
             03 M3CPARENT2P  PIC X.                                     00004920
             03 M3CPARENT2H  PIC X.                                     00004930
             03 M3CPARENT2V  PIC X.                                     00004940
             03 M3CPARENT2O  PIC X.                                     00004950
             03 FILLER       PIC X(2).                                  00004960
             03 M3CORIGZ2A   PIC X.                                     00004970
             03 M3CORIGZ2C   PIC X.                                     00004980
             03 M3CORIGZ2P   PIC X.                                     00004990
             03 M3CORIGZ2H   PIC X.                                     00005000
             03 M3CORIGZ2V   PIC X.                                     00005010
             03 M3CORIGZ2O   PIC X(2).                                  00005020
             03 FILLER       PIC X(2).                                  00005030
             03 M3PMONTZ2A   PIC X.                                     00005040
             03 M3PMONTZ2C   PIC X.                                     00005050
             03 M3PMONTZ2P   PIC X.                                     00005060
             03 M3PMONTZ2H   PIC X.                                     00005070
             03 M3PMONTZ2V   PIC X.                                     00005080
             03 M3PMONTZ2O   PIC X(6).                                  00005090
             03 FILLER       PIC X(2).                                  00005100
             03 M3CPARENT3A  PIC X.                                     00005110
             03 M3CPARENT3C  PIC X.                                     00005120
             03 M3CPARENT3P  PIC X.                                     00005130
             03 M3CPARENT3H  PIC X.                                     00005140
             03 M3CPARENT3V  PIC X.                                     00005150
             03 M3CPARENT3O  PIC X.                                     00005160
             03 FILLER       PIC X(2).                                  00005170
             03 M3COPER2A    PIC X.                                     00005180
             03 M3COPER2C    PIC X.                                     00005190
             03 M3COPER2P    PIC X.                                     00005200
             03 M3COPER2H    PIC X.                                     00005210
             03 M3COPER2V    PIC X.                                     00005220
             03 M3COPER2O    PIC X.                                     00005230
             03 FILLER       PIC X(2).                                  00005240
             03 M3CORIGZ3A   PIC X.                                     00005250
             03 M3CORIGZ3C   PIC X.                                     00005260
             03 M3CORIGZ3P   PIC X.                                     00005270
             03 M3CORIGZ3H   PIC X.                                     00005280
             03 M3CORIGZ3V   PIC X.                                     00005290
             03 M3CORIGZ3O   PIC X(2).                                  00005300
             03 FILLER       PIC X(2).                                  00005310
             03 M3PMONTZ3A   PIC X.                                     00005320
             03 M3PMONTZ3C   PIC X.                                     00005330
             03 M3PMONTZ3P   PIC X.                                     00005340
             03 M3PMONTZ3H   PIC X.                                     00005350
             03 M3PMONTZ3V   PIC X.                                     00005360
             03 M3PMONTZ3O   PIC X(6).                                  00005370
             03 FILLER       PIC X(2).                                  00005380
             03 M3CPARENT4A  PIC X.                                     00005390
             03 M3CPARENT4C  PIC X.                                     00005400
             03 M3CPARENT4P  PIC X.                                     00005410
             03 M3CPARENT4H  PIC X.                                     00005420
             03 M3CPARENT4V  PIC X.                                     00005430
             03 M3CPARENT4O  PIC X.                                     00005440
             03 FILLER       PIC X(2).                                  00005450
             03 M3CCALCULA   PIC X.                                     00005460
             03 M3CCALCULC   PIC X.                                     00005470
             03 M3CCALCULP   PIC X.                                     00005480
             03 M3CCALCULH   PIC X.                                     00005490
             03 M3CCALCULV   PIC X.                                     00005500
             03 M3CCALCULO   PIC X.                                     00005510
           02 FILLER    PIC X(2).                                       00005520
           02 M3LTYPRUA      PIC X.                                     00005530
           02 M3LTYPRUC PIC X.                                          00005540
           02 M3LTYPRUP PIC X.                                          00005550
           02 M3LTYPRUH PIC X.                                          00005560
           02 M3LTYPRUV PIC X.                                          00005570
           02 M3LTYPRUO      PIC X(30).                                 00005580
           02 FILLER    PIC X(2).                                       00005590
           02 MZONCMDA  PIC X.                                          00005600
           02 MZONCMDC  PIC X.                                          00005610
           02 MZONCMDP  PIC X.                                          00005620
           02 MZONCMDH  PIC X.                                          00005630
           02 MZONCMDV  PIC X.                                          00005640
           02 MZONCMDO  PIC X(15).                                      00005650
           02 FILLER    PIC X(2).                                       00005660
           02 MLIBERRA  PIC X.                                          00005670
           02 MLIBERRC  PIC X.                                          00005680
           02 MLIBERRP  PIC X.                                          00005690
           02 MLIBERRH  PIC X.                                          00005700
           02 MLIBERRV  PIC X.                                          00005710
           02 MLIBERRO  PIC X(58).                                      00005720
           02 FILLER    PIC X(2).                                       00005730
           02 MCODTRAA  PIC X.                                          00005740
           02 MCODTRAC  PIC X.                                          00005750
           02 MCODTRAP  PIC X.                                          00005760
           02 MCODTRAH  PIC X.                                          00005770
           02 MCODTRAV  PIC X.                                          00005780
           02 MCODTRAO  PIC X(4).                                       00005790
           02 FILLER    PIC X(2).                                       00005800
           02 MCICSA    PIC X.                                          00005810
           02 MCICSC    PIC X.                                          00005820
           02 MCICSP    PIC X.                                          00005830
           02 MCICSH    PIC X.                                          00005840
           02 MCICSV    PIC X.                                          00005850
           02 MCICSO    PIC X(5).                                       00005860
           02 FILLER    PIC X(2).                                       00005870
           02 MNETNAMA  PIC X.                                          00005880
           02 MNETNAMC  PIC X.                                          00005890
           02 MNETNAMP  PIC X.                                          00005900
           02 MNETNAMH  PIC X.                                          00005910
           02 MNETNAMV  PIC X.                                          00005920
           02 MNETNAMO  PIC X(8).                                       00005930
           02 FILLER    PIC X(2).                                       00005940
           02 MSCREENA  PIC X.                                          00005950
           02 MSCREENC  PIC X.                                          00005960
           02 MSCREENP  PIC X.                                          00005970
           02 MSCREENH  PIC X.                                          00005980
           02 MSCREENV  PIC X.                                          00005990
           02 MSCREENO  PIC X(4).                                       00006000
                                                                                
