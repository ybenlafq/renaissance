      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPV410 AU 29/12/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,20,BI,A,                          *        
      *                           33,03,BI,A,                          *        
      *                           36,03,BI,A,                          *        
      *                           39,03,PD,A,                          *        
      *                           42,02,BI,A,                          *        
      *                           44,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPV410.                                                        
            05 NOMETAT-IPV410           PIC X(6) VALUE 'IPV410'.                
            05 RUPTURES-IPV410.                                                 
           10 IPV410-NSOCIETE           PIC X(03).                      007  003
           10 IPV410-NLIEU              PIC X(03).                      010  003
           10 IPV410-LVENDEUR           PIC X(20).                      013  020
           10 IPV410-NSOCVTE            PIC X(03).                      033  003
           10 IPV410-NLIEUVTE           PIC X(03).                      036  003
           10 IPV410-WSEQED             PIC S9(05)      COMP-3.         039  003
           10 IPV410-NAGREGATED         PIC X(02).                      042  002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPV410-SEQUENCE           PIC S9(04) COMP.                044  002
      *--                                                                       
           10 IPV410-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPV410.                                                   
           10 IPV410-CVENDEUR           PIC X(07).                      046  007
           10 IPV410-RUBRIQUE           PIC X(20).                      053  020
           10 IPV410-PCA                PIC S9(08)      COMP-3.         073  005
           10 IPV410-PMTADD             PIC S9(07)V9(2) COMP-3.         078  005
           10 IPV410-PMTEP              PIC S9(07)V9(2) COMP-3.         083  005
           10 IPV410-PMTJAUNE           PIC S9(07)V9(2) COMP-3.         088  005
           10 IPV410-PMTPSE             PIC S9(07)V9(2) COMP-3.         093  005
           10 IPV410-PMTTOT             PIC S9(07)V9(2) COMP-3.         098  005
           10 IPV410-PMTVOL             PIC S9(07)V9(2) COMP-3.         103  005
           10 IPV410-QVENDUE            PIC S9(08)      COMP-3.         108  005
           10 IPV410-DEBPERIODE         PIC X(08).                      113  008
           10 IPV410-FINPERIODE         PIC X(08).                      121  008
            05 FILLER                      PIC X(384).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPV410-LONG           PIC S9(4)   COMP  VALUE +128.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPV410-LONG           PIC S9(4) COMP-5  VALUE +128.           
                                                                                
      *}                                                                        
