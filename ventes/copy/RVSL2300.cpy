      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVSL2300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSL2300                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVSL2300.                                                            
           02  SL23-NMUTATION      PIC X(0007).                                 
           02  SL23-WMULTI         PIC X(0001).                                 
           02  SL23-WTYPENVOI      PIC X(0001).                                 
           02  SL23-WBTOB          PIC X(0001).                                 
           02  SL23-DEXPED         PIC X(0008).                                 
           02  SL23-CFILIERE       PIC X(0002).                                 
           02  SL23-DFILIERE       PIC X(0008).                                 
           02  SL23-NBLIGNES       PIC S9(05) COMP-3.                           
           02  SL23-QTE            PIC S9(05) COMP-3.                           
           02  SL23-QTEREC         PIC S9(05) COMP-3.                           
           02  SL23-DRECUEMO       PIC X(0008).                                 
           02  SL23-CETAT          PIC X(0010).                                 
           02  SL23-DCLOTURE       PIC X(0008).                                 
           02  SL23-HCLOTURE       PIC X(0004).                                 
           02  SL23-PGMMAJ         PIC X(0008).                                 
           02  SL23-DSYST          PIC S9(13) COMP-3.                           
                                                                                
