      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SLDA DATE BUTOIR EMRX SUIVI EXPED      *        
      *----------------------------------------------------------------*        
       01  RVSLDAT.                                                             
           05  SLDAT-CTABLEG2     PIC X(15).                                    
           05  SLDAT-CTABLEG2-REDEF REDEFINES SLDAT-CTABLEG2.                   
               10  SLDAT-DATLIM           PIC X(08).                            
           05  SLDAT-WTABLEG      PIC X(80).                                    
           05  SLDAT-WTABLEG-REDEF   REDEFINES SLDAT-WTABLEG.                   
               10  SLDAT-WACTIF           PIC X(01).                            
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVSLDAT-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLDAT-CTABLEG2-F   PIC S9(4)  COMP.                              
      *--                                                                       
           05  SLDAT-CTABLEG2-F   PIC S9(4) COMP-5.                             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SLDAT-WTABLEG-F    PIC S9(4)  COMP.                              
      *                                                                         
      *--                                                                       
           05  SLDAT-WTABLEG-F    PIC S9(4) COMP-5.                             
                                                                                
      *}                                                                        
