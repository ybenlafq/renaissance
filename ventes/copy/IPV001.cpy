      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPV001 AU 19/06/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,08,BI,A,                          *        
      *                           15,03,BI,A,                          *        
      *                           18,03,BI,A,                          *        
      *                           21,04,BI,A,                          *        
      *                           25,05,BI,A,                          *        
      *                           30,05,BI,A,                          *        
      *                           35,03,PD,A,                          *        
      *                           38,20,BI,A,                          *        
      *                           58,07,BI,A,                          *        
      *                           65,08,BI,A,                          *        
      *                           73,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPV001.                                                        
            05 NOMETAT-IPV001           PIC X(6) VALUE 'IPV001'.                
            05 RUPTURES-IPV001.                                                 
           10 IPV001-FDATE              PIC X(08).                      007  008
           10 IPV001-NSOCIETE           PIC X(03).                      015  003
           10 IPV001-NLIEU              PIC X(03).                      018  003
           10 IPV001-NCONC              PIC X(04).                      021  004
           10 IPV001-CHEFPROD           PIC X(05).                      025  005
           10 IPV001-CMARQ              PIC X(05).                      030  005
           10 IPV001-WSEQFAM            PIC S9(05)      COMP-3.         035  003
           10 IPV001-LREFFOURN          PIC X(20).                      038  020
           10 IPV001-NCODIC             PIC X(07).                      058  007
           10 IPV001-DEFFET             PIC X(08).                      065  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPV001-SEQUENCE           PIC S9(04) COMP.                073  002
      *--                                                                       
           10 IPV001-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPV001.                                                   
           10 IPV001-CFAM               PIC X(05).                      075  005
           10 IPV001-CPROGRAMME         PIC X(05).                      080  005
           10 IPV001-LENSCONC           PIC X(15).                      085  015
           10 IPV001-NZONPRIX           PIC X(02).                      100  002
           10 IPV001-PEXPTTC            PIC S9(08)V9(2) COMP-3.         102  006
           10 IPV001-PSTDTTC            PIC S9(08)V9(2) COMP-3.         108  006
           10 IPV001-DFINEFFET          PIC X(08).                      114  008
            05 FILLER                      PIC X(391).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPV001-LONG           PIC S9(4)   COMP  VALUE +121.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPV001-LONG           PIC S9(4) COMP-5  VALUE +121.           
                                                                                
      *}                                                                        
