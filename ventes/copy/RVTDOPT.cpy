      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TDOPT TD00 OPTION TYPDOS/LIEU/ECRAN    *        
      *----------------------------------------------------------------*        
       01  RVTDOPT.                                                             
           05  TDOPT-CTABLEG2    PIC X(15).                                     
           05  TDOPT-CTABLEG2-REDEF REDEFINES TDOPT-CTABLEG2.                   
               10  TDOPT-TYPDOSS         PIC X(05).                             
               10  TDOPT-ECRAN           PIC X(05).                             
               10  TDOPT-TYPLIEU         PIC X(01).                             
               10  TDOPT-NOPTION         PIC X(02).                             
           05  TDOPT-WTABLEG     PIC X(80).                                     
           05  TDOPT-WTABLEG-REDEF  REDEFINES TDOPT-WTABLEG.                    
               10  TDOPT-LOPTION         PIC X(20).                             
               10  TDOPT-NAFFICHE        PIC X(02).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTDOPT-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDOPT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TDOPT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDOPT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TDOPT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
