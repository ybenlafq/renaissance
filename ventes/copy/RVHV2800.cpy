      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVHV2800                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV2800                         
      **********************************************************                
       01  RVHV2800.                                                            
           02  HV28-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV28-NLIEU                                                       
               PIC X(0003).                                                     
           02  HV28-WSEQPRO                                                     
               PIC S9(7) COMP-3.                                                
           02  HV28-DVENTE                                                      
               PIC X(0006).                                                     
           02  HV28-QPIECES                                                     
               PIC S9(5) COMP-3.                                                
           02  HV28-PCA                                                         
               PIC S9(9) COMP-3.                                                
           02  HV28-PMTACHATS                                                   
               PIC S9(9) COMP-3.                                                
           02  HV28-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVHV2800                                  
      **********************************************************                
       01  RVHV2800-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV28-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV28-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV28-LIEU-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV28-LIEU-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV28-WSEQPRO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV28-WSEQPRO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV28-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV28-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV28-QPIECES-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV28-QPIECES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV28-PCA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV28-PCA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV28-PMTACHATS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV28-PMTACHATS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV28-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  HV28-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
