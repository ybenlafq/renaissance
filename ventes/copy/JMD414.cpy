      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JMD414 AU 22/04/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-JMD414.                                                        
            05 NOMETAT-JMD414           PIC X(6) VALUE 'JMD414'.                
            05 RUPTURES-JMD414.                                                 
           10 JMD414-NMAG               PIC X(03).                      007  003
           10 JMD414-WSEQFAM            PIC 9(05).                      010  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 JMD414-SEQUENCE           PIC S9(04) COMP.                015  002
      *--                                                                       
           10 JMD414-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-JMD414.                                                   
           10 JMD414-CFAM               PIC X(05).                      017  005
           10 JMD414-LFAM               PIC X(20).                      022  020
           10 JMD414-LNMAG              PIC X(20).                      042  020
           10 JMD414-NSOC               PIC X(03).                      062  003
           10 JMD414-PRMP               PIC S9(07)V9(2) COMP-3.         065  005
           10 JMD414-PRMPEXER           PIC S9(07)V9(2) COMP-3.         070  005
           10 JMD414-QTE                PIC S9(06)      COMP-3.         075  004
           10 JMD414-QTEEXER            PIC S9(06)      COMP-3.         079  004
           10 JMD414-DDEB               PIC X(08).                      083  008
           10 JMD414-DFIN               PIC X(08).                      091  008
            05 FILLER                      PIC X(414).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-JMD414-LONG           PIC S9(4)   COMP  VALUE +098.           
      *                                                                         
      *--                                                                       
        01  DSECT-JMD414-LONG           PIC S9(4) COMP-5  VALUE +098.           
                                                                                
      *}                                                                        
