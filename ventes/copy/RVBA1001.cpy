      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVBA1001                           *        
      ******************************************************************        
       01  RVBA1001.                                                            
           10 BA10-NSOCIETE             PIC X(3).                               
           10 BA10-NLIEU                PIC X(3).                               
           10 BA10-DRECEP               PIC X(8).                               
           10 BA10-NECART               PIC X(2).                               
           10 BA10-CMODRGLT             PIC X(5).                               
           10 BA10-NTIERSCV             PIC X(6).                               
           10 BA10-NLETTRAGE            PIC X(6).                               
           10 BA10-PMONTANT             PIC S9(7)V9(2) USAGE COMP-3.            
           10 BA10-WRECEP               PIC X(1).                               
           10 BA10-DSYST                PIC S9(13)V USAGE COMP-3.               
           10 BA10-NLETTRAGE2           PIC X(4).                               
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVBA1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA10-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA10-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA10-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA10-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA10-DRECEP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA10-DRECEP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA10-NECART-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA10-NECART-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA10-CMODRGLT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA10-CMODRGLT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA10-NTIERSCV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA10-NTIERSCV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA10-NLETTRAGE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA10-NLETTRAGE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA10-PMONTANT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA10-PMONTANT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA10-WRECEP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA10-WRECEP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA10-NLETTRAGE2-F                                                
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  BA10-NLETTRAGE2-F                                                
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
