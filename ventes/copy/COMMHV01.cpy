      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      **************************************************************    00020000
      * COMMAREA SPECIFIQUE PRG THV01 (THV00 -> MENU)    TR: HV00  *    00030000
      *                                                                 00031000
      *                       SUIVI  DES  VENTES                   *    00040000
      *                                                                 00050000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3660  00060003
      *                                                                 00070000
      *        TRANSACTION HV01 : SUIVI  JOURNALIER  DES  VENTES   *    00080000
      *                                                                 00090000
      *------------------------------ ZONE DONNEES THV00                00110000
      *                                                                 00720200
           02 COMM-HV01-APPLI   REDEFINES  COMM-HV00-APPLI.             00721000
              03 COMM-HV01-ZONCMD     PIC X(15).                        00740102
              03 COMM-HV01-NSEMAINE   PIC X(06).                        00740103
              03 COMM-HV01-CRAYON     PIC X(05).                        00740202
              03 COMM-HV01-LRAYON     PIC X(20).                        00740302
              03 COMM-HV01-CFAM       PIC X(05).                        00740402
              03 COMM-HV01-LFAM       PIC X(20).                        00740502
              03 COMM-HV01-CINDIC     PIC X(02).                        00740603
              03 COMM-HV01-DANNEE     PIC X(04).                        00740702
              03 COMM-HV01-CTYPE      PIC X(02).                        00740803
      *                                                                 00741100
      *------------------------------ ZONE DONNEES THV01                00741200
      *                                                                 00741300
              03 COMM-HV01-DLUNDI     PIC X(04).                        00741404
              03 COMM-HV01-DMARDI     PIC X(04).                        00741504
              03 COMM-HV01-DMERCREDI  PIC X(04).                        00741604
              03 COMM-HV01-DJEUDI     PIC X(04).                        00741704
              03 COMM-HV01-DVENDREDI  PIC X(04).                        00741804
              03 COMM-HV01-DSAMEDI    PIC X(04).                        00741904
              03 COMM-HV01-DDIMANCHE  PIC X(04).                        00742004
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-HV01-ITEM       PIC 9(04) COMP.                   00742104
      *--                                                                       
              03 COMM-HV01-ITEM       PIC 9(04) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-HV01-POS-MAX    PIC 9(04) COMP.                   00742200
      *--                                                                       
              03 COMM-HV01-POS-MAX    PIC 9(04) COMP-5.                         
      *}                                                                        
              03 FILLER               PIC X(3613).                      00743004
      *                                                                 00750000
                                                                                
