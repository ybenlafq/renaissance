      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVSL2400                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSL2400                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVSL2400.                                                            
           02  SL24-NCODIC       PIC X(0007).                                   
           02  SL24-DDELIV       PIC X(0008).                                   
           02  SL24-WEMPORTE     PIC X(0001).                                   
           02  SL24-NSOCIETE     PIC X(0003).                                   
           02  SL24-NLIEU        PIC X(0003).                                   
           02  SL24-NVENTE       PIC X(0007).                                   
           02  SL24-ETATLIGNE    PIC X(0007).                                   
           02  SL24-ETATVENTE    PIC X(0007).                                   
           02  SL24-QTE          PIC S9(5) COMP-3.                              
           02  SL24-QTEAL        PIC S9(5) COMP-3.                              
           02  SL24-EMPL         PIC X(0015).                                   
           02  SL24-VERSION      PIC X(0005).                                   
           02  SL24-MONO         PIC X(0001).                                   
           02  SL24-DCREATION    PIC X(0023).                                   
           02  SL24-VALIDATOS    PIC X(0001).                                   
           02  SL24-DATEATOS     PIC X(0023).                                   
           02  SL24-NSEQNQ       PIC S9(5) COMP-3.                              
           02  SL24-FORMATM      PIC X(0010).                                   
           02  SL24-FORMAT       PIC X(0050).                                   
           02  SL24-WDACEM       PIC X(0011).                                   
           02  SL24-LCOMMENT     PIC X(0025).                                   
           02  SL24-TIMESTP      PIC X(0026).                                   
           02  SL24-DATE         PIC X(0008).                                   
           02  SL24-NSEQF        PIC S9(5) COMP-3.                              
       01  RVSL2400-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-NCODIC-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-NCODIC-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-DDELIV-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-DDELIV-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-WEMPORTE-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-WEMPORTE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-NSOCIETE-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-NSOCIETE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-NLIEU-F        PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-NLIEU-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-NVENTE-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-NVENTE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-ETATLIGNE-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-ETATLIGNE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-ETATVENTE-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-ETATVENTE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-QTE-F          PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-QTE-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-QTEAL-F        PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-QTEAL-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-EMPL-F         PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-EMPL-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-VERSION-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-VERSION-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-MONO-F         PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-MONO-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-DCREATION-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-DCREATION-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-VALIDATOS-F    PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-VALIDATOS-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-DATEATOS-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-DATEATOS-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-NSEQNQ-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-NSEQNQ-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-FORMATM-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-FORMATM-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-FORMAT-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-FORMAT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-WDACEM-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-WDACEM-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-LCOMMENT-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-LCOMMENT-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-TIMESTP-F      PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-TIMESTP-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-DATE-F         PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-DATE-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL24-NSEQF-F        PIC S9(4) COMP.                              
      *--                                                                       
           02  SL24-NSEQF-F        PIC S9(4) COMP-5.                            
      *}                                                                        
       EJECT                                                                    
                                                                                
