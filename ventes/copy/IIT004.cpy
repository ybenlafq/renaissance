      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IIT004 AU 04/04/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,03,BI,A,                          *        
      *                           15,03,BI,A,                          *        
      *                           18,05,BI,A,                          *        
      *                           23,05,BI,A,                          *        
      *                           28,09,BI,A,                          *        
      *                           37,07,BI,A,                          *        
      *                           44,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IIT004.                                                        
            05 NOMETAT-IIT004           PIC X(6) VALUE 'IIT004'.                
            05 RUPTURES-IIT004.                                                 
           10 IIT004-NINVENTAIRE        PIC X(05).                      007  005
           10 IIT004-NSOCIETE           PIC X(03).                      012  003
           10 IIT004-NLIEU              PIC X(03).                      015  003
           10 IIT004-CFAM               PIC X(05).                      018  005
           10 IIT004-CMARQ              PIC X(05).                      023  005
           10 IIT004-NLIEUCFAM          PIC X(09).                      028  009
           10 IIT004-NCODIC             PIC X(07).                      037  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IIT004-SEQUENCE           PIC S9(04) COMP.                044  002
      *--                                                                       
           10 IIT004-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IIT004.                                                   
           10 IIT004-LLIEU              PIC X(20).                      046  020
           10 IIT004-LREFFOURN          PIC X(20).                      066  020
           10 IIT004-QADME              PIC S9(06)      COMP-3.         086  004
           10 IIT004-QART               PIC S9(06)      COMP-3.         090  004
           10 IIT004-QHC                PIC S9(06)      COMP-3.         094  004
           10 IIT004-QHE                PIC S9(06)      COMP-3.         098  004
           10 IIT004-QHRE               PIC S9(05)      COMP-3.         102  003
           10 IIT004-QHT                PIC S9(06)      COMP-3.         105  004
           10 IIT004-QTOTE              PIC S9(06)      COMP-3.         109  004
           10 IIT004-QTOTR              PIC S9(06)      COMP-3.         113  004
           10 IIT004-QTT                PIC S9(06)      COMP-3.         117  004
           10 IIT004-VADME              PIC S9(08)V9(2) COMP-3.         121  006
           10 IIT004-VHE                PIC S9(08)V9(2) COMP-3.         127  006
           10 IIT004-VTOTE              PIC S9(08)V9(2) COMP-3.         133  006
           10 IIT004-DCOMPTAGE          PIC X(08).                      139  008
           10 IIT004-DEDITSUPPORT       PIC X(08).                      147  008
           10 IIT004-DEXTRACTION        PIC X(08).                      155  008
           10 IIT004-DSTOCKTHEO         PIC X(08).                      163  008
            05 FILLER                      PIC X(342).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IIT004-LONG           PIC S9(4)   COMP  VALUE +170.           
      *                                                                         
      *--                                                                       
        01  DSECT-IIT004-LONG           PIC S9(4) COMP-5  VALUE +170.           
                                                                                
      *}                                                                        
