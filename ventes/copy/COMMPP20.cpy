      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : PREPARATION DE PAYE                              *        
      *  PROGRAMME  : MPP20                                            *        
      *  TITRE      : COMMAREA DU MODULE TP DE CALCUL DE               *        
      *               L'ENVELOPPE D'UNE CATEGORIE DE PRIME             *        
      *  LONGUEUR   : 3000 C                                           *        
      *                                                                *        
      *  ENTREE     : COMM-PP20-RTPP00-EXISTE COMM-PP20-LTYPRUB        *        
      *               COMM-PP20-RUBRIQUE                               *        
      *  SORTIE     : COMM-PP20-MESSAGE       COMM-PP20-NLGERR         *        
      *               COMM-PP20-PENVELOPPE    COMM-PP20-PTOTAL         *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
       01  COMM-PP20-DONNEES.                                                   
      *--- CODE RETOUR + MESSAGE ------------------------------------ 62        
           05 COMM-PP20-MESSAGE.                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       10 COMM-PP20-CODRET           PIC S9(04) COMP.                    
      *--                                                                       
              10 COMM-PP20-CODRET           PIC S9(04) COMP-5.                  
      *}                                                                        
              10 COMM-PP20-LIBERR           PIC X(58).                          
           05 COMM-PP20-NLGERR          PIC 9(02).                              
      *--- DONNEES ENVELOPPE (RTPP00) ------------------------------- 17        
           05 COMM-PP20-RTPP00-EXISTE   PIC X(01).                              
              88 COMM-PP20-RTPP00-EXISTENCE                   VALUE '1'.        
           05 COMM-PP20-PENVELOPPE-F    PIC S9(11)V9999 COMP-3.                 
           05 COMM-PP20-PENVELOPPE-E    PIC S9(11)V9999 COMP-3.                 
      *---    DONNEES TYPE RUBRIQUE (1->MVTS 2->INTER 3->FRANCS) --- 111        
           05 COMM-PP20-TYPRUB          OCCURS 3.                               
              10 COMM-PP20-LTYPRUB         PIC X(30).                           
              10 COMM-PP20-PTOTAL          PIC S9(11)V99   COMP-3.              
      *---    DONNEES RUBRIQUE (14 * 127) ------------------------- 1778        
           05 COMM-PP20-RUBRIQUE        OCCURS 14.                              
              10 COMM-PP20-RTPP01-EXISTE   PIC X(01).                           
                 88 COMM-PP20-RTPP01-EXISTENCE                VALUE '1'.        
              10 COMM-PP20-NLIGNE          PIC X(02).                           
                 88 COMM-PP20-LIGNE-MOUVT          VALUE '01' THRU '10'.        
                 88 COMM-PP20-LIGNE-INTER          VALUE '11' THRU '12'.        
                 88 COMM-PP20-LIGNE-INTER1         VALUE '11'.                  
                 88 COMM-PP20-LIGNE-INTER2         VALUE '12'.                  
                 88 COMM-PP20-LIGNE-FRANC          VALUE '13' THRU '14'.        
              10 COMM-PP20-LRUB            PIC X(30).                           
              10 COMM-PP20-CORIGINEZIN     PIC X(02).                           
              10 COMM-PP20-CORIGINEZ1      PIC X(02).                           
              10 COMM-PP20-CORIGINEZ2      PIC X(02).                           
              10 COMM-PP20-CORIGINEZ3      PIC X(02).                           
              10 COMM-PP20-COPERATEUR1     PIC X(01).                           
              10 COMM-PP20-COPERATEUR2     PIC X(01).                           
              10 COMM-PP20-CPRIORITE       PIC X(01).                           
              10 COMM-PP20-CCALCUL         PIC X(01).                           
              10 COMM-PP20-RTPP01-FICHIER  PIC X(41).                           
              10 COMM-PP20-RTPP01-ECRAN.                                        
                 15 COMM-PP20-NRUB            PIC X(02).                        
                 15 COMM-PP20-PMONTANTZIN     PIC S9(11)V9999 COMP-3.           
                 15 COMM-PP20-PMONTANTZ1      PIC S9(11)V9999 COMP-3.           
                   88 COMM-PP20-Z1-NON-SAISIE VALUE 99999999999,9999.           
                 15 COMM-PP20-PMONTANTZ2      PIC S9(11)V9999 COMP-3.           
                   88 COMM-PP20-Z2-NON-SAISIE VALUE 99999999999,9999.           
                 15 COMM-PP20-PMONTANTZ3      PIC S9(11)V9999 COMP-3.           
                   88 COMM-PP20-Z3-NON-SAISIE VALUE 99999999999,9999.           
                 15 COMM-PP20-PRESULTAT       PIC S9(11)V99   COMP-3.           
                   88 COMM-PP20-RES-NON-SAISIE VALUE 99999999999,99.            
      *---    DONNEES SAUVEGARDE ZONE A ET B ------------------------ 17        
           05 COMM-PP20-SAVE.                                                   
              10 COMM-PP20-TOP-PASSAGE     PIC X(01).                           
                 88 COMM-PP20-PREMIER-PASSAGE                 VALUE ' '.        
              10 COMM-PP20-PMONTANT-A      PIC S9(11)V9999 COMP-3.              
              10 COMM-PP20-PMONTANT-B      PIC S9(11)V9999 COMP-3.              
      *--- DISPONIBLES -------------------------------------------- 1013        
           05 COMM-PP20-FILLER          PIC X(1013).                            
                                                                                
