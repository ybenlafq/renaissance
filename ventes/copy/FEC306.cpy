      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00000010
      * FICHIER DE MORPHEUS : VENTES EN ALLOCATION                   *  00000020
      ****************************************************************  00000030
       01  FEC306-DSECT.                                                        
  1        05  FEC306-NCODIC     PIC X(07).                                     
  3        05  FEC306-DDELIV     PIC X(08).                                     
  6        05  FEC306-WEMPORTE   PIC X(01).                                     
  6        05  FEC306-NSOC       PIC X(03).                                     
  6        05  FEC306-NLIEU      PIC X(03).                                     
  6        05  FEC306-NVENTE     PIC X(07).                                     
  6        05  FEC306-ETATLIGNE  PIC X(07).                                     
  6        05  FEC306-ETATVENTE  PIC X(07).                                     
  6        05  FEC306-QTE        PIC X(05).                                     
  6        05  FEC306-QTEAL      PIC X(05).                                     
  6        05  FEC306-EMPL       PIC X(15).                                     
  6        05  FEC306-VERSION    PIC X(05).                                     
  6        05  FEC306-MONO       PIC X(01).                                     
  6        05  FEC306-DCREATION  PIC X(23).                                     
  6        05  FEC306-VALIDATOS  PIC X(01).                                     
  6        05  FEC306-DATEATOS   PIC X(23).                                     
  6        05  FEC306-NSEQNQ     PIC X(05).                                     
      *    05  FEC306-FORMATM    PIC X(10).                                     
  6        05  FEC306-FORMAT     PIC X(50).                                     
  6        05  FEC306-WDACEM     PIC X(11).                                     
  6        05  FEC306-LCOMMENT   PIC X(25).                                     
 11 * *                                                                 00000050
      **** FICHIER BRUT **********************************************  00000030
       01  FEC306-ENR.                                                          
  1        05  FEC306-ENR-NCODIC     PIC X(07).                                 
  3        05  FEC306-ENR-DDELIV     PIC X(08).                                 
  6        05  FEC306-ENR-WEMPORTE   PIC X(01).                                 
  6        05  FEC306-ENR-NSOC       PIC X(03).                                 
  6        05  FEC306-ENR-NLIEU      PIC X(03).                                 
  6        05  FEC306-ENR-NVENTE     PIC X(07).                                 
  6        05  FEC306-ENR-RESTE      PIC X(300).                                
                                                                                
