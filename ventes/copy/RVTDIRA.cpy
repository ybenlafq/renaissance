      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TDIRA CDOSSIER EXCLUS DU WARI          *        
      *----------------------------------------------------------------*        
       01  RVTDIRA.                                                             
           05  TDIRA-CTABLEG2    PIC X(15).                                     
           05  TDIRA-CTABLEG2-REDEF REDEFINES TDIRA-CTABLEG2.                   
               10  TDIRA-TYPDOSS         PIC X(05).                             
               10  TDIRA-CDOSSIER        PIC X(05).                             
           05  TDIRA-WTABLEG     PIC X(80).                                     
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTDIRA-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDIRA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TDIRA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDIRA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TDIRA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
