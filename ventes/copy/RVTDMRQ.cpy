      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TDMRQ CODE MARQUE                      *        
      *----------------------------------------------------------------*        
       01  RVTDMRQ.                                                             
           05  TDMRQ-CTABLEG2    PIC X(15).                                     
           05  TDMRQ-CTABLEG2-REDEF REDEFINES TDMRQ-CTABLEG2.                   
               10  TDMRQ-TYPDOSS         PIC X(05).                             
               10  TDMRQ-CMARQ           PIC X(05).                             
           05  TDMRQ-WTABLEG     PIC X(80).                                     
           05  TDMRQ-WTABLEG-REDEF  REDEFINES TDMRQ-WTABLEG.                    
               10  TDMRQ-LMARQ           PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTDMRQ-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDMRQ-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TDMRQ-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDMRQ-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TDMRQ-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
