      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT ICA040 AU 07/11/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,BI,A,                          *        
      *                           16,08,BI,A,                          *        
      *                           24,08,BI,A,                          *        
      *                           32,03,BI,A,                          *        
      *                           35,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-ICA040.                                                        
            05 NOMETAT-ICA040           PIC X(6) VALUE 'ICA040'.                
            05 RUPTURES-ICA040.                                                 
           10 ICA040-NSOCGRP            PIC X(03).                      007  003
           10 ICA040-NSOCIETE           PIC X(03).                      010  003
           10 ICA040-NLIEU              PIC X(03).                      013  003
           10 ICA040-DOPER              PIC X(08).                      016  008
           10 ICA040-DMVT               PIC X(08).                      024  008
           10 ICA040-CMODEL             PIC X(03).                      032  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ICA040-SEQUENCE           PIC S9(04) COMP.                035  002
      *--                                                                       
           10 ICA040-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-ICA040.                                                   
           10 ICA040-LFILIALE           PIC X(20).                      037  020
           10 ICA040-LMODEL             PIC X(20).                      057  020
           10 ICA040-PCATTCELA          PIC S9(11)V9(2) COMP-3.         077  007
           10 ICA040-PCATTCTLM          PIC S9(11)V9(2) COMP-3.         084  007
           10 ICA040-DATEDEB            PIC X(08).                      091  008
           10 ICA040-DATEFIN            PIC X(08).                      099  008
            05 FILLER                      PIC X(406).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-ICA040-LONG           PIC S9(4)   COMP  VALUE +106.           
      *                                                                         
      *--                                                                       
        01  DSECT-ICA040-LONG           PIC S9(4) COMP-5  VALUE +106.           
                                                                                
      *}                                                                        
