      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IAV002 AU 22/02/2000  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,01,BI,A,                          *        
      *                           14,01,BI,A,                          *        
      *                           15,07,BI,A,                          *        
      *                           22,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IAV002.                                                        
            05 NOMETAT-IAV002           PIC X(6) VALUE 'IAV002'.                
            05 RUPTURES-IAV002.                                                 
           10 IAV002-NSOCIETE           PIC X(03).                      007  003
           10 IAV002-NLIEU              PIC X(03).                      010  003
           10 IAV002-CANNULATION        PIC X(01).                      013  001
           10 IAV002-CTYPAVOIR          PIC X(01).                      014  001
           10 IAV002-NAVOIR             PIC X(07).                      015  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IAV002-SEQUENCE           PIC S9(04) COMP.                022  002
      *--                                                                       
           10 IAV002-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IAV002.                                                   
           10 IAV002-CDEVISE            PIC X(03).                      024  003
           10 IAV002-FMOIS              PIC X(07).                      027  007
           10 IAV002-LCOMMENT           PIC X(23).                      034  023
           10 IAV002-LDEVISE            PIC X(05).                      057  005
           10 IAV002-LLIEU              PIC X(20).                      062  020
           10 IAV002-LNOM               PIC X(20).                      082  020
           10 IAV002-PMONTANT           PIC S9(08)V9(2) COMP-3.         102  006
           10 IAV002-DEMIS              PIC X(08).                      108  008
            05 FILLER                      PIC X(397).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IAV002-LONG           PIC S9(4)   COMP  VALUE +115.           
      *                                                                         
      *--                                                                       
        01  DSECT-IAV002-LONG           PIC S9(4) COMP-5  VALUE +115.           
                                                                                
      *}                                                                        
