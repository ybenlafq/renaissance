      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TDANO TABLE DES ANOMALIES DOSSIERS     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTDANO.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTDANO.                                                             
      *}                                                                        
           05  TDANO-CTABLEG2    PIC X(15).                                     
           05  TDANO-CTABLEG2-REDEF REDEFINES TDANO-CTABLEG2.                   
               10  TDANO-CDOSSIER        PIC X(05).                             
               10  TDANO-CODANO          PIC X(05).                             
           05  TDANO-WTABLEG     PIC X(80).                                     
           05  TDANO-WTABLEG-REDEF  REDEFINES TDANO-WTABLEG.                    
               10  TDANO-NAFFICHE        PIC X(02).                             
               10  TDANO-LIBANO          PIC X(36).                             
               10  TDANO-WCTRL           PIC X(01).                             
               10  TDANO-PROV            PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTDANO-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTDANO-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDANO-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TDANO-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDANO-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TDANO-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
