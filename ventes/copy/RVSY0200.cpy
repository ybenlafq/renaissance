      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVSY0200                      *        
      ******************************************************************        
       01  RVSY0200.                                                            
      *                       NSOC                                              
           10 SY02-NSOC            PIC X(3).                                    
      *                       NLIEU                                             
           10 SY02-NLIEU           PIC X(3).                                    
      *                       CFONC                                             
           10 SY02-CFONC           PIC X(10).                                   
      *                       DTRT                                              
           10 SY02-DTRT            PIC S9(13)V USAGE COMP-3.                    
      *                       IMAJ                                              
           10 SY02-IMAJ            PIC S9(9)V USAGE COMP-3.                     
      *                       DSYST                                             
           10 SY02-DSYST           PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 6       *        
      ******************************************************************        
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVSY0200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVSY0200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SY02-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SY02-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SY02-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SY02-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SY02-CFONC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SY02-CFONC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SY02-DTRT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SY02-DTRT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SY02-IMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SY02-IMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SY02-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  SY02-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
