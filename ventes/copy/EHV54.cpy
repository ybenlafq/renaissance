      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHV54   EHV54                                              00000020
      ***************************************************************** 00000030
       01   EHV54I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAGL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MMAGL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MMAGF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MMAGI     PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNVENTEI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIENTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCLIENTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLIENTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCLIENTI  PIC X(9).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVENTEL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDVENTEF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDVENTEI  PIC X(8).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNORDREL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNORDREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNORDREF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNORDREI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMCL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNOMCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNOMCF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNOMCI    PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMCL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLNOMCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLNOMCF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLNOMCI   PIC X(25).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPNOMCL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLPNOMCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPNOMCF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLPNOMCI  PIC X(15).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVOIECL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNVOIECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVOIECF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNVOIECI  PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTPVOIECL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MTPVOIECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTPVOIECF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MTPVOIECI      PIC X(4).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVOIECL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLVOIECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLVOIECF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLVOIECI  PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCBATCL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCBATCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCBATCF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCBATCI   PIC X(3).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCESCCL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCESCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCESCCF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCESCCI   PIC X(3).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCETAGCL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCETAGCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCETAGCF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCETAGCI  PIC X(3).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPORTECL      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MCPORTECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPORTECF      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCPORTECI      PIC X(3).                                  00000770
           02 MADRESCI OCCURS   1 TIMES .                               00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLADDRCL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MLADDRCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLADDRCF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MLADDRCI     PIC X(32).                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTCL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCPOSTCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPOSTCF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCPOSTCI  PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCOMNCL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCCOMNCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCCOMNCF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCCOMNCI  PIC X(32).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPOSTCL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MLPOSTCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPOSTCF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MLPOSTCI  PIC X(26).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD10L  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MTELD10L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD10F  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MTELD10I  PIC X(2).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD11L  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MTELD11L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD11F  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MTELD11I  PIC X(2).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD12L  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MTELD12L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD12F  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MTELD12I  PIC X(2).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD13L  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MTELD13L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD13F  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MTELD13I  PIC X(2).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD14L  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MTELD14L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD14F  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MTELD14I  PIC X(2).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB10L  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MTELB10L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB10F  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MTELB10I  PIC X(2).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB11L  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MTELB11L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB11F  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MTELB11I  PIC X(2).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB12L  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MTELB12L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB12F  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MTELB12I  PIC X(2).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB13L  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MTELB13L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB13F  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MTELB13I  PIC X(2).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB14L  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MTELB14L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB14F  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MTELB14I  PIC X(2).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPOSTEL   COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MPOSTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPOSTEF   PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MPOSTEI   PIC X(5).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRETR1L      COMP PIC S9(4).                            00001390
      *--                                                                       
           02 MADRETR1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MADRETR1F      PIC X.                                     00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MADRETR1I      PIC X(31).                                 00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRETR2L      COMP PIC S9(4).                            00001430
      *--                                                                       
           02 MADRETR2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MADRETR2F      PIC X.                                     00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MADRETR2I      PIC X(31).                                 00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRETR3L      COMP PIC S9(4).                            00001470
      *--                                                                       
           02 MADRETR3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MADRETR3F      PIC X.                                     00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MADRETR3I      PIC X(31).                                 00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRETR4L      COMP PIC S9(4).                            00001510
      *--                                                                       
           02 MADRETR4L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MADRETR4F      PIC X.                                     00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MADRETR4I      PIC X(31).                                 00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRETR5L      COMP PIC S9(4).                            00001550
      *--                                                                       
           02 MADRETR5L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MADRETR5F      PIC X.                                     00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MADRETR5I      PIC X(31).                                 00001580
           02 MLLIVRI OCCURS   2 TIMES .                                00001590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMENL     COMP PIC S9(4).                            00001600
      *--                                                                       
             03 MCOMMENL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCOMMENF     PIC X.                                     00001610
             03 FILLER  PIC X(4).                                       00001620
             03 MCOMMENI     PIC X(78).                                 00001630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001640
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001650
           02 FILLER    PIC X(4).                                       00001660
           02 MZONCMDI  PIC X(15).                                      00001670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001680
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001690
           02 FILLER    PIC X(4).                                       00001700
           02 MLIBERRI  PIC X(58).                                      00001710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001720
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001730
           02 FILLER    PIC X(4).                                       00001740
           02 MCODTRAI  PIC X(4).                                       00001750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001760
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001770
           02 FILLER    PIC X(4).                                       00001780
           02 MCICSI    PIC X(5).                                       00001790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001800
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001810
           02 FILLER    PIC X(4).                                       00001820
           02 MNETNAMI  PIC X(8).                                       00001830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001840
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001850
           02 FILLER    PIC X(4).                                       00001860
           02 MSCREENI  PIC X(4).                                       00001870
      ***************************************************************** 00001880
      * SDF: EHV54   EHV54                                              00001890
      ***************************************************************** 00001900
       01   EHV54O REDEFINES EHV54I.                                    00001910
           02 FILLER    PIC X(12).                                      00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MDATJOUA  PIC X.                                          00001940
           02 MDATJOUC  PIC X.                                          00001950
           02 MDATJOUP  PIC X.                                          00001960
           02 MDATJOUH  PIC X.                                          00001970
           02 MDATJOUV  PIC X.                                          00001980
           02 MDATJOUO  PIC X(10).                                      00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MTIMJOUA  PIC X.                                          00002010
           02 MTIMJOUC  PIC X.                                          00002020
           02 MTIMJOUP  PIC X.                                          00002030
           02 MTIMJOUH  PIC X.                                          00002040
           02 MTIMJOUV  PIC X.                                          00002050
           02 MTIMJOUO  PIC X(5).                                       00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MWPAGEA   PIC X.                                          00002080
           02 MWPAGEC   PIC X.                                          00002090
           02 MWPAGEP   PIC X.                                          00002100
           02 MWPAGEH   PIC X.                                          00002110
           02 MWPAGEV   PIC X.                                          00002120
           02 MWPAGEO   PIC X(3).                                       00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MMAGA     PIC X.                                          00002150
           02 MMAGC     PIC X.                                          00002160
           02 MMAGP     PIC X.                                          00002170
           02 MMAGH     PIC X.                                          00002180
           02 MMAGV     PIC X.                                          00002190
           02 MMAGO     PIC X(3).                                       00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MNVENTEA  PIC X.                                          00002220
           02 MNVENTEC  PIC X.                                          00002230
           02 MNVENTEP  PIC X.                                          00002240
           02 MNVENTEH  PIC X.                                          00002250
           02 MNVENTEV  PIC X.                                          00002260
           02 MNVENTEO  PIC X(7).                                       00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MCLIENTA  PIC X.                                          00002290
           02 MCLIENTC  PIC X.                                          00002300
           02 MCLIENTP  PIC X.                                          00002310
           02 MCLIENTH  PIC X.                                          00002320
           02 MCLIENTV  PIC X.                                          00002330
           02 MCLIENTO  PIC X(9).                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MDVENTEA  PIC X.                                          00002360
           02 MDVENTEC  PIC X.                                          00002370
           02 MDVENTEP  PIC X.                                          00002380
           02 MDVENTEH  PIC X.                                          00002390
           02 MDVENTEV  PIC X.                                          00002400
           02 MDVENTEO  PIC X(8).                                       00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MNORDREA  PIC X.                                          00002430
           02 MNORDREC  PIC X.                                          00002440
           02 MNORDREP  PIC X.                                          00002450
           02 MNORDREH  PIC X.                                          00002460
           02 MNORDREV  PIC X.                                          00002470
           02 MNORDREO  PIC X(5).                                       00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MNOMCA    PIC X.                                          00002500
           02 MNOMCC    PIC X.                                          00002510
           02 MNOMCP    PIC X.                                          00002520
           02 MNOMCH    PIC X.                                          00002530
           02 MNOMCV    PIC X.                                          00002540
           02 MNOMCO    PIC X(5).                                       00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MLNOMCA   PIC X.                                          00002570
           02 MLNOMCC   PIC X.                                          00002580
           02 MLNOMCP   PIC X.                                          00002590
           02 MLNOMCH   PIC X.                                          00002600
           02 MLNOMCV   PIC X.                                          00002610
           02 MLNOMCO   PIC X(25).                                      00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MLPNOMCA  PIC X.                                          00002640
           02 MLPNOMCC  PIC X.                                          00002650
           02 MLPNOMCP  PIC X.                                          00002660
           02 MLPNOMCH  PIC X.                                          00002670
           02 MLPNOMCV  PIC X.                                          00002680
           02 MLPNOMCO  PIC X(15).                                      00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MNVOIECA  PIC X.                                          00002710
           02 MNVOIECC  PIC X.                                          00002720
           02 MNVOIECP  PIC X.                                          00002730
           02 MNVOIECH  PIC X.                                          00002740
           02 MNVOIECV  PIC X.                                          00002750
           02 MNVOIECO  PIC X(5).                                       00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MTPVOIECA      PIC X.                                     00002780
           02 MTPVOIECC PIC X.                                          00002790
           02 MTPVOIECP PIC X.                                          00002800
           02 MTPVOIECH PIC X.                                          00002810
           02 MTPVOIECV PIC X.                                          00002820
           02 MTPVOIECO      PIC X(4).                                  00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MLVOIECA  PIC X.                                          00002850
           02 MLVOIECC  PIC X.                                          00002860
           02 MLVOIECP  PIC X.                                          00002870
           02 MLVOIECH  PIC X.                                          00002880
           02 MLVOIECV  PIC X.                                          00002890
           02 MLVOIECO  PIC X(20).                                      00002900
           02 FILLER    PIC X(2).                                       00002910
           02 MCBATCA   PIC X.                                          00002920
           02 MCBATCC   PIC X.                                          00002930
           02 MCBATCP   PIC X.                                          00002940
           02 MCBATCH   PIC X.                                          00002950
           02 MCBATCV   PIC X.                                          00002960
           02 MCBATCO   PIC X(3).                                       00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MCESCCA   PIC X.                                          00002990
           02 MCESCCC   PIC X.                                          00003000
           02 MCESCCP   PIC X.                                          00003010
           02 MCESCCH   PIC X.                                          00003020
           02 MCESCCV   PIC X.                                          00003030
           02 MCESCCO   PIC X(3).                                       00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MCETAGCA  PIC X.                                          00003060
           02 MCETAGCC  PIC X.                                          00003070
           02 MCETAGCP  PIC X.                                          00003080
           02 MCETAGCH  PIC X.                                          00003090
           02 MCETAGCV  PIC X.                                          00003100
           02 MCETAGCO  PIC X(3).                                       00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MCPORTECA      PIC X.                                     00003130
           02 MCPORTECC PIC X.                                          00003140
           02 MCPORTECP PIC X.                                          00003150
           02 MCPORTECH PIC X.                                          00003160
           02 MCPORTECV PIC X.                                          00003170
           02 MCPORTECO      PIC X(3).                                  00003180
           02 MADRESCO OCCURS   1 TIMES .                               00003190
             03 FILLER       PIC X(2).                                  00003200
             03 MLADDRCA     PIC X.                                     00003210
             03 MLADDRCC     PIC X.                                     00003220
             03 MLADDRCP     PIC X.                                     00003230
             03 MLADDRCH     PIC X.                                     00003240
             03 MLADDRCV     PIC X.                                     00003250
             03 MLADDRCO     PIC X(32).                                 00003260
           02 FILLER    PIC X(2).                                       00003270
           02 MCPOSTCA  PIC X.                                          00003280
           02 MCPOSTCC  PIC X.                                          00003290
           02 MCPOSTCP  PIC X.                                          00003300
           02 MCPOSTCH  PIC X.                                          00003310
           02 MCPOSTCV  PIC X.                                          00003320
           02 MCPOSTCO  PIC X(5).                                       00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MCCOMNCA  PIC X.                                          00003350
           02 MCCOMNCC  PIC X.                                          00003360
           02 MCCOMNCP  PIC X.                                          00003370
           02 MCCOMNCH  PIC X.                                          00003380
           02 MCCOMNCV  PIC X.                                          00003390
           02 MCCOMNCO  PIC X(32).                                      00003400
           02 FILLER    PIC X(2).                                       00003410
           02 MLPOSTCA  PIC X.                                          00003420
           02 MLPOSTCC  PIC X.                                          00003430
           02 MLPOSTCP  PIC X.                                          00003440
           02 MLPOSTCH  PIC X.                                          00003450
           02 MLPOSTCV  PIC X.                                          00003460
           02 MLPOSTCO  PIC X(26).                                      00003470
           02 FILLER    PIC X(2).                                       00003480
           02 MTELD10A  PIC X.                                          00003490
           02 MTELD10C  PIC X.                                          00003500
           02 MTELD10P  PIC X.                                          00003510
           02 MTELD10H  PIC X.                                          00003520
           02 MTELD10V  PIC X.                                          00003530
           02 MTELD10O  PIC X(2).                                       00003540
           02 FILLER    PIC X(2).                                       00003550
           02 MTELD11A  PIC X.                                          00003560
           02 MTELD11C  PIC X.                                          00003570
           02 MTELD11P  PIC X.                                          00003580
           02 MTELD11H  PIC X.                                          00003590
           02 MTELD11V  PIC X.                                          00003600
           02 MTELD11O  PIC X(2).                                       00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MTELD12A  PIC X.                                          00003630
           02 MTELD12C  PIC X.                                          00003640
           02 MTELD12P  PIC X.                                          00003650
           02 MTELD12H  PIC X.                                          00003660
           02 MTELD12V  PIC X.                                          00003670
           02 MTELD12O  PIC X(2).                                       00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MTELD13A  PIC X.                                          00003700
           02 MTELD13C  PIC X.                                          00003710
           02 MTELD13P  PIC X.                                          00003720
           02 MTELD13H  PIC X.                                          00003730
           02 MTELD13V  PIC X.                                          00003740
           02 MTELD13O  PIC X(2).                                       00003750
           02 FILLER    PIC X(2).                                       00003760
           02 MTELD14A  PIC X.                                          00003770
           02 MTELD14C  PIC X.                                          00003780
           02 MTELD14P  PIC X.                                          00003790
           02 MTELD14H  PIC X.                                          00003800
           02 MTELD14V  PIC X.                                          00003810
           02 MTELD14O  PIC X(2).                                       00003820
           02 FILLER    PIC X(2).                                       00003830
           02 MTELB10A  PIC X.                                          00003840
           02 MTELB10C  PIC X.                                          00003850
           02 MTELB10P  PIC X.                                          00003860
           02 MTELB10H  PIC X.                                          00003870
           02 MTELB10V  PIC X.                                          00003880
           02 MTELB10O  PIC X(2).                                       00003890
           02 FILLER    PIC X(2).                                       00003900
           02 MTELB11A  PIC X.                                          00003910
           02 MTELB11C  PIC X.                                          00003920
           02 MTELB11P  PIC X.                                          00003930
           02 MTELB11H  PIC X.                                          00003940
           02 MTELB11V  PIC X.                                          00003950
           02 MTELB11O  PIC X(2).                                       00003960
           02 FILLER    PIC X(2).                                       00003970
           02 MTELB12A  PIC X.                                          00003980
           02 MTELB12C  PIC X.                                          00003990
           02 MTELB12P  PIC X.                                          00004000
           02 MTELB12H  PIC X.                                          00004010
           02 MTELB12V  PIC X.                                          00004020
           02 MTELB12O  PIC X(2).                                       00004030
           02 FILLER    PIC X(2).                                       00004040
           02 MTELB13A  PIC X.                                          00004050
           02 MTELB13C  PIC X.                                          00004060
           02 MTELB13P  PIC X.                                          00004070
           02 MTELB13H  PIC X.                                          00004080
           02 MTELB13V  PIC X.                                          00004090
           02 MTELB13O  PIC X(2).                                       00004100
           02 FILLER    PIC X(2).                                       00004110
           02 MTELB14A  PIC X.                                          00004120
           02 MTELB14C  PIC X.                                          00004130
           02 MTELB14P  PIC X.                                          00004140
           02 MTELB14H  PIC X.                                          00004150
           02 MTELB14V  PIC X.                                          00004160
           02 MTELB14O  PIC X(2).                                       00004170
           02 FILLER    PIC X(2).                                       00004180
           02 MPOSTEA   PIC X.                                          00004190
           02 MPOSTEC   PIC X.                                          00004200
           02 MPOSTEP   PIC X.                                          00004210
           02 MPOSTEH   PIC X.                                          00004220
           02 MPOSTEV   PIC X.                                          00004230
           02 MPOSTEO   PIC X(5).                                       00004240
           02 FILLER    PIC X(2).                                       00004250
           02 MADRETR1A      PIC X.                                     00004260
           02 MADRETR1C PIC X.                                          00004270
           02 MADRETR1P PIC X.                                          00004280
           02 MADRETR1H PIC X.                                          00004290
           02 MADRETR1V PIC X.                                          00004300
           02 MADRETR1O      PIC X(31).                                 00004310
           02 FILLER    PIC X(2).                                       00004320
           02 MADRETR2A      PIC X.                                     00004330
           02 MADRETR2C PIC X.                                          00004340
           02 MADRETR2P PIC X.                                          00004350
           02 MADRETR2H PIC X.                                          00004360
           02 MADRETR2V PIC X.                                          00004370
           02 MADRETR2O      PIC X(31).                                 00004380
           02 FILLER    PIC X(2).                                       00004390
           02 MADRETR3A      PIC X.                                     00004400
           02 MADRETR3C PIC X.                                          00004410
           02 MADRETR3P PIC X.                                          00004420
           02 MADRETR3H PIC X.                                          00004430
           02 MADRETR3V PIC X.                                          00004440
           02 MADRETR3O      PIC X(31).                                 00004450
           02 FILLER    PIC X(2).                                       00004460
           02 MADRETR4A      PIC X.                                     00004470
           02 MADRETR4C PIC X.                                          00004480
           02 MADRETR4P PIC X.                                          00004490
           02 MADRETR4H PIC X.                                          00004500
           02 MADRETR4V PIC X.                                          00004510
           02 MADRETR4O      PIC X(31).                                 00004520
           02 FILLER    PIC X(2).                                       00004530
           02 MADRETR5A      PIC X.                                     00004540
           02 MADRETR5C PIC X.                                          00004550
           02 MADRETR5P PIC X.                                          00004560
           02 MADRETR5H PIC X.                                          00004570
           02 MADRETR5V PIC X.                                          00004580
           02 MADRETR5O      PIC X(31).                                 00004590
           02 MLLIVRO OCCURS   2 TIMES .                                00004600
             03 FILLER       PIC X(2).                                  00004610
             03 MCOMMENA     PIC X.                                     00004620
             03 MCOMMENC     PIC X.                                     00004630
             03 MCOMMENP     PIC X.                                     00004640
             03 MCOMMENH     PIC X.                                     00004650
             03 MCOMMENV     PIC X.                                     00004660
             03 MCOMMENO     PIC X(78).                                 00004670
           02 FILLER    PIC X(2).                                       00004680
           02 MZONCMDA  PIC X.                                          00004690
           02 MZONCMDC  PIC X.                                          00004700
           02 MZONCMDP  PIC X.                                          00004710
           02 MZONCMDH  PIC X.                                          00004720
           02 MZONCMDV  PIC X.                                          00004730
           02 MZONCMDO  PIC X(15).                                      00004740
           02 FILLER    PIC X(2).                                       00004750
           02 MLIBERRA  PIC X.                                          00004760
           02 MLIBERRC  PIC X.                                          00004770
           02 MLIBERRP  PIC X.                                          00004780
           02 MLIBERRH  PIC X.                                          00004790
           02 MLIBERRV  PIC X.                                          00004800
           02 MLIBERRO  PIC X(58).                                      00004810
           02 FILLER    PIC X(2).                                       00004820
           02 MCODTRAA  PIC X.                                          00004830
           02 MCODTRAC  PIC X.                                          00004840
           02 MCODTRAP  PIC X.                                          00004850
           02 MCODTRAH  PIC X.                                          00004860
           02 MCODTRAV  PIC X.                                          00004870
           02 MCODTRAO  PIC X(4).                                       00004880
           02 FILLER    PIC X(2).                                       00004890
           02 MCICSA    PIC X.                                          00004900
           02 MCICSC    PIC X.                                          00004910
           02 MCICSP    PIC X.                                          00004920
           02 MCICSH    PIC X.                                          00004930
           02 MCICSV    PIC X.                                          00004940
           02 MCICSO    PIC X(5).                                       00004950
           02 FILLER    PIC X(2).                                       00004960
           02 MNETNAMA  PIC X.                                          00004970
           02 MNETNAMC  PIC X.                                          00004980
           02 MNETNAMP  PIC X.                                          00004990
           02 MNETNAMH  PIC X.                                          00005000
           02 MNETNAMV  PIC X.                                          00005010
           02 MNETNAMO  PIC X(8).                                       00005020
           02 FILLER    PIC X(2).                                       00005030
           02 MSCREENA  PIC X.                                          00005040
           02 MSCREENC  PIC X.                                          00005050
           02 MSCREENP  PIC X.                                          00005060
           02 MSCREENH  PIC X.                                          00005070
           02 MSCREENV  PIC X.                                          00005080
           02 MSCREENO  PIC X(4).                                       00005090
                                                                                
