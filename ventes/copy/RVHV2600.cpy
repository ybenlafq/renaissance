      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVHV2600                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV2600                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHV2600.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHV2600.                                                            
      *}                                                                        
           02  HV26-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV26-NLIEU                                                       
               PIC X(0003).                                                     
           02  HV26-DMVENTE                                                     
               PIC X(0006).                                                     
           02  HV26-CGARANTIE                                                   
               PIC X(0005).                                                     
           02  HV26-NCODIC                                                      
               PIC X(0007).                                                     
           02  HV26-QNBPSE                                                      
               PIC S9(5) COMP-3.                                                
           02  HV26-PCAPSE                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV26-PMTPRIMPSE                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV26-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVHV2600                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHV2600-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHV2600-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV26-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV26-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV26-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV26-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV26-DMVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV26-DMVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV26-CGARANTIE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV26-CGARANTIE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV26-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV26-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV26-QNBPSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV26-QNBPSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV26-PCAPSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV26-PCAPSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV26-PMTPRIMPSE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV26-PMTPRIMPSE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV26-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  HV26-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
