      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00000010
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00000020
      **************************************************************    00000030
                                                                        00000040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-CL14-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00000050
      *                                                                         
      *--                                                                       
       01  COM-CL14-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
                                                                        00000060
      *}                                                                        
       01  Z-COMMAREA.                                                  00000070
                                                                        00000080
      * ZONES RESERVEES AIDA ------------------------------------- 100  00000090
          02 FILLER-COM-AIDA      PIC  X(100).                          00000091
                                                                        00000092
      * ZONES RESERVEES CICS ------------------------------------- 020  00000093
          02 COMM-CICS-APPLID     PIC  X(8).                            00000094
          02 COMM-CICS-NETNAM     PIC  X(8).                            00000095
          02 COMM-CICS-TRANSA     PIC  X(4).                            00000096
                                                                        00000097
      * DATE DU JOUR --------------------------------------------- 100  00000098
          02 COMM-DATE-SIECLE     PIC  99.                              00000099
          02 COMM-DATE-ANNEE      PIC  99.                              00000100
          02 COMM-DATE-MOIS       PIC  99.                              00000110
          02 COMM-DATE-JOUR       PIC  99.                              00000120
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000130
          02 COMM-DATE-QNTA       PIC  999.                             00000140
          02 COMM-DATE-QNT0       PIC  99999.                           00000150
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000160
          02 COMM-DATE-BISX       PIC  9.                               00000170
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00000180
          02 COMM-DATE-JSM        PIC  9.                               00000190
      *   LIBELLE DU JOUR COURT - LONG                                  00000191
          02 COMM-DATE-JSM-LC     PIC  XXX.                             00000192
          02 COMM-DATE-JSM-LL     PIC  X(8).                            00000193
      *   LIBELLE DU MOIS COURT - LONG                                  00000194
          02 COMM-DATE-MOIS-LC    PIC  XXX.                             00000195
          02 COMM-DATE-MOIS-LL    PIC  X(9).                            00000196
      *   DIFFERENTES FORMES DE DATE                                    00000197
          02 COMM-DATE-SSAAMMJJ   PIC  X(8).                            00000198
          02 COMM-DATE-AAMMJJ     PIC  X(6).                            00000199
          02 COMM-DATE-JJMMSSAA   PIC  X(8).                            00000200
          02 COMM-DATE-JJMMAA     PIC  X(6).                            00000210
          02 COMM-DATE-JJ-MM-AA   PIC  X(8).                            00000220
          02 COMM-DATE-JJ-MM-SSAA PIC  X(10).                           00000230
      *   TRAITEMENT NUMERO DE SEMAINE                                  00000240
          02 COMM-DATE-WEEK.                                            00000250
             05 COMM-DATE-SEMSS   PIC  99.                              00000260
             05 COMM-DATE-SEMAA   PIC  99.                              00000270
             05 COMM-DATE-SEMNU   PIC  99.                              00000280
          02 COMM-DATE-FILLER     PIC  X(07).                           00000290
                                                                        00000291
      * ATTRIBUTS BMS======================================== 4 + AAAA  00000292
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              00000293
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-CL14-ZMAP       PIC  X(1400).                         00000294
                                                                        00000295
      * ZONES APPLICATIVES FF00 ================================= BBBB  00000296
      * ZONES ISSUE DE LA SAISIE DES PIECES COMPTABLE TFF51             00000301
          02 COMM-CL14-CACID      PIC  X(8).                            00000302
          02 COMM-CL14-NSOCIETE   PIC  X(3).                            00000303
          02 COMM-CL14-LSOCGCA    PIC  X(2).                            00000304
          02 COMM-CL14-PREFT.                                           00000305
             03 COMM-CL14-PREFT1  PIC  X(1).                            00000306
             03 COMM-CL14-PREFT2  PIC  X(1).                            00000307
          02 COMM-CL14-LTLMELA.                                         00000308
             03 COMM-CL14-WTLMELA PIC  X(1).                            00000309
             03 COMM-CL14-STLMELA PIC  X(2).                            00000310
          02 COMM-CL14-DCPT       PIC  X(8).                            00000311
          02 COMM-CL14-DEXCPT     PIC  X(4).                            00000312
          02 COMM-CL14-DPERCPT    PIC  X(2).                            00000320
      * ZONES ISSUE DE LA SAISIE DES PIECES COMPTABLE TFF52             00000330
          02 COMM-CL14-NBL        PIC  X(10).                           00000344
          02 COMM-CL14-NRECCPT    PIC  X(7).                            00000350
          02 COMM-CL14-LSOLDE     PIC  X.                               00000370
          02 COMM-CL14-CTIERS.                                          00001100
             03 COMM-CL14-CTIERS1 PIC  X(2).                            00001200
             03 COMM-CL14-CTIERS2 PIC  X(5).                            00001210
             03 COMM-CL14-CTIERS3 PIC  X(2).                            00001400
          02  COMM-CL14-REMITNAME     PIC X(20).                        00002528
          02  COMM-CL14-SHORTDESC     PIC X(14).                        00002529
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  COMM-CL14-ITEMFF52      PIC S9(4) VALUE ZERO COMP.        00002535
      *--                                                                       
          02  COMM-CL14-ITEMFF52      PIC S9(4) VALUE ZERO COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  COMM-CL14-ITEMFF53      PIC S9(4) VALUE ZERO COMP.        00002540
      *--                                                                       
          02  COMM-CL14-ITEMFF53      PIC S9(4) VALUE ZERO COMP-5.              
      *}                                                                        
          02  COMM-CL14-ACTION        PIC X     VALUE  SPACE.           00002540
          02  COMM-CL14-TVIDE         PIC X     VALUE  SPACE.           00002540
      /                                                                 00002541
           02  COMM-CL14.                                                       
               04  COMM-CL14-PGCHOIX         PIC  99.                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04  COMM-CL14-NUMPG           PIC  S9(2)  COMP.                  
      *--                                                                       
               04  COMM-CL14-NUMPG           PIC  S9(2) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04  COMM-CL14-NBRPG           PIC  S9(2)  COMP.                  
      *--                                                                       
               04  COMM-CL14-NBRPG           PIC  S9(2) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04  COMM-CL14-NUM-ITEM        PIC  S9(2)  COMP.                  
      *--                                                                       
               04  COMM-CL14-NUM-ITEM        PIC  S9(2) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04  COMM-CL14-NBR-ITEM        PIC  S9(2)  COMP.                  
      *--                                                                       
               04  COMM-CL14-NBR-ITEM        PIC  S9(2) COMP-5.                 
      *}                                                                        
               04  COMM-CL14-LIGNE.                                             
                   06  COMM-CL14-CTYPDOC1    PIC  X(02).                        
                   06  COMM-CL14-COPAR1      PIC  X(05).                        
                   06  COMM-CL14-LOPER       PIC  X(30).                        
                   06  COMM-CL14-CPROG       PIC  X(10).                        
                   06  COMM-CL14-COPER       PIC  X(10).                        
                   06  COMM-CL14-CTYPDOC     PIC  X(02).                        
                   06  COMM-CL14-COPAR       PIC  X(10).                        
                   06  COMM-CL14-EVENT       PIC  X(01).                        
                   06  COMM-CL14-NSEQ        PIC  X(01).                        
                   06  COMM-CL14-WHOST       PIC  X(01).                        
                   06  COMM-CL14-WEXIST      PIC  X(01).                        
                   06  COMM-CL14-NSOCO       PIC  X(03).                        
                   06  COMM-CL14-NLIEUO      PIC  X(03).                        
                   06  COMM-CL14-NSLIEUO     PIC  X(03).                        
                   06  COMM-CL14-CLITRTO     PIC  X(03).                        
                   06  COMM-CL14-NSOCD       PIC  X(03).                        
                   06  COMM-CL14-NLIEUD      PIC  X(03).                        
                   06  COMM-CL14-NSLIEUD     PIC  X(03).                        
                   06  COMM-CL14-CLITRTD     PIC  X(03).                        
                   06  COMM-CL14-CDSUPP      PIC  X(01).                        
               04  COMM-CL14-TEST.                                              
                   06  COMM-CL14-CTYPDOC1-2  PIC  X(02).                        
                   06  COMM-CL14-COPAR1-2    PIC  X(05).                        
                   06  COMM-CL14-LOPER-2     PIC  X(30).                        
                   06  COMM-CL14-CPROG-2     PIC  X(10).                        
                   06  COMM-CL14-COPER-2     PIC  X(10).                        
                   06  COMM-CL14-CTYPDOC-2   PIC  X(02).                        
                   06  COMM-CL14-COPAR-2     PIC  X(10).                        
                   06  COMM-CL14-EVENT-2     PIC  X(01).                        
                   06  COMM-CL14-NSEQ-2      PIC  X(01).                        
                   06  COMM-CL14-WHOST-2     PIC  X(01).                        
                   06  COMM-CL14-WEXIST-2    PIC  X(01).                        
                   06  COMM-CL14-NSOCO-2     PIC  X(03).                        
                   06  COMM-CL14-NLIEUO-2    PIC  X(03).                        
                   06  COMM-CL14-NSLIEUO-2   PIC  X(03).                        
                   06  COMM-CL14-CLITRTO-2   PIC  X(03).                        
                   06  COMM-CL14-NSOCD-2     PIC  X(03).                        
                   06  COMM-CL14-NLIEUD-2    PIC  X(03).                        
                   06  COMM-CL14-NSLIEUD-2   PIC  X(03).                        
                   06  COMM-CL14-CLITRTD-2   PIC  X(03).                        
                   06  COMM-CL14-CDSUPP-2    PIC  X(01).                        
                                                                                
