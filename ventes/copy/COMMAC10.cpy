      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * COMMAREA AC10 - RESULTATS                                               
      ******************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-AC00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-AC00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
       01  Z-COMMAREA.                                                          
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
          02 FILLER-COM-AIDA      PIC X(100).                                   
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
          02 COMM-CICS-APPLID     PIC X(8).                                     
          02 COMM-CICS-NETNAM     PIC X(8).                                     
          02 COMM-CICS-TRANSA     PIC X(4).                                     
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
          02 COMM-DATE-SIECLE     PIC XX.                                       
          02 COMM-DATE-ANNEE      PIC XX.                                       
          02 COMM-DATE-MOIS       PIC XX.                                       
          02 COMM-DATE-JOUR       PIC XX.                                       
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
          02 COMM-DATE-QNTA       PIC 999.                                      
          02 COMM-DATE-QNT0       PIC 99999.                                    
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
          02 COMM-DATE-BISX       PIC 9.                                        
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                   
          02 COMM-DATE-JSM        PIC 9.                                        
      *   LIBELLES DU JOUR COURT - LONG                                         
          02 COMM-DATE-JSM-LC     PIC XXX.                                      
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                 
      *   LIBELLES DU MOIS COURT - LONG                                         
          02 COMM-DATE-MOIS-LC    PIC XXX.                                      
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                 
      *   DIFFERENTES FORMES DE DATE                                            
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                                     
          02 COMM-DATE-AAMMJJ     PIC X(6).                                     
          02 COMM-DATE-JJMMSSAA   PIC X(8).                                     
          02 COMM-DATE-JJMMAA     PIC X(6).                                     
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                                     
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                    
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
          02 COMM-DATE-WEEK.                                                    
             05 COMM-DATE-SEMSS   PIC 9(02).                                    
             05 COMM-DATE-SEMAA   PIC 9(02).                                    
             05 COMM-DATE-SEMNU   PIC 9(02).                                    
          02 COMM-DATE-FILLER     PIC X(08).                                    
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                      
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 200 PIC X(1).                                
      ******************************************************************        
      * APPLI AC10                                                              
      ******************************************************************        
          02 COMM-AC10-APPLI.                                                   
             03 COMM-AC10-CACID          PIC X(8).                              
             03 COMM-AC10-NSOCIETE       PIC X(3).                              
             03 COMM-AC10-NLIEU          PIC X(3).                              
             03 COMM-AC10-LLIEU          PIC X(20).                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-AC10-PAGEA          PIC S9(4) BINARY.                      
      *--                                                                       
             03 COMM-AC10-PAGEA          PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-AC10-NBP            PIC S9(4) BINARY.                      
      *--                                                                       
             03 COMM-AC10-NBP            PIC S9(4) COMP-5.                      
      *}                                                                        
             03 COMM-AC10-DDEBUT         PIC X(8).                              
             03 COMM-AC10-JDEBUT         PIC X(8).                              
             03 COMM-AC10-DFIN           PIC X(8).                              
             03 COMM-AC10-JFIN           PIC X(8).                              
             03 COMM-AC10-DDEBUT-J-EQUI  PIC X(8).                              
             03 COMM-AC10-JDEBUT-J-EQUI  PIC X(8).                              
             03 COMM-AC10-DFIN-J-EQUI    PIC X(8).                              
             03 COMM-AC10-JFIN-J-EQUI    PIC X(8).                              
             03 COMM-AC10-DDEBUT-D-EQUI  PIC X(8).                              
             03 COMM-AC10-JDEBUT-D-EQUI  PIC X(8).                              
             03 COMM-AC10-DFIN-D-EQUI    PIC X(8).                              
             03 COMM-AC10-JFIN-D-EQUI    PIC X(8).                              
             03 COMM-AC10-EVOLUTION      PIC X.                                 
                88 COMM-AC10-EVOL-JOUR   VALUE 'J'.                             
                88 COMM-AC10-EVOL-DATE   VALUE 'D'.                             
                88 COMM-AC10-EVOL-SANS   VALUE 'S'.                             
             03 COMM-AC10-FOURCHETTE     PIC X.                                 
                88 COMM-AC10-FOUR-SEM    VALUE 'S'.                             
                88 COMM-AC10-FOUR-MOIS   VALUE 'M'.                             
                88 COMM-AC10-FOUR-SEMMOI VALUE 'X'.                             
                88 COMM-AC10-FOUR-AUTRE  VALUE 'A'.                             
             03 COMM-AC10-SURFACE        PIC X.                                 
                88 COMM-AC10-SURF-EGALE  VALUE 'E'.                             
                88 COMM-AC10-SURF-TOTALE VALUE 'T'.                             
             03 COMM-AC10-TRI            PIC X.                                 
                88 COMM-AC10-TRI-SOC-A      VALUE '1'.                          
                88 COMM-AC10-TRI-SOC-D      VALUE '2'.                          
                88 COMM-AC10-TRI-CATLM-A    VALUE '3'.                          
                88 COMM-AC10-TRI-CATLM-D    VALUE '4'.                          
                88 COMM-AC10-TRI-CAELA-A    VALUE '5'.                          
                88 COMM-AC10-TRI-CAELA-D    VALUE '6'.                          
                88 COMM-AC10-TRI-CADAC-A    VALUE '7'.                          
                88 COMM-AC10-TRI-CADAC-D    VALUE '8'.                          
                88 COMM-AC10-TRI-CATOT-A    VALUE '9'.                          
                88 COMM-AC10-TRI-CATOT-D    VALUE 'A'.                          
                88 COMM-AC10-TRI-QTLM-A     VALUE 'B'.                          
                88 COMM-AC10-TRI-QTLM-D     VALUE 'C'.                          
                88 COMM-AC10-TRI-QTRDARTY-A VALUE 'D'.                          
                88 COMM-AC10-TRI-QTRDARTY-D VALUE 'E'.                          
                88 COMM-AC10-TRI-QENTRE-A   VALUE 'F'.                          
                88 COMM-AC10-TRI-QENTRE-D   VALUE 'G'.                          
                88 COMM-AC10-TRI-TXCCRT-A   VALUE 'H'.                          
                88 COMM-AC10-TRI-TXCCRT-D   VALUE 'I'.                          
                88 COMM-AC10-TRI-PANMOY-A   VALUE 'J'.                          
                88 COMM-AC10-TRI-PANMOY-D   VALUE 'K'.                          
             03 COMM-AC10-CA             PIC X.                                 
                88 COMM-AC10-CA-EN-1        VALUE '1'.                          
                88 COMM-AC10-CA-EN-10       VALUE '2'.                          
                88 COMM-AC10-CA-EN-100      VALUE '3'.                          
                88 COMM-AC10-CA-EN-1000     VALUE '4'.                          
             03 COMM-AC10-NB             PIC X.                                 
                88 COMM-AC10-NB-EN-1        VALUE '1'.                          
                88 COMM-AC10-NB-EN-10       VALUE '2'.                          
                88 COMM-AC10-NB-EN-100      VALUE '3'.                          
                88 COMM-AC10-NB-EN-1000     VALUE '4'.                          
             03 COMM-AC10-JOUR-EQUI      PIC X.                                 
                88 COMM-AC10-J-E-REMPLI     VALUE 'R'.                          
                88 COMM-AC10-J-E-VIDE       VALUE 'V'.                          
             03 COMM-AC10-DATE-EQUI      PIC X.                                 
                88 COMM-AC10-D-E-REMPLIE    VALUE 'R'.                          
                88 COMM-AC10-D-E-VIDE       VALUE 'V'.                          
             03 COMM-AC11-NSOCIETE       PIC X(3).                              
             03 COMM-AC11-LSOCIETE       PIC X(20).                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-AC11-PAGEA          PIC S9(4) BINARY.                      
      *--                                                                       
             03 COMM-AC11-PAGEA          PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-AC11-NBP            PIC S9(4) BINARY.                      
      *--                                                                       
             03 COMM-AC11-NBP            PIC S9(4) COMP-5.                      
      *}                                                                        
             03 COMM-AC11-DDEBUT         PIC X(8).                              
             03 COMM-AC11-JDEBUT         PIC X(8).                              
             03 COMM-AC11-DFIN           PIC X(8).                              
             03 COMM-AC11-JFIN           PIC X(8).                              
             03 COMM-AC11-DDEBUT-J-EQUI  PIC X(8).                              
             03 COMM-AC11-JDEBUT-J-EQUI  PIC X(8).                              
             03 COMM-AC11-DFIN-J-EQUI    PIC X(8).                              
             03 COMM-AC11-JFIN-J-EQUI    PIC X(8).                              
             03 COMM-AC11-DDEBUT-D-EQUI  PIC X(8).                              
             03 COMM-AC11-JDEBUT-D-EQUI  PIC X(8).                              
             03 COMM-AC11-DFIN-D-EQUI    PIC X(8).                              
             03 COMM-AC11-JFIN-D-EQUI    PIC X(8).                              
             03 COMM-AC11-EVOLUTION      PIC X.                                 
                88 COMM-AC11-EVOL-JOUR   VALUE 'J'.                             
                88 COMM-AC11-EVOL-DATE   VALUE 'D'.                             
                88 COMM-AC11-EVOL-SANS   VALUE 'S'.                             
             03 COMM-AC11-FOURCHETTE     PIC X.                                 
                88 COMM-AC11-FOUR-SEM    VALUE 'S'.                             
                88 COMM-AC11-FOUR-MOIS   VALUE 'M'.                             
                88 COMM-AC11-FOUR-SEMMOI VALUE 'X'.                             
                88 COMM-AC11-FOUR-AUTRE  VALUE 'A'.                             
             03 COMM-AC11-SURFACE        PIC X.                                 
                88 COMM-AC11-SURF-EGALE  VALUE 'E'.                             
                88 COMM-AC11-SURF-TOTALE VALUE 'T'.                             
             03 COMM-AC11-TRI            PIC X.                                 
                88 COMM-AC11-TRI-NLIEU-A    VALUE '1'.                          
                88 COMM-AC11-TRI-NLIEU-D    VALUE '2'.                          
                88 COMM-AC11-TRI-CATLM-A    VALUE '3'.                          
                88 COMM-AC11-TRI-CATLM-D    VALUE '4'.                          
                88 COMM-AC11-TRI-CAELA-A    VALUE '5'.                          
                88 COMM-AC11-TRI-CAELA-D    VALUE '6'.                          
                88 COMM-AC11-TRI-CADAC-A    VALUE '7'.                          
                88 COMM-AC11-TRI-CADAC-D    VALUE '8'.                          
                88 COMM-AC11-TRI-CATOT-A    VALUE '9'.                          
                88 COMM-AC11-TRI-CATOT-D    VALUE 'A'.                          
                88 COMM-AC11-TRI-QTLM-A     VALUE 'B'.                          
                88 COMM-AC11-TRI-QTLM-D     VALUE 'C'.                          
                88 COMM-AC11-TRI-QTRDARTY-A VALUE 'D'.                          
                88 COMM-AC11-TRI-QTRDARTY-D VALUE 'E'.                          
                88 COMM-AC11-TRI-QENTRE-A   VALUE 'F'.                          
                88 COMM-AC11-TRI-QENTRE-D   VALUE 'G'.                          
                88 COMM-AC11-TRI-TXCCRT-A   VALUE 'H'.                          
                88 COMM-AC11-TRI-TXCCRT-D   VALUE 'I'.                          
                88 COMM-AC11-TRI-PANMOY-A   VALUE 'J'.                          
                88 COMM-AC11-TRI-PANMOY-D   VALUE 'K'.                          
                88 COMM-AC11-TRI-LLIEU-A    VALUE 'L'.                          
                88 COMM-AC11-TRI-LLIEU-D    VALUE 'M'.                          
             03 COMM-AC11-CA             PIC X.                                 
                88 COMM-AC11-CA-EN-1        VALUE '1'.                          
                88 COMM-AC11-CA-EN-10       VALUE '2'.                          
                88 COMM-AC11-CA-EN-100      VALUE '3'.                          
                88 COMM-AC11-CA-EN-1000     VALUE '4'.                          
             03 COMM-AC11-NB             PIC X.                                 
                88 COMM-AC11-NB-EN-1        VALUE '1'.                          
                88 COMM-AC11-NB-EN-10       VALUE '2'.                          
                88 COMM-AC11-NB-EN-100      VALUE '3'.                          
                88 COMM-AC11-NB-EN-1000     VALUE '4'.                          
             03 COMM-AC11-JOUR-EQUI      PIC X.                                 
                88 COMM-AC11-J-E-REMPLI     VALUE 'R'.                          
                88 COMM-AC11-J-E-VIDE       VALUE 'V'.                          
             03 COMM-AC11-DATE-EQUI      PIC X.                                 
                88 COMM-AC11-D-E-REMPLIE    VALUE 'R'.                          
                88 COMM-AC11-D-E-VIDE       VALUE 'V'.                          
             03 COMM-AC12-NSOCIETE       PIC X(3).                              
             03 COMM-AC12-LSOCIETE       PIC X(20).                             
             03 COMM-AC12-NLIEU          PIC X(3).                              
             03 COMM-AC12-LLIEU          PIC X(20).                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-AC12-PAGEA          PIC S9(4) BINARY.                      
      *--                                                                       
             03 COMM-AC12-PAGEA          PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-AC12-NBP            PIC S9(4) BINARY.                      
      *--                                                                       
             03 COMM-AC12-NBP            PIC S9(4) COMP-5.                      
      *}                                                                        
             03 COMM-AC12-DDEBUT         PIC X(8).                              
             03 COMM-AC12-JDEBUT         PIC X(8).                              
             03 COMM-AC12-DFIN           PIC X(8).                              
             03 COMM-AC12-JFIN           PIC X(8).                              
             03 COMM-AC12-DDEBUT-J-EQUI  PIC X(8).                              
             03 COMM-AC12-JDEBUT-J-EQUI  PIC X(8).                              
             03 COMM-AC12-DFIN-J-EQUI    PIC X(8).                              
             03 COMM-AC12-JFIN-J-EQUI    PIC X(8).                              
             03 COMM-AC12-DDEBUT-D-EQUI  PIC X(8).                              
             03 COMM-AC12-JDEBUT-D-EQUI  PIC X(8).                              
             03 COMM-AC12-DFIN-D-EQUI    PIC X(8).                              
             03 COMM-AC12-JFIN-D-EQUI    PIC X(8).                              
             03 COMM-AC12-CA             PIC X.                                 
                88 COMM-AC12-CA-EN-1        VALUE '1'.                          
                88 COMM-AC12-CA-EN-10       VALUE '2'.                          
                88 COMM-AC12-CA-EN-100      VALUE '3'.                          
                88 COMM-AC12-CA-EN-1000     VALUE '4'.                          
             03 COMM-AC12-CAC            PIC X.                                 
                88 COMM-AC12-CAC-EN-1       VALUE '1'.                          
                88 COMM-AC12-CAC-EN-10      VALUE '2'.                          
                88 COMM-AC12-CAC-EN-100     VALUE '3'.                          
                88 COMM-AC12-CAC-EN-1000    VALUE '4'.                          
             03 COMM-AC12-NB             PIC X.                                 
                88 COMM-AC12-NB-EN-1        VALUE '1'.                          
                88 COMM-AC12-NB-EN-10       VALUE '2'.                          
                88 COMM-AC12-NB-EN-100      VALUE '3'.                          
                88 COMM-AC12-NB-EN-1000     VALUE '4'.                          
             03 COMM-AC12-NBC            PIC X.                                 
                88 COMM-AC12-NBC-EN-1       VALUE '1'.                          
                88 COMM-AC12-NBC-EN-10      VALUE '2'.                          
                88 COMM-AC12-NBC-EN-100     VALUE '3'.                          
                88 COMM-AC12-NBC-EN-1000    VALUE '4'.                          
             03 COMM-AC12-EVOLUTION      PIC X.                                 
                88 COMM-AC12-EVOL-JOUR   VALUE 'J'.                             
                88 COMM-AC12-EVOL-DATE   VALUE 'D'.                             
                88 COMM-AC12-EVOL-SANS   VALUE 'S'.                             
             03 COMM-AC13-NSOCIETE       PIC X(3).                              
             03 COMM-AC13-LSOCIETE       PIC X(20).                             
             03 COMM-AC13-NLIEU          PIC X(3).                              
             03 COMM-AC13-LLIEU          PIC X(20).                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-AC13-PAGEA          PIC S9(4) BINARY.                      
      *--                                                                       
             03 COMM-AC13-PAGEA          PIC S9(4) COMP-5.                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-AC13-NBP            PIC S9(4) BINARY.                      
      *--                                                                       
             03 COMM-AC13-NBP            PIC S9(4) COMP-5.                      
      *}                                                                        
             03 COMM-AC13-DDEBUT         PIC X(8).                              
             03 COMM-AC13-JDEBUT         PIC X(8).                              
             03 COMM-AC13-DFIN           PIC X(8).                              
             03 COMM-AC13-JFIN           PIC X(8).                              
             03 COMM-AC13-DDEBUT-SEM     PIC X(8).                              
             03 COMM-AC13-JDEBUT-SEM     PIC X(8).                              
             03 COMM-AC13-DFIN-SEM       PIC X(8).                              
             03 COMM-AC13-JFIN-SEM       PIC X(8).                              
             03 COMM-AC13-DDEBUT-J-EQUI  PIC X(8).                              
             03 COMM-AC13-JDEBUT-J-EQUI  PIC X(8).                              
             03 COMM-AC13-DFIN-J-EQUI    PIC X(8).                              
             03 COMM-AC13-JFIN-J-EQUI    PIC X(8).                              
             03 COMM-AC13-DDEBUT-D-EQUI  PIC X(8).                              
             03 COMM-AC13-JDEBUT-D-EQUI  PIC X(8).                              
             03 COMM-AC13-DFIN-D-EQUI    PIC X(8).                              
             03 COMM-AC13-JFIN-D-EQUI    PIC X(8).                              
             03 COMM-AC13-DDEBUT-SEM-EQ  PIC X(8).                              
             03 COMM-AC13-JDEBUT-SEM-EQ  PIC X(8).                              
             03 COMM-AC13-DFIN-SEM-EQ    PIC X(8).                              
             03 COMM-AC13-JFIN-SEM-EQ    PIC X(8).                              
             03 COMM-AC13-EVOLUTION      PIC X.                                 
                88 COMM-AC13-EVOL-JOUR   VALUE 'J'.                             
                88 COMM-AC13-EVOL-DATE   VALUE 'D'.                             
                88 COMM-AC13-EVOL-SANS   VALUE 'S'.                             
             03 COMM-AC13-TYPE-AFFICHAGE PIC X.                                 
                88 COMM-AC13-EVOL-CA     VALUE '0'.                             
                88 COMM-AC13-EVOL-QENTRE VALUE '1'.                             
                                                                                
