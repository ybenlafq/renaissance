      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TDLBA TABLE LIBELLE DES ACTION         *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTDLBA.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTDLBA.                                                             
      *}                                                                        
           05  TDLBA-CTABLEG2    PIC X(15).                                     
           05  TDLBA-CTABLEG2-REDEF REDEFINES TDLBA-CTABLEG2.                   
               10  TDLBA-CODACT          PIC X(05).                             
           05  TDLBA-WTABLEG     PIC X(80).                                     
           05  TDLBA-WTABLEG-REDEF  REDEFINES TDLBA-WTABLEG.                    
               10  TDLBA-LIBACT          PIC X(60).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTDLBA-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTDLBA-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDLBA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TDLBA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDLBA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TDLBA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
