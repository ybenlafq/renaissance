      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IAV005 AU 01/02/1999  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,07,BI,A,                          *        
      *                           19,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IAV005.                                                        
            05 NOMETAT-IAV005           PIC X(6) VALUE 'IAV005'.                
            05 RUPTURES-IAV005.                                                 
           10 IAV005-WTYPE              PIC X(05).                      007  005
           10 IAV005-NAVOIR             PIC X(07).                      012  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IAV005-SEQUENCE           PIC S9(04) COMP.                019  002
      *--                                                                       
           10 IAV005-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IAV005.                                                   
           10 IAV005-NSOCTRAIT          PIC X(03).                      021  003
           10 IAV005-WANNUL             PIC X(03).                      024  003
           10 IAV005-WPERIM             PIC X(03).                      027  003
           10 IAV005-WUTIL              PIC X(03).                      030  003
           10 IAV005-DEPUR              PIC 9(03)     .                 033  003
           10 IAV005-DATETRAIT          PIC X(08).                      036  008
            05 FILLER                      PIC X(469).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IAV005-LONG           PIC S9(4)   COMP  VALUE +043.           
      *                                                                         
      *--                                                                       
        01  DSECT-IAV005-LONG           PIC S9(4) COMP-5  VALUE +043.           
                                                                                
      *}                                                                        
