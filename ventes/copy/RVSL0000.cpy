      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVSL0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSL0000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVSL0000.                                                            
           02  SL00-DOMAINE                                                     
               PIC X(0005).                                                     
           02  SL00-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  SL00-NLIEU                                                       
               PIC X(0003).                                                     
           02  SL00-NCOMPT                                                      
               PIC X(0002).                                                     
           02  SL00-NREFERENCE                                                  
               PIC X(0020).                                                     
           02  SL00-NSEQ                                                        
               PIC X(0003).                                                     
           02  SL00-NSOCENC                                                     
               PIC X(0003).                                                     
           02  SL00-NLIEUENC                                                    
               PIC X(0003).                                                     
           02  SL00-NCAISSE                                                     
               PIC X(0003).                                                     
           02  SL00-NTRAN                                                       
               PIC X(0008).                                                     
           02  SL00-NLIGNE                                                      
               PIC X(0003).                                                     
           02  SL00-DSAISIE                                                     
               PIC X(0008).                                                     
           02  SL00-PMONTANT                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  SL00-DRAPPRO                                                     
               PIC X(0008).                                                     
           02  SL00-DPRERAPP                                                    
               PIC X(0008).                                                     
           02  SL00-CACID                                                       
               PIC X(0008).                                                     
           02  SL00-DMAJ                                                        
               PIC X(0008).                                                     
           02  SL00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVSL0000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVSL0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL00-DOMAINE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL00-DOMAINE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL00-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL00-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL00-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL00-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL00-NCOMPT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL00-NCOMPT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL00-NREFERENCE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL00-NREFERENCE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL00-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL00-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL00-NSOCENC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL00-NSOCENC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL00-NLIEUENC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL00-NLIEUENC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL00-NCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL00-NCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL00-NTRAN-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL00-NTRAN-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL00-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL00-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL00-DSAISIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL00-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL00-PMONTANT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL00-PMONTANT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL00-DRAPPRO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL00-DRAPPRO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL00-DPRERAPP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL00-DPRERAPP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL00-CACID-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL00-CACID-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL00-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL00-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
