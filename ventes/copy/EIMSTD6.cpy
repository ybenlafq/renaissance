      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: EMISSION IMAGE STANDARD                         * 00030000
      * NOM FICHIER.: EIMSTD6                                         * 00040000
      *---------------------------------------------------------------* 00050000
      * CR   .......: 27/08/2013                                      * 00060000
      * MODIFIE.....:   /  /                                          * 00070000
      * VERSION N�..: 001                                             * 00080000
      * LONGUEUR....: 092                                             * 00090000
      ***************************************************************** 00100000
      *                                                                 00110000
       01  EIMSTD6.                                                     00120000
      * CODE IDENTIFICATEUR                                             00130000
           05      EIMSTD6-TYP-ENREG      PIC  X(0006).                 00140000
      * CODE SOCI�T�                                                    00150000
           05      EIMSTD6-CSOCIETE       PIC  X(0005).                 00160000
      * R�SERV�                                                         00160100
           05      EIMSTD6-RESERVE        PIC  X(0005).                 00160200
      * CODE ARTICLE                                                    00160300
           05      EIMSTD6-CARTICLE       PIC  X(0018).                 00160400
      * NUM�RO DE LOT                                                   00160500
           05      EIMSTD6-NLOT           PIC  X(0015).                 00160600
      * �TAT DE BLOCAGE                                                 00160700
           05      EIMSTD6-ETAT-BLOCAGE   PIC  9(0003).                 00160800
      * QUANTIT�                                                        00160900
           05      EIMSTD6-QTE            PIC  X(0009).                 00161000
      * FILLER 30                                                       00162000
           05      EIMSTD6-FILLER         PIC  X(0030).                 00163000
      * FIN                                                             00164000
           05      EIMSTD6-FIN            PIC  X(0001).                 00165000
                                                                                
