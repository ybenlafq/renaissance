      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVVT0500                                     00020001
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVT0500                 00060001
      *---------------------------------------------------------        00070000
      *                                                                 00080000
       01  RVVT0500.                                                    00090001
           02  VT05-NFLAG                                               00100001
               PIC X(0001).                                             00110000
           02  VT05-NCLUSTER                                            00120001
               PIC X(0013).                                             00130000
           02  VT05-NSOCIETE                                            00140001
               PIC X(0003).                                             00150000
           02  VT05-NDOSSIER                                            00160001
               PIC S9(3) COMP-3.                                        00170003
           02  VT05-CDOSSIER                                            00180001
               PIC X(0005).                                             00190001
           02  VT05-CTYPENT                                             00200001
               PIC X(0002).                                             00210001
           02  VT05-NENTITE                                             00220001
               PIC X(0007).                                             00230001
           02  VT05-NSEQENS                                             00240001
               PIC S9(3) COMP-3.                                        00250001
           02  VT05-CAGENT                                              00260001
               PIC X(0002).                                             00270001
           02  VT05-DCREATION                                           00280001
               PIC X(0008).                                             00290001
           02  VT05-DMODIF                                              00300001
               PIC X(0008).                                             00310001
           02  VT05-DANNUL                                              00320001
               PIC X(0008).                                             00330001
      *                                                                 00340000
      *---------------------------------------------------------        00350000
      *   LISTE DES FLAGS DE LA TABLE RVVT0500                          00360001
      *---------------------------------------------------------        00370000
      *                                                                 00380000
       01  RVVT0500-FLAGS.                                              00390001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT05-NFLAG-F                                             00400001
      *        PIC S9(4) COMP.                                          00410000
      *--                                                                       
           02  VT05-NFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT05-NCLUSTER-F                                          00420001
      *        PIC S9(4) COMP.                                          00430000
      *--                                                                       
           02  VT05-NCLUSTER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT05-NSOCIETE-F                                          00440001
      *        PIC S9(4) COMP.                                          00450000
      *--                                                                       
           02  VT05-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT05-NDOSSIER-F                                          00460001
      *        PIC S9(4) COMP.                                          00470000
      *--                                                                       
           02  VT05-NDOSSIER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT05-CDOSSIER-F                                          00480001
      *        PIC S9(4) COMP.                                          00490000
      *--                                                                       
           02  VT05-CDOSSIER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT05-CTYPENT-F                                           00500001
      *        PIC S9(4) COMP.                                          00510000
      *--                                                                       
           02  VT05-CTYPENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT05-NENTITE-F                                           00520001
      *        PIC S9(4) COMP.                                          00530000
      *--                                                                       
           02  VT05-NENTITE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT05-NSEQENS-F                                           00540001
      *        PIC S9(4) COMP.                                          00550000
      *--                                                                       
           02  VT05-NSEQENS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT05-CAGENT-F                                            00560001
      *        PIC S9(4) COMP.                                          00570000
      *--                                                                       
           02  VT05-CAGENT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT05-DCREATION-F                                         00580001
      *        PIC S9(4) COMP.                                          00590000
      *--                                                                       
           02  VT05-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT05-DMODIF-F                                            00600001
      *        PIC S9(4) COMP.                                          00610000
      *--                                                                       
           02  VT05-DMODIF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT05-DANNUL-F                                            00620001
      *        PIC S9(4) COMP.                                          00630000
      *--                                                                       
           02  VT05-DANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00640000
                                                                                
