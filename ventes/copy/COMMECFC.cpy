      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
           02  W-FACTURE-CLIENT-CRET       PIC  X(02).                          
           02  W-FACTURE-CLIENT.                                                
               03  W-FC-VENTE-GV.                                               
                   04  W-FC-E-NSOC         PIC  X(03).                          
                   04  W-FC-E-NLIEU        PIC  X(03).                          
                   04  W-FC-E-NVENTE       PIC  X(07).                          
                   04  W-FC-E-C-RET        PIC  X(02).                          
               03  W-FC-ENTETE.                                                 
                   04  W-FC-E-DVENTE       PIC  X(08).                          
                   04  W-FC-E-HVENTE       PIC  X(04).                          
                   04  W-FC-E-PTTVENTE     PIC S9(7)V99 PACKED-DECIMAL.         
                   04  W-FC-E-PCOMPT       PIC S9(7)V99 PACKED-DECIMAL.         
                   04  W-FC-E-PLIVR        PIC S9(7)V99 PACKED-DECIMAL.         
                   04  W-FC-E-PDIFFERE     PIC S9(7)V99 PACKED-DECIMAL.         
                   04  W-FC-E-PRFACT       PIC S9(7)V99 PACKED-DECIMAL.         
                   04  W-FC-E-LCOMVTE1     PIC  X(30).                          
                   04  W-FC-E-LCOMVTE2     PIC  X(30).                          
                   04  W-FC-E-LCOMVTE3     PIC  X(30).                          
                   04  W-FC-E-LCOMVTE4     PIC  X(30).                          
                   04  W-FC-E-DMODIFVTE    PIC X(008).                          
                   04  W-FC-E-WFACTURE     PIC X(001).                          
                   04  W-FC-E-WEXPORT      PIC X(001).                          
                   04  W-FC-E-WDETAXEC     PIC X(001).                          
                   04  W-FC-E-WDETAXEHC    PIC X(001).                          
                   04  W-FC-E-LDESCRIPTIF1 PIC X(030).                          
                   04  W-FC-E-LDESCRIPTIF2 PIC X(030).                          
                   04  W-FC-E-TYPVTE       PIC X(001).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        03  W-FC-C-I                PIC S9(4) BINARY.                    
      *--                                                                       
               03  W-FC-C-I                PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        03  W-FC-C-U                PIC S9(4) BINARY.                    
      *--                                                                       
               03  W-FC-C-U                PIC S9(4) COMP-5.                    
      *}                                                                        
               03  W-FC-CLIENT    OCCURS 2.                                     
                   04  W-FC-C-TYPEADR      PIC X(001).                          
                   04  W-FC-C-TITRE        PIC X(005).                          
                   04  W-FC-C-NOM          PIC X(025).                          
                   04  W-FC-C-PRENOM       PIC X(015).                          
                   04  W-FC-C-BAT          PIC X(003).                          
                   04  W-FC-C-ESC          PIC X(003).                          
                   04  W-FC-C-PORTE        PIC X(003).                          
                   04  W-FC-C-COMPL1       PIC X(032).                          
                   04  W-FC-C-COMPL2       PIC X(032).                          
                   04  W-FC-C-CVOIE        PIC X(005).                          
                   04  W-FC-C-TYPEVOIE     PIC X(004).                          
                   04  W-FC-C-NOMVOIE      PIC X(021).                          
                   04  W-FC-C-COMMUNE      PIC X(032).                          
                   04  W-FC-C-CPOSTAL      PIC X(005).                          
                   04  W-FC-C-TELDOM       PIC X(010).                          
                   04  W-FC-C-TELBUR       PIC X(010).                          
                   04  W-FC-C-TELMOB       PIC X(010).                          
                   04  W-FC-C-COMLIV1      PIC X(078).                          
                   04  W-FC-C-COMLIV2      PIC X(078).                          
                   04  W-FC-C-WETRANGER    PIC X(001).                          
                   04  W-FC-C-CPAYS        PIC X(003).                          
                   04  W-FC-C-NCLIENT      PIC X(008).                          
                   04  W-FC-C-EMAIL        PIC X(256).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        03  W-FC-R-I                PIC S9(4) BINARY.                    
      *--                                                                       
               03  W-FC-R-I                PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        03  W-FC-R-U                PIC S9(4) BINARY.                    
      *--                                                                       
               03  W-FC-R-U                PIC S9(4) COMP-5.                    
      *}                                                                        
               03  W-FC-REGLEMENT       OCCURS 10.                              
                   04  W-FC-R-L-PMENT-SI   PIC X(002).                          
                   04  W-FC-R-MD-PMENT     PIC X(005).                          
                   04  W-FC-R-ID-PMENT     PIC X(015).                          
                   04  W-FC-R-NUM-CAISSE   PIC X(005).                          
                   04  W-FC-R-NUM-ESSEMT   PIC X(007).                          
                   04  W-FC-R-DT-ESSEMT    PIC X(008).                          
                   04  W-FC-R-MT           PIC S9(7)V99 PACKED-DECIMAL.         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        03  W-FC-V-I                PIC S9(4) BINARY.                    
      *--                                                                       
               03  W-FC-V-I                PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        03  W-FC-V-U                PIC S9(4) BINARY.                    
      *--                                                                       
               03  W-FC-V-U                PIC S9(4) COMP-5.                    
      *}                                                                        
               03  W-FC-VENTE           OCCURS 81.                              
                   04 W-FC-V-CTYPENREG    PIC X(001).                           
                   04 W-FC-V-NCODICGRP    PIC X(007).                           
                   04 W-FC-V-NCODIC       PIC X(007).                           
                   04 W-FC-V-LREFFOURN    PIC X(020).                           
                   04 W-FC-V-CFAM         PIC X(005).                           
                   04 W-FC-V-LFAM         PIC X(020).                           
                   04 W-FC-V-CMARQ        PIC X(005).                           
                   04 W-FC-V-LMARQ        PIC X(020).                           
                   04 W-FC-V-CMODDEL      PIC X(003).                           
                   04 W-FC-V-DDELIV       PIC X(008).                           
                   04 W-FC-V-CENREG       PIC X(05).                            
                   04 W-FC-V-QVENDUE      PIC S9(5).                            
                   04 W-FC-V-PVUNIT       PIC S9(7)V99 PACKED-DECIMAL.          
                   04 W-FC-V-PVTOTAL      PIC S9(7)V99 PACKED-DECIMAL.          
                   04 W-FC-V-TAUXTVA      PIC S9(5)V99 PACKED-DECIMAL.          
                   04 W-FC-V-WEMPORTE     PIC X(001).                           
                   04 W-FC-V-CPLAGE       PIC X(002).                           
                   04 W-FC-V-WTOPELIVRE   PIC X(001).                           
                   04 W-FC-V-DCREATION    PIC X(008).                           
                   04 W-FC-V-HCREATION    PIC X(004).                           
                   04 W-FC-V-DATENC       PIC X(008).                           
                   04 W-FC-V-DANNULATION  PIC X(008).                           
                   04 W-FC-V-NSOCP        PIC X(003).                           
                   04 W-FC-V-NLIEUP       PIC X(003).                           
                   04 W-FC-V-NTRANS       PIC S9(8)V   PACKED-DECIMAL.          
                   04 W-FC-V-NSEQNQ       PIC S9(5) PACKED-DECIMAL.             
                   04 W-FC-V-NSEQREF      PIC S9(5) PACKED-DECIMAL.             
                   04 W-FC-V-DTOPE        PIC X(008).                           
                   04 W-FC-V-PECO         PIC X(008).                           
                   04 W-FC-V-ECOMOB       PIC X(008).                           
                   04 W-FC-V-NPSE         PIC X(008).                           
                                                                                
