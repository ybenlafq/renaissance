      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IBA020 AU 11/01/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,06,BI,A,                          *        
      *                           13,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IBA020.                                                        
            05 NOMETAT-IBA020           PIC X(6) VALUE 'IBA020'.                
            05 RUPTURES-IBA020.                                                 
           10 IBA020-NBA                PIC X(06).                      007  006
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IBA020-SEQUENCE           PIC S9(04) COMP.                013  002
      *--                                                                       
           10 IBA020-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IBA020.                                                   
           10 IBA020-CDEVISE            PIC X(03).                      015  003
           10 IBA020-CTYPMVT            PIC X(10).                      018  010
           10 IBA020-DATEGCT            PIC X(10).                      028  010
           10 IBA020-DEMIS              PIC X(11).                      038  011
           10 IBA020-NATURE             PIC X(03).                      049  003
           10 IBA020-NDELAI             PIC X(03).                      052  003
           10 IBA020-NEXERDEB           PIC X(06).                      055  006
           10 IBA020-NEXERFIN           PIC X(06).                      061  006
           10 IBA020-NFACTBA            PIC X(10).                      067  010
           10 IBA020-NFOUR              PIC X(08).                      077  008
           10 IBA020-NLIEU              PIC X(03).                      085  003
           10 IBA020-NPIECE             PIC X(06).                      088  006
           10 IBA020-NSOC               PIC X(03).                      094  003
           10 IBA020-NSOCIETE           PIC X(03).                      097  003
           10 IBA020-NTIERSCV           PIC X(06).                      100  006
           10 IBA020-WANNUL             PIC X(01).                      106  001
           10 IBA020-WRECEP             PIC X(01).                      107  001
           10 IBA020-WSENS              PIC X(01).                      108  001
           10 IBA020-MONTANT            PIC S9(06)V9(2) COMP-3.         109  005
           10 IBA020-MONTANT-AUT-C      PIC S9(09)V9(2) COMP-3.         114  006
           10 IBA020-MONTANT-AUT-D      PIC S9(09)V9(2) COMP-3.         120  006
           10 IBA020-MONTANT-BA0-C      PIC S9(09)V9(2) COMP-3.         126  006
           10 IBA020-MONTANT-BA0-D      PIC S9(09)V9(2) COMP-3.         132  006
           10 IBA020-MONTANT-BA2-C      PIC S9(09)V9(2) COMP-3.         138  006
           10 IBA020-MONTANT-BA2-D      PIC S9(09)V9(2) COMP-3.         144  006
           10 IBA020-MONTANT-TOT-C      PIC S9(09)V9(2) COMP-3.         150  006
           10 IBA020-MONTANT-TOT-D      PIC S9(09)V9(2) COMP-3.         156  006
           10 IBA020-PBA                PIC S9(06)V9(2) COMP-3.         162  005
           10 IBA020-PBA-BA0-C          PIC S9(09)V9(2) COMP-3.         167  006
           10 IBA020-PBA-BA0-D          PIC S9(09)V9(2) COMP-3.         173  006
           10 IBA020-PBA-BA2-C          PIC S9(09)V9(2) COMP-3.         179  006
           10 IBA020-PBA-BA2-D          PIC S9(09)V9(2) COMP-3.         185  006
           10 IBA020-PBA-C              PIC S9(09)V9(2) COMP-3.         191  006
           10 IBA020-PBA-D              PIC S9(09)V9(2) COMP-3.         197  006
           10 IBA020-PBA-TOT-C          PIC S9(09)V9(2) COMP-3.         203  006
           10 IBA020-PBA-TOT-D          PIC S9(09)V9(2) COMP-3.         209  006
           10 IBA020-DANNUL             PIC X(08).                      215  008
           10 IBA020-DDEBUT             PIC X(08).                      223  008
           10 IBA020-DFIN               PIC X(08).                      231  008
           10 IBA020-DICEMIS            PIC X(08).                      239  008
           10 IBA020-DICRECEP           PIC X(08).                      247  008
           10 IBA020-DRECEP             PIC X(08).                      255  008
            05 FILLER                      PIC X(250).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IBA020-LONG           PIC S9(4)   COMP  VALUE +262.           
      *                                                                         
      *--                                                                       
        01  DSECT-IBA020-LONG           PIC S9(4) COMP-5  VALUE +262.           
                                                                                
      *}                                                                        
