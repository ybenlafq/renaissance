      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IBA001 AU 29/04/2008  *        
      *                                                                *        
      *          CRITERES DE TRI  07,08,BI,A,                          *        
      *                           15,10,BI,A,                          *        
      *                           25,06,BI,A,                          *        
      *                           31,06,BI,A,                          *        
      *                           37,10,BI,A,                          *        
      *                           47,04,PD,A,                          *        
      *                           51,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IBA001.                                                        
            05 NOMETAT-IBA001           PIC X(6) VALUE 'IBA001'.                
            05 RUPTURES-IBA001.                                                 
           10 IBA001-JOUR               PIC X(08).                      007  008
           10 IBA001-NFACTBA            PIC X(10).                      015  010
           10 IBA001-NBADEB             PIC X(06).                      025  006
           10 IBA001-CTIERSBA           PIC X(06).                      031  006
           10 IBA001-NTIERSSAP          PIC X(10).                      037  010
           10 IBA001-PU                 PIC S9(05)V9(2) COMP-3.         047  004
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IBA001-SEQUENCE           PIC S9(04) COMP.                051  002
      *--                                                                       
           10 IBA001-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IBA001.                                                   
           10 IBA001-FACTURE-AVOIR      PIC X(01).                      053  001
           10 IBA001-LSIGNAT            PIC X(15).                      054  015
           10 IBA001-NBAFIN             PIC X(06).                      069  006
           10 IBA001-NFACTREM           PIC X(10).                      075  010
           10 IBA001-CA-REEL            PIC S9(07)V9(2) COMP-3.         085  005
           10 IBA001-QTE                PIC S9(05)      COMP-3.         090  003
           10 IBA001-REMISE             PIC S9(03)V9(2) COMP-3.         093  003
            05 FILLER                      PIC X(417).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IBA001-LONG           PIC S9(4)   COMP  VALUE +095.           
      *                                                                         
      *--                                                                       
        01  DSECT-IBA001-LONG           PIC S9(4) COMP-5  VALUE +095.           
                                                                                
      *}                                                                        
