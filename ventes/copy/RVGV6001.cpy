      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGV6000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV6000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV6000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV6000.                                                            
      *}                                                                        
           02  GV60-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GV60-NLIEU                                                       
               PIC X(0003).                                                     
           02  GV60-NVENTE                                                      
               PIC X(0007).                                                     
           02  GV60-NCODICGRP                                                   
               PIC X(0007).                                                     
           02  GV60-NCODIC                                                      
               PIC X(0007).                                                     
           02  GV60-NSEQ                                                        
               PIC X(0001).                                                     
           02  GV60-DCREATION                                                   
               PIC X(0008).                                                     
           02  GV60-DTRANSFERT                                                  
               PIC X(0008).                                                     
           02  GV60-CMODDEL                                                     
               PIC X(0003).                                                     
           02  GV60-CFAM                                                        
               PIC X(0005).                                                     
           02  GV60-CMARQ                                                       
               PIC X(0005).                                                     
           02  GV60-LREF                                                        
               PIC X(0020).                                                     
           02  GV60-CFAMGRP                                                     
               PIC X(0005).                                                     
           02  GV60-CMARQGRP                                                    
               PIC X(0005).                                                     
           02  GV60-LREFGRP                                                     
               PIC X(0020).                                                     
           02  GV60-QVENDUE                                                     
               PIC S9(5) COMP-3.                                                
           02  GV60-PSE                                                         
               PIC X(0003).                                                     
           02  GV60-CTITRE                                                      
               PIC X(0005).                                                     
           02  GV60-LNOM                                                        
               PIC X(0025).                                                     
           02  GV60-LPRENOM                                                     
               PIC X(0015).                                                     
           02  GV60-LBATIMENT                                                   
               PIC X(0003).                                                     
           02  GV60-LESCALIER                                                   
               PIC X(0003).                                                     
           02  GV60-LETAGE                                                      
               PIC X(0003).                                                     
           02  GV60-LPORTE                                                      
               PIC X(0003).                                                     
           02  GV60-LCMPAD1                                                     
               PIC X(0032).                                                     
           02  GV60-LCMPAD2                                                     
               PIC X(0032).                                                     
           02  GV60-CVOIE                                                       
               PIC X(0005).                                                     
           02  GV60-CTVOIE                                                      
               PIC X(0004).                                                     
           02  GV60-LNOMVOIE                                                    
               PIC X(0021).                                                     
           02  GV60-LCOMMUNE                                                    
               PIC X(0032).                                                     
           02  GV60-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  GV60-LBUREAU                                                     
               PIC X(0026).                                                     
           02  GV60-TELDOM                                                      
               PIC X(0010).                                                     
           02  GV60-TELBUR                                                      
               PIC X(0010).                                                     
           02  GV60-LPOSTEBUR                                                   
               PIC X(0005).                                                     
           02  GV60-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GV60-CENREG                                                      
               PIC X(0005).                                                     
           02  GV60-LCOMMENT                                                    
               PIC X(0008).                                                     
           02  GV60-DEFFET                                                      
               PIC X(0008).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGV6000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV6000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV6000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-NCODICGRP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-NCODICGRP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-DTRANSFERT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-DTRANSFERT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-CMODDEL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-CMODDEL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-LREF-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-LREF-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-CFAMGRP-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-CFAMGRP-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-CMARQGRP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-CMARQGRP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-LREFGRP-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-LREFGRP-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-QVENDUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-QVENDUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-PSE-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-PSE-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-CTITRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-CTITRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-LNOM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-LNOM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-LPRENOM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-LPRENOM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-LBATIMENT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-LBATIMENT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-LESCALIER-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-LESCALIER-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-LETAGE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-LETAGE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-LPORTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-LPORTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-LCMPAD1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-LCMPAD1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-LCMPAD2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-LCMPAD2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-CVOIE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-CVOIE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-CTVOIE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-CTVOIE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-LNOMVOIE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-LNOMVOIE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-LCOMMUNE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-LCOMMUNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-LBUREAU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-LBUREAU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-TELDOM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-TELDOM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-TELBUR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-TELBUR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-LPOSTEBUR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-LPOSTEBUR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-CENREG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-CENREG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV60-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV60-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
