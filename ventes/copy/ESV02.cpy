      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESV02   ESV02                                              00000020
      ***************************************************************** 00000030
       01   ESV02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAM1L   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAM1F   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCFAM1I   PIC X(5).                                       00000210
           02 MLIGNEI OCCURS   15 TIMES .                               00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000230
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MCFAMI  PIC X(5).                                       00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFAML  COMP PIC S9(4).                                 00000270
      *--                                                                       
             03 MLFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLFAMF  PIC X.                                          00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MLFAMI  PIC X(20).                                      00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSECGASL    COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MCSECGASL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCSECGASF    PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MCSECGASI    PIC X(5).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAFFGASL    COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MCAFFGASL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCAFFGASF    PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MCAFFGASI    PIC X.                                     00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCGRPL  COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MCGRPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCGRPF  PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCGRPI  PIC X(5).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSECT1L     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MCSECT1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCSECT1F     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCSECT1I     PIC X(5).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAFFEC1L    COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MCAFFEC1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCAFFEC1F    PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCAFFEC1I    PIC X.                                     00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSECT2L     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MCSECT2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCSECT2F     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCSECT2I     PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MZONCMDI  PIC X(15).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MLIBERRI  PIC X(58).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCODTRAI  PIC X(4).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCICSI    PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MNETNAMI  PIC X(8).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MSCREENI  PIC X(4).                                       00000780
      ***************************************************************** 00000790
      * SDF: ESV02   ESV02                                              00000800
      ***************************************************************** 00000810
       01   ESV02O REDEFINES ESV02I.                                    00000820
           02 FILLER    PIC X(12).                                      00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MDATJOUA  PIC X.                                          00000850
           02 MDATJOUC  PIC X.                                          00000860
           02 MDATJOUP  PIC X.                                          00000870
           02 MDATJOUH  PIC X.                                          00000880
           02 MDATJOUV  PIC X.                                          00000890
           02 MDATJOUO  PIC X(10).                                      00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MTIMJOUA  PIC X.                                          00000920
           02 MTIMJOUC  PIC X.                                          00000930
           02 MTIMJOUP  PIC X.                                          00000940
           02 MTIMJOUH  PIC X.                                          00000950
           02 MTIMJOUV  PIC X.                                          00000960
           02 MTIMJOUO  PIC X(5).                                       00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MPAGEA    PIC X.                                          00000990
           02 MPAGEC    PIC X.                                          00001000
           02 MPAGEP    PIC X.                                          00001010
           02 MPAGEH    PIC X.                                          00001020
           02 MPAGEV    PIC X.                                          00001030
           02 MPAGEO    PIC X(3).                                       00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MCFAM1A   PIC X.                                          00001060
           02 MCFAM1C   PIC X.                                          00001070
           02 MCFAM1P   PIC X.                                          00001080
           02 MCFAM1H   PIC X.                                          00001090
           02 MCFAM1V   PIC X.                                          00001100
           02 MCFAM1O   PIC X(5).                                       00001110
           02 MLIGNEO OCCURS   15 TIMES .                               00001120
             03 FILLER       PIC X(2).                                  00001130
             03 MCFAMA  PIC X.                                          00001140
             03 MCFAMC  PIC X.                                          00001150
             03 MCFAMP  PIC X.                                          00001160
             03 MCFAMH  PIC X.                                          00001170
             03 MCFAMV  PIC X.                                          00001180
             03 MCFAMO  PIC X(5).                                       00001190
             03 FILLER       PIC X(2).                                  00001200
             03 MLFAMA  PIC X.                                          00001210
             03 MLFAMC  PIC X.                                          00001220
             03 MLFAMP  PIC X.                                          00001230
             03 MLFAMH  PIC X.                                          00001240
             03 MLFAMV  PIC X.                                          00001250
             03 MLFAMO  PIC X(20).                                      00001260
             03 FILLER       PIC X(2).                                  00001270
             03 MCSECGASA    PIC X.                                     00001280
             03 MCSECGASC    PIC X.                                     00001290
             03 MCSECGASP    PIC X.                                     00001300
             03 MCSECGASH    PIC X.                                     00001310
             03 MCSECGASV    PIC X.                                     00001320
             03 MCSECGASO    PIC X(5).                                  00001330
             03 FILLER       PIC X(2).                                  00001340
             03 MCAFFGASA    PIC X.                                     00001350
             03 MCAFFGASC    PIC X.                                     00001360
             03 MCAFFGASP    PIC X.                                     00001370
             03 MCAFFGASH    PIC X.                                     00001380
             03 MCAFFGASV    PIC X.                                     00001390
             03 MCAFFGASO    PIC X.                                     00001400
             03 FILLER       PIC X(2).                                  00001410
             03 MCGRPA  PIC X.                                          00001420
             03 MCGRPC  PIC X.                                          00001430
             03 MCGRPP  PIC X.                                          00001440
             03 MCGRPH  PIC X.                                          00001450
             03 MCGRPV  PIC X.                                          00001460
             03 MCGRPO  PIC X(5).                                       00001470
             03 FILLER       PIC X(2).                                  00001480
             03 MCSECT1A     PIC X.                                     00001490
             03 MCSECT1C     PIC X.                                     00001500
             03 MCSECT1P     PIC X.                                     00001510
             03 MCSECT1H     PIC X.                                     00001520
             03 MCSECT1V     PIC X.                                     00001530
             03 MCSECT1O     PIC X(5).                                  00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MCAFFEC1A    PIC X.                                     00001560
             03 MCAFFEC1C    PIC X.                                     00001570
             03 MCAFFEC1P    PIC X.                                     00001580
             03 MCAFFEC1H    PIC X.                                     00001590
             03 MCAFFEC1V    PIC X.                                     00001600
             03 MCAFFEC1O    PIC X.                                     00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MCSECT2A     PIC X.                                     00001630
             03 MCSECT2C     PIC X.                                     00001640
             03 MCSECT2P     PIC X.                                     00001650
             03 MCSECT2H     PIC X.                                     00001660
             03 MCSECT2V     PIC X.                                     00001670
             03 MCSECT2O     PIC X(5).                                  00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MZONCMDA  PIC X.                                          00001700
           02 MZONCMDC  PIC X.                                          00001710
           02 MZONCMDP  PIC X.                                          00001720
           02 MZONCMDH  PIC X.                                          00001730
           02 MZONCMDV  PIC X.                                          00001740
           02 MZONCMDO  PIC X(15).                                      00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MLIBERRA  PIC X.                                          00001770
           02 MLIBERRC  PIC X.                                          00001780
           02 MLIBERRP  PIC X.                                          00001790
           02 MLIBERRH  PIC X.                                          00001800
           02 MLIBERRV  PIC X.                                          00001810
           02 MLIBERRO  PIC X(58).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCODTRAA  PIC X.                                          00001840
           02 MCODTRAC  PIC X.                                          00001850
           02 MCODTRAP  PIC X.                                          00001860
           02 MCODTRAH  PIC X.                                          00001870
           02 MCODTRAV  PIC X.                                          00001880
           02 MCODTRAO  PIC X(4).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCICSA    PIC X.                                          00001910
           02 MCICSC    PIC X.                                          00001920
           02 MCICSP    PIC X.                                          00001930
           02 MCICSH    PIC X.                                          00001940
           02 MCICSV    PIC X.                                          00001950
           02 MCICSO    PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNETNAMA  PIC X.                                          00001980
           02 MNETNAMC  PIC X.                                          00001990
           02 MNETNAMP  PIC X.                                          00002000
           02 MNETNAMH  PIC X.                                          00002010
           02 MNETNAMV  PIC X.                                          00002020
           02 MNETNAMO  PIC X(8).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MSCREENA  PIC X.                                          00002050
           02 MSCREENC  PIC X.                                          00002060
           02 MSCREENP  PIC X.                                          00002070
           02 MSCREENH  PIC X.                                          00002080
           02 MSCREENV  PIC X.                                          00002090
           02 MSCREENO  PIC X(4).                                       00002100
                                                                                
