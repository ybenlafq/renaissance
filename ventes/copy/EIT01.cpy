      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EIT01   EIT01                                              00000020
      ***************************************************************** 00000030
       01   EIT01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCTL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MFONCTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFONCTF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MFONCTI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINVENTL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MINVENTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MINVENTF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MINVENTI  PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWLOCALL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MWLOCALL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWLOCALF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MWLOCALI  PIC X.                                          00000250
           02 MTYPLD OCCURS   4 TIMES .                                 00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPLL  COMP PIC S9(4).                                 00000270
      *--                                                                       
             03 MTYPLL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTYPLF  PIC X.                                          00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MTYPLI  PIC X.                                          00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCOMPTL  COMP PIC S9(4).                                 00000310
      *--                                                                       
           02 MDCOMPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDCOMPTF  PIC X.                                          00000320
           02 FILLER    PIC X(4).                                       00000330
           02 MDCOMPTI  PIC X(10).                                      00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDTHEOL   COMP PIC S9(4).                                 00000350
      *--                                                                       
           02 MDTHEOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDTHEOF   PIC X.                                          00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MDTHEOI   PIC X(10).                                      00000380
           02 MFAMINVI OCCURS   8 TIMES .                               00000390
             03 MFAMD OCCURS   8 TIMES .                                00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MFAML      COMP PIC S9(4).                            00000410
      *--                                                                       
               04 MFAML COMP-5 PIC S9(4).                                       
      *}                                                                        
               04 MFAMF      PIC X.                                     00000420
               04 FILLER     PIC X(4).                                  00000430
               04 MFAMI      PIC X(05).                                 00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MLIBERRI  PIC X(78).                                      00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000490
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000500
           02 FILLER    PIC X(4).                                       00000510
           02 MCODTRAI  PIC X(4).                                       00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MZONCMDI  PIC X(15).                                      00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MCICSI    PIC X(5).                                       00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MNETNAMI  PIC X(8).                                       00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MSCREENI  PIC X(4).                                       00000680
      ***************************************************************** 00000690
      * SDF: EIT01   EIT01                                              00000700
      ***************************************************************** 00000710
       01   EIT01O REDEFINES EIT01I.                                    00000720
           02 FILLER    PIC X(12).                                      00000730
           02 FILLER    PIC X(2).                                       00000740
           02 MDATJOUA  PIC X.                                          00000750
           02 MDATJOUC  PIC X.                                          00000760
           02 MDATJOUP  PIC X.                                          00000770
           02 MDATJOUH  PIC X.                                          00000780
           02 MDATJOUV  PIC X.                                          00000790
           02 MDATJOUO  PIC X(10).                                      00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MTIMJOUA  PIC X.                                          00000820
           02 MTIMJOUC  PIC X.                                          00000830
           02 MTIMJOUP  PIC X.                                          00000840
           02 MTIMJOUH  PIC X.                                          00000850
           02 MTIMJOUV  PIC X.                                          00000860
           02 MTIMJOUO  PIC X(5).                                       00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MFONCTA   PIC X.                                          00000890
           02 MFONCTC   PIC X.                                          00000900
           02 MFONCTP   PIC X.                                          00000910
           02 MFONCTH   PIC X.                                          00000920
           02 MFONCTV   PIC X.                                          00000930
           02 MFONCTO   PIC X(3).                                       00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MINVENTA  PIC X.                                          00000960
           02 MINVENTC  PIC X.                                          00000970
           02 MINVENTP  PIC X.                                          00000980
           02 MINVENTH  PIC X.                                          00000990
           02 MINVENTV  PIC X.                                          00001000
           02 MINVENTO  PIC X(5).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MWLOCALA  PIC X.                                          00001030
           02 MWLOCALC  PIC X.                                          00001040
           02 MWLOCALP  PIC X.                                          00001050
           02 MWLOCALH  PIC X.                                          00001060
           02 MWLOCALV  PIC X.                                          00001070
           02 MWLOCALO  PIC X.                                          00001080
           02 DFHMS1 OCCURS   4 TIMES .                                 00001090
             03 FILLER       PIC X(2).                                  00001100
             03 MTYPLA  PIC X.                                          00001110
             03 MTYPLC  PIC X.                                          00001120
             03 MTYPLP  PIC X.                                          00001130
             03 MTYPLH  PIC X.                                          00001140
             03 MTYPLV  PIC X.                                          00001150
             03 MTYPLO  PIC X.                                          00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MDCOMPTA  PIC X.                                          00001180
           02 MDCOMPTC  PIC X.                                          00001190
           02 MDCOMPTP  PIC X.                                          00001200
           02 MDCOMPTH  PIC X.                                          00001210
           02 MDCOMPTV  PIC X.                                          00001220
           02 MDCOMPTO  PIC X(10).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MDTHEOA   PIC X.                                          00001250
           02 MDTHEOC   PIC X.                                          00001260
           02 MDTHEOP   PIC X.                                          00001270
           02 MDTHEOH   PIC X.                                          00001280
           02 MDTHEOV   PIC X.                                          00001290
           02 MDTHEOO   PIC X(10).                                      00001300
           02 MFAMINVO OCCURS   8 TIMES .                               00001310
             03 DFHMS2 OCCURS   8 TIMES .                               00001320
               04 FILLER     PIC X(2).                                  00001330
               04 MFAMA      PIC X.                                     00001340
               04 MFAMC PIC X.                                          00001350
               04 MFAMP PIC X.                                          00001360
               04 MFAMH PIC X.                                          00001370
               04 MFAMV PIC X.                                          00001380
               04 MFAMO      PIC X(5).                                  00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MLIBERRA  PIC X.                                          00001410
           02 MLIBERRC  PIC X.                                          00001420
           02 MLIBERRP  PIC X.                                          00001430
           02 MLIBERRH  PIC X.                                          00001440
           02 MLIBERRV  PIC X.                                          00001450
           02 MLIBERRO  PIC X(78).                                      00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MCODTRAA  PIC X.                                          00001480
           02 MCODTRAC  PIC X.                                          00001490
           02 MCODTRAP  PIC X.                                          00001500
           02 MCODTRAH  PIC X.                                          00001510
           02 MCODTRAV  PIC X.                                          00001520
           02 MCODTRAO  PIC X(4).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MZONCMDA  PIC X.                                          00001550
           02 MZONCMDC  PIC X.                                          00001560
           02 MZONCMDP  PIC X.                                          00001570
           02 MZONCMDH  PIC X.                                          00001580
           02 MZONCMDV  PIC X.                                          00001590
           02 MZONCMDO  PIC X(15).                                      00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MCICSA    PIC X.                                          00001620
           02 MCICSC    PIC X.                                          00001630
           02 MCICSP    PIC X.                                          00001640
           02 MCICSH    PIC X.                                          00001650
           02 MCICSV    PIC X.                                          00001660
           02 MCICSO    PIC X(5).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MNETNAMA  PIC X.                                          00001690
           02 MNETNAMC  PIC X.                                          00001700
           02 MNETNAMP  PIC X.                                          00001710
           02 MNETNAMH  PIC X.                                          00001720
           02 MNETNAMV  PIC X.                                          00001730
           02 MNETNAMO  PIC X(8).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MSCREENA  PIC X.                                          00001760
           02 MSCREENC  PIC X.                                          00001770
           02 MSCREENP  PIC X.                                          00001780
           02 MSCREENH  PIC X.                                          00001790
           02 MSCREENV  PIC X.                                          00001800
           02 MSCREENO  PIC X(4).                                       00001810
                                                                                
