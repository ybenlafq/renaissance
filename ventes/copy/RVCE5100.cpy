      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************00000010
      * COBOL DECLARATION FOR TABLE DSA000.RTCE51                      *00000020
      ******************************************************************00000030
       01  RVCE5100.                                                    00000040
           10 CE51-NCODIC          PIC X(7).                            00000050
           10 CE51-CICONE          PIC X(5).                            00000060
           10 CE51-CVALEUR         PIC X(3).                            00000070
           10 CE51-DSYST           PIC S9(13)V USAGE COMP-3.            00000080
      ******************************************************************00000090
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 8       *00000100
      ******************************************************************00000110
      ******************************************************************00000120
      * INDICATOR VARIABLE STRUCTURE                                   *00000130
      ******************************************************************00000140
       01  RVCE5100-FLAGS.                                              00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE51-NCODIC-F        PIC S9(4) COMP.                      00000160
      *--                                                                       
           10 CE51-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE51-CICONE-F        PIC S9(4) COMP.                      00000170
      *--                                                                       
           10 CE51-CICONE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE51-CVALEUR-F       PIC S9(4) COMP.                      00000180
      *--                                                                       
           10 CE51-CVALEUR-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE51-DSYST-F         PIC S9(4) COMP.                      00000190
      *                                                                         
      *--                                                                       
           10 CE51-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
      *}                                                                        
