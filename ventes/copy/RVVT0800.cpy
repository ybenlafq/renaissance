      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVVT0800                                     00020001
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVT0800                 00060001
      *---------------------------------------------------------        00070000
      *                                                                 00080000
       01  RVVT0800.                                                    00090001
           02  VT08-NFLAG                                               00100001
               PIC X(0001).                                             00110000
           02  VT08-NCLUSTER                                            00120001
               PIC X(0013).                                             00130000
           02  VT08-NSOCIETE                                            00140001
               PIC X(0003).                                             00150000
           02  VT08-NDOSSIER                                            00160001
               PIC S9(3) COMP-3.                                        00170003
           02  VT08-NSEQ                                                00180002
               PIC S9(3) COMP-3.                                        00190002
           02  VT08-CCTRL                                               00200002
               PIC X(0005).                                             00210000
           02  VT08-LGCTRL                                              00220002
               PIC S9(3) COMP-3.                                        00230002
           02  VT08-VALCTRL                                             00240002
               PIC X(0050).                                             00250002
           02  VT08-CAUTOR                                              00260002
               PIC X(0005).                                             00270002
           02  VT08-CJUSTIF                                             00280002
               PIC X(0005).                                             00290002
           02  VT08-DMODIF                                              00300001
               PIC X(0008).                                             00310000
      *                                                                 00320000
      *---------------------------------------------------------        00330000
      *   LISTE DES FLAGS DE LA TABLE RVVT0800                          00340001
      *---------------------------------------------------------        00350000
      *                                                                 00360000
       01  RVVT0800-FLAGS.                                              00370001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT08-NFLAG-F                                             00380001
      *        PIC S9(4) COMP.                                          00390000
      *--                                                                       
           02  VT08-NFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT08-NCLUSTER-F                                          00400001
      *        PIC S9(4) COMP.                                          00410000
      *--                                                                       
           02  VT08-NCLUSTER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT08-NSOCIETE-F                                          00420001
      *        PIC S9(4) COMP.                                          00430000
      *--                                                                       
           02  VT08-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT08-NDOSSIER-F                                          00440001
      *        PIC S9(4) COMP.                                          00450000
      *--                                                                       
           02  VT08-NDOSSIER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT08-NSEQ-F                                              00460002
      *        PIC S9(4) COMP.                                          00470000
      *--                                                                       
           02  VT08-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT08-CCTRL-F                                             00480002
      *        PIC S9(4) COMP.                                          00490000
      *--                                                                       
           02  VT08-CCTRL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT08-LGCTRL-F                                            00500002
      *        PIC S9(4) COMP.                                          00510000
      *--                                                                       
           02  VT08-LGCTRL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT08-VALCTRL-F                                           00520002
      *        PIC S9(4) COMP.                                          00530000
      *--                                                                       
           02  VT08-VALCTRL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT08-CAUTOR-F                                            00540002
      *        PIC S9(4) COMP.                                          00550000
      *--                                                                       
           02  VT08-CAUTOR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT08-CJUSTIF-F                                           00560002
      *        PIC S9(4) COMP.                                          00570000
      *--                                                                       
           02  VT08-CJUSTIF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT08-DMODIF-F                                            00580001
      *        PIC S9(4) COMP.                                          00590000
      *--                                                                       
           02  VT08-DMODIF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00600000
                                                                                
