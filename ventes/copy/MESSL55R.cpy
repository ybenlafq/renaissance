      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ INTERROGATION STOCK LOCAL                            
      *         B-VERSION                                                       
      *****************************************************************         
      *****************************************************************         
      *  POUR TSL55 AVEC GET ET PUT EXTERNES                                    
      *                                                                         
           10  WS-MESSAGE-RECU REDEFINES COMM-MQ13-MESSAGE.                     
      *---                                                                      
           15  MES-ENTETE.                                                      
               20   MESR-TYPE      PIC    X(3).                                 
               20   MESR-NSOCMSG   PIC    X(3).                                 
               20   MESR-NLIEUMSG  PIC    X(3).                                 
               20   MESR-NSOCDST   PIC    X(3).                                 
               20   MESR-NLIEUDST  PIC    X(3).                                 
               20   MESR-NORD      PIC    9(8).                                 
               20   MESR-LPROG     PIC    X(10).                                
               20   MESR-DJOUR     PIC    X(8).                                 
               20   MESR-WSID      PIC    X(10).                                
               20   MESR-USER      PIC    X(10).                                
               20   MESR-CHRONO    PIC    9(7).                                 
               20   MESR-NBRMSG    PIC    9(7).                                 
               20   MESR-OCCURS    PIC    9(05).                                
               20   MESR-FILLER    PIC    X(25).                                
           15  MESR-QSTOCK-LOCAL OCCURS 900.                                    
               20   MESR-SL55-NSOCIETE     PIC  X(03).                          
               20   MESR-SL55-NLIEU        PIC  X(03).                          
               20   MESR-SL55-NCODIC       PIC  X(07).                          
               20   MESR-SL55-NVENTE       PIC  X(07).                          
               20   MESR-SL55-NSEQNQ       PIC  9(05).                          
               20   MESR-SL55-QVENDUE      PIC  9(05).                          
               20   MESR-SL55-QTMANQUANT   PIC  9(05).                          
               20   MESR-SL55-QTRECUE      PIC  9(05).                          
                                                                                
