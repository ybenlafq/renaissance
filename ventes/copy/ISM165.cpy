      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT ISM165 AU 25/01/2005  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,03,PD,A,                          *        
      *                           20,05,BI,A,                          *        
      *                           25,20,BI,A,                          *        
      *                           45,20,BI,A,                          *        
      *                           65,20,BI,A,                          *        
      *                           85,20,BI,A,                          *        
      *                           05,02,PD,A,                          *        
      *                           07,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-ISM165.                                                        
            05 NOMETAT-ISM165           PIC X(6) VALUE 'ISM165'.                
            05 RUPTURES-ISM165.                                                 
           10 ISM165-NSOCIETE           PIC X(03).                      007  003
           10 ISM165-GRP                PIC X(02).                      010  002
           10 ISM165-CHEFPROD           PIC X(05).                      012  005
           10 ISM165-WSEQFAM            PIC S9(05)      COMP-3.         017  003
           10 ISM165-CFAM               PIC X(05).                      020  005
           10 ISM165-LFAM               PIC X(20).                      025  020
           10 ISM165-LVMARKET1          PIC X(20).                      045  020
           10 ISM165-LVMARKET2          PIC X(20).                      065  020
           10 ISM165-LVMARKET3          PIC X(20).                      085  020
           10 ISM165-TRIEXPO            PIC S9(03)      COMP-3.         105  002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ISM165-SEQUENCE           PIC S9(04) COMP.                107  002
      *--                                                                       
           10 ISM165-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-ISM165.                                                   
           10 ISM165-GAMME1             PIC X(05).                      109  005
           10 ISM165-GAMME10            PIC X(05).                      114  005
           10 ISM165-GAMME11            PIC X(05).                      119  005
           10 ISM165-GAMME12            PIC X(05).                      124  005
           10 ISM165-GAMME13            PIC X(05).                      129  005
           10 ISM165-GAMME14            PIC X(05).                      134  005
           10 ISM165-GAMME15            PIC X(05).                      139  005
           10 ISM165-GAMME16            PIC X(05).                      144  005
           10 ISM165-GAMME17            PIC X(05).                      149  005
           10 ISM165-GAMME18            PIC X(05).                      154  005
           10 ISM165-GAMME19            PIC X(05).                      159  005
           10 ISM165-GAMME2             PIC X(05).                      164  005
           10 ISM165-GAMME3             PIC X(05).                      169  005
           10 ISM165-GAMME4             PIC X(05).                      174  005
           10 ISM165-GAMME5             PIC X(05).                      179  005
           10 ISM165-GAMME6             PIC X(05).                      184  005
           10 ISM165-GAMME7             PIC X(05).                      189  005
           10 ISM165-GAMME8             PIC X(05).                      194  005
           10 ISM165-GAMME9             PIC X(05).                      199  005
           10 ISM165-LIBDATE            PIC X(12).                      204  012
           10 ISM165-LIBELLE            PIC X(06).                      216  006
           10 ISM165-MAG1               PIC X(03).                      222  003
           10 ISM165-MAG10              PIC X(03).                      225  003
           10 ISM165-MAG11              PIC X(03).                      228  003
           10 ISM165-MAG12              PIC X(03).                      231  003
           10 ISM165-MAG13              PIC X(03).                      234  003
           10 ISM165-MAG14              PIC X(03).                      237  003
           10 ISM165-MAG15              PIC X(03).                      240  003
           10 ISM165-MAG16              PIC X(03).                      243  003
           10 ISM165-MAG17              PIC X(03).                      246  003
           10 ISM165-MAG18              PIC X(03).                      249  003
           10 ISM165-MAG19              PIC X(03).                      252  003
           10 ISM165-MAG2               PIC X(03).                      255  003
           10 ISM165-MAG3               PIC X(03).                      258  003
           10 ISM165-MAG4               PIC X(03).                      261  003
           10 ISM165-MAG5               PIC X(03).                      264  003
           10 ISM165-MAG6               PIC X(03).                      267  003
           10 ISM165-MAG7               PIC X(03).                      270  003
           10 ISM165-MAG8               PIC X(03).                      273  003
           10 ISM165-MAG9               PIC X(03).                      276  003
           10 ISM165-TAMPON1            PIC X(30).                      279  030
           10 ISM165-TAMPON2            PIC X(27).                      309  027
           10 ISM165-TAUX1              PIC X(04).                      336  004
           10 ISM165-TAUX10             PIC X(04).                      340  004
           10 ISM165-TAUX11             PIC X(04).                      344  004
           10 ISM165-TAUX12             PIC X(04).                      348  004
           10 ISM165-TAUX13             PIC X(04).                      352  004
           10 ISM165-TAUX14             PIC X(04).                      356  004
           10 ISM165-TAUX15             PIC X(04).                      360  004
           10 ISM165-TAUX16             PIC X(04).                      364  004
           10 ISM165-TAUX17             PIC X(04).                      368  004
           10 ISM165-TAUX18             PIC X(04).                      372  004
           10 ISM165-TAUX19             PIC X(04).                      376  004
           10 ISM165-TAUX2              PIC X(04).                      380  004
           10 ISM165-TAUX3              PIC X(04).                      384  004
           10 ISM165-TAUX4              PIC X(04).                      388  004
           10 ISM165-TAUX5              PIC X(04).                      392  004
           10 ISM165-TAUX6              PIC X(04).                      396  004
           10 ISM165-TAUX7              PIC X(04).                      400  004
           10 ISM165-TAUX8              PIC X(04).                      404  004
           10 ISM165-TAUX9              PIC X(04).                      408  004
           10 ISM165-WEDIT1             PIC X(01).                      412  001
           10 ISM165-WEDIT2             PIC X(01).                      413  001
           10 ISM165-WEDIT3             PIC X(01).                      414  001
           10 ISM165-QSTOCK1            PIC S9(04)      COMP-3.         415  003
           10 ISM165-QSTOCK10           PIC S9(04)      COMP-3.         418  003
           10 ISM165-QSTOCK11           PIC S9(04)      COMP-3.         421  003
           10 ISM165-QSTOCK12           PIC S9(04)      COMP-3.         424  003
           10 ISM165-QSTOCK13           PIC S9(04)      COMP-3.         427  003
           10 ISM165-QSTOCK14           PIC S9(04)      COMP-3.         430  003
           10 ISM165-QSTOCK15           PIC S9(04)      COMP-3.         433  003
           10 ISM165-QSTOCK16           PIC S9(04)      COMP-3.         436  003
           10 ISM165-QSTOCK17           PIC S9(04)      COMP-3.         439  003
           10 ISM165-QSTOCK18           PIC S9(04)      COMP-3.         442  003
           10 ISM165-QSTOCK19           PIC S9(04)      COMP-3.         445  003
           10 ISM165-QSTOCK2            PIC S9(04)      COMP-3.         448  003
           10 ISM165-QSTOCK3            PIC S9(04)      COMP-3.         451  003
           10 ISM165-QSTOCK4            PIC S9(04)      COMP-3.         454  003
           10 ISM165-QSTOCK5            PIC S9(04)      COMP-3.         457  003
           10 ISM165-QSTOCK6            PIC S9(04)      COMP-3.         460  003
           10 ISM165-QSTOCK7            PIC S9(04)      COMP-3.         463  003
           10 ISM165-QSTOCK8            PIC S9(04)      COMP-3.         466  003
           10 ISM165-QSTOCK9            PIC S9(04)      COMP-3.         469  003
            05 FILLER                      PIC X(041).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-ISM165-LONG           PIC S9(4)   COMP  VALUE +471.           
      *                                                                         
      *--                                                                       
        01  DSECT-ISM165-LONG           PIC S9(4) COMP-5  VALUE +471.           
                                                                                
      *}                                                                        
