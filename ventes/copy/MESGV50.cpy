      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ INTERROGATION EXISTENCE RESERVATION                  
      *    EN TOURNEE                                                           
      *****************************************************************         
      *    FT09 : CE MESSAGE SERT AUSSI EN MODIFICATION DE MODE DE              
      *           PAIEMENT                                                      
      *****************************************************************         
      *  POUR MGV50  INTERROGATION DU LOCAL OU MAJ                              
      *  TAILLE 118                                                             
       01  WS-MQ10.                                                             
         02 WS-QUEUE.                                                           
               10   MQ10-CORRELID PIC    X(24).                                 
         02 WS-CODRET             PIC    XX.                                    
         02 MESGV50.                                                            
           10  MESGV50-ENTETE.                                                  
               20   MESGV50-TYPE  PIC    X(3).                                  
               20   MESGV50-NSOCMSG PIC  X(3).                                  
               20   MESGV50-NLIEUMSG PIC X(3).                                  
               20   MESGV50-NSOCDST PIC  X(3).                                  
               20   MESGV50-NLIEUDST PIC X(3).                                  
               20   MESGV50-NORD  PIC    9(8).                                  
               20   MESGV50-LPROG PIC    X(10).                                 
               20   MESGV50-DJOUR PIC    X(8).                                  
               20   MESGV50-WSID  PIC    X(10).                                 
               20   MESGV50-USER  PIC    X(10).                                 
               20   MESGV50-CHRONO PIC   9(7).                                  
               20   MESGV50-NBRMSG PIC   9(7).                                  
               20   MESGV50-FILLER PIC   X(30).                                 
           10  MESGV50-VENTE.                                                   
            15  MESGV50-DATA-VTE.                                               
               20   MESGV50-NSOC              PIC X(3).                         
               20   MESGV50-NLIEU             PIC X(3).                         
               20   MESGV50-NVENTE            PIC X(7).                         
      *  POUR MGV50  MAJ GV EN TOURNEE DE LIVRAISON                             
               20   MESMAJ-PTTVENTE          PIC S9(7)V99 COMP-3.               
               20   MESMAJ-PCOMPT            PIC S9(7)V99 COMP-3.               
               20   MESMAJ-PVERSE            PIC S9(7)V99 COMP-3.               
               20   MESMAJ-PLIVR             PIC S9(7)V99 COMP-3.               
               20   MESMAJ-PDIFFERE          PIC S9(7)V99 COMP-3.               
               20   MESMAJ-PRFACT            PIC S9(7)V99 COMP-3.               
            15  MESMAJ-COMMENTAIRE.                                             
               20   MESMAJ-ENC-LIV           PIC X(1).                          
               20   MESMAJ-LCOMMENT1         PIC X(71).                         
               20   MESMAJ-LCOMMENT2         PIC X(79).                         
               20   MESMAJ-LCOMMENT3         PIC X(79).                         
               20   MESMAJ-LCOMMENT4         PIC X(79).                         
               20   MESMAJ-LCOMMENT5         PIC X(79).                         
            15  MESMAJ-CREDIT.                                                  
               20   MESMAJ-CORGORED          PIC X(5).                          
               20   MESMAJ-CFCRED            PIC X(5).                          
               20   MESMAJ-NCREDI            PIC X(14).                         
            15  MESMAJ-ARF.                                                     
               20   MESMAJ-NAUTO             PIC X(20).                         
            15   MES50-NBPA01                PIC 9(3).                          
FT09        15   MES50-INDMOD                PIC X(1).                          
            15 MES50-PAIMT   OCCURS 0 TO 100 DEPENDING ON MES50-NBPA01.         
               20   PAI-NSOC      PIC     X(3).                                 
               20   PAI-NLIEU     PIC     X(3).                                 
               20   PAI-NMVTFC    PIC     9(8).                                 
               20   PAI-CODEFC    PIC     X(2).                                 
               20   PAI-PVENTF    PIC     S9(7)V9(002) COMP-3.                  
               20   PAI-CDEV      PIC     X(3).                                 
               20   PAI-PPMVTF    PIC     S9(7)V9(002) COMP-3.                  
               20   PAI-CDEVRF    PIC     X(3).                                 
               20   PAI-NSOCP     PIC     X(3).                                 
               20   PAI-NLIEUP    PIC     X(3).                                 
               20   PAI-NTRANS    PIC     9(8).                                 
               20   PAI-NLGPAI    PIC     9(3).                                 
               20   PAI-CMOPAI    PIC     X(5).                                 
               20   PAI-CDEVP     PIC     X(3).                                 
               20   PAI-PMECRT    PIC     S9(7)V9(002) COMP-3.                  
               20   PAI-CMOPANC   PIC     X(5).                                 
FT09           20   PAI-INDMOD    PIC     X(1).                                 
                                                                                
