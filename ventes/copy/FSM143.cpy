      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * COPIE UTILISE DANS LE BSM143 + BSM144  (LRECL = 150)                    
AL1008* AGRANDISSEMENT DU LRECL A 150 FICHIER AJOUTE EN ENTREE DU BSM145        
       01 DSECT-FSM143.                                                         
          02 FSM143-DONNEES.                                                    
             05 FSM143-NCODIC       PIC X(07).                                  
             05 FSM143-CFAM         PIC X(05).                                  
             05 FSM143-LFAM         PIC X(20).                                  
             05 FSM143-LVMARKET1    PIC X(20).                                  
             05 FSM143-LVMARKET2    PIC X(20).                                  
             05 FSM143-LVMARKET3    PIC X(20).                                  
             05 FSM143-WSEQFAM      PIC S9(5) COMP-3.                           
             05 FSM143-CHEFPROD     PIC X(05).                                  
AL1008       05 FSM143-PRMP         PIC S9(7)V9(0006) COMP-3.                   
AL1008       05 FSM143-CTAUXTVA     PIC X(05).                                  
AL1008       05 FSM143-TXTVA        PIC S9(03)V99 COMP-3.                       
AL1008       05 FILLER              PIC X(35).                                  
                                                                                
