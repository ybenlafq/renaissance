      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010001
      *   COPY DE LA TABLE RVVT1400                                     00020003
      **********************************************************        00030001
      *                                                                 00040001
      *---------------------------------------------------------        00050001
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVT1400                 00060003
      *---------------------------------------------------------        00070001
      *                                                                 00080001
       01  RVVT1400.                                                    00090003
           02  VT14-NFLAG                                               00100003
               PIC X(0001).                                             00110003
           02  VT14-NCLUSTER                                            00120003
               PIC X(0013).                                             00130003
           02  VT14-NSOCIETE                                            00140003
               PIC X(0003).                                             00150003
           02  VT14-NSEQ                                                00160003
               PIC X(0003).                                             00170001
           02  VT14-DSAISIE                                             00180003
               PIC X(0008).                                             00190001
           02  VT14-CMODPAIMT                                           00200003
               PIC X(0005).                                             00210001
           02  VT14-PREGLTVTE                                           00220003
               PIC S9(7)V9(0002) COMP-3.                                00230001
           02  VT14-NREGLTVTE                                           00240003
               PIC X(0007).                                             00250001
           02  VT14-DREGLTVTE                                           00260003
               PIC X(0008).                                             00270001
           02  VT14-WREGLTVTE                                           00280003
               PIC X(0001).                                             00290001
           02  VT14-NSOCP                                               00300004
               PIC X(0003).                                             00310004
           02  VT14-NLIEUP                                              00320004
               PIC X(0003).                                             00330004
           02  VT14-NTRANS                                              00340004
               PIC S9(8) COMP-3.                                        00350004
           02  VT14-CVENDEUR                                            00360004
               PIC X(0007).                                             00370004
           02  VT14-COCRED                                              00380003
               PIC X(0001).                                             00390001
           02  VT14-CFCRED                                              00400003
               PIC X(0005).                                             00410001
           02  VT14-NCREDI                                              00420003
               PIC X(0014).                                             00430001
      *                                                                 00440001
      *---------------------------------------------------------        00450001
      *   LISTE DES FLAGS DE LA TABLE RVVT1400                          00460003
      *---------------------------------------------------------        00470001
      *                                                                 00480001
       01  RVVT1400-FLAGS.                                              00490003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT14-NFLAG-F                                             00500003
      *        PIC S9(4) COMP.                                          00510001
      *--                                                                       
           02  VT14-NFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT14-NCLUSTER-F                                          00520003
      *        PIC S9(4) COMP.                                          00530001
      *--                                                                       
           02  VT14-NCLUSTER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT14-NSOCIETE-F                                          00540003
      *        PIC S9(4) COMP.                                          00550003
      *--                                                                       
           02  VT14-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT14-NSEQ-F                                              00560003
      *        PIC S9(4) COMP.                                          00570001
      *--                                                                       
           02  VT14-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT14-DSAISIE-F                                           00580003
      *        PIC S9(4) COMP.                                          00590001
      *--                                                                       
           02  VT14-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT14-CMODPAIMT-F                                         00600003
      *        PIC S9(4) COMP.                                          00610001
      *--                                                                       
           02  VT14-CMODPAIMT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT14-PREGLTVTE-F                                         00620003
      *        PIC S9(4) COMP.                                          00630001
      *--                                                                       
           02  VT14-PREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT14-NREGLTVTE-F                                         00640003
      *        PIC S9(4) COMP.                                          00650001
      *--                                                                       
           02  VT14-NREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT14-DREGLTVTE-F                                         00660003
      *        PIC S9(4) COMP.                                          00670001
      *--                                                                       
           02  VT14-DREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT14-WREGLTVTE-F                                         00680003
      *        PIC S9(4) COMP.                                          00690001
      *--                                                                       
           02  VT14-WREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT14-NSOCP-F                                             00700004
      *        PIC S9(4) COMP.                                          00710001
      *--                                                                       
           02  VT14-NSOCP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT14-NLIEUP-F                                            00720004
      *        PIC S9(4) COMP.                                          00730004
      *--                                                                       
           02  VT14-NLIEUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT14-NTRANS-F                                            00740004
      *        PIC S9(4) COMP.                                          00750004
      *--                                                                       
           02  VT14-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT14-CVENDEUR-F                                          00760004
      *        PIC S9(4) COMP.                                          00770004
      *--                                                                       
           02  VT14-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT14-COCRED-F                                            00780003
      *        PIC S9(4) COMP.                                          00790001
      *--                                                                       
           02  VT14-COCRED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT14-CFCRED-F                                            00800003
      *        PIC S9(4) COMP.                                          00810001
      *--                                                                       
           02  VT14-CFCRED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT14-NCREDI-F                                            00820003
      *        PIC S9(4) COMP.                                          00830001
      *--                                                                       
           02  VT14-NCREDI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00840001
                                                                                
