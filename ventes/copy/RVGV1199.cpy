      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RTGV11                        *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV1199.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV1199.                                                            
      *}                                                                        
           10 GV11-NSOCIETE        PIC X(3).                                    
           10 GV11-NLIEU           PIC X(3).                                    
           10 GV11-NVENTE          PIC X(7).                                    
           10 GV11-CTYPENREG       PIC X(1).                                    
           10 GV11-NCODICGRP       PIC X(7).                                    
           10 GV11-NCODIC          PIC X(7).                                    
           10 GV11-NSEQ            PIC X(2).                                    
           10 GV11-CMODDEL         PIC X(3).                                    
           10 GV11-DDELIV          PIC X(8).                                    
           10 GV11-NORDRE          PIC X(5).                                    
           10 GV11-CENREG          PIC X(5).                                    
           10 GV11-QVENDUE         PIC S9(5)V USAGE COMP-3.                     
           10 GV11-PVUNIT          PIC S9(7)V9(2) USAGE COMP-3.                 
           10 GV11-PVUNITF         PIC S9(7)V9(2) USAGE COMP-3.                 
           10 GV11-PVTOTAL         PIC S9(7)V9(2) USAGE COMP-3.                 
           10 GV11-CEQUIPE         PIC X(5).                                    
           10 GV11-NLIGNE          PIC X(2).                                    
           10 GV11-TAUXTVA         PIC S9(3)V9(2) USAGE COMP-3.                 
           10 GV11-QCONDT          PIC S9(5)V USAGE COMP-3.                     
           10 GV11-PRMP            PIC S9(7)V9(2) USAGE COMP-3.                 
           10 GV11-WEMPORTE        PIC X(1).                                    
           10 GV11-CPLAGE          PIC X(2).                                    
           10 GV11-CPROTOUR        PIC X(5).                                    
           10 GV11-CADRTOUR        PIC X(1).                                    
           10 GV11-CPOSTAL         PIC X(5).                                    
           10 GV11-LCOMMENT        PIC X(35).                                   
           10 GV11-CVENDEUR        PIC X(6).                                    
           10 GV11-NSOCLIVR        PIC X(3).                                    
           10 GV11-NDEPOT          PIC X(3).                                    
           10 GV11-NAUTORM         PIC X(5).                                    
           10 GV11-WARTINEX        PIC X(1).                                    
           10 GV11-WEDITBL         PIC X(1).                                    
           10 GV11-WACOMMUTER      PIC X(1).                                    
           10 GV11-WCQERESF        PIC X(1).                                    
           10 GV11-NMUTATION       PIC X(7).                                    
           10 GV11-CTOURNEE        PIC X(8).                                    
           10 GV11-WTOPELIVRE      PIC X(1).                                    
           10 GV11-DCOMPTA         PIC X(8).                                    
           10 GV11-DCREATION       PIC X(8).                                    
           10 GV11-HCREATION       PIC X(4).                                    
           10 GV11-DANNULATION     PIC X(8).                                    
           10 GV11-NLIEUORIG       PIC X(3).                                    
           10 GV11-WINTMAJ         PIC X(1).                                    
           10 GV11-DSYST           PIC S9(13)V USAGE COMP-3.                    
           10 GV11-DSTAT           PIC X(4).                                    
           10 GV11-CPRESTGRP       PIC X(5).                                    
      *                       CODE SOCIETE DE MODIFICATION                      
           10 GV11-NSOCMODIF       PIC X(3).                                    
      *                       CODE LIEU DE MODIFICATION                         
           10 GV11-NLIEUMODIF      PIC X(3).                                    
      *                       DATE DU PREMIER ENCAISSEMENT                      
           10 GV11-DATENC          PIC X(8).                                    
      *                       CODE DEVISE DE REFERENCE                          
           10 GV11-CDEV            PIC X(3).                                    
      *                       DONNEE UNITAIRE VENTE REPRISE                     
           10 GV11-WUNITR          PIC X(20).                                   
      *                       COMMENTAIRE RESERVATION                           
           10 GV11-LRCMMT          PIC X(10).                                   
      *                       CODE SOCIETE PAIEMENT                             
           10 GV11-NSOCP           PIC X(3).                                    
      *                       CODE MAGASIN DE PAIEMENT                          
           10 GV11-NLIEUP          PIC X(3).                                    
      *                       N� TRANSACTION PAIEMENT                           
           10 GV11-NTRANS          PIC S9(8)V USAGE COMP-3.                     
      *                       N� SEQUENCE FACTURE VENTE                         
           10 GV11-NSEQFV          PIC X(2).                                    
           10 GV11-NSEQNQ          PIC S9(5)V USAGE COMP-3.                     
           10 GV11-NSEQREF         PIC S9(5)V USAGE COMP-3.                     
           10 GV11-NMODIF          PIC S9(5)V USAGE COMP-3.                     
           10 GV11-NSOCORIG        PIC X(3).                                    
           10 GV11-DTOPE           PIC X(8).                                    
      *                       SOCIETE DE STOCK DE DEPART                        
           10 GV11-NSOCLIVR1       PIC X(3).                                    
      *                       SOCIETE DE GESTION LIVRAISON                      
           10 GV11-NSOCGEST        PIC X(3).                                    
      *                       LIEU DE GESTION DE LIVRAISON                      
           10 GV11-NLIEUGEST       PIC X(3).                                    
      *                       SOCIETE DE DEPART LIVRAISON                       
           10 GV11-NSOCDEPLIV      PIC X(3).                                    
      *                       LIEU DE DEPART DE LIVRAISON                       
           10 GV11-NLIEUDEPLIV     PIC X(3).                                    
      *                       LIEU    DE STOCK DE DEPART                        
           10 GV11-NDEPOT1         PIC X(3).                                    
      *                       TYPE DE VENTE G,B,L,D,A                           
           10 GV11-TYPVTE          PIC X(1).                                    
      *                       TYPE D ENTITE                                     
           10 GV11-CTYPENT         PIC X(2).                                    
      *                       NSEQNQ ENTITE LIEE                                
           10 GV11-NLIEN           PIC S9(5)V USAGE COMP-3.                     
      *                       N� ACTE DE VENTE                                  
           10 GV11-NACTVTE         PIC S9(5)V USAGE COMP-3.                     
      *                       PRIX CODICGRP OU OFFRE                            
           10 GV11-PVCODIG         PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       QUANTITE GROUPE                                   
           10 GV11-QTCODIG         PIC S9(5)V USAGE COMP-3.                     
      *                       DATE RESERVATION PROLONGEE                        
           10 GV11-DPRLG           PIC X(8).                                    
      *                       CODE ALERTE STOCK                                 
           10 GV11-CALERTE         PIC X(5).                                    
      *                       CODE REPRISE                                      
           10 GV11-CREPRISE        PIC X(5).                                    
      *                       N� SEQUENCE ENTITE ASSEMBLEE                      
           10 GV11-NSEQENS         PIC S9(5)V USAGE COMP-3.                     
      *                       DIFFERENTIEL                                      
           10 GV11-MPRIMECLI       PIC S9(7)V9(2) USAGE COMP-3.                 
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  IRTGV11.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  IRTGV11.                                                             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NSOCIETE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NSOCIETE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NLIEU-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NLIEU-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NVENTE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NVENTE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-CTYPENREG-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-CTYPENREG-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NCODICGRP-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NCODICGRP-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NSEQ-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NSEQ-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-CMODDEL-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-CMODDEL-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-DDELIV-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-DDELIV-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NORDRE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NORDRE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-CENREG-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-CENREG-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-QVENDUE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-QVENDUE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-PVUNIT-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-PVUNIT-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-PVUNITF-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-PVUNITF-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-PVTOTAL-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-PVTOTAL-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-CEQUIPE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-CEQUIPE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NLIGNE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NLIGNE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-TAUXTVA-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-TAUXTVA-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-QCONDT-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-QCONDT-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-PRMP-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-PRMP-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-WEMPORTE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-WEMPORTE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-CPLAGE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-CPLAGE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-CPROTOUR-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-CPROTOUR-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-CADRTOUR-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-CADRTOUR-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-CPOSTAL-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-CPOSTAL-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-LCOMMENT-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-LCOMMENT-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-CVENDEUR-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-CVENDEUR-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NSOCLIVR-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NSOCLIVR-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NDEPOT-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NDEPOT-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NAUTORM-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NAUTORM-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-WARTINEX-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-WARTINEX-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-WEDITBL-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-WEDITBL-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-WACOMMUTER-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-WACOMMUTER-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-WCQERESF-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-WCQERESF-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NMUTATION-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NMUTATION-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-CTOURNEE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-CTOURNEE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-WTOPELIVRE-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-WTOPELIVRE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-DCOMPTA-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-DCOMPTA-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-DCREATION-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-DCREATION-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-HCREATION-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-HCREATION-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-DANNULATION-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-DANNULATION-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NLIEUORIG-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NLIEUORIG-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-WINTMAJ-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-WINTMAJ-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-DSYST-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-DSTAT-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-DSTAT-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-CPRESTGRP-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-CPRESTGRP-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NSOCMODIF-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NSOCMODIF-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NLIEUMODIF-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NLIEUMODIF-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-DATENC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-DATENC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-CDEV-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-CDEV-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-WUNITR-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-WUNITR-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-LRCMMT-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-LRCMMT-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NSOCP-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NSOCP-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NLIEUP-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NLIEUP-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NTRANS-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NTRANS-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NSEQFV-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NSEQFV-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NSEQNQ-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NSEQNQ-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NSEQREF-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NSEQREF-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NMODIF-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NMODIF-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NSOCORIG-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NSOCORIG-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-DTOPE-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-DTOPE-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NSOCLIVR1-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NSOCLIVR1-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NSOCGEST-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NSOCGEST-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NLIEUGEST-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NLIEUGEST-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NSOCDEPLIV-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NSOCDEPLIV-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NLIEUDEPLIV-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NLIEUDEPLIV-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NDEPOT1-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NDEPOT1-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-TYPVTE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-TYPVTE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-CTYPENT-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-CTYPENT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NLIEN-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NLIEN-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NACTVTE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NACTVTE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-PVCODIG-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-PVCODIG-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-QTCODIG-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-QTCODIG-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-DPRLG-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-DPRLG-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-CALERTE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-CALERTE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-CREPRISE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-CREPRISE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-NSEQENS-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-NSEQENS-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV11-MPRIMECLI-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GV11-MPRIMECLI-F     PIC S9(4) COMP-5.                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 78      *        
      ******************************************************************        
                                                                                
