      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000010
      * >>>>>>>>>>>>>>>>> DESCRIPTION DE LA TSHV42 <<<<<<<<<<<<<<<<<< * 00000020
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000030
      *                                                               * 00000040
       01  TSHV42-IDENTIFICATEUR.                                       00000050
           05  TSHV42-TRANSID               PIC X(04) VALUE 'HV42'.     00000060
           05  TSHV42-NUMERO-DE-TERMINAL    PIC X(04) VALUE SPACES.     00000070
      *                                                               * 00000080
       01  TSHV42-ITEM.                                                 00000090
           05  TSHV42-LONGUEUR              PIC S9(4) VALUE +0496.      00000100
           05  TSHV42-DATAS.                                            00000110
               10  TSHV42-LIGNE             OCCURS 08.                  00000120
      *         DESCRIPTION DETAIL VENTE 062 OCTETS PAR OCCURENCE     * 00000200
                       20 TSHV42-TLG               PIC X(03).                   
                       20 TSHV42-CMARQ             PIC X(05).                   
                       20 TSHV42-CFAM              PIC X(05).                   
                       20 TSHV42-LREF              PIC X(20).                   
                       20 TSHV42-NCODIC            PIC X(07).                   
                       20 TSHV42-CMODDEL           PIC X(03).                   
                       20 TSHV42-DDELIV            PIC X(06).                   
                       20 TSHV42-QTE               PIC S9(03) COMP-3.           
                       20 TSHV42-PVUNIT            PIC S9(05)V99 COMP-3.        
                       20 TSHV42-PVTOTAL           PIC S9(07)V99 COMP-3.        
      *                                                               * 00000200
      *                                                               * 00000200
      *                                                               * 00000200
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000210
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< * 00000220
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000230
                                                                                
