      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IFS085 AU 10/12/1996  *        
      *                                                                *        
      *          CRITERES DE TRI  07,06,BI,A,                          *        
      *                           13,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IFS085.                                                        
            05 NOMETAT-IFS085           PIC X(6) VALUE 'IFS085'.                
            05 RUPTURES-IFS085.                                                 
           10 IFS085-DDELIV             PIC X(06).                      007  006
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IFS085-SEQUENCE           PIC S9(04) COMP.                013  002
      *--                                                                       
           10 IFS085-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IFS085.                                                   
           10 IFS085-CPROTOUR           PIC X(05).                      015  005
           10 IFS085-CTOURNEE           PIC X(03).                      020  003
           10 IFS085-LSOC               PIC X(25).                      023  025
           10 IFS085-NLIEU              PIC X(03).                      048  003
           10 IFS085-NSOC               PIC X(03).                      051  003
           10 IFS085-NVENTE             PIC X(07).                      054  007
           10 IFS085-FDATE              PIC X(06).                      061  006
            05 FILLER                      PIC X(446).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IFS085-LONG           PIC S9(4)   COMP  VALUE +066.           
      *                                                                         
      *--                                                                       
        01  DSECT-IFS085-LONG           PIC S9(4) COMP-5  VALUE +066.           
                                                                                
      *}                                                                        
