      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TDCTY CORRESPONDANCE CTYPPREST/CTYPE   *        
      *----------------------------------------------------------------*        
       01  RVTDCTY.                                                             
           05  TDCTY-CTABLEG2    PIC X(15).                                     
           05  TDCTY-CTABLEG2-REDEF REDEFINES TDCTY-CTABLEG2.                   
               10  TDCTY-CTYPPRES        PIC X(05).                             
           05  TDCTY-WTABLEG     PIC X(80).                                     
           05  TDCTY-WTABLEG-REDEF  REDEFINES TDCTY-WTABLEG.                    
               10  TDCTY-CTYPE           PIC X(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTDCTY-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDCTY-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TDCTY-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDCTY-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TDCTY-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
