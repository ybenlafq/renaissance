      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: Eem47   Eem47                                              00000020
      ***************************************************************** 00000030
       01   EEM47I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * date du jour                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * heure                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * n� de page                                                      00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOPAGEL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MNOPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOPAGEF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MNOPAGEI  PIC X(3).                                       00000200
      * nbr de page total                                               00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGEL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPAGEF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBPAGEI  PIC X(3).                                       00000250
      * n� societe                                                      00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MNSOCI    PIC X(3).                                       00000300
      * n� magasin                                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MNMAGI    PIC X(3).                                       00000350
      * n� bon commande                                                 00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MNVENTEI  PIC X(8).                                       00000400
      * n� caisse                                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCAISL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNCAISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCAISF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNCAISI   PIC X(3).                                       00000450
      * date transaction                                                00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDTRANSL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MDTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDTRANSF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MDTRANSI  PIC X(10).                                      00000500
      * n� operateur                                                    00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOPERL   COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MNOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNOPERF   PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MNOPERI   PIC X(7).                                       00000550
      * n� transaction                                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTRANSL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MNTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNTRANSF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MNTRANSI  PIC X(8).                                       00000600
      * heure transaction                                               00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDHTRANSL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MDHTRANSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDHTRANSF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDHTRANSI      PIC X(5).                                  00000650
      * no vendeur                                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENDEURL     COMP PIC S9(4).                            00000670
      *--                                                                       
           02 MNVENDEURL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNVENDEURF     PIC X.                                     00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MNVENDEURI     PIC X(7).                                  00000700
      * type de vente                                                   00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPVTEL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MTYPVTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPVTEF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MTYPVTEI  PIC X(3).                                       00000750
      * date de vente                                                   00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVTEL    COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MDVTEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDVTEF    PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MDVTEI    PIC X(10).                                      00000800
      * heure de vente                                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MHVTEL    COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MHVTEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MHVTEF    PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MHVTEI    PIC X(5).                                       00000850
           02 MTAB1I OCCURS   8 TIMES .                                 00000860
      * marque                                                          00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000880
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000890
             03 FILLER  PIC X(4).                                       00000900
             03 MCMARQI      PIC X(5).                                  00000910
      * famille                                                         00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000930
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000940
             03 FILLER  PIC X(4).                                       00000950
             03 MCFAMI  PIC X(5).                                       00000960
      * libelle famille                                                 00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000980
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000990
             03 FILLER  PIC X(4).                                       00001000
             03 MLREFI  PIC X(20).                                      00001010
      * codic:no article                                                00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MNCODICI     PIC X(7).                                  00001060
      * mode de delivrance                                              00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MODDELL      COMP PIC S9(4).                            00001080
      *--                                                                       
             03 MODDELL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MODDELF      PIC X.                                     00001090
             03 FILLER  PIC X(4).                                       00001100
             03 MODDELI      PIC X(3).                                  00001110
      * quantite                                                        00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00001130
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00001140
             03 FILLER  PIC X(4).                                       00001150
             03 MQTEI   PIC X(3).                                       00001160
      * remise                                                          00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPREML  COMP PIC S9(4).                                 00001180
      *--                                                                       
             03 MPREML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPREMF  PIC X.                                          00001190
             03 FILLER  PIC X(4).                                       00001200
             03 MPREMI  PIC X(3).                                       00001210
      * prix vente unitaire                                             00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPVUNITL     COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MPVUNITL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPVUNITF     PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MPVUNITI     PIC X(8).                                  00001260
      * montant remise                                                  00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRABL  COMP PIC S9(4).                                 00001280
      *--                                                                       
             03 MPRABL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPRABF  PIC X.                                          00001290
             03 FILLER  PIC X(4).                                       00001300
             03 MPRABI  PIC X(7).                                       00001310
      * montant a payer                                                 00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPVTOTALL    COMP PIC S9(4).                            00001330
      *--                                                                       
             03 MPVTOTALL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPVTOTALF    PIC X.                                     00001340
             03 FILLER  PIC X(4).                                       00001350
             03 MPVTOTALI    PIC X(8).                                  00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMONTANTL     COMP PIC S9(4).                            00001370
      *--                                                                       
           02 MLMONTANTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLMONTANTF     PIC X.                                     00001380
           02 FILLER    PIC X(4).                                       00001390
           02 MLMONTANTI     PIC X(9).                                  00001400
      * code devise                                                     00001410
           02 MCDEVISED OCCURS   6 TIMES .                              00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCDEVISEL    COMP PIC S9(4).                            00001430
      *--                                                                       
             03 MCDEVISEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCDEVISEF    PIC X.                                     00001440
             03 FILLER  PIC X(4).                                       00001450
             03 MCDEVISEI    PIC X.                                     00001460
      * lib devise                                                      00001470
           02 MCDEVD OCCURS   6 TIMES .                                 00001480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCDEVL  COMP PIC S9(4).                                 00001490
      *--                                                                       
             03 MCDEVL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCDEVF  PIC X.                                          00001500
             03 FILLER  PIC X(4).                                       00001510
             03 MCDEVI  PIC X(5).                                       00001520
      * libelle mode paiement                                           00001530
           02 MLPAID OCCURS   6 TIMES .                                 00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPAIL  COMP PIC S9(4).                                 00001550
      *--                                                                       
             03 MLPAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLPAIF  PIC X.                                          00001560
             03 FILLER  PIC X(4).                                       00001570
             03 MLPAII  PIC X(16).                                      00001580
      * montant transaction                                             00001590
           02 MMTTRSD OCCURS   6 TIMES .                                00001600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTTRSL      COMP PIC S9(4).                            00001610
      *--                                                                       
             03 MMTTRSL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MMTTRSF      PIC X.                                     00001620
             03 FILLER  PIC X(4).                                       00001630
             03 MMTTRSI      PIC X(9).                                  00001640
      * date transaction                                                00001650
           02 MDTRSD OCCURS   6 TIMES .                                 00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDTRSL  COMP PIC S9(4).                                 00001670
      *--                                                                       
             03 MDTRSL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDTRSF  PIC X.                                          00001680
             03 FILLER  PIC X(4).                                       00001690
             03 MDTRSI  PIC X(10).                                      00001700
      * montant rendu                                                   00001710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRENDUL  COMP PIC S9(4).                                 00001720
      *--                                                                       
           02 MTRENDUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTRENDUF  PIC X.                                          00001730
           02 FILLER    PIC X(4).                                       00001740
           02 MTRENDUI  PIC X(9).                                       00001750
      * montant de la vente                                             00001760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTVENTEL  COMP PIC S9(4).                                 00001770
      *--                                                                       
           02 MTVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTVENTEF  PIC X.                                          00001780
           02 FILLER    PIC X(4).                                       00001790
           02 MTVENTEI  PIC X(9).                                       00001800
      * montant au comptant                                             00001810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTCPTL    COMP PIC S9(4).                                 00001820
      *--                                                                       
           02 MTCPTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTCPTF    PIC X.                                          00001830
           02 FILLER    PIC X(4).                                       00001840
           02 MTCPTI    PIC X(9).                                       00001850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTADEDL   COMP PIC S9(4).                                 00001860
      *--                                                                       
           02 MTADEDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTADEDF   PIC X.                                          00001870
           02 FILLER    PIC X(4).                                       00001880
           02 MTADEDI   PIC X(9).                                       00001890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTALIVRL  COMP PIC S9(4).                                 00001900
      *--                                                                       
           02 MTALIVRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTALIVRF  PIC X.                                          00001910
           02 FILLER    PIC X(4).                                       00001920
           02 MTALIVRI  PIC X(9).                                       00001930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTARECFL  COMP PIC S9(4).                                 00001940
      *--                                                                       
           02 MTARECFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTARECFF  PIC X.                                          00001950
           02 FILLER    PIC X(4).                                       00001960
           02 MTARECFI  PIC X(9).                                       00001970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTDREGLL  COMP PIC S9(4).                                 00001980
      *--                                                                       
           02 MTDREGLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTDREGLF  PIC X.                                          00001990
           02 FILLER    PIC X(4).                                       00002000
           02 MTDREGLI  PIC X(9).                                       00002010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFACTL   COMP PIC S9(4).                                 00002020
      *--                                                                       
           02 MNFACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNFACTF   PIC X.                                          00002030
           02 FILLER    PIC X(4).                                       00002040
           02 MNFACTI   PIC X(6).                                       00002050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCREDL   COMP PIC S9(4).                                 00002060
      *--                                                                       
           02 MCCREDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCCREDF   PIC X.                                          00002070
           02 FILLER    PIC X(4).                                       00002080
           02 MCCREDI   PIC X(5).                                       00002090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTCREDL   COMP PIC S9(4).                                 00002100
      *--                                                                       
           02 MTCREDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTCREDF   PIC X.                                          00002110
           02 FILLER    PIC X(4).                                       00002120
           02 MTCREDI   PIC X(9).                                       00002130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00002140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00002150
           02 FILLER    PIC X(4).                                       00002160
           02 MZONCMDI  PIC X(15).                                      00002170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00002180
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00002190
           02 FILLER    PIC X(4).                                       00002200
           02 MLIBERRI  PIC X(58).                                      00002210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00002220
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00002230
           02 FILLER    PIC X(4).                                       00002240
           02 MCODTRAI  PIC X(4).                                       00002250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00002260
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00002270
           02 FILLER    PIC X(4).                                       00002280
           02 MCICSI    PIC X(5).                                       00002290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00002300
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002310
           02 FILLER    PIC X(4).                                       00002320
           02 MNETNAMI  PIC X(8).                                       00002330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002340
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002350
           02 FILLER    PIC X(4).                                       00002360
           02 MSCREENI  PIC X(4).                                       00002370
      ***************************************************************** 00002380
      * SDF: Eem47   Eem47                                              00002390
      ***************************************************************** 00002400
       01   EEM47O REDEFINES EEM47I.                                    00002410
           02 FILLER    PIC X(12).                                      00002420
      * date du jour                                                    00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MDATJOUA  PIC X.                                          00002450
           02 MDATJOUC  PIC X.                                          00002460
           02 MDATJOUP  PIC X.                                          00002470
           02 MDATJOUH  PIC X.                                          00002480
           02 MDATJOUV  PIC X.                                          00002490
           02 MDATJOUO  PIC X(10).                                      00002500
      * heure                                                           00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MTIMJOUA  PIC X.                                          00002530
           02 MTIMJOUC  PIC X.                                          00002540
           02 MTIMJOUP  PIC X.                                          00002550
           02 MTIMJOUH  PIC X.                                          00002560
           02 MTIMJOUV  PIC X.                                          00002570
           02 MTIMJOUO  PIC X(5).                                       00002580
      * n� de page                                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MNOPAGEA  PIC X.                                          00002610
           02 MNOPAGEC  PIC X.                                          00002620
           02 MNOPAGEP  PIC X.                                          00002630
           02 MNOPAGEH  PIC X.                                          00002640
           02 MNOPAGEV  PIC X.                                          00002650
           02 MNOPAGEO  PIC X(3).                                       00002660
      * nbr de page total                                               00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MNBPAGEA  PIC X.                                          00002690
           02 MNBPAGEC  PIC X.                                          00002700
           02 MNBPAGEP  PIC X.                                          00002710
           02 MNBPAGEH  PIC X.                                          00002720
           02 MNBPAGEV  PIC X.                                          00002730
           02 MNBPAGEO  PIC X(3).                                       00002740
      * n� societe                                                      00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MNSOCA    PIC X.                                          00002770
           02 MNSOCC    PIC X.                                          00002780
           02 MNSOCP    PIC X.                                          00002790
           02 MNSOCH    PIC X.                                          00002800
           02 MNSOCV    PIC X.                                          00002810
           02 MNSOCO    PIC X(3).                                       00002820
      * n� magasin                                                      00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MNMAGA    PIC X.                                          00002850
           02 MNMAGC    PIC X.                                          00002860
           02 MNMAGP    PIC X.                                          00002870
           02 MNMAGH    PIC X.                                          00002880
           02 MNMAGV    PIC X.                                          00002890
           02 MNMAGO    PIC X(3).                                       00002900
      * n� bon commande                                                 00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MNVENTEA  PIC X.                                          00002930
           02 MNVENTEC  PIC X.                                          00002940
           02 MNVENTEP  PIC X.                                          00002950
           02 MNVENTEH  PIC X.                                          00002960
           02 MNVENTEV  PIC X.                                          00002970
           02 MNVENTEO  PIC X(8).                                       00002980
      * n� caisse                                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MNCAISA   PIC X.                                          00003010
           02 MNCAISC   PIC X.                                          00003020
           02 MNCAISP   PIC X.                                          00003030
           02 MNCAISH   PIC X.                                          00003040
           02 MNCAISV   PIC X.                                          00003050
           02 MNCAISO   PIC X(3).                                       00003060
      * date transaction                                                00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MDTRANSA  PIC X.                                          00003090
           02 MDTRANSC  PIC X.                                          00003100
           02 MDTRANSP  PIC X.                                          00003110
           02 MDTRANSH  PIC X.                                          00003120
           02 MDTRANSV  PIC X.                                          00003130
           02 MDTRANSO  PIC X(10).                                      00003140
      * n� operateur                                                    00003150
           02 FILLER    PIC X(2).                                       00003160
           02 MNOPERA   PIC X.                                          00003170
           02 MNOPERC   PIC X.                                          00003180
           02 MNOPERP   PIC X.                                          00003190
           02 MNOPERH   PIC X.                                          00003200
           02 MNOPERV   PIC X.                                          00003210
           02 MNOPERO   PIC X(7).                                       00003220
      * n� transaction                                                  00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MNTRANSA  PIC X.                                          00003250
           02 MNTRANSC  PIC X.                                          00003260
           02 MNTRANSP  PIC X.                                          00003270
           02 MNTRANSH  PIC X.                                          00003280
           02 MNTRANSV  PIC X.                                          00003290
           02 MNTRANSO  PIC X(8).                                       00003300
      * heure transaction                                               00003310
           02 FILLER    PIC X(2).                                       00003320
           02 MDHTRANSA      PIC X.                                     00003330
           02 MDHTRANSC PIC X.                                          00003340
           02 MDHTRANSP PIC X.                                          00003350
           02 MDHTRANSH PIC X.                                          00003360
           02 MDHTRANSV PIC X.                                          00003370
           02 MDHTRANSO      PIC X(5).                                  00003380
      * no vendeur                                                      00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MNVENDEURA     PIC X.                                     00003410
           02 MNVENDEURC     PIC X.                                     00003420
           02 MNVENDEURP     PIC X.                                     00003430
           02 MNVENDEURH     PIC X.                                     00003440
           02 MNVENDEURV     PIC X.                                     00003450
           02 MNVENDEURO     PIC X(7).                                  00003460
      * type de vente                                                   00003470
           02 FILLER    PIC X(2).                                       00003480
           02 MTYPVTEA  PIC X.                                          00003490
           02 MTYPVTEC  PIC X.                                          00003500
           02 MTYPVTEP  PIC X.                                          00003510
           02 MTYPVTEH  PIC X.                                          00003520
           02 MTYPVTEV  PIC X.                                          00003530
           02 MTYPVTEO  PIC X(3).                                       00003540
      * date de vente                                                   00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MDVTEA    PIC X.                                          00003570
           02 MDVTEC    PIC X.                                          00003580
           02 MDVTEP    PIC X.                                          00003590
           02 MDVTEH    PIC X.                                          00003600
           02 MDVTEV    PIC X.                                          00003610
           02 MDVTEO    PIC X(10).                                      00003620
      * heure de vente                                                  00003630
           02 FILLER    PIC X(2).                                       00003640
           02 MHVTEA    PIC X.                                          00003650
           02 MHVTEC    PIC X.                                          00003660
           02 MHVTEP    PIC X.                                          00003670
           02 MHVTEH    PIC X.                                          00003680
           02 MHVTEV    PIC X.                                          00003690
           02 MHVTEO    PIC X(5).                                       00003700
           02 MTAB1O OCCURS   8 TIMES .                                 00003710
      * marque                                                          00003720
             03 FILLER       PIC X(2).                                  00003730
             03 MCMARQA      PIC X.                                     00003740
             03 MCMARQC PIC X.                                          00003750
             03 MCMARQP PIC X.                                          00003760
             03 MCMARQH PIC X.                                          00003770
             03 MCMARQV PIC X.                                          00003780
             03 MCMARQO      PIC X(5).                                  00003790
      * famille                                                         00003800
             03 FILLER       PIC X(2).                                  00003810
             03 MCFAMA  PIC X.                                          00003820
             03 MCFAMC  PIC X.                                          00003830
             03 MCFAMP  PIC X.                                          00003840
             03 MCFAMH  PIC X.                                          00003850
             03 MCFAMV  PIC X.                                          00003860
             03 MCFAMO  PIC X(5).                                       00003870
      * libelle famille                                                 00003880
             03 FILLER       PIC X(2).                                  00003890
             03 MLREFA  PIC X.                                          00003900
             03 MLREFC  PIC X.                                          00003910
             03 MLREFP  PIC X.                                          00003920
             03 MLREFH  PIC X.                                          00003930
             03 MLREFV  PIC X.                                          00003940
             03 MLREFO  PIC X(20).                                      00003950
      * codic:no article                                                00003960
             03 FILLER       PIC X(2).                                  00003970
             03 MNCODICA     PIC X.                                     00003980
             03 MNCODICC     PIC X.                                     00003990
             03 MNCODICP     PIC X.                                     00004000
             03 MNCODICH     PIC X.                                     00004010
             03 MNCODICV     PIC X.                                     00004020
             03 MNCODICO     PIC X(7).                                  00004030
      * mode de delivrance                                              00004040
             03 FILLER       PIC X(2).                                  00004050
             03 MODDELA      PIC X.                                     00004060
             03 MODDELC PIC X.                                          00004070
             03 MODDELP PIC X.                                          00004080
             03 MODDELH PIC X.                                          00004090
             03 MODDELV PIC X.                                          00004100
             03 MODDELO      PIC X(3).                                  00004110
      * quantite                                                        00004120
             03 FILLER       PIC X(2).                                  00004130
             03 MQTEA   PIC X.                                          00004140
             03 MQTEC   PIC X.                                          00004150
             03 MQTEP   PIC X.                                          00004160
             03 MQTEH   PIC X.                                          00004170
             03 MQTEV   PIC X.                                          00004180
             03 MQTEO   PIC X(3).                                       00004190
      * remise                                                          00004200
             03 FILLER       PIC X(2).                                  00004210
             03 MPREMA  PIC X.                                          00004220
             03 MPREMC  PIC X.                                          00004230
             03 MPREMP  PIC X.                                          00004240
             03 MPREMH  PIC X.                                          00004250
             03 MPREMV  PIC X.                                          00004260
             03 MPREMO  PIC X(3).                                       00004270
      * prix vente unitaire                                             00004280
             03 FILLER       PIC X(2).                                  00004290
             03 MPVUNITA     PIC X.                                     00004300
             03 MPVUNITC     PIC X.                                     00004310
             03 MPVUNITP     PIC X.                                     00004320
             03 MPVUNITH     PIC X.                                     00004330
             03 MPVUNITV     PIC X.                                     00004340
             03 MPVUNITO     PIC Z(4)9,99.                              00004350
      * montant remise                                                  00004360
             03 FILLER       PIC X(2).                                  00004370
             03 MPRABA  PIC X.                                          00004380
             03 MPRABC  PIC X.                                          00004390
             03 MPRABP  PIC X.                                          00004400
             03 MPRABH  PIC X.                                          00004410
             03 MPRABV  PIC X.                                          00004420
             03 MPRABO  PIC Z(3)9,99.                                   00004430
      * montant a payer                                                 00004440
             03 FILLER       PIC X(2).                                  00004450
             03 MPVTOTALA    PIC X.                                     00004460
             03 MPVTOTALC    PIC X.                                     00004470
             03 MPVTOTALP    PIC X.                                     00004480
             03 MPVTOTALH    PIC X.                                     00004490
             03 MPVTOTALV    PIC X.                                     00004500
             03 MPVTOTALO    PIC Z(4)9,99.                              00004510
           02 FILLER    PIC X(2).                                       00004520
           02 MLMONTANTA     PIC X.                                     00004530
           02 MLMONTANTC     PIC X.                                     00004540
           02 MLMONTANTP     PIC X.                                     00004550
           02 MLMONTANTH     PIC X.                                     00004560
           02 MLMONTANTV     PIC X.                                     00004570
           02 MLMONTANTO     PIC X(9).                                  00004580
      * code devise                                                     00004590
           02 DFHMS1 OCCURS   6 TIMES .                                 00004600
             03 FILLER       PIC X(2).                                  00004610
             03 MCDEVISEA    PIC X.                                     00004620
             03 MCDEVISEC    PIC X.                                     00004630
             03 MCDEVISEP    PIC X.                                     00004640
             03 MCDEVISEH    PIC X.                                     00004650
             03 MCDEVISEV    PIC X.                                     00004660
             03 MCDEVISEO    PIC X.                                     00004670
      * lib devise                                                      00004680
           02 DFHMS2 OCCURS   6 TIMES .                                 00004690
             03 FILLER       PIC X(2).                                  00004700
             03 MCDEVA  PIC X.                                          00004710
             03 MCDEVC  PIC X.                                          00004720
             03 MCDEVP  PIC X.                                          00004730
             03 MCDEVH  PIC X.                                          00004740
             03 MCDEVV  PIC X.                                          00004750
             03 MCDEVO  PIC X(5).                                       00004760
      * libelle mode paiement                                           00004770
           02 DFHMS3 OCCURS   6 TIMES .                                 00004780
             03 FILLER       PIC X(2).                                  00004790
             03 MLPAIA  PIC X.                                          00004800
             03 MLPAIC  PIC X.                                          00004810
             03 MLPAIP  PIC X.                                          00004820
             03 MLPAIH  PIC X.                                          00004830
             03 MLPAIV  PIC X.                                          00004840
             03 MLPAIO  PIC X(16).                                      00004850
      * montant transaction                                             00004860
           02 DFHMS4 OCCURS   6 TIMES .                                 00004870
             03 FILLER       PIC X(2).                                  00004880
             03 MMTTRSA      PIC X.                                     00004890
             03 MMTTRSC PIC X.                                          00004900
             03 MMTTRSP PIC X.                                          00004910
             03 MMTTRSH PIC X.                                          00004920
             03 MMTTRSV PIC X.                                          00004930
             03 MMTTRSO      PIC Z(5)9,99.                              00004940
      * date transaction                                                00004950
           02 DFHMS5 OCCURS   6 TIMES .                                 00004960
             03 FILLER       PIC X(2).                                  00004970
             03 MDTRSA  PIC X.                                          00004980
             03 MDTRSC  PIC X.                                          00004990
             03 MDTRSP  PIC X.                                          00005000
             03 MDTRSH  PIC X.                                          00005010
             03 MDTRSV  PIC X.                                          00005020
             03 MDTRSO  PIC X(10).                                      00005030
      * montant rendu                                                   00005040
           02 FILLER    PIC X(2).                                       00005050
           02 MTRENDUA  PIC X.                                          00005060
           02 MTRENDUC  PIC X.                                          00005070
           02 MTRENDUP  PIC X.                                          00005080
           02 MTRENDUH  PIC X.                                          00005090
           02 MTRENDUV  PIC X.                                          00005100
           02 MTRENDUO  PIC Z(5)9,99.                                   00005110
      * montant de la vente                                             00005120
           02 FILLER    PIC X(2).                                       00005130
           02 MTVENTEA  PIC X.                                          00005140
           02 MTVENTEC  PIC X.                                          00005150
           02 MTVENTEP  PIC X.                                          00005160
           02 MTVENTEH  PIC X.                                          00005170
           02 MTVENTEV  PIC X.                                          00005180
           02 MTVENTEO  PIC Z(5)9,99.                                   00005190
      * montant au comptant                                             00005200
           02 FILLER    PIC X(2).                                       00005210
           02 MTCPTA    PIC X.                                          00005220
           02 MTCPTC    PIC X.                                          00005230
           02 MTCPTP    PIC X.                                          00005240
           02 MTCPTH    PIC X.                                          00005250
           02 MTCPTV    PIC X.                                          00005260
           02 MTCPTO    PIC Z(5)9,99.                                   00005270
           02 FILLER    PIC X(2).                                       00005280
           02 MTADEDA   PIC X.                                          00005290
           02 MTADEDC   PIC X.                                          00005300
           02 MTADEDP   PIC X.                                          00005310
           02 MTADEDH   PIC X.                                          00005320
           02 MTADEDV   PIC X.                                          00005330
           02 MTADEDO   PIC X(9).                                       00005340
           02 FILLER    PIC X(2).                                       00005350
           02 MTALIVRA  PIC X.                                          00005360
           02 MTALIVRC  PIC X.                                          00005370
           02 MTALIVRP  PIC X.                                          00005380
           02 MTALIVRH  PIC X.                                          00005390
           02 MTALIVRV  PIC X.                                          00005400
           02 MTALIVRO  PIC X(9).                                       00005410
           02 FILLER    PIC X(2).                                       00005420
           02 MTARECFA  PIC X.                                          00005430
           02 MTARECFC  PIC X.                                          00005440
           02 MTARECFP  PIC X.                                          00005450
           02 MTARECFH  PIC X.                                          00005460
           02 MTARECFV  PIC X.                                          00005470
           02 MTARECFO  PIC X(9).                                       00005480
           02 FILLER    PIC X(2).                                       00005490
           02 MTDREGLA  PIC X.                                          00005500
           02 MTDREGLC  PIC X.                                          00005510
           02 MTDREGLP  PIC X.                                          00005520
           02 MTDREGLH  PIC X.                                          00005530
           02 MTDREGLV  PIC X.                                          00005540
           02 MTDREGLO  PIC X(9).                                       00005550
           02 FILLER    PIC X(2).                                       00005560
           02 MNFACTA   PIC X.                                          00005570
           02 MNFACTC   PIC X.                                          00005580
           02 MNFACTP   PIC X.                                          00005590
           02 MNFACTH   PIC X.                                          00005600
           02 MNFACTV   PIC X.                                          00005610
           02 MNFACTO   PIC X(6).                                       00005620
           02 FILLER    PIC X(2).                                       00005630
           02 MCCREDA   PIC X.                                          00005640
           02 MCCREDC   PIC X.                                          00005650
           02 MCCREDP   PIC X.                                          00005660
           02 MCCREDH   PIC X.                                          00005670
           02 MCCREDV   PIC X.                                          00005680
           02 MCCREDO   PIC X(5).                                       00005690
           02 FILLER    PIC X(2).                                       00005700
           02 MTCREDA   PIC X.                                          00005710
           02 MTCREDC   PIC X.                                          00005720
           02 MTCREDP   PIC X.                                          00005730
           02 MTCREDH   PIC X.                                          00005740
           02 MTCREDV   PIC X.                                          00005750
           02 MTCREDO   PIC X(9).                                       00005760
           02 FILLER    PIC X(2).                                       00005770
           02 MZONCMDA  PIC X.                                          00005780
           02 MZONCMDC  PIC X.                                          00005790
           02 MZONCMDP  PIC X.                                          00005800
           02 MZONCMDH  PIC X.                                          00005810
           02 MZONCMDV  PIC X.                                          00005820
           02 MZONCMDO  PIC X(15).                                      00005830
           02 FILLER    PIC X(2).                                       00005840
           02 MLIBERRA  PIC X.                                          00005850
           02 MLIBERRC  PIC X.                                          00005860
           02 MLIBERRP  PIC X.                                          00005870
           02 MLIBERRH  PIC X.                                          00005880
           02 MLIBERRV  PIC X.                                          00005890
           02 MLIBERRO  PIC X(58).                                      00005900
           02 FILLER    PIC X(2).                                       00005910
           02 MCODTRAA  PIC X.                                          00005920
           02 MCODTRAC  PIC X.                                          00005930
           02 MCODTRAP  PIC X.                                          00005940
           02 MCODTRAH  PIC X.                                          00005950
           02 MCODTRAV  PIC X.                                          00005960
           02 MCODTRAO  PIC X(4).                                       00005970
           02 FILLER    PIC X(2).                                       00005980
           02 MCICSA    PIC X.                                          00005990
           02 MCICSC    PIC X.                                          00006000
           02 MCICSP    PIC X.                                          00006010
           02 MCICSH    PIC X.                                          00006020
           02 MCICSV    PIC X.                                          00006030
           02 MCICSO    PIC X(5).                                       00006040
           02 FILLER    PIC X(2).                                       00006050
           02 MNETNAMA  PIC X.                                          00006060
           02 MNETNAMC  PIC X.                                          00006070
           02 MNETNAMP  PIC X.                                          00006080
           02 MNETNAMH  PIC X.                                          00006090
           02 MNETNAMV  PIC X.                                          00006100
           02 MNETNAMO  PIC X(8).                                       00006110
           02 FILLER    PIC X(2).                                       00006120
           02 MSCREENA  PIC X.                                          00006130
           02 MSCREENC  PIC X.                                          00006140
           02 MSCREENP  PIC X.                                          00006150
           02 MSCREENH  PIC X.                                          00006160
           02 MSCREENV  PIC X.                                          00006170
           02 MSCREENO  PIC X(4).                                       00006180
                                                                                
