      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA VUE RVVE1100                                               
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA VUE RVVE1100                           
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVE1100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVE1100.                                                            
      *}                                                                        
           02  VE11-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  VE11-NLIEU                                                       
               PIC X(0003).                                                     
           02  VE11-NVENTE                                                      
               PIC X(0007).                                                     
           02  VE11-CTYPENREG                                                   
               PIC X(0001).                                                     
           02  VE11-NCODICGRP                                                   
               PIC X(0007).                                                     
           02  VE11-NCODIC                                                      
               PIC X(0007).                                                     
           02  VE11-NSEQ                                                        
               PIC X(0002).                                                     
           02  VE11-CMODDEL                                                     
               PIC X(0003).                                                     
           02  VE11-DDELIV                                                      
               PIC X(0008).                                                     
           02  VE11-NORDRE                                                      
               PIC X(0005).                                                     
           02  VE11-CENREG                                                      
               PIC X(0005).                                                     
           02  VE11-QVENDUE                                                     
               PIC S9(5) COMP-3.                                                
           02  VE11-PVUNIT                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE11-PVUNITF                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE11-PVTOTAL                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE11-CEQUIPE                                                     
               PIC X(0005).                                                     
           02  VE11-NLIGNE                                                      
               PIC X(0002).                                                     
           02  VE11-TAUXTVA                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  VE11-QCONDT                                                      
               PIC S9(5) COMP-3.                                                
           02  VE11-PRMP                                                        
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE11-WEMPORTE                                                    
               PIC X(0001).                                                     
           02  VE11-CPLAGE                                                      
               PIC X(0002).                                                     
           02  VE11-CPROTOUR                                                    
               PIC X(0005).                                                     
           02  VE11-CADRTOUR                                                    
               PIC X(0001).                                                     
           02  VE11-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  VE11-LCOMMENT                                                    
               PIC X(0035).                                                     
           02  VE11-CVENDEUR                                                    
               PIC X(0006).                                                     
           02  VE11-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  VE11-NDEPOT                                                      
               PIC X(0003).                                                     
           02  VE11-NAUTORM                                                     
               PIC X(0005).                                                     
           02  VE11-WARTINEX                                                    
               PIC X(0001).                                                     
           02  VE11-WEDITBL                                                     
               PIC X(0001).                                                     
           02  VE11-WACOMMUTER                                                  
               PIC X(0001).                                                     
           02  VE11-WCQERESF                                                    
               PIC X(0001).                                                     
           02  VE11-NMUTATION                                                   
               PIC X(0007).                                                     
           02  VE11-CTOURNEE                                                    
               PIC X(0008).                                                     
           02  VE11-WTOPELIVRE                                                  
               PIC X(0001).                                                     
           02  VE11-DCOMPTA                                                     
               PIC X(0008).                                                     
           02  VE11-DCREATION                                                   
               PIC X(0008).                                                     
           02  VE11-HCREATION                                                   
               PIC X(0004).                                                     
           02  VE11-DANNULATION                                                 
               PIC X(0008).                                                     
           02  VE11-NLIEUORIG                                                   
               PIC X(0003).                                                     
           02  VE11-WINTMAJ                                                     
               PIC X(0001).                                                     
           02  VE11-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  VE11-DSTAT                                                       
               PIC X(0004).                                                     
           02  VE11-CPRESTGRP                                                   
               PIC X(0005).                                                     
           02  VE11-NSOCMODIF                                                   
               PIC X(03).                                                       
           02  VE11-NLIEUMODIF                                                  
               PIC X(03).                                                       
           02  VE11-DATENC                                                      
               PIC X(08).                                                       
           02  VE11-CDEV                                                        
               PIC X(03).                                                       
           02  VE11-WUNITR                                                      
               PIC X(20).                                                       
           02  VE11-LRCMMT                                                      
               PIC X(10).                                                       
           02  VE11-NSOCP                                                       
               PIC X(03).                                                       
           02  VE11-NLIEUP                                                      
               PIC X(03).                                                       
           02  VE11-NTRANS                                                      
               PIC S9(8) COMP-3.                                                
           02  VE11-NSEQFV                                                      
               PIC X(02).                                                       
           02  VE11-NSEQNQ                                                      
               PIC S9(5) COMP-3.                                                
           02  VE11-NSEQREF                                                     
               PIC S9(5) COMP-3.                                                
           02  VE11-NMODIF                                                      
               PIC S9(5) COMP-3.                                                
           02  VE11-NSOCORIG                                                    
               PIC X(03).                                                       
           02  VE11-DTOPE                                                       
               PIC X(08).                                                       
           02  VE11-NSOCLIVR1                                                   
               PIC X(03).                                                       
           02  VE11-NDEPOT1                                                     
               PIC X(03).                                                       
           02  VE11-NSOCGEST                                                    
               PIC X(03).                                                       
           02  VE11-NLIEUGEST                                                   
               PIC X(03).                                                       
           02  VE11-NSOCDEPLIV                                                  
               PIC X(03).                                                       
           02  VE11-NLIEUDEPLIV                                                 
               PIC X(03).                                                       
           02  VE11-TYPVTE                                                      
               PIC X(01).                                                       
           02  VE11-CTYPENT                                                     
               PIC X(02).                                                       
           02  VE11-NLIEN                                                       
               PIC S9(5) COMP-3.                                                
           02  VE11-NACTVTE                                                     
               PIC S9(5) COMP-3.                                                
           02  VE11-PVCODIG                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE11-QTCODIG                                                     
               PIC S9(5) COMP-3.                                                
           02  VE11-DPRLG                                                       
               PIC X(08).                                                       
           02  VE11-CALERTE                                                     
               PIC X(05).                                                       
           02  VE11-CREPRISE                                                    
               PIC X(05).                                                       
           02  VE11-NSEQENS                                                     
               PIC S9(5) COMP-3.                                                
           02  VE11-MPRIMECLI                                                   
               PIC S9(7)V9(2) COMP-3.                                           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA VUE RVVE1100                                    
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVE1100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVE1100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-CTYPENREG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-CTYPENREG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NCODICGRP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NCODICGRP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-CMODDEL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-CMODDEL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-DDELIV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-CENREG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-CENREG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-QVENDUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-QVENDUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-PVUNIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-PVUNIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-PVUNITF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-PVUNITF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-PVTOTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-PVTOTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-CEQUIPE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-CEQUIPE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-TAUXTVA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-TAUXTVA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-QCONDT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-QCONDT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-PRMP-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-PRMP-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-WEMPORTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-WEMPORTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-CPLAGE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-CPLAGE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-CPROTOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-CPROTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-CADRTOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-CADRTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NAUTORM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NAUTORM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-WARTINEX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-WARTINEX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-WEDITBL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-WEDITBL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-WACOMMUTER-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-WACOMMUTER-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-WCQERESF-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-WCQERESF-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-CTOURNEE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-CTOURNEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-WTOPELIVRE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-WTOPELIVRE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-DCOMPTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-HCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-HCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-DANNULATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-DANNULATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NLIEUORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-WINTMAJ-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-WINTMAJ-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-DSTAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-DSTAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-CPRESTGRP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-CPRESTGRP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NSOCMODIF-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NSOCMODIF-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NLIEUMODIF-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NLIEUMODIF-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-DATENC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-DATENC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-CDEV-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-CDEV-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-WUNITR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-WUNITR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-LRCMMT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-LRCMMT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NSOCP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NSOCP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NLIEUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NLIEUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NTRANS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NSEQFV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NSEQFV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NSEQNQ-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NSEQNQ-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NSEQREF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NSEQREF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NMODIF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NMODIF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NSOCORIG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NSOCORIG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-DTOPE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-DTOPE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NSOCLIVR1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NSOCLIVR1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NDEPOT1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NDEPOT1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NSOCGEST-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NSOCGEST-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NLIEUGEST-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NLIEUGEST-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NSOCDEPLIV-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NSOCDEPLIV-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NLIEUDEPLIV-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NLIEUDEPLIV-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-TYPVTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-TYPVTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-CTYPENT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-CTYPENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NLIEN-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NLIEN-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NACTVTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NACTVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-PVCODIG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-PVCODIG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-QTCODIG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-QTCODIG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-DPRLG-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-DPRLG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-CALERTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-CALERTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-CREPRISE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-CREPRISE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-NSEQENS-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-NSEQENS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE11-MPRIMECLI-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE11-MPRIMECLI-F                                                 
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
