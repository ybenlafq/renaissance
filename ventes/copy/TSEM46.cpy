      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00010001
      * >>>>>>>>>>>>>>>>> DESCRIPTION DE LA TSEM46 <<<<<<<<<<<<<<<<<< * 00020001
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00030001
      *                                                               * 00040001
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00050001
B&S   *         MODIFICATIONS POUR PASSAGE A BIENS ET SERVICES          00060001
B&S   *  -> NTRANS PASSE DE X(4) � X(8)                                 00070001
B&S   *  -> NOPERATEUR PASSE DE X(4) � X(7)                             00080001
B&S   *  -> CMODPAIMT (1 � 6)  PASSE DE X(1) � X(5)                     00090001
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00100001
      *                                                               * 00110001
       01  TSEM46-IDENTIFICATEUR.                                       00120001
           05  TSEM46-TRANSID               PIC X(04) VALUE 'EM46'.     00130001
           05  TSEM46-NUMERO-DE-TERMINAL    PIC X(04) VALUE SPACES.     00140001
      *                                                               * 00150001
       01  TSEM46-ITEM.                                                 00160001
B&S   *    05  TSEM46-LONGUEUR              PIC S9(4) VALUE +2730.      00170001
B&S   * AJOUT SOC/LIEU DE VENTE 6 * 12 = + 72                           00180001
B&S        05  TSEM46-LONGUEUR              PIC S9(4) VALUE +2802.      00190001
           05  TSEM46-DATAS.                                            00200001
               10  TSEM46-LIGNE             OCCURS 12.                  00210001
      *      PREMIERE PARTIE = 037 OCTETS PAR OCCURENCE               * 00220000
                   15  TSEM46-CTYPE            PIC X(01).               00230001
                   15  TSEM46-NSOCIETE         PIC X(03).               00240001
                   15  TSEM46-NLIEU            PIC X(03).               00250001
      * SOC / LIEU DE VENTE                                             00260001
                   15  TSEM46-NSOCVENTE        PIC X(03).               00270001
                   15  TSEM46-NLIEUVENTE       PIC X(03).               00280001
      * DATE DE CAISSE                                                  00290001
                   15  TSEM46-DCAISSE.                                  00300001
                       25  TSEM46-DCAISSE-SSAA PIC X(04).               00310001
                       25  TSEM46-DCAISSE-MM   PIC X(02).               00320001
                       25  TSEM46-DCAISSE-JJ   PIC X(02).               00330001
      * DATE DE VENTE                                                   00340001
B&S                15  TSEM46-DVENTE.                                   00350001
B&S                    25  TSEM46-DVENTE-SSAA  PIC X(04).               00360001
B&S                    25  TSEM46-DVENTE-MM    PIC X(02).               00370001
B&S                    25  TSEM46-DVENTE-JJ    PIC X(02).               00380001
                   15  TSEM46-NCAISSE          PIC X(04).               00390001
B&S                15  TSEM46-NTRANS           PIC X(08).               00400001
                                                                        00410001
B&S   * CAISSE ET TRANSACTION DE CONTREPARTIE                           00420001
B&S                15  TSEM46-NCAISC           PIC X(04).               00430001
B&S                15  TSEM46-NTRCPT           PIC X(08).               00440001
                                                                        00450001
                   15  TSEM46-FLAG-DONNEES-ENTETE PIC X.                00460001
                       88 TSEM46-ACCES-EM51-PAS-EFFECTUE VALUE ' '.     00470001
                       88 TSEM46-ACCES-EM51-EFFECTUE     VALUE 'O'.     00480001
                   15  TSEM46-NTRANS-ANNULEE   PIC X(04).               00490001
                   15  TSEM46-ANNULEE          PIC X VALUE ' '.         00500001
                       88 TSEM46-TRANSAC-RIEN        VALUE ' '.         00510001
                       88 TSEM46-TRANSAC-ANNULEE     VALUE '1'.         00520001
                       88 TSEM46-TRANSAC-ANNULANTE   VALUE '2'.         00530001
B&S                15  TSEM46-TTRANS           PIC X(03).               00540001
B&S                15  TSEM46-NTYPVENTE        PIC X(03).               00550001
B&S                15  TSEM46-APVF             PIC X(01).               00560001
B&S                15  TSEM46-CODE-APVF        PIC X(04).               00570001
B&S                15  TSEM46-CODE-TRANSACTION PIC 9(01).               00580001
B&S                    88 TSEM46-TRANS-DECOMPTE     VALUE 1.            00590001
B&S                    88 TSEM46-TRANS-APP-RETRAIT  VALUE 2.            00600001
B&S                    88 TSEM46-AUTRE-TRANSACTION  VALUE 3.            00610001
                   15  TSEM46-NVENTE           PIC X(07).               00620001
B&S                15  TSEM46-NEMPORT          PIC X(08).               00630001
                   15  TSEM46-CNATVENTE        PIC X(03).               00640001
                   15  TSEM46-NFACT            PIC X(06).               00650001
B&S                15  TSEM46-NOPERATEUR       PIC X(07).               00660001
B&S                15  TSEM46-NVENDEUR         PIC X(07).               00670001
B&S   * HEURE DE PAIEMENT                                               00680001
                   15  TSEM46-DHMPAIMT.                                 00690001
                       25  TSEM46-DHPAIMT      PIC X(02).               00700001
                       25  TSEM46-DMPAIMT      PIC X(02).               00710001
B&S   * HEURE DE VENTE                                                  00720001
                   15  TSEM46-DHMVENTE.                                 00730001
                       25  TSEM46-DHVENTE      PIC X(02).               00740001
                       25  TSEM46-DMVENTE      PIC X(02).               00750001
                   15  TSEM46-PTTVENTE         PIC S9(7)V99 COMP-3.     00760001
B&S                15  TSEM46-PTTTRANS         PIC S9(7)V99 COMP-3.     00770001
                   15  TSEM46-PDEDUCT          PIC S9(7)V99 COMP-3.     00780001
                   15  TSEM46-PVERSE           PIC S9(7)V99 COMP-3.     00790001
                   15  TSEM46-PCOMPT           PIC S9(7)V99 COMP-3.     00800001
                   15  TSEM46-PLIVR            PIC S9(7)V99 COMP-3.     00810001
                   15  TSEM46-PDIFFERE         PIC S9(7)V99 COMP-3.     00820001
                   15  TSEM46-PRFACT           PIC S9(7)V99 COMP-3.     00830001
                   15  TSEM46-CORGORED         PIC X(05).               00840001
B&S                15  TSEM46-CMODPAIMT1       PIC X(5).                00850001
B&S                15  TSEM46-CMODPAIMT2       PIC X(5).                00860001
B&S                15  TSEM46-CMODPAIMT3       PIC X(5).                00870001
B&S                15  TSEM46-CMODPAIMT4       PIC X(5).                00880001
B&S                15  TSEM46-CMODPAIMT5       PIC X(5).                00890001
B&S                15  TSEM46-CMODPAIMT6       PIC X(5).                00900001
JD                 15  TSEM46-DMODPAIMT1       PIC X.                   00910001
JD                 15  TSEM46-DMODPAIMT2       PIC X.                   00920001
JD                 15  TSEM46-DMODPAIMT3       PIC X.                   00930001
JD                 15  TSEM46-DMODPAIMT4       PIC X.                   00940001
JD                 15  TSEM46-DMODPAIMT5       PIC X.                   00950001
JD                 15  TSEM46-DMODPAIMT6       PIC X.                   00960001
JD                 15  TSEM46-DEVENT           PIC X.                   00970001
JD                 15  TSEM46-DEVCRED          PIC X.                   00980001
                   15  TSEM46-CAPRET           PIC X.                   00990001
JD                 15  TSEM46-PREGLEM          PIC S9(7)V99.            01000001
JD                 15  TSEM46-DEVREGL          PIC X.                   01010001
B&S                15  TSEM46-NLIGNE           PIC X(03).               01020001
B&S                15  TSEM46-NOLIGNE          PIC 9(3).                01030001
B&S                15  TSEM46-NENVLP           PIC X(10).               01040001
B&S                15  TSEM46-PMTENV           PIC S9(7)V99 COMP-3.     01050001
B&S            10  TSEM46-TOP-PF9              PIC 9(01).               01060001
B&S   *   SI TSEM46-CODE-ECRAN = 0 ==> ECRAN RENSEIGNE                  01070001
B&S   *   SI TSEM46-CODE-ECRAN = 9 ==> ECRAN NON RENSEIGNE              01080001
B&S            10  TSEM46-CODE-ECRAN           PIC 9(01).               01090001
      *                                                                 01100001
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 01110001
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< * 01120001
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 01130001
                                                                                
