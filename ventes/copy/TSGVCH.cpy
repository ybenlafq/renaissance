      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * --------------------------------------------------------------  00010001
      * PORTAIL UNIQUE : BON D'�CHANGE SUR LIGNES DE VENTE.             00020001
      * --------------------------------------------------------------- 00040001
       01 TSGVCH-DONNEES.                                               00050001
          05 WNB-TSGVCH                     PIC 99.                     00051001
          05 FILLER                         PIC X(20).                  00060000
          05 TSGVCH-LIGNES-VENTE            OCCURS 80.                  00070001
             10 TSGVCH-NCODIC               PIC X(07).                  00080000
             10 TSGVCH-NSEQNQ               PIC S9(5) COMP-3.           00090000
             10 TSGVCH-NUECH                PIC S9(9)V USAGE COMP-3.    00100000
             10 TSGVCH-NNCODIC              PIC X(07).                  00110001
             10 TSGVCH-NNSEQNQ              PIC S9(5) COMP-3.           00111001
             10 TSGVCH-MODREP               PIC X(01).                  00120001
             10 TSGVCH-1ER-PASSAGE          PIC X(01).                  00121001
             10 FILLER                      PIC X(08).                  00130001
                                                                                
