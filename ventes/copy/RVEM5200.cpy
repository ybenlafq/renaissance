      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVEM5200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEM5200                         
      *   CLE UNIQUE : NSOCIETE A NLIGNE LG = 161                               
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEM5200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEM5200.                                                            
      *}                                                                        
1          02  EM52-NSOCIETE                                                    
               PIC X(0003).                                                     
4          02  EM52-NLIEU                                                       
               PIC X(0003).                                                     
7          02  EM52-DCAISSE                                                     
               PIC X(0008).                                                     
15         02  EM52-NCAISSE                                                     
               PIC X(0003).                                                     
18         02  EM52-NTRANS                                                      
               PIC X(0008).                                                     
26         02  EM52-NTYPVENTE                                                   
               PIC X(0003).                                                     
29         02  EM52-NVENTE                                                      
               PIC X(0007).                                                     
36         02  EM52-NLIGNE                                                      
               PIC X(0003).                                                     
39         02  EM52-CNATVENTE                                                   
               PIC X(0003).                                                     
42         02  EM52-NEMPORT                                                     
               PIC X(0008).                                                     
50         02  EM52-NCODIC                                                      
               PIC X(0007).                                                     
57         02  EM52-NCODICGRP                                                   
               PIC X(0007).                                                     
64         02  EM52-LREF                                                        
               PIC X(0020).                                                     
84         02  EM52-CPSE                                                        
               PIC X(0007).                                                     
91         02  EM52-QVENDUE                                                     
               PIC S9(5) COMP-3.                                                
94         02  EM52-PVUNIT                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
99         02  EM52-PVUNITF                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
104        02  EM52-PVTOTAL                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
109        02  EM52-QVENDUEGRP                                                  
               PIC S9(5) COMP-3.                                                
112        02  EM52-PVUNITGRP                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
117        02  EM52-MPRIMECLI                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
122        02  EM52-PVTOTALGRP                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
127        02  EM52-CFORCE                                                      
               PIC X(0001).                                                     
128        02  EM52-CODEREM                                                     
               PIC X(0005).                                                     
133        02  EM52-TAUXREM                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
136        02  EM52-PRABAIS                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
141        02  EM52-PPRIME                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
146        02  EM52-TAUXTVA                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
149        02  EM52-CVENDEUR                                                    
               PIC X(0007).                                                     
156        02  EM52-NSOCM                                                       
               PIC X(0003).                                                     
159        02  EM52-NLIEUM                                                      
               PIC X(0003).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVEM5201                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEM5201-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEM5201-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-DCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-DCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-NCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-NCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-NTRANS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-NTYPVENTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-NTYPVENTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-CNATVENTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-CNATVENTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-NCODICGRP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-NCODICGRP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-LREF-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-LREF-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-CPSE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-CPSE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-QVENDUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-QVENDUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-PVUNIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-PVUNIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-PVUNITF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-PVUNITF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-PVTOTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-PVTOTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-QVENDUEGRP-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-QVENDUEGRP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-PVUNITGRP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-PVUNITGRP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-MPRIMECLI-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-MPRIMECLI-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-PVTOTALGRP-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-PVTOTALGRP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-CFORCE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-CFORCE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-CODEREM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-CODEREM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-TAUXREM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-TAUXREM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-PRABAIS-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-PRABAIS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-PPRIME-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-PPRIME-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-TAUXTVA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-TAUXTVA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-NSOCM-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-NSOCM-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM52-NLIEUM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM52-NLIEUM-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
