      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT ISM091 AU 22/06/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,05,BI,A,                          *        
      *                           18,05,BI,A,                          *        
      *                           23,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-ISM091.                                                        
            05 NOMETAT-ISM091           PIC X(6) VALUE 'ISM091'.                
            05 RUPTURES-ISM091.                                                 
           10 ISM091-NSOCDEPOT          PIC X(03).                      007  003
           10 ISM091-NDEPOT             PIC X(03).                      010  003
           10 ISM091-WSEQFAM            PIC 9(05).                      013  005
           10 ISM091-CFAM               PIC X(05).                      018  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ISM091-SEQUENCE           PIC S9(04) COMP.                023  002
      *--                                                                       
           10 ISM091-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-ISM091.                                                   
           10 ISM091-CMARQ              PIC X(05).                      025  005
           10 ISM091-LLIEU1             PIC X(20).                      030  020
           10 ISM091-LLIEU2             PIC X(20).                      050  020
           10 ISM091-LREFFOURN          PIC X(20).                      070  020
           10 ISM091-NCODIC             PIC X(07).                      090  007
           10 ISM091-PRMP1              PIC S9(09)V9(2) COMP-3.         097  006
           10 ISM091-PRMP2              PIC S9(09)V9(2) COMP-3.         103  006
           10 ISM091-QSTOCK             PIC S9(05)      COMP-3.         109  003
           10 ISM091-QSTOCKDISPO        PIC S9(03)V9(2) COMP-3.         112  003
           10 ISM091-QSTOCKRES          PIC S9(03)V9(2) COMP-3.         115  003
            05 FILLER                      PIC X(395).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-ISM091-LONG           PIC S9(4)   COMP  VALUE +117.           
      *                                                                         
      *--                                                                       
        01  DSECT-ISM091-LONG           PIC S9(4) COMP-5  VALUE +117.           
                                                                                
      *}                                                                        
