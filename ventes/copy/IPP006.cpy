      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPP006 AU 09/10/1996  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,03,BI,A,                          *        
      *                           15,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPP006.                                                        
            05 NOMETAT-IPP006           PIC X(6) VALUE 'IPP006'.                
            05 RUPTURES-IPP006.                                                 
           10 IPP006-NSOCIETE           PIC X(03).                      007  003
           10 IPP006-CGRPMAG            PIC X(02).                      010  002
           10 IPP006-NLIEU              PIC X(03).                      012  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPP006-SEQUENCE           PIC S9(04) COMP.                015  002
      *--                                                                       
           10 IPP006-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPP006.                                                   
           10 IPP006-LPRIME             PIC X(30).                      017  030
           10 IPP006-PENVELOPPE-A1      PIC S9(05)V9(2) COMP-3.         047  004
           10 IPP006-PENVELOPPE-M       PIC S9(05)V9(2) COMP-3.         051  004
           10 IPP006-PENVELOPPE-M1      PIC S9(05)V9(2) COMP-3.         055  004
           10 IPP006-DDEVERSIGA         PIC X(08).                      059  008
           10 IPP006-DMOISPAYE-A1       PIC X(08).                      067  008
           10 IPP006-DMOISPAYE-M        PIC X(08).                      075  008
           10 IPP006-DMOISPAYE-M1       PIC X(08).                      083  008
            05 FILLER                      PIC X(422).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPP006-LONG           PIC S9(4)   COMP  VALUE +090.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPP006-LONG           PIC S9(4) COMP-5  VALUE +090.           
                                                                                
      *}                                                                        
