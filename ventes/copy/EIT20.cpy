      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EIT20   EIT20                                              00000020
      ***************************************************************** 00000030
       01   EIT20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(8).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNINVENTAIREL  COMP PIC S9(4).                            00000160
      *--                                                                       
           02 MNINVENTAIREL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNINVENTAIREF  PIC X.                                     00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MNINVENTAIREI  PIC X(5).                                  00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNSOCI    PIC X(3).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MLSOCI    PIC X(20).                                      00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MNLIEUI   PIC X(3).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLLIEUI   PIC X(20).                                      00000350
           02 M47I OCCURS   5 TIMES .                                   00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 CODICL  COMP PIC S9(4).                                 00000370
      *--                                                                       
             03 CODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 CODICF  PIC X.                                          00000380
             03 FILLER  PIC X(4).                                       00000390
             03 CODICI  PIC X(7).                                       00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 QTEEL   COMP PIC S9(4).                                 00000410
      *--                                                                       
             03 QTEEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 QTEEF   PIC X.                                          00000420
             03 FILLER  PIC X(4).                                       00000430
             03 QTEEI   PIC X(5).                                       00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 QTERL   COMP PIC S9(4).                                 00000450
      *--                                                                       
             03 QTERL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 QTERF   PIC X.                                          00000460
             03 FILLER  PIC X(4).                                       00000470
             03 QTERI   PIC X(5).                                       00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 QTECL   COMP PIC S9(4).                                 00000490
      *--                                                                       
             03 QTECL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 QTECF   PIC X.                                          00000500
             03 FILLER  PIC X(4).                                       00000510
             03 QTECI   PIC X(5).                                       00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 QTEHL   COMP PIC S9(4).                                 00000530
      *--                                                                       
             03 QTEHL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 QTEHF   PIC X.                                          00000540
             03 FILLER  PIC X(4).                                       00000550
             03 QTEHI   PIC X(5).                                       00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 QTEPL   COMP PIC S9(4).                                 00000570
      *--                                                                       
             03 QTEPL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 QTEPF   PIC X.                                          00000580
             03 FILLER  PIC X(4).                                       00000590
             03 QTEPI   PIC X(5).                                       00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MARL    COMP PIC S9(4).                                 00000610
      *--                                                                       
             03 MARL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MARF    PIC X.                                          00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MARI    PIC X(5).                                       00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 FAML    COMP PIC S9(4).                                 00000650
      *--                                                                       
             03 FAML COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 FAMF    PIC X.                                          00000660
             03 FILLER  PIC X(4).                                       00000670
             03 FAMI    PIC X(5).                                       00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSGL    COMP PIC S9(4).                                 00000690
      *--                                                                       
             03 MSGL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MSGF    PIC X.                                          00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MSGI    PIC X(42).                                      00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 REFL    COMP PIC S9(4).                                 00000730
      *--                                                                       
             03 REFL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 REFF    PIC X.                                          00000740
             03 FILLER  PIC X(4).                                       00000750
             03 REFI    PIC X(20).                                      00000760
      * MESSAGE ERREUR                                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLIBERRI  PIC X(78).                                      00000810
      * CODE TRANSACTION                                                00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      * CICS DE TRAVAIL                                                 00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MCICSI    PIC X(5).                                       00000910
      * NETNAME                                                         00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000930
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000940
           02 FILLER    PIC X(4).                                       00000950
           02 MNETNAMI  PIC X(8).                                       00000960
      * CODE TERMINAL                                                   00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MSCREENI  PIC X(5).                                       00001010
      ***************************************************************** 00001020
      * SDF: EIT20   EIT20                                              00001030
      ***************************************************************** 00001040
       01   EIT20O REDEFINES EIT20I.                                    00001050
           02 FILLER    PIC X(12).                                      00001060
      * DATE DU JOUR                                                    00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MDATJOUA  PIC X.                                          00001090
           02 MDATJOUC  PIC X.                                          00001100
           02 MDATJOUP  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUV  PIC X.                                          00001130
           02 MDATJOUO  PIC X(10).                                      00001140
      * HEURE                                                           00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MTIMJOUA  PIC X.                                          00001170
           02 MTIMJOUC  PIC X.                                          00001180
           02 MTIMJOUP  PIC X.                                          00001190
           02 MTIMJOUH  PIC X.                                          00001200
           02 MTIMJOUV  PIC X.                                          00001210
           02 MTIMJOUO  PIC X(8).                                       00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MNINVENTAIREA  PIC X.                                     00001240
           02 MNINVENTAIREC  PIC X.                                     00001250
           02 MNINVENTAIREP  PIC X.                                     00001260
           02 MNINVENTAIREH  PIC X.                                     00001270
           02 MNINVENTAIREV  PIC X.                                     00001280
           02 MNINVENTAIREO  PIC X(5).                                  00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MNSOCA    PIC X.                                          00001310
           02 MNSOCC    PIC X.                                          00001320
           02 MNSOCP    PIC X.                                          00001330
           02 MNSOCH    PIC X.                                          00001340
           02 MNSOCV    PIC X.                                          00001350
           02 MNSOCO    PIC X(3).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MLSOCA    PIC X.                                          00001380
           02 MLSOCC    PIC X.                                          00001390
           02 MLSOCP    PIC X.                                          00001400
           02 MLSOCH    PIC X.                                          00001410
           02 MLSOCV    PIC X.                                          00001420
           02 MLSOCO    PIC X(20).                                      00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MNLIEUA   PIC X.                                          00001450
           02 MNLIEUC   PIC X.                                          00001460
           02 MNLIEUP   PIC X.                                          00001470
           02 MNLIEUH   PIC X.                                          00001480
           02 MNLIEUV   PIC X.                                          00001490
           02 MNLIEUO   PIC X(3).                                       00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MLLIEUA   PIC X.                                          00001520
           02 MLLIEUC   PIC X.                                          00001530
           02 MLLIEUP   PIC X.                                          00001540
           02 MLLIEUH   PIC X.                                          00001550
           02 MLLIEUV   PIC X.                                          00001560
           02 MLLIEUO   PIC X(20).                                      00001570
           02 M47O OCCURS   5 TIMES .                                   00001580
             03 FILLER       PIC X(2).                                  00001590
             03 CODICA  PIC X.                                          00001600
             03 CODICC  PIC X.                                          00001610
             03 CODICP  PIC X.                                          00001620
             03 CODICH  PIC X.                                          00001630
             03 CODICV  PIC X.                                          00001640
             03 CODICO  PIC X(7).                                       00001650
             03 FILLER       PIC X(2).                                  00001660
             03 QTEEA   PIC X.                                          00001670
             03 QTEEC   PIC X.                                          00001680
             03 QTEEP   PIC X.                                          00001690
             03 QTEEH   PIC X.                                          00001700
             03 QTEEV   PIC X.                                          00001710
             03 QTEEO   PIC ZZZZ9.                                      00001720
             03 FILLER       PIC X(2).                                  00001730
             03 QTERA   PIC X.                                          00001740
             03 QTERC   PIC X.                                          00001750
             03 QTERP   PIC X.                                          00001760
             03 QTERH   PIC X.                                          00001770
             03 QTERV   PIC X.                                          00001780
             03 QTERO   PIC ZZZZ9.                                      00001790
             03 FILLER       PIC X(2).                                  00001800
             03 QTECA   PIC X.                                          00001810
             03 QTECC   PIC X.                                          00001820
             03 QTECP   PIC X.                                          00001830
             03 QTECH   PIC X.                                          00001840
             03 QTECV   PIC X.                                          00001850
             03 QTECO   PIC ZZZZ9.                                      00001860
             03 FILLER       PIC X(2).                                  00001870
             03 QTEHA   PIC X.                                          00001880
             03 QTEHC   PIC X.                                          00001890
             03 QTEHP   PIC X.                                          00001900
             03 QTEHH   PIC X.                                          00001910
             03 QTEHV   PIC X.                                          00001920
             03 QTEHO   PIC ZZZZ9.                                      00001930
             03 FILLER       PIC X(2).                                  00001940
             03 QTEPA   PIC X.                                          00001950
             03 QTEPC   PIC X.                                          00001960
             03 QTEPP   PIC X.                                          00001970
             03 QTEPH   PIC X.                                          00001980
             03 QTEPV   PIC X.                                          00001990
             03 QTEPO   PIC ZZZZ9.                                      00002000
             03 FILLER       PIC X(2).                                  00002010
             03 MARA    PIC X.                                          00002020
             03 MARC    PIC X.                                          00002030
             03 MARP    PIC X.                                          00002040
             03 MARH    PIC X.                                          00002050
             03 MARV    PIC X.                                          00002060
             03 MARO    PIC X(5).                                       00002070
             03 FILLER       PIC X(2).                                  00002080
             03 FAMA    PIC X.                                          00002090
             03 FAMC    PIC X.                                          00002100
             03 FAMP    PIC X.                                          00002110
             03 FAMH    PIC X.                                          00002120
             03 FAMV    PIC X.                                          00002130
             03 FAMO    PIC X(5).                                       00002140
             03 FILLER       PIC X(2).                                  00002150
             03 MSGA    PIC X.                                          00002160
             03 MSGC    PIC X.                                          00002170
             03 MSGP    PIC X.                                          00002180
             03 MSGH    PIC X.                                          00002190
             03 MSGV    PIC X.                                          00002200
             03 MSGO    PIC X(42).                                      00002210
             03 FILLER       PIC X(2).                                  00002220
             03 REFA    PIC X.                                          00002230
             03 REFC    PIC X.                                          00002240
             03 REFP    PIC X.                                          00002250
             03 REFH    PIC X.                                          00002260
             03 REFV    PIC X.                                          00002270
             03 REFO    PIC X(20).                                      00002280
      * MESSAGE ERREUR                                                  00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MLIBERRA  PIC X.                                          00002310
           02 MLIBERRC  PIC X.                                          00002320
           02 MLIBERRP  PIC X.                                          00002330
           02 MLIBERRH  PIC X.                                          00002340
           02 MLIBERRV  PIC X.                                          00002350
           02 MLIBERRO  PIC X(78).                                      00002360
      * CODE TRANSACTION                                                00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCODTRAA  PIC X.                                          00002390
           02 MCODTRAC  PIC X.                                          00002400
           02 MCODTRAP  PIC X.                                          00002410
           02 MCODTRAH  PIC X.                                          00002420
           02 MCODTRAV  PIC X.                                          00002430
           02 MCODTRAO  PIC X(4).                                       00002440
      * CICS DE TRAVAIL                                                 00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MCICSA    PIC X.                                          00002470
           02 MCICSC    PIC X.                                          00002480
           02 MCICSP    PIC X.                                          00002490
           02 MCICSH    PIC X.                                          00002500
           02 MCICSV    PIC X.                                          00002510
           02 MCICSO    PIC X(5).                                       00002520
      * NETNAME                                                         00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MNETNAMA  PIC X.                                          00002550
           02 MNETNAMC  PIC X.                                          00002560
           02 MNETNAMP  PIC X.                                          00002570
           02 MNETNAMH  PIC X.                                          00002580
           02 MNETNAMV  PIC X.                                          00002590
           02 MNETNAMO  PIC X(8).                                       00002600
      * CODE TERMINAL                                                   00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MSCREENA  PIC X.                                          00002630
           02 MSCREENC  PIC X.                                          00002640
           02 MSCREENP  PIC X.                                          00002650
           02 MSCREENH  PIC X.                                          00002660
           02 MSCREENV  PIC X.                                          00002670
           02 MSCREENO  PIC X(5).                                       00002680
                                                                                
