      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE      RVEC0303                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVEC0303.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVEC0303.                                                             
      *}                                                                        
           10 EC03-NSOCIETE        PIC  X(03).                                  
           10 EC03-NLIEU           PIC  X(03).                                  
           10 EC03-NVENTE          PIC  X(07).                                  
           10 EC03-DVENTE          PIC  X(08).                                  
           10 EC03-NSEQNQ          PIC S9(05)V USAGE COMP-3.                    
           10 EC03-CCOLIS          PIC  X(26).                                  
           10 EC03-DEXP            PIC  X(08).                                  
           10 EC03-DSYST           PIC S9(13)V USAGE COMP-3.                    
           10 EC03-CCDDEL          PIC  X(08).                                  
           10 EC03-CCHDEL          PIC  X(04).                                  
           10 EC03-DDELIV          PIC  X(08).                                  
           10 EC03-WEMPORTE        PIC  X(01).                                  
           10 EC03-NRELAISID       PIC  X(10).                                  
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *-- FLAG                                                                  
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVEC0303-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVEC0303-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 EC03-NSOCIETE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 EC03-NSOCIETE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 EC03-NLIEU-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 EC03-NLIEU-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 EC03-NVENTE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 EC03-NVENTE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 EC03-DVENTE-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 EC03-DVENTE-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 EC03-NSEQNQ-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 EC03-NSEQNQ-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 EC03-CCOLIS-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 EC03-CCOLIS-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 EC03-DEXP-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 EC03-DEXP-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 EC03-DSYST-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 EC03-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 EC03-CCDDEL-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 EC03-CCDDEL-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 EC03-CCHDEL-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 EC03-CCHDEL-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 EC03-DDELIV-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 EC03-DDELIV-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 EC03-WEMPORTE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 EC03-WEMPORTE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 EC03-NRELAISID-F     PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 EC03-NRELAISID-F     PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
