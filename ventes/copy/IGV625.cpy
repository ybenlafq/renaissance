      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGV625 AU 15/07/1994  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,PD,A,                          *        
      *                           16,07,BI,A,                          *        
      *                           23,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGV625.                                                        
            05 NOMETAT-IGV625           PIC X(6) VALUE 'IGV625'.                
            05 RUPTURES-IGV625.                                                 
           10 IGV625-NSOCIETE           PIC X(03).                      007  003
           10 IGV625-NLIEU              PIC X(03).                      010  003
           10 IGV625-QECART             PIC S9(03)V9(2) COMP-3.         013  003
           10 IGV625-NVENTE             PIC X(07).                      016  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGV625-SEQUENCE           PIC S9(04) COMP.                023  002
      *--                                                                       
           10 IGV625-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGV625.                                                   
           10 IGV625-LCODIC             PIC X(28).                      025  028
           10 IGV625-LVENTE             PIC X(07).                      053  007
           10 IGV625-NCODIC             PIC X(07).                      060  007
           10 IGV625-NETNAME            PIC X(08).                      067  008
           10 IGV625-NLIEUORIG          PIC X(03).                      075  003
           10 IGV625-NMAG               PIC X(03).                      078  003
           10 IGV625-PECARTNEG          PIC S9(07)V9(2) COMP-3.         081  005
           10 IGV625-PECARTPOS          PIC S9(07)V9(2) COMP-3.         086  005
           10 IGV625-PVFINAL            PIC S9(07)V9(2) COMP-3.         091  005
           10 IGV625-PVINIT             PIC S9(07)V9(2) COMP-3.         096  005
           10 IGV625-PVTHEOR            PIC S9(07)V9(2) COMP-3.         101  005
           10 IGV625-QECARTNEG          PIC S9(03)V9(2) COMP-3.         106  003
           10 IGV625-QECARTPOS          PIC S9(03)V9(2) COMP-3.         109  003
           10 IGV625-QVENDUEF           PIC S9(05)      COMP-3.         112  003
           10 IGV625-QVENDUEI           PIC S9(05)      COMP-3.         115  003
            05 FILLER                      PIC X(395).                          
                                                                                
