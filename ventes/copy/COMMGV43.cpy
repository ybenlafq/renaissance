      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  TRANSACTION: GV00                                             *        
      *  TITRE      : COMMAREA DE L'APPLICATION DGL  APELLEE PAR TGV01 *        
      *  LONGUEUR   : 700                                              *        
      *                                                                *        
      ******************************************************************        
      *                                                                 00530000
      *                                                                 00230000
       01  COMM-MGV43-APPLI.                                            00260000
         02 COMM-MGV43-ENTREE.                                          00260000
      * IDENTIFIANT VENTE                                                       
           05 COMM-MGV43-NSOCIETE       PIC X(03).                      00320000
           05 COMM-MGV43-NLIEU          PIC X(03).                      00330000
           05 COMM-MGV43-NVENTE         PIC X(07).                      00340000
      * LIEU DE MODIFICATION                                                    
           05 COMM-MGV43-NSOCMODIF      PIC X(03).                      00320000
           05 COMM-MGV43-NLIEUMODIF     PIC X(03).                      00330000
      * LIEU DE PAIEMENT                                                        
           05 COMM-MGV43-NSOCP          PIC X(03).                      00320000
           05 COMM-MGV43-NLIEUP         PIC X(03).                      00330000
           05 COMM-MGV43-CIMPRIM        PIC X(10).                      00330000
         02 COMM-MGV43-SORTIE.                                          00260000
           05 COMM-MGV43-CODE-RETOUR    PIC X(01).                      00340000
           05 COMM-MGV43-MESSAGE.                                               
                10 COMM-MGV43-CODRET         PIC X.                             
                   88  COMM-MGV43-OK         VALUE ' '.                         
                   88  COMM-MGV43-ERR-BLQ    VALUE '1'.                         
                   88  COMM-MGV43-ERR-STO    VALUE '2'.                         
                   88  COMM-MGV43-ERR-DELAI  VALUE '3'.                         
                10 COMM-MGV43-LIBERR         PIC X(58).                         
           05 COMM-MGV43-PBSTOCK OCCURS 40.                                     
              10 COMM-MGV43-NCODIC         PIC X(07).                   00340000
              10 COMM-MGV43-QSTOCK         PIC 9(05).                   00340000
              10 COMM-MGV43-NLIGNE         PIC X(02).                   00340000
           05 FILLER                    PIC X(111).                     00340000
                                                                                
