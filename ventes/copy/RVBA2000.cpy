      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVBA2000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBA2000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVBA2000.                                                            
           02  BA20-CTIERSBA                                                    
               PIC X(0006).                                                     
           02  BA20-DMOIS                                                       
               PIC X(0006).                                                     
           02  BA20-NTIERSCV                                                    
               PIC X(0006).                                                     
           02  BA20-PBA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  BA20-QBA                                                         
               PIC S9(5) COMP-3.                                                
           02  BA20-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVBA2000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVBA2000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA20-CTIERSBA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA20-CTIERSBA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA20-DMOIS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA20-DMOIS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA20-NTIERSCV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA20-NTIERSCV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA20-PBA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA20-PBA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA20-QBA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA20-QBA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA20-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA20-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
