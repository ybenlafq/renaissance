      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAV10   EAV10                                              00000020
      ***************************************************************** 00000030
       01   EAV10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      * n�de societe                                                    00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCSAISIEL   COMP PIC S9(4).                            00000150
      *--                                                                       
           02 MNSOCSAISIEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNSOCSAISIEF   PIC X.                                     00000160
           02 FILLER    PIC X(4).                                       00000170
           02 MNSOCSAISIEI   PIC X(3).                                  00000180
      * n�du lieu                                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUSAISIEL  COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MNLIEUSAISIEL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNLIEUSAISIEF  PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNLIEUSAISIEI  PIC X(3).                                  00000230
      * llibelle lieu                                                   00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUSAISIEL  COMP PIC S9(4).                            00000250
      *--                                                                       
           02 MLLIEUSAISIEL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MLLIEUSAISIEF  PIC X.                                     00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MLLIEUSAISIEI  PIC X(20).                                 00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSECTIONL     COMP PIC S9(4).                            00000290
      *--                                                                       
           02 MNSECTIONL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSECTIONF     PIC X.                                     00000300
           02 FILLER    PIC X(4).                                       00000310
           02 MNSECTIONI     PIC X(6).                                  00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLEL      COMP PIC S9(4).                            00000330
      *--                                                                       
           02 MLIBELLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBELLEF      PIC X.                                     00000340
           02 FILLER    PIC X(4).                                       00000350
           02 MLIBELLEI      PIC X(20).                                 00000360
      * zone de commande                                                00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MZONCMDI  PIC X(15).                                      00000410
      * date d'emmission de l'avoir                                     00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEMISL   COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MDEMISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEMISF   PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MDEMISI   PIC X(10).                                      00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MLIBERRI  PIC X(78).                                      00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MCODTRAI  PIC X(4).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MCICSI    PIC X(5).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MNETNAMI  PIC X(8).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MSCREENI  PIC X(4).                                       00000660
      ***************************************************************** 00000670
      * SDF: EAV10   EAV10                                              00000680
      ***************************************************************** 00000690
       01   EAV10O REDEFINES EAV10I.                                    00000700
           02 FILLER    PIC X(12).                                      00000710
           02 FILLER    PIC X(2).                                       00000720
           02 MDATJOUA  PIC X.                                          00000730
           02 MDATJOUC  PIC X.                                          00000740
           02 MDATJOUP  PIC X.                                          00000750
           02 MDATJOUH  PIC X.                                          00000760
           02 MDATJOUV  PIC X.                                          00000770
           02 MDATJOUO  PIC X(10).                                      00000780
           02 FILLER    PIC X(2).                                       00000790
           02 MTIMJOUA  PIC X.                                          00000800
           02 MTIMJOUC  PIC X.                                          00000810
           02 MTIMJOUP  PIC X.                                          00000820
           02 MTIMJOUH  PIC X.                                          00000830
           02 MTIMJOUV  PIC X.                                          00000840
           02 MTIMJOUO  PIC X(5).                                       00000850
      * n�de societe                                                    00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MNSOCSAISIEA   PIC X.                                     00000880
           02 MNSOCSAISIEC   PIC X.                                     00000890
           02 MNSOCSAISIEP   PIC X.                                     00000900
           02 MNSOCSAISIEH   PIC X.                                     00000910
           02 MNSOCSAISIEV   PIC X.                                     00000920
           02 MNSOCSAISIEO   PIC X(3).                                  00000930
      * n�du lieu                                                       00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MNLIEUSAISIEA  PIC X.                                     00000960
           02 MNLIEUSAISIEC  PIC X.                                     00000970
           02 MNLIEUSAISIEP  PIC X.                                     00000980
           02 MNLIEUSAISIEH  PIC X.                                     00000990
           02 MNLIEUSAISIEV  PIC X.                                     00001000
           02 MNLIEUSAISIEO  PIC X(3).                                  00001010
      * llibelle lieu                                                   00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MLLIEUSAISIEA  PIC X.                                     00001040
           02 MLLIEUSAISIEC  PIC X.                                     00001050
           02 MLLIEUSAISIEP  PIC X.                                     00001060
           02 MLLIEUSAISIEH  PIC X.                                     00001070
           02 MLLIEUSAISIEV  PIC X.                                     00001080
           02 MLLIEUSAISIEO  PIC X(20).                                 00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MNSECTIONA     PIC X.                                     00001110
           02 MNSECTIONC     PIC X.                                     00001120
           02 MNSECTIONP     PIC X.                                     00001130
           02 MNSECTIONH     PIC X.                                     00001140
           02 MNSECTIONV     PIC X.                                     00001150
           02 MNSECTIONO     PIC X(6).                                  00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MLIBELLEA      PIC X.                                     00001180
           02 MLIBELLEC PIC X.                                          00001190
           02 MLIBELLEP PIC X.                                          00001200
           02 MLIBELLEH PIC X.                                          00001210
           02 MLIBELLEV PIC X.                                          00001220
           02 MLIBELLEO      PIC X(20).                                 00001230
      * zone de commande                                                00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MZONCMDA  PIC X.                                          00001260
           02 MZONCMDC  PIC X.                                          00001270
           02 MZONCMDP  PIC X.                                          00001280
           02 MZONCMDH  PIC X.                                          00001290
           02 MZONCMDV  PIC X.                                          00001300
           02 MZONCMDO  PIC X(15).                                      00001310
      * date d'emmission de l'avoir                                     00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MDEMISA   PIC X.                                          00001340
           02 MDEMISC   PIC X.                                          00001350
           02 MDEMISP   PIC X.                                          00001360
           02 MDEMISH   PIC X.                                          00001370
           02 MDEMISV   PIC X.                                          00001380
           02 MDEMISO   PIC X(10).                                      00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MLIBERRA  PIC X.                                          00001410
           02 MLIBERRC  PIC X.                                          00001420
           02 MLIBERRP  PIC X.                                          00001430
           02 MLIBERRH  PIC X.                                          00001440
           02 MLIBERRV  PIC X.                                          00001450
           02 MLIBERRO  PIC X(78).                                      00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MCODTRAA  PIC X.                                          00001480
           02 MCODTRAC  PIC X.                                          00001490
           02 MCODTRAP  PIC X.                                          00001500
           02 MCODTRAH  PIC X.                                          00001510
           02 MCODTRAV  PIC X.                                          00001520
           02 MCODTRAO  PIC X(4).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MCICSA    PIC X.                                          00001550
           02 MCICSC    PIC X.                                          00001560
           02 MCICSP    PIC X.                                          00001570
           02 MCICSH    PIC X.                                          00001580
           02 MCICSV    PIC X.                                          00001590
           02 MCICSO    PIC X(5).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MNETNAMA  PIC X.                                          00001620
           02 MNETNAMC  PIC X.                                          00001630
           02 MNETNAMP  PIC X.                                          00001640
           02 MNETNAMH  PIC X.                                          00001650
           02 MNETNAMV  PIC X.                                          00001660
           02 MNETNAMO  PIC X(8).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MSCREENA  PIC X.                                          00001690
           02 MSCREENC  PIC X.                                          00001700
           02 MSCREENP  PIC X.                                          00001710
           02 MSCREENH  PIC X.                                          00001720
           02 MSCREENV  PIC X.                                          00001730
           02 MSCREENO  PIC X(4).                                       00001740
                                                                                
