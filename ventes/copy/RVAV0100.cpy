      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVAV0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVAV0100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVAV0100.                                                            
           02  AV01-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  AV01-NLIEU                                                       
               PIC X(0003).                                                     
           02  AV01-NAVOIR                                                      
               PIC X(0007).                                                     
           02  AV01-CTYPAVOIR                                                   
               PIC X(0005).                                                     
           02  AV01-DEMIS                                                       
               PIC X(0008).                                                     
           02  AV01-DICEMIS                                                     
               PIC X(0008).                                                     
           02  AV01-CTYPUTIL                                                    
               PIC X(0005).                                                     
           02  AV01-CAPPLI                                                      
               PIC X(0003).                                                     
           02  AV01-CMOTIFAV                                                    
               PIC X(0005).                                                     
           02  AV01-PMONTANT                                                    
               PIC S9(5)V9(0002) COMP-3.                                        
           02  AV01-NLIEUCOMPT                                                  
               PIC X(0003).                                                     
           02  AV01-CIMPUTATION                                                 
               PIC X(0010).                                                     
           02  AV01-NSOCVENTE                                                   
               PIC X(0003).                                                     
           02  AV01-NLIEUVENTE                                                  
               PIC X(0003).                                                     
           02  AV01-NVENTE                                                      
               PIC X(0007).                                                     
           02  AV01-DCAISSE                                                     
               PIC X(0008).                                                     
           02  AV01-NCAISSE                                                     
               PIC X(0003).                                                     
           02  AV01-NTRANS                                                      
               PIC X(0004).                                                     
           02  AV01-NCRI                                                        
               PIC X(0011).                                                     
           02  AV01-CMODPAIMT                                                   
               PIC X(0005).                                                     
           02  AV01-WUTIL                                                       
               PIC X(0001).                                                     
           02  AV01-DUTIL                                                       
               PIC X(0008).                                                     
           02  AV01-DICUTIL                                                     
               PIC X(0008).                                                     
           02  AV01-NSOCUTIL                                                    
               PIC X(0003).                                                     
           02  AV01-NLIEUUTIL                                                   
               PIC X(0003).                                                     
           02  AV01-CANNULATION                                                 
               PIC X(0001).                                                     
           02  AV01-DANNULATION                                                 
               PIC X(0008).                                                     
           02  AV01-NAVOIRANNUL                                                 
               PIC X(0007).                                                     
           02  AV01-PAVOIRDEV2                                                  
               PIC S9(5)V9(0002) COMP-3.                                        
           02  AV01-CDEVREF                                                     
               PIC X(0003).                                                     
           02  AV01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVAV0100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVAV0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-NAVOIR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-NAVOIR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-CTYPAVOIR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-CTYPAVOIR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-DEMIS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-DEMIS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-DICEMIS-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-DICEMIS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-CTYPUTIL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-CTYPUTIL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-CAPPLI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-CAPPLI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-CMOTIFAV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-CMOTIFAV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-PMONTANT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-PMONTANT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-NLIEUCOMPT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-NLIEUCOMPT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-CIMPUTATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-CIMPUTATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-NSOCVENTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-NSOCVENTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-NLIEUVENTE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-NLIEUVENTE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-DCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-DCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-NCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-NCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-NTRANS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-NCRI-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-NCRI-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-CMODPAIMT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-CMODPAIMT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-WUTIL-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-WUTIL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-DUTIL-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-DUTIL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-DICUTIL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-DICUTIL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-NSOCUTIL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-NSOCUTIL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-NLIEUUTIL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-NLIEUUTIL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-CANNULATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-CANNULATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-DANNULATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-DANNULATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-NAVOIRANNUL-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-NAVOIRANNUL-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-PAVOIRDEV2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-PAVOIRDEV2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-CDEVREF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-CDEVREF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
