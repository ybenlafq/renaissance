      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGV860 AU 06/12/1993  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,BI,A,                          *        
      *                           16,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGV860.                                                        
            05 NOMETAT-IGV860           PIC X(6) VALUE 'IGV860'.                
            05 RUPTURES-IGV860.                                                 
           10 IGV860-NSOCIETE           PIC X(03).                      007  003
           10 IGV860-NLIEU              PIC X(03).                      010  003
           10 IGV860-LIVRAISON          PIC X(03).                      013  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGV860-SEQUENCE           PIC S9(04) COMP.                016  002
      *--                                                                       
           10 IGV860-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGV860.                                                   
           10 IGV860-CODE-JOUR          PIC X(01).                      018  001
           10 IGV860-MONTANT-TTC        PIC S9(07)V9(2) COMP-3.         019  005
           10 IGV860-REMISE             PIC S9(07)V9(2) COMP-3.         024  005
           10 IGV860-TAUX-TVA           PIC S9(03)V9(2) COMP-3.         029  003
            05 FILLER                      PIC X(481).                          
                                                                                
