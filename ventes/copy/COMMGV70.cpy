      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                 00000010
      ***************************************************************   00000020
      *         MAQUETTE COMMAREA STANDARD AIDA COBOL2              *   00000030
      ***************************************************************   00000040
      *                                                                 00000050
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00000060
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00000070
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00000080
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00000090
      *                                                                 00000091
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00000092
      * COMPRENANT :                                                    00000093
      * 1 - LES ZONES RESERVEES A AIDA                                  00000094
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00000095
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00000096
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00000097
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00000098
      *                                                                 00000099
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00000100
      * PAR AIDA                                                        00000110
      *                                                                 00000120
      *-------------------------------------------------------------    00000130
      *                                                                 00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-GV70-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00000150
      *--                                                                       
       01  COM-GV70-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00000160
       01  Z-COMMAREA.                                                  00000170
      *                                                                 00000180
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00000190
           02 FILLER-COM-AIDA      PIC X(100).                          00000191
      *                                                                 00000192
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00000193
           02 COMM-CICS-APPLID     PIC X(8).                            00000194
           02 COMM-CICS-NETNAM     PIC X(8).                            00000195
           02 COMM-CICS-TRANSA     PIC X(4).                            00000196
      *                                                                 00000197
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00000198
           02 COMM-DATE-SIECLE     PIC XX.                              00000199
           02 COMM-DATE-ANNEE      PIC XX.                              00000200
           02 COMM-DATE-MOIS       PIC XX.                              00000210
           02 COMM-DATE-JOUR       PIC 99.                              00000220
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000230
           02 COMM-DATE-QNTA       PIC 999.                             00000240
           02 COMM-DATE-QNT0       PIC 99999.                           00000250
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000260
           02 COMM-DATE-BISX       PIC 9.                               00000270
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00000280
           02 COMM-DATE-JSM        PIC 9.                               00000290
      *   LIBELLES DU JOUR COURT - LONG                                 00000291
           02 COMM-DATE-JSM-LC     PIC XXX.                             00000292
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00000293
      *   LIBELLES DU MOIS COURT - LONG                                 00000294
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00000295
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00000296
      *   DIFFERENTES FORMES DE DATE                                    00000297
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00000298
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00000299
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00000300
           02 COMM-DATE-JJMMAA     PIC X(6).                            00000310
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00000320
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00000330
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00000340
           02 COMM-DATE-WEEK.                                           00000350
              05 COMM-DATE-SEMSS   PIC 99.                              00000360
              05 COMM-DATE-SEMAA   PIC 99.                              00000370
              05 COMM-DATE-SEMNU   PIC 99.                              00000380
      *                                                                 00000390
           02 COMM-DATE-FILLER     PIC X(08).                           00000391
      *                                                                 00000392
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00000393
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00000394
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 270 PIC X(1).                       00000395
      *                                                                 00000396
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00000397
           02 COMM-GV70-MENU.                                           00000398
      *                                                                 00000399
      *                                              CODE VENDEUR       00000400
              03 COMM-GV70-CVENDEUR       PIC X(6).                     00000410
      *                                              CODE SOCIETE       00000400
              03 COMM-GV70-NSOCIETE       PIC X(3).                     00000410
      *                                                                 00000460
      *                                              OCTETS LIBRES      00000695
           02 COMM-GV70-APPLI.                                          00000696
              03 COMM-GV70-FILLER         PIC X(3640).                  00000697
                                                                                
