      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVSL1100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSL1100                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSL1100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSL1100.                                                            
      *}                                                                        
           10 SL11-NSOCIETE        PIC X(3).                                    
           10 SL11-NLIEU           PIC X(3).                                    
           10 SL11-NCODIC          PIC X(7).                                    
           10 SL11-CRSL            PIC X(5).                                    
           10 SL11-QUANTITE        PIC S9(7)V USAGE COMP-3.                     
           10 SL11-DSYST           PIC S9(13)V USAGE COMP-3.                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSL1100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSL1100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11-NSOCIETE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 SL11-NSOCIETE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11-NLIEU-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 SL11-NLIEU-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 SL11-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11-CRSL-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 SL11-CRSL-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11-QUANTITE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 SL11-QUANTITE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11-DSYST-F         PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 SL11-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
