      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      * ------------------------------------------------------------            
      *   COPY DE LA TABLE RRTGV03                                              
      * ------------------------------------------------------------            
      *   LISTE DES HOST VARIABLES DE LA VUE RVGV0303                           
      * ------------------------------------------------------------            
      * !!! ATTENTION D�PHASAGE DE STRUCTURE ENTRE 400 ET HOST.                 
      * CETTE COPIE REPR�SENTE LA STRUCTURE DU 400.                             
      * INVERSION WADPRP WASC ET WFLAG                                          
      * IL FAUT DONC NOMMER CHAQUE NOM DE COLONNE SI ON                         
      * VEUT R�CUP�RER TOUTES LES DONN�ES (NE PAS FAIRE DE SELECT *             
      * ------------------------------------------------------------            
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV0303.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV0303.                                                            
      *}                                                                        
           02  GV03-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GV03-NLIEU                                                       
               PIC X(0003).                                                     
           02  GV03-NORDRE                                                      
               PIC X(0005).                                                     
           02  GV03-NVENTE                                                      
               PIC X(0007).                                                     
           02  GV03-WTYPEADR                                                    
               PIC X(0001).                                                     
           02  GV03-EMAIL                                                       
               PIC X(0063).                                                     
           02  GV03-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GV03-CPORTE                                                      
               PIC X(0010).                                                     
           02  GV03-WADPRP                                                      
               PIC X(0001).                                                     
           02  GV03-WASC                                                        
               PIC X(0001).                                                     
           02  GV03-WFLAG                                                       
               PIC X(0010).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA VUE RVGV0303                                    
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV0300-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV0300-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV03-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV03-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV03-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV03-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV03-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV03-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV03-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV03-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV03-WTYPEADR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV03-WTYPEADR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV03-EMAIL-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV03-EMAIL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV03-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV03-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV03-CPORTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV03-CPORTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV03-WFLAG-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV03-WFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV03-WASC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV03-WASC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV03-WADPRP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GV03-WADPRP-F                                                    
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
