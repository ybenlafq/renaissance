      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVSL0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSL0100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVSL0100.                                                            
           02 SL01-CEMPL            PIC X(05).                                  
           02 SL01-LEMPL            PIC X(20).                                  
           02 SL01-WVALO            PIC X(01).                                  
           02 SL01-WPRES            PIC X(01).                                  
           02 SL01-WWVND            PIC X(01).                                  
           02 SL01-WWHS             PIC X(01).                                  
           02 SL01-WWUT             PIC X(01).                                  
           02 SL01-WCTR             PIC X(01).                                  
           02 SL01-WATR             PIC X(01).                                  
           02 SL01-WAUX             PIC X(01).                                  
           02 SL01-WTRANSIT         PIC X(01).                                  
           02 SL01-WDISPO           PIC X(01).                                  
           02 SL01-NSEQEDT          PIC S9(03) COMP-3.                          
           02 SL01-DSYST            PIC S9(13) COMP-3.                          
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVSL0100                                  
      *---------------------------------------------------------                
       01  RVSL0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL01-CEMPL-F          PIC S9(04) COMP.                            
      *--                                                                       
           02 SL01-CEMPL-F          PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL01-LEMPL-F          PIC S9(04) COMP.                            
      *--                                                                       
           02 SL01-LEMPL-F          PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL01-WVALO-F          PIC S9(04) COMP.                            
      *--                                                                       
           02 SL01-WVALO-F          PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL01-WPRES-F          PIC S9(04) COMP.                            
      *--                                                                       
           02 SL01-WPRES-F          PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL01-WWVND-F          PIC S9(04) COMP.                            
      *--                                                                       
           02 SL01-WWVND-F          PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL01-WWHS-F           PIC S9(04) COMP.                            
      *--                                                                       
           02 SL01-WWHS-F           PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL01-WWUT-F           PIC S9(04) COMP.                            
      *--                                                                       
           02 SL01-WWUT-F           PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL01-WCTR-F           PIC S9(04) COMP.                            
      *--                                                                       
           02 SL01-WCTR-F           PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL01-WATR-F           PIC S9(04) COMP.                            
      *--                                                                       
           02 SL01-WATR-F           PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL01-WAUX-F           PIC S9(04) COMP.                            
      *--                                                                       
           02 SL01-WAUX-F           PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL01-WTRANSIT-F       PIC S9(04) COMP.                            
      *--                                                                       
           02 SL01-WTRANSIT-F       PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL01-WDISPO-F         PIC S9(04) COMP.                            
      *--                                                                       
           02 SL01-WDISPO-F         PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL01-NSEQEDT-F        PIC S9(04) COMP.                            
      *--                                                                       
           02 SL01-NSEQEDT-F        PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL01-DSYST-F          PIC S9(04) COMP.                            
      *--                                                                       
           02 SL01-DSYST-F          PIC S9(04) COMP-5.                          
      *}                                                                        
       EJECT                                                                    
                                                                                
