      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EPF22   EPF22                                              00000020
      ***************************************************************** 00000030
       01   EPM21I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIBELL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIBELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIBELF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIBELI  PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATERECL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MDATERECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATERECF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDATERECI      PIC X(10).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNMAGI    PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBMAGL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLIBMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBMAGF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLIBMAGI  PIC X(20).                                      00000370
           02 MNLIGNE1I OCCURS   14 TIMES .                             00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNLIEUI      PIC X(3).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNVENTEL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNVENTEF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNVENTEI     PIC X(7).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDETAILL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MDETAILL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDETAILF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MDETAILI     PIC X(21).                                 00000500
           02 MNLIGNE4I OCCURS   14 TIMES .                             00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDIFFL  COMP PIC S9(4).                                 00000520
      *--                                                                       
             03 MDIFFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDIFFF  PIC X.                                          00000530
             03 FILLER  PIC X(4).                                       00000540
             03 MDIFFI  PIC X(10).                                      00000550
           02 MNLIGNE5I OCCURS   14 TIMES .                             00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRFACL  COMP PIC S9(4).                                 00000570
      *--                                                                       
             03 MRFACL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MRFACF  PIC X.                                          00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MRFACI  PIC X(10).                                      00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MLIBERRI  PIC X(79).                                      00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MCODTRAI  PIC X(4).                                       00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MCICSI    PIC X(5).                                       00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MNETNAMI  PIC X(8).                                       00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MSCREENI  PIC X(4).                                       00000800
      ***************************************************************** 00000810
      * SDF: EPF22   EPF22                                              00000820
      ***************************************************************** 00000830
       01   EPM21O REDEFINES EPM21I.                                    00000840
           02 FILLER    PIC X(12).                                      00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MDATJOUA  PIC X.                                          00000870
           02 MDATJOUC  PIC X.                                          00000880
           02 MDATJOUP  PIC X.                                          00000890
           02 MDATJOUH  PIC X.                                          00000900
           02 MDATJOUV  PIC X.                                          00000910
           02 MDATJOUO  PIC X(10).                                      00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MTIMJOUA  PIC X.                                          00000940
           02 MTIMJOUC  PIC X.                                          00000950
           02 MTIMJOUP  PIC X.                                          00000960
           02 MTIMJOUH  PIC X.                                          00000970
           02 MTIMJOUV  PIC X.                                          00000980
           02 MTIMJOUO  PIC X(5).                                       00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MPAGEA    PIC X.                                          00001010
           02 MPAGEC    PIC X.                                          00001020
           02 MPAGEP    PIC X.                                          00001030
           02 MPAGEH    PIC X.                                          00001040
           02 MPAGEV    PIC X.                                          00001050
           02 MPAGEO    PIC X(3).                                       00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MNSOCA    PIC X.                                          00001080
           02 MNSOCC    PIC X.                                          00001090
           02 MNSOCP    PIC X.                                          00001100
           02 MNSOCH    PIC X.                                          00001110
           02 MNSOCV    PIC X.                                          00001120
           02 MNSOCO    PIC X(3).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MNLIBELA  PIC X.                                          00001150
           02 MNLIBELC  PIC X.                                          00001160
           02 MNLIBELP  PIC X.                                          00001170
           02 MNLIBELH  PIC X.                                          00001180
           02 MNLIBELV  PIC X.                                          00001190
           02 MNLIBELO  PIC X(20).                                      00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MDATERECA      PIC X.                                     00001220
           02 MDATERECC PIC X.                                          00001230
           02 MDATERECP PIC X.                                          00001240
           02 MDATERECH PIC X.                                          00001250
           02 MDATERECV PIC X.                                          00001260
           02 MDATERECO      PIC X(10).                                 00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MNMAGA    PIC X.                                          00001290
           02 MNMAGC    PIC X.                                          00001300
           02 MNMAGP    PIC X.                                          00001310
           02 MNMAGH    PIC X.                                          00001320
           02 MNMAGV    PIC X.                                          00001330
           02 MNMAGO    PIC X(3).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MLIBMAGA  PIC X.                                          00001360
           02 MLIBMAGC  PIC X.                                          00001370
           02 MLIBMAGP  PIC X.                                          00001380
           02 MLIBMAGH  PIC X.                                          00001390
           02 MLIBMAGV  PIC X.                                          00001400
           02 MLIBMAGO  PIC X(20).                                      00001410
           02 MNLIGNE1O OCCURS   14 TIMES .                             00001420
             03 FILLER       PIC X(2).                                  00001430
             03 MNLIEUA      PIC X.                                     00001440
             03 MNLIEUC PIC X.                                          00001450
             03 MNLIEUP PIC X.                                          00001460
             03 MNLIEUH PIC X.                                          00001470
             03 MNLIEUV PIC X.                                          00001480
             03 MNLIEUO      PIC X(3).                                  00001490
             03 FILLER       PIC X(2).                                  00001500
             03 MNVENTEA     PIC X.                                     00001510
             03 MNVENTEC     PIC X.                                     00001520
             03 MNVENTEP     PIC X.                                     00001530
             03 MNVENTEH     PIC X.                                     00001540
             03 MNVENTEV     PIC X.                                     00001550
             03 MNVENTEO     PIC X(7).                                  00001560
             03 FILLER       PIC X(2).                                  00001570
             03 MDETAILA     PIC X.                                     00001580
             03 MDETAILC     PIC X.                                     00001590
             03 MDETAILP     PIC X.                                     00001600
             03 MDETAILH     PIC X.                                     00001610
             03 MDETAILV     PIC X.                                     00001620
             03 MDETAILO     PIC X(21).                                 00001630
           02 MNLIGNE4O OCCURS   14 TIMES .                             00001640
             03 FILLER       PIC X(2).                                  00001650
             03 MDIFFA  PIC X.                                          00001660
             03 MDIFFC  PIC X.                                          00001670
             03 MDIFFP  PIC X.                                          00001680
             03 MDIFFH  PIC X.                                          00001690
             03 MDIFFV  PIC X.                                          00001700
             03 MDIFFO  PIC X(10).                                      00001710
           02 MNLIGNE5O OCCURS   14 TIMES .                             00001720
             03 FILLER       PIC X(2).                                  00001730
             03 MRFACA  PIC X.                                          00001740
             03 MRFACC  PIC X.                                          00001750
             03 MRFACP  PIC X.                                          00001760
             03 MRFACH  PIC X.                                          00001770
             03 MRFACV  PIC X.                                          00001780
             03 MRFACO  PIC X(10).                                      00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MLIBERRA  PIC X.                                          00001810
           02 MLIBERRC  PIC X.                                          00001820
           02 MLIBERRP  PIC X.                                          00001830
           02 MLIBERRH  PIC X.                                          00001840
           02 MLIBERRV  PIC X.                                          00001850
           02 MLIBERRO  PIC X(79).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MCODTRAA  PIC X.                                          00001880
           02 MCODTRAC  PIC X.                                          00001890
           02 MCODTRAP  PIC X.                                          00001900
           02 MCODTRAH  PIC X.                                          00001910
           02 MCODTRAV  PIC X.                                          00001920
           02 MCODTRAO  PIC X(4).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCICSA    PIC X.                                          00001950
           02 MCICSC    PIC X.                                          00001960
           02 MCICSP    PIC X.                                          00001970
           02 MCICSH    PIC X.                                          00001980
           02 MCICSV    PIC X.                                          00001990
           02 MCICSO    PIC X(5).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MNETNAMA  PIC X.                                          00002020
           02 MNETNAMC  PIC X.                                          00002030
           02 MNETNAMP  PIC X.                                          00002040
           02 MNETNAMH  PIC X.                                          00002050
           02 MNETNAMV  PIC X.                                          00002060
           02 MNETNAMO  PIC X(8).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MSCREENA  PIC X.                                          00002090
           02 MSCREENC  PIC X.                                          00002100
           02 MSCREENP  PIC X.                                          00002110
           02 MSCREENH  PIC X.                                          00002120
           02 MSCREENV  PIC X.                                          00002130
           02 MSCREENO  PIC X(4).                                       00002140
                                                                                
