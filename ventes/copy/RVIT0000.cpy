      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVIT0000                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIT0000                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIT0000.                                                    00000090
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIT0000.                                                            
      *}                                                                        
           02  IT00-NINVENTAIRE                                         00000100
               PIC X(0005).                                             00000110
           02  IT00-DEDITSUPPORT                                        00000120
               PIC X(0008).                                             00000130
           02  IT00-DSTOCKTHEO                                          00000140
               PIC X(0008).                                             00000150
           02  IT00-DCOMPTAGE                                           00000160
               PIC X(0008).                                             00000170
           02  IT00-DEDITECARTS                                         00000180
               PIC X(0008).                                             00000190
           02  IT00-DSYST                                               00000200
               PIC S9(13) COMP-3.                                       00000210
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00000220
      *---------------------------------------------------------        00000230
      *   LISTE DES FLAGS DE LA TABLE RVIT0000                          00000240
      *---------------------------------------------------------        00000250
      *                                                                 00000260
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIT0000-FLAGS.                                              00000270
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIT0000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT00-NINVENTAIRE-F                                       00000280
      *        PIC S9(4) COMP.                                          00000290
      *--                                                                       
           02  IT00-NINVENTAIRE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT00-DEDITSUPPORT-F                                      00000300
      *        PIC S9(4) COMP.                                          00000310
      *--                                                                       
           02  IT00-DEDITSUPPORT-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT00-DSTOCKTHEO-F                                        00000320
      *        PIC S9(4) COMP.                                          00000330
      *--                                                                       
           02  IT00-DSTOCKTHEO-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT00-DCOMPTAGE-F                                         00000340
      *        PIC S9(4) COMP.                                          00000350
      *--                                                                       
           02  IT00-DCOMPTAGE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT00-DEDITECARTS-F                                       00000360
      *        PIC S9(4) COMP.                                          00000370
      *--                                                                       
           02  IT00-DEDITECARTS-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT00-DSYST-F                                             00000380
      *        PIC S9(4) COMP.                                          00000390
      *--                                                                       
           02  IT00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                            00000400
                                                                                
