      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *    COPY  COMMGV80.                                                      
      *                                                                 00010000
      **************************************************************    00020000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00030000
      **************************************************************    00040000
      *                                                                 00050000
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00060000
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00070000
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00080000
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00090000
      *                                                                 00100000
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00110000
      * COMPRENANT :                                                    00120000
      * 1 - LES ZONES RESERVEES A AIDA                                  00130000
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00140000
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00150000
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00160000
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00170000
      *                                                                 00180000
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00190000
      * PAR AIDA                                                        00200000
      * ------------------------------------------------------------    00200000
      * DETAILS DES MODIFICATIONS !                                     00200000
      * ------------------------------------------------------------    00200000
      * AL1507 (15072002) DSA024 (AL)                                   00200000
      * AJOUT DE LA ZONE COMM-GV82-NLIEUDEPLIV                          00200000
      *-------------------------------------------------------------    00220000
      * 25/01/12 : PERMETTRE LA RECEPTION DE BE D'UNE VENTE ANNUL�E.            
      *-------------------------------------------------------------            
      *-------------------------------------------------------------            
      *-------------------------------------------------------------            
      *-------------------------------------------------------------            
      *                                                                 00230000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-GV80-LONG-COMMAREA PIC S9(5) COMP VALUE +4096.           00240001
      *--                                                                       
       01  COM-GV80-LONG-COMMAREA PIC S9(5) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00250000
       01  Z-COMMAREA.                                                  00260000
      *                                                                 00270000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00280000
           02 FILLER-COM-AIDA      PIC X(100).                          00290000
      *                                                                 00300000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00310000
           02 COMM-CICS-APPLID     PIC X(8).                            00320000
           02 COMM-CICS-NETNAM     PIC X(8).                            00330000
           02 COMM-CICS-TRANSA     PIC X(4).                            00340000
      *                                                                 00350000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00360000
           02 COMM-DATE-SIECLE     PIC XX.                              00370000
           02 COMM-DATE-ANNEE      PIC XX.                              00380000
           02 COMM-DATE-MOIS       PIC XX.                              00390000
           02 COMM-DATE-JOUR       PIC 99.                              00400000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00410000
           02 COMM-DATE-QNTA       PIC 999.                             00420000
           02 COMM-DATE-QNT0       PIC 99999.                           00430000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00440000
           02 COMM-DATE-BISX       PIC 9.                               00450000
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00460000
           02 COMM-DATE-JSM        PIC 9.                               00470000
      *   LIBELLES DU JOUR COURT - LONG                                 00480000
           02 COMM-DATE-JSM-LC     PIC XXX.                             00490000
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00500000
      *   LIBELLES DU MOIS COURT - LONG                                 00510000
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00520000
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00530000
      *   DIFFERENTES FORMES DE DATE                                    00540000
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00550000
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00560000
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00570000
           02 COMM-DATE-JJMMAA     PIC X(6).                            00580000
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00590000
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00600000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00610000
           02 COMM-DATE-WEEK.                                           00620000
              05 COMM-DATE-SEMSS   PIC 99.                              00630000
              05 COMM-DATE-SEMAA   PIC 99.                              00640000
              05 COMM-DATE-SEMNU   PIC 99.                              00650000
      *                                                                 00660000
           02 COMM-DATE-FILLER     PIC X(08).                           00670000
      *                                                                 00680000
      * ZONES RESERVEES TRAITEMENT DU SWAP -----------------------152   00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00700000
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR       PIC X(150).                          00710000
      *                                                                 00720000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00730000
           02 FILLER   PIC X(22) VALUE ' *** COMMAREA GV80 ***'.        00741002
           02 COMM-GV80-APPLI.                                          00740000
              03 COMM-GV80-ORIGINE        PIC X.                        00741002
      *{ remove-comma-in-dde 1.5                                                
      *          88 COMM-GV80-MAGASIN         VALUE 'M' , 'S' , 'T'.            
      *--                                                                       
                 88 COMM-GV80-MAGASIN         VALUE 'M'   'S'   'T'.            
      *}                                                                        
                 88 COMM-MAGASIN-CLASSIQUE    VALUE 'M'.                        
                 88 COMM-MAGASIN-SPECIAL      VALUE 'S'.                        
                 88 COMM-MAGASIN-TRES-SPECIAL VALUE 'T'.                        
                 88 COMM-GV80-ENTREPOT        VALUE 'E'.                        
              03 COMM-GV80-NSOC           PIC X(3).                     00741002
              03 COMM-GV80-NLIEU          PIC X(3).                     00741002
              03 COMM-GV80-NLIEUDEST      PIC X(3).                     00743002
              03 COMM-GV80-NSSLIEUDEST    PIC X(3).                     00743002
              03 COMM-GV80-LLIEUDEST      PIC X(20).                    00743002
              03 COMM-GV80-DEST-TYPE      PIC X.                        00743002
                 88  COMM-GV80-DEST-VIDE      VALUE ' '.                        
      *{ remove-comma-in-dde 1.5                                                
      *          88  COMM-GV80-DEST-SAISI     VALUE '2' , '3'.                  
      *--                                                                       
                 88  COMM-GV80-DEST-SAISI     VALUE '2'   '3'.                  
      *}                                                                        
                 88  COMM-GV80-DEST-MAG       VALUE '3'.                        
                 88  COMM-GV80-DEST-ENT       VALUE '2'.                        
CL1209        03 COMM-GV80-BLOCAGE-CHAMPS PIC X.                                
CL1209        03 FILLER                   PIC X(281).                   00743002
CL1209*       03 FILLER                   PIC X(282).                   00743002
      *                                                                         
      *  ZONES REINITIALISEES A CHAQUE OPTION                           00960000
              03 COMM-GV80-ZONES.                                       00741002
                 05 COMM-GV80-NMAGASIN       PIC X(3).                  00742002
                 05 COMM-GV80-NBONENLV       PIC X(7).                  00743002
                 05 COMM-GV80-NDOCENLV       PIC X(7).                  00743002
                 05 COMM-GV80-DDLIV          PIC X(8).                  00743002
                 05 COMM-GV80-LMAGASIN       PIC X(20).                 00743002
                 05 COMM-GV80-CODRET         PIC X.                     00743002
                    88  COMM-GV80-PAS-MESS   VALUE ' '.                         
                    88  COMM-GV80-MESS-RET   VALUE '1'.                         
                 05 COMM-GV80-MESSAGE        PIC X(50).                 00743002
                 05 COMM-GV80-NSSMAG         PIC X(3).                  00743002
                 05 COMM-GV80-NCODIMP        PIC X(4).                  00743002
                 05 COMM-GV80-NBIMP          PIC 9(3).                  00743002
                 05 COMM-GV80-PRGI           PIC X(5).                  00743002
                 05 COMM-GV80-TOPESEL        PIC X(01).                 00743002
                    88  COMM-GV80-NONTOPE    VALUE '1'.                         
                 05 FILLER                   PIC X(277).                00743002
                 05 COMM-GV80-SSPRGS         PIC X(3000).               00743002
      *                                                                 00960000
      *--------ZONES PROPRES A TGV81                                    00960000
                 05 COMM-GV80-SSPRG-81    REDEFINES COMM-GV80-SSPRGS .  00743002
                    10  COMM-GV81-CRITERE   PIC   X.                            
                        88  COMM-GV81-UN-BE    VALUE  'B'.                      
                        88  COMM-GV81-TOUS-BE  VALUE  'L'.                      
                        88  COMM-GV81-UN-MAG   VALUE  'M'.                      
                        88  COMM-GV81-TOUS-MAG VALUE  'T'.                      
                    10  COMM-GV81-PAGE-MAX      PIC  9(3).                      
                    10  COMM-GV81-PAGE-ENC      PIC  9(3).                      
                    10  COMM-GV81-NLIEU-LU      PIC  X(3).                      
                    10  COMM-GV81-NBONENLV-LU   PIC  X(7).                      
                    10  FILLER                  PIC  X(2983).                   
      *                                                                 00960000
      *--------ZONES PROPRES A TGV82                                    00960000
                 05 COMM-GV80-SSPRG-82    REDEFINES COMM-GV80-SSPRGS .  00743002
                    10  COMM-GV82-TRAIT     PIC   X.                            
                        88  COMM-GV82-UN-MAG   VALUE  'U'.                      
                        88  COMM-GV82-TOUS-MAG VALUE  ' '.                      
                    10  COMM-GV82-PAGE-MAX      PIC  9(3).                      
                    10  COMM-GV82-PAGE-ENC      PIC  9(3).                      
                    10  COMM-GV82-NLIEU-LU      PIC  X(3).                      
                    10  COMM-GV82-NLIEU-SUI     PIC  X(3).                      
                    10  COMM-GV82-NBONENLV-LU   PIC  X(7).                      
                    10  COMM-GV82-DDLIV-LU      PIC  X(8).                      
                    10  COMM-GV82-CONSULT       PIC  X.                         
                        88  COMM-GV82-CFIN   VALUE  '1'.                        
                        88  COMM-GV82-CSUI   VALUE  ' '.                        
                    10  COMM-GV82-VALID         PIC  X.                         
AL1507              10  COMM-GV82-NLIEUDEPLIV   PIC  X(03).                     
                    10  FILLER                  PIC  X(2967).                   
      *                                                                 00960000
      *--------ZONES PROPRES A TGV83                                    00960000
                 05 COMM-GV80-SSPRG-83    REDEFINES COMM-GV80-SSPRGS .  00743002
                    10  COMM-GV83-PAGE-MAX      PIC  9(3).                      
                    10  COMM-GV83-PAGE-ENC      PIC  9(3).                      
                    10  COMM-GV83-NBONENLV-LU   PIC  X(7).                      
                    10  COMM-GV83-DRECEPT-FILE  PIC  X(8).                      
                    10  COMM-GV83-DRECEPT-MAP   PIC  X(8).                      
                    10  COMM-GV83-VALID         PIC  X.                         
                    10  COMM-GV83-VTE-ANNUL     PIC  X(1).                      
                    10  FILLER                  PIC  X(2969).                   
      *                                                                 00960000
CR3004*--------ZONES PROPRES A TGV86                                    00960000
                 05 COMM-GV80-SSPRG-86    REDEFINES COMM-GV80-SSPRGS .  00743002
                    10  COMM-GV86-NSOC          PIC  X(3).                      
                    10  COMM-GV86-PTF           PIC  X(3).                      
                    10  COMM-GV86-DANNUL        PIC  X(8).                      
                    10  COMM-GV86-MESS          PIC  X(79).                     
                    10  FILLER                  PIC  X(2907).                   
                                                                                
