      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER CUMUL ANNUEL POUR REMISE DE PRIX   *        
      *                                                                *        
      *          CRITERES DE TRI  01,06,BI,A,28,6                      *        
      *----------------------------------------------------------------*        
       01  DSECT-IRECUM.                                                        
           10 CUMUL-NSOCIETE            PIC X(03).                      007  003
           10 CUMUL-NLIEU               PIC X(03).                      007  003
           10 CUMUL-VAL-FORC-P          PIC S9(09) COMP-3.              082  004
           10 CUMUL-VAL-FORC-M          PIC S9(09) COMP-3.              086  004
           10 CUMUL-VAL-REM             PIC S9(09) COMP-3.              090  002
           10 CUMUL-VENTES              PIC S9(11) COMP-3.              092  004
           10 CUMUL-EXERCICE            PIC X(6).                       092  004
           10 CUMUL-CGRPMAG             PIC X(2).                       092  004
           10 FILLER                    PIC X(65).                              
                                                                                
