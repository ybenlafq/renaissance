      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000010
      * >>>>>>>>>>>>>>>>> DESCRIPTION DE LA TSHV45 <<<<<<<<<<<<<<<<<< * 00000020
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000030
02/00 * MODIF      DATE : 09/02/2000  AUTEUR : JONATHAN DESAUNOIS     * 00000040
02/00 * OBJET : PRISE EN COMPTE DE L'ARCHIVAGE DES PSE                * 00000040
02/00 *                                                               * 00000040
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000030
       01  TSHV45-IDENTIFICATEUR.                                       00000050
           05  TSHV45-TRANSID               PIC X(04) VALUE 'HV45'.     00000060
           05  TSHV45-NUMERO-DE-TERMINAL    PIC X(04) VALUE SPACES.     00000070
      *                                                               * 00000080
       01  TSHV45-ITEM.                                                 00000090
02/00      05  TSHV45-LONGUEUR              PIC S9(4) VALUE +0252.      00000100
           05  TSHV45-DATAS.                                            00000110
               10  TSHV45-LIGNE             OCCURS 7.                   00000120
      *      PREMIERE PARTIE = 036 OCTETS PAR OCCURENCE               * 00000200
                   15  TSHV45-NDOC             PIC X(03).               00000130
                   15  TSHV45-NSOCIETE         PIC X(03).               00000130
                   15  TSHV45-NLIEU            PIC X(03).               00000140
                   15  TSHV45-NSERVICE         PIC X(04).               00000140
                   15  TSHV45-NBOX             PIC X(05).               00000150
                   15  TSHV45-NBOITE           PIC X(06).               00000170
                   15  TSHV45-NFICHE           PIC X(03).                       
02/00              15  TSHV45-NVENTE           PIC X(07).                       
02/00              15  TSHV45-NSEQ             PIC X(02).                       
      *                                                               * 00000200
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000210
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< * 00000220
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000230
                                                                                
