      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVVI0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVI0100                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVI0100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVI0100.                                                            
      *}                                                                        
           02  VI01-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  VI01-NLIEU                                                       
               PIC X(0003).                                                     
           02  VI01-NVENTE                                                      
               PIC X(0007).                                                     
           02  VI01-CTYPENREG                                                   
               PIC X(0001).                                                     
           02  VI01-NCODICGRP                                                   
               PIC X(0007).                                                     
           02  VI01-NCODIC                                                      
               PIC X(0007).                                                     
           02  VI01-NSEQ                                                        
               PIC X(0002).                                                     
           02  VI01-CMODDEL                                                     
               PIC X(0003).                                                     
           02  VI01-DDELIV                                                      
               PIC X(0008).                                                     
           02  VI01-NORDRE                                                      
               PIC X(0005).                                                     
           02  VI01-CENREG                                                      
               PIC X(0005).                                                     
           02  VI01-QVENDUE                                                     
               PIC S9(5) COMP-3.                                                
           02  VI01-PVUNIT                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VI01-PVUNITF                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VI01-PVTOTAL                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VI01-CEQUIPE                                                     
               PIC X(0005).                                                     
           02  VI01-NLIGNE                                                      
               PIC X(0002).                                                     
           02  VI01-TAUXTVA                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  VI01-QCONDT                                                      
               PIC S9(5) COMP-3.                                                
           02  VI01-PRMP                                                        
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VI01-WEMPORTE                                                    
               PIC X(0001).                                                     
           02  VI01-CPLAGE                                                      
               PIC X(0002).                                                     
           02  VI01-CPROTOUR                                                    
               PIC X(0005).                                                     
           02  VI01-CADRTOUR                                                    
               PIC X(0001).                                                     
           02  VI01-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  VI01-LCOMMENT                                                    
               PIC X(0035).                                                     
           02  VI01-CVENDEUR                                                    
               PIC X(0006).                                                     
           02  VI01-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  VI01-NDEPOT                                                      
               PIC X(0003).                                                     
           02  VI01-NAUTORM                                                     
               PIC X(0005).                                                     
           02  VI01-WARTINEX                                                    
               PIC X(0001).                                                     
           02  VI01-WEDITBL                                                     
               PIC X(0001).                                                     
           02  VI01-WACOMMUTER                                                  
               PIC X(0001).                                                     
           02  VI01-WCQERESF                                                    
               PIC X(0001).                                                     
           02  VI01-NMUTATION                                                   
               PIC X(0007).                                                     
           02  VI01-CTOURNEE                                                    
               PIC X(0008).                                                     
           02  VI01-WTOPELIVRE                                                  
               PIC X(0001).                                                     
           02  VI01-DCOMPTA                                                     
               PIC X(0008).                                                     
           02  VI01-DCREATION                                                   
               PIC X(0008).                                                     
           02  VI01-HCREATION                                                   
               PIC X(0004).                                                     
           02  VI01-DANNULATION                                                 
               PIC X(0008).                                                     
           02  VI01-NLIEUORIG                                                   
               PIC X(0003).                                                     
           02  VI01-WINTMAJ                                                     
               PIC X(0001).                                                     
           02  VI01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  VI01-DSTAT                                                       
               PIC X(0004).                                                     
           02  VI01-CPRESTGRP                                                   
               PIC X(0005).                                                     
           02  VI01-NSOCMODIF                                                   
               PIC X(03).                                                       
           02  VI01-NLIEUMODIF                                                  
               PIC X(03).                                                       
           02  VI01-DATENC                                                      
               PIC X(08).                                                       
           02  VI01-CDEV                                                        
               PIC X(03).                                                       
           02  VI01-WUNITR                                                      
               PIC X(20).                                                       
           02  VI01-LRCMMT                                                      
               PIC X(10).                                                       
           02  VI01-NSOCP                                                       
               PIC X(03).                                                       
           02  VI01-NLIEUP                                                      
               PIC X(03).                                                       
           02  VI01-NTRANS                                                      
               PIC S9(8) COMP-3.                                                
           02  VI01-NSEQFV                                                      
               PIC X(02).                                                       
           02  VI01-NSEQNQ                                                      
               PIC S9(5) COMP-3.                                                
           02  VI01-NSEQREF                                                     
               PIC S9(5) COMP-3.                                                
           02  VI01-NMODIF                                                      
               PIC S9(5) COMP-3.                                                
           02  VI01-NSOCORIG                                                    
               PIC X(03).                                                       
           02  VI01-DTOPE                                                       
               PIC X(08).                                                       
           02  VI01-NSOCLIVR1                                                   
               PIC X(03).                                                       
           02  VI01-NDEPOT1                                                     
               PIC X(03).                                                       
           02  VI01-NSOCGEST                                                    
               PIC X(03).                                                       
           02  VI01-NLIEUGEST                                                   
               PIC X(03).                                                       
           02  VI01-NSOCDEPLIV                                                  
               PIC X(03).                                                       
           02  VI01-NLIEUDEPLIV                                                 
               PIC X(03).                                                       
           02  VI01-TYPVTE                                                      
               PIC X(01).                                                       
           02  VI01-CTYPENT                                                     
               PIC X(02).                                                       
           02  VI01-NLIEN                                                       
               PIC S9(5) COMP-3.                                                
           02  VI01-NACTVTE                                                     
               PIC S9(5) COMP-3.                                                
           02  VI01-PVCODIG                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VI01-QTCODIG                                                     
               PIC S9(5) COMP-3.                                                
           02  VI01-DPRLG                                                       
               PIC X(08).                                                       
           02  VI01-CALERTE                                                     
               PIC X(05).                                                       
           02  VI01-CREPRISE                                                    
               PIC X(05).                                                       
           02  VI01-NSEQENS                                                     
               PIC S9(5) COMP-3.                                                
           02  VI01-MPRIMECLI                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVVI0100                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVI0100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVI0100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-CTYPENREG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-CTYPENREG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NCODICGRP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NCODICGRP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-CMODDEL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-CMODDEL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-DDELIV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-CENREG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-CENREG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-QVENDUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-QVENDUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-PVUNIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-PVUNIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-PVUNITF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-PVUNITF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-PVTOTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-PVTOTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-CEQUIPE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-CEQUIPE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-TAUXTVA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-TAUXTVA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-QCONDT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-QCONDT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-PRMP-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-PRMP-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-WEMPORTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-WEMPORTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-CPLAGE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-CPLAGE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-CPROTOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-CPROTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-CADRTOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-CADRTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NAUTORM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NAUTORM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-WARTINEX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-WARTINEX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-WEDITBL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-WEDITBL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-WACOMMUTER-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-WACOMMUTER-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-WCQERESF-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-WCQERESF-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-CTOURNEE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-CTOURNEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-WTOPELIVRE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-WTOPELIVRE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-DCOMPTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-HCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-HCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-DANNULATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-DANNULATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NLIEUORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-WINTMAJ-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-WINTMAJ-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-DSTAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-DSTAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-CPRESTGRP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-CPRESTGRP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NSOCMODIF-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NSOCMODIF-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NLIEUMODIF-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NLIEUMODIF-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-DATENC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-DATENC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-CDEV-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-CDEV-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-WUNITR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-WUNITR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-LRCMMT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-LRCMMT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NSOCP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NSOCP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NLIEUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NLIEUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NTRANS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NSEQFV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NSEQFV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NSEQNQ-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NSEQNQ-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NSEQREF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NSEQREF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NMODIF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NMODIF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NSOCORIG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NSOCORIG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-DTOPE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-DTOPE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NSOCLIVR1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NSOCLIVR1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NDEPOT1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NDEPOT1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NSOCGEST-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NSOCGEST-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NLIEUGEST-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NLIEUGEST-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NSOCDEPLIV-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NSOCDEPLIV-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NLIEUDEPLIV-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NLIEUDEPLIV-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-TYPVTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-TYPVTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-CTYPENT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-CTYPENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NLIEN-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NLIEN-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NACTVTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NACTVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-PVCODIG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-PVCODIG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-QTCODIG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-QTCODIG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-DPRLG-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-DPRLG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-CALERTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-CALERTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-CREPRISE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-CREPRISE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-NSEQENS-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-NSEQENS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI01-MPRIMECLI-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI01-MPRIMECLI-F                                                 
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
