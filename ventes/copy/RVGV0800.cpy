      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVGV0800                                     00020005
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV0800                 00060005
      *---------------------------------------------------------        00070000
      *                                                                 00080000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV0800.                                                    00090005
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV0800.                                                            
      *}                                                                        
           02  GV08-NSOCIETE                                            00100004
               PIC X(0003).                                             00110004
           02  GV08-NLIEU                                               00120004
               PIC X(0003).                                             00130004
           02  GV08-NVENTE                                              00140004
               PIC X(0007).                                             00150004
           02  GV08-NDOSSIER                                            00160004
               PIC S9(3) COMP-3.                                        00170006
           02  GV08-NSEQ                                                00180004
               PIC S9(3) COMP-3.                                        00190002
           02  GV08-CCTRL                                               00200004
               PIC X(0005).                                             00210000
           02  GV08-LGCTRL                                              00220004
               PIC S9(3) COMP-3.                                        00230002
           02  GV08-VALCTRL                                             00240004
               PIC X(0050).                                             00250002
           02  GV08-CAUTOR                                              00260004
               PIC X(0005).                                             00270002
           02  GV08-CJUSTIF                                             00280004
               PIC X(0005).                                             00290002
           02  GV08-DMODIF                                              00300004
               PIC X(0008).                                             00310000
           02  GV08-DSYST                                               00320004
               PIC S9(13) COMP-3.                                       00330004
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00340000
      *---------------------------------------------------------        00350000
      *   LISTE DES FLAGS DE LA TABLE RVGV0800                          00360005
      *---------------------------------------------------------        00370000
      *                                                                 00380000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV0800-FLAGS.                                              00390005
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV0800-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-NSOCIETE-F                                          00400004
      *        PIC S9(4) COMP.                                          00410000
      *--                                                                       
           02  GV08-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-NLIEU-F                                             00420004
      *        PIC S9(4) COMP.                                          00430000
      *--                                                                       
           02  GV08-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-NVENTE-F                                            00440004
      *        PIC S9(4) COMP.                                          00450000
      *--                                                                       
           02  GV08-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-NDOSSIER-F                                          00460004
      *        PIC S9(4) COMP.                                          00470000
      *--                                                                       
           02  GV08-NDOSSIER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-NSEQ-F                                              00480004
      *        PIC S9(4) COMP.                                          00490000
      *--                                                                       
           02  GV08-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-CCTRL-F                                             00500004
      *        PIC S9(4) COMP.                                          00510000
      *--                                                                       
           02  GV08-CCTRL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-LGCTRL-F                                            00520004
      *        PIC S9(4) COMP.                                          00530000
      *--                                                                       
           02  GV08-LGCTRL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-VALCTRL-F                                           00540004
      *        PIC S9(4) COMP.                                          00550000
      *--                                                                       
           02  GV08-VALCTRL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-CAUTOR-F                                            00560004
      *        PIC S9(4) COMP.                                          00570000
      *--                                                                       
           02  GV08-CAUTOR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-CJUSTIF-F                                           00580004
      *        PIC S9(4) COMP.                                          00590000
      *--                                                                       
           02  GV08-CJUSTIF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-DMODIF-F                                            00600004
      *        PIC S9(4) COMP.                                          00610000
      *--                                                                       
           02  GV08-DMODIF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-DSYST-F                                             00620004
      *        PIC S9(4) COMP.                                          00630004
      *--                                                                       
           02  GV08-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                            00640000
                                                                                
