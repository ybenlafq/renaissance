      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
           EJECT                                                        00000010
      **********************************************************        00000020
      *   COPY DE LA TABLE RVCE1002                                     00000030
      **********************************************************        00000040
      *   LISTE DES HOST VARIABLES DE LA TABLE RVCE1002                 00000050
      **********************************************************        00000060
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCE1002.                                                    00000070
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCE1002.                                                            
      *}                                                                        
           02  CE10-CFAM                                                00000080
               PIC X(0005).                                             00000090
           02  CE10-WSEQFAM                                             00000100
               PIC S9(5) COMP-3.                                        00000110
           02  CE10-NCODIC                                              00000120
               PIC X(0007).                                             00000130
           02  CE10-CMARQ                                               00000140
               PIC X(0005).                                             00000150
           02  CE10-LREFFOURN                                           00000160
               PIC X(0020).                                             00000170
           02  CE10-NZONPRIX                                            00000180
               PIC X(0002).                                             00000190
           02  CE10-NSOCIETE                                            00000200
               PIC X(0003).                                             00000210
           02  CE10-NLIEU                                               00000220
               PIC X(0003).                                             00000230
           02  CE10-PSTDTTCA                                            00000240
               PIC S9(5)V9(0002) COMP-3.                                00000250
           02  CE10-PSTDTTCN                                            00000260
               PIC S9(5)V9(0002) COMP-3.                                00000270
           02  CE10-DEFFET                                              00000280
               PIC X(0008).                                             00000290
           02  CE10-DFINEFFET                                           00000300
               PIC X(0008).                                             00000310
           02  CE10-WETIQUETTE                                          00000320
               PIC X(0001).                                             00000330
           02  CE10-DSYST                                               00000340
               PIC S9(13) COMP-3.                                       00000350
           02  CE10-PRIME                                               00000360
               PIC S9(5)V9(0002) COMP-3.                                00000370
           02  CE10-CSTATUT                                             00000380
               PIC X(0005).                                             00000390
           02  CE10-CSENS                                               00000400
               PIC X(0005).                                             00000410
           02  CE10-WDACEM                                              00000411
               PIC X(0001).                                             00000412
           02  CE10-QCONDT                                              00000415
               PIC S9(5) COMP-3.                                        00000416
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************        00000420
      *   LISTE DES FLAGS DE LA TABLE RVCE1002                          00000430
      **********************************************************        00000440
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCE1002-FLAGS.                                              00000450
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCE1002-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-CFAM-F                                              00000460
      *        PIC S9(4) COMP.                                          00000470
      *--                                                                       
           02  CE10-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-WSEQFAM-F                                           00000480
      *        PIC S9(4) COMP.                                          00000490
      *--                                                                       
           02  CE10-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-NCODIC-F                                            00000500
      *        PIC S9(4) COMP.                                          00000510
      *--                                                                       
           02  CE10-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-CMARQ-F                                             00000520
      *        PIC S9(4) COMP.                                          00000530
      *--                                                                       
           02  CE10-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-LREFFOURN-F                                         00000540
      *        PIC S9(4) COMP.                                          00000550
      *--                                                                       
           02  CE10-LREFFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-NZONPRIX-F                                          00000560
      *        PIC S9(4) COMP.                                          00000570
      *--                                                                       
           02  CE10-NZONPRIX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-NSOCIETE-F                                          00000580
      *        PIC S9(4) COMP.                                          00000590
      *--                                                                       
           02  CE10-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-NLIEU-F                                             00000600
      *        PIC S9(4) COMP.                                          00000610
      *--                                                                       
           02  CE10-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-PSTDTTCA-F                                          00000620
      *        PIC S9(4) COMP.                                          00000630
      *--                                                                       
           02  CE10-PSTDTTCA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-PSTDTTCN-F                                          00000640
      *        PIC S9(4) COMP.                                          00000650
      *--                                                                       
           02  CE10-PSTDTTCN-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-DEFFET-F                                            00000660
      *        PIC S9(4) COMP.                                          00000670
      *--                                                                       
           02  CE10-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-DFINEFFET-F                                         00000680
      *        PIC S9(4) COMP.                                          00000690
      *--                                                                       
           02  CE10-DFINEFFET-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-WETIQUETTE-F                                        00000700
      *        PIC S9(4) COMP.                                          00000710
      *--                                                                       
           02  CE10-WETIQUETTE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-DSYST-F                                             00000720
      *        PIC S9(4) COMP.                                          00000730
      *--                                                                       
           02  CE10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-PRIME-F                                             00000740
      *        PIC S9(4) COMP.                                          00000750
      *--                                                                       
           02  CE10-PRIME-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-CSTATUT-F                                           00000760
      *        PIC S9(4) COMP.                                          00000770
      *--                                                                       
           02  CE10-CSTATUT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-CSENS-F                                             00000780
      *        PIC S9(4) COMP.                                          00000790
      *--                                                                       
           02  CE10-CSENS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-WDACEM-F                                            00000800
      *        PIC S9(4) COMP.                                          00000900
      *--                                                                       
           02  CE10-WDACEM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CE10-QCONDT-F                                            00001200
      *        PIC S9(4) COMP.                                          00001300
      *                                                                         
      *--                                                                       
           02  CE10-QCONDT-F                                                    
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
