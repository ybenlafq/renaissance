      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAV13   EAV13                                              00000020
      ***************************************************************** 00000030
       01   EAV13I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      * n�societe emettrice de l'avoir                                  00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCSAISIEL   COMP PIC S9(4).                            00000150
      *--                                                                       
           02 MNSOCSAISIEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNSOCSAISIEF   PIC X.                                     00000160
           02 FILLER    PIC X(4).                                       00000170
           02 MNSOCSAISIEI   PIC X(3).                                  00000180
      * n�lieu  "  "  "  "  "  "  "                                     00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUSAISIEL  COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MNLIEUSAISIEL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNLIEUSAISIEF  PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNLIEUSAISIEI  PIC X(3).                                  00000230
      * libelle lieu                                                    00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUSAISIEL  COMP PIC S9(4).                            00000250
      *--                                                                       
           02 MLLIEUSAISIEL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MLLIEUSAISIEF  PIC X.                                     00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MLLIEUSAISIEI  PIC X(20).                                 00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUAFFL     COMP PIC S9(4).                            00000290
      *--                                                                       
           02 MNLIEUAFFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNLIEUAFFF     PIC X.                                     00000300
           02 FILLER    PIC X(4).                                       00000310
           02 MNLIEUAFFI     PIC X(3).                                  00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUAFFL     COMP PIC S9(4).                            00000330
      *--                                                                       
           02 MLLIEUAFFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLLIEUAFFF     PIC X.                                     00000340
           02 FILLER    PIC X(4).                                       00000350
           02 MLLIEUAFFI     PIC X(20).                                 00000360
      * n�d'avoir                                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAVOIRL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNAVOIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNAVOIRF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNAVOIRI  PIC X(6).                                       00000410
      * modulo                                                          00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMODULOL  COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MMODULOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMODULOF  PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MMODULOI  PIC X.                                          00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPAVOIRL    COMP PIC S9(4).                            00000470
      *--                                                                       
           02 MCTYPAVOIRL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCTYPAVOIRF    PIC X.                                     00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCTYPAVOIRI    PIC X.                                     00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPAVOIRL    COMP PIC S9(4).                            00000510
      *--                                                                       
           02 MLTYPAVOIRL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLTYPAVOIRF    PIC X.                                     00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MLTYPAVOIRI    PIC X(20).                                 00000540
      * code type utilisation                                           00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPUTILL     COMP PIC S9(4).                            00000560
      *--                                                                       
           02 MCTYPUTILL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPUTILF     PIC X.                                     00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MCTYPUTILI     PIC X(5).                                  00000590
      * libelle type utilisation                                        00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPUTILL     COMP PIC S9(4).                            00000610
      *--                                                                       
           02 MLTYPUTILL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLTYPUTILF     PIC X.                                     00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MLTYPUTILI     PIC X(20).                                 00000640
      * code motif                                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMOTIFL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCMOTIFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMOTIFF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCMOTIFI  PIC X(5).                                       00000690
      * libelle motif                                                   00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMOTIFL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLMOTIFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMOTIFF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLMOTIFI  PIC X(20).                                      00000740
      * montant de l'avoir                                              00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAVOIRL  COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MPAVOIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPAVOIRF  PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MPAVOIRI  PIC X(10).                                      00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00000800
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MCDEVISEI      PIC X(3).                                  00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEVISEL      COMP PIC S9(4).                            00000840
      *--                                                                       
           02 MLDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDEVISEF      PIC X.                                     00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MLDEVISEI      PIC X(5).                                  00000870
      * titre nom                                                       00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTITRENOML    COMP PIC S9(4).                            00000890
      *--                                                                       
           02 MCTITRENOML COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCTITRENOMF    PIC X.                                     00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MCTITRENOMI    PIC X(5).                                  00000920
      * nom                                                             00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MLNOMI    PIC X(25).                                      00000970
      * prenom                                                          00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPRENOML      COMP PIC S9(4).                            00000990
      *--                                                                       
           02 MLPRENOML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLPRENOMF      PIC X.                                     00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MLPRENOMI      PIC X(15).                                 00001020
      * n� de voie                                                      00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVOIEL   COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MCVOIEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCVOIEF   PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MCVOIEI   PIC X(5).                                       00001070
      * code type voie                                                  00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTVOIEL  COMP PIC S9(4).                                 00001090
      *--                                                                       
           02 MCTVOIEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTVOIEF  PIC X.                                          00001100
           02 FILLER    PIC X(4).                                       00001110
           02 MCTVOIEI  PIC X(4).                                       00001120
      * nom de voie                                                     00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMVOIEL     COMP PIC S9(4).                            00001140
      *--                                                                       
           02 MLNOMVOIEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLNOMVOIEF     PIC X.                                     00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MLNOMVOIEI     PIC X(20).                                 00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTALL      COMP PIC S9(4).                            00001180
      *--                                                                       
           02 MCPOSTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPOSTALF      PIC X.                                     00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MCPOSTALI      PIC X(5).                                  00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMUNEL     COMP PIC S9(4).                            00001220
      *--                                                                       
           02 MLCOMMUNEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCOMMUNEF     PIC X.                                     00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MLCOMMUNEI     PIC X(32).                                 00001250
      * bureau de distribution                                          00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLBUREAUL      COMP PIC S9(4).                            00001270
      *--                                                                       
           02 MLBUREAUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLBUREAUF      PIC X.                                     00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MLBUREAUI      PIC X(26).                                 00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MLIBERRI  PIC X(78).                                      00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MCODTRAI  PIC X(4).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MCICSI    PIC X(5).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MNETNAMI  PIC X(8).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MSCREENI  PIC X(4).                                       00001500
      ***************************************************************** 00001510
      * SDF: EAV13   EAV13                                              00001520
      ***************************************************************** 00001530
       01   EAV13O REDEFINES EAV13I.                                    00001540
           02 FILLER    PIC X(12).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MDATJOUA  PIC X.                                          00001570
           02 MDATJOUC  PIC X.                                          00001580
           02 MDATJOUP  PIC X.                                          00001590
           02 MDATJOUH  PIC X.                                          00001600
           02 MDATJOUV  PIC X.                                          00001610
           02 MDATJOUO  PIC X(10).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MTIMJOUA  PIC X.                                          00001640
           02 MTIMJOUC  PIC X.                                          00001650
           02 MTIMJOUP  PIC X.                                          00001660
           02 MTIMJOUH  PIC X.                                          00001670
           02 MTIMJOUV  PIC X.                                          00001680
           02 MTIMJOUO  PIC X(5).                                       00001690
      * n�societe emettrice de l'avoir                                  00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MNSOCSAISIEA   PIC X.                                     00001720
           02 MNSOCSAISIEC   PIC X.                                     00001730
           02 MNSOCSAISIEP   PIC X.                                     00001740
           02 MNSOCSAISIEH   PIC X.                                     00001750
           02 MNSOCSAISIEV   PIC X.                                     00001760
           02 MNSOCSAISIEO   PIC X(3).                                  00001770
      * n�lieu  "  "  "  "  "  "  "                                     00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MNLIEUSAISIEA  PIC X.                                     00001800
           02 MNLIEUSAISIEC  PIC X.                                     00001810
           02 MNLIEUSAISIEP  PIC X.                                     00001820
           02 MNLIEUSAISIEH  PIC X.                                     00001830
           02 MNLIEUSAISIEV  PIC X.                                     00001840
           02 MNLIEUSAISIEO  PIC X(3).                                  00001850
      * libelle lieu                                                    00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLLIEUSAISIEA  PIC X.                                     00001880
           02 MLLIEUSAISIEC  PIC X.                                     00001890
           02 MLLIEUSAISIEP  PIC X.                                     00001900
           02 MLLIEUSAISIEH  PIC X.                                     00001910
           02 MLLIEUSAISIEV  PIC X.                                     00001920
           02 MLLIEUSAISIEO  PIC X(20).                                 00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MNLIEUAFFA     PIC X.                                     00001950
           02 MNLIEUAFFC     PIC X.                                     00001960
           02 MNLIEUAFFP     PIC X.                                     00001970
           02 MNLIEUAFFH     PIC X.                                     00001980
           02 MNLIEUAFFV     PIC X.                                     00001990
           02 MNLIEUAFFO     PIC X(3).                                  00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MLLIEUAFFA     PIC X.                                     00002020
           02 MLLIEUAFFC     PIC X.                                     00002030
           02 MLLIEUAFFP     PIC X.                                     00002040
           02 MLLIEUAFFH     PIC X.                                     00002050
           02 MLLIEUAFFV     PIC X.                                     00002060
           02 MLLIEUAFFO     PIC X(20).                                 00002070
      * n�d'avoir                                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MNAVOIRA  PIC X.                                          00002100
           02 MNAVOIRC  PIC X.                                          00002110
           02 MNAVOIRP  PIC X.                                          00002120
           02 MNAVOIRH  PIC X.                                          00002130
           02 MNAVOIRV  PIC X.                                          00002140
           02 MNAVOIRO  PIC X(6).                                       00002150
      * modulo                                                          00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MMODULOA  PIC X.                                          00002180
           02 MMODULOC  PIC X.                                          00002190
           02 MMODULOP  PIC X.                                          00002200
           02 MMODULOH  PIC X.                                          00002210
           02 MMODULOV  PIC X.                                          00002220
           02 MMODULOO  PIC X.                                          00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MCTYPAVOIRA    PIC X.                                     00002250
           02 MCTYPAVOIRC    PIC X.                                     00002260
           02 MCTYPAVOIRP    PIC X.                                     00002270
           02 MCTYPAVOIRH    PIC X.                                     00002280
           02 MCTYPAVOIRV    PIC X.                                     00002290
           02 MCTYPAVOIRO    PIC X.                                     00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLTYPAVOIRA    PIC X.                                     00002320
           02 MLTYPAVOIRC    PIC X.                                     00002330
           02 MLTYPAVOIRP    PIC X.                                     00002340
           02 MLTYPAVOIRH    PIC X.                                     00002350
           02 MLTYPAVOIRV    PIC X.                                     00002360
           02 MLTYPAVOIRO    PIC X(20).                                 00002370
      * code type utilisation                                           00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MCTYPUTILA     PIC X.                                     00002400
           02 MCTYPUTILC     PIC X.                                     00002410
           02 MCTYPUTILP     PIC X.                                     00002420
           02 MCTYPUTILH     PIC X.                                     00002430
           02 MCTYPUTILV     PIC X.                                     00002440
           02 MCTYPUTILO     PIC X(5).                                  00002450
      * libelle type utilisation                                        00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MLTYPUTILA     PIC X.                                     00002480
           02 MLTYPUTILC     PIC X.                                     00002490
           02 MLTYPUTILP     PIC X.                                     00002500
           02 MLTYPUTILH     PIC X.                                     00002510
           02 MLTYPUTILV     PIC X.                                     00002520
           02 MLTYPUTILO     PIC X(20).                                 00002530
      * code motif                                                      00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MCMOTIFA  PIC X.                                          00002560
           02 MCMOTIFC  PIC X.                                          00002570
           02 MCMOTIFP  PIC X.                                          00002580
           02 MCMOTIFH  PIC X.                                          00002590
           02 MCMOTIFV  PIC X.                                          00002600
           02 MCMOTIFO  PIC X(5).                                       00002610
      * libelle motif                                                   00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MLMOTIFA  PIC X.                                          00002640
           02 MLMOTIFC  PIC X.                                          00002650
           02 MLMOTIFP  PIC X.                                          00002660
           02 MLMOTIFH  PIC X.                                          00002670
           02 MLMOTIFV  PIC X.                                          00002680
           02 MLMOTIFO  PIC X(20).                                      00002690
      * montant de l'avoir                                              00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MPAVOIRA  PIC X.                                          00002720
           02 MPAVOIRC  PIC X.                                          00002730
           02 MPAVOIRP  PIC X.                                          00002740
           02 MPAVOIRH  PIC X.                                          00002750
           02 MPAVOIRV  PIC X.                                          00002760
           02 MPAVOIRO  PIC X(10).                                      00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MCDEVISEA      PIC X.                                     00002790
           02 MCDEVISEC PIC X.                                          00002800
           02 MCDEVISEP PIC X.                                          00002810
           02 MCDEVISEH PIC X.                                          00002820
           02 MCDEVISEV PIC X.                                          00002830
           02 MCDEVISEO      PIC X(3).                                  00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MLDEVISEA      PIC X.                                     00002860
           02 MLDEVISEC PIC X.                                          00002870
           02 MLDEVISEP PIC X.                                          00002880
           02 MLDEVISEH PIC X.                                          00002890
           02 MLDEVISEV PIC X.                                          00002900
           02 MLDEVISEO      PIC X(5).                                  00002910
      * titre nom                                                       00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MCTITRENOMA    PIC X.                                     00002940
           02 MCTITRENOMC    PIC X.                                     00002950
           02 MCTITRENOMP    PIC X.                                     00002960
           02 MCTITRENOMH    PIC X.                                     00002970
           02 MCTITRENOMV    PIC X.                                     00002980
           02 MCTITRENOMO    PIC X(5).                                  00002990
      * nom                                                             00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MLNOMA    PIC X.                                          00003020
           02 MLNOMC    PIC X.                                          00003030
           02 MLNOMP    PIC X.                                          00003040
           02 MLNOMH    PIC X.                                          00003050
           02 MLNOMV    PIC X.                                          00003060
           02 MLNOMO    PIC X(25).                                      00003070
      * prenom                                                          00003080
           02 FILLER    PIC X(2).                                       00003090
           02 MLPRENOMA      PIC X.                                     00003100
           02 MLPRENOMC PIC X.                                          00003110
           02 MLPRENOMP PIC X.                                          00003120
           02 MLPRENOMH PIC X.                                          00003130
           02 MLPRENOMV PIC X.                                          00003140
           02 MLPRENOMO      PIC X(15).                                 00003150
      * n� de voie                                                      00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MCVOIEA   PIC X.                                          00003180
           02 MCVOIEC   PIC X.                                          00003190
           02 MCVOIEP   PIC X.                                          00003200
           02 MCVOIEH   PIC X.                                          00003210
           02 MCVOIEV   PIC X.                                          00003220
           02 MCVOIEO   PIC X(5).                                       00003230
      * code type voie                                                  00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MCTVOIEA  PIC X.                                          00003260
           02 MCTVOIEC  PIC X.                                          00003270
           02 MCTVOIEP  PIC X.                                          00003280
           02 MCTVOIEH  PIC X.                                          00003290
           02 MCTVOIEV  PIC X.                                          00003300
           02 MCTVOIEO  PIC X(4).                                       00003310
      * nom de voie                                                     00003320
           02 FILLER    PIC X(2).                                       00003330
           02 MLNOMVOIEA     PIC X.                                     00003340
           02 MLNOMVOIEC     PIC X.                                     00003350
           02 MLNOMVOIEP     PIC X.                                     00003360
           02 MLNOMVOIEH     PIC X.                                     00003370
           02 MLNOMVOIEV     PIC X.                                     00003380
           02 MLNOMVOIEO     PIC X(20).                                 00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MCPOSTALA      PIC X.                                     00003410
           02 MCPOSTALC PIC X.                                          00003420
           02 MCPOSTALP PIC X.                                          00003430
           02 MCPOSTALH PIC X.                                          00003440
           02 MCPOSTALV PIC X.                                          00003450
           02 MCPOSTALO      PIC X(5).                                  00003460
           02 FILLER    PIC X(2).                                       00003470
           02 MLCOMMUNEA     PIC X.                                     00003480
           02 MLCOMMUNEC     PIC X.                                     00003490
           02 MLCOMMUNEP     PIC X.                                     00003500
           02 MLCOMMUNEH     PIC X.                                     00003510
           02 MLCOMMUNEV     PIC X.                                     00003520
           02 MLCOMMUNEO     PIC X(32).                                 00003530
      * bureau de distribution                                          00003540
           02 FILLER    PIC X(2).                                       00003550
           02 MLBUREAUA      PIC X.                                     00003560
           02 MLBUREAUC PIC X.                                          00003570
           02 MLBUREAUP PIC X.                                          00003580
           02 MLBUREAUH PIC X.                                          00003590
           02 MLBUREAUV PIC X.                                          00003600
           02 MLBUREAUO      PIC X(26).                                 00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MLIBERRA  PIC X.                                          00003630
           02 MLIBERRC  PIC X.                                          00003640
           02 MLIBERRP  PIC X.                                          00003650
           02 MLIBERRH  PIC X.                                          00003660
           02 MLIBERRV  PIC X.                                          00003670
           02 MLIBERRO  PIC X(78).                                      00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MCODTRAA  PIC X.                                          00003700
           02 MCODTRAC  PIC X.                                          00003710
           02 MCODTRAP  PIC X.                                          00003720
           02 MCODTRAH  PIC X.                                          00003730
           02 MCODTRAV  PIC X.                                          00003740
           02 MCODTRAO  PIC X(4).                                       00003750
           02 FILLER    PIC X(2).                                       00003760
           02 MCICSA    PIC X.                                          00003770
           02 MCICSC    PIC X.                                          00003780
           02 MCICSP    PIC X.                                          00003790
           02 MCICSH    PIC X.                                          00003800
           02 MCICSV    PIC X.                                          00003810
           02 MCICSO    PIC X(5).                                       00003820
           02 FILLER    PIC X(2).                                       00003830
           02 MNETNAMA  PIC X.                                          00003840
           02 MNETNAMC  PIC X.                                          00003850
           02 MNETNAMP  PIC X.                                          00003860
           02 MNETNAMH  PIC X.                                          00003870
           02 MNETNAMV  PIC X.                                          00003880
           02 MNETNAMO  PIC X(8).                                       00003890
           02 FILLER    PIC X(2).                                       00003900
           02 MSCREENA  PIC X.                                          00003910
           02 MSCREENC  PIC X.                                          00003920
           02 MSCREENP  PIC X.                                          00003930
           02 MSCREENH  PIC X.                                          00003940
           02 MSCREENV  PIC X.                                          00003950
           02 MSCREENO  PIC X(4).                                       00003960
                                                                                
