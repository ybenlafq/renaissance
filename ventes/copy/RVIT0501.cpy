      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVIT0500                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIT0500                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIT0500.                                                    00000090
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIT0500.                                                            
      *}                                                                        
           02  IT05-NINVENTAIRE                                         00000100
               PIC X(0005).                                             00000110
           02  IT05-CTYPLIEU                                            00000120
               PIC X(0001).                                             00000130
           02  IT05-NLIEU                                               00000140
               PIC X(0003).                                             00000150
           02  IT05-WFININV                                             00000160
               PIC X(0001).                                             00000170
           02  IT05-WEDITE                                              00000180
               PIC X(0001).                                             00000190
           02  IT05-DSYST                                               00000200
               PIC S9(13) COMP-3.                                       00000210
           02  IT05-NSOCIETE                                            00000220
               PIC X(0003).                                             00000230
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00000240
      *---------------------------------------------------------        00000250
      *   LISTE DES FLAGS DE LA TABLE RVIT0500                          00000260
      *---------------------------------------------------------        00000270
      *                                                                 00000280
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIT0500-FLAGS.                                              00000290
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIT0500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT05-NINVENTAIRE-F                                       00000300
      *        PIC S9(4) COMP.                                          00000310
      *--                                                                       
           02  IT05-NINVENTAIRE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT05-CTYPLIEU-F                                          00000320
      *        PIC S9(4) COMP.                                          00000330
      *--                                                                       
           02  IT05-CTYPLIEU-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT05-NLIEU-F                                             00000340
      *        PIC S9(4) COMP.                                          00000350
      *--                                                                       
           02  IT05-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT05-WFININV-F                                           00000360
      *        PIC S9(4) COMP.                                          00000370
      *--                                                                       
           02  IT05-WFININV-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT05-WEDITE-F                                            00000380
      *        PIC S9(4) COMP.                                          00000390
      *--                                                                       
           02  IT05-WEDITE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT05-DSYST-F                                             00000400
      *        PIC S9(4) COMP.                                          00000410
      *--                                                                       
           02  IT05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT05-NSOCIETE-F                                          00000420
      *        PIC S9(4) COMP.                                          00000430
      *--                                                                       
           02  IT05-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                            00000440
                                                                                
