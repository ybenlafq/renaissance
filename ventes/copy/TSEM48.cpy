      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00010001
      * >>>>>>>>>>>>>>>>> DESCRIPTION DE LA TSEM48 <<<<<<<<<<<<<<<<<< * 00020003
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00030001
      *                                                               * 00040001
       01  TSEM48-IDENTIFICATEUR.                                       00050003
           05  TSEM48-TRANSID               PIC X(04) VALUE 'EM48'.     00060004
           05  TSEM48-NUMERO-DE-TERMINAL    PIC X(04) VALUE SPACES.     00070003
      *                                                               * 00080001
       01  TSEM48-ITEM.                                                 00090003
JD         05  TSEM48-LONGUEUR              PIC S9(4) VALUE +1200.      00100003
           05  TSEM48-DATAS.                                            00110003
               10  TSEM48-LIGNE             OCCURS 30.                  00120003
      *     DESCRIPTION REGLEMENTS TRANSACTIONS                       * 00130000
                       25 TSEM48-CODE              PIC X(05).           00140003
                       25 TSEM48-LIBELLE           PIC X(20).           00150003
B&S                    25 TSEM48-PMONTSAIS         PIC S9(07)V99 COMP-3.00160003
JDB   *                25 TSEM48-PMONTAUT          PIC S9(07)V99 COMP-3.00170003
JDB                    25 TSEM48-PMONTECAR         PIC S9(07)V99 COMP-3.00180003
               10  TSEM48-PRECFACT                 PIC S9(07)V99 COMP-3.00190003
      *                                                               * 00200000
      *                                                               * 00210000
      *                                                               * 00220000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00230001
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< * 00240001
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00250001
                                                                                
