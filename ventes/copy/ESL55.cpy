      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV55   EGV55                                              00000020
      ***************************************************************** 00000030
       01   ESL55I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * NUMERO DE PAGE COURANTE                                         00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOPAGEL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MNOPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOPAGEF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MNOPAGEI  PIC X(3).                                       00000200
      * NOMBRE TOTAL DE PAGES                                           00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGEL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPAGEF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBPAGEI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRITVL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCRITVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRITVF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCRITVI   PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRITCL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCRITCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRITCF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCRITCI   PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRITSL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCRITSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRITSF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCRITSI   PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRITLL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCRITLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRITLF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCRITLI   PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRITAL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCRITAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRITAF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCRITAI   PIC X.                                          00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRITDDL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCRITDDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCRITDDF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCRITDDI  PIC X(8).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRITDFL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCRITDFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCRITDFF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCRITDFI  PIC X(8).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRITEL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCRITEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRITEF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCRITEI   PIC X.                                          00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRITSTAL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MCRITSTAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCRITSTAF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCRITSTAI      PIC X.                                     00000610
           02 MLIGNEI OCCURS   10 TIMES .                               00000620
      * ZONE DE SELECTION                                               00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSELECTL   COMP PIC S9(4).                            00000640
      *--                                                                       
               04 MSELECTL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MSELECTF   PIC X.                                     00000650
               04 FILLER     PIC X(4).                                  00000660
               04 MSELECTI   PIC X.                                     00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNCODICL   COMP PIC S9(4).                            00000680
      *--                                                                       
               04 MNCODICL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MNCODICF   PIC X.                                     00000690
               04 FILLER     PIC X(4).                                  00000700
               04 MNCODICI   PIC X(7).                                  00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCFAML     COMP PIC S9(4).                            00000720
      *--                                                                       
               04 MCFAML COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MCFAMF     PIC X.                                     00000730
               04 FILLER     PIC X(4).                                  00000740
               04 MCFAMI     PIC X(5).                                  00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MDTYL      COMP PIC S9(4).                            00000760
      *--                                                                       
               04 MDTYL COMP-5 PIC S9(4).                                       
      *}                                                                        
               04 MDTYF      PIC X.                                     00000770
               04 FILLER     PIC X(4).                                  00000780
               04 MDTYI      PIC X.                                     00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSTAL      COMP PIC S9(4).                            00000800
      *--                                                                       
               04 MSTAL COMP-5 PIC S9(4).                                       
      *}                                                                        
               04 MSTAF      PIC X.                                     00000810
               04 FILLER     PIC X(4).                                  00000820
               04 MSTAI      PIC X(3).                                  00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MQTL  COMP PIC S9(4).                                 00000840
      *--                                                                       
               04 MQTL COMP-5 PIC S9(4).                                        
      *}                                                                        
               04 MQTF  PIC X.                                          00000850
               04 FILLER     PIC X(4).                                  00000860
               04 MQTI  PIC X(4).                                       00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MQTDEPL    COMP PIC S9(4).                            00000880
      *--                                                                       
               04 MQTDEPL COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MQTDEPF    PIC X.                                     00000890
               04 FILLER     PIC X(4).                                  00000900
               04 MQTDEPI    PIC X(5).                                  00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MQTMAL     COMP PIC S9(4).                            00000920
      *--                                                                       
               04 MQTMAL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MQTMAF     PIC X.                                     00000930
               04 FILLER     PIC X(4).                                  00000940
               04 MQTMAI     PIC X(3).                                  00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MDATEFL    COMP PIC S9(4).                            00000960
      *--                                                                       
               04 MDATEFL COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MDATEFF    PIC X.                                     00000970
               04 FILLER     PIC X(4).                                  00000980
               04 MDATEFI    PIC X(6).                                  00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MACTL      COMP PIC S9(4).                            00001000
      *--                                                                       
               04 MACTL COMP-5 PIC S9(4).                                       
      *}                                                                        
               04 MACTF      PIC X.                                     00001010
               04 FILLER     PIC X(4).                                  00001020
               04 MACTI      PIC X.                                     00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNBVL      COMP PIC S9(4).                            00001040
      *--                                                                       
               04 MNBVL COMP-5 PIC S9(4).                                       
      *}                                                                        
               04 MNBVF      PIC X.                                     00001050
               04 FILLER     PIC X(4).                                  00001060
               04 MNBVI      PIC X(3).                                  00001070
      * N� de vente                                                     00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNVENTEL   COMP PIC S9(4).                            00001090
      *--                                                                       
               04 MNVENTEL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MNVENTEF   PIC X.                                     00001100
               04 FILLER     PIC X(4).                                  00001110
               04 MNVENTEI   PIC X(14).                                 00001120
      * MESSAGE ERREUR                                                  00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MLIBERRI  PIC X(78).                                      00001170
      * CODE TRANSACTION                                                00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCODTRAI  PIC X(4).                                       00001220
      * CICS DE TRAVAIL                                                 00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001240
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001250
           02 FILLER    PIC X(4).                                       00001260
           02 MCICSI    PIC X(5).                                       00001270
      * NETNAME                                                         00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001290
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001300
           02 FILLER    PIC X(4).                                       00001310
           02 MNETNAMI  PIC X(8).                                       00001320
      * CODE TERMINAL                                                   00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MSCREENI  PIC X(5).                                       00001370
      ***************************************************************** 00001380
      * SDF: EGV55   EGV55                                              00001390
      ***************************************************************** 00001400
       01   ESL55O REDEFINES ESL55I.                                    00001410
           02 FILLER    PIC X(12).                                      00001420
      * DATE DU JOUR                                                    00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MDATJOUA  PIC X.                                          00001450
           02 MDATJOUC  PIC X.                                          00001460
           02 MDATJOUP  PIC X.                                          00001470
           02 MDATJOUH  PIC X.                                          00001480
           02 MDATJOUV  PIC X.                                          00001490
           02 MDATJOUO  PIC X(10).                                      00001500
      * HEURE                                                           00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MTIMJOUA  PIC X.                                          00001530
           02 MTIMJOUC  PIC X.                                          00001540
           02 MTIMJOUP  PIC X.                                          00001550
           02 MTIMJOUH  PIC X.                                          00001560
           02 MTIMJOUV  PIC X.                                          00001570
           02 MTIMJOUO  PIC X(5).                                       00001580
      * NUMERO DE PAGE COURANTE                                         00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MNOPAGEA  PIC X.                                          00001610
           02 MNOPAGEC  PIC X.                                          00001620
           02 MNOPAGEP  PIC X.                                          00001630
           02 MNOPAGEH  PIC X.                                          00001640
           02 MNOPAGEV  PIC X.                                          00001650
           02 MNOPAGEO  PIC X(3).                                       00001660
      * NOMBRE TOTAL DE PAGES                                           00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MNBPAGEA  PIC X.                                          00001690
           02 MNBPAGEC  PIC X.                                          00001700
           02 MNBPAGEP  PIC X.                                          00001710
           02 MNBPAGEH  PIC X.                                          00001720
           02 MNBPAGEV  PIC X.                                          00001730
           02 MNBPAGEO  PIC X(3).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MCRITVA   PIC X.                                          00001760
           02 MCRITVC   PIC X.                                          00001770
           02 MCRITVP   PIC X.                                          00001780
           02 MCRITVH   PIC X.                                          00001790
           02 MCRITVV   PIC X.                                          00001800
           02 MCRITVO   PIC X(7).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MCRITCA   PIC X.                                          00001830
           02 MCRITCC   PIC X.                                          00001840
           02 MCRITCP   PIC X.                                          00001850
           02 MCRITCH   PIC X.                                          00001860
           02 MCRITCV   PIC X.                                          00001870
           02 MCRITCO   PIC X(7).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MCRITSA   PIC X.                                          00001900
           02 MCRITSC   PIC X.                                          00001910
           02 MCRITSP   PIC X.                                          00001920
           02 MCRITSH   PIC X.                                          00001930
           02 MCRITSV   PIC X.                                          00001940
           02 MCRITSO   PIC X(3).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MCRITLA   PIC X.                                          00001970
           02 MCRITLC   PIC X.                                          00001980
           02 MCRITLP   PIC X.                                          00001990
           02 MCRITLH   PIC X.                                          00002000
           02 MCRITLV   PIC X.                                          00002010
           02 MCRITLO   PIC X(3).                                       00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MCRITAA   PIC X.                                          00002040
           02 MCRITAC   PIC X.                                          00002050
           02 MCRITAP   PIC X.                                          00002060
           02 MCRITAH   PIC X.                                          00002070
           02 MCRITAV   PIC X.                                          00002080
           02 MCRITAO   PIC X.                                          00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MCRITDDA  PIC X.                                          00002110
           02 MCRITDDC  PIC X.                                          00002120
           02 MCRITDDP  PIC X.                                          00002130
           02 MCRITDDH  PIC X.                                          00002140
           02 MCRITDDV  PIC X.                                          00002150
           02 MCRITDDO  PIC X(8).                                       00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MCRITDFA  PIC X.                                          00002180
           02 MCRITDFC  PIC X.                                          00002190
           02 MCRITDFP  PIC X.                                          00002200
           02 MCRITDFH  PIC X.                                          00002210
           02 MCRITDFV  PIC X.                                          00002220
           02 MCRITDFO  PIC X(8).                                       00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MCRITEA   PIC X.                                          00002250
           02 MCRITEC   PIC X.                                          00002260
           02 MCRITEP   PIC X.                                          00002270
           02 MCRITEH   PIC X.                                          00002280
           02 MCRITEV   PIC X.                                          00002290
           02 MCRITEO   PIC X.                                          00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MCRITSTAA      PIC X.                                     00002320
           02 MCRITSTAC PIC X.                                          00002330
           02 MCRITSTAP PIC X.                                          00002340
           02 MCRITSTAH PIC X.                                          00002350
           02 MCRITSTAV PIC X.                                          00002360
           02 MCRITSTAO      PIC X.                                     00002370
           02 MLIGNEO OCCURS   10 TIMES .                               00002380
      * ZONE DE SELECTION                                               00002390
               04 FILLER     PIC X(2).                                  00002400
               04 MSELECTA   PIC X.                                     00002410
               04 MSELECTC   PIC X.                                     00002420
               04 MSELECTP   PIC X.                                     00002430
               04 MSELECTH   PIC X.                                     00002440
               04 MSELECTV   PIC X.                                     00002450
               04 MSELECTO   PIC X.                                     00002460
               04 FILLER     PIC X(2).                                  00002470
               04 MNCODICA   PIC X.                                     00002480
               04 MNCODICC   PIC X.                                     00002490
               04 MNCODICP   PIC X.                                     00002500
               04 MNCODICH   PIC X.                                     00002510
               04 MNCODICV   PIC X.                                     00002520
               04 MNCODICO   PIC X(7).                                  00002530
               04 FILLER     PIC X(2).                                  00002540
               04 MCFAMA     PIC X.                                     00002550
               04 MCFAMC     PIC X.                                     00002560
               04 MCFAMP     PIC X.                                     00002570
               04 MCFAMH     PIC X.                                     00002580
               04 MCFAMV     PIC X.                                     00002590
               04 MCFAMO     PIC X(5).                                  00002600
               04 FILLER     PIC X(2).                                  00002610
               04 MDTYA      PIC X.                                     00002620
               04 MDTYC PIC X.                                          00002630
               04 MDTYP PIC X.                                          00002640
               04 MDTYH PIC X.                                          00002650
               04 MDTYV PIC X.                                          00002660
               04 MDTYO      PIC X.                                     00002670
               04 FILLER     PIC X(2).                                  00002680
               04 MSTAA      PIC X.                                     00002690
               04 MSTAC PIC X.                                          00002700
               04 MSTAP PIC X.                                          00002710
               04 MSTAH PIC X.                                          00002720
               04 MSTAV PIC X.                                          00002730
               04 MSTAO      PIC X(3).                                  00002740
               04 FILLER     PIC X(2).                                  00002750
               04 MQTA  PIC X.                                          00002760
               04 MQTC  PIC X.                                          00002770
               04 MQTP  PIC X.                                          00002780
               04 MQTH  PIC X.                                          00002790
               04 MQTV  PIC X.                                          00002800
               04 MQTO  PIC ZZZ9.                                       00002810
               04 FILLER     PIC X(2).                                  00002820
               04 MQTDEPA    PIC X.                                     00002830
               04 MQTDEPC    PIC X.                                     00002840
               04 MQTDEPP    PIC X.                                     00002850
               04 MQTDEPH    PIC X.                                     00002860
               04 MQTDEPV    PIC X.                                     00002870
               04 MQTDEPO    PIC ZZZZ9.                                 00002880
               04 FILLER     PIC X(2).                                  00002890
               04 MQTMAA     PIC X.                                     00002900
               04 MQTMAC     PIC X.                                     00002910
               04 MQTMAP     PIC X.                                     00002920
               04 MQTMAH     PIC X.                                     00002930
               04 MQTMAV     PIC X.                                     00002940
               04 MQTMAO     PIC ZZ9.                                   00002950
               04 FILLER     PIC X(2).                                  00002960
               04 MDATEFA    PIC X.                                     00002970
               04 MDATEFC    PIC X.                                     00002980
               04 MDATEFP    PIC X.                                     00002990
               04 MDATEFH    PIC X.                                     00003000
               04 MDATEFV    PIC X.                                     00003010
               04 MDATEFO    PIC X(6).                                  00003020
               04 FILLER     PIC X(2).                                  00003030
               04 MACTA      PIC X.                                     00003040
               04 MACTC PIC X.                                          00003050
               04 MACTP PIC X.                                          00003060
               04 MACTH PIC X.                                          00003070
               04 MACTV PIC X.                                          00003080
               04 MACTO      PIC X.                                     00003090
               04 FILLER     PIC X(2).                                  00003100
               04 MNBVA      PIC X.                                     00003110
               04 MNBVC PIC X.                                          00003120
               04 MNBVP PIC X.                                          00003130
               04 MNBVH PIC X.                                          00003140
               04 MNBVV PIC X.                                          00003150
               04 MNBVO      PIC X(3).                                  00003160
      * N� de vente                                                     00003170
               04 FILLER     PIC X(2).                                  00003180
               04 MNVENTEA   PIC X.                                     00003190
               04 MNVENTEC   PIC X.                                     00003200
               04 MNVENTEP   PIC X.                                     00003210
               04 MNVENTEH   PIC X.                                     00003220
               04 MNVENTEV   PIC X.                                     00003230
               04 MNVENTEO   PIC X(14).                                 00003240
      * MESSAGE ERREUR                                                  00003250
           02 FILLER    PIC X(2).                                       00003260
           02 MLIBERRA  PIC X.                                          00003270
           02 MLIBERRC  PIC X.                                          00003280
           02 MLIBERRP  PIC X.                                          00003290
           02 MLIBERRH  PIC X.                                          00003300
           02 MLIBERRV  PIC X.                                          00003310
           02 MLIBERRO  PIC X(78).                                      00003320
      * CODE TRANSACTION                                                00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MCODTRAA  PIC X.                                          00003350
           02 MCODTRAC  PIC X.                                          00003360
           02 MCODTRAP  PIC X.                                          00003370
           02 MCODTRAH  PIC X.                                          00003380
           02 MCODTRAV  PIC X.                                          00003390
           02 MCODTRAO  PIC X(4).                                       00003400
      * CICS DE TRAVAIL                                                 00003410
           02 FILLER    PIC X(2).                                       00003420
           02 MCICSA    PIC X.                                          00003430
           02 MCICSC    PIC X.                                          00003440
           02 MCICSP    PIC X.                                          00003450
           02 MCICSH    PIC X.                                          00003460
           02 MCICSV    PIC X.                                          00003470
           02 MCICSO    PIC X(5).                                       00003480
      * NETNAME                                                         00003490
           02 FILLER    PIC X(2).                                       00003500
           02 MNETNAMA  PIC X.                                          00003510
           02 MNETNAMC  PIC X.                                          00003520
           02 MNETNAMP  PIC X.                                          00003530
           02 MNETNAMH  PIC X.                                          00003540
           02 MNETNAMV  PIC X.                                          00003550
           02 MNETNAMO  PIC X(8).                                       00003560
      * CODE TERMINAL                                                   00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MSCREENA  PIC X.                                          00003590
           02 MSCREENC  PIC X.                                          00003600
           02 MSCREENP  PIC X.                                          00003610
           02 MSCREENH  PIC X.                                          00003620
           02 MSCREENV  PIC X.                                          00003630
           02 MSCREENO  PIC X(5).                                       00003640
                                                                                
