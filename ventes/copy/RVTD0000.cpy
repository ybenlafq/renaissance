      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      * COBOL DECLARATION FOR TABLE RVTD0000                           *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTD0000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTD0000.                                                            
      *}                                                                        
           10 TD00-NSOCIETE        PIC X(3).                                    
           10 TD00-NLIEU           PIC X(3).                                    
           10 TD00-NVENTE          PIC X(7).                                    
           10 TD00-NDOSSIER        PIC S9(3)V USAGE COMP-3.                     
           10 TD00-CDOSSIER        PIC X(5).                                    
           10 TD00-CTYPENT         PIC X(2).                                    
           10 TD00-NENTITE         PIC X(7).                                    
           10 TD00-NSEQENS         PIC S9(3)V USAGE COMP-3.                     
           10 TD00-CAGENT          PIC X(2).                                    
           10 TD00-DCREATION       PIC X(8).                                    
           10 TD00-DMODIF          PIC X(8).                                    
           10 TD00-DANNUL          PIC X(8).                                    
           10 TD00-DSYST           PIC S9(13)V USAGE COMP-3.                    
           10 TD00-CTYPE           PIC X(5).                                    
           10 TD00-CMARQ           PIC X(5).                                    
           10 TD00-DENVOI          PIC X(8).                                    
           10 TD00-WARI            PIC X(5).                                    
           10 TD00-DARI            PIC X(8).                                    
           10 TD00-WARO            PIC X(5).                                    
           10 TD00-DARO            PIC X(8).                                    
           10 TD00-ORIGINE         PIC X(1).                                    
           10 TD00-CTITRENOM       PIC X(5).                                    
           10 TD00-LNOM            PIC X(25).                                   
           10 TD00-LPRENOM         PIC X(15).                                   
           10 TD00-WSAISIE         PIC X(5).                                    
           10 TD00-TYPDOSS         PIC X(5).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 23      *        
      ******************************************************************        
                                                                                
