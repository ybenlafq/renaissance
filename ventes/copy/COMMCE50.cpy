      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-CE50-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.          00000020
      *--                                                                       
       01  COMM-CE50-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                
      *}                                                                        
      *                                                                 00000030
       01  Z-COMMAREA.                                                  00000040
                                                                        00000050
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00000060
           02 FILLER-COM-AIDA      PIC X(100).                          00000070
                                                                        00000080
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00000090
           02 COMM-CICS-APPLID     PIC X(08).                           00000100
           02 COMM-CICS-NETNAM     PIC X(08).                           00000110
           02 COMM-CICS-TRANSA     PIC X(04).                           00000120
                                                                        00000130
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00000140
           02 COMM-DATE-SIECLE     PIC X(02).                           00000150
           02 COMM-DATE-ANNEE      PIC X(02).                           00000160
           02 COMM-DATE-MOIS       PIC X(02).                           00000170
           02 COMM-DATE-JOUR       PIC 99.                              00000180
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000190
           02 COMM-DATE-QNTA       PIC 999.                             00000200
           02 COMM-DATE-QNT0       PIC 99999.                           00000210
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000220
           02 COMM-DATE-BISX       PIC 9.                               00000230
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00000240
           02 COMM-DATE-JSM        PIC 9.                               00000250
      *   LIBELLES DU JOUR COURT - LONG                                 00000260
           02 COMM-DATE-JSM-LC     PIC X(03).                           00000270
           02 COMM-DATE-JSM-LL     PIC X(08).                           00000280
      *   LIBELLES DU MOIS COURT - LONG                                 00000290
           02 COMM-DATE-MOIS-LC    PIC X(03).                           00000300
           02 COMM-DATE-MOIS-LL    PIC X(08).                           00000310
      *   DIFFERENTES FORMES DE DATE                                    00000320
           02 COMM-DATE-SSAAMMJJ   PIC X(08).                           00000330
           02 COMM-DATE-AAMMJJ     PIC X(06).                           00000340
           02 COMM-DATE-JJMMSSAA   PIC X(08).                           00000350
           02 COMM-DATE-JJMMAA     PIC X(06).                           00000360
           02 COMM-DATE-JJ-MM-AA   PIC X(08).                           00000370
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00000380
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00000390
           02 COMM-DATE-WEEK.                                           00000400
              05 COMM-DATE-SEMSS   PIC 99.                              00000410
              05 COMM-DATE-SEMAA   PIC 99.                              00000420
              05 COMM-DATE-SEMNU   PIC 99.                              00000430
           02 COMM-DATE-FILLER     PIC X(08).                           00000440
      *   ZONES RESERVEES TRAITEMENT DU SWAP                            00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00000460
      *                                                                         
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
                                                                        00000470
      *}                                                                        
      *--  ZONES RESERVEES APPLICATIVES COMMUNES ----------------- 3874 00000480
                                                                        00000490
           02 COMM-CE50-APPLI.                                          00000500
      *--  ZONES RESERVEES TCE50 ---------------------------------   19 00000510
              03 COMM-ENTETE.                                           00000520
                 05 COMM-CODLANG            PIC X(02).                  00000530
                 05 COMM-CODPIC             PIC X(02).                  00000540
                 05 COMM-CODESFONCTION      PIC X(12).                  00000550
              03 COMM-50-ENTETE.                                        00000560
                 05 COMM-50-WFONC           PIC X(03).                  00000570
      *--  ZONES REDEFINES --------------------------------------- 3855 00000580
           02 COMM-CE50-FILLER              PIC X(3855).                00000590
                                                                        00000600
           02 COMM-CE51-CE52-APPLI REDEFINES COMM-CE50-FILLER.          00000610
                                                                        00000620
      *--  ZONES RESERVEES TCE51 ---------------------------------  143 00000630
              03 COMM-51.                                               00000640
                 05 COMM-51-NCODICE         PIC X(7).                   00000650
                 05 COMM-51-CFAME           PIC X(5).                   00000660
                 05 COMM-51-CMARQE          PIC X(5).                   00000670
                 05 COMM-51-LREFFOURNE      PIC X(20).                  00000680
                 05 COMM-51-INDTS           PIC S9(5) COMP-3.           00000690
                 05 COMM-51-INDMAX          PIC S9(5) COMP-3.           00000700
                                                                        00000710
      *--  ZONES RESERVEES TCE52 ---------------------------------    0 00000720
              03 COMM-52-E.                                             00000730
                 05 COMM-52-NCODIC          PIC X(7).                   00000740
                 05 COMM-52-CFAM            PIC X(5).                   00000750
                 05 COMM-52-CMARQ           PIC X(5).                   00000760
                 05 COMM-52-LREFFOURN       PIC X(20).                  00000770
              03 COMM-52-S.                                             00000780
                 05 COMM-52-INDTS           PIC S9(5) COMP-3.           00000790
                 05 COMM-52-INDMAX          PIC S9(5) COMP-3.           00000800
                 05 COMM-52-TABFORMAT OCCURS 6.                         00000810
                    07 COMM-52-CFORMAT       PIC X(5).                  00000820
                    07 COMM-52-LFORMAT       PIC X(20).                 00000830
                    07 COMM-52-PRIX          PIC X(1).                  00000840
                    07 COMM-52-COMPATIBLE    PIC X(5).                  00000850
                    07 COMM-52-DMAJ          PIC X(8).                  00000860
                 05 COMM-52-SUP-FORMAT OCCURS 6.                        00000870
                    07 COMM-52-FLG-SUP       PIC X(1).                  00000880
                    07 COMM-52-FORMAT-SUP    PIC X(5).                  00000890
                 05 COMM-52-VERSION          PIC 9(1).                  00000900
                 05 COMM-52-NB-POSTE         PIC 9(1).                  00000910
                 05 COMM-52-CHANGEMENT-CODICMAJEUR PIC 9.               00000920
                    88 CHANGEMENT-CODICMAJEUR             VALUE 1.      00000930
                 05  COMM-52-NCODIC-AVANT    PIC X(7).                  00000940
                                                                        00000950
              03 COMM-51-52-FILLER          PIC X(3489).                00000960
                                                                                
