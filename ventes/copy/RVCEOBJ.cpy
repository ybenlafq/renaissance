      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CEOBJ OBJET DES FORMATS QUICK PRESS    *        
      *----------------------------------------------------------------*        
       01  RVCEOBJ.                                                             
           05  CEOBJ-CTABLEG2    PIC X(15).                                     
           05  CEOBJ-CTABLEG2-REDEF REDEFINES CEOBJ-CTABLEG2.                   
               10  CEOBJ-TOBJET          PIC X(01).                             
               10  CEOBJ-COBJET          PIC X(05).                             
           05  CEOBJ-WTABLEG     PIC X(80).                                     
           05  CEOBJ-WTABLEG-REDEF  REDEFINES CEOBJ-WTABLEG.                    
               10  CEOBJ-LIBELLE         PIC X(20).                             
               10  CEOBJ-CBORNE          PIC X(03).                             
               10  CEOBJ-CBORNE-N       REDEFINES CEOBJ-CBORNE                  
                                         PIC 9(03).                             
               10  CEOBJ-DEFAUT          PIC X(03).                             
               10  CEOBJ-DEFAUT-N       REDEFINES CEOBJ-DEFAUT                  
                                         PIC 9(03).                             
               10  CEOBJ-WDATA           PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCEOBJ-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CEOBJ-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CEOBJ-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CEOBJ-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CEOBJ-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
