      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVBA0104                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBA0104                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVBA0104.                                                            
           02  BA01-NBA                                                         
               PIC X(0006).                                                     
           02  BA01-CTIERSBA                                                    
               PIC X(0006).                                                     
           02  BA01-NTIERSCV                                                    
               PIC X(0006).                                                     
           02  BA01-DEMIS                                                       
               PIC X(0008).                                                     
           02  BA01-NFACTBA                                                     
               PIC X(0010).                                                     
           02  BA01-PBA                                                         
               PIC S9(5)V9(0002) COMP-3.                                        
           02  BA01-NCODIC                                                      
               PIC X(0007).                                                     
           02  BA01-WSTAT                                                       
               PIC X(0001).                                                     
           02  BA01-WRECAP                                                      
               PIC X(0001).                                                     
           02  BA01-WANNUL                                                      
               PIC X(0001).                                                     
           02  BA01-WRECEP                                                      
               PIC X(0001).                                                     
           02  BA01-DICEMIS                                                     
               PIC X(0008).                                                     
           02  BA01-DANNUL                                                      
               PIC X(0008).                                                     
           02  BA01-DRECEP                                                      
               PIC X(0008).                                                     
           02  BA01-DICRECEP                                                    
               PIC X(0008).                                                     
           02  BA01-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  BA01-NLIEU                                                       
               PIC X(0003).                                                     
           02  BA01-NCHRONO                                                     
               PIC X(0007).                                                     
           02  BA01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  BA01-PBA2                                                        
               PIC S9(5)V9(0002) COMP-3.                                        
           02  BA01-CDEVISE                                                     
               PIC X(0003).                                                     
           02  BA01-CDEVISA                                                     
               PIC X(0003).                                                     
           02  BA01-WEMISEURO                                                   
               PIC X(0001).                                                     
           02  BA01-PTAUX                                                       
               PIC S9(5)V9(0005) COMP-3.                                        
           02  BA01-WVENTE                                                      
               PIC X(0001).                                                     
           02  BA01-DVENTE                                                      
               PIC X(0008).                                                     
           02  BA01-DICANNUL                                                    
               PIC X(0008).                                                     
           02  BA01-NSOCIETER                                                   
               PIC X(0003).                                                     
           02  BA01-NLIEUR                                                      
               PIC X(0003).                                                     
           02  BA01-DRECEPR                                                     
               PIC X(0008).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVBA0101                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVBA0101-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-NBA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-NBA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-CTIERSBA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-CTIERSBA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-NTIERSCV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-NTIERSCV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-DEMIS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-DEMIS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-NFACTBA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-NFACTBA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-PBA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-PBA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-WSTAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-WSTAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-WRECAP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-WRECAP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-WANNUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-WANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-WRECEP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-WRECEP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-DICEMIS-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-DICEMIS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-DANNUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-DANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-DRECEP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-DRECEP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-DICRECEP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-DICRECEP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-NCHRONO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-NCHRONO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-PBA2-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-PBA2-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-CDEVISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-CDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-CDEVISA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-CDEVISA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-WEMISEURO-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-WEMISEURO-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-PTAUX-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-PTAUX-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-WVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-WVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-DICANNUL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-DICANNUL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-NSOCIETER-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-NSOCIETER-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-NLIEUR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-NLIEUR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-DRECEPR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-DRECEPR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
