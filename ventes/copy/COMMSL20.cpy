      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : EXPLOITATION MQ SERIES                           *        
      *  TRANSACTION: SL20                                             *        
      *  TITRE      : COMMAREA DE LA TRANSACTION SL20                  *        
      *  LONGUEUR   : 4096                                             *        
      *                                                                *        
      * Z-COMMAREA, D' UNE LONGUEUR DE 4096 C, COMPRENANT :            *        
      * 1 - LES ZONES RESERVEES A AIDA                           100 C *        
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS             20 C *        
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT          100 C *        
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP               152 C *        
      * 5 - LES ZONES RESERVEES APPLICATIVES                    3724 C *        
      *                                                        -----   *        
      * COM-SL20-LONG-COMMAREA A UNE VALUE DE                   4096 C *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-SL20-LONG-COMMAREA       PIC S9(4) COMP   VALUE +4096.           
      *--                                                                       
       01  COM-SL20-LONG-COMMAREA       PIC S9(4) COMP-5   VALUE +4096.         
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *--- ZONES RESERVEES A AIDA -------------------------------- 100          
           02 FILLER-COM-AIDA           PIC X(100).                             
      *--- ZONES RESERVEES EN PROVENANCE DE CICS ----------------- 020          
           02 COMM-CICS-APPLID          PIC X(08).                              
           02 COMM-CICS-NETNAM          PIC X(08).                              
           02 COMM-CICS-TRANSA          PIC X(04).                              
      *--- ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION --- 100          
           02 COMM-DATE-SIECLE          PIC X(02).                              
           02 COMM-DATE-ANNEE           PIC X(02).                              
           02 COMM-DATE-MOIS            PIC X(02).                              
           02 COMM-DATE-JOUR            PIC 9(02).                              
           02 COMM-DATE-QNTA            PIC 9(03).                              
           02 COMM-DATE-QNT0            PIC 9(05).                              
           02 COMM-DATE-BISX            PIC 9(01).                              
           02 COMM-DATE-JSM             PIC 9(01).                              
           02 COMM-DATE-JSM-LC          PIC X(03).                              
           02 COMM-DATE-JSM-LL          PIC X(08).                              
           02 COMM-DATE-MOIS-LC         PIC X(03).                              
           02 COMM-DATE-MOIS-LL         PIC X(09).                              
           02 COMM-DATE-SSAAMMJJ        PIC X(08).                              
           02 COMM-DATE-AAMMJJ          PIC X(06).                              
           02 COMM-DATE-JJMMSSAA        PIC X(08).                              
           02 COMM-DATE-JJMMAA          PIC X(06).                              
           02 COMM-DATE-JJ-MM-AA        PIC X(08).                              
           02 COMM-DATE-JJ-MM-SSAA      PIC X(10).                              
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS           PIC 9(02).                           
              05 COMM-DATE-SEMAA           PIC 9(02).                           
              05 COMM-DATE-SEMNU           PIC 9(02).                           
           02 COMM-DATE-FILLER          PIC X(07).                              
      *--- ZONES RESERVEES TRAITEMENT DU SWAP -------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS            PIC S9(4) COMP VALUE -1.                
      *--                                                                       
           02 COMM-SWAP-CURS            PIC S9(4) COMP-5 VALUE -1.              
      *}                                                                        
           02 COMM-SWAP-ATTR            OCCURS 150 PIC X(1).                    
      *                                                                         
      ******************************************************************        
      *--- ZONES RESERVEES APPLICATIVES DE LA TRANSACTION SL20 - 3724 C*        
      ******************************************************************        
      *                                                                         
           02 COMM-SL20-APPLI.                                                  
      *       ZONES SL11                      -----------------   90 C*         
             03 COMM-SL11-DATA.                                                 
               05 COMM-SL11-VALIDE-TRT     PIC X(1).                            
               05 COMM-SL11-VALIDE-SUP     PIC 9(1).                            
               05 COMM-SL11-PAGE             PIC 9(3).                          
               05 COMM-SL11-NBPAGES          PIC 9(3).                          
      *        FLAG INDIQUANT SI LONGUEUR-TS < 500                              
               05 COMM-SL11-LONGUEUR-TS      PIC 9 .                            
                    88 LONGUEUR-TS-OK               VALUE 0.                    
                    88 LONGUEUR-TS-KO               VALUE 1.                    
      * NBRE D'ENREG AFFICHES PAR PAGE (DEPEND DU NIVEAU DE DETAIL)             
               05 COMM-SL11-NBENR-PAGE       PIC 9(2)  COMP-3 .                 
      * NBRE D'ENREG MIS DANS LA TS                                             
               05 COMM-SL11-CPT              PIC S9(4)  COMP-3 .                
      * CRITERES DE SELECTION SAUVEGARDES POUR VOIR SI ILS ONT CHANGE           
               05 COMM-SL11-NUMDOC           PIC X(7).                          
               05 COMM-SL11-SOCCRE           PIC X(3).                          
               05 COMM-SL11-LIEUCCRE         PIC X(3).                          
               05 COMM-SL11-TYPDOC           PIC X(2).                          
               05 COMM-SL11-CFAM             PIC X(5).                          
               05 COMM-SL11-CMARQ            PIC X(5).                          
               05 COMM-SL11-NCODIC           PIC X(7).                          
               05 COMM-SL11-NSOCO            PIC X(3).                          
               05 COMM-SL11-NLIEUO           PIC X(3).                          
               05 COMM-SL11-NSOCD            PIC X(3).                          
               05 COMM-SL11-NLIEUD           PIC X(3).                          
               05 COMM-SL11-DATER            PIC X(8).                          
               05 COMM-SL11-DATEE            PIC X(8).                          
               05 COMM-SL11-WARB             PIC X(1).                          
               05 COMM-SL11-COPSL            PIC X(5).                          
      *DETAIL EST MIS EN COMMAREA POUR VOIR SI IL A CHANGE                      
               05 COMM-SL11-DETAIL           PIC 9(1).                          
      *FLAG SERVANT A DETERMINER QUEL MODE DE CONSULTATION                      
      *VA ETRE UTILISE                                                          
               05  COMM-SL11-MODE-CONSULT       PIC 9(1).                       
                   88 MODE-NON-RENSEIGNE       VALUE 0.                         
                   88 MODE-DOCUMENT            VALUE 1.                         
                   88 MODE-CODIC               VALUE 2.                         
                   88 MODE-CODE-OPERATION      VALUE 3.                         
                   88 MODE-FAMILLE             VALUE 4.                         
                   88 MODE-MARQUE              VALUE 5.                         
                   88 MODE-LIEU-ORIG           VALUE 6.                         
                   88 MODE-LIEU-DEST           VALUE 7.                         
      * ZONE SERVANT A DETERMINER COMBIEN DE FOIS L'UTILISATEUR                 
      * A APPUYE SUR PF9                                                        
              05   COMM-SL11-CPT-PF9      PIC 9.                                
                   88 ZERO-PF9                 VALUE 0.                         
                   88 UN-PF9                   VALUE 1.                         
                   88 DEUX-PF9                 VALUE 2.                         
              05   COMM-SL11-TEMOIN-LIGNE      PIC S9(4) COMP-3.                
      *INDICATEUR D'EGALITE ORIGINE-DESTINATAIRE POUR TRAITEMENT SPECIAL        
              05   COMM-SL11-ORIG-DEST         PIC 9.                           
                   88 INEGALITE-ORIG-DEST      VALUE 0.                         
                   88 EGALITE-ORIG-DEST        VALUE 1.                         
              05   COMM-SL11-MENU              PIC X(4).                        
      *       ZONES SL12                      -----------------   90 C*         
             03 COMM-SL12-DATA.                                                 
               05 COMM-SL12-VALIDE-TRT     PIC X(1).                            
               05 COMM-SL12-VALIDE-SUP     PIC 9(1).                            
               05 COMM-SL12-PAGE             PIC 9(3).                          
               05 COMM-SL12-NBPAGES          PIC 9(3).                          
      *        FLAG INDIQUANT SI LONGUEUR-TS < 500                              
               05 COMM-SL12-LONGUEUR-TS      PIC 9 .                            
                    88 LONGUEUR-TS-OK2              VALUE 0.                    
                    88 LONGUEUR-TS-KO2              VALUE 1.                    
      * NBRE D'ENREG AFFICHES PAR PAGE (DEPEND DU NIVEAU DE DETAIL)             
               05 COMM-SL12-NBENR-PAGE       PIC 9(2)  COMP-3 .                 
      * NBRE D'ENREG MIS DANS LA TS                                             
               05 COMM-SL12-CPT              PIC S9(4)  COMP-3 .                
      * CRITERES DE SELECTION SAUVEGARDES POUR VOIR SI ILS ONT CHANGE           
               05 COMM-SL12-NUMDOC           PIC X(7).                          
               05 COMM-SL12-SOCCRE           PIC X(3).                          
               05 COMM-SL12-LIEUCCRE         PIC X(3).                          
               05 COMM-SL12-TYPDOC           PIC X(2).                          
               05 COMM-SL12-CFAM             PIC X(5).                          
               05 COMM-SL12-CMARQ            PIC X(5).                          
               05 COMM-SL12-NCODIC           PIC X(7).                          
               05 COMM-SL12-NSOCO            PIC X(3).                          
               05 COMM-SL12-NLIEUO           PIC X(3).                          
               05 COMM-SL12-NSOCD            PIC X(3).                          
               05 COMM-SL12-NLIEUD           PIC X(3).                          
               05 COMM-SL12-DATER            PIC X(8).                          
               05 COMM-SL12-DATEE            PIC X(8).                          
               05 COMM-SL12-WARB             PIC X(1).                          
               05 COMM-SL12-COPSL            PIC X(5).                          
      *DETAIL EST MIS EN COMMAREA POUR VOIR SI IL A CHANGE                      
               05 COMM-SL12-DETAIL           PIC 9(1).                          
      *FLAG SERVANT A DETERMINER QUEL MODE DE CONSULTATION                      
      *VA ETRE UTILISE                                                          
               05  COMM-SL12-MODE-CONSULT       PIC 9(1).                       
                   88 MODE-NON-RENSEIGNE2      VALUE 0.                         
                   88 MODE-DOCUMENT2           VALUE 1.                         
                   88 MODE-CODIC2              VALUE 2.                         
                   88 MODE-CODE-OPERATION2     VALUE 3.                         
                   88 MODE-FAMILLE2            VALUE 4.                         
                   88 MODE-MARQUE2             VALUE 5.                         
                   88 MODE-LIEU-ORIG2          VALUE 6.                         
                   88 MODE-LIEU-DEST2          VALUE 7.                         
      * ZONE SERVANT A DETERMINER COMBIEN DE FOIS L'UTILISATEUR                 
      * A APPUYE SUR PF9                                                        
              05   COMM-SL12-CPT-PF9      PIC 9.                                
                   88 ZERO-PF92                VALUE 0.                         
                   88 UN-PF92                  VALUE 1.                         
                   88 DEUX-PF92                VALUE 2.                         
              05   COMM-SL12-TEMOIN-LIGNE      PIC S9(4) COMP-3.                
      *INDICATEUR D'EGALITE ORIGINE-DESTINATAIRE POUR TRAITEMENT SPECIAL        
              05   COMM-SL12-ORIG-DEST         PIC 9.                           
                   88 INEGALITE-ORIG-DEST2     VALUE 0.                         
                   88 EGALITE-ORIG-DEST2       VALUE 1.                         
              05   COMM-SL12-MENU              PIC X(4).                        
      *       ZONES SL23                      -----------------  140 C*         
             03 COMM-SL23-DATA.                                                 
              05 COMM-SL23-TAB-NLDOC OCCURS 14.                                 
                10 COMM-SL23-NLDOC       PIC S9(05).                            
                10 COMM-SL23-WARB        PIC X(01).                             
              05 COMM-SL23-PAGE            PIC 9(3) VALUE 0.                    
              05 COMM-SL23-NBPAGES         PIC 9(3) VALUE 1.                    
              05 COMM-SL23-POSPAGE         PIC 9(2) VALUE 0.                    
              05 COMM-SL23-QEXPTOT         PIC 9(5) VALUE 0.                    
              05 COMM-SL23-QRECTOT         PIC 9(5) VALUE 0.                    
              05 COMM-SL23-QARBTOT         PIC 9(5) VALUE 0.                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 COMM-SL23-RANGTS          PIC S9(4) COMP VALUE 0.              
      *--                                                                       
              05 COMM-SL23-RANGTS          PIC S9(4) COMP-5 VALUE 0.            
      *}                                                                        
              05 COMM-SL23-NBDOC           PIC 9(2) VALUE 0.                    
              05 COMM-SL23-DOC-1.                                               
                10 COMM-SL23-NSOCCRE-1   PIC X(03).                             
                10 COMM-SL23-NLIEUCRE-1  PIC X(03).                             
                10 COMM-SL23-CTYPDOC-1   PIC X(02).                             
                10 COMM-SL23-NUMDOC-1    PIC S9(7) COMP-3.                      
                10 COMM-SL23-NCODIC-1    PIC X(07).                             
              05 COMM-SL23-DOC-2.                                               
                10 COMM-SL23-NSOCCRE-2   PIC X(03).                             
                10 COMM-SL23-NLIEUCRE-2  PIC X(03).                             
                10 COMM-SL23-CTYPDOC-2   PIC X(02).                             
                10 COMM-SL23-NUMDOC-2    PIC S9(7) COMP-3.                      
                10 COMM-SL23-NCODIC-2    PIC X(07).                             
              05 COMM-SL23-SELARTICLE.                                          
                10 COMM-SL23-NSOCO       PIC X(03).                             
                10 COMM-SL23-NLIEUO      PIC X(03).                             
                10 COMM-SL23-CFAM        PIC X(05).                             
                10 COMM-SL23-CMARQ       PIC X(05).                             
                10 COMM-SL23-NCODIC      PIC X(07).                             
                10 COMM-SL23-DDEBUT      PIC X(08).                             
                10 COMM-SL23-DFIN        PIC X(08).                             
              05 COMM-SL23-TOPMODIF        PIC X VALUE '1'.                     
                 88 COMM-SL23-TELQUEL       VALUE '1'.                          
                 88 COMM-SL23-MODIFICATION  VALUE '2'.                          
              05 COMM-SL23-SELECTION       PIC X VALUE '1'.                     
                 88 COMM-SL23-DOCUMENT      VALUE '1'.                          
                 88 COMM-SL23-ARTICLE       VALUE '2'.                          
              05 COMM-SL23-TOPMAJ          PIC X VALUE '2'.                     
                 88 COMM-SL23-MAJ           VALUE '1'.                          
                 88 COMM-SL23-RIEN          VALUE '2'.                          
      *       ZONES SL24                      -----------------  140 C*         
             03 COMM-SL24-DATA.                                                 
              05 COMM-SL24-TAB-NLDOC OCCURS 14.                                 
                10 COMM-SL24-NLDOC       PIC S9(05).                            
                10 COMM-SL24-WARB        PIC X(01).                             
              05 COMM-SL24-PAGE            PIC 9(3) VALUE 0.                    
              05 COMM-SL24-NBPAGES         PIC 9(3) VALUE 1.                    
              05 COMM-SL24-POSPAGE         PIC 9(2) VALUE 0.                    
              05 COMM-SL24-QEXPTOT         PIC 9(5) VALUE 0.                    
              05 COMM-SL24-QRECTOT         PIC 9(5) VALUE 0.                    
              05 COMM-SL24-QARBTOT         PIC 9(5) VALUE 0.                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 COMM-SL24-RANGTS          PIC S9(4) COMP VALUE 0.              
      *--                                                                       
              05 COMM-SL24-RANGTS          PIC S9(4) COMP-5 VALUE 0.            
      *}                                                                        
              05 COMM-SL24-NBDOC           PIC 9(2) VALUE 0.                    
              05 COMM-SL24-DOC-1.                                               
                10 COMM-SL24-NSOCCRE-1   PIC X(03).                             
                10 COMM-SL24-NLIEUCRE-1  PIC X(03).                             
                10 COMM-SL24-CTYPDOC-1   PIC X(02).                             
                10 COMM-SL24-NUMDOC-1    PIC S9(7) COMP-3.                      
                10 COMM-SL24-NCODIC-1    PIC X(07).                             
DC22  *       05 COMM-SL24-DOC-2.                                               
DC22  *         10 COMM-SL24-NSOCCRE-2   PIC X(03).                             
DC22  *         10 COMM-SL24-NLIEUCRE-2  PIC X(03).                             
DC22  *         10 COMM-SL24-CTYPDOC-2   PIC X(02).                             
DC22  *         10 COMM-SL24-NUMDOC-2    PIC S9(7) COMP-3.                      
DC22  *         10 COMM-SL24-NCODIC-2    PIC X(07).                             
              05 COMM-SL24-SELARTICLE.                                          
                10 COMM-SL24-NSOCO       PIC X(03).                             
                10 COMM-SL24-NLIEUO      PIC X(03).                             
                10 COMM-SL24-CFAM        PIC X(05).                             
                10 COMM-SL24-CMARQ       PIC X(05).                             
                10 COMM-SL24-NCODIC      PIC X(07).                             
                10 COMM-SL24-DDEBUT      PIC X(08).                             
                10 COMM-SL24-DFIN        PIC X(08).                             
              05 COMM-SL24-TOPMODIF        PIC X VALUE '1'.                     
                 88 COMM-SL24-TELQUEL       VALUE '1'.                          
                 88 COMM-SL24-MODIFICATION  VALUE '2'.                          
              05 COMM-SL24-SELECTION       PIC X VALUE '1'.                     
                 88 COMM-SL24-DOCUMENT      VALUE '1'.                          
                 88 COMM-SL24-ARTICLE       VALUE '2'.                          
              05 COMM-SL24-TOPMAJ          PIC X VALUE '2'.                     
                 88 COMM-SL24-MAJ           VALUE '1'.                          
                 88 COMM-SL24-RIEN          VALUE '2'.                          
      ******************************************************************        
      *       ZONES DISPONIBLES ------------------------------- 3724 C*         
      *       - 90  SL11        ------------------------------- 3634 C*         
      *       - 90  SL12        ------------------------------- 3544 C*         
      *       -171  SL23        ------------------------------- 3544 C*         
      *       -171  SL24        ------------------------------       C*         
      ******************************************************************        
! ! ! *       05 COMM-SL20-FILLER          PIC X(3544).                         
!!!!! * Y A 2 DE TROP QUELQUE PART!!!!!!!!!                                     
! ! ! *       05 COMM-SL20-FILLER          PIC X(3542).                         
!!!!! * 171 ?? POUR SL23                                                        
! ! ! *       05 COMM-SL20-FILLER          PIC X(3371).                         
!!!!! * Y A 20 DE TROP !!!!!!!!!                                                
! ! ! *        05 COMM-SL20-FILLER          PIC X(3351).                        
!!!!! * 200 DE MOINS POUR SL24 HISTOIRE DE VOIR LARGE!!!!                       
! ! !          05 COMM-SL20-FILLER          PIC X(3151).                        
                                                                                
