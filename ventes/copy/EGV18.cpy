      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV18   EGV18                                              00000020
      ***************************************************************** 00000030
       01   EGV18I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMPVOIEL    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLNOMPVOIEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLNOMPVOIEF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLNOMPVOIEI    PIC X(41).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTALL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCPOSTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPOSTALF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCPOSTALI      PIC X(5).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMUNEL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLCOMMUNEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCOMMUNEF     PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLCOMMUNEI     PIC X(20).                                 00000330
           02 MTABLEI OCCURS   12 TIMES .                               00000340
             03 MCTVOIED OCCURS   2 TIMES .                             00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCTVOIEL   COMP PIC S9(4).                            00000360
      *--                                                                       
               04 MCTVOIEL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MCTVOIEF   PIC X.                                     00000370
               04 FILLER     PIC X(4).                                  00000380
               04 MCTVOIEI   PIC X(4).                                  00000390
             03 MLNOMVOIED OCCURS   2 TIMES .                           00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MLNOMVOIEL      COMP PIC S9(4).                       00000410
      *--                                                                       
               04 MLNOMVOIEL COMP-5 PIC S9(4).                                  
      *}                                                                        
               04 MLNOMVOIEF      PIC X.                                00000420
               04 FILLER     PIC X(4).                                  00000430
               04 MLNOMVOIEI      PIC X(21).                            00000440
             03 MSELECTD OCCURS   2 TIMES .                             00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSELECTL   COMP PIC S9(4).                            00000460
      *--                                                                       
               04 MSELECTL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MSELECTF   PIC X.                                     00000470
               04 FILLER     PIC X(4).                                  00000480
               04 MSELECTI   PIC X.                                     00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MZONCMDI  PIC X(15).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLIBERRI  PIC X(58).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCODTRAI  PIC X(4).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCICSI    PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNETNAMI  PIC X(8).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSCREENI  PIC X(4).                                       00000730
      ***************************************************************** 00000740
      * SDF: EGV18   EGV18                                              00000750
      ***************************************************************** 00000760
       01   EGV18O REDEFINES EGV18I.                                    00000770
           02 FILLER    PIC X(12).                                      00000780
           02 FILLER    PIC X(2).                                       00000790
           02 MDATJOUA  PIC X.                                          00000800
           02 MDATJOUC  PIC X.                                          00000810
           02 MDATJOUP  PIC X.                                          00000820
           02 MDATJOUH  PIC X.                                          00000830
           02 MDATJOUV  PIC X.                                          00000840
           02 MDATJOUO  PIC X(10).                                      00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MTIMJOUA  PIC X.                                          00000870
           02 MTIMJOUC  PIC X.                                          00000880
           02 MTIMJOUP  PIC X.                                          00000890
           02 MTIMJOUH  PIC X.                                          00000900
           02 MTIMJOUV  PIC X.                                          00000910
           02 MTIMJOUO  PIC X(5).                                       00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MPAGEA    PIC X.                                          00000940
           02 MPAGEC    PIC X.                                          00000950
           02 MPAGEP    PIC X.                                          00000960
           02 MPAGEH    PIC X.                                          00000970
           02 MPAGEV    PIC X.                                          00000980
           02 MPAGEO    PIC X(2).                                       00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MPAGEMAXA      PIC X.                                     00001010
           02 MPAGEMAXC PIC X.                                          00001020
           02 MPAGEMAXP PIC X.                                          00001030
           02 MPAGEMAXH PIC X.                                          00001040
           02 MPAGEMAXV PIC X.                                          00001050
           02 MPAGEMAXO      PIC X(2).                                  00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MLNOMPVOIEA    PIC X.                                     00001080
           02 MLNOMPVOIEC    PIC X.                                     00001090
           02 MLNOMPVOIEP    PIC X.                                     00001100
           02 MLNOMPVOIEH    PIC X.                                     00001110
           02 MLNOMPVOIEV    PIC X.                                     00001120
           02 MLNOMPVOIEO    PIC X(41).                                 00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MCPOSTALA      PIC X.                                     00001150
           02 MCPOSTALC PIC X.                                          00001160
           02 MCPOSTALP PIC X.                                          00001170
           02 MCPOSTALH PIC X.                                          00001180
           02 MCPOSTALV PIC X.                                          00001190
           02 MCPOSTALO      PIC X(5).                                  00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MLCOMMUNEA     PIC X.                                     00001220
           02 MLCOMMUNEC     PIC X.                                     00001230
           02 MLCOMMUNEP     PIC X.                                     00001240
           02 MLCOMMUNEH     PIC X.                                     00001250
           02 MLCOMMUNEV     PIC X.                                     00001260
           02 MLCOMMUNEO     PIC X(20).                                 00001270
           02 MTABLEO OCCURS   12 TIMES .                               00001280
             03 DFHMS1 OCCURS   2 TIMES .                               00001290
               04 FILLER     PIC X(2).                                  00001300
               04 MCTVOIEA   PIC X.                                     00001310
               04 MCTVOIEC   PIC X.                                     00001320
               04 MCTVOIEP   PIC X.                                     00001330
               04 MCTVOIEH   PIC X.                                     00001340
               04 MCTVOIEV   PIC X.                                     00001350
               04 MCTVOIEO   PIC X(4).                                  00001360
             03 DFHMS2 OCCURS   2 TIMES .                               00001370
               04 FILLER     PIC X(2).                                  00001380
               04 MLNOMVOIEA      PIC X.                                00001390
               04 MLNOMVOIEC PIC X.                                     00001400
               04 MLNOMVOIEP PIC X.                                     00001410
               04 MLNOMVOIEH PIC X.                                     00001420
               04 MLNOMVOIEV PIC X.                                     00001430
               04 MLNOMVOIEO      PIC X(21).                            00001440
             03 DFHMS3 OCCURS   2 TIMES .                               00001450
               04 FILLER     PIC X(2).                                  00001460
               04 MSELECTA   PIC X.                                     00001470
               04 MSELECTC   PIC X.                                     00001480
               04 MSELECTP   PIC X.                                     00001490
               04 MSELECTH   PIC X.                                     00001500
               04 MSELECTV   PIC X.                                     00001510
               04 MSELECTO   PIC X.                                     00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MZONCMDA  PIC X.                                          00001540
           02 MZONCMDC  PIC X.                                          00001550
           02 MZONCMDP  PIC X.                                          00001560
           02 MZONCMDH  PIC X.                                          00001570
           02 MZONCMDV  PIC X.                                          00001580
           02 MZONCMDO  PIC X(15).                                      00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MLIBERRA  PIC X.                                          00001610
           02 MLIBERRC  PIC X.                                          00001620
           02 MLIBERRP  PIC X.                                          00001630
           02 MLIBERRH  PIC X.                                          00001640
           02 MLIBERRV  PIC X.                                          00001650
           02 MLIBERRO  PIC X(58).                                      00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MCODTRAA  PIC X.                                          00001680
           02 MCODTRAC  PIC X.                                          00001690
           02 MCODTRAP  PIC X.                                          00001700
           02 MCODTRAH  PIC X.                                          00001710
           02 MCODTRAV  PIC X.                                          00001720
           02 MCODTRAO  PIC X(4).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MCICSA    PIC X.                                          00001750
           02 MCICSC    PIC X.                                          00001760
           02 MCICSP    PIC X.                                          00001770
           02 MCICSH    PIC X.                                          00001780
           02 MCICSV    PIC X.                                          00001790
           02 MCICSO    PIC X(5).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MNETNAMA  PIC X.                                          00001820
           02 MNETNAMC  PIC X.                                          00001830
           02 MNETNAMP  PIC X.                                          00001840
           02 MNETNAMH  PIC X.                                          00001850
           02 MNETNAMV  PIC X.                                          00001860
           02 MNETNAMO  PIC X(8).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MSCREENA  PIC X.                                          00001890
           02 MSCREENC  PIC X.                                          00001900
           02 MSCREENP  PIC X.                                          00001910
           02 MSCREENH  PIC X.                                          00001920
           02 MSCREENV  PIC X.                                          00001930
           02 MSCREENO  PIC X(4).                                       00001940
                                                                                
