      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVVE0800                                     00020006
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVE0800                 00060006
      *---------------------------------------------------------        00070000
      *                                                                 00080000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVE0800.                                                    00090006
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVE0800.                                                            
      *}                                                                        
           02  VE08-NSOCIETE                                            00100005
               PIC X(0003).                                             00110004
           02  VE08-NLIEU                                               00120005
               PIC X(0003).                                             00130004
           02  VE08-NVENTE                                              00140005
               PIC X(0007).                                             00150004
           02  VE08-NDOSSIER                                            00160005
               PIC S9(3) COMP-3.                                        00170007
           02  VE08-NSEQ                                                00180005
               PIC S9(3) COMP-3.                                        00190002
           02  VE08-CCTRL                                               00200005
               PIC X(0005).                                             00210000
           02  VE08-LGCTRL                                              00220005
               PIC S9(3) COMP-3.                                        00230002
           02  VE08-VALCTRL                                             00240005
               PIC X(0050).                                             00250002
           02  VE08-CAUTOR                                              00260005
               PIC X(0005).                                             00270002
           02  VE08-CJUSTIF                                             00280005
               PIC X(0005).                                             00290002
           02  VE08-DMODIF                                              00300005
               PIC X(0008).                                             00310000
           02  VE08-DSYST                                               00320005
               PIC S9(13) COMP-3.                                       00330004
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00340000
      *---------------------------------------------------------        00350000
      *   LISTE DES FLAGS DE LA TABLE RVVE0800                          00360006
      *---------------------------------------------------------        00370000
      *                                                                 00380000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVE0800-FLAGS.                                              00390006
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVE0800-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE08-NSOCIETE-F                                          00400005
      *        PIC S9(4) COMP.                                          00410000
      *--                                                                       
           02  VE08-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE08-NLIEU-F                                             00420005
      *        PIC S9(4) COMP.                                          00430000
      *--                                                                       
           02  VE08-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE08-NVENTE-F                                            00440005
      *        PIC S9(4) COMP.                                          00450000
      *--                                                                       
           02  VE08-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE08-NDOSSIER-F                                          00460005
      *        PIC S9(4) COMP.                                          00470000
      *--                                                                       
           02  VE08-NDOSSIER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE08-NSEQ-F                                              00480005
      *        PIC S9(4) COMP.                                          00490000
      *--                                                                       
           02  VE08-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE08-CCTRL-F                                             00500005
      *        PIC S9(4) COMP.                                          00510000
      *--                                                                       
           02  VE08-CCTRL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE08-LGCTRL-F                                            00520005
      *        PIC S9(4) COMP.                                          00530000
      *--                                                                       
           02  VE08-LGCTRL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE08-VALCTRL-F                                           00540005
      *        PIC S9(4) COMP.                                          00550000
      *--                                                                       
           02  VE08-VALCTRL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE08-CAUTOR-F                                            00560005
      *        PIC S9(4) COMP.                                          00570000
      *--                                                                       
           02  VE08-CAUTOR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE08-CJUSTIF-F                                           00580005
      *        PIC S9(4) COMP.                                          00590000
      *--                                                                       
           02  VE08-CJUSTIF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE08-DMODIF-F                                            00600005
      *        PIC S9(4) COMP.                                          00610000
      *--                                                                       
           02  VE08-DMODIF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE08-DSYST-F                                             00620005
      *        PIC S9(4) COMP.                                          00630004
      *--                                                                       
           02  VE08-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                            00640000
                                                                                
