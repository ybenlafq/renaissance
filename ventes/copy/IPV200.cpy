      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPV200 AU 31/10/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,PD,A,                          *        
      *                           16,10,BI,A,                          *        
      *                           26,05,BI,A,                          *        
      *                           31,05,BI,A,                          *        
      *                           36,04,PD,A,                          *        
      *                           40,05,BI,A,                          *        
      *                           45,07,BI,A,                          *        
      *                           52,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPV200.                                                        
            05 NOMETAT-IPV200           PIC X(6) VALUE 'IPV200'.                
            05 RUPTURES-IPV200.                                                 
           10 IPV200-NSOCIETE           PIC X(03).                      007  003
           10 IPV200-NLIEU              PIC X(03).                      010  003
           10 IPV200-WSEQFAM            PIC S9(05)      COMP-3.         013  003
           10 IPV200-CODPROF            PIC X(10).                      016  010
           10 IPV200-CMARKETING         PIC X(05).                      026  005
           10 IPV200-CVMARKETING        PIC X(05).                      031  005
           10 IPV200-PSTDTTC            PIC S9(07)      COMP-3.         036  004
           10 IPV200-CMARQ              PIC X(05).                      040  005
           10 IPV200-NCODIC-NORMAL      PIC X(07).                      045  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPV200-SEQUENCE           PIC S9(04) COMP.                052  002
      *--                                                                       
           10 IPV200-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPV200.                                                   
           10 IPV200-CFAM               PIC X(05).                      054  005
           10 IPV200-INTER              PIC X(03).                      059  003
           10 IPV200-LIBPROFIL          PIC X(30).                      062  030
           10 IPV200-LMARKETING         PIC X(20).                      092  020
           10 IPV200-LREFFOURN          PIC X(20).                      112  020
           10 IPV200-LSTATCOMP          PIC X(03).                      132  003
           10 IPV200-LVMARKETING        PIC X(20).                      135  020
           10 IPV200-NCODIC             PIC X(09).                      155  009
           10 IPV200-VAR                PIC X(01).                      164  001
           10 IPV200-PERDEB             PIC X(08).                      165  008
           10 IPV200-PERFIN             PIC X(08).                      173  008
            05 FILLER                      PIC X(332).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPV200-LONG           PIC S9(4)   COMP  VALUE +180.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPV200-LONG           PIC S9(4) COMP-5  VALUE +180.           
                                                                                
      *}                                                                        
