      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010004
      *   COPY DE LA TABLE RVEM4900                                     00020004
      **********************************************************        00030004
      *                                                                 00040004
      *---------------------------------------------------------        00050004
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEM4900                 00060004
      *---------------------------------------------------------        00070004
      *                                                                 00080004
       01  RVEM4900.                                                    00090004
           02  EM49-NFLAG                                               00100004
               PIC X(0001).                                             00110004
           02  EM49-NCLUSTER                                            00120004
               PIC X(0013).                                             00130004
           02  EM49-DVENTE                                              00140004
               PIC X(0008).                                             00150004
           02  EM49-NVENTE                                              00160004
               PIC X(0007).                                             00170004
           02  EM49-NTYPVENTE                                           00180004
               PIC X(0003).                                             00190004
           02  EM49-NLIEUVENTE                                          00200004
               PIC X(0003).                                             00210004
           02  EM49-NSOCIETE                                            00220004
               PIC X(0003).                                             00230004
           02  EM49-NLIEU                                               00240004
               PIC X(0003).                                             00250004
           02  EM49-DCAISSE                                             00260004
               PIC X(0008).                                             00270004
           02  EM49-NCAISSE                                             00280004
               PIC X(0003).                                             00290004
           02  EM49-NTRANS                                              00300004
               PIC X(0008).                                             00310004
           02  EM49-NTYPETRANS                                          00320004
               PIC X(0003).                                             00330004
           02  EM49-NOPERATEUR                                          00340004
               PIC X(0007).                                             00350004
           02  EM49-NVENDEUR                                            00360004
               PIC X(0007).                                             00370004
           02  EM49-NFACTURE                                            00380004
               PIC X(0006).                                             00390004
           02  EM49-NEMPORT                                             00400004
               PIC X(0008).                                             00410004
           02  EM49-DHVENTE                                             00420004
               PIC X(0002).                                             00430004
           02  EM49-DMVENTE                                             00440004
               PIC X(0002).                                             00450004
           02  EM49-PTTVENTE                                            00460004
               PIC S9(7)V9(0002) COMP-3.                                00470004
           02  EM49-PDIFFERE                                            00480004
               PIC S9(7)V9(0002) COMP-3.                                00490004
           02  EM49-PRFACT                                              00500004
               PIC S9(7)V9(0002) COMP-3.                                00510004
           02  EM49-CORGORED                                            00520004
               PIC X(0005).                                             00530004
      *                                                                 00540004
      *---------------------------------------------------------        00550004
      *   LISTE DES FLAGS DE LA TABLE RVEM4900                          00560004
      *---------------------------------------------------------        00570004
      *                                                                 00580004
       01  RVEM4900-FLAGS.                                              00590004
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-NFLAG-F                                             00600004
      *        PIC S9(4) COMP.                                          00610004
      *--                                                                       
           02  EM49-NFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-NCLUSTER-F                                          00620004
      *        PIC S9(4) COMP.                                          00630004
      *--                                                                       
           02  EM49-NCLUSTER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-DVENTE-F                                            00640004
      *        PIC S9(4) COMP.                                          00650004
      *--                                                                       
           02  EM49-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-NVENTE-F                                            00660004
      *        PIC S9(4) COMP.                                          00670004
      *--                                                                       
           02  EM49-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-NTYPVENTE-F                                         00680004
      *        PIC S9(4) COMP.                                          00690004
      *--                                                                       
           02  EM49-NTYPVENTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-NLIEUVENTE-F                                        00700004
      *        PIC S9(4) COMP.                                          00710004
      *--                                                                       
           02  EM49-NLIEUVENTE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-NSOCIETE-F                                          00720004
      *        PIC S9(4) COMP.                                          00730004
      *--                                                                       
           02  EM49-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-NLIEU-F                                             00740004
      *        PIC S9(4) COMP.                                          00750004
      *--                                                                       
           02  EM49-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-DCAISSE-F                                           00760004
      *        PIC S9(4) COMP.                                          00770004
      *--                                                                       
           02  EM49-DCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-NCAISSE-F                                           00780004
      *        PIC S9(4) COMP.                                          00790004
      *--                                                                       
           02  EM49-NCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-NTRANS-F                                            00800004
      *        PIC S9(4) COMP.                                          00810004
      *--                                                                       
           02  EM49-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-NTYPETRANS-F                                        00820004
      *        PIC S9(4) COMP.                                          00830004
      *--                                                                       
           02  EM49-NTYPETRANS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-NOPERATEUR-F                                        00840004
      *        PIC S9(4) COMP.                                          00850004
      *--                                                                       
           02  EM49-NOPERATEUR-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-NVENDEUR-F                                          00860004
      *        PIC S9(4) COMP.                                          00870004
      *--                                                                       
           02  EM49-NVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-NFACTURE-F                                          00880004
      *        PIC S9(4) COMP.                                          00890004
      *--                                                                       
           02  EM49-NFACTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-NEMPORT-F                                           00900004
      *        PIC S9(4) COMP.                                          00910004
      *--                                                                       
           02  EM49-NEMPORT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-DHVENTE-F                                           00920004
      *        PIC S9(4) COMP.                                          00930004
      *--                                                                       
           02  EM49-DHVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-DMVENTE-F                                           00940004
      *        PIC S9(4) COMP.                                          00950004
      *--                                                                       
           02  EM49-DMVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-PTTVENTE-F                                          00960004
      *        PIC S9(4) COMP.                                          00970004
      *--                                                                       
           02  EM49-PTTVENTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-PDIFFERE-F                                          00980004
      *        PIC S9(4) COMP.                                          00990004
      *--                                                                       
           02  EM49-PDIFFERE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-PRFACT-F                                            01000004
      *        PIC S9(4) COMP.                                          01010004
      *--                                                                       
           02  EM49-PRFACT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM49-CORGORED-F                                          01020005
      *        PIC S9(4) COMP.                                          01030004
      *--                                                                       
           02  EM49-CORGORED-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            01040004
                                                                                
