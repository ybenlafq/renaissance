      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JMD410 AU 05/07/2005  *        
      *                                                                *        
      *          CRITERES DE TRI  07,10,BI,A,                          *        
      *                           17,03,BI,A,                          *        
      *                           20,03,BI,A,                          *        
      *                           23,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-JMD410.                                                        
            05 NOMETAT-JMD410           PIC X(6) VALUE 'JMD410'.                
            05 RUPTURES-JMD410.                                                 
           10 JMD410-COPER              PIC X(10).                      007  010
           10 JMD410-NLIEUEXT           PIC X(03).                      017  003
           10 JMD410-NSOC               PIC X(03).                      020  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 JMD410-SEQUENCE           PIC S9(04) COMP.                023  002
      *--                                                                       
           10 JMD410-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-JMD410.                                                   
           10 JMD410-LCOPER             PIC X(20).                      025  020
           10 JMD410-LNMAG              PIC X(20).                      045  020
           10 JMD410-NLIEUXEXT          PIC X(03).                      065  003
           10 JMD410-NMAG               PIC X(03).                      068  003
           10 JMD410-PCESSION           PIC S9(10)V9(2) COMP-3.         071  007
           10 JMD410-PCESSIONCUM        PIC S9(12)V9(2) COMP-3.         078  008
           10 JMD410-PRMP               PIC S9(10)V9(2) COMP-3.         086  007
           10 JMD410-PRMPCUM            PIC S9(12)V9(2) COMP-3.         093  008
           10 JMD410-QTE                PIC S9(06)      COMP-3.         101  004
           10 JMD410-QTECUM             PIC S9(07)      COMP-3.         105  004
           10 JMD410-DDEB               PIC X(08).                      109  008
           10 JMD410-DFIN               PIC X(08).                      117  008
            05 FILLER                      PIC X(388).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-JMD410-LONG           PIC S9(4)   COMP  VALUE +124.           
      *                                                                         
      *--                                                                       
        01  DSECT-JMD410-LONG           PIC S9(4) COMP-5  VALUE +124.           
                                                                                
      *}                                                                        
