      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE EM5500                       
      ******************************************************************        
      *                                                                         
       CLEF-EM5500             SECTION.                                         
      *                                                                         
           MOVE 'RVEM5500          '       TO   TABLE-NAME.                     
           MOVE 'EM5500'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-EM5500. EXIT.                                                   
                EJECT                                                           
                                                                                
