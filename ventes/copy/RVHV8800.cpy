      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHV8800                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV8800                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVHV8800.                                                            
           02  HV88-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV88-CFAM                                                        
               PIC X(0005).                                                     
           02  HV88-DVENTECIALE                                                 
               PIC X(0008).                                                     
           02  HV88-QPIECES                                                     
               PIC S9(5) COMP-3.                                                
           02  HV88-QPIECESCOMM                                                 
               PIC S9(5) COMP-3.                                                
           02  HV88-PCA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV88-PCACOMM                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV88-PMTACHATS                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV88-PMTACHATSCOMM                                               
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV88-PMTPRIMES                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV88-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  HV88-QPIECESEMP                                                  
               PIC S9(5) COMP-3.                                                
           02  HV88-PCAEMP                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV88-PMTACHATSEMP                                                
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV88-PMTPRIMVOL                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV88-QPIECESEXC                                                  
               PIC S9(5) COMP-3.                                                
           02  HV88-PCAEXC                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV88-PMTACHATSEXC                                                
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV88-QPIECESEXE                                                  
               PIC S9(5) COMP-3.                                                
           02  HV88-PCAEXE                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV88-PMTACHATSEXE                                                
               PIC S9(7)V9(0002) COMP-3.                                        
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHV8800                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHV8800-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-DVENTECIALE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-DVENTECIALE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-QPIECES-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-QPIECES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-QPIECESCOMM-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-QPIECESCOMM-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-PCA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-PCA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-PCACOMM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-PCACOMM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-PMTACHATS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-PMTACHATS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-PMTACHATSCOMM-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-PMTACHATSCOMM-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-PMTPRIMES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-PMTPRIMES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-QPIECESEMP-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-QPIECESEMP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-PCAEMP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-PCAEMP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-PMTACHATSEMP-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-PMTACHATSEMP-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-PMTPRIMVOL-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-PMTPRIMVOL-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-QPIECESEXC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-QPIECESEXC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-PCAEXC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-PCAEXC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-PMTACHATSEXC-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-PMTACHATSEXC-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-QPIECESEXE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-QPIECESEXE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-PCAEXE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-PCAEXE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV88-PMTACHATSEXE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV88-PMTACHATSEXE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
