      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************00010003
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *00020003
      ******************************************************************00030003
      *                                                                *00040003
      *  PROJET     : GESTION DES VENTES                               *00050003
      *  TRANSACTION: GV00                                             *00060003
      *  TITRE      : COMMAREA DE L'APPLICATION ISL  APELLEE PAR TGV01 *00070003
      *                        INTERROGATION DU STOCK LOCAL            *00080003
      *  LONGUEUR   : 1200 --> 1320 (PF1)                              *00090003
      * -------------------------------------------------------------- *00100003
      * 12/13 : EMPORTE ET LIVRE SUR STOCK PLATEFORME                  *00110003
      * DE1213 MODIFICATION COMPLETE DE LA COMMAREA                    *00120003
      * LE PROGRAMME MGV48 DEVIENT APPELABLE PAR MGV21 22 ET MBS45.    *00121003
      *                                                                *00122003
      *                                                                *00123003
      *                                                                *00124003
      *                                                                *00125003
      ******************************************************************00126003
      *01  COMM-MGV48-APPLI.                                            00127003
      *  02 COMM-MGV48-ENTREE.                                          00128003
      ** IDENTIFIANT VENTE                                              00129003
      *     05 COMM-MGV48-NSOCLIVR       PIC X(03).                     00130003
      *     05 COMM-MGV48-NDEPOT         PIC X(03).                     00140003
      *     05 COMM-MGV48-NVENTE         PIC X(07).                     00150003
      ** LIGNES DE VENTE                                                00160003
      *     05 COMM-MGV48-LIGNE  OCCURS 40.                             00170003
      *        10 COMM-MGV48-E-NCODIC         PIC X(07).                00180003
      *        10 COMM-MGV48-E-QSTOCK         PIC S9(05).               00190003
      *        10 COMM-MGV48-E-NLIGNE         PIC X(02).                00200003
PF1   *        10 COMM-MGV48-E-NSEQNQ         PIC S9(3).                00210003
      *   02 COMM-MGV48-SORTIE.                                         00220003
      *     05 COMM-MGV48-MESSAGE.                                      00230003
      *          10 COMM-MGV48-CODRET         PIC X VALUE SPACES.       00240003
      *             88  COMM-MGV48-OK          VALUE ' '.               00250003
      *             88  COMM-MGV48-ERR-BLQ     VALUE '1'.               00260003
      *             88  COMM-MGV48-ERR-STO     VALUE '2'.               00270003
      *             88  COMM-MGV48-ERR-NON-BLQ VALUE '3'.               00280003
      *          10 COMM-MGV48-LIBERR         PIC X(58).                00290003
      *     05 COMM-MGV48-PBSTOCK OCCURS 40.                            00300003
      *        10 COMM-MGV48-NCODIC         PIC X(07).                  00310003
      *        10 COMM-MGV48-QSTOCK         PIC S9(05).                 00320003
      *        10 COMM-MGV48-NLIGNE         PIC X(02).                  00330003
      *     05 FILLER                    PIC X(08).                     00340003
       01 COMM-MGV48-APPLI.                                             00350003
          02 COMM-MGV48-ENTREE.                                         00351003
             05 COMM-MGV48-SSAAMMJJ              PIC X(08).             00352003
      * -> LE TYPE DE DEMANDE POUR L'ENSEMBLE DES PRODUITS              00353003
             05 COMM-MGV48-DEMANDE               PIC X(01).             00354003
                88 COMM-MGV48-CONSULTATION  VALUE 'C'.                  00355003
                88 COMM-MGV48-RESERVATION   VALUE 'R'.                  00355103
                88 COMM-MGV48-DERESERVATION VALUE 'D'.                  00355203
                88 COMM-MGV48-VALIDATION    VALUE 'V'.                  00355303
      * -> LA REF�RENCE VENTE OU NORDRE AVEC DATE DE VENTE              00355403
             05 COMM-MGV48-REF.                                         00355503
                10 COMM-MGV48-NSOCIETE           PIC X(03).             00355603
                10 COMM-MGV48-NLIEU              PIC X(03).             00355703
                10 COMM-MGV48-NVENTE             PIC X(07).             00355803
                10 COMM-MGV48-DVENTE             PIC X(08).             00355903
                10 COMM-MGV48-NORDRE             PIC X(05).             00356003
                10 COMM-MGV48-NUMRES             PIC X(07).             00357003
      * ->                                                              00358003
          02 COMM-MGV48-SORTIE.                                         00359003
             05 COMM-MGV48-NLIGNE-ERR        PIC X(02).                 00360003
             05 COMM-MGV48-INDICE-ERR        PIC 9(02).                 00370003
             05 COMM-MGV48-MESSAGE.                                     00380003
                10 COMM-MGV48-CODRET         PIC X VALUE SPACES.        00390003
                   88 COMM-MGV48-OK          VALUE ' '.                 00400003
                   88 COMM-MGV48-ERR-BLQ     VALUE '1'.                 00410003
                   88 COMM-MGV48-ERR-STO     VALUE '2'.                 00420003
                   88 COMM-MGV48-ERR-NON-BLQ VALUE '3'.                 00430003
                10 COMM-MGV48-LIBERR         PIC X(58).                 00440003
      * ->                                                              00441003
          02 COMM-MGV48-DATAS.                                          00442003
             05 COMM-MGV48-DATA-LOC OCCURS 40.                          00443003
      * -> LIEU A INTERROGER                                            00444003
                10 COMM-MGV48-NSOCDEPOT     PIC X(03).                  00445003
                10 COMM-MGV48-NLIEUDEPOT    PIC X(03).                  00446003
      * -> TYPE D'INTERROGATION / RESERVATION                           00447003
                10 COMM-MGV48-TYPESTOCK     PIC X(01).                  00448003
      *{ remove-comma-in-dde 1.5                                                
      *            88 COMM-MGV48-PTF           VALUE 'P' , 'E' , 'L'.           
      *--                                                                       
                   88 COMM-MGV48-PTF           VALUE 'P'   'E'   'L'.           
      *}                                                                        
                   88 COMM-MGV48-PTF-E         VALUE 'E'.               00448204
                   88 COMM-MGV48-PTF-L         VALUE 'L'.               00448304
      *{ remove-comma-in-dde 1.5                                                
      *            88 COMM-MGV48-MAG           VALUE 'M' , ' '.         00448404
      *--                                                                       
                   88 COMM-MGV48-MAG           VALUE 'M'   ' '.                 
      *}                                                                        
      * -> PRODUITS � INTERROGER / RESERVER.                            00460003
                10 COMM-MGV48-NCODIC         PIC X(07).                 00470003
                10 COMM-MGV48-QSTOCK         PIC S9(05).                00480003
                10 COMM-MGV48-NLIGNE         PIC X(02).                 00490003
                10 COMM-MGV48-NSEQNQ         PIC S9(3).                 00500003
                10 COMM-MGV48-NSEQNQ-ORIG    PIC S9(3).                 00510003
                10 COMM-MGV48-CRET           PIC X(01).                 00520003
                                                                                
