      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *    COPY  COMMCL050                                                      
      *                                                                         
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      * COMMAREA DE LA TRANSACTION CL05 PARAMéTRAGE INVENTAIRE POUR    *        
      * MAGASIN NMD                                                    *        
      *                                                                *        
      * Z-COMMAREA, D' UNE LONGUEUR DE 4096 C, COMPRENANT :            *        
      * 1 - LES ZONES RESERVEES A AIDA                                 *        
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                  *        
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                *        
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                     *        
      * 5 - LES ZONES RESERVEES APPLICATIVES                           *        
      *                                                                *        
      * COM-CL05-LONG-COMMAREA A UNE VALUE DE +4096                    *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-CL05-LONG-COMMAREA       PIC S9(4) COMP   VALUE +4096.           
      *--                                                                       
       01  COM-CL05-LONG-COMMAREA       PIC S9(4) COMP-5   VALUE +4096.         
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
           02 FILLER-COM-AIDA           PIC X(100).                             
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
           02 COMM-CICS-APPLID          PIC X(8).                               
           02 COMM-CICS-NETNAM          PIC X(8).                               
           02 COMM-CICS-TRANSA          PIC X(4).                               
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
           02 COMM-DATE-SIECLE          PIC XX.                                 
           02 COMM-DATE-ANNEE           PIC XX.                                 
           02 COMM-DATE-MOIS            PIC XX.                                 
           02 COMM-DATE-JOUR            PIC 99.                                 
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA            PIC 999.                                
           02 COMM-DATE-QNT0            PIC 99999.                              
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX            PIC 9.                                  
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM             PIC 9.                                  
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC          PIC XXX.                                
           02 COMM-DATE-JSM-LL          PIC XXXXXXXX.                           
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC         PIC XXX.                                
           02 COMM-DATE-MOIS-LL         PIC X(9).                               
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ        PIC X(8).                               
           02 COMM-DATE-AAMMJJ          PIC X(6).                               
           02 COMM-DATE-JJMMSSAA        PIC X(8).                               
           02 COMM-DATE-JJMMAA          PIC X(6).                               
           02 COMM-DATE-JJ-MM-AA        PIC X(8).                               
           02 COMM-DATE-JJ-MM-SSAA      PIC X(10).                              
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS        PIC 99.                                 
              05 COMM-DATE-SEMAA        PIC 99.                                 
              05 COMM-DATE-SEMNU        PIC 99.                                 
      *                                                                         
           02 COMM-DATE-FILLER          PIC X(07).                              
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS            PIC S9(4) COMP VALUE -1.                
      *--                                                                       
           02 COMM-SWAP-CURS            PIC S9(4) COMP-5 VALUE -1.              
      *}                                                                        
           02 COMM-SWAP-ATTR            OCCURS 150 PIC X(1).                    
      *                                                                         
      *    ZONES RESERVEES APPLICATIVES (3724 C) ---------------------          
      *                                                                         
      * -CL05-                                                                  
           02 COMM-CL05-APPLI.                                                  
             05 COMM-CL05-NSOCIETE      PIC X(03).                              
      * -CL06--                                                                 
           02 COMM-CL06-APPLI.                                                  
             05 COMM-CL06-PAGE-COURANTE PIC S9(03) COMP-3.                      
             05 COMM-CL06-NBR-PAGE      PIC S9(03) COMP-3.                      
             05 COMM-CL06-NB-LIPA-LUES  PIC  9(02) COMP-3.                      
             05 COMM-CL06-CTRL-LECTURE  PIC 9.                                  
                88 COMM-CL06-RAF-TS                 VALUE 1.                    
      * - DEMANDE DE SUPPRESSION                                                
             05 COMM-CL06-ACTION-USER   PIC 9.                                  
                88 COMM-CL06-DEMA-SUP               VALUE 1.                    
                88 COMM-CL06-CONF-SUP               VALUE 2.                    
                88 COMM-CL06-VUE-DETAIL             VALUE 3.                    
      * - LIGNE CHOISIE POUR LA SUPPRESSION                                     
             05 COMM-CL06-LIGNE-CHOISIE PIC 9(02) COMP-3.                       
      * ------                                                                  
      * - ZONE DE COMMUNICATION ENTRE TCL06 ET TCL07                            
           02 COMM-CL07-APPLI.                                                  
             05 COMM-CL07-CLIEUINV      PIC X(05).                              
             05 COMM-CL07-LLIEUINV      PIC X(20).                              
             05 COMM-CL07-PAGE-COURANTE PIC S9(03) COMP-3.                      
             05 COMM-CL07-NBR-PAGE      PIC S9(03) COMP-3.                      
             05 COMM-CL07-NB-LIPA-LUES  PIC  9(02) COMP-3.                      
             05 COMM-CL07-CTRL-LECTURE  PIC 9.                                  
                88 COMM-CL07-RAF-TS                 VALUE 1.                    
      * - DEMANDE DE SUPPRESSION                                                
             05 COMM-CL07-ACTION-USER   PIC 9.                                  
                88 COMM-CL07-DEMA-SUP               VALUE 1.                    
                88 COMM-CL07-CONF-SUP               VALUE 2.                    
                88 COMM-CL07-VUE-DETAIL             VALUE 3.                    
             05 COMM-CL07-LIGNE-CHOISIE PIC 9(02) COMP-3.                       
      * ------                                                                  
           02 FILLER                    PIC X(3682) VALUE SPACES.               
                                                                                
