      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVRE0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRE0000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRE0000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRE0000.                                                            
      *}                                                                        
           02  RE00-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  RE00-NLIEU                                                       
               PIC X(0003).                                                     
           02  RE00-DMVENTE                                                     
               PIC X(0006).                                                     
           02  RE00-CVENDEUR                                                    
               PIC X(0006).                                                     
           02  RE00-CFAM                                                        
               PIC X(0005).                                                     
           02  RE00-MTVENTES                                                    
               PIC S9(7) COMP-3.                                                
           02  RE00-MTFORCP                                                     
               PIC S9(7) COMP-3.                                                
           02  RE00-MTFORCM                                                     
               PIC S9(7) COMP-3.                                                
           02  RE00-MTREM                                                       
               PIC S9(9) COMP-3.                                                
           02  RE00-CLASSE1                                                     
               PIC S9(3) COMP-3.                                                
           02  RE00-CLASSE2                                                     
               PIC S9(3) COMP-3.                                                
           02  RE00-CLASSE3                                                     
               PIC S9(3) COMP-3.                                                
           02  RE00-CLASSE4                                                     
               PIC S9(3) COMP-3.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRE0000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRE0000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRE0000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RE00-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RE00-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RE00-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RE00-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RE00-DMVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RE00-DMVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RE00-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RE00-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RE00-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RE00-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RE00-MTVENTES-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RE00-MTVENTES-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RE00-MTFORCP-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RE00-MTFORCP-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RE00-MTFORCM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RE00-MTFORCM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RE00-MTREM-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RE00-MTREM-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RE00-CLASSE1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RE00-CLASSE1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RE00-CLASSE2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RE00-CLASSE2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RE00-CLASSE3-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RE00-CLASSE3-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RE00-CLASSE4-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RE00-CLASSE4-F                                                   
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
