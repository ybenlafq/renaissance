      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGV4100                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV4100                         
      **********************************************************                
       01  RVGV4100.                                                            
           02  GV41-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GV41-NLIEU                                                       
               PIC X(0003).                                                     
           02  GV41-DVENTE                                                      
               PIC X(0008).                                                     
           02  GV41-NORDRE                                                      
               PIC X(0005).                                                     
           02  GV41-CMODDEL                                                     
               PIC X(0003).                                                     
           02  GV41-DDELIV                                                      
               PIC X(0008).                                                     
           02  GV41-CZONLIV                                                     
               PIC X(0005).                                                     
           02  GV41-CPLAGE                                                      
               PIC X(0002).                                                     
           02  GV41-CEQUIP                                                      
               PIC X(0005).                                                     
           02  GV41-CTYPADR                                                     
               PIC X(0001).                                                     
           02  GV41-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  GV41-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GV41-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGV4100                                  
      **********************************************************                
       01  RVGV4100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV41-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV41-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV41-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV41-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV41-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV41-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV41-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV41-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV41-CMODDEL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV41-CMODDEL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV41-DDELIV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV41-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV41-CZONLIV-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV41-CZONLIV-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV41-CPLAGE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV41-CPLAGE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV41-CEQUIP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV41-CEQUIP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV41-CTYPADR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV41-CTYPADR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV41-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV41-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV41-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV41-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV41-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GV41-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
