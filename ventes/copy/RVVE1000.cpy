      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA VUE RVVE1000                                               
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA VUE RVVE1000                           
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVE1000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVE1000.                                                            
      *}                                                                        
           02  VE10-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  VE10-NLIEU                                                       
               PIC X(0003).                                                     
           02  VE10-NVENTE                                                      
               PIC X(0007).                                                     
           02  VE10-NORDRE                                                      
               PIC X(0005).                                                     
           02  VE10-NCLIENT                                                     
               PIC X(0009).                                                     
           02  VE10-DVENTE                                                      
               PIC X(0008).                                                     
           02  VE10-DHVENTE                                                     
               PIC X(0002).                                                     
           02  VE10-DMVENTE                                                     
               PIC X(0002).                                                     
           02  VE10-PTTVENTE                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10-PVERSE                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10-PCOMPT                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10-PLIVR                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10-PDIFFERE                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10-PRFACT                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10-CREMVTE                                                     
               PIC X(0005).                                                     
           02  VE10-PREMVTE                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10-LCOMVTE1                                                    
               PIC X(0030).                                                     
           02  VE10-LCOMVTE2                                                    
               PIC X(0030).                                                     
           02  VE10-LCOMVTE3                                                    
               PIC X(0030).                                                     
           02  VE10-LCOMVTE4                                                    
               PIC X(0030).                                                     
           02  VE10-DMODIFVTE                                                   
               PIC X(0008).                                                     
           02  VE10-WFACTURE                                                    
               PIC X(0001).                                                     
           02  VE10-WEXPORT                                                     
               PIC X(0001).                                                     
           02  VE10-WDETAXEC                                                    
               PIC X(0001).                                                     
           02  VE10-WDETAXEHC                                                   
               PIC X(0001).                                                     
           02  VE10-CORGORED                                                    
               PIC X(0005).                                                     
           02  VE10-CMODPAIMTI                                                  
               PIC X(0005).                                                     
           02  VE10-LDESCRIPTIF1                                                
               PIC X(0030).                                                     
           02  VE10-LDESCRIPTIF2                                                
               PIC X(0030).                                                     
           02  VE10-DLIVRBL                                                     
               PIC X(0008).                                                     
           02  VE10-NFOLIOBL                                                    
               PIC X(0003).                                                     
           02  VE10-LAUTORM                                                     
               PIC X(0005).                                                     
           02  VE10-NAUTORD                                                     
               PIC X(0005).                                                     
           02  VE10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  VE10-DSTAT                                                       
               PIC X(04).                                                       
           02  VE10-DFACTURE                                                    
               PIC X(08).                                                       
           02  VE10-CACID                                                       
               PIC X(08).                                                       
           02  VE10-NSOCMODIF                                                   
               PIC X(03).                                                       
           02  VE10-NLIEUMODIF                                                  
               PIC X(03).                                                       
           02  VE10-NSOCP                                                       
               PIC X(03).                                                       
           02  VE10-NLIEUP                                                      
               PIC X(03).                                                       
           02  VE10-CDEV                                                        
               PIC X(03).                                                       
           02  VE10-CFCRED                                                      
               PIC X(05).                                                       
           02  VE10-NCREDI                                                      
               PIC X(14).                                                       
           02  VE10-NSEQNQ                                                      
               PIC S9(5) COMP-3.                                                
           02  VE10-TYPVTE                                                      
               PIC X(01).                                                       
           02  VE10-VTEGPE                                                      
               PIC X(01).                                                       
           02  VE10-CTRMRQ                                                      
               PIC X(08).                                                       
           02  VE10-DATENC                                                      
               PIC X(08).                                                       
           02  VE10-WDGRAD                                                      
               PIC X(01).                                                       
           02  VE10-NAUTO                                                       
               PIC X(20).                                                       
           02  VE10-NSEQENS                                                     
               PIC S9(5) COMP-3.                                                
           02  VE10-NSOCO                                                       
               PIC X(03).                                                       
           02  VE10-NLIEUO                                                      
               PIC X(03).                                                       
           02  VE10-NVENTO                                                      
               PIC X(08).                                                       
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA VUE RVVE1000                                    
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVE1000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVE1000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-NCLIENT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-NCLIENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-DHVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-DHVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-DMVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-DMVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-PTTVENTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-PTTVENTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-PVERSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-PVERSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-PCOMPT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-PCOMPT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-PLIVR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-PLIVR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-PDIFFERE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-PDIFFERE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-PRFACT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-PRFACT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-CREMVTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-CREMVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-PREMVTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-PREMVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-LCOMVTE1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-LCOMVTE1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-LCOMVTE2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-LCOMVTE2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-LCOMVTE3-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-LCOMVTE3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-LCOMVTE4-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-LCOMVTE4-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-DMODIFVTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-DMODIFVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-WFACTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-WFACTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-WEXPORT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-WEXPORT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-WDETAXEC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-WDETAXEC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-WDETAXEHC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-WDETAXEHC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-CORGORED-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-CORGORED-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-CMODPAIMTI-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-CMODPAIMTI-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-LDESCRIPTIF1-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-LDESCRIPTIF1-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-LDESCRIPTIF2-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-LDESCRIPTIF2-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-DLIVRBL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-DLIVRBL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-NFOLIOBL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-NFOLIOBL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-LAUTORM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-LAUTORM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-NAUTORD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-NAUTORD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-DSTAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-DSTAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-DFACTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-DFACTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-CACID-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-CACID-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-NSOCMODIF-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-NSOCMODIF-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-NLIEUMODIF-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-NLIEUMODIF-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-NSOCP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-NSOCP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-NLIEUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-NLIEUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-CDEV-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-CDEV-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-CFCRED-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-CFCRED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-NCREDI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-NCREDI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-NSEQNQ-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-NSEQNQ-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-TYPVTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-TYPVTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-VTEGPE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-VTEGPE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-CTRMRQ-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-CTRMRQ-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-DATENC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-DATENC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-WDGRAD-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-WDGRAD-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-NAUTO-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-NAUTO-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-NSEQENS-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-NSEQENS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-NSOCO-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-NSOCO-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-NLIEUO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-NLIEUO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10-NVENTO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10-NVENTO-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
