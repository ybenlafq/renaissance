      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *    COPY  COMMIT00.                                              00000010
      *                                                                 00000020
      **************************************************************    00000030
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00000040
      **************************************************************    00000050
      *                                                                 00000060
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00000070
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00000080
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00000090
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00000100
      *                                                                 00000110
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00000120
      * COMPRENANT :                                                    00000130
      * 1 - LES ZONES RESERVEES A AIDA                                  00000140
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00000150
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00000160
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00000170
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00000180
      *                                                                 00000190
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00000200
      * PAR AIDA                                                        00000210
      *                                                                 00000220
      *-------------------------------------------------------------    00000230
      *                                                                 00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-IT00-LONG-COMMAREA PIC S9(5) COMP VALUE +4096.           00000250
      *--                                                                       
       01  COM-IT00-LONG-COMMAREA PIC S9(5) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00000260
       01  Z-COMMAREA.                                                  00000270
      *                                                                 00000280
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00000290
           02 FILLER-COM-AIDA      PIC X(100).                          00000300
      *                                                                 00000310
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00000320
           02 COMM-CICS-APPLID     PIC X(8).                            00000330
           02 COMM-CICS-NETNAM     PIC X(8).                            00000340
           02 COMM-CICS-TRANSA     PIC X(4).                            00000350
      *                                                                 00000360
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00000370
           02 COMM-DATE-SIECLE     PIC XX.                              00000380
           02 COMM-DATE-ANNEE      PIC XX.                              00000390
           02 COMM-DATE-MOIS       PIC XX.                              00000400
           02 COMM-DATE-JOUR       PIC 99.                              00000410
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000420
           02 COMM-DATE-QNTA       PIC 999.                             00000430
           02 COMM-DATE-QNT0       PIC 99999.                           00000440
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000450
           02 COMM-DATE-BISX       PIC 9.                               00000460
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00000470
           02 COMM-DATE-JSM        PIC 9.                               00000480
      *   LIBELLES DU JOUR COURT - LONG                                 00000490
           02 COMM-DATE-JSM-LC     PIC XXX.                             00000500
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00000510
      *   LIBELLES DU MOIS COURT - LONG                                 00000520
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00000530
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00000540
      *   DIFFERENTES FORMES DE DATE                                    00000550
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00000560
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00000570
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00000580
           02 COMM-DATE-JJMMAA     PIC X(6).                            00000590
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00000600
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00000610
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00000620
           02 COMM-DATE-WEEK.                                           00000630
              05 COMM-DATE-SEMSS   PIC 99.                              00000640
              05 COMM-DATE-SEMAA   PIC 99.                              00000650
              05 COMM-DATE-SEMNU   PIC 99.                              00000660
      *                                                                 00000670
           02 COMM-DATE-FILLER     PIC X(08).                           00000680
      *                                                                 00000690
      * ZONES RESERVEES TRAITEMENT DU SWAP -----------------------152   00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00000710
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR       OCCURS 150 PIC X(01).                00000720
      *                                                                 00000730
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00000740
           02 FILLER   PIC X(22) VALUE ' *** COMMAREA IT00 ***'.        00000750
           02 COMM-IT00-APPLI.                                          00000760
              03 COMM-IT00-NSOCIETE           PIC X(3).                 00000770
              03 COMM-IT00-CODRET             PIC X(01).                00000780
                 88 COMM-IT00-PAS-MESS        VALUE ' '.                00000790
                 88 COMM-IT00-MESS-RET        VALUE '1'.                00000800
              03 COMM-IT00-MESSAGE            PIC X(50).                00000810
              03 COMM-IT00-NLIEUMIN           PIC X(03).                00000820
              03 COMM-IT00-NLIEUMAX           PIC X(03).                00000830
              03 COMM-IT00-TRAIT              PIC X(01).                00000840
                 88 COMM-IT00-TRTMAG          VALUE ' '.                00000850
                 88 COMM-IT00-TRTSIEGE        VALUE '1'.                00000860
              03 COMM-IT00-FLAGINV            PIC X(1).                 00000870
              03 COMM-IT00-IMPRIM             PIC X(4).                 00000880
              03 COMM-IT00-EDIMAG             PIC X.                    00000890
              03 COMM-IT00-EDIVAL             PIC X.                    00000900
MB            03 COMM-IT00-WLOCAL             PIC X.                    00000910
              03 COMM-IT00-PROG.                                        00000920
                 05 COMM-IT00-FONCTION           PIC X(03).             00000930
                 05 COMM-IT00-NINVENTAIRE        PIC X(05).             00000940
                 05 COMM-IT00-TABLE-CTYPLIEUX.                          00000950
                    10 COMM-IT00-TYPE-LIEUX         OCCURS 4.           00000960
                       15 COMM-IT00-CTYPLIEU    PIC X.                  00000970
                       15 COMM-IT00-DEJA-SAISIE PIC X.                  00000980
                 05 COMM-IT00-DEDITSUPPORT       PIC X(08).             00000990
                 05 COMM-IT00-DSTOCKTHEO         PIC X(08).             00001000
                 05 COMM-IT00-DCOMPTAGE          PIC X(08).             00001010
                 05 COMM-IT00-DEXTRACTION        PIC X(08).             00001020
                 05 COMM-IT00-INDICELIEU         PIC 9(05) COMP-3.      00001030
                 05 COMM-IT00-PAGENC             PIC 9(03).             00001040
                 05 COMM-IT00-PAGFIN             PIC 9(03).             00001050
                 05 COMM-IT01-TABLE-FAMILLE.                            00001060
                    10 COMM-IT01-TAB-LIG         OCCURS 8.              00001070
                       15 COMM-IT01-TAB-COL      OCCURS 8.              00001080
                          20 COMM-CFAM           PIC X(05).             00001090
                          20 COMM-TRAIT-CFAM     PIC X(01).             00001100
                 05 COMM-IT02-TABLE-LIEU.                               00001110
                    10 COMM-IT02-TAB-LIEU  OCCURS 115 ASCENDING         00001120
                            COMM-CLIEU INDEXED X-CLIEU.                 00001130
                       15 COMM-CLIEU          PIC X(03).                00001140
                       15 COMM-LLIEU          PIC X(20).                00001150
                       15 COMM-WFININV        PIC X(01).                00001160
                       15 COMM-WEDITE         PIC X(01).                00001170
                       15 COMM-AV             PIC X(01).                00001180
                       15 COMM-AP             PIC X(01).                00001190
                 05 COMM-IT00-MSELECTLIEUI    PIC X(01).                00001200
              03 COMM-IT00-OPTION.                                      00001210
                 05 COMM-IT00-FILLER             PIC X(108).            00001220
      *                                                                 00001230
      *-------- ZONES PGM IT10 IT20   LONG 45 + 63 FILLER               00001240
      *                                                                 00001250
              03 COMM-IT10-APPLI REDEFINES COMM-IT00-OPTION.            00001260
                 05 COMM-IT10-LSOCIETE PIC X(20).                       00001270
                 05 COMM-IT10-NLIEU  PIC X(3).                          00001280
                 05 COMM-IT10-LLIEU  PIC X(20).                         00001290
                 05 COMM-IT10-FLAG   PIC 9(1).                          00001300
                 05 COMM-IT10-MAGDEP PIC X(1).                          00001310
                     88 COMM-IT10-IT15 VALUE '0'.                       00001320
                     88 COMM-IT10-IT90 VALUE '1'.                       00001330
                 05 COMM-IT10-FILLER PIC X(63).                         00001340
                                                                                
