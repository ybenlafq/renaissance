      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JMD411 AU 13/12/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,10,BI,A,                          *        
      *                           17,03,BI,A,                          *        
      *                           20,03,BI,A,                          *        
      *                           23,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-JMD411.                                                        
            05 NOMETAT-JMD411           PIC X(6) VALUE 'JMD411'.                
            05 RUPTURES-JMD411.                                                 
           10 JMD411-OPER               PIC X(10).                      007  010
           10 JMD411-LIEUEXT            PIC X(03).                      017  003
           10 JMD411-NMAG               PIC X(03).                      020  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 JMD411-SEQUENCE           PIC S9(04) COMP.                023  002
      *--                                                                       
           10 JMD411-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-JMD411.                                                   
           10 JMD411-COPER              PIC X(10).                      025  010
           10 JMD411-LCOPER             PIC X(20).                      035  020
           10 JMD411-NLIEUEXT           PIC X(03).                      055  003
           10 JMD411-NSOC               PIC X(03).                      058  003
           10 JMD411-PCESSION           PIC S9(10)V9(2) COMP-3.         061  007
           10 JMD411-PCESSIONCUM        PIC S9(12)V9(2) COMP-3.         068  008
           10 JMD411-PRMP               PIC S9(10)V9(2) COMP-3.         076  007
           10 JMD411-PRMPCUM            PIC S9(12)V9(2) COMP-3.         083  008
           10 JMD411-QTE                PIC S9(05)      COMP-3.         091  003
           10 JMD411-QTECUM             PIC S9(06)      COMP-3.         094  004
           10 JMD411-DDEB               PIC X(08).                      098  008
           10 JMD411-DFIN               PIC X(08).                      106  008
            05 FILLER                      PIC X(399).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-JMD411-LONG           PIC S9(4)   COMP  VALUE +113.           
      *                                                                         
      *--                                                                       
        01  DSECT-JMD411-LONG           PIC S9(4) COMP-5  VALUE +113.           
                                                                                
      *}                                                                        
