      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRE007      *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,01,BI,A,                          *        
      *                           11,03,BI,A,                          *        
      *                           14,02,BI,A,                          *        
      *                           16,03,BI,A,                          *        
      *                           19,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRE007.                                                        
            05 NOMETAT-IRE007           PIC X(6) VALUE 'IRE007'.                
            05 RUPTURES-IRE007.                                                 
           10 IRE007-NSOCIETE           PIC X(03).                      007  003
           10 IRE007-TYPE-ENR           PIC X(01).                      010  001
           10 IRE007-TLM-ELA            PIC X(03).                      011  003
           10 IRE007-CGRPMAG            PIC X(02).                      014  002
           10 IRE007-NLIEU              PIC X(03).                      016  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRE007-SEQUENCE           PIC S9(04) COMP.                019  002
      *--                                                                       
           10 IRE007-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRE007.                                                   
           10 IRE007-LMOISENC           PIC X(09).                      021  009
           10 IRE007-CAA                PIC S9(13)V9(2) COMP-3.         030  008
           10 IRE007-CAM                PIC S9(13)V9(2) COMP-3.         038  008
           10 IRE007-FORCA              PIC S9(11)V9(2) COMP-3.         046  007
           10 IRE007-FORCM              PIC S9(11)V9(2) COMP-3.         053  007
           10 IRE007-PRMPA              PIC S9(11)V9(2) COMP-3.         060  007
           10 IRE007-PRMPM              PIC S9(11)V9(2) COMP-3.         067  007
           10 IRE007-REMA               PIC S9(11)V9(2) COMP-3.         074  007
           10 IRE007-REMM               PIC S9(11)V9(2) COMP-3.         081  007
            05 FILLER                      PIC X(425).                          
                                                                                
