      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV52   EGV52                                              00000020
      ***************************************************************** 00000030
       01   EGV52I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MS1L      COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MS1L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MS1F      PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MS1I      PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELSOCL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MLIBELSOCL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBELSOCF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLIBELSOCI     PIC X(20).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MM1L      COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MM1L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MM1F      PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MM1I      PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELMAGL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLIBELMAGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBELMAGF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLIBELMAGI     PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 DATEAL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 DATEAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 DATEAF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 DATEAI    PIC X(8).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMAL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNUMAL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNUMAF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNUMAI    PIC X(7).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMGL      COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MMGL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MMGF      PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MMGI      PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOL      COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MSOL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MSOF      PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSOI      PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMOL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNUMOL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNUMOF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNUMOI    PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMVL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNUMVL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNUMVF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNUMVI    PIC X(7).                                       00000530
           02 M6I OCCURS   10 TIMES .                                   00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCL     COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MCL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MCF     PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCI     PIC X(2).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDL     COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MDL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MDF     PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MDI     PIC X(2).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MAL     COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MAL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MAF     PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MAI     PIC X(2).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MBL     COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MBL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MBF     PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MBI     PIC X(2).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MTYPAL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTYPAF  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MTYPAI  PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELAL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MLIBELAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIBELAF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MLIBELAI     PIC X(15).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MZONCMDI  PIC X(12).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(61).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCICSI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNETNAMI  PIC X(8).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MSCREENI  PIC X(4).                                       00001020
      ***************************************************************** 00001030
      * SDF: EGV52   EGV52                                              00001040
      ***************************************************************** 00001050
       01   EGV52O REDEFINES EGV52I.                                    00001060
           02 FILLER    PIC X(12).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MDATJOUA  PIC X.                                          00001090
           02 MDATJOUC  PIC X.                                          00001100
           02 MDATJOUP  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUV  PIC X.                                          00001130
           02 MDATJOUO  PIC X(10).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MS1A      PIC X.                                          00001230
           02 MS1C      PIC X.                                          00001240
           02 MS1P      PIC X.                                          00001250
           02 MS1H      PIC X.                                          00001260
           02 MS1V      PIC X.                                          00001270
           02 MS1O      PIC X(3).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MLIBELSOCA     PIC X.                                     00001300
           02 MLIBELSOCC     PIC X.                                     00001310
           02 MLIBELSOCP     PIC X.                                     00001320
           02 MLIBELSOCH     PIC X.                                     00001330
           02 MLIBELSOCV     PIC X.                                     00001340
           02 MLIBELSOCO     PIC X(20).                                 00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MM1A      PIC X.                                          00001370
           02 MM1C      PIC X.                                          00001380
           02 MM1P      PIC X.                                          00001390
           02 MM1H      PIC X.                                          00001400
           02 MM1V      PIC X.                                          00001410
           02 MM1O      PIC X(3).                                       00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MLIBELMAGA     PIC X.                                     00001440
           02 MLIBELMAGC     PIC X.                                     00001450
           02 MLIBELMAGP     PIC X.                                     00001460
           02 MLIBELMAGH     PIC X.                                     00001470
           02 MLIBELMAGV     PIC X.                                     00001480
           02 MLIBELMAGO     PIC X(20).                                 00001490
           02 FILLER    PIC X(2).                                       00001500
           02 DATEAA    PIC X.                                          00001510
           02 DATEAC    PIC X.                                          00001520
           02 DATEAP    PIC X.                                          00001530
           02 DATEAH    PIC X.                                          00001540
           02 DATEAV    PIC X.                                          00001550
           02 DATEAO    PIC X(8).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MNUMAA    PIC X.                                          00001580
           02 MNUMAC    PIC X.                                          00001590
           02 MNUMAP    PIC X.                                          00001600
           02 MNUMAH    PIC X.                                          00001610
           02 MNUMAV    PIC X.                                          00001620
           02 MNUMAO    PIC X(7).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MMGA      PIC X.                                          00001650
           02 MMGC      PIC X.                                          00001660
           02 MMGP      PIC X.                                          00001670
           02 MMGH      PIC X.                                          00001680
           02 MMGV      PIC X.                                          00001690
           02 MMGO      PIC X(3).                                       00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MSOA      PIC X.                                          00001720
           02 MSOC      PIC X.                                          00001730
           02 MSOP      PIC X.                                          00001740
           02 MSOH      PIC X.                                          00001750
           02 MSOV      PIC X.                                          00001760
           02 MSOO      PIC X(3).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MNUMOA    PIC X.                                          00001790
           02 MNUMOC    PIC X.                                          00001800
           02 MNUMOP    PIC X.                                          00001810
           02 MNUMOH    PIC X.                                          00001820
           02 MNUMOV    PIC X.                                          00001830
           02 MNUMOO    PIC X(5).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MNUMVA    PIC X.                                          00001860
           02 MNUMVC    PIC X.                                          00001870
           02 MNUMVP    PIC X.                                          00001880
           02 MNUMVH    PIC X.                                          00001890
           02 MNUMVV    PIC X.                                          00001900
           02 MNUMVO    PIC X(7).                                       00001910
           02 M6O OCCURS   10 TIMES .                                   00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MCA     PIC X.                                          00001940
             03 MCC     PIC X.                                          00001950
             03 MCP     PIC X.                                          00001960
             03 MCH     PIC X.                                          00001970
             03 MCV     PIC X.                                          00001980
             03 MCO     PIC X(2).                                       00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MDA     PIC X.                                          00002010
             03 MDC     PIC X.                                          00002020
             03 MDP     PIC X.                                          00002030
             03 MDH     PIC X.                                          00002040
             03 MDV     PIC X.                                          00002050
             03 MDO     PIC X(2).                                       00002060
             03 FILLER       PIC X(2).                                  00002070
             03 MAA     PIC X.                                          00002080
             03 MAC     PIC X.                                          00002090
             03 MAP     PIC X.                                          00002100
             03 MAH     PIC X.                                          00002110
             03 MAV     PIC X.                                          00002120
             03 MAO     PIC X(2).                                       00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MBA     PIC X.                                          00002150
             03 MBC     PIC X.                                          00002160
             03 MBP     PIC X.                                          00002170
             03 MBH     PIC X.                                          00002180
             03 MBV     PIC X.                                          00002190
             03 MBO     PIC X(2).                                       00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MTYPAA  PIC X.                                          00002220
             03 MTYPAC  PIC X.                                          00002230
             03 MTYPAP  PIC X.                                          00002240
             03 MTYPAH  PIC X.                                          00002250
             03 MTYPAV  PIC X.                                          00002260
             03 MTYPAO  PIC X(5).                                       00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MLIBELAA     PIC X.                                     00002290
             03 MLIBELAC     PIC X.                                     00002300
             03 MLIBELAP     PIC X.                                     00002310
             03 MLIBELAH     PIC X.                                     00002320
             03 MLIBELAV     PIC X.                                     00002330
             03 MLIBELAO     PIC X(15).                                 00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MZONCMDA  PIC X.                                          00002360
           02 MZONCMDC  PIC X.                                          00002370
           02 MZONCMDP  PIC X.                                          00002380
           02 MZONCMDH  PIC X.                                          00002390
           02 MZONCMDV  PIC X.                                          00002400
           02 MZONCMDO  PIC X(12).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLIBERRA  PIC X.                                          00002430
           02 MLIBERRC  PIC X.                                          00002440
           02 MLIBERRP  PIC X.                                          00002450
           02 MLIBERRH  PIC X.                                          00002460
           02 MLIBERRV  PIC X.                                          00002470
           02 MLIBERRO  PIC X(61).                                      00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCODTRAA  PIC X.                                          00002500
           02 MCODTRAC  PIC X.                                          00002510
           02 MCODTRAP  PIC X.                                          00002520
           02 MCODTRAH  PIC X.                                          00002530
           02 MCODTRAV  PIC X.                                          00002540
           02 MCODTRAO  PIC X(4).                                       00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MCICSA    PIC X.                                          00002570
           02 MCICSC    PIC X.                                          00002580
           02 MCICSP    PIC X.                                          00002590
           02 MCICSH    PIC X.                                          00002600
           02 MCICSV    PIC X.                                          00002610
           02 MCICSO    PIC X(5).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MNETNAMA  PIC X.                                          00002640
           02 MNETNAMC  PIC X.                                          00002650
           02 MNETNAMP  PIC X.                                          00002660
           02 MNETNAMH  PIC X.                                          00002670
           02 MNETNAMV  PIC X.                                          00002680
           02 MNETNAMO  PIC X(8).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MSCREENA  PIC X.                                          00002710
           02 MSCREENC  PIC X.                                          00002720
           02 MSCREENP  PIC X.                                          00002730
           02 MSCREENH  PIC X.                                          00002740
           02 MSCREENV  PIC X.                                          00002750
           02 MSCREENO  PIC X(4).                                       00002760
                                                                                
