      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVPP0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPP0000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVPP0000.                                                            
           02  PP00-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  PP00-NLIEU                                                       
               PIC X(0003).                                                     
           02  PP00-DMOISPAYE                                                   
               PIC X(0006).                                                     
           02  PP00-CPRIME                                                      
               PIC X(0005).                                                     
           02  PP00-PENVELOPPE                                                  
               PIC S9(5)V9(0002) COMP-3.                                        
           02  PP00-DDEVERSIGA                                                  
               PIC X(0008).                                                     
           02  PP00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPP0000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVPP0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP00-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP00-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP00-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP00-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP00-DMOISPAYE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP00-DMOISPAYE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP00-CPRIME-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP00-CPRIME-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP00-PENVELOPPE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP00-PENVELOPPE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP00-DDEVERSIGA-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP00-DDEVERSIGA-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
