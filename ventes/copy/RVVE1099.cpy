      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVVE1099                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVE1099.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVE1099.                                                            
      *}                                                                        
      *                       NSOCIETE                                          
           10 VE10-NSOCIETE        PIC X(3).                                    
      *                       NLIEU                                             
           10 VE10-NLIEU           PIC X(3).                                    
      *                       NVENTE                                            
           10 VE10-NVENTE          PIC X(7).                                    
      *                       NORDRE                                            
           10 VE10-NORDRE          PIC X(5).                                    
      *                       NCLIENT                                           
           10 VE10-NCLIENT         PIC X(9).                                    
      *                       DVENTE                                            
           10 VE10-DVENTE          PIC X(8).                                    
      *                       DHVENTE                                           
           10 VE10-DHVENTE         PIC X(2).                                    
      *                       DMVENTE                                           
           10 VE10-DMVENTE         PIC X(2).                                    
      *                       PTTVENTE                                          
           10 VE10-PTTVENTE        PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       PVERSE                                            
           10 VE10-PVERSE          PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       PCOMPT                                            
           10 VE10-PCOMPT          PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       PLIVR                                             
           10 VE10-PLIVR           PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       PDIFFERE                                          
           10 VE10-PDIFFERE        PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       PRFACT                                            
           10 VE10-PRFACT          PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       CREMVTE                                           
           10 VE10-CREMVTE         PIC X(5).                                    
      *                       PREMVTE                                           
           10 VE10-PREMVTE         PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       LCOMVTE1                                          
           10 VE10-LCOMVTE1        PIC X(30).                                   
      *                       LCOMVTE2                                          
           10 VE10-LCOMVTE2        PIC X(30).                                   
      *                       LCOMVTE3                                          
           10 VE10-LCOMVTE3        PIC X(30).                                   
      *                       LCOMVTE4                                          
           10 VE10-LCOMVTE4        PIC X(30).                                   
      *                       DMODIFVTE                                         
           10 VE10-DMODIFVTE       PIC X(8).                                    
      *                       WFACTURE                                          
           10 VE10-WFACTURE        PIC X(1).                                    
      *                       WEXPORT                                           
           10 VE10-WEXPORT         PIC X(1).                                    
      *                       WDETAXEC                                          
           10 VE10-WDETAXEC        PIC X(1).                                    
      *                       WDETAXEHC                                         
           10 VE10-WDETAXEHC       PIC X(1).                                    
      *                       CORGORED                                          
           10 VE10-CORGORED        PIC X(5).                                    
      *                       CMODPAIMTI                                        
           10 VE10-CMODPAIMTI      PIC X(5).                                    
      *                       LDESCRIPTIF1                                      
           10 VE10-LDESCRIPTIF1    PIC X(30).                                   
      *                       LDESCRIPTIF2                                      
           10 VE10-LDESCRIPTIF2    PIC X(30).                                   
      *                       DLIVRBL                                           
           10 VE10-DLIVRBL         PIC X(8).                                    
      *                       NFOLIOBL                                          
           10 VE10-NFOLIOBL        PIC X(3).                                    
      *                       LAUTORM                                           
           10 VE10-LAUTORM         PIC X(5).                                    
      *                       NAUTORD                                           
           10 VE10-NAUTORD         PIC X(5).                                    
      *                       DSYST                                             
           10 VE10-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       DSTAT                                             
           10 VE10-DSTAT           PIC X(4).                                    
      *                       DFACTURE                                          
           10 VE10-DFACTURE        PIC X(8).                                    
      *                       CACID                                             
           10 VE10-CACID           PIC X(8).                                    
      *                       NSOCMODIF                                         
           10 VE10-NSOCMODIF       PIC X(3).                                    
      *                       NLIEUMODIF                                        
           10 VE10-NLIEUMODIF      PIC X(3).                                    
      *                       NSOCP                                             
           10 VE10-NSOCP           PIC X(3).                                    
      *                       NLIEUP                                            
           10 VE10-NLIEUP          PIC X(3).                                    
      *                       CDEV                                              
           10 VE10-CDEV            PIC X(3).                                    
      *                       CFCRED                                            
           10 VE10-CFCRED          PIC X(5).                                    
      *                       NCREDI                                            
           10 VE10-NCREDI          PIC X(14).                                   
      *                       NSEQNQ                                            
           10 VE10-NSEQNQ          PIC S9(5)V USAGE COMP-3.                     
      *                       TYPVTE                                            
           10 VE10-TYPVTE          PIC X(1).                                    
      *                       VTEGPE                                            
           10 VE10-VTEGPE          PIC X(1).                                    
      *                       CTRMRQ                                            
           10 VE10-CTRMRQ          PIC X(8).                                    
      *                       DATENC                                            
           10 VE10-DATENC          PIC X(8).                                    
      *                       WDGRAD                                            
           10 VE10-WDGRAD          PIC X(1).                                    
      *                       NAUTO                                             
           10 VE10-NAUTO           PIC X(20).                                   
      *                       NSEQENS                                           
           10 VE10-NSEQENS         PIC S9(5)V USAGE COMP-3.                     
      *                       NSOCO                                             
           10 VE10-NSOCO           PIC X(3).                                    
      *                       NLIEUO                                            
           10 VE10-NLIEUO          PIC X(3).                                    
      *                       NVENTO                                            
           10 VE10-NVENTO          PIC X(8).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 55      *        
      ******************************************************************        
                                                                                
