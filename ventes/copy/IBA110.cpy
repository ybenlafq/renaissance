      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IBA110 AU 14/03/2008  *        
      *                                                                *        
      *          CRITERES DE TRI  07,10,BI,A,                          *        
      *                           17,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IBA110.                                                        
            05 NOMETAT-IBA110           PIC X(6) VALUE 'IBA110'.                
            05 RUPTURES-IBA110.                                                 
           10 IBA110-NTIERSSAP          PIC X(10).                      007  010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IBA110-SEQUENCE           PIC S9(04) COMP.                017  002
      *--                                                                       
           10 IBA110-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IBA110.                                                   
           10 IBA110-CTIERSBA           PIC X(06).                      019  006
           10 IBA110-PBA                PIC S9(07)V9(2) COMP-3.         025  005
           10 IBA110-PBA-1              PIC S9(07)V9(2) COMP-3.         030  005
           10 IBA110-PBA-2              PIC S9(07)V9(2) COMP-3.         035  005
           10 IBA110-PBA-3              PIC S9(07)V9(2) COMP-3.         040  005
           10 IBA110-PBA-4              PIC S9(07)V9(2) COMP-3.         045  005
           10 IBA110-PBA-5              PIC S9(07)V9(2) COMP-3.         050  005
           10 IBA110-DMOIS              PIC X(08).                      055  008
           10 IBA110-DMOIS-1            PIC X(08).                      063  008
           10 IBA110-DMOIS-2            PIC X(08).                      071  008
           10 IBA110-DMOIS-3            PIC X(08).                      079  008
           10 IBA110-DMOIS-4            PIC X(08).                      087  008
           10 IBA110-DMOIS-5            PIC X(08).                      095  008
            05 FILLER                      PIC X(410).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IBA110-LONG           PIC S9(4)   COMP  VALUE +102.           
      *                                                                         
      *--                                                                       
        01  DSECT-IBA110-LONG           PIC S9(4) COMP-5  VALUE +102.           
                                                                                
      *}                                                                        
