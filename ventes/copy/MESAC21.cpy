      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:04 >
      
      *****************************************************************
      *   COPY MESSAGE MQ CHIFFRES AFFAIRES MAGASINS
      *   PGM MAC21
      *****************************************************************
      *****************************************************************
      *   COPY ENTETE DES MESSAGES MQ RECUPERES PAR LE HOST
      *****************************************************************
      *
       01  WS-MQ10.
      * ZONES D'ECHANGE AVEC MQHI (IDENTIFIANT MSG / CODE RET TRT)
         02 WS-QUEUE.
               10   MQ10-MSGID       PIC  X(24).
               10   MQ10-CORRELID    PIC  X(24).
         02 WS-CODRET                PIC  XX.
      * DESCRIPTION DU MSG
         02 MESSL.
      *    ENTETE STANDARD                                       LG=105C
           05  MESSL-ENTETE.
               10   MESSL-TYPE       PIC  X(03).
               10   MESSL-NSOCMSG    PIC  X(03).
               10   MESSL-NLIEUMSG   PIC  X(03).
               10   MESSL-NSOCDST    PIC  X(03).
               10   MESSL-NLIEUDST   PIC  X(03).
               10   MESSL-NORD       PIC  9(08).
               10   MESSL-LPROG      PIC  X(10).
               10   MESSL-DJOUR      PIC  X(08).
               10   MESSL-WSID       PIC  X(10).
               10   MESSL-USER       PIC  X(10).
               10   MESSL-CHRONO     PIC  9(07).
               10   MESSL-NBRMSG     PIC  9(07).
               10   MESSL-NBRENR     PIC  9(05).
               10   MESSL-TAILLE     PIC  9(05).
               10   MESSL-VERSION    PIC  X(02).
               10   MESSL-DSYST      PIC S9(13).
               10   MESSL-FILLER     PIC  X(05).
      *
      *---- FIN COPIE
           05  MES21-DATA                    PIC X(5845).
      *
      * Version 1.
       01  MES21-DATA-V1.
           15  MES21-V1-DATE                 PIC X(8).
           15  MES21-V1-NSOCIETE             PIC X(3).
           15  MES21-V1-NLIEU                PIC X(3).
           15  MES21-V1-QTLM                 PIC S9(4) COMP-3.
           15  MES21-V1-QELA                 PIC S9(4) COMP-3.
           15  MES21-V1-QDACEM               PIC S9(4) COMP-3.
           15  MES21-V1-CATLM                PIC S9(7) COMP-3.
           15  MES21-V1-CAELA                PIC S9(7) COMP-3.
           15  MES21-V1-CADACEM              PIC S9(7) COMP-3.
           15  MES21-V1-QTRDARTY             PIC S9(4) COMP-3.
           15  MES21-V1-QTRDACEM             PIC S9(4) COMP-3.
      *
      * Version 2.
       01  MES21-DATA-V2.
           05  MES21-V2-NSOCIETE             PIC X(3).
           05  MES21-V2-NLIEU                PIC X(3).
           05  MES21-V2-DATE                 PIC X(8).
           05  MES21-V2-DONNEES-HORAIRES  OCCURS 24.
               10  MES21-V2-QTLM             PIC S9(5).
               10  MES21-V2-QELA             PIC S9(5).
               10  MES21-V2-QDACEM           PIC S9(5).
               10  MES21-V2-CATLM            PIC S9(10)V99.
               10  MES21-V2-CAELA            PIC S9(10)V99.
               10  MES21-V2-CADACEM          PIC S9(10)V99.
               10  MES21-V2-QTRDARTY         PIC S9(8).
               10  MES21-V2-QENTREE          PIC S9(5).
      *
           05  MES21-V2-DONNEES-ENTREE    OCCURS 96.
               10  MES21-V2-DE-HEURE         PIC X(6).
               10  MES21-V2-DE-QENTREE       PIC S9(5).
      *
      * Version 3.
       01  MES21-DATA-V3.
           05  MES21-V3-NSOCIETE             PIC X(3).
           05  MES21-V3-NLIEU                PIC X(3).
           05  MES21-V3-DATE                 PIC X(8).
           05  MES21-V3-DONNEES-HORAIRES  OCCURS 24.
               10  MES21-V3-QTLM             PIC S9(5).
               10  MES21-V3-QELA             PIC S9(5).
               10  MES21-V3-QDACEM           PIC S9(5).
               10  MES21-V3-CATLM            PIC S9(10)V99.
               10  MES21-V3-CAELA            PIC S9(10)V99.
               10  MES21-V3-CADACEM          PIC S9(10)V99.
               10  MES21-V3-QTRDARTY         PIC S9(8).
               10  MES21-V3-QENTREE          PIC S9(5).
      *
           05  MES21-V3-DONNEES-ENTREE    OCCURS 96.
               10  MES21-V3-DE-HEURE         PIC X(6).
               10  MES21-V3-DE-QENTREE       PIC S9(5).
               10  MES21-V3-DE-QBOUCHON      PIC X(2).
               10  MES21-V3-DE-QSTATUT       PIC X(2).
               10  MES21-V3-DE-WPRESENCE     PIC X.
      
