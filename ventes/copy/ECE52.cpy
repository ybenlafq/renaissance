      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * menu definition format etiquette                                00000020
      ***************************************************************** 00000030
       01   ECE52I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNCODICI  PIC X(7).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCFAMI    PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCMARQI   PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLREFFOURNI    PIC X(20).                                 00000290
           02 MTABLEAUI OCCURS   6 TIMES .                              00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFORMATL    COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MCFORMATL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCFORMATF    PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MCFORMATI    PIC X(5).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFORMATL    COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MLFORMATL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLFORMATF    PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MLFORMATI    PIC X(20).                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIXL  COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MPRIXL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPRIXF  PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MPRIXI  PIC X.                                          00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMPATIBLEL      COMP PIC S9(4).                       00000430
      *--                                                                       
             03 MCOMPATIBLEL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 MCOMPATIBLEF      PIC X.                                00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCOMPATIBLEI      PIC X(5).                             00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMAJL  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MDMAJL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDMAJF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MDMAJI  PIC X(10).                                      00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVERSIONL      COMP PIC S9(4).                            00000510
      *--                                                                       
           02 MVERSIONL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MVERSIONF      PIC X.                                     00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MVERSIONI      PIC X.                                     00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVERSIONMAXL   COMP PIC S9(4).                            00000550
      *--                                                                       
           02 MVERSIONMAXL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MVERSIONMAXF   PIC X.                                     00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MVERSIONMAXI   PIC X.                                     00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MPAGEI    PIC X(3).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000630
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MPAGEMAXI      PIC X(3).                                  00000660
           02 MTABI OCCURS   14 TIMES .                                 00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELLEL    COMP PIC S9(4).                            00000680
      *--                                                                       
             03 MLIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBELLEF    PIC X.                                     00000690
             03 FILLER  PIC X(4).                                       00000700
             03 MLIBELLEI    PIC X(67).                                 00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVALEURL     COMP PIC S9(4).                            00000720
      *--                                                                       
             03 MVALEURL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MVALEURF     PIC X.                                     00000730
             03 FILLER  PIC X(4).                                       00000740
             03 MVALEURI     PIC X(7).                                  00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIX2L      COMP PIC S9(4).                            00000760
      *--                                                                       
             03 MPRIX2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPRIX2F      PIC X.                                     00000770
             03 FILLER  PIC X(4).                                       00000780
             03 MPRIX2I      PIC X.                                     00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPEL  COMP PIC S9(4).                                 00000800
      *--                                                                       
             03 MTYPEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTYPEF  PIC X.                                          00000810
             03 FILLER  PIC X(4).                                       00000820
             03 MTYPEI  PIC X.                                          00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MZONCMDI  PIC X(15).                                      00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MLIBERRI  PIC X(57).                                      00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MCODTRAI  PIC X(4).                                       00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MCICSI    PIC X(5).                                       00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MNETNAMI  PIC X(8).                                       00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MSCREENI  PIC X(4).                                       00001070
      ***************************************************************** 00001080
      * menu definition format etiquette                                00001090
      ***************************************************************** 00001100
       01   ECE52O REDEFINES ECE52I.                                    00001110
           02 FILLER    PIC X(12).                                      00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MDATJOUA  PIC X.                                          00001140
           02 MDATJOUC  PIC X.                                          00001150
           02 MDATJOUP  PIC X.                                          00001160
           02 MDATJOUH  PIC X.                                          00001170
           02 MDATJOUV  PIC X.                                          00001180
           02 MDATJOUO  PIC X(10).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MTIMJOUA  PIC X.                                          00001210
           02 MTIMJOUC  PIC X.                                          00001220
           02 MTIMJOUP  PIC X.                                          00001230
           02 MTIMJOUH  PIC X.                                          00001240
           02 MTIMJOUV  PIC X.                                          00001250
           02 MTIMJOUO  PIC X(5).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MNCODICA  PIC X.                                          00001280
           02 MNCODICC  PIC X.                                          00001290
           02 MNCODICP  PIC X.                                          00001300
           02 MNCODICH  PIC X.                                          00001310
           02 MNCODICV  PIC X.                                          00001320
           02 MNCODICO  PIC X(7).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MCFAMA    PIC X.                                          00001350
           02 MCFAMC    PIC X.                                          00001360
           02 MCFAMP    PIC X.                                          00001370
           02 MCFAMH    PIC X.                                          00001380
           02 MCFAMV    PIC X.                                          00001390
           02 MCFAMO    PIC X(5).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MCMARQA   PIC X.                                          00001420
           02 MCMARQC   PIC X.                                          00001430
           02 MCMARQP   PIC X.                                          00001440
           02 MCMARQH   PIC X.                                          00001450
           02 MCMARQV   PIC X.                                          00001460
           02 MCMARQO   PIC X(5).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MLREFFOURNA    PIC X.                                     00001490
           02 MLREFFOURNC    PIC X.                                     00001500
           02 MLREFFOURNP    PIC X.                                     00001510
           02 MLREFFOURNH    PIC X.                                     00001520
           02 MLREFFOURNV    PIC X.                                     00001530
           02 MLREFFOURNO    PIC X(20).                                 00001540
           02 MTABLEAUO OCCURS   6 TIMES .                              00001550
             03 FILLER       PIC X(2).                                  00001560
             03 MCFORMATA    PIC X.                                     00001570
             03 MCFORMATC    PIC X.                                     00001580
             03 MCFORMATP    PIC X.                                     00001590
             03 MCFORMATH    PIC X.                                     00001600
             03 MCFORMATV    PIC X.                                     00001610
             03 MCFORMATO    PIC X(5).                                  00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MLFORMATA    PIC X.                                     00001640
             03 MLFORMATC    PIC X.                                     00001650
             03 MLFORMATP    PIC X.                                     00001660
             03 MLFORMATH    PIC X.                                     00001670
             03 MLFORMATV    PIC X.                                     00001680
             03 MLFORMATO    PIC X(20).                                 00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MPRIXA  PIC X.                                          00001710
             03 MPRIXC  PIC X.                                          00001720
             03 MPRIXP  PIC X.                                          00001730
             03 MPRIXH  PIC X.                                          00001740
             03 MPRIXV  PIC X.                                          00001750
             03 MPRIXO  PIC X.                                          00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MCOMPATIBLEA      PIC X.                                00001780
             03 MCOMPATIBLEC PIC X.                                     00001790
             03 MCOMPATIBLEP PIC X.                                     00001800
             03 MCOMPATIBLEH PIC X.                                     00001810
             03 MCOMPATIBLEV PIC X.                                     00001820
             03 MCOMPATIBLEO      PIC X(5).                             00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MDMAJA  PIC X.                                          00001850
             03 MDMAJC  PIC X.                                          00001860
             03 MDMAJP  PIC X.                                          00001870
             03 MDMAJH  PIC X.                                          00001880
             03 MDMAJV  PIC X.                                          00001890
             03 MDMAJO  PIC X(10).                                      00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MVERSIONA      PIC X.                                     00001920
           02 MVERSIONC PIC X.                                          00001930
           02 MVERSIONP PIC X.                                          00001940
           02 MVERSIONH PIC X.                                          00001950
           02 MVERSIONV PIC X.                                          00001960
           02 MVERSIONO      PIC X.                                     00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MVERSIONMAXA   PIC X.                                     00001990
           02 MVERSIONMAXC   PIC X.                                     00002000
           02 MVERSIONMAXP   PIC X.                                     00002010
           02 MVERSIONMAXH   PIC X.                                     00002020
           02 MVERSIONMAXV   PIC X.                                     00002030
           02 MVERSIONMAXO   PIC X.                                     00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MPAGEA    PIC X.                                          00002060
           02 MPAGEC    PIC X.                                          00002070
           02 MPAGEP    PIC X.                                          00002080
           02 MPAGEH    PIC X.                                          00002090
           02 MPAGEV    PIC X.                                          00002100
           02 MPAGEO    PIC X(3).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MPAGEMAXA      PIC X.                                     00002130
           02 MPAGEMAXC PIC X.                                          00002140
           02 MPAGEMAXP PIC X.                                          00002150
           02 MPAGEMAXH PIC X.                                          00002160
           02 MPAGEMAXV PIC X.                                          00002170
           02 MPAGEMAXO      PIC X(3).                                  00002180
           02 MTABO OCCURS   14 TIMES .                                 00002190
             03 FILLER       PIC X(2).                                  00002200
             03 MLIBELLEA    PIC X.                                     00002210
             03 MLIBELLEC    PIC X.                                     00002220
             03 MLIBELLEP    PIC X.                                     00002230
             03 MLIBELLEH    PIC X.                                     00002240
             03 MLIBELLEV    PIC X.                                     00002250
             03 MLIBELLEO    PIC X(67).                                 00002260
             03 FILLER       PIC X(2).                                  00002270
             03 MVALEURA     PIC X.                                     00002280
             03 MVALEURC     PIC X.                                     00002290
             03 MVALEURP     PIC X.                                     00002300
             03 MVALEURH     PIC X.                                     00002310
             03 MVALEURV     PIC X.                                     00002320
             03 MVALEURO     PIC X(7).                                  00002330
             03 FILLER       PIC X(2).                                  00002340
             03 MPRIX2A      PIC X.                                     00002350
             03 MPRIX2C PIC X.                                          00002360
             03 MPRIX2P PIC X.                                          00002370
             03 MPRIX2H PIC X.                                          00002380
             03 MPRIX2V PIC X.                                          00002390
             03 MPRIX2O      PIC X.                                     00002400
             03 FILLER       PIC X(2).                                  00002410
             03 MTYPEA  PIC X.                                          00002420
             03 MTYPEC  PIC X.                                          00002430
             03 MTYPEP  PIC X.                                          00002440
             03 MTYPEH  PIC X.                                          00002450
             03 MTYPEV  PIC X.                                          00002460
             03 MTYPEO  PIC X.                                          00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MZONCMDA  PIC X.                                          00002490
           02 MZONCMDC  PIC X.                                          00002500
           02 MZONCMDP  PIC X.                                          00002510
           02 MZONCMDH  PIC X.                                          00002520
           02 MZONCMDV  PIC X.                                          00002530
           02 MZONCMDO  PIC X(15).                                      00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MLIBERRA  PIC X.                                          00002560
           02 MLIBERRC  PIC X.                                          00002570
           02 MLIBERRP  PIC X.                                          00002580
           02 MLIBERRH  PIC X.                                          00002590
           02 MLIBERRV  PIC X.                                          00002600
           02 MLIBERRO  PIC X(57).                                      00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MCODTRAA  PIC X.                                          00002630
           02 MCODTRAC  PIC X.                                          00002640
           02 MCODTRAP  PIC X.                                          00002650
           02 MCODTRAH  PIC X.                                          00002660
           02 MCODTRAV  PIC X.                                          00002670
           02 MCODTRAO  PIC X(4).                                       00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MCICSA    PIC X.                                          00002700
           02 MCICSC    PIC X.                                          00002710
           02 MCICSP    PIC X.                                          00002720
           02 MCICSH    PIC X.                                          00002730
           02 MCICSV    PIC X.                                          00002740
           02 MCICSO    PIC X(5).                                       00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MNETNAMA  PIC X.                                          00002770
           02 MNETNAMC  PIC X.                                          00002780
           02 MNETNAMP  PIC X.                                          00002790
           02 MNETNAMH  PIC X.                                          00002800
           02 MNETNAMV  PIC X.                                          00002810
           02 MNETNAMO  PIC X(8).                                       00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MSCREENA  PIC X.                                          00002840
           02 MSCREENC  PIC X.                                          00002850
           02 MSCREENP  PIC X.                                          00002860
           02 MSCREENH  PIC X.                                          00002870
           02 MSCREENV  PIC X.                                          00002880
           02 MSCREENO  PIC X(4).                                       00002890
                                                                                
