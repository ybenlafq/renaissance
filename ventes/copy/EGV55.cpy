      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV55   EGV55                                              00000020
      ***************************************************************** 00000030
       01   EGV55I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * NUMERO DE PAGE COURANTE                                         00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOPAGEL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MNOPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOPAGEF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MNOPAGEI  PIC X(3).                                       00000200
      * NOMBRE TOTAL DE PAGES                                           00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGEL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPAGEF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBPAGEI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPEL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MTYPEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTYPEF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MTYPEI    PIC X(35).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLNOML   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCLNOML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCLNOMF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCLNOMI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCNTELL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCNTELL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCNTELF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCNTELI   PIC X(10).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCPOSTALL     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MCCPOSTALL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCCPOSTALF     PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCCPOSTALI     PIC X(5).                                  00000410
           02 MLIGNEI OCCURS   10 TIMES .                               00000420
      * ZONE DE SELECTION                                               00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSELECTL   COMP PIC S9(4).                            00000440
      *--                                                                       
               04 MSELECTL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MSELECTF   PIC X.                                     00000450
               04 FILLER     PIC X(4).                                  00000460
               04 MSELECTI   PIC X.                                     00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNMAGL     COMP PIC S9(4).                            00000480
      *--                                                                       
               04 MNMAGL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MNMAGF     PIC X.                                     00000490
               04 FILLER     PIC X(4).                                  00000500
               04 MNMAGI     PIC X(3).                                  00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNVENTEL   COMP PIC S9(4).                            00000520
      *--                                                                       
               04 MNVENTEL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MNVENTEF   PIC X.                                     00000530
               04 FILLER     PIC X(4).                                  00000540
               04 MNVENTEI   PIC X(7).                                  00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MLNOML     COMP PIC S9(4).                            00000560
      *--                                                                       
               04 MLNOML COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MLNOMF     PIC X.                                     00000570
               04 FILLER     PIC X(4).                                  00000580
               04 MLNOMI     PIC X(20).                                 00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MLPRENOML  COMP PIC S9(4).                            00000600
      *--                                                                       
               04 MLPRENOML COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MLPRENOMF  PIC X.                                     00000610
               04 FILLER     PIC X(4).                                  00000620
               04 MLPRENOMI  PIC X(8).                                  00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNTELL     COMP PIC S9(4).                            00000640
      *--                                                                       
               04 MNTELL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MNTELF     PIC X.                                     00000650
               04 FILLER     PIC X(4).                                  00000660
               04 MNTELI     PIC X(10).                                 00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCPOSTALL  COMP PIC S9(4).                            00000680
      *--                                                                       
               04 MCPOSTALL COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MCPOSTALF  PIC X.                                     00000690
               04 FILLER     PIC X(4).                                  00000700
               04 MCPOSTALI  PIC X(5).                                  00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MLCOMMUNEL      COMP PIC S9(4).                       00000720
      *--                                                                       
               04 MLCOMMUNEL COMP-5 PIC S9(4).                                  
      *}                                                                        
               04 MLCOMMUNEF      PIC X.                                00000730
               04 FILLER     PIC X(4).                                  00000740
               04 MLCOMMUNEI      PIC X(17).                            00000750
      * MESSAGE ERREUR                                                  00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MLIBERRI  PIC X(78).                                      00000800
      * CODE TRANSACTION                                                00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCODTRAI  PIC X(4).                                       00000850
      * CICS DE TRAVAIL                                                 00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      * NETNAME                                                         00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MNETNAMI  PIC X(8).                                       00000950
      * CODE TERMINAL                                                   00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MSCREENI  PIC X(5).                                       00001000
      ***************************************************************** 00001010
      * SDF: EGV55   EGV55                                              00001020
      ***************************************************************** 00001030
       01   EGV55O REDEFINES EGV55I.                                    00001040
           02 FILLER    PIC X(12).                                      00001050
      * DATE DU JOUR                                                    00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MDATJOUA  PIC X.                                          00001080
           02 MDATJOUC  PIC X.                                          00001090
           02 MDATJOUP  PIC X.                                          00001100
           02 MDATJOUH  PIC X.                                          00001110
           02 MDATJOUV  PIC X.                                          00001120
           02 MDATJOUO  PIC X(10).                                      00001130
      * HEURE                                                           00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
      * NUMERO DE PAGE COURANTE                                         00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MNOPAGEA  PIC X.                                          00001240
           02 MNOPAGEC  PIC X.                                          00001250
           02 MNOPAGEP  PIC X.                                          00001260
           02 MNOPAGEH  PIC X.                                          00001270
           02 MNOPAGEV  PIC X.                                          00001280
           02 MNOPAGEO  PIC X(3).                                       00001290
      * NOMBRE TOTAL DE PAGES                                           00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MNBPAGEA  PIC X.                                          00001320
           02 MNBPAGEC  PIC X.                                          00001330
           02 MNBPAGEP  PIC X.                                          00001340
           02 MNBPAGEH  PIC X.                                          00001350
           02 MNBPAGEV  PIC X.                                          00001360
           02 MNBPAGEO  PIC X(3).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MTYPEA    PIC X.                                          00001390
           02 MTYPEC    PIC X.                                          00001400
           02 MTYPEP    PIC X.                                          00001410
           02 MTYPEH    PIC X.                                          00001420
           02 MTYPEV    PIC X.                                          00001430
           02 MTYPEO    PIC X(35).                                      00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MCLNOMA   PIC X.                                          00001460
           02 MCLNOMC   PIC X.                                          00001470
           02 MCLNOMP   PIC X.                                          00001480
           02 MCLNOMH   PIC X.                                          00001490
           02 MCLNOMV   PIC X.                                          00001500
           02 MCLNOMO   PIC X(20).                                      00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MCNTELA   PIC X.                                          00001530
           02 MCNTELC   PIC X.                                          00001540
           02 MCNTELP   PIC X.                                          00001550
           02 MCNTELH   PIC X.                                          00001560
           02 MCNTELV   PIC X.                                          00001570
           02 MCNTELO   PIC X(10).                                      00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MCCPOSTALA     PIC X.                                     00001600
           02 MCCPOSTALC     PIC X.                                     00001610
           02 MCCPOSTALP     PIC X.                                     00001620
           02 MCCPOSTALH     PIC X.                                     00001630
           02 MCCPOSTALV     PIC X.                                     00001640
           02 MCCPOSTALO     PIC X(5).                                  00001650
           02 MLIGNEO OCCURS   10 TIMES .                               00001660
      * ZONE DE SELECTION                                               00001670
               04 FILLER     PIC X(2).                                  00001680
               04 MSELECTA   PIC X.                                     00001690
               04 MSELECTC   PIC X.                                     00001700
               04 MSELECTP   PIC X.                                     00001710
               04 MSELECTH   PIC X.                                     00001720
               04 MSELECTV   PIC X.                                     00001730
               04 MSELECTO   PIC X.                                     00001740
               04 FILLER     PIC X(2).                                  00001750
               04 MNMAGA     PIC X.                                     00001760
               04 MNMAGC     PIC X.                                     00001770
               04 MNMAGP     PIC X.                                     00001780
               04 MNMAGH     PIC X.                                     00001790
               04 MNMAGV     PIC X.                                     00001800
               04 MNMAGO     PIC X(3).                                  00001810
               04 FILLER     PIC X(2).                                  00001820
               04 MNVENTEA   PIC X.                                     00001830
               04 MNVENTEC   PIC X.                                     00001840
               04 MNVENTEP   PIC X.                                     00001850
               04 MNVENTEH   PIC X.                                     00001860
               04 MNVENTEV   PIC X.                                     00001870
               04 MNVENTEO   PIC X(7).                                  00001880
               04 FILLER     PIC X(2).                                  00001890
               04 MLNOMA     PIC X.                                     00001900
               04 MLNOMC     PIC X.                                     00001910
               04 MLNOMP     PIC X.                                     00001920
               04 MLNOMH     PIC X.                                     00001930
               04 MLNOMV     PIC X.                                     00001940
               04 MLNOMO     PIC X(20).                                 00001950
               04 FILLER     PIC X(2).                                  00001960
               04 MLPRENOMA  PIC X.                                     00001970
               04 MLPRENOMC  PIC X.                                     00001980
               04 MLPRENOMP  PIC X.                                     00001990
               04 MLPRENOMH  PIC X.                                     00002000
               04 MLPRENOMV  PIC X.                                     00002010
               04 MLPRENOMO  PIC X(8).                                  00002020
               04 FILLER     PIC X(2).                                  00002030
               04 MNTELA     PIC X.                                     00002040
               04 MNTELC     PIC X.                                     00002050
               04 MNTELP     PIC X.                                     00002060
               04 MNTELH     PIC X.                                     00002070
               04 MNTELV     PIC X.                                     00002080
               04 MNTELO     PIC X(10).                                 00002090
               04 FILLER     PIC X(2).                                  00002100
               04 MCPOSTALA  PIC X.                                     00002110
               04 MCPOSTALC  PIC X.                                     00002120
               04 MCPOSTALP  PIC X.                                     00002130
               04 MCPOSTALH  PIC X.                                     00002140
               04 MCPOSTALV  PIC X.                                     00002150
               04 MCPOSTALO  PIC X(5).                                  00002160
               04 FILLER     PIC X(2).                                  00002170
               04 MLCOMMUNEA      PIC X.                                00002180
               04 MLCOMMUNEC PIC X.                                     00002190
               04 MLCOMMUNEP PIC X.                                     00002200
               04 MLCOMMUNEH PIC X.                                     00002210
               04 MLCOMMUNEV PIC X.                                     00002220
               04 MLCOMMUNEO      PIC X(17).                            00002230
      * MESSAGE ERREUR                                                  00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MLIBERRA  PIC X.                                          00002260
           02 MLIBERRC  PIC X.                                          00002270
           02 MLIBERRP  PIC X.                                          00002280
           02 MLIBERRH  PIC X.                                          00002290
           02 MLIBERRV  PIC X.                                          00002300
           02 MLIBERRO  PIC X(78).                                      00002310
      * CODE TRANSACTION                                                00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MCODTRAA  PIC X.                                          00002340
           02 MCODTRAC  PIC X.                                          00002350
           02 MCODTRAP  PIC X.                                          00002360
           02 MCODTRAH  PIC X.                                          00002370
           02 MCODTRAV  PIC X.                                          00002380
           02 MCODTRAO  PIC X(4).                                       00002390
      * CICS DE TRAVAIL                                                 00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MCICSA    PIC X.                                          00002420
           02 MCICSC    PIC X.                                          00002430
           02 MCICSP    PIC X.                                          00002440
           02 MCICSH    PIC X.                                          00002450
           02 MCICSV    PIC X.                                          00002460
           02 MCICSO    PIC X(5).                                       00002470
      * NETNAME                                                         00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MNETNAMA  PIC X.                                          00002500
           02 MNETNAMC  PIC X.                                          00002510
           02 MNETNAMP  PIC X.                                          00002520
           02 MNETNAMH  PIC X.                                          00002530
           02 MNETNAMV  PIC X.                                          00002540
           02 MNETNAMO  PIC X(8).                                       00002550
      * CODE TERMINAL                                                   00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MSCREENA  PIC X.                                          00002580
           02 MSCREENC  PIC X.                                          00002590
           02 MSCREENP  PIC X.                                          00002600
           02 MSCREENH  PIC X.                                          00002610
           02 MSCREENV  PIC X.                                          00002620
           02 MSCREENO  PIC X(5).                                       00002630
                                                                                
