      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SVFAM SVFAM TRANSCO FAMILLE SIEBEL     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSVFAM.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSVFAM.                                                             
      *}                                                                        
           05  SVFAM-CTABLEG2    PIC X(15).                                     
           05  SVFAM-CTABLEG2-REDEF REDEFINES SVFAM-CTABLEG2.                   
               10  SVFAM-APPLICAT        PIC X(08).                             
               10  SVFAM-CFAM            PIC X(05).                             
               10  SVFAM-NSEQ            PIC X(02).                             
           05  SVFAM-WTABLEG     PIC X(80).                                     
           05  SVFAM-WTABLEG-REDEF  REDEFINES SVFAM-WTABLEG.                    
               10  SVFAM-CMARKET1        PIC X(10).                             
               10  SVFAM-CMARKET2        PIC X(10).                             
               10  SVFAM-CFAMCIB         PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSVFAM-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSVFAM-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SVFAM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SVFAM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SVFAM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SVFAM-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
