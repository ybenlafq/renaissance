      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *    COPY  COMMLT00.                                                      
      *                                                                 00010000
      ******************************************************************00020000
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *00030000
      ******************************************************************00040000
      *                                                                *00000050
      *      COMMAREA DE LA TRANSACTION LT00 LOGITRANS                 *00000060
      *                                                                *00050000
      * Z-COMMAREA, D' UNE LONGUEUR DE 4096 C, COMPRENANT :            *00120000
      * 1 - LES ZONES RESERVEES A AIDA                                 *00130000
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                  *00140000
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                *00150000
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                     *00160000
      * 5 - LES ZONES RESERVEES APPLICATIVES                           *00170000
      *                                                                *00050000
      * COM-LT00-LONG-COMMAREA A UNE VALUE DE +4096                    *00110000
      *                                                                *00050000
      ******************************************************************00020000
      *                                                                 00230000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-LT00-LONG-COMMAREA  PIC S9(4) COMP   VALUE +4096.        00240000
      *--                                                                       
       01  COM-LT00-LONG-COMMAREA  PIC S9(4) COMP-5   VALUE +4096.              
      *}                                                                        
      ************************************** NOMBRE DE RECORD COMMAREA  00000098
       01  COMM-LG05-NB-RECORD     PIC S999  COMP-3 VALUE +016.         00240000
       01  COMM-LG10-NB-RECORD     PIC S999  COMP-3 VALUE +093.         00240000
      ************************************** LONGUEUR D' UN RECORD      00000098
       01  COMM-LG05-LG-RECORD     PIC S999  COMP-3 VALUE +197.         00240000
       01  COMM-LG10-LG-RECORD     PIC S999  COMP-3 VALUE +034.         00240000
                                                                        00250000
       01  Z-COMMAREA.                                                  00260000
      *                                                                 00270000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00280000
           02 FILLER-COM-AIDA      PIC X(100).                          00290000
      *                                                                 00300000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00310000
           02 COMM-CICS-APPLID     PIC X(8).                            00320000
           02 COMM-CICS-NETNAM     PIC X(8).                            00330000
           02 COMM-CICS-TRANSA     PIC X(4).                            00340000
      *                                                                 00350000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00360000
           02 COMM-DATE-SIECLE     PIC XX.                              00370000
           02 COMM-DATE-ANNEE      PIC XX.                              00380000
           02 COMM-DATE-MOIS       PIC XX.                              00390000
           02 COMM-DATE-JOUR       PIC 99.                              00400000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00410000
           02 COMM-DATE-QNTA       PIC 999.                             00420000
           02 COMM-DATE-QNT0       PIC 99999.                           00430000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00440000
           02 COMM-DATE-BISX       PIC 9.                               00450000
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00460000
           02 COMM-DATE-JSM        PIC 9.                               00470000
      *   LIBELLES DU JOUR COURT - LONG                                 00480000
           02 COMM-DATE-JSM-LC     PIC XXX.                             00490000
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00500000
      *   LIBELLES DU MOIS COURT - LONG                                 00510000
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00520000
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00530000
      *   DIFFERENTES FORMES DE DATE                                    00540000
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00550000
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00560000
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00570000
           02 COMM-DATE-JJMMAA     PIC X(6).                            00580000
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00590000
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00600000
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00610000
           02 COMM-DATE-WEEK.                                           00620000
              05 COMM-DATE-SEMSS   PIC 99.                              00630000
              05 COMM-DATE-SEMAA   PIC 99.                              00640000
              05 COMM-DATE-SEMNU   PIC 99.                              00650000
      *                                                                 00660000
           02 COMM-DATE-FILLER     PIC X(08).                           00670000
      *                                                                 00680000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00700000
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                       00710000
      *                                                                 00720000
      * ZONES RESERVEES APPLICATIVES ----------------------------- 528  00720103
      * ZONES COMMUNES AUX PROGRAMMES TLT01 TLT02 TLT03        -------  00720103
      *                                                                 00720000
           02 COMM-LT00-APPLI.                                          00721003
      *                                                                 00720000
              05 COMM-LT00-NLETTRE            PIC X(06).                00740103
              05 COMM-LT00-LIBLET             PIC X(30).                00740103
              05 COMM-LT00-LIGDEF             PIC 9(02).                00740103
              05 COMM-LT00-TRAIT              PIC X(03).                00740103
                 88  COMM-LT00-CRE        VALUE    'CRE'.                       
                 88  COMM-LT00-MAJ        VALUE    'MAJ'.                       
                 88  COMM-LT00-INT        VALUE    'INT'.                       
                 88  COMM-LT00-SUP        VALUE    'SUP'.                       
              05 COMM-LT00-CODRET             PIC X.                    00740103
                 88  COMM-LT00-PAS-MESS   VALUE    ' '.                         
                 88  COMM-LT00-MESS-RET   VALUE    '1'.                         
              05 COMM-LT00-MESSAGE            PIC X(50).                00740103
              05 COMM-LT00-PAGEENC            PIC 9(02).                00740103
              05 COMM-LT00-PAGEMAX            PIC 9(02).                00740103
              05 COMM-LT00-RANGTS             PIC 9(02).                00740103
              05 COMM-LT00-NBTS               PIC 9(02).                00740103
              05 COMM-LT00-LIGMIN             PIC 9(02).                00740103
              05 COMM-LT00-LIGMAX             PIC 9(02).                00740103
              05 COMM-LT00-NCODIMP            PIC X(04).                00740103
              05 COMM-LT00-MODELE             PIC X(06).                00740103
              05 COMM-LT00-LIBMOD             PIC X(20).                00740103
              05 COMM-LT00-NLETTRCOP          PIC X(06).                00740103
              05 COMM-LT00-LETTRTEXT          PIC X(06).                00740103
              05 COMM-LT00-LIGREEL            PIC 9(2).                 00740103
              05 FILLER                       PIC X(285).               00740103
              05 COMM-LT00-SSPRGS             PIC X(2966).              00740103
                                                                                
