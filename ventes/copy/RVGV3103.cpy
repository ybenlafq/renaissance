      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGV3103                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV3103                         
      **********************************************************                
       01  RVGV3103.                                                            
           02  GV31-CVENDEUR                                                    
               PIC X(0006).                                                     
           02  GV31-LVENDEUR                                                    
               PIC X(0015).                                                     
           02  GV31-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GV31-NMAG                                                        
               PIC X(0003).                                                     
           02  GV31-NMATSIGA                                                    
               PIC X(0007).                                                     
           02  GV31-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GV31-WPRIME                                                      
               PIC X(0001).                                                     
           02  GV31-DCREATION                                                   
               PIC X(0008).                                                     
           02  GV31-CROLE                                                       
               PIC X(0005).                                                     
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGV3103                                  
      **********************************************************                
       01  RVGV3103-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV31-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV31-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV31-LVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV31-LVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV31-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV31-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV31-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV31-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV31-NMATSIGA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV31-NMATSIGA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV31-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV31-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV31-WPRIME-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV31-WPRIME-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV31-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV31-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV31-CROLE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GV31-CROLE-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
