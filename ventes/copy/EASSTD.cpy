      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 16:58 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREPOT        *
      * FICHIER.....: EMISSION ARTICLE SITE STANDARD                  *
      * NOM FICHIER.: EASSTD                                          *
      *---------------------------------------------------------------*
      * CR   .......: 29/02/2012                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 200                                             *
      *****************************************************************
      *
       01  EASSTD.
      * TYPE ENREGISTREMENT : RECEPTION ARTICLE SITE STANDARD
           05      EASSTD-TYP-ENREG       PIC  X(0006).
      * CODE ETAT
           05      EASSTD-CETAT           PIC  X(0001).
      * CODE SOCIETE
           05      EASSTD-CSOCIETE        PIC  X(0005).
      * CODE ARTICLE
           05      EASSTD-CARTICLE        PIC  X(0018).
      * CODE UNITE LOGISTIQUE
           05      EASSTD-CODE-UL         PIC  X(0005).
      * NUMERO DE SITE
           05      EASSTD-NSITE           PIC  9(0003).
      * CODE EMBALLAGE
           05      EASSTD-CEMBALLAGE      PIC  X(0010).
      * DISPONIBLE
           05      EASSTD-DISPONIBLE      PIC  9(0001).
      * N� PROFIL RANGEMENT
           05      EASSTD-NPROFIL-RANG    PIC  9(0003).
      * TYPE DE PREPARATION
           05      EASSTD-TYPE-PREP       PIC  9(0003).
      * MODE PREPARATION
           05      EASSTD-MODE-PREP       PIC  9(0003).
      * ADRESSE
           05      EASSTD-ADRESSE         PIC  X(0020).
      * TYPE REAPPRO
           05      EASSTD-TYPE-REAPPRO    PIC  9(0003).
      * SEUIL MINI
           05      EASSTD-SEUIL-MINI      PIC  9(0009).
      * SEUIL MAXI
           05      EASSTD-SEUIL-MAXI      PIC  9(0009).
      * N� PROFIL REAPPRO
           05      EASSTD-NPROFIL-REAPPRO PIC  9(0003).
      * SEUIL PREPARATION PALETTE
           05      EASSTD-SEUIL-PREPA-PAL PIC  9(0009).
      * N� PROFIL PREPARATION GLOBALE
           05      EASSTD-NPROFIL-PREP-GLOB PIC 9(003).
      * CARACTERISTIQUE 1
           05      EASSTD-CARACT1         PIC  X(0020).
      * CARACTERISTIQUE 2
           05      EASSTD-CARACT2         PIC  X(0020).
      * CARACTERISTIQUE 3
           05      EASSTD-CARACT3         PIC  X(0020).
      * UL DE REAPPRO 1
           05      EASSTD-UL-REAPPRO1     PIC  X(0005).
      * UL DE REAPPRO 2
           05      EASSTD-UL-REAPPRO2     PIC  X(0005).
      * UL DE REAPPRO 3
           05      EASSTD-UL-REAPPRO3     PIC  X(0005).
      * UL DE REAPPRO 4
           05      EASSTD-UL-REAPPRO4     PIC  X(0005).
      * UL DE REAPPRO 5
           05      EASSTD-UL-REAPPRO5     PIC  X(0005).
      * FILLER
           05      EASSTD-FILLER          PIC  X(0001).
      
