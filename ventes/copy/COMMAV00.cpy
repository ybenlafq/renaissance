      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : AVOIRS COMMERCIAUX                               *        
      *  TRANSACTION: AV00                                             *        
      *  TITRE      : COMMAREA DE LA TRANSACTION AV00                  *        
      *  LONGUEUR   : 4096                                             *        
      *                                                                *        
      * Z-COMMAREA, D' UNE LONGUEUR DE 4096 C, COMPRENANT :            *        
      * 1 - LES ZONES RESERVEES A AIDA                           100 C *        
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS             20 C *        
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT          100 C *        
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP               152 C *        
      * 5 - LES ZONES RESERVEES APPLICATIVES                    3724 C *        
      *                                                                *        
      * -------------------------------------------------------------- *        
      * MAINTENANCES                                                   *        
      * -------------------------------------------------------------- *        
      * PROJET : BIENS ET SERVICES                 DATE : 23/10/02     *        
      *        ! LA ZONE COMM-AV01-NTRANS PASSE DE 4 � 8 CARACT�RES    *        
      *        !                                                       *        
      ******************************************************************        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-AV00-LONG-COMMAREA       PIC S9(4) COMP   VALUE +4096.           
      *--                                                                       
       01  COM-AV00-LONG-COMMAREA       PIC S9(4) COMP-5   VALUE +4096.         
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      **** ZONES RESERVEES A AIDA ---------------------------------- 100        
           02 FILLER-COM-AIDA           PIC X(100).                             
      *                                                                         
      **** ZONES RESERVEES EN PROVENANCE DE CICS ------------------- 020        
           02 COMM-CICS-APPLID          PIC X(08).                              
           02 COMM-CICS-NETNAM          PIC X(08).                              
           02 COMM-CICS-TRANSA          PIC X(04).                              
           02 COMM-CICS-PROG            PIC X(05).                              
           02 COMM-CICS-MENU            PIC X(05).                              
      *                                                                         
      **** ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ----- 100        
           02 COMM-DATE-SIECLE          PIC X(02).                              
           02 COMM-DATE-ANNEE           PIC X(02).                              
           02 COMM-DATE-MOIS            PIC X(02).                              
           02 COMM-DATE-JOUR            PIC 9(02).                              
           02 COMM-DATE-QNTA            PIC 9(03).                              
           02 COMM-DATE-QNT0            PIC 9(05).                              
           02 COMM-DATE-BISX            PIC 9(01).                              
           02 COMM-DATE-JSM             PIC 9(01).                              
           02 COMM-DATE-JSM-LC          PIC X(03).                              
           02 COMM-DATE-JSM-LL          PIC X(08).                              
           02 COMM-DATE-MOIS-LC         PIC X(03).                              
           02 COMM-DATE-MOIS-LL         PIC X(09).                              
           02 COMM-DATE-SSAAMMJJ        PIC X(08).                              
           02 COMM-DATE-AAMMJJ          PIC X(06).                              
           02 COMM-DATE-JJMMSSAA        PIC X(08).                              
           02 COMM-DATE-JJMMAA          PIC X(06).                              
           02 COMM-DATE-JJ-MM-AA        PIC X(08).                              
           02 COMM-DATE-JJ-MM-SSAA      PIC X(10).                              
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS        PIC 9(02).                              
              05 COMM-DATE-SEMAA        PIC 9(02).                              
              05 COMM-DATE-SEMNU        PIC 9(02).                              
           02 COMM-DATE-FILLER          PIC X(07).                              
      *                                                                         
      **** ZONES RESERVEES TRAITEMENT DU SWAP ---------------------- 152        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS            PIC S9(4) COMP VALUE -1.                
      *--                                                                       
           02 COMM-SWAP-CURS            PIC S9(4) COMP-5 VALUE -1.              
      *}                                                                        
           02 COMM-SWAP-ATTR            OCCURS 150 PIC X(1).                    
      *                                                                         
      **** ZONES APPLICATIVES AV00-AV10 GENERALES ----------------- 1724        
           02 COMM-AV00-APPLI.                                                  
      *-      LIEU EMETTEUR (DE SAISIE)                                         
              05 COMM-AV00-MAGASIN.                                             
                 10 COMM-AV00-NSOCIETE        PIC X(03).                        
                 10 COMM-AV00-NLIEU           PIC X(03).                        
                 10 COMM-AV00-LLIEU           PIC X(20).                        
EMU              10 COMM-AV00-CTYPSOC         PIC X(03).                        
                 10 COMM-AV00-CTYPLIEU        PIC X(03).                        
      *-      LIEU EMETTEUR (DEMANDE (CF TAV14))                                
              05 COMM-AV00-MAGASIN.                                             
                 10 COMM-AV00-NSOCIETE2       PIC X(03).                        
                 10 COMM-AV00-NLIEU2          PIC X(03).                        
                 10 COMM-AV00-LLIEU2          PIC X(20).                        
      *-      DONNEES COMMUNES A TOUS LES PROGRAMMES                            
              05 COMM-AV00-DONNEES.                                             
                 10 COMM-AV00-NAVOIR           PIC X(007).                      
                 10 COMM-AV00-CTYPAVOIR        PIC X(005).                      
                 10 COMM-AV00-LTYPAVOIR        PIC X(20).                       
                 10 COMM-AV00-DEMIS.                                            
                    15 COMM-AV00-DEMIS-SSAA    PIC X(004).                      
                    15 COMM-AV00-DEMIS-MM      PIC X(002).                      
                    15 COMM-AV00-DEMIS-JJ      PIC X(002).                      
                 10 COMM-AV00-MESSAGE.                                          
                    15 COMM-AV00-CODRET          PIC X(01).                     
                       88 COMM-AV00-CODRET-OK               VALUE ' '.          
                       88 COMM-AV00-CODRET-ERREUR           VALUE '1'.          
                 10 COMM-AV00-LIBERR          PIC X(58).                        
      *---       NOMBRE DE PAGES POUR LES TS                                    
              05 COMM-AV00-NOPAGE          PIC 9(03).                           
      *---       NOMBRE DE PROPOSITIONS TROUV�ES                                
              05 COMM-AV00-NBPROPO         PIC 9(03).                           
      *---       TABLEAU DES INDICES DES PROPOSITIONS SELECTIONNEES             
              05 COMM-AV00-TAB-SELECTION.                                       
                 10 COMM-AV00-SELECTION OCCURS 300 PIC S9(03) COMP-3.           
      *---       NOMBRE DE PROPOSITIONS SELECTIONNEES                           
                 10 COMM-AV00-MAX-SELECT           PIC S9(03) COMP-3.           
      *---       INDICE DE LA DERNIERRRE SELECTION AFFICHEE                     
                 10 COMM-AV00-DERN-SELECT          PIC S9(03) COMP-3.           
                    88 COMM-AV00-PAS-EN-SELECTION  VALUE 0.                     
                    88 COMM-AV00-EN-SELECTION      VALUE 1 THRU 999.            
      *---       N� DE PROPOSITIONS AFFICHEES                                   
              05 COMM-AV00-NOPROPO         PIC S9(03) COMP-3.                   
      *---       INDICE DE LA PROPOSITION AFFICHEE DANS LA PAGE TS              
              05 COMM-AV00-NOINDICE        PIC S9(03) COMP-3.                   
      *---       NOMBRE DE PROPOSITIONS DEJA AFFICHEES                          
              05 COMM-AV00-NBPROPO-AFFICHES   PIC S9(03) COMP-3.                
      *--- ZONES POUR DEBRANCHEMENT SUR SELECTION CODES POSTAUX                 
              05  COMM-AV00-CINSEE         PIC X(05) OCCURS 2.                  
              05  COMM-AV00-LTITRE         PIC X(20).                           
      *       05  COMM-AV00-LBUREAU        PIC X(20).                           
      *       05  COMM-AV00-LCOMUNE        PIC X(25).                           
              05  COMM-AV00-LBUREAU        PIC X(26).                           
              05  COMM-AV00-LCOMUNE        PIC X(32).                           
              05  COMM-AV00-C1POSTAL       PIC X(05).                           
              05  COMM-AV00-NPAGE          PIC 999.                             
              05  COMM-AV00-NBRPAG         PIC 999.                             
      *       05 COMM-AV00-FILLER          PIC X(875).                          
DEP           05 COMM-AV00-NSECTION        PIC X(06).                           
DEP           05 COMM-AV00-LIBELLE         PIC X(20).                           
DEP           05 COMM-AV00-CACID           PIC X(07).                           
BS2309        05 COMM-AV00-CODLANG         PIC X(02).                           
BS2309        05 COMM-AV00-CODPIC          PIC X(02).                           
      *       05 COMM-AV00-FILLER          PIC X(825).                          
              05 COMM-AV00-NSOC-ORIG       PIC X(3).                            
              05 COMM-AV00-NLIEU-ORIG      PIC X(3).                            
LA2811        05 COMM-AV00-COMPADR         PIC X(32).                           
LA2811        05 COMM-AV00-BATIMENT        PIC X(3).                            
LA2811        05 COMM-AV00-ESCALIER        PIC X(3).                            
LA2811        05 COMM-AV00-ETAGE           PIC X(3).                            
LA2811        05 COMM-AV00-PORTE           PIC X(3).                            
LA2811*       05 COMM-AV00-FILLER          PIC X(819).                          
LA2811        05 COMM-AV00-FILLER          PIC X(775).                          
      *                                                                         
      *--- ZONES APPLICATIVES AV00 -------------------------------- 2000        
           02 COMM-AV00-REDEFINES          PIC X(2000).                         
      *                                                                         
      *--- ZONES APPLICATIVES AV01 ---------------------------------- 13        
           02 COMM-AV01-APPLI REDEFINES COMM-AV00-REDEFINES.                    
              05  COMM-AV01-FLAG-VTE-EX PIC X.                                  
                  88 COMM-AV01-VTE-EX-PAS VALUE 'N'.                            
                  88 COMM-AV01-VTE-EX     VALUE 'O'.                            
              05  COMM-AV01-FLAG-SAISIE-CODIC PIC X.                            
                  88 COMM-AV01-YAPA-SAISIE-CODIC VALUE 'N'.                     
                  88 COMM-AV01-YA-SAISIE-CODIC   VALUE 'O'.                     
              05  COMM-AV01-FLAG-CONTRE-AVOIR PIC X.                            
                  88 COMM-AV01-YAPA-CONTRE-AVOIR VALUE 'N'.                     
                  88 COMM-AV01-YA-CONTRE-AVOIR   VALUE 'O'.                     
              05  COMM-AV01-FLAG-CREER-AVOIR  PIC X.                            
                  88 COMM-AV01-CREER-AVOIR VALUE 'O'.                           
                  88 COMM-AV01-PAS-CREER-AVOIR  VALUE 'N'.                      
              05  COMM-AV01-TITRECODIC     PIC X(35).                           
              05  COMM-AV01-CODIC       PIC X(07).                              
              05  COMM-AV01-MLMONTANT   PIC X(14).                              
              05  COMM-AV01-MLAVOIR     PIC X(25).                              
              05  COMM-AV01-MLINFO      PIC X(15).                              
              05  COMM-AV01-MLMANU      PIC X(06).                              
              05  COMM-AV01-MLNOUV      PIC X(07).                              
              05  COMM-AV01-MINFO       PIC X(07).                              
              05  COMM-AV01-MMANU       PIC X(06).                              
              05  COMM-AV01-MNOUV       PIC X(09).                              
              05  COMM-AV01-CDEVREF     PIC X(03).                              
              05  COMM-AV01-CDEV2       PIC X(03).                              
              05  COMM-AV01-CTYPUTIL       PIC X(05).                           
              05  COMM-AV01-CMOTIF         PIC X(05).                           
AJ            05  COMM-AV01-LMOTIF         PIC X(20).                           
              05  COMM-AV01-PAVOIR         PIC S9(5)V9(2).                      
              05  COMM-AV01-PAVOIR-EDIT    PIC X(10).                           
              05  COMM-AV01-NLIEUAFF       PIC X(03).                           
              05  COMM-AV01-NVENTE         PIC X(07).                           
              05  COMM-AV01-NCRI           PIC X(11).                           
              05  COMM-AV01-CTITRENOM      PIC X(05).                           
              05  COMM-AV01-LNOM           PIC X(25).                           
              05  COMM-AV01-LREFNOM        PIC X(25).                           
              05  COMM-AV01-LPRENOM        PIC X(25).                           
              05  COMM-AV01-CVOIE          PIC X(05).                           
              05  COMM-AV01-CTVOIE         PIC X(04).                           
              05  COMM-AV01-LNOMVOIE       PIC X(25).                           
      *       05  COMM-AV01-LBUREAU        PIC X(25).                           
              05  COMM-AV01-LBUREAU        PIC X(26).                           
              05  COMM-AV01-DCAISSE        PIC X(10).                           
              05  COMM-AV01-NCAISSE        PIC X(03).                           
BS2309        05  COMM-AV01-NTRANS         PIC X(08).                           
              05  COMM-AV01-CMODPAIMT      PIC X(05).                           
BB    *       05  COMM-AV01-LMODPAIMT      PIC X(25).                           
              05  COMM-AV01-NSOCVENTE      PIC X(03).                           
              05  COMM-AV01-NLIEUVENTE     PIC X(03).                           
BB    *       05  COMM-AV01-LLIEUVENTE     PIC X(25).                           
      *                                                                         
      *--- ZONES APPLICATIVES AV03 ---------------------------------- 07        
      *    02 COMM-AV03-APPLI REDEFINES COMM-AV00-REDEFINES.                    
      *---    N� D'AVOIR RECHERCHE                                              
      *       05  COMM-AV03-LTYPAVOIR      PIC X(20).                           
      *--- ZONES APPLICATIVES AV05 ---------------------------------- 07        
           02 COMM-AV05-APPLI REDEFINES COMM-AV00-REDEFINES.                    
              05  COMM-AV05-FLAG-ANNULER  PIC X.                                
                  88 COMM-AV05-ANNULER-AVOIR-OK VALUE 'O'.                      
                  88 COMM-AV05-ANNULER-AVOIR-KO  VALUE 'N'.                     
      *                                                                         
      *--- ZONES APPLICATIVES AV11 ---------------------------------- 07        
           02 COMM-AV11-APPLI REDEFINES COMM-AV00-REDEFINES.                    
              05  COMM-AV11-FLAG-ANNULER  PIC X.                                
                  88 COMM-AV11-ANNULER-AVOIR-OK VALUE 'O'.                      
                  88 COMM-AV11-ANNULER-AVOIR-KO  VALUE 'N'.                     
      *                                                                         
      *--- ZONES APPLICATIVES AV13 ---------------------------------- 13        
           02 COMM-AV13-APPLI REDEFINES COMM-AV00-REDEFINES.                    
              05  COMM-AV13-FLAG-CREER-AVOIR  PIC X.                            
                  88 COMM-AV13-CREER-AVOIR VALUE 'O'.                           
                  88 COMM-AV13-PAS-CREER-AVOIR  VALUE 'N'.                      
              05  COMM-AV13-CDEVREF     PIC X(03).                              
              05  COMM-AV13-CDEV2       PIC X(03).                              
              05  COMM-AV13-TYPAVOIR       PIC X(5).                            
              05  COMM-AV13-NAVOIR         PIC X(7).                            
              05  COMM-AV13-MODULO         PIC X(1).                            
              05  COMM-AV13-CTYPUTIL       PIC X(05).                           
              05  COMM-AV13-CMOTIF         PIC X(05).                           
              05  COMM-AV13-PAVOIR         PIC S9(5)V9(2).                      
              05  COMM-AV13-PAVOIR-EDIT    PIC X(10).                           
              05  COMM-AV13-NLIEUAFF       PIC X(03).                           
              05  COMM-AV13-C1POSTAL       PIC X(05).                           
              05  COMM-AV13-CTITRENOM      PIC X(05).                           
              05  COMM-AV13-LNOM           PIC X(25).                           
              05  COMM-AV13-LREFNOM        PIC X(25).                           
              05  COMM-AV13-LPRENOM        PIC X(25).                           
              05  COMM-AV13-CVOIE          PIC X(05).                           
              05  COMM-AV13-CTVOIE         PIC X(04).                           
              05  COMM-AV13-LNOMVOIE       PIC X(25).                           
      *       05  COMM-AV13-LCOMUNE        PIC X(25).                           
      *       05  COMM-AV13-LBUREAU        PIC X(25).                           
              05  COMM-AV13-LCOMUNE        PIC X(32).                           
              05  COMM-AV13-LBUREAU        PIC X(26).                           
              05  COMM-AV13-CINSEE         PIC X(05) OCCURS 2.                  
              05  COMM-AV13-LTITRE         PIC X(20).                           
              05  COMM-AV13-NPAGE          PIC 999.                             
              05  COMM-AV13-NBRPAG         PIC 999.                             
      *                                                                         
      *--- ZONES APPLICATIVES AV14 -------------------------------------        
           02 COMM-AV14-APPLI REDEFINES COMM-AV00-REDEFINES.                    
              05 COMM-AV14-FILLER          PIC X(07).                           
              05 COMM-AV14-PAGINATION.                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          10 COMM-AV14-MAX-PAGE        PIC 9(04) COMP.                   
      *--                                                                       
                 10 COMM-AV14-MAX-PAGE        PIC 9(04) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          10 COMM-AV14-NO-PAGE         PIC 9(04) COMP.                   
      *--                                                                       
                 10 COMM-AV14-NO-PAGE         PIC 9(04) COMP-5.                 
      *}                                                                        
              05 COMM-AV14-DONNEES-SELECTION.                                   
                 10 COMM-AV14-TAB-ECRAN-SELECTION.                              
                    15 COMM-AV14-SELECT       OCCURS 13 PIC X.                  
                 10 COMM-AV14-WCRITERE           PIC X.                         
                    88 COMM-AV14-SELECT-NAVOIR         VALUE '1'.               
                    88 COMM-AV14-SELECT-NSOC           VALUE '2'.               
                    88 COMM-AV14-SELECT-NLIEU          VALUE '3'.               
                    88 COMM-AV14-SELECT-LNOM           VALUE '4'.               
                    88 COMM-AV14-SELECT-LNOM-NLIEU     VALUE '5'.               
                    88 COMM-AV14-SELECT-DATE           VALUE '6'.               
                    88 COMM-AV14-SELECT-DATE-NLIEU     VALUE '7'.               
CG               10 COMM-AV14-NAVOIR          PIC X(08).                        
                 10 COMM-AV14-NSOCIETE        PIC X(03).                        
                 10 COMM-AV14-NLIEU           PIC X(03).                        
                 10 COMM-AV14-LLIEU           PIC X(20).                        
                 10 COMM-AV14-DEMIS           PIC X(08).                        
                 10 COMM-AV14-CMOTIF          PIC X(05).                        
                 10 COMM-AV14-LNOM            PIC X(20).                        
                 10 COMM-AV14-PMONTANT        PIC X(10).                        
                 10 COMM-AV14-WANNUL          PIC X(01).                        
                 10 COMM-AV14-WUTIL           PIC X(01).                        
                 10 COMM-AV14-CCRITERE        PIC X(42).                        
                                                                                
