      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      * taille des tableaux de la COMMECCC                                      
      ******************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-CC-R-MAX              PIC S9(4) BINARY VALUE 10.                   
      *--                                                                       
       01  W-CC-R-MAX              PIC S9(4) COMP-5 VALUE 10.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-CC-V-MAX              PIC S9(4) BINARY VALUE 50.                   
      *                                                                         
      *--                                                                       
       01  W-CC-V-MAX              PIC S9(4) COMP-5 VALUE 50.                   
                                                                                
      *}                                                                        
