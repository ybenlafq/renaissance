      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000010
      * >>>>>>>>>>>>>>>>> DESCRIPTION DE LA TSGV55 <<<<<<<<<<<<<<<<<< * 00000020
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000030
      *                                                               * 00000040
       01  TSGV55-IDENTIFICATEUR.                                       00000050
           05  TSGV55-TRANSID               PIC X(04) VALUE 'GV55'.     00000060
           05  TSGV55-NUMERO-DE-TERMINAL    PIC X(04) VALUE SPACES.     00000070
      *                                                               * 00000080
       01  TSGV55-ITEM.                                                 00000090
           05  TSGV55-LONGUEUR              PIC S9(4) VALUE +700.       00000100
           05  TSGV55-DATAS.                                            00000110
               10  TSGV55-LIGNE             OCCURS 10.                  00000120
                   15  TSGV55-NMAG          PIC X(03).                  00000130
                   15  TSGV55-NVENTE        PIC X(07).                  00000140
                   15  TSGV55-LNOM          PIC X(20).                  00000150
                   15  TSGV55-LPRENOM       PIC X(08).                  00000160
                   15  TSGV55-NTEL          PIC X(10).                  00000170
                   15  TSGV55-CPOSTAL       PIC X(05).                  00000180
                   15  TSGV55-LCOMMUNE      PIC X(17).                  00000190
      *                                                               * 00000200
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000210
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< * 00000220
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000230
                                                                                
