      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
       01  GVPSE-IND-GCPLT                 PIC 999  VALUE 100.                  
       01  GVPSE-IND-GLOBALE               PIC 999  VALUE ZEROES.               
       01  GVPSE-NOMTS                     PIC X(08) VALUE 'TSGVPSE'.           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  GV-PSE-LONG                     PIC S9(4) COMP VALUE +5008.          
      *--                                                                       
       01  GV-PSE-LONG                     PIC S9(4) COMP-5 VALUE +5008.        
      *}                                                                        
       01  GV-PSE-DONNEES.                                                      
           05 GV-PSE-DCREATION             PIC X(08).                           
      *                                                100*16 = + 1600.         
           05 TS-PSE-GCPLT.                                                     
              10 TS-PSE-GCPLT-POSTE       OCCURS 100.                           
                 15 TS-PSE-GCPLT-CGCPL    PIC X(05).                            
                 15 TS-PSE-GCPLT-CTAUXTVA PIC X(05).                            
                 15 TS-PSE-GCPLT-QDC      PIC 9(03).                            
                 15 TS-PSE-GCPLT-QDS      PIC 9(03).                            
           05 FILLER                      PIC X(3400).                          
                                                                                
