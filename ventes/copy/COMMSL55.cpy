      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*00730000
      * COMMAREA POUR MODULE MSL54 - APPEL DEPUIS TSL55                         
      * 16 FEV 2010                                                             
      *                                                                         
      ******************************************************************        
AP0410*  DATE       : 28/04/2010                                       *        
AP0410*  AUTEUR     : DSA074                                           *        
AP0410*  OBJET      : MODIFICATION DE LA GESTION DE LA COMMAREA        *        
AP0410*                 -Ecriture en zone memoire des r�sultats        *        
AP0410*                 -Passage au programme appelant du nombre de    *        
AP0410*                  r�sultat et du nom de la zone d'�criture des  *        
AP0410*                  r�sultats (TS)                                *        
      *                                                                         
AP0410*01  COMM-SL55-LONG-COMMAREA PIC S9(5) COMP VALUE +50124.                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
AP0410*01  COMM-SL55-LONG-COMMAREA PIC S9(3) COMP VALUE   +128.                 
      *--                                                                       
       01  COMM-SL55-LONG-COMMAREA PIC S9(3) COMP-5 VALUE   +128.               
      *}                                                                        
      *----------------------------------------------------------------*00730000
       01  Z-COMMAREA-SL55.                                                     
      *                                                                 00730000
           03 COMM-SL55-CODERET          PIC 9(04).                             
      *                                                                 00730000
           03 COMM-SL55-APPELANT         PIC X(05).                             
      *                                                                 00730000
           03 COMM-SL55.                                                        
              05 COMM-SL55-CRIT-RECH     PIC 9(05).                             
      *                                                                 00730000
              05 COMM-SL55-CRITERES.                                            
                 10 COMM-SL55-OPTION      PIC X(01).                            
                 10 COMM-SL55-NSOCIETE    PIC X(03).                            
                 10 COMM-SL55-NLIEU       PIC X(03).                            
                 10 COMM-SL55-NCODIC      PIC X(07).                            
                 10 COMM-SL55-NVENTE      PIC X(07).                            
                 10 COMM-SL55-CODACT      PIC X(01).                            
                 10 COMM-SL55-DATD        PIC X(08).                            
                 10 COMM-SL55-DATF        PIC X(08).                            
                 10 COMM-SL55-SOCDEP      PIC X(03).                            
                 10 COMM-SL55-LIEUDEP     PIC X(03).                            
AP9999           10 COMM-SL55-NENVOI      PIC X(01).                            
      * => 44                                                                   
              05 COMM-SL55-RESULT.                                              
AP0410*          10  COMM-SL55-TAB-LIGNE OCCURS 665.                            
AP0410*              15  COMM-SL55-SOCIETE    PIC X(03).                        
AP0410*              15  COMM-SL55-LIEU       PIC X(03).                        
AP0410*              15  COMM-SL55-CODIC      PIC X(07).                        
AP0410*              15  COMM-SL55-VENTE      PIC X(07).                        
AP0410*              15  COMM-SL55-SEQNQ      PIC 9(05).                        
AP0410*              15  COMM-SL55-VENDUE     PIC 9(05).                        
AP0410*              15  COMM-SL55-SOCDEPLIV  PIC X(03).                        
AP0410*              15  COMM-SL55-LIEUDEPLIV PIC X(03).                        
AP0410** SEULS LES 19 PREMIERS CARACT�RES SONT UTILIS�S PAR TSL55               
AP0410*              15  COMM-SL55-COMMENT    PIC X(19).                        
AP0410*              15  COMM-SL55-EQUIPE     PIC X(07).                        
AP0410*              15  COMM-SL55-STATUT     PIC X(05).                        
AP0410*              15  COMM-SL55-DDELIV     PIC X(08).                        
      *                                                                         
                 10  COMM-SL55-NOM-TS           PIC X(08).                      
                 10  COMM-SL55-NOM-TSB          PIC X(08).                      
                 10  COMM-SL55-NBLIGNE          PIC 9(05).                      
      * => 665 * 75 = 49875                                                     
      * LONGEUR TOTALE : 4 + 1 + 44 + 49875 +3 = 49927                          
AP0410* LONGEUR TOTALE : 4 + 1 + 44 +     8 +3 =    60                          
      * FILLER ---------------------------------------------------------        
AP0410*    03 FILLER                      PIC X(197).                           
AP9999* AP0410   03 FILLER                      PIC X(068).                     
AP9999     03 FILLER                      PIC X(048).                           
                                                                                
