      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IAV006 AU 08/06/1999  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,07,BI,A,                          *        
      *                           19,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IAV006.                                                        
            05 NOMETAT-IAV006           PIC X(6) VALUE 'IAV006'.                
            05 RUPTURES-IAV006.                                                 
           10 IAV006-WTYPE              PIC X(05).                      007  005
           10 IAV006-NAVOIR             PIC X(07).                      012  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IAV006-SEQUENCE           PIC S9(04) COMP.                019  002
      *--                                                                       
           10 IAV006-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IAV006.                                                   
           10 IAV006-NSOCTRAIT          PIC X(03).                      021  003
           10 IAV006-WANNUL             PIC X(03).                      024  003
           10 IAV006-WPERIM             PIC X(03).                      027  003
           10 IAV006-WUTIL              PIC X(03).                      030  003
           10 IAV006-DEPUR              PIC 9(03)     .                 033  003
           10 IAV006-DATETRAIT          PIC X(08).                      036  008
            05 FILLER                      PIC X(469).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IAV006-LONG           PIC S9(4)   COMP  VALUE +043.           
      *                                                                         
      *--                                                                       
        01  DSECT-IAV006-LONG           PIC S9(4) COMP-5  VALUE +043.           
                                                                                
      *}                                                                        
