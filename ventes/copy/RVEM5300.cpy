      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVEM5300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEM5300                         
      *   CLE UNIQUE : NSOCIETE A NSEQPNEM LG = 87                              
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEM5300.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEM5300.                                                            
      *}                                                                        
1          02  EM53-NSOCIETE                                                    
               PIC X(0003).                                                     
4          02  EM53-NLIEU                                                       
               PIC X(0003).                                                     
7          02  EM53-DCAISSE                                                     
               PIC X(0008).                                                     
15         02  EM53-NCAISSE                                                     
               PIC X(0003).                                                     
18         02  EM53-NTRANS                                                      
               PIC X(0008).                                                     
26         02  EM53-NTYPVENTE                                                   
               PIC X(0003).                                                     
29         02  EM53-NVENTE                                                      
               PIC X(0007).                                                     
36         02  EM53-NSEQPNEM                                                    
               PIC X(0003).                                                     
39         02  EM53-NEMPORT                                                     
               PIC X(0008).                                                     
47         02  EM53-CMODPAIMT                                                   
               PIC X(0005).                                                     
52         02  EM53-PREGLTVTE                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
57         02  EM53-PREGLRENDU                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
62         02  EM53-DEVREGL                                                     
               PIC X(0001).                                                     
63         02  EM53-NREGLTNEM                                                   
               PIC X(0020).                                                     
83         02  EM53-PVENTL                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVEM5300                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEM5300-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEM5300-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM53-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM53-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM53-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM53-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM53-DCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM53-DCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM53-NCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM53-NCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM53-NTRANS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM53-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM53-NTYPVENTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM53-NTYPVENTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM53-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM53-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM53-NSEQPNEM-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM53-NSEQPNEM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM53-NEMPORT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM53-NEMPORT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM53-CMODPAIMT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM53-CMODPAIMT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM53-PREGLTVTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM53-PREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM53-PREGLRENDU-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM53-PREGLRENDU-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM53-DEVREGL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM53-DEVREGL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM53-NREGLTNEM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM53-NREGLTNEM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM53-PVENTL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM53-PVENTL-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
