      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *********************************************************                 
      *   COPY DE LA TABLE RVBA0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBA0100                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBA0100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBA0100.                                                            
      *}                                                                        
           02  BA01-NBA                                                         
               PIC X(0006).                                                     
           02  BA01-CTIERSBA                                                    
               PIC X(0006).                                                     
           02  BA01-NTIERSCV                                                    
               PIC X(0006).                                                     
           02  BA01-DEMIS                                                       
               PIC X(0008).                                                     
           02  BA01-NFACTBA                                                     
               PIC X(0010).                                                     
           02  BA01-PBA                                                         
               PIC S9(5)V9(0002) COMP-3.                                        
           02  BA01-NCODIC                                                      
               PIC X(0007).                                                     
           02  BA01-WSTAT                                                       
               PIC X(0001).                                                     
           02  BA01-WRECAP                                                      
               PIC X(0001).                                                     
           02  BA01-WANNUL                                                      
               PIC X(0001).                                                     
           02  BA01-WRECEP                                                      
               PIC X(0001).                                                     
           02  BA01-DICEMIS                                                     
               PIC X(0008).                                                     
           02  BA01-DANNUL                                                      
               PIC X(0008).                                                     
           02  BA01-DRECEP                                                      
               PIC X(0008).                                                     
           02  BA01-DICRECEP                                                    
               PIC X(0008).                                                     
           02  BA01-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  BA01-NLIEU                                                       
               PIC X(0003).                                                     
           02  BA01-NCHRONO                                                     
               PIC X(0007).                                                     
           02  BA01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVBA0100                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBA0100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBA0100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-NBA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-NBA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-CTIERSBA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-CTIERSBA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-NTIERSCV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-NTIERSCV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-DEMIS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-DEMIS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-NFACTBA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-NFACTBA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-PBA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-PBA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-WSTAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-WSTAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-WRECAP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-WRECAP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-WANNUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-WANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-WRECEP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-WRECEP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-DICEMIS-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-DICEMIS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-DANNUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-DANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-DRECEP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-DRECEP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-DICRECEP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-DICRECEP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-NCHRONO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-NCHRONO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
