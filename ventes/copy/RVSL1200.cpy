      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVSL1200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSL1200                         
      *   TABLE D'ECHANGE NMD STOCK LOCAL                                       
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVSL1200.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVSL1200.                                                             
      *}                                                                        
          02  SL12-NSOCCRE                                                      
              PIC   X(03).                                                      
          02  SL12-NLIEUCRE                                                     
              PIC   X(03).                                                      
          02  SL12-CTYPDOC                                                      
              PIC   X(02).                                                      
          02  SL12-NUMDOC                                                       
              PIC   S9(07) COMP-3.                                              
          02  SL12-NLDOC                                                        
              PIC   S9(05) COMP-3.                                              
          02  SL12-NCOMM                                                        
              PIC   S9(09) COMP-3.                                              
          02  SL12-NSEQ                                                         
              PIC  S9(05) COMP-3.                                               
          02  SL12-COMMENT                                                      
              PIC   X(75).                                                      
          02  SL12-DSYST                                                        
              PIC   S9(13) COMP-3.                                              
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA VUE RVSL1200                                    
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVSL1200-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVSL1200-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL12-NSOCCRE-F                                                    
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL12-NSOCCRE-F                                                    
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL12-NLIEUCRE-F                                                   
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL12-NLIEUCRE-F                                                   
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL12-CTYPDOC-F                                                    
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL12-CTYPDOC-F                                                    
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL12-NUMDOC-F                                                     
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL12-NUMDOC-F                                                     
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL12-NLDOC-F                                                      
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL12-NLDOC-F                                                      
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL12-NCOMM-F                                                      
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL12-NCOMM-F                                                      
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL12-NSEQ-F                                                       
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL12-NSEQ-F                                                       
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL12-NCOMMENT-F                                                   
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL12-NCOMMENT-F                                                   
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL12-DSYST-F                                                      
      *       PIC   S9(4) COMP.                                                 
      *                                                                         
      *--                                                                       
          02  SL12-DSYST-F                                                      
              PIC   S9(4) COMP-5.                                               
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
