      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Contr�le Commandes WebSph�re 1/2                                00000020
      ***************************************************************** 00000030
       01   EEC21I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSEUILL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MSEUILL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSEUILF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MSEUILI   PIC X(19).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEAL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEAF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEAI   PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBPI     PIC X(2).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRAYONL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCRAYONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCRAYONF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCRAYONI  PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRAYONL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLRAYONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLRAYONF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLRAYONI  PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRCFAML   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MRCFAML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MRCFAMF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MRCFAMI   PIC X(5).                                       00000370
           02 MLIGNESI OCCURS   16 TIMES .                              00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MXL     COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MXL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MXF     PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MXI     PIC X.                                          00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCFAMI  PIC X(5).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFAML  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MLFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLFAMF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MLFAMI  PIC X(20).                                      00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWCCL   COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MWCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MWCCF   PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MWCCI   PIC X.                                          00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSARTL  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MSARTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSARTF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MSARTI  PIC X(4).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSMAGL  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MSMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSMAGF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MSMAGI  PIC X(4).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSDEPOTL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MSDEPOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSDEPOTF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MSDEPOTI     PIC X(4).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLIBERRI  PIC X(79).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MZONCMDI  PIC X(15).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNETNAMI  PIC X(8).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      ***************************************************************** 00000910
      * Contr�le Commandes WebSph�re 1/2                                00000920
      ***************************************************************** 00000930
       01   EEC21O REDEFINES EEC21I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUP  PIC X.                                          00000990
           02 MDATJOUH  PIC X.                                          00001000
           02 MDATJOUV  PIC X.                                          00001010
           02 MDATJOUO  PIC X(10).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MTIMJOUA  PIC X.                                          00001040
           02 MTIMJOUC  PIC X.                                          00001050
           02 MTIMJOUP  PIC X.                                          00001060
           02 MTIMJOUH  PIC X.                                          00001070
           02 MTIMJOUV  PIC X.                                          00001080
           02 MTIMJOUO  PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MSEUILA   PIC X.                                          00001110
           02 MSEUILC   PIC X.                                          00001120
           02 MSEUILP   PIC X.                                          00001130
           02 MSEUILH   PIC X.                                          00001140
           02 MSEUILV   PIC X.                                          00001150
           02 MSEUILO   PIC X(19).                                      00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MPAGEAA   PIC X.                                          00001180
           02 MPAGEAC   PIC X.                                          00001190
           02 MPAGEAP   PIC X.                                          00001200
           02 MPAGEAH   PIC X.                                          00001210
           02 MPAGEAV   PIC X.                                          00001220
           02 MPAGEAO   PIC Z9.                                         00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MNBPA     PIC X.                                          00001250
           02 MNBPC     PIC X.                                          00001260
           02 MNBPP     PIC X.                                          00001270
           02 MNBPH     PIC X.                                          00001280
           02 MNBPV     PIC X.                                          00001290
           02 MNBPO     PIC Z9.                                         00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MCRAYONA  PIC X.                                          00001320
           02 MCRAYONC  PIC X.                                          00001330
           02 MCRAYONP  PIC X.                                          00001340
           02 MCRAYONH  PIC X.                                          00001350
           02 MCRAYONV  PIC X.                                          00001360
           02 MCRAYONO  PIC X(5).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MLRAYONA  PIC X.                                          00001390
           02 MLRAYONC  PIC X.                                          00001400
           02 MLRAYONP  PIC X.                                          00001410
           02 MLRAYONH  PIC X.                                          00001420
           02 MLRAYONV  PIC X.                                          00001430
           02 MLRAYONO  PIC X(20).                                      00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MRCFAMA   PIC X.                                          00001460
           02 MRCFAMC   PIC X.                                          00001470
           02 MRCFAMP   PIC X.                                          00001480
           02 MRCFAMH   PIC X.                                          00001490
           02 MRCFAMV   PIC X.                                          00001500
           02 MRCFAMO   PIC X(5).                                       00001510
           02 MLIGNESO OCCURS   16 TIMES .                              00001520
             03 FILLER       PIC X(2).                                  00001530
             03 MXA     PIC X.                                          00001540
             03 MXC     PIC X.                                          00001550
             03 MXP     PIC X.                                          00001560
             03 MXH     PIC X.                                          00001570
             03 MXV     PIC X.                                          00001580
             03 MXO     PIC X.                                          00001590
             03 FILLER       PIC X(2).                                  00001600
             03 MCFAMA  PIC X.                                          00001610
             03 MCFAMC  PIC X.                                          00001620
             03 MCFAMP  PIC X.                                          00001630
             03 MCFAMH  PIC X.                                          00001640
             03 MCFAMV  PIC X.                                          00001650
             03 MCFAMO  PIC X(5).                                       00001660
             03 FILLER       PIC X(2).                                  00001670
             03 MLFAMA  PIC X.                                          00001680
             03 MLFAMC  PIC X.                                          00001690
             03 MLFAMP  PIC X.                                          00001700
             03 MLFAMH  PIC X.                                          00001710
             03 MLFAMV  PIC X.                                          00001720
             03 MLFAMO  PIC X(20).                                      00001730
             03 FILLER       PIC X(2).                                  00001740
             03 MWCCA   PIC X.                                          00001750
             03 MWCCC   PIC X.                                          00001760
             03 MWCCP   PIC X.                                          00001770
             03 MWCCH   PIC X.                                          00001780
             03 MWCCV   PIC X.                                          00001790
             03 MWCCO   PIC X.                                          00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MSARTA  PIC X.                                          00001820
             03 MSARTC  PIC X.                                          00001830
             03 MSARTP  PIC X.                                          00001840
             03 MSARTH  PIC X.                                          00001850
             03 MSARTV  PIC X.                                          00001860
             03 MSARTO  PIC ---9.                                       00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MSMAGA  PIC X.                                          00001890
             03 MSMAGC  PIC X.                                          00001900
             03 MSMAGP  PIC X.                                          00001910
             03 MSMAGH  PIC X.                                          00001920
             03 MSMAGV  PIC X.                                          00001930
             03 MSMAGO  PIC ---9.                                       00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MSDEPOTA     PIC X.                                     00001960
             03 MSDEPOTC     PIC X.                                     00001970
             03 MSDEPOTP     PIC X.                                     00001980
             03 MSDEPOTH     PIC X.                                     00001990
             03 MSDEPOTV     PIC X.                                     00002000
             03 MSDEPOTO     PIC ---9.                                  00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MLIBERRA  PIC X.                                          00002030
           02 MLIBERRC  PIC X.                                          00002040
           02 MLIBERRP  PIC X.                                          00002050
           02 MLIBERRH  PIC X.                                          00002060
           02 MLIBERRV  PIC X.                                          00002070
           02 MLIBERRO  PIC X(79).                                      00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MCODTRAA  PIC X.                                          00002100
           02 MCODTRAC  PIC X.                                          00002110
           02 MCODTRAP  PIC X.                                          00002120
           02 MCODTRAH  PIC X.                                          00002130
           02 MCODTRAV  PIC X.                                          00002140
           02 MCODTRAO  PIC X(4).                                       00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MZONCMDA  PIC X.                                          00002170
           02 MZONCMDC  PIC X.                                          00002180
           02 MZONCMDP  PIC X.                                          00002190
           02 MZONCMDH  PIC X.                                          00002200
           02 MZONCMDV  PIC X.                                          00002210
           02 MZONCMDO  PIC X(15).                                      00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCICSA    PIC X.                                          00002240
           02 MCICSC    PIC X.                                          00002250
           02 MCICSP    PIC X.                                          00002260
           02 MCICSH    PIC X.                                          00002270
           02 MCICSV    PIC X.                                          00002280
           02 MCICSO    PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNETNAMA  PIC X.                                          00002310
           02 MNETNAMC  PIC X.                                          00002320
           02 MNETNAMP  PIC X.                                          00002330
           02 MNETNAMH  PIC X.                                          00002340
           02 MNETNAMV  PIC X.                                          00002350
           02 MNETNAMO  PIC X(8).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MSCREENA  PIC X.                                          00002380
           02 MSCREENC  PIC X.                                          00002390
           02 MSCREENP  PIC X.                                          00002400
           02 MSCREENH  PIC X.                                          00002410
           02 MSCREENV  PIC X.                                          00002420
           02 MSCREENO  PIC X(4).                                       00002430
                                                                                
