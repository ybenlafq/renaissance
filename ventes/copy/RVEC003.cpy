      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EC003 .COM PARAMETRES PAR ENTITE       *        
      *----------------------------------------------------------------*        
       01  RVEC003.                                                             
           05  EC003-CTABLEG2    PIC X(15).                                     
           05  EC003-CTABLEG2-REDEF REDEFINES EC003-CTABLEG2.                   
               10  EC003-IDSI            PIC X(10).                             
           05  EC003-WTABLEG     PIC X(80).                                     
           05  EC003-WTABLEG-REDEF  REDEFINES EC003-WTABLEG.                    
               10  EC003-CREATION        PIC X(01).                             
               10  EC003-MODIF           PIC X(01).                             
               10  EC003-SUPPRESS        PIC X(01).                             
               10  EC003-DIVERS          PIC X(50).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVEC003-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EC003-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EC003-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EC003-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EC003-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
