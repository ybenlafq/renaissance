      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      *********************************************************                 
      * PREPARATION DE LA TRACE SQL POUR LE MODELE PM9900                       
      *********************************************************                 
      *                                                                         
       CLEF-PM9900             SECTION.                                         
      *                                                                         
           MOVE 'RVPM9900          '       TO   TABLE-NAME.                     
           MOVE 'PM9900'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-PM9900. EXIT.                                                   
                                                                                
