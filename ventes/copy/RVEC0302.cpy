      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVEC0302                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEC0302.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEC0302.                                                            
      *}                                                                        
           10 EC03-NSOCIETE        PIC X(3).                                    
           10 EC03-NLIEU           PIC X(3).                                    
           10 EC03-NVENTE          PIC X(7).                                    
           10 EC03-DVENTE          PIC X(8).                                    
           10 EC03-NSEQNQ          PIC S9(5)V USAGE COMP-3.                     
           10 EC03-CCOLIS          PIC X(26).                                   
           10 EC03-DEXP            PIC X(8).                                    
           10 EC03-DSYST           PIC S9(13)V USAGE COMP-3.                    
           10 EC03-CCDDEL          PIC X(8).                                    
           10 EC03-CCHDEL          PIC X(4).                                    
           10 EC03-DDELIV          PIC X(8).                                    
           10 EC03-WEMPORTE        PIC X(1).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 12      *        
      ******************************************************************        
                                                                                
