      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EIQ01   EIQ01                                              00000020
      ***************************************************************** 00000030
       01   EIQ01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEML   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEMF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMI   PIC X(3).                                       00000210
           02 MTABI OCCURS   10 TIMES .                                 00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000230
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MSELI   PIC X.                                          00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000270
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MCFAMI  PIC X(5).                                       00000300
      * service livraison                                               00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFAML  COMP PIC S9(4).                                 00000320
      *--                                                                       
             03 MLFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLFAMF  PIC X.                                          00000330
             03 FILLER  PIC X(4).                                       00000340
             03 MLFAMI  PIC X(50).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MLIBERRI  PIC X(78).                                      00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MCODTRAI  PIC X(4).                                       00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MCICSI    PIC X(5).                                       00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MNETNAMI  PIC X(8).                                       00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MSCREENI  PIC X(5).                                       00000550
      ***************************************************************** 00000560
      * SDF: EIQ01   EIQ01                                              00000570
      ***************************************************************** 00000580
       01   EIQ01O REDEFINES EIQ01I.                                    00000590
           02 FILLER    PIC X(12).                                      00000600
           02 FILLER    PIC X(2).                                       00000610
           02 MDATJOUA  PIC X.                                          00000620
           02 MDATJOUC  PIC X.                                          00000630
           02 MDATJOUP  PIC X.                                          00000640
           02 MDATJOUH  PIC X.                                          00000650
           02 MDATJOUV  PIC X.                                          00000660
           02 MDATJOUO  PIC X(10).                                      00000670
           02 FILLER    PIC X(2).                                       00000680
           02 MTIMJOUA  PIC X.                                          00000690
           02 MTIMJOUC  PIC X.                                          00000700
           02 MTIMJOUP  PIC X.                                          00000710
           02 MTIMJOUH  PIC X.                                          00000720
           02 MTIMJOUV  PIC X.                                          00000730
           02 MTIMJOUO  PIC X(5).                                       00000740
           02 FILLER    PIC X(2).                                       00000750
           02 MPAGEA    PIC X.                                          00000760
           02 MPAGEC    PIC X.                                          00000770
           02 MPAGEP    PIC X.                                          00000780
           02 MPAGEH    PIC X.                                          00000790
           02 MPAGEV    PIC X.                                          00000800
           02 MPAGEO    PIC X(3).                                       00000810
           02 FILLER    PIC X(2).                                       00000820
           02 MPAGEMA   PIC X.                                          00000830
           02 MPAGEMC   PIC X.                                          00000840
           02 MPAGEMP   PIC X.                                          00000850
           02 MPAGEMH   PIC X.                                          00000860
           02 MPAGEMV   PIC X.                                          00000870
           02 MPAGEMO   PIC X(3).                                       00000880
           02 MTABO OCCURS   10 TIMES .                                 00000890
             03 FILLER       PIC X(2).                                  00000900
             03 MSELA   PIC X.                                          00000910
             03 MSELC   PIC X.                                          00000920
             03 MSELP   PIC X.                                          00000930
             03 MSELH   PIC X.                                          00000940
             03 MSELV   PIC X.                                          00000950
             03 MSELO   PIC X.                                          00000960
             03 FILLER       PIC X(2).                                  00000970
             03 MCFAMA  PIC X.                                          00000980
             03 MCFAMC  PIC X.                                          00000990
             03 MCFAMP  PIC X.                                          00001000
             03 MCFAMH  PIC X.                                          00001010
             03 MCFAMV  PIC X.                                          00001020
             03 MCFAMO  PIC X(5).                                       00001030
      * service livraison                                               00001040
             03 FILLER       PIC X(2).                                  00001050
             03 MLFAMA  PIC X.                                          00001060
             03 MLFAMC  PIC X.                                          00001070
             03 MLFAMP  PIC X.                                          00001080
             03 MLFAMH  PIC X.                                          00001090
             03 MLFAMV  PIC X.                                          00001100
             03 MLFAMO  PIC X(50).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MLIBERRA  PIC X.                                          00001130
           02 MLIBERRC  PIC X.                                          00001140
           02 MLIBERRP  PIC X.                                          00001150
           02 MLIBERRH  PIC X.                                          00001160
           02 MLIBERRV  PIC X.                                          00001170
           02 MLIBERRO  PIC X(78).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MCODTRAA  PIC X.                                          00001200
           02 MCODTRAC  PIC X.                                          00001210
           02 MCODTRAP  PIC X.                                          00001220
           02 MCODTRAH  PIC X.                                          00001230
           02 MCODTRAV  PIC X.                                          00001240
           02 MCODTRAO  PIC X(4).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MCICSA    PIC X.                                          00001270
           02 MCICSC    PIC X.                                          00001280
           02 MCICSP    PIC X.                                          00001290
           02 MCICSH    PIC X.                                          00001300
           02 MCICSV    PIC X.                                          00001310
           02 MCICSO    PIC X(5).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MNETNAMA  PIC X.                                          00001340
           02 MNETNAMC  PIC X.                                          00001350
           02 MNETNAMP  PIC X.                                          00001360
           02 MNETNAMH  PIC X.                                          00001370
           02 MNETNAMV  PIC X.                                          00001380
           02 MNETNAMO  PIC X(8).                                       00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MSCREENA  PIC X.                                          00001410
           02 MSCREENC  PIC X.                                          00001420
           02 MSCREENP  PIC X.                                          00001430
           02 MSCREENH  PIC X.                                          00001440
           02 MSCREENV  PIC X.                                          00001450
           02 MSCREENO  PIC X(5).                                       00001460
                                                                                
