      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : N M D                                            *        
      *  TRANSACTION: MQ10 OU AUTRE                                    *        
      *  TITRE      : COMMAREA DU MODULE STANDARD MSL14                *        
      *  LONGUEUR   : 200                                              *        
      *                                                                *        
      ******************************************************************        
      *                                                                 00230000
      *                                                                 00230000
       01  COMM-SL14-APPLI.                                             00260000
           02 COMM-SL14-NSOCCRE         PIC X(03).                      00320000
           02 COMM-SL14-NLIEUCRE        PIC X(03).                      00330000
           02 COMM-SL14-CTYPDOC         PIC X(03).                      00330000
           02 COMM-SL14-NUMDOC          PIC S9(7) COMP-3.               00330000
           02 COMM-SL14-NSOCMSG         PIC X(03).                      00320000
           02 COMM-SL14-NLIEUMSG        PIC X(03).                      00330000
           02 COMM-SL14-TRAIT           PIC X(01).                      00330000
           02 COMM-SL14-LIGNES          PIC X(01).                      00330000
           02 COMM-SL14-CODRET          PIC X(02).                      00330000
           02 COMM-SL14-ERREUR          PIC X(52).                      00330000
           02 FILLER                    PIC X(109).                     00340000
                                                                                
