      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV51   EGV51                                              00000020
      ***************************************************************** 00000030
       01   EGV51I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTITREL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MLIBTITREL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBTITREF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLIBTITREI     PIC X(26).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MS1L      COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MS1L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MS1F      PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MS1I      PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELSOCL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLIBELSOCL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBELSOCF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLIBELSOCI     PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MM1L      COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MM1L COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MM1F      PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MM1I      PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELMAGL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLIBELMAGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLIBELMAGF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLIBELMAGI     PIC X(20).                                 00000370
           02 M4I OCCURS   11 TIMES .                                   00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 DATEAL  COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 DATEAL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 DATEAF  PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 DATEAI  PIC X(6).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MAL     COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MAL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MAF     PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MAI     PIC X(2).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MBL     COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MBL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MBF     PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MBI     PIC X(2).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCL     COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MCL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MCF     PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MCI     PIC X(2).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDL     COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MDL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MDF     PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MDI     PIC X(2).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNUMAL  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MNUMAL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNUMAF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNUMAI  PIC X(7).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMGL    COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MMGL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MMGF    PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MMGI    PIC X(3).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOL    COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MSOL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MSOF    PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MSOI    PIC X(3).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNUMOL  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MNUMOL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNUMOF  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MNUMOI  PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNUMVL  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MNUMVL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNUMVF  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNUMVI  PIC X(7).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MTYPAL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTYPAF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MTYPAI  PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELAL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MLIBELAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIBELAF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MLIBELAI     PIC X(15).                                 00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MZONCMDI  PIC X(12).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MLIBERRI  PIC X(61).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCODTRAI  PIC X(4).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCICSI    PIC X(5).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MNETNAMI  PIC X(8).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MSCREENI  PIC X(4).                                       00001100
      ***************************************************************** 00001110
      * SDF: EGV51   EGV51                                              00001120
      ***************************************************************** 00001130
       01   EGV51O REDEFINES EGV51I.                                    00001140
           02 FILLER    PIC X(12).                                      00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MDATJOUA  PIC X.                                          00001170
           02 MDATJOUC  PIC X.                                          00001180
           02 MDATJOUP  PIC X.                                          00001190
           02 MDATJOUH  PIC X.                                          00001200
           02 MDATJOUV  PIC X.                                          00001210
           02 MDATJOUO  PIC X(10).                                      00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MTIMJOUA  PIC X.                                          00001240
           02 MTIMJOUC  PIC X.                                          00001250
           02 MTIMJOUP  PIC X.                                          00001260
           02 MTIMJOUH  PIC X.                                          00001270
           02 MTIMJOUV  PIC X.                                          00001280
           02 MTIMJOUO  PIC X(5).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MPAGEA    PIC X.                                          00001310
           02 MPAGEC    PIC X.                                          00001320
           02 MPAGEP    PIC X.                                          00001330
           02 MPAGEH    PIC X.                                          00001340
           02 MPAGEV    PIC X.                                          00001350
           02 MPAGEO    PIC X(3).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MLIBTITREA     PIC X.                                     00001380
           02 MLIBTITREC     PIC X.                                     00001390
           02 MLIBTITREP     PIC X.                                     00001400
           02 MLIBTITREH     PIC X.                                     00001410
           02 MLIBTITREV     PIC X.                                     00001420
           02 MLIBTITREO     PIC X(26).                                 00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MS1A      PIC X.                                          00001450
           02 MS1C      PIC X.                                          00001460
           02 MS1P      PIC X.                                          00001470
           02 MS1H      PIC X.                                          00001480
           02 MS1V      PIC X.                                          00001490
           02 MS1O      PIC X(3).                                       00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MLIBELSOCA     PIC X.                                     00001520
           02 MLIBELSOCC     PIC X.                                     00001530
           02 MLIBELSOCP     PIC X.                                     00001540
           02 MLIBELSOCH     PIC X.                                     00001550
           02 MLIBELSOCV     PIC X.                                     00001560
           02 MLIBELSOCO     PIC X(20).                                 00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MM1A      PIC X.                                          00001590
           02 MM1C      PIC X.                                          00001600
           02 MM1P      PIC X.                                          00001610
           02 MM1H      PIC X.                                          00001620
           02 MM1V      PIC X.                                          00001630
           02 MM1O      PIC X(3).                                       00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MLIBELMAGA     PIC X.                                     00001660
           02 MLIBELMAGC     PIC X.                                     00001670
           02 MLIBELMAGP     PIC X.                                     00001680
           02 MLIBELMAGH     PIC X.                                     00001690
           02 MLIBELMAGV     PIC X.                                     00001700
           02 MLIBELMAGO     PIC X(20).                                 00001710
           02 M4O OCCURS   11 TIMES .                                   00001720
             03 FILLER       PIC X(2).                                  00001730
             03 DATEAA  PIC X.                                          00001740
             03 DATEAC  PIC X.                                          00001750
             03 DATEAP  PIC X.                                          00001760
             03 DATEAH  PIC X.                                          00001770
             03 DATEAV  PIC X.                                          00001780
             03 DATEAO  PIC X(6).                                       00001790
             03 FILLER       PIC X(2).                                  00001800
             03 MAA     PIC X.                                          00001810
             03 MAC     PIC X.                                          00001820
             03 MAP     PIC X.                                          00001830
             03 MAH     PIC X.                                          00001840
             03 MAV     PIC X.                                          00001850
             03 MAO     PIC X(2).                                       00001860
             03 FILLER       PIC X(2).                                  00001870
             03 MBA     PIC X.                                          00001880
             03 MBC     PIC X.                                          00001890
             03 MBP     PIC X.                                          00001900
             03 MBH     PIC X.                                          00001910
             03 MBV     PIC X.                                          00001920
             03 MBO     PIC X(2).                                       00001930
             03 FILLER       PIC X(2).                                  00001940
             03 MCA     PIC X.                                          00001950
             03 MCC     PIC X.                                          00001960
             03 MCP     PIC X.                                          00001970
             03 MCH     PIC X.                                          00001980
             03 MCV     PIC X.                                          00001990
             03 MCO     PIC X(2).                                       00002000
             03 FILLER       PIC X(2).                                  00002010
             03 MDA     PIC X.                                          00002020
             03 MDC     PIC X.                                          00002030
             03 MDP     PIC X.                                          00002040
             03 MDH     PIC X.                                          00002050
             03 MDV     PIC X.                                          00002060
             03 MDO     PIC X(2).                                       00002070
             03 FILLER       PIC X(2).                                  00002080
             03 MNUMAA  PIC X.                                          00002090
             03 MNUMAC  PIC X.                                          00002100
             03 MNUMAP  PIC X.                                          00002110
             03 MNUMAH  PIC X.                                          00002120
             03 MNUMAV  PIC X.                                          00002130
             03 MNUMAO  PIC X(7).                                       00002140
             03 FILLER       PIC X(2).                                  00002150
             03 MMGA    PIC X.                                          00002160
             03 MMGC    PIC X.                                          00002170
             03 MMGP    PIC X.                                          00002180
             03 MMGH    PIC X.                                          00002190
             03 MMGV    PIC X.                                          00002200
             03 MMGO    PIC X(3).                                       00002210
             03 FILLER       PIC X(2).                                  00002220
             03 MSOA    PIC X.                                          00002230
             03 MSOC    PIC X.                                          00002240
             03 MSOP    PIC X.                                          00002250
             03 MSOH    PIC X.                                          00002260
             03 MSOV    PIC X.                                          00002270
             03 MSOO    PIC X(3).                                       00002280
             03 FILLER       PIC X(2).                                  00002290
             03 MNUMOA  PIC X.                                          00002300
             03 MNUMOC  PIC X.                                          00002310
             03 MNUMOP  PIC X.                                          00002320
             03 MNUMOH  PIC X.                                          00002330
             03 MNUMOV  PIC X.                                          00002340
             03 MNUMOO  PIC X(5).                                       00002350
             03 FILLER       PIC X(2).                                  00002360
             03 MNUMVA  PIC X.                                          00002370
             03 MNUMVC  PIC X.                                          00002380
             03 MNUMVP  PIC X.                                          00002390
             03 MNUMVH  PIC X.                                          00002400
             03 MNUMVV  PIC X.                                          00002410
             03 MNUMVO  PIC X(7).                                       00002420
             03 FILLER       PIC X(2).                                  00002430
             03 MTYPAA  PIC X.                                          00002440
             03 MTYPAC  PIC X.                                          00002450
             03 MTYPAP  PIC X.                                          00002460
             03 MTYPAH  PIC X.                                          00002470
             03 MTYPAV  PIC X.                                          00002480
             03 MTYPAO  PIC X(5).                                       00002490
             03 FILLER       PIC X(2).                                  00002500
             03 MLIBELAA     PIC X.                                     00002510
             03 MLIBELAC     PIC X.                                     00002520
             03 MLIBELAP     PIC X.                                     00002530
             03 MLIBELAH     PIC X.                                     00002540
             03 MLIBELAV     PIC X.                                     00002550
             03 MLIBELAO     PIC X(15).                                 00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MZONCMDA  PIC X.                                          00002580
           02 MZONCMDC  PIC X.                                          00002590
           02 MZONCMDP  PIC X.                                          00002600
           02 MZONCMDH  PIC X.                                          00002610
           02 MZONCMDV  PIC X.                                          00002620
           02 MZONCMDO  PIC X(12).                                      00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MLIBERRA  PIC X.                                          00002650
           02 MLIBERRC  PIC X.                                          00002660
           02 MLIBERRP  PIC X.                                          00002670
           02 MLIBERRH  PIC X.                                          00002680
           02 MLIBERRV  PIC X.                                          00002690
           02 MLIBERRO  PIC X(61).                                      00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MCODTRAA  PIC X.                                          00002720
           02 MCODTRAC  PIC X.                                          00002730
           02 MCODTRAP  PIC X.                                          00002740
           02 MCODTRAH  PIC X.                                          00002750
           02 MCODTRAV  PIC X.                                          00002760
           02 MCODTRAO  PIC X(4).                                       00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MCICSA    PIC X.                                          00002790
           02 MCICSC    PIC X.                                          00002800
           02 MCICSP    PIC X.                                          00002810
           02 MCICSH    PIC X.                                          00002820
           02 MCICSV    PIC X.                                          00002830
           02 MCICSO    PIC X(5).                                       00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MNETNAMA  PIC X.                                          00002860
           02 MNETNAMC  PIC X.                                          00002870
           02 MNETNAMP  PIC X.                                          00002880
           02 MNETNAMH  PIC X.                                          00002890
           02 MNETNAMV  PIC X.                                          00002900
           02 MNETNAMO  PIC X(8).                                       00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MSCREENA  PIC X.                                          00002930
           02 MSCREENC  PIC X.                                          00002940
           02 MSCREENP  PIC X.                                          00002950
           02 MSCREENH  PIC X.                                          00002960
           02 MSCREENV  PIC X.                                          00002970
           02 MSCREENO  PIC X(4).                                       00002980
                                                                                
