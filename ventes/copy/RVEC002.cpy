      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EC002 .COM ATTRIBUTS PAR ENTITE        *        
      *----------------------------------------------------------------*        
       01  RVEC002.                                                             
           05  EC002-CTABLEG2    PIC X(15).                                     
           05  EC002-CTABLEG2-REDEF REDEFINES EC002-CTABLEG2.                   
               10  EC002-IDSI            PIC X(10).                             
               10  EC002-NSEQ            PIC X(03).                             
               10  EC002-NSEQ-N         REDEFINES EC002-NSEQ                    
                                         PIC 9(03).                             
           05  EC002-WTABLEG     PIC X(80).                                     
           05  EC002-WTABLEG-REDEF  REDEFINES EC002-WTABLEG.                    
               10  EC002-CMASQ           PIC X(07).                             
               10  EC002-IDATTR          PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVEC002-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EC002-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EC002-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EC002-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EC002-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
