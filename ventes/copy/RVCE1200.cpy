      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVCE1200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVCE1200                         
      *   CLE UNIQUE : NSOCIETE � CFORMAT                                       
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVCE1200.                                                            
           10 CE12-NCODIC          PIC X(07).                                   
           10 CE12-CFAM            PIC X(05).                                   
           10 CE12-CTYPENT         PIC X(02).                                   
           10 CE12-NSOC            PIC X(03).                                   
           10 CE12-NLIEU           PIC X(03).                                   
           10 CE12-FETIQ           PIC X(01).                                   
           10 CE12-CORIG           PIC X(01).                                   
           10 CE12-LORIG           PIC X(10).                                   
           10 CE12-DINSERT         PIC X(14).                                   
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVCE1200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVCE1200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE12-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 CE12-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE12-CFAM-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 CE12-CFAM-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE12-CTYPENT-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 CE12-CTYPENT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE12-NSOC-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 CE12-NSOC-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE12-NLIEU-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 CE12-NLIEU-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE12-FETIQ-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 CE12-FETIQ-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE12-CORIG-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 CE12-CORIG-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE12-LORIG-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 CE12-LORIG-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE12-DINSERT-F       PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 CE12-DINSERT-F       PIC S9(4) COMP-5.                            
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
