      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IAV007 AU 06/07/1999  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,05,BI,A,                          *        
      *                           18,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IAV007.                                                        
            05 NOMETAT-IAV007           PIC X(6) VALUE 'IAV007'.                
            05 RUPTURES-IAV007.                                                 
           10 IAV007-NSOCIETE           PIC X(03).                      007  003
           10 IAV007-NLIEU              PIC X(03).                      010  003
           10 IAV007-CMOTIF             PIC X(05).                      013  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IAV007-SEQUENCE           PIC S9(04) COMP.                018  002
      *--                                                                       
           10 IAV007-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IAV007.                                                   
           10 IAV007-CDEVISE            PIC X(03).                      020  003
           10 IAV007-CTYPAVOIR          PIC X(03).                      023  003
           10 IAV007-FMOIS              PIC X(07).                      026  007
           10 IAV007-LDEVISE            PIC X(05).                      033  005
           10 IAV007-LLIEU              PIC X(20).                      038  020
           10 IAV007-NAVOIRANNUL        PIC X(07).                      058  007
           10 IAV007-TOT-CMOTIF         PIC X(05).                      065  005
           10 IAV007-TOT-LMOTIF         PIC X(20).                      070  020
           10 IAV007-WUTIL              PIC X(01).                      090  001
           10 IAV007-NAVOIR             PIC 9(07)     .                 091  007
           10 IAV007-NBR-AVOIR-MOTIF    PIC 9(05)     .                 098  005
           10 IAV007-DEMIS              PIC X(08).                      103  008
           10 IAV007-PMONTANT           PIC S9(08)V9(2).                111  010
           10 IAV007-PMONTANT1          PIC S9(08)V9(2).                121  010
            05 FILLER                      PIC X(382).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IAV007-LONG           PIC S9(4)   COMP  VALUE +130.           
      *                                                                         
      *--                                                                       
        01  DSECT-IAV007-LONG           PIC S9(4) COMP-5  VALUE +130.           
                                                                                
      *}                                                                        
