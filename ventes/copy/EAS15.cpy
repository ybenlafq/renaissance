      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAS10   EAS10                                              00000020
      ***************************************************************** 00000030
       01   EAS15I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAMPAL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCAMPAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCAMPAF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCAMPAI   PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MLIBELLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBELLF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLIBELLI  PIC X(30).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CFAM1L    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 CFAM1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 CFAM1F    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 CFAM1I    PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LFAM1L    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 LFAM1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 LFAM1F    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 LFAM1I    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CFAM2L    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 CFAM2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 CFAM2F    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 CFAM2I    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LFAM2L    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 LFAM2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 LFAM2F    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 LFAM2I    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CFAM3L    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 CFAM3L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 CFAM3F    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 CFAM3I    PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LFAM3L    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 LFAM3L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 LFAM3F    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 LFAM3I    PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CFAM4L    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 CFAM4L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 CFAM4F    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 CFAM4I    PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LFAM4L    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 LFAM4L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 LFAM4F    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 LFAM4I    PIC X(20).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CFAM5L    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 CFAM5L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 CFAM5F    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 CFAM5I    PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LFAM5L    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 LFAM5L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 LFAM5F    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 LFAM5I    PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CFAM6L    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 CFAM6L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 CFAM6F    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 CFAM6I    PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LFAM6L    COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 LFAM6L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 LFAM6F    PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 LFAM6I    PIC X(20).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CFAM7L    COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 CFAM7L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 CFAM7F    PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 CFAM7I    PIC X(5).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LFAM7L    COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 LFAM7L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 LFAM7F    PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 LFAM7I    PIC X(20).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CFAM8L    COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 CFAM8L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 CFAM8F    PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 CFAM8I    PIC X(5).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LFAM8L    COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 LFAM8L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 LFAM8F    PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 LFAM8I    PIC X(20).                                      00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CFAM9L    COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 CFAM9L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 CFAM9F    PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 CFAM9I    PIC X(5).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LFAM9L    COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 LFAM9L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 LFAM9F    PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 LFAM9I    PIC X(20).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CFAM10L   COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 CFAM10L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 CFAM10F   PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 CFAM10I   PIC X(5).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LFAM10L   COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 LFAM10L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 LFAM10F   PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 LFAM10I   PIC X(20).                                      00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MLIBERRI  PIC X(78).                                      00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MCODTRAI  PIC X(4).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MCICSI    PIC X(5).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MNETNAMI  PIC X(8).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MSCREENI  PIC X(5).                                       00001210
      ***************************************************************** 00001220
      * SDF: EAS10   EAS10                                              00001230
      ***************************************************************** 00001240
       01   EAS15O REDEFINES EAS15I.                                    00001250
           02 FILLER    PIC X(12).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MDATJOUA  PIC X.                                          00001280
           02 MDATJOUC  PIC X.                                          00001290
           02 MDATJOUP  PIC X.                                          00001300
           02 MDATJOUH  PIC X.                                          00001310
           02 MDATJOUV  PIC X.                                          00001320
           02 MDATJOUO  PIC X(10).                                      00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MTIMJOUA  PIC X.                                          00001350
           02 MTIMJOUC  PIC X.                                          00001360
           02 MTIMJOUP  PIC X.                                          00001370
           02 MTIMJOUH  PIC X.                                          00001380
           02 MTIMJOUV  PIC X.                                          00001390
           02 MTIMJOUO  PIC X(5).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MCAMPAA   PIC X.                                          00001420
           02 MCAMPAC   PIC X.                                          00001430
           02 MCAMPAP   PIC X.                                          00001440
           02 MCAMPAH   PIC X.                                          00001450
           02 MCAMPAV   PIC X.                                          00001460
           02 MCAMPAO   PIC X(4).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MLIBELLA  PIC X.                                          00001490
           02 MLIBELLC  PIC X.                                          00001500
           02 MLIBELLP  PIC X.                                          00001510
           02 MLIBELLH  PIC X.                                          00001520
           02 MLIBELLV  PIC X.                                          00001530
           02 MLIBELLO  PIC X(30).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 CFAM1A    PIC X.                                          00001560
           02 CFAM1C    PIC X.                                          00001570
           02 CFAM1P    PIC X.                                          00001580
           02 CFAM1H    PIC X.                                          00001590
           02 CFAM1V    PIC X.                                          00001600
           02 CFAM1O    PIC X(5).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 LFAM1A    PIC X.                                          00001630
           02 LFAM1C    PIC X.                                          00001640
           02 LFAM1P    PIC X.                                          00001650
           02 LFAM1H    PIC X.                                          00001660
           02 LFAM1V    PIC X.                                          00001670
           02 LFAM1O    PIC X(20).                                      00001680
           02 FILLER    PIC X(2).                                       00001690
           02 CFAM2A    PIC X.                                          00001700
           02 CFAM2C    PIC X.                                          00001710
           02 CFAM2P    PIC X.                                          00001720
           02 CFAM2H    PIC X.                                          00001730
           02 CFAM2V    PIC X.                                          00001740
           02 CFAM2O    PIC X(5).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 LFAM2A    PIC X.                                          00001770
           02 LFAM2C    PIC X.                                          00001780
           02 LFAM2P    PIC X.                                          00001790
           02 LFAM2H    PIC X.                                          00001800
           02 LFAM2V    PIC X.                                          00001810
           02 LFAM2O    PIC X(20).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 CFAM3A    PIC X.                                          00001840
           02 CFAM3C    PIC X.                                          00001850
           02 CFAM3P    PIC X.                                          00001860
           02 CFAM3H    PIC X.                                          00001870
           02 CFAM3V    PIC X.                                          00001880
           02 CFAM3O    PIC X(5).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 LFAM3A    PIC X.                                          00001910
           02 LFAM3C    PIC X.                                          00001920
           02 LFAM3P    PIC X.                                          00001930
           02 LFAM3H    PIC X.                                          00001940
           02 LFAM3V    PIC X.                                          00001950
           02 LFAM3O    PIC X(20).                                      00001960
           02 FILLER    PIC X(2).                                       00001970
           02 CFAM4A    PIC X.                                          00001980
           02 CFAM4C    PIC X.                                          00001990
           02 CFAM4P    PIC X.                                          00002000
           02 CFAM4H    PIC X.                                          00002010
           02 CFAM4V    PIC X.                                          00002020
           02 CFAM4O    PIC X(5).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 LFAM4A    PIC X.                                          00002050
           02 LFAM4C    PIC X.                                          00002060
           02 LFAM4P    PIC X.                                          00002070
           02 LFAM4H    PIC X.                                          00002080
           02 LFAM4V    PIC X.                                          00002090
           02 LFAM4O    PIC X(20).                                      00002100
           02 FILLER    PIC X(2).                                       00002110
           02 CFAM5A    PIC X.                                          00002120
           02 CFAM5C    PIC X.                                          00002130
           02 CFAM5P    PIC X.                                          00002140
           02 CFAM5H    PIC X.                                          00002150
           02 CFAM5V    PIC X.                                          00002160
           02 CFAM5O    PIC X(5).                                       00002170
           02 FILLER    PIC X(2).                                       00002180
           02 LFAM5A    PIC X.                                          00002190
           02 LFAM5C    PIC X.                                          00002200
           02 LFAM5P    PIC X.                                          00002210
           02 LFAM5H    PIC X.                                          00002220
           02 LFAM5V    PIC X.                                          00002230
           02 LFAM5O    PIC X(20).                                      00002240
           02 FILLER    PIC X(2).                                       00002250
           02 CFAM6A    PIC X.                                          00002260
           02 CFAM6C    PIC X.                                          00002270
           02 CFAM6P    PIC X.                                          00002280
           02 CFAM6H    PIC X.                                          00002290
           02 CFAM6V    PIC X.                                          00002300
           02 CFAM6O    PIC X(5).                                       00002310
           02 FILLER    PIC X(2).                                       00002320
           02 LFAM6A    PIC X.                                          00002330
           02 LFAM6C    PIC X.                                          00002340
           02 LFAM6P    PIC X.                                          00002350
           02 LFAM6H    PIC X.                                          00002360
           02 LFAM6V    PIC X.                                          00002370
           02 LFAM6O    PIC X(20).                                      00002380
           02 FILLER    PIC X(2).                                       00002390
           02 CFAM7A    PIC X.                                          00002400
           02 CFAM7C    PIC X.                                          00002410
           02 CFAM7P    PIC X.                                          00002420
           02 CFAM7H    PIC X.                                          00002430
           02 CFAM7V    PIC X.                                          00002440
           02 CFAM7O    PIC X(5).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 LFAM7A    PIC X.                                          00002470
           02 LFAM7C    PIC X.                                          00002480
           02 LFAM7P    PIC X.                                          00002490
           02 LFAM7H    PIC X.                                          00002500
           02 LFAM7V    PIC X.                                          00002510
           02 LFAM7O    PIC X(20).                                      00002520
           02 FILLER    PIC X(2).                                       00002530
           02 CFAM8A    PIC X.                                          00002540
           02 CFAM8C    PIC X.                                          00002550
           02 CFAM8P    PIC X.                                          00002560
           02 CFAM8H    PIC X.                                          00002570
           02 CFAM8V    PIC X.                                          00002580
           02 CFAM8O    PIC X(5).                                       00002590
           02 FILLER    PIC X(2).                                       00002600
           02 LFAM8A    PIC X.                                          00002610
           02 LFAM8C    PIC X.                                          00002620
           02 LFAM8P    PIC X.                                          00002630
           02 LFAM8H    PIC X.                                          00002640
           02 LFAM8V    PIC X.                                          00002650
           02 LFAM8O    PIC X(20).                                      00002660
           02 FILLER    PIC X(2).                                       00002670
           02 CFAM9A    PIC X.                                          00002680
           02 CFAM9C    PIC X.                                          00002690
           02 CFAM9P    PIC X.                                          00002700
           02 CFAM9H    PIC X.                                          00002710
           02 CFAM9V    PIC X.                                          00002720
           02 CFAM9O    PIC X(5).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 LFAM9A    PIC X.                                          00002750
           02 LFAM9C    PIC X.                                          00002760
           02 LFAM9P    PIC X.                                          00002770
           02 LFAM9H    PIC X.                                          00002780
           02 LFAM9V    PIC X.                                          00002790
           02 LFAM9O    PIC X(20).                                      00002800
           02 FILLER    PIC X(2).                                       00002810
           02 CFAM10A   PIC X.                                          00002820
           02 CFAM10C   PIC X.                                          00002830
           02 CFAM10P   PIC X.                                          00002840
           02 CFAM10H   PIC X.                                          00002850
           02 CFAM10V   PIC X.                                          00002860
           02 CFAM10O   PIC X(5).                                       00002870
           02 FILLER    PIC X(2).                                       00002880
           02 LFAM10A   PIC X.                                          00002890
           02 LFAM10C   PIC X.                                          00002900
           02 LFAM10P   PIC X.                                          00002910
           02 LFAM10H   PIC X.                                          00002920
           02 LFAM10V   PIC X.                                          00002930
           02 LFAM10O   PIC X(20).                                      00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MLIBERRA  PIC X.                                          00002960
           02 MLIBERRC  PIC X.                                          00002970
           02 MLIBERRP  PIC X.                                          00002980
           02 MLIBERRH  PIC X.                                          00002990
           02 MLIBERRV  PIC X.                                          00003000
           02 MLIBERRO  PIC X(78).                                      00003010
           02 FILLER    PIC X(2).                                       00003020
           02 MCODTRAA  PIC X.                                          00003030
           02 MCODTRAC  PIC X.                                          00003040
           02 MCODTRAP  PIC X.                                          00003050
           02 MCODTRAH  PIC X.                                          00003060
           02 MCODTRAV  PIC X.                                          00003070
           02 MCODTRAO  PIC X(4).                                       00003080
           02 FILLER    PIC X(2).                                       00003090
           02 MCICSA    PIC X.                                          00003100
           02 MCICSC    PIC X.                                          00003110
           02 MCICSP    PIC X.                                          00003120
           02 MCICSH    PIC X.                                          00003130
           02 MCICSV    PIC X.                                          00003140
           02 MCICSO    PIC X(5).                                       00003150
           02 FILLER    PIC X(2).                                       00003160
           02 MNETNAMA  PIC X.                                          00003170
           02 MNETNAMC  PIC X.                                          00003180
           02 MNETNAMP  PIC X.                                          00003190
           02 MNETNAMH  PIC X.                                          00003200
           02 MNETNAMV  PIC X.                                          00003210
           02 MNETNAMO  PIC X(8).                                       00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MSCREENA  PIC X.                                          00003240
           02 MSCREENC  PIC X.                                          00003250
           02 MSCREENP  PIC X.                                          00003260
           02 MSCREENH  PIC X.                                          00003270
           02 MSCREENV  PIC X.                                          00003280
           02 MSCREENO  PIC X(5).                                       00003290
                                                                                
