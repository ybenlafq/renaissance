      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAC00   EAC00                                              00000020
      ***************************************************************** 00000030
       01   EAC00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * numero de semaine                                               00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMSEML  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MNUMSEML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUMSEMF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MNUMSEMI  PIC X(2).                                       00000200
      * date du lundi                                                   00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLUNDIL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLUNDIL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLUNDIF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLUNDII   PIC X(8).                                       00000250
      * jour semaine                                                    00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJSEML    COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MJSEML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MJSEMF    PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MJSEMI    PIC X(8).                                       00000300
      * date jour semaine                                               00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDJSEML   COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MDJSEML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDJSEMF   PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MDJSEMI   PIC X(8).                                       00000350
      * nsociete                                                        00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEU1L   COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MLIEU1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEU1F   PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MLIEU1I   PIC X(3).                                       00000400
      * nlieu                                                           00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEU2L   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLIEU2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEU2F   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLIEU2I   PIC X(3).                                       00000450
      * numero option                                                   00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOPTIONL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MOPTIONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MOPTIONF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MOPTIONI  PIC X.                                          00000500
      * zone de commande                                                00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MZONCMDI  PIC X(15).                                      00000550
      * message erreur                                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MLIBERRI  PIC X(54).                                      00000600
      * CODE TRANSACTION                                                00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCODTRAI  PIC X(4).                                       00000650
      * NOM DU CICS                                                     00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCICSI    PIC X(5).                                       00000700
      * NETNAME                                                         00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MNETNAMI  PIC X(8).                                       00000750
      * CODE TERMINAL                                                   00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MSCREENI  PIC X(5).                                       00000800
      ***************************************************************** 00000810
      * SDF: EAC00   EAC00                                              00000820
      ***************************************************************** 00000830
       01   EAC00O REDEFINES EAC00I.                                    00000840
           02 FILLER    PIC X(12).                                      00000850
      * DATE DU JOUR                                                    00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MDATJOUA  PIC X.                                          00000880
           02 MDATJOUC  PIC X.                                          00000890
           02 MDATJOUP  PIC X.                                          00000900
           02 MDATJOUH  PIC X.                                          00000910
           02 MDATJOUV  PIC X.                                          00000920
           02 MDATJOUO  PIC X(10).                                      00000930
      * HEURE                                                           00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MTIMJOUA  PIC X.                                          00000960
           02 MTIMJOUC  PIC X.                                          00000970
           02 MTIMJOUP  PIC X.                                          00000980
           02 MTIMJOUH  PIC X.                                          00000990
           02 MTIMJOUV  PIC X.                                          00001000
           02 MTIMJOUO  PIC X(5).                                       00001010
      * numero de semaine                                               00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MNUMSEMA  PIC X.                                          00001040
           02 MNUMSEMC  PIC X.                                          00001050
           02 MNUMSEMP  PIC X.                                          00001060
           02 MNUMSEMH  PIC X.                                          00001070
           02 MNUMSEMV  PIC X.                                          00001080
           02 MNUMSEMO  PIC X(2).                                       00001090
      * date du lundi                                                   00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MLUNDIA   PIC X.                                          00001120
           02 MLUNDIC   PIC X.                                          00001130
           02 MLUNDIP   PIC X.                                          00001140
           02 MLUNDIH   PIC X.                                          00001150
           02 MLUNDIV   PIC X.                                          00001160
           02 MLUNDIO   PIC X(8).                                       00001170
      * jour semaine                                                    00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MJSEMA    PIC X.                                          00001200
           02 MJSEMC    PIC X.                                          00001210
           02 MJSEMP    PIC X.                                          00001220
           02 MJSEMH    PIC X.                                          00001230
           02 MJSEMV    PIC X.                                          00001240
           02 MJSEMO    PIC X(8).                                       00001250
      * date jour semaine                                               00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MDJSEMA   PIC X.                                          00001280
           02 MDJSEMC   PIC X.                                          00001290
           02 MDJSEMP   PIC X.                                          00001300
           02 MDJSEMH   PIC X.                                          00001310
           02 MDJSEMV   PIC X.                                          00001320
           02 MDJSEMO   PIC X(8).                                       00001330
      * nsociete                                                        00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MLIEU1A   PIC X.                                          00001360
           02 MLIEU1C   PIC X.                                          00001370
           02 MLIEU1P   PIC X.                                          00001380
           02 MLIEU1H   PIC X.                                          00001390
           02 MLIEU1V   PIC X.                                          00001400
           02 MLIEU1O   PIC X(3).                                       00001410
      * nlieu                                                           00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MLIEU2A   PIC X.                                          00001440
           02 MLIEU2C   PIC X.                                          00001450
           02 MLIEU2P   PIC X.                                          00001460
           02 MLIEU2H   PIC X.                                          00001470
           02 MLIEU2V   PIC X.                                          00001480
           02 MLIEU2O   PIC X(3).                                       00001490
      * numero option                                                   00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MOPTIONA  PIC X.                                          00001520
           02 MOPTIONC  PIC X.                                          00001530
           02 MOPTIONP  PIC X.                                          00001540
           02 MOPTIONH  PIC X.                                          00001550
           02 MOPTIONV  PIC X.                                          00001560
           02 MOPTIONO  PIC X.                                          00001570
      * zone de commande                                                00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MZONCMDA  PIC X.                                          00001600
           02 MZONCMDC  PIC X.                                          00001610
           02 MZONCMDP  PIC X.                                          00001620
           02 MZONCMDH  PIC X.                                          00001630
           02 MZONCMDV  PIC X.                                          00001640
           02 MZONCMDO  PIC X(15).                                      00001650
      * message erreur                                                  00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MLIBERRA  PIC X.                                          00001680
           02 MLIBERRC  PIC X.                                          00001690
           02 MLIBERRP  PIC X.                                          00001700
           02 MLIBERRH  PIC X.                                          00001710
           02 MLIBERRV  PIC X.                                          00001720
           02 MLIBERRO  PIC X(54).                                      00001730
      * CODE TRANSACTION                                                00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MCODTRAA  PIC X.                                          00001760
           02 MCODTRAC  PIC X.                                          00001770
           02 MCODTRAP  PIC X.                                          00001780
           02 MCODTRAH  PIC X.                                          00001790
           02 MCODTRAV  PIC X.                                          00001800
           02 MCODTRAO  PIC X(4).                                       00001810
      * NOM DU CICS                                                     00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCICSA    PIC X.                                          00001840
           02 MCICSC    PIC X.                                          00001850
           02 MCICSP    PIC X.                                          00001860
           02 MCICSH    PIC X.                                          00001870
           02 MCICSV    PIC X.                                          00001880
           02 MCICSO    PIC X(5).                                       00001890
      * NETNAME                                                         00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MNETNAMA  PIC X.                                          00001920
           02 MNETNAMC  PIC X.                                          00001930
           02 MNETNAMP  PIC X.                                          00001940
           02 MNETNAMH  PIC X.                                          00001950
           02 MNETNAMV  PIC X.                                          00001960
           02 MNETNAMO  PIC X(8).                                       00001970
      * CODE TERMINAL                                                   00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MSCREENA  PIC X.                                          00002000
           02 MSCREENC  PIC X.                                          00002010
           02 MSCREENP  PIC X.                                          00002020
           02 MSCREENH  PIC X.                                          00002030
           02 MSCREENV  PIC X.                                          00002040
           02 MSCREENO  PIC X(5).                                       00002050
                                                                                
