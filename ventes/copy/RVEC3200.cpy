      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RTEC32                      *        
      ******************************************************************        
       01  RVEC3200.                                                            
      *                       N. Commande WCPE                                  
           10 EC32-NCDEWC          PIC S9(15)V USAGE COMP-3.                    
      *                       N. Commande WCPE                                  
           10 EC32-NCDEWC2        PIC S9(15)V USAGE COMP-3.                     
      *                       Date de Vente                                     
           10 EC32-DVENTE          PIC X(8).                                    
      *                       date validation du paiement                       
           10 EC32-DVALP           PIC X(8).                                    
      *                       �tat du paiement                                  
           10 EC32-WVALP           PIC X(1).                                    
      *                       code paiement                                     
           10 EC32-CPAYM           PIC X(8).                                    
      *                       Montant du paiement                               
           10 EC32-MTPAYM          PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       Programme ayant mis � jour                        
           10 EC32-CNOMPROG        PIC X(8).                                    
      *                       Date syst�me                                      
           10 EC32-DSYST          PIC S9(13)V USAGE COMP-3.                     
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 05      *        
      ******************************************************************        
                                                                                
