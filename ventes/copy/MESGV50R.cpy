      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ INTERROGATION EXISTENCE VENTE                        
      *    EN TOURNEE                                                           
      * ---------------------------------------------------------------         
      * 12/08/2011  MARQUE INNOV                                                
      * INNOV PROBLEME DE LONGUEUR DE MESSAGE ON PERDAIT LES INFOS              
      * CREDIT ET IL MANQUAIT LE N� D'AUTORISATION ARF                          
      *****************************************************************         
      *  POUR MGV50  REPONSE A INTERROGATION                                    
      *  TAILLE 218                                                             
           10  WS-MESSAGE REDEFINES COMM-MQ20-MESSAGE.                          
      *---                                                                      
           15  MES-ENTETE.                                                      
               20   MES-TYPE      PIC    X(3).                                  
               20   MES-NSOCMSG   PIC    X(3).                                  
               20   MES-NLIEUMSG  PIC    X(3).                                  
               20   MES-NSOCDST   PIC    X(3).                                  
               20   MES-NLIEUDST  PIC    X(3).                                  
               20   MES-NORD      PIC    9(8).                                  
               20   MES-LPROG     PIC    X(10).                                 
               20   MES-DJOUR     PIC    X(8).                                  
               20   MES-WSID      PIC    X(10).                                 
               20   MES-USER      PIC    X(10).                                 
               20   MES-CHRONO    PIC    9(7).                                  
               20   MES-NBRMSG    PIC    9(7).                                  
               20   MES-FILLER    PIC    X(30).                                 
           15  MES-DATA           PIC    X(118).                                
           15  MESGV50-DATA REDEFINES MES-DATA.                                 
      *  VALEURS O : EXISTE / N : N'EXISTE PAS / T : EXISTE, EN TOURNEE         
      *          A : ADRESSE NON TROUVE                                         
           20  MES-INTER-EXISTE   PIC    X.                                     
           20  MES-CLIENT.                                                      
               25   MES-LNOM                 PIC X(25).                         
               25   MES-LCOMMUNE             PIC X(32).                         
               25   MES-CPOSTAL              PIC X(5).                          
           20  MES-VENTE.                                                       
               25   MES-PTTVENTE             PIC S9(7)V99 COMP-3.               
               25   MES-PCOMPT               PIC S9(7)V99 COMP-3.               
               25   MES-PVERSE               PIC S9(7)V99 COMP-3.               
               25   MES-PLIVR                PIC S9(7)V99 COMP-3.               
               25   MES-PDIFFERE             PIC S9(7)V99 COMP-3.               
               25   MES-PRFACT               PIC S9(7)V99 COMP-3.               
           20  MES-CREDIT.                                                      
               25   MES-CORGORED             PIC X(5).                          
               25   MES-CFCRED               PIC X(5).                          
               25   MES-NCREDI               PIC X(14).                         
               25   MES-NAUTO                PIC X(20).                         
           15  MESERR-DATA REDEFINES MES-DATA.                                  
               20   MESERR-DATA-X PIC    X(100).                                
          10   LONGUEUR-MESSAGE         PIC S9(03) VALUE +242.                  
                                                                                
