      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAC14   EAC14                                              00000020
      ***************************************************************** 00000030
       01   EAC14I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDLCL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MDLCL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MDLCF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MDLCI     PIC X(3).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEBL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MDATDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDEBF  PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MDATDEBI  PIC X(10).                                      00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFLCL     COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MFLCL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MFLCF     PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MFLCI     PIC X(3).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFINL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MDATFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFINF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MDATFINI  PIC X(10).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDLCN1L   COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MDLCN1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDLCN1F   PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MDLCN1I   PIC X(3).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDN1L  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MDATDN1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDN1F  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MDATDN1I  PIC X(10).                                      00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFLCN1L   COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MFLCN1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFLCN1F   PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MFLCN1I   PIC X(3).                                       00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFN1L  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MDATFN1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFN1F  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MDATFN1I  PIC X(10).                                      00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSOCL    COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MCSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCSOCF    PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MCSOCI    PIC X(3).                                       00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MLSOCI    PIC X(21).                                      00000590
           02 FILLER  OCCURS   10 TIMES .                               00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMAGL  COMP PIC S9(4).                                 00000610
      *--                                                                       
             03 MCMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCMAGF  PIC X.                                          00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MCMAGI  PIC X(3).                                       00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMAGL  COMP PIC S9(4).                                 00000650
      *--                                                                       
             03 MLMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLMAGF  PIC X.                                          00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MLMAGI  PIC X(10).                                      00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCATLML      COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MCATLML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCATLMF      PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MCATLMI      PIC X(6).                                  00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAELAL      COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MCAELAL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCAELAF      PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MCAELAI      PIC X(6).                                  00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCADACL      COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MCADACL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCADACF      PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MCADACI      PIC X(6).                                  00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCATOTL      COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MCATOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCATOTF      PIC X.                                     00000820
             03 FILLER  PIC X(4).                                       00000830
             03 MCATOTI      PIC X(6).                                  00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTRDARL      COMP PIC S9(4).                            00000850
      *--                                                                       
             03 MTRDARL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTRDARF      PIC X.                                     00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MTRDARI      PIC X(6).                                  00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTRDACL      COMP PIC S9(4).                            00000890
      *--                                                                       
             03 MTRDACL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTRDACF      PIC X.                                     00000900
             03 FILLER  PIC X(4).                                       00000910
             03 MTRDACI      PIC X(6).                                  00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBENTL      COMP PIC S9(4).                            00000930
      *--                                                                       
             03 MNBENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNBENTF      PIC X.                                     00000940
             03 FILLER  PIC X(4).                                       00000950
             03 MNBENTI      PIC X(6).                                  00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTXCRL  COMP PIC S9(4).                                 00000970
      *--                                                                       
             03 MTXCRL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTXCRF  PIC X.                                          00000980
             03 FILLER  PIC X(4).                                       00000990
             03 MTXCRI  PIC X(5).                                       00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001020
           02 FILLER    PIC X(4).                                       00001030
           02 MZONCMDI  PIC X(15).                                      00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MLIBERRI  PIC X(54).                                      00001080
      * CODE TRANSACTION                                                00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MCODTRAI  PIC X(4).                                       00001130
      * NOM DU CICS                                                     00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      * NETNAME                                                         00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001200
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001210
           02 FILLER    PIC X(4).                                       00001220
           02 MNETNAMI  PIC X(8).                                       00001230
      * CODE TERMINAL                                                   00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001250
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001260
           02 FILLER    PIC X(4).                                       00001270
           02 MSCREENI  PIC X(5).                                       00001280
      ***************************************************************** 00001290
      * SDF: EAC14   EAC14                                              00001300
      ***************************************************************** 00001310
       01   EAC14O REDEFINES EAC14I.                                    00001320
           02 FILLER    PIC X(12).                                      00001330
      * DATE DU JOUR                                                    00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MDATJOUA  PIC X.                                          00001360
           02 MDATJOUC  PIC X.                                          00001370
           02 MDATJOUP  PIC X.                                          00001380
           02 MDATJOUH  PIC X.                                          00001390
           02 MDATJOUV  PIC X.                                          00001400
           02 MDATJOUO  PIC X(10).                                      00001410
      * HEURE                                                           00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MTIMJOUA  PIC X.                                          00001440
           02 MTIMJOUC  PIC X.                                          00001450
           02 MTIMJOUP  PIC X.                                          00001460
           02 MTIMJOUH  PIC X.                                          00001470
           02 MTIMJOUV  PIC X.                                          00001480
           02 MTIMJOUO  PIC X(5).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MPAGEA    PIC X.                                          00001510
           02 MPAGEC    PIC X.                                          00001520
           02 MPAGEP    PIC X.                                          00001530
           02 MPAGEH    PIC X.                                          00001540
           02 MPAGEV    PIC X.                                          00001550
           02 MPAGEO    PIC X(2).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MDLCA     PIC X.                                          00001580
           02 MDLCC     PIC X.                                          00001590
           02 MDLCP     PIC X.                                          00001600
           02 MDLCH     PIC X.                                          00001610
           02 MDLCV     PIC X.                                          00001620
           02 MDLCO     PIC X(3).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MDATDEBA  PIC X.                                          00001650
           02 MDATDEBC  PIC X.                                          00001660
           02 MDATDEBP  PIC X.                                          00001670
           02 MDATDEBH  PIC X.                                          00001680
           02 MDATDEBV  PIC X.                                          00001690
           02 MDATDEBO  PIC X(10).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MFLCA     PIC X.                                          00001720
           02 MFLCC     PIC X.                                          00001730
           02 MFLCP     PIC X.                                          00001740
           02 MFLCH     PIC X.                                          00001750
           02 MFLCV     PIC X.                                          00001760
           02 MFLCO     PIC X(3).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MDATFINA  PIC X.                                          00001790
           02 MDATFINC  PIC X.                                          00001800
           02 MDATFINP  PIC X.                                          00001810
           02 MDATFINH  PIC X.                                          00001820
           02 MDATFINV  PIC X.                                          00001830
           02 MDATFINO  PIC X(10).                                      00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MDLCN1A   PIC X.                                          00001860
           02 MDLCN1C   PIC X.                                          00001870
           02 MDLCN1P   PIC X.                                          00001880
           02 MDLCN1H   PIC X.                                          00001890
           02 MDLCN1V   PIC X.                                          00001900
           02 MDLCN1O   PIC X(3).                                       00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MDATDN1A  PIC X.                                          00001930
           02 MDATDN1C  PIC X.                                          00001940
           02 MDATDN1P  PIC X.                                          00001950
           02 MDATDN1H  PIC X.                                          00001960
           02 MDATDN1V  PIC X.                                          00001970
           02 MDATDN1O  PIC X(10).                                      00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MFLCN1A   PIC X.                                          00002000
           02 MFLCN1C   PIC X.                                          00002010
           02 MFLCN1P   PIC X.                                          00002020
           02 MFLCN1H   PIC X.                                          00002030
           02 MFLCN1V   PIC X.                                          00002040
           02 MFLCN1O   PIC X(3).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MDATFN1A  PIC X.                                          00002070
           02 MDATFN1C  PIC X.                                          00002080
           02 MDATFN1P  PIC X.                                          00002090
           02 MDATFN1H  PIC X.                                          00002100
           02 MDATFN1V  PIC X.                                          00002110
           02 MDATFN1O  PIC X(10).                                      00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MCSOCA    PIC X.                                          00002140
           02 MCSOCC    PIC X.                                          00002150
           02 MCSOCP    PIC X.                                          00002160
           02 MCSOCH    PIC X.                                          00002170
           02 MCSOCV    PIC X.                                          00002180
           02 MCSOCO    PIC X(3).                                       00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLSOCA    PIC X.                                          00002210
           02 MLSOCC    PIC X.                                          00002220
           02 MLSOCP    PIC X.                                          00002230
           02 MLSOCH    PIC X.                                          00002240
           02 MLSOCV    PIC X.                                          00002250
           02 MLSOCO    PIC X(21).                                      00002260
           02 FILLER  OCCURS   10 TIMES .                               00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MCMAGA  PIC X.                                          00002290
             03 MCMAGC  PIC X.                                          00002300
             03 MCMAGP  PIC X.                                          00002310
             03 MCMAGH  PIC X.                                          00002320
             03 MCMAGV  PIC X.                                          00002330
             03 MCMAGO  PIC X(3).                                       00002340
             03 FILLER       PIC X(2).                                  00002350
             03 MLMAGA  PIC X.                                          00002360
             03 MLMAGC  PIC X.                                          00002370
             03 MLMAGP  PIC X.                                          00002380
             03 MLMAGH  PIC X.                                          00002390
             03 MLMAGV  PIC X.                                          00002400
             03 MLMAGO  PIC X(10).                                      00002410
             03 FILLER       PIC X(2).                                  00002420
             03 MCATLMA      PIC X.                                     00002430
             03 MCATLMC PIC X.                                          00002440
             03 MCATLMP PIC X.                                          00002450
             03 MCATLMH PIC X.                                          00002460
             03 MCATLMV PIC X.                                          00002470
             03 MCATLMO      PIC X(6).                                  00002480
             03 FILLER       PIC X(2).                                  00002490
             03 MCAELAA      PIC X.                                     00002500
             03 MCAELAC PIC X.                                          00002510
             03 MCAELAP PIC X.                                          00002520
             03 MCAELAH PIC X.                                          00002530
             03 MCAELAV PIC X.                                          00002540
             03 MCAELAO      PIC X(6).                                  00002550
             03 FILLER       PIC X(2).                                  00002560
             03 MCADACA      PIC X.                                     00002570
             03 MCADACC PIC X.                                          00002580
             03 MCADACP PIC X.                                          00002590
             03 MCADACH PIC X.                                          00002600
             03 MCADACV PIC X.                                          00002610
             03 MCADACO      PIC X(6).                                  00002620
             03 FILLER       PIC X(2).                                  00002630
             03 MCATOTA      PIC X.                                     00002640
             03 MCATOTC PIC X.                                          00002650
             03 MCATOTP PIC X.                                          00002660
             03 MCATOTH PIC X.                                          00002670
             03 MCATOTV PIC X.                                          00002680
             03 MCATOTO      PIC X(6).                                  00002690
             03 FILLER       PIC X(2).                                  00002700
             03 MTRDARA      PIC X.                                     00002710
             03 MTRDARC PIC X.                                          00002720
             03 MTRDARP PIC X.                                          00002730
             03 MTRDARH PIC X.                                          00002740
             03 MTRDARV PIC X.                                          00002750
             03 MTRDARO      PIC X(6).                                  00002760
             03 FILLER       PIC X(2).                                  00002770
             03 MTRDACA      PIC X.                                     00002780
             03 MTRDACC PIC X.                                          00002790
             03 MTRDACP PIC X.                                          00002800
             03 MTRDACH PIC X.                                          00002810
             03 MTRDACV PIC X.                                          00002820
             03 MTRDACO      PIC X(6).                                  00002830
             03 FILLER       PIC X(2).                                  00002840
             03 MNBENTA      PIC X.                                     00002850
             03 MNBENTC PIC X.                                          00002860
             03 MNBENTP PIC X.                                          00002870
             03 MNBENTH PIC X.                                          00002880
             03 MNBENTV PIC X.                                          00002890
             03 MNBENTO      PIC X(6).                                  00002900
             03 FILLER       PIC X(2).                                  00002910
             03 MTXCRA  PIC X.                                          00002920
             03 MTXCRC  PIC X.                                          00002930
             03 MTXCRP  PIC X.                                          00002940
             03 MTXCRH  PIC X.                                          00002950
             03 MTXCRV  PIC X.                                          00002960
             03 MTXCRO  PIC X(5).                                       00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MZONCMDA  PIC X.                                          00002990
           02 MZONCMDC  PIC X.                                          00003000
           02 MZONCMDP  PIC X.                                          00003010
           02 MZONCMDH  PIC X.                                          00003020
           02 MZONCMDV  PIC X.                                          00003030
           02 MZONCMDO  PIC X(15).                                      00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MLIBERRA  PIC X.                                          00003060
           02 MLIBERRC  PIC X.                                          00003070
           02 MLIBERRP  PIC X.                                          00003080
           02 MLIBERRH  PIC X.                                          00003090
           02 MLIBERRV  PIC X.                                          00003100
           02 MLIBERRO  PIC X(54).                                      00003110
      * CODE TRANSACTION                                                00003120
           02 FILLER    PIC X(2).                                       00003130
           02 MCODTRAA  PIC X.                                          00003140
           02 MCODTRAC  PIC X.                                          00003150
           02 MCODTRAP  PIC X.                                          00003160
           02 MCODTRAH  PIC X.                                          00003170
           02 MCODTRAV  PIC X.                                          00003180
           02 MCODTRAO  PIC X(4).                                       00003190
      * NOM DU CICS                                                     00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MCICSA    PIC X.                                          00003220
           02 MCICSC    PIC X.                                          00003230
           02 MCICSP    PIC X.                                          00003240
           02 MCICSH    PIC X.                                          00003250
           02 MCICSV    PIC X.                                          00003260
           02 MCICSO    PIC X(5).                                       00003270
      * NETNAME                                                         00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MNETNAMA  PIC X.                                          00003300
           02 MNETNAMC  PIC X.                                          00003310
           02 MNETNAMP  PIC X.                                          00003320
           02 MNETNAMH  PIC X.                                          00003330
           02 MNETNAMV  PIC X.                                          00003340
           02 MNETNAMO  PIC X(8).                                       00003350
      * CODE TERMINAL                                                   00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MSCREENA  PIC X.                                          00003380
           02 MSCREENC  PIC X.                                          00003390
           02 MSCREENP  PIC X.                                          00003400
           02 MSCREENH  PIC X.                                          00003410
           02 MSCREENV  PIC X.                                          00003420
           02 MSCREENO  PIC X(5).                                       00003430
                                                                                
