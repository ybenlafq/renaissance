      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVLT0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLT0000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVLT0000.                                                            
           02  LT00-NLETTRE                                                     
               PIC X(0006).                                                     
           02  LT00-LIBELLE                                                     
               PIC X(0020).                                                     
           02  LT00-NLETMOD                                                     
               PIC X(0006).                                                     
           02  LT00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLT0000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLT0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LT00-NLETTRE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LT00-NLETTRE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LT00-LIBELLE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LT00-LIBELLE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LT00-NLETMOD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LT00-NLETMOD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LT00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LT00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
