      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*00730000
      * COMMAREA POUR MENU SL54 10 MAI 06                                       
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-SL54-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                  
      *--                                                                       
       01  COMM-SL54-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                
      *}                                                                        
      *----------------------------------------------------------------*00730000
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA -----------------------------------------        
           03 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS --------------------------        
           03 COMM-CICS-APPLID     PIC X(8).                                    
           03 COMM-CICS-NETNAM     PIC X(8).                                    
           03 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      ******************************************************************00000100
      **** ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION --- 100  00004200
           03 COMM-DATE-SIECLE          PIC X(02).                      00004300
           03 COMM-DATE-ANNEE           PIC X(02).                      00004400
           03 COMM-DATE-MOIS            PIC X(02).                      00004500
           03 COMM-DATE-JOUR            PIC 9(02).                      00004600
           03 COMM-DATE-QNTA            PIC 9(03).                      00004700
           03 COMM-DATE-QNT0            PIC 9(05).                      00004800
           03 COMM-DATE-BISX            PIC 9(01).                      00004900
           03 COMM-DATE-JSM             PIC 9(01).                      00005000
           03 COMM-DATE-JSM-LC          PIC X(03).                      00005100
           03 COMM-DATE-JSM-LL          PIC X(08).                      00005200
           03 COMM-DATE-MOIS-LC         PIC X(03).                      00005300
           03 COMM-DATE-MOIS-LL         PIC X(08).                      00005400
           03 COMM-DATE-SSAAMMJJ        PIC X(08).                      00005500
           03 COMM-DATE-AAMMJJ          PIC X(06).                      00005600
           03 COMM-DATE-JJMMSSAA        PIC X(08).                      00005700
           03 COMM-DATE-JJMMAA          PIC X(06).                      00005800
           03 COMM-DATE-JJ-MM-AA        PIC X(08).                      00005900
           03 COMM-DATE-JJ-MM-SSAA      PIC X(10).                      00006000
           03 COMM-DATE-WEEK.                                           00006100
              05 COMM-DATE-SEMSS              PIC 9(02).                00006200
              05 COMM-DATE-SEMAA              PIC 9(02).                00006300
              05 COMM-DATE-SEMNU              PIC 9(02).                00006400
           03 COMM-DATE-FILLER          PIC X(08).                      00006500
      *                                                                 00006600
      * ZONES RESERVEES TRAITEMENT DU SWAP                              00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00700000
      *                                                                         
      *--                                                                       
           03 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
                                                                        00680000
      *}                                                                        
      * ZONES RESERVEES APPLICATIVES -------------                      00730000
           03 COMM-SL54-NOPAGE          PIC 9(03).                              
           03 COMM-SL54-NBPAGE          PIC 9(03).                              
           03 COMM-SL54-CODERET         PIC 9(04).                              
           03 COMM-SL54-NSEQNQ          PIC 9(05).                              
           03 COMM-SL54-CPROG           PIC X(05).                              
           03 COMM-SL54-CMARQ           PIC X(05).                              
           03 COMM-SL54-CFAM            PIC X(05).                              
           03 COMM-SL54-MLREF           PIC X(20).                              
           03 COMM-SL54-STA             PIC X(03).                              
           03 COMM-SL54-NOM             PIC X(25).                              
           03 COMM-SL54-PRENOM          PIC X(15).                              
           03 COMM-SL54.                                                        
              05 COMM-SL54-CRITERES.                                            
                 10 COMM-SL54-NSOCIETE   PIC X(03).                             
                 10 COMM-SL54-NLIEU      PIC X(03).                             
                 10 COMM-SL54-NCODIC     PIC X(07).                             
                 10 COMM-SL54-NVENTE     PIC X(07).                             
                 10 COMM-SL54-CODACT     PIC X(01).                             
                 10 COMM-SL54-COMGV80    PIC X(01).                             
                 10 COMM-SL54-NENVOI     PIC X(01).                             
                 10 COMM-SL54-CSTATUT    PIC X(01).                             
      *                                                                         
           03 COMM-SLXX.                                                        
              05 COMM-SL54-SAUVEGARDE.                                          
                 10 COMM-SLXX-NSOCIETE   PIC X(03).                             
                 10 COMM-SLXX-NLIEU      PIC X(03).                             
                 10 COMM-SLXX-NCODIC     PIC X(07).                             
                 10 COMM-SLXX-NVENTE     PIC X(07).                             
                 10 COMM-SLXX-CODACT     PIC X(01).                             
                 10 COMM-SLXX-NENVOI     PIC X(01).                             
                 10 COMM-SLXX-CSTATUT    PIC X(01).                             
MH0906           10 COMM-SLXX-DATD       PIC X(08).                             
  "              10 COMM-SLXX-DATF       PIC X(08).                             
  "              10 COMM-SLXX-OPTION     PIC X(01).                             
  "              10 COMM-SLXX-PGAPP      PIC X(05).                             
                 10 COMM-SLXX-NBLIGNE    PIC 9(05).                             
                 10 COMM-SLXX-NOM-TS     PIC X(08).                             
  "           05 COMM-SL56-SAUVEGARDE.                                          
  "              10 COMM-SL56-NOPAGE     PIC 9(03).                             
  "              10 COMM-SL56-NBPAGE     PIC 9(03).                             
  "           05 COMM-SL57-SAUVEGARDE.                                          
  "              10 COMM-SL57-NOPAGE     PIC 9(03).                             
MH0906           10 COMM-SL57-NBPAGE     PIC 9(03).                             
                 10 COMM-SL57-CODACT     PIC X(01).                             
                 10 COMM-SL57-NCODIC     PIC X(07).                             
                 10 COMM-SL57-CMARQ      PIC X(05).                             
                 10 COMM-SL57-CFAM       PIC X(05).                             
                 10 COMM-SL57-MLREF      PIC X(20).                             
                 10 COMM-SL57-MAJ-AUTOR  PIC X(01).                             
      *                                                                         
      * FILLER ---------------------------------------------------------        
                                                                                
