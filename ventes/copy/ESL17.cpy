      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESL17   ESL17                                              00000020
      ***************************************************************** 00000030
       01   ESL17I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEML   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEMF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCCREL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSOCCREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCCREF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSOCCREI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUCREL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLIEUCREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIEUCREF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLIEUCREI      PIC X(3).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPDOCL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MTYPDOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPDOCF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MTYPDOCI  PIC X(2).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMDOCL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNUMDOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUMDOCF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNUMDOCI  PIC X(7).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEXPL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDEXPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDEXPF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDEXPI    PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCEXPL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MSOCEXPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCEXPF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSOCEXPI  PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUEXPL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MLIEUEXPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIEUEXPF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLIEUEXPI      PIC X(3).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBEXPL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLIBEXPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBEXPF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLIBEXPI  PIC X(20).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCRECL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MSOCRECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCRECF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSOCRECI  PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEURECL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MLIEURECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIEURECF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLIEURECI      PIC X(3).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBRECL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLIBRECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBRECF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLIBRECI  PIC X(20).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCODICL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODICF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCODICI   PIC X(7).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCFAMI    PIC X(5).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCMARQI   PIC X(5).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREFERL   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MREFERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MREFERF   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MREFERI   PIC X(20).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQEXPL    COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MQEXPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQEXPF    PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MQEXPI    PIC X(5).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQRECL    COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MQRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQRECF    PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MQRECI    PIC X(5).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQARBL    COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MQARBL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQARBF    PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MQARBI    PIC X(5).                                       00000930
           02 MLIGD OCCURS   12 TIMES .                                 00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGL   COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MLIGL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MLIGF   PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MLIGI   PIC X(75).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MLIBERRI  PIC X(78).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCODTRAI  PIC X(4).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCICSI    PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MNETNAMI  PIC X(8).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MSCREENI  PIC X(4).                                       00001180
      ***************************************************************** 00001190
      * SDF: ESL17   ESL17                                              00001200
      ***************************************************************** 00001210
       01   ESL17O REDEFINES ESL17I.                                    00001220
           02 FILLER    PIC X(12).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MDATJOUA  PIC X.                                          00001250
           02 MDATJOUC  PIC X.                                          00001260
           02 MDATJOUP  PIC X.                                          00001270
           02 MDATJOUH  PIC X.                                          00001280
           02 MDATJOUV  PIC X.                                          00001290
           02 MDATJOUO  PIC X(10).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MTIMJOUA  PIC X.                                          00001320
           02 MTIMJOUC  PIC X.                                          00001330
           02 MTIMJOUP  PIC X.                                          00001340
           02 MTIMJOUH  PIC X.                                          00001350
           02 MTIMJOUV  PIC X.                                          00001360
           02 MTIMJOUO  PIC X(5).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MPAGEA    PIC X.                                          00001390
           02 MPAGEC    PIC X.                                          00001400
           02 MPAGEP    PIC X.                                          00001410
           02 MPAGEH    PIC X.                                          00001420
           02 MPAGEV    PIC X.                                          00001430
           02 MPAGEO    PIC X(3).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MPAGEMA   PIC X.                                          00001460
           02 MPAGEMC   PIC X.                                          00001470
           02 MPAGEMP   PIC X.                                          00001480
           02 MPAGEMH   PIC X.                                          00001490
           02 MPAGEMV   PIC X.                                          00001500
           02 MPAGEMO   PIC X(3).                                       00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MSOCCREA  PIC X.                                          00001530
           02 MSOCCREC  PIC X.                                          00001540
           02 MSOCCREP  PIC X.                                          00001550
           02 MSOCCREH  PIC X.                                          00001560
           02 MSOCCREV  PIC X.                                          00001570
           02 MSOCCREO  PIC X(3).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MLIEUCREA      PIC X.                                     00001600
           02 MLIEUCREC PIC X.                                          00001610
           02 MLIEUCREP PIC X.                                          00001620
           02 MLIEUCREH PIC X.                                          00001630
           02 MLIEUCREV PIC X.                                          00001640
           02 MLIEUCREO      PIC X(3).                                  00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MTYPDOCA  PIC X.                                          00001670
           02 MTYPDOCC  PIC X.                                          00001680
           02 MTYPDOCP  PIC X.                                          00001690
           02 MTYPDOCH  PIC X.                                          00001700
           02 MTYPDOCV  PIC X.                                          00001710
           02 MTYPDOCO  PIC X(2).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MNUMDOCA  PIC X.                                          00001740
           02 MNUMDOCC  PIC X.                                          00001750
           02 MNUMDOCP  PIC X.                                          00001760
           02 MNUMDOCH  PIC X.                                          00001770
           02 MNUMDOCV  PIC X.                                          00001780
           02 MNUMDOCO  PIC X(7).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MDEXPA    PIC X.                                          00001810
           02 MDEXPC    PIC X.                                          00001820
           02 MDEXPP    PIC X.                                          00001830
           02 MDEXPH    PIC X.                                          00001840
           02 MDEXPV    PIC X.                                          00001850
           02 MDEXPO    PIC X(5).                                       00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MSOCEXPA  PIC X.                                          00001880
           02 MSOCEXPC  PIC X.                                          00001890
           02 MSOCEXPP  PIC X.                                          00001900
           02 MSOCEXPH  PIC X.                                          00001910
           02 MSOCEXPV  PIC X.                                          00001920
           02 MSOCEXPO  PIC X(3).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MLIEUEXPA      PIC X.                                     00001950
           02 MLIEUEXPC PIC X.                                          00001960
           02 MLIEUEXPP PIC X.                                          00001970
           02 MLIEUEXPH PIC X.                                          00001980
           02 MLIEUEXPV PIC X.                                          00001990
           02 MLIEUEXPO      PIC X(3).                                  00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MLIBEXPA  PIC X.                                          00002020
           02 MLIBEXPC  PIC X.                                          00002030
           02 MLIBEXPP  PIC X.                                          00002040
           02 MLIBEXPH  PIC X.                                          00002050
           02 MLIBEXPV  PIC X.                                          00002060
           02 MLIBEXPO  PIC X(20).                                      00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MSOCRECA  PIC X.                                          00002090
           02 MSOCRECC  PIC X.                                          00002100
           02 MSOCRECP  PIC X.                                          00002110
           02 MSOCRECH  PIC X.                                          00002120
           02 MSOCRECV  PIC X.                                          00002130
           02 MSOCRECO  PIC X(3).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MLIEURECA      PIC X.                                     00002160
           02 MLIEURECC PIC X.                                          00002170
           02 MLIEURECP PIC X.                                          00002180
           02 MLIEURECH PIC X.                                          00002190
           02 MLIEURECV PIC X.                                          00002200
           02 MLIEURECO      PIC X(3).                                  00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MLIBRECA  PIC X.                                          00002230
           02 MLIBRECC  PIC X.                                          00002240
           02 MLIBRECP  PIC X.                                          00002250
           02 MLIBRECH  PIC X.                                          00002260
           02 MLIBRECV  PIC X.                                          00002270
           02 MLIBRECO  PIC X(20).                                      00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MCODICA   PIC X.                                          00002300
           02 MCODICC   PIC X.                                          00002310
           02 MCODICP   PIC X.                                          00002320
           02 MCODICH   PIC X.                                          00002330
           02 MCODICV   PIC X.                                          00002340
           02 MCODICO   PIC X(7).                                       00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MCFAMA    PIC X.                                          00002370
           02 MCFAMC    PIC X.                                          00002380
           02 MCFAMP    PIC X.                                          00002390
           02 MCFAMH    PIC X.                                          00002400
           02 MCFAMV    PIC X.                                          00002410
           02 MCFAMO    PIC X(5).                                       00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MCMARQA   PIC X.                                          00002440
           02 MCMARQC   PIC X.                                          00002450
           02 MCMARQP   PIC X.                                          00002460
           02 MCMARQH   PIC X.                                          00002470
           02 MCMARQV   PIC X.                                          00002480
           02 MCMARQO   PIC X(5).                                       00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MREFERA   PIC X.                                          00002510
           02 MREFERC   PIC X.                                          00002520
           02 MREFERP   PIC X.                                          00002530
           02 MREFERH   PIC X.                                          00002540
           02 MREFERV   PIC X.                                          00002550
           02 MREFERO   PIC X(20).                                      00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MQEXPA    PIC X.                                          00002580
           02 MQEXPC    PIC X.                                          00002590
           02 MQEXPP    PIC X.                                          00002600
           02 MQEXPH    PIC X.                                          00002610
           02 MQEXPV    PIC X.                                          00002620
           02 MQEXPO    PIC X(5).                                       00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MQRECA    PIC X.                                          00002650
           02 MQRECC    PIC X.                                          00002660
           02 MQRECP    PIC X.                                          00002670
           02 MQRECH    PIC X.                                          00002680
           02 MQRECV    PIC X.                                          00002690
           02 MQRECO    PIC X(5).                                       00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MQARBA    PIC X.                                          00002720
           02 MQARBC    PIC X.                                          00002730
           02 MQARBP    PIC X.                                          00002740
           02 MQARBH    PIC X.                                          00002750
           02 MQARBV    PIC X.                                          00002760
           02 MQARBO    PIC X(5).                                       00002770
           02 DFHMS1 OCCURS   12 TIMES .                                00002780
             03 FILLER       PIC X(2).                                  00002790
             03 MLIGA   PIC X.                                          00002800
             03 MLIGC   PIC X.                                          00002810
             03 MLIGP   PIC X.                                          00002820
             03 MLIGH   PIC X.                                          00002830
             03 MLIGV   PIC X.                                          00002840
             03 MLIGO   PIC X(75).                                      00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLIBERRA  PIC X.                                          00002870
           02 MLIBERRC  PIC X.                                          00002880
           02 MLIBERRP  PIC X.                                          00002890
           02 MLIBERRH  PIC X.                                          00002900
           02 MLIBERRV  PIC X.                                          00002910
           02 MLIBERRO  PIC X(78).                                      00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MCODTRAA  PIC X.                                          00002940
           02 MCODTRAC  PIC X.                                          00002950
           02 MCODTRAP  PIC X.                                          00002960
           02 MCODTRAH  PIC X.                                          00002970
           02 MCODTRAV  PIC X.                                          00002980
           02 MCODTRAO  PIC X(4).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCICSA    PIC X.                                          00003010
           02 MCICSC    PIC X.                                          00003020
           02 MCICSP    PIC X.                                          00003030
           02 MCICSH    PIC X.                                          00003040
           02 MCICSV    PIC X.                                          00003050
           02 MCICSO    PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MNETNAMA  PIC X.                                          00003080
           02 MNETNAMC  PIC X.                                          00003090
           02 MNETNAMP  PIC X.                                          00003100
           02 MNETNAMH  PIC X.                                          00003110
           02 MNETNAMV  PIC X.                                          00003120
           02 MNETNAMO  PIC X(8).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MSCREENA  PIC X.                                          00003150
           02 MSCREENC  PIC X.                                          00003160
           02 MSCREENP  PIC X.                                          00003170
           02 MSCREENH  PIC X.                                          00003180
           02 MSCREENV  PIC X.                                          00003190
           02 MSCREENO  PIC X(4).                                       00003200
                                                                                
