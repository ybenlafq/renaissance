      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHV55   EHV55                                              00000020
      ***************************************************************** 00000030
       01   EHV55I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOPAGEL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNOPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOPAGEF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNOPAGEI  PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGEL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNBPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPAGEF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPAGEI  PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSOCL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSOCF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSOCI     PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAGL     COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MMAGL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MMAGF     PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MMAGI     PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNVENTEI  PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETATL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLETATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLETATF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLETATI   PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVENTEL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDVENTEF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDVENTEI  PIC X(8).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMCL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNOMCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNOMCF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNOMCI    PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMCL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLNOMCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLNOMCF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLNOMCI   PIC X(25).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPNOMCL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLPNOMCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPNOMCF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLPNOMCI  PIC X(15).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVOIECL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNVOIECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVOIECF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNVOIECI  PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTPVOIECL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MTPVOIECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTPVOIECF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MTPVOIECI      PIC X(4).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVOIECL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLVOIECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLVOIECF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLVOIECI  PIC X(20).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCBATCL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCBATCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCBATCF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCBATCI   PIC X(3).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCESCCL   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCESCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCESCCF   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCESCCI   PIC X(3).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCETAGCL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCETAGCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCETAGCF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCETAGCI  PIC X(3).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPORTECL      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MCPORTECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPORTECF      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCPORTECI      PIC X(3).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLADDRCL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLADDRCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLADDRCF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLADDRCI  PIC X(32).                                      00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTCL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCPOSTCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPOSTCF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCPOSTCI  PIC X(5).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCOMNCL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCCOMNCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCCOMNCF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCCOMNCI  PIC X(32).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPOSTCL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MLPOSTCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPOSTCF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MLPOSTCI  PIC X(26).                                      00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD10L  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MTELD10L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD10F  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MTELD10I  PIC X(2).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD11L  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MTELD11L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD11F  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MTELD11I  PIC X(2).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD12L  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MTELD12L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD12F  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MTELD12I  PIC X(2).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD13L  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MTELD13L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD13F  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MTELD13I  PIC X(2).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD14L  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MTELD14L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD14F  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MTELD14I  PIC X(2).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB10L  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MTELB10L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB10F  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MTELB10I  PIC X(2).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB11L  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MTELB11L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB11F  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MTELB11I  PIC X(2).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB12L  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MTELB12L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB12F  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MTELB12I  PIC X(2).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB13L  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MTELB13L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB13F  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MTELB13I  PIC X(2).                                       00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB14L  COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MTELB14L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB14F  PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MTELB14I  PIC X(2).                                       00001370
           02 MARCHI .                                                  00001380
               04 MLARCHIVED OCCURS   7 TIMES .                         00001390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 MLARCHIVEL    COMP PIC S9(4).                       00001400
      *--                                                                       
                 05 MLARCHIVEL COMP-5 PIC S9(4).                                
      *}                                                                        
                 05 MLARCHIVEF    PIC X.                                00001410
                 05 FILLER   PIC X(4).                                  00001420
                 05 MLARCHIVEI    PIC X(17).                            00001430
               04 MNPSED OCCURS   7 TIMES .                             00001440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 MNPSEL   COMP PIC S9(4).                            00001450
      *--                                                                       
                 05 MNPSEL COMP-5 PIC S9(4).                                    
      *}                                                                        
                 05 MNPSEF   PIC X.                                     00001460
                 05 FILLER   PIC X(4).                                  00001470
                 05 MNPSEI   PIC X(8).                                  00001480
               04 MNSOCIETED OCCURS   7 TIMES .                         00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 MNSOCIETEL    COMP PIC S9(4).                       00001500
      *--                                                                       
                 05 MNSOCIETEL COMP-5 PIC S9(4).                                
      *}                                                                        
                 05 MNSOCIETEF    PIC X.                                00001510
                 05 FILLER   PIC X(4).                                  00001520
                 05 MNSOCIETEI    PIC X(3).                             00001530
               04 MNSERVICED OCCURS   7 TIMES .                         00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 MNSERVICEL    COMP PIC S9(4).                       00001550
      *--                                                                       
                 05 MNSERVICEL COMP-5 PIC S9(4).                                
      *}                                                                        
                 05 MNSERVICEF    PIC X.                                00001560
                 05 FILLER   PIC X(4).                                  00001570
                 05 MNSERVICEI    PIC X(4).                             00001580
               04 MNBOXD OCCURS   7 TIMES .                             00001590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 MNBOXL   COMP PIC S9(4).                            00001600
      *--                                                                       
                 05 MNBOXL COMP-5 PIC S9(4).                                    
      *}                                                                        
                 05 MNBOXF   PIC X.                                     00001610
                 05 FILLER   PIC X(4).                                  00001620
                 05 MNBOXI   PIC X(5).                                  00001630
               04 MNBOITED OCCURS   7 TIMES .                           00001640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 MNBOITEL      COMP PIC S9(4).                       00001650
      *--                                                                       
                 05 MNBOITEL COMP-5 PIC S9(4).                                  
      *}                                                                        
                 05 MNBOITEF      PIC X.                                00001660
                 05 FILLER   PIC X(4).                                  00001670
                 05 MNBOITEI      PIC X(6).                             00001680
               04 MNFICHED OCCURS   7 TIMES .                           00001690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 MNFICHEL      COMP PIC S9(4).                       00001700
      *--                                                                       
                 05 MNFICHEL COMP-5 PIC S9(4).                                  
      *}                                                                        
                 05 MNFICHEF      PIC X.                                00001710
                 05 FILLER   PIC X(4).                                  00001720
                 05 MNFICHEI      PIC X(3).                             00001730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001740
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001750
           02 FILLER    PIC X(4).                                       00001760
           02 MLIBERRI  PIC X(78).                                      00001770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001780
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001790
           02 FILLER    PIC X(4).                                       00001800
           02 MCODTRAI  PIC X(4).                                       00001810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001820
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001830
           02 FILLER    PIC X(4).                                       00001840
           02 MCICSI    PIC X(5).                                       00001850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001860
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001870
           02 FILLER    PIC X(4).                                       00001880
           02 MNETNAMI  PIC X(8).                                       00001890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001900
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001910
           02 FILLER    PIC X(4).                                       00001920
           02 MSCREENI  PIC X(4).                                       00001930
      ***************************************************************** 00001940
      * SDF: EHV55   EHV55                                              00001950
      ***************************************************************** 00001960
       01   EHV55O REDEFINES EHV55I.                                    00001970
           02 FILLER    PIC X(12).                                      00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MDATJOUA  PIC X.                                          00002000
           02 MDATJOUC  PIC X.                                          00002010
           02 MDATJOUP  PIC X.                                          00002020
           02 MDATJOUH  PIC X.                                          00002030
           02 MDATJOUV  PIC X.                                          00002040
           02 MDATJOUO  PIC X(10).                                      00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MTIMJOUA  PIC X.                                          00002070
           02 MTIMJOUC  PIC X.                                          00002080
           02 MTIMJOUP  PIC X.                                          00002090
           02 MTIMJOUH  PIC X.                                          00002100
           02 MTIMJOUV  PIC X.                                          00002110
           02 MTIMJOUO  PIC X(5).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MNOPAGEA  PIC X.                                          00002140
           02 MNOPAGEC  PIC X.                                          00002150
           02 MNOPAGEP  PIC X.                                          00002160
           02 MNOPAGEH  PIC X.                                          00002170
           02 MNOPAGEV  PIC X.                                          00002180
           02 MNOPAGEO  PIC X(3).                                       00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MNBPAGEA  PIC X.                                          00002210
           02 MNBPAGEC  PIC X.                                          00002220
           02 MNBPAGEP  PIC X.                                          00002230
           02 MNBPAGEH  PIC X.                                          00002240
           02 MNBPAGEV  PIC X.                                          00002250
           02 MNBPAGEO  PIC X(3).                                       00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MSOCA     PIC X.                                          00002280
           02 MSOCC     PIC X.                                          00002290
           02 MSOCP     PIC X.                                          00002300
           02 MSOCH     PIC X.                                          00002310
           02 MSOCV     PIC X.                                          00002320
           02 MSOCO     PIC X(3).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MMAGA     PIC X.                                          00002350
           02 MMAGC     PIC X.                                          00002360
           02 MMAGP     PIC X.                                          00002370
           02 MMAGH     PIC X.                                          00002380
           02 MMAGV     PIC X.                                          00002390
           02 MMAGO     PIC X(3).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNVENTEA  PIC X.                                          00002420
           02 MNVENTEC  PIC X.                                          00002430
           02 MNVENTEP  PIC X.                                          00002440
           02 MNVENTEH  PIC X.                                          00002450
           02 MNVENTEV  PIC X.                                          00002460
           02 MNVENTEO  PIC X(7).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MLETATA   PIC X.                                          00002490
           02 MLETATC   PIC X.                                          00002500
           02 MLETATP   PIC X.                                          00002510
           02 MLETATH   PIC X.                                          00002520
           02 MLETATV   PIC X.                                          00002530
           02 MLETATO   PIC X(20).                                      00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MDVENTEA  PIC X.                                          00002560
           02 MDVENTEC  PIC X.                                          00002570
           02 MDVENTEP  PIC X.                                          00002580
           02 MDVENTEH  PIC X.                                          00002590
           02 MDVENTEV  PIC X.                                          00002600
           02 MDVENTEO  PIC X(8).                                       00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MNOMCA    PIC X.                                          00002630
           02 MNOMCC    PIC X.                                          00002640
           02 MNOMCP    PIC X.                                          00002650
           02 MNOMCH    PIC X.                                          00002660
           02 MNOMCV    PIC X.                                          00002670
           02 MNOMCO    PIC X(5).                                       00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MLNOMCA   PIC X.                                          00002700
           02 MLNOMCC   PIC X.                                          00002710
           02 MLNOMCP   PIC X.                                          00002720
           02 MLNOMCH   PIC X.                                          00002730
           02 MLNOMCV   PIC X.                                          00002740
           02 MLNOMCO   PIC X(25).                                      00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MLPNOMCA  PIC X.                                          00002770
           02 MLPNOMCC  PIC X.                                          00002780
           02 MLPNOMCP  PIC X.                                          00002790
           02 MLPNOMCH  PIC X.                                          00002800
           02 MLPNOMCV  PIC X.                                          00002810
           02 MLPNOMCO  PIC X(15).                                      00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MNVOIECA  PIC X.                                          00002840
           02 MNVOIECC  PIC X.                                          00002850
           02 MNVOIECP  PIC X.                                          00002860
           02 MNVOIECH  PIC X.                                          00002870
           02 MNVOIECV  PIC X.                                          00002880
           02 MNVOIECO  PIC X(5).                                       00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MTPVOIECA      PIC X.                                     00002910
           02 MTPVOIECC PIC X.                                          00002920
           02 MTPVOIECP PIC X.                                          00002930
           02 MTPVOIECH PIC X.                                          00002940
           02 MTPVOIECV PIC X.                                          00002950
           02 MTPVOIECO      PIC X(4).                                  00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MLVOIECA  PIC X.                                          00002980
           02 MLVOIECC  PIC X.                                          00002990
           02 MLVOIECP  PIC X.                                          00003000
           02 MLVOIECH  PIC X.                                          00003010
           02 MLVOIECV  PIC X.                                          00003020
           02 MLVOIECO  PIC X(20).                                      00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MCBATCA   PIC X.                                          00003050
           02 MCBATCC   PIC X.                                          00003060
           02 MCBATCP   PIC X.                                          00003070
           02 MCBATCH   PIC X.                                          00003080
           02 MCBATCV   PIC X.                                          00003090
           02 MCBATCO   PIC X(3).                                       00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MCESCCA   PIC X.                                          00003120
           02 MCESCCC   PIC X.                                          00003130
           02 MCESCCP   PIC X.                                          00003140
           02 MCESCCH   PIC X.                                          00003150
           02 MCESCCV   PIC X.                                          00003160
           02 MCESCCO   PIC X(3).                                       00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MCETAGCA  PIC X.                                          00003190
           02 MCETAGCC  PIC X.                                          00003200
           02 MCETAGCP  PIC X.                                          00003210
           02 MCETAGCH  PIC X.                                          00003220
           02 MCETAGCV  PIC X.                                          00003230
           02 MCETAGCO  PIC X(3).                                       00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MCPORTECA      PIC X.                                     00003260
           02 MCPORTECC PIC X.                                          00003270
           02 MCPORTECP PIC X.                                          00003280
           02 MCPORTECH PIC X.                                          00003290
           02 MCPORTECV PIC X.                                          00003300
           02 MCPORTECO      PIC X(3).                                  00003310
           02 FILLER    PIC X(2).                                       00003320
           02 MLADDRCA  PIC X.                                          00003330
           02 MLADDRCC  PIC X.                                          00003340
           02 MLADDRCP  PIC X.                                          00003350
           02 MLADDRCH  PIC X.                                          00003360
           02 MLADDRCV  PIC X.                                          00003370
           02 MLADDRCO  PIC X(32).                                      00003380
           02 FILLER    PIC X(2).                                       00003390
           02 MCPOSTCA  PIC X.                                          00003400
           02 MCPOSTCC  PIC X.                                          00003410
           02 MCPOSTCP  PIC X.                                          00003420
           02 MCPOSTCH  PIC X.                                          00003430
           02 MCPOSTCV  PIC X.                                          00003440
           02 MCPOSTCO  PIC X(5).                                       00003450
           02 FILLER    PIC X(2).                                       00003460
           02 MCCOMNCA  PIC X.                                          00003470
           02 MCCOMNCC  PIC X.                                          00003480
           02 MCCOMNCP  PIC X.                                          00003490
           02 MCCOMNCH  PIC X.                                          00003500
           02 MCCOMNCV  PIC X.                                          00003510
           02 MCCOMNCO  PIC X(32).                                      00003520
           02 FILLER    PIC X(2).                                       00003530
           02 MLPOSTCA  PIC X.                                          00003540
           02 MLPOSTCC  PIC X.                                          00003550
           02 MLPOSTCP  PIC X.                                          00003560
           02 MLPOSTCH  PIC X.                                          00003570
           02 MLPOSTCV  PIC X.                                          00003580
           02 MLPOSTCO  PIC X(26).                                      00003590
           02 FILLER    PIC X(2).                                       00003600
           02 MTELD10A  PIC X.                                          00003610
           02 MTELD10C  PIC X.                                          00003620
           02 MTELD10P  PIC X.                                          00003630
           02 MTELD10H  PIC X.                                          00003640
           02 MTELD10V  PIC X.                                          00003650
           02 MTELD10O  PIC X(2).                                       00003660
           02 FILLER    PIC X(2).                                       00003670
           02 MTELD11A  PIC X.                                          00003680
           02 MTELD11C  PIC X.                                          00003690
           02 MTELD11P  PIC X.                                          00003700
           02 MTELD11H  PIC X.                                          00003710
           02 MTELD11V  PIC X.                                          00003720
           02 MTELD11O  PIC X(2).                                       00003730
           02 FILLER    PIC X(2).                                       00003740
           02 MTELD12A  PIC X.                                          00003750
           02 MTELD12C  PIC X.                                          00003760
           02 MTELD12P  PIC X.                                          00003770
           02 MTELD12H  PIC X.                                          00003780
           02 MTELD12V  PIC X.                                          00003790
           02 MTELD12O  PIC X(2).                                       00003800
           02 FILLER    PIC X(2).                                       00003810
           02 MTELD13A  PIC X.                                          00003820
           02 MTELD13C  PIC X.                                          00003830
           02 MTELD13P  PIC X.                                          00003840
           02 MTELD13H  PIC X.                                          00003850
           02 MTELD13V  PIC X.                                          00003860
           02 MTELD13O  PIC X(2).                                       00003870
           02 FILLER    PIC X(2).                                       00003880
           02 MTELD14A  PIC X.                                          00003890
           02 MTELD14C  PIC X.                                          00003900
           02 MTELD14P  PIC X.                                          00003910
           02 MTELD14H  PIC X.                                          00003920
           02 MTELD14V  PIC X.                                          00003930
           02 MTELD14O  PIC X(2).                                       00003940
           02 FILLER    PIC X(2).                                       00003950
           02 MTELB10A  PIC X.                                          00003960
           02 MTELB10C  PIC X.                                          00003970
           02 MTELB10P  PIC X.                                          00003980
           02 MTELB10H  PIC X.                                          00003990
           02 MTELB10V  PIC X.                                          00004000
           02 MTELB10O  PIC X(2).                                       00004010
           02 FILLER    PIC X(2).                                       00004020
           02 MTELB11A  PIC X.                                          00004030
           02 MTELB11C  PIC X.                                          00004040
           02 MTELB11P  PIC X.                                          00004050
           02 MTELB11H  PIC X.                                          00004060
           02 MTELB11V  PIC X.                                          00004070
           02 MTELB11O  PIC X(2).                                       00004080
           02 FILLER    PIC X(2).                                       00004090
           02 MTELB12A  PIC X.                                          00004100
           02 MTELB12C  PIC X.                                          00004110
           02 MTELB12P  PIC X.                                          00004120
           02 MTELB12H  PIC X.                                          00004130
           02 MTELB12V  PIC X.                                          00004140
           02 MTELB12O  PIC X(2).                                       00004150
           02 FILLER    PIC X(2).                                       00004160
           02 MTELB13A  PIC X.                                          00004170
           02 MTELB13C  PIC X.                                          00004180
           02 MTELB13P  PIC X.                                          00004190
           02 MTELB13H  PIC X.                                          00004200
           02 MTELB13V  PIC X.                                          00004210
           02 MTELB13O  PIC X(2).                                       00004220
           02 FILLER    PIC X(2).                                       00004230
           02 MTELB14A  PIC X.                                          00004240
           02 MTELB14C  PIC X.                                          00004250
           02 MTELB14P  PIC X.                                          00004260
           02 MTELB14H  PIC X.                                          00004270
           02 MTELB14V  PIC X.                                          00004280
           02 MTELB14O  PIC X(2).                                       00004290
           02 MARCHO .                                                  00004300
               04 DFHMS1 OCCURS   7 TIMES .                             00004310
                 05 FILLER   PIC X(2).                                  00004320
                 05 MLARCHIVEA    PIC X.                                00004330
                 05 MLARCHIVEC    PIC X.                                00004340
                 05 MLARCHIVEP    PIC X.                                00004350
                 05 MLARCHIVEH    PIC X.                                00004360
                 05 MLARCHIVEV    PIC X.                                00004370
                 05 MLARCHIVEO    PIC X(17).                            00004380
               04 DFHMS2 OCCURS   7 TIMES .                             00004390
                 05 FILLER   PIC X(2).                                  00004400
                 05 MNPSEA   PIC X.                                     00004410
                 05 MNPSEC   PIC X.                                     00004420
                 05 MNPSEP   PIC X.                                     00004430
                 05 MNPSEH   PIC X.                                     00004440
                 05 MNPSEV   PIC X.                                     00004450
                 05 MNPSEO   PIC X(8).                                  00004460
               04 DFHMS3 OCCURS   7 TIMES .                             00004470
                 05 FILLER   PIC X(2).                                  00004480
                 05 MNSOCIETEA    PIC X.                                00004490
                 05 MNSOCIETEC    PIC X.                                00004500
                 05 MNSOCIETEP    PIC X.                                00004510
                 05 MNSOCIETEH    PIC X.                                00004520
                 05 MNSOCIETEV    PIC X.                                00004530
                 05 MNSOCIETEO    PIC X(3).                             00004540
               04 DFHMS4 OCCURS   7 TIMES .                             00004550
                 05 FILLER   PIC X(2).                                  00004560
                 05 MNSERVICEA    PIC X.                                00004570
                 05 MNSERVICEC    PIC X.                                00004580
                 05 MNSERVICEP    PIC X.                                00004590
                 05 MNSERVICEH    PIC X.                                00004600
                 05 MNSERVICEV    PIC X.                                00004610
                 05 MNSERVICEO    PIC X(4).                             00004620
               04 DFHMS5 OCCURS   7 TIMES .                             00004630
                 05 FILLER   PIC X(2).                                  00004640
                 05 MNBOXA   PIC X.                                     00004650
                 05 MNBOXC   PIC X.                                     00004660
                 05 MNBOXP   PIC X.                                     00004670
                 05 MNBOXH   PIC X.                                     00004680
                 05 MNBOXV   PIC X.                                     00004690
                 05 MNBOXO   PIC X(5).                                  00004700
               04 DFHMS6 OCCURS   7 TIMES .                             00004710
                 05 FILLER   PIC X(2).                                  00004720
                 05 MNBOITEA      PIC X.                                00004730
                 05 MNBOITEC PIC X.                                     00004740
                 05 MNBOITEP PIC X.                                     00004750
                 05 MNBOITEH PIC X.                                     00004760
                 05 MNBOITEV PIC X.                                     00004770
                 05 MNBOITEO      PIC X(6).                             00004780
               04 DFHMS7 OCCURS   7 TIMES .                             00004790
                 05 FILLER   PIC X(2).                                  00004800
                 05 MNFICHEA      PIC X.                                00004810
                 05 MNFICHEC PIC X.                                     00004820
                 05 MNFICHEP PIC X.                                     00004830
                 05 MNFICHEH PIC X.                                     00004840
                 05 MNFICHEV PIC X.                                     00004850
                 05 MNFICHEO      PIC X(3).                             00004860
           02 FILLER    PIC X(2).                                       00004870
           02 MLIBERRA  PIC X.                                          00004880
           02 MLIBERRC  PIC X.                                          00004890
           02 MLIBERRP  PIC X.                                          00004900
           02 MLIBERRH  PIC X.                                          00004910
           02 MLIBERRV  PIC X.                                          00004920
           02 MLIBERRO  PIC X(78).                                      00004930
           02 FILLER    PIC X(2).                                       00004940
           02 MCODTRAA  PIC X.                                          00004950
           02 MCODTRAC  PIC X.                                          00004960
           02 MCODTRAP  PIC X.                                          00004970
           02 MCODTRAH  PIC X.                                          00004980
           02 MCODTRAV  PIC X.                                          00004990
           02 MCODTRAO  PIC X(4).                                       00005000
           02 FILLER    PIC X(2).                                       00005010
           02 MCICSA    PIC X.                                          00005020
           02 MCICSC    PIC X.                                          00005030
           02 MCICSP    PIC X.                                          00005040
           02 MCICSH    PIC X.                                          00005050
           02 MCICSV    PIC X.                                          00005060
           02 MCICSO    PIC X(5).                                       00005070
           02 FILLER    PIC X(2).                                       00005080
           02 MNETNAMA  PIC X.                                          00005090
           02 MNETNAMC  PIC X.                                          00005100
           02 MNETNAMP  PIC X.                                          00005110
           02 MNETNAMH  PIC X.                                          00005120
           02 MNETNAMV  PIC X.                                          00005130
           02 MNETNAMO  PIC X(8).                                       00005140
           02 FILLER    PIC X(2).                                       00005150
           02 MSCREENA  PIC X.                                          00005160
           02 MSCREENC  PIC X.                                          00005170
           02 MSCREENP  PIC X.                                          00005180
           02 MSCREENH  PIC X.                                          00005190
           02 MSCREENV  PIC X.                                          00005200
           02 MSCREENO  PIC X(4).                                       00005210
                                                                                
