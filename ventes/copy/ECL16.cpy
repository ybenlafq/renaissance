      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * INIT DES BR (LORS DU DEMARRAGE)                                 00000020
      ***************************************************************** 00000030
       01   ECL16I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPAGESI      PIC X(2).                                  00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CHOIXL    COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 CHOIXL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 CHOIXF    PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 CHOIXI    PIC X(5).                                       00000270
           02 MLIGNESI OCCURS   14 TIMES .                              00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COPSLL  COMP PIC S9(4).                                 00000290
      *--                                                                       
             03 COPSLL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 COPSLF  PIC X.                                          00000300
             03 FILLER  PIC X(4).                                       00000310
             03 COPSLI  PIC X(5).                                       00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 LOPSLL  COMP PIC S9(4).                                 00000330
      *--                                                                       
             03 LOPSLL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 LOPSLF  PIC X.                                          00000340
             03 FILLER  PIC X(4).                                       00000350
             03 LOPSLI  PIC X(21).                                      00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 CTYPDOCL     COMP PIC S9(4).                            00000370
      *--                                                                       
             03 CTYPDOCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 CTYPDOCF     PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 CTYPDOCI     PIC X(2).                                  00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 WCONFL  COMP PIC S9(4).                                 00000410
      *--                                                                       
             03 WCONFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 WCONFF  PIC X.                                          00000420
             03 FILLER  PIC X(4).                                       00000430
             03 WCONFI  PIC X.                                          00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 CTYPRSL      COMP PIC S9(4).                            00000450
      *--                                                                       
             03 CTYPRSL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 CTYPRSF      PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 CTYPRSI      PIC X(2).                                  00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 WSUPPRSL     COMP PIC S9(4).                            00000490
      *--                                                                       
             03 WSUPPRSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 WSUPPRSF     PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 WSUPPRSI     PIC X.                                     00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 WWVNDL  COMP PIC S9(4).                                 00000530
      *--                                                                       
             03 WWVNDL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 WWVNDF  PIC X.                                          00000540
             03 FILLER  PIC X(4).                                       00000550
             03 WWVNDI  PIC X.                                          00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 WWHSL   COMP PIC S9(4).                                 00000570
      *--                                                                       
             03 WWHSL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 WWHSF   PIC X.                                          00000580
             03 FILLER  PIC X(4).                                       00000590
             03 WWHSI   PIC X.                                          00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 WWUTL   COMP PIC S9(4).                                 00000610
      *--                                                                       
             03 WWUTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 WWUTF   PIC X.                                          00000620
             03 FILLER  PIC X(4).                                       00000630
             03 WWUTI   PIC X.                                          00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 REGL    COMP PIC S9(4).                                 00000650
      *--                                                                       
             03 REGL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 REGF    PIC X.                                          00000660
             03 FILLER  PIC X(4).                                       00000670
             03 REGI    PIC X.                                          00000680
      * MESSAGE ERREUR                                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLIBERRI  PIC X(79).                                      00000730
      * CODE TRANSACTION                                                00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MZONCMDI  PIC X(15).                                      00000820
      * CICS DE TRAVAIL                                                 00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MCICSI    PIC X(5).                                       00000870
      * NETNAME                                                         00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MNETNAMI  PIC X(8).                                       00000920
      * CODE TERMINAL                                                   00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MSCREENI  PIC X(4).                                       00000970
      ***************************************************************** 00000980
      * INIT DES BR (LORS DU DEMARRAGE)                                 00000990
      ***************************************************************** 00001000
       01   ECL16O REDEFINES ECL16I.                                    00001010
           02 FILLER    PIC X(12).                                      00001020
      * DATE DU JOUR                                                    00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
      * HEURE                                                           00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MTIMJOUA  PIC X.                                          00001130
           02 MTIMJOUC  PIC X.                                          00001140
           02 MTIMJOUP  PIC X.                                          00001150
           02 MTIMJOUH  PIC X.                                          00001160
           02 MTIMJOUV  PIC X.                                          00001170
           02 MTIMJOUO  PIC X(5).                                       00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MPAGEA    PIC X.                                          00001200
           02 MPAGEC    PIC X.                                          00001210
           02 MPAGEP    PIC X.                                          00001220
           02 MPAGEH    PIC X.                                          00001230
           02 MPAGEV    PIC X.                                          00001240
           02 MPAGEO    PIC X(2).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MNBPAGESA      PIC X.                                     00001270
           02 MNBPAGESC PIC X.                                          00001280
           02 MNBPAGESP PIC X.                                          00001290
           02 MNBPAGESH PIC X.                                          00001300
           02 MNBPAGESV PIC X.                                          00001310
           02 MNBPAGESO      PIC X(2).                                  00001320
           02 FILLER    PIC X(2).                                       00001330
           02 CHOIXA    PIC X.                                          00001340
           02 CHOIXC    PIC X.                                          00001350
           02 CHOIXP    PIC X.                                          00001360
           02 CHOIXH    PIC X.                                          00001370
           02 CHOIXV    PIC X.                                          00001380
           02 CHOIXO    PIC X(5).                                       00001390
           02 MLIGNESO OCCURS   14 TIMES .                              00001400
             03 FILLER       PIC X(2).                                  00001410
             03 COPSLA  PIC X.                                          00001420
             03 COPSLC  PIC X.                                          00001430
             03 COPSLP  PIC X.                                          00001440
             03 COPSLH  PIC X.                                          00001450
             03 COPSLV  PIC X.                                          00001460
             03 COPSLO  PIC X(5).                                       00001470
             03 FILLER       PIC X(2).                                  00001480
             03 LOPSLA  PIC X.                                          00001490
             03 LOPSLC  PIC X.                                          00001500
             03 LOPSLP  PIC X.                                          00001510
             03 LOPSLH  PIC X.                                          00001520
             03 LOPSLV  PIC X.                                          00001530
             03 LOPSLO  PIC X(21).                                      00001540
             03 FILLER       PIC X(2).                                  00001550
             03 CTYPDOCA     PIC X.                                     00001560
             03 CTYPDOCC     PIC X.                                     00001570
             03 CTYPDOCP     PIC X.                                     00001580
             03 CTYPDOCH     PIC X.                                     00001590
             03 CTYPDOCV     PIC X.                                     00001600
             03 CTYPDOCO     PIC X(2).                                  00001610
             03 FILLER       PIC X(2).                                  00001620
             03 WCONFA  PIC X.                                          00001630
             03 WCONFC  PIC X.                                          00001640
             03 WCONFP  PIC X.                                          00001650
             03 WCONFH  PIC X.                                          00001660
             03 WCONFV  PIC X.                                          00001670
             03 WCONFO  PIC X.                                          00001680
             03 FILLER       PIC X(2).                                  00001690
             03 CTYPRSA      PIC X.                                     00001700
             03 CTYPRSC PIC X.                                          00001710
             03 CTYPRSP PIC X.                                          00001720
             03 CTYPRSH PIC X.                                          00001730
             03 CTYPRSV PIC X.                                          00001740
             03 CTYPRSO      PIC X(2).                                  00001750
             03 FILLER       PIC X(2).                                  00001760
             03 WSUPPRSA     PIC X.                                     00001770
             03 WSUPPRSC     PIC X.                                     00001780
             03 WSUPPRSP     PIC X.                                     00001790
             03 WSUPPRSH     PIC X.                                     00001800
             03 WSUPPRSV     PIC X.                                     00001810
             03 WSUPPRSO     PIC X.                                     00001820
             03 FILLER       PIC X(2).                                  00001830
             03 WWVNDA  PIC X.                                          00001840
             03 WWVNDC  PIC X.                                          00001850
             03 WWVNDP  PIC X.                                          00001860
             03 WWVNDH  PIC X.                                          00001870
             03 WWVNDV  PIC X.                                          00001880
             03 WWVNDO  PIC X.                                          00001890
             03 FILLER       PIC X(2).                                  00001900
             03 WWHSA   PIC X.                                          00001910
             03 WWHSC   PIC X.                                          00001920
             03 WWHSP   PIC X.                                          00001930
             03 WWHSH   PIC X.                                          00001940
             03 WWHSV   PIC X.                                          00001950
             03 WWHSO   PIC X.                                          00001960
             03 FILLER       PIC X(2).                                  00001970
             03 WWUTA   PIC X.                                          00001980
             03 WWUTC   PIC X.                                          00001990
             03 WWUTP   PIC X.                                          00002000
             03 WWUTH   PIC X.                                          00002010
             03 WWUTV   PIC X.                                          00002020
             03 WWUTO   PIC X.                                          00002030
             03 FILLER       PIC X(2).                                  00002040
             03 REGA    PIC X.                                          00002050
             03 REGC    PIC X.                                          00002060
             03 REGP    PIC X.                                          00002070
             03 REGH    PIC X.                                          00002080
             03 REGV    PIC X.                                          00002090
             03 REGO    PIC X.                                          00002100
      * MESSAGE ERREUR                                                  00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MLIBERRA  PIC X.                                          00002130
           02 MLIBERRC  PIC X.                                          00002140
           02 MLIBERRP  PIC X.                                          00002150
           02 MLIBERRH  PIC X.                                          00002160
           02 MLIBERRV  PIC X.                                          00002170
           02 MLIBERRO  PIC X(79).                                      00002180
      * CODE TRANSACTION                                                00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MCODTRAA  PIC X.                                          00002210
           02 MCODTRAC  PIC X.                                          00002220
           02 MCODTRAP  PIC X.                                          00002230
           02 MCODTRAH  PIC X.                                          00002240
           02 MCODTRAV  PIC X.                                          00002250
           02 MCODTRAO  PIC X(4).                                       00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MZONCMDA  PIC X.                                          00002280
           02 MZONCMDC  PIC X.                                          00002290
           02 MZONCMDP  PIC X.                                          00002300
           02 MZONCMDH  PIC X.                                          00002310
           02 MZONCMDV  PIC X.                                          00002320
           02 MZONCMDO  PIC X(15).                                      00002330
      * CICS DE TRAVAIL                                                 00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MCICSA    PIC X.                                          00002360
           02 MCICSC    PIC X.                                          00002370
           02 MCICSP    PIC X.                                          00002380
           02 MCICSH    PIC X.                                          00002390
           02 MCICSV    PIC X.                                          00002400
           02 MCICSO    PIC X(5).                                       00002410
      * NETNAME                                                         00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MNETNAMA  PIC X.                                          00002440
           02 MNETNAMC  PIC X.                                          00002450
           02 MNETNAMP  PIC X.                                          00002460
           02 MNETNAMH  PIC X.                                          00002470
           02 MNETNAMV  PIC X.                                          00002480
           02 MNETNAMO  PIC X(8).                                       00002490
      * CODE TERMINAL                                                   00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MSCREENA  PIC X.                                          00002520
           02 MSCREENC  PIC X.                                          00002530
           02 MSCREENP  PIC X.                                          00002540
           02 MSCREENH  PIC X.                                          00002550
           02 MSCREENV  PIC X.                                          00002560
           02 MSCREENO  PIC X(4).                                       00002570
                                                                                
