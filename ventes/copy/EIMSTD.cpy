      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREPOT        * 00020001
      * FICHIER.....: IMAGE DE STOCK                                  * 00030000
      * NOM FICHIER.: EIMSTD                                          * 00040000
      *---------------------------------------------------------------* 00050000
      * CR   .......: 29/02/2012                                      * 00060000
      * VERSION N�..: 001                                             * 00070000
      * LONGUEUR....: 143                                             * 00080001
      *---------------------------------------------------------------* 00090001
      * MODIFIE.....: 06/11/2012                                      * 00100001
      * VERSION N�..: 002                                             * 00110001
      * LONGUEUR....: 181                                             * 00120001
      ***************************************************************** 00130000
      *                                                                 00140000
       01  EIMSTD.                                                      00150000
      * TYPE ENREGISTREMENT : EMISSION IMAGE STANDARD                   00160000
1          05      EIMSTD-TYP-ENREG       PIC  X(0006).                 00170000
      * NUMERO DE LIGNE                                                 00180000
7          05      EIMSTD-NLIGNE          PIC  9(0009).                 00190000
      * NUMERO DE SITE                                                  00200000
16         05      EIMSTD-NSITE           PIC  9(0003).                 00210000
      * CODE SOCIETE                                                    00220000
19         05      EIMSTD-CSOCIETE        PIC  X(0005).                 00230000
      * CODE ARTICLE                                                    00240000
24         05      EIMSTD-CARTICLE        PIC  X(0018).                 00250000
      * NUMERO DE LOT                                                   00260000
42         05      EIMSTD-NLOT            PIC  X(0015).                 00270000
      * NUMERO DE CARTON                                                00280000
57         05      EIMSTD-NCARTON         PIC  X(0010).                 00290000
      * DATE LIMITE DE VENTE                                            00300000
67         05      EIMSTD-DLV             PIC  X(0008).                 00310000
      * DATE LIMITE DE CONSOMMATION                                     00320000
75         05      EIMSTD-DLC             PIC  X(0008).                 00330000
      * INFORMATION 1                                                   00340000
83         05      EIMSTD-INFO1           PIC  X(0010).                 00350000
      * INFORMATION 2                                                   00360001
93         05      EIMSTD-INFO2           PIC  X(0010).                 00370000
      * INFORMATION 3                                                   00380000
103        05      EIMSTD-INFO3           PIC  X(0010).                 00390000
      * ETAT DE BLOCAGE                                                 00400000
113        05      EIMSTD-ETAT-BLOCAGE    PIC  9(0003).                 00410000
      * ETAT DE STOCK N3                                                00420000
116        05      EIMSTD-ETAT-STOCK      PIC  9(0003).                 00430000
      * RESERVATION                                                     00440000
119        05      EIMSTD-RESERVATION     PIC  X(0015).                 00450000
      * QUANTITE                                                        00460000
134        05      EIMSTD-QTE             PIC  X(0009).                 00470000
      * NUMERO DE SUPPORT LM7                                           00480001
143        05      EIMSTD-NSUPPORT        PIC  X(0018).                 00490001
      * NUMERO D'EMPLACEMENT LM7                                        00500001
161        05      EIMSTD-ADRESSE         PIC  X(0020).                 00510001
      * FIN DE L'ENREGISTREMENT                                         00520001
181        05      EIMSTD-FIN             PIC  X(0001).                 00530001
                                                                                
