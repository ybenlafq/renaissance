      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVHV8301                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV8301                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHV8301.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHV8301.                                                            
      *}                                                                        
           02  HV83-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV83-NLIEU                                                       
               PIC X(0003).                                                     
           02  HV83-NSOCMAG                                                     
               PIC X(0003).                                                     
           02  HV83-NMAG                                                        
               PIC X(0003).                                                     
           02  HV83-CRAYONFAM                                                   
               PIC X(0005).                                                     
           02  HV83-CVENDEUR                                                    
               PIC X(0006).                                                     
           02  HV83-DMVENTE                                                     
               PIC X(0006).                                                     
           02  HV83-NAGREGATED                                                  
               PIC S9(5) COMP-3.                                                
           02  HV83-LAGREGATED                                                  
               PIC X(0020).                                                     
           02  HV83-QVENDD2                                                     
               PIC S9(5)V9(0002) COMP-3.                                        
           02  HV83-PCA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-PMTACHATS                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-PMTPRIMES                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-PMTCOMM1                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-PMTCOMM2                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-QNBPSE                                                      
               PIC S9(5) COMP-3.                                                
           02  HV83-PCAPSE                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-PMTPRIMPSE                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  HV83-QNBPSAB                                                     
               PIC S9(5) COMP-3.                                                
           02  HV83-QVENDD2EP                                                   
               PIC S9(5)V9(0002) COMP-3.                                        
           02  HV83-QVENDD2SV                                                   
               PIC S9(5)V9(0002) COMP-3.                                        
           02  HV83-QVENDD2CM                                                   
               PIC S9(5)V9(0002) COMP-3.                                        
           02  HV83-PCAEP                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-PCASV                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-PCACM                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-PMTACHEP                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-PMTACHSV                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-PMTACHCM                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-PMTREM                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-PMTREMEP                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-PMTCOMMEP                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-MTVA                                                        
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-MTVAEP                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-MTVASV                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV83-MTVACM                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHV8301                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHV8301-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHV8301-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-NSOCMAG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-NSOCMAG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-CRAYONFAM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-CRAYONFAM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-DMVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-DMVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-NAGREGATED-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-NAGREGATED-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-LAGREGATED-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-LAGREGATED-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-QVENDD2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-QVENDD2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-PCA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-PCA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-PMTACHATS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-PMTACHATS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-PMTPRIMES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-PMTPRIMES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-PMTCOMM1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-PMTCOMM1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-PMTCOMM2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-PMTCOMM2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-QNBPSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-QNBPSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-PCAPSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-PCAPSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-PMTPRIMPSE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-PMTPRIMPSE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-QNBPSAB-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-QNBPSAB-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-QVENDD2EP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-QVENDD2EP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-QVENDD2SV-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-QVENDD2SV-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-QVENDD2CM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-QVENDD2CM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-PCAEP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-PCAEP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-PCASV-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-PCASV-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-PCACM-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-PCACM-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-PMTACHEP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-PMTACHEP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-PMTACHSV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-PMTACHSV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-PMTACHCM-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-PMTACHCM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-PMTREM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-PMTREM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-PMTREMEP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-PMTREMEP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-PMTCOMMEP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-PMTCOMMEP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-MTVA-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-MTVA-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-MTVAEP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-MTVAEP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-MTVASV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-MTVASV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV83-MTVACM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV83-MTVACM-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
