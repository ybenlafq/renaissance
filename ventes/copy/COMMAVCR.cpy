      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : APPLICATION AVOIRS COMMERCIAUX                   *        
      *  PROGRAMMES : MAVCR                                            *        
      *  TITRE      : COMMAREA DES MODULES (TP) DE CREATION DE L'AVOIR *        
      *  LONGUEUR   :      C                                           *        
      *                                                                *        
      ******************************************************************        
       01  COMM-AVCR-APPLI.                                                     
      *--- DONNEES EN ENTREE DU MODULE ------------------------------ 12        
           05 COMM-AVCR-DONNEES-ENTREE.                                         
              10 COMM-AVCR-DATEJOUR         PIC X(08).                          
              10 COMM-AVCR-NSOCIETE         PIC X(03).                          
EMU           10 COMM-AVCR-CTYPSOC          PIC X(03).                          
              10 COMM-AVCR-NLIEU            PIC X(03).                          
DEP           10 COMM-AVCR-NSECTION         PIC X(06).                          
DEP           10 COMM-AVCR-CACID            PIC X(07).                          
DEP           10 COMM-AVCR-NCHRONO          PIC X(08).                          
              10 COMM-AVCR-NAVOIR           PIC X(7).                           
              10 COMM-AVCR-CTYPAVOIR        PIC X(5).                           
              10 COMM-AVCR-DEMIS            PIC X(8).                           
              10 COMM-AVCR-DICEMIS          PIC X(8).                           
              10 COMM-AVCR-CTYPUTIL         PIC X(5).                           
              10 COMM-AVCR-CAPPLI           PIC X(5).                           
              10 COMM-AVCR-CMOTIF           PIC X(5).                           
AJ            10 COMM-AVCR-LMOTIF           PIC X(20).                          
              10 COMM-AVCR-PMONTANT         PIC S9(5)V9(2).                     
              10 COMM-AVCR-NLIEUCOMPT       PIC X(3).                           
              10 COMM-AVCR-CIMPUTATION      PIC X(10).                          
              10 COMM-AVCR-NSOCVENTE        PIC X(3).                           
              10 COMM-AVCR-NLIEUVENTE       PIC X(3).                           
              10 COMM-AVCR-NVENTE           PIC X(7).                           
              10 COMM-AVCR-DCAISSE          PIC X(8).                           
              10 COMM-AVCR-NCAISSE          PIC X(3).                           
              10 COMM-AVCR-NTRANS           PIC X(4).                           
              10 COMM-AVCR-NCRI             PIC X(11).                          
              10 COMM-AVCR-CMODPAIMT        PIC X(5).                           
              10 COMM-AVCR-CANNULATION      PIC X(5).                           
              10 COMM-AVCR-DANNULATION      PIC X(8).                           
              10 COMM-AVCR-NAVOIRANNUL      PIC X(7).                           
              10 COMM-AVCR-CDEVREF          PIC X(3).                           
              10 COMM-AVCR-PAVOIRDEV2       PIC S9(5)V9(2).                     
              10 COMM-AVCR-CDEV2            PIC X(3).                           
              10 COMM-AVCR-PTAUX            PIC S9(6)V9(5) COMP-3.              
              10 COMM-AVCR-CTITRENOM        PIC X(5).                           
              10 COMM-AVCR-LNOM             PIC X(25).                          
              10 COMM-AVCR-LPRENOM          PIC X(15).                          
              10 COMM-AVCR-CVOIE            PIC X(5).                           
              10 COMM-AVCR-CTVOIE           PIC X(4).                           
              10 COMM-AVCR-LNOMVOIE         PIC X(21).                          
              10 COMM-AVCR-CPOSTAL          PIC X(5).                           
              10 COMM-AVCR-LCOMMUNE         PIC X(32).                          
              10 COMM-AVCR-LBUREAU          PIC X(26).                          
LA2811        10 COMM-AVCR-LBATIMENT        PIC X(03).                          
LA2811        10 COMM-AVCR-LESCALIER        PIC X(03).                          
LA2811        10 COMM-AVCR-LETAGE           PIC X(03).                          
LA2811        10 COMM-AVCR-LPORTE           PIC X(03).                          
LA2811        10 COMM-AVCR-LCMPAD1          PIC X(32).                          
LA2811        10 COMM-AVCR-LCMPAD2          PIC X(32).                          
      *--- CODE RETOUR + MESSAGE ------------------------------------ 59        
           05 COMM-AVCR-MESSAGE.                                                
              10 COMM-AVCR-CODRET           PIC X(01).                          
                 88 COMM-AVCR-CODRET-OK                  VALUE ' '.             
                 88 COMM-AVCR-CODRET-ERREUR              VALUE '1'.             
              10 COMM-AVCR-LIBERR           PIC X(58).                          
                                                                                
