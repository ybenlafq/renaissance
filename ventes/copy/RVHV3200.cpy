      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVHV3200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV3200                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHV3200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHV3200.                                                            
      *}                                                                        
           02  HV32-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV32-NLIEU                                                       
               PIC X(0003).                                                     
           02  HV32-NSOCMAG                                                     
               PIC X(0003).                                                     
           02  HV32-NMAG                                                        
               PIC X(0003).                                                     
           02  HV32-CRAYONFAM                                                   
               PIC X(0005).                                                     
           02  HV32-CVENDEUR                                                    
               PIC X(0006).                                                     
           02  HV32-DMVENTE                                                     
               PIC X(0006).                                                     
           02  HV32-NAGREGATED                                                  
               PIC S9(5) COMP-3.                                                
           02  HV32-LAGREGATED                                                  
               PIC X(0020).                                                     
           02  HV32-QVENDUE                                                     
               PIC S9(5) COMP-3.                                                
           02  HV32-PCA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV32-PMTACHATS                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV32-PMTPRIMES                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV32-PMTCOMM1                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV32-PMTCOMM2                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV32-QNBPSE                                                      
               PIC S9(5) COMP-3.                                                
           02  HV32-PCAPSE                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV32-PMTPRIMPSE                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV32-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  HV32-QNBPSAB                                                     
               PIC S9(5) COMP-3.                                                
           02  HV32-QVENDUEEP                                                   
               PIC S9(5) COMP-3.                                                
           02  HV32-QVENDUESV                                                   
               PIC S9(5) COMP-3.                                                
           02  HV32-QVENDUECM                                                   
               PIC S9(5) COMP-3.                                                
           02  HV32-PCAEP                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV32-PCASV                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV32-PCACM                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV32-PMTACHEP                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV32-PMTACHSV                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV32-PMTACHCM                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV32-PMTREM                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV32-PMTREMEP                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV32-PMTCOMMEP                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHV3200                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHV3200-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHV3200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-NSOCMAG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-NSOCMAG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-CRAYONFAM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-CRAYONFAM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-DMVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-DMVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-NAGREGATED-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-NAGREGATED-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-LAGREGATED-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-LAGREGATED-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-QVENDUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-QVENDUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-PCA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-PCA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-PMTACHATS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-PMTACHATS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-PMTPRIMES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-PMTPRIMES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-PMTCOMM1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-PMTCOMM1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-PMTCOMM2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-PMTCOMM2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-QNBPSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-QNBPSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-PCAPSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-PCAPSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-PMTPRIMPSE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-PMTPRIMPSE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-QNBPSAB-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-QNBPSAB-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-QVENDUEEP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-QVENDUEEP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-QVENDUESV-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-QVENDUESV-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-QVENDUECM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-QVENDUECM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-PCAEP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-PCAEP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-PCASV-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-PCASV-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-PCACM-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-PCACM-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-PMTACHEP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-PMTACHEP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-PMTACHSV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-PMTACHSV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-PMTACHCM-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-PMTACHCM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-PMTREM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-PMTREM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-PMTREMEP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-PMTREMEP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV32-PMTCOMMEP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV32-PMTCOMMEP-F                                                 
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
