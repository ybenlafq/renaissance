      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TDMAA MISE AUTOMATIQUE EN ANOMALIE     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTDMAA.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTDMAA.                                                             
      *}                                                                        
           05  TDMAA-CTABLEG2    PIC X(15).                                     
           05  TDMAA-CTABLEG2-REDEF REDEFINES TDMAA-CTABLEG2.                   
               10  TDMAA-CDOSSIER        PIC X(05).                             
               10  TDMAA-WARO            PIC X(05).                             
               10  TDMAA-CODANO          PIC X(05).                             
           05  TDMAA-WTABLEG     PIC X(80).                                     
           05  TDMAA-WTABLEG-REDEF  REDEFINES TDMAA-WTABLEG.                    
               10  TDMAA-DELAI           PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  TDMAA-DELAI-N        REDEFINES TDMAA-DELAI                   
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  TDMAA-ACTIF           PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTDMAA-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTDMAA-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDMAA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TDMAA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDMAA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TDMAA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
