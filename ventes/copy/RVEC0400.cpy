      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RDPP.RVEC0400                      *        
      ******************************************************************        
       01  RVEC0400.                                                            
           10 EC04-NSOCIETE        PIC X(3).                                    
           10 EC04-NLIEU           PIC X(3).                                    
           10 EC04-NVENTE          PIC X(7).                                    
           10 EC04-DVENTE          PIC X(8).                                    
           10 EC04-NCDEWC          PIC S9(15)V USAGE COMP-3.                    
           10 EC04-CCCHOIX         PIC X(10).                                   
           10 EC04-CCPSWD          PIC X(10).                                   
           10 EC04-CCEVENT         PIC X(4).                                    
           10 EC04-CEVENTC         PIC X(15).                                   
           10 EC04-CCASIER         PIC X(10).                                   
           10 EC04-CPSWDC          PIC X(10).                                   
           10 EC04-CEMPLOYE        PIC X(10).                                   
           10 EC04-DSYST           PIC S9(13)V USAGE COMP-3.                    
                                                                                
