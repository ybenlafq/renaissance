      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV71   EGV71                                              00000020
      ***************************************************************** 00000030
       01   EGV71I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
           02 M128I OCCURS   15 TIMES .                                 00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCVENDEURL   COMP PIC S9(4).                            00000150
      *--                                                                       
             03 MCVENDEURL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCVENDEURF   PIC X.                                     00000160
             03 FILLER  PIC X(4).                                       00000170
             03 MCVENDEURI   PIC X(6).                                  00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLVENDEURL   COMP PIC S9(4).                            00000190
      *--                                                                       
             03 MLVENDEURL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLVENDEURF   PIC X.                                     00000200
             03 FILLER  PIC X(4).                                       00000210
             03 MLVENDEURI   PIC X(15).                                 00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGL  COMP PIC S9(4).                                 00000230
      *--                                                                       
             03 MNMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMAGF  PIC X.                                          00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MNMAGI  PIC X(3).                                       00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMATL  COMP PIC S9(4).                                 00000270
      *--                                                                       
             03 MNMATL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMATF  PIC X.                                          00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MNMATI  PIC X(7).                                       00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWPRIMEL     COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MWPRIMEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWPRIMEF     PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MWPRIMEI     PIC X.                                     00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWPRIMETL    COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MWPRIMETL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWPRIMETF    PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MWPRIMETI    PIC X(5).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MZONCMDI  PIC X(15).                                      00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MLIBERRI  PIC X(58).                                      00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCODTRAI  PIC X(4).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MCICSI    PIC X(5).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MNETNAMI  PIC X(8).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MSCREENI  PIC X(4).                                       00000620
      ***************************************************************** 00000630
      * SDF: EGV71   EGV71                                              00000640
      ***************************************************************** 00000650
       01   EGV71O REDEFINES EGV71I.                                    00000660
           02 FILLER    PIC X(12).                                      00000670
           02 FILLER    PIC X(2).                                       00000680
           02 MDATJOUA  PIC X.                                          00000690
           02 MDATJOUC  PIC X.                                          00000700
           02 MDATJOUP  PIC X.                                          00000710
           02 MDATJOUH  PIC X.                                          00000720
           02 MDATJOUV  PIC X.                                          00000730
           02 MDATJOUO  PIC X(10).                                      00000740
           02 FILLER    PIC X(2).                                       00000750
           02 MTIMJOUA  PIC X.                                          00000760
           02 MTIMJOUC  PIC X.                                          00000770
           02 MTIMJOUP  PIC X.                                          00000780
           02 MTIMJOUH  PIC X.                                          00000790
           02 MTIMJOUV  PIC X.                                          00000800
           02 MTIMJOUO  PIC X(5).                                       00000810
           02 M128O OCCURS   15 TIMES .                                 00000820
             03 FILLER       PIC X(2).                                  00000830
             03 MCVENDEURA   PIC X.                                     00000840
             03 MCVENDEURC   PIC X.                                     00000850
             03 MCVENDEURP   PIC X.                                     00000860
             03 MCVENDEURH   PIC X.                                     00000870
             03 MCVENDEURV   PIC X.                                     00000880
             03 MCVENDEURO   PIC X(6).                                  00000890
             03 FILLER       PIC X(2).                                  00000900
             03 MLVENDEURA   PIC X.                                     00000910
             03 MLVENDEURC   PIC X.                                     00000920
             03 MLVENDEURP   PIC X.                                     00000930
             03 MLVENDEURH   PIC X.                                     00000940
             03 MLVENDEURV   PIC X.                                     00000950
             03 MLVENDEURO   PIC X(15).                                 00000960
             03 FILLER       PIC X(2).                                  00000970
             03 MNMAGA  PIC X.                                          00000980
             03 MNMAGC  PIC X.                                          00000990
             03 MNMAGP  PIC X.                                          00001000
             03 MNMAGH  PIC X.                                          00001010
             03 MNMAGV  PIC X.                                          00001020
             03 MNMAGO  PIC X(3).                                       00001030
             03 FILLER       PIC X(2).                                  00001040
             03 MNMATA  PIC X.                                          00001050
             03 MNMATC  PIC X.                                          00001060
             03 MNMATP  PIC X.                                          00001070
             03 MNMATH  PIC X.                                          00001080
             03 MNMATV  PIC X.                                          00001090
             03 MNMATO  PIC X(7).                                       00001100
             03 FILLER       PIC X(2).                                  00001110
             03 MWPRIMEA     PIC X.                                     00001120
             03 MWPRIMEC     PIC X.                                     00001130
             03 MWPRIMEP     PIC X.                                     00001140
             03 MWPRIMEH     PIC X.                                     00001150
             03 MWPRIMEV     PIC X.                                     00001160
             03 MWPRIMEO     PIC X.                                     00001170
             03 FILLER       PIC X(2).                                  00001180
             03 MWPRIMETA    PIC X.                                     00001190
             03 MWPRIMETC    PIC X.                                     00001200
             03 MWPRIMETP    PIC X.                                     00001210
             03 MWPRIMETH    PIC X.                                     00001220
             03 MWPRIMETV    PIC X.                                     00001230
             03 MWPRIMETO    PIC X(5).                                  00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MZONCMDA  PIC X.                                          00001260
           02 MZONCMDC  PIC X.                                          00001270
           02 MZONCMDP  PIC X.                                          00001280
           02 MZONCMDH  PIC X.                                          00001290
           02 MZONCMDV  PIC X.                                          00001300
           02 MZONCMDO  PIC X(15).                                      00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MLIBERRA  PIC X.                                          00001330
           02 MLIBERRC  PIC X.                                          00001340
           02 MLIBERRP  PIC X.                                          00001350
           02 MLIBERRH  PIC X.                                          00001360
           02 MLIBERRV  PIC X.                                          00001370
           02 MLIBERRO  PIC X(58).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MCODTRAA  PIC X.                                          00001400
           02 MCODTRAC  PIC X.                                          00001410
           02 MCODTRAP  PIC X.                                          00001420
           02 MCODTRAH  PIC X.                                          00001430
           02 MCODTRAV  PIC X.                                          00001440
           02 MCODTRAO  PIC X(4).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MCICSA    PIC X.                                          00001470
           02 MCICSC    PIC X.                                          00001480
           02 MCICSP    PIC X.                                          00001490
           02 MCICSH    PIC X.                                          00001500
           02 MCICSV    PIC X.                                          00001510
           02 MCICSO    PIC X(5).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNETNAMA  PIC X.                                          00001540
           02 MNETNAMC  PIC X.                                          00001550
           02 MNETNAMP  PIC X.                                          00001560
           02 MNETNAMH  PIC X.                                          00001570
           02 MNETNAMV  PIC X.                                          00001580
           02 MNETNAMO  PIC X(8).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MSCREENA  PIC X.                                          00001610
           02 MSCREENC  PIC X.                                          00001620
           02 MSCREENP  PIC X.                                          00001630
           02 MSCREENH  PIC X.                                          00001640
           02 MSCREENV  PIC X.                                          00001650
           02 MSCREENO  PIC X(4).                                       00001660
                                                                                
