      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVSL2200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSL2200                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVSL2200.                                                            
           02  SL22-NMUTATION      PIC X(0007).                                 
           02  SL22-DMUTATION      PIC X(0008).                                 
           02  SL22-NSOCIETE       PIC X(0003).                                 
           02  SL22-NLIEU          PIC X(0003).                                 
           02  SL22-NVENTE         PIC X(0007).                                 
           02  SL22-NCODIC         PIC X(0007).                                 
           02  SL22-NSEQNQ         PIC S9(05) COMP-3.                           
           02  SL22-QTE            PIC S9(03) COMP-3.                           
           02  SL22-PGMMAJ         PIC X(0008).                                 
           02  SL22-DSYST          PIC S9(13) COMP-3.                           
       EJECT                                                                    
                                                                                
