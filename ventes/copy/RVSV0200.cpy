      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RTSV02                        *        
      ******************************************************************        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVSV0200.                                                            
      *    *************************************************************        
      *                       CODE GROUPE                                       
           10 SV02-CGROUPE         PIC X(5).                                    
      *    *************************************************************        
      *                       LIBELLE GROUPE CHASSIS                            
           10 SV02-LGROUPE         PIC X(20).                                   
      *    *************************************************************        
      *                       TYPE GROUPE CHASSIS                               
           10 SV02-TGROUPE         PIC X(5).                                    
      *    *************************************************************        
      *                       NUMERO SEQUENCE                                   
           10 SV02-NSEQ            PIC S9(4)V USAGE COMP-3.                     
      *    *************************************************************        
      *                       FLAG ACTIF                                        
           10 SV02-FACTIF          PIC X(1).                                    
      *    *************************************************************        
      *                       DATE SYSTEME                                      
           10 SV02-DSYST           PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 6       *        
      ******************************************************************        
       01  RVSV0200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SV02-CGROUPE-F PIC S9(4) COMP.                                    
      *--                                                                       
           10 SV02-CGROUPE-F PIC S9(4) COMP-5.                                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SV02-LGROUPE-F PIC S9(4) COMP.                                    
      *--                                                                       
           10 SV02-LGROUPE-F PIC S9(4) COMP-5.                                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SV02-TGROUPE-F PIC S9(4) COMP.                                    
      *--                                                                       
           10 SV02-TGROUPE-F PIC S9(4) COMP-5.                                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SV02-NSEQ-F    PIC S9(4) COMP.                                    
      *--                                                                       
           10 SV02-NSEQ-F    PIC S9(4) COMP-5.                                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SV02-FACTIF-F  PIC S9(4) COMP.                                    
      *--                                                                       
           10 SV02-FACTIF-F  PIC S9(4) COMP-5.                                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SV02-DSYST-F   PIC S9(4) COMP.                                    
      *                                                                         
      *--                                                                       
           10 SV02-DSYST-F   PIC S9(4) COMP-5.                                  
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
