      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *==> D{BUT                                                                
      *                                                                         
      *    STRUCTURE DE L'ENT�TE DES MESSAGES MQ-SERIES                         
      * CORRESPOND @ COPY AS400 : COPIE CCBEMQENTW                              
      * CE NIVEAU 05 EST NECESSAIRE                                             
      * AJOUTER ENSUITE COPY DE MESSAGE @ TRAITER                               
      * (NIVEAU 05 AUSSI)                                                       
          05 MESSMQ-ENT.                                                        
      *         TYPE DU MESSAGE:                                                
             10 MESSMQ-TYPE        PIC X(03).                                   
      *         N[ DE SOCI{T{ {METTRICE DU MESSAGE:                             
             10 MESSMQ-NSOCMSG     PIC X(03).                                   
      *         N[ DE MAGASIN {M{TTEUR DU MESSAGE:                              
             10 MESSMQ-NLIEUMSG    PIC X(03).                                   
      *         N[ DE SOCI{T{ DESTINATAIRE DU MESSAGE                           
             10 MESSMQ-NSOCDST     PIC X(03).                                   
      *         N[ DE SOCI{T{ DESTINATAIRE DU MESSAGE                           
             10 MESSMQ-NLIEUDST    PIC X(03).                                   
      *         N[ D'ORDRE:                                                     
             10 MESSMQ-NORD        PIC 9(08).                                   
      *         NOM DU PROGRAMME {M{TTEUR:                                      
             10 MESSMQ-LPROG       PIC X(10).                                   
      *         DATE DU JOUR:                                                   
             10 MESSMQ-DATEJ       PIC X(08).                                   
      *         NOM DU TERMINAL {METTEUR                                        
             10 MESSMQ-WSID        PIC X(10).                                   
      *         NOM DU USER {METTEUR                                            
             10 MESSMQ-USER        PIC X(10).                                   
      *         N[ CHRONO DU MESSAGE (1[ = 0000001)                             
             10 MESSMQ-CHRONO      PIC 9(07).                                   
      *         NOMBRE TOTAL DE MESSAGES DE DONN{ES D'UN LOT                    
      *         MESSAGE DE CONTR�LE EXCLU                                       
             10 MESSMQ-NBRMSG      PIC 9(07).                                   
      *         NOMBRE D'OCCURENCES POUR UN MESSAGE                             
             10 MESSMQ-NBROCC      PIC 9(05).                                   
      *         LONGUEUR D'UNE OCCURENCE                                        
             10 MESSMQ-LNGOCC      PIC 9(05).                                   
      *         NUM{RO DE VERSION DU MESSAGE.                                   
             10 MESSMQ-VERSION     PIC X(02).                                   
      *         DATE SYST}ME HOST DE L'ENVOI DU MESSAGE                         
             10 MESSMQ-DSYST       PIC S9(13).                                  
      *         FILLER R{SERV{ AUX MODIFICATIONS FUTURES                        
             10 MESSMQ-FILLER      PIC X(05).                                   
      *                                                                         
      *---- FIN COPIE                                                           
                                                                                
