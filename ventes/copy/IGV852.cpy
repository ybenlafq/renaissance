      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGV852 AU 29/10/2013  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,08,BI,A,                          *        
      *                           18,08,BI,A,                          *        
      *                           26,07,BI,A,                          *        
      *                           33,07,BI,A,                          *        
      *                           40,05,BI,A,                          *        
      *                           45,03,BI,A,                          *        
      *                           48,07,BI,A,                          *        
      *                           55,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGV852.                                                        
            05 NOMETAT-IGV852           PIC X(6) VALUE 'IGV852'.                
            05 RUPTURES-IGV852.                                                 
           10 IGV852-NLIEU              PIC X(03).                      007  003
           10 IGV852-DVENTE             PIC X(08).                      010  008
           10 IGV852-DDELIV             PIC X(08).                      018  008
           10 IGV852-NVENTE             PIC X(07).                      026  007
           10 IGV852-NCODIC             PIC X(07).                      033  007
           10 IGV852-CFAM               PIC X(05).                              
           10 IGV852-CMODDEL            PIC X(03).                              
           10 IGV852-PTF                PIC X(07).                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGV852-SEQUENCE           PIC S9(04) COMP.                040  002
      *--                                                                       
           10 IGV852-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGV852.                                                   
           10 IGV852-WCQERESF           PIC X(01).                      042  001
           10 IGV852-PVTOTAL            PIC S9(07)V9(2) COMP-3.         043  005
           10 IGV852-QVENDUE            PIC S9(05)      COMP-3.         048  003
           10 IGV852-REMISE             PIC S9(07)V9(2) COMP-3.         051  005
           10 IGV852-SOLDE              PIC S9(07)V9(2) COMP-3.                 
            05 FILLER                      PIC X(437).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGV852-LONG           PIC S9(4)   COMP  VALUE +075.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGV852-LONG           PIC S9(4) COMP-5  VALUE +075.           
                                                                                
      *}                                                                        
