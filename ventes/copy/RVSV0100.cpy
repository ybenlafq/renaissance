      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVSV0100 *                                           
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSV0100 *                       
      *---------------------------------------------------------                
      *                                                                         
       01  RVSV0100.                                                            
           02  SV01-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  SV01-NMAG                                                        
               PIC X(0003).                                                     
           02  SV01-CTPSAV                                                      
               PIC X(0005).                                                     
           02  SV01-DEFFET                                                      
               PIC X(0008).                                                     
           02  SV01-NSOCSAV                                                     
               PIC X(0003).                                                     
           02  SV01-WTAUXSAV                                                    
               PIC S9(0003) COMP-3.                                             
           02  SV01-DSYST                                                       
               PIC S9(0013) COMP-3.                                             
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVSV0100 *                                
      *---------------------------------------------------------                
      *                                                                         
       01  RVSV0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SV01-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SV01-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SV01-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SV01-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SV01-CTYPRD-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SV01-CTYPRD-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SV01-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SV01-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SV01-NSOCSAV-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SV01-NSOCSAV-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SV01-WTAUXSAV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SV01-WTAUXSAV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SV01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SV01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
