      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00000010
      * darty.com : Export stocks Host => Websphere                     00000020
      ****************************************************************  00000030
       01     FEC119-DSECT.                                                     
           05 FEC119-CLE.                                                       
            7 FEC119-NCODIC        PIC  X(07).                                  
            7 FEC119-NSOC          PIC  X(03).                                  
            7 FEC119-NLIEU         PIC  X(03).                                  
           05 FEC119-QSTOCK        PIC  9(05).                                  
           05 FEC119-DDISPO        PIC  X(08).                                  
      *    05 FEC119-CC-DEPOT      PIC  X(01).                                  
V33S1      05 FEC119-CC-DEPOT      PIC  X(07).                                  
                                                                                
