      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *    COPY  COMMAS20.                                                      
      *                                                                         
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *      COMMAREA DE LA TRANSACTION AS00 ASILAGE CEN               *        
      *                                                                *        
      *      PARTIES APPLICATIVES COMMUNES AUX TRANSACTION :           *        
      *      AS20                                                      *        
      *                                                                *        
      *      LONGUEUR = 1 - ZONES COMMUNES               372 C         *        
      *                 2 - ZONES APPLICATIVES PM       3724 C         *        
      *                     TOTAL                       4096           *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
      ******************************************************************        
      *    ZONES APPLICATIVES DES TRANSACTION AS00,AS20,     --- 199 C *        
      ******************************************************************        
      *                                                                         
           02 COMM-AS00-ZONES   REDEFINES  COMM-AS00-APPLI.                     
      *                                    NOM DU MENU APPELANT                 
              05 COMM-AS00-CMENU           PIC X(05).                           
      *                                    MAGASIN DE RECEPTION                 
              05 COMM-AS00-CCAMPAGNE       PIC X(04).                           
      *                                                                         
      ******************************************************************        
      *--- ZONES APPLICATIVES DES DONNEES PARAMETRES   - TAS20 - 171 C *        
      ******************************************************************        
      *                                                                         
              05 COMM-AS20-APPLI.                                               
                 10 COMM-AS20-CFONCT          PIC X(03).                00008601
                 10 COMM-AS20-CCAMPAGNE       PIC X(04).                        
                 10 COMM-AS20-CODE-RETOUR.                                      
                    15 COMM-AS20-CODRET       PIC X(01).                        
                    15 COMM-AS20-LIBERR       PIC X(58).                        
      *                                                                         
      ******************************************************************        
      *       ZONES DISPONIBLES ------------------------------- 2918 C*         
      ******************************************************************        
      *                                                                         
              05 COMM-AS00-FILLER          PIC X(2918).                         
      *                                                                         
                                                                                
