      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * menu liste des codics                                           00000020
      ***************************************************************** 00000030
       01   ECE51I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICEL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNCODICEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODICEF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICEI      PIC X(7).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMEL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCFAMEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAMEF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCFAMEI   PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQEL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCMARQEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQEF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCMARQEI  PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNEL   COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLREFFOURNEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLREFFOURNEF   PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLREFFOURNEI   PIC X(20).                                 00000370
           02 MTABI OCCURS   15 TIMES .                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNCODICI     PIC X(7).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCFAMI  PIC X(5).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCMARQI      PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFFOURNL  COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MLREFFOURNL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLREFFOURNF  PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLREFFOURNI  PIC X(20).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTAL   COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MSTAL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSTAF   PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MSTAI   PIC X(3).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRIXL  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MPRIXL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPRIXF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MPRIXI  PIC X(9).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 METQL   COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 METQL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 METQF   PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 METQI   PIC X(3).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MSELI   PIC X.                                          00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MZONCMDI  PIC X(15).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLIBERRI  PIC X(58).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNETNAMI  PIC X(8).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MSCREENI  PIC X(4).                                       00000940
      ***************************************************************** 00000950
      * menu liste des codics                                           00000960
      ***************************************************************** 00000970
       01   ECE51O REDEFINES ECE51I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MPAGEA    PIC X.                                          00001150
           02 MPAGEC    PIC X.                                          00001160
           02 MPAGEP    PIC X.                                          00001170
           02 MPAGEH    PIC X.                                          00001180
           02 MPAGEV    PIC X.                                          00001190
           02 MPAGEO    PIC X(3).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MPAGEMAXA      PIC X.                                     00001220
           02 MPAGEMAXC PIC X.                                          00001230
           02 MPAGEMAXP PIC X.                                          00001240
           02 MPAGEMAXH PIC X.                                          00001250
           02 MPAGEMAXV PIC X.                                          00001260
           02 MPAGEMAXO      PIC X(3).                                  00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MNCODICEA      PIC X.                                     00001290
           02 MNCODICEC PIC X.                                          00001300
           02 MNCODICEP PIC X.                                          00001310
           02 MNCODICEH PIC X.                                          00001320
           02 MNCODICEV PIC X.                                          00001330
           02 MNCODICEO      PIC X(7).                                  00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MCFAMEA   PIC X.                                          00001360
           02 MCFAMEC   PIC X.                                          00001370
           02 MCFAMEP   PIC X.                                          00001380
           02 MCFAMEH   PIC X.                                          00001390
           02 MCFAMEV   PIC X.                                          00001400
           02 MCFAMEO   PIC X(5).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MCMARQEA  PIC X.                                          00001430
           02 MCMARQEC  PIC X.                                          00001440
           02 MCMARQEP  PIC X.                                          00001450
           02 MCMARQEH  PIC X.                                          00001460
           02 MCMARQEV  PIC X.                                          00001470
           02 MCMARQEO  PIC X(5).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MLREFFOURNEA   PIC X.                                     00001500
           02 MLREFFOURNEC   PIC X.                                     00001510
           02 MLREFFOURNEP   PIC X.                                     00001520
           02 MLREFFOURNEH   PIC X.                                     00001530
           02 MLREFFOURNEV   PIC X.                                     00001540
           02 MLREFFOURNEO   PIC X(20).                                 00001550
           02 MTABO OCCURS   15 TIMES .                                 00001560
             03 FILLER       PIC X(2).                                  00001570
             03 MNCODICA     PIC X.                                     00001580
             03 MNCODICC     PIC X.                                     00001590
             03 MNCODICP     PIC X.                                     00001600
             03 MNCODICH     PIC X.                                     00001610
             03 MNCODICV     PIC X.                                     00001620
             03 MNCODICO     PIC X(7).                                  00001630
             03 FILLER       PIC X(2).                                  00001640
             03 MCFAMA  PIC X.                                          00001650
             03 MCFAMC  PIC X.                                          00001660
             03 MCFAMP  PIC X.                                          00001670
             03 MCFAMH  PIC X.                                          00001680
             03 MCFAMV  PIC X.                                          00001690
             03 MCFAMO  PIC X(5).                                       00001700
             03 FILLER       PIC X(2).                                  00001710
             03 MCMARQA      PIC X.                                     00001720
             03 MCMARQC PIC X.                                          00001730
             03 MCMARQP PIC X.                                          00001740
             03 MCMARQH PIC X.                                          00001750
             03 MCMARQV PIC X.                                          00001760
             03 MCMARQO      PIC X(5).                                  00001770
             03 FILLER       PIC X(2).                                  00001780
             03 MLREFFOURNA  PIC X.                                     00001790
             03 MLREFFOURNC  PIC X.                                     00001800
             03 MLREFFOURNP  PIC X.                                     00001810
             03 MLREFFOURNH  PIC X.                                     00001820
             03 MLREFFOURNV  PIC X.                                     00001830
             03 MLREFFOURNO  PIC X(20).                                 00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MSTAA   PIC X.                                          00001860
             03 MSTAC   PIC X.                                          00001870
             03 MSTAP   PIC X.                                          00001880
             03 MSTAH   PIC X.                                          00001890
             03 MSTAV   PIC X.                                          00001900
             03 MSTAO   PIC X(3).                                       00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MPRIXA  PIC X.                                          00001930
             03 MPRIXC  PIC X.                                          00001940
             03 MPRIXP  PIC X.                                          00001950
             03 MPRIXH  PIC X.                                          00001960
             03 MPRIXV  PIC X.                                          00001970
             03 MPRIXO  PIC X(9).                                       00001980
             03 FILLER       PIC X(2).                                  00001990
             03 METQA   PIC X.                                          00002000
             03 METQC   PIC X.                                          00002010
             03 METQP   PIC X.                                          00002020
             03 METQH   PIC X.                                          00002030
             03 METQV   PIC X.                                          00002040
             03 METQO   PIC X(3).                                       00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MSELA   PIC X.                                          00002070
             03 MSELC   PIC X.                                          00002080
             03 MSELP   PIC X.                                          00002090
             03 MSELH   PIC X.                                          00002100
             03 MSELV   PIC X.                                          00002110
             03 MSELO   PIC X.                                          00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MZONCMDA  PIC X.                                          00002140
           02 MZONCMDC  PIC X.                                          00002150
           02 MZONCMDP  PIC X.                                          00002160
           02 MZONCMDH  PIC X.                                          00002170
           02 MZONCMDV  PIC X.                                          00002180
           02 MZONCMDO  PIC X(15).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLIBERRA  PIC X.                                          00002210
           02 MLIBERRC  PIC X.                                          00002220
           02 MLIBERRP  PIC X.                                          00002230
           02 MLIBERRH  PIC X.                                          00002240
           02 MLIBERRV  PIC X.                                          00002250
           02 MLIBERRO  PIC X(58).                                      00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCICSA    PIC X.                                          00002350
           02 MCICSC    PIC X.                                          00002360
           02 MCICSP    PIC X.                                          00002370
           02 MCICSH    PIC X.                                          00002380
           02 MCICSV    PIC X.                                          00002390
           02 MCICSO    PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNETNAMA  PIC X.                                          00002420
           02 MNETNAMC  PIC X.                                          00002430
           02 MNETNAMP  PIC X.                                          00002440
           02 MNETNAMH  PIC X.                                          00002450
           02 MNETNAMV  PIC X.                                          00002460
           02 MNETNAMO  PIC X(8).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSCREENA  PIC X.                                          00002490
           02 MSCREENC  PIC X.                                          00002500
           02 MSCREENP  PIC X.                                          00002510
           02 MSCREENH  PIC X.                                          00002520
           02 MSCREENV  PIC X.                                          00002530
           02 MSCREENO  PIC X(4).                                       00002540
                                                                                
