      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EIQ00   EIQ00                                              00000020
      ***************************************************************** 00000030
       01   EIQ00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEML   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEMF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTALFL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCPOSTALFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCPOSTALFF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCPOSTALFI     PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMMUNEFL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCOMMUNEFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCOMMUNEFF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCOMMUNEFI     PIC X(5).                                  00000290
           02 MTABI OCCURS   18 TIMES .                                 00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MSELI   PIC X.                                          00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPOSTALL    COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MCPOSTALL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCPOSTALF    PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MCPOSTALI    PIC X(5).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMMUNEL   COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MLCOMMUNEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLCOMMUNEF   PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MLCOMMUNEI   PIC X(32).                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MLIBERRI  PIC X(78).                                      00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCODTRAI  PIC X(4).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MCICSI    PIC X(5).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MNETNAMI  PIC X(8).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MSCREENI  PIC X(5).                                       00000620
      ***************************************************************** 00000630
      * SDF: EIQ00   EIQ00                                              00000640
      ***************************************************************** 00000650
       01   EIQ00O REDEFINES EIQ00I.                                    00000660
           02 FILLER    PIC X(12).                                      00000670
           02 FILLER    PIC X(2).                                       00000680
           02 MDATJOUA  PIC X.                                          00000690
           02 MDATJOUC  PIC X.                                          00000700
           02 MDATJOUP  PIC X.                                          00000710
           02 MDATJOUH  PIC X.                                          00000720
           02 MDATJOUV  PIC X.                                          00000730
           02 MDATJOUO  PIC X(10).                                      00000740
           02 FILLER    PIC X(2).                                       00000750
           02 MTIMJOUA  PIC X.                                          00000760
           02 MTIMJOUC  PIC X.                                          00000770
           02 MTIMJOUP  PIC X.                                          00000780
           02 MTIMJOUH  PIC X.                                          00000790
           02 MTIMJOUV  PIC X.                                          00000800
           02 MTIMJOUO  PIC X(5).                                       00000810
           02 FILLER    PIC X(2).                                       00000820
           02 MPAGEA    PIC X.                                          00000830
           02 MPAGEC    PIC X.                                          00000840
           02 MPAGEP    PIC X.                                          00000850
           02 MPAGEH    PIC X.                                          00000860
           02 MPAGEV    PIC X.                                          00000870
           02 MPAGEO    PIC X(3).                                       00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MPAGEMA   PIC X.                                          00000900
           02 MPAGEMC   PIC X.                                          00000910
           02 MPAGEMP   PIC X.                                          00000920
           02 MPAGEMH   PIC X.                                          00000930
           02 MPAGEMV   PIC X.                                          00000940
           02 MPAGEMO   PIC X(3).                                       00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MCPOSTALFA     PIC X.                                     00000970
           02 MCPOSTALFC     PIC X.                                     00000980
           02 MCPOSTALFP     PIC X.                                     00000990
           02 MCPOSTALFH     PIC X.                                     00001000
           02 MCPOSTALFV     PIC X.                                     00001010
           02 MCPOSTALFO     PIC X(5).                                  00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MCOMMUNEFA     PIC X.                                     00001040
           02 MCOMMUNEFC     PIC X.                                     00001050
           02 MCOMMUNEFP     PIC X.                                     00001060
           02 MCOMMUNEFH     PIC X.                                     00001070
           02 MCOMMUNEFV     PIC X.                                     00001080
           02 MCOMMUNEFO     PIC X(5).                                  00001090
           02 MTABO OCCURS   18 TIMES .                                 00001100
             03 FILLER       PIC X(2).                                  00001110
             03 MSELA   PIC X.                                          00001120
             03 MSELC   PIC X.                                          00001130
             03 MSELP   PIC X.                                          00001140
             03 MSELH   PIC X.                                          00001150
             03 MSELV   PIC X.                                          00001160
             03 MSELO   PIC X.                                          00001170
             03 FILLER       PIC X(2).                                  00001180
             03 MCPOSTALA    PIC X.                                     00001190
             03 MCPOSTALC    PIC X.                                     00001200
             03 MCPOSTALP    PIC X.                                     00001210
             03 MCPOSTALH    PIC X.                                     00001220
             03 MCPOSTALV    PIC X.                                     00001230
             03 MCPOSTALO    PIC X(5).                                  00001240
             03 FILLER       PIC X(2).                                  00001250
             03 MLCOMMUNEA   PIC X.                                     00001260
             03 MLCOMMUNEC   PIC X.                                     00001270
             03 MLCOMMUNEP   PIC X.                                     00001280
             03 MLCOMMUNEH   PIC X.                                     00001290
             03 MLCOMMUNEV   PIC X.                                     00001300
             03 MLCOMMUNEO   PIC X(32).                                 00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MLIBERRA  PIC X.                                          00001330
           02 MLIBERRC  PIC X.                                          00001340
           02 MLIBERRP  PIC X.                                          00001350
           02 MLIBERRH  PIC X.                                          00001360
           02 MLIBERRV  PIC X.                                          00001370
           02 MLIBERRO  PIC X(78).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MCODTRAA  PIC X.                                          00001400
           02 MCODTRAC  PIC X.                                          00001410
           02 MCODTRAP  PIC X.                                          00001420
           02 MCODTRAH  PIC X.                                          00001430
           02 MCODTRAV  PIC X.                                          00001440
           02 MCODTRAO  PIC X(4).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MCICSA    PIC X.                                          00001470
           02 MCICSC    PIC X.                                          00001480
           02 MCICSP    PIC X.                                          00001490
           02 MCICSH    PIC X.                                          00001500
           02 MCICSV    PIC X.                                          00001510
           02 MCICSO    PIC X(5).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNETNAMA  PIC X.                                          00001540
           02 MNETNAMC  PIC X.                                          00001550
           02 MNETNAMP  PIC X.                                          00001560
           02 MNETNAMH  PIC X.                                          00001570
           02 MNETNAMV  PIC X.                                          00001580
           02 MNETNAMO  PIC X(8).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MSCREENA  PIC X.                                          00001610
           02 MSCREENC  PIC X.                                          00001620
           02 MSCREENP  PIC X.                                          00001630
           02 MSCREENH  PIC X.                                          00001640
           02 MSCREENV  PIC X.                                          00001650
           02 MSCREENO  PIC X(5).                                       00001660
                                                                                
