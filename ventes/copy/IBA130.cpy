      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IBA130 AU 13/03/2008  *        
      *                                                                *        
      *          CRITERES DE TRI  07,06,BI,A,                          *        
      *                           13,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IBA130.                                                        
            05 NOMETAT-IBA130           PIC X(6) VALUE 'IBA130'.                
            05 RUPTURES-IBA130.                                                 
           10 IBA130-CTIERSBA           PIC X(06).                      007  006
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IBA130-SEQUENCE           PIC S9(04) COMP.                013  002
      *--                                                                       
           10 IBA130-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IBA130.                                                   
           10 IBA130-CPOSTAL            PIC X(05).                      015  005
           10 IBA130-LADRESSE1          PIC X(30).                      020  030
           10 IBA130-LADRESSE2          PIC X(30).                      050  030
           10 IBA130-LRSOCIALE          PIC X(30).                      080  030
           10 IBA130-LVILLE             PIC X(24).                      110  024
           10 IBA130-NTIERSSAP          PIC X(10).                      134  010
           10 IBA130-WEFACTURE          PIC X(01).                      144  001
           10 IBA130-WELIBELLE          PIC X(01).                      145  001
           10 IBA130-WEMISEURO          PIC X(01).                      146  001
           10 IBA130-WTYPREM            PIC X(01).                      147  001
           10 IBA130-WVENTEMINI         PIC X(01).                      148  001
           10 IBA130-PVENTEMINI         PIC S9(07)V9(2) COMP-3.         149  005
           10 IBA130-QTAUXREM           PIC S9(03)V9(2) COMP-3.         154  003
            05 FILLER                      PIC X(356).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IBA130-LONG           PIC S9(4)   COMP  VALUE +156.           
      *                                                                         
      *--                                                                       
        01  DSECT-IBA130-LONG           PIC S9(4) COMP-5  VALUE +156.           
                                                                                
      *}                                                                        
