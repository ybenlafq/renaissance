      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGV1000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV1000                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV1000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV1000.                                                            
      *}                                                                        
           02  GV10-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GV10-NLIEU                                                       
               PIC X(0003).                                                     
           02  GV10-NVENTE                                                      
               PIC X(0007).                                                     
           02  GV10-NORDRE                                                      
               PIC X(0005).                                                     
           02  GV10-NCLIENT                                                     
               PIC X(0009).                                                     
           02  GV10-DVENTE                                                      
               PIC X(0008).                                                     
           02  GV10-DHVENTE                                                     
               PIC X(0002).                                                     
           02  GV10-DMVENTE                                                     
               PIC X(0002).                                                     
           02  GV10-PTTVENTE                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV10-PVERSE                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV10-PCOMPT                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV10-PLIVR                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV10-PDIFFERE                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV10-PRFACT                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV10-CREMVTE                                                     
               PIC X(0005).                                                     
           02  GV10-PREMVTE                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV10-LCOMVTE1                                                    
               PIC X(0030).                                                     
           02  GV10-LCOMVTE2                                                    
               PIC X(0030).                                                     
           02  GV10-LCOMVTE3                                                    
               PIC X(0030).                                                     
           02  GV10-LCOMVTE4                                                    
               PIC X(0030).                                                     
           02  GV10-DMODIFVTE                                                   
               PIC X(0008).                                                     
           02  GV10-WFACTURE                                                    
               PIC X(0001).                                                     
           02  GV10-WEXPORT                                                     
               PIC X(0001).                                                     
           02  GV10-WDETAXEC                                                    
               PIC X(0001).                                                     
           02  GV10-WDETAXEHC                                                   
               PIC X(0001).                                                     
           02  GV10-CORGORED                                                    
               PIC X(0005).                                                     
           02  GV10-CMODPAIMTI                                                  
               PIC X(0005).                                                     
           02  GV10-LDESCRIPTIF1                                                
               PIC X(0030).                                                     
           02  GV10-LDESCRIPTIF2                                                
               PIC X(0030).                                                     
           02  GV10-DLIVRBL                                                     
               PIC X(0008).                                                     
           02  GV10-NFOLIOBL                                                    
               PIC X(0003).                                                     
           02  GV10-LAUTORM                                                     
               PIC X(0005).                                                     
           02  GV10-NAUTORD                                                     
               PIC X(0005).                                                     
           02  GV10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGV1000                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV1000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV1000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-NCLIENT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-NCLIENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-DHVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-DHVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-DMVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-DMVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-PTTVENTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-PTTVENTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-PVERSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-PVERSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-PCOMPT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-PCOMPT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-PLIVR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-PLIVR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-PDIFFERE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-PDIFFERE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-PRFACT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-PRFACT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-CREMVTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-CREMVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-PREMVTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-PREMVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-LCOMVTE1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-LCOMVTE1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-LCOMVTE2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-LCOMVTE2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-LCOMVTE3-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-LCOMVTE3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-LCOMVTE4-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-LCOMVTE4-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-DMODIFVTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-DMODIFVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-WFACTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-WFACTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-WEXPORT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-WEXPORT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-WDETAXEC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-WDETAXEC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-WDETAXEHC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-WDETAXEHC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-CORGORED-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-CORGORED-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-CMODPAIMTI-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-CMODPAIMTI-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-LDESCRIPTIF1-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-LDESCRIPTIF1-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-LDESCRIPTIF2-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-LDESCRIPTIF2-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-DLIVRBL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-DLIVRBL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-NFOLIOBL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-NFOLIOBL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-LAUTORM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-LAUTORM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-NAUTORD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV10-NAUTORD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GV10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
