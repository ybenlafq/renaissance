      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVVE1499                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVE1499.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVE1499.                                                            
      *}                                                                        
      *                       NSOCIETE                                          
           10 VE14-NSOCIETE        PIC X(3).                                    
      *                       NLIEU                                             
           10 VE14-NLIEU           PIC X(3).                                    
      *                       NORDRE                                            
           10 VE14-NORDRE          PIC X(5).                                    
      *                       NVENTE                                            
           10 VE14-NVENTE          PIC X(7).                                    
      *                       DSAISIE                                           
           10 VE14-DSAISIE         PIC X(8).                                    
      *                       NSEQ                                              
           10 VE14-NSEQ            PIC X(2).                                    
      *                       CMODPAIMT                                         
           10 VE14-CMODPAIMT       PIC X(5).                                    
      *                       PREGLTVTE                                         
           10 VE14-PREGLTVTE       PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       NREGLTVTE                                         
           10 VE14-NREGLTVTE       PIC X(7).                                    
      *                       DREGLTVTE                                         
           10 VE14-DREGLTVTE       PIC X(8).                                    
      *                       WREGLTVTE                                         
           10 VE14-WREGLTVTE       PIC X(1).                                    
      *                       DCOMPTA                                           
           10 VE14-DCOMPTA         PIC X(8).                                    
      *                       DSYST                                             
           10 VE14-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       CPROTOUR                                          
           10 VE14-CPROTOUR        PIC X(5).                                    
      *                       CTOURNEE                                          
           10 VE14-CTOURNEE        PIC X(3).                                    
      *                       CDEVISE                                           
           10 VE14-CDEVISE         PIC X(3).                                    
      *                       MDEVISE                                           
           10 VE14-MDEVISE         PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       MECART                                            
           10 VE14-MECART          PIC S9(1)V9(2) USAGE COMP-3.                 
      *                       CDEV                                              
           10 VE14-CDEV            PIC X(3).                                    
      *                       NSOCP                                             
           10 VE14-NSOCP           PIC X(3).                                    
      *                       NLIEUP                                            
           10 VE14-NLIEUP          PIC X(3).                                    
      *                       NTRANS                                            
           10 VE14-NTRANS          PIC S9(8)V USAGE COMP-3.                     
      *                       PMODPAIMT                                         
           10 VE14-PMODPAIMT       PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       CVENDEUR                                          
           10 VE14-CVENDEUR        PIC X(6).                                    
      *                       CFCRED                                            
           10 VE14-CFCRED          PIC X(5).                                    
      *                       NCREDI                                            
           10 VE14-NCREDI          PIC X(14).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 26      *        
      ******************************************************************        
                                                                                
