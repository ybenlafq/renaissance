      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAS10   EAS10                                              00000020
      ***************************************************************** 00000030
       01   EAS20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAMPAL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCAMPAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCAMPAF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCAMPAI   PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MLIBELLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBELLF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLIBELLI  PIC X(30).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHOIX1L  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCHOIX1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCHOIX1F  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCHOIX1I  PIC X.                                          00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CCHOIX1L  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 CCHOIX1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 CCHOIX1F  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 CCHOIX1I  PIC X.                                          00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LCHOIX1L  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 LCHOIX1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 LCHOIX1F  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 LCHOIX1I  PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHOIX2L  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCHOIX2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCHOIX2F  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCHOIX2I  PIC X.                                          00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CCHOIX2L  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 CCHOIX2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 CCHOIX2F  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 CCHOIX2I  PIC X.                                          00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LCHOIX2L  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 LCHOIX2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 LCHOIX2F  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 LCHOIX2I  PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHOIX3L  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCHOIX3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCHOIX3F  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCHOIX3I  PIC X.                                          00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CCHOIX3L  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 CCHOIX3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 CCHOIX3F  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 CCHOIX3I  PIC X.                                          00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LCHOIX3L  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 LCHOIX3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 LCHOIX3F  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 LCHOIX3I  PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHOIX4L  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCHOIX4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCHOIX4F  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCHOIX4I  PIC X.                                          00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CCHOIX4L  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 CCHOIX4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 CCHOIX4F  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 CCHOIX4I  PIC X.                                          00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LCHOIX4L  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 LCHOIX4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 LCHOIX4F  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 LCHOIX4I  PIC X(20).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHOIX5L  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCHOIX5L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCHOIX5F  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCHOIX5I  PIC X.                                          00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CCHOIX5L  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 CCHOIX5L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 CCHOIX5F  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 CCHOIX5I  PIC X.                                          00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LCHOIX5L  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 LCHOIX5L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 LCHOIX5F  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 LCHOIX5I  PIC X(20).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHOIX6L  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCHOIX6L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCHOIX6F  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCHOIX6I  PIC X.                                          00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CCHOIX6L  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 CCHOIX6L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 CCHOIX6F  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 CCHOIX6I  PIC X.                                          00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LCHOIX6L  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 LCHOIX6L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 LCHOIX6F  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 LCHOIX6I  PIC X(20).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHOIX7L  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MCHOIX7L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCHOIX7F  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MCHOIX7I  PIC X.                                          00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CCHOIX7L  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 CCHOIX7L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 CCHOIX7F  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 CCHOIX7I  PIC X.                                          00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LCHOIX7L  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 LCHOIX7L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 LCHOIX7F  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 LCHOIX7I  PIC X(20).                                      00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHOIX8L  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MCHOIX8L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCHOIX8F  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MCHOIX8I  PIC X.                                          00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CCHOIX8L  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 CCHOIX8L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 CCHOIX8F  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 CCHOIX8I  PIC X.                                          00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LCHOIX8L  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 LCHOIX8L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 LCHOIX8F  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 LCHOIX8I  PIC X(20).                                      00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MLIBERRI  PIC X(78).                                      00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MCODTRAI  PIC X(4).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MCICSI    PIC X(5).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MNETNAMI  PIC X(8).                                       00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MSCREENI  PIC X(5).                                       00001370
      ***************************************************************** 00001380
      * SDF: EAS10   EAS10                                              00001390
      ***************************************************************** 00001400
       01   EAS20O REDEFINES EAS20I.                                    00001410
           02 FILLER    PIC X(12).                                      00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MDATJOUA  PIC X.                                          00001440
           02 MDATJOUC  PIC X.                                          00001450
           02 MDATJOUP  PIC X.                                          00001460
           02 MDATJOUH  PIC X.                                          00001470
           02 MDATJOUV  PIC X.                                          00001480
           02 MDATJOUO  PIC X(10).                                      00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MTIMJOUA  PIC X.                                          00001510
           02 MTIMJOUC  PIC X.                                          00001520
           02 MTIMJOUP  PIC X.                                          00001530
           02 MTIMJOUH  PIC X.                                          00001540
           02 MTIMJOUV  PIC X.                                          00001550
           02 MTIMJOUO  PIC X(5).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MCAMPAA   PIC X.                                          00001580
           02 MCAMPAC   PIC X.                                          00001590
           02 MCAMPAP   PIC X.                                          00001600
           02 MCAMPAH   PIC X.                                          00001610
           02 MCAMPAV   PIC X.                                          00001620
           02 MCAMPAO   PIC X(4).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MLIBELLA  PIC X.                                          00001650
           02 MLIBELLC  PIC X.                                          00001660
           02 MLIBELLP  PIC X.                                          00001670
           02 MLIBELLH  PIC X.                                          00001680
           02 MLIBELLV  PIC X.                                          00001690
           02 MLIBELLO  PIC X(30).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MCHOIX1A  PIC X.                                          00001720
           02 MCHOIX1C  PIC X.                                          00001730
           02 MCHOIX1P  PIC X.                                          00001740
           02 MCHOIX1H  PIC X.                                          00001750
           02 MCHOIX1V  PIC X.                                          00001760
           02 MCHOIX1O  PIC X.                                          00001770
           02 FILLER    PIC X(2).                                       00001780
           02 CCHOIX1A  PIC X.                                          00001790
           02 CCHOIX1C  PIC X.                                          00001800
           02 CCHOIX1P  PIC X.                                          00001810
           02 CCHOIX1H  PIC X.                                          00001820
           02 CCHOIX1V  PIC X.                                          00001830
           02 CCHOIX1O  PIC X.                                          00001840
           02 FILLER    PIC X(2).                                       00001850
           02 LCHOIX1A  PIC X.                                          00001860
           02 LCHOIX1C  PIC X.                                          00001870
           02 LCHOIX1P  PIC X.                                          00001880
           02 LCHOIX1H  PIC X.                                          00001890
           02 LCHOIX1V  PIC X.                                          00001900
           02 LCHOIX1O  PIC X(20).                                      00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MCHOIX2A  PIC X.                                          00001930
           02 MCHOIX2C  PIC X.                                          00001940
           02 MCHOIX2P  PIC X.                                          00001950
           02 MCHOIX2H  PIC X.                                          00001960
           02 MCHOIX2V  PIC X.                                          00001970
           02 MCHOIX2O  PIC X.                                          00001980
           02 FILLER    PIC X(2).                                       00001990
           02 CCHOIX2A  PIC X.                                          00002000
           02 CCHOIX2C  PIC X.                                          00002010
           02 CCHOIX2P  PIC X.                                          00002020
           02 CCHOIX2H  PIC X.                                          00002030
           02 CCHOIX2V  PIC X.                                          00002040
           02 CCHOIX2O  PIC X.                                          00002050
           02 FILLER    PIC X(2).                                       00002060
           02 LCHOIX2A  PIC X.                                          00002070
           02 LCHOIX2C  PIC X.                                          00002080
           02 LCHOIX2P  PIC X.                                          00002090
           02 LCHOIX2H  PIC X.                                          00002100
           02 LCHOIX2V  PIC X.                                          00002110
           02 LCHOIX2O  PIC X(20).                                      00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MCHOIX3A  PIC X.                                          00002140
           02 MCHOIX3C  PIC X.                                          00002150
           02 MCHOIX3P  PIC X.                                          00002160
           02 MCHOIX3H  PIC X.                                          00002170
           02 MCHOIX3V  PIC X.                                          00002180
           02 MCHOIX3O  PIC X.                                          00002190
           02 FILLER    PIC X(2).                                       00002200
           02 CCHOIX3A  PIC X.                                          00002210
           02 CCHOIX3C  PIC X.                                          00002220
           02 CCHOIX3P  PIC X.                                          00002230
           02 CCHOIX3H  PIC X.                                          00002240
           02 CCHOIX3V  PIC X.                                          00002250
           02 CCHOIX3O  PIC X.                                          00002260
           02 FILLER    PIC X(2).                                       00002270
           02 LCHOIX3A  PIC X.                                          00002280
           02 LCHOIX3C  PIC X.                                          00002290
           02 LCHOIX3P  PIC X.                                          00002300
           02 LCHOIX3H  PIC X.                                          00002310
           02 LCHOIX3V  PIC X.                                          00002320
           02 LCHOIX3O  PIC X(20).                                      00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCHOIX4A  PIC X.                                          00002350
           02 MCHOIX4C  PIC X.                                          00002360
           02 MCHOIX4P  PIC X.                                          00002370
           02 MCHOIX4H  PIC X.                                          00002380
           02 MCHOIX4V  PIC X.                                          00002390
           02 MCHOIX4O  PIC X.                                          00002400
           02 FILLER    PIC X(2).                                       00002410
           02 CCHOIX4A  PIC X.                                          00002420
           02 CCHOIX4C  PIC X.                                          00002430
           02 CCHOIX4P  PIC X.                                          00002440
           02 CCHOIX4H  PIC X.                                          00002450
           02 CCHOIX4V  PIC X.                                          00002460
           02 CCHOIX4O  PIC X.                                          00002470
           02 FILLER    PIC X(2).                                       00002480
           02 LCHOIX4A  PIC X.                                          00002490
           02 LCHOIX4C  PIC X.                                          00002500
           02 LCHOIX4P  PIC X.                                          00002510
           02 LCHOIX4H  PIC X.                                          00002520
           02 LCHOIX4V  PIC X.                                          00002530
           02 LCHOIX4O  PIC X(20).                                      00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MCHOIX5A  PIC X.                                          00002560
           02 MCHOIX5C  PIC X.                                          00002570
           02 MCHOIX5P  PIC X.                                          00002580
           02 MCHOIX5H  PIC X.                                          00002590
           02 MCHOIX5V  PIC X.                                          00002600
           02 MCHOIX5O  PIC X.                                          00002610
           02 FILLER    PIC X(2).                                       00002620
           02 CCHOIX5A  PIC X.                                          00002630
           02 CCHOIX5C  PIC X.                                          00002640
           02 CCHOIX5P  PIC X.                                          00002650
           02 CCHOIX5H  PIC X.                                          00002660
           02 CCHOIX5V  PIC X.                                          00002670
           02 CCHOIX5O  PIC X.                                          00002680
           02 FILLER    PIC X(2).                                       00002690
           02 LCHOIX5A  PIC X.                                          00002700
           02 LCHOIX5C  PIC X.                                          00002710
           02 LCHOIX5P  PIC X.                                          00002720
           02 LCHOIX5H  PIC X.                                          00002730
           02 LCHOIX5V  PIC X.                                          00002740
           02 LCHOIX5O  PIC X(20).                                      00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MCHOIX6A  PIC X.                                          00002770
           02 MCHOIX6C  PIC X.                                          00002780
           02 MCHOIX6P  PIC X.                                          00002790
           02 MCHOIX6H  PIC X.                                          00002800
           02 MCHOIX6V  PIC X.                                          00002810
           02 MCHOIX6O  PIC X.                                          00002820
           02 FILLER    PIC X(2).                                       00002830
           02 CCHOIX6A  PIC X.                                          00002840
           02 CCHOIX6C  PIC X.                                          00002850
           02 CCHOIX6P  PIC X.                                          00002860
           02 CCHOIX6H  PIC X.                                          00002870
           02 CCHOIX6V  PIC X.                                          00002880
           02 CCHOIX6O  PIC X.                                          00002890
           02 FILLER    PIC X(2).                                       00002900
           02 LCHOIX6A  PIC X.                                          00002910
           02 LCHOIX6C  PIC X.                                          00002920
           02 LCHOIX6P  PIC X.                                          00002930
           02 LCHOIX6H  PIC X.                                          00002940
           02 LCHOIX6V  PIC X.                                          00002950
           02 LCHOIX6O  PIC X(20).                                      00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MCHOIX7A  PIC X.                                          00002980
           02 MCHOIX7C  PIC X.                                          00002990
           02 MCHOIX7P  PIC X.                                          00003000
           02 MCHOIX7H  PIC X.                                          00003010
           02 MCHOIX7V  PIC X.                                          00003020
           02 MCHOIX7O  PIC X.                                          00003030
           02 FILLER    PIC X(2).                                       00003040
           02 CCHOIX7A  PIC X.                                          00003050
           02 CCHOIX7C  PIC X.                                          00003060
           02 CCHOIX7P  PIC X.                                          00003070
           02 CCHOIX7H  PIC X.                                          00003080
           02 CCHOIX7V  PIC X.                                          00003090
           02 CCHOIX7O  PIC X.                                          00003100
           02 FILLER    PIC X(2).                                       00003110
           02 LCHOIX7A  PIC X.                                          00003120
           02 LCHOIX7C  PIC X.                                          00003130
           02 LCHOIX7P  PIC X.                                          00003140
           02 LCHOIX7H  PIC X.                                          00003150
           02 LCHOIX7V  PIC X.                                          00003160
           02 LCHOIX7O  PIC X(20).                                      00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MCHOIX8A  PIC X.                                          00003190
           02 MCHOIX8C  PIC X.                                          00003200
           02 MCHOIX8P  PIC X.                                          00003210
           02 MCHOIX8H  PIC X.                                          00003220
           02 MCHOIX8V  PIC X.                                          00003230
           02 MCHOIX8O  PIC X.                                          00003240
           02 FILLER    PIC X(2).                                       00003250
           02 CCHOIX8A  PIC X.                                          00003260
           02 CCHOIX8C  PIC X.                                          00003270
           02 CCHOIX8P  PIC X.                                          00003280
           02 CCHOIX8H  PIC X.                                          00003290
           02 CCHOIX8V  PIC X.                                          00003300
           02 CCHOIX8O  PIC X.                                          00003310
           02 FILLER    PIC X(2).                                       00003320
           02 LCHOIX8A  PIC X.                                          00003330
           02 LCHOIX8C  PIC X.                                          00003340
           02 LCHOIX8P  PIC X.                                          00003350
           02 LCHOIX8H  PIC X.                                          00003360
           02 LCHOIX8V  PIC X.                                          00003370
           02 LCHOIX8O  PIC X(20).                                      00003380
           02 FILLER    PIC X(2).                                       00003390
           02 MLIBERRA  PIC X.                                          00003400
           02 MLIBERRC  PIC X.                                          00003410
           02 MLIBERRP  PIC X.                                          00003420
           02 MLIBERRH  PIC X.                                          00003430
           02 MLIBERRV  PIC X.                                          00003440
           02 MLIBERRO  PIC X(78).                                      00003450
           02 FILLER    PIC X(2).                                       00003460
           02 MCODTRAA  PIC X.                                          00003470
           02 MCODTRAC  PIC X.                                          00003480
           02 MCODTRAP  PIC X.                                          00003490
           02 MCODTRAH  PIC X.                                          00003500
           02 MCODTRAV  PIC X.                                          00003510
           02 MCODTRAO  PIC X(4).                                       00003520
           02 FILLER    PIC X(2).                                       00003530
           02 MCICSA    PIC X.                                          00003540
           02 MCICSC    PIC X.                                          00003550
           02 MCICSP    PIC X.                                          00003560
           02 MCICSH    PIC X.                                          00003570
           02 MCICSV    PIC X.                                          00003580
           02 MCICSO    PIC X(5).                                       00003590
           02 FILLER    PIC X(2).                                       00003600
           02 MNETNAMA  PIC X.                                          00003610
           02 MNETNAMC  PIC X.                                          00003620
           02 MNETNAMP  PIC X.                                          00003630
           02 MNETNAMH  PIC X.                                          00003640
           02 MNETNAMV  PIC X.                                          00003650
           02 MNETNAMO  PIC X(8).                                       00003660
           02 FILLER    PIC X(2).                                       00003670
           02 MSCREENA  PIC X.                                          00003680
           02 MSCREENC  PIC X.                                          00003690
           02 MSCREENP  PIC X.                                          00003700
           02 MSCREENH  PIC X.                                          00003710
           02 MSCREENV  PIC X.                                          00003720
           02 MSCREENO  PIC X(5).                                       00003730
                                                                                
