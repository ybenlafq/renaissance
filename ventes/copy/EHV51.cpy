      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHV51   EHV51                                              00000020
      ***************************************************************** 00000030
       01   EHV51I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * NUMERO DE PAGE COURANTE                                         00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOPAGEL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MNOPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOPAGEF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MNOPAGEI  PIC X(3).                                       00000200
      * NOMBRE TOTAL DE PAGES                                           00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGEL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPAGEF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBPAGEI  PIC X(3).                                       00000250
           02 MCRTBI OCCURS   4 TIMES .                                 00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MLCRTL     COMP PIC S9(4).                            00000270
      *--                                                                       
               04 MLCRTL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MLCRTF     PIC X.                                     00000280
               04 FILLER     PIC X(4).                                  00000290
               04 MLCRTI     PIC X(15).                                 00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MTCRTL     COMP PIC S9(4).                            00000310
      *--                                                                       
               04 MTCRTL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MTCRTF     PIC X.                                     00000320
               04 FILLER     PIC X(4).                                  00000330
               04 MTCRTI     PIC X(19).                                 00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MLCRT2L    COMP PIC S9(4).                            00000350
      *--                                                                       
               04 MLCRT2L COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MLCRT2F    PIC X.                                     00000360
               04 FILLER     PIC X(4).                                  00000370
               04 MLCRT2I    PIC X(15).                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MTCRT2L    COMP PIC S9(4).                            00000390
      *--                                                                       
               04 MTCRT2L COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MTCRT2F    PIC X.                                     00000400
               04 FILLER     PIC X(4).                                  00000410
               04 MTCRT2I    PIC X(19).                                 00000420
           02 MLIGNEI OCCURS   11 TIMES .                               00000430
      * ZONE DE SELECTION                                               00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSELECTL   COMP PIC S9(4).                            00000450
      *--                                                                       
               04 MSELECTL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MSELECTF   PIC X.                                     00000460
               04 FILLER     PIC X(4).                                  00000470
               04 MSELECTI   PIC X.                                     00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNMAGL     COMP PIC S9(4).                            00000490
      *--                                                                       
               04 MNMAGL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MNMAGF     PIC X.                                     00000500
               04 FILLER     PIC X(4).                                  00000510
               04 MNMAGI     PIC X(3).                                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNVENTEL   COMP PIC S9(4).                            00000530
      *--                                                                       
               04 MNVENTEL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MNVENTEF   PIC X.                                     00000540
               04 FILLER     PIC X(4).                                  00000550
               04 MNVENTEI   PIC X(7).                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MLNOML     COMP PIC S9(4).                            00000570
      *--                                                                       
               04 MLNOML COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MLNOMF     PIC X.                                     00000580
               04 FILLER     PIC X(4).                                  00000590
               04 MLNOMI     PIC X(20).                                 00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MLPRENOML  COMP PIC S9(4).                            00000610
      *--                                                                       
               04 MLPRENOML COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MLPRENOMF  PIC X.                                     00000620
               04 FILLER     PIC X(4).                                  00000630
               04 MLPRENOMI  PIC X(8).                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNTELL     COMP PIC S9(4).                            00000650
      *--                                                                       
               04 MNTELL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MNTELF     PIC X.                                     00000660
               04 FILLER     PIC X(4).                                  00000670
               04 MNTELI     PIC X(10).                                 00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCPOSTALL  COMP PIC S9(4).                            00000690
      *--                                                                       
               04 MCPOSTALL COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MCPOSTALF  PIC X.                                     00000700
               04 FILLER     PIC X(4).                                  00000710
               04 MCPOSTALI  PIC X(5).                                  00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MLCOMMUNEL      COMP PIC S9(4).                       00000730
      *--                                                                       
               04 MLCOMMUNEL COMP-5 PIC S9(4).                                  
      *}                                                                        
               04 MLCOMMUNEF      PIC X.                                00000740
               04 FILLER     PIC X(4).                                  00000750
               04 MLCOMMUNEI      PIC X(17).                            00000760
      * MESSAGE ERREUR                                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLIBERRI  PIC X(78).                                      00000810
      * CODE TRANSACTION                                                00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      * CICS DE TRAVAIL                                                 00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MCICSI    PIC X(5).                                       00000910
      * NETNAME                                                         00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000930
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000940
           02 FILLER    PIC X(4).                                       00000950
           02 MNETNAMI  PIC X(8).                                       00000960
      * CODE TERMINAL                                                   00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MSCREENI  PIC X(5).                                       00001010
      ***************************************************************** 00001020
      * SDF: EHV51   EHV51                                              00001030
      ***************************************************************** 00001040
       01   EHV51O REDEFINES EHV51I.                                    00001050
           02 FILLER    PIC X(12).                                      00001060
      * DATE DU JOUR                                                    00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MDATJOUA  PIC X.                                          00001090
           02 MDATJOUC  PIC X.                                          00001100
           02 MDATJOUP  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUV  PIC X.                                          00001130
           02 MDATJOUO  PIC X(10).                                      00001140
      * HEURE                                                           00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MTIMJOUA  PIC X.                                          00001170
           02 MTIMJOUC  PIC X.                                          00001180
           02 MTIMJOUP  PIC X.                                          00001190
           02 MTIMJOUH  PIC X.                                          00001200
           02 MTIMJOUV  PIC X.                                          00001210
           02 MTIMJOUO  PIC X(5).                                       00001220
      * NUMERO DE PAGE COURANTE                                         00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MNOPAGEA  PIC X.                                          00001250
           02 MNOPAGEC  PIC X.                                          00001260
           02 MNOPAGEP  PIC X.                                          00001270
           02 MNOPAGEH  PIC X.                                          00001280
           02 MNOPAGEV  PIC X.                                          00001290
           02 MNOPAGEO  PIC X(3).                                       00001300
      * NOMBRE TOTAL DE PAGES                                           00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MNBPAGEA  PIC X.                                          00001330
           02 MNBPAGEC  PIC X.                                          00001340
           02 MNBPAGEP  PIC X.                                          00001350
           02 MNBPAGEH  PIC X.                                          00001360
           02 MNBPAGEV  PIC X.                                          00001370
           02 MNBPAGEO  PIC X(3).                                       00001380
           02 MCRTBO OCCURS   4 TIMES .                                 00001390
               04 FILLER     PIC X(2).                                  00001400
               04 MLCRTA     PIC X.                                     00001410
               04 MLCRTC     PIC X.                                     00001420
               04 MLCRTP     PIC X.                                     00001430
               04 MLCRTH     PIC X.                                     00001440
               04 MLCRTV     PIC X.                                     00001450
               04 MLCRTO     PIC X(15).                                 00001460
               04 FILLER     PIC X(2).                                  00001470
               04 MTCRTA     PIC X.                                     00001480
               04 MTCRTC     PIC X.                                     00001490
               04 MTCRTP     PIC X.                                     00001500
               04 MTCRTH     PIC X.                                     00001510
               04 MTCRTV     PIC X.                                     00001520
               04 MTCRTO     PIC X(19).                                 00001530
               04 FILLER     PIC X(2).                                  00001540
               04 MLCRT2A    PIC X.                                     00001550
               04 MLCRT2C    PIC X.                                     00001560
               04 MLCRT2P    PIC X.                                     00001570
               04 MLCRT2H    PIC X.                                     00001580
               04 MLCRT2V    PIC X.                                     00001590
               04 MLCRT2O    PIC X(15).                                 00001600
               04 FILLER     PIC X(2).                                  00001610
               04 MTCRT2A    PIC X.                                     00001620
               04 MTCRT2C    PIC X.                                     00001630
               04 MTCRT2P    PIC X.                                     00001640
               04 MTCRT2H    PIC X.                                     00001650
               04 MTCRT2V    PIC X.                                     00001660
               04 MTCRT2O    PIC X(19).                                 00001670
           02 MLIGNEO OCCURS   11 TIMES .                               00001680
      * ZONE DE SELECTION                                               00001690
               04 FILLER     PIC X(2).                                  00001700
               04 MSELECTA   PIC X.                                     00001710
               04 MSELECTC   PIC X.                                     00001720
               04 MSELECTP   PIC X.                                     00001730
               04 MSELECTH   PIC X.                                     00001740
               04 MSELECTV   PIC X.                                     00001750
               04 MSELECTO   PIC X.                                     00001760
               04 FILLER     PIC X(2).                                  00001770
               04 MNMAGA     PIC X.                                     00001780
               04 MNMAGC     PIC X.                                     00001790
               04 MNMAGP     PIC X.                                     00001800
               04 MNMAGH     PIC X.                                     00001810
               04 MNMAGV     PIC X.                                     00001820
               04 MNMAGO     PIC X(3).                                  00001830
               04 FILLER     PIC X(2).                                  00001840
               04 MNVENTEA   PIC X.                                     00001850
               04 MNVENTEC   PIC X.                                     00001860
               04 MNVENTEP   PIC X.                                     00001870
               04 MNVENTEH   PIC X.                                     00001880
               04 MNVENTEV   PIC X.                                     00001890
               04 MNVENTEO   PIC X(7).                                  00001900
               04 FILLER     PIC X(2).                                  00001910
               04 MLNOMA     PIC X.                                     00001920
               04 MLNOMC     PIC X.                                     00001930
               04 MLNOMP     PIC X.                                     00001940
               04 MLNOMH     PIC X.                                     00001950
               04 MLNOMV     PIC X.                                     00001960
               04 MLNOMO     PIC X(20).                                 00001970
               04 FILLER     PIC X(2).                                  00001980
               04 MLPRENOMA  PIC X.                                     00001990
               04 MLPRENOMC  PIC X.                                     00002000
               04 MLPRENOMP  PIC X.                                     00002010
               04 MLPRENOMH  PIC X.                                     00002020
               04 MLPRENOMV  PIC X.                                     00002030
               04 MLPRENOMO  PIC X(8).                                  00002040
               04 FILLER     PIC X(2).                                  00002050
               04 MNTELA     PIC X.                                     00002060
               04 MNTELC     PIC X.                                     00002070
               04 MNTELP     PIC X.                                     00002080
               04 MNTELH     PIC X.                                     00002090
               04 MNTELV     PIC X.                                     00002100
               04 MNTELO     PIC X(10).                                 00002110
               04 FILLER     PIC X(2).                                  00002120
               04 MCPOSTALA  PIC X.                                     00002130
               04 MCPOSTALC  PIC X.                                     00002140
               04 MCPOSTALP  PIC X.                                     00002150
               04 MCPOSTALH  PIC X.                                     00002160
               04 MCPOSTALV  PIC X.                                     00002170
               04 MCPOSTALO  PIC X(5).                                  00002180
               04 FILLER     PIC X(2).                                  00002190
               04 MLCOMMUNEA      PIC X.                                00002200
               04 MLCOMMUNEC PIC X.                                     00002210
               04 MLCOMMUNEP PIC X.                                     00002220
               04 MLCOMMUNEH PIC X.                                     00002230
               04 MLCOMMUNEV PIC X.                                     00002240
               04 MLCOMMUNEO      PIC X(17).                            00002250
      * MESSAGE ERREUR                                                  00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MLIBERRA  PIC X.                                          00002280
           02 MLIBERRC  PIC X.                                          00002290
           02 MLIBERRP  PIC X.                                          00002300
           02 MLIBERRH  PIC X.                                          00002310
           02 MLIBERRV  PIC X.                                          00002320
           02 MLIBERRO  PIC X(78).                                      00002330
      * CODE TRANSACTION                                                00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MCODTRAA  PIC X.                                          00002360
           02 MCODTRAC  PIC X.                                          00002370
           02 MCODTRAP  PIC X.                                          00002380
           02 MCODTRAH  PIC X.                                          00002390
           02 MCODTRAV  PIC X.                                          00002400
           02 MCODTRAO  PIC X(4).                                       00002410
      * CICS DE TRAVAIL                                                 00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MCICSA    PIC X.                                          00002440
           02 MCICSC    PIC X.                                          00002450
           02 MCICSP    PIC X.                                          00002460
           02 MCICSH    PIC X.                                          00002470
           02 MCICSV    PIC X.                                          00002480
           02 MCICSO    PIC X(5).                                       00002490
      * NETNAME                                                         00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MNETNAMA  PIC X.                                          00002520
           02 MNETNAMC  PIC X.                                          00002530
           02 MNETNAMP  PIC X.                                          00002540
           02 MNETNAMH  PIC X.                                          00002550
           02 MNETNAMV  PIC X.                                          00002560
           02 MNETNAMO  PIC X(8).                                       00002570
      * CODE TERMINAL                                                   00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MSCREENA  PIC X.                                          00002600
           02 MSCREENC  PIC X.                                          00002610
           02 MSCREENP  PIC X.                                          00002620
           02 MSCREENH  PIC X.                                          00002630
           02 MSCREENV  PIC X.                                          00002640
           02 MSCREENO  PIC X(5).                                       00002650
                                                                                
