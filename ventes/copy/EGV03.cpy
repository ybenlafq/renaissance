      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV03   EGV03                                              00000020
      ***************************************************************** 00000030
       01   EGV03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMODDELL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MCMODDELL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCMODDELF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCMODDELI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCZONLIVL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCZONLIVL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCZONLIVF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCZONLIVI      PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEQUIPEL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCEQUIPEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCEQUIPEF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCEQUIPEI      PIC X(5).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPLAG1L  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCPLAG1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPLAG1F  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCPLAG1I  PIC X(2).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPLAG3L  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCPLAG3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPLAG3F  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCPLAG3I  PIC X(2).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPLAG5L  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCPLAG5L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPLAG5F  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCPLAG5I  PIC X(2).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPLAG7L  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCPLAG7L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPLAG7F  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCPLAG7I  PIC X(2).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPLAG9L  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCPLAG9L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPLAG9F  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCPLAG9I  PIC X(2).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPLAG1L  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLPLAG1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPLAG1F  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLPLAG1I  PIC X(8).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPLAG3L  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLPLAG3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPLAG3F  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLPLAG3I  PIC X(8).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPLAG5L  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLPLAG5L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPLAG5F  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLPLAG5I  PIC X(8).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPLAG7L  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLPLAG7L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPLAG7F  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLPLAG7I  PIC X(8).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPLAG9L  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLPLAG9L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPLAG9F  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLPLAG9I  PIC X(8).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPLAG2L  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCPLAG2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPLAG2F  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCPLAG2I  PIC X(2).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPLAG4L  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCPLAG4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPLAG4F  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCPLAG4I  PIC X(2).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPLAG6L  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCPLAG6L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPLAG6F  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCPLAG6I  PIC X(2).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPLAG8L  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCPLAG8L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPLAG8F  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCPLAG8I  PIC X(2).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPLAG0L  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCPLAG0L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPLAG0F  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCPLAG0I  PIC X(2).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPLAG2L  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MLPLAG2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPLAG2F  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLPLAG2I  PIC X(8).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPLAG4L  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MLPLAG4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPLAG4F  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MLPLAG4I  PIC X(8).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPLAG6L  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MLPLAG6L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPLAG6F  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MLPLAG6I  PIC X(8).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPLAG8L  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MLPLAG8L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPLAG8F  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MLPLAG8I  PIC X(8).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPLAG0L  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MLPLAG0L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPLAG0F  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MLPLAG0I  PIC X(8).                                       00001090
           02 M780I OCCURS   15 TIMES .                                 00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEL  COMP PIC S9(4).                                 00001110
      *--                                                                       
             03 MDATEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDATEF  PIC X.                                          00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MDATEI  PIC X(10).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBCL  COMP PIC S9(4).                                 00001150
      *--                                                                       
             03 MLIBCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLIBCF  PIC X.                                          00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MLIBCI  PIC X(3).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTA1L     COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MQUOTA1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQUOTA1F     PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MQUOTA1I     PIC X(5).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTA2L     COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MQUOTA2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQUOTA2F     PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MQUOTA2I     PIC X(5).                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTA3L     COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MQUOTA3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQUOTA3F     PIC X.                                     00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MQUOTA3I     PIC X(5).                                  00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTA4L     COMP PIC S9(4).                            00001310
      *--                                                                       
             03 MQUOTA4L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQUOTA4F     PIC X.                                     00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MQUOTA4I     PIC X(5).                                  00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTA5L     COMP PIC S9(4).                            00001350
      *--                                                                       
             03 MQUOTA5L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQUOTA5F     PIC X.                                     00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MQUOTA5I     PIC X(5).                                  00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTA6L     COMP PIC S9(4).                            00001390
      *--                                                                       
             03 MQUOTA6L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQUOTA6F     PIC X.                                     00001400
             03 FILLER  PIC X(4).                                       00001410
             03 MQUOTA6I     PIC X(5).                                  00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTA7L     COMP PIC S9(4).                            00001430
      *--                                                                       
             03 MQUOTA7L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQUOTA7F     PIC X.                                     00001440
             03 FILLER  PIC X(4).                                       00001450
             03 MQUOTA7I     PIC X(5).                                  00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTA8L     COMP PIC S9(4).                            00001470
      *--                                                                       
             03 MQUOTA8L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQUOTA8F     PIC X.                                     00001480
             03 FILLER  PIC X(4).                                       00001490
             03 MQUOTA8I     PIC X(5).                                  00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTA9L     COMP PIC S9(4).                            00001510
      *--                                                                       
             03 MQUOTA9L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQUOTA9F     PIC X.                                     00001520
             03 FILLER  PIC X(4).                                       00001530
             03 MQUOTA9I     PIC X(5).                                  00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTA0L     COMP PIC S9(4).                            00001550
      *--                                                                       
             03 MQUOTA0L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQUOTA0F     PIC X.                                     00001560
             03 FILLER  PIC X(4).                                       00001570
             03 MQUOTA0I     PIC X(5).                                  00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MZONCMDI  PIC X(15).                                      00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MLIBERRI  PIC X(58).                                      00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MCODTRAI  PIC X(4).                                       00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001710
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001720
           02 FILLER    PIC X(4).                                       00001730
           02 MCICSI    PIC X(5).                                       00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001750
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001760
           02 FILLER    PIC X(4).                                       00001770
           02 MNETNAMI  PIC X(8).                                       00001780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001800
           02 FILLER    PIC X(4).                                       00001810
           02 MSCREENI  PIC X(4).                                       00001820
      ***************************************************************** 00001830
      * SDF: EGV03   EGV03                                              00001840
      ***************************************************************** 00001850
       01   EGV03O REDEFINES EGV03I.                                    00001860
           02 FILLER    PIC X(12).                                      00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MDATJOUA  PIC X.                                          00001890
           02 MDATJOUC  PIC X.                                          00001900
           02 MDATJOUP  PIC X.                                          00001910
           02 MDATJOUH  PIC X.                                          00001920
           02 MDATJOUV  PIC X.                                          00001930
           02 MDATJOUO  PIC X(10).                                      00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MTIMJOUA  PIC X.                                          00001960
           02 MTIMJOUC  PIC X.                                          00001970
           02 MTIMJOUP  PIC X.                                          00001980
           02 MTIMJOUH  PIC X.                                          00001990
           02 MTIMJOUV  PIC X.                                          00002000
           02 MTIMJOUO  PIC X(5).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MWPAGEA   PIC X.                                          00002030
           02 MWPAGEC   PIC X.                                          00002040
           02 MWPAGEP   PIC X.                                          00002050
           02 MWPAGEH   PIC X.                                          00002060
           02 MWPAGEV   PIC X.                                          00002070
           02 MWPAGEO   PIC X(3).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MCMODDELA      PIC X.                                     00002100
           02 MCMODDELC PIC X.                                          00002110
           02 MCMODDELP PIC X.                                          00002120
           02 MCMODDELH PIC X.                                          00002130
           02 MCMODDELV PIC X.                                          00002140
           02 MCMODDELO      PIC X(3).                                  00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCZONLIVA      PIC X.                                     00002170
           02 MCZONLIVC PIC X.                                          00002180
           02 MCZONLIVP PIC X.                                          00002190
           02 MCZONLIVH PIC X.                                          00002200
           02 MCZONLIVV PIC X.                                          00002210
           02 MCZONLIVO      PIC X(5).                                  00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCEQUIPEA      PIC X.                                     00002240
           02 MCEQUIPEC PIC X.                                          00002250
           02 MCEQUIPEP PIC X.                                          00002260
           02 MCEQUIPEH PIC X.                                          00002270
           02 MCEQUIPEV PIC X.                                          00002280
           02 MCEQUIPEO      PIC X(5).                                  00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MCPLAG1A  PIC X.                                          00002310
           02 MCPLAG1C  PIC X.                                          00002320
           02 MCPLAG1P  PIC X.                                          00002330
           02 MCPLAG1H  PIC X.                                          00002340
           02 MCPLAG1V  PIC X.                                          00002350
           02 MCPLAG1O  PIC X(2).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MCPLAG3A  PIC X.                                          00002380
           02 MCPLAG3C  PIC X.                                          00002390
           02 MCPLAG3P  PIC X.                                          00002400
           02 MCPLAG3H  PIC X.                                          00002410
           02 MCPLAG3V  PIC X.                                          00002420
           02 MCPLAG3O  PIC X(2).                                       00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MCPLAG5A  PIC X.                                          00002450
           02 MCPLAG5C  PIC X.                                          00002460
           02 MCPLAG5P  PIC X.                                          00002470
           02 MCPLAG5H  PIC X.                                          00002480
           02 MCPLAG5V  PIC X.                                          00002490
           02 MCPLAG5O  PIC X(2).                                       00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MCPLAG7A  PIC X.                                          00002520
           02 MCPLAG7C  PIC X.                                          00002530
           02 MCPLAG7P  PIC X.                                          00002540
           02 MCPLAG7H  PIC X.                                          00002550
           02 MCPLAG7V  PIC X.                                          00002560
           02 MCPLAG7O  PIC X(2).                                       00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MCPLAG9A  PIC X.                                          00002590
           02 MCPLAG9C  PIC X.                                          00002600
           02 MCPLAG9P  PIC X.                                          00002610
           02 MCPLAG9H  PIC X.                                          00002620
           02 MCPLAG9V  PIC X.                                          00002630
           02 MCPLAG9O  PIC X(2).                                       00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MLPLAG1A  PIC X.                                          00002660
           02 MLPLAG1C  PIC X.                                          00002670
           02 MLPLAG1P  PIC X.                                          00002680
           02 MLPLAG1H  PIC X.                                          00002690
           02 MLPLAG1V  PIC X.                                          00002700
           02 MLPLAG1O  PIC X(8).                                       00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MLPLAG3A  PIC X.                                          00002730
           02 MLPLAG3C  PIC X.                                          00002740
           02 MLPLAG3P  PIC X.                                          00002750
           02 MLPLAG3H  PIC X.                                          00002760
           02 MLPLAG3V  PIC X.                                          00002770
           02 MLPLAG3O  PIC X(8).                                       00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MLPLAG5A  PIC X.                                          00002800
           02 MLPLAG5C  PIC X.                                          00002810
           02 MLPLAG5P  PIC X.                                          00002820
           02 MLPLAG5H  PIC X.                                          00002830
           02 MLPLAG5V  PIC X.                                          00002840
           02 MLPLAG5O  PIC X(8).                                       00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLPLAG7A  PIC X.                                          00002870
           02 MLPLAG7C  PIC X.                                          00002880
           02 MLPLAG7P  PIC X.                                          00002890
           02 MLPLAG7H  PIC X.                                          00002900
           02 MLPLAG7V  PIC X.                                          00002910
           02 MLPLAG7O  PIC X(8).                                       00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MLPLAG9A  PIC X.                                          00002940
           02 MLPLAG9C  PIC X.                                          00002950
           02 MLPLAG9P  PIC X.                                          00002960
           02 MLPLAG9H  PIC X.                                          00002970
           02 MLPLAG9V  PIC X.                                          00002980
           02 MLPLAG9O  PIC X(8).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCPLAG2A  PIC X.                                          00003010
           02 MCPLAG2C  PIC X.                                          00003020
           02 MCPLAG2P  PIC X.                                          00003030
           02 MCPLAG2H  PIC X.                                          00003040
           02 MCPLAG2V  PIC X.                                          00003050
           02 MCPLAG2O  PIC X(2).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MCPLAG4A  PIC X.                                          00003080
           02 MCPLAG4C  PIC X.                                          00003090
           02 MCPLAG4P  PIC X.                                          00003100
           02 MCPLAG4H  PIC X.                                          00003110
           02 MCPLAG4V  PIC X.                                          00003120
           02 MCPLAG4O  PIC X(2).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MCPLAG6A  PIC X.                                          00003150
           02 MCPLAG6C  PIC X.                                          00003160
           02 MCPLAG6P  PIC X.                                          00003170
           02 MCPLAG6H  PIC X.                                          00003180
           02 MCPLAG6V  PIC X.                                          00003190
           02 MCPLAG6O  PIC X(2).                                       00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MCPLAG8A  PIC X.                                          00003220
           02 MCPLAG8C  PIC X.                                          00003230
           02 MCPLAG8P  PIC X.                                          00003240
           02 MCPLAG8H  PIC X.                                          00003250
           02 MCPLAG8V  PIC X.                                          00003260
           02 MCPLAG8O  PIC X(2).                                       00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MCPLAG0A  PIC X.                                          00003290
           02 MCPLAG0C  PIC X.                                          00003300
           02 MCPLAG0P  PIC X.                                          00003310
           02 MCPLAG0H  PIC X.                                          00003320
           02 MCPLAG0V  PIC X.                                          00003330
           02 MCPLAG0O  PIC X(2).                                       00003340
           02 FILLER    PIC X(2).                                       00003350
           02 MLPLAG2A  PIC X.                                          00003360
           02 MLPLAG2C  PIC X.                                          00003370
           02 MLPLAG2P  PIC X.                                          00003380
           02 MLPLAG2H  PIC X.                                          00003390
           02 MLPLAG2V  PIC X.                                          00003400
           02 MLPLAG2O  PIC X(8).                                       00003410
           02 FILLER    PIC X(2).                                       00003420
           02 MLPLAG4A  PIC X.                                          00003430
           02 MLPLAG4C  PIC X.                                          00003440
           02 MLPLAG4P  PIC X.                                          00003450
           02 MLPLAG4H  PIC X.                                          00003460
           02 MLPLAG4V  PIC X.                                          00003470
           02 MLPLAG4O  PIC X(8).                                       00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MLPLAG6A  PIC X.                                          00003500
           02 MLPLAG6C  PIC X.                                          00003510
           02 MLPLAG6P  PIC X.                                          00003520
           02 MLPLAG6H  PIC X.                                          00003530
           02 MLPLAG6V  PIC X.                                          00003540
           02 MLPLAG6O  PIC X(8).                                       00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MLPLAG8A  PIC X.                                          00003570
           02 MLPLAG8C  PIC X.                                          00003580
           02 MLPLAG8P  PIC X.                                          00003590
           02 MLPLAG8H  PIC X.                                          00003600
           02 MLPLAG8V  PIC X.                                          00003610
           02 MLPLAG8O  PIC X(8).                                       00003620
           02 FILLER    PIC X(2).                                       00003630
           02 MLPLAG0A  PIC X.                                          00003640
           02 MLPLAG0C  PIC X.                                          00003650
           02 MLPLAG0P  PIC X.                                          00003660
           02 MLPLAG0H  PIC X.                                          00003670
           02 MLPLAG0V  PIC X.                                          00003680
           02 MLPLAG0O  PIC X(8).                                       00003690
           02 M780O OCCURS   15 TIMES .                                 00003700
             03 FILLER       PIC X(2).                                  00003710
             03 MDATEA  PIC X.                                          00003720
             03 MDATEC  PIC X.                                          00003730
             03 MDATEP  PIC X.                                          00003740
             03 MDATEH  PIC X.                                          00003750
             03 MDATEV  PIC X.                                          00003760
             03 MDATEO  PIC X(10).                                      00003770
             03 FILLER       PIC X(2).                                  00003780
             03 MLIBCA  PIC X.                                          00003790
             03 MLIBCC  PIC X.                                          00003800
             03 MLIBCP  PIC X.                                          00003810
             03 MLIBCH  PIC X.                                          00003820
             03 MLIBCV  PIC X.                                          00003830
             03 MLIBCO  PIC X(3).                                       00003840
             03 FILLER       PIC X(2).                                  00003850
             03 MQUOTA1A     PIC X.                                     00003860
             03 MQUOTA1C     PIC X.                                     00003870
             03 MQUOTA1P     PIC X.                                     00003880
             03 MQUOTA1H     PIC X.                                     00003890
             03 MQUOTA1V     PIC X.                                     00003900
             03 MQUOTA1O     PIC X(5).                                  00003910
             03 FILLER       PIC X(2).                                  00003920
             03 MQUOTA2A     PIC X.                                     00003930
             03 MQUOTA2C     PIC X.                                     00003940
             03 MQUOTA2P     PIC X.                                     00003950
             03 MQUOTA2H     PIC X.                                     00003960
             03 MQUOTA2V     PIC X.                                     00003970
             03 MQUOTA2O     PIC X(5).                                  00003980
             03 FILLER       PIC X(2).                                  00003990
             03 MQUOTA3A     PIC X.                                     00004000
             03 MQUOTA3C     PIC X.                                     00004010
             03 MQUOTA3P     PIC X.                                     00004020
             03 MQUOTA3H     PIC X.                                     00004030
             03 MQUOTA3V     PIC X.                                     00004040
             03 MQUOTA3O     PIC X(5).                                  00004050
             03 FILLER       PIC X(2).                                  00004060
             03 MQUOTA4A     PIC X.                                     00004070
             03 MQUOTA4C     PIC X.                                     00004080
             03 MQUOTA4P     PIC X.                                     00004090
             03 MQUOTA4H     PIC X.                                     00004100
             03 MQUOTA4V     PIC X.                                     00004110
             03 MQUOTA4O     PIC X(5).                                  00004120
             03 FILLER       PIC X(2).                                  00004130
             03 MQUOTA5A     PIC X.                                     00004140
             03 MQUOTA5C     PIC X.                                     00004150
             03 MQUOTA5P     PIC X.                                     00004160
             03 MQUOTA5H     PIC X.                                     00004170
             03 MQUOTA5V     PIC X.                                     00004180
             03 MQUOTA5O     PIC X(5).                                  00004190
             03 FILLER       PIC X(2).                                  00004200
             03 MQUOTA6A     PIC X.                                     00004210
             03 MQUOTA6C     PIC X.                                     00004220
             03 MQUOTA6P     PIC X.                                     00004230
             03 MQUOTA6H     PIC X.                                     00004240
             03 MQUOTA6V     PIC X.                                     00004250
             03 MQUOTA6O     PIC X(5).                                  00004260
             03 FILLER       PIC X(2).                                  00004270
             03 MQUOTA7A     PIC X.                                     00004280
             03 MQUOTA7C     PIC X.                                     00004290
             03 MQUOTA7P     PIC X.                                     00004300
             03 MQUOTA7H     PIC X.                                     00004310
             03 MQUOTA7V     PIC X.                                     00004320
             03 MQUOTA7O     PIC X(5).                                  00004330
             03 FILLER       PIC X(2).                                  00004340
             03 MQUOTA8A     PIC X.                                     00004350
             03 MQUOTA8C     PIC X.                                     00004360
             03 MQUOTA8P     PIC X.                                     00004370
             03 MQUOTA8H     PIC X.                                     00004380
             03 MQUOTA8V     PIC X.                                     00004390
             03 MQUOTA8O     PIC X(5).                                  00004400
             03 FILLER       PIC X(2).                                  00004410
             03 MQUOTA9A     PIC X.                                     00004420
             03 MQUOTA9C     PIC X.                                     00004430
             03 MQUOTA9P     PIC X.                                     00004440
             03 MQUOTA9H     PIC X.                                     00004450
             03 MQUOTA9V     PIC X.                                     00004460
             03 MQUOTA9O     PIC X(5).                                  00004470
             03 FILLER       PIC X(2).                                  00004480
             03 MQUOTA0A     PIC X.                                     00004490
             03 MQUOTA0C     PIC X.                                     00004500
             03 MQUOTA0P     PIC X.                                     00004510
             03 MQUOTA0H     PIC X.                                     00004520
             03 MQUOTA0V     PIC X.                                     00004530
             03 MQUOTA0O     PIC X(5).                                  00004540
           02 FILLER    PIC X(2).                                       00004550
           02 MZONCMDA  PIC X.                                          00004560
           02 MZONCMDC  PIC X.                                          00004570
           02 MZONCMDP  PIC X.                                          00004580
           02 MZONCMDH  PIC X.                                          00004590
           02 MZONCMDV  PIC X.                                          00004600
           02 MZONCMDO  PIC X(15).                                      00004610
           02 FILLER    PIC X(2).                                       00004620
           02 MLIBERRA  PIC X.                                          00004630
           02 MLIBERRC  PIC X.                                          00004640
           02 MLIBERRP  PIC X.                                          00004650
           02 MLIBERRH  PIC X.                                          00004660
           02 MLIBERRV  PIC X.                                          00004670
           02 MLIBERRO  PIC X(58).                                      00004680
           02 FILLER    PIC X(2).                                       00004690
           02 MCODTRAA  PIC X.                                          00004700
           02 MCODTRAC  PIC X.                                          00004710
           02 MCODTRAP  PIC X.                                          00004720
           02 MCODTRAH  PIC X.                                          00004730
           02 MCODTRAV  PIC X.                                          00004740
           02 MCODTRAO  PIC X(4).                                       00004750
           02 FILLER    PIC X(2).                                       00004760
           02 MCICSA    PIC X.                                          00004770
           02 MCICSC    PIC X.                                          00004780
           02 MCICSP    PIC X.                                          00004790
           02 MCICSH    PIC X.                                          00004800
           02 MCICSV    PIC X.                                          00004810
           02 MCICSO    PIC X(5).                                       00004820
           02 FILLER    PIC X(2).                                       00004830
           02 MNETNAMA  PIC X.                                          00004840
           02 MNETNAMC  PIC X.                                          00004850
           02 MNETNAMP  PIC X.                                          00004860
           02 MNETNAMH  PIC X.                                          00004870
           02 MNETNAMV  PIC X.                                          00004880
           02 MNETNAMO  PIC X(8).                                       00004890
           02 FILLER    PIC X(2).                                       00004900
           02 MSCREENA  PIC X.                                          00004910
           02 MSCREENC  PIC X.                                          00004920
           02 MSCREENP  PIC X.                                          00004930
           02 MSCREENH  PIC X.                                          00004940
           02 MSCREENV  PIC X.                                          00004950
           02 MSCREENO  PIC X(4).                                       00004960
                                                                                
