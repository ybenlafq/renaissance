      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHV53   EHV53                                              00000020
      ***************************************************************** 00000030
       01   EHV53I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAGL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MMAGL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MMAGF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MMAGI     PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNVENTEI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIENTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCLIENTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLIENTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCLIENTI  PIC X(9).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVENTEL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDVENTEF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDVENTEI  PIC X(8).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNORDREL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNORDREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNORDREF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNORDREI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMCL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNOMCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNOMCF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNOMCI    PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMCL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLNOMCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLNOMCF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLNOMCI   PIC X(25).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPNOMCL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLPNOMCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPNOMCF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLPNOMCI  PIC X(15).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVOIECL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNVOIECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVOIECF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNVOIECI  PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTPVOIECL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MTPVOIECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTPVOIECF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MTPVOIECI      PIC X(4).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVOIECL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLVOIECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLVOIECF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLVOIECI  PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCBATCL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCBATCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCBATCF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCBATCI   PIC X(3).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCESCCL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCESCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCESCCF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCESCCI   PIC X(3).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCETAGCL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCETAGCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCETAGCF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCETAGCI  PIC X(3).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPORTECL      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MCPORTECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPORTECF      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCPORTECI      PIC X(3).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLADDRCL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLADDRCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLADDRCF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLADDRCI  PIC X(32).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTCL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCPOSTCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPOSTCF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCPOSTCI  PIC X(5).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCOMNCL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCCOMNCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCCOMNCF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCCOMNCI  PIC X(32).                                      00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPOSTCL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MLPOSTCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPOSTCF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLPOSTCI  PIC X(26).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD10L  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MTELD10L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD10F  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MTELD10I  PIC X(2).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD11L  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MTELD11L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD11F  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MTELD11I  PIC X(2).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD12L  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MTELD12L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD12F  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MTELD12I  PIC X(2).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD13L  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MTELD13L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD13F  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MTELD13I  PIC X(2).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD14L  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MTELD14L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD14F  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MTELD14I  PIC X(2).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB10L  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MTELB10L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB10F  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MTELB10I  PIC X(2).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB11L  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MTELB11L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB11F  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MTELB11I  PIC X(2).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB12L  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MTELB12L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB12F  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MTELB12I  PIC X(2).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB13L  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MTELB13L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB13F  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MTELB13I  PIC X(2).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB14L  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MTELB14L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB14F  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MTELB14I  PIC X(2).                                       00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPOSTEL   COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MPOSTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPOSTEF   PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MPOSTEI   PIC X(5).                                       00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMLL    COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MNOMLL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNOMLF    PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MNOMLI    PIC X(5).                                       00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMLL   COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MLNOMLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLNOMLF   PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MLNOMLI   PIC X(25).                                      00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPNOMLL  COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MLPNOMLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPNOMLF  PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MLPNOMLI  PIC X(15).                                      00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVOIELL  COMP PIC S9(4).                                 00001500
      *--                                                                       
           02 MNVOIELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVOIELF  PIC X.                                          00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MNVOIELI  PIC X(5).                                       00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTPVOIELL      COMP PIC S9(4).                            00001540
      *--                                                                       
           02 MTPVOIELL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTPVOIELF      PIC X.                                     00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MTPVOIELI      PIC X(4).                                  00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVOIELL  COMP PIC S9(4).                                 00001580
      *--                                                                       
           02 MLVOIELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLVOIELF  PIC X.                                          00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MLVOIELI  PIC X(20).                                      00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCBATLL   COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MCBATLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCBATLF   PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MCBATLI   PIC X(3).                                       00001650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCESCLL   COMP PIC S9(4).                                 00001660
      *--                                                                       
           02 MCESCLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCESCLF   PIC X.                                          00001670
           02 FILLER    PIC X(4).                                       00001680
           02 MCESCLI   PIC X(3).                                       00001690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCETAGLL  COMP PIC S9(4).                                 00001700
      *--                                                                       
           02 MCETAGLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCETAGLF  PIC X.                                          00001710
           02 FILLER    PIC X(4).                                       00001720
           02 MCETAGLI  PIC X(3).                                       00001730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPORTELL      COMP PIC S9(4).                            00001740
      *--                                                                       
           02 MCPORTELL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPORTELF      PIC X.                                     00001750
           02 FILLER    PIC X(4).                                       00001760
           02 MCPORTELI      PIC X(3).                                  00001770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLADDRLL  COMP PIC S9(4).                                 00001780
      *--                                                                       
           02 MLADDRLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLADDRLF  PIC X.                                          00001790
           02 FILLER    PIC X(4).                                       00001800
           02 MLADDRLI  PIC X(32).                                      00001810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTLL  COMP PIC S9(4).                                 00001820
      *--                                                                       
           02 MCPOSTLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPOSTLF  PIC X.                                          00001830
           02 FILLER    PIC X(4).                                       00001840
           02 MCPOSTLI  PIC X(5).                                       00001850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCOMNLL  COMP PIC S9(4).                                 00001860
      *--                                                                       
           02 MCCOMNLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCCOMNLF  PIC X.                                          00001870
           02 FILLER    PIC X(4).                                       00001880
           02 MCCOMNLI  PIC X(32).                                      00001890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPOSTLL  COMP PIC S9(4).                                 00001900
      *--                                                                       
           02 MLPOSTLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPOSTLF  PIC X.                                          00001910
           02 FILLER    PIC X(4).                                       00001920
           02 MLPOSTLI  PIC X(26).                                      00001930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD20L  COMP PIC S9(4).                                 00001940
      *--                                                                       
           02 MTELD20L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD20F  PIC X.                                          00001950
           02 FILLER    PIC X(4).                                       00001960
           02 MTELD20I  PIC X(2).                                       00001970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD21L  COMP PIC S9(4).                                 00001980
      *--                                                                       
           02 MTELD21L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD21F  PIC X.                                          00001990
           02 FILLER    PIC X(4).                                       00002000
           02 MTELD21I  PIC X(2).                                       00002010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD22L  COMP PIC S9(4).                                 00002020
      *--                                                                       
           02 MTELD22L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD22F  PIC X.                                          00002030
           02 FILLER    PIC X(4).                                       00002040
           02 MTELD22I  PIC X(2).                                       00002050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD23L  COMP PIC S9(4).                                 00002060
      *--                                                                       
           02 MTELD23L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD23F  PIC X.                                          00002070
           02 FILLER    PIC X(4).                                       00002080
           02 MTELD23I  PIC X(2).                                       00002090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD24L  COMP PIC S9(4).                                 00002100
      *--                                                                       
           02 MTELD24L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD24F  PIC X.                                          00002110
           02 FILLER    PIC X(4).                                       00002120
           02 MTELD24I  PIC X(2).                                       00002130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB20L  COMP PIC S9(4).                                 00002140
      *--                                                                       
           02 MTELB20L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB20F  PIC X.                                          00002150
           02 FILLER    PIC X(4).                                       00002160
           02 MTELB20I  PIC X(2).                                       00002170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB21L  COMP PIC S9(4).                                 00002180
      *--                                                                       
           02 MTELB21L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB21F  PIC X.                                          00002190
           02 FILLER    PIC X(4).                                       00002200
           02 MTELB21I  PIC X(2).                                       00002210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB22L  COMP PIC S9(4).                                 00002220
      *--                                                                       
           02 MTELB22L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB22F  PIC X.                                          00002230
           02 FILLER    PIC X(4).                                       00002240
           02 MTELB22I  PIC X(2).                                       00002250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB23L  COMP PIC S9(4).                                 00002260
      *--                                                                       
           02 MTELB23L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB23F  PIC X.                                          00002270
           02 FILLER    PIC X(4).                                       00002280
           02 MTELB23I  PIC X(2).                                       00002290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB24L  COMP PIC S9(4).                                 00002300
      *--                                                                       
           02 MTELB24L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB24F  PIC X.                                          00002310
           02 FILLER    PIC X(4).                                       00002320
           02 MTELB24I  PIC X(2).                                       00002330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPOSTELL  COMP PIC S9(4).                                 00002340
      *--                                                                       
           02 MPOSTELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPOSTELF  PIC X.                                          00002350
           02 FILLER    PIC X(4).                                       00002360
           02 MPOSTELI  PIC X(5).                                       00002370
           02 MLLIVRI OCCURS   2 TIMES .                                00002380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMENL     COMP PIC S9(4).                            00002390
      *--                                                                       
             03 MCOMMENL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCOMMENF     PIC X.                                     00002400
             03 FILLER  PIC X(4).                                       00002410
             03 MCOMMENI     PIC X(78).                                 00002420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00002430
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00002440
           02 FILLER    PIC X(4).                                       00002450
           02 MZONCMDI  PIC X(15).                                      00002460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00002470
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00002480
           02 FILLER    PIC X(4).                                       00002490
           02 MLIBERRI  PIC X(58).                                      00002500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00002510
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00002520
           02 FILLER    PIC X(4).                                       00002530
           02 MCODTRAI  PIC X(4).                                       00002540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00002550
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00002560
           02 FILLER    PIC X(4).                                       00002570
           02 MCICSI    PIC X(5).                                       00002580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00002590
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002600
           02 FILLER    PIC X(4).                                       00002610
           02 MNETNAMI  PIC X(8).                                       00002620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002630
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002640
           02 FILLER    PIC X(4).                                       00002650
           02 MSCREENI  PIC X(4).                                       00002660
      ***************************************************************** 00002670
      * SDF: EHV53   EHV53                                              00002680
      ***************************************************************** 00002690
       01   EHV53O REDEFINES EHV53I.                                    00002700
           02 FILLER    PIC X(12).                                      00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MDATJOUA  PIC X.                                          00002730
           02 MDATJOUC  PIC X.                                          00002740
           02 MDATJOUP  PIC X.                                          00002750
           02 MDATJOUH  PIC X.                                          00002760
           02 MDATJOUV  PIC X.                                          00002770
           02 MDATJOUO  PIC X(10).                                      00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MTIMJOUA  PIC X.                                          00002800
           02 MTIMJOUC  PIC X.                                          00002810
           02 MTIMJOUP  PIC X.                                          00002820
           02 MTIMJOUH  PIC X.                                          00002830
           02 MTIMJOUV  PIC X.                                          00002840
           02 MTIMJOUO  PIC X(5).                                       00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MWPAGEA   PIC X.                                          00002870
           02 MWPAGEC   PIC X.                                          00002880
           02 MWPAGEP   PIC X.                                          00002890
           02 MWPAGEH   PIC X.                                          00002900
           02 MWPAGEV   PIC X.                                          00002910
           02 MWPAGEO   PIC X(3).                                       00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MMAGA     PIC X.                                          00002940
           02 MMAGC     PIC X.                                          00002950
           02 MMAGP     PIC X.                                          00002960
           02 MMAGH     PIC X.                                          00002970
           02 MMAGV     PIC X.                                          00002980
           02 MMAGO     PIC X(3).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MNVENTEA  PIC X.                                          00003010
           02 MNVENTEC  PIC X.                                          00003020
           02 MNVENTEP  PIC X.                                          00003030
           02 MNVENTEH  PIC X.                                          00003040
           02 MNVENTEV  PIC X.                                          00003050
           02 MNVENTEO  PIC X(7).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MCLIENTA  PIC X.                                          00003080
           02 MCLIENTC  PIC X.                                          00003090
           02 MCLIENTP  PIC X.                                          00003100
           02 MCLIENTH  PIC X.                                          00003110
           02 MCLIENTV  PIC X.                                          00003120
           02 MCLIENTO  PIC X(9).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MDVENTEA  PIC X.                                          00003150
           02 MDVENTEC  PIC X.                                          00003160
           02 MDVENTEP  PIC X.                                          00003170
           02 MDVENTEH  PIC X.                                          00003180
           02 MDVENTEV  PIC X.                                          00003190
           02 MDVENTEO  PIC X(8).                                       00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MNORDREA  PIC X.                                          00003220
           02 MNORDREC  PIC X.                                          00003230
           02 MNORDREP  PIC X.                                          00003240
           02 MNORDREH  PIC X.                                          00003250
           02 MNORDREV  PIC X.                                          00003260
           02 MNORDREO  PIC X(5).                                       00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MNOMCA    PIC X.                                          00003290
           02 MNOMCC    PIC X.                                          00003300
           02 MNOMCP    PIC X.                                          00003310
           02 MNOMCH    PIC X.                                          00003320
           02 MNOMCV    PIC X.                                          00003330
           02 MNOMCO    PIC X(5).                                       00003340
           02 FILLER    PIC X(2).                                       00003350
           02 MLNOMCA   PIC X.                                          00003360
           02 MLNOMCC   PIC X.                                          00003370
           02 MLNOMCP   PIC X.                                          00003380
           02 MLNOMCH   PIC X.                                          00003390
           02 MLNOMCV   PIC X.                                          00003400
           02 MLNOMCO   PIC X(25).                                      00003410
           02 FILLER    PIC X(2).                                       00003420
           02 MLPNOMCA  PIC X.                                          00003430
           02 MLPNOMCC  PIC X.                                          00003440
           02 MLPNOMCP  PIC X.                                          00003450
           02 MLPNOMCH  PIC X.                                          00003460
           02 MLPNOMCV  PIC X.                                          00003470
           02 MLPNOMCO  PIC X(15).                                      00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MNVOIECA  PIC X.                                          00003500
           02 MNVOIECC  PIC X.                                          00003510
           02 MNVOIECP  PIC X.                                          00003520
           02 MNVOIECH  PIC X.                                          00003530
           02 MNVOIECV  PIC X.                                          00003540
           02 MNVOIECO  PIC X(5).                                       00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MTPVOIECA      PIC X.                                     00003570
           02 MTPVOIECC PIC X.                                          00003580
           02 MTPVOIECP PIC X.                                          00003590
           02 MTPVOIECH PIC X.                                          00003600
           02 MTPVOIECV PIC X.                                          00003610
           02 MTPVOIECO      PIC X(4).                                  00003620
           02 FILLER    PIC X(2).                                       00003630
           02 MLVOIECA  PIC X.                                          00003640
           02 MLVOIECC  PIC X.                                          00003650
           02 MLVOIECP  PIC X.                                          00003660
           02 MLVOIECH  PIC X.                                          00003670
           02 MLVOIECV  PIC X.                                          00003680
           02 MLVOIECO  PIC X(20).                                      00003690
           02 FILLER    PIC X(2).                                       00003700
           02 MCBATCA   PIC X.                                          00003710
           02 MCBATCC   PIC X.                                          00003720
           02 MCBATCP   PIC X.                                          00003730
           02 MCBATCH   PIC X.                                          00003740
           02 MCBATCV   PIC X.                                          00003750
           02 MCBATCO   PIC X(3).                                       00003760
           02 FILLER    PIC X(2).                                       00003770
           02 MCESCCA   PIC X.                                          00003780
           02 MCESCCC   PIC X.                                          00003790
           02 MCESCCP   PIC X.                                          00003800
           02 MCESCCH   PIC X.                                          00003810
           02 MCESCCV   PIC X.                                          00003820
           02 MCESCCO   PIC X(3).                                       00003830
           02 FILLER    PIC X(2).                                       00003840
           02 MCETAGCA  PIC X.                                          00003850
           02 MCETAGCC  PIC X.                                          00003860
           02 MCETAGCP  PIC X.                                          00003870
           02 MCETAGCH  PIC X.                                          00003880
           02 MCETAGCV  PIC X.                                          00003890
           02 MCETAGCO  PIC X(3).                                       00003900
           02 FILLER    PIC X(2).                                       00003910
           02 MCPORTECA      PIC X.                                     00003920
           02 MCPORTECC PIC X.                                          00003930
           02 MCPORTECP PIC X.                                          00003940
           02 MCPORTECH PIC X.                                          00003950
           02 MCPORTECV PIC X.                                          00003960
           02 MCPORTECO      PIC X(3).                                  00003970
           02 FILLER    PIC X(2).                                       00003980
           02 MLADDRCA  PIC X.                                          00003990
           02 MLADDRCC  PIC X.                                          00004000
           02 MLADDRCP  PIC X.                                          00004010
           02 MLADDRCH  PIC X.                                          00004020
           02 MLADDRCV  PIC X.                                          00004030
           02 MLADDRCO  PIC X(32).                                      00004040
           02 FILLER    PIC X(2).                                       00004050
           02 MCPOSTCA  PIC X.                                          00004060
           02 MCPOSTCC  PIC X.                                          00004070
           02 MCPOSTCP  PIC X.                                          00004080
           02 MCPOSTCH  PIC X.                                          00004090
           02 MCPOSTCV  PIC X.                                          00004100
           02 MCPOSTCO  PIC X(5).                                       00004110
           02 FILLER    PIC X(2).                                       00004120
           02 MCCOMNCA  PIC X.                                          00004130
           02 MCCOMNCC  PIC X.                                          00004140
           02 MCCOMNCP  PIC X.                                          00004150
           02 MCCOMNCH  PIC X.                                          00004160
           02 MCCOMNCV  PIC X.                                          00004170
           02 MCCOMNCO  PIC X(32).                                      00004180
           02 FILLER    PIC X(2).                                       00004190
           02 MLPOSTCA  PIC X.                                          00004200
           02 MLPOSTCC  PIC X.                                          00004210
           02 MLPOSTCP  PIC X.                                          00004220
           02 MLPOSTCH  PIC X.                                          00004230
           02 MLPOSTCV  PIC X.                                          00004240
           02 MLPOSTCO  PIC X(26).                                      00004250
           02 FILLER    PIC X(2).                                       00004260
           02 MTELD10A  PIC X.                                          00004270
           02 MTELD10C  PIC X.                                          00004280
           02 MTELD10P  PIC X.                                          00004290
           02 MTELD10H  PIC X.                                          00004300
           02 MTELD10V  PIC X.                                          00004310
           02 MTELD10O  PIC X(2).                                       00004320
           02 FILLER    PIC X(2).                                       00004330
           02 MTELD11A  PIC X.                                          00004340
           02 MTELD11C  PIC X.                                          00004350
           02 MTELD11P  PIC X.                                          00004360
           02 MTELD11H  PIC X.                                          00004370
           02 MTELD11V  PIC X.                                          00004380
           02 MTELD11O  PIC X(2).                                       00004390
           02 FILLER    PIC X(2).                                       00004400
           02 MTELD12A  PIC X.                                          00004410
           02 MTELD12C  PIC X.                                          00004420
           02 MTELD12P  PIC X.                                          00004430
           02 MTELD12H  PIC X.                                          00004440
           02 MTELD12V  PIC X.                                          00004450
           02 MTELD12O  PIC X(2).                                       00004460
           02 FILLER    PIC X(2).                                       00004470
           02 MTELD13A  PIC X.                                          00004480
           02 MTELD13C  PIC X.                                          00004490
           02 MTELD13P  PIC X.                                          00004500
           02 MTELD13H  PIC X.                                          00004510
           02 MTELD13V  PIC X.                                          00004520
           02 MTELD13O  PIC X(2).                                       00004530
           02 FILLER    PIC X(2).                                       00004540
           02 MTELD14A  PIC X.                                          00004550
           02 MTELD14C  PIC X.                                          00004560
           02 MTELD14P  PIC X.                                          00004570
           02 MTELD14H  PIC X.                                          00004580
           02 MTELD14V  PIC X.                                          00004590
           02 MTELD14O  PIC X(2).                                       00004600
           02 FILLER    PIC X(2).                                       00004610
           02 MTELB10A  PIC X.                                          00004620
           02 MTELB10C  PIC X.                                          00004630
           02 MTELB10P  PIC X.                                          00004640
           02 MTELB10H  PIC X.                                          00004650
           02 MTELB10V  PIC X.                                          00004660
           02 MTELB10O  PIC X(2).                                       00004670
           02 FILLER    PIC X(2).                                       00004680
           02 MTELB11A  PIC X.                                          00004690
           02 MTELB11C  PIC X.                                          00004700
           02 MTELB11P  PIC X.                                          00004710
           02 MTELB11H  PIC X.                                          00004720
           02 MTELB11V  PIC X.                                          00004730
           02 MTELB11O  PIC X(2).                                       00004740
           02 FILLER    PIC X(2).                                       00004750
           02 MTELB12A  PIC X.                                          00004760
           02 MTELB12C  PIC X.                                          00004770
           02 MTELB12P  PIC X.                                          00004780
           02 MTELB12H  PIC X.                                          00004790
           02 MTELB12V  PIC X.                                          00004800
           02 MTELB12O  PIC X(2).                                       00004810
           02 FILLER    PIC X(2).                                       00004820
           02 MTELB13A  PIC X.                                          00004830
           02 MTELB13C  PIC X.                                          00004840
           02 MTELB13P  PIC X.                                          00004850
           02 MTELB13H  PIC X.                                          00004860
           02 MTELB13V  PIC X.                                          00004870
           02 MTELB13O  PIC X(2).                                       00004880
           02 FILLER    PIC X(2).                                       00004890
           02 MTELB14A  PIC X.                                          00004900
           02 MTELB14C  PIC X.                                          00004910
           02 MTELB14P  PIC X.                                          00004920
           02 MTELB14H  PIC X.                                          00004930
           02 MTELB14V  PIC X.                                          00004940
           02 MTELB14O  PIC X(2).                                       00004950
           02 FILLER    PIC X(2).                                       00004960
           02 MPOSTEA   PIC X.                                          00004970
           02 MPOSTEC   PIC X.                                          00004980
           02 MPOSTEP   PIC X.                                          00004990
           02 MPOSTEH   PIC X.                                          00005000
           02 MPOSTEV   PIC X.                                          00005010
           02 MPOSTEO   PIC X(5).                                       00005020
           02 FILLER    PIC X(2).                                       00005030
           02 MNOMLA    PIC X.                                          00005040
           02 MNOMLC    PIC X.                                          00005050
           02 MNOMLP    PIC X.                                          00005060
           02 MNOMLH    PIC X.                                          00005070
           02 MNOMLV    PIC X.                                          00005080
           02 MNOMLO    PIC X(5).                                       00005090
           02 FILLER    PIC X(2).                                       00005100
           02 MLNOMLA   PIC X.                                          00005110
           02 MLNOMLC   PIC X.                                          00005120
           02 MLNOMLP   PIC X.                                          00005130
           02 MLNOMLH   PIC X.                                          00005140
           02 MLNOMLV   PIC X.                                          00005150
           02 MLNOMLO   PIC X(25).                                      00005160
           02 FILLER    PIC X(2).                                       00005170
           02 MLPNOMLA  PIC X.                                          00005180
           02 MLPNOMLC  PIC X.                                          00005190
           02 MLPNOMLP  PIC X.                                          00005200
           02 MLPNOMLH  PIC X.                                          00005210
           02 MLPNOMLV  PIC X.                                          00005220
           02 MLPNOMLO  PIC X(15).                                      00005230
           02 FILLER    PIC X(2).                                       00005240
           02 MNVOIELA  PIC X.                                          00005250
           02 MNVOIELC  PIC X.                                          00005260
           02 MNVOIELP  PIC X.                                          00005270
           02 MNVOIELH  PIC X.                                          00005280
           02 MNVOIELV  PIC X.                                          00005290
           02 MNVOIELO  PIC X(5).                                       00005300
           02 FILLER    PIC X(2).                                       00005310
           02 MTPVOIELA      PIC X.                                     00005320
           02 MTPVOIELC PIC X.                                          00005330
           02 MTPVOIELP PIC X.                                          00005340
           02 MTPVOIELH PIC X.                                          00005350
           02 MTPVOIELV PIC X.                                          00005360
           02 MTPVOIELO      PIC X(4).                                  00005370
           02 FILLER    PIC X(2).                                       00005380
           02 MLVOIELA  PIC X.                                          00005390
           02 MLVOIELC  PIC X.                                          00005400
           02 MLVOIELP  PIC X.                                          00005410
           02 MLVOIELH  PIC X.                                          00005420
           02 MLVOIELV  PIC X.                                          00005430
           02 MLVOIELO  PIC X(20).                                      00005440
           02 FILLER    PIC X(2).                                       00005450
           02 MCBATLA   PIC X.                                          00005460
           02 MCBATLC   PIC X.                                          00005470
           02 MCBATLP   PIC X.                                          00005480
           02 MCBATLH   PIC X.                                          00005490
           02 MCBATLV   PIC X.                                          00005500
           02 MCBATLO   PIC X(3).                                       00005510
           02 FILLER    PIC X(2).                                       00005520
           02 MCESCLA   PIC X.                                          00005530
           02 MCESCLC   PIC X.                                          00005540
           02 MCESCLP   PIC X.                                          00005550
           02 MCESCLH   PIC X.                                          00005560
           02 MCESCLV   PIC X.                                          00005570
           02 MCESCLO   PIC X(3).                                       00005580
           02 FILLER    PIC X(2).                                       00005590
           02 MCETAGLA  PIC X.                                          00005600
           02 MCETAGLC  PIC X.                                          00005610
           02 MCETAGLP  PIC X.                                          00005620
           02 MCETAGLH  PIC X.                                          00005630
           02 MCETAGLV  PIC X.                                          00005640
           02 MCETAGLO  PIC X(3).                                       00005650
           02 FILLER    PIC X(2).                                       00005660
           02 MCPORTELA      PIC X.                                     00005670
           02 MCPORTELC PIC X.                                          00005680
           02 MCPORTELP PIC X.                                          00005690
           02 MCPORTELH PIC X.                                          00005700
           02 MCPORTELV PIC X.                                          00005710
           02 MCPORTELO      PIC X(3).                                  00005720
           02 FILLER    PIC X(2).                                       00005730
           02 MLADDRLA  PIC X.                                          00005740
           02 MLADDRLC  PIC X.                                          00005750
           02 MLADDRLP  PIC X.                                          00005760
           02 MLADDRLH  PIC X.                                          00005770
           02 MLADDRLV  PIC X.                                          00005780
           02 MLADDRLO  PIC X(32).                                      00005790
           02 FILLER    PIC X(2).                                       00005800
           02 MCPOSTLA  PIC X.                                          00005810
           02 MCPOSTLC  PIC X.                                          00005820
           02 MCPOSTLP  PIC X.                                          00005830
           02 MCPOSTLH  PIC X.                                          00005840
           02 MCPOSTLV  PIC X.                                          00005850
           02 MCPOSTLO  PIC X(5).                                       00005860
           02 FILLER    PIC X(2).                                       00005870
           02 MCCOMNLA  PIC X.                                          00005880
           02 MCCOMNLC  PIC X.                                          00005890
           02 MCCOMNLP  PIC X.                                          00005900
           02 MCCOMNLH  PIC X.                                          00005910
           02 MCCOMNLV  PIC X.                                          00005920
           02 MCCOMNLO  PIC X(32).                                      00005930
           02 FILLER    PIC X(2).                                       00005940
           02 MLPOSTLA  PIC X.                                          00005950
           02 MLPOSTLC  PIC X.                                          00005960
           02 MLPOSTLP  PIC X.                                          00005970
           02 MLPOSTLH  PIC X.                                          00005980
           02 MLPOSTLV  PIC X.                                          00005990
           02 MLPOSTLO  PIC X(26).                                      00006000
           02 FILLER    PIC X(2).                                       00006010
           02 MTELD20A  PIC X.                                          00006020
           02 MTELD20C  PIC X.                                          00006030
           02 MTELD20P  PIC X.                                          00006040
           02 MTELD20H  PIC X.                                          00006050
           02 MTELD20V  PIC X.                                          00006060
           02 MTELD20O  PIC X(2).                                       00006070
           02 FILLER    PIC X(2).                                       00006080
           02 MTELD21A  PIC X.                                          00006090
           02 MTELD21C  PIC X.                                          00006100
           02 MTELD21P  PIC X.                                          00006110
           02 MTELD21H  PIC X.                                          00006120
           02 MTELD21V  PIC X.                                          00006130
           02 MTELD21O  PIC X(2).                                       00006140
           02 FILLER    PIC X(2).                                       00006150
           02 MTELD22A  PIC X.                                          00006160
           02 MTELD22C  PIC X.                                          00006170
           02 MTELD22P  PIC X.                                          00006180
           02 MTELD22H  PIC X.                                          00006190
           02 MTELD22V  PIC X.                                          00006200
           02 MTELD22O  PIC X(2).                                       00006210
           02 FILLER    PIC X(2).                                       00006220
           02 MTELD23A  PIC X.                                          00006230
           02 MTELD23C  PIC X.                                          00006240
           02 MTELD23P  PIC X.                                          00006250
           02 MTELD23H  PIC X.                                          00006260
           02 MTELD23V  PIC X.                                          00006270
           02 MTELD23O  PIC X(2).                                       00006280
           02 FILLER    PIC X(2).                                       00006290
           02 MTELD24A  PIC X.                                          00006300
           02 MTELD24C  PIC X.                                          00006310
           02 MTELD24P  PIC X.                                          00006320
           02 MTELD24H  PIC X.                                          00006330
           02 MTELD24V  PIC X.                                          00006340
           02 MTELD24O  PIC X(2).                                       00006350
           02 FILLER    PIC X(2).                                       00006360
           02 MTELB20A  PIC X.                                          00006370
           02 MTELB20C  PIC X.                                          00006380
           02 MTELB20P  PIC X.                                          00006390
           02 MTELB20H  PIC X.                                          00006400
           02 MTELB20V  PIC X.                                          00006410
           02 MTELB20O  PIC X(2).                                       00006420
           02 FILLER    PIC X(2).                                       00006430
           02 MTELB21A  PIC X.                                          00006440
           02 MTELB21C  PIC X.                                          00006450
           02 MTELB21P  PIC X.                                          00006460
           02 MTELB21H  PIC X.                                          00006470
           02 MTELB21V  PIC X.                                          00006480
           02 MTELB21O  PIC X(2).                                       00006490
           02 FILLER    PIC X(2).                                       00006500
           02 MTELB22A  PIC X.                                          00006510
           02 MTELB22C  PIC X.                                          00006520
           02 MTELB22P  PIC X.                                          00006530
           02 MTELB22H  PIC X.                                          00006540
           02 MTELB22V  PIC X.                                          00006550
           02 MTELB22O  PIC X(2).                                       00006560
           02 FILLER    PIC X(2).                                       00006570
           02 MTELB23A  PIC X.                                          00006580
           02 MTELB23C  PIC X.                                          00006590
           02 MTELB23P  PIC X.                                          00006600
           02 MTELB23H  PIC X.                                          00006610
           02 MTELB23V  PIC X.                                          00006620
           02 MTELB23O  PIC X(2).                                       00006630
           02 FILLER    PIC X(2).                                       00006640
           02 MTELB24A  PIC X.                                          00006650
           02 MTELB24C  PIC X.                                          00006660
           02 MTELB24P  PIC X.                                          00006670
           02 MTELB24H  PIC X.                                          00006680
           02 MTELB24V  PIC X.                                          00006690
           02 MTELB24O  PIC X(2).                                       00006700
           02 FILLER    PIC X(2).                                       00006710
           02 MPOSTELA  PIC X.                                          00006720
           02 MPOSTELC  PIC X.                                          00006730
           02 MPOSTELP  PIC X.                                          00006740
           02 MPOSTELH  PIC X.                                          00006750
           02 MPOSTELV  PIC X.                                          00006760
           02 MPOSTELO  PIC X(5).                                       00006770
           02 MLLIVRO OCCURS   2 TIMES .                                00006780
             03 FILLER       PIC X(2).                                  00006790
             03 MCOMMENA     PIC X.                                     00006800
             03 MCOMMENC     PIC X.                                     00006810
             03 MCOMMENP     PIC X.                                     00006820
             03 MCOMMENH     PIC X.                                     00006830
             03 MCOMMENV     PIC X.                                     00006840
             03 MCOMMENO     PIC X(78).                                 00006850
           02 FILLER    PIC X(2).                                       00006860
           02 MZONCMDA  PIC X.                                          00006870
           02 MZONCMDC  PIC X.                                          00006880
           02 MZONCMDP  PIC X.                                          00006890
           02 MZONCMDH  PIC X.                                          00006900
           02 MZONCMDV  PIC X.                                          00006910
           02 MZONCMDO  PIC X(15).                                      00006920
           02 FILLER    PIC X(2).                                       00006930
           02 MLIBERRA  PIC X.                                          00006940
           02 MLIBERRC  PIC X.                                          00006950
           02 MLIBERRP  PIC X.                                          00006960
           02 MLIBERRH  PIC X.                                          00006970
           02 MLIBERRV  PIC X.                                          00006980
           02 MLIBERRO  PIC X(58).                                      00006990
           02 FILLER    PIC X(2).                                       00007000
           02 MCODTRAA  PIC X.                                          00007010
           02 MCODTRAC  PIC X.                                          00007020
           02 MCODTRAP  PIC X.                                          00007030
           02 MCODTRAH  PIC X.                                          00007040
           02 MCODTRAV  PIC X.                                          00007050
           02 MCODTRAO  PIC X(4).                                       00007060
           02 FILLER    PIC X(2).                                       00007070
           02 MCICSA    PIC X.                                          00007080
           02 MCICSC    PIC X.                                          00007090
           02 MCICSP    PIC X.                                          00007100
           02 MCICSH    PIC X.                                          00007110
           02 MCICSV    PIC X.                                          00007120
           02 MCICSO    PIC X(5).                                       00007130
           02 FILLER    PIC X(2).                                       00007140
           02 MNETNAMA  PIC X.                                          00007150
           02 MNETNAMC  PIC X.                                          00007160
           02 MNETNAMP  PIC X.                                          00007170
           02 MNETNAMH  PIC X.                                          00007180
           02 MNETNAMV  PIC X.                                          00007190
           02 MNETNAMO  PIC X(8).                                       00007200
           02 FILLER    PIC X(2).                                       00007210
           02 MSCREENA  PIC X.                                          00007220
           02 MSCREENC  PIC X.                                          00007230
           02 MSCREENP  PIC X.                                          00007240
           02 MSCREENH  PIC X.                                          00007250
           02 MSCREENV  PIC X.                                          00007260
           02 MSCREENO  PIC X(4).                                       00007270
                                                                                
