      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAS10   EAS10                                              00000020
      ***************************************************************** 00000030
       01   EAS10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAMPAL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCAMPAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCAMPAF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCAMPAI   PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MLIBELLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBELLF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLIBELLI  PIC X(30).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDJJL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDJJL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MDJJF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDJJI     PIC X(2).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMML     COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDMML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MDMMF     PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDMMI     PIC X(2).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDAAL     COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDAAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MDAAF     PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDAAI     PIC X(4).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFJJL     COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MFJJL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MFJJF     PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MFJJI     PIC X(2).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFMML     COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MFMML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MFMMF     PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MFMMI     PIC X(2).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFAAL     COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MFAAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MFAAF     PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MFAAI     PIC X(4).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMONOL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MMONOL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MMONOF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MMONOI    PIC X.                                          00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMULTIL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MMULTIL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MMULTIF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MMULTII   PIC X.                                          00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAMINL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MPAMINL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAMINF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MPAMINI   PIC X(10).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAMAXL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MPAMAXL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAMAXF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MPAMAXI   PIC X(10).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTR1L     COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MTR1L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTR1F     PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MTR1I     PIC X.                                          00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTR1L  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLIBTR1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBTR1F  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLIBTR1I  PIC X(20).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTR2L     COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MTR2L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTR2F     PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MTR2I     PIC X.                                          00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTR2L  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLIBTR2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBTR2F  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLIBTR2I  PIC X(20).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTR3L     COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MTR3L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTR3F     PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MTR3I     PIC X.                                          00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTR3L  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLIBTR3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBTR3F  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLIBTR3I  PIC X(20).                                      00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTR4L     COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MTR4L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTR4F     PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MTR4I     PIC X.                                          00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTR4L  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MLIBTR4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBTR4F  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLIBTR4I  PIC X(20).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTR5L     COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MTR5L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTR5F     PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MTR5I     PIC X.                                          00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTR5L  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MLIBTR5L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBTR5F  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MLIBTR5I  PIC X(20).                                      00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTR6L     COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MTR6L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTR6F     PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MTR6I     PIC X.                                          00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTR6L  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MLIBTR6L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBTR6F  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MLIBTR6I  PIC X(20).                                      00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTR7L     COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MTR7L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTR7F     PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MTR7I     PIC X.                                          00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTR7L  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MLIBTR7L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBTR7F  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MLIBTR7I  PIC X(20).                                      00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTR8L     COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MTR8L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTR8F     PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MTR8I     PIC X.                                          00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBTR8L  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MLIBTR8L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBTR8F  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MLIBTR8I  PIC X(20).                                      00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOUSL    COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MTOUSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTOUSF    PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MTOUSI    PIC X.                                          00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCOML    COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MDCOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDCOMF    PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MDCOMI    PIC X.                                          00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMGDL     COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MMGDL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MMGDF     PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MMGDI     PIC X.                                          00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBTOBL    COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MBTOBL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MBTOBF    PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MBTOBI    PIC X.                                          00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAGL     COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MMAGL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MMAGF     PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MMAGI     PIC X.                                          00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MLIBERRI  PIC X(78).                                      00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001500
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MCODTRAI  PIC X(4).                                       00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001540
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MCICSI    PIC X(5).                                       00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001580
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MNETNAMI  PIC X(8).                                       00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MSCREENI  PIC X(5).                                       00001650
      ***************************************************************** 00001660
      * SDF: EAS10   EAS10                                              00001670
      ***************************************************************** 00001680
       01   EAS10O REDEFINES EAS10I.                                    00001690
           02 FILLER    PIC X(12).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MDATJOUA  PIC X.                                          00001720
           02 MDATJOUC  PIC X.                                          00001730
           02 MDATJOUP  PIC X.                                          00001740
           02 MDATJOUH  PIC X.                                          00001750
           02 MDATJOUV  PIC X.                                          00001760
           02 MDATJOUO  PIC X(10).                                      00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MTIMJOUA  PIC X.                                          00001790
           02 MTIMJOUC  PIC X.                                          00001800
           02 MTIMJOUP  PIC X.                                          00001810
           02 MTIMJOUH  PIC X.                                          00001820
           02 MTIMJOUV  PIC X.                                          00001830
           02 MTIMJOUO  PIC X(5).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MCAMPAA   PIC X.                                          00001860
           02 MCAMPAC   PIC X.                                          00001870
           02 MCAMPAP   PIC X.                                          00001880
           02 MCAMPAH   PIC X.                                          00001890
           02 MCAMPAV   PIC X.                                          00001900
           02 MCAMPAO   PIC X(4).                                       00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MLIBELLA  PIC X.                                          00001930
           02 MLIBELLC  PIC X.                                          00001940
           02 MLIBELLP  PIC X.                                          00001950
           02 MLIBELLH  PIC X.                                          00001960
           02 MLIBELLV  PIC X.                                          00001970
           02 MLIBELLO  PIC X(30).                                      00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MDJJA     PIC X.                                          00002000
           02 MDJJC     PIC X.                                          00002010
           02 MDJJP     PIC X.                                          00002020
           02 MDJJH     PIC X.                                          00002030
           02 MDJJV     PIC X.                                          00002040
           02 MDJJO     PIC X(2).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MDMMA     PIC X.                                          00002070
           02 MDMMC     PIC X.                                          00002080
           02 MDMMP     PIC X.                                          00002090
           02 MDMMH     PIC X.                                          00002100
           02 MDMMV     PIC X.                                          00002110
           02 MDMMO     PIC X(2).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MDAAA     PIC X.                                          00002140
           02 MDAAC     PIC X.                                          00002150
           02 MDAAP     PIC X.                                          00002160
           02 MDAAH     PIC X.                                          00002170
           02 MDAAV     PIC X.                                          00002180
           02 MDAAO     PIC X(4).                                       00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MFJJA     PIC X.                                          00002210
           02 MFJJC     PIC X.                                          00002220
           02 MFJJP     PIC X.                                          00002230
           02 MFJJH     PIC X.                                          00002240
           02 MFJJV     PIC X.                                          00002250
           02 MFJJO     PIC X(2).                                       00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MFMMA     PIC X.                                          00002280
           02 MFMMC     PIC X.                                          00002290
           02 MFMMP     PIC X.                                          00002300
           02 MFMMH     PIC X.                                          00002310
           02 MFMMV     PIC X.                                          00002320
           02 MFMMO     PIC X(2).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MFAAA     PIC X.                                          00002350
           02 MFAAC     PIC X.                                          00002360
           02 MFAAP     PIC X.                                          00002370
           02 MFAAH     PIC X.                                          00002380
           02 MFAAV     PIC X.                                          00002390
           02 MFAAO     PIC X(4).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MMONOA    PIC X.                                          00002420
           02 MMONOC    PIC X.                                          00002430
           02 MMONOP    PIC X.                                          00002440
           02 MMONOH    PIC X.                                          00002450
           02 MMONOV    PIC X.                                          00002460
           02 MMONOO    PIC X.                                          00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MMULTIA   PIC X.                                          00002490
           02 MMULTIC   PIC X.                                          00002500
           02 MMULTIP   PIC X.                                          00002510
           02 MMULTIH   PIC X.                                          00002520
           02 MMULTIV   PIC X.                                          00002530
           02 MMULTIO   PIC X.                                          00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MPAMINA   PIC X.                                          00002560
           02 MPAMINC   PIC X.                                          00002570
           02 MPAMINP   PIC X.                                          00002580
           02 MPAMINH   PIC X.                                          00002590
           02 MPAMINV   PIC X.                                          00002600
           02 MPAMINO   PIC X(10).                                      00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MPAMAXA   PIC X.                                          00002630
           02 MPAMAXC   PIC X.                                          00002640
           02 MPAMAXP   PIC X.                                          00002650
           02 MPAMAXH   PIC X.                                          00002660
           02 MPAMAXV   PIC X.                                          00002670
           02 MPAMAXO   PIC X(10).                                      00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MTR1A     PIC X.                                          00002700
           02 MTR1C     PIC X.                                          00002710
           02 MTR1P     PIC X.                                          00002720
           02 MTR1H     PIC X.                                          00002730
           02 MTR1V     PIC X.                                          00002740
           02 MTR1O     PIC X.                                          00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MLIBTR1A  PIC X.                                          00002770
           02 MLIBTR1C  PIC X.                                          00002780
           02 MLIBTR1P  PIC X.                                          00002790
           02 MLIBTR1H  PIC X.                                          00002800
           02 MLIBTR1V  PIC X.                                          00002810
           02 MLIBTR1O  PIC X(20).                                      00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MTR2A     PIC X.                                          00002840
           02 MTR2C     PIC X.                                          00002850
           02 MTR2P     PIC X.                                          00002860
           02 MTR2H     PIC X.                                          00002870
           02 MTR2V     PIC X.                                          00002880
           02 MTR2O     PIC X.                                          00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MLIBTR2A  PIC X.                                          00002910
           02 MLIBTR2C  PIC X.                                          00002920
           02 MLIBTR2P  PIC X.                                          00002930
           02 MLIBTR2H  PIC X.                                          00002940
           02 MLIBTR2V  PIC X.                                          00002950
           02 MLIBTR2O  PIC X(20).                                      00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MTR3A     PIC X.                                          00002980
           02 MTR3C     PIC X.                                          00002990
           02 MTR3P     PIC X.                                          00003000
           02 MTR3H     PIC X.                                          00003010
           02 MTR3V     PIC X.                                          00003020
           02 MTR3O     PIC X.                                          00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MLIBTR3A  PIC X.                                          00003050
           02 MLIBTR3C  PIC X.                                          00003060
           02 MLIBTR3P  PIC X.                                          00003070
           02 MLIBTR3H  PIC X.                                          00003080
           02 MLIBTR3V  PIC X.                                          00003090
           02 MLIBTR3O  PIC X(20).                                      00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MTR4A     PIC X.                                          00003120
           02 MTR4C     PIC X.                                          00003130
           02 MTR4P     PIC X.                                          00003140
           02 MTR4H     PIC X.                                          00003150
           02 MTR4V     PIC X.                                          00003160
           02 MTR4O     PIC X.                                          00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MLIBTR4A  PIC X.                                          00003190
           02 MLIBTR4C  PIC X.                                          00003200
           02 MLIBTR4P  PIC X.                                          00003210
           02 MLIBTR4H  PIC X.                                          00003220
           02 MLIBTR4V  PIC X.                                          00003230
           02 MLIBTR4O  PIC X(20).                                      00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MTR5A     PIC X.                                          00003260
           02 MTR5C     PIC X.                                          00003270
           02 MTR5P     PIC X.                                          00003280
           02 MTR5H     PIC X.                                          00003290
           02 MTR5V     PIC X.                                          00003300
           02 MTR5O     PIC X.                                          00003310
           02 FILLER    PIC X(2).                                       00003320
           02 MLIBTR5A  PIC X.                                          00003330
           02 MLIBTR5C  PIC X.                                          00003340
           02 MLIBTR5P  PIC X.                                          00003350
           02 MLIBTR5H  PIC X.                                          00003360
           02 MLIBTR5V  PIC X.                                          00003370
           02 MLIBTR5O  PIC X(20).                                      00003380
           02 FILLER    PIC X(2).                                       00003390
           02 MTR6A     PIC X.                                          00003400
           02 MTR6C     PIC X.                                          00003410
           02 MTR6P     PIC X.                                          00003420
           02 MTR6H     PIC X.                                          00003430
           02 MTR6V     PIC X.                                          00003440
           02 MTR6O     PIC X.                                          00003450
           02 FILLER    PIC X(2).                                       00003460
           02 MLIBTR6A  PIC X.                                          00003470
           02 MLIBTR6C  PIC X.                                          00003480
           02 MLIBTR6P  PIC X.                                          00003490
           02 MLIBTR6H  PIC X.                                          00003500
           02 MLIBTR6V  PIC X.                                          00003510
           02 MLIBTR6O  PIC X(20).                                      00003520
           02 FILLER    PIC X(2).                                       00003530
           02 MTR7A     PIC X.                                          00003540
           02 MTR7C     PIC X.                                          00003550
           02 MTR7P     PIC X.                                          00003560
           02 MTR7H     PIC X.                                          00003570
           02 MTR7V     PIC X.                                          00003580
           02 MTR7O     PIC X.                                          00003590
           02 FILLER    PIC X(2).                                       00003600
           02 MLIBTR7A  PIC X.                                          00003610
           02 MLIBTR7C  PIC X.                                          00003620
           02 MLIBTR7P  PIC X.                                          00003630
           02 MLIBTR7H  PIC X.                                          00003640
           02 MLIBTR7V  PIC X.                                          00003650
           02 MLIBTR7O  PIC X(20).                                      00003660
           02 FILLER    PIC X(2).                                       00003670
           02 MTR8A     PIC X.                                          00003680
           02 MTR8C     PIC X.                                          00003690
           02 MTR8P     PIC X.                                          00003700
           02 MTR8H     PIC X.                                          00003710
           02 MTR8V     PIC X.                                          00003720
           02 MTR8O     PIC X.                                          00003730
           02 FILLER    PIC X(2).                                       00003740
           02 MLIBTR8A  PIC X.                                          00003750
           02 MLIBTR8C  PIC X.                                          00003760
           02 MLIBTR8P  PIC X.                                          00003770
           02 MLIBTR8H  PIC X.                                          00003780
           02 MLIBTR8V  PIC X.                                          00003790
           02 MLIBTR8O  PIC X(20).                                      00003800
           02 FILLER    PIC X(2).                                       00003810
           02 MTOUSA    PIC X.                                          00003820
           02 MTOUSC    PIC X.                                          00003830
           02 MTOUSP    PIC X.                                          00003840
           02 MTOUSH    PIC X.                                          00003850
           02 MTOUSV    PIC X.                                          00003860
           02 MTOUSO    PIC X.                                          00003870
           02 FILLER    PIC X(2).                                       00003880
           02 MDCOMA    PIC X.                                          00003890
           02 MDCOMC    PIC X.                                          00003900
           02 MDCOMP    PIC X.                                          00003910
           02 MDCOMH    PIC X.                                          00003920
           02 MDCOMV    PIC X.                                          00003930
           02 MDCOMO    PIC X.                                          00003940
           02 FILLER    PIC X(2).                                       00003950
           02 MMGDA     PIC X.                                          00003960
           02 MMGDC     PIC X.                                          00003970
           02 MMGDP     PIC X.                                          00003980
           02 MMGDH     PIC X.                                          00003990
           02 MMGDV     PIC X.                                          00004000
           02 MMGDO     PIC X.                                          00004010
           02 FILLER    PIC X(2).                                       00004020
           02 MBTOBA    PIC X.                                          00004030
           02 MBTOBC    PIC X.                                          00004040
           02 MBTOBP    PIC X.                                          00004050
           02 MBTOBH    PIC X.                                          00004060
           02 MBTOBV    PIC X.                                          00004070
           02 MBTOBO    PIC X.                                          00004080
           02 FILLER    PIC X(2).                                       00004090
           02 MMAGA     PIC X.                                          00004100
           02 MMAGC     PIC X.                                          00004110
           02 MMAGP     PIC X.                                          00004120
           02 MMAGH     PIC X.                                          00004130
           02 MMAGV     PIC X.                                          00004140
           02 MMAGO     PIC X.                                          00004150
           02 FILLER    PIC X(2).                                       00004160
           02 MLIBERRA  PIC X.                                          00004170
           02 MLIBERRC  PIC X.                                          00004180
           02 MLIBERRP  PIC X.                                          00004190
           02 MLIBERRH  PIC X.                                          00004200
           02 MLIBERRV  PIC X.                                          00004210
           02 MLIBERRO  PIC X(78).                                      00004220
           02 FILLER    PIC X(2).                                       00004230
           02 MCODTRAA  PIC X.                                          00004240
           02 MCODTRAC  PIC X.                                          00004250
           02 MCODTRAP  PIC X.                                          00004260
           02 MCODTRAH  PIC X.                                          00004270
           02 MCODTRAV  PIC X.                                          00004280
           02 MCODTRAO  PIC X(4).                                       00004290
           02 FILLER    PIC X(2).                                       00004300
           02 MCICSA    PIC X.                                          00004310
           02 MCICSC    PIC X.                                          00004320
           02 MCICSP    PIC X.                                          00004330
           02 MCICSH    PIC X.                                          00004340
           02 MCICSV    PIC X.                                          00004350
           02 MCICSO    PIC X(5).                                       00004360
           02 FILLER    PIC X(2).                                       00004370
           02 MNETNAMA  PIC X.                                          00004380
           02 MNETNAMC  PIC X.                                          00004390
           02 MNETNAMP  PIC X.                                          00004400
           02 MNETNAMH  PIC X.                                          00004410
           02 MNETNAMV  PIC X.                                          00004420
           02 MNETNAMO  PIC X(8).                                       00004430
           02 FILLER    PIC X(2).                                       00004440
           02 MSCREENA  PIC X.                                          00004450
           02 MSCREENC  PIC X.                                          00004460
           02 MSCREENP  PIC X.                                          00004470
           02 MSCREENH  PIC X.                                          00004480
           02 MSCREENV  PIC X.                                          00004490
           02 MSCREENO  PIC X(5).                                       00004500
                                                                                
