      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************                
      *   COPY DE LA TABLE RVHV0400                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV0404                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHV0400.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHV0400.                                                            
      *}                                                                        
           02  HV04-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV04-NCODIC                                                      
               PIC X(0007).                                                     
           02  HV04-NLIEU                                                       
               PIC X(0003).                                                     
           02  HV04-DSVENTECIALE                                                
               PIC X(0006).                                                     
           02  HV04-QPIECES                                                     
               PIC S9(5) COMP-3.                                                
           02  HV04-PCA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV04-PMTACHATS                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV04-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  HV04-PMTPRIMES                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV04-QPIECESEMP                                                  
               PIC S9(5) COMP-3.                                                
           02  HV04-PCAEMP                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV04-PMTACHATSEMP                                                
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV04-PMTPRIMVOL                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV04-QPIECESEXC                                                  
               PIC S9(5) COMP-3.                                                
           02  HV04-PCAEXC                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV04-PMTACHATSEXC                                                
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV04-QPIECESEXE                                                  
               PIC S9(5) COMP-3.                                                
           02  HV04-PCAEXE                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV04-PMTACHATSEXE                                                
               PIC S9(7)V9(0002) COMP-3.                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHV0404                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHV0400-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHV0400-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-DSVENTECIALE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-DSVENTECIALE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-QPIECES-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-QPIECES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-PCA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-PCA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-PMTACHATS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-PMTACHATS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-PMTPRIMES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-PMTPRIMES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-QPIECESEMP-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-QPIECESEMP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-PCAEMP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-PCAEMP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-PMTACHATSEMP-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-PMTACHATSEMP-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-PMTPRIMVOL-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-PMTPRIMVOL-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-QPIECESEXC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-QPIECESEXC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-PCAEXC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-PCAEXC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-PMTACHATSEXC-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-PMTACHATSEXC-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-QPIECESEXE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-QPIECESEXE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-PCAEXE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-PCAEXE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV04-PMTACHATSEXE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV04-PMTACHATSEXE-F                                              
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
