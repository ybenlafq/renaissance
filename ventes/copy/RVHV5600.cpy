      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHV5600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV5600                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVHV5600.                                                            
           02  HV56-NCLUSTERP                                                   
               PIC X(0009).                                                     
           02  HV56-DVENTE                                                      
               PIC X(0008).                                                     
           02  HV56-NVENTE                                                      
               PIC X(0007).                                                     
           02  HV56-NLIEU                                                       
               PIC X(0003).                                                     
           02  HV56-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV56-NCLUSTER                                                    
               PIC X(0009).                                                     
           02  HV56-LNOM                                                        
               PIC X(0025).                                                     
           02  HV56-CFAM                                                        
               PIC X(0005).                                                     
           02  HV56-CMARQ                                                       
               PIC X(0005).                                                     
           02  HV56-TELDOM                                                      
               PIC X(0010).                                                     
           02  HV56-TELBUR                                                      
               PIC X(0010).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHV5600                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHV5600-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV56-NCLUSTERP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV56-NCLUSTERP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV56-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV56-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV56-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV56-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV56-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV56-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV56-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV56-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV56-NCLUSTER-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV56-NCLUSTER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV56-LNOM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV56-LNOM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV56-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV56-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV56-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV56-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV56-TELDOM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV56-TELDOM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV56-TELBUR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV56-TELBUR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
