      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SVMKG MKG PAR FAMILLE SAV SIEBEL       *        
      *----------------------------------------------------------------*        
       01  RVSVMKG.                                                             
           05  SVMKG-CTABLEG2    PIC X(15).                                     
           05  SVMKG-CTABLEG2-REDEF REDEFINES SVMKG-CTABLEG2.                   
               10  SVMKG-MARQUE          PIC X(05).                             
               10  SVMKG-FSAV            PIC X(05).                             
               10  SVMKG-SEQ             PIC X(02).                             
           05  SVMKG-WTABLEG     PIC X(80).                                     
           05  SVMKG-WTABLEG-REDEF  REDEFINES SVMKG-WTABLEG.                    
               10  SVMKG-CDESC           PIC X(05).                             
               10  SVMKG-CMKG            PIC X(05).                             
               10  SVMKG-DEFFET          PIC X(08).                             
               10  SVMKG-DEFFET-N       REDEFINES SVMKG-DEFFET                  
                                         PIC 9(08).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVSVMKG-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SVMKG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SVMKG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SVMKG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SVMKG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
