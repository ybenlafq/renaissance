      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      ******************************************************************        
      * RTEC11 - Suivi de Commandes WebSph�re - Lignes                          
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEC1100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEC1100.                                                            
      *}                                                                        
           10 EC11-NCDEWC          PIC S9(15)V USAGE COMP-3.                    
           10 EC11-NSOCIETE        PIC X(3).                                    
           10 EC11-NLIEU           PIC X(3).                                    
           10 EC11-NVENTE          PIC X(7).                                    
           10 EC11-NSEQNQ          PIC S9(5)V USAGE COMP-3.                     
           10 EC11-NSEQECOM        PIC S9(3)V USAGE COMP-3.                     
           10 EC11-STAT            PIC X(1).                                    
           10 EC11-NSEQERR         PIC X(4).                                    
           10 EC11-TEXTE           PIC X(80).                                   
           10 EC11-DCRE            PIC X(8).                                    
           10 EC11-HCRE            PIC X(6).                                    
           10 EC11-DSYST           PIC S9(13)V USAGE COMP-3.                    
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
