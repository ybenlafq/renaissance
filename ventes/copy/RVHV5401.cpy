      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHV5401                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV5401                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVHV5401.                                                            
           02  HV54-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV54-NLIEU                                                       
               PIC X(0003).                                                     
           02  HV54-DCAISSE                                                     
               PIC X(0008).                                                     
           02  HV54-NCAISSE                                                     
               PIC X(0003).                                                     
           02  HV54-NTRANS                                                      
               PIC X(0004).                                                     
           02  HV54-DHVENTE                                                     
               PIC X(0002).                                                     
           02  HV54-DMVENTE                                                     
               PIC X(0002).                                                     
           02  HV54-NTYPETRANS                                                  
               PIC X(0001).                                                     
           02  HV54-CANNULATION                                                 
               PIC X(0001).                                                     
           02  HV54-NTRANANUL                                                   
               PIC X(0004).                                                     
           02  HV54-NOPERATEUR                                                  
               PIC X(0004).                                                     
           02  HV54-PMONTANT                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV54-CAPRET                                                      
               PIC X(0001).                                                     
           02  HV54-DEVISOPE                                                    
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHV5401                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHV5401-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV54-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV54-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV54-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV54-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV54-DCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV54-DCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV54-NCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV54-NCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV54-NTRANS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV54-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV54-DHVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV54-DHVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV54-DMVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV54-DMVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV54-NTYPETRANS-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV54-NTYPETRANS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV54-CANNULATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV54-CANNULATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV54-NTRANANUL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV54-NTRANANUL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV54-NOPERATEUR-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV54-NOPERATEUR-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV54-PMONTANT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV54-PMONTANT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV54-CAPRET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV54-CAPRET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV54-DEVISOPE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV54-DEVISOPE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
