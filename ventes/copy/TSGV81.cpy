      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      *                                                               * 00000020
      *      TS SPECIFIQUE DU PROGRAMME TGV81                         * 00000030
      *      1 ITEM PAR PAGE                                          * 00000050
      *       5 BONS D'ENLEVEMENTS PAR ITEM                           * 00000050
      *         2 LIGNES PAR BONS                                     * 00000051
      *            --->  10 LIGNES PAR TS                             * 00000052
      *                                                               * 00000052
      *      TR : GV08  GESTION DES VENTES                            * 00000060
      *      PG : TGV81 CONSULTATION DES PRODUITS AVEC BON D'ENLEV.   * 00000070
      *                                                               * 00000080
      *      NOM: 'GV81' + EIBTRMID                                   * 00000090
      *      LG : 780 C                                               * 00000091
      *                                                               * 00000092
      ***************************************************************** 00000093
      *                                                               * 00000094
       01  TS-GV81.                                                     00000095
      ********************************** LONGUEUR TS                    00000096
           05 TS-GV81-LONG               PIC S9(5) COMP-3 VALUE +780.   00000097
      ********************************** DONNEES                        00000098
           05 TS-GV81-DONNEES.                                          00000099
              10 TS-GV81-LIGNE           OCCURS 10.                     00000100
                 15 TS-GV81-BONS       PIC  X(78) .                             
                                                                                
