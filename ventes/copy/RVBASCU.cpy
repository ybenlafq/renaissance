      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BASCU INIT ASSORTIMENT NRM DACEM       *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBASCU.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBASCU.                                                             
      *}                                                                        
           05  BASCU-CTABLEG2    PIC X(15).                                     
           05  BASCU-CTABLEG2-REDEF REDEFINES BASCU-CTABLEG2.                   
               10  BASCU-CPROG           PIC X(06).                             
               10  BASCU-NSOCIETE        PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  BASCU-NSOCIETE-N     REDEFINES BASCU-NSOCIETE                
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  BASCU-NLIEU           PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  BASCU-NLIEU-N        REDEFINES BASCU-NLIEU                   
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
               10  BASCU-WFLAG           PIC X(01).                             
               10  BASCU-WFLAG2          PIC X(01).                             
           05  BASCU-WTABLEG     PIC X(80).                                     
           05  BASCU-WTABLEG-REDEF  REDEFINES BASCU-WTABLEG.                    
               10  BASCU-DATE            PIC X(08).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVBASCU-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVBASCU-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BASCU-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BASCU-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BASCU-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BASCU-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
