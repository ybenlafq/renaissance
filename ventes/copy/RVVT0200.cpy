      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010001
      *   COPY DE LA TABLE RVVT0200                                     00020001
      **********************************************************        00030001
      *                                                                 00040001
      *---------------------------------------------------------        00050001
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVT0200                 00060001
      *---------------------------------------------------------        00070001
      *                                                                 00080001
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
       01  RVVT0200.                                                    00090001
           02  VT02-NFLAG                                               00100001
               PIC X(0001).                                             00110001
           02  VT02-NCLUSTER                                            00120001
               PIC X(0013).                                             00130001
           02  VT02-NSOCIETE                                            00140001
               PIC X(0003).                                             00150001
           02  VT02-WTYPEADR                                            00160001
               PIC X(0001).                                             00170001
           02  VT02-CTITRENOM                                           00180001
               PIC X(0005).                                             00190001
           02  VT02-LNOM                                                00200001
               PIC X(0025).                                             00210001
           02  VT02-LPRENOM                                             00220001
               PIC X(0015).                                             00230001
           02  VT02-LBATIMENT                                           00240001
               PIC X(0003).                                             00250001
           02  VT02-LESCALIER                                           00260001
               PIC X(0003).                                             00270001
           02  VT02-LETAGE                                              00280001
               PIC X(0003).                                             00290001
           02  VT02-LPORTE                                              00300001
               PIC X(0003).                                             00310001
           02  VT02-LCMPAD1                                             00320001
               PIC X(0032).                                             00330001
           02  VT02-CVOIE                                               00340001
               PIC X(0005).                                             00350001
           02  VT02-CTVOIE                                              00360001
               PIC X(0004).                                             00370001
           02  VT02-LNOMVOIE                                            00380001
               PIC X(0021).                                             00390001
           02  VT02-LCOMMUNE                                            00400001
               PIC X(0032).                                             00410001
           02  VT02-CPOSTAL                                             00420001
               PIC X(0005).                                             00430001
           02  VT02-TELDOM                                              00440001
               PIC X(0010).                                             00450001
           02  VT02-TELBUR                                              00460001
               PIC X(0010).                                             00470001
           02  VT02-WEXPTAXE                                            00480001
               PIC X(0001).                                             00490001
           02  VT02-NORDRE                                              00500001
               PIC X(0005).                                             00510001
           02  VT02-IDCLIENT                                            00520001
               PIC X(0008).                                             00530001
           02  VT02-CPAYS                                               00540001
               PIC X(0003).                                             00550001
           02  VT02-NGSM                                                00560001
               PIC X(0015).                                             00570001
       EXEC SQL END DECLARE SECTION END-EXEC.
      *                                                                 00580001
      *---------------------------------------------------------        00590001
      *   LISTE DES FLAGS DE LA TABLE RVVT0200                          00600001
      *---------------------------------------------------------        00610001
      *                                                                 00620001
       01  RVVT0200-FLAGS.                                              00630001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-NFLAG-F                                             00640001
      *        PIC S9(4) COMP.                                          00650001
      *--                                                                       
           02  VT02-NFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-NCLUSTER-F                                          00660001
      *        PIC S9(4) COMP.                                          00670001
      *--                                                                       
           02  VT02-NCLUSTER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-NSOCIETE-F                                          00680001
      *        PIC S9(4) COMP.                                          00690001
      *--                                                                       
           02  VT02-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-WTYPEADR-F                                          00700001
      *        PIC S9(4) COMP.                                          00710001
      *--                                                                       
           02  VT02-WTYPEADR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-CTITRENOM-F                                         00720001
      *        PIC S9(4) COMP.                                          00730001
      *--                                                                       
           02  VT02-CTITRENOM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-LNOM-F                                              00740001
      *        PIC S9(4) COMP.                                          00750001
      *--                                                                       
           02  VT02-LNOM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-LPRENOM-F                                           00760001
      *        PIC S9(4) COMP.                                          00770001
      *--                                                                       
           02  VT02-LPRENOM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-LBATIMENT-F                                         00780001
      *        PIC S9(4) COMP.                                          00790001
      *--                                                                       
           02  VT02-LBATIMENT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-LESCALIER-F                                         00800001
      *        PIC S9(4) COMP.                                          00810001
      *--                                                                       
           02  VT02-LESCALIER-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-LETAGE-F                                            00820001
      *        PIC S9(4) COMP.                                          00830001
      *--                                                                       
           02  VT02-LETAGE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-LPORTE-F                                            00840001
      *        PIC S9(4) COMP.                                          00850001
      *--                                                                       
           02  VT02-LPORTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-LCMPAD1-F                                           00860001
      *        PIC S9(4) COMP.                                          00870001
      *--                                                                       
           02  VT02-LCMPAD1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-CVOIE-F                                             00880001
      *        PIC S9(4) COMP.                                          00890001
      *--                                                                       
           02  VT02-CVOIE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-CTVOIE-F                                            00900001
      *        PIC S9(4) COMP.                                          00910001
      *--                                                                       
           02  VT02-CTVOIE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-LNOMVOIE-F                                          00920001
      *        PIC S9(4) COMP.                                          00930001
      *--                                                                       
           02  VT02-LNOMVOIE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-LCOMMUNE-F                                          00940001
      *        PIC S9(4) COMP.                                          00950001
      *--                                                                       
           02  VT02-LCOMMUNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-CPOSTAL-F                                           00960001
      *        PIC S9(4) COMP.                                          00970001
      *--                                                                       
           02  VT02-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-TELDOM-F                                            00980001
      *        PIC S9(4) COMP.                                          00990001
      *--                                                                       
           02  VT02-TELDOM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-TELBUR-F                                            01000001
      *        PIC S9(4) COMP.                                          01010001
      *--                                                                       
           02  VT02-TELBUR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-WEXPTAXE-F                                          01020001
      *        PIC S9(4) COMP.                                          01030001
      *--                                                                       
           02  VT02-WEXPTAXE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-NORDRE-F                                            01040001
      *        PIC S9(4) COMP.                                          01050001
      *--                                                                       
           02  VT02-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-IDCLIENT-F                                          01060001
      *        PIC S9(4) COMP.                                          01070001
      *--                                                                       
           02  VT02-IDCLIENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-CPAYS-F                                             01080001
      *        PIC S9(4) COMP.                                          01090001
      *--                                                                       
           02  VT02-CPAYS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT02-NGSM-F                                              01100001
      *        PIC S9(4) COMP.                                          01110001
      *--                                                                       
           02  VT02-NGSM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            01120001
                                                                                
