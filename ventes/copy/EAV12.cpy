      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAV12   EAV12                                              00000020
      ***************************************************************** 00000030
       01   EAV12I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      * n� de societe                                                   00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCSAISIEL   COMP PIC S9(4).                            00000150
      *--                                                                       
           02 MNSOCSAISIEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNSOCSAISIEF   PIC X.                                     00000160
           02 FILLER    PIC X(4).                                       00000170
           02 MNSOCSAISIEI   PIC X(3).                                  00000180
      * n� de lieu                                                      00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUSAISIEL  COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MNLIEUSAISIEL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNLIEUSAISIEF  PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNLIEUSAISIEI  PIC X(3).                                  00000230
      * libell� du lieu                                                 00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUSAISIEL  COMP PIC S9(4).                            00000250
      *--                                                                       
           02 MLLIEUSAISIEL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MLLIEUSAISIEF  PIC X.                                     00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MLLIEUSAISIEI  PIC X(20).                                 00000280
           02 MLIGNEI OCCURS   13 TIMES .                               00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCIETEL   COMP PIC S9(4).                            00000300
      *--                                                                       
             03 MNSOCIETEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNSOCIETEF   PIC X.                                     00000310
             03 FILLER  PIC X(4).                                       00000320
             03 MNSOCIETEI   PIC X(3).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000340
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000350
             03 FILLER  PIC X(4).                                       00000360
             03 MNLIEUI      PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPAVOIRL  COMP PIC S9(4).                            00000380
      *--                                                                       
             03 MCTYPAVOIRL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MCTYPAVOIRF  PIC X.                                     00000390
             03 FILLER  PIC X(4).                                       00000400
             03 MCTYPAVOIRI  PIC X.                                     00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNAVOIRL     COMP PIC S9(4).                            00000420
      *--                                                                       
             03 MNAVOIRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNAVOIRF     PIC X.                                     00000430
             03 FILLER  PIC X(4).                                       00000440
             03 MNAVOIRI     PIC X(7).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCUTILL   COMP PIC S9(4).                            00000460
      *--                                                                       
             03 MNSOCUTILL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNSOCUTILF   PIC X.                                     00000470
             03 FILLER  PIC X(4).                                       00000480
             03 MNSOCUTILI   PIC X(3).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUUTILL  COMP PIC S9(4).                            00000500
      *--                                                                       
             03 MNLIEUUTILL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNLIEUUTILF  PIC X.                                     00000510
             03 FILLER  PIC X(4).                                       00000520
             03 MNLIEUUTILI  PIC X(3).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000540
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000550
             03 FILLER  PIC X(4).                                       00000560
             03 MDEFFETI     PIC X(10).                                 00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPUTILL   COMP PIC S9(4).                            00000580
      *--                                                                       
             03 MCTYPUTILL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCTYPUTILF   PIC X.                                     00000590
             03 FILLER  PIC X(4).                                       00000600
             03 MCTYPUTILI   PIC X(5).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMOTIFAVL   COMP PIC S9(4).                            00000620
      *--                                                                       
             03 MCMOTIFAVL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCMOTIFAVF   PIC X.                                     00000630
             03 FILLER  PIC X(4).                                       00000640
             03 MCMOTIFAVI   PIC X(5).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPMONTANTL   COMP PIC S9(4).                            00000660
      *--                                                                       
             03 MPMONTANTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MPMONTANTF   PIC X.                                     00000670
             03 FILLER  PIC X(4).                                       00000680
             03 MPMONTANTI   PIC X(10).                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCDEVISEL    COMP PIC S9(4).                            00000700
      *--                                                                       
             03 MCDEVISEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCDEVISEF    PIC X.                                     00000710
             03 FILLER  PIC X(4).                                       00000720
             03 MCDEVISEI    PIC X(3).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEMISL      COMP PIC S9(4).                            00000740
      *--                                                                       
             03 MDEMISL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDEMISF      PIC X.                                     00000750
             03 FILLER  PIC X(4).                                       00000760
             03 MDEMISI      PIC X(10).                                 00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLIBERRI  PIC X(78).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCODTRAI  PIC X(4).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCICSI    PIC X(5).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MNETNAMI  PIC X(8).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MSCREENI  PIC X(4).                                       00000970
      ***************************************************************** 00000980
      * SDF: EAV12   EAV12                                              00000990
      ***************************************************************** 00001000
       01   EAV12O REDEFINES EAV12I.                                    00001010
           02 FILLER    PIC X(12).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MDATJOUA  PIC X.                                          00001040
           02 MDATJOUC  PIC X.                                          00001050
           02 MDATJOUP  PIC X.                                          00001060
           02 MDATJOUH  PIC X.                                          00001070
           02 MDATJOUV  PIC X.                                          00001080
           02 MDATJOUO  PIC X(10).                                      00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MTIMJOUA  PIC X.                                          00001110
           02 MTIMJOUC  PIC X.                                          00001120
           02 MTIMJOUP  PIC X.                                          00001130
           02 MTIMJOUH  PIC X.                                          00001140
           02 MTIMJOUV  PIC X.                                          00001150
           02 MTIMJOUO  PIC X(5).                                       00001160
      * n� de societe                                                   00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MNSOCSAISIEA   PIC X.                                     00001190
           02 MNSOCSAISIEC   PIC X.                                     00001200
           02 MNSOCSAISIEP   PIC X.                                     00001210
           02 MNSOCSAISIEH   PIC X.                                     00001220
           02 MNSOCSAISIEV   PIC X.                                     00001230
           02 MNSOCSAISIEO   PIC X(3).                                  00001240
      * n� de lieu                                                      00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MNLIEUSAISIEA  PIC X.                                     00001270
           02 MNLIEUSAISIEC  PIC X.                                     00001280
           02 MNLIEUSAISIEP  PIC X.                                     00001290
           02 MNLIEUSAISIEH  PIC X.                                     00001300
           02 MNLIEUSAISIEV  PIC X.                                     00001310
           02 MNLIEUSAISIEO  PIC X(3).                                  00001320
      * libell� du lieu                                                 00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MLLIEUSAISIEA  PIC X.                                     00001350
           02 MLLIEUSAISIEC  PIC X.                                     00001360
           02 MLLIEUSAISIEP  PIC X.                                     00001370
           02 MLLIEUSAISIEH  PIC X.                                     00001380
           02 MLLIEUSAISIEV  PIC X.                                     00001390
           02 MLLIEUSAISIEO  PIC X(20).                                 00001400
           02 MLIGNEO OCCURS   13 TIMES .                               00001410
             03 FILLER       PIC X(2).                                  00001420
             03 MNSOCIETEA   PIC X.                                     00001430
             03 MNSOCIETEC   PIC X.                                     00001440
             03 MNSOCIETEP   PIC X.                                     00001450
             03 MNSOCIETEH   PIC X.                                     00001460
             03 MNSOCIETEV   PIC X.                                     00001470
             03 MNSOCIETEO   PIC X(3).                                  00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MNLIEUA      PIC X.                                     00001500
             03 MNLIEUC PIC X.                                          00001510
             03 MNLIEUP PIC X.                                          00001520
             03 MNLIEUH PIC X.                                          00001530
             03 MNLIEUV PIC X.                                          00001540
             03 MNLIEUO      PIC X(3).                                  00001550
             03 FILLER       PIC X(2).                                  00001560
             03 MCTYPAVOIRA  PIC X.                                     00001570
             03 MCTYPAVOIRC  PIC X.                                     00001580
             03 MCTYPAVOIRP  PIC X.                                     00001590
             03 MCTYPAVOIRH  PIC X.                                     00001600
             03 MCTYPAVOIRV  PIC X.                                     00001610
             03 MCTYPAVOIRO  PIC X.                                     00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MNAVOIRA     PIC X.                                     00001640
             03 MNAVOIRC     PIC X.                                     00001650
             03 MNAVOIRP     PIC X.                                     00001660
             03 MNAVOIRH     PIC X.                                     00001670
             03 MNAVOIRV     PIC X.                                     00001680
             03 MNAVOIRO     PIC X(7).                                  00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MNSOCUTILA   PIC X.                                     00001710
             03 MNSOCUTILC   PIC X.                                     00001720
             03 MNSOCUTILP   PIC X.                                     00001730
             03 MNSOCUTILH   PIC X.                                     00001740
             03 MNSOCUTILV   PIC X.                                     00001750
             03 MNSOCUTILO   PIC X(3).                                  00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MNLIEUUTILA  PIC X.                                     00001780
             03 MNLIEUUTILC  PIC X.                                     00001790
             03 MNLIEUUTILP  PIC X.                                     00001800
             03 MNLIEUUTILH  PIC X.                                     00001810
             03 MNLIEUUTILV  PIC X.                                     00001820
             03 MNLIEUUTILO  PIC X(3).                                  00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MDEFFETA     PIC X.                                     00001850
             03 MDEFFETC     PIC X.                                     00001860
             03 MDEFFETP     PIC X.                                     00001870
             03 MDEFFETH     PIC X.                                     00001880
             03 MDEFFETV     PIC X.                                     00001890
             03 MDEFFETO     PIC X(10).                                 00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MCTYPUTILA   PIC X.                                     00001920
             03 MCTYPUTILC   PIC X.                                     00001930
             03 MCTYPUTILP   PIC X.                                     00001940
             03 MCTYPUTILH   PIC X.                                     00001950
             03 MCTYPUTILV   PIC X.                                     00001960
             03 MCTYPUTILO   PIC X(5).                                  00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MCMOTIFAVA   PIC X.                                     00001990
             03 MCMOTIFAVC   PIC X.                                     00002000
             03 MCMOTIFAVP   PIC X.                                     00002010
             03 MCMOTIFAVH   PIC X.                                     00002020
             03 MCMOTIFAVV   PIC X.                                     00002030
             03 MCMOTIFAVO   PIC X(5).                                  00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MPMONTANTA   PIC X.                                     00002060
             03 MPMONTANTC   PIC X.                                     00002070
             03 MPMONTANTP   PIC X.                                     00002080
             03 MPMONTANTH   PIC X.                                     00002090
             03 MPMONTANTV   PIC X.                                     00002100
             03 MPMONTANTO   PIC X(10).                                 00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MCDEVISEA    PIC X.                                     00002130
             03 MCDEVISEC    PIC X.                                     00002140
             03 MCDEVISEP    PIC X.                                     00002150
             03 MCDEVISEH    PIC X.                                     00002160
             03 MCDEVISEV    PIC X.                                     00002170
             03 MCDEVISEO    PIC X(3).                                  00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MDEMISA      PIC X.                                     00002200
             03 MDEMISC PIC X.                                          00002210
             03 MDEMISP PIC X.                                          00002220
             03 MDEMISH PIC X.                                          00002230
             03 MDEMISV PIC X.                                          00002240
             03 MDEMISO      PIC X(10).                                 00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MLIBERRA  PIC X.                                          00002270
           02 MLIBERRC  PIC X.                                          00002280
           02 MLIBERRP  PIC X.                                          00002290
           02 MLIBERRH  PIC X.                                          00002300
           02 MLIBERRV  PIC X.                                          00002310
           02 MLIBERRO  PIC X(78).                                      00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MCODTRAA  PIC X.                                          00002340
           02 MCODTRAC  PIC X.                                          00002350
           02 MCODTRAP  PIC X.                                          00002360
           02 MCODTRAH  PIC X.                                          00002370
           02 MCODTRAV  PIC X.                                          00002380
           02 MCODTRAO  PIC X(4).                                       00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MCICSA    PIC X.                                          00002410
           02 MCICSC    PIC X.                                          00002420
           02 MCICSP    PIC X.                                          00002430
           02 MCICSH    PIC X.                                          00002440
           02 MCICSV    PIC X.                                          00002450
           02 MCICSO    PIC X(5).                                       00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MNETNAMA  PIC X.                                          00002480
           02 MNETNAMC  PIC X.                                          00002490
           02 MNETNAMP  PIC X.                                          00002500
           02 MNETNAMH  PIC X.                                          00002510
           02 MNETNAMV  PIC X.                                          00002520
           02 MNETNAMO  PIC X(8).                                       00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MSCREENA  PIC X.                                          00002550
           02 MSCREENC  PIC X.                                          00002560
           02 MSCREENP  PIC X.                                          00002570
           02 MSCREENH  PIC X.                                          00002580
           02 MSCREENV  PIC X.                                          00002590
           02 MSCREENO  PIC X(4).                                       00002600
                                                                                
