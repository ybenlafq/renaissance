      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPV002 AU 19/06/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,08,BI,A,                          *        
      *                           15,03,BI,A,                          *        
      *                           18,05,BI,A,                          *        
      *                           23,05,BI,A,                          *        
      *                           28,03,PD,A,                          *        
      *                           31,20,BI,A,                          *        
      *                           51,03,BI,A,                          *        
      *                           54,07,BI,A,                          *        
      *                           61,06,PD,A,                          *        
      *                           67,08,BI,A,                          *        
      *                           75,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPV002.                                                        
            05 NOMETAT-IPV002           PIC X(6) VALUE 'IPV002'.                
            05 RUPTURES-IPV002.                                                 
           10 IPV002-FDATE              PIC X(08).                      007  008
           10 IPV002-NSOCIETE           PIC X(03).                      015  003
           10 IPV002-CHEFPROD           PIC X(05).                      018  005
           10 IPV002-CMARQ              PIC X(05).                      023  005
           10 IPV002-WSEQFAM            PIC S9(05)      COMP-3.         028  003
           10 IPV002-LREFFOURN          PIC X(20).                      031  020
           10 IPV002-NLIEU              PIC X(03).                      051  003
           10 IPV002-NCODIC             PIC X(07).                      054  007
           10 IPV002-PEXPTTC            PIC S9(08)V9(2) COMP-3.         061  006
           10 IPV002-DEFFET             PIC X(08).                      067  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPV002-SEQUENCE           PIC S9(04) COMP.                075  002
      *--                                                                       
           10 IPV002-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPV002.                                                   
           10 IPV002-CFAM               PIC X(05).                      077  005
           10 IPV002-CPROGRAMME         PIC X(05).                      082  005
           10 IPV002-LENSCONC           PIC X(15).                      087  015
           10 IPV002-NCONC              PIC X(04).                      102  004
           10 IPV002-NZONPRIX           PIC X(02).                      106  002
           10 IPV002-PSTDTTC            PIC S9(08)V9(2) COMP-3.         108  006
           10 IPV002-DFINEFFET          PIC X(08).                      114  008
            05 FILLER                      PIC X(391).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPV002-LONG           PIC S9(4)   COMP  VALUE +121.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPV002-LONG           PIC S9(4) COMP-5  VALUE +121.           
                                                                                
      *}                                                                        
