      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *********************************************************         00010004
      * MESSAGE DU MEC06 / BEC306                                       00020004
      * ANNO RECUE DE MORPHEUS                                          00030004
       01 W-MESSAGE-MEC06.                                              00110005
           10 W-ENTETE-MEC06.                                           00111004
             15 W-COMM-TYPE                  PIC X(08).                 00120004
             15 W-COMM-NMUTATION             PIC X(07).                 00130004
             15 W-COMM-DATE                  PIC X(08).                 00140004
             15 W-COMM-QTERECU               PIC X(07).                 00150004
             15 W-COMM-NBPROD                PIC X(05).                 00160004
           10 W-DATA-MEC06     OCCURS  1000.                            00170004
             15 W-COMM-NSOC                  PIC X(03).                 00180004
             15 W-COMM-NLIEU                 PIC X(03).                 00190004
             15 W-COMM-NVENTE                PIC X(07).                 00200004
             15 W-COMM-NCODIC                PIC X(07).                 00210004
             15 W-COMM-NSEQ                  PIC X(05).                 00220004
             15 W-COMM-QTE                   PIC X(05).                 00230004
                                                                                
