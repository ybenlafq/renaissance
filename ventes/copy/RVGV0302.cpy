      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * DCLGEN TABLE(DSA000.RTGV03)                                    *        
      *        LIBRARY(DSA024.DEVL.SOURCE(RVGV0302))                   *        
      *        ACTION(REPLACE)                                         *        
      *        LANGUAGE(COBOL)                                         *        
      *        APOST                                                   *        
      *        INDVAR(YES)                                             *        
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *        
      ******************************************************************        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RTGV03                      *        
      ******************************************************************        
       01  RVGV0302.                                                            
           10 GV03-NSOCIETE        PIC X(3).                                    
           10 GV03-NLIEU           PIC X(3).                                    
           10 GV03-NORDRE          PIC X(5).                                    
           10 GV03-NVENTE          PIC X(7).                                    
           10 GV03-WTYPEADR        PIC X(1).                                    
           10 GV03-EMAIL           PIC X(63).                                   
           10 GV03-DSYST           PIC S9(13)V USAGE COMP-3.                    
           10 GV03-CPORTE          PIC X(10).                                   
           10 GV03-WADPRP          PIC X(1).                                    
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  IRTGV03.                                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INDSTRUC           PIC S9(4) USAGE COMP OCCURS 9 TIMES.           
      *--                                                                       
           10 INDSTRUC           PIC S9(4) COMP-5 OCCURS 9 TIMES.               
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 9       *        
      ******************************************************************        
                                                                                
