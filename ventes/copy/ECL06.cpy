      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ECL06   ECL06                                              00000020
      ***************************************************************** 00000030
       01   ECL06I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEML   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEMF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMI   PIC X(3).                                       00000210
           02 MLIGNESI OCCURS   14 TIMES .                              00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCLIEUINVL   COMP PIC S9(4).                            00000230
      *--                                                                       
             03 MCLIEUINVL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCLIEUINVF   PIC X.                                     00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MCLIEUINVI   PIC X(5).                                  00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLLIEUINVL   COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MLLIEUINVL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLLIEUINVF   PIC X.                                     00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MLLIEUINVI   PIC X(20).                                 00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWEXPOL      COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MWEXPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWEXPOF      PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MWEXPOI      PIC X.                                     00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEMPLL      COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MCEMPLL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCEMPLF      PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MCEMPLI      PIC X(5).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWGLOTSL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MWGLOTSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWGLOTSF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MWGLOTSI     PIC X.                                     00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSEQEDTL    COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNSEQEDTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNSEQEDTF    PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNSEQEDTI    PIC X(3).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MLIBERRI  PIC X(78).                                      00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MCODTRAI  PIC X(4).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MCICSI    PIC X(5).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MNETNAMI  PIC X(8).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MSCREENI  PIC X(4).                                       00000660
      ***************************************************************** 00000670
      * SDF: ECL06   ECL06                                              00000680
      ***************************************************************** 00000690
       01   ECL06O REDEFINES ECL06I.                                    00000700
           02 FILLER    PIC X(12).                                      00000710
           02 FILLER    PIC X(2).                                       00000720
           02 MDATJOUA  PIC X.                                          00000730
           02 MDATJOUC  PIC X.                                          00000740
           02 MDATJOUP  PIC X.                                          00000750
           02 MDATJOUH  PIC X.                                          00000760
           02 MDATJOUV  PIC X.                                          00000770
           02 MDATJOUO  PIC X(10).                                      00000780
           02 FILLER    PIC X(2).                                       00000790
           02 MTIMJOUA  PIC X.                                          00000800
           02 MTIMJOUC  PIC X.                                          00000810
           02 MTIMJOUP  PIC X.                                          00000820
           02 MTIMJOUH  PIC X.                                          00000830
           02 MTIMJOUV  PIC X.                                          00000840
           02 MTIMJOUO  PIC X(5).                                       00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MPAGEA    PIC X.                                          00000870
           02 MPAGEC    PIC X.                                          00000880
           02 MPAGEP    PIC X.                                          00000890
           02 MPAGEH    PIC X.                                          00000900
           02 MPAGEV    PIC X.                                          00000910
           02 MPAGEO    PIC X(3).                                       00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MPAGEMA   PIC X.                                          00000940
           02 MPAGEMC   PIC X.                                          00000950
           02 MPAGEMP   PIC X.                                          00000960
           02 MPAGEMH   PIC X.                                          00000970
           02 MPAGEMV   PIC X.                                          00000980
           02 MPAGEMO   PIC X(3).                                       00000990
           02 MLIGNESO OCCURS   14 TIMES .                              00001000
             03 FILLER       PIC X(2).                                  00001010
             03 MCLIEUINVA   PIC X.                                     00001020
             03 MCLIEUINVC   PIC X.                                     00001030
             03 MCLIEUINVP   PIC X.                                     00001040
             03 MCLIEUINVH   PIC X.                                     00001050
             03 MCLIEUINVV   PIC X.                                     00001060
             03 MCLIEUINVO   PIC X(5).                                  00001070
             03 FILLER       PIC X(2).                                  00001080
             03 MLLIEUINVA   PIC X.                                     00001090
             03 MLLIEUINVC   PIC X.                                     00001100
             03 MLLIEUINVP   PIC X.                                     00001110
             03 MLLIEUINVH   PIC X.                                     00001120
             03 MLLIEUINVV   PIC X.                                     00001130
             03 MLLIEUINVO   PIC X(20).                                 00001140
             03 FILLER       PIC X(2).                                  00001150
             03 MWEXPOA      PIC X.                                     00001160
             03 MWEXPOC PIC X.                                          00001170
             03 MWEXPOP PIC X.                                          00001180
             03 MWEXPOH PIC X.                                          00001190
             03 MWEXPOV PIC X.                                          00001200
             03 MWEXPOO      PIC X.                                     00001210
             03 FILLER       PIC X(2).                                  00001220
             03 MCEMPLA      PIC X.                                     00001230
             03 MCEMPLC PIC X.                                          00001240
             03 MCEMPLP PIC X.                                          00001250
             03 MCEMPLH PIC X.                                          00001260
             03 MCEMPLV PIC X.                                          00001270
             03 MCEMPLO      PIC X(5).                                  00001280
             03 FILLER       PIC X(2).                                  00001290
             03 MWGLOTSA     PIC X.                                     00001300
             03 MWGLOTSC     PIC X.                                     00001310
             03 MWGLOTSP     PIC X.                                     00001320
             03 MWGLOTSH     PIC X.                                     00001330
             03 MWGLOTSV     PIC X.                                     00001340
             03 MWGLOTSO     PIC X.                                     00001350
             03 FILLER       PIC X(2).                                  00001360
             03 MNSEQEDTA    PIC X.                                     00001370
             03 MNSEQEDTC    PIC X.                                     00001380
             03 MNSEQEDTP    PIC X.                                     00001390
             03 MNSEQEDTH    PIC X.                                     00001400
             03 MNSEQEDTV    PIC X.                                     00001410
             03 MNSEQEDTO    PIC X(3).                                  00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MLIBERRA  PIC X.                                          00001440
           02 MLIBERRC  PIC X.                                          00001450
           02 MLIBERRP  PIC X.                                          00001460
           02 MLIBERRH  PIC X.                                          00001470
           02 MLIBERRV  PIC X.                                          00001480
           02 MLIBERRO  PIC X(78).                                      00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MCODTRAA  PIC X.                                          00001510
           02 MCODTRAC  PIC X.                                          00001520
           02 MCODTRAP  PIC X.                                          00001530
           02 MCODTRAH  PIC X.                                          00001540
           02 MCODTRAV  PIC X.                                          00001550
           02 MCODTRAO  PIC X(4).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MCICSA    PIC X.                                          00001580
           02 MCICSC    PIC X.                                          00001590
           02 MCICSP    PIC X.                                          00001600
           02 MCICSH    PIC X.                                          00001610
           02 MCICSV    PIC X.                                          00001620
           02 MCICSO    PIC X(5).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MNETNAMA  PIC X.                                          00001650
           02 MNETNAMC  PIC X.                                          00001660
           02 MNETNAMP  PIC X.                                          00001670
           02 MNETNAMH  PIC X.                                          00001680
           02 MNETNAMV  PIC X.                                          00001690
           02 MNETNAMO  PIC X(8).                                       00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MSCREENA  PIC X.                                          00001720
           02 MSCREENC  PIC X.                                          00001730
           02 MSCREENP  PIC X.                                          00001740
           02 MSCREENH  PIC X.                                          00001750
           02 MSCREENV  PIC X.                                          00001760
           02 MSCREENO  PIC X(4).                                       00001770
                                                                                
