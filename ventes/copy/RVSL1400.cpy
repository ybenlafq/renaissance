      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      *****             COPY DE LA TABLE RVSL1400.                 *****        
      *****     LISTE DES HOST VARIABLES DE LA TABLE RVSL1400.     *****        
      ******************************************************************        
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSL1400.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSL1400.                                                            
      *}                                                                        
           02  SL14-CPROG            PIC  X(05).                                
           02  SL14-COPER            PIC  X(10).                                
           02  SL14-CTYPDOC          PIC  X(02).                                
           02  SL14-COPAR            PIC  X(05).                                
           02  SL14-EVENT            PIC  X(01).                                
           02  SL14-NSEQ             PIC  X(01).                                
           02  SL14-LOPER            PIC  X(20).                                
           02  SL14-WHOST            PIC  X(01).                                
           02  SL14-WCREATSL         PIC  X(01).                                
           02  SL14-NSOCO            PIC  X(03).                                
           02  SL14-NLIEUO           PIC  X(03).                                
           02  SL14-NSSLIEUO         PIC  X(03).                                
           02  SL14-CLIEUTRTO        PIC  X(05).                                
           02  SL14-NSOCD            PIC  X(03).                                
           02  SL14-NLIEUD           PIC  X(03).                                
           02  SL14-NSSLIEUD         PIC  X(03).                                
           02  SL14-CLIEUTRTD        PIC  X(05).                                
           02  SL14-DSYST            PIC  S9(13)  COMP-3.                       
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      ******************************************************************        
      *****           LISTE DES FLAGS DE LA TABLE RVSL1400         *****        
      ******************************************************************        
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSL1400-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSL1400-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL14-CPROG-F          PIC  S9(04)  COMP.                         
      *--                                                                       
           02  SL14-CPROG-F          PIC  S9(04) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL14-COPER-F          PIC  S9(04)  COMP.                         
      *--                                                                       
           02  SL14-COPER-F          PIC  S9(04) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL14-CTYPDOC-F        PIC  S9(04)  COMP.                         
      *--                                                                       
           02  SL14-CTYPDOC-F        PIC  S9(04) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL14-COPAR-F          PIC  S9(04)  COMP.                         
      *--                                                                       
           02  SL14-COPAR-F          PIC  S9(04) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL14-EVENT-F          PIC  S9(04)  COMP.                         
      *--                                                                       
           02  SL14-EVENT-F          PIC  S9(04) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL14-NSEQ-F           PIC  S9(04)  COMP.                         
      *--                                                                       
           02  SL14-NSEQ-F           PIC  S9(04) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL14-LOPER-F          PIC  S9(04)  COMP.                         
      *--                                                                       
           02  SL14-LOPER-F          PIC  S9(04) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL14-WHOST-F          PIC  S9(04)  COMP.                         
      *--                                                                       
           02  SL14-WHOST-F          PIC  S9(04) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL14-WCREATSL-F       PIC  S9(04)  COMP.                         
      *--                                                                       
           02  SL14-WCREATSL-F       PIC  S9(04) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL14-NSOCO-F          PIC  S9(04)  COMP.                         
      *--                                                                       
           02  SL14-NSOCO-F          PIC  S9(04) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL14-NLIEUO-F         PIC  S9(04)  COMP.                         
      *--                                                                       
           02  SL14-NLIEUO-F         PIC  S9(04) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL14-NSSLIEUO-F       PIC  S9(04)  COMP.                         
      *--                                                                       
           02  SL14-NSSLIEUO-F       PIC  S9(04) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL14-CLIEUTRTO-F      PIC  S9(04)  COMP.                         
      *--                                                                       
           02  SL14-CLIEUTRTO-F      PIC  S9(04) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL14-NSOCD-F          PIC  S9(04)  COMP.                         
      *--                                                                       
           02  SL14-NSOCD-F          PIC  S9(04) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL14-NLIEUD-F         PIC  S9(04)  COMP.                         
      *--                                                                       
           02  SL14-NLIEUD-F         PIC  S9(04) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL14-NSSLIEUD-F       PIC  S9(04)  COMP.                         
      *--                                                                       
           02  SL14-NSSLIEUD-F       PIC  S9(04) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL14-CLIEUTRTD-F      PIC  S9(04)  COMP.                         
      *--                                                                       
           02  SL14-CLIEUTRTD-F      PIC  S9(04) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL14-DSYST-F          PIC  S9(04)  COMP.                         
      *--                                                                       
           02  SL14-DSYST-F          PIC  S9(04) COMP-5.                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
      *                                                                         
                                                                                
