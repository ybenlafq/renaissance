      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EBA12   EBA12                                              00000020
      ***************************************************************** 00000030
       01   EBA12I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNUMPAGEI      PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECEPL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDRECEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRECEPF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDRECEPI  PIC X(10).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPECARTL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MPECARTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPECARTF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MPECARTI  PIC X(11).                                      00000330
           02 MLIGNESI OCCURS   12 TIMES .                              00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNECARTL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MNECARTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNECARTF     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MNECARTI     PIC X(2).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMODRGLTL   COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MCMODRGLTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCMODRGLTF   PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCMODRGLTI   PIC X(5).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLECARTL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MLECARTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLECARTF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MLECARTI     PIC X(20).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSIGNEL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MWSIGNEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWSIGNEF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MWSIGNEI     PIC X.                                     00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPMONTANTL   COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MPMONTANTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MPMONTANTF   PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MPMONTANTI   PIC X(11).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNTIERSL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNTIERSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNTIERSF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNTIERSI     PIC X(6).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLETTREL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNLETTREL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNLETTREF    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNLETTREI    PIC X(10).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MZONCMDI  PIC X(15).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLIBERRI  PIC X(58).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCICSI    PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNETNAMI  PIC X(8).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MSCREENI  PIC X(4).                                       00000860
      ***************************************************************** 00000870
      * SDF: EBA12   EBA12                                              00000880
      ***************************************************************** 00000890
       01   EBA12O REDEFINES EBA12I.                                    00000900
           02 FILLER    PIC X(12).                                      00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MDATJOUA  PIC X.                                          00000930
           02 MDATJOUC  PIC X.                                          00000940
           02 MDATJOUP  PIC X.                                          00000950
           02 MDATJOUH  PIC X.                                          00000960
           02 MDATJOUV  PIC X.                                          00000970
           02 MDATJOUO  PIC X(10).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MTIMJOUA  PIC X.                                          00001000
           02 MTIMJOUC  PIC X.                                          00001010
           02 MTIMJOUP  PIC X.                                          00001020
           02 MTIMJOUH  PIC X.                                          00001030
           02 MTIMJOUV  PIC X.                                          00001040
           02 MTIMJOUO  PIC X(5).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MNUMPAGEA      PIC X.                                     00001070
           02 MNUMPAGEC PIC X.                                          00001080
           02 MNUMPAGEP PIC X.                                          00001090
           02 MNUMPAGEH PIC X.                                          00001100
           02 MNUMPAGEV PIC X.                                          00001110
           02 MNUMPAGEO      PIC X(2).                                  00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MPAGEMAXA      PIC X.                                     00001140
           02 MPAGEMAXC PIC X.                                          00001150
           02 MPAGEMAXP PIC X.                                          00001160
           02 MPAGEMAXH PIC X.                                          00001170
           02 MPAGEMAXV PIC X.                                          00001180
           02 MPAGEMAXO      PIC X(2).                                  00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MNLIEUA   PIC X.                                          00001210
           02 MNLIEUC   PIC X.                                          00001220
           02 MNLIEUP   PIC X.                                          00001230
           02 MNLIEUH   PIC X.                                          00001240
           02 MNLIEUV   PIC X.                                          00001250
           02 MNLIEUO   PIC X(3).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MDRECEPA  PIC X.                                          00001280
           02 MDRECEPC  PIC X.                                          00001290
           02 MDRECEPP  PIC X.                                          00001300
           02 MDRECEPH  PIC X.                                          00001310
           02 MDRECEPV  PIC X.                                          00001320
           02 MDRECEPO  PIC X(10).                                      00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MPECARTA  PIC X.                                          00001350
           02 MPECARTC  PIC X.                                          00001360
           02 MPECARTP  PIC X.                                          00001370
           02 MPECARTH  PIC X.                                          00001380
           02 MPECARTV  PIC X.                                          00001390
           02 MPECARTO  PIC X(11).                                      00001400
           02 MLIGNESO OCCURS   12 TIMES .                              00001410
             03 FILLER       PIC X(2).                                  00001420
             03 MNECARTA     PIC X.                                     00001430
             03 MNECARTC     PIC X.                                     00001440
             03 MNECARTP     PIC X.                                     00001450
             03 MNECARTH     PIC X.                                     00001460
             03 MNECARTV     PIC X.                                     00001470
             03 MNECARTO     PIC X(2).                                  00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MCMODRGLTA   PIC X.                                     00001500
             03 MCMODRGLTC   PIC X.                                     00001510
             03 MCMODRGLTP   PIC X.                                     00001520
             03 MCMODRGLTH   PIC X.                                     00001530
             03 MCMODRGLTV   PIC X.                                     00001540
             03 MCMODRGLTO   PIC X(5).                                  00001550
             03 FILLER       PIC X(2).                                  00001560
             03 MLECARTA     PIC X.                                     00001570
             03 MLECARTC     PIC X.                                     00001580
             03 MLECARTP     PIC X.                                     00001590
             03 MLECARTH     PIC X.                                     00001600
             03 MLECARTV     PIC X.                                     00001610
             03 MLECARTO     PIC X(20).                                 00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MWSIGNEA     PIC X.                                     00001640
             03 MWSIGNEC     PIC X.                                     00001650
             03 MWSIGNEP     PIC X.                                     00001660
             03 MWSIGNEH     PIC X.                                     00001670
             03 MWSIGNEV     PIC X.                                     00001680
             03 MWSIGNEO     PIC X.                                     00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MPMONTANTA   PIC X.                                     00001710
             03 MPMONTANTC   PIC X.                                     00001720
             03 MPMONTANTP   PIC X.                                     00001730
             03 MPMONTANTH   PIC X.                                     00001740
             03 MPMONTANTV   PIC X.                                     00001750
             03 MPMONTANTO   PIC X(11).                                 00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MNTIERSA     PIC X.                                     00001780
             03 MNTIERSC     PIC X.                                     00001790
             03 MNTIERSP     PIC X.                                     00001800
             03 MNTIERSH     PIC X.                                     00001810
             03 MNTIERSV     PIC X.                                     00001820
             03 MNTIERSO     PIC X(6).                                  00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MNLETTREA    PIC X.                                     00001850
             03 MNLETTREC    PIC X.                                     00001860
             03 MNLETTREP    PIC X.                                     00001870
             03 MNLETTREH    PIC X.                                     00001880
             03 MNLETTREV    PIC X.                                     00001890
             03 MNLETTREO    PIC X(10).                                 00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MZONCMDA  PIC X.                                          00001920
           02 MZONCMDC  PIC X.                                          00001930
           02 MZONCMDP  PIC X.                                          00001940
           02 MZONCMDH  PIC X.                                          00001950
           02 MZONCMDV  PIC X.                                          00001960
           02 MZONCMDO  PIC X(15).                                      00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLIBERRA  PIC X.                                          00001990
           02 MLIBERRC  PIC X.                                          00002000
           02 MLIBERRP  PIC X.                                          00002010
           02 MLIBERRH  PIC X.                                          00002020
           02 MLIBERRV  PIC X.                                          00002030
           02 MLIBERRO  PIC X(58).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCODTRAA  PIC X.                                          00002060
           02 MCODTRAC  PIC X.                                          00002070
           02 MCODTRAP  PIC X.                                          00002080
           02 MCODTRAH  PIC X.                                          00002090
           02 MCODTRAV  PIC X.                                          00002100
           02 MCODTRAO  PIC X(4).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCICSA    PIC X.                                          00002130
           02 MCICSC    PIC X.                                          00002140
           02 MCICSP    PIC X.                                          00002150
           02 MCICSH    PIC X.                                          00002160
           02 MCICSV    PIC X.                                          00002170
           02 MCICSO    PIC X(5).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNETNAMA  PIC X.                                          00002200
           02 MNETNAMC  PIC X.                                          00002210
           02 MNETNAMP  PIC X.                                          00002220
           02 MNETNAMH  PIC X.                                          00002230
           02 MNETNAMV  PIC X.                                          00002240
           02 MNETNAMO  PIC X(8).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MSCREENA  PIC X.                                          00002270
           02 MSCREENC  PIC X.                                          00002280
           02 MSCREENP  PIC X.                                          00002290
           02 MSCREENH  PIC X.                                          00002300
           02 MSCREENV  PIC X.                                          00002310
           02 MSCREENO  PIC X(4).                                       00002320
                                                                                
