      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00010000
      *   COPY MESSAGE MQ ENCAISSEMENT WEBSPHERE COMMERCE               00020000
      ***************************************************************** 00080000
      *                                                                 00090000
       01  WS-MQ10-EC12.                                                00100000
         02 WS-QUEUE-EC12.                                              00110000
               10   MQ10-MSGID-EC12     PIC    X(24).                   00120000
               10   MQ10-CORRELID-EC12  PIC    X(24).                   00130000
         02 WS-CODRET-EC12              PIC    XX.                      00140000
         02  MESEC12.                                                   00150000
           03 MESEC12-ENTETE.                                           00160000
               05   MESEC12-TYPE     PIC    X(3).                       00170000
               05   MESEC12-NSOCMSG PIC   X(3).                         00180000
               05   MESEC12-NLIEUMSG PIC  X(3).                         00190000
               05   MESEC12-NSOCDST PIC   X(3).                         00200000
               05   MESEC12-NLIEUDST PIC  X(3).                         00210000
               05   MESEC12-NORD   PIC    9(8).                         00220000
               05   MESEC12-LPROG  PIC    X(10).                        00230000
               05   MESEC12-DJOUR  PIC    X(8).                         00240000
               05   MESEC12-WSID   PIC    X(10).                        00250000
               05   MESEC12-USER   PIC    X(10).                        00260000
               05   MESEC12-CHRONO PIC    9(7).                         00270000
               05   MESEC12-NBRMSG PIC    9(7).                         00280000
               05   MESEC12-NBROCC PIC    X(05).                        00300000
               05   MESEC12-LGNOCC PIC    X(05).                        00310000
               05   MESEC12-VERSION PIC   X(02).                        00320000
               05   MESEC12-FILLER PIC    X(18).                        00330000
                                                                        00340000
         02 MESEC12-DATA.                                               00350000
           05  MESEC12-NSOCVTE     PIC    X(3).                         00360000
           05  MESEC12-NLIEUVTE    PIC    X(3).                         00370000
           05  MESEC12-NVENTE      PIC    X(7).                         00380000
           05  MESEC12-FLAG-DUPLI            PIC X(01).                 00440000
             88 W-COMM-FLAG-DUPLI-VENTE VALUE 'D'.                      00450000
           05 MESEC12-FONC             PIC X(01).                       00460000
             88 MESEC12-FONC-CEN        VALUE 'C'.                      00470000
      *{ remove-comma-in-dde 1.5                                                
      *      88 MESEC12-FONC-REMBT      VALUE 'R' , '2'.                00480000
      *--                                                                       
             88 MESEC12-FONC-REMBT      VALUE 'R'   '2'.                        
      *}                                                                        
             88 MESEC12-FLAG-REMBT-NOR  VALUE 'R'.                      00490000
             88 MESEC12-FLAG-REMBT-RD2  VALUE '2'.                      00500000
                                                                                
