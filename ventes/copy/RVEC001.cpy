      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 10/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EC001 .COM BALISE PAR ENTITE           *        
      *----------------------------------------------------------------*        
       01  RVEC001.                                                             
           05  EC001-CTABLEG2    PIC X(15).                                     
           05  EC001-CTABLEG2-REDEF REDEFINES EC001-CTABLEG2.                   
               10  EC001-IDSI            PIC X(10).                             
           05  EC001-WTABLEG     PIC X(80).                                     
           05  EC001-WTABLEG-REDEF  REDEFINES EC001-WTABLEG.                    
               10  EC001-IDXML           PIC X(16).                             
               10  EC001-IDROOT          PIC X(17).                             
               10  EC001-DTDNAME         PIC X(20).                             
               10  EC001-WTRT            PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVEC001-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EC001-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EC001-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EC001-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EC001-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
