      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVIT1000                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIT1000                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIT1000.                                                    00000090
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIT1000.                                                            
      *}                                                                        
           02  IT10-NINVENTAIRE                                         00000100
               PIC X(0005).                                             00000110
           02  IT10-CFAM                                                00000120
               PIC X(0005).                                             00000130
           02  IT10-DSYST                                               00000140
               PIC S9(13) COMP-3.                                       00000150
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00000160
      *---------------------------------------------------------        00000170
      *   LISTE DES FLAGS DE LA TABLE RVIT1000                          00000180
      *---------------------------------------------------------        00000190
      *                                                                 00000200
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIT1000-FLAGS.                                              00000210
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIT1000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT10-NINVENTAIRE-F                                       00000220
      *        PIC S9(4) COMP.                                          00000230
      *--                                                                       
           02  IT10-NINVENTAIRE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT10-CFAM-F                                              00000240
      *        PIC S9(4) COMP.                                          00000250
      *--                                                                       
           02  IT10-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT10-DSYST-F                                             00000260
      *        PIC S9(4) COMP.                                          00000270
      *--                                                                       
           02  IT10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                            00000280
                                                                                
