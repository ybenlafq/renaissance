      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-CL01-LONG            PIC  S9(4)  COMP-3  VALUE  +37.              
       01  TS-CL01-RECORD.                                                      
           02  TS-CL01-SSLIEU      PIC  X(05).                                  
           02  TS-CL01-LIBELLE     PIC  X(20).                                  
           02  TS-CL01-VALO        PIC  X(01).                                  
           02  TS-CL01-PRES        PIC  X(01).                                  
           02  TS-CL01-VND         PIC  X(01).                                  
           02  TS-CL01-HS          PIC  X(01).                                  
           02  TS-CL01-PRET        PIC  X(01).                                  
           02  TS-CL01-CTR         PIC  X(01).                                  
           02  TS-CL01-ATR         PIC  X(01).                                  
           02  TS-CL01-AUX         PIC  X(01).                                  
           02  TS-CL01-TRANSIT     PIC  X(01).                                  
           02  TS-CL01-DISPO       PIC  X(01).                                  
           02  TS-CL01-SEQ         PIC  S9(03) COMP-3.                          
                                                                                
