      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV02   EGV02                                              00000020
      ***************************************************************** 00000030
       01   EGV02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAGL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MMAGL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MMAGF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MMAGI     PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNVENTEI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIENTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCLIENTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLIENTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCLIENTI  PIC X(9).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVENTEL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDVENTEF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDVENTEI  PIC X(8).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNORDREL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNORDREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNORDREF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNORDREI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMCL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNOMCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNOMCF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNOMCI    PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMCL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLNOMCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLNOMCF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLNOMCI   PIC X(25).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPNOMCL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLPNOMCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPNOMCF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLPNOMCI  PIC X(15).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVOIECL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNVOIECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVOIECF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNVOIECI  PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTPVOIECL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MTPVOIECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTPVOIECF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MTPVOIECI      PIC X(4).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVOIECL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLVOIECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLVOIECF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLVOIECI  PIC X(21).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCBATCL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCBATCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCBATCF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCBATCI   PIC X(3).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCESCCL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCESCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCESCCF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCESCCI   PIC X(3).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCETAGCL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCETAGCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCETAGCF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCETAGCI  PIC X(3).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASCCL   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCASCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCASCCF   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCASCCI   PIC X.                                          00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPORTECL      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MCPORTECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPORTECF      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCPORTECI      PIC X(3).                                  00000810
           02 MADRESCI OCCURS   1 TIMES .                               00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLADDRCL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MLADDRCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLADDRCF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MLADDRCI     PIC X(32).                                 00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 DIGICODCL      COMP PIC S9(4).                            00000870
      *--                                                                       
           02 DIGICODCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 DIGICODCF      PIC X.                                     00000880
           02 FILLER    PIC X(4).                                       00000890
           02 DIGICODCI      PIC X(6).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTCL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCPOSTCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPOSTCF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCPOSTCI  PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCOMNCL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCCOMNCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCCOMNCF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCCOMNCI  PIC X(32).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPOSTCL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MLPOSTCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPOSTCF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MLPOSTCI  PIC X(26).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD10L  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MTELD10L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD10F  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MTELD10I  PIC X(2).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD11L  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MTELD11L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD11F  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MTELD11I  PIC X(2).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD12L  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MTELD12L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD12F  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MTELD12I  PIC X(2).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD13L  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MTELD13L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD13F  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MTELD13I  PIC X(2).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD14L  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MTELD14L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD14F  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MTELD14I  PIC X(2).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB10L  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MTELB10L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB10F  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MTELB10I  PIC X(2).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB11L  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MTELB11L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB11F  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MTELB11I  PIC X(2).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB12L  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MTELB12L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB12F  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MTELB12I  PIC X(2).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB13L  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MTELB13L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB13F  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MTELB13I  PIC X(2).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB14L  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MTELB14L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB14F  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MTELB14I  PIC X(2).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPOSTEL   COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MPOSTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPOSTEF   PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MPOSTEI   PIC X(5).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNGSM10L  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MNGSM10L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNGSM10F  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MNGSM10I  PIC X(2).                                       00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNGSM11L  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MNGSM11L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNGSM11F  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MNGSM11I  PIC X(2).                                       00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNGSM12L  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MNGSM12L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNGSM12F  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MNGSM12I  PIC X(2).                                       00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNGSM13L  COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MNGSM13L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNGSM13F  PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MNGSM13I  PIC X(2).                                       00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNGSM14L  COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MNGSM14L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNGSM14F  PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MNGSM14I  PIC X(2).                                       00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOMLL    COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MNOMLL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNOMLF    PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MNOMLI    PIC X(5).                                       00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMLL   COMP PIC S9(4).                                 00001710
      *--                                                                       
           02 MLNOMLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLNOMLF   PIC X.                                          00001720
           02 FILLER    PIC X(4).                                       00001730
           02 MLNOMLI   PIC X(25).                                      00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPNOMLL  COMP PIC S9(4).                                 00001750
      *--                                                                       
           02 MLPNOMLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPNOMLF  PIC X.                                          00001760
           02 FILLER    PIC X(4).                                       00001770
           02 MLPNOMLI  PIC X(15).                                      00001780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVOIELL  COMP PIC S9(4).                                 00001790
      *--                                                                       
           02 MNVOIELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVOIELF  PIC X.                                          00001800
           02 FILLER    PIC X(4).                                       00001810
           02 MNVOIELI  PIC X(5).                                       00001820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTPVOIELL      COMP PIC S9(4).                            00001830
      *--                                                                       
           02 MTPVOIELL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTPVOIELF      PIC X.                                     00001840
           02 FILLER    PIC X(4).                                       00001850
           02 MTPVOIELI      PIC X(4).                                  00001860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVOIELL  COMP PIC S9(4).                                 00001870
      *--                                                                       
           02 MLVOIELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLVOIELF  PIC X.                                          00001880
           02 FILLER    PIC X(4).                                       00001890
           02 MLVOIELI  PIC X(21).                                      00001900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCBATLL   COMP PIC S9(4).                                 00001910
      *--                                                                       
           02 MCBATLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCBATLF   PIC X.                                          00001920
           02 FILLER    PIC X(4).                                       00001930
           02 MCBATLI   PIC X(3).                                       00001940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCESCLL   COMP PIC S9(4).                                 00001950
      *--                                                                       
           02 MCESCLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCESCLF   PIC X.                                          00001960
           02 FILLER    PIC X(4).                                       00001970
           02 MCESCLI   PIC X(3).                                       00001980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCETAGLL  COMP PIC S9(4).                                 00001990
      *--                                                                       
           02 MCETAGLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCETAGLF  PIC X.                                          00002000
           02 FILLER    PIC X(4).                                       00002010
           02 MCETAGLI  PIC X(3).                                       00002020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCASCLL   COMP PIC S9(4).                                 00002030
      *--                                                                       
           02 MCASCLL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCASCLF   PIC X.                                          00002040
           02 FILLER    PIC X(4).                                       00002050
           02 MCASCLI   PIC X.                                          00002060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPORTELL      COMP PIC S9(4).                            00002070
      *--                                                                       
           02 MCPORTELL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPORTELF      PIC X.                                     00002080
           02 FILLER    PIC X(4).                                       00002090
           02 MCPORTELI      PIC X(3).                                  00002100
           02 MADRESLI OCCURS   1 TIMES .                               00002110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLADDRLL     COMP PIC S9(4).                            00002120
      *--                                                                       
             03 MLADDRLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLADDRLF     PIC X.                                     00002130
             03 FILLER  PIC X(4).                                       00002140
             03 MLADDRLI     PIC X(32).                                 00002150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 DIGICODLL      COMP PIC S9(4).                            00002160
      *--                                                                       
           02 DIGICODLL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 DIGICODLF      PIC X.                                     00002170
           02 FILLER    PIC X(4).                                       00002180
           02 DIGICODLI      PIC X(6).                                  00002190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTLL  COMP PIC S9(4).                                 00002200
      *--                                                                       
           02 MCPOSTLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPOSTLF  PIC X.                                          00002210
           02 FILLER    PIC X(4).                                       00002220
           02 MCPOSTLI  PIC X(5).                                       00002230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCOMNLL  COMP PIC S9(4).                                 00002240
      *--                                                                       
           02 MCCOMNLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCCOMNLF  PIC X.                                          00002250
           02 FILLER    PIC X(4).                                       00002260
           02 MCCOMNLI  PIC X(32).                                      00002270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPOSTLL  COMP PIC S9(4).                                 00002280
      *--                                                                       
           02 MLPOSTLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLPOSTLF  PIC X.                                          00002290
           02 FILLER    PIC X(4).                                       00002300
           02 MLPOSTLI  PIC X(26).                                      00002310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD20L  COMP PIC S9(4).                                 00002320
      *--                                                                       
           02 MTELD20L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD20F  PIC X.                                          00002330
           02 FILLER    PIC X(4).                                       00002340
           02 MTELD20I  PIC X(2).                                       00002350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD21L  COMP PIC S9(4).                                 00002360
      *--                                                                       
           02 MTELD21L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD21F  PIC X.                                          00002370
           02 FILLER    PIC X(4).                                       00002380
           02 MTELD21I  PIC X(2).                                       00002390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD22L  COMP PIC S9(4).                                 00002400
      *--                                                                       
           02 MTELD22L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD22F  PIC X.                                          00002410
           02 FILLER    PIC X(4).                                       00002420
           02 MTELD22I  PIC X(2).                                       00002430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD23L  COMP PIC S9(4).                                 00002440
      *--                                                                       
           02 MTELD23L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD23F  PIC X.                                          00002450
           02 FILLER    PIC X(4).                                       00002460
           02 MTELD23I  PIC X(2).                                       00002470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELD24L  COMP PIC S9(4).                                 00002480
      *--                                                                       
           02 MTELD24L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELD24F  PIC X.                                          00002490
           02 FILLER    PIC X(4).                                       00002500
           02 MTELD24I  PIC X(2).                                       00002510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB20L  COMP PIC S9(4).                                 00002520
      *--                                                                       
           02 MTELB20L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB20F  PIC X.                                          00002530
           02 FILLER    PIC X(4).                                       00002540
           02 MTELB20I  PIC X(2).                                       00002550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB21L  COMP PIC S9(4).                                 00002560
      *--                                                                       
           02 MTELB21L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB21F  PIC X.                                          00002570
           02 FILLER    PIC X(4).                                       00002580
           02 MTELB21I  PIC X(2).                                       00002590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB22L  COMP PIC S9(4).                                 00002600
      *--                                                                       
           02 MTELB22L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB22F  PIC X.                                          00002610
           02 FILLER    PIC X(4).                                       00002620
           02 MTELB22I  PIC X(2).                                       00002630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB23L  COMP PIC S9(4).                                 00002640
      *--                                                                       
           02 MTELB23L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB23F  PIC X.                                          00002650
           02 FILLER    PIC X(4).                                       00002660
           02 MTELB23I  PIC X(2).                                       00002670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELB24L  COMP PIC S9(4).                                 00002680
      *--                                                                       
           02 MTELB24L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELB24F  PIC X.                                          00002690
           02 FILLER    PIC X(4).                                       00002700
           02 MTELB24I  PIC X(2).                                       00002710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPOSTELL  COMP PIC S9(4).                                 00002720
      *--                                                                       
           02 MPOSTELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPOSTELF  PIC X.                                          00002730
           02 FILLER    PIC X(4).                                       00002740
           02 MPOSTELI  PIC X(5).                                       00002750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNGSM20L  COMP PIC S9(4).                                 00002760
      *--                                                                       
           02 MNGSM20L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNGSM20F  PIC X.                                          00002770
           02 FILLER    PIC X(4).                                       00002780
           02 MNGSM20I  PIC X(2).                                       00002790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNGSM21L  COMP PIC S9(4).                                 00002800
      *--                                                                       
           02 MNGSM21L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNGSM21F  PIC X.                                          00002810
           02 FILLER    PIC X(4).                                       00002820
           02 MNGSM21I  PIC X(2).                                       00002830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNGSM22L  COMP PIC S9(4).                                 00002840
      *--                                                                       
           02 MNGSM22L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNGSM22F  PIC X.                                          00002850
           02 FILLER    PIC X(4).                                       00002860
           02 MNGSM22I  PIC X(2).                                       00002870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNGSM23L  COMP PIC S9(4).                                 00002880
      *--                                                                       
           02 MNGSM23L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNGSM23F  PIC X.                                          00002890
           02 FILLER    PIC X(4).                                       00002900
           02 MNGSM23I  PIC X(2).                                       00002910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNGSM24L  COMP PIC S9(4).                                 00002920
      *--                                                                       
           02 MNGSM24L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNGSM24F  PIC X.                                          00002930
           02 FILLER    PIC X(4).                                       00002940
           02 MNGSM24I  PIC X(2).                                       00002950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00002960
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00002970
           02 FILLER    PIC X(4).                                       00002980
           02 MZONCMDI  PIC X(15).                                      00002990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00003000
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00003010
           02 FILLER    PIC X(4).                                       00003020
           02 MLIBERRI  PIC X(58).                                      00003030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00003040
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00003050
           02 FILLER    PIC X(4).                                       00003060
           02 MCODTRAI  PIC X(4).                                       00003070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00003080
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00003090
           02 FILLER    PIC X(4).                                       00003100
           02 MCICSI    PIC X(5).                                       00003110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00003120
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00003130
           02 FILLER    PIC X(4).                                       00003140
           02 MNETNAMI  PIC X(8).                                       00003150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00003160
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00003170
           02 FILLER    PIC X(4).                                       00003180
           02 MSCREENI  PIC X(4).                                       00003190
      ***************************************************************** 00003200
      * SDF: EGV02   EGV02                                              00003210
      ***************************************************************** 00003220
       01   EGV02O REDEFINES EGV02I.                                    00003230
           02 FILLER    PIC X(12).                                      00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MDATJOUA  PIC X.                                          00003260
           02 MDATJOUC  PIC X.                                          00003270
           02 MDATJOUP  PIC X.                                          00003280
           02 MDATJOUH  PIC X.                                          00003290
           02 MDATJOUV  PIC X.                                          00003300
           02 MDATJOUO  PIC X(10).                                      00003310
           02 FILLER    PIC X(2).                                       00003320
           02 MTIMJOUA  PIC X.                                          00003330
           02 MTIMJOUC  PIC X.                                          00003340
           02 MTIMJOUP  PIC X.                                          00003350
           02 MTIMJOUH  PIC X.                                          00003360
           02 MTIMJOUV  PIC X.                                          00003370
           02 MTIMJOUO  PIC X(5).                                       00003380
           02 FILLER    PIC X(2).                                       00003390
           02 MWPAGEA   PIC X.                                          00003400
           02 MWPAGEC   PIC X.                                          00003410
           02 MWPAGEP   PIC X.                                          00003420
           02 MWPAGEH   PIC X.                                          00003430
           02 MWPAGEV   PIC X.                                          00003440
           02 MWPAGEO   PIC X(3).                                       00003450
           02 FILLER    PIC X(2).                                       00003460
           02 MMAGA     PIC X.                                          00003470
           02 MMAGC     PIC X.                                          00003480
           02 MMAGP     PIC X.                                          00003490
           02 MMAGH     PIC X.                                          00003500
           02 MMAGV     PIC X.                                          00003510
           02 MMAGO     PIC X(3).                                       00003520
           02 FILLER    PIC X(2).                                       00003530
           02 MNVENTEA  PIC X.                                          00003540
           02 MNVENTEC  PIC X.                                          00003550
           02 MNVENTEP  PIC X.                                          00003560
           02 MNVENTEH  PIC X.                                          00003570
           02 MNVENTEV  PIC X.                                          00003580
           02 MNVENTEO  PIC X(7).                                       00003590
           02 FILLER    PIC X(2).                                       00003600
           02 MCLIENTA  PIC X.                                          00003610
           02 MCLIENTC  PIC X.                                          00003620
           02 MCLIENTP  PIC X.                                          00003630
           02 MCLIENTH  PIC X.                                          00003640
           02 MCLIENTV  PIC X.                                          00003650
           02 MCLIENTO  PIC X(9).                                       00003660
           02 FILLER    PIC X(2).                                       00003670
           02 MDVENTEA  PIC X.                                          00003680
           02 MDVENTEC  PIC X.                                          00003690
           02 MDVENTEP  PIC X.                                          00003700
           02 MDVENTEH  PIC X.                                          00003710
           02 MDVENTEV  PIC X.                                          00003720
           02 MDVENTEO  PIC X(8).                                       00003730
           02 FILLER    PIC X(2).                                       00003740
           02 MNORDREA  PIC X.                                          00003750
           02 MNORDREC  PIC X.                                          00003760
           02 MNORDREP  PIC X.                                          00003770
           02 MNORDREH  PIC X.                                          00003780
           02 MNORDREV  PIC X.                                          00003790
           02 MNORDREO  PIC X(5).                                       00003800
           02 FILLER    PIC X(2).                                       00003810
           02 MNOMCA    PIC X.                                          00003820
           02 MNOMCC    PIC X.                                          00003830
           02 MNOMCP    PIC X.                                          00003840
           02 MNOMCH    PIC X.                                          00003850
           02 MNOMCV    PIC X.                                          00003860
           02 MNOMCO    PIC X(5).                                       00003870
           02 FILLER    PIC X(2).                                       00003880
           02 MLNOMCA   PIC X.                                          00003890
           02 MLNOMCC   PIC X.                                          00003900
           02 MLNOMCP   PIC X.                                          00003910
           02 MLNOMCH   PIC X.                                          00003920
           02 MLNOMCV   PIC X.                                          00003930
           02 MLNOMCO   PIC X(25).                                      00003940
           02 FILLER    PIC X(2).                                       00003950
           02 MLPNOMCA  PIC X.                                          00003960
           02 MLPNOMCC  PIC X.                                          00003970
           02 MLPNOMCP  PIC X.                                          00003980
           02 MLPNOMCH  PIC X.                                          00003990
           02 MLPNOMCV  PIC X.                                          00004000
           02 MLPNOMCO  PIC X(15).                                      00004010
           02 FILLER    PIC X(2).                                       00004020
           02 MNVOIECA  PIC X.                                          00004030
           02 MNVOIECC  PIC X.                                          00004040
           02 MNVOIECP  PIC X.                                          00004050
           02 MNVOIECH  PIC X.                                          00004060
           02 MNVOIECV  PIC X.                                          00004070
           02 MNVOIECO  PIC X(5).                                       00004080
           02 FILLER    PIC X(2).                                       00004090
           02 MTPVOIECA      PIC X.                                     00004100
           02 MTPVOIECC PIC X.                                          00004110
           02 MTPVOIECP PIC X.                                          00004120
           02 MTPVOIECH PIC X.                                          00004130
           02 MTPVOIECV PIC X.                                          00004140
           02 MTPVOIECO      PIC X(4).                                  00004150
           02 FILLER    PIC X(2).                                       00004160
           02 MLVOIECA  PIC X.                                          00004170
           02 MLVOIECC  PIC X.                                          00004180
           02 MLVOIECP  PIC X.                                          00004190
           02 MLVOIECH  PIC X.                                          00004200
           02 MLVOIECV  PIC X.                                          00004210
           02 MLVOIECO  PIC X(21).                                      00004220
           02 FILLER    PIC X(2).                                       00004230
           02 MCBATCA   PIC X.                                          00004240
           02 MCBATCC   PIC X.                                          00004250
           02 MCBATCP   PIC X.                                          00004260
           02 MCBATCH   PIC X.                                          00004270
           02 MCBATCV   PIC X.                                          00004280
           02 MCBATCO   PIC X(3).                                       00004290
           02 FILLER    PIC X(2).                                       00004300
           02 MCESCCA   PIC X.                                          00004310
           02 MCESCCC   PIC X.                                          00004320
           02 MCESCCP   PIC X.                                          00004330
           02 MCESCCH   PIC X.                                          00004340
           02 MCESCCV   PIC X.                                          00004350
           02 MCESCCO   PIC X(3).                                       00004360
           02 FILLER    PIC X(2).                                       00004370
           02 MCETAGCA  PIC X.                                          00004380
           02 MCETAGCC  PIC X.                                          00004390
           02 MCETAGCP  PIC X.                                          00004400
           02 MCETAGCH  PIC X.                                          00004410
           02 MCETAGCV  PIC X.                                          00004420
           02 MCETAGCO  PIC X(3).                                       00004430
           02 FILLER    PIC X(2).                                       00004440
           02 MCASCCA   PIC X.                                          00004450
           02 MCASCCC   PIC X.                                          00004460
           02 MCASCCP   PIC X.                                          00004470
           02 MCASCCH   PIC X.                                          00004480
           02 MCASCCV   PIC X.                                          00004490
           02 MCASCCO   PIC X.                                          00004500
           02 FILLER    PIC X(2).                                       00004510
           02 MCPORTECA      PIC X.                                     00004520
           02 MCPORTECC PIC X.                                          00004530
           02 MCPORTECP PIC X.                                          00004540
           02 MCPORTECH PIC X.                                          00004550
           02 MCPORTECV PIC X.                                          00004560
           02 MCPORTECO      PIC X(3).                                  00004570
           02 MADRESCO OCCURS   1 TIMES .                               00004580
             03 FILLER       PIC X(2).                                  00004590
             03 MLADDRCA     PIC X.                                     00004600
             03 MLADDRCC     PIC X.                                     00004610
             03 MLADDRCP     PIC X.                                     00004620
             03 MLADDRCH     PIC X.                                     00004630
             03 MLADDRCV     PIC X.                                     00004640
             03 MLADDRCO     PIC X(32).                                 00004650
           02 FILLER    PIC X(2).                                       00004660
           02 DIGICODCA      PIC X.                                     00004670
           02 DIGICODCC PIC X.                                          00004680
           02 DIGICODCP PIC X.                                          00004690
           02 DIGICODCH PIC X.                                          00004700
           02 DIGICODCV PIC X.                                          00004710
           02 DIGICODCO      PIC X(6).                                  00004720
           02 FILLER    PIC X(2).                                       00004730
           02 MCPOSTCA  PIC X.                                          00004740
           02 MCPOSTCC  PIC X.                                          00004750
           02 MCPOSTCP  PIC X.                                          00004760
           02 MCPOSTCH  PIC X.                                          00004770
           02 MCPOSTCV  PIC X.                                          00004780
           02 MCPOSTCO  PIC X(5).                                       00004790
           02 FILLER    PIC X(2).                                       00004800
           02 MCCOMNCA  PIC X.                                          00004810
           02 MCCOMNCC  PIC X.                                          00004820
           02 MCCOMNCP  PIC X.                                          00004830
           02 MCCOMNCH  PIC X.                                          00004840
           02 MCCOMNCV  PIC X.                                          00004850
           02 MCCOMNCO  PIC X(32).                                      00004860
           02 FILLER    PIC X(2).                                       00004870
           02 MLPOSTCA  PIC X.                                          00004880
           02 MLPOSTCC  PIC X.                                          00004890
           02 MLPOSTCP  PIC X.                                          00004900
           02 MLPOSTCH  PIC X.                                          00004910
           02 MLPOSTCV  PIC X.                                          00004920
           02 MLPOSTCO  PIC X(26).                                      00004930
           02 FILLER    PIC X(2).                                       00004940
           02 MTELD10A  PIC X.                                          00004950
           02 MTELD10C  PIC X.                                          00004960
           02 MTELD10P  PIC X.                                          00004970
           02 MTELD10H  PIC X.                                          00004980
           02 MTELD10V  PIC X.                                          00004990
           02 MTELD10O  PIC X(2).                                       00005000
           02 FILLER    PIC X(2).                                       00005010
           02 MTELD11A  PIC X.                                          00005020
           02 MTELD11C  PIC X.                                          00005030
           02 MTELD11P  PIC X.                                          00005040
           02 MTELD11H  PIC X.                                          00005050
           02 MTELD11V  PIC X.                                          00005060
           02 MTELD11O  PIC X(2).                                       00005070
           02 FILLER    PIC X(2).                                       00005080
           02 MTELD12A  PIC X.                                          00005090
           02 MTELD12C  PIC X.                                          00005100
           02 MTELD12P  PIC X.                                          00005110
           02 MTELD12H  PIC X.                                          00005120
           02 MTELD12V  PIC X.                                          00005130
           02 MTELD12O  PIC X(2).                                       00005140
           02 FILLER    PIC X(2).                                       00005150
           02 MTELD13A  PIC X.                                          00005160
           02 MTELD13C  PIC X.                                          00005170
           02 MTELD13P  PIC X.                                          00005180
           02 MTELD13H  PIC X.                                          00005190
           02 MTELD13V  PIC X.                                          00005200
           02 MTELD13O  PIC X(2).                                       00005210
           02 FILLER    PIC X(2).                                       00005220
           02 MTELD14A  PIC X.                                          00005230
           02 MTELD14C  PIC X.                                          00005240
           02 MTELD14P  PIC X.                                          00005250
           02 MTELD14H  PIC X.                                          00005260
           02 MTELD14V  PIC X.                                          00005270
           02 MTELD14O  PIC X(2).                                       00005280
           02 FILLER    PIC X(2).                                       00005290
           02 MTELB10A  PIC X.                                          00005300
           02 MTELB10C  PIC X.                                          00005310
           02 MTELB10P  PIC X.                                          00005320
           02 MTELB10H  PIC X.                                          00005330
           02 MTELB10V  PIC X.                                          00005340
           02 MTELB10O  PIC X(2).                                       00005350
           02 FILLER    PIC X(2).                                       00005360
           02 MTELB11A  PIC X.                                          00005370
           02 MTELB11C  PIC X.                                          00005380
           02 MTELB11P  PIC X.                                          00005390
           02 MTELB11H  PIC X.                                          00005400
           02 MTELB11V  PIC X.                                          00005410
           02 MTELB11O  PIC X(2).                                       00005420
           02 FILLER    PIC X(2).                                       00005430
           02 MTELB12A  PIC X.                                          00005440
           02 MTELB12C  PIC X.                                          00005450
           02 MTELB12P  PIC X.                                          00005460
           02 MTELB12H  PIC X.                                          00005470
           02 MTELB12V  PIC X.                                          00005480
           02 MTELB12O  PIC X(2).                                       00005490
           02 FILLER    PIC X(2).                                       00005500
           02 MTELB13A  PIC X.                                          00005510
           02 MTELB13C  PIC X.                                          00005520
           02 MTELB13P  PIC X.                                          00005530
           02 MTELB13H  PIC X.                                          00005540
           02 MTELB13V  PIC X.                                          00005550
           02 MTELB13O  PIC X(2).                                       00005560
           02 FILLER    PIC X(2).                                       00005570
           02 MTELB14A  PIC X.                                          00005580
           02 MTELB14C  PIC X.                                          00005590
           02 MTELB14P  PIC X.                                          00005600
           02 MTELB14H  PIC X.                                          00005610
           02 MTELB14V  PIC X.                                          00005620
           02 MTELB14O  PIC X(2).                                       00005630
           02 FILLER    PIC X(2).                                       00005640
           02 MPOSTEA   PIC X.                                          00005650
           02 MPOSTEC   PIC X.                                          00005660
           02 MPOSTEP   PIC X.                                          00005670
           02 MPOSTEH   PIC X.                                          00005680
           02 MPOSTEV   PIC X.                                          00005690
           02 MPOSTEO   PIC X(5).                                       00005700
           02 FILLER    PIC X(2).                                       00005710
           02 MNGSM10A  PIC X.                                          00005720
           02 MNGSM10C  PIC X.                                          00005730
           02 MNGSM10P  PIC X.                                          00005740
           02 MNGSM10H  PIC X.                                          00005750
           02 MNGSM10V  PIC X.                                          00005760
           02 MNGSM10O  PIC X(2).                                       00005770
           02 FILLER    PIC X(2).                                       00005780
           02 MNGSM11A  PIC X.                                          00005790
           02 MNGSM11C  PIC X.                                          00005800
           02 MNGSM11P  PIC X.                                          00005810
           02 MNGSM11H  PIC X.                                          00005820
           02 MNGSM11V  PIC X.                                          00005830
           02 MNGSM11O  PIC X(2).                                       00005840
           02 FILLER    PIC X(2).                                       00005850
           02 MNGSM12A  PIC X.                                          00005860
           02 MNGSM12C  PIC X.                                          00005870
           02 MNGSM12P  PIC X.                                          00005880
           02 MNGSM12H  PIC X.                                          00005890
           02 MNGSM12V  PIC X.                                          00005900
           02 MNGSM12O  PIC X(2).                                       00005910
           02 FILLER    PIC X(2).                                       00005920
           02 MNGSM13A  PIC X.                                          00005930
           02 MNGSM13C  PIC X.                                          00005940
           02 MNGSM13P  PIC X.                                          00005950
           02 MNGSM13H  PIC X.                                          00005960
           02 MNGSM13V  PIC X.                                          00005970
           02 MNGSM13O  PIC X(2).                                       00005980
           02 FILLER    PIC X(2).                                       00005990
           02 MNGSM14A  PIC X.                                          00006000
           02 MNGSM14C  PIC X.                                          00006010
           02 MNGSM14P  PIC X.                                          00006020
           02 MNGSM14H  PIC X.                                          00006030
           02 MNGSM14V  PIC X.                                          00006040
           02 MNGSM14O  PIC X(2).                                       00006050
           02 FILLER    PIC X(2).                                       00006060
           02 MNOMLA    PIC X.                                          00006070
           02 MNOMLC    PIC X.                                          00006080
           02 MNOMLP    PIC X.                                          00006090
           02 MNOMLH    PIC X.                                          00006100
           02 MNOMLV    PIC X.                                          00006110
           02 MNOMLO    PIC X(5).                                       00006120
           02 FILLER    PIC X(2).                                       00006130
           02 MLNOMLA   PIC X.                                          00006140
           02 MLNOMLC   PIC X.                                          00006150
           02 MLNOMLP   PIC X.                                          00006160
           02 MLNOMLH   PIC X.                                          00006170
           02 MLNOMLV   PIC X.                                          00006180
           02 MLNOMLO   PIC X(25).                                      00006190
           02 FILLER    PIC X(2).                                       00006200
           02 MLPNOMLA  PIC X.                                          00006210
           02 MLPNOMLC  PIC X.                                          00006220
           02 MLPNOMLP  PIC X.                                          00006230
           02 MLPNOMLH  PIC X.                                          00006240
           02 MLPNOMLV  PIC X.                                          00006250
           02 MLPNOMLO  PIC X(15).                                      00006260
           02 FILLER    PIC X(2).                                       00006270
           02 MNVOIELA  PIC X.                                          00006280
           02 MNVOIELC  PIC X.                                          00006290
           02 MNVOIELP  PIC X.                                          00006300
           02 MNVOIELH  PIC X.                                          00006310
           02 MNVOIELV  PIC X.                                          00006320
           02 MNVOIELO  PIC X(5).                                       00006330
           02 FILLER    PIC X(2).                                       00006340
           02 MTPVOIELA      PIC X.                                     00006350
           02 MTPVOIELC PIC X.                                          00006360
           02 MTPVOIELP PIC X.                                          00006370
           02 MTPVOIELH PIC X.                                          00006380
           02 MTPVOIELV PIC X.                                          00006390
           02 MTPVOIELO      PIC X(4).                                  00006400
           02 FILLER    PIC X(2).                                       00006410
           02 MLVOIELA  PIC X.                                          00006420
           02 MLVOIELC  PIC X.                                          00006430
           02 MLVOIELP  PIC X.                                          00006440
           02 MLVOIELH  PIC X.                                          00006450
           02 MLVOIELV  PIC X.                                          00006460
           02 MLVOIELO  PIC X(21).                                      00006470
           02 FILLER    PIC X(2).                                       00006480
           02 MCBATLA   PIC X.                                          00006490
           02 MCBATLC   PIC X.                                          00006500
           02 MCBATLP   PIC X.                                          00006510
           02 MCBATLH   PIC X.                                          00006520
           02 MCBATLV   PIC X.                                          00006530
           02 MCBATLO   PIC X(3).                                       00006540
           02 FILLER    PIC X(2).                                       00006550
           02 MCESCLA   PIC X.                                          00006560
           02 MCESCLC   PIC X.                                          00006570
           02 MCESCLP   PIC X.                                          00006580
           02 MCESCLH   PIC X.                                          00006590
           02 MCESCLV   PIC X.                                          00006600
           02 MCESCLO   PIC X(3).                                       00006610
           02 FILLER    PIC X(2).                                       00006620
           02 MCETAGLA  PIC X.                                          00006630
           02 MCETAGLC  PIC X.                                          00006640
           02 MCETAGLP  PIC X.                                          00006650
           02 MCETAGLH  PIC X.                                          00006660
           02 MCETAGLV  PIC X.                                          00006670
           02 MCETAGLO  PIC X(3).                                       00006680
           02 FILLER    PIC X(2).                                       00006690
           02 MCASCLA   PIC X.                                          00006700
           02 MCASCLC   PIC X.                                          00006710
           02 MCASCLP   PIC X.                                          00006720
           02 MCASCLH   PIC X.                                          00006730
           02 MCASCLV   PIC X.                                          00006740
           02 MCASCLO   PIC X.                                          00006750
           02 FILLER    PIC X(2).                                       00006760
           02 MCPORTELA      PIC X.                                     00006770
           02 MCPORTELC PIC X.                                          00006780
           02 MCPORTELP PIC X.                                          00006790
           02 MCPORTELH PIC X.                                          00006800
           02 MCPORTELV PIC X.                                          00006810
           02 MCPORTELO      PIC X(3).                                  00006820
           02 MADRESLO OCCURS   1 TIMES .                               00006830
             03 FILLER       PIC X(2).                                  00006840
             03 MLADDRLA     PIC X.                                     00006850
             03 MLADDRLC     PIC X.                                     00006860
             03 MLADDRLP     PIC X.                                     00006870
             03 MLADDRLH     PIC X.                                     00006880
             03 MLADDRLV     PIC X.                                     00006890
             03 MLADDRLO     PIC X(32).                                 00006900
           02 FILLER    PIC X(2).                                       00006910
           02 DIGICODLA      PIC X.                                     00006920
           02 DIGICODLC PIC X.                                          00006930
           02 DIGICODLP PIC X.                                          00006940
           02 DIGICODLH PIC X.                                          00006950
           02 DIGICODLV PIC X.                                          00006960
           02 DIGICODLO      PIC X(6).                                  00006970
           02 FILLER    PIC X(2).                                       00006980
           02 MCPOSTLA  PIC X.                                          00006990
           02 MCPOSTLC  PIC X.                                          00007000
           02 MCPOSTLP  PIC X.                                          00007010
           02 MCPOSTLH  PIC X.                                          00007020
           02 MCPOSTLV  PIC X.                                          00007030
           02 MCPOSTLO  PIC X(5).                                       00007040
           02 FILLER    PIC X(2).                                       00007050
           02 MCCOMNLA  PIC X.                                          00007060
           02 MCCOMNLC  PIC X.                                          00007070
           02 MCCOMNLP  PIC X.                                          00007080
           02 MCCOMNLH  PIC X.                                          00007090
           02 MCCOMNLV  PIC X.                                          00007100
           02 MCCOMNLO  PIC X(32).                                      00007110
           02 FILLER    PIC X(2).                                       00007120
           02 MLPOSTLA  PIC X.                                          00007130
           02 MLPOSTLC  PIC X.                                          00007140
           02 MLPOSTLP  PIC X.                                          00007150
           02 MLPOSTLH  PIC X.                                          00007160
           02 MLPOSTLV  PIC X.                                          00007170
           02 MLPOSTLO  PIC X(26).                                      00007180
           02 FILLER    PIC X(2).                                       00007190
           02 MTELD20A  PIC X.                                          00007200
           02 MTELD20C  PIC X.                                          00007210
           02 MTELD20P  PIC X.                                          00007220
           02 MTELD20H  PIC X.                                          00007230
           02 MTELD20V  PIC X.                                          00007240
           02 MTELD20O  PIC X(2).                                       00007250
           02 FILLER    PIC X(2).                                       00007260
           02 MTELD21A  PIC X.                                          00007270
           02 MTELD21C  PIC X.                                          00007280
           02 MTELD21P  PIC X.                                          00007290
           02 MTELD21H  PIC X.                                          00007300
           02 MTELD21V  PIC X.                                          00007310
           02 MTELD21O  PIC X(2).                                       00007320
           02 FILLER    PIC X(2).                                       00007330
           02 MTELD22A  PIC X.                                          00007340
           02 MTELD22C  PIC X.                                          00007350
           02 MTELD22P  PIC X.                                          00007360
           02 MTELD22H  PIC X.                                          00007370
           02 MTELD22V  PIC X.                                          00007380
           02 MTELD22O  PIC X(2).                                       00007390
           02 FILLER    PIC X(2).                                       00007400
           02 MTELD23A  PIC X.                                          00007410
           02 MTELD23C  PIC X.                                          00007420
           02 MTELD23P  PIC X.                                          00007430
           02 MTELD23H  PIC X.                                          00007440
           02 MTELD23V  PIC X.                                          00007450
           02 MTELD23O  PIC X(2).                                       00007460
           02 FILLER    PIC X(2).                                       00007470
           02 MTELD24A  PIC X.                                          00007480
           02 MTELD24C  PIC X.                                          00007490
           02 MTELD24P  PIC X.                                          00007500
           02 MTELD24H  PIC X.                                          00007510
           02 MTELD24V  PIC X.                                          00007520
           02 MTELD24O  PIC X(2).                                       00007530
           02 FILLER    PIC X(2).                                       00007540
           02 MTELB20A  PIC X.                                          00007550
           02 MTELB20C  PIC X.                                          00007560
           02 MTELB20P  PIC X.                                          00007570
           02 MTELB20H  PIC X.                                          00007580
           02 MTELB20V  PIC X.                                          00007590
           02 MTELB20O  PIC X(2).                                       00007600
           02 FILLER    PIC X(2).                                       00007610
           02 MTELB21A  PIC X.                                          00007620
           02 MTELB21C  PIC X.                                          00007630
           02 MTELB21P  PIC X.                                          00007640
           02 MTELB21H  PIC X.                                          00007650
           02 MTELB21V  PIC X.                                          00007660
           02 MTELB21O  PIC X(2).                                       00007670
           02 FILLER    PIC X(2).                                       00007680
           02 MTELB22A  PIC X.                                          00007690
           02 MTELB22C  PIC X.                                          00007700
           02 MTELB22P  PIC X.                                          00007710
           02 MTELB22H  PIC X.                                          00007720
           02 MTELB22V  PIC X.                                          00007730
           02 MTELB22O  PIC X(2).                                       00007740
           02 FILLER    PIC X(2).                                       00007750
           02 MTELB23A  PIC X.                                          00007760
           02 MTELB23C  PIC X.                                          00007770
           02 MTELB23P  PIC X.                                          00007780
           02 MTELB23H  PIC X.                                          00007790
           02 MTELB23V  PIC X.                                          00007800
           02 MTELB23O  PIC X(2).                                       00007810
           02 FILLER    PIC X(2).                                       00007820
           02 MTELB24A  PIC X.                                          00007830
           02 MTELB24C  PIC X.                                          00007840
           02 MTELB24P  PIC X.                                          00007850
           02 MTELB24H  PIC X.                                          00007860
           02 MTELB24V  PIC X.                                          00007870
           02 MTELB24O  PIC X(2).                                       00007880
           02 FILLER    PIC X(2).                                       00007890
           02 MPOSTELA  PIC X.                                          00007900
           02 MPOSTELC  PIC X.                                          00007910
           02 MPOSTELP  PIC X.                                          00007920
           02 MPOSTELH  PIC X.                                          00007930
           02 MPOSTELV  PIC X.                                          00007940
           02 MPOSTELO  PIC X(5).                                       00007950
           02 FILLER    PIC X(2).                                       00007960
           02 MNGSM20A  PIC X.                                          00007970
           02 MNGSM20C  PIC X.                                          00007980
           02 MNGSM20P  PIC X.                                          00007990
           02 MNGSM20H  PIC X.                                          00008000
           02 MNGSM20V  PIC X.                                          00008010
           02 MNGSM20O  PIC X(2).                                       00008020
           02 FILLER    PIC X(2).                                       00008030
           02 MNGSM21A  PIC X.                                          00008040
           02 MNGSM21C  PIC X.                                          00008050
           02 MNGSM21P  PIC X.                                          00008060
           02 MNGSM21H  PIC X.                                          00008070
           02 MNGSM21V  PIC X.                                          00008080
           02 MNGSM21O  PIC X(2).                                       00008090
           02 FILLER    PIC X(2).                                       00008100
           02 MNGSM22A  PIC X.                                          00008110
           02 MNGSM22C  PIC X.                                          00008120
           02 MNGSM22P  PIC X.                                          00008130
           02 MNGSM22H  PIC X.                                          00008140
           02 MNGSM22V  PIC X.                                          00008150
           02 MNGSM22O  PIC X(2).                                       00008160
           02 FILLER    PIC X(2).                                       00008170
           02 MNGSM23A  PIC X.                                          00008180
           02 MNGSM23C  PIC X.                                          00008190
           02 MNGSM23P  PIC X.                                          00008200
           02 MNGSM23H  PIC X.                                          00008210
           02 MNGSM23V  PIC X.                                          00008220
           02 MNGSM23O  PIC X(2).                                       00008230
           02 FILLER    PIC X(2).                                       00008240
           02 MNGSM24A  PIC X.                                          00008250
           02 MNGSM24C  PIC X.                                          00008260
           02 MNGSM24P  PIC X.                                          00008270
           02 MNGSM24H  PIC X.                                          00008280
           02 MNGSM24V  PIC X.                                          00008290
           02 MNGSM24O  PIC X(2).                                       00008300
           02 FILLER    PIC X(2).                                       00008310
           02 MZONCMDA  PIC X.                                          00008320
           02 MZONCMDC  PIC X.                                          00008330
           02 MZONCMDP  PIC X.                                          00008340
           02 MZONCMDH  PIC X.                                          00008350
           02 MZONCMDV  PIC X.                                          00008360
           02 MZONCMDO  PIC X(15).                                      00008370
           02 FILLER    PIC X(2).                                       00008380
           02 MLIBERRA  PIC X.                                          00008390
           02 MLIBERRC  PIC X.                                          00008400
           02 MLIBERRP  PIC X.                                          00008410
           02 MLIBERRH  PIC X.                                          00008420
           02 MLIBERRV  PIC X.                                          00008430
           02 MLIBERRO  PIC X(58).                                      00008440
           02 FILLER    PIC X(2).                                       00008450
           02 MCODTRAA  PIC X.                                          00008460
           02 MCODTRAC  PIC X.                                          00008470
           02 MCODTRAP  PIC X.                                          00008480
           02 MCODTRAH  PIC X.                                          00008490
           02 MCODTRAV  PIC X.                                          00008500
           02 MCODTRAO  PIC X(4).                                       00008510
           02 FILLER    PIC X(2).                                       00008520
           02 MCICSA    PIC X.                                          00008530
           02 MCICSC    PIC X.                                          00008540
           02 MCICSP    PIC X.                                          00008550
           02 MCICSH    PIC X.                                          00008560
           02 MCICSV    PIC X.                                          00008570
           02 MCICSO    PIC X(5).                                       00008580
           02 FILLER    PIC X(2).                                       00008590
           02 MNETNAMA  PIC X.                                          00008600
           02 MNETNAMC  PIC X.                                          00008610
           02 MNETNAMP  PIC X.                                          00008620
           02 MNETNAMH  PIC X.                                          00008630
           02 MNETNAMV  PIC X.                                          00008640
           02 MNETNAMO  PIC X(8).                                       00008650
           02 FILLER    PIC X(2).                                       00008660
           02 MSCREENA  PIC X.                                          00008670
           02 MSCREENC  PIC X.                                          00008680
           02 MSCREENP  PIC X.                                          00008690
           02 MSCREENH  PIC X.                                          00008700
           02 MSCREENV  PIC X.                                          00008710
           02 MSCREENO  PIC X(4).                                       00008720
                                                                                
