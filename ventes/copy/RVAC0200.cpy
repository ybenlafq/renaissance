      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVAC0200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVAC0200                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVAC0200.                                                            
           02  AC02-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  AC02-NLIEU                                                       
               PIC X(0003).                                                     
           02  AC02-DVENTE                                                      
               PIC X(0008).                                                     
           02  AC02-HVENTE                                                      
               PIC X(0006).                                                     
           02  AC02-QTLM                                                        
               PIC S9(4) COMP-3.                                                
           02  AC02-QELA                                                        
               PIC S9(4) COMP-3.                                                
           02  AC02-QDACEM                                                      
               PIC S9(4) COMP-3.                                                
           02  AC02-CATLM                                                       
               PIC S9(7)V99 COMP-3.                                             
           02  AC02-CAELA                                                       
               PIC S9(7)V99 COMP-3.                                             
           02  AC02-CADACEM                                                     
               PIC S9(7)V99 COMP-3.                                             
           02  AC02-QTRDARTY                                                    
               PIC S9(4) COMP-3.                                                
           02  AC02-QENTRE                                                      
               PIC S9(5) COMP-3.                                                
           02  AC02-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVAC0200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVAC0200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC02-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC02-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC02-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC02-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC02-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC02-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC02-HVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC02-HVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC02-QTLM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC02-QTLM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC02-QELA-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC02-QELA-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC02-QDACEM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC02-QDACEM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC02-CATLM-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC02-CATLM-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC02-CAELA-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC02-CAELA-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC02-CADACEM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC02-CADACEM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC02-QTRDARTY-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC02-QTRDARTY-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC02-QENTRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC02-QENTRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC02-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC02-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
