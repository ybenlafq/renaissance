      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRD0001                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRD0001                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRD0001.                                                            
           02  RD00-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  RD00-NLIEU                                                       
               PIC X(0003).                                                     
           02  RD00-NVENTE                                                      
               PIC X(0007).                                                     
           02  RD00-NCODIC                                                      
               PIC X(0007).                                                     
           02  RD00-DDECLARATION                                                
               PIC X(0008).                                                     
           02  RD00-NDECLARATION                                                
               PIC X(0013).                                                     
           02  RD00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  RD00-DANNULATION                                                 
               PIC X(0008).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRD0001                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRD0001-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RD00-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RD00-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RD00-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RD00-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RD00-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RD00-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RD00-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RD00-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RD00-DDECLARATION-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RD00-DDECLARATION-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RD00-NDECLARATION-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RD00-NDECLARATION-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RD00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RD00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RD00-DANNULATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RD00-DANNULATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
