      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  FSV030-RECORD.                                                       
           02 FSV030-NSOCIETE             PIC X(03).                            
           02 FSV030-NLIEU                PIC X(03).                            
           02 FSV030-NVENTE               PIC X(07).                            
           02 FSV030-NSEQNQ               PIC  9(5).                            
           02 FSV030-TYPMAJ               PIC X.                                
           02 FSV030-DATA-01.                                                   
      * LIGNES ARTICLES                                                         
              05 FSV030-NCLIENTADR        PIC X(08).                            
              05 FSV030-NCLIENTLIE        PIC X(08).                            
              05 FSV030-CTITRENOM         PIC X(05).                            
              05 FSV030-LNOM              PIC X(25).                            
              05 FSV030-LPRENOM           PIC X(15).                            
              05 FSV030-CPOSTAL           PIC X(05).                            
              05 FSV030-TBIEN             PIC X(01).                            
              05 FSV030-TPRESTA           PIC X(01).                            
              05 FSV030-NCODIC            PIC X(07).                            
              05 FSV030-NCODICGRP         PIC X(07).                            
PM    *       NUMERO DE SERIE                                                   
ECE           05 FSV030-NSERIE            PIC X(50).                            
              05 FSV030-NLIGNE            PIC X(03).                            
              05 FSV030-DVENTE            PIC X(08).                            
              05 FSV030-DANNULATION       PIC X(08).                            
              05 FSV030-CVENDEUR          PIC X(06).                            
              05 FSV030-LVENDEUR          PIC X(20).                            
              05 FSV030-QVENDUE           PIC ----9.                            
              05 FSV030-CMODDEL           PIC X(05).                            
              05 FSV030-WCQERESF          PIC X(01).                            
              05 FSV030-DCQERESF          PIC X(08).                            
              05 FSV030-NMUTATION         PIC X(08).                            
              05 FSV030-DMUTATION         PIC X(08).                            
              05 FSV030-NSOCPHY           PIC X(03).                            
              05 FSV030-NLIEUPHY          PIC X(03).                            
              05 FSV030-DDELIV            PIC X(08).                            
              05 FSV030-CPLAGE            PIC X(02).                            
              05 FSV030-CTOPE             PIC X(01).                            
              05 FSV030-CGAR              PIC X(05).                            
              05 FSV030-DGAR              PIC X(08).                            
              05 FSV030-CPSE              PIC X(05).                            
      *  HISTORIQUEMENT:NPSE MAIS DEPUIS D'AUTRES TYPES DE CONTRAT EXIST        
              05 FSV030-NCONTRAT          PIC X(08).                            
              05 FSV030-DPSE              PIC X(08).                            
              05 FSV030-LTCOMMUT          PIC X(20).                            
              05 FSV030-LRCOMMUT          PIC X(20).                            
              05 FSV030-NSOCDEL           PIC X(03).                            
              05 FSV030-NLIEUDEL          PIC X(03).                            
              05 FSV030-PVTOTAL           PIC -------9V99.                      
              05 FSV030-MTREMISE          PIC --------V--.                      
              05 FSV030-CDEV              PIC X(03).                            
              05 FSV030-TAUXTVA           PIC  9(03)V99.                        
              05 FSV030-COMMENT           PIC X(092).                           
              05 FSV030-CPTMQ             PIC 99.                               
              05 FSV030-PVCONTRAT         PIC -------9V99.                      
              05 FSV030-NSEQREF           PIC  9(5).                            
              05 FSV030-ICONTRAT.                                               
                 10 FSV030-FCONTRAT          PIC X(1).                          
                 10 FSV030-TCONTRAT          PIC X(1).                          
                 10 FSV030-SCONTRAT          PIC X(2).                          
                 10 FSV030-PCONTRAT          PIC X(6).                          
                 10 FSV030-P-CCTRL OCCURS 3.                                    
                    15 FSV030-CCTRL             PIC X(5).                       
                    15 FSV030-VALCTRLT          PIC X(50).                      
      * INFOS ERREUR                                                            
           02 FSV030-ERREUR.                                                    
             05 FSV030-CODRET            PIC X(01).                             
                88  FSV030-CODRET-OK        VALUE ' '.                          
                88  FSV030-CODRET-ERREUR    VALUE '1'.                          
             05 FSV030-LIBERR.                                                  
               10 FSV030-NCREDI     PIC X(14).                                  
               10 FSV030-LIBERR1    PIC X(46).                                  
           02 FSV030-COMPLEMENT.                                                
             05 FSV030-NCONTRATRB PIC X(20).                                    
             05 FSV030-TIMESTAMP  PIC X(26).                                    
             05 FSV030-NCARTE     PIC X(09).                                    
             05 FSV030-NREDBOX    PIC X(10).                                    
ECE          05 FSV030-NRT        PIC X(50).                                    
ECE          05 FSV030-NSNS       PIC X(50).                                    
             05 FSV030-NCLIENTF   PIC X(08).                                    
             05 FSV030-REF-EXP    PIC X(26).                                    
             05 FSV030-TYPE-DELIV PIC X(01).                                    
             05 FSV030-STAT-DELIV PIC X(01).                                    
             05 FSV030-NUM-CMD-DC PIC X(30).                                    
             05 FSV030-TP-CMD     PIC X(01).                                    
             05 FSV030-CDE-STAT   PIC X(01).                                    
             05 FSV030-NSEQNQIV   PIC X(11).                                    
             05 FSV030-IDCONTRAT  PIC X(40).                                    
             05 FSV030-ECHANGESAV PIC X(01).                                    
             05 FSV030-ADRMAIL    PIC X(38).                                    
      *---   ATTENTION ON A ATTEINT LA LONGUEUR MAX DE 1024                     
      *---   AUCUNE DONNEE NE PEUT ETRE AJOUTER                                 
                                                                                
