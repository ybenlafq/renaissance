      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVEC2101                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEC2101.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEC2101.                                                            
      *}                                                                        
           10 EC21-NCODIC          PIC X(7).                                    
           10 EC21-SMAG            PIC S9(5)V USAGE COMP-3.                     
           10 EC21-SART            PIC S9(5)V USAGE COMP-3.                     
           10 EC21-DSYST           PIC S9(13)V USAGE COMP-3.                    
           10 EC21-SMIN            PIC S9(3)V USAGE COMP-3.                     
           10 EC21-SDEPOT          PIC S9(5)V USAGE COMP-3.                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 6       *        
      ******************************************************************        
                                                                                
