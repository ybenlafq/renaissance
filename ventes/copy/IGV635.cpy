      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGV635 AU 11/10/2000  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,08,BI,A,                          *        
      *                           18,07,BI,A,                          *        
      *                           25,07,BI,A,                          *        
      *                           32,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGV635.                                                        
            05 NOMETAT-IGV635           PIC X(6) VALUE 'IGV635'.                
            05 RUPTURES-IGV635.                                                 
           10 IGV635-NLIEU              PIC X(03).                      007  003
           10 IGV635-DVENTE             PIC X(08).                      010  008
           10 IGV635-NVENTE             PIC X(07).                      018  007
           10 IGV635-NCODIC             PIC X(07).                      025  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGV635-SEQUENCE           PIC S9(04) COMP.                032  002
      *--                                                                       
           10 IGV635-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGV635.                                                   
           10 IGV635-CENREG             PIC X(07).                      034  007
           10 IGV635-NSOCIETE           PIC X(03).                      041  003
           10 IGV635-PVTOTAL            PIC S9(08)V9(2) COMP-3.         044  006
            05 FILLER                      PIC X(463).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGV635-LONG           PIC S9(4)   COMP  VALUE +049.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGV635-LONG           PIC S9(4) COMP-5  VALUE +049.           
                                                                                
      *}                                                                        
