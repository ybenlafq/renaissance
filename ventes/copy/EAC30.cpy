      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * CA VAD                                                          00000020
      ***************************************************************** 00000030
       01   EAC30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPI     PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEBUTL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MDDEBUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDEBUTF  PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MDDEBUTI  PIC X(10).                                      00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJDEBUTL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MJDEBUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MJDEBUTF  PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MJDEBUTI  PIC X(10).                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMODEL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MMODEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MMODEF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MMODEI    PIC X(8).                                       00000350
           02 MLIGNEI OCCURS   15 TIMES .                               00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUL  COMP PIC S9(4).                                 00000370
      *--                                                                       
             03 MLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLIEUF  PIC X.                                          00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MLIEUI  PIC X(10).                                      00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCATPML      COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MCATPML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCATPMF      PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MCATPMI      PIC X(7).                                  00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAACCL      COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MCAACCL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCAACCF      PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MCAACCI      PIC X(7).                                  00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCATOTL      COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MCATOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCATOTF      PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MCATOTI      PIC X(7).                                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPSEL   COMP PIC S9(4).                                 00000530
      *--                                                                       
             03 MPSEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPSEF   PIC X.                                          00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MPSEI   PIC X(7).                                       00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREML   COMP PIC S9(4).                                 00000570
      *--                                                                       
             03 MREML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MREMF   PIC X.                                          00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MREMI   PIC X(7).                                       00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVOLL   COMP PIC S9(4).                                 00000610
      *--                                                                       
             03 MVOLL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MVOLF   PIC X.                                          00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MVOLI   PIC X(5).                                       00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBVTL  COMP PIC S9(4).                                 00000650
      *--                                                                       
             03 MNBVTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNBVTF  PIC X.                                          00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MNBVTI  PIC X(5).                                       00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBVTCL      COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MNBVTCL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNBVTCF      PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MNBVTCI      PIC X(5).                                  00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MLIBERRI  PIC X(70).                                      00000760
      * CODE TRANSACTION                                                00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCODTRAI  PIC X(4).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MZONCMDI  PIC X(15).                                      00000850
      * NOM DU CICS                                                     00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      * NETNAME                                                         00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MNETNAMI  PIC X(8).                                       00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MSCREENI  PIC X(4).                                       00000990
      ***************************************************************** 00001000
      * CA VAD                                                          00001010
      ***************************************************************** 00001020
       01   EAC30O REDEFINES EAC30I.                                    00001030
           02 FILLER    PIC X(12).                                      00001040
      * DATE DU JOUR                                                    00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MDATJOUA  PIC X.                                          00001070
           02 MDATJOUC  PIC X.                                          00001080
           02 MDATJOUP  PIC X.                                          00001090
           02 MDATJOUH  PIC X.                                          00001100
           02 MDATJOUV  PIC X.                                          00001110
           02 MDATJOUO  PIC X(10).                                      00001120
      * HEURE                                                           00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MTIMJOUA  PIC X.                                          00001150
           02 MTIMJOUC  PIC X.                                          00001160
           02 MTIMJOUP  PIC X.                                          00001170
           02 MTIMJOUH  PIC X.                                          00001180
           02 MTIMJOUV  PIC X.                                          00001190
           02 MTIMJOUO  PIC X(5).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MPAGEA    PIC X.                                          00001220
           02 MPAGEC    PIC X.                                          00001230
           02 MPAGEP    PIC X.                                          00001240
           02 MPAGEH    PIC X.                                          00001250
           02 MPAGEV    PIC X.                                          00001260
           02 MPAGEO    PIC Z9.                                         00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MNBPA     PIC X.                                          00001290
           02 MNBPC     PIC X.                                          00001300
           02 MNBPP     PIC X.                                          00001310
           02 MNBPH     PIC X.                                          00001320
           02 MNBPV     PIC X.                                          00001330
           02 MNBPO     PIC Z9.                                         00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MDDEBUTA  PIC X.                                          00001360
           02 MDDEBUTC  PIC X.                                          00001370
           02 MDDEBUTP  PIC X.                                          00001380
           02 MDDEBUTH  PIC X.                                          00001390
           02 MDDEBUTV  PIC X.                                          00001400
           02 MDDEBUTO  PIC X(10).                                      00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MJDEBUTA  PIC X.                                          00001430
           02 MJDEBUTC  PIC X.                                          00001440
           02 MJDEBUTP  PIC X.                                          00001450
           02 MJDEBUTH  PIC X.                                          00001460
           02 MJDEBUTV  PIC X.                                          00001470
           02 MJDEBUTO  PIC X(10).                                      00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MMODEA    PIC X.                                          00001500
           02 MMODEC    PIC X.                                          00001510
           02 MMODEP    PIC X.                                          00001520
           02 MMODEH    PIC X.                                          00001530
           02 MMODEV    PIC X.                                          00001540
           02 MMODEO    PIC X(8).                                       00001550
           02 MLIGNEO OCCURS   15 TIMES .                               00001560
             03 FILLER       PIC X(2).                                  00001570
             03 MLIEUA  PIC X.                                          00001580
             03 MLIEUC  PIC X.                                          00001590
             03 MLIEUP  PIC X.                                          00001600
             03 MLIEUH  PIC X.                                          00001610
             03 MLIEUV  PIC X.                                          00001620
             03 MLIEUO  PIC X(10).                                      00001630
             03 FILLER       PIC X(2).                                  00001640
             03 MCATPMA      PIC X.                                     00001650
             03 MCATPMC PIC X.                                          00001660
             03 MCATPMP PIC X.                                          00001670
             03 MCATPMH PIC X.                                          00001680
             03 MCATPMV PIC X.                                          00001690
             03 MCATPMO      PIC ------9.                               00001700
             03 FILLER       PIC X(2).                                  00001710
             03 MCAACCA      PIC X.                                     00001720
             03 MCAACCC PIC X.                                          00001730
             03 MCAACCP PIC X.                                          00001740
             03 MCAACCH PIC X.                                          00001750
             03 MCAACCV PIC X.                                          00001760
             03 MCAACCO      PIC ------9.                               00001770
             03 FILLER       PIC X(2).                                  00001780
             03 MCATOTA      PIC X.                                     00001790
             03 MCATOTC PIC X.                                          00001800
             03 MCATOTP PIC X.                                          00001810
             03 MCATOTH PIC X.                                          00001820
             03 MCATOTV PIC X.                                          00001830
             03 MCATOTO      PIC ------9.                               00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MPSEA   PIC X.                                          00001860
             03 MPSEC   PIC X.                                          00001870
             03 MPSEP   PIC X.                                          00001880
             03 MPSEH   PIC X.                                          00001890
             03 MPSEV   PIC X.                                          00001900
             03 MPSEO   PIC ------9.                                    00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MREMA   PIC X.                                          00001930
             03 MREMC   PIC X.                                          00001940
             03 MREMP   PIC X.                                          00001950
             03 MREMH   PIC X.                                          00001960
             03 MREMV   PIC X.                                          00001970
             03 MREMO   PIC ------9.                                    00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MVOLA   PIC X.                                          00002000
             03 MVOLC   PIC X.                                          00002010
             03 MVOLP   PIC X.                                          00002020
             03 MVOLH   PIC X.                                          00002030
             03 MVOLV   PIC X.                                          00002040
             03 MVOLO   PIC ----9.                                      00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MNBVTA  PIC X.                                          00002070
             03 MNBVTC  PIC X.                                          00002080
             03 MNBVTP  PIC X.                                          00002090
             03 MNBVTH  PIC X.                                          00002100
             03 MNBVTV  PIC X.                                          00002110
             03 MNBVTO  PIC X(5).                                       00002120
             03 FILLER       PIC X(2).                                  00002130
             03 MNBVTCA      PIC X.                                     00002140
             03 MNBVTCC PIC X.                                          00002150
             03 MNBVTCP PIC X.                                          00002160
             03 MNBVTCH PIC X.                                          00002170
             03 MNBVTCV PIC X.                                          00002180
             03 MNBVTCO      PIC X(5).                                  00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLIBERRA  PIC X.                                          00002210
           02 MLIBERRC  PIC X.                                          00002220
           02 MLIBERRP  PIC X.                                          00002230
           02 MLIBERRH  PIC X.                                          00002240
           02 MLIBERRV  PIC X.                                          00002250
           02 MLIBERRO  PIC X(70).                                      00002260
      * CODE TRANSACTION                                                00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MCODTRAA  PIC X.                                          00002290
           02 MCODTRAC  PIC X.                                          00002300
           02 MCODTRAP  PIC X.                                          00002310
           02 MCODTRAH  PIC X.                                          00002320
           02 MCODTRAV  PIC X.                                          00002330
           02 MCODTRAO  PIC X(4).                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MZONCMDA  PIC X.                                          00002360
           02 MZONCMDC  PIC X.                                          00002370
           02 MZONCMDP  PIC X.                                          00002380
           02 MZONCMDH  PIC X.                                          00002390
           02 MZONCMDV  PIC X.                                          00002400
           02 MZONCMDO  PIC X(15).                                      00002410
      * NOM DU CICS                                                     00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MCICSA    PIC X.                                          00002440
           02 MCICSC    PIC X.                                          00002450
           02 MCICSP    PIC X.                                          00002460
           02 MCICSH    PIC X.                                          00002470
           02 MCICSV    PIC X.                                          00002480
           02 MCICSO    PIC X(5).                                       00002490
      * NETNAME                                                         00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MNETNAMA  PIC X.                                          00002520
           02 MNETNAMC  PIC X.                                          00002530
           02 MNETNAMP  PIC X.                                          00002540
           02 MNETNAMH  PIC X.                                          00002550
           02 MNETNAMV  PIC X.                                          00002560
           02 MNETNAMO  PIC X(8).                                       00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MSCREENA  PIC X.                                          00002590
           02 MSCREENC  PIC X.                                          00002600
           02 MSCREENP  PIC X.                                          00002610
           02 MSCREENH  PIC X.                                          00002620
           02 MSCREENV  PIC X.                                          00002630
           02 MSCREENO  PIC X(4).                                       00002640
                                                                                
