      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
          01 COMM-HV49-DONNEES.                                         00010001
             10 COMM-HV49-FONCTIONS               PIC X(01).            00020001
                88 COMM-HV49-TOUCHE-PF7           VALUE '1'.            00030001
                88 COMM-HV49-TOUCHE-PF8           VALUE '2'.            00040001
                88 COMM-HV49-TOUCHE-PF10          VALUE '5'.            00050001
                88 COMM-HV49-TOUCHE-PF11          VALUE '6'.            00060001
                88 COMM-HV49-DEBUT                VALUE '3'.            00070001
             10 COMM-HV49-CODRET                  PIC 9(03) COMP-3.     00080001
                88 COMM-HV49-BOUCLE               VALUE 555.            00090001
                88 COMM-HV49-ERREUR-MANIP         VALUE 999.            00100001
             10 COMM-HV49-LIBERR                  PIC X(25).            00110001
             10 COMM-HV49-EHV-NBLG                PIC 9(02).            00120001
             10 COMM-HV49-DONNEES-COMMUNE.                              00130001
                15 COMM-HV49-NOPAGE                  PIC 9(03).         00140001
      *-->         NOMBRE DE PROPOSITIONS TROUV�ES                      00150001
                15 COMM-HV49-NBPROPO                 PIC 9(03).         00160001
                15 COMM-HV49-TAB-SELECTION.                             00170001
                   20 COMM-HV49-SELECTION OCCURS 250 PIC S9(3) COMP-3.  00180001
                   20 COMM-HV49-MAX-SELECT           PIC S9(3) COMP-3.  00190001
                   20 COMM-HV49-DERN-SELECT          PIC S9(3) COMP-3.  00200001
                      88 COMM-HV49-PAS-EN-SELECTION    VALUE 0.         00210001
                      88 COMM-HV49-EN-SELECTION        VALUE 1 THRU 999.00220001
      *-->         N� DE LA PROPOSITION AFFICH�E                        00230001
                15 COMM-HV49-NOPROPO                PIC S9(3) COMP-3.   00240001
      *-->         N� INDICE DE LA PROPOSITION AFFICH�E DANS LE TABLEAU 00250001
                15 COMM-HV49-NOINDICE               PIC S9(3) COMP-3.   00260001
      *-->         NB DE PROPOSITIONS D�J� AFFICH�ES                    00270001
                15 COMM-HV49-NBPROPO-AFFICHES       PIC S9(3) COMP-3.   00280000
                                                                                
