      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************00000260
      * DCLGEN TABLE(DSA000.RVSL071B)                                  *        
      *        LIBRARY(DSA016.DEVL.RVSL071B.COBOL)                     *        
      *        ACTION(REPLACE)                                         *        
      *        LANGUAGE(COBOL)                                         *        
      *        NAMES(SL07B-)                                           *        
      *        STRUCTURE(RVSL071B)                                     *        
      *        APOST                                                   *        
      *        COLSUFFIX(YES)                                          *        
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *        
      ******************************************************************        
           EXEC SQL DECLARE DSA000.RVSL071B TABLE                               
           ( NSOCIETE                       CHAR(3) NOT NULL,                   
             NLIEU                          CHAR(3) NOT NULL,                   
             CEMPL                          CHAR(5) NOT NULL,                   
             CAUX                           CHAR(6) NOT NULL,                   
             CTIERS                         CHAR(6) NOT NULL,                   
             NCODIC                         CHAR(7) NOT NULL,                   
             CETAT                          CHAR(2) NOT NULL,                   
             QSTK                           DECIMAL(7, 0) NOT NULL,             
             QSTR                           DECIMAL(7, 0) NOT NULL,             
             IMAJ                           DECIMAL(7, 0) NOT NULL,             
             DSYST                          DECIMAL(13, 0) NOT NULL,            
             QSTKRP                         DECIMAL(7, 0) NOT NULL              
           ) END-EXEC.                                                          
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RVSL071B                    *00000270
      ******************************************************************00000280
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSL071B.                                                    00000290
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSL071B.                                                            
      *}                                                                        
      *                       NSOCIETE                                  00000300
           10 SL07B-NSOCIETE       PIC X(3).                            00000310
      *                       NLIEU                                     00000320
           10 SL07B-NLIEU          PIC X(3).                            00000330
      *                       CEMPL                                     00000340
           10 SL07B-CEMPL          PIC X(5).                            00000350
      *                       CAUX                                      00000360
           10 SL07B-CAUX           PIC X(6).                            00000370
      *                       CTIERS                                    00000380
           10 SL07B-CTIERS         PIC X(6).                            00000390
      *                       NCODIC                                    00000400
           10 SL07B-NCODIC         PIC X(7).                            00000410
      *                       CETAT                                     00000420
           10 SL07B-CETAT          PIC X(2).                            00000430
      *                       QSTK                                      00000440
           10 SL07B-QSTK           PIC S9(7)V USAGE COMP-3.             00000450
      *                       QSTR                                      00000460
           10 SL07B-QSTR           PIC S9(7)V USAGE COMP-3.             00000470
      *                       IMAJ                                      00000480
           10 SL07B-IMAJ           PIC S9(7)V USAGE COMP-3.             00000490
      *                       DSYST                                     00000500
           10 SL07B-DSYST          PIC S9(13)V USAGE COMP-3.            00000510
      *                       QSTKRP                                    00000520
           10 SL07B-QSTKRP         PIC S9(7)V USAGE COMP-3.             00000530
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************00000540
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 12      *00000550
      ******************************************************************00000560
      **********************************************************        00000570
      *   LISTE DES FLAGS DE LA TABLE RVGS1000                          00000580
      **********************************************************        00000590
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSL071B-FLAGS.                                              00000600
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSL071B-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 SL07B-NSOCIETE-F     PIC S9(4) COMP.                      00000610
      *--                                                                       
           05 SL07B-NSOCIETE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 SL07B-NLIEU-F        PIC S9(4) COMP.                      00000620
      *--                                                                       
           05 SL07B-NLIEU-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 SL07B-CEMPL-F        PIC S9(4) COMP.                      00000630
      *--                                                                       
           05 SL07B-CEMPL-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 SL07B-CAUX-F         PIC S9(4) COMP.                      00000640
      *--                                                                       
           05 SL07B-CAUX-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 SL07B-CTIERS-F       PIC S9(4) COMP.                      00000650
      *--                                                                       
           05 SL07B-CTIERS-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 SL07B-NCODIC-F       PIC S9(4) COMP.                      00000660
      *--                                                                       
           05 SL07B-NCODIC-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 SL07B-CETAT-F        PIC S9(4) COMP.                      00000670
      *--                                                                       
           05 SL07B-CETAT-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 SL07B-QSTK-F         PIC S9(4) COMP.                      00000680
      *--                                                                       
           05 SL07B-QSTK-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 SL07B-QSTR-F         PIC S9(4) COMP.                      00000690
      *--                                                                       
           05 SL07B-QSTR-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 SL07B-IMAJ-F         PIC S9(4) COMP.                      00000700
      *--                                                                       
           05 SL07B-IMAJ-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 SL07B-DSYST-F        PIC S9(4) COMP.                      00000710
      *--                                                                       
           05 SL07B-DSYST-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 SL07B-QSTKRP-F       PIC S9(4) COMP.                      00000720
      *                                                                         
      *--                                                                       
           05 SL07B-QSTKRP-F       PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
