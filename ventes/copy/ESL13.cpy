      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESL13   ESL13                                              00000020
      ***************************************************************** 00000030
       01   ESL13I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPAGESI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOCCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUCL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNLIEUCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEUCF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNLIEUCI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPDOCL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MTYPDOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPDOCF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MTYPDOCI  PIC X(2).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMDOCL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNUMDOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUMDOCF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNUMDOCI  PIC X(7).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEDOCL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MDATEDOCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEDOCF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDATEDOCI      PIC X(10).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCOL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNSOCOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOCOF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNSOCOI   PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUOL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNLIEUOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEUOF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNLIEUOI  PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUOL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLLIEUOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIEUOF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLLIEUOI  PIC X(20).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNSOCDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOCDF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNSOCDI   PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUDL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNLIEUDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEUDF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNLIEUDI  PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUDL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLLIEUDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIEUDF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLLIEUDI  PIC X(20).                                      00000650
           02 MCFAMD OCCURS   14 TIMES .                                00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCFAMI  PIC X(5).                                       00000700
           02 MCMARQD OCCURS   14 TIMES .                               00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000720
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000730
             03 FILLER  PIC X(4).                                       00000740
             03 MCMARQI      PIC X(5).                                  00000750
           02 MNCODICD OCCURS   14 TIMES .                              00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MNCODICI     PIC X(7).                                  00000800
           02 MLREFD OCCURS   14 TIMES .                                00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000820
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000830
             03 FILLER  PIC X(4).                                       00000840
             03 MLREFI  PIC X(20).                                      00000850
           02 MQEXPD OCCURS   14 TIMES .                                00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPL  COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MQEXPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQEXPF  PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQEXPI  PIC X(5).                                       00000900
           02 MQRECD OCCURS   14 TIMES .                                00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQRECL  COMP PIC S9(4).                                 00000920
      *--                                                                       
             03 MQRECL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQRECF  PIC X.                                          00000930
             03 FILLER  PIC X(4).                                       00000940
             03 MQRECI  PIC X(5).                                       00000950
           02 MQARBD OCCURS   14 TIMES .                                00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQARBL  COMP PIC S9(4).                                 00000970
      *--                                                                       
             03 MQARBL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQARBF  PIC X.                                          00000980
             03 FILLER  PIC X(4).                                       00000990
             03 MQARBI  PIC X(5).                                       00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001020
           02 FILLER    PIC X(4).                                       00001030
           02 MLIBERRI  PIC X(78).                                      00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MCODTRAI  PIC X(4).                                       00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001090
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001100
           02 FILLER    PIC X(4).                                       00001110
           02 MCICSI    PIC X(5).                                       00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001130
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001140
           02 FILLER    PIC X(4).                                       00001150
           02 MNETNAMI  PIC X(8).                                       00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001170
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001180
           02 FILLER    PIC X(4).                                       00001190
           02 MSCREENI  PIC X(4).                                       00001200
      ***************************************************************** 00001210
      * SDF: ESL13   ESL13                                              00001220
      ***************************************************************** 00001230
       01   ESL13O REDEFINES ESL13I.                                    00001240
           02 FILLER    PIC X(12).                                      00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MDATJOUA  PIC X.                                          00001270
           02 MDATJOUC  PIC X.                                          00001280
           02 MDATJOUP  PIC X.                                          00001290
           02 MDATJOUH  PIC X.                                          00001300
           02 MDATJOUV  PIC X.                                          00001310
           02 MDATJOUO  PIC X(10).                                      00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MTIMJOUA  PIC X.                                          00001340
           02 MTIMJOUC  PIC X.                                          00001350
           02 MTIMJOUP  PIC X.                                          00001360
           02 MTIMJOUH  PIC X.                                          00001370
           02 MTIMJOUV  PIC X.                                          00001380
           02 MTIMJOUO  PIC X(5).                                       00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MNPAGEA   PIC X.                                          00001410
           02 MNPAGEC   PIC X.                                          00001420
           02 MNPAGEP   PIC X.                                          00001430
           02 MNPAGEH   PIC X.                                          00001440
           02 MNPAGEV   PIC X.                                          00001450
           02 MNPAGEO   PIC X(3).                                       00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MNBPAGESA      PIC X.                                     00001480
           02 MNBPAGESC PIC X.                                          00001490
           02 MNBPAGESP PIC X.                                          00001500
           02 MNBPAGESH PIC X.                                          00001510
           02 MNBPAGESV PIC X.                                          00001520
           02 MNBPAGESO      PIC X(3).                                  00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MNSOCCA   PIC X.                                          00001550
           02 MNSOCCC   PIC X.                                          00001560
           02 MNSOCCP   PIC X.                                          00001570
           02 MNSOCCH   PIC X.                                          00001580
           02 MNSOCCV   PIC X.                                          00001590
           02 MNSOCCO   PIC X(3).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MNLIEUCA  PIC X.                                          00001620
           02 MNLIEUCC  PIC X.                                          00001630
           02 MNLIEUCP  PIC X.                                          00001640
           02 MNLIEUCH  PIC X.                                          00001650
           02 MNLIEUCV  PIC X.                                          00001660
           02 MNLIEUCO  PIC X(3).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MTYPDOCA  PIC X.                                          00001690
           02 MTYPDOCC  PIC X.                                          00001700
           02 MTYPDOCP  PIC X.                                          00001710
           02 MTYPDOCH  PIC X.                                          00001720
           02 MTYPDOCV  PIC X.                                          00001730
           02 MTYPDOCO  PIC X(2).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MNUMDOCA  PIC X.                                          00001760
           02 MNUMDOCC  PIC X.                                          00001770
           02 MNUMDOCP  PIC X.                                          00001780
           02 MNUMDOCH  PIC X.                                          00001790
           02 MNUMDOCV  PIC X.                                          00001800
           02 MNUMDOCO  PIC X(7).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MDATEDOCA      PIC X.                                     00001830
           02 MDATEDOCC PIC X.                                          00001840
           02 MDATEDOCP PIC X.                                          00001850
           02 MDATEDOCH PIC X.                                          00001860
           02 MDATEDOCV PIC X.                                          00001870
           02 MDATEDOCO      PIC X(10).                                 00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MNSOCOA   PIC X.                                          00001900
           02 MNSOCOC   PIC X.                                          00001910
           02 MNSOCOP   PIC X.                                          00001920
           02 MNSOCOH   PIC X.                                          00001930
           02 MNSOCOV   PIC X.                                          00001940
           02 MNSOCOO   PIC X(3).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MNLIEUOA  PIC X.                                          00001970
           02 MNLIEUOC  PIC X.                                          00001980
           02 MNLIEUOP  PIC X.                                          00001990
           02 MNLIEUOH  PIC X.                                          00002000
           02 MNLIEUOV  PIC X.                                          00002010
           02 MNLIEUOO  PIC X(3).                                       00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MLLIEUOA  PIC X.                                          00002040
           02 MLLIEUOC  PIC X.                                          00002050
           02 MLLIEUOP  PIC X.                                          00002060
           02 MLLIEUOH  PIC X.                                          00002070
           02 MLLIEUOV  PIC X.                                          00002080
           02 MLLIEUOO  PIC X(20).                                      00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MNSOCDA   PIC X.                                          00002110
           02 MNSOCDC   PIC X.                                          00002120
           02 MNSOCDP   PIC X.                                          00002130
           02 MNSOCDH   PIC X.                                          00002140
           02 MNSOCDV   PIC X.                                          00002150
           02 MNSOCDO   PIC X(3).                                       00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MNLIEUDA  PIC X.                                          00002180
           02 MNLIEUDC  PIC X.                                          00002190
           02 MNLIEUDP  PIC X.                                          00002200
           02 MNLIEUDH  PIC X.                                          00002210
           02 MNLIEUDV  PIC X.                                          00002220
           02 MNLIEUDO  PIC X(3).                                       00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MLLIEUDA  PIC X.                                          00002250
           02 MLLIEUDC  PIC X.                                          00002260
           02 MLLIEUDP  PIC X.                                          00002270
           02 MLLIEUDH  PIC X.                                          00002280
           02 MLLIEUDV  PIC X.                                          00002290
           02 MLLIEUDO  PIC X(20).                                      00002300
           02 DFHMS1 OCCURS   14 TIMES .                                00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MCFAMA  PIC X.                                          00002330
             03 MCFAMC  PIC X.                                          00002340
             03 MCFAMP  PIC X.                                          00002350
             03 MCFAMH  PIC X.                                          00002360
             03 MCFAMV  PIC X.                                          00002370
             03 MCFAMO  PIC X(5).                                       00002380
           02 DFHMS2 OCCURS   14 TIMES .                                00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MCMARQA      PIC X.                                     00002410
             03 MCMARQC PIC X.                                          00002420
             03 MCMARQP PIC X.                                          00002430
             03 MCMARQH PIC X.                                          00002440
             03 MCMARQV PIC X.                                          00002450
             03 MCMARQO      PIC X(5).                                  00002460
           02 DFHMS3 OCCURS   14 TIMES .                                00002470
             03 FILLER       PIC X(2).                                  00002480
             03 MNCODICA     PIC X.                                     00002490
             03 MNCODICC     PIC X.                                     00002500
             03 MNCODICP     PIC X.                                     00002510
             03 MNCODICH     PIC X.                                     00002520
             03 MNCODICV     PIC X.                                     00002530
             03 MNCODICO     PIC X(7).                                  00002540
           02 DFHMS4 OCCURS   14 TIMES .                                00002550
             03 FILLER       PIC X(2).                                  00002560
             03 MLREFA  PIC X.                                          00002570
             03 MLREFC  PIC X.                                          00002580
             03 MLREFP  PIC X.                                          00002590
             03 MLREFH  PIC X.                                          00002600
             03 MLREFV  PIC X.                                          00002610
             03 MLREFO  PIC X(20).                                      00002620
           02 DFHMS5 OCCURS   14 TIMES .                                00002630
             03 FILLER       PIC X(2).                                  00002640
             03 MQEXPA  PIC X.                                          00002650
             03 MQEXPC  PIC X.                                          00002660
             03 MQEXPP  PIC X.                                          00002670
             03 MQEXPH  PIC X.                                          00002680
             03 MQEXPV  PIC X.                                          00002690
             03 MQEXPO  PIC X(5).                                       00002700
           02 DFHMS6 OCCURS   14 TIMES .                                00002710
             03 FILLER       PIC X(2).                                  00002720
             03 MQRECA  PIC X.                                          00002730
             03 MQRECC  PIC X.                                          00002740
             03 MQRECP  PIC X.                                          00002750
             03 MQRECH  PIC X.                                          00002760
             03 MQRECV  PIC X.                                          00002770
             03 MQRECO  PIC X(5).                                       00002780
           02 DFHMS7 OCCURS   14 TIMES .                                00002790
             03 FILLER       PIC X(2).                                  00002800
             03 MQARBA  PIC X.                                          00002810
             03 MQARBC  PIC X.                                          00002820
             03 MQARBP  PIC X.                                          00002830
             03 MQARBH  PIC X.                                          00002840
             03 MQARBV  PIC X.                                          00002850
             03 MQARBO  PIC X(5).                                       00002860
           02 FILLER    PIC X(2).                                       00002870
           02 MLIBERRA  PIC X.                                          00002880
           02 MLIBERRC  PIC X.                                          00002890
           02 MLIBERRP  PIC X.                                          00002900
           02 MLIBERRH  PIC X.                                          00002910
           02 MLIBERRV  PIC X.                                          00002920
           02 MLIBERRO  PIC X(78).                                      00002930
           02 FILLER    PIC X(2).                                       00002940
           02 MCODTRAA  PIC X.                                          00002950
           02 MCODTRAC  PIC X.                                          00002960
           02 MCODTRAP  PIC X.                                          00002970
           02 MCODTRAH  PIC X.                                          00002980
           02 MCODTRAV  PIC X.                                          00002990
           02 MCODTRAO  PIC X(4).                                       00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MCICSA    PIC X.                                          00003020
           02 MCICSC    PIC X.                                          00003030
           02 MCICSP    PIC X.                                          00003040
           02 MCICSH    PIC X.                                          00003050
           02 MCICSV    PIC X.                                          00003060
           02 MCICSO    PIC X(5).                                       00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MNETNAMA  PIC X.                                          00003090
           02 MNETNAMC  PIC X.                                          00003100
           02 MNETNAMP  PIC X.                                          00003110
           02 MNETNAMH  PIC X.                                          00003120
           02 MNETNAMV  PIC X.                                          00003130
           02 MNETNAMO  PIC X(8).                                       00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MSCREENA  PIC X.                                          00003160
           02 MSCREENC  PIC X.                                          00003170
           02 MSCREENP  PIC X.                                          00003180
           02 MSCREENH  PIC X.                                          00003190
           02 MSCREENV  PIC X.                                          00003200
           02 MSCREENO  PIC X(4).                                       00003210
                                                                                
