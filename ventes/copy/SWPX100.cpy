      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      * LG = 210                                                                
      * LG = 250 AJOUT PRIME BRUTE + FILLER                                     
      *                                                                         
       01  FPX100-DSECT.                                                        
           03  FPX100-CLEF.                                                     
>003           05  FPX100-NSOCIETE         PIC  X(03).                          
>006           05  FPX100-NMAG             PIC  X(03).                          
>012           05  FPX100-CVENDEUR         PIC  X(06).                          
>018           05  FPX100-DMVENTE          PIC  X(06).                          
>023           05  FPX100-AGREGAT          PIC  9(05).                          
      *                                                                         
               05  FPX100-AGREG1.                                               
>031            10 FPX100-DCREATION        PIC  X(08).                          
>041            10 FPX100-LVEND10          PIC  X(10).                          
>048            10 FPX100-NCODIC           PIC  X(07).                          
>053            10 FPX100-CFAM             PIC  X(05).                          
>058            10 FPX100-CMARQ            PIC  X(05).                          
>065            10 FPX100-NVENTE           PIC  X(07).                          
      *                                                                         
               05  FPX100-AGREG2 REDEFINES FPX100-AGREG1.                       
>028            10 FPX100-CRAYONFAM        PIC  X(05).                          
>048            10 FPX100-LAGREGATED       PIC  X(20).                          
>065            10 FILLER                  PIC  X(17).                          
      *                                                                         
>066           05  FPX100-REPRISE          PIC  X(01).                          
>067           05  FPX100-GROUPE           PIC  X(01).                          
      *                                                                         
>071       03  FPX100-QVENDUE              PIC S9(05)V99  COMP-3.               
>075       03  FPX100-QVENDUEEP            PIC S9(05)V99  COMP-3.               
>079       03  FPX100-QVENDUESV            PIC S9(05)V99  COMP-3.               
>083       03  FPX100-QVENDUECM            PIC S9(05)V99  COMP-3.               
>086       03  FPX100-QNBPSE               PIC S9(05)     COMP-3.               
>090       03  FPX100-QNBPSAB              PIC S9(05)V99  COMP-3.               
      *                                                                         
>095       03  FPX100-PCA                  PIC S9(07)V99  COMP-3.               
>100       03  FPX100-PCAEP                PIC S9(07)V99  COMP-3.               
>105       03  FPX100-PCASV                PIC S9(07)V99  COMP-3.               
>110       03  FPX100-PCACM                PIC S9(07)V99  COMP-3.               
>115       03  FPX100-PCAPSE               PIC S9(07)V99  COMP-3.               
>120       03  FPX100-PRMP                 PIC S9(07)V99  COMP-3.               
>125       03  FPX100-PRMPEP               PIC S9(07)V99  COMP-3.               
>130       03  FPX100-PRMPSV               PIC S9(07)V99  COMP-3.               
>135       03  FPX100-PRMPCM               PIC S9(07)V99  COMP-3.               
>140       03  FPX100-MTREM                PIC S9(07)V99  COMP-3.               
>145       03  FPX100-MTREMEP              PIC S9(07)V99  COMP-3.               
>150       03  FPX100-PMTVOL               PIC S9(07)V99  COMP-3.               
>155       03  FPX100-PMTJAUNE             PIC S9(07)V99  COMP-3.               
>160       03  FPX100-PMTGRISE             PIC S9(07)V99  COMP-3.               
>165       03  FPX100-PMTEP                PIC S9(07)V99  COMP-3.               
>170       03  FPX100-PMTPSE               PIC S9(07)V99  COMP-3.               
>175       03  FPX100-CGARANTIE            PIC  X(05).                          
>180       03  FPX100-MTVA                 PIC S9(07)V99  COMP-3.               
>185       03  FPX100-MTVAEP               PIC S9(07)V99  COMP-3.               
>190       03  FPX100-MTVASV               PIC S9(07)V99  COMP-3.               
>195       03  FPX100-MTVACM               PIC S9(07)V99  COMP-3.               
>200       03  FPX100-MTVAPSE              PIC S9(07)V99  COMP-3.               
      *                                                                         
>208       03  FPX100-DMVT                 PIC  X(08).                          
>213       03  FPX100-PMTBRUTE             PIC S9(07)V99  COMP-3.               
>250       03  FILLER                      PIC  X(37).                          
      *                                                                         
                                                                                
