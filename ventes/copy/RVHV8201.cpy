      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVHV8201                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV8201                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHV8201.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHV8201.                                                            
      *}                                                                        
           02  HV82-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV82-NLIEU                                                       
               PIC X(0003).                                                     
           02  HV82-NSOCMAG                                                     
               PIC X(0003).                                                     
           02  HV82-NMAG                                                        
               PIC X(0003).                                                     
           02  HV82-CRAYONFAM                                                   
               PIC X(0005).                                                     
           02  HV82-CVENDEUR                                                    
               PIC X(0006).                                                     
           02  HV82-DMVENTE                                                     
               PIC X(0006).                                                     
           02  HV82-NAGREGATED                                                  
               PIC S9(5) COMP-3.                                                
           02  HV82-LAGREGATED                                                  
               PIC X(0020).                                                     
           02  HV82-QVENDUE                                                     
               PIC S9(5) COMP-3.                                                
           02  HV82-PCA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-PMTACHATS                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-PMTPRIMES                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-PMTCOMM1                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-PMTCOMM2                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-QNBPSE                                                      
               PIC S9(5) COMP-3.                                                
           02  HV82-PCAPSE                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-PMTPRIMPSE                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  HV82-QNBPSAB                                                     
               PIC S9(5) COMP-3.                                                
           02  HV82-QVENDUEEP                                                   
               PIC S9(5) COMP-3.                                                
           02  HV82-QVENDUESV                                                   
               PIC S9(5) COMP-3.                                                
           02  HV82-QVENDUECM                                                   
               PIC S9(5) COMP-3.                                                
           02  HV82-PCAEP                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-PCASV                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-PCACM                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-PMTACHEP                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-PMTACHSV                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-PMTACHCM                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-PMTREM                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-PMTREMEP                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-PMTCOMMEP                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-MTVA                                                        
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-MTVAEP                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-MTVASV                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV82-MTVACM                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHV8201                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHV8201-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHV8201-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-NSOCMAG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-NSOCMAG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-CRAYONFAM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-CRAYONFAM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-DMVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-DMVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-NAGREGATED-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-NAGREGATED-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-LAGREGATED-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-LAGREGATED-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-QVENDUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-QVENDUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-PCA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-PCA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-PMTACHATS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-PMTACHATS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-PMTPRIMES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-PMTPRIMES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-PMTCOMM1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-PMTCOMM1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-PMTCOMM2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-PMTCOMM2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-QNBPSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-QNBPSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-PCAPSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-PCAPSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-PMTPRIMPSE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-PMTPRIMPSE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-QNBPSAB-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-QNBPSAB-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-QVENDUEEP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-QVENDUEEP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-QVENDUESV-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-QVENDUESV-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-QVENDUECM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-QVENDUECM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-PCAEP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-PCAEP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-PCASV-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-PCASV-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-PCACM-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-PCACM-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-PMTACHEP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-PMTACHEP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-PMTACHSV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-PMTACHSV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-PMTACHCM-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-PMTACHCM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-PMTREM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-PMTREM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-PMTREMEP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-PMTREMEP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-PMTCOMMEP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-PMTCOMMEP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-MTVA-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-MTVA-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-MTVAEP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-MTVAEP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-MTVASV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-MTVASV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV82-MTVACM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV82-MTVACM-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
