      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * ------------------------------------------------------------ *          
      * 18/07/2007 : projet portail unique                           *          
      * zone de communication pour le module MPO02                   *          
      *                                                              *          
      * ------------------------------------------------------------ *          
       01  COMM-PO02-appli.                                                     
         05  COMM-PO02-ENTREE.                                                  
           10  COMM-PO02E-CAPPEL            PIC X(01).                          
           10  COMM-PO02E-NUECH             PIC 9(09).                          
           10  COMM-PO02E-NSOCV             PIC X(03).                          
           10  COMM-PO02E-NLIEUV            PIC X(03).                          
           10  COMM-PO02E-NVENTE            PIC X(07).                          
           10  COMM-PO02E-NCODIC            PIC X(07).                          
           10  COMM-PO02E-NSEQNQ            PIC S9(5) COMP-3.                   
           10  COMM-PO02E-NSOCR             PIC X(03).                          
           10  COMM-PO02E-NLIEUR            PIC X(03).                          
           10  COMM-PO02E-NNSOC             PIC X(03).                          
           10  COMM-PO02E-NNLIEU            PIC X(03).                          
           10  COMM-PO02E-NNVENTE           PIC X(07).                          
           10  COMM-PO02E-NNCODIC           PIC X(07).                          
           10  COMM-PO02E-NNSEQNQ           PIC S9(5) COMP-3.                   
           10  COMM-PO02E-PECHA             PIC S9(7)V9(2) USAGE COMP-3.        
           10  COMM-PO02E-ACID              PIC X(10).                          
           10  COMM-PO02E-NSOCE             PIC X(03).                          
           10  COMM-PO02E-NLIEUE            PIC X(03).                          
           10  FILLER                       PIC X(14).                          
      *                                                                         
         05  COMM-PO02-SORTIE.                                                  
           10  COMM-PO02S-CODRET            PIC X(04).                          
           10  COMM-PO02S-LIBRET            PIC X(80).                          
           10  COMM-PO02S-NUECH             PIC 9(09).                          
           10  COMM-PO02S-NSOCV             PIC X(03).                          
           10  COMM-PO02S-NLIEUV            PIC X(03).                          
           10  COMM-PO02S-NVENTE            PIC X(07).                          
           10  COMM-PO02S-NSOCE             PIC X(03).                          
           10  COMM-PO02S-NLIEUE            PIC X(03).                          
           10  COMM-PO02S-NSOCR             PIC X(03).                          
           10  COMM-PO02S-NLIEUR            PIC X(03).                          
           10  COMM-PO02S-NNSOC             PIC X(03).                          
           10  COMM-PO02S-NNLIEU            PIC X(03).                          
           10  COMM-PO02S-NNVENTE           PIC X(07).                          
           10  COMM-PO02s-PaCHAt            PIC S9(7)V9(2) USAGE COMP-3.        
           10  filler                       pic x(20).                          
                                                                                
