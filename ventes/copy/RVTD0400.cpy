      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RVTD0400                           *        
      ******************************************************************        
       01  RVTD0400.                                                            
           10 TD04-NSOCIETE             PIC X(3).                               
           10 TD04-NLIEU                PIC X(3).                               
           10 TD04-NVENTE               PIC X(7).                               
           10 TD04-NDOSSIER             PIC S9(3)V USAGE COMP-3.                
           10 TD04-NSEQNQ               PIC S9(5)V USAGE COMP-3.                
           10 TD04-NCODIC               PIC X(7).                               
           10 TD04-NCODICGRP            PIC X(7).                               
           10 TD04-CPRESTATION          PIC X(5).                               
           10 TD04-CTYPPREST            PIC X(5).                               
           10 TD04-CTYPCND              PIC X(5).                               
           10 TD04-DVALO                PIC X(8).                               
           10 TD04-PMONTANT             PIC S9(7)V9(2) USAGE COMP-3.            
           10 TD04-DSYST                PIC S9(13)V USAGE COMP-3.               
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
                                                                                
