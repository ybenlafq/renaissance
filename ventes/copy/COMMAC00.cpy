      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!          
FT0900* MODIFS EFFECTU�ES EN SEPT 2000 PAR FT                                   
      * METTRE EN ROUGE LES MAGASINS DONT LE CHIFFRE N'EST PAS REMONTE          
      * DEPUIS UN CERTAIN DELAI PAR MQ                                          
      *!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!          
08/00 * MODIFS EFFECTU�ES EN AOUT 2000 PAR JONATHAN(DSA024) SUR LES             
08/00 * ZONES DE CA POUR QUE CELLE CI ACCEPTE LES MONTANTS NEGATIFS             
08/00 * POUR LA PLUPART DES ZONES PASSAGE DE 9(X) A S9(X)                       
      *!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!          
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      **************************************************************            
      *                                                                         
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET                    
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR          
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU                  
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                            
      *                                                                         
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                              
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES               
      * PAR AIDA                                                                
      *                                                                         
      *-------------------------------------------------------------            
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-AC00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-AC00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
          02 FILLER-COM-AIDA      PIC X(100).                                   
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
          02 COMM-CICS-APPLID     PIC X(8).                                     
          02 COMM-CICS-NETNAM     PIC X(8).                                     
          02 COMM-CICS-TRANSA     PIC X(4).                                     
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
          02 COMM-DATE-SIECLE     PIC XX.                                       
          02 COMM-DATE-ANNEE      PIC XX.                                       
          02 COMM-DATE-MOIS       PIC XX.                                       
          02 COMM-DATE-JOUR       PIC XX.                                       
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
          02 COMM-DATE-QNTA       PIC 999.                                      
          02 COMM-DATE-QNT0       PIC 99999.                                    
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
          02 COMM-DATE-BISX       PIC 9.                                        
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                   
          02 COMM-DATE-JSM        PIC 9.                                        
      *   LIBELLES DU JOUR COURT - LONG                                         
          02 COMM-DATE-JSM-LC     PIC XXX.                                      
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                 
      *   LIBELLES DU MOIS COURT - LONG                                         
          02 COMM-DATE-MOIS-LC    PIC XXX.                                      
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                 
      *   DIFFERENTES FORMES DE DATE                                            
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                                     
          02 COMM-DATE-AAMMJJ     PIC X(6).                                     
          02 COMM-DATE-JJMMSSAA   PIC X(8).                                     
          02 COMM-DATE-JJMMAA     PIC X(6).                                     
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                                     
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                    
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
          02 COMM-DATE-WEEK.                                                    
             05 COMM-DATE-SEMSS   PIC 9(02).                                    
             05 COMM-DATE-SEMAA   PIC 9(02).                                    
             05 COMM-DATE-SEMNU   PIC 9(02).                                    
          02 COMM-DATE-FILLER     PIC X(08).                                    
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                      
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 200 PIC X(1).                                
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3674          
      *                                                                         
      *            TRANSACTION AC00 : ADMINISTRATION DES DONNEES      *         
      *                                                                         
      *------------------------------ ZONE COMMUNE -----------------            
      *****************************************************************         
      *   LIEU DE CONSULTATION OU DE SAISIE                                     
      *   DEFINIS EN FONCTION DE LA SIGNATURE                                   
      *****************************************************************         
          02 COMM-AC00-APPLI.                                                   
      * DONNEE ORIGINE DE LA SIGNATURE CONTROLEES DANS RVGA1003                 
      * EN FONCTION DE L' ACID DU DEMANDEUR                                     
      *   SOC LIEU: 000 000 = GRP / XXX 000 = SOC OU MGI / XXX XXX = MGC        
      *   CTYPSOC(MGC,MGI,SOC,GRP) DE LA SIGNATURE                              
      *   CTYPLIEU : 1 = MGC 3 = SOC OU MGI OU GRP                              
      *   CACID = CACID1/2/3/4/5/6                                              
      *   SOCGRP : SOC DE REGROUPEMENT DU LIEU DE LA SIGNATURE                  
      *   CTYPSOCGRP : SOC SI MGC OU MGI /  GRP SI SOC                          
             03 COMM-AC00-NSOCIETE       PIC X(3).                              
             03 COMM-AC00-NLIEU          PIC X(3).                              
             03 COMM-AC00-LLIEU          PIC X(20).                             
             03 COMM-AC00-CTYPLIEU       PIC X(1).                              
             03 COMM-AC00-CTYPSOC        PIC X(3).                              
             03 COMM-AC00-CACID          PIC X(4).                              
             03 COMM-AC00-NSOCGRP        PIC X(3).                              
             03 COMM-AC00-NORDRE         PIC X(3).                              
             03 COMM-AC00-CTYPSOCGRP     PIC X(3).                              
             03 COMM-AC00-CUMCA          PIC X(1).                              
             03 COMM-AC00-DOUV           PIC X(8).                              
      *DONNEE LIEU SAISIE DANS TAC20 : SOC OU MGI OU MGC                        
      *   APPARTENANT � LA SOCIETE DE REGROUPEMENT SI MODIFIABLE                
      *   NLIEU1 = NSOCIETE                                                     
      *   NLIEU2 = NLIEU                                                        
      *   CMOD1/2 : NON MODIFIABLE SI MGC OU MGI A LA SIGNATURE                 
      *   CMOD1/2 : MODIFIABLE SI GRP OU SOC A LA SIGNATURE                     
      *   CMOD1/2 :  0 = NON MODIFIABLE / 1 = MODIFIABLE                        
      *   CTYPMOD :  SOC / MGI / MGC                                            
             03 COMM-AC00-NLIEU1         PIC X(3).                              
             03 COMM-AC00-CMOD1          PIC X(1).                              
             03 COMM-AC00-NLIEU2         PIC X(3).                              
             03 COMM-AC00-CMOD2          PIC X(1).                              
             03 COMM-AC00-LLIEU1         PIC X(20).                             
             03 COMM-AC00-CTYPMOD        PIC X(3).                              
             03 COMM-AC00-CUMCAMOD       PIC X(1).                              
             03 COMM-AC00-DOUVMOD        PIC X(8).                              
             03 COMM-AC00-SELECT         PIC X(1).                              
      *PARAMETRE HEURE PAR JOUR                                                 
             03 COMM-AC00-HEURE-JOUR.                                           
                04 COMM-AC00-HJ          PIC 99V99.                             
             03 COMM-AC00-FILLER         PIC X(4099).                           
      *****************************************************************         
      *                                                               *         
      *                    ********************                       *         
      *                    * COMM-AC00-FILLER *                       *         
      *                    *--------+---------*                       *         
      *                             !                                 *         
      *                +------------+----------+                      *         
      *       *--------!--------+              !                      *         
      *       ! COMM-AC20-DONNEE!              !                      *         
      *       *--------!--------+              !                      *         
      *                !                       !                      *         
      *       *--------!--------+              !                      *         
      *       ! COMM-AC20-FILLER!              !                      *         
      *       *--------+--------+              !                      *         
      *                !                       !                      *         
      *    +-----------!-----------+           !                      *         
      *COMM-AC21-APPLI     COMM-AC22-APPLI     !                      *         
      *                    !                   !                      *         
      *           +--------!--------+ +--------!--------+             *         
      *           ! COMM-AC23-DONNEE! ! COMM-AC10-DONNEE!             *         
      *           +--------!--------+ +--------!--------+             *         
      *                                        !                      *         
      *                               +--------!--------+             *         
      *                               ! COMM-AC10-FILLER!             *         
      *                               +-----------------+             *         
      *                                        !                      *         
      *                       +----------------!------------+         *         
      *                       !                !            !                   
      *               +-------!----------+     !   +--------!--------+          
      *               ! COMM-AC11-DONNEE       !   !COMM-AC13-DONNEE !*         
      *               +-------!----------+     !   +--------!--------+          
      *                       !                !            !                   
      *               +-------!----------+     !   +--------!--------+          
      *               ! COMM-AC11-FILLER !     !   ! COMM-AC13-FILLER!          
      *               +------------------+     !   +-----------------+          
      *                       !                !            !                   
      *               +-------!----------+     !   +--------!--------+          
      *               ! COMM-AC12-DONNEE !     !   ! COMM-AC14-DONNEE!          
      *               +------------------+     !   +-----------------+          
      *                                        !                                
      *                               +--------!---------+            *         
      *                               ! COMM-AC0X-DONNEE !            *         
      *                               +------------------+            *         
      *                                                               *         
      *****************************************************************         
      *   SAISIE PERIODE DE CONSULTATION                              *         
      *                                                               *         
      *****************************************************************         
             03 COMM-AC10-APPLI          REDEFINES COMM-AC00-FILLER.            
      *  DATE DE DEBUT DE LA PERIODE DE CONSULTATION                            
             05 COMM-AC10-PERDEB.                                               
                07 COMM-AC10-DSS       PIC X(2).                                
                07 COMM-AC10-DAA       PIC X(2).                                
                07 COMM-AC10-DMM       PIC X(2).                                
                07 COMM-AC10-DJJ       PIC X(2).                                
             05    COMM-AC10-DLC       PIC X(3).                                
      *  DATE DE DEBUT PERIODE EQUIVALENTE  ANNEE - 1                           
             05 COMM-AC10-PERDEBEN1.                                            
                07 COMM-AC10-DSSEN1    PIC X(2).                                
                07 COMM-AC10-DAAEN1    PIC X(2).                                
                07 COMM-AC10-DMMEN1    PIC X(2).                                
                07 COMM-AC10-DJJEN1    PIC X(2).                                
             05    COMM-AC10-DLCEN1    PIC X(3).                                
      *  DATE DE DEBUT DE PERIODE DE CONSULTATION MEME JOUR ANNEE-1             
             05 COMM-AC10-PERDEBJN1.                                            
                07 COMM-AC10-DSSJN1    PIC X(2).                                
                07 COMM-AC10-DAAJN1    PIC X(2).                                
                07 COMM-AC10-DMMJN1    PIC X(2).                                
                07 COMM-AC10-DJJJN1    PIC X(2).                                
             05    COMM-AC10-DLCJN1    PIC X(3).                                
      *  DATE DE FIN DE LA PERIODE DE CONSULTATION                              
             05 COMM-AC10-PERFIN.                                               
                07 COMM-AC10-FSS       PIC X(2).                                
                07 COMM-AC10-FAA       PIC X(2).                                
                07 COMM-AC10-FMM       PIC X(2).                                
                07 COMM-AC10-FJJ       PIC X(2).                                
             05    COMM-AC10-FLC       PIC X(3).                                
      *  DATE DE FIN PERIODE EQUIVALENTE  ANNEE - 1                             
             05 COMM-AC10-PERFINEN1.                                            
                07 COMM-AC10-FSSEN1    PIC X(2).                                
                07 COMM-AC10-FAAEN1    PIC X(2).                                
                07 COMM-AC10-FMMEN1    PIC X(2).                                
                07 COMM-AC10-FJJEN1    PIC X(2).                                
             05    COMM-AC10-FLCEN1    PIC X(3).                                
      *  DATE DE FIN DE PERIODE DE CONSULTATION MEME JOUR ANNEE-1               
             05 COMM-AC10-PERFINJN1.                                            
                07 COMM-AC10-FSSJN1    PIC X(2).                                
                07 COMM-AC10-FAAJN1    PIC X(2).                                
                07 COMM-AC10-FMMJN1    PIC X(2).                                
                07 COMM-AC10-FJJJN1    PIC X(2).                                
             05    COMM-AC10-FLCJN1    PIC X(3).                                
      *  DATE DE DEBUT DE SEMAINE DE LA PERIODE DE CONSULTATION                 
             05 COMM-AC10-PERDEBSEM.                                            
                07 COMM-AC10-SSS       PIC X(2).                                
                07 COMM-AC10-SAA       PIC X(2).                                
                07 COMM-AC10-SMM       PIC X(2).                                
                07 COMM-AC10-SJJ       PIC X(2).                                
             05    COMM-AC10-SLC       PIC X(3).                                
      *  DATE DE DEBUT DE SEMAINE DE LA PERIODE EQUIVALENTE ANNEE-1             
      *      05 COMM-AC10-PERDEBSEMEN1.                                         
      *         07 COMM-AC10-SSSEN1    PIC X(2).                                
      *         07 COMM-AC10-SAAEN1    PIC X(2).                                
      *         07 COMM-AC10-SMMEN1    PIC X(2).                                
      *         07 COMM-AC10-SJJEN1    PIC X(2).                                
      *      05    COMM-AC10-SLCEN1    PIC X(2).                                
      *  DATE DE DEBUT DE SEMAINE DE LA PERIODE MEME JOUR ANNEE-1               
      *      05 COMM-AC10-PERDEBSEMJN1.                                         
      *         07 COMM-AC10-SSSJN1    PIC X(2).                                
      *         07 COMM-AC10-SAAJN1    PIC X(2).                                
      *         07 COMM-AC10-SMMJN1    PIC X(2).                                
      *         07 COMM-AC10-SJJJN1    PIC X(2).                                
      *      05    COMM-AC10-SLCJN1    PIC X(2).                                
      *  DATE DE DEBUT DE MOIS DE LA PERIODE DE CONSULTATION                    
             05 COMM-AC10-PERDEBMOIS.                                           
                07 COMM-AC10-MSS       PIC X(2).                                
                07 COMM-AC10-MAA       PIC X(2).                                
                07 COMM-AC10-MMM       PIC X(2).                                
                07 COMM-AC10-MJJ       PIC X(2).                                
             05    COMM-AC10-MLC       PIC X(3).                                
      *  DATE DE DEBUT DE MOIS DE LA PERIODE EQUIVALENTE ANNEE-1                
      *      05 COMM-AC10-PERDEBMOISEN1.                                        
      *         07 COMM-AC10-MSSEN1    PIC X(2).                                
      *         07 COMM-AC10-MAAEN1    PIC X(2).                                
      *         07 COMM-AC10-MMMEN1    PIC X(2).                                
      *         07 COMM-AC10-MJJEN1    PIC X(2).                                
      *      05    COMM-AC10-MLCEN1    PIC X(2).                                
      *  DATE DE DEBUT DE MOIS DE LA PERIODE MEME JOUR ANNEE-1                  
      *      05 COMM-AC10-PERDEBMOISJN1.                                        
      *         07 COMM-AC10-MSSJN1    PIC X(2).                                
      *         07 COMM-AC10-MAAJN1    PIC X(2).                                
      *         07 COMM-AC10-MMMJN1    PIC X(2).                                
      *         07 COMM-AC10-MJJJN1    PIC X(2).                                
      *      05    COMM-AC10-MLCJN1    PIC X(2).                                
      *  DATE DE LIMITE DE CONSULTATION  = DATE DU JOUR - 60 JOURS              
             05 COMM-AC10-PER-J-X.                                              
                07 COMM-AC10-SSJX      PIC X(2).                                
                07 COMM-AC10-AAJX      PIC X(2).                                
                07 COMM-AC10-MMJX      PIC X(2).                                
                07 COMM-AC10-JJJX      PIC X(2).                                
      * NOM DU PROGRAMME D'ORIGINE                                              
             05 COMM-AC10-PROG         PIC X(05).                               
      * CA A SURFACE TOTALE = T / EGALE = E                                     
             05 COMM-AC10-SURFACE      PIC X(01).                               
      * EVOLUTION CA A DATE EQUIVALENTE = E / MEME-JOUR =J                      
             05 COMM-AC10-EVOL         PIC X(01).                               
      * DETAIL MAGASIN DE SOCIETE A CONSULTER A PARTIR DU TAC11                 
             05 COMM-AC10-NSOC         PIC X(03).                               
             05 COMM-AC10-LSOC         PIC X(20).                               
      * ORDRE VISU CA MAGASINS/ SOCIETE  C = CROIS / O = N� ORDRE               
             05 COMM-AC10-CROIS        PIC X(01).                               
      * UNITE DE VISUALISATION DES CA EN KILO FRANC OU FRANC                    
             05 COMM-AC10-KILO         PIC X(11).                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05 COMM-AC10-DKILO        PIC S9(4) COMP.                          
      *--                                                                       
             05 COMM-AC10-DKILO        PIC S9(4) COMP-5.                        
      *}                                                                        
      * UNITE DE VISUALISATION DES NBRES EN CENTAINES OU UNITES                 
             05 COMM-AC10-UNIT         PIC X(09).                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05 COMM-AC10-DUNIT        PIC S9(4) COMP.                          
      *--                                                                       
             05 COMM-AC10-DUNIT        PIC S9(4) COMP-5.                        
      *}                                                                        
             05 COMM-AC10-FILLER         PIC X(3783).                           
      *****************************************************************         
      *   DONNEE CONSULTATION PAR SOCIETE  POUR LE GROUPE                       
      *   DONNEE CONSULTATION POUR UNE SOCIETE                                  
      *****************************************************************         
             05 COMM-AC11-APPLI          REDEFINES COMM-AC10-FILLER.            
                07 COMM-AC11-TRAIT        PIC X(1).                             
      *  CA DE CHAQUE SOCIETE                                                   
                07 COMM-AC11-T-LIG.                                             
                   09 COMM-AC11-TMNSOC.                                         
                      11 COMM-AC11-MNSOC1       PIC X(3).                       
                      11 COMM-AC11-MNSOC2       PIC X(3).                       
                      11 COMM-AC11-MNSOC3       PIC X(3).                       
                      11 COMM-AC11-MNSOC4       PIC X(3).                       
                      11 COMM-AC11-MNSOC5       PIC X(3).                       
                      11 COMM-AC11-MNSOC6       PIC X(3).                       
                      11 COMM-AC11-MNSOC7       PIC X(3).                       
                   09 COMM-AC11-T-MNSOC REDEFINES COMM-AC11-TMNSOC.             
                      11 COMM-AC11-MNSOC        PIC X(3) OCCURS 7.              
                   09 COMM-AC11-F-MNSOC.                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *               11 COMM-AC11-FMNSOC  PIC S9(4) COMP OCCURS 7.             
      *--                                                                       
                      11 COMM-AC11-FMNSOC  PIC S9(4) COMP-5 OCCURS 7.           
      *}                                                                        
                   09 COMM-AC11-TLIG            OCCURS 07.                      
                      11 COMM-AC11-MSELECT      PIC X(1).                       
08/00                 11 COMM-AC11-MCCA         PIC S9(7).                      
                      11 COMM-AC11-MCTR         PIC 9(6).                       
                      11 COMM-AC11-MCENT        PIC 9(6).                       
08/00                 11 COMM-AC11-MICA         PIC S9(7).                      
                      11 COMM-AC11-MITR         PIC 9(6).                       
                      11 COMM-AC11-MIENT        PIC 9(6).                       
08/00                 11 COMM-AC11-MTCA         PIC S9(7).                      
                      11 COMM-AC11-MTTR         PIC 9(6).                       
                      11 COMM-AC11-MTENT        PIC 9(6).                       
      *  TOTAUX CA GROUPE                                                       
                07 COMM-AC11-TOT.                                               
                   09  COMM-AC11-TNSOC        PIC X(3).                         
08/00              09  COMM-AC11-TCCA         PIC S9(7).                        
                   09  COMM-AC11-TCTR         PIC 9(6).                         
                   09  COMM-AC11-TCENT        PIC 9(6).                         
08/00              09  COMM-AC11-TICA         PIC S9(7).                        
                   09  COMM-AC11-TITR         PIC 9(6).                         
                   09  COMM-AC11-TIENT        PIC 9(6).                         
08/00              09  COMM-AC11-TTCA         PIC S9(7).                        
                   09  COMM-AC11-TTTR         PIC 9(6).                         
                   09  COMM-AC11-TTENT        PIC 9(6).                         
                07 COMM-AC11-FILLER             PIC X(3130).                    
      *                                                                         
      *---------------------------------------------------------------          
      * DONNEE EVOLUTION SOCIETE                                                
      *---------------------------------------------------------------          
             07 COMM-AC12-APPLI          REDEFINES COMM-AC11-FILLER.            
                   09 COMM-AC12-TLIG            OCCURS 07.                      
                      11 COMM-AC12-MCCA         PIC 9(7).                       
                      11 COMM-AC12-MCCAE        PIC ---9,9.                     
                      11 COMM-AC12-ZMCCAE       PIC X(06).                      
                      11 COMM-AC12-MCTR         PIC 9(6).                       
                      11 COMM-AC12-MCTRE        PIC ---9,9.                     
                      11 COMM-AC12-ZMCTRE       PIC X(6).                       
                      11 COMM-AC12-MCENT        PIC 9(6).                       
                      11 COMM-AC12-MCENTE       PIC ---9,9.                     
                      11 COMM-AC12-ZMCENTE      PIC X(6).                       
                      11 COMM-AC12-MICA         PIC 9(7).                       
                      11 COMM-AC12-MICAE        PIC ---9,9.                     
                      11 COMM-AC12-ZMICAE       PIC X(6).                       
                      11 COMM-AC12-MITR         PIC 9(6).                       
                      11 COMM-AC12-MITRE        PIC ---9,9.                     
                      11 COMM-AC12-ZMITRE       PIC X(6).                       
                      11 COMM-AC12-MIENT        PIC 9(6).                       
                      11 COMM-AC12-MIENTE       PIC ---9,9.                     
                      11 COMM-AC12-ZMIENTE      PIC X(6).                       
                      11 COMM-AC12-MTCA         PIC 9(7).                       
                      11 COMM-AC12-MTCAE        PIC ---9,9.                     
                      11 COMM-AC12-ZMTCAE       PIC X(6).                       
                      11 COMM-AC12-MTTR         PIC 9(6).                       
                      11 COMM-AC12-MTTRE        PIC ---9,9.                     
                      11 COMM-AC12-ZMTTRE       PIC X(6).                       
                      11 COMM-AC12-MTENT        PIC 9(6).                       
                      11 COMM-AC12-MTENTE       PIC ---9,9.                     
                      11 COMM-AC12-ZMTENTE      PIC X(6).                       
      *  TOTAUX CA N-1 DU GROUPE                                                
                   09 COMM-AC12-TOT.                                            
                      11 COMM-AC12-TCCA         PIC 9(7).                       
                      11 COMM-AC12-TCCAE        PIC ---9,9.                     
                      11 COMM-AC12-ZTCCAE       PIC X(6).                       
                      11 COMM-AC12-TCTR         PIC 9(6).                       
                      11 COMM-AC12-TCTRE        PIC ---9,9.                     
                      11 COMM-AC12-ZTCTRE       PIC X(6).                       
                      11 COMM-AC12-TCENT        PIC 9(6).                       
                      11 COMM-AC12-TCENTE       PIC ---9,9.                     
                      11 COMM-AC12-ZTCENTE      PIC X(6).                       
                      11 COMM-AC12-TICA         PIC 9(7).                       
                      11 COMM-AC12-TICAE        PIC ---9,9.                     
                      11 COMM-AC12-ZTICAE       PIC X(6).                       
                      11 COMM-AC12-TITR         PIC 9(6).                       
                      11 COMM-AC12-TITRE        PIC ---9,9.                     
                      11 COMM-AC12-ZTITRE       PIC X(6).                       
                      11 COMM-AC12-TIENT        PIC 9(6).                       
                      11 COMM-AC12-TIENTE       PIC ---9,9.                     
                      11 COMM-AC12-ZTIENTE      PIC X(6).                       
                      11 COMM-AC12-TTCA         PIC 9(7).                       
                      11 COMM-AC12-TTCAE        PIC ---9,9.                     
                      11 COMM-AC12-ZTTCAE       PIC X(6).                       
                      11 COMM-AC12-TTTR         PIC 9(6).                       
                      11 COMM-AC12-TTTRE        PIC ---9,9.                     
                      11 COMM-AC12-ZTTTRE       PIC X(6).                       
                      11 COMM-AC12-TTENT        PIC 9(6).                       
                      11 COMM-AC12-TTENTE       PIC ---9,9.                     
                      11 COMM-AC12-ZTTENTE      PIC X(6).                       
                   09 COMM-AC12-FILLER             PIC X(1810).                 
      *---------------------------------------------------------------          
      * DONNEE CONSULTATION MAGASIN MGI D'UNE SOCIETE                *          
      *---------------------------------------------------------------          
             05 COMM-AC13-APPLI          REDEFINES COMM-AC10-FILLER.            
                07 COMM-AC13-NB-PAGE            PIC 99.                         
                07 COMM-AC13-NO-PAGE            PIC 99.                         
                07 COMM-AC13-NB-MAG             PIC 999.                        
                07 COMM-AC13-TRAIT              PIC X.                          
                07 COMM-AC13-T-LIG.                                             
                   09 COMM-AC13-TLIG            OCCURS 10.                      
                      11 COMM-AC13-CMAG        PIC X(3).                        
                      11 COMM-AC13-LMAG        PIC X(10).                       
08/00                 11 COMM-AC13-CATLM       PIC S9(6).                       
  "                   11 COMM-AC13-CAELA       PIC S9(6).                       
  "                   11 COMM-AC13-CADAC       PIC S9(6).                       
  "                   11 COMM-AC13-CATOT       PIC S9(7).                       
08/00                 11 COMM-AC13-NBTOT       PIC S9(4).                       
                      11 COMM-AC13-TRDAR       PIC 9(4).                        
                      11 COMM-AC13-TRDAC       PIC 9(4).                        
                      11 COMM-AC13-NBENT       PIC 9(4).                        
                      11 COMM-AC13-TXCR        PIC 99V9.                        
                      11 COMM-AC13-PVM         PIC S9(4).                       
                      11 COMM-AC13-NBVD        PIC 9(4).                        
                      11 COMM-AC13-CAJOUR      PIC X(1).                        
FT0900                11 COMM-AC13-PBDELAI     PIC X(1).                        
FT0900          07 COMM-AC13-DELAI.                                             
FT0900             09 COMM-AC13-DELAI-HH        PIC 9(2).                       
FT0900             09 COMM-AC13-DELAI-MM        PIC 9(2).                       
FT0900          07 COMM-AC13-CTYPSOC            PIC X(3).                       
                07 COMM-AC13-FILLER             PIC X(2558).                    
      *                                                                         
      *---------------------------------------------------------------          
      * DONNEE CONSULTATION MAGASIN MGI D'UNE SOCIETE  EVOLUTIONS    *          
      *---------------------------------------------------------------          
             07 COMM-AC14-APPLI          REDEFINES COMM-AC13-FILLER.            
                09 COMM-AC14-NB-PAGE            PIC 99.                         
                09 COMM-AC14-NO-PAGE            PIC 99.                         
                09 COMM-AC14-NB-MAG             PIC 999.                        
                09 COMM-AC14-TRAIT              PIC X.                          
                09 COMM-AC14-T-LIG.                                             
                   11 COMM-AC14-TLIG            OCCURS 10.                      
                      13 COMM-AC14-CMAG        PIC X(3).                        
                      13 COMM-AC14-LMAG        PIC X(10).                       
                      13 COMM-AC14-CATLM       PIC X(6).                        
                      13 COMM-AC14-CAELA       PIC X(6).                        
                      13 COMM-AC14-CADAC       PIC X(6).                        
                      13 COMM-AC14-CATOT       PIC X(6).                        
                      13 COMM-AC14-TRDAR       PIC X(6).                        
                      13 COMM-AC14-TRDAC       PIC X(6).                        
                      13 COMM-AC14-NBENT       PIC X(6).                        
                      13 COMM-AC14-TXCR        PIC X(5).                        
                09 COMM-AC14-T-RNG.                                             
                   11 COMM-AC14-TRNG            OCCURS 160.                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *               13 COMM-AC14-RNG         PIC S9(4) COMP.                  
      *--                                                                       
                      13 COMM-AC14-RNG         PIC S9(4) COMP-5.                
      *}                                                                        
FT0900          09 COMM-AC14-FILLER             PIC X(1630).                    
      *---------------------------------------------------------------*         
      *     CODES ET MESSAGES DE RETOUR DE LINK (IAC01/IAC02/IAC03)   *         
      *---------------------------------------------------------------*         
      *                                                                         
             05 COMM-AC0X-DONNEE         REDEFINES COMM-AC10-FILLER.            
                07 COMM-AC0X-RC-IAC0X    PIC X(04).                             
                07 COMM-AC0X-MESSAGE     PIC X(50).                             
                07 COMM-AC0X-QUANT-D     PIC 9(05).                             
                07 COMM-AC0X-QUANT-F     PIC 9(05).                             
      *            DATES DE LA SEMAINE                                          
                07 COMM-AC0X-JOURS-SEMAINE.                                     
                   09 COMM-AC0X-J        PIC X(08) OCCURS 7.                    
                07 COMM-AC0X-NUMSEM      PIC 9(02).                             
      *            DATES EGALES (ANNEE - 1)                                     
                07 COMM-W-AC0X-JN1.                                             
                   09 COMM-AC0X-JN1      PIC X(08) OCCURS 7.                    
      *            DATES EQUIVALENTES (ANNEE - 1)                               
                07 COMM-W-AC0X-EN1.                                             
                   09 COMM-AC0X-EN1      PIC X(08) OCCURS 7.                    
                07 COMM-AC0X-HEUR        PIC X(09).                             
                07 COMM-AC0X-FILLER      PIC X(3171).                           
      *                                                                         
      *****************************************************************         
      *   DONNEE DE DATE DE SAISIE POUR LES RESULTATS                           
      *   SAISIE SOCIETE / MGC OU MGI                                           
      *****************************************************************         
             03 COMM-AC20-APPLI          REDEFINES COMM-AC00-FILLER.            
      *  DATE DE SAISIE DES RESULTATS                                           
             05 COMM-AC20-SAIS.                                                 
                07 COMM-AC20-SS       PIC X(2).                                 
                07 COMM-AC20-AA       PIC X(2).                                 
                07 COMM-AC20-MM       PIC X(2).                                 
                07 COMM-AC20-JJ       PIC X(2).                                 
             05    COMM-AC20-LC       PIC X(3).                                 
      *  DATE CORRESPONDANTE A L'ANNEE N-1                                      
             05 COMM-AC20-SAIS-N-1.                                             
                07 COMM-AC20-SSN1     PIC X(2).                                 
                07 COMM-AC20-AAN1     PIC X(2).                                 
                07 COMM-AC20-MMN1     PIC X(2).                                 
                07 COMM-AC20-JJN1     PIC X(2).                                 
             05    COMM-AC20-LCN1     PIC X(3).                                 
      *  DATE CORRESPONDANTE A L'ANNEE J-1                                      
             05 COMM-AC20-SAIS-J-1.                                             
                07 COMM-AC20-SSJ1     PIC X(2).                                 
                07 COMM-AC20-AAJ1     PIC X(2).                                 
                07 COMM-AC20-MMJ1     PIC X(2).                                 
                07 COMM-AC20-JJJ1     PIC X(2).                                 
      *  DATE CORRESPONDANTE DATE DU JOUR - 7 JOURS                             
             05 COMM-AC20-SAIS-J-7.                                             
                07 COMM-AC20-SSJ7     PIC X(2).                                 
                07 COMM-AC20-AAJ7     PIC X(2).                                 
                07 COMM-AC20-MMJ7     PIC X(2).                                 
                07 COMM-AC20-JJJ7     PIC X(2).                                 
             05 COMM-AC20-FILLER         PIC X(3539).                           
      *****************************************************************         
      *   DONNEE DES CA ... SAISIE POUR LES RESULTATS  MAGASIN                  
      *   SAISIE GROUPE SOCIETE OU MAGASIN                                      
      *****************************************************************         
             05 COMM-AC21-APPLI          REDEFINES COMM-AC20-FILLER.            
08/00           07 COMM-AC21-CATLM       PIC S9(7).                             
  "             07 COMM-AC21-CATLMN1     PIC S9(7).                             
  "             07 COMM-AC21-CAELA       PIC S9(7).                             
  "             07 COMM-AC21-CAELAN1     PIC S9(7).                             
  "             07 COMM-AC21-CADAC       PIC S9(6).                             
  "             07 COMM-AC21-CADACN1     PIC S9(6).                             
  "             07 COMM-AC21-CATOT       PIC S9(7).                             
  "             07 COMM-AC21-CATOTN1     PIC S9(7).                             
  "             07 COMM-AC21-NBTLM       PIC S9(4).                             
  "             07 COMM-AC21-NBTLMN1     PIC S9(4).                             
  "             07 COMM-AC21-NBELA       PIC S9(4).                             
  "             07 COMM-AC21-NBELAN1     PIC S9(4).                             
  "             07 COMM-AC21-NBDAC       PIC S9(4).                             
  "             07 COMM-AC21-NBDACN1     PIC S9(4).                             
  "             07 COMM-AC21-NBTOT       PIC S9(4).                             
08/00           07 COMM-AC21-NBTOTN1     PIC S9(4).                             
                07 COMM-AC21-TRDAR       PIC 9(4).                              
                07 COMM-AC21-TRDARN1     PIC 9(4).                              
                07 COMM-AC21-TRDAC       PIC 9(4).                              
                07 COMM-AC21-TRDACN1     PIC 9(4).                              
                07 COMM-AC21-TRTOT       PIC 9(4).                              
                07 COMM-AC21-TRTOTN1     PIC 9(4).                              
                07 COMM-AC21-NBENT       PIC 9(4).                              
                07 COMM-AC21-NBENTN1     PIC 9(4).                              
                07 COMM-AC21-TXCR        PIC 999V9.                             
                07 COMM-AC21-TXCRN1      PIC 999V9.                             
                07 COMM-AC21-PVM         PIC 9(4).                              
                07 COMM-AC21-PVMN1       PIC 9(4).                              
                07 COMM-AC21-NBVD        PIC 9(3)V9.                            
                07 COMM-AC21-NBVDN1      PIC 9(3)V9.                            
      *   ZONE AFFICHAGE DES EVOLUTIONS CA JOUR ET JOUR ANNEE N-1               
                07 COMM-AC21-ECATLM      PIC ---9,99.                           
                07 COMM-AC21-ZECATLM     PIC XXXXXXX.                           
                07 COMM-AC21-ECAELA      PIC ---9,99.                           
                07 COMM-AC21-ZECAELA     PIC XXXXXXX.                           
                07 COMM-AC21-ECADAC      PIC ---9,99.                           
                07 COMM-AC21-ZECADAC     PIC XXXXXXX.                           
                07 COMM-AC21-ECATOT      PIC ---9,99.                           
                07 COMM-AC21-ZECATOT     PIC XXXXXXX.                           
                07 COMM-AC21-ENBENT      PIC ---9,9.                            
                07 COMM-AC21-ZENBENT     PIC XXXXXX.                            
                07 COMM-AC21-ETXCR       PIC --9,9.                             
                07 COMM-AC21-ZETXCR      PIC XXXXX.                             
      *   EXISTANT = 1 SINON 0 POUR ENREG CA JOUR ET JOUR ANNEE N-1             
                07 COMM-AC21-CAJOUR      PIC X.                                 
                07 COMM-AC21-CAJOURN1    PIC X.                                 
AD0207          07 COMM-AC21-MAG-EN-ANO  PIC X.                                 
AD0207          07 COMM-AC21-MAG-SAISI   PIC X.                                 
AD0207          07 COMM-AC21-FILLER      PIC X(3305).                           
AD0207*         07 COMM-AC21-FILLER      PIC X(3307).                           
      *---------------------------------------------------------------          
      * DONNEE SAISIE SOCIETE                                        *          
      *---------------------------------------------------------------          
             05 COMM-AC22-APPLI          REDEFINES COMM-AC20-FILLER.            
                07 COMM-AC22-NB-PAGE            PIC 99.                         
                07 COMM-AC22-NO-PAGE            PIC 99.                         
                07 COMM-AC22-NB-MAG             PIC 999.                        
                07 COMM-AC22-T-LIG.                                             
                   09 COMM-AC22-TMAG            OCCURS 100.                     
                      11 COMM-AC22-TCMAG        PIC X(3).                       
                      11 COMM-AC22-TLMAG        PIC X(10).                      
                      11 COMM-AC22-TCTYPMAG     PIC X(3).                       
                      11 COMM-AC22-TCUMCA       PIC X(1).                       
                      11 COMM-AC22-TDOUV        PIC X(8).                       
                   09 COMM-AC22-TLIG            OCCURS 10.                      
                      11 COMM-AC22-MCMAG        PIC X(3).                       
                      11 COMM-AC22-MLMAG        PIC X(09).                      
                      11 COMM-AC22-MCATLM       PIC 9(7).                       
                      11 COMM-AC22-MCAELA       PIC 9(7).                       
                      11 COMM-AC22-MCADAC       PIC 9(6).                       
                      11 COMM-AC22-MCATOT       PIC 9(7).                       
                      11 COMM-AC22-MNBTLM       PIC 9(4).                       
                      11 COMM-AC22-MNBELA       PIC 9(4).                       
                      11 COMM-AC22-MNBDAC       PIC 9(4).                       
                      11 COMM-AC22-MTRDAR       PIC 9(4).                       
                      11 COMM-AC22-MTRDAC       PIC 9(4).                       
                      11 COMM-AC22-MNBENT       PIC 9(4).                       
                      11 COMM-AC22-MNBVD        PIC 9(3)V9.                     
                      11 COMM-AC22-MCAJOUR      PIC X(1).                       
AD0207                11 COMM-AC22-MAG-EN-ANO   PIC X(1).                       
                07 COMM-AC22-CMAG        PIC X(3).                              
                07 COMM-AC22-CTYPMAG     PIC X(3).                              
                07 COMM-AC22-CUMCA       PIC X(1).                              
                07 COMM-AC22-DOUV        PIC X(8).                              
                07 COMM-AC22-LMAG        PIC X(09).                             
                07 COMM-AC22-CATLM       PIC 9(7).                              
                07 COMM-AC22-CAELA       PIC 9(7).                              
                07 COMM-AC22-CADAC       PIC 9(6).                              
                07 COMM-AC22-CATOT       PIC 9(7).                              
                07 COMM-AC22-NBTLM       PIC 9(4).                              
                07 COMM-AC22-NBELA       PIC 9(4).                              
                07 COMM-AC22-NBDAC       PIC 9(4).                              
                07 COMM-AC22-NBTOT       PIC 9(4).                              
                07 COMM-AC22-TRDAR       PIC 9(4).                              
                07 COMM-AC22-TRDAC       PIC 9(4).                              
                07 COMM-AC22-TRTOT       PIC 9(4).                              
                07 COMM-AC22-NBENT       PIC 9(4).                              
                07 COMM-AC22-TXCR        PIC 99V9.                              
                07 COMM-AC22-PVM         PIC 9(4).                              
                07 COMM-AC22-NBVD        PIC 9(3)V9.                            
                07 COMM-AC22-CAJOUR      PIC X(1).                              
                07 COMM-AC22-RESP        PIC 9.                                 
AD0207          07 COMM-AC22-MAG-SAISI   PIC X.                                 
AD0207*         07 COMM-AC22-FILLER             PIC X(0237).                    
AD0207          07 COMM-AC22-FILLER      PIC X(0206).                           
AD0207          07 COMM-AC23 REDEFINES   COMM-AC22-FILLER.                      
                   09 COMM-AC23-ORIGINE  PIC X(05).                             
                   09 COMM-AC23-NSOC     PIC X(03).                             
                   09 COMM-AC23-NLIEU    PIC X(03).                             
                   09 COMM-AC23-LLIEU    PIC X(20).                             
                   09 COMM-AC23-JOUR     PIC X(03).                             
                   09 COMM-AC23-DVENTE   PIC X(08).                             
                   09 COMM-AC23-QTOT     PIC 9(06) COMP-3.                      
                   09 COMM-AC23-MESTIM   PIC 9(06) COMP-3.                      
                   09 COMM-AC23-MNBQ     PIC 9(02).                             
                   09 COMM-AC23-MQENTREP PIC 9(03) COMP-3.                      
                                                                                
