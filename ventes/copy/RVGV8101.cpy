      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGV8101                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV8101                         
      **********************************************************                
       01  RVGV8101.                                                            
           02  GV81-CVENDEUR                                                    
               PIC X(0006).                                                     
           02  GV81-LVENDEUR                                                    
               PIC X(0015).                                                     
           02  GV81-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GV81-NMAG                                                        
               PIC X(0003).                                                     
           02  GV81-NMATSIGA                                                    
               PIC X(0007).                                                     
           02  GV81-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GV81-WPRIME                                                      
               PIC X(0001).                                                     
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGV8101                                  
      **********************************************************                
       01  RVGV8101-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV81-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV81-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV81-LVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV81-LVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV81-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV81-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV81-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV81-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV81-NMATSIGA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV81-NMATSIGA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV81-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV81-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV81-WPRIME-F                                                    
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GV81-WPRIME-F                                                    
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
