      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVST3000                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVST3000                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       01  RVST3000.                                                    00000090
           10 ST30-NSOCIETE        PIC X(3).                            00000100
           10 ST30-NLIEU           PIC X(3).                            00000110
           10 ST30-DSTAT           PIC X(8).                            00000120
           10 ST30-NCODIC          PIC X(7).                            00000130
           10 ST30-QT              PIC S9(05)V USAGE COMP-3.            00000140
           10 ST30-QTL             PIC S9(05)V USAGE COMP-3.            00000150
           10 ST30-QTACCL          PIC S9(05)V USAGE COMP-3.            00000160
           10 ST30-MTHT            PIC S9(09)V9(2) USAGE COMP-3.        00000170
           10 ST30-MTHTACC         PIC S9(09)V9(2) USAGE COMP-3.        00000180
           10 ST30-MTPRMP          PIC S9(09)V9(2) USAGE COMP-3.        00000190
           10 ST30-MTPRMPACC       PIC S9(09)V9(2) USAGE COMP-3.        00000200
           10 ST30-DSYST           PIC S9(13)V USAGE COMP-3.            00000210
      *                                                                 00000220
      *---------------------------------------------------------        00000230
      *   LISTE DES FLAGS DE LA TABLE RVST3000                          00000240
      *---------------------------------------------------------        00000250
      *                                                                 00000260
       01  RVST3000-FLAGS.                                              00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ST30-NSOCIETE-F      PIC S9(4) COMP.                      00000280
      *--                                                                       
           10 ST30-NSOCIETE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ST30-NLIEU-F         PIC S9(4) COMP.                      00000290
      *--                                                                       
           10 ST30-NLIEU-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ST30-DSTAT-F         PIC S9(4) COMP.                      00000300
      *--                                                                       
           10 ST30-DSTAT-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ST30-NCODIC-F        PIC S9(4) COMP.                      00000310
      *--                                                                       
           10 ST30-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ST30-QT-F            PIC S9(4) COMP.                      00000320
      *--                                                                       
           10 ST30-QT-F            PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ST30-QTL-F           PIC S9(4) COMP.                      00000330
      *--                                                                       
           10 ST30-QTL-F           PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ST30-QTACCL-F        PIC S9(4) COMP.                      00000340
      *--                                                                       
           10 ST30-QTACCL-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ST30-MTHT-F          PIC S9(4) COMP.                      00000350
      *--                                                                       
           10 ST30-MTHT-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ST30-MTHTACC-F       PIC S9(4) COMP.                      00000360
      *--                                                                       
           10 ST30-MTHTACC-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ST30-MTPRMP-F        PIC S9(4) COMP.                      00000370
      *--                                                                       
           10 ST30-MTPRMP-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ST30-MTPRMPACC-F     PIC S9(4) COMP.                      00000380
      *--                                                                       
           10 ST30-MTPRMPACC-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ST30-DSYST-F         PIC S9(4) COMP.                      00000390
      *                                                                         
      *--                                                                       
           10 ST30-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
      *}                                                                        
