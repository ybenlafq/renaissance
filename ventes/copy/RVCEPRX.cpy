      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CEPRX FLAG PRIX DES FORMATS QUICK P    *        
      *----------------------------------------------------------------*        
       01  RVCEPRX.                                                             
           05  CEPRX-CTABLEG2    PIC X(15).                                     
           05  CEPRX-CTABLEG2-REDEF REDEFINES CEPRX-CTABLEG2.                   
               10  CEPRX-WPRIX           PIC X(01).                             
           05  CEPRX-WTABLEG     PIC X(80).                                     
           05  CEPRX-WTABLEG-REDEF  REDEFINES CEPRX-WTABLEG.                    
               10  CEPRX-WACTIF          PIC X(01).                             
               10  CEPRX-LIBELLE         PIC X(20).                             
               10  CEPRX-WDATA           PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCEPRX-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CEPRX-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CEPRX-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CEPRX-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CEPRX-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
