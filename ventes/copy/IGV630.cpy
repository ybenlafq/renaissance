      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGV630 AU 28/06/1994  *        
      *                                                                *        
      *          CRITERES DE TRI  07,08,BI,A,                          *        
      *                           15,03,BI,A,                          *        
      *                           18,03,BI,A,                          *        
      *                           21,07,BI,A,                          *        
      *                           28,08,BI,A,                          *        
      *                           36,07,BI,A,                          *        
      *                           43,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGV630.                                                        
            05 NOMETAT-IGV630           PIC X(6) VALUE 'IGV630'.                
            05 RUPTURES-IGV630.                                                 
           10 IGV630-DDELIV             PIC X(08).                      007  008
           10 IGV630-NSOCIETE           PIC X(03).                      015  003
           10 IGV630-NLIEU              PIC X(03).                      018  003
           10 IGV630-NVENTE             PIC X(07).                      021  007
           10 IGV630-DDELIV1            PIC X(08).                      028  008
           10 IGV630-NCODIC             PIC X(07).                      036  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGV630-SEQUENCE           PIC S9(04) COMP.                043  002
      *--                                                                       
           10 IGV630-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGV630.                                                   
           10 IGV630-NCODICA1           PIC X(07).                      045  007
           10 IGV630-NCODICA2           PIC X(07).                      052  007
           10 IGV630-NCODICA3           PIC X(07).                      059  007
           10 IGV630-NETNAME            PIC X(08).                      066  008
           10 IGV630-NLIEUORIG          PIC X(03).                      074  003
           10 IGV630-DDELIVA1           PIC X(08).                      077  008
           10 IGV630-DDELIVA2           PIC X(08).                      085  008
           10 IGV630-DDELIVA3           PIC X(08).                      093  008
            05 FILLER                      PIC X(412).                          
                                                                                
