      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      *                                                               * 00000020
      *      TS SPECIFIQUE DU PROGRAMME TGV26                         * 00000030
      *      1 ITEM PAR PAGE                                          * 00000050
      *      07 LIGNES PAR ITEM CONTENANT LES PRE-RESERVATIONS        * 00000051
      *                                                               * 00000052
      *      TR : GV00  GESTION DES VENTES                            * 00000060
      *      PG : TGV26 CONSULTATION D UNE VENTE RAN                  * 00000070
      *                                                               * 00000080
      *      NOM: 'GV26' + EIBTRMID                                   * 00000090
      *                                                               * 00000092
      ***************************************************************** 00000093
      *                                                               * 00000094
       01  TS-GV26.                                                     00000095
      ********************************** DONNEES                        00000098
           05 TS-GV26-RECORD.                                           00000099
              10 TS-GV26-LIGNE           OCCURS 7.                      00000100
                 15 TS-GV26-TYPEV        PIC    X(02).                  00000110
                 15 TS-GV26-NLIGNE       PIC    9(02).                  00000110
                 15 TS-GV26-CODIC        PIC    X(07).                  00000110
      *--        CODIC D AFFICHAGE SUR GV26                                     
                 15 TS-GV26-CODIC-AFF    PIC    X(38).                          
                 15 TS-GV26-CFAM         PIC    X(05).                  00000110
                 15 TS-GV26-CMARQ        PIC    X(05).                  00000110
                 15 TS-GV26-REF          PIC    X(20).                  00000110
                 15 TS-GV26-QTE          PIC S9(10)V USAGE COMP-3.      00000110
                 15 TS-GV26-PVUNIT       PIC S9(7)V9(2) USAGE COMP-3.   00000110
      *--        PRIX  D AFFICHAGE SUR GV26                                     
                 15 TS-GV26-PVUNIT-AFF   PIC S9(7)V9(2) USAGE COMP-3.           
                 15 TS-GV26-PVTOTAL      PIC S9(7)V9(2) USAGE COMP-3.   00000110
                 15 TS-GV26-DDELIV       PIC    X(08).                  00000110
                 15 TS-GV26-CMODDEL      PIC    X(08).                  00000110
                 15 TS-GV26-WEMPORTE     PIC    X(01).                  00000110
                 15 TS-GV26-DTOPE        PIC    X(08).                  00000110
                 15 TS-GV26-DANNUL-EXT   PIC    X(08).                          
                 15 TS-GV26-DATENC       PIC    X(08).                          
                                                                                
