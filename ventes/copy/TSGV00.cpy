      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      * TSGV00 = TS PAR SOCIETE, CLE = 'GV0' + 'DARTY'                *         
      *                                                               *         
      * DESCRIPTION DE LA TS CONTENANT LES INFORMATIONS DES           *         
      * TABLES OU SOUS-TABLES COMMUNES A TOUS LES MAGASINS            *         
      * TABLES OU SOUS-TABLES RENSEIGNES :                            *         
      *   1- CUTIL  : CODE UTILISATEUR                                *         
      *   2- GCPLT  : GARANTIES COMPLEMENTAIRES                       *         
      *   3- LIENT  : DEGRE DE LIBERTE                                *         
      *   4- LPLGE  : PLAGES DE DELIVRANCE                            *         
      *   5- MDLIV  : MODES DE DELIVRANCE                             *         
      *   6- NGCFC  : AIGUILLAGE DES TRANSACTIONS                     *         
      *   7- PLAFD  : QUANTITE PLAFONNEE                              *         
LGT   *   8- TPLPR  : ASSOCIATION PLAN PROFIL TOURNEE (SUPPRIMER LGT) *         
LGT2  *   9- TPROG  : PROFIL TOURNEE GENERAL          (SUPPRIMER LGT2)*         
      *  10- TXTVA  : TAUX DE TVA                                     *         
      * LONGUEUR TOTALE = 4000                                        *         
      *                                                               *         
      * DE02009 AUGMENTER LE TABLEAU DES MODES DE DéLIVRANCE          *         
      *****************************************************************         
      *                                                               *         
      * NOMBRE DE POSTES MAXIMUM POUR CHAQUE TABLE OU SOUS-TABLE      *         
      *(A METTRE A JOUR A CHAQUE CHANGEMENT DU NOMBRE D' OCCURS)      *         
      *                                                               *         
       01  IND-GCPLT                       PIC 999  VALUE 20.                   
       01  IND-LIENT                       PIC 999  VALUE 10.                   
       01  IND-LPLGE                       PIC 999  VALUE 20.                   
      *01  IND-MDLIV                       PIC 999  VALUE 30.                   
       01  IND-MDLIV                       PIC 999  VALUE 50.                   
       01  IND-NCGFC                       PIC 999  VALUE 10.                   
       01  IND-TXTVA                       PIC 999  VALUE 10.                   
       01  IND-GLOBALE                     PIC 999  VALUE ZEROES.               
      *                                                               *         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS-GV00-LONG                    PIC S9(4) COMP VALUE +4000.          
      *--                                                                       
       01  TS-GV00-LONG                    PIC S9(4) COMP-5 VALUE +4000.        
      *}                                                                        
      *                                                               *         
       01  TS-GV00-DONNEES.                                                     
      *                                                         +   8 C         
           05 TS-GV00-DCREATION            PIC X(08).                           
      *                                                         +   7 C         
           05 TS-GV00-CUTIL.                                                    
              10 TS-GV00-CUTIL-CUTIL       PIC X(06).                           
              10 TS-GV00-CUTIL-WFLAG       PIC X(01).                           
      *                                                 20*16 = + 320 C         
           05 TS-GV00-GCPLT.                                                    
              10 TS-GV00-GCPLT-POSTE       OCCURS 20.                           
                 15 TS-GV00-GCPLT-CGCPL    PIC X(05).                           
                 15 TS-GV00-GCPLT-CTAUXTVA PIC X(05).                           
                 15 TS-GV00-GCPLT-QDC      PIC 9(03).                           
                 15 TS-GV00-GCPLT-QDS      PIC 9(03).                           
      *                                                 10*11 = + 110 C         
           05 TS-GV00-LIENT.                                                    
              10 TS-GV00-LIENT-POSTE       OCCURS 10.                           
                 15 TS-GV00-LIENT-CTYPLIEN PIC X(05).                           
                 15 TS-GV00-LIENT-WDEGRELIB                                     
                                           PIC X(05).                           
                 15 TS-GV00-LIENT-WFLAG    PIC X(01).                           
      *                                                 20* 6 = + 120 C         
           05 TS-GV00-LPLGE.                                                    
              10 TS-GV00-LPLGE-POSTE       OCCURS 20.                           
                 15 TS-GV00-LPLGE-CPLAGE   PIC X(02).                           
                 15 TS-GV00-LPLGE-HLIMITE  PIC 9(04).                           
      *                                                 30*10 = + 300 C         
      *                                                 20*10 = + 200 C         
           05 TS-GV00-MDLIV.                                                    
      *       10 TS-GV00-MDLIV-POSTE       OCCURS 30.                           
              10 TS-GV00-MDLIV-POSTE       OCCURS 50.                           
                 15 TS-GV00-MDLIV-CMODDEL  PIC X(03).                           
                 15 TS-GV00-MDLIV-CPLAGE   PIC X(01).                           
                 15 TS-GV00-MDLIV-DMIN     PIC 9(02).                           
                 15 TS-GV00-MDLIV-DMAX     PIC 9(03).                           
                 15 TS-GV00-MDLIV-WFLAG    PIC X(01).                           
      *                                                 10*25 = + 250 C         
           05 TS-GV00-NCGFC.                                                    
              10 TS-GV00-NCGFC-POSTE       OCCURS 10.                           
                 15 TS-GV00-NCGFC-CPROG    PIC X(05).                           
                 15 TS-GV00-NCGFC-WFLAG    PIC X(01).                           
                 15 TS-GV00-NCGFC-QPVUNIT  PIC 9(03).                           
                 15 TS-GV00-NCGFC-COMMENT  PIC X(15).                           
                 15 TS-GV00-NCGFC-WFLAG2   PIC X(01).                           
      *                                                         +   3 C         
           05 TS-GV00-PLAFD.                                                    
              10 TS-GV00-PLAFD-QPLAFD      PIC S9(5) COMP-3 VALUE ZERO.         
      *    TPLPR                                                + 500 C         
           05 TS-GV00-FILLER               PIC X(500).                          
      *    TPROG                                        50* 7 = + 350 C         
           05 TS-GV00-FILLER2              PIC X(350).                          
      *                                                 10*19 = + 190 C         
           05 TS-GV00-TXTVA.                                                    
              10 TS-GV00-TXTVA-POSTE       OCCURS 10.                           
                 15 TS-GV00-TXTVA-CTAUXTVA PIC X(05).                           
                 15 TS-GV00-TXTVA-DEFFET   PIC 9(08).                           
                 15 TS-GV00-TXTVA-PNOUTAUX PIC S9(3)V99 COMP-3.                 
                 15 TS-GV00-TXTVA-PANCTAUX PIC S9(3)V99 COMP-3.                 
      *                                                 30*32 = + 960 C         
      *                                                 20*32 = + 640 C         
           05 TS-GV00-MDLIV-SUITE.                                              
      *       10 TS-GV00-MDLIV-WDONNEES    OCCURS 30.                           
              10 TS-GV00-MDLIV-WDONNEES    OCCURS 50.                           
                 15 TS-GV00-MDLIV-CTYPTRAIT PIC X(05).                          
                 15 TS-GV00-MDLIV-CORIGLIEU PIC X(05).                          
                 15 TS-GV00-MDLIV-GEST      PIC X(06).                          
                 15 TS-GV00-MDLIV-DEPLIV    PIC X(06).                          
                 15 TS-GV00-MDLIV-WEXP      PIC X(01).                          
                 15 TS-GV00-MDLIV-DEPLIVEXP PIC X(06).                          
                 15 TS-GV00-MDLIV-FILLER    PIC X(03).                          
      *                                                         + 882 C         
      *    05 FILLER                       PIC X(0882)     VALUE SPACES.        
           05 FILLER                       PIC X(0042)     VALUE SPACES.        
                                                                                
