      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:05 >
      
      ******************************************************************
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *
      ******************************************************************
      *                                                                *
      *  TITRE      : COMMAREA DU MODULE STANDARD MSL21                *
      *  LONGUEUR   : 2500                                             *
      *                                                                *
      ******************************************************************
      *
      *
       01  COMM-MSL21-APPLI.
           02 COMM-MSL21-CPGM             PIC X(10).
           02 COMM-MSL21-CETAT            PIC X(02).
           02 COMM-MSL21-CEMPL            PIC X(05).
           02 COMM-MSL21-QSTK             PIC S9(7) COMP-3.
           02 COMM-MSL21-QSTR             PIC S9(7) COMP-3.
           02 COMM-MSL21-NBCOL            PIC S9(3) COMP-3.
           02 COMM-MSL21-TABLE-COL.
               03 COMM-MSL21-COLONNE OCCURS 99 INDEXED COMM-MSL21-IC.
                  05 COMM-MSL21-NCOL             PIC 9(2).
                  05 COMM-MSL21-CRSL             PIC X(5).
                  05 COMM-MSL21-TQTE             PIC X(1).
                  05 COMM-MSL21-MSG              PIC X(7).
                  05 COMM-MSL21-QTC              PIC S9(7) COMP-3.
           02 COMM-MSL21-CODRET           PIC X(2).
           02 COMM-MSL21-ERREUR           PIC X(52).
           02 FILLER                      PIC X(538).
      
