      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EBA21   EBA21                                              00000020
      ***************************************************************** 00000030
       01   EBA21I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCTIERSI  PIC X(6).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCBAL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCBAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCBAF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCBAI  PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBAL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBAF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBAI     PIC X(6).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTIERSL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNTIERSF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNTIERSI  PIC X(10).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPBAL     COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MPBAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPBAF     PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MPBAI     PIC X(8).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFACTBAL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNFACTBAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNFACTBAF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNFACTBAI      PIC X(10).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEMISL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDEMISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEMISF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDEMISI   PIC X(10).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVENTEL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDVENTEF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDVENTEI  PIC X(10).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECEPRL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MDRECEPRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDRECEPRF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDRECEPRI      PIC X(10).                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECEPL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDRECEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRECEPF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDRECEPI  PIC X(10).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDANNULL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDANNULL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDANNULF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDANNULI  PIC X(10).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDICEMISL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MDICEMISL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDICEMISF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MDICEMISI      PIC X(10).                                 00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDICRECEPL     COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MDICRECEPL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDICRECEPF     PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDICRECEPI     PIC X(10).                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDICANNULL     COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MDICANNULL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDICANNULF     PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDICANNULI     PIC X(10).                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWEMISL   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MWEMISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWEMISF   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MWEMISI   PIC X.                                          00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWVENTEL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MWVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWVENTEF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MWVENTEI  PIC X.                                          00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWRECEPL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MWRECEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWRECEPF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MWRECEPI  PIC X.                                          00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWVALIDL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MWVALIDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWVALIDF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MWVALIDI  PIC X.                                          00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWANNULL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MWANNULL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWANNULF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MWANNULI  PIC X.                                          00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCRL   COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MNSOCRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOCRF   PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MNSOCRI   PIC X(3).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MNSOCI    PIC X(3).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEURL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MNLIEURL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEURF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MNLIEURI  PIC X(3).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MNLIEUI   PIC X(3).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MZONCMDI  PIC X(15).                                      00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MLIBERRI  PIC X(58).                                      00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MCODTRAI  PIC X(4).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MCICSI    PIC X(5).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MNETNAMI  PIC X(8).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MSCREENI  PIC X(4).                                       00001290
      ***************************************************************** 00001300
      * SDF: EBA21   EBA21                                              00001310
      ***************************************************************** 00001320
       01   EBA21O REDEFINES EBA21I.                                    00001330
           02 FILLER    PIC X(12).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MDATJOUA  PIC X.                                          00001360
           02 MDATJOUC  PIC X.                                          00001370
           02 MDATJOUP  PIC X.                                          00001380
           02 MDATJOUH  PIC X.                                          00001390
           02 MDATJOUV  PIC X.                                          00001400
           02 MDATJOUO  PIC X(10).                                      00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MTIMJOUA  PIC X.                                          00001430
           02 MTIMJOUC  PIC X.                                          00001440
           02 MTIMJOUP  PIC X.                                          00001450
           02 MTIMJOUH  PIC X.                                          00001460
           02 MTIMJOUV  PIC X.                                          00001470
           02 MTIMJOUO  PIC X(5).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MCTIERSA  PIC X.                                          00001500
           02 MCTIERSC  PIC X.                                          00001510
           02 MCTIERSP  PIC X.                                          00001520
           02 MCTIERSH  PIC X.                                          00001530
           02 MCTIERSV  PIC X.                                          00001540
           02 MCTIERSO  PIC X(6).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNSOCBAA  PIC X.                                          00001570
           02 MNSOCBAC  PIC X.                                          00001580
           02 MNSOCBAP  PIC X.                                          00001590
           02 MNSOCBAH  PIC X.                                          00001600
           02 MNSOCBAV  PIC X.                                          00001610
           02 MNSOCBAO  PIC X(3).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MNBAA     PIC X.                                          00001640
           02 MNBAC     PIC X.                                          00001650
           02 MNBAP     PIC X.                                          00001660
           02 MNBAH     PIC X.                                          00001670
           02 MNBAV     PIC X.                                          00001680
           02 MNBAO     PIC X(6).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MNTIERSA  PIC X.                                          00001710
           02 MNTIERSC  PIC X.                                          00001720
           02 MNTIERSP  PIC X.                                          00001730
           02 MNTIERSH  PIC X.                                          00001740
           02 MNTIERSV  PIC X.                                          00001750
           02 MNTIERSO  PIC X(10).                                      00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MPBAA     PIC X.                                          00001780
           02 MPBAC     PIC X.                                          00001790
           02 MPBAP     PIC X.                                          00001800
           02 MPBAH     PIC X.                                          00001810
           02 MPBAV     PIC X.                                          00001820
           02 MPBAO     PIC X(8).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MNFACTBAA      PIC X.                                     00001850
           02 MNFACTBAC PIC X.                                          00001860
           02 MNFACTBAP PIC X.                                          00001870
           02 MNFACTBAH PIC X.                                          00001880
           02 MNFACTBAV PIC X.                                          00001890
           02 MNFACTBAO      PIC X(10).                                 00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MDEMISA   PIC X.                                          00001920
           02 MDEMISC   PIC X.                                          00001930
           02 MDEMISP   PIC X.                                          00001940
           02 MDEMISH   PIC X.                                          00001950
           02 MDEMISV   PIC X.                                          00001960
           02 MDEMISO   PIC X(10).                                      00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MDVENTEA  PIC X.                                          00001990
           02 MDVENTEC  PIC X.                                          00002000
           02 MDVENTEP  PIC X.                                          00002010
           02 MDVENTEH  PIC X.                                          00002020
           02 MDVENTEV  PIC X.                                          00002030
           02 MDVENTEO  PIC X(10).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MDRECEPRA      PIC X.                                     00002060
           02 MDRECEPRC PIC X.                                          00002070
           02 MDRECEPRP PIC X.                                          00002080
           02 MDRECEPRH PIC X.                                          00002090
           02 MDRECEPRV PIC X.                                          00002100
           02 MDRECEPRO      PIC X(10).                                 00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MDRECEPA  PIC X.                                          00002130
           02 MDRECEPC  PIC X.                                          00002140
           02 MDRECEPP  PIC X.                                          00002150
           02 MDRECEPH  PIC X.                                          00002160
           02 MDRECEPV  PIC X.                                          00002170
           02 MDRECEPO  PIC X(10).                                      00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MDANNULA  PIC X.                                          00002200
           02 MDANNULC  PIC X.                                          00002210
           02 MDANNULP  PIC X.                                          00002220
           02 MDANNULH  PIC X.                                          00002230
           02 MDANNULV  PIC X.                                          00002240
           02 MDANNULO  PIC X(10).                                      00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MDICEMISA      PIC X.                                     00002270
           02 MDICEMISC PIC X.                                          00002280
           02 MDICEMISP PIC X.                                          00002290
           02 MDICEMISH PIC X.                                          00002300
           02 MDICEMISV PIC X.                                          00002310
           02 MDICEMISO      PIC X(10).                                 00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MDICRECEPA     PIC X.                                     00002340
           02 MDICRECEPC     PIC X.                                     00002350
           02 MDICRECEPP     PIC X.                                     00002360
           02 MDICRECEPH     PIC X.                                     00002370
           02 MDICRECEPV     PIC X.                                     00002380
           02 MDICRECEPO     PIC X(10).                                 00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MDICANNULA     PIC X.                                     00002410
           02 MDICANNULC     PIC X.                                     00002420
           02 MDICANNULP     PIC X.                                     00002430
           02 MDICANNULH     PIC X.                                     00002440
           02 MDICANNULV     PIC X.                                     00002450
           02 MDICANNULO     PIC X(10).                                 00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MWEMISA   PIC X.                                          00002480
           02 MWEMISC   PIC X.                                          00002490
           02 MWEMISP   PIC X.                                          00002500
           02 MWEMISH   PIC X.                                          00002510
           02 MWEMISV   PIC X.                                          00002520
           02 MWEMISO   PIC X.                                          00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MWVENTEA  PIC X.                                          00002550
           02 MWVENTEC  PIC X.                                          00002560
           02 MWVENTEP  PIC X.                                          00002570
           02 MWVENTEH  PIC X.                                          00002580
           02 MWVENTEV  PIC X.                                          00002590
           02 MWVENTEO  PIC X.                                          00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MWRECEPA  PIC X.                                          00002620
           02 MWRECEPC  PIC X.                                          00002630
           02 MWRECEPP  PIC X.                                          00002640
           02 MWRECEPH  PIC X.                                          00002650
           02 MWRECEPV  PIC X.                                          00002660
           02 MWRECEPO  PIC X.                                          00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MWVALIDA  PIC X.                                          00002690
           02 MWVALIDC  PIC X.                                          00002700
           02 MWVALIDP  PIC X.                                          00002710
           02 MWVALIDH  PIC X.                                          00002720
           02 MWVALIDV  PIC X.                                          00002730
           02 MWVALIDO  PIC X.                                          00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MWANNULA  PIC X.                                          00002760
           02 MWANNULC  PIC X.                                          00002770
           02 MWANNULP  PIC X.                                          00002780
           02 MWANNULH  PIC X.                                          00002790
           02 MWANNULV  PIC X.                                          00002800
           02 MWANNULO  PIC X.                                          00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MNSOCRA   PIC X.                                          00002830
           02 MNSOCRC   PIC X.                                          00002840
           02 MNSOCRP   PIC X.                                          00002850
           02 MNSOCRH   PIC X.                                          00002860
           02 MNSOCRV   PIC X.                                          00002870
           02 MNSOCRO   PIC X(3).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MNSOCA    PIC X.                                          00002900
           02 MNSOCC    PIC X.                                          00002910
           02 MNSOCP    PIC X.                                          00002920
           02 MNSOCH    PIC X.                                          00002930
           02 MNSOCV    PIC X.                                          00002940
           02 MNSOCO    PIC X(3).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNLIEURA  PIC X.                                          00002970
           02 MNLIEURC  PIC X.                                          00002980
           02 MNLIEURP  PIC X.                                          00002990
           02 MNLIEURH  PIC X.                                          00003000
           02 MNLIEURV  PIC X.                                          00003010
           02 MNLIEURO  PIC X(3).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MNLIEUA   PIC X.                                          00003040
           02 MNLIEUC   PIC X.                                          00003050
           02 MNLIEUP   PIC X.                                          00003060
           02 MNLIEUH   PIC X.                                          00003070
           02 MNLIEUV   PIC X.                                          00003080
           02 MNLIEUO   PIC X(3).                                       00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MZONCMDA  PIC X.                                          00003110
           02 MZONCMDC  PIC X.                                          00003120
           02 MZONCMDP  PIC X.                                          00003130
           02 MZONCMDH  PIC X.                                          00003140
           02 MZONCMDV  PIC X.                                          00003150
           02 MZONCMDO  PIC X(15).                                      00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MLIBERRA  PIC X.                                          00003180
           02 MLIBERRC  PIC X.                                          00003190
           02 MLIBERRP  PIC X.                                          00003200
           02 MLIBERRH  PIC X.                                          00003210
           02 MLIBERRV  PIC X.                                          00003220
           02 MLIBERRO  PIC X(58).                                      00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MCODTRAA  PIC X.                                          00003250
           02 MCODTRAC  PIC X.                                          00003260
           02 MCODTRAP  PIC X.                                          00003270
           02 MCODTRAH  PIC X.                                          00003280
           02 MCODTRAV  PIC X.                                          00003290
           02 MCODTRAO  PIC X(4).                                       00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MCICSA    PIC X.                                          00003320
           02 MCICSC    PIC X.                                          00003330
           02 MCICSP    PIC X.                                          00003340
           02 MCICSH    PIC X.                                          00003350
           02 MCICSV    PIC X.                                          00003360
           02 MCICSO    PIC X(5).                                       00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MNETNAMA  PIC X.                                          00003390
           02 MNETNAMC  PIC X.                                          00003400
           02 MNETNAMP  PIC X.                                          00003410
           02 MNETNAMH  PIC X.                                          00003420
           02 MNETNAMV  PIC X.                                          00003430
           02 MNETNAMO  PIC X(8).                                       00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MSCREENA  PIC X.                                          00003460
           02 MSCREENC  PIC X.                                          00003470
           02 MSCREENP  PIC X.                                          00003480
           02 MSCREENH  PIC X.                                          00003490
           02 MSCREENV  PIC X.                                          00003500
           02 MSCREENO  PIC X(4).                                       00003510
                                                                                
