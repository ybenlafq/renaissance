      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER D'EXTRACTION                       *        
      *                                                                *        
      *          CRITERES DE TRI  01,03,BI,A,  SOCIETE                 *        
      *                           04,03,BI,A,  NLIEU                   *        
      *                           07,03,BI,A,  CAISSE                  *        
      *                           10,04,BI,A,  TRANS                   *        
      *                           14,07,BI,A,  CODIC                   *        
      *                           21,06,BI,A,  VENDEUR                 *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRE000.                                                        
           10 IRE000-NSOCIETE           PIC X(03).                      007  003
           10 IRE000-NLIEU              PIC X(03).                      007  003
           10 IRE000-CAISSE             PIC X(03).                      007  003
           10 IRE000-TRANS              PIC X(04).                      007  003
           10 IRE000-NCODIC             PIC X(07).                      023  007
           10 IRE000-VENDEUR            PIC X(06).                      078  004
           10 IRE000-VAL-FORC-P         PIC S9(05)V9(2) COMP-3.         082  004
           10 IRE000-VAL-FORC-M         PIC S9(05)V9(2) COMP-3.         082  004
           10 IRE000-VAL-THEOR          PIC S9(07)V9(2) COMP-3.         086  004
           10 IRE000-QTE                PIC S9(03)      COMP-3.         090  002
           10 IRE000-VAL-REMIS          PIC S9(05)V9(2) COMP-3.         092  004
           10 IRE000-DATE               PIC X(8).                       092  004
            05 FILLER                      PIC X(47).                           
                                                                                
