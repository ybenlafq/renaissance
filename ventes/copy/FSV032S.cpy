      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  FSV032-RECORD.                                                       
           05 FSV032-POSTE OCCURS 15.                                           
               10 FSV032-NSOCIETE             PIC X(03).                        
               10 FSV032-NLIEU                PIC X(03).                        
               10 FSV032-NVENTE               PIC X(07).                        
               10 FSV032-NSEQNQ               PIC 9(5).                         
               10 FSV032-TYPMAJ               PIC X.                            
               10 FSV032-TBIEN                PIC X(01).                        
               10 FSV032-LIBRE                PIC X(19).                        
               10 FSV032-DATA-VENTE.                                            
                  15 FSV032-NCODIC            PIC X(07).                        
      *        NUMERO DE SERIE                                                  
                  15 FSV032-NSERIE            PIC X(20).                        
                  15 FSV032-DANNULATION       PIC X(08).                        
                  15 FSV032-QVENDUE           PIC 9(5).                         
                  15 FSV032-CMODDEL           PIC X(05).                        
                  15 FSV032-DDELIV            PIC X(08).                        
                  15 FSV032-CPLAGE            PIC X(02).                        
                  15 FSV032-CTOPE             PIC X(01).                        
                  15 FSV032-CGAR              PIC X(05).                        
                  15 FSV032-DGAR              PIC X(08).                        
                  15 FSV032-CPSE              PIC X(05).                        
      *  HISTORIQUEMENT:NPSE MAIS DEPUIS D'AUTRES TYPES DE CONTRAT EXIST        
                  15 FSV032-NCONTRAT          PIC X(08).                        
                  15 FSV032-DPSE              PIC X(08).                        
                  15 FSV032-PVTOTAL           PIC 9(8)V99.                      
                  15 FSV032-MTREMISE          PIC 9(8)V99.                      
                  15 FSV032-PVCONTRAT         PIC 9(8)V99.                      
                  15 FSV032-VENTELIBRE        PIC X(10).                        
               10 FSV032-DATA-CLIENT.                                           
      * LIGNES ARTICLES                                                         
                  15 FSV032-NCLIENTADR        PIC X(08).                        
                  15 FSV032-CTITRENOM         PIC X(05).                        
                  15 FSV032-LNOM              PIC X(25).                        
                  15 FSV032-LPRENOM           PIC X(15).                        
                  15 FSV032-CPOSTAL           PIC X(05).                        
                  15 FSV032-LBATIMENT         PIC X(03).                        
                  15 FSV032-LESCALIER         PIC X(03).                        
                  15 FSV032-LETAGE            PIC X(03).                        
                  15 FSV032-LPORTE            PIC X(03).                        
                  15 FSV032-CVOIE             PIC X(05).                        
                  15 FSV032-CTVOIE            PIC X(04).                        
                  15 FSV032-LNOMVOIE          PIC X(21).                        
                  15 FSV032-LCOMMUNE          PIC X(32).                        
                  15 FSV032-CPAYS             PIC X(03).                        
                  15 FSV032-TELDOM            PIC X(15).                        
                  15 FSV032-TELBUR            PIC X(15).                        
                  15 FSV032-LPOSTEBUR         PIC X(05).                        
                  15 FSV032-NGSM              PIC X(15).                        
                  15 FSV032-CINSEE            PIC X(05).                        
                  15 FSV032-NCARTE            PIC X(08).                        
                  15 FSV032-CLIENTLIBRE       PIC X(10).                        
                                                                                
