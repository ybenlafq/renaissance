      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPP050 AU 15/10/1997  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,03,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,02,BI,A,                          *        
      *                           22,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPP050.                                                        
            05 NOMETAT-IPP050           PIC X(6) VALUE 'IPP050'.                
            05 RUPTURES-IPP050.                                                 
           10 IPP050-NSOCIETE           PIC X(03).                      007  003
           10 IPP050-CGRPMAG            PIC X(02).                      010  002
           10 IPP050-NLIEU              PIC X(03).                      012  003
           10 IPP050-CPRIME             PIC X(05).                      015  005
           10 IPP050-NLIGNE             PIC X(02).                      020  002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPP050-SEQUENCE           PIC S9(04) COMP.                022  002
      *--                                                                       
           10 IPP050-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPP050.                                                   
           10 IPP050-FLAG-LFRC          PIC X(01).                      024  001
           10 IPP050-FLAG-LMVT          PIC X(01).                      025  001
           10 IPP050-FLAG-LRUB          PIC X(01).                      026  001
           10 IPP050-LFRC               PIC X(30).                      027  030
           10 IPP050-LLIEU              PIC X(20).                      057  020
           10 IPP050-LMVT               PIC X(30).                      077  030
           10 IPP050-LOPTION            PIC X(20).                      107  020
           10 IPP050-LRUB               PIC X(20).                      127  020
           10 IPP050-PMONT-ZONE1-A-1    PIC X(09).                      147  009
           10 IPP050-PMONT-ZONE1-M      PIC X(09).                      156  009
           10 IPP050-PMONT-ZONE1-M-1    PIC X(09).                      165  009
           10 IPP050-PMONT-ZONE2-A-1    PIC X(06).                      174  006
           10 IPP050-PMONT-ZONE2-M      PIC X(06).                      180  006
           10 IPP050-PMONT-ZONE2-M-1    PIC X(06).                      186  006
           10 IPP050-PMONT-ZONE3-A-1    PIC X(06).                      192  006
           10 IPP050-PMONT-ZONE3-M      PIC X(06).                      198  006
           10 IPP050-PMONT-ZONE3-M-1    PIC X(06).                      204  006
           10 IPP050-POUR-ZONE1-A-1     PIC X(11).                      210  011
           10 IPP050-POUR-ZONE1-M-1     PIC X(11).                      221  011
           10 IPP050-POUR-ZONE2-A-1     PIC X(08).                      232  008
           10 IPP050-POUR-ZONE2-M-1     PIC X(08).                      240  008
           10 IPP050-POUR-ZONE3-A-1     PIC X(08).                      248  008
           10 IPP050-POUR-ZONE3-M-1     PIC X(08).                      256  008
           10 IPP050-WACTIF-FRC         PIC X(01).                      264  001
           10 IPP050-WACTIF-MVT         PIC X(01).                      265  001
           10 IPP050-WACTIF-OPTION      PIC X(01).                      266  001
           10 IPP050-TOT-FRC-A-1        PIC S9(11)      COMP-3.         267  006
           10 IPP050-TOT-FRC-M          PIC S9(11)      COMP-3.         273  006
           10 IPP050-TOT-FRC-M-1        PIC S9(11)      COMP-3.         279  006
           10 IPP050-TOT-MVT-A-1        PIC S9(11)      COMP-3.         285  006
           10 IPP050-TOT-MVT-M          PIC S9(11)      COMP-3.         291  006
           10 IPP050-TOT-MVT-M-1        PIC S9(11)      COMP-3.         297  006
           10 IPP050-ANNEE-A-1          PIC X(06).                      303  006
           10 IPP050-MOIS-M             PIC X(06).                      309  006
           10 IPP050-MOIS-M-1           PIC X(06).                      315  006
            05 FILLER                      PIC X(192).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPP050-LONG           PIC S9(4)   COMP  VALUE +320.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPP050-LONG           PIC S9(4) COMP-5  VALUE +320.           
                                                                                
      *}                                                                        
