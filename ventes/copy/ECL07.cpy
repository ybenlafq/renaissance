      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ECL06   ECL06                                              00000020
      ***************************************************************** 00000030
       01   ECL07I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEML   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEMF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEUINVL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCLIEUINVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCLIEUINVF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCLIEUINVI     PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUINVL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLLIEUINVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLLIEUINVF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEUINVI     PIC X(20).                                 00000290
           02 MLIGNESI OCCURS   14 TIMES .                              00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBCPTL      COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MNBCPTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNBCPTF      PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MNBCPTI      PIC X.                                     00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWCTRGL      COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MWCTRGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWCTRGF      PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MWCTRGI      PIC X.                                     00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWQTUNL      COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MWQTUNL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWQTUNF      PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MWQTUNI      PIC X.                                     00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWETATCL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MWETATCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWETATCF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MWETATCI     PIC X.                                     00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWCTHEOL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MWCTHEOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWCTHEOF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MWCTHEOI     PIC X.                                     00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWCBL   COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MWCBL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MWCBF   PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MWCBI   PIC X.                                          00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREGLL      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MLREGLL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLREGLF      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLREGLI      PIC X(20).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MLIBERRI  PIC X(78).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCODTRAI  PIC X(4).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCICSI    PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MNETNAMI  PIC X(8).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MSCREENI  PIC X(4).                                       00000780
      ***************************************************************** 00000790
      * SDF: ECL06   ECL06                                              00000800
      ***************************************************************** 00000810
       01   ECL07O REDEFINES ECL07I.                                    00000820
           02 FILLER    PIC X(12).                                      00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MDATJOUA  PIC X.                                          00000850
           02 MDATJOUC  PIC X.                                          00000860
           02 MDATJOUP  PIC X.                                          00000870
           02 MDATJOUH  PIC X.                                          00000880
           02 MDATJOUV  PIC X.                                          00000890
           02 MDATJOUO  PIC X(10).                                      00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MTIMJOUA  PIC X.                                          00000920
           02 MTIMJOUC  PIC X.                                          00000930
           02 MTIMJOUP  PIC X.                                          00000940
           02 MTIMJOUH  PIC X.                                          00000950
           02 MTIMJOUV  PIC X.                                          00000960
           02 MTIMJOUO  PIC X(5).                                       00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MPAGEA    PIC X.                                          00000990
           02 MPAGEC    PIC X.                                          00001000
           02 MPAGEP    PIC X.                                          00001010
           02 MPAGEH    PIC X.                                          00001020
           02 MPAGEV    PIC X.                                          00001030
           02 MPAGEO    PIC X(3).                                       00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MPAGEMA   PIC X.                                          00001060
           02 MPAGEMC   PIC X.                                          00001070
           02 MPAGEMP   PIC X.                                          00001080
           02 MPAGEMH   PIC X.                                          00001090
           02 MPAGEMV   PIC X.                                          00001100
           02 MPAGEMO   PIC X(3).                                       00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MCLIEUINVA     PIC X.                                     00001130
           02 MCLIEUINVC     PIC X.                                     00001140
           02 MCLIEUINVP     PIC X.                                     00001150
           02 MCLIEUINVH     PIC X.                                     00001160
           02 MCLIEUINVV     PIC X.                                     00001170
           02 MCLIEUINVO     PIC X(5).                                  00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MLLIEUINVA     PIC X.                                     00001200
           02 MLLIEUINVC     PIC X.                                     00001210
           02 MLLIEUINVP     PIC X.                                     00001220
           02 MLLIEUINVH     PIC X.                                     00001230
           02 MLLIEUINVV     PIC X.                                     00001240
           02 MLLIEUINVO     PIC X(20).                                 00001250
           02 MLIGNESO OCCURS   14 TIMES .                              00001260
             03 FILLER       PIC X(2).                                  00001270
             03 MNBCPTA      PIC X.                                     00001280
             03 MNBCPTC PIC X.                                          00001290
             03 MNBCPTP PIC X.                                          00001300
             03 MNBCPTH PIC X.                                          00001310
             03 MNBCPTV PIC X.                                          00001320
             03 MNBCPTO      PIC X.                                     00001330
             03 FILLER       PIC X(2).                                  00001340
             03 MWCTRGA      PIC X.                                     00001350
             03 MWCTRGC PIC X.                                          00001360
             03 MWCTRGP PIC X.                                          00001370
             03 MWCTRGH PIC X.                                          00001380
             03 MWCTRGV PIC X.                                          00001390
             03 MWCTRGO      PIC X.                                     00001400
             03 FILLER       PIC X(2).                                  00001410
             03 MWQTUNA      PIC X.                                     00001420
             03 MWQTUNC PIC X.                                          00001430
             03 MWQTUNP PIC X.                                          00001440
             03 MWQTUNH PIC X.                                          00001450
             03 MWQTUNV PIC X.                                          00001460
             03 MWQTUNO      PIC X.                                     00001470
             03 FILLER       PIC X(2).                                  00001480
             03 MWETATCA     PIC X.                                     00001490
             03 MWETATCC     PIC X.                                     00001500
             03 MWETATCP     PIC X.                                     00001510
             03 MWETATCH     PIC X.                                     00001520
             03 MWETATCV     PIC X.                                     00001530
             03 MWETATCO     PIC X.                                     00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MWCTHEOA     PIC X.                                     00001560
             03 MWCTHEOC     PIC X.                                     00001570
             03 MWCTHEOP     PIC X.                                     00001580
             03 MWCTHEOH     PIC X.                                     00001590
             03 MWCTHEOV     PIC X.                                     00001600
             03 MWCTHEOO     PIC X.                                     00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MWCBA   PIC X.                                          00001630
             03 MWCBC   PIC X.                                          00001640
             03 MWCBP   PIC X.                                          00001650
             03 MWCBH   PIC X.                                          00001660
             03 MWCBV   PIC X.                                          00001670
             03 MWCBO   PIC X.                                          00001680
             03 FILLER       PIC X(2).                                  00001690
             03 MLREGLA      PIC X.                                     00001700
             03 MLREGLC PIC X.                                          00001710
             03 MLREGLP PIC X.                                          00001720
             03 MLREGLH PIC X.                                          00001730
             03 MLREGLV PIC X.                                          00001740
             03 MLREGLO      PIC X(20).                                 00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MLIBERRA  PIC X.                                          00001770
           02 MLIBERRC  PIC X.                                          00001780
           02 MLIBERRP  PIC X.                                          00001790
           02 MLIBERRH  PIC X.                                          00001800
           02 MLIBERRV  PIC X.                                          00001810
           02 MLIBERRO  PIC X(78).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCODTRAA  PIC X.                                          00001840
           02 MCODTRAC  PIC X.                                          00001850
           02 MCODTRAP  PIC X.                                          00001860
           02 MCODTRAH  PIC X.                                          00001870
           02 MCODTRAV  PIC X.                                          00001880
           02 MCODTRAO  PIC X(4).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCICSA    PIC X.                                          00001910
           02 MCICSC    PIC X.                                          00001920
           02 MCICSP    PIC X.                                          00001930
           02 MCICSH    PIC X.                                          00001940
           02 MCICSV    PIC X.                                          00001950
           02 MCICSO    PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNETNAMA  PIC X.                                          00001980
           02 MNETNAMC  PIC X.                                          00001990
           02 MNETNAMP  PIC X.                                          00002000
           02 MNETNAMH  PIC X.                                          00002010
           02 MNETNAMV  PIC X.                                          00002020
           02 MNETNAMO  PIC X(8).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MSCREENA  PIC X.                                          00002050
           02 MSCREENC  PIC X.                                          00002060
           02 MSCREENP  PIC X.                                          00002070
           02 MSCREENH  PIC X.                                          00002080
           02 MSCREENV  PIC X.                                          00002090
           02 MSCREENO  PIC X(4).                                       00002100
                                                                                
