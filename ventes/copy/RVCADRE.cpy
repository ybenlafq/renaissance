      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CADRE CADRE DE VENTE  PERIMETRE        *        
      *----------------------------------------------------------------*        
       01  RVCADRE.                                                             
           05  CADRE-CTABLEG2    PIC X(15).                                     
           05  CADRE-CTABLEG2-REDEF REDEFINES CADRE-CTABLEG2.                   
               10  CADRE-PRESTA          PIC X(05).                             
               10  CADRE-FAMILLE         PIC X(05).                             
           05  CADRE-WTABLEG     PIC X(80).                                     
           05  CADRE-WTABLEG-REDEF  REDEFINES CADRE-WTABLEG.                    
               10  CADRE-CADRE           PIC X(05).                             
               10  CADRE-EQUIPE          PIC X(05).                             
               10  CADRE-COMMENT1        PIC X(20).                             
               10  CADRE-COMMENT2        PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCADRE-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CADRE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CADRE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CADRE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CADRE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
