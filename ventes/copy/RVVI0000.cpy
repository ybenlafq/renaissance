      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVVI0000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVI0000                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVI0000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVI0000.                                                            
      *}                                                                        
           02  VI00-NSOCO                                                       
               PIC X(0003).                                                     
           02  VI00-NLIEUO                                                      
               PIC X(0003).                                                     
           02  VI00-NSOCV                                                       
               PIC X(0003).                                                     
           02  VI00-NLIEUV                                                      
               PIC X(0003).                                                     
           02  VI00-NVENTE                                                      
               PIC X(0007).                                                     
           02  VI00-DVENTE                                                      
               PIC X(0008).                                                     
           02  VI00-DATENC                                                      
               PIC X(0008).                                                     
           02  VI00-CVENDEURO                                                   
               PIC X(0006).                                                     
           02  VI00-CVENDEURG                                                   
               PIC X(0006).                                                     
           02  VI00-CTITRENOM                                                   
               PIC X(0005).                                                     
           02  VI00-LNOM                                                        
               PIC X(0025).                                                     
           02  VI00-LPRENOM                                                     
               PIC X(0015).                                                     
           02  VI00-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  VI00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVVI0000                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVI0000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVI0000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI00-NSOCO-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI00-NSOCO-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI00-NLIEUO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI00-NLIEUO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI00-NSOCV-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI00-NSOCV-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI00-NLIEUV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI00-NLIEUV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI00-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI00-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI00-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI00-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI00-DATENC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI00-DATENC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI00-CVENDEURO-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI00-CVENDEURO-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI00-CVENDEURG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI00-CVENDEURG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI00-CTITRENOM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI00-CTITRENOM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI00-LNOM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI00-LNOM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI00-LPRENOM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI00-LPRENOM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI00-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VI00-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VI00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  VI00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
