      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVSL0500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSL0500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVSL0500.                                                            
           02  SL05-DOMAINE                                                     
               PIC X(0005).                                                     
           02  SL05-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  SL05-NLIEU                                                       
               PIC X(0003).                                                     
           02  SL05-NCOMPT                                                      
               PIC X(0002).                                                     
           02  SL05-NREFERENCE                                                  
               PIC X(0020).                                                     
           02  SL05-NSEQ                                                        
               PIC X(0003).                                                     
           02  SL05-NREFERENC2                                                  
               PIC X(0020).                                                     
           02  SL05-DSAISIE                                                     
               PIC X(0008).                                                     
           02  SL05-DEFGE                                                       
               PIC X(0008).                                                     
           02  SL05-NSOCENC                                                     
               PIC X(0003).                                                     
           02  SL05-NLIEUENC                                                    
               PIC X(0003).                                                     
           02  SL05-PMONTANT                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  SL05-DRAPPRO                                                     
               PIC X(0008).                                                     
           02  SL05-DPRERAPP                                                    
               PIC X(0008).                                                     
           02  SL05-CACID                                                       
               PIC X(0008).                                                     
           02  SL05-DMAJ                                                        
               PIC X(0008).                                                     
           02  SL05-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVSL0500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVSL0500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL05-DOMAINE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL05-DOMAINE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL05-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL05-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL05-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL05-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL05-NCOMPT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL05-NCOMPT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL05-NREFERENCE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL05-NREFERENCE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL05-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL05-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL05-NREFERENC2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL05-NREFERENC2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL05-DSAISIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL05-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL05-DEFGE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL05-DEFGE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL05-NSOCENC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL05-NSOCENC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL05-NLIEUENC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL05-NLIEUENC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL05-PMONTANT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL05-PMONTANT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL05-DRAPPRO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL05-DRAPPRO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL05-DPRERAPP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL05-DPRERAPP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL05-CACID-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL05-CACID-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL05-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL05-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL05-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  SL05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
