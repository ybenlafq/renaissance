      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CSTSP CODE S�L. ART � TRT SP�CIFIQUE   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCSTSP .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCSTSP .                                                            
      *}                                                                        
           05  CSTSP-CTABLEG2    PIC X(15).                                     
           05  CSTSP-CTABLEG2-REDEF REDEFINES CSTSP-CTABLEG2.                   
               10  CSTSP-CSELA           PIC X(05).                             
           05  CSTSP-WTABLEG     PIC X(80).                                     
           05  CSTSP-WTABLEG-REDEF  REDEFINES CSTSP-WTABLEG.                    
               10  CSTSP-ENVNOMAD        PIC X(01).                             
               10  CSTSP-CTYPRES         PIC X(02).                             
               10  CSTSP-COPSL           PIC X(05).                             
               10  CSTSP-AUTORRM         PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCSTSP-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCSTSP-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CSTSP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CSTSP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CSTSP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CSTSP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
