      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TD065 EXTRACTION TABLE TD00            *        
      *----------------------------------------------------------------*        
       01  RVTD065.                                                             
           05  TD065-CTABLEG2    PIC X(15).                                     
           05  TD065-CTABLEG2-REDEF REDEFINES TD065-CTABLEG2.                   
               10  TD065-PARAM           PIC X(10).                             
           05  TD065-WTABLEG     PIC X(80).                                     
           05  TD065-WTABLEG-REDEF  REDEFINES TD065-WTABLEG.                    
               10  TD065-DATDEB          PIC X(08).                             
               10  TD065-DATDEB-N       REDEFINES TD065-DATDEB                  
                                         PIC 9(08).                             
               10  TD065-WNBJ            PIC X(01).                             
               10  TD065-NBJ             PIC X(03).                             
               10  TD065-NBJ-N          REDEFINES TD065-NBJ                     
                                         PIC 9(03).                             
               10  TD065-WDATFIN         PIC X(01).                             
               10  TD065-DATFIN          PIC X(08).                             
               10  TD065-DATFIN-N       REDEFINES TD065-DATFIN                  
                                         PIC 9(08).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTD065-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TD065-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TD065-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TD065-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TD065-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
