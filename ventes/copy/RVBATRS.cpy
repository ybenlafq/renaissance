      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BATRS TRANSPOSITION BA DN-> DNN        *        
      *----------------------------------------------------------------*        
       01  RVBATRS.                                                             
           05  BATRS-CTABLEG2    PIC X(15).                                     
           05  BATRS-CTABLEG2-REDEF REDEFINES BATRS-CTABLEG2.                   
               10  BATRS-ANCSOC          PIC X(03).                             
               10  BATRS-ANCBA           PIC X(06).                             
               10  BATRS-VALIDE          PIC X(01).                             
           05  BATRS-WTABLEG     PIC X(80).                                     
           05  BATRS-WTABLEG-REDEF  REDEFINES BATRS-WTABLEG.                    
               10  BATRS-NOUVBA          PIC X(06).                             
               10  BATRS-NOUVSOC         PIC X(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVBATRS-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BATRS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BATRS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BATRS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BATRS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
