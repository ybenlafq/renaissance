      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
          01 COMM-EM52-DONNEES.                                         00000010
             10 COMM-EM52-NB-POSTES               PIC S9(09) COMP-3.    00000070
             10 COMM-EM52-CODRET                  PIC 9(03) COMP-3.     00000070
                88 COMM-EM52-ERREUR-MANIP         VALUE 999.            00000070
             10 COMM-EM52-LIBERR                  PIC X(25).            00000070
             10 COMM-EM52-CRITERES.                                     00000020
                20 COMM-EM52-NSOCIETE             PIC X(03).            00000020
                20 COMM-EM52-NLIEU                PIC X(03).            00000030
                20 COMM-EM52-NVENTE               PIC X(07).            00000030
                20 COMM-EM52-NTYPVTE              PIC X(01).            00000030
                20 COMM-EM52-DVENTE               PIC X(08).            00000020
                20 COMM-EM52-LNOM                 PIC X(25).            00000020
                20 COMM-EM52-NCODIC               PIC X(07).            00000030
                20 COMM-EM52-CFAM                 PIC X(05).            00000030
                20 COMM-EM52-CMARQ                PIC X(05).            00000040
                20 COMM-EM52-NOTEL                PIC X(10).            00000030
                20 COMM-EM52-DDEBUT               PIC X(08).            00000020
                20 COMM-EM52-DFIN                 PIC X(08).            00000020
             10 COMM-EM52-TYPE-RECHERCHE          PIC X.                00000070
                88 COMM-EM52-PAS-RECH                  VALUE ' '.               
                88 COMM-EM52-RECH-DVENTE               VALUE '1'.               
                88 COMM-EM52-RECH-NOM-MARQUE           VALUE '2'.               
                88 COMM-EM52-RECH-NOM-FAMILLE          VALUE '3'.               
                88 COMM-EM52-RECH-NOM-MARQ-FAM         VALUE '4'.               
                88 COMM-EM52-RECH-NOM-SEUL             VALUE '5'.               
                88 COMM-EM52-RECH-NOM         VALUE '2' '3' '4' '5'.            
                88 COMM-EM52-RECH-NTRANS               VALUE 'A'.               
                88 COMM-EM52-RECH-TTRANS               VALUE 'B'.               
                88 COMM-EM52-RECH-TTRANS-MONTANT       VALUE 'C'.               
                88 COMM-EM52-RECH-TPAIE-MONTANT        VALUE 'D'.               
                88 COMM-EM52-RECH-NCODIC               VALUE 'E'.               
                88 COMM-EM52-RECH-TPAIE-CDEV           VALUE 'F'.               
                88 COMM-EM52-RECH-TPAI-CDEV-PREGL      VALUE 'G'.               
                                                                                
