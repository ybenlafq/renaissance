      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV83   EGV83                                              00000020
      ***************************************************************** 00000030
       01   EGV83I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * PAGE ECRAN                                                      00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MWPAGEI   PIC X(3).                                       00000200
      * NUMERO DOCUMENT                                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDOCENLVL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNDOCENLVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNDOCENLVF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNDOCENLVI     PIC X(7).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLBECTRLL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLBECTRLL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLBECTRLF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLBECTRLI      PIC X(18).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBECTRLL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNBECTRLL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBECTRLF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNBECTRLI      PIC X(7).                                  00000330
      * MAGASIN CEDANT                                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUORIGL    COMP PIC S9(4).                            00000350
      *--                                                                       
           02 MNLIEUORIGL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNLIEUORIGF    PIC X.                                     00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MNLIEUORIGI    PIC X(3).                                  00000380
      * LIBELLE LIEU CEDANT                                             00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUORIGL    COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MLLIEUORIGL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLLIEUORIGF    PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MLLIEUORIGI    PIC X(20).                                 00000430
      * DATE DE RECEPTION                                               00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECEPTL      COMP PIC S9(4).                            00000450
      *--                                                                       
           02 MDRECEPTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDRECEPTF      PIC X.                                     00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MDRECEPTI      PIC X(8).                                  00000480
      * LIEU FINAL                                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUDESTL    COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MNLIEUDESTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNLIEUDESTF    PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNLIEUDESTI    PIC X(3).                                  00000530
      * SOUS LIEU FINAL                                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSSLIEUDESTL  COMP PIC S9(4).                            00000550
      *--                                                                       
           02 MNSSLIEUDESTL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNSSLIEUDESTF  PIC X.                                     00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MNSSLIEUDESTI  PIC X(3).                                  00000580
      * LIBELLE LIEU  FINAL                                             00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUDESTL    COMP PIC S9(4).                            00000600
      *--                                                                       
           02 MLLIEUDESTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLLIEUDESTF    PIC X.                                     00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MLLIEUDESTI    PIC X(20).                                 00000630
      * DETAIL BE                                                       00000640
           02 MBESI OCCURS   15 TIMES .                                 00000650
      * PREMIERE PARTIE                                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNE1L     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MLIGNE1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIGNE1F     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLIGNE1I     PIC X(8).                                  00000700
      * QUANTITE BE                                                     00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000720
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000730
             03 FILLER  PIC X(4).                                       00000740
             03 MQTEI   PIC X(3).                                       00000750
      * DEUXIEME PARTIE                                                 00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNE2L     COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MLIGNE2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIGNE2F     PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MLIGNE2I     PIC X(64).                                 00000800
      * ZONE CMD AIDA                                                   00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MZONCMDI  PIC X(15).                                      00000850
      * MESSAGE ERREUR                                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLIBERRI  PIC X(58).                                      00000900
      * CODE TRANSACTION                                                00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MCODTRAI  PIC X(4).                                       00000950
      * CICS DE TRAVAIL                                                 00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MCICSI    PIC X(5).                                       00001000
      * NETNAME                                                         00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MNETNAMI  PIC X(8).                                       00001050
      * CODE TERMINAL                                                   00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MSCREENI  PIC X(5).                                       00001100
      ***************************************************************** 00001110
      * SDF: EGV83   EGV83                                              00001120
      ***************************************************************** 00001130
       01   EGV83O REDEFINES EGV83I.                                    00001140
           02 FILLER    PIC X(12).                                      00001150
      * DATE DU JOUR                                                    00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MDATJOUA  PIC X.                                          00001180
           02 MDATJOUC  PIC X.                                          00001190
           02 MDATJOUP  PIC X.                                          00001200
           02 MDATJOUH  PIC X.                                          00001210
           02 MDATJOUV  PIC X.                                          00001220
           02 MDATJOUO  PIC X(10).                                      00001230
      * HEURE                                                           00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MTIMJOUA  PIC X.                                          00001260
           02 MTIMJOUC  PIC X.                                          00001270
           02 MTIMJOUP  PIC X.                                          00001280
           02 MTIMJOUH  PIC X.                                          00001290
           02 MTIMJOUV  PIC X.                                          00001300
           02 MTIMJOUO  PIC X(5).                                       00001310
      * PAGE ECRAN                                                      00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MWPAGEA   PIC X.                                          00001340
           02 MWPAGEC   PIC X.                                          00001350
           02 MWPAGEP   PIC X.                                          00001360
           02 MWPAGEH   PIC X.                                          00001370
           02 MWPAGEV   PIC X.                                          00001380
           02 MWPAGEO   PIC ZZ9.                                        00001390
      * NUMERO DOCUMENT                                                 00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MNDOCENLVA     PIC X.                                     00001420
           02 MNDOCENLVC     PIC X.                                     00001430
           02 MNDOCENLVP     PIC X.                                     00001440
           02 MNDOCENLVH     PIC X.                                     00001450
           02 MNDOCENLVV     PIC X.                                     00001460
           02 MNDOCENLVO     PIC X(7).                                  00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MLBECTRLA      PIC X.                                     00001490
           02 MLBECTRLC PIC X.                                          00001500
           02 MLBECTRLP PIC X.                                          00001510
           02 MLBECTRLH PIC X.                                          00001520
           02 MLBECTRLV PIC X.                                          00001530
           02 MLBECTRLO      PIC X(18).                                 00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MNBECTRLA      PIC X.                                     00001560
           02 MNBECTRLC PIC X.                                          00001570
           02 MNBECTRLP PIC X.                                          00001580
           02 MNBECTRLH PIC X.                                          00001590
           02 MNBECTRLV PIC X.                                          00001600
           02 MNBECTRLO      PIC X(7).                                  00001610
      * MAGASIN CEDANT                                                  00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MNLIEUORIGA    PIC X.                                     00001640
           02 MNLIEUORIGC    PIC X.                                     00001650
           02 MNLIEUORIGP    PIC X.                                     00001660
           02 MNLIEUORIGH    PIC X.                                     00001670
           02 MNLIEUORIGV    PIC X.                                     00001680
           02 MNLIEUORIGO    PIC X(3).                                  00001690
      * LIBELLE LIEU CEDANT                                             00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MLLIEUORIGA    PIC X.                                     00001720
           02 MLLIEUORIGC    PIC X.                                     00001730
           02 MLLIEUORIGP    PIC X.                                     00001740
           02 MLLIEUORIGH    PIC X.                                     00001750
           02 MLLIEUORIGV    PIC X.                                     00001760
           02 MLLIEUORIGO    PIC X(20).                                 00001770
      * DATE DE RECEPTION                                               00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MDRECEPTA      PIC X.                                     00001800
           02 MDRECEPTC PIC X.                                          00001810
           02 MDRECEPTP PIC X.                                          00001820
           02 MDRECEPTH PIC X.                                          00001830
           02 MDRECEPTV PIC X.                                          00001840
           02 MDRECEPTO      PIC X(8).                                  00001850
      * LIEU FINAL                                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MNLIEUDESTA    PIC X.                                     00001880
           02 MNLIEUDESTC    PIC X.                                     00001890
           02 MNLIEUDESTP    PIC X.                                     00001900
           02 MNLIEUDESTH    PIC X.                                     00001910
           02 MNLIEUDESTV    PIC X.                                     00001920
           02 MNLIEUDESTO    PIC X(3).                                  00001930
      * SOUS LIEU FINAL                                                 00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MNSSLIEUDESTA  PIC X.                                     00001960
           02 MNSSLIEUDESTC  PIC X.                                     00001970
           02 MNSSLIEUDESTP  PIC X.                                     00001980
           02 MNSSLIEUDESTH  PIC X.                                     00001990
           02 MNSSLIEUDESTV  PIC X.                                     00002000
           02 MNSSLIEUDESTO  PIC X(3).                                  00002010
      * LIBELLE LIEU  FINAL                                             00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MLLIEUDESTA    PIC X.                                     00002040
           02 MLLIEUDESTC    PIC X.                                     00002050
           02 MLLIEUDESTP    PIC X.                                     00002060
           02 MLLIEUDESTH    PIC X.                                     00002070
           02 MLLIEUDESTV    PIC X.                                     00002080
           02 MLLIEUDESTO    PIC X(20).                                 00002090
      * DETAIL BE                                                       00002100
           02 MBESO OCCURS   15 TIMES .                                 00002110
      * PREMIERE PARTIE                                                 00002120
             03 FILLER       PIC X(2).                                  00002130
             03 MLIGNE1A     PIC X.                                     00002140
             03 MLIGNE1C     PIC X.                                     00002150
             03 MLIGNE1P     PIC X.                                     00002160
             03 MLIGNE1H     PIC X.                                     00002170
             03 MLIGNE1V     PIC X.                                     00002180
             03 MLIGNE1O     PIC X(8).                                  00002190
      * QUANTITE BE                                                     00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MQTEA   PIC X.                                          00002220
             03 MQTEC   PIC X.                                          00002230
             03 MQTEP   PIC X.                                          00002240
             03 MQTEH   PIC X.                                          00002250
             03 MQTEV   PIC X.                                          00002260
             03 MQTEO   PIC ZZ9.                                        00002270
      * DEUXIEME PARTIE                                                 00002280
             03 FILLER       PIC X(2).                                  00002290
             03 MLIGNE2A     PIC X.                                     00002300
             03 MLIGNE2C     PIC X.                                     00002310
             03 MLIGNE2P     PIC X.                                     00002320
             03 MLIGNE2H     PIC X.                                     00002330
             03 MLIGNE2V     PIC X.                                     00002340
             03 MLIGNE2O     PIC X(64).                                 00002350
      * ZONE CMD AIDA                                                   00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MZONCMDA  PIC X.                                          00002380
           02 MZONCMDC  PIC X.                                          00002390
           02 MZONCMDP  PIC X.                                          00002400
           02 MZONCMDH  PIC X.                                          00002410
           02 MZONCMDV  PIC X.                                          00002420
           02 MZONCMDO  PIC X(15).                                      00002430
      * MESSAGE ERREUR                                                  00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MLIBERRA  PIC X.                                          00002460
           02 MLIBERRC  PIC X.                                          00002470
           02 MLIBERRP  PIC X.                                          00002480
           02 MLIBERRH  PIC X.                                          00002490
           02 MLIBERRV  PIC X.                                          00002500
           02 MLIBERRO  PIC X(58).                                      00002510
      * CODE TRANSACTION                                                00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MCODTRAA  PIC X.                                          00002540
           02 MCODTRAC  PIC X.                                          00002550
           02 MCODTRAP  PIC X.                                          00002560
           02 MCODTRAH  PIC X.                                          00002570
           02 MCODTRAV  PIC X.                                          00002580
           02 MCODTRAO  PIC X(4).                                       00002590
      * CICS DE TRAVAIL                                                 00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MCICSA    PIC X.                                          00002620
           02 MCICSC    PIC X.                                          00002630
           02 MCICSP    PIC X.                                          00002640
           02 MCICSH    PIC X.                                          00002650
           02 MCICSV    PIC X.                                          00002660
           02 MCICSO    PIC X(5).                                       00002670
      * NETNAME                                                         00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MNETNAMA  PIC X.                                          00002700
           02 MNETNAMC  PIC X.                                          00002710
           02 MNETNAMP  PIC X.                                          00002720
           02 MNETNAMH  PIC X.                                          00002730
           02 MNETNAMV  PIC X.                                          00002740
           02 MNETNAMO  PIC X(8).                                       00002750
      * CODE TERMINAL                                                   00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MSCREENA  PIC X.                                          00002780
           02 MSCREENC  PIC X.                                          00002790
           02 MSCREENP  PIC X.                                          00002800
           02 MSCREENH  PIC X.                                          00002810
           02 MSCREENV  PIC X.                                          00002820
           02 MSCREENO  PIC X(5).                                       00002830
                                                                                
