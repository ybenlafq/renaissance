      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JMD413 AU 22/04/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,05,BI,A,                          *        
      *                           25,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-JMD413.                                                        
            05 NOMETAT-JMD413           PIC X(6) VALUE 'JMD413'.                
            05 RUPTURES-JMD413.                                                 
           10 JMD413-NMAG               PIC X(03).                      007  003
           10 JMD413-COPER              PIC X(05).                      010  005
           10 JMD413-CFAM               PIC X(05).                      015  005
           10 JMD413-WSEQFAM            PIC 9(05).                      020  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 JMD413-SEQUENCE           PIC S9(04) COMP.                025  002
      *--                                                                       
           10 JMD413-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-JMD413.                                                   
           10 JMD413-CMARQ              PIC X(05).                      027  005
           10 JMD413-LFAM               PIC X(20).                      032  020
           10 JMD413-LMAG               PIC X(20).                      052  020
           10 JMD413-LOPER              PIC X(20).                      072  020
           10 JMD413-LREFFOURN          PIC X(20).                      092  020
           10 JMD413-NCODIC             PIC X(07).                      112  007
           10 JMD413-NSOC               PIC X(03).                      119  003
           10 JMD413-PCESSION           PIC S9(09)V9(2) COMP-3.         122  006
           10 JMD413-PCESSION-EXC       PIC S9(11)V9(2) COMP-3.         128  007
           10 JMD413-PRMP               PIC S9(09)V9(2) COMP-3.         135  006
           10 JMD413-PRMP-EXC           PIC S9(11)V9(2) COMP-3.         141  007
           10 JMD413-QTE                PIC S9(06)      COMP-3.         148  004
           10 JMD413-QTE-EXC            PIC S9(06)      COMP-3.         152  004
           10 JMD413-DDEB               PIC X(08).                      156  008
           10 JMD413-DFIN               PIC X(08).                      164  008
            05 FILLER                      PIC X(341).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-JMD413-LONG           PIC S9(4)   COMP  VALUE +171.           
      *                                                                         
      *--                                                                       
        01  DSECT-JMD413-LONG           PIC S9(4) COMP-5  VALUE +171.           
                                                                                
      *}                                                                        
