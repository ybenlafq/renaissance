      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      *                                                                         
      ******************************************************************        
      * PREPARATION DE L'ACCES AU FICHIER FSET1C PAR LE CHEMIN FSET1K           
      ******************************************************************        
      *                                                                         
       CLEF-FSET1K             SECTION.                                         
      *                                                                         
      *                                                                         
           MOVE FSET1-FSET1K TO VSAM-KEY.                                       
      *                                                                         
           MOVE 'FSET1K' TO PATH-NAME.                                          
           MOVE 'FSETERM' TO FILE-NAME.                                         
           MOVE +100  TO FILE-LONG.                                             
           MOVE FSET1    TO Z-INOUT.                                            
      *                                                                         
       FIN-CLEF-FSET1K.  EXIT.                                                  
                EJECT                                                           
                                                                                
