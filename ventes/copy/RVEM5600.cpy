      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVEM5600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEM5600                         
      *   TABLE DES ENCAISSEMENTS VENDEURS                                      
      *---------------------------------------------------------                
      *                                                                         
       01 RVEM5600.                                                             
          02  EM56-NSOCIETE                                                     
              PIC   X(03).                                                      
          02  EM56-NLIEU                                                        
              PIC   X(03).                                                      
          02  EM56-DMOIS                                                        
              PIC   X(06).                                                      
          02  EM56-NBVENTES                                                     
              PIC   S9(07)    COMP-3.                                           
          02  EM56-NBENCAISS                                                    
              PIC   S9(07)    COMP-3.                                           
          02  EM56-NBVENCAISS                                                   
              PIC   S9(07)    COMP-3.                                           
          02  EM56-DSYST                                                        
              PIC   S9(13)    COMP-3.                                           
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA VUE RVEM5600                                    
      *---------------------------------------------------------                
      *                                                                         
       01 RVEM5600-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  EM56-NSOCIETE-F                                                   
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  EM56-NSOCIETE-F                                                   
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  EM56-NLIEU-F                                                      
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  EM56-NLIEU-F                                                      
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  EM56-DMOIS-F                                                      
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  EM56-DMOIS-F                                                      
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  EM56-NBVENTES-F                                                   
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  EM56-NBVENTES-F                                                   
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  EM56-NBENCAISS-F                                                  
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  EM56-NBENCAISS-F                                                  
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  EM56-NBVENCAISS-F                                                 
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  EM56-NBVENCAISS-F                                                 
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  EM56-DSYST-F                                                      
      *       PIC   S9(4) COMP.                                                 
      *                                                                         
      *--                                                                       
          02  EM56-DSYST-F                                                      
              PIC   S9(4) COMP-5.                                               
                                                                                
      *}                                                                        
