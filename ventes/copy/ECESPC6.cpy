      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:06 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREP�T        *
      * FICHIER.....: DECLARATION PRODUIT                             *
      * FICHIER.....: EMISSION COLIS ENT�TE SPECIFIQUE (MUTATION)     *
      * NOM FICHIER.: ECESPC6                                         *
      *---------------------------------------------------------------*
      * CR   .......: 27/08/2013                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 101                                             *
      *****************************************************************
      *
       01  ECESPC6.
      * CODE IDENTIFICATEUR
           05      ECESPC6-TYP-ENREG       PIC  X(0006).
      * CODE SOCI�T�
           05      ECESPC6-CSOCIETE        PIC  X(0005).
      * NUM�RO D'OP
           05      ECESPC6-NOP             PIC  X(0015).
      * NUM�RO DE COLIS
           05      ECESPC6-NCOLIS          PIC  X(0008).
      * POIDS TH�O BRUT
           05      ECESPC6-POIDS           PIC  9(0009).
      * CONTR�L� OUI / NON
           05      ECESPC6-ECONTROLE       PIC  X(0001).
      * NOM CONTR�LEUR
           05      ECESPC6-NOM-CONTROLEUR  PIC  X(0025).
      * FORC� OUI / NON
           05      ECESPC6-EFORCE          PIC  X(0001).
      * FILLER 30
           05      ECESPC6-FILLER          PIC  X(0030).
      * FIN
           05      ECESPC6-FIN             PIC  X(0001).
      
