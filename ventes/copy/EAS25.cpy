      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: Erx21                                                      00000020
      ***************************************************************** 00000030
       01   EAS25I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(5).                                       00000170
           02 MNDETCAMI OCCURS   14 TIMES .                             00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAMPAL      COMP PIC S9(4).                            00000190
      *--                                                                       
             03 MCAMPAL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCAMPAF      PIC X.                                     00000200
             03 FILLER  PIC X(4).                                       00000210
             03 MCAMPAI      PIC X(14).                                 00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MINFOL  COMP PIC S9(4).                                 00000230
      *--                                                                       
             03 MINFOL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MINFOF  PIC X.                                          00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MINFOI  PIC X(58).                                      00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MLIBERRI  PIC X(78).                                      00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000310
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000320
           02 FILLER    PIC X(4).                                       00000330
           02 MCODTRAI  PIC X(4).                                       00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000350
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MCICSI    PIC X(5).                                       00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MNETNAMI  PIC X(8).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MSCREENI  PIC X(4).                                       00000460
      ***************************************************************** 00000470
      * SDF: Erx21                                                      00000480
      ***************************************************************** 00000490
       01   EAS25O REDEFINES EAS25I.                                    00000500
           02 FILLER    PIC X(12).                                      00000510
           02 FILLER    PIC X(2).                                       00000520
           02 MDATJOUA  PIC X.                                          00000530
           02 MDATJOUC  PIC X.                                          00000540
           02 MDATJOUP  PIC X.                                          00000550
           02 MDATJOUH  PIC X.                                          00000560
           02 MDATJOUV  PIC X.                                          00000570
           02 MDATJOUO  PIC X(10).                                      00000580
           02 FILLER    PIC X(2).                                       00000590
           02 MTIMJOUA  PIC X.                                          00000600
           02 MTIMJOUC  PIC X.                                          00000610
           02 MTIMJOUP  PIC X.                                          00000620
           02 MTIMJOUH  PIC X.                                          00000630
           02 MTIMJOUV  PIC X.                                          00000640
           02 MTIMJOUO  PIC X(5).                                       00000650
           02 FILLER    PIC X(2).                                       00000660
           02 MPAGEA    PIC X.                                          00000670
           02 MPAGEC    PIC X.                                          00000680
           02 MPAGEP    PIC X.                                          00000690
           02 MPAGEH    PIC X.                                          00000700
           02 MPAGEV    PIC X.                                          00000710
           02 MPAGEO    PIC X(5).                                       00000720
           02 MNDETCAMO OCCURS   14 TIMES .                             00000730
             03 FILLER       PIC X(2).                                  00000740
             03 MCAMPAA      PIC X.                                     00000750
             03 MCAMPAC PIC X.                                          00000760
             03 MCAMPAP PIC X.                                          00000770
             03 MCAMPAH PIC X.                                          00000780
             03 MCAMPAV PIC X.                                          00000790
             03 MCAMPAO      PIC X(14).                                 00000800
             03 FILLER       PIC X(2).                                  00000810
             03 MINFOA  PIC X.                                          00000820
             03 MINFOC  PIC X.                                          00000830
             03 MINFOP  PIC X.                                          00000840
             03 MINFOH  PIC X.                                          00000850
             03 MINFOV  PIC X.                                          00000860
             03 MINFOO  PIC X(58).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MLIBERRA  PIC X.                                          00000890
           02 MLIBERRC  PIC X.                                          00000900
           02 MLIBERRP  PIC X.                                          00000910
           02 MLIBERRH  PIC X.                                          00000920
           02 MLIBERRV  PIC X.                                          00000930
           02 MLIBERRO  PIC X(78).                                      00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MCODTRAA  PIC X.                                          00000960
           02 MCODTRAC  PIC X.                                          00000970
           02 MCODTRAP  PIC X.                                          00000980
           02 MCODTRAH  PIC X.                                          00000990
           02 MCODTRAV  PIC X.                                          00001000
           02 MCODTRAO  PIC X(4).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MCICSA    PIC X.                                          00001030
           02 MCICSC    PIC X.                                          00001040
           02 MCICSP    PIC X.                                          00001050
           02 MCICSH    PIC X.                                          00001060
           02 MCICSV    PIC X.                                          00001070
           02 MCICSO    PIC X(5).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MNETNAMA  PIC X.                                          00001100
           02 MNETNAMC  PIC X.                                          00001110
           02 MNETNAMP  PIC X.                                          00001120
           02 MNETNAMH  PIC X.                                          00001130
           02 MNETNAMV  PIC X.                                          00001140
           02 MNETNAMO  PIC X(8).                                       00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MSCREENA  PIC X.                                          00001170
           02 MSCREENC  PIC X.                                          00001180
           02 MSCREENP  PIC X.                                          00001190
           02 MSCREENH  PIC X.                                          00001200
           02 MSCREENV  PIC X.                                          00001210
           02 MSCREENO  PIC X(4).                                       00001220
                                                                                
