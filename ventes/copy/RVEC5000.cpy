      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * DCLGEN TABLE(RDPP.RTEC50)                                      *        
      *        LIBRARY(DSA024.DEVL.SOURCE(RVEC5000))                   *        
      *        LANGUAGE(COBOL)                                         *        
      *        APOST                                                   *        
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *        
      ******************************************************************        
       01  RVEC5000.                                                            
      *{ SQL-LUW-Rowid-Type                                                     
      *    10 EC50-REFID                USAGE SQL TYPE IS ROWID.                
      *--                                                                       
           10 EC50-REFID PIC X(40).                                             
      *}                                                                        
           10 EC50-SOURCE.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 EC50-SOURCE-LEN        PIC S9(4) USAGE COMP.                   
      *--                                                                       
              49 EC50-SOURCE-LEN        PIC S9(4) COMP-5.                       
      *}                                                                        
              49 EC50-SOURCE-TEXT       PIC X(32).                              
           10 EC50-REFERENCE.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 EC50-REFERENCE-LEN     PIC S9(4) USAGE COMP.                   
      *--                                                                       
              49 EC50-REFERENCE-LEN     PIC S9(4) COMP-5.                       
      *}                                                                        
              49 EC50-REFERENCE-TEXT    PIC X(32).                              
           10 EC50-DCREATION            PIC X(26).                              
           10 EC50-DMAJ                 PIC X(26).                              
           10 EC50-XDATA.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 EC50-XDATA-LEN         PIC S9(4) USAGE COMP.                   
      *--                                                                       
              49 EC50-XDATA-LEN         PIC S9(4) COMP-5.                       
      *}                                                                        
              49 EC50-XDATA-TEXT        PIC X(3936).                            
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 6       *        
      ******************************************************************        
                                                                                
