      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TSL05 (MENU)             TR: SL05  *    00002200
      *                                                            *    00002900
      *           POUR L'ADMINISTATION DES DONNEES                 *    00003000
      **************************************************************    00003100
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00003200
      **************************************************************    00003300
      *                                                                 00003400
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00003500
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00003600
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00003700
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00003800
      *                                                                 00003900
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00004000
      * COMPRENANT :                                                    00004100
      * 1 - LES ZONES RESERVEES A AIDA                                  00004200
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00004300
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00004400
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00004500
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00004600
      *                                                                 00004700
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00004800
      * PAR AIDA                                                        00004900
      *                                                                 00005000
      *-------------------------------------------------------------    00005100
      *                                                                 00005200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-SL05-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00005300
      *--                                                                       
       01  COM-SL05-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00005400
       01  Z-COMMAREA.                                                  00005500
      *                                                                 00005600
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00005700
          02 FILLER-COM-AIDA                  PIC X(100).               00005800
      *                                                                 00005900
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00006000
          02 COMM-CICS-APPLID                 PIC X(8).                 00006100
          02 COMM-CICS-NETNAM                 PIC X(8).                 00006200
          02 COMM-CICS-TRANSA                 PIC X(4).                 00006300
      *                                                                 00006400
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00006500
          02 COMM-DATE-SIECLE                 PIC XX.                   00006600
          02 COMM-DATE-ANNEE                  PIC XX.                   00006700
          02 COMM-DATE-MOIS                   PIC XX.                   00006800
          02 COMM-DATE-JOUR                   PIC XX.                   00006900
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00007000
          02 COMM-DATE-QNTA                   PIC 999.                  00007100
          02 COMM-DATE-QNT0                   PIC 99999.                00007200
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00007300
          02 COMM-DATE-BISX                   PIC 9.                    00007400
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00007500
          02 COMM-DATE-JSM                    PIC 9.                    00007600
      *   LIBELLES DU JOUR COURT - LONG                                 00007700
          02 COMM-DATE-JSM-LC                 PIC XXX.                  00007800
          02 COMM-DATE-JSM-LL                 PIC XXXXXXXX.             00007900
      *   LIBELLES DU MOIS COURT - LONG                                 00008000
          02 COMM-DATE-MOIS-LC                PIC XXX.                  00008100
          02 COMM-DATE-MOIS-LL                PIC XXXXXXXX.             00008200
      *   DIFFERENTES FORMES DE DATE                                    00008300
          02 COMM-DATE-SSAAMMJJ               PIC X(8).                 00008400
          02 COMM-DATE-AAMMJJ                 PIC X(6).                 00008500
          02 COMM-DATE-JJMMSSAA               PIC X(8).                 00008600
          02 COMM-DATE-JJMMAA                 PIC X(6).                 00008700
          02 COMM-DATE-JJ-MM-AA               PIC X(8).                 00008800
          02 COMM-DATE-JJ-MM-SSAA             PIC X(10).                00008900
      *   DIFFERENTES FORMES DE DATE                                    00009000
          02 COMM-DATE-SEMSS                  PIC X(02).                00009100
          02 COMM-DATE-SEMAA                  PIC X(02).                00009100
          02 COMM-DATE-SEMNU                  PIC X(02).                00009100
          02 COMM-DATE-FILLER                 PIC X(08).                00009100
      *                                                                 00009200
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00009300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS                   PIC S9(4) COMP VALUE -1.  00009400
      *--                                                                       
          02 COMM-SWAP-CURS                   PIC S9(4) COMP-5 VALUE -1.        
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150        PIC X(1).                 00009500
      *                                                                 00009600
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00010000
      *                                                                 00410100
      *            TRANSACTION SL05 : ADMINISTRATION DES DONNEES      * 00411000
      *                                                                         
3724      02 COMM-SL05-APPLI.                                           00420000
      *                                                                 00412000
                05 COMM-SL05-GROUPE           PIC X(01).                        
                   88 TOP-GROUPE   VALUE 'O'.                                   
                05 COMM-SL05-DOMAINE          PIC X(05).                        
                05 COMM-SL05-SLENC.                                             
                   10 COMM-SL05-NSTEENC       PIC X(03).                        
                   10 COMM-SL05-NLIEUENC      PIC X(03).                        
                05 COMM-SL05-CPTR             PIC X(02).                        
                05 COMM-SL05-REF              PIC X(20).                        
                05 COMM-SL05-DEFFET           PIC X(10).                        
                05 COMM-SL05-DEFFET8          PIC X(08).                        
                05 COMM-SL05-DSAISIE          PIC X(10).                        
                05 COMM-SL05-DSAISIE8         PIC X(08).                        
                05 COMM-SL05-REF2             PIC X(20).                        
                05 COMM-SL05-STELIEUS.                                          
                   10 COMM-SL05-STES          PIC X(03).                        
                   10 COMM-SL05-LIEUS         PIC X(03).                        
                05 COMM-SL05-CACID            PIC X(08).                        
                05 COMM-SL05-LIBERR           PIC X(61).                        
                05 COMM-SL05-NSEQ             PIC X(03).                        
                05 COMM-SL05-FILLER              PIC X(3556).           00520000
      *                                                                         
      ***************************************************************** 00740000
                                                                                
