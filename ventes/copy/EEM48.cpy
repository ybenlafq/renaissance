      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: Eem48   Eem48                                              00000020
      ***************************************************************** 00000030
       01   EEM48I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * date du jour                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * heure du jour                                                   00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * numero de page en cours                                         00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOPAGEL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MNOPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOPAGEF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MNOPAGEI  PIC X(3).                                       00000200
      * nombre de pages                                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGEL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPAGEF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBPAGEI  PIC X(3).                                       00000250
      * n� de societe                                                   00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MNSOCI    PIC X(3).                                       00000300
      * n� de magasin                                                   00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MNMAGI    PIC X(3).                                       00000350
      * n� de caisse                                                    00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCAISL   COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MNCAISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCAISF   PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MNCAISI   PIC X(3).                                       00000400
      * date transaction                                                00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDTRANSL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDTRANSF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDTRANSI  PIC X(10).                                      00000450
      * n� operateur                                                    00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOPERL   COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MNOPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNOPERF   PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MNOPERI   PIC X(7).                                       00000500
      * n� de transaction                                               00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTRANSL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MNTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNTRANSF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MNTRANSI  PIC X(8).                                       00000550
      * heure transaction                                               00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDHTRANSL      COMP PIC S9(4).                            00000570
      *--                                                                       
           02 MDHTRANSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDHTRANSF      PIC X.                                     00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MDHTRANSI      PIC X(5).                                  00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEVISEL  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MDEVISEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEVISEF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MDEVISEI  PIC X(4).                                       00000640
      * type transaction                                                00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTTRANSL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MTTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTTRANSF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MTTRANSI  PIC X(3).                                       00000690
      * libelle type transaction                                        00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETATL   COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLETATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLETATF   PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLETATI   PIC X(27).                                      00000740
      * mode de paiement                                                00000750
           02 MPAID OCCURS   20 TIMES .                                 00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPAIL   COMP PIC S9(4).                                 00000770
      *--                                                                       
             03 MPAIL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPAIF   PIC X.                                          00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MPAII   PIC X.                                          00000800
      * libelle mode de paiement                                        00000810
           02 MLPAID OCCURS   20 TIMES .                                00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPAIL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MLPAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLPAIF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MLPAII  PIC X(11).                                      00000860
      * Montant saisi                                                   00000870
           02 MPMTSAISD OCCURS   20 TIMES .                             00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPMTSAISL    COMP PIC S9(4).                            00000890
      *--                                                                       
             03 MPMTSAISL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPMTSAISF    PIC X.                                     00000900
             03 FILLER  PIC X(4).                                       00000910
             03 MPMTSAISI    PIC X(10).                                 00000920
      * Montant ecart                                                   00000930
           02 MPMTECARD OCCURS   20 TIMES .                             00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPMTECARL    COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MPMTECARL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MPMTECARF    PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MPMTECARI    PIC X(10).                                 00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 FILLER    COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 FILLER COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 FILLER    PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 FILLER    PIC X(5).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MZONCMDI  PIC X(15).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MLIBERRI  PIC X(58).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCODTRAI  PIC X(4).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MNETNAMI  PIC X(8).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MSCREENI  PIC X(4).                                       00001260
      ***************************************************************** 00001270
      * SDF: Eem48   Eem48                                              00001280
      ***************************************************************** 00001290
       01   EEM48O REDEFINES EEM48I.                                    00001300
           02 FILLER    PIC X(12).                                      00001310
      * date du jour                                                    00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MDATJOUA  PIC X.                                          00001340
           02 MDATJOUC  PIC X.                                          00001350
           02 MDATJOUP  PIC X.                                          00001360
           02 MDATJOUH  PIC X.                                          00001370
           02 MDATJOUV  PIC X.                                          00001380
           02 MDATJOUO  PIC X(10).                                      00001390
      * heure du jour                                                   00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MTIMJOUA  PIC X.                                          00001420
           02 MTIMJOUC  PIC X.                                          00001430
           02 MTIMJOUP  PIC X.                                          00001440
           02 MTIMJOUH  PIC X.                                          00001450
           02 MTIMJOUV  PIC X.                                          00001460
           02 MTIMJOUO  PIC X(5).                                       00001470
      * numero de page en cours                                         00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNOPAGEA  PIC X.                                          00001500
           02 MNOPAGEC  PIC X.                                          00001510
           02 MNOPAGEP  PIC X.                                          00001520
           02 MNOPAGEH  PIC X.                                          00001530
           02 MNOPAGEV  PIC X.                                          00001540
           02 MNOPAGEO  PIC X(3).                                       00001550
      * nombre de pages                                                 00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MNBPAGEA  PIC X.                                          00001580
           02 MNBPAGEC  PIC X.                                          00001590
           02 MNBPAGEP  PIC X.                                          00001600
           02 MNBPAGEH  PIC X.                                          00001610
           02 MNBPAGEV  PIC X.                                          00001620
           02 MNBPAGEO  PIC X(3).                                       00001630
      * n� de societe                                                   00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MNSOCA    PIC X.                                          00001660
           02 MNSOCC    PIC X.                                          00001670
           02 MNSOCP    PIC X.                                          00001680
           02 MNSOCH    PIC X.                                          00001690
           02 MNSOCV    PIC X.                                          00001700
           02 MNSOCO    PIC X(3).                                       00001710
      * n� de magasin                                                   00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MNMAGA    PIC X.                                          00001740
           02 MNMAGC    PIC X.                                          00001750
           02 MNMAGP    PIC X.                                          00001760
           02 MNMAGH    PIC X.                                          00001770
           02 MNMAGV    PIC X.                                          00001780
           02 MNMAGO    PIC X(3).                                       00001790
      * n� de caisse                                                    00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MNCAISA   PIC X.                                          00001820
           02 MNCAISC   PIC X.                                          00001830
           02 MNCAISP   PIC X.                                          00001840
           02 MNCAISH   PIC X.                                          00001850
           02 MNCAISV   PIC X.                                          00001860
           02 MNCAISO   PIC X(3).                                       00001870
      * date transaction                                                00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MDTRANSA  PIC X.                                          00001900
           02 MDTRANSC  PIC X.                                          00001910
           02 MDTRANSP  PIC X.                                          00001920
           02 MDTRANSH  PIC X.                                          00001930
           02 MDTRANSV  PIC X.                                          00001940
           02 MDTRANSO  PIC X(10).                                      00001950
      * n� operateur                                                    00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNOPERA   PIC X.                                          00001980
           02 MNOPERC   PIC X.                                          00001990
           02 MNOPERP   PIC X.                                          00002000
           02 MNOPERH   PIC X.                                          00002010
           02 MNOPERV   PIC X.                                          00002020
           02 MNOPERO   PIC X(7).                                       00002030
      * n� de transaction                                               00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MNTRANSA  PIC X.                                          00002060
           02 MNTRANSC  PIC X.                                          00002070
           02 MNTRANSP  PIC X.                                          00002080
           02 MNTRANSH  PIC X.                                          00002090
           02 MNTRANSV  PIC X.                                          00002100
           02 MNTRANSO  PIC X(8).                                       00002110
      * heure transaction                                               00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MDHTRANSA      PIC X.                                     00002140
           02 MDHTRANSC PIC X.                                          00002150
           02 MDHTRANSP PIC X.                                          00002160
           02 MDHTRANSH PIC X.                                          00002170
           02 MDHTRANSV PIC X.                                          00002180
           02 MDHTRANSO      PIC X(5).                                  00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MDEVISEA  PIC X.                                          00002210
           02 MDEVISEC  PIC X.                                          00002220
           02 MDEVISEP  PIC X.                                          00002230
           02 MDEVISEH  PIC X.                                          00002240
           02 MDEVISEV  PIC X.                                          00002250
           02 MDEVISEO  PIC X(4).                                       00002260
      * type transaction                                                00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MTTRANSA  PIC X.                                          00002290
           02 MTTRANSC  PIC X.                                          00002300
           02 MTTRANSP  PIC X.                                          00002310
           02 MTTRANSH  PIC X.                                          00002320
           02 MTTRANSV  PIC X.                                          00002330
           02 MTTRANSO  PIC X(3).                                       00002340
      * libelle type transaction                                        00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MLETATA   PIC X.                                          00002370
           02 MLETATC   PIC X.                                          00002380
           02 MLETATP   PIC X.                                          00002390
           02 MLETATH   PIC X.                                          00002400
           02 MLETATV   PIC X.                                          00002410
           02 MLETATO   PIC X(27).                                      00002420
      * mode de paiement                                                00002430
           02 DFHMS1 OCCURS   20 TIMES .                                00002440
             03 FILLER       PIC X(2).                                  00002450
             03 MPAIA   PIC X.                                          00002460
             03 MPAIC   PIC X.                                          00002470
             03 MPAIP   PIC X.                                          00002480
             03 MPAIH   PIC X.                                          00002490
             03 MPAIV   PIC X.                                          00002500
             03 MPAIO   PIC X.                                          00002510
      * libelle mode de paiement                                        00002520
           02 DFHMS2 OCCURS   20 TIMES .                                00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MLPAIA  PIC X.                                          00002550
             03 MLPAIC  PIC X.                                          00002560
             03 MLPAIP  PIC X.                                          00002570
             03 MLPAIH  PIC X.                                          00002580
             03 MLPAIV  PIC X.                                          00002590
             03 MLPAIO  PIC X(11).                                      00002600
      * Montant saisi                                                   00002610
           02 DFHMS3 OCCURS   20 TIMES .                                00002620
             03 FILLER       PIC X(2).                                  00002630
             03 MPMTSAISA    PIC X.                                     00002640
             03 MPMTSAISC    PIC X.                                     00002650
             03 MPMTSAISP    PIC X.                                     00002660
             03 MPMTSAISH    PIC X.                                     00002670
             03 MPMTSAISV    PIC X.                                     00002680
             03 MPMTSAISO    PIC X(10).                                 00002690
      * Montant ecart                                                   00002700
           02 DFHMS4 OCCURS   20 TIMES .                                00002710
             03 FILLER       PIC X(2).                                  00002720
             03 MPMTECARA    PIC X.                                     00002730
             03 MPMTECARC    PIC X.                                     00002740
             03 MPMTECARP    PIC X.                                     00002750
             03 MPMTECARH    PIC X.                                     00002760
             03 MPMTECARV    PIC X.                                     00002770
             03 MPMTECARO    PIC X(10).                                 00002780
           02 FILLER    PIC X(2).                                       00002790
           02 FILLER    PIC X.                                          00002800
           02 FILLER    PIC X.                                          00002810
           02 FILLER    PIC X.                                          00002820
           02 FILLER    PIC X.                                          00002830
           02 FILLER    PIC X.                                          00002840
           02 FILLER    PIC X(5).                                       00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MZONCMDA  PIC X.                                          00002870
           02 MZONCMDC  PIC X.                                          00002880
           02 MZONCMDP  PIC X.                                          00002890
           02 MZONCMDH  PIC X.                                          00002900
           02 MZONCMDV  PIC X.                                          00002910
           02 MZONCMDO  PIC X(15).                                      00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MLIBERRA  PIC X.                                          00002940
           02 MLIBERRC  PIC X.                                          00002950
           02 MLIBERRP  PIC X.                                          00002960
           02 MLIBERRH  PIC X.                                          00002970
           02 MLIBERRV  PIC X.                                          00002980
           02 MLIBERRO  PIC X(58).                                      00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCODTRAA  PIC X.                                          00003010
           02 MCODTRAC  PIC X.                                          00003020
           02 MCODTRAP  PIC X.                                          00003030
           02 MCODTRAH  PIC X.                                          00003040
           02 MCODTRAV  PIC X.                                          00003050
           02 MCODTRAO  PIC X(4).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MCICSA    PIC X.                                          00003080
           02 MCICSC    PIC X.                                          00003090
           02 MCICSP    PIC X.                                          00003100
           02 MCICSH    PIC X.                                          00003110
           02 MCICSV    PIC X.                                          00003120
           02 MCICSO    PIC X(5).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MNETNAMA  PIC X.                                          00003150
           02 MNETNAMC  PIC X.                                          00003160
           02 MNETNAMP  PIC X.                                          00003170
           02 MNETNAMH  PIC X.                                          00003180
           02 MNETNAMV  PIC X.                                          00003190
           02 MNETNAMO  PIC X(8).                                       00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MSCREENA  PIC X.                                          00003220
           02 MSCREENC  PIC X.                                          00003230
           02 MSCREENP  PIC X.                                          00003240
           02 MSCREENH  PIC X.                                          00003250
           02 MSCREENV  PIC X.                                          00003260
           02 MSCREENO  PIC X(4).                                       00003270
                                                                                
