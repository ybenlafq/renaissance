      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ELT00   ELT00                                              00000020
      ***************************************************************** 00000030
       01   ELT00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(15).                                      00000170
      * nom de la lettre                                                00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETTREL  COMP PIC S9(4).                                 00000190
      *--                                                                       
           02 MLETTREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLETTREF  PIC X.                                          00000200
           02 FILLER    PIC X(4).                                       00000210
           02 MLETTREI  PIC X(6).                                       00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBL     COMP PIC S9(4).                                 00000230
      *--                                                                       
           02 MLIBL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MLIBF     PIC X.                                          00000240
           02 FILLER    PIC X(4).                                       00000250
           02 MLIBI     PIC X(20).                                      00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMODELL   COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MMODELL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MMODELF   PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MMODELI   PIC X(6).                                       00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLETTCOPL      COMP PIC S9(4).                            00000310
      *--                                                                       
           02 MLETTCOPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLETTCOPF      PIC X.                                     00000320
           02 FILLER    PIC X(4).                                       00000330
           02 MLETTCOPI      PIC X(6).                                  00000340
      * code imprimante                                                 00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODIMPL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MCODIMPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODIMPF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MCODIMPI  PIC X(4).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MLIBERRI  PIC X(78).                                      00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MCODTRAI  PIC X(4).                                       00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MCICSI    PIC X(5).                                       00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MNETNAMI  PIC X(8).                                       00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MSCREENI  PIC X(4).                                       00000590
      ***************************************************************** 00000600
      * SDF: ELT00   ELT00                                              00000610
      ***************************************************************** 00000620
       01   ELT00O REDEFINES ELT00I.                                    00000630
           02 FILLER    PIC X(12).                                      00000640
           02 FILLER    PIC X(2).                                       00000650
           02 MDATJOUA  PIC X.                                          00000660
           02 MDATJOUC  PIC X.                                          00000670
           02 MDATJOUP  PIC X.                                          00000680
           02 MDATJOUH  PIC X.                                          00000690
           02 MDATJOUV  PIC X.                                          00000700
           02 MDATJOUO  PIC X(10).                                      00000710
           02 FILLER    PIC X(2).                                       00000720
           02 MTIMJOUA  PIC X.                                          00000730
           02 MTIMJOUC  PIC X.                                          00000740
           02 MTIMJOUP  PIC X.                                          00000750
           02 MTIMJOUH  PIC X.                                          00000760
           02 MTIMJOUV  PIC X.                                          00000770
           02 MTIMJOUO  PIC X(5).                                       00000780
           02 FILLER    PIC X(2).                                       00000790
           02 MZONCMDA  PIC X.                                          00000800
           02 MZONCMDC  PIC X.                                          00000810
           02 MZONCMDP  PIC X.                                          00000820
           02 MZONCMDH  PIC X.                                          00000830
           02 MZONCMDV  PIC X.                                          00000840
           02 MZONCMDO  PIC X(15).                                      00000850
      * nom de la lettre                                                00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MLETTREA  PIC X.                                          00000880
           02 MLETTREC  PIC X.                                          00000890
           02 MLETTREP  PIC X.                                          00000900
           02 MLETTREH  PIC X.                                          00000910
           02 MLETTREV  PIC X.                                          00000920
           02 MLETTREO  PIC X(6).                                       00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MLIBA     PIC X.                                          00000950
           02 MLIBC     PIC X.                                          00000960
           02 MLIBP     PIC X.                                          00000970
           02 MLIBH     PIC X.                                          00000980
           02 MLIBV     PIC X.                                          00000990
           02 MLIBO     PIC X(20).                                      00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MMODELA   PIC X.                                          00001020
           02 MMODELC   PIC X.                                          00001030
           02 MMODELP   PIC X.                                          00001040
           02 MMODELH   PIC X.                                          00001050
           02 MMODELV   PIC X.                                          00001060
           02 MMODELO   PIC X(6).                                       00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MLETTCOPA      PIC X.                                     00001090
           02 MLETTCOPC PIC X.                                          00001100
           02 MLETTCOPP PIC X.                                          00001110
           02 MLETTCOPH PIC X.                                          00001120
           02 MLETTCOPV PIC X.                                          00001130
           02 MLETTCOPO      PIC X(6).                                  00001140
      * code imprimante                                                 00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MCODIMPA  PIC X.                                          00001170
           02 MCODIMPC  PIC X.                                          00001180
           02 MCODIMPP  PIC X.                                          00001190
           02 MCODIMPH  PIC X.                                          00001200
           02 MCODIMPV  PIC X.                                          00001210
           02 MCODIMPO  PIC X(4).                                       00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MLIBERRA  PIC X.                                          00001240
           02 MLIBERRC  PIC X.                                          00001250
           02 MLIBERRP  PIC X.                                          00001260
           02 MLIBERRH  PIC X.                                          00001270
           02 MLIBERRV  PIC X.                                          00001280
           02 MLIBERRO  PIC X(78).                                      00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MCODTRAA  PIC X.                                          00001310
           02 MCODTRAC  PIC X.                                          00001320
           02 MCODTRAP  PIC X.                                          00001330
           02 MCODTRAH  PIC X.                                          00001340
           02 MCODTRAV  PIC X.                                          00001350
           02 MCODTRAO  PIC X(4).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MCICSA    PIC X.                                          00001380
           02 MCICSC    PIC X.                                          00001390
           02 MCICSP    PIC X.                                          00001400
           02 MCICSH    PIC X.                                          00001410
           02 MCICSV    PIC X.                                          00001420
           02 MCICSO    PIC X(5).                                       00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MNETNAMA  PIC X.                                          00001450
           02 MNETNAMC  PIC X.                                          00001460
           02 MNETNAMP  PIC X.                                          00001470
           02 MNETNAMH  PIC X.                                          00001480
           02 MNETNAMV  PIC X.                                          00001490
           02 MNETNAMO  PIC X(8).                                       00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MSCREENA  PIC X.                                          00001520
           02 MSCREENC  PIC X.                                          00001530
           02 MSCREENP  PIC X.                                          00001540
           02 MSCREENH  PIC X.                                          00001550
           02 MSCREENV  PIC X.                                          00001560
           02 MSCREENO  PIC X(4).                                       00001570
                                                                                
