      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RTSV03                        *        
      ******************************************************************        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVSV0300.                                                            
      *    *************************************************************        
      *                       CODE CHASSIS                                      
           10 SV03-CCHASSIS        PIC X(7).                                    
      *    *************************************************************        
      *                       CODE GROUPE                                       
           10 SV03-CGROUPE         PIC X(5).                                    
      *    *************************************************************        
      *                       LIBELLE CHASSIS                                   
           10 SV03-LCHASSIS        PIC X(20).                                   
      *    *************************************************************        
      *                       FAMILLE PRINCIPALE                                
           10 SV03-CFAMCH          PIC X(5).                                    
      *    *************************************************************        
      *                       DATE CREATION                                     
           10 SV03-DCHASSIS        PIC X(8).                                    
      *    *************************************************************        
      *                       DATE SYSTEME                                      
           10 SV03-DSYST           PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 5       *        
      ******************************************************************        
       01  RVSV0200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SV02-CCHASSIS-F PIC S9(4) COMP.                                   
      *--                                                                       
           10 SV02-CCHASSIS-F PIC S9(4) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SV02-CGROUPE-F  PIC S9(4) COMP.                                   
      *--                                                                       
           10 SV02-CGROUPE-F  PIC S9(4) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SV02-LCHASSIS-F PIC S9(4) COMP.                                   
      *--                                                                       
           10 SV02-LCHASSIS-F PIC S9(4) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SV02-CFAMCH-F   PIC S9(4) COMP.                                   
      *--                                                                       
           10 SV02-CFAMCH-F   PIC S9(4) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SV02-DCHASSIS-F PIC S9(4) COMP.                                   
      *--                                                                       
           10 SV02-DCHASSIS-F PIC S9(4) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SV02-DSYST-F    PIC S9(4) COMP.                                   
      *                                                                         
      *--                                                                       
           10 SV02-DSYST-F    PIC S9(4) COMP-5.                                 
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
