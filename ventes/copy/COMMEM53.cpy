      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
          01 COMM-EM53-DONNEES.                                         00010001
             10 COMM-EM53-NB-POSTES               PIC S9(09) COMP-3.    00020001
             10 COMM-EM53-CODRET                  PIC 9(03) COMP-3.     00030001
                88 COMM-EM53-ERREUR-MANIP         VALUE 999.            00040001
             10 COMM-EM53-LIBERR                  PIC X(25).            00050001
             10 COMM-EM53-CRITERES.                                     00060001
                20 COMM-EM53-NSOCIETE             PIC X(03).            00070001
                20 COMM-EM53-NLIEU                PIC X(03).            00080001
                20 COMM-EM53-DVENTE               PIC X(08).            00090001
                20 COMM-EM53-LNOM                 PIC X(25).            00100001
                20 COMM-EM53-CMARQ                PIC X(05).            00110001
                20 COMM-EM53-CFAM                 PIC X(05).            00120001
                20 COMM-EM53-DDEBUT               PIC X(08).            00130001
                20 COMM-EM53-DFIN                 PIC X(08).            00140001
                20 COMM-EM53-NCAISSE              PIC X(03).            00150001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         20 COMM-EM53-MONTANT              PIC S9(9)V99 COMP.    00160001
      *--                                                                       
                20 COMM-EM53-MONTANT              PIC S9(9)V99 COMP-5.          
      *}                                                                        
B&S   *         20 COMM-EM53-TPAIE                PIC X(01).            00170001
B&S             20 COMM-EM53-TPAIE                PIC X(05).            00180001
B&S   *         20 COMM-EM53-TTRANS               PIC X(01).            00190001
B&S             20 COMM-EM53-TTRANS               PIC X(03).            00200001
                20 COMM-EM53-NTRANS               PIC X(08).            00210001
B&S   * AJOUT DE LA ZONE 'MONTANT DE LA TRANSACTION'                    00220001
B&S             20 COMM-EM53-TRANS-MONTANT        PIC S9(9)V99.         00230001
                20 COMM-EM53-NCODIC               PIC X(07).            00240001
B&S   * AJOUT DE LA ZONE 'MODE DE DELIVRANCE'                           00250001
B&S             20 COMM-EM53-DELIVRANCE           PIC X(03).            00260001
JD              20 COMM-EM53-CDEVISE              PIC X(01).            00270001
JD              20 COMM-EM53-NVENTE               PIC X(07).            00280001
B&S             20 COMM-EM53-NEMPORT              PIC X(08).            00290001
B&S   * AJOUT DE LA ZONE 'MONTANT DE LA VENTE'                          00300001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
B&S   *         20 COMM-EM53-VENTE-MONTANT        PIC S9(9)V99 COMP.    00310001
      *--                                                                       
                20 COMM-EM53-VENTE-MONTANT        PIC S9(9)V99 COMP-5.          
      *}                                                                        
B&S   * MONTANT DU PAIEMENT                                             00320001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
JD    *         20 COMM-EM53-MTREGLEM             PIC S9(9)V99 COMP.    00330001
      *--                                                                       
                20 COMM-EM53-MTREGLEM             PIC S9(9)V99 COMP-5.          
      *}                                                                        
             10 COMM-EM53-TYPE-RECHERCHE          PIC X.                00340001
                88 COMM-EM53-PAS-RECH             VALUE '0'.            00350001
B&S             88 COMM-EM53-RECH-CODIC           VALUE '1'.            00360001
B&S             88 COMM-EM53-RECH-CODIC-DEL       VALUE '2'.            00370001
B&S             88 COMM-EM53-RECH-CODIC-DEL-NCAI  VALUE '3'.            00380001
B&S             88 COMM-EM53-RECH-CODIC-NCAI      VALUE '4'.            00390001
B&S             88 COMM-EM53-RECH-DEL             VALUE '5'.            00400001
B&S             88 COMM-EM53-RECH-DEL-DEV         VALUE '6'.            00410001
B&S             88 COMM-EM53-RECH-DEL-DEV-NCAI    VALUE '7'.            00420001
B&S             88 COMM-EM53-RECH-DEL-NCAI        VALUE '8'.            00430001
B&S             88 COMM-EM53-RECH-DEL-NCAI-PMONT  VALUE '9'.            00440001
B&S             88 COMM-EM53-RECH-DEL-NCAI-PVTE   VALUE 'A'.            00450001
B&S             88 COMM-EM53-RECH-DEV             VALUE 'B'.            00460001
B&S             88 COMM-EM53-RECH-DEV-MOD-PVTE    VALUE 'C'.            00470001
B&S             88 COMM-EM53-RECH-DEV-MOD-VTE     VALUE 'D'.            00480001
B&S             88 COMM-EM53-RECH-DEV-MOD-NC-PVTE VALUE 'E'.            00490001
B&S             88 COMM-EM53-RECH-DEV-NCAI        VALUE 'F'.            00500001
B&S             88 COMM-EM53-RECH-DEV-NCAI-VTEMOD VALUE 'G'.            00510001
B&S             88 COMM-EM53-RECH-DFIN            VALUE 'H'.            00520001
B&S             88 COMM-EM53-RECH-NCAI            VALUE 'I'.            00530001
B&S             88 COMM-EM53-RECH-NCAI-NTRANS     VALUE 'J'.            00540001
B&S             88 COMM-EM53-RECH-NCAI-NTRANS-PMO VALUE 'K'.            00550001
B&S             88 COMM-EM53-RECH-NCAI-NVTE       VALUE 'L'.            00560001
B&S             88 COMM-EM53-RECH-NCAI-PMONT      VALUE 'M'.            00570001
B&S             88 COMM-EM53-RECH-NCAI-PVTE       VALUE 'N'.            00580001
B&S             88 COMM-EM53-RECH-NTRANS          VALUE 'O'.            00590001
B&S             88 COMM-EM53-RECH-NTRANS-PMONT    VALUE 'P'.            00600001
B&S             88 COMM-EM53-RECH-NVTE            VALUE 'Q'.            00610001
B&S             88 COMM-EM53-RECH-PMONT           VALUE 'R'.            00620001
B&S             88 COMM-EM53-RECH-PVTE            VALUE 'S'.            00630001
B&S             88 COMM-EM53-RECH-PAS-CRITERE     VALUE 'Z'.            00640001
B&S          10 COMM-EM53-APVF                    PIC X(01).            00650001
                                                                                
