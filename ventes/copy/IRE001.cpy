      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRE001      *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,PD,A,                          *        
      *                           13,05,BI,A,                          *        
      *                           18,03,PD,A,                          *        
      *                           21,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRE001.                                                        
            05 NOMETAT-IRE001           PIC X(6) VALUE 'IRE001'.                
            05 RUPTURES-IRE001.                                                 
           10 IRE001-NLIEU              PIC X(03).                      007  003
           10 IRE001-WSEQFAM            PIC S9(05)      COMP-3.         010  003
           10 IRE001-CFAM               PIC X(05).                      013  005
           10 IRE001-PC-FORC-REM        PIC S9(03)V9(1) COMP-3.         018  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRE001-SEQUENCE           PIC S9(04) COMP.                021  002
      *--                                                                       
           10 IRE001-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRE001.                                                   
           10 IRE001-CMARQ              PIC X(05).                      023  005
           10 IRE001-EXCEPT             PIC X(01).                      028  001
           10 IRE001-LREFFOURN          PIC X(20).                      029  020
           10 IRE001-NCODIC             PIC X(07).                      049  007
           10 IRE001-ORIGINE            PIC X(09).                      056  009
           10 IRE001-REMARQUES          PIC X(09).                      065  009
           10 IRE001-VENDEUR            PIC X(06).                      074  006
           10 IRE001-PC-FORCAGE         PIC S9(02)V9(1) COMP-3.         080  002
           10 IRE001-PC-REMISE          PIC S9(02)V9(1) COMP-3.         082  002
           10 IRE001-PRIX-FORC          PIC S9(11)V9(2) COMP-3.         084  007
           10 IRE001-PRIX-THEOR         PIC S9(11)V9(2) COMP-3.         091  007
           10 IRE001-QTE                PIC S9(03)      COMP-3.         098  002
           10 IRE001-VAL-REMIS          PIC S9(11)V9(2) COMP-3.         100  007
            05 FILLER                      PIC X(406).                          
                                                                                
