      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRE002      *        
      *                                                                *        
      *          CRITERES DE TRI  07,02,BI,A,                          *        
      *                           09,03,BI,A,                          *        
      *                           12,03,PD,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,03,PD,A,                          *        
      *                           23,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRE002.                                                        
            05 NOMETAT-IRE002           PIC X(6) VALUE 'IRE002'.                
            05 RUPTURES-IRE002.                                                 
           10 IRE002-CGRPMAG            PIC X(02).                      007  002
           10 IRE002-NLIEU              PIC X(03).                      009  003
           10 IRE002-WSEQFAM            PIC S9(05)      COMP-3.         012  003
           10 IRE002-CFAM               PIC X(05).                      015  005
           10 IRE002-PC-FORC-REM        PIC S9(03)V9(2) COMP-3.         020  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRE002-SEQUENCE           PIC S9(04) COMP.                023  002
      *--                                                                       
           10 IRE002-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRE002.                                                   
           10 IRE002-CMARQ              PIC X(05).                      025  005
           10 IRE002-EXCEPT             PIC X(01).                      030  001
           10 IRE002-LREFFOURN          PIC X(20).                      031  020
           10 IRE002-NCLIENT            PIC X(20).                      051  020
           10 IRE002-NCODIC             PIC X(07).                      071  007
           10 IRE002-ORIGINE            PIC X(09).                      078  009
           10 IRE002-REMARQUES          PIC X(09).                      087  009
           10 IRE002-VENDEUR            PIC X(06).                      096  006
           10 IRE002-PC-FORCAGE         PIC S9(02)V9(1) COMP-3.         102  002
           10 IRE002-PC-REMISE          PIC S9(02)V9(1) COMP-3.         104  002
           10 IRE002-PRIX-FORC          PIC S9(11)V9(2) COMP-3.         106  007
           10 IRE002-PRIX-THEOR         PIC S9(11)V9(2) COMP-3.         113  007
           10 IRE002-QTE                PIC S9(03)      COMP-3.         120  002
           10 IRE002-VAL-REMIS          PIC S9(11)V9(2) COMP-3.         122  007
           10 IRE002-FMOIS              PIC X(08).                      129  008
           10 IRE002-JJMM               PIC X(08).                      137  008
            05 FILLER                      PIC X(368).                          
                                                                                
