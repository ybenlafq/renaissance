      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00010001
      *   COPY DE LA TABLE RVVT0100                                     00020001
      **********************************************************        00030001
      *                                                                 00040001
      *---------------------------------------------------------        00050001
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVT0100                 00060001
      *---------------------------------------------------------        00070001
      *                                                                 00080001
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVT0100.                                                    00090001
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVT0100.                                                            
      *}                                                                        
           02  VT01-NFLAG                                               00100001
               PIC X(0001).                                             00110001
           02  VT01-NCLUSTERP                                           00120001
               PIC X(0013).                                             00130001
           02  VT01-NSOCIETE                                            00140001
               PIC X(0003).                                             00150001
           02  VT01-NLIEU                                               00160001
               PIC X(0003).                                             00170001
           02  VT01-DVENTE                                              00180001
               PIC X(0008).                                             00190001
           02  VT01-NVENTE                                              00200001
               PIC X(0007).                                             00210001
           02  VT01-NCLUSTER                                            00220001
               PIC X(0013).                                             00230001
           02  VT01-LNOM                                                00240001
               PIC X(0025).                                             00250001
           02  VT01-CFAM                                                00260001
               PIC X(0005).                                             00270001
           02  VT01-CMARQ                                               00280001
               PIC X(0005).                                             00290001
           02  VT01-TELDOM                                              00300001
               PIC X(0010).                                             00310001
           02  VT01-TELBUR                                              00320001
               PIC X(0010).                                             00330001
           02  VT01-NTYPVENTE                                           00340001
               PIC X(0003).                                             00350001
           02  VT01-NCODIC                                              00360001
               PIC X(0007).                                             00370001
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00380001
      *---------------------------------------------------------        00390001
      *   LISTE DES FLAGS DE LA TABLE RVVT0100                          00400001
      *---------------------------------------------------------        00410001
      *                                                                 00420001
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVT0100-FLAGS.                                              00430001
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVT0100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT01-NFLAG-F                                             00440001
      *        PIC S9(4) COMP.                                          00450001
      *--                                                                       
           02  VT01-NFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT01-NCLUSTERP-F                                         00460001
      *        PIC S9(4) COMP.                                          00470001
      *--                                                                       
           02  VT01-NCLUSTERP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT01-NSOCIETE-F                                          00480001
      *        PIC S9(4) COMP.                                          00490001
      *--                                                                       
           02  VT01-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT01-NLIEU-F                                             00500001
      *        PIC S9(4) COMP.                                          00510001
      *--                                                                       
           02  VT01-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT01-DVENTE-F                                            00520001
      *        PIC S9(4) COMP.                                          00530001
      *--                                                                       
           02  VT01-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT01-NVENTE-F                                            00540001
      *        PIC S9(4) COMP.                                          00550001
      *--                                                                       
           02  VT01-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT01-NCLUSTER-F                                          00560001
      *        PIC S9(4) COMP.                                          00570001
      *--                                                                       
           02  VT01-NCLUSTER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT01-LNOM-F                                              00580001
      *        PIC S9(4) COMP.                                          00590001
      *--                                                                       
           02  VT01-LNOM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT01-CFAM-F                                              00600001
      *        PIC S9(4) COMP.                                          00610001
      *--                                                                       
           02  VT01-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT01-CMARQ-F                                             00620001
      *        PIC S9(4) COMP.                                          00630001
      *--                                                                       
           02  VT01-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT01-TELDOM-F                                            00640001
      *        PIC S9(4) COMP.                                          00650001
      *--                                                                       
           02  VT01-TELDOM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT01-TELBUR-F                                            00660001
      *        PIC S9(4) COMP.                                          00670001
      *--                                                                       
           02  VT01-TELBUR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT01-NTYPVENTE-F                                         00680001
      *        PIC S9(4) COMP.                                          00690001
      *--                                                                       
           02  VT01-NTYPVENTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT01-NCODIC-F                                            00700001
      *        PIC S9(4) COMP.                                          00710001
      *--                                                                       
           02  VT01-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                            00720001
                                                                                
