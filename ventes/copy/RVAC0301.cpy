      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RTAC03                        *        
      ******************************************************************        
       01  RVAC0301.                                                            
           10 AC03-NSOCIETE             PIC X(3).                               
           10 AC03-NLIEU                PIC X(3).                               
           10 AC03-DVENTE               PIC X(8).                               
           10 AC03-HVENTE               PIC X(6).                               
           10 AC03-QENTRE               PIC S9(5)V USAGE COMP-3.                
           10 AC03-DSYST                PIC S9(13)V USAGE COMP-3.               
           10 AC03-QSTATUT              PIC X(2).                               
           10 AC03-QBOUCHON             PIC X(2).                               
           10 AC03-WPRESENCE            PIC X(1).                               
           10 AC03-WMODIF               PIC X(1).                               
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVAC0100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVAC0301-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AC03-NSOCIETE-F           PIC S9(4) COMP.                         
      *--                                                                       
           10 AC03-NSOCIETE-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AC03-NLIEU-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 AC03-NLIEU-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AC03-DVENTE-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 AC03-DVENTE-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AC03-HVENTE-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 AC03-HVENTE-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AC03-QENTRE-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 AC03-QENTRE-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AC03-DSYST-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 AC03-DSYST-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AC03-QSTATUT-F            PIC S9(4) COMP.                         
      *--                                                                       
           10 AC03-QSTATUT-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AC03-QBOUCHON-F           PIC S9(4) COMP.                         
      *--                                                                       
           10 AC03-QBOUCHON-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AC03-WPRESENCE-F          PIC S9(4) COMP.                         
      *--                                                                       
           10 AC03-WPRESENCE-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AC03-WMODIF-F             PIC S9(4) COMP.                         
      *                                                                         
      *--                                                                       
           10 AC03-WMODIF-F             PIC S9(4) COMP-5.                       
                                                                                
      *}                                                                        
