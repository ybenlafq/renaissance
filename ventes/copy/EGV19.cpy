      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV19   EGV19                                              00000020
      ***************************************************************** 00000030
       01   EGV19I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNUMPAGEI      PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICSELL    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNCODICSELL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNCODICSELF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICSELI    PIC X(7).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCMARQI   PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLREFFOURNI    PIC X(20).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTCDESELL   COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNENTCDESELL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNENTCDESELF   PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNENTCDESELI   PIC X(5).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTCDESELL   COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLENTCDESELL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLENTCDESELF   PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLENTCDESELI   PIC X(20).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTPRESTSELL   COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MCTPRESTSELL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCTPRESTSELF   PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCTPRESTSELI   PIC X(5).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTPRESTSELL   COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MLTPRESTSELL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLTPRESTSELF   PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLTPRESTSELI   PIC X(20).                                 00000530
           02 MTAB1I OCCURS   12 TIMES .                                00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPPRESTL  COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCTYPPRESTL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MCTYPPRESTF  PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCTYPPRESTI  PIC X(5).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPRESTATIONL     COMP PIC S9(4).                       00000590
      *--                                                                       
             03 MCPRESTATIONL COMP-5 PIC S9(4).                                 
      *}                                                                        
             03 MCPRESTATIONF     PIC X.                                00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCPRESTATIONI     PIC X(5).                             00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSELECTL    COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MWSELECTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWSELECTF    PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MWSELECTI    PIC X.                                     00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPRESTATIONL     COMP PIC S9(4).                       00000670
      *--                                                                       
             03 MLPRESTATIONL COMP-5 PIC S9(4).                                 
      *}                                                                        
             03 MLPRESTATIONF     PIC X.                                00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLPRESTATIONI     PIC X(20).                            00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTCDEL    COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MNENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNENTCDEF    PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MNENTCDEI    PIC X(5).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWGROUPEL    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MWGROUPEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWGROUPEF    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MWGROUPEI    PIC X.                                     00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPRESTGRPL  COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MCPRESTGRPL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MCPRESTGRPF  PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCPRESTGRPI  PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWPRODMAJL   COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MWPRODMAJL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWPRODMAJF   PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MWPRODMAJI   PIC X.                                     00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTPRESTL    COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MMTPRESTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MMTPRESTF    PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MMTPRESTI    PIC X(9).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(15).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(58).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * SDF: EGV19   EGV19                                              00001160
      ***************************************************************** 00001170
       01   EGV19O REDEFINES EGV19I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MNUMPAGEA      PIC X.                                     00001350
           02 MNUMPAGEC PIC X.                                          00001360
           02 MNUMPAGEP PIC X.                                          00001370
           02 MNUMPAGEH PIC X.                                          00001380
           02 MNUMPAGEV PIC X.                                          00001390
           02 MNUMPAGEO      PIC X(2).                                  00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MPAGEMAXA      PIC X.                                     00001420
           02 MPAGEMAXC PIC X.                                          00001430
           02 MPAGEMAXP PIC X.                                          00001440
           02 MPAGEMAXH PIC X.                                          00001450
           02 MPAGEMAXV PIC X.                                          00001460
           02 MPAGEMAXO      PIC X(2).                                  00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MNCODICSELA    PIC X.                                     00001490
           02 MNCODICSELC    PIC X.                                     00001500
           02 MNCODICSELP    PIC X.                                     00001510
           02 MNCODICSELH    PIC X.                                     00001520
           02 MNCODICSELV    PIC X.                                     00001530
           02 MNCODICSELO    PIC X(7).                                  00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MCMARQA   PIC X.                                          00001560
           02 MCMARQC   PIC X.                                          00001570
           02 MCMARQP   PIC X.                                          00001580
           02 MCMARQH   PIC X.                                          00001590
           02 MCMARQV   PIC X.                                          00001600
           02 MCMARQO   PIC X(5).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MCFAMA    PIC X.                                          00001630
           02 MCFAMC    PIC X.                                          00001640
           02 MCFAMP    PIC X.                                          00001650
           02 MCFAMH    PIC X.                                          00001660
           02 MCFAMV    PIC X.                                          00001670
           02 MCFAMO    PIC X(5).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MLREFFOURNA    PIC X.                                     00001700
           02 MLREFFOURNC    PIC X.                                     00001710
           02 MLREFFOURNP    PIC X.                                     00001720
           02 MLREFFOURNH    PIC X.                                     00001730
           02 MLREFFOURNV    PIC X.                                     00001740
           02 MLREFFOURNO    PIC X(20).                                 00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MNENTCDESELA   PIC X.                                     00001770
           02 MNENTCDESELC   PIC X.                                     00001780
           02 MNENTCDESELP   PIC X.                                     00001790
           02 MNENTCDESELH   PIC X.                                     00001800
           02 MNENTCDESELV   PIC X.                                     00001810
           02 MNENTCDESELO   PIC X(5).                                  00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MLENTCDESELA   PIC X.                                     00001840
           02 MLENTCDESELC   PIC X.                                     00001850
           02 MLENTCDESELP   PIC X.                                     00001860
           02 MLENTCDESELH   PIC X.                                     00001870
           02 MLENTCDESELV   PIC X.                                     00001880
           02 MLENTCDESELO   PIC X(20).                                 00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCTPRESTSELA   PIC X.                                     00001910
           02 MCTPRESTSELC   PIC X.                                     00001920
           02 MCTPRESTSELP   PIC X.                                     00001930
           02 MCTPRESTSELH   PIC X.                                     00001940
           02 MCTPRESTSELV   PIC X.                                     00001950
           02 MCTPRESTSELO   PIC X(5).                                  00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MLTPRESTSELA   PIC X.                                     00001980
           02 MLTPRESTSELC   PIC X.                                     00001990
           02 MLTPRESTSELP   PIC X.                                     00002000
           02 MLTPRESTSELH   PIC X.                                     00002010
           02 MLTPRESTSELV   PIC X.                                     00002020
           02 MLTPRESTSELO   PIC X(20).                                 00002030
           02 MTAB1O OCCURS   12 TIMES .                                00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MCTYPPRESTA  PIC X.                                     00002060
             03 MCTYPPRESTC  PIC X.                                     00002070
             03 MCTYPPRESTP  PIC X.                                     00002080
             03 MCTYPPRESTH  PIC X.                                     00002090
             03 MCTYPPRESTV  PIC X.                                     00002100
             03 MCTYPPRESTO  PIC X(5).                                  00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MCPRESTATIONA     PIC X.                                00002130
             03 MCPRESTATIONC     PIC X.                                00002140
             03 MCPRESTATIONP     PIC X.                                00002150
             03 MCPRESTATIONH     PIC X.                                00002160
             03 MCPRESTATIONV     PIC X.                                00002170
             03 MCPRESTATIONO     PIC X(5).                             00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MWSELECTA    PIC X.                                     00002200
             03 MWSELECTC    PIC X.                                     00002210
             03 MWSELECTP    PIC X.                                     00002220
             03 MWSELECTH    PIC X.                                     00002230
             03 MWSELECTV    PIC X.                                     00002240
             03 MWSELECTO    PIC X.                                     00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MLPRESTATIONA     PIC X.                                00002270
             03 MLPRESTATIONC     PIC X.                                00002280
             03 MLPRESTATIONP     PIC X.                                00002290
             03 MLPRESTATIONH     PIC X.                                00002300
             03 MLPRESTATIONV     PIC X.                                00002310
             03 MLPRESTATIONO     PIC X(20).                            00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MNENTCDEA    PIC X.                                     00002340
             03 MNENTCDEC    PIC X.                                     00002350
             03 MNENTCDEP    PIC X.                                     00002360
             03 MNENTCDEH    PIC X.                                     00002370
             03 MNENTCDEV    PIC X.                                     00002380
             03 MNENTCDEO    PIC X(5).                                  00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MWGROUPEA    PIC X.                                     00002410
             03 MWGROUPEC    PIC X.                                     00002420
             03 MWGROUPEP    PIC X.                                     00002430
             03 MWGROUPEH    PIC X.                                     00002440
             03 MWGROUPEV    PIC X.                                     00002450
             03 MWGROUPEO    PIC X.                                     00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MCPRESTGRPA  PIC X.                                     00002480
             03 MCPRESTGRPC  PIC X.                                     00002490
             03 MCPRESTGRPP  PIC X.                                     00002500
             03 MCPRESTGRPH  PIC X.                                     00002510
             03 MCPRESTGRPV  PIC X.                                     00002520
             03 MCPRESTGRPO  PIC X(5).                                  00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MWPRODMAJA   PIC X.                                     00002550
             03 MWPRODMAJC   PIC X.                                     00002560
             03 MWPRODMAJP   PIC X.                                     00002570
             03 MWPRODMAJH   PIC X.                                     00002580
             03 MWPRODMAJV   PIC X.                                     00002590
             03 MWPRODMAJO   PIC X.                                     00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MMTPRESTA    PIC X.                                     00002620
             03 MMTPRESTC    PIC X.                                     00002630
             03 MMTPRESTP    PIC X.                                     00002640
             03 MMTPRESTH    PIC X.                                     00002650
             03 MMTPRESTV    PIC X.                                     00002660
             03 MMTPRESTO    PIC X(9).                                  00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MZONCMDA  PIC X.                                          00002690
           02 MZONCMDC  PIC X.                                          00002700
           02 MZONCMDP  PIC X.                                          00002710
           02 MZONCMDH  PIC X.                                          00002720
           02 MZONCMDV  PIC X.                                          00002730
           02 MZONCMDO  PIC X(15).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(58).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(4).                                       00003090
                                                                                
