      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRE004      *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,06,BI,A,                          *        
      *                           19,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRE004.                                                        
            05 NOMETAT-IRE004           PIC X(6) VALUE 'IRE004'.                
            05 RUPTURES-IRE004.                                                 
           10 IRE004-NSOCIETE           PIC X(03).                      007  003
           10 IRE004-NLIEU              PIC X(03).                      010  003
           10 IRE004-CVENDEUR           PIC X(06).                      013  006
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRE004-SEQUENCE           PIC S9(04) COMP.                019  002
      *--                                                                       
           10 IRE004-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRE004.                                                   
           10 IRE004-LIBMOIS            PIC X(09).                      021  009
           10 IRE004-TOT-VAL-FORC-M     PIC S9(13)      COMP-3.         030  007
           10 IRE004-TOT-VAL-REM        PIC S9(13)      COMP-3.         037  007
           10 IRE004-TOT-VENTES2        PIC S9(15)      COMP-3.         044  008
           10 IRE004-VAL-FORC-M         PIC S9(13)      COMP-3.         052  007
           10 IRE004-VAL-REM            PIC S9(13)      COMP-3.         059  007
           10 IRE004-VENTES1            PIC S9(13)      COMP-3.         066  007
            05 FILLER                      PIC X(440).                          
                                                                                
