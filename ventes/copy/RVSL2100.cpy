      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVSL2100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSL2100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVSL2100.                                                            
           02  SL21-NSOCIETE     PIC X(0003).                                   
           02  SL21-NLIEU        PIC X(0003).                                   
           02  SL21-NVENTE       PIC X(0007).                                   
           02  SL21-NCODIC       PIC X(0007).                                   
           02  SL21-NSEQNQ       PIC S9(5) COMP-3.                              
           02  SL21-WEMPORTE     PIC X(0001).                                   
           02  SL21-QTE          PIC S9(3) COMP-3.                              
           02  SL21-QTEMANQUANTE PIC S9(3) COMP-3.                              
           02  SL21-STATUT       PIC X(0001).                                   
           02  SL21-DDELIV       PIC X(0008).                                   
           02  SL21-PGMMAJ       PIC X(0008).                                   
           02  SL21-DSYST        PIC S9(13) COMP-3.                             
       01  RVSL2100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL21-NSOCIETE-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  SL21-NSOCIETE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL21-NLIEU-F        PIC S9(4) COMP.                              
      *--                                                                       
           02  SL21-NLIEU-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL21-NVENTE-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  SL21-NVENTE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL21-NCODIC-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  SL21-NCODIC-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL21-NSEQNQ-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  SL21-NSEQNQ-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL21-WEMPORTE-F     PIC S9(4) COMP.                              
      *--                                                                       
           02  SL21-WEMPORTE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL21-QTE-F          PIC S9(4) COMP.                              
      *--                                                                       
           02  SL21-QTE-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL21-QTEMANQUANTE-F PIC S9(4) COMP.                              
      *--                                                                       
           02  SL21-QTEMANQUANTE-F PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL21-STATUT-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  SL21-STATUT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL21-DDELIV-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  SL21-DDELIV-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL21-PGMMAJ-F       PIC S9(4) COMP.                              
      *--                                                                       
           02  SL21-PGMMAJ-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL21-DSYST-F        PIC S9(4) COMP.                              
      *--                                                                       
           02  SL21-DSYST-F        PIC S9(4) COMP-5.                            
      *}                                                                        
       EJECT                                                                    
                                                                                
