      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ GENERATION GV10/GV11 LOCAL                           
      *                                                                         
      *                                                                         
      *****************************************************************         
      *  POUR BGV630 AVEC PUT                                                   
      *                                                                         
  T002      10  WS-MESSAGE REDEFINES WORK-MQ21-MESSAGE.                         
             15  MES-GV11.                                                      
  T002          20  MES-GV11-ENRGV10    PIC X(422).                             
                20  MES-GV11-PHOTO      PIC X(001).                             
                20  MES-GV11-NB-LG      PIC 9(003).                             
                20  MES-GV11-ENR OCCURS 80.                                     
  T002           25 MES-GV11-LIGNE      PIC X(0362).                            
      *---                                                                      
                                                                                
