      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BPV108.                                                     
       AUTHOR.      DSA019.                                                     
      * ---------------------------------------------------------- *            
      *                                                            *            
      *    PROJET         : PAIES VENDEURS.                        *            
      *    PROGRAMME      : BPV108                                 *            
      *    PERIODICITE    : JOURNALIERE                            *            
      *    FONCTION       : EXTRACTION FICHIER HISTORIQUE          *            
      *    DATE DE CREAT. : 19/03/1992                             *            
      *                                                            *            
      * ---------------------------------------------------------- *            
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       INPUT-OUTPUT     SECTION.                                                
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FNSOC          ASSIGN  TO  FNSOC.                            
      *--                                                                       
           SELECT  FNSOC          ASSIGN  TO  FNSOC                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FMOIS          ASSIGN  TO  FMOIS.                            
      *--                                                                       
           SELECT  FMOIS          ASSIGN  TO  FMOIS                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FPV100H        ASSIGN  TO  FPV100H.                          
      *--                                                                       
           SELECT  FPV100H        ASSIGN  TO  FPV100H                           
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FPV108M        ASSIGN  TO  FPV108M.                          
      *--                                                                       
           SELECT  FPV108M        ASSIGN  TO  FPV108M                           
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FPV100H                                                              
           RECORDING F                                                          
           BLOCK CONTAINS 0 RECORDS                                             
           LABEL RECORD STANDARD.                                               
       01  ENR-FPV100H PIC X(250).                                              
       FD  FPV108M                                                              
           RECORDING F                                                          
           BLOCK CONTAINS 0 RECORDS                                             
           LABEL RECORD STANDARD.                                               
       01  ENR-FPV108M PIC X(250).                                              
       FD  FMOIS                                                                
           RECORDING F                                                          
           BLOCK CONTAINS 0 RECORDS                                             
           LABEL RECORD STANDARD.                                               
       01  FILE-MOIS.                                                           
           03 FILE-SSAAMM.                                                      
              05 FILE-MM              PIC  X(02).                               
              05 FILE-SS              PIC  X(02).                               
              05 FILE-AA              PIC  X(02).                               
           03 FILLER                  PIC  X(74).                               
      *                                                                         
       FD  FNSOC                                                                
           RECORDING F                                                          
           BLOCK CONTAINS 0 RECORDS                                             
           LABEL RECORD STANDARD.                                               
       01  ENR-FNSOC.                                                           
           03 FSOC        PIC X(03).                                            
           03 FILLER      PIC X(77).                                            
      * ---------------------------------------------------------- *            
      *                W O R K I N G  -  S T O R A G E             *            
      * ---------------------------------------------------------- *            
       WORKING-STORAGE SECTION.                                                 
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *77  ABEND                 PIC X(08) VALUE 'ABEND'.                       
      *--                                                                       
       77  MW-ABEND                 PIC X(08) VALUE 'ABEND'.                    
      *}                                                                        
       77  I                     PIC  S9(07) COMP-3 VALUE ZERO.                 
       77  W-NSOC                PIC  X(03)  VALUE SPACES.                      
       77  W-MOIS                PIC  X(06)  VALUE SPACES.                      
       77  CPT-FPV100H           PIC  S9(07) COMP-3 VALUE ZERO.                 
       77  CPT-FPV108M           PIC  S9(07) COMP-3 VALUE ZERO.                 
       77  CPT-REJET             PIC  S9(07) COMP-3 VALUE ZERO.                 
       01  CODE-FPV100H          PIC X(01) VALUE '0'.                           
           88  NON-FIN-FPV100H             VALUE '0'.                           
           88  FIN-FPV100H                 VALUE '1'.                           
      *----------------------------------------------------------- *            
           COPY SWPV100.                                                        
           COPY ABENDCOP.                                                       
      *----------------------------------------------------------- *            
      *          P R O C E D U R E   D I V I S I O N               *            
      *----------------------------------------------------------- *            
       PROCEDURE DIVISION.                                                      
           PERFORM DEBUT-BPV108.                                                
           PERFORM TRAIT-BPV108 UNTIL FIN-FPV100H.                              
           PERFORM FIN-BPV108.                                                  
      *----------------------------------------------------------- *            
      *                  D E B U T   B P V 1 0 8                   *            
      *----------------------------------------------------------- *            
       DEBUT-BPV108 SECTION.                                                    
           PERFORM OUVERTURE-FICHIER.                                           
           READ FMOIS AT END                                                    
                MOVE 'PAS DE MOIS EN ENTREE' TO ABEND-MESS                      
                PERFORM FIN-ANORMALE                                            
           END-READ.                                                            
           STRING FILE-SS FILE-AA FILE-MM                                       
                  DELIMITED SIZE INTO W-MOIS.                                   
           READ FNSOC AT END                                                    
                MOVE 'PAS DE SOCIETE EN ENTREE' TO ABEND-MESS                   
                PERFORM FIN-ANORMALE                                            
           END-READ.                                                            
           MOVE FSOC     TO  W-NSOC.                                            
           PERFORM LECTURE-FPV100H.                                             
       FIN-DEBUT-BPV108.  EXIT.                                                 
      * ---------------------------------------------------------- *            
       OUVERTURE-FICHIER SECTION.                                               
           OPEN INPUT  FMOIS FNSOC FPV100H                                      
                OUTPUT FPV108M.                                                 
       FIN-OUVERTURE-FICHIER. EXIT.                                             
      * ---------------------------------------------------------- *            
      *            T R A I T E M E N T   B P V 1 0 1               *            
      * ---------------------------------------------------------- *            
       TRAIT-BPV108 SECTION.                                                    
           IF   FPV100-DMVENTE    = W-MOIS                                      
              PERFORM ECRITURE-FPV108M                                          
           ELSE                                                                 
              ADD 1 TO CPT-REJET                                                
           END-IF.                                                              
           PERFORM LECTURE-FPV100H.                                             
       FIN-TRAIT-BPV108. EXIT.                                                  
      * ---------------------------------------------------------- *            
      *                      F I N    B P V 1 0 8                  *            
      * ---------------------------------------------------------- *            
       FIN-BPV108 SECTION.                                                      
           CLOSE FMOIS FNSOC FPV100H FPV108M.                                   
           PERFORM COMPTE-RENDU.                                                
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-FIN-BPV108. EXIT.                                                    
      *----------------------------------------------------------- *            
       LECTURE-FPV100H            SECTION.                                      
      *----------------------------------------------------------- *            
           READ FPV100H INTO FPV100-DSECT AT END                                
              SET FIN-FPV100H TO TRUE                                           
           END-READ.                                                            
           IF NON-FIN-FPV100H                                                   
              ADD 1 TO CPT-FPV100H                                              
           END-IF.                                                              
       FIN-LECTURE-FPV100H. EXIT.                                               
      *----------------------------------------------------------- *            
       ECRITURE-FPV108M           SECTION.                                      
      *----------------------------------------------------------- *            
           ADD 1 TO CPT-FPV108M.                                                
           MOVE  FPV100-DSECT TO  ENR-FPV108M.                                  
           WRITE ENR-FPV108M.                                                   
       FIN-ECRITURE-FPV108M. EXIT.                                              
      *----------------------------------------------------------- *            
       COMPTE-RENDU SECTION.                                                    
      *----------------------------------------------------------- *            
           DISPLAY '**'.                                                        
           DISPLAY '**       B P V 1 0 8 '.                                     
           DISPLAY '**'.                                                        
           DISPLAY '** NBRE DE LECTURES  FPV100H : ' CPT-FPV100H.               
           DISPLAY '** NBRE D''ECRITURES FPV108M : ' CPT-FPV108M.               
           DISPLAY '** NBRE DE REJETS            : ' CPT-REJET.                 
           DISPLAY '**'.                                                        
       FIN-COMPTE-RENDU.  EXIT.                                                 
      * ------------------------ FIN-ANORMALE ---------------------- *          
       FIN-ANORMALE SECTION.                                                    
           CLOSE FMOIS FNSOC FPV100H FPV108M.                                   
           MOVE 'BPV108' TO ABEND-PROG.                                         
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL  ABEND  USING ABEND-PROG ABEND-MESS.                            
      *--                                                                       
           CALL  MW-ABEND  USING ABEND-PROG ABEND-MESS.                         
      *}                                                                        
       FIN-FIN-ANORMALE. EXIT.                                                  
