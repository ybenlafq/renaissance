      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                  BRE205.                                     
       AUTHOR. DSA026.                                                          
      *                                                                         
      *****************************************************************         
      *  F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E  *         
      *****************************************************************         
      *                                                               *         
      * MISE AU FORMAT HOST DU FICHIER DES REMISES FTS057 REMONTE     *         
      * DU LOCAL.                                                     *         
      *                                                               *         
      *****************************************************************         
      * FT0900 : CORRECTION ANOMALIE SUR LES MONTANTS FORCES          *         
      *  (MISE EN FORME CORRESPONDANT AUX ATTENTES DU HOST)           *         
      *****************************************************************         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * DESCRIPTION DES SELECTS DES FICHIERS SAM.                     *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
      *===============================================================*         
      * FICHIER EN ENTREE:                                           *          
      *===============================================================*         
      *                                                               *         
      * FTS057  : FICHIER DES REMISES AU FORMAT LOCAL AS400           *         
      *                                                               *         
      *===============================================================*         
      * FICHIER EN SORTIE :                                          *          
      *===============================================================*         
      *                                                               *         
      * FRE200  : FICHIER DES REMISES AU FORMAT HOST                  *         
      *                                                               *         
      *===============================================================*         
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FTS057        ASSIGN  TO  FTS057.                           
      *--                                                                       
            SELECT  FTS057        ASSIGN  TO  FTS057                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FRE200        ASSIGN  TO  FRE200.                           
      *--                                                                       
            SELECT  FRE200        ASSIGN  TO  FRE200                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *  DESCRIPTION DES FD DES FICHIERS SAM.                         *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       FD   FTS057                                                              
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01   ENR-FTS057              PIC X(107).                                 
      *                                                                         
       FD   FRE200                                                              
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01   ENR-FRE200              PIC X(100).                                 
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * ZONES DE WORKING SPECIFIQUES AU PROGRAMME                     *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
      *---------------------------------------------------------------*         
      * BUFFERS DE TRAVAIL                                            *         
      *---------------------------------------------------------------*         
       01  W-DSYST-JJ-MM-SSAA       PIC X(10)  VALUE SPACES.                    
      *                                                                         
       01  W-HSYST.                                                             
           05 W-HSYST-HH            PIC X(02)  VALUE SPACES.                    
           05 W-HSYST-MM            PIC X(02)  VALUE SPACES.                    
           05 W-HSYST-SS            PIC X(02)  VALUE SPACES.                    
           05 W-HSYST-CC            PIC X(02)  VALUE SPACES.                    
      *                                                                         
       01  W-HSYST-HH-MM-SS.                                                    
           05 W-HSYST-HH            PIC X(02)  VALUE SPACES.                    
           05 FILLER                PIC X(01)  VALUE ':'.                       
           05 W-HSYST-MM            PIC X(02)  VALUE SPACES.                    
           05 FILLER                PIC X(01)  VALUE ':'.                       
           05 W-HSYST-SS            PIC X(02)  VALUE SPACES.                    
      *                                                                         
      *---------------------------------------------------------------*         
      * COMPTEURS DIVERS                                              *         
      *---------------------------------------------------------------*         
       01  W-CPT-DIVERS.                                                        
           05  W-CPT-FTS057                PIC  9(06).                          
           05  W-CPT-FRE200                PIC  9(06).                          
           05  W-CPT-MASQUE                PIC  Z(05)9.                         
      *                                                                         
      *---------------------------------------------------------------*         
      * FLAGS DE CONTROLE.                                            *         
      *---------------------------------------------------------------*         
       01  W-FLAG-FIN-FTS057         PIC X(1)    VALUE '0'.                     
           88 W-NON-FIN-FTS057                   VALUE '0'.                     
           88 W-FIN-FTS057                       VALUE '1'.                     
      *                                                                         
      *---------------------------------------------------------------*         
      * DESCRIPTION DES FICHIERS.                                     *         
      *---------------------------------------------------------------*         
           COPY FTS057.                                                         
           COPY IRE000.                                                         
      *                                                                         
           COPY WORKDATC.                                                       
           COPY ABENDCOP.                                                       
      *                                                                         
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *****************************************************************         
      *              T R A M E   DU   P R O G R A M M E               *         
      *****************************************************************         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *           ------------------------------------------          *         
      *           --  MODULE DE BASE DU PROGRAMME  BRE205 --          *         
      *           ------------------------------------------          *         
      *                               I                               *         
      *           -----------------------------------------           *         
      *           I                   I                   I           *         
      *   -----------------   -----------------   -----------------   *         
      *   -    DEBUT      -   -  TRAITEMENT   -   -      FIN      -   *         
      *   -----------------   -----------------   -----------------   *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
      *                                                                         
      *                                                                         
      *================================================================         
       MODULE-BRE205                   SECTION.                                 
      *================================================================         
      *                                                                         
           PERFORM MODULE-ENTREE.                                               
           PERFORM MODULE-TRAITEMENT UNTIL W-FIN-FTS057.                        
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
      *================================================================         
       MODULE-ENTREE                   SECTION.                                 
      *================================================================         
      *                                                                         
           INITIALIZE W-CPT-DIVERS                                              
                      DSECT-IRE000.                                             
           MOVE  'BRE205'     TO   ABEND-PROG.                                  
      *--> OUVERTURE DES FICHIERS.                                              
           OPEN INPUT  FTS057                                                   
                OUTPUT FRE200.                                                  
      *                                                                         
      *--> RECUPERATION  DATE SYSTEME/HEURE SYSTEME.                            
      *    MOVE '4'        TO GFDATA.                                           
      *    CALL 'BETDATC'  USING WORK-BETDATC.                                  
      *    IF (GFVDAT = 0)                                                      
      *       MOVE 'PROBLEME RECUPERATION DATE SYSTEME' TO ABEND-MESS           
      *       PERFORM ABEND-PROGRAMME                                           
      *    END-IF.                                                              
      *    MOVE GFJMSA-5    TO W-DSYST-JJ-MM-SSAA.                              
      *    ACCEPT W-HSYST FROM TIME.                                            
      *    MOVE CORRESPONDING W-HSYST TO W-HSYST-HH-MM-SS.                      
      *                                                                         
           PERFORM LECTURE-FTS057.                                              
      *                                                                         
      *================================================================         
       MODULE-TRAITEMENT               SECTION.                                 
      *================================================================         
      *                                                                         
           PERFORM CHARGEMENT-FRE200.                                           
           PERFORM ECRITURE-FRE200.                                             
           PERFORM LECTURE-FTS057.                                              
      *                                                                         
       MODULE-TRAITEMENT-FIN.      EXIT.                                        
      *                                                                         
      *---------------------------------------------------------------*         
       CHARGEMENT-FRE200               SECTION.                                 
      *---------------------------------------------------------------*         
      *                                                                         
      *                                                                         
           MOVE FTS057-NSOC            TO  IRE000-NSOCIETE.                     
           MOVE FTS057-NLIEU           TO  IRE000-NLIEU.                        
           MOVE FTS057-NCAISS          TO  IRE000-CAISSE.                       
           MOVE FTS057-NTRANS          TO  IRE000-TRANS.                        
           MOVE FTS057-NCODIC          TO  IRE000-NCODIC.                       
           MOVE FTS057-CVEND           TO  IRE000-VENDEUR.                      
FT0900     IF FTS057-VFORCM > 0                                                 
              SUBTRACT FTS057-VFORCM FROM FTS057-VALTH                          
                GIVING IRE000-VAL-FORC-M                                        
           ELSE                                                                 
              MOVE 0 TO IRE000-VAL-FORC-M                                       
           END-IF.                                                              
           IF FTS057-VFORCP > 0                                                 
              SUBTRACT FTS057-VALTH   FROM FTS057-VFORCP                        
                GIVING IRE000-VAL-FORC-P                                        
           ELSE                                                                 
              MOVE 0 TO IRE000-VAL-FORC-P                                       
           END-IF.                                                              
      *    MOVE FTS057-VFORCP          TO  IRE000-VAL-FORC-P.                   
      *    MOVE FTS057-VFORCM          TO  IRE000-VAL-FORC-M.                   
           MOVE FTS057-VALTH           TO  IRE000-VAL-THEOR.                    
           MOVE FTS057-QLGVRP          TO  IRE000-QTE.                          
           MOVE FTS057-VALREM          TO  IRE000-VAL-REMIS.                    
           MOVE FTS057-DJOUR           TO  IRE000-DATE.                         
      *                                                                         
       CHARGEMENT-FRE200-FIN.        EXIT.                                      
      *                                                                         
      *---------------------------------------------------------------*         
       LECTURE-FTS057                  SECTION.                                 
      *---------------------------------------------------------------*         
      *                                                                         
           INITIALIZE DSECT-FTS057.                                             
      *                                                                         
           READ FTS057                                                          
              INTO DSECT-FTS057                                                 
                  AT END      SET W-FIN-FTS057  TO TRUE                         
                  NOT AT END  ADD 1  TO  W-CPT-FTS057                           
           END-READ.                                                            
      *                                                                         
       LECTURE-FTS057-FIN.       EXIT.                                          
      *                                                                         
      *---------------------------------------------------------------*         
       ECRITURE-FRE200               SECTION.                                   
      *---------------------------------------------------------------*         
      *                                                                         
           ADD 1  TO  W-CPT-FRE200.                                             
      *                                                                         
           WRITE ENR-FRE200                                                     
              FROM DSECT-IRE000                                                 
           END-WRITE.                                                           
      *                                                                         
       ECRITURE-FRE200-FIN.      EXIT.                                          
      *                                                                         
      *                                                                         
      *                                                                         
      * -AIDA *********************************************************         
      *                                                               *         
      *   SSSSSSS   OOOOOOO  RRRRRRRR  TTTTTTTTT IIIIIIIII EEEEEEEEE  *         
      *  SSSSSSSS  OOOOOOOOO RRRRRRRR  TTTTTTTTT IIIIIIIII EEEEEEEEE  *         
      *  SSSS      OOO   OOO RRR   RRR    TTT       III    EEE        *         
      *   SSSS     OOO   OOO RRR   RRR    TTT       III    EEE        *         
      *    SSSS    OOO   OOO RRRRRRRR     TTT       III    EEEEEE     *         
      *     SSSS   OOO   OOO RRRRRRRR     TTT       III    EEEEEE     *         
      *      SSSS  OOO   OOO RRR  RRR     TTT       III    EEE        *         
      *      SSSS  OOO   OOO RRR  RRR     TTT       III    EEE        *         
      *  SSSSSSSS  OOOOOOOOO RRR   RRR    TTT    IIIIIIIII EEEEEEEEE  *         
      *  SSSSSSS    OOOOOOO  RRR   RRR    TTT    IIIIIIIII EEEEEEEEE  *         
      *                                                               *         
      *****************************************************************         
      *                                                                         
      *================================================================         
       MODULE-SORTIE                   SECTION.                                 
      *================================================================         
      *                                                                         
           CLOSE FTS057,                                                        
                 FRE200.                                                        
      *                                                                         
           DISPLAY '**************************************************'         
           DISPLAY '*     B R E 2 0 5  :  F I N   N O R M A L E      *'         
           DISPLAY '**************************************************'         
           DISPLAY '*                                                *'         
      *                                                                         
      *    DISPLAY '* DATE DE TRAITEMENT                : '                     
      *             W-DSYST-JJ-MM-SSAA                ' *'.                     
      *    DISPLAY '*                                     '                     
      *             W-HSYST-HH-MM-SS                 '   *'.                    
      *    DISPLAY '*                                                *'         
      *    DISPLAY '*------------------------------------------------*'         
      *    DISPLAY '*                                                *'         
      *                                                                         
           MOVE      W-CPT-FTS057               TO W-CPT-MASQUE.                
           DISPLAY '* NB ENREG LUS DANS FTS057          : '                     
                    W-CPT-MASQUE                   '     *'.                    
      *                                                                         
           MOVE      W-CPT-FRE200               TO W-CPT-MASQUE.                
           DISPLAY '* NB ENREG ECRIS DANS FRE200        : '                     
                    W-CPT-MASQUE                   '     *'.                    
      *                                                                         
           DISPLAY '*                                                *'         
           DISPLAY '**************************************************'         
           GOBACK.                                                              
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * PROCEDURE DE SORTIE EN CAS DE FIN ANORMALE DU PROGRAMME.      *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       ABEND-PROGRAMME            SECTION.                                      
      *-----------------------------------                                      
           CLOSE FTS057 FRE200.                                                 
           DISPLAY '**************************************************'         
           DISPLAY '*    B R E 2 0 5  :  F I N   A N O R M A L E     *'         
           DISPLAY '**************************************************'         
           CALL  'ABEND'   USING   ABEND-PROG  ABEND-MESS.                      
      *                                                                         
      *****************************************************************         
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*         
      *>>>>>>>>>>>>>>> FIN DU PROGRAMME SOURCE  BRE205 <<<<<<<<<<<<<<<*         
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*         
      *****************************************************************         
