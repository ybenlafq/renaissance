      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00010000
       PROGRAM-ID.  BPV408.                                             00020000
       AUTHOR.      DSA049.                                             00030000
                                                                        00040000
      * ---------------------------------------------------------- *    00050000
      *                                                            *    00060000
      *    PROJET         : PAIES VENDEURS.                        *    00070000
      *    PROGRAMME      : BPV408                                 *    00080000
      *    PERIODICITE    : JOURNALIERE                            *    00090000
      *    FONCTION       : EXTRACTION FICHIER HISTORIQUE          *    00100000
      *    DATE DE CREAT. : 18/09/2003                             *    00110000
      *                                                            *    00120000
      * ---------------------------------------------------------- *    00130000
                                                                        00140000
       ENVIRONMENT DIVISION.                                            00150000
       CONFIGURATION SECTION.                                           00160000
       INPUT-OUTPUT     SECTION.                                        00170000
       FILE-CONTROL.                                                    00180000
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FNSOC          ASSIGN  TO  FNSOC.                    00190000
      *--                                                                       
           SELECT  FNSOC          ASSIGN  TO  FNSOC                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FMOIS          ASSIGN  TO  FMOIS.                    00200000
      *--                                                                       
           SELECT  FMOIS          ASSIGN  TO  FMOIS                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FPV400H        ASSIGN  TO  FPV400H.                  00210000
      *--                                                                       
           SELECT  FPV400H        ASSIGN  TO  FPV400H                           
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FPV408M        ASSIGN  TO  FPV408M.                  00220000
      *                                                                         
      *--                                                                       
           SELECT  FPV408M        ASSIGN  TO  FPV408M                           
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00230000
      *}                                                                        
       DATA DIVISION.                                                   00240000
       FILE SECTION.                                                    00250000
                                                                        00260000
       FD  FPV400H                                                      00270000
           RECORDING F                                                  00280000
           BLOCK CONTAINS 0 RECORDS                                     00290000
           LABEL RECORD STANDARD.                                       00300000
                                                                        00310000
       01  ENR-FPV400H PIC X(210).                                      00320000
                                                                        00330000
       FD  FPV408M                                                      00340000
           RECORDING F                                                  00350000
           BLOCK CONTAINS 0 RECORDS                                     00360000
           LABEL RECORD STANDARD.                                       00370000
                                                                        00380000
       01  ENR-FPV408M PIC X(210).                                      00390000
                                                                        00400000
       FD  FMOIS                                                        00410000
           RECORDING F                                                  00420000
           BLOCK CONTAINS 0 RECORDS                                     00430000
           LABEL RECORD STANDARD.                                       00440000
                                                                        00450000
       01  FILE-MOIS.                                                   00460000
           03 FILE-SSAAMM.                                              00470000
              05 FILE-MM              PIC  X(02).                       00480000
              05 FILE-SS              PIC  X(02).                       00490000
              05 FILE-AA              PIC  X(02).                       00500000
           03 FILLER                  PIC  X(74).                       00510000
      *                                                                 00520000
       FD  FNSOC                                                        00530000
           RECORDING F                                                  00540000
           BLOCK CONTAINS 0 RECORDS                                     00550000
           LABEL RECORD STANDARD.                                       00560000
                                                                        00570000
       01  ENR-FNSOC.                                                   00580000
           03 FSOC        PIC X(03).                                    00590000
           03 FILLER      PIC X(77).                                    00600000
                                                                        00610000
      * ---------------------------------------------------------- *    00620000
      *                W O R K I N G  -  S T O R A G E             *    00630000
      * ---------------------------------------------------------- *    00640000
                                                                        00650000
       WORKING-STORAGE SECTION.                                         00660000
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *77  ABEND                 PIC X(08) VALUE 'ABEND'.               00670000
      *--                                                                       
       77  MW-ABEND                 PIC X(08) VALUE 'ABEND'.                    
      *}                                                                        
                                                                        00680000
       77  I                     PIC  S9(07) COMP-3 VALUE ZERO.         00690000
       77  W-NSOC                PIC  X(03)  VALUE SPACES.              00700000
       77  W-MOIS                PIC  X(06)  VALUE SPACES.              00710000
       77  CPT-FPV400H           PIC  S9(07) COMP-3 VALUE ZERO.         00720000
       77  CPT-FPV408M           PIC  S9(07) COMP-3 VALUE ZERO.         00730000
       77  CPT-REJET             PIC  S9(07) COMP-3 VALUE ZERO.         00740000
                                                                        00750000
       01  CODE-FPV400H          PIC X(01) VALUE '0'.                   00760000
           88  NON-FIN-FPV400H             VALUE '0'.                   00770000
           88  FIN-FPV400H                 VALUE '1'.                   00780000
                                                                        00790000
      *----------------------------------------------------------- *    00800000
           COPY SWPV100.                                                00810000
           COPY ABENDCOP.                                               00820000
      *----------------------------------------------------------- *    00830000
      *          P R O C E D U R E   D I V I S I O N               *    00840000
      *----------------------------------------------------------- *    00850000
                                                                        00860000
       PROCEDURE DIVISION.                                              00870000
                                                                        00880000
           PERFORM DEBUT-BPV408.                                        00890000
           PERFORM TRAIT-BPV408 UNTIL FIN-FPV400H.                      00900000
           PERFORM FIN-BPV408.                                          00910000
                                                                        00920000
      *----------------------------------------------------------- *    00930000
      *                  D E B U T   B P V 1 0 8                   *    00940000
      *----------------------------------------------------------- *    00950000
                                                                        00960000
       DEBUT-BPV408 SECTION.                                            00970000
                                                                        00980000
           PERFORM OUVERTURE-FICHIER.                                   00990000
           READ FMOIS AT END                                            01000000
                MOVE 'PAS DE MOIS EN ENTREE' TO ABEND-MESS              01010000
                PERFORM FIN-ANORMALE                                    01020000
           END-READ.                                                    01030000
           STRING FILE-SS FILE-AA FILE-MM                               01040000
                  DELIMITED SIZE INTO W-MOIS.                           01050000
                                                                        01060000
           READ FNSOC AT END                                            01070000
                MOVE 'PAS DE SOCIETE EN ENTREE' TO ABEND-MESS           01080000
                PERFORM FIN-ANORMALE                                    01090000
           END-READ.                                                    01100000
           MOVE FSOC     TO  W-NSOC.                                    01110000
                                                                        01120000
           PERFORM LECTURE-FPV400H.                                     01130000
                                                                        01140000
       FIN-DEBUT-BPV408.  EXIT.                                         01150000
                                                                        01160000
      * ---------------------------------------------------------- *    01170000
                                                                        01180000
       OUVERTURE-FICHIER SECTION.                                       01190000
                                                                        01200000
           OPEN INPUT  FMOIS FNSOC FPV400H                              01210000
                OUTPUT FPV408M.                                         01220000
                                                                        01230000
       FIN-OUVERTURE-FICHIER. EXIT.                                     01240000
                                                                        01250000
      * ---------------------------------------------------------- *    01260000
      *            T R A I T E M E N T   B P V 1 0 1               *    01270000
      * ---------------------------------------------------------- *    01280000
                                                                        01290000
       TRAIT-BPV408 SECTION.                                            01300000
                                                                        01310000
                                                                        01320000
           IF   FPV100-DMVENTE    = W-MOIS                              01330000
              PERFORM ECRITURE-FPV408M                                  01340000
           ELSE                                                         01350000
              ADD 1 TO CPT-REJET                                        01360000
           END-IF.                                                      01370000
                                                                        01380000
           PERFORM LECTURE-FPV400H.                                     01390000
                                                                        01400000
       FIN-TRAIT-BPV408. EXIT.                                          01410000
                                                                        01420000
      * ---------------------------------------------------------- *    01430000
      *                      F I N    B P V 1 0 8                  *    01440000
      * ---------------------------------------------------------- *    01450000
                                                                        01460000
       FIN-BPV408 SECTION.                                              01470000
                                                                        01480000
           CLOSE FMOIS FNSOC FPV400H FPV408M.                           01490000
           PERFORM COMPTE-RENDU.                                        01500000
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    01510000
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        01520000
       FIN-FIN-BPV408. EXIT.                                            01530000
                                                                        01540000
      *----------------------------------------------------------- *    01550000
       LECTURE-FPV400H            SECTION.                              01560000
      *----------------------------------------------------------- *    01570000
                                                                        01580000
           READ FPV400H INTO FPV100-DSECT AT END                        01590000
              SET FIN-FPV400H TO TRUE                                   01600000
           END-READ.                                                    01610000
                                                                        01620000
           IF NON-FIN-FPV400H                                           01630000
              ADD 1 TO CPT-FPV400H                                      01640000
           END-IF.                                                      01650000
                                                                        01660000
       FIN-LECTURE-FPV400H. EXIT.                                       01670000
                                                                        01680000
      *----------------------------------------------------------- *    01690000
       ECRITURE-FPV408M           SECTION.                              01700000
      *----------------------------------------------------------- *    01710000
                                                                        01720000
           ADD 1 TO CPT-FPV408M.                                        01730000
           MOVE  FPV100-DSECT TO  ENR-FPV408M.                          01740000
           WRITE ENR-FPV408M.                                           01750000
                                                                        01760000
       FIN-ECRITURE-FPV408M. EXIT.                                      01770000
                                                                        01780000
      *----------------------------------------------------------- *    01790000
       COMPTE-RENDU SECTION.                                            01800000
      *----------------------------------------------------------- *    01810000
                                                                        01820000
           DISPLAY '**'.                                                01830000
           DISPLAY '**       B P V 4 0 8 '.                             01840000
           DISPLAY '**'.                                                01850000
           DISPLAY '** NBRE DE LECTURES  FPV400H : ' CPT-FPV400H.       01860000
           DISPLAY '** NBRE D''ECRITURES FPV408M : ' CPT-FPV408M.       01870000
           DISPLAY '** NBRE DE REJETS            : ' CPT-REJET.         01880000
           DISPLAY '**'.                                                01890000
                                                                        01900000
       FIN-COMPTE-RENDU.  EXIT.                                         01910000
                                                                        01920000
      * ------------------------ FIN-ANORMALE ---------------------- *  01930000
                                                                        01940000
       FIN-ANORMALE SECTION.                                            01950000
                                                                        01960000
           CLOSE FMOIS FNSOC FPV400H FPV408M.                           01970000
           MOVE 'BPV408' TO ABEND-PROG.                                 01980000
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL  ABEND  USING ABEND-PROG ABEND-MESS.                    01990000
      *--                                                                       
           CALL  MW-ABEND  USING ABEND-PROG ABEND-MESS.                         
      *}                                                                        
                                                                        02000000
       FIN-FIN-ANORMALE. EXIT.                                          02010000
