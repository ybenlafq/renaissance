      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.   BGV536.                                                    
       DATE-WRITTEN. 18/08/89.                                                  
       AUTHOR.       OLIVIER BES  CEAFI.                                        
      *                                                                         
      *    LISTE DES RESERVATIONS CLIENTS SUR COMMANDES FOURNISSEURS.           
      *    PROGRAMME INSPIRE DU BGB022.                                         
      *                                                                         
      ******************************************************************        
      * MODIF JA LE 4/07/20000 NLG2                                             
      * PASSAGE UNIQUE DE LA CHAINE POUR L ENSEMBLE DES FILIALES                
      * GESTION DU SAUT DE PAGE POUR LA RUPTURE SOCIETE DEPOT                   
      * PASSAGE DU FICHIER FGV135 A 126 DE LONG                                 
      * AJOUT SOCIETE DEPOT DANS ENTETE                                         
      ******************************************************************        
      *                                                                         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
      *                                                                         
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FDATE         ASSIGN  TO  FDATE.                             
      *--                                                                       
           SELECT  FDATE         ASSIGN  TO  FDATE                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  IGV536        ASSIGN  TO  IGV536.                            
      *--                                                                       
           SELECT  IGV536        ASSIGN  TO  IGV536                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FGV535        ASSIGN  TO  FGV535.                            
      *--                                                                       
           SELECT  FGV535        ASSIGN  TO  FGV535                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
      *                                                                         
       FILE SECTION.                                                            
      *                                                                         
      *---------------------------------------------------------------*         
      *  D E F I N I T I O N    F I C H I E R  D A T E  D U  J O U R  *         
      *---------------------------------------------------------------*         
      *                                                                         
       FD  FDATE                                                                
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FILE-DATE.                                                           
           03  FILE-JJ             PIC  X(02).                                  
           03  FILE-MM             PIC  X(02).                                  
           03  FILE-SS             PIC  X(02).                                  
           03  FILE-AA             PIC  X(02).                                  
           03  FILLER              PIC  X(72).                                  
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *  D E F I N I T I O N    F I C H I E R    I M P R E S S I O N  *         
      *---------------------------------------------------------------*         
      *                                                                         
       FD  IGV536                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  LIGNE.                                                               
           03  FIL-SAUT            PIC  X(01).                                  
           03  LIGNE-XX            PIC  X(132).                                 
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *  D E F I N I T I O N   F I C H I E R   R E S E R V A T I O N  *         
      *---------------------------------------------------------------*         
      *                                                                         
       FD  FGV535                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FGV-RECORD.                                                          
           05 FILLER        PIC X(129).                                         
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *                 W O R K I N G  -  S T O R A G E               *         
      *---------------------------------------------------------------*         
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
      *                                                                         
       01  CODE-RETOUR      PIC  X   VALUE '0'.                                 
           88  FIN-FICHIER           VALUE '3'.                                 
      *                                                                         
           COPY ABENDCOP.                                                       
      *                                                                         
      *---------------------------------------------------------------*         
      *           D E S C R I P T I O N    D E S    Z O N E S         *         
      *        S P E C I F I Q U E S    A U    P R O G R A M M E      *         
      *---------------------------------------------------------------*         
      *                                                                         
       01  W-CALCDAT.                                                           
           05  W-CALCDAT-SS        PIC  X(02).                                  
           05  W-CALCDAT-AA        PIC  X(02).                                  
           05  W-CALCDAT-MM        PIC  X(02).                                  
           05  W-CALCDAT-JJ        PIC  X(02).                                  
       01  W-CALCDAJ.                                                           
           05  W-CALCDAJ-SS        PIC  X(02).                                  
           05  W-CALCDAJ-AA        PIC  X(02).                                  
           05  W-CALCDAJ-MM        PIC  X(02).                                  
           05  W-CALCDAJ-JJ        PIC  X(02).                                  
       01  TOP-DOUBLE              PIC  X   VALUE '0'.                          
           88  DOUBLE                       VALUE '1'.                          
           88  NON-DOUBLE                   VALUE '0'.                          
       01  W-CONT-MARQ.                                                         
           05  W-CAR               PIC  X(01).                                  
           05  FILLER              PIC  X(04).                                  
       01  W-CDEFOUR.                                                           
           05  W-CDE               PIC  X(07)        VALUE SPACES.              
           05  FILLER              PIC  X(01).                                  
           05  W-DATE.                                                          
               10 W-DATEJJ         PIC  X(02)        VALUE SPACES.              
               10 FILLER-1         PIC  X(01)        VALUE '/'.                 
               10 W-DATEMM         PIC  X(02)        VALUE SPACES.              
               10 FILLER-2         PIC  X(01)        VALUE '/'.                 
               10 W-DATEAA         PIC  X(02)        VALUE SPACES.              
           05  FILLER              PIC  X(01).                                  
           05  W-QTE               PIC  ZZZZ9.                                  
           05  FILLER              PIC  X(01).                                  
      *                                                                         
       01  W-QTEC                  PIC  ZZ9.                                    
      *                                                                         
       77  QUANT                   PIC  9(5).                                   
      *                                                                         
       01  W-CRITERE.                                                           
           05  CR-NCODIC           PIC  X(07)        VALUE SPACES.              
           05  CR-NCDE             PIC  X(07)        VALUE SPACES.              
           05  CR-FLAG             PIC  X(01)        VALUE SPACES.              
           05  CR-DATE             PIC  X(08)        VALUE SPACES.              
           05  CR-QTE              PIC S9(05) COMP-3 VALUE ZEROES.              
      *                                                                         
       01  EDT-DATE.                                                            
           05  EDT-JJ              PIC  X(02).                                  
           05  FILLER              PIC  X(01)        VALUE '/'.                 
           05  EDT-MM              PIC  X(02).                                  
           05  FILLER              PIC  X(01)        VALUE '/'.                 
           05  EDT-AA              PIC  X(02).                                  
      *                                                                         
       01  COMPTEURS.                                                           
           05  CTR-PAGE            PIC S9(03) COMP-3 VALUE +0.                  
           05  CTR-LIGNE           PIC S9(07) COMP-3 VALUE +99.                 
           05  CTR-LIGNES-LUES     PIC S9(09) COMP-3 VALUE +0.                  
           05  CTR-LIGNES-EDIT     PIC S9(09) COMP-3 VALUE +0.                  
           05  CTR-QTE             PIC S9(05) COMP-3 VALUE +0.                  
      *                                                                         
       01  RESERVES.                                                            
           05  R-NCODIC            PIC X(7)          VALUE SPACES.              
           05  R-NSOCIETE          PIC XXX           VALUE SPACES.              
           05  R-NLIEU             PIC XXX           VALUE SPACES.              
      *                                                                         
       01  W-MESS1.                                                             
           05  FILLER              PIC  X(14)                                   
                                   VALUE 'AFF PAR DISPO '.                      
           05  MESS-DATE           PIC  X(08).                                  
           05  FILLER              PIC  X(01)        VALUE SPACES.              
      *                                                                         
       01  W-MESS2.                                                             
           05  FILLER              PIC X(23)                                    
                      VALUE 'SANS RECEPT. RATTACHEE '.                          
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *     D E S C R I P T I O N    R E C O R D    F I C H I E R     *         
      *---------------------------------------------------------------*         
      *                                                                         
      * LG : 129                                                                
      *                                                                         
       01  RECORD-FGV535.                                                       
      *                                                                         
           05  FGV535-FLAG           PIC  X(01).                                
           05  FGV535-NSOCDEPOT      PIC  X(03).                                
           05  FGV535-NDEPOT         PIC  X(03).                                
           05  FGV535-NCODIC         PIC  X(07).                                
           05  FGV535-STDES.                                                    
               10  FGV535-STDFAM     PIC  X(05).                                
               10  FGV535-STDMARQ    PIC  X(05).                                
               10  FGV535-STDREF     PIC  X(20).                                
           05  FGV535-CDEFOUR.                                                  
               10  FGV535-FQCDE      PIC  X(07).                                
           05  FGV535-FQDLV.                                                    
               10  FGV535-DATCDESS   PIC  X(02).                                
               10  FGV535-DATCDEAA   PIC  X(02).                                
               10  FGV535-DATCDEMM   PIC  X(02).                                
               10  FGV535-DATCDEJJ   PIC  X(02).                                
           05  FGV535-FQQCD          PIC S9(05) COMP-3.                         
           05  FGV535-NCDE.                                                     
               10  FGV535-NLIEU      PIC  X(03).                                
               10  FGV535-NVENTE     PIC  X(07).                                
           05  FGV535-QCDE           PIC S9(03) COMP-3.                         
           05  FGV535-CCDATLIV.                                                 
               10  FGV535-DATLIVSS   PIC  X(02).                                
               10  FGV535-DATLIVAA   PIC  X(02).                                
               10  FGV535-DATLIVMM   PIC  X(02).                                
               10  FGV535-DATLIVJJ   PIC  X(02).                                
           05  FGV535-CCNOM          PIC  X(25).                                
           05  FGV535-CCTEL          PIC  X(10).                                
           05  FGV535-CONT-MARQ      PIC  X(05).                                
           05  FGV535-M              PIC  X(01).                                
           05  FGV535-NSOCIETE       PIC  XXX.                                  
           05  FGV535-FILIALE        PIC  XXX.                                  
           EJECT                                                                
           COPY WORKDATC.                                                       
                EJECT                                                           
      *                                                                         
      *---------------------------------------------------------------*         
      *   D E S C R I P T I O N   L I G N E S   I M P R E S S I O N   *         
      *---------------------------------------------------------------*         
      *                                                                         
      ** AIDA ************************************************* IRDF **         
      *  DESCRIPTION DE L'ETAT IGV536                                           
      *****************************************************************         
      *                                                                         
       01  ETAT-IGV536.                                                         
      *****************************************************************         
      *  ENTETE PHYSIQUE DE PAGE                                                
      *****************************************************************         
           05  HENTET01.                                                        
               10  FILLER  PIC  X(20)  VALUE '         I G V 5 3 6'.            
               10  FILLER  PIC  X(20)  VALUE SPACES.                            
               10  FILLER  PIC  X(20)  VALUE 'E T A B L I S S E M '.            
               10  FILLER  PIC  X(20)  VALUE 'E N T S    D A R T Y'.            
               10  FILLER  PIC  X(51)  VALUE SPACES.                            
               10  FILLER  PIC  X(01)  VALUE SPACES.                            
      *****************************************************************         
           05  HENTET02.                                                        
               10  FILLER  PIC  X(40)  VALUE SPACES.                            
               10  FILLER  PIC  X(40)  VALUE SPACES.                            
               10  FILLER  PIC  X(51)  VALUE SPACES.                            
               10  FILLER  PIC  X(01)  VALUE SPACES.                            
      *****************************************************************         
           05  HENTET03.                                                        
               10  FILLER  PIC  X(15)  VALUE '  JOURNEE DU : '.                 
               10  FILLER  PIC  X(01)  VALUE SPACES.                            
               10  Z-DATE  PIC  X(08).                                          
               10  FILLER  PIC  X(01)  VALUE SPACES.                            
               10  FILLER  PIC  X(20)  VALUE '                 LIS'.            
               10  FILLER  PIC  X(20)  VALUE 'TE DES RESERVATIONS '.            
               10  FILLER  PIC  X(20)  VALUE 'CLIENTS SUR         '.            
               10  FILLER  PIC  X(20)  VALUE SPACES.                            
               10  FILLER  PIC  X(20)  VALUE '               PAGE '.            
               10  FILLER  PIC  X(02)  VALUE SPACES.                            
               10  Z-PAGE  PIC  ZZZ9.                                           
               10  FILLER  PIC  X(01)  VALUE SPACES.                            
      *****************************************************************         
           05  HENTET04.                                                        
               10  FILLER       PIC  X(16)  VALUE '  DEPOT      :  '.           
               10  R-NSOCDEPOT  PIC  X(03).                                     
               10  FILLER       PIC  X(01)  VALUE SPACES.                       
               10  R-NDEPOT     PIC  X(03).                                     
               10  FILLER  PIC  X(17)  VALUE SPACES.                            
               10  FILLER  PIC  X(20)  VALUE '        COMMANDES FO'.            
               10  FILLER  PIC  X(20)  VALUE 'URNISSEURS          '.            
               10  FILLER  PIC  X(51)  VALUE SPACES.                            
               10  FILLER  PIC  X(01)  VALUE SPACES.                            
      *****************************************************************         
           05  HENTET06.                                                        
               10  FILLER  PIC  X(20)  VALUE '--------------------'.            
               10  FILLER  PIC  X(20)  VALUE '--------------------'.            
               10  FILLER  PIC  X(18)  VALUE '!-----------------'.              
               10  FILLER  PIC  X(18)  VALUE '------!-----------'.              
               10  FILLER  PIC  X(20)  VALUE '--------------------'.            
               10  FILLER  PIC  X(20)  VALUE '--------------------'.            
               10  FILLER  PIC  X(15)  VALUE '---------------'.                 
               10  FILLER  PIC  X(01)  VALUE SPACES.                            
      *****************************************************************         
           05  HENTET07.                                                        
               10  FILLER  PIC  X(20)  VALUE '             A R T I'.            
               10  FILLER  PIC  X(20)  VALUE ' C L E              '.            
               10  FILLER  PIC  X(16)  VALUE '! COMMANDE FOURN'.                
               10  FILLER  PIC  X(16)  VALUE 'ISSEUR  !       '.                
               10  FILLER  PIC  X(24)  VALUE '           C O M M A N D'.        
               10  FILLER  PIC  X(24)  VALUE ' E   C L I E N T        '.        
               10  FILLER  PIC  X(11)  VALUE '           '.                     
               10  FILLER  PIC  X(01)  VALUE SPACES.                            
      *****************************************************************         
           05  HENTET08.                                                        
               10  FILLER  PIC  X(20)  VALUE '  CODIC    FAM  MARQ'.            
               10  FILLER  PIC  X(20)  VALUE '      REFERENCE     '.            
               10  FILLER  PIC  X(15)  VALUE '   NO CDE   DAT'.                 
               10  FILLER  PIC  X(21)  VALUE 'E    QTE   SOC MAG  V'.           
               10  FILLER  PIC  X(24)  VALUE 'ENTE  QTE DATE LIV      '.        
               10  FILLER  PIC  X(20)  VALUE '     CLIENT         '.            
               10  FILLER  PIC  X(11)  VALUE ' TELEPHONE '.                     
               10  FILLER  PIC  X(01)  VALUE SPACES.                            
      *****************************************************************         
           05  LIGNE01.                                                         
               10  Z-CONT-MARQ PIC  X(01).                                      
               10  FILLER      PIC  X(01)  VALUE SPACES.                        
               10  Z-CODIC     PIC  X(07).                                      
               10  Z-FAM       PIC  X(05).                                      
               10  FILLER      PIC  X(01)  VALUE SPACES.                        
               10  Z-MARQ      PIC  X(05).                                      
               10  FILLER      PIC  X(01)  VALUE SPACES.                        
               10  Z-REF       PIC  X(20).                                      
               10  FILLER      PIC  X(01)  VALUE SPACES.                        
               10  Z-CDEF      PIC  X(23).                                      
               10  Z-FIL-ASTE  PIC  X(01).                                      
               10  Z-NSOC      PIC  X(3).                                       
               10  FILLER      PIC  X(1).                                       
               10  Z-NMAG      PIC  X(3).                                       
               10  Z-M         PIC  X(01).                                      
               10  Z-NVENTE    PIC  X(7).                                       
               10  FILLER      PIC  X(01)  VALUE SPACES.                        
               10  Z-QTEC      PIC  X(03).                                      
               10  FILLER      PIC  X(01)  VALUE SPACES.                        
               10  Z-DATLIVJJ  PIC  X(02)  VALUE SPACES.                        
               10  FILLER      PIC  X(01)  VALUE '/'.                           
               10  Z-DATLIVMM  PIC  X(02)  VALUE SPACES.                        
               10  FILLER      PIC  X(01)  VALUE '/'.                           
               10  Z-DATLIVAA  PIC  X(02)  VALUE SPACES.                        
               10  FILLER      PIC  X(01).                                      
               10  Z-CLIENT    PIC  X(25).                                      
               10  FILLER      PIC  X(01)  VALUE SPACES.                        
               10  Z-TEL       PIC  X(10).                                      
               10  FILLER      PIC  X(01)  VALUE SPACES.                        
           05  LIGNE02.                                                         
               10  FILLER      PIC  X(02)  VALUE SPACES.                        
               10  FILLER      PIC  X(13)  VALUE SPACES.                        
               10  FILLER      PIC  X(06)  VALUE SPACES.                        
               10  FILLER      PIC  X(21)  VALUE SPACES.                        
               10  FILLER      PIC  X(14)  VALUE SPACES.                        
               10  FILLER      PIC  X(14)  VALUE 'TOTAL MAGASIN '.              
               10  Z-NLIEU     PIC  XXX    VALUE SPACES.                        
               10  FILLER      PIC  X(9)   VALUE SPACES.                        
               10  Z-QTET      PIC  ZZ9.                                        
               10  FILLER      PIC  X(10)  VALUE SPACES.                        
               10  FILLER      PIC  X(26)  VALUE SPACES.                        
               10  FILLER      PIC  X(11)  VALUE SPACES.                        
           EJECT                                                                
      *                                                                         
      *****************************************************************         
      *       ZONES    BUFFER   D'ENTREE  /  SORTIE                   *         
      *****************************************************************         
      *                                                                         
       01  FILLER              PIC  X(0016)  VALUE '*** Z-INOUT ****'.          
       01  Z-INOUT             PIC  X(4096)  VALUE SPACES.                      
           EJECT                                                                
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
      *****************************************************************         
      *              T R A M E   DU   P R O G R A M M E               *         
      *****************************************************************         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *           ------------------------------------------          *         
      *           --  MODULE DE BASE DU PROGRAMME  BGV536 --          *         
      *           ------------------------------------------          *         
      *                               I                               *         
      *           -----------------------------------------           *         
      *           I                   I                   I           *         
      *   -----------------   -----------------   -----------------   *         
      *   -    DEBUT      -   -  TRAITEMENT   -   -      FIN      -   *         
      *   -----------------   -----------------   -----------------   *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
      *===============================================================*         
       MODULE-BGV536        SECTION.                                            
      *===============================================================*         
      *                                                                         
           PERFORM  DEBUT-BGV536.                                               
      *                                                                         
           PERFORM  TRAIT-BGV536  UNTIL  FIN-FICHIER.                           
      *                                                                         
           PERFORM  FIN-BGV536.                                                 
      *                                                                         
       F-MODULE-BGV536. EXIT.                                                   
           EJECT                                                                
      *                                                                         
      *===============================================================*         
       DEBUT-BGV536         SECTION.                                            
      *===============================================================*         
      *                                                                         
           OPEN  INPUT  FDATE                                                   
                 INPUT  FGV535                                                  
                 OUTPUT IGV536.                                                 
      *                                                                         
           READ  FDATE  AT END DISPLAY '*** PAS DE DATE EN ENTREE ***'          
                               DISPLAY '***      B G V 5 3 6      ***'          
                               PERFORM FIN-ANORMALE.                            
      *                                                                         
           MOVE  FILE-JJ   TO  EDT-JJ.                                          
           MOVE  FILE-MM   TO  EDT-MM.                                          
           MOVE  FILE-AA   TO  EDT-AA.                                          
           MOVE  EDT-DATE  TO  Z-DATE.                                          
           MOVE  FILE-JJ   TO  W-CALCDAJ-JJ.                                    
           MOVE  FILE-MM   TO  W-CALCDAJ-MM.                                    
           MOVE  FILE-AA   TO  W-CALCDAJ-AA.                                    
           IF  W-CALCDAJ-AA < '50'                                              
               MOVE '20' TO W-CALCDAJ-SS                                        
           ELSE                                                                 
               MOVE '19' TO W-CALCDAJ-SS.                                       
           MOVE '5'              TO GFDATA                                      
           MOVE W-CALCDAJ        TO GFSAMJ-0                                    
           CALL 'BETDATC' USING WORK-BETDATC                                    
           IF GFVDAT NOT = '1'                                                  
              DISPLAY ' PROBLEME DATE ' FILE-DATE                               
              MOVE 'PROBLEME CALCUL DATE JOUR         ' TO ABEND-MESS           
              PERFORM FIN-ANORMALE                                              
           END-IF                                                               
           COMPUTE QUANT = GFQNT0 + 7                                           
           MOVE '3'              TO GFDATA                                      
           MOVE QUANT            TO GFQNT0                                      
           CALL 'BETDATC' USING WORK-BETDATC                                    
           IF GFVDAT NOT = '1'                                                  
              MOVE 'PROBLEME CALCUL DATE JOUR  + 7 ' TO ABEND-MESS              
              PERFORM FIN-ANORMALE                                              
           END-IF                                                               
           MOVE GFJJ             TO W-CALCDAJ-JJ.                               
           MOVE GFMM             TO W-CALCDAJ-MM.                               
           MOVE GFAA             TO W-CALCDAJ-AA.                               
           MOVE GFSS             TO W-CALCDAJ-SS.                               
           DISPLAY 'DATE DU JOUR + 7 ' W-CALCDAJ.                               
      *                                                                         
           PERFORM  LIRE.                                                       
           MOVE FGV535-NSOCDEPOT TO R-NSOCDEPOT                                 
           MOVE FGV535-NDEPOT    TO R-NDEPOT                                    
           PERFORM  ALIMENTER-RESERVES.                                         
      *                                                                         
       F-DEBUT-BGV536. EXIT.                                                    
           EJECT                                                                
      *                                                                         
      *===============================================================*         
       TRAIT-BGV536          SECTION.                                           
      *===============================================================*         
      *                                                                         
           PERFORM  RUPTURE.                                                    
      *                                                                         
           PERFORM  REMPLIR.                                                    
      *                                                                         
           PERFORM  ECRIRE.                                                     
      *                                                                         
           PERFORM  LIRE.                                                       
      *                                                                         
       F-TRAIT-BGV536. EXIT.                                                    
           EJECT                                                                
      *                                                                         
      *===============================================================*         
       FIN-BGV536          SECTION.                                             
      *===============================================================*         
      *                                                                         
           PERFORM TOTAL-LIEU.                                                  
           DISPLAY 'LISTE DES COMPTEURS DU PROGRAMME BGV536'.                   
           DISPLAY 'DATE DE LA CARTE PARAMETRE :     ' EDT-DATE.                
           DISPLAY 'NOMBRE DE LIGNES DE FGV535 LUES : ' CTR-LIGNES-LUES.        
           DISPLAY 'NOMBRE DE LIGNES EDITEES :       ' CTR-LIGNES-EDIT.         
      *                                                                         
           CLOSE  FDATE  FGV535  IGV536.                                        
      *                                                                         
           GOBACK.                                                              
      *                                                                         
       F-FIN-BGV536. EXIT.                                                      
           EJECT                                                                
      *                                                                         
      *================================================================*        
       LIRE                SECTION.                                             
      *================================================================*        
      *                                                                         
           READ FGV535 RECORD INTO RECORD-FGV535                                
                      AT END SET FIN-FICHIER TO TRUE.                           
      *                                                                         
           IF NOT FIN-FICHIER                                                   
              ADD 1 TO CTR-LIGNES-LUES.                                         
      *                                                                         
       F-LIRE. EXIT.                                                            
           EJECT                                                                
      *                                                                         
      *================================================================*        
       RUPTURE             SECTION.                                             
      *================================================================*        
      *                                                                         
           IF FGV535-NSOCIETE  NOT = R-NSOCIETE                                 
           OR FGV535-NLIEU     NOT = R-NLIEU                                    
           OR FGV535-NSOCDEPOT NOT = R-NSOCDEPOT                                
           OR FGV535-NDEPOT    NOT = R-NDEPOT                                   
              PERFORM TOTAL-LIEU                                                
           END-IF.                                                              
           IF CTR-LIGNE > +59                                                   
           OR FGV535-NSOCDEPOT NOT = R-NSOCDEPOT                                
           OR FGV535-NDEPOT    NOT = R-NDEPOT                                   
              INITIALIZE  W-CRITERE                                             
              IF FGV535-NSOCDEPOT NOT = R-NSOCDEPOT                             
              OR FGV535-NDEPOT    NOT = R-NDEPOT                                
                 MOVE 0              TO  CTR-PAGE                               
              END-IF                                                            
              MOVE FGV535-NSOCDEPOT TO R-NSOCDEPOT                              
              MOVE FGV535-NDEPOT  TO R-NDEPOT                                   
              PERFORM  ENTETE                                                   
              PERFORM  ARTICLE                                                  
              PERFORM  CDE-FOUR                                                 
           ELSE                                                                 
              IF CR-NCODIC NOT = FGV535-NCODIC                                  
                 INITIALIZE  W-CRITERE                                          
                 PERFORM  ARTICLE                                               
                 PERFORM  CDE-FOUR                                              
              ELSE                                                              
                 IF CR-NCDE NOT = FGV535-CDEFOUR                                
                 OR CR-FLAG NOT = FGV535-FLAG                                   
                 OR CR-DATE NOT = FGV535-FQDLV                                  
      *          OR CR-QTE  NOT = FGV535-FQQCD                                  
                    PERFORM  CDE-FOUR                                           
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
       F-RUPTURE. EXIT.                                                         
           EJECT                                                                
      *                                                                         
      *================================================================*        
       ARTICLE             SECTION.                                             
      *================================================================*        
      *                                                                         
           MOVE  FGV535-NCODIC   TO  CR-NCODIC                                  
                                     Z-CODIC.                                   
           MOVE  FGV535-STDFAM   TO  Z-FAM.                                     
           MOVE  FGV535-STDMARQ  TO  Z-MARQ.                                    
           MOVE  FGV535-STDREF   TO  Z-REF.                                     
      *                                                                         
       F-ARTICLE. EXIT.                                                         
           EJECT                                                                
      *                                                                         
      *================================================================*        
       CDE-FOUR            SECTION.                                             
      *================================================================*        
      *                                                                         
           MOVE SPACE TO W-CALCDAT.                                             
           IF CR-FLAG NOT = '1' AND FGV535-FLAG = '1'                           
              MOVE  HENTET02           TO  LIGNE-XX                             
              WRITE LIGNE                                                       
              ADD   1                  TO  CTR-LIGNE                            
              MOVE  EDT-DATE           TO  MESS-DATE                            
              MOVE  W-MESS1            TO  Z-CDEF                               
           ELSE                                                                 
              IF FGV535-FLAG = '2'                                              
                 MOVE  HENTET02        TO  LIGNE-XX                             
                 WRITE LIGNE                                                    
                 ADD   1               TO  CTR-LIGNE                            
                IF FGV535-FQDLV NUMERIC AND                                     
                   FGV535-FQDLV NOT = '99999999'                                
                 MOVE '5'              TO GFDATA                                
                 MOVE FGV535-FQDLV     TO GFSAMJ-0                              
                 CALL 'BETDATC' USING WORK-BETDATC                              
                 IF GFVDAT NOT = '1'                                            
                    DISPLAY ' PROBLEME DATE ' FGV535-FQDLV                      
                    MOVE 'PROBLEME CALCUL DATE LIVRAISON1' TO ABEND-MESS        
                    PERFORM FIN-ANORMALE                                        
                 END-IF                                                         
                 COMPUTE QUANT = GFQNT0 + 7                                     
      *                                                                         
                 MOVE '3'              TO GFDATA                                
                 MOVE QUANT            TO GFQNT0                                
                 CALL 'BETDATC' USING WORK-BETDATC                              
                 IF GFVDAT NOT = '1'                                            
                    MOVE 'PROBLEME CALCUL DATE LIVRAISON' TO ABEND-MESS         
                    PERFORM FIN-ANORMALE                                        
                 END-IF                                                         
                 MOVE GFJJ             TO W-CALCDAT-JJ                          
                 MOVE GFMM             TO W-CALCDAT-MM                          
                 MOVE GFAA             TO W-CALCDAT-AA                          
                 MOVE GFSS             TO W-CALCDAT-SS                          
                 MOVE FGV535-DATCDEJJ  TO W-DATEJJ                              
                 MOVE FGV535-DATCDEMM  TO W-DATEMM                              
                 MOVE FGV535-DATCDEAA  TO W-DATEAA                              
                ELSE                                                            
                 MOVE '99'             TO W-DATEJJ W-CALCDAT-JJ                 
                 MOVE '99'             TO W-DATEMM W-CALCDAT-MM                 
                 MOVE '99'             TO W-DATEAA W-CALCDAT-AA                 
                 MOVE '99'             TO          W-CALCDAT-SS                 
                END-IF                                                          
                 MOVE  FGV535-CDEFOUR  TO  W-CDE                                
                 MOVE  FGV535-FQQCD    TO  W-QTE                                
                 MOVE  W-CDEFOUR       TO  Z-CDEF                               
              ELSE                                                              
                 IF CR-FLAG NOT = '3' AND FGV535-FLAG = '3'                     
                    MOVE  HENTET02     TO  LIGNE-XX                             
                    WRITE LIGNE                                                 
                    ADD   1            TO  CTR-LIGNE                            
                    MOVE  W-MESS2      TO  Z-CDEF                               
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
           MOVE  FGV535-CDEFOUR        TO  CR-NCDE.                             
           MOVE  FGV535-FLAG           TO  CR-FLAG.                             
           MOVE  FGV535-FQDLV          TO  CR-DATE.                             
           MOVE  FGV535-FQQCD          TO  CR-QTE.                              
      *                                                                         
       F-CDE-FOUR. EXIT.                                                        
           EJECT                                                                
      *                                                                         
      *================================================================*        
       REMPLIR             SECTION.                                             
      *================================================================*        
      *                                                                         
           MOVE  FGV535-CONT-MARQ TO  W-CONT-MARQ.                              
           IF W-CAR = 'C'                                                       
               MOVE 'C' TO Z-CONT-MARQ                                          
           ELSE                                                                 
               MOVE ' ' TO Z-CONT-MARQ                                          
           END-IF.                                                              
           IF W-CALCDAT NOT < FGV535-CCDATLIV OR                                
              W-CALCDAJ NOT < FGV535-CCDATLIV                                   
              SET DOUBLE TO TRUE                                                
              MOVE '*' TO Z-FIL-ASTE                                            
           ELSE                                                                 
              MOVE ' ' TO Z-FIL-ASTE                                            
           END-IF.                                                              
           MOVE  FGV535-NSOCIETE  TO  Z-NSOC.                                   
           MOVE  FGV535-NLIEU     TO  Z-NMAG.                                   
           MOVE  FGV535-M         TO  Z-M.                                      
           MOVE  FGV535-NVENTE    TO  Z-NVENTE.                                 
           MOVE  FGV535-QCDE      TO  W-QTEC.                                   
           MOVE  W-QTEC           TO  Z-QTEC.                                   
           MOVE  FGV535-DATLIVJJ  TO  Z-DATLIVJJ.                               
           MOVE  FGV535-DATLIVMM  TO  Z-DATLIVMM.                               
           MOVE  FGV535-DATLIVAA  TO  Z-DATLIVAA.                               
           MOVE  FGV535-CCNOM     TO  Z-CLIENT.                                 
           MOVE  FGV535-CCTEL     TO  Z-TEL.                                    
           ADD  FGV535-QCDE         TO CTR-QTE.                                 
      *                                                                         
       F-REMPLIR. EXIT.                                                         
           EJECT                                                                
      *================================================================*        
       TOTAL-LIEU                      SECTION.                                 
      *================================================================*        
           IF R-NLIEU = '000'                                                   
              MOVE R-NSOCIETE       TO Z-NLIEU                                  
           ELSE                                                                 
              MOVE R-NLIEU          TO Z-NLIEU                                  
           END-IF.                                                              
           MOVE CTR-QTE             TO Z-QTET.                                  
           MOVE '0' TO FIL-SAUT.                                                
           MOVE  LIGNE02   TO  LIGNE-XX.                                        
           WRITE LIGNE.                                                         
           PERFORM ALIMENTER-RESERVES.                                          
           MOVE ' ' TO FIL-SAUT.                                                
           ADD   +2        TO  CTR-LIGNE.                                       
      *                                                                         
      *================================================================*        
       ENTETE              SECTION.                                             
      *================================================================*        
      *                                                                         
           MOVE '1' TO FIL-SAUT.                                                
           MOVE  HENTET01  TO  LIGNE-XX.                                        
           WRITE LIGNE.                                                         
           MOVE ' ' TO FIL-SAUT.                                                
           MOVE  HENTET02  TO  LIGNE-XX.                                        
           WRITE LIGNE.                                                         
           ADD   1         TO  CTR-PAGE.                                        
           MOVE  CTR-PAGE  TO  Z-PAGE.                                          
           MOVE  HENTET03  TO  LIGNE-XX.                                        
           WRITE LIGNE.                                                         
           MOVE  HENTET04  TO  LIGNE-XX.                                        
           WRITE LIGNE.                                                         
           MOVE  HENTET02  TO  LIGNE-XX.                                        
           WRITE LIGNE.                                                         
           MOVE  HENTET06  TO  LIGNE-XX.                                        
           WRITE LIGNE.                                                         
           MOVE  HENTET07  TO  LIGNE-XX.                                        
           WRITE LIGNE.                                                         
           MOVE  HENTET06  TO  LIGNE-XX.                                        
           WRITE LIGNE.                                                         
           MOVE  HENTET08  TO  LIGNE-XX.                                        
           WRITE LIGNE.                                                         
           MOVE  HENTET02  TO  LIGNE-XX.                                        
           WRITE LIGNE.                                                         
           MOVE  +10       TO  CTR-LIGNE.                                       
      *                                                                         
       F-ENTETE. EXIT.                                                          
           EJECT                                                                
      *                                                                         
      *================================================================*        
       ECRIRE              SECTION.                                             
      *================================================================*        
      *                                                                         
           MOVE  LIGNE01  TO  LIGNE-XX.                                         
           WRITE LIGNE.                                                         
           IF DOUBLE                                                            
              MOVE '+' TO FIL-SAUT                                              
              WRITE LIGNE                                                       
              SET NON-DOUBLE TO TRUE                                            
           END-IF.                                                              
           MOVE ' ' TO FIL-SAUT.                                                
           ADD   1        TO  CTR-LIGNE.                                        
           INITIALIZE  LIGNE01.                                                 
           ADD   1        TO  CTR-LIGNES-EDIT.                                  
      *                                                                         
       F-ECRIRE. EXIT.                                                          
           EJECT                                                                
      *================================================================*        
       ALIMENTER-RESERVES   SECTION.                                            
      *================================================================*        
           MOVE ZERO                TO CTR-QTE.                                 
           MOVE FGV535-NCODIC       TO R-NCODIC.                                
           MOVE FGV535-NSOCIETE     TO R-NSOCIETE.                              
           MOVE FGV535-NLIEU        TO R-NLIEU.                                 
      *                                                                         
      *================================================================*        
       FIN-ANORMALE   SECTION.                                                  
      *================================================================*        
      *                                                                         
           MOVE  ' BGV536 '  TO  ABEND-PROG.                                    
      *                                                                         
           CLOSE  FDATE  FGV535  IGV536.                                        
      *                                                                         
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                        
      *                                                                         
       F-FIN-ANORMALE. EXIT.                                                    
           EJECT                                                                
