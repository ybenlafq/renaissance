      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000010
       PROGRAM-ID.    BGV016.                                           00000020
       AUTHOR.      SCSM.                                               00000030
      *                                                                 00000040
      * ============================================================= * 00000050
      *                                                               * 00000060
      *       PROJET        :  STATISTIQUE COMMERCIALE                * 00000070
      *       PROGRAMME     :  BGV016.                                * 00000080
      *       FONCTION      :  SUIVI MENSUEL DES VENTES N/N-1/N-2     * 00000090
      *       DATE CREATION :  11/10/1994.                            * 00000100
      *                                                               * 00000110
      * ============================================================= * 00000120
      *                                                                 00000130
       ENVIRONMENT DIVISION.                                            00000140
       CONFIGURATION SECTION.                                           00000150
       SPECIAL-NAMES.                                                   00000160
           DECIMAL-POINT IS COMMA.                                      00000170
       INPUT-OUTPUT SECTION.                                            00000180
      *                                                                 00000190
       FILE-CONTROL.                                                    00000200
      *                                                                 00000210
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FDATE         ASSIGN  TO  FDATE.                    00000220
      *                                                                         
      *--                                                                       
            SELECT  FDATE         ASSIGN  TO  FDATE                             
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00000230
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FEX001        ASSIGN  TO  FEX001.                   00000240
      *                                                                         
      *--                                                                       
            SELECT  FEX001        ASSIGN  TO  FEX001                            
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00000250
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FHV02         ASSIGN  TO  FHV02.                    00000240
      *                                                                         
      *--                                                                       
            SELECT  FHV02         ASSIGN  TO  FHV02                             
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00000250
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGV016        ASSIGN  TO  FGV016.                   00000240
      *                                                                         
      *--                                                                       
            SELECT  FGV016        ASSIGN  TO  FGV016                            
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00000250
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FICDAT        ASSIGN  TO  FICDAT.                   00000240
      *                                                                         
      *--                                                                       
            SELECT  FICDAT        ASSIGN  TO  FICDAT                            
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00000250
      *}                                                                        
      *                                                                 00000260
       DATA DIVISION.                                                   00000270
       FILE SECTION.                                                    00000280
      *                                                                 00000290
      * ============================================================= * 00000300
      *    D E F I N I T I O N   F I C H I E R   D A T E   J O U R    * 00000310
      * ============================================================= * 00000320
      *                                                                 00000330
       FD   FDATE                                                       00000340
            RECORDING F                                                 00000350
            BLOCK 0 RECORDS                                             00000360
            LABEL RECORD STANDARD.                                      00000370
      *                                                                 00000330
       01   FILE-DATE.                                                          
            03  FILE-JJ   PIC X(02).                                            
            03  FILE-MM   PIC X(02).                                            
            03  FILE-SS   PIC X(02).                                            
            03  FILE-AA   PIC X(02).                                            
            03  FILLER    PIC X(72).                                            
      *                                                                 00000330
       FD   FEX001                                                      00000340
            RECORDING F                                                 00000350
            BLOCK 0 RECORDS                                             00000360
            LABEL RECORD STANDARD.                                      00000370
                                                                        00000380
            COPY FEX001.                                                00000390
      * ============================================================= * 00000450
      *  D E F I N I T I O N    F I C H I E R    E N T R E E          * 00000460
      * ============================================================= * 00000470
      *                                                                 00000480
       FD   FHV02                                                       00000490
            RECORDING F                                                 00000350
            BLOCK 0 RECORDS                                             00000360
            LABEL RECORD STANDARD.                                      00000500
JA                                                                      00000510
JA    **********************************************************        00000510
JA    *  DESCRIPTION FICHIER CORRESPONDANT TABLE HV02          *        00000510
JA    **********************************************************        00000510
JA     01  HV02-DSECT.                                                  00003770
JA         03  HV02-NSOCIETE           PIC  X(03).                      00003780
JA         03  HV02-NCODIC             PIC  X(07).                      00003790
JA         03  HV02-DVENTECIALE        PIC  X(08).                      00003800
JA         03  HV02-QPIECES            PIC  S9(05)     COMP-3.          00003810
JA         03  HV02-PCA                PIC  S9(07)V99  COMP-3.          00003820
JA         03  HV02-PMTACHATS          PIC  S9(07)V99  COMP-3.          00003830
JA         03  HV02-QPIECES-EMP        PIC  S9(05)     COMP-3.          00003850
JA         03  HV02-PCA-EMP            PIC  S9(07)V99  COMP-3.          00003860
JA         03  HV02-PMTACHATS-EMP      PIC  S9(07)V99  COMP-3.          00003870
JA         03  HV02-QPIECES-COM        PIC  S9(05)     COMP-3.          00003850
JA         03  HV02-PCA-COM            PIC  S9(07)V99  COMP-3.          00003860
JA         03  HV02-PMTACHATS-COM      PIC  S9(07)V99  COMP-3.          00003870
JA         03  HV02-PMTPRIMES          PIC  S9(07)V99  COMP-3.          00003860
JA         03  HV02-PMTPRIMVOL         PIC  S9(07)V99  COMP-3.          00003860
JA         03  HV02-QPIECESEXC         PIC  S9(05)     COMP-3.          00003600
JA         03  HV02-PCAEXC             PIC  S9(07)V99  COMP-3.          00003650
JA         03  HV02-PMTACHATSEXC       PIC  S9(07)V99  COMP-3.          00003640
JA         03  HV02-QPIECESEXE         PIC  S9(05)     COMP-3.          00003600
JA         03  HV02-PCAEXE             PIC  S9(07)V99  COMP-3.          00003650
JA         03  HV02-PMTACHATSEXE       PIC  S9(07)V99  COMP-3.          00003640
                                                                        00000510
      *                                                                 00000480
       FD   FICDAT                                                      00000490
            RECORDING F                                                 00000350
            BLOCK 0 RECORDS                                             00000360
            LABEL RECORD STANDARD.                                      00000500
                                                                        00000510
       01   FICDAT-DSECT.                                               00000390
            02 FICDAT-DVENTECIALE  PIC X(08).                                   
            02 FICDAT-FILLER       PIC X(72).                                   
      * ============================================================= * 00000450
      *  D E F I N I T I O N    F I C H I E R    S O R T I E          * 00000460
      * ============================================================= * 00000470
      *                                                                 00000480
       FD   FGV016                                                      00000490
            RECORDING F                                                 00000350
            BLOCK 0 RECORDS                                             00000360
            LABEL RECORD STANDARD.                                      00000500
       01   W-FGV016            PIC X(87).                                      
      *                                                                 00000640
      * ============================================================= * 00000650
      *                 W O R K I N G  -  S T O R A G E               * 00000660
      * ============================================================= * 00000670
      *                                                                 00000680
       WORKING-STORAGE SECTION.                                         00000690
      *                                                                 00000700
      * ============================================================= * 00001050
      *        D E S C R I P T I O N      D E S      Z O N E S        * 00001060
      *           P R O P R E S  A U   P R O G R A M M E              * 00001070
      * ============================================================= * 00001080
      *                                                                 00001090
       01  CODE-N-1                 PIC X      VALUE '0'.               00000850
           88 NON-RECHERCHE-N-1                VALUE '0'.               00000860
           88 RECHERCHE-N-1                    VALUE '1'.               00000870
       01  CODE-N-2                 PIC X      VALUE '0'.               00000850
           88 NON-RECHERCHE-N-2                VALUE '0'.               00000860
           88 RECHERCHE-N-2                    VALUE '1'.               00000870
       01  CODE-FEX001              PIC X      VALUE ' '.               00000880
           88 NON-FIN-FEX001                   VALUE '0'.               00000890
           88 FIN-FEX001                       VALUE '1'.               00000900
       01  CODE-ECRIT               PIC X      VALUE ' '.               00000880
           88 FLAG-NON-ECRIT                   VALUE '0'.               00000890
           88 FLAG-ECRIT                       VALUE '1'.               00000900
      *                                                                 00001040
            COPY FGV015.                                                00000390
      *                                                                 00001040
       01  COMPTEURS.                                                   00001100
           05 W-ANNEE               PIC 9(2)   VALUE 0.                 00001120
           05 I                     PIC 9(7)   VALUE 0.                 00001120
           05 W-I                   PIC 9(7)   VALUE 0.                 00001120
       01  FILLER.                                                      00001130
           05  NCODIC-PREC              PIC X(7).                       00001130
           02  W-CPT-ENR                 PIC  9(05)        VALUE ZERO.  00000500
           02  CODE-HV02                 PIC  9      VALUE 0.           00000530
               88  NON-FIN-HV02                      VALUE 0.           00000540
               88  FIN-HV02                          VALUE 1.           00000550
      *                                                                 00000560
      *================================================================*00000570
      *    B  O  R  N  E  S    ( S  E  L  E  C  T  /  T  E  S  T )     *00000580
      *================================================================*00000590
      *                                                                 00001390
       01  W-MIN-N.                                                     00001400
           05 W-SS-N-MIN              PIC X(2).                         00001410
           05 W-AA-N-MIN              PIC X(2).                         00001420
           05 W-MM-N-MIN              PIC X(2).                         00001430
           05 W-JJ-N-MIN              PIC X(2) VALUE '01'.              00001440
       01  MIN-N                      PIC X(8).                         00001450
      *                                                                 00001460
       01  W-MIN-N-1.                                                   00001470
           05 W-SS-N-1-MIN            PIC X(2).                         00001480
           05 W-AA-N-1-MIN            PIC X(2).                         00001490
           05 W-MM-N-1-MIN            PIC X(2).                         00001500
           05 W-JJ-N-1-MIN            PIC X(2) VALUE '01'.              00001510
       01  MIN-N-1                    PIC X(8).                         00001520
      *                                                                 00001530
       01  W-MIN-N-2.                                                   00001540
           05 W-SS-N-2-MIN            PIC X(2).                         00001550
           05 W-AA-N-2-MIN            PIC X(2).                         00001560
           05 W-MM-N-2-MIN            PIC X(2).                         00001570
           05 W-JJ-N-2-MIN            PIC X(2) VALUE '01'.              00001580
       01  MIN-N-2                    PIC X(8).                         00001590
      *                                                                 00001600
       01  W-MAX-N.                                                     00001610
           05 W-SS-N-MAX              PIC X(2).                         00001620
           05 W-AA-N-MAX              PIC X(2).                         00001630
           05 W-MM-N-MAX              PIC X(2).                         00001640
           05 W-JJ-N-MAX              PIC X(2) VALUE '31'.              00001650
       01  MAX-N                      PIC X(8).                         00001660
      *                                                                 00001670
       01  W-MAX-N-1.                                                   00001680
           05 W-SS-N-1-MAX            PIC X(2).                         00001690
           05 W-AA-N-1-MAX            PIC X(2).                         00001700
           05 W-MM-N-1-MAX            PIC X(2).                         00001710
           05 W-JJ-N-1-MAX            PIC X(2) VALUE '31'.              00001720
       01  MAX-N-1                    PIC X(8).                         00001730
      *                                                                 00001740
       01  W-MAX-N-2.                                                   00001750
           05 W-SS-N-2-MAX            PIC X(2).                         00001760
           05 W-AA-N-2-MAX            PIC X(2).                         00001770
           05 W-MM-N-2-MAX            PIC X(2).                         00001780
           05 W-JJ-N-2-MAX            PIC X(2) VALUE '31'.              00001790
       01  MAX-N-2  PIC X(8).                                           00001800
      *                                                                 00001810
       01  W-DVENTE.                                                    00001820
           05 W-SS                    PIC 9(2).                         00001830
           05 W-AA                    PIC 9(2).                         00001840
           05 W-MM                    PIC 9(2).                         00001850
           05 W-JJ                    PIC 9(2).                         00001860
      *                                                                 00001870
       01  DATE-SYS.                                                    00001880
           05 IS-DATE-AA              PIC  X(02).                       00001890
           05 IS-DATE-MM              PIC  X(02).                       00001900
           05 IS-DATE-JJ              PIC  X(02).                       00001910
                                                                        00000600
      *================================================================*00001060
      *       M  A  T  R  I  C  E                                      *00001070
      *================================================================*00001080
                                                                        00001090
           02  MATRICE.                                                 00001100
               03  M-POSTE               OCCURS 100 TIMES.              00001110
                   04  M-WSEQPRO         PIC S9(07) COMP-3.             00001120
                   04  M-CRAYONFAM       PIC  X(05).                    00001130
                   04  M-LSEQPRO         PIC  X(26).                    00001130
                   04  M-WTYPE           PIC  X(01).                    00001140
      *                                                                 00001190
      * ============================================================= * 00001200
      *        D E S C R I P T I O N      D E S      Z O N E S        * 00001210
      *           D ' A P P E L    M O D U L E    D A T E             * 00001220
      * ============================================================= * 00001230
      *                                                                 00001240
           COPY  WORKDATC.                                              00001250
      *                                                                 00001260
      * ============================================================= * 00001270
      *        D E S C R I P T I O N      D E S      Z O N E S        * 00001280
      *           D ' A P P E L    M O D U L E    A B E N D           * 00001290
      * ============================================================= * 00001300
      *                                                                 00001310
           COPY  ABENDCOP.                                              00001320
      *                                                                 00001330
       PROCEDURE DIVISION.                                              00001340
      *                                                                 00001350
                                                                        00001530
           PERFORM  DEBUT-BGV016.                                       00001540
           PERFORM  TRAIT-BGV016 UNTIL FIN-HV02 OR FIN-FEX001           00001550
           PERFORM  FIN-BGV016.                                         00001560
                                                                        00001570
      * ============================================================= * 00001580
      *                  D E B U T        B G V 0 1 6                 * 00001590
      * ============================================================= * 00001600
      *                                                                 00001610
       DEBUT-BGV016      SECTION.                                       00001620
                                                                        00001630
           PERFORM  OUVERTURE-FICHIERS.                                 00001640
           PERFORM  INIT-DATE.                                          00001650
      *                                                                 00001700
      * ============================================================= * 00001710
      *                    T R A I T        B G V 0 1 6               * 00001720
      * ============================================================= * 00001730
      *                                                                 00001740
       TRAIT-BGV016     SECTION.                                        00001750
                                                                        00001760
           IF HV02-NCODIC < FEX001-NCODIC                                       
              PERFORM  LECTURE-HV02                                     00001810
           ELSE                                                                 
              IF HV02-NCODIC > FEX001-NCODIC                                    
                 PERFORM  LECTURE-FEX001                                00001810
              ELSE                                                              
                 INITIALIZE MATRICE I                                           
                 PERFORM UNTIL HV02-NCODIC NOT = FEX001-NCODIC                  
                         OR FIN-FEX001                                          
                    ADD 1 TO I                                                  
                    MOVE FEX001-WSEQPRO      TO M-WSEQPRO (I)                   
                    MOVE FEX001-LRUPTURE (1) TO M-LSEQPRO (I)                   
                    MOVE FEX001-CRAYONFAM    TO M-CRAYONFAM (I)                 
                    MOVE FEX001-WRAYONFAM    TO M-WTYPE (I)                     
                    PERFORM  LECTURE-FEX001                             00001810
                 END-PERFORM                                                    
                 MOVE HV02-NCODIC TO NCODIC-PREC                                
                 SET FLAG-NON-ECRIT TO TRUE                                     
                 INITIALIZE FGV015-RECORD                                       
                 PERFORM UNTIL HV02-NCODIC NOT = NCODIC-PREC                    
                         OR FIN-HV02                                            
                    PERFORM  RENSEIGNEMENT-FGV016                               
                    PERFORM  LECTURE-HV02                               00001810
                 END-PERFORM                                                    
                 IF FLAG-ECRIT                                                  
                    PERFORM VARYING W-I FROM 1 BY 1 UNTIL W-I > 100             
                       OR M-CRAYONFAM (W-I) = SPACES                            
                       MOVE HV02-NSOCIETE     TO FGV015-NSOCIETE                
                       MOVE M-CRAYONFAM (W-I) TO FGV015-CRAYONFAM               
                       MOVE M-WSEQPRO (W-I)   TO FGV015-WSEQPRO                 
                       MOVE M-LSEQPRO (W-I)   TO FGV015-LSEQPRO                 
                       MOVE M-WTYPE   (W-I)   TO FGV015-WTYPE                   
                       PERFORM  ECRITURE-FGV016                         00001800
                    END-PERFORM                                                 
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *                                                                 00001840
      * ============================================================= * 00001850
      *                      F I N        B G V 0 1 6                 * 00001860
      * ============================================================= * 00001870
      *                                                                 00001880
       FIN-BGV016     SECTION.                                          00001890
                                                                        00001900
           PERFORM  COMPTE-RENDU.                                       00001910
           PERFORM  FERMETURE-FICHIERS.                                 00001920
           PERFORM  FIN-PROGRAMME.                                      00001930
      *                                                                 00002040
       OUVERTURE-FICHIERS     SECTION.                                  00002050
                                                                        00002060
           OPEN  INPUT  FDATE FEX001 FHV02 FICDAT                       00002070
           OPEN  OUTPUT FGV016.                                         00002080
           READ FEX001 AT END                                           00002180
             MOVE  '**  PAS  DE  PARAMETRAGE IGV020  **'                00002190
                                           TO  ABEND-MESS               00002200
             PERFORM  FIN-ANORMALE                                      00002210
           END-READ.                                                    00002220
           READ FHV02 AT END                                            00001660
                DISPLAY 'FICHIER HV02 VIDE'                                     
                SET FIN-HV02 TO TRUE                                            
           END-READ.                                                            
      *                                                                 00002110
      *-----------------------------------------------------------------00002120
      *                          I N I T  -  D A T E                    00002130
      *-----------------------------------------------------------------00002140
      *                                                                 00002150
       INIT-DATE     SECTION.                                           00002160
                                                                        00002170
           READ FDATE AT END                                            00002180
             MOVE  '**  PAS  DE  DATE  EN  ENTREE  **'                  00002190
                                           TO  ABEND-MESS               00002200
             PERFORM  FIN-ANORMALE                                      00002210
           END-READ.                                                    00002220
      *                                                                 00002930
           READ FICDAT AT END                                           00002180
             MOVE  '**  PAS  DE  FICHIER FICDAT  EN  ENTREE  **'        00002190
                                           TO  ABEND-MESS               00002200
             PERFORM  FIN-ANORMALE                                      00002210
           END-READ.                                                    00002220
      *                                                                 00003120
           MOVE FILE-SS           TO W-SS-N-MIN, W-SS-N-MAX.            00003130
           MOVE FILE-SS           TO W-SS-N-1-MIN, W-SS-N-1-MAX.        00003140
           MOVE FILE-SS           TO W-SS-N-2-MIN, W-SS-N-2-MAX.        00003150
                                                                        00003160
           MOVE FILE-AA           TO W-AA-N-MIN, W-AA-N-MAX.            00003170
           MOVE FILE-AA           TO W-ANNEE.                           00003180
A2         IF W-ANNEE > 0                                                       
              SUBTRACT 1          FROM W-ANNEE                          00003190
"          ELSE                                                                 
"             MOVE 99    TO W-ANNEE                                             
"             MOVE '19'  TO W-SS-N-1-MIN                                00003190
"             MOVE '19'  TO W-SS-N-1-MAX                                00003190
"          END-IF                                                               
           MOVE W-ANNEE           TO W-AA-N-1-MIN, W-AA-N-1-MAX.        00003200
A2         IF W-ANNEE > 0                                                       
              SUBTRACT 1          FROM W-ANNEE                          00003190
"          ELSE                                                                 
"             MOVE 99    TO W-ANNEE                                             
"             MOVE '19'  TO W-SS-N-2-MIN                                00003190
"             MOVE '19'  TO W-SS-N-2-MAX                                00003190
"          END-IF                                                               
           MOVE W-ANNEE           TO W-AA-N-2-MIN, W-AA-N-2-MAX.        00003220
                                                                        00003230
           MOVE FILE-MM           TO W-MM-N-MIN, W-MM-N-MAX.            00003240
           MOVE FILE-MM           TO W-MM-N-1-MIN, W-MM-N-1-MAX.        00003250
           MOVE FILE-MM           TO W-MM-N-2-MIN, W-MM-N-2-MAX.        00003260
                                                                        00003270
           DISPLAY 'W-MAX-N ' W-MAX-N                                           
           DISPLAY 'W-MAX-N-1 ' W-MAX-N-1                                       
           DISPLAY 'W-MAX-N-2 ' W-MAX-N-2                                       
      * MODIF J.MICHAUX 08/02/96                                                
      *    IF FICDAT-DVENTECIALE > W-MAX-N-1                                    
      *       SET RECHERCHE-N-1 TO TRUE                                         
      *       SET RECHERCHE-N-2 TO TRUE                                         
      *    ELSE                                                                 
      *       IF FICDAT-DVENTECIALE > W-MIN-N-2 AND                             
      *          FICDAT-DVENTECIALE < W-MIN-N-1                                 
      *          SET RECHERCHE-N-1 TO TRUE                                      
      *       END-IF                                                            
      *    END-IF.                                                              
      * MODIF J.MICHAUX 08/02/96                                                
           IF FICDAT-DVENTECIALE > W-MIN-N-1                                    
              SET RECHERCHE-N-1 TO TRUE                                         
              SET RECHERCHE-N-2 TO TRUE                                         
           ELSE                                                                 
              IF FICDAT-DVENTECIALE > W-MIN-N-2                                 
                 SET RECHERCHE-N-2 TO TRUE                                      
              END-IF                                                            
           END-IF.                                                              
      *                                                                 00002890
      * ============================================================= * 00002900
      *          M O D U L E S      T R A I T E M E N T               * 00002910
      * ============================================================= * 00002920
      *                                                                 00002930
      *                                                                 00003550
      *-----------------------------------------------------------------00003560
      *          R E N S E I G N E M E N T                            * 00003570
      *-----------------------------------------------------------------00003580
      *                                                                 00003590
       RENSEIGNEMENT-FGV016 SECTION.                                    00003600
      *                                                                 00003590
           MOVE HV02-DVENTECIALE TO W-DVENTE                            00006220
           IF W-AA = W-AA-N-MIN AND W-MM = W-MM-N-MIN                   00006290
              ADD HV02-QPIECES   TO FGV015-NB-N                         320     
              ADD HV02-PCA       TO FGV015-CA-N                         330     
              ADD HV02-PMTACHATS TO FGV015-MTA-N                        340     
              SET FLAG-ECRIT TO TRUE                                            
           END-IF                                                       350     
           IF W-AA = W-AA-N-1-MIN AND NON-RECHERCHE-N-1                 00006290
              AND W-MM = W-MM-N-1-MIN                                   00006290
              ADD HV02-QPIECES   TO FGV015-NB-N-1                       320     
              ADD HV02-PCA       TO FGV015-CA-N-1                       330     
              ADD HV02-PMTACHATS TO FGV015-MTA-N-1                      340     
              SET FLAG-ECRIT TO TRUE                                            
           END-IF                                                       350     
           IF W-AA = W-AA-N-2-MIN AND NON-RECHERCHE-N-2                 00006290
              AND W-MM = W-MM-N-2-MIN                                   00006290
              ADD HV02-QPIECES   TO FGV015-NB-N-2                       380     
              ADD HV02-PCA       TO FGV015-CA-N-2                       390     
              ADD HV02-PMTACHATS TO FGV015-MTA-N-2                      400     
              SET FLAG-ECRIT TO TRUE                                            
           END-IF.                                                      00006420
      *                                                                 00003590
       ECRITURE-FGV016 SECTION.                                         00003600
      *                                                                 00003590
           WRITE W-FGV016  FROM  FGV015-RECORD.                         00006430
           ADD 1 TO W-CPT-ENR.                                          00006440
      *-----------------------------------------------------------------00003760
      *          L E C T U R E                                        * 00003770
      *-----------------------------------------------------------------00003780
      *                                                                 00003790
       LECTURE-FEX001 SECTION.                                          00003800
                                                                        00003810
           READ FEX001 AT END                                           00003820
              SET FIN-FEX001 TO TRUE                                            
           END-READ.                                                            
      *                                                                 00003790
       LECTURE-HV02 SECTION.                                            00003800
                                                                        00003810
           READ FHV02 AT END                                            00003820
              SET FIN-HV02 TO TRUE                                              
           END-READ.                                                            
                                                                        00003840
      * ============================================================= * 00003860
      *          M O D U L E S      F I N      B G V 0 1 6            * 00003870
      * ============================================================= * 00003880
      *                                                                 00003890
      *-----------------------------------------------------------------00003900
      *       F I N  -  A N O R M A L E.                                00003910
      *-----------------------------------------------------------------00003920
      *                                                                 00003930
       FIN-ANORMALE    SECTION.                                         00003940
                                                                        00003950
           MOVE  'BGV016'                  TO  ABEND-PROG.              00003960
                                                                        00003970
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                00003980
                                                                        00003990
      *                                                                 00004010
      *-----------------------------------------------------------------00004020
      *       C O M P T E  -  R E N D U.                                00004030
      *-----------------------------------------------------------------00004040
      *                                                                 00004050
       COMPTE-RENDU      SECTION.                                       00004060
                                                                        00004070
           DISPLAY  '**'.                                               00004080
           DISPLAY  '**           B G V 0 1 6 '.                        00004090
           DISPLAY  '**'.                                               00004100
           DISPLAY  '** NBR DE LIGNES ECRITES       : '                 00004110
                    W-CPT-ENR.                                          00004120
           DISPLAY  '**'.                                               00004150
                                                                        00004160
      *-----------------------------------------------------------------00004190
      *       F E R M E T U R E    F I C H I E R S.                     00004200
      *-----------------------------------------------------------------00004210
      *                                                                 00004220
       FERMETURE-FICHIERS    SECTION.                                   00004230
                                                                        00004240
           CLOSE  FGV016 FEX001 FHV02 FICDAT                            00004260
           CLOSE  FDATE.                                                00004270
                                                                        00004280
      *-----------------------------------------------------------------00004310
      *       F I N    P R O G R A M M E.                               00004320
      *-----------------------------------------------------------------00004330
      *                                                                 00004340
       FIN-PROGRAMME        SECTION.                                    00004350
                                                                        00004360
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP  RUN.                                                   00004370
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
