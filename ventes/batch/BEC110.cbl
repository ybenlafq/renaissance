      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00020001
       PROGRAM-ID.    BEC110.                                           00030001
       AUTHOR.        DE02033.                                          00040099
       ENVIRONMENT    DIVISION.                                         00050001
       CONFIGURATION  SECTION.                                          00060001
      *SOURCE-COMPUTER. HOST WITH DEBUGGING MODE.                       00061099
      *                                                                 00062099
      *=================================================================00070001
      *        ===>  PROGRAMME DE CONVERSION DE DONNEES AU FORMAT       00080001
      *        ===>  XML.                                               00090001
      *    BUT ===>                                                     00100001
      *              UTILISE LE SOUS-PROGRAMME: MEC000                  00110001
      *              AINSI QUE LA COPIE COBOL : MEC000C                 00120099
      *                                                                 00130001
      *=================================================================00140001
      * MODIFICATIONS:                                                  00150001
      *    ??/??/20??                                                   00160001
      *=================================================================00170001
       INPUT-OUTPUT   SECTION.                                          00180001
       FILE-CONTROL.                                                    00190001
                                                                        00200001
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FICHIER-FEC11001  ASSIGN TO FEC11001                  00210099
      *    STATUS STATUT-FEC11001.                                      00211099
      *                                                                         
      *--                                                                       
           SELECT FICHIER-FEC11001  ASSIGN TO FEC11001                          
           STATUS STATUT-FEC11001                                               
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00230043
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FICHIER-FEC11002  ASSIGN TO FEC11002                  00240099
      *    STATUS STATUT-FEC11002.                                      00250099
      *                                                                         
      *--                                                                       
           SELECT FICHIER-FEC11002  ASSIGN TO FEC11002                          
           STATUS STATUT-FEC11002                                               
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00260001
      *}                                                                        
       DATA DIVISION.                                                   00270001
       FILE SECTION.                                                    00280001
                                                                        00290001
       FD   FICHIER-FEC11001                                            00300099
            RECORDING F                                                 00310025
            BLOCK 0  RECORDS                                            00320025
            LABEL RECORD STANDARD                                       00330062
            DATA RECORD ENR-FEC11001.                                   00340099
       01   ENR-FEC11001            PIC X(385).                         00350099
                                                                        00360001
       FD   FICHIER-FEC11002                                            00370099
            RECORDING F                                                 00380025
            BLOCK 0  RECORDS                                            00390025
            LABEL RECORD STANDARD                                       00400062
            DATA RECORD ENR-FEC11002.                                   00410099
       01   ENR-FEC11002            PIC X(27956).                       00420099
                                                                        00430001
      *=================================================================00440001
       WORKING-STORAGE SECTION.                                         00450001
      *-----------------------------------------------------------------00460001
                                                                        00470001
      *--> ZONE D'APPEL AU MODULE ABEND                                 00480025
           COPY ABENDCOP.                                               00490025
                                                                        00500025
      *--> DESCRIPTION DU FICHIER EN ENTREE.                            00510025
           COPY FEC900C.                                                00520099
                                                                        00530001
      *--> COPY DU SOUS-MODULE MEC000.                                  00540002
           COPY MEC000C.                                                00550099
                                                                        00560034
      ******************************************************************00620034
      *--> VARIABLES                                                    00630034
      ******************************************************************00640034
                                                                        00650034
       01  WS-NOMPROG                     PIC  X(08) VALUE SPACES.      00660099
       01  WS-WHEN-COMPILED               PIC  X(16).                   00670099
                                                                        00680099
      ******************************************************************00710064
      *--> COMPTEURS                                                    00720064
      ******************************************************************00730064
                                                                        00740064
       01  WS-NBR-ENR-LU-FEC11001         PIC  9(07) VALUE ZEROES.      00750099
       01  WS-NBR-MESSAGE-ECRIT-FICHIER   PIC  9(07) VALUE ZEROES.      00760099
       01  WS-NBR-MESSAGE-TRAITE          PIC  9(07) VALUE ZEROES.      00770099
                                                                        00780057
      ******************************************************************00790034
      *--> INDICATEURS                                                  00800034
      ******************************************************************00810034
                                                                        00820034
       01  IND-FIN-FICHIER-FEC11001       PIC  X(01) VALUE 'F'.         00830099
           88  FIN-FICHIER-FEC11001                  VALUE 'T'.         00840099
                                                                        00850034
       01  STATUT-FEC11001                PIC  X(02).                   00871099
           88  STATUT-FEC11001-OK                    VALUE '00'.        00872099
                                                                        00872199
       01  STATUT-FEC11002                PIC  X(02).                   00873099
           88  STATUT-FEC11002-OK                    VALUE '00'.        00874099
                                                                        00875099
      ******************************************************************00880034
      *--> CONSTANTES                                                   00890034
      ******************************************************************00900034
                                                                        00910034
       01  PGDISP                   PIC  X(14) VALUE "* * * BEC110: ".  00920099
                                                                        00930034
      *=================================================================00940001
      *                           ___________________                   00950099
      *                          |                   |                  00960099
      *                          | PROGRAMME BEC110  |                  00970099
      *                          |___________________|                  00980099
      *             _______________________!____________________        00990099
      *      ______!_______        ________!__________     _____!______ 01000060
      *     |              |      |                   |   |            |01010060
      * ____|    DEBUT     |     _|    TRAITEMENT     |   |    FIN     |01020099
      * |   |______________|    | |___________________|   |____________|01030060
      * |  ___________________  |  ___________________                  01040061
      * | |                   | | |                   |                 01050061
      * |_|  OUVRIR-FEC11001  | |_| LECTURE-FEC11001  |                 01060099
      * | |___________________| | |___________________|                 01070061
      * |  ___________________  |  ___________________                  01080061
      * | |                   | | |                   |   ==============01090099
      * |_|  OUVRIR-FEC11002  | |_|   APPEL-MEC000    |___=   MEC000   =01100099
      *   |___________________| | |___________________|   ==============01110099
      *                         |  ___________________                  01120099
      *   ___________________   | |                   |                 01130099
      *  |                   |  |_|     AFF-STATS     |                 01140099
      *  |     PLANTAGE      |  | |___________________|                 01150099
      *  |___________________|  |  ___________________                  01160099
      *   _________|_________   | |                   |                 01170099
      *  |                   |  |_|  AFF-STATS-FINALE |                 01180099
      *  |    FIN-ANORMALE   |    |___________________|                 01190099
      *  |___________________|                                          01200099
      *                                                                 01210061
      *=================================================================01220060
       PROCEDURE DIVISION.                                              01230001
                                                                        01240001
      *=================================================================01250001
       PROGRAMME-BEC110 SECTION.                                        01260001
      *=========================                                        01270001
      D    DISPLAY PGDISP 'SECTION: PROGRAMME-BEC110'.                  01280099
                                                                        01290095
           PERFORM 10000-DEBUT.                                         01410099
                                                                        01420001
           PERFORM 20000-TRAITEMENT.                                    01430099
                                                                        01440001
           PERFORM 30000-FIN.                                           01450099
                                                                        01460001
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    01470099
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        01480001
      *=================================================================01490001
       10000-DEBUT SECTION.                                             01500099
      *====================                                             01510099
      D    DISPLAY PGDISP 'SECTION: 10000-DEBUT'.                       01520099
                                                                        01530045
           MOVE WHEN-COMPILED       TO WS-WHEN-COMPILED.                01531099
                                                                        01532099
           DISPLAY PGDISP.                                              01533099
           DISPLAY PGDISP '********************************************'01533199
           DISPLAY PGDISP '*       DEBUT PROGRAMME BEC110             *'01534099
           DISPLAY PGDISP '********************************************'01535099
           DISPLAY PGDISP 'STAMP WHEN-COMPILED: '                       01536099
                           WS-WHEN-COMPILED(4:3)                        01537099
                           WS-WHEN-COMPILED(1:3)                        01538099
                           WS-WHEN-COMPILED(7:2)                        01539099
                       ' ' WS-WHEN-COMPILED(9:).                        01539199
                                                                        01539299
      *--> OUVERTURE DES FICHIERS.                                      01540044
           PERFORM 80010-OUVRIR-FEC11001.                               01550099
           PERFORM 80030-OUVRIR-FEC11002.                               01560099
                                                                        01570001
      *--> INITIALISATION DES INDICATEURS.                              01580040
           MOVE 'F'                 TO IND-FIN-FICHIER-FEC11001.        01590099
                                                                        01600025
      *--> INTIALISATION DES COMPTEURS.                                 01610040
           MOVE ZEROES              TO WS-NBR-MESSAGE-ECRIT-FICHIER     01620099
                                       WS-NBR-MESSAGE-TRAITE            01630099
                                       WS-NBR-ENR-LU-FEC11001           01640099
                                       .                                01650099
                                                                        01660095
      *--> INTIALISATION DES VARIABLES.                                 01670040
      *    MOVE SPACES              TO WS-TAMPON-XML                    01680099
      *{ Tr-Empty-Sentence 1.6                                                  
      *                                .                                01690099
      *                                                                         
      *}                                                                        
      *--> INITIALISATION DE LA ZONE DE COMM. AVEC LE SOUS-PGM MEC000   01710095
           INITIALIZE MEC000C-COMMAREA.                                 01720099
                                                                        01730090
      *=================================================================01740001
       20000-TRAITEMENT SECTION.                                        01750099
      *=========================                                        01760099
      D    DISPLAY PGDISP 'SECTION: 20000-TRAITEMENT'.                  01770099
                                                                        01780034
      *--> LECTURE DU PREMIER ENREGISTREMENT.                           01790040
           PERFORM 81000-LECTURE-FEC11001.                              01800099
                                                                        01810038
      *--> PARCOURS LE FICHIER AU COMPLET.                              01820088
           PERFORM UNTIL FIN-FICHIER-FEC11001                           01830099
                                                                        01840099
      *-->    APPEL DU SOUS-MODULE DE CONVERSION ET D'ENVOI MESSAGE MQ. 01850099
              PERFORM 70000-APPEL-MEC000                                01860099
                                                                        01870099
              PERFORM 21000-AFF-STATS                                   01880099
                                                                        01890099
      *-->    LECTURE DU PROCHAIN ENREGISTREMENT.                       01900099
              PERFORM 81000-LECTURE-FEC11001                            01910099
                                                                        01920050
           END-PERFORM.                                                 01930034
                                                                        01940099
      *--> EFFECTUE UN DERNIER APPEL AU SOUS-MODULE POUR LUI SIGNIFIER  01950099
      *--> LA FIN DE TRAITEMENT.                                        01960099
           PERFORM 70000-APPEL-MEC000.                                  01970099
                                                                        01980099
           PERFORM 21000-AFF-STATS-FINALE.                              01990099
                                                                        02000099
      *=================================================================02010099
       21000-AFF-STATS SECTION.                                         02020099
      *========================                                         02030099
      D    DISPLAY PGDISP 'SECTION: 21000-AFF-STATS'.                   02040099
                                                                        02050099
           IF ( MEC000C-NBR-MESSAGE-TRAITE                              02060099
              NOT = WS-NBR-MESSAGE-TRAITE )                             02070099
                                                                        02080099
              IF ( MEC000C-NBR-MESS-PAR-TABLE-T NOT = 0 )               02090099
                                                                        02100099
                  DISPLAY PGDISP "NBR MESS TABLE: "                     02110099
                                  MEC000C-TABLE-SI-TRAITE(1:6)          02120099
                                 " -> "                                 02130099
                                  MEC000C-TABLE-ECOMM-TRAITE(1:10)      02140099
                                 " "                                    02150099
                                  MEC000C-NBR-MESS-PAR-TABLE-T          02160099
                                 " AVEC: "                              02170099
                                  MEC000C-NBR-ENREG-PAR-TABLE-T         02180099
                                 " ENRS"                                02190099
                                                                        02200099
                  MOVE 0            TO MEC000C-NBR-MESS-PAR-TABLE-T     02210099
                                       MEC000C-NBR-ENREG-PAR-TABLE-T    02220099
                                                                        02230099
              END-IF                                                    02240099
                                                                        02250099
              ADD 1                 TO WS-NBR-MESSAGE-TRAITE            02260099
                                                                        02270099
              PERFORM 83000-ECRIRE-FEC11002                             02280099
                                                                        02290099
           END-IF.                                                      02300099
                                                                        02310099
      *=================================================================02320099
       21000-AFF-STATS-FINALE SECTION.                                  02330099
      *===============================                                  02340099
      D    DISPLAY PGDISP 'SECTION: 21000-AFF-STATS-FINALE'.            02350099
                                                                        02360099
           IF ( MEC000C-NBR-MESSAGE-TRAITE                              02370099
               NOT = WS-NBR-MESSAGE-TRAITE )                            02380099
                                                                        02390099
              ADD 1                 TO WS-NBR-MESSAGE-TRAITE            02400099
                                                                        02410099
              PERFORM 83000-ECRIRE-FEC11002                             02420099
                                                                        02430099
              DISPLAY PGDISP "NBR MESS TABLE: "                         02440099
                             MEC000C-TABLE-SI-TRAITE(1:6)               02450099
                             " -> "                                     02460099
                             MEC000C-TABLE-ECOMM-TRAITE(1:10)           02470099
                             " " MEC000C-NBR-MESS-PAR-TABLE-T           02480099
                             " AVEC: "                                  02490099
                             MEC000C-NBR-ENREG-PAR-TABLE-T              02500099
                             " ENRS"                                    02510099
           END-IF.                                                      02520099
                                                                        02530099
      *=================================================================02540063
       30000-FIN SECTION.                                               02550099
      *==================                                               02560099
      D    DISPLAY PGDISP 'SECTION: 30000-FIN'.                         02570099
                                                                        02580063
           DISPLAY PGDISP '********************************************'02590063
           DISPLAY PGDISP '*       FIN PROGRAMME BEC110               *'02600063
           DISPLAY PGDISP '********************************************'02610063
           DISPLAY PGDISP 'LONGUEUR D''UN MESSAGE                   : ' 02620099
                           MEC000C-LONG-MESSAGE-MAX.                    02630099
           DISPLAY PGDISP 'NBR ENREGS. LUS     DANS FICHIER ENTREE : '  02640099
                           WS-NBR-ENR-LU-FEC11001                       02650099
           DISPLAY PGDISP 'NBR MESSAGES TRAITE                     : '  02660099
                           MEC000C-NBR-MESSAGE-TRAITE                   02670099
           DISPLAY PGDISP 'NBR MESSAGES ECRIT  DANS FICHIER SORTIE : '  02680099
                           WS-NBR-MESSAGE-ECRIT-FICHIER.                02690099
           DISPLAY PGDISP 'NBR MESSAGES ENVOYE DANS FILE MQSERIES  : '  02700099
                           MEC000C-NBR-MESSAGE-ENVOYE.                  02710099
           DISPLAY PGDISP '********************************************'02711099
           DISPLAY PGDISP.                                              02712099
                                                                        02720063
      *--> FERMETURE DES FICHIERS.                                      02730063
           CLOSE FICHIER-FEC11001                                       02740099
                 FICHIER-FEC11002.                                      02750099
                                                                        02760063
      *=================================================================02770099
       70000-APPEL-MEC000 SECTION.                                      02780099
      *===========================                                      02790099
      D    DISPLAY PGDISP 'SECTION: 70000-APPEL-MEC000'.                02800099
                                                                        02810099
      *--> NOM DE LA TABLE "SI DARTY".                                  02820099
           MOVE FEC900-FICHIER      TO MEC000C-FICHIER-SI.              02830099
                                                                        02840099
      *--> CODE DE MISE A JOUR.                                         02850099
           MOVE FEC900-MAJ          TO MEC000C-CODE-MAJ.                02860099
                                                                        02870099
      *--> DONNEES A TRANSMETTRE.                                       02880099
           MOVE FEC900-DATA         TO MEC000C-DATA-SI.                 02890099
                                                                        02900099
      *--> LONGUEUR MAXIMUM DE LA DONNES TRANSMISE.                     02910099
           MOVE 252                 TO MEC000C-DATA-SI-L.               02920099
                                                                        02930099
      *--> INDICATEUR DE FIN DE TRAITEMENT.                             02940099
           IF FIN-FICHIER-FEC11001                                      02950099
              SET MEC000C-FIN-TRAITEMENT TO TRUE                        02960099
           ELSE                                                         02970099
              MOVE SPACE            TO MEC000C-IND-FIN-TRAITEMENT       02980099
           END-IF.                                                      02990099
                                                                        03000099
           MOVE 'MEC000'            TO WS-NOMPROG.                      03010099
           CALL WS-NOMPROG       USING MEC000C-COMMAREA.                03020099
                                                                        03030099
           IF ( MEC000C-CODE-RETOUR > 0 )                               03050099
                                                                        03060099
              DISPLAY PGDISP 'ERREUR APPEL DU PG MEC000'                03200099
              DISPLAY PGDISP 'MEC000C-CODE-RETOUR  :'                   03210099
                              MEC000C-CODE-RETOUR                       03220099
              DISPLAY PGDISP 'MEC000C-CODE-DB2     :'                   03230099
                              MEC000C-CODE-DB2                          03240099
              DISPLAY PGDISP 'MEC000C-MESSAGE      :'                   03250099
                              MEC000C-MESSAGE                           03260099
                                                                        03270099
      *-->    FIN ANORMALE DU PROGRAMME.                                03280099
              PERFORM 90000-PLANTAGE                                    03290099
                                                                        03300099
           END-IF.                                                      03310099
                                                                        03320099
      *=================================================================03330063
       80010-OUVRIR-FEC11001 SECTION.                                   03340099
      *==============================                                   03350099
      D    DISPLAY PGDISP 'SECTION: 80010-OUVRIR-FEC11001'.             03360099
                                                                        03370063
      *--> OUVERTURE DU FICHIERS FEC11001.                              03380099
           OPEN INPUT FICHIER-FEC11001.                                 03390099
                                                                        03400063
           IF NOT STATUT-FEC11001-OK                                    03410099
                                                                        03420063
              DISPLAY PGDISP 'ERREUR OUVERTURE DU FICHIER: FEC11001'    03430099
              CLOSE   FICHIER-FEC11001                                  03440099
                                                                        03450063
              PERFORM 90000-PLANTAGE                                    03460063
                                                                        03470063
           END-IF.                                                      03480063
                                                                        03490063
      *=================================================================03500063
       80030-OUVRIR-FEC11002 SECTION.                                   03510099
      *==============================                                   03520099
      D    DISPLAY PGDISP 'SECTION: 80030-OUVRIR-FEC11002'.             03530099
                                                                        03540063
      *--> OUVERTURE DU FICHIERS FEC11002.                              03550099
           OPEN OUTPUT FICHIER-FEC11002.                                03560099
                                                                        03570099
           IF NOT STATUT-FEC11002-OK                                    03580099
              DISPLAY PGDISP 'ERREUR OUVERTURE DU FICHIER: FEC11002'    03590099
              CLOSE   FICHIER-FEC11002                                  03600099
                                                                        03610099
              PERFORM 90000-PLANTAGE                                    03620063
                                                                        03630099
           END-IF.                                                      03640063
                                                                        03650063
      *=================================================================03660063
       81000-LECTURE-FEC11001 SECTION.                                  03670099
      *===============================                                  03680099
      D    DISPLAY PGDISP 'SECTION: 81000-LECTURE-FEC11001'.            03690099
                                                                        03700063
           MOVE SPACES              TO ENR-FEC11001                     03710099
                                       FEC900-ENREG.                    03720099
                                                                        03730063
           READ FICHIER-FEC11001                                        03740099
             AT END                                                     03750063
                MOVE 'T'            TO IND-FIN-FICHIER-FEC11001         03760099
                                                                        03770063
                IF ( WS-NBR-ENR-LU-FEC11001 = 0 )                       03780099
                    DISPLAY PGDISP 'FICHIER FEC11001 EN ENTREE VIDE'    03790099
                END-IF                                                  03800063
                                                                        03810063
           END-READ.                                                    03820063
                                                                        03830063
           IF NOT FIN-FICHIER-FEC11001                                  03840099
              ADD 1                 TO WS-NBR-ENR-LU-FEC11001           03850099
              MOVE ENR-FEC11001     TO FEC900-ENREG                     03860099
           END-IF.                                                      03870063
                                                                        03880063
      *=================================================================03890001
       83000-ECRIRE-FEC11002 SECTION.                                   03900099
      *==============================                                   03910099
      D    DISPLAY PGDISP 'SECTION: 83000-ECRIRE-FEC11002'.             03920099
                                                                        03930001
      *--> SI ON DESIRE LA SORTIE SUR FICHIER                           03940099
           IF MEC000C-WTRT-FICHIER-REQUIS                               03950099
                                                                        03960099
              MOVE SPACES           TO ENR-FEC11002                     03970099
                                                                        03980034
              IF MEC000C-TYPE-FICHIER-UNIQUE                            03981099
                 WRITE ENR-FEC11002                                     03990099
                  FROM                                                  04000099
                  MEC000C-MESSAGE-U-ENVOYE(1:MEC000C-MESSAGE-U-ENVOYE-L)04010099
              ELSE                                                      04011099
                 WRITE ENR-FEC11002                                     04011199
                  FROM                                                  04011299
                  MEC000C-MESSAGE-ENVOYE(1:MEC000C-MESSAGE-ENVOYE-L)    04011399
              END-IF                                                    04020099
                                                                        04021099
              ADD 1                 TO WS-NBR-MESSAGE-ECRIT-FICHIER     04030099
                                                                        04040099
           END-IF.                                                      04050099
                                                                        04060001
      *=================================================================04070001
       90000-PLANTAGE SECTION.                                          04080025
      *=======================                                          04090025
      D    DISPLAY PGDISP 'SECTION: 90000-PLANTAGE'.                    04100099
                                                                        04110001
           PERFORM 91000-FIN-ANORMALE.                                  04120025
                                                                        04130001
           MOVE 'BEC110'            TO ABEND-PROG.                      04140001
                                                                        04150001
           CALL 'ABEND'          USING ABEND-PROG                       04160001
                                       ABEND-MESS.                      04170001
                                                                        04180001
      *=================================================================04190001
       91000-FIN-ANORMALE SECTION.                                      04200025
      *===========================                                      04210025
      D    DISPLAY PGDISP 'SECTION: 91000-FIN-ANORMALE'.                04220099
                                                                        04230001
           DISPLAY PGDISP '*******************************************'.04240058
           DISPLAY PGDISP ' INTERRUPTION ANORMALE DU PROGRAMME     ***'.04250058
           DISPLAY PGDISP '               BEC110                   ***'.04260058
           DISPLAY PGDISP '*******************************************'.04270058
           DISPLAY PGDISP.                                              04271099
                                                                        04280001
      *--> FERMETURE DES FICHIERS.                                      04290025
           CLOSE FICHIER-FEC11001                                       04300099
                 FICHIER-FEC11002.                                      04310099
                                                                        04320001
