      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                         00000020
       PROGRAM-ID.  BIT031.                                             00000030
       AUTHOR.      DSA007.                                             00000040
                                                                        00000050
      *-----------------------------------------------------------      00000060
      *                                                                 00000070
      *    PROJET         : INVENTAIRE TOURNANT                         00000080
      *    PROGRAMME      : BIT031 INSPIRE DU BIT011                    00000090
      *    FONCTION       : EDITION DU SUPPORT DE COMPTAGE VERSION GS36 00000100
      *    DATE DE CREAT. : 20/07/2000                                  00000110
      *                                                                 00000120
      *-----------------------------------------------------------      00000130
      *                      ECRITURE DANS FICHIER IMPR IIT031          00000140
      *                      SEULEMENT SI CODE LIEU <  '090'            00000150
      *                      OU        SI SOCIETE   =  '907'            00000160
      *-----------------------------------------------------------      00000170
                                                                        00000180
       ENVIRONMENT DIVISION.                                            00000190
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                           00000200
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
      *{  Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                     
       SPECIAL-NAMES.                                                           
           COPY MW-COLSEQ.                                                      
      *}                                                                        
                                                                                
      *}                                                                        
       INPUT-OUTPUT     SECTION.                                        00000210
       FILE-CONTROL.                                                    00000220
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FDATE          ASSIGN  TO  FDATE.                    00000230
      *--                                                                       
           SELECT  FDATE          ASSIGN  TO  FDATE                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FIT011         ASSIGN  TO  FIT011.                   00000240
      *--                                                                       
           SELECT  FIT011         ASSIGN  TO  FIT011                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  IIT031         ASSIGN  TO  IIT031.                   00000250
      *--                                                                       
           SELECT  IIT031         ASSIGN  TO  IIT031                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FEDG36         ASSIGN  TO  FEDG36.                   00000260
      *--                                                                       
           SELECT  FEDG36         ASSIGN  TO  FEDG36                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                   00000270
       FILE SECTION.                                                    00000280
                                                                        00000290
      * ----------------------------------------------------------      00000300
      *            D E S C R I P T I O N     F I C H I E R              00000310
      * ----------------------------------------------------------      00000320
                                                                        00000330
                                                                        00000340
       FD  FDATE                                                        00000350
           RECORDING F                                                  00000360
           BLOCK CONTAINS 0 RECORDS                                     00000370
           LABEL RECORD STANDARD.                                       00000380
                                                                        00000390
       01  ENR-FDATE.                                                   00000400
           03  FDATE-JJ          PIC X(02).                             00000410
           03  FDATE-MM          PIC X(02).                             00000420
           03  FDATE-SS          PIC X(02).                             00000430
           03  FDATE-AA          PIC X(02).                             00000440
           03  FILLER            PIC X(72).                             00000450
                                                                        00000460
       FD  FIT011                                                       00000470
           RECORDING F                                                  00000480
           BLOCK CONTAINS 0 RECORDS                                     00000490
           LABEL RECORD STANDARD.                                       00000500
       01  FIT011-ENREG          PIC X(90).                             00000510
                                                                        00000520
       FD  IIT031                                                       00000530
           RECORDING F                                                  00000540
           BLOCK CONTAINS 0 RECORDS                                     00000550
           LABEL RECORD STANDARD.                                       00000560
       01  IIT031-ENREG.                                                00000570
           05     IIT031-ASA     PIC X(001).                            00000580
           05     IIT031-DATA    PIC X(132).                            00000590
                                                                        00000600
       FD  FEDG36                                                       00000610
           RECORDING F                                                  00000620
           BLOCK CONTAINS 0 RECORDS                                     00000630
           LABEL RECORD STANDARD.                                       00000640
       01  FEDG36-ENREG     PIC X(156).                                 00000650
                                                                        00000660
                                                                        00000670
      * ----------------------------------------------------------      00000680
      *                W O R K I N G  -  S T O R A G E                  00000690
      * ----------------------------------------------------------      00000700
                                                                        00000710
       WORKING-STORAGE SECTION.                                         00000720
                                                                        00000730
       77  W-NPAGE                       PIC S9(4) COMP-3  VALUE 0.     00000740
       77  W-NLIG                        PIC S9(4) COMP-3  VALUE 0.     00000750
       77  W-NLIG-MAX                    PIC S9(4) COMP-3  VALUE 62.    00000760
                                                                        00000770
       77  W-NSOC                        PIC X(03)         VALUE SPACE. 00000780
       77  W-CFAM                        PIC X(05)         VALUE SPACE. 00000790
       77  W-CMARQ                       PIC X(05)         VALUE SPACE. 00000800
                                                                        00000810
       77  W-LUS-FIT011                  PIC S9(7) COMP-3  VALUE ZERO.  00000820
       77  W-ECRITS-IIT031               PIC S9(7) COMP-3  VALUE ZERO.  00000830
       77  W-ECRITS-FEDG36               PIC S9(7) COMP-3  VALUE ZERO.  00000840
       77  W-PAGE-IIT031                 PIC S9(7) COMP-3  VALUE ZERO.  00000850
                                                                        00000860
           COPY SWIT010.                                                00000870
                                                                        00000880
           COPY ABENDCOP.                                               00000890
                                                                        00000900
           COPY WORKDATC.                                               00000910
                                                                        00000920
       01  FEDG36-WORK.                                                 00000930
           03  FEDG36-CETAT      PIC X(08).                             00000940
           03  FEDG36-NSEQ       PIC S9(07) COMP-3.                     00000950
           03  FEDG36-NSOC       PIC X(03).                             00000960
           03  FEDG36-NLIEU      PIC X(03).                             00000970
           03  FEDG36-CRIT       PIC X(05).                             00000980
           03  FEDG36-ASA        PIC X(01).                             00000990
           03  FEDG36-DATA       PIC X(132).                            00001000
                                                                        00001010
                                                                        00001020
       01  ETAT-FIT011                   PIC  X(1)         VALUE '0'.   00001030
           88   NON-FIN-FIT011                             VALUE '0'.   00001040
           88       FIN-FIT011                             VALUE '1'.   00001050
                                                                        00001060
       01  W-SSAAMMJJ.                                                  00001070
           02 W-SS                       PIC X(2).                      00001080
           02 W-AA                       PIC X(2).                      00001090
           02 W-MM                       PIC X(2).                      00001100
           02 W-JJ                       PIC X(2).                      00001110
       01  W-JJ-MM-AA                    PIC X(8).                      00001120
                                                                        00001130
      *----------------------------------------------------------------*00001140
      *                                                                 00001150
      * DEFINITION DES LIGNES D'EDITION                                 00001160
      *                                                                 00001170
      *----------------------------------------------------------------*00001180
                                                                        00001190
       01  IIT031-LIGNES.                                               00001200
      *--------------------------- LIGNE E00  --------------------------00001210
           05  IIT031-E00.                                              00001220
               10  FILLER                         PIC X(58) VALUE       00001230
           'IIT031                                 SUPPORT DE COMPTAGE'.00001240
               10  FILLER                         PIC X(23) VALUE       00001250
           ' INVENTAIRE TOURNANT : '.                                   00001260
               10  IIT031-E00-NINVENTAIRE         PIC X(05).            00001270
               10  FILLER                         PIC X(19) VALUE       00001280
           '                   '.                                       00001290
               10  IIT031-E00-DATEDITE            PIC X(10).            00001300
               10  FILLER                         PIC X(03) VALUE       00001310
           '   '.                                                       00001320
               10  IIT031-E00-HEUREDITE           PIC 99.               00001330
               10  FILLER                         PIC X VALUE ' '.      00001340
               10  IIT031-E00-MINUEDITE           PIC 99.               00001350
               10  FILLER                         PIC X(06) VALUE       00001360
           ' PAGE '.                                                    00001370
               10  IIT031-E00-NOPAGE              PIC ZZZ.              00001380
      *--------------------------- LIGNE E04  --------------------------00001390
           05  IIT031-E04.                                              00001400
               10  FILLER                         PIC X(07) VALUE       00001410
           'LIEU : '.                                                   00001420
               10  IIT031-E04-NSOCIETE            PIC X(03).            00001430
               10  FILLER                         PIC X(01) VALUE       00001440
           ' '.                                                         00001450
               10  IIT031-E04-NLIEU               PIC X(03).            00001460
               10  FILLER                         PIC X(14) VALUE       00001470
           '              '.                                            00001480
               10  IIT031-E04-LLIEU               PIC X(20).            00001490
               10  FILLER                         PIC X(58) VALUE       00001500
           '                                                          '.00001510
               10  FILLER                         PIC X(26) VALUE       00001520
           '                          '.                                00001530
      *--------------------------- LIGNE E05  --------------------------00001540
           05  IIT031-E05.                                              00001550
               10  FILLER                         PIC X(58) VALUE       00001560
           '                                                          '.00001570
               10  FILLER                         PIC X(58) VALUE       00001580
           '                                                          '.00001590
               10  FILLER                         PIC X(16) VALUE       00001600
           '                '.                                          00001610
      *--------------------------- LIGNE E06  --------------------------00001620
           05  IIT031-E06.                                              00001630
               10  FILLER                         PIC X(32) VALUE       00001640
           'DATE D''EDITION DU SUPPORT     : '.                         00001650
               10  IIT031-E06-DEDITSUPPORT        PIC X(10).            00001660
               10  FILLER                         PIC X(58) VALUE       00001670
           '                                                          '.00001680
               10  FILLER                         PIC X(32) VALUE       00001690
           '                                '.                          00001700
      *--------------------------- LIGNE E07  --------------------------00001710
           05  IIT031-E07.                                              00001720
               10  FILLER                         PIC X(32) VALUE       00001730
           'DATE DE COMPTAGE              : '.                          00001740
               10  IIT031-E07-DCOMPTAGE           PIC X(10).            00001750
               10  FILLER                         PIC X(58) VALUE       00001760
           '                                                          '.00001770
               10  FILLER                         PIC X(32) VALUE       00001780
           '                                '.                          00001790
      *--------------------------- LIGNE E08  --------------------------00001800
           05  IIT031-E08.                                              00001810
               10  FILLER                         PIC X(32) VALUE       00001820
           'RAPPROCHEMENT STOCK THEORIQUE : '.                          00001830
               10  IIT031-E08-DSTOCKTHEO          PIC X(10).            00001840
               10  FILLER                         PIC X(58) VALUE       00001850
           '                                                          '.00001860
               10  FILLER                         PIC X(32) VALUE       00001870
           '                                '.                          00001880
      *--------------------------- LIGNE E09  --------------------------00001890
           05  IIT031-E09.                                              00001900
               10  FILLER                         PIC X(58) VALUE       00001910
           '                                                          '.00001920
               10  FILLER                         PIC X(58) VALUE       00001930
           '                                                          '.00001940
               10  FILLER                         PIC X(16) VALUE       00001950
           '                '.                                          00001960
      *--------------------------- LIGNE E10  --------------------------00001970
           05  IIT031-E10.                                              00001980
               10  FILLER                         PIC X(58) VALUE       00001990
           '                                                          '.00002000
               10  FILLER                         PIC X(58) VALUE       00002010
           '                                                          '.00002020
               10  FILLER                         PIC X(16) VALUE       00002030
           '                '.                                          00002040
      *--------------------------- LIGNE E14  --------------------------00002050
           05  IIT031-E14.                                              00002060
               10  FILLER                         PIC X(58) VALUE       00002070
           '+---------+--------+----------------------+---------+-----'.00002080
               10  FILLER                         PIC X(58) VALUE       00002090
           '-------+------------+------------+------------+-----------'.00002100
               10  FILLER                         PIC X(16) VALUE       00002110
           '-+-------------+'.                                          00002120
      *--------------------------- LIGNE E15  --------------------------00002130
           05  IIT031-E15.                                              00002140
               10  FILLER                         PIC X(58) VALUE       00002150
           '! FAMILLE ! MARQUE !      REFERENCE       !  CODIC  !    E'.00002160
               10  FILLER                         PIC X(58) VALUE       00002170
           'XPO    !  RESERVE   ! RES CLIENT !     HS     !    PRET   '.00002180
               10  FILLER                         PIC X(16) VALUE       00002190
           ' !    TOTAL    !'.                                          00002200
      *--------------------------- LIGNE E16  --------------------------00002210
           05  IIT031-E16.                                              00002220
               10  FILLER                         PIC X(58) VALUE       00002230
           '+---------+--------+----------------------+---------+-----'.00002240
               10  FILLER                         PIC X(58) VALUE       00002250
           '-------+------------+------------+------------+-----------'.00002260
               10  FILLER                         PIC X(16) VALUE       00002270
           '-+-------------+'.                                          00002280
      *--------------------------- LIGNE D05  --------------------------00002290
           05  IIT031-D05.                                              00002300
               10  FILLER                         PIC X(03) VALUE       00002310
           '!  '.                                                       00002320
               10  IIT031-D05-CFAM                PIC X(05).            00002330
               10  FILLER                         PIC X(04) VALUE       00002340
           '  ! '.                                                      00002350
               10  IIT031-D05-CMARQ               PIC X(05).            00002360
               10  FILLER                         PIC X(04) VALUE       00002370
           '  ! '.                                                      00002380
               10  IIT031-D05-LREFFOURN           PIC X(20).            00002390
               10  FILLER                         PIC X(03) VALUE       00002400
           ' ! '.                                                       00002410
               10  IIT031-D05-NCODIC              PIC X(07).            00002420
               10  FILLER                         PIC X(58) VALUE       00002430
           ' !            !            !            !            !    '.00002440
               10  FILLER                         PIC X(23) VALUE       00002450
           '        !             !'.                                   00002460
      *--------------------------- LIGNE D10  --------------------------00002470
           05  IIT031-D10.                                              00002480
               10  FILLER                         PIC X(58) VALUE       00002490
           '+---------+--------+----------------------+---------+-----'.00002500
               10  FILLER                         PIC X(58) VALUE       00002510
           '-------+------------+------------+------------+-----------'.00002520
               10  FILLER                         PIC X(16) VALUE       00002530
           '-+-------------+'.                                          00002540
                                                                        00002550
                                                                        00002560
       PROCEDURE DIVISION.                                              00002570
                                                                        00002580
      *---------------------------------------------------------------  00002590
      * SQUELETTE DU PROGRAMME                                          00002600
      *---------------------------------------------------------------  00002610
                                                                        00002620
           PERFORM DEBUT-PGM-BIT031.                                    00002630
                                                                        00002640
           PERFORM TRAIT-FIT011 UNTIL FIN-FIT011.                       00002650
                                                                        00002660
           PERFORM FIN-PGM-BIT031.                                      00002670
                                                                        00002680
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00002690
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        00002700
      *---------------------------------------------------------------  00002710
      *                                                                 00002720
      *                                                                 00002730
      *---------------------------------------------------------------  00002740
      *                  D E B U T   B I T 0 3 1                        00002750
      *---------------------------------------------------------------  00002760
       DEBUT-PGM-BIT031.                                                00002770
           OPEN INPUT         FDATE FIT011                              00002780
                OUTPUT IIT031 FEDG36.                                   00002790
                                                                        00002800
           READ FDATE INTO GFJJMMSSAA                                   00002810
                END   MOVE 'PAS DE DATE EN ENTREE' TO ABEND-MESS        00002820
                      PERFORM FIN-ANORMALE                              00002830
           END-READ.                                                    00002840
                                                                        00002850
           MOVE '1' TO GFDATA                                           00002860
           CALL 'BETDATC' USING WORK-BETDATC                            00002870
                                                                        00002880
           IF GFVDAT NOT = '1'                                          00002890
              MOVE 'DATE PARAM. REJETEE PAR BETDATC' TO ABEND-MESS      00002900
              PERFORM FIN-ANORMALE                                      00002910
           END-IF                                                       00002920
                                                                        00002930
           DISPLAY '*** DATE EN ENTREE '                                00002940
                   FDATE-JJ '/' FDATE-MM '/'                            00002950
                   FDATE-SS     FDATE-AA.                               00002960
                                                                        00002970
      *--> +1 SUR DATE EN ENTREE                                        00002980
           ADD   1  TO GFQNT0.                                          00002990
           MOVE '3' TO GFDATA.                                          00003000
           CALL 'BETDATC' USING WORK-BETDATC                            00003010
                                                                        00003020
           DISPLAY '*** EDITION DES SUPPORTS DE COMPTAGE DU '           00003030
                   GFJJ  '/' GFMM '/' GFSS GFAA.                        00003040
                                                                        00003050
                                                                        00003060
           INITIALIZE         FEDG36-WORK.                              00003070
                                                                        00003080
           MOVE 'IIT031'   TO FEDG36-CETAT.                             00003090
                                                                        00003100
       FIN-DEBUT-PGM-BIT031.  EXIT.                                     00003110
                                                                        00003120
                                                                        00003130
      *---------------------------------------------------------------  00003140
      * FIN DU PROGRAMME / COMPTE RENDU                                 00003150
      *---------------------------------------------------------------  00003160
       FIN-PGM-BIT031.                                                  00003170
           PERFORM FERMETURE-FICHIERS.                                  00003180
                                                                        00003190
           DISPLAY '*------------------------------------------------'  00003200
           DISPLAY '*'                                                  00003210
           DISPLAY '*   P R O G R A M M E      B I T 0 3 1  :   O  K '  00003220
           DISPLAY '*'                                                  00003230
           DISPLAY '*'                                                  00003240
           DISPLAY '* ' W-LUS-FIT011    ' LUS    DANS FICHIER FIT011;.' 00003250
           DISPLAY '*'                                                  00003260
           DISPLAY '* ' W-ECRITS-IIT031 ' ECRITS DANS FICHIER IIT031 '  00003270
           DISPLAY '*'                                                  00003280
           DISPLAY '* ' W-ECRITS-FEDG36 ' ECRITS DANS FICHIER FEDG36;.' 00003290
           DISPLAY '*'                                                  00003300
           DISPLAY '* (' W-PAGE-IIT031  ' PAGES AU TOTAL);'             00003310
           DISPLAY '*'                                                  00003320
           DISPLAY '*------------------------------------------------'. 00003330
       FIN-FIN-PGM-BIT031. EXIT.                                        00003340
                                                                        00003350
       FERMETURE-FICHIERS.                                              00003360
           CLOSE        FDATE FIT011 IIT031 FEDG36.                     00003370
       FIN-FERMETURE-FICHIERS. EXIT.                                    00003380
      *---------------------------------------------------------------  00003390
      *                                                                 00003400
      *                                                                 00003410
      *---------------------------------------------------------------  00003420
      * LECTURE DES ENREGISTREMENTS.                                    00003430
      *---------------------------------------------------------------* 00003440
       TRAIT-FIT011.                                                    00003450
                                                                        00003460
           PERFORM LECTURE.                                             00003470
      *                                                                 00003480
           MOVE FIT010-NSOCIETE TO IIT031-E04-NSOCIETE W-NSOC           00003490
                                   FEDG36-NSOC.                         00003500
                                                                        00003510
           MOVE SPACE TO IIT031-D05-CFAM.                               00003520
           MOVE SPACE TO IIT031-D05-CMARQ.                              00003530
                                                                        00003540
           IF  NON-FIN-FIT011                                           00003550
               ADD 1 TO W-LUS-FIT011                                    00003560
                                                                        00003570
               IF       FIT010-NLIEU     NOT =  FEDG36-NLIEU            00003580
                        MOVE    0            TO FEDG36-NSEQ             00003590
                        MOVE    FIT010-NLIEU TO FEDG36-NLIEU            00003600
               END-IF                                                   00003610
                                                                        00003620
               IF       FIT010-NLIEU       NOT = IIT031-E04-NLIEU       00003630
                     OR FIT010-NINVENTAIRE NOT = IIT031-E00-NINVENTAIRE 00003640
                                                                        00003650
                        MOVE     FIT010-NLIEU       TO IIT031-E04-NLIEU 00003660
                        MOVE     FIT010-NINVENTAIRE                     00003670
                                              TO IIT031-E00-NINVENTAIRE 00003680
                        MOVE     GFJMA-4      TO IIT031-E06-DEDITSUPPORT00003690
                                                                        00003700
                        MOVE     FIT010-DCOMPTAGE   TO W-SSAAMMJJ       00003710
                        PERFORM  T-DATE                                 00003720
                        MOVE     W-JJ-MM-AA   TO IIT031-E07-DCOMPTAGE   00003730
                                                                        00003740
                        MOVE     FIT010-DSTOCKTHEO  TO W-SSAAMMJJ       00003750
                        PERFORM  T-DATE                                 00003760
                        MOVE     W-JJ-MM-AA   TO IIT031-E08-DSTOCKTHEO  00003770
                        MOVE     0                  TO W-NPAGE          00003780
                        MOVE     99                 TO W-NLIG           00003790
               END-IF                                                   00003800
                                                                        00003810
               IF       W-NLIG    > W-NLIG-MAX                          00003820
                        PERFORM EDIT-ENTETE                             00003830
                        MOVE      FIT010-CFAM        TO W-CFAM          00003840
                                                       IIT031-D05-CFAM  00003850
                        MOVE      FIT010-CMARQ       TO W-CMARQ         00003860
                                                       IIT031-D05-CMARQ 00003870
               END-IF                                                   00003880
                                                                        00003890
               IF       FIT010-CFAM   NOT = W-CFAM                      00003900
                        MOVE      FIT010-CFAM        TO W-CFAM          00003910
                                                        IIT031-D05-CFAM 00003920
                        MOVE      SPACE              TO W-CMARQ         00003930
               END-IF                                                   00003940
                                                                        00003950
               IF       FIT010-CMARQ  NOT = W-CMARQ                     00003960
                        MOVE     FIT010-CMARQ       TO W-CMARQ          00003970
                                                       IIT031-D05-CMARQ 00003980
               END-IF                                                   00003990
                                                                        00004000
               MOVE     FIT010-LREFFOURN TO IIT031-D05-LREFFOURN        00004010
               MOVE     FIT010-NCODIC    TO IIT031-D05-NCODIC           00004020
               MOVE     ' '              TO IIT031-ASA                  00004030
               MOVE     IIT031-D05       TO IIT031-DATA                 00004040
               PERFORM  EDIT-LIGNE                                      00004050
               MOVE ' '                  TO IIT031-ASA                  00004060
               MOVE IIT031-D10           TO IIT031-DATA                 00004070
               PERFORM EDIT-LIGNE                                       00004080
                                                                        00004090
           END-IF.                                                      00004100
                                                                        00004110
       LECTURE.                                                         00004120
                                                                        00004130
           READ     FIT011   INTO FIT010-DSECT                          00004140
                    AT END   SET FIN-FIT011 TO TRUE                     00004150
           END-READ.                                                    00004160
                                                                        00004170
      *-----------------------------------------------------------------00004180
      * EDITION DE L'ENTETE DU DOCUMENT                                 00004190
      *-----------------------------------------------------------------00004200
       EDIT-ENTETE.                                                     00004210
               ADD   1 TO W-NPAGE                                       00004220
               MOVE       W-NPAGE TO IIT031-E00-NOPAGE.                 00004230
               MOVE '1'                  TO IIT031-ASA.                 00004240
               MOVE IIT031-E00           TO IIT031-DATA.                00004250
               PERFORM EDIT-LIGNE.                                      00004260
                                                                        00004270
               MOVE '0'                  TO IIT031-ASA.                 00004280
               MOVE IIT031-E04           TO IIT031-DATA.                00004290
               PERFORM EDIT-LIGNE.                                      00004300
                                                                        00004310
               MOVE '0'                  TO IIT031-ASA.                 00004320
               MOVE IIT031-E06           TO IIT031-DATA.                00004330
               PERFORM EDIT-LIGNE.                                      00004340
                                                                        00004350
               MOVE ' '                  TO IIT031-ASA.                 00004360
               MOVE IIT031-E07           TO IIT031-DATA.                00004370
               PERFORM EDIT-LIGNE.                                      00004380
                                                                        00004390
               MOVE ' '                  TO IIT031-ASA.                 00004400
               MOVE IIT031-E08           TO IIT031-DATA.                00004410
               PERFORM EDIT-LIGNE.                                      00004420
                                                                        00004430
               MOVE '0'                  TO IIT031-ASA.                 00004440
               MOVE IIT031-E14           TO IIT031-DATA.                00004450
               PERFORM EDIT-LIGNE.                                      00004460
                                                                        00004470
               MOVE ' '                  TO IIT031-ASA.                 00004480
               MOVE IIT031-E15           TO IIT031-DATA.                00004490
               PERFORM EDIT-LIGNE.                                      00004500
                                                                        00004510
               MOVE ' '                  TO IIT031-ASA.                 00004520
               MOVE IIT031-E14           TO IIT031-DATA.                00004530
               PERFORM EDIT-LIGNE.                                      00004540
                                                                        00004550
               MOVE '0'                  TO IIT031-ASA.                 00004560
               MOVE IIT031-E14           TO IIT031-DATA.                00004570
               PERFORM EDIT-LIGNE.                                      00004580
                                                                        00004590
      *-----------------------------------------------------------------00004600
      * EDITION D'UNE LIGNE                                             00004610
      *-----------------------------------------------------------------00004620
       EDIT-LIGNE.                                                      00004630
      *                                                                 00004640
      * MODIF DU 21/05/2001 M.BRAULT                                    00004650
      *                                                                 00004660
      * ---> ON NE PASSE PLUS PAR LA PROCEDURE FEGD36 POUR LES FILIALES 00004670
      *                                                                 00004680
      *                                                                 00004690
           WRITE      IIT031-ENREG.                                     00004700
           ADD 1 TO W-ECRITS-IIT031.                                    00004710
      *    SI ENTREPOT OU SOCIETE 907 ECRIT SUR IIT031                  00004720
      *    IF FEDG36-NLIEU NOT < '090'                                  00004730
      *    OR W-NSOC         =   '907'                                  00004740
      *       WRITE      IIT031-ENREG                                   00004750
      *       ADD 1 TO W-ECRITS-IIT031                                  00004760
      *    ELSE                                                         00004770
      *                                                                 00004780
      *    SINON       ECRIT SUR FEDG36 FICHIER EDITION 36              00004790
      *                                                                 00004800
      *       ADD  1             TO FEDG36-NSEQ                         00004810
      *       MOVE IIT031-ASA    TO FEDG36-ASA                          00004820
      *       MOVE IIT031-DATA   TO FEDG36-DATA                         00004830
      *       WRITE                 FEDG36-ENREG FROM FEDG36-WORK       00004840
      *       ADD  1             TO W-ECRITS-FEDG36                     00004850
      *    END-IF.                                                      00004860
           EVALUATE IIT031-ASA                                          00004870
              WHEN '1'        ADD  1 TO W-PAGE-IIT031                   00004880
                              MOVE 1 TO W-NLIG                          00004890
              WHEN ' '        ADD  1 TO W-NLIG                          00004900
              WHEN '0'        ADD  2 TO W-NLIG                          00004910
              WHEN '-'        ADD  3 TO W-NLIG                          00004920
           END-EVALUATE.                                                00004930
       FIN-EDIT-LIGNE. EXIT.                                            00004940
      *-----------------------------------------------------------------00004950
      * INVERSION DATE                                                  00004960
      *-----------------------------------------------------------------00004970
       T-DATE.                                                          00004980
           STRING W-JJ '/' W-MM '/' W-AA '/'                            00004990
                  DELIMITED SIZE INTO W-JJ-MM-AA.                       00005000
       FIN-T-DATE. EXIT.                                                00005010
                                                                        00005020
      *-----------------------------------------------------------------00005030
      * FIN POUR ABEND                                                  00005040
      *-----------------------------------------------------------------00005050
       FIN-ANORMALE.                                                    00005060
           PERFORM  FERMETURE-FICHIERS.                                 00005070
           MOVE  'BIT011'                  TO  ABEND-PROG.              00005080
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                00005090
       FIN-FIN-ANORMALE.    EXIT.                                       00005100
