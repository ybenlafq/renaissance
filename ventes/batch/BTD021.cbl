      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. BTD021.                                                      
       AUTHOR. SG.                                                              
      *----------------------------------------------------------------*        
      *                                                                *        
      *  Traitement fichier financement                                *        
      *                        BTD020                                  *        
      *                        BTD021                                  *        
      *                                                                *        
      *----------------------------------------------------------------*        
      *  Description courte : matching des fichiers                    *        
      *----------------------------------------------------------------*        
      *  Fichiers en entr�e :                                          *        
      *  - FTD02DE: Fichier DAF t�l�phonie (l = 500)                   *        
      *  - FTD020E: Extraction des dossiers TDxx ( l = 230 )           *        
      *----------------------------------------------------------------*        
      *  Fichiers en sortie :                                          *        
      *  - FTD02DS: Fichier DAF t�l�phonie (l = 500) ( non match� )    *        
      *  - FTD020S: Extraction des dossiers TDxx ( l = 230) ( non match�        
      *  - FTD021 : Fichier de matching  ( l = 500)                    *        
      *----------------------------------------------------------------*        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FTD02DE ASSIGN TO FTD02DE.                                    
      *--                                                                       
           SELECT FTD02DE ASSIGN TO FTD02DE                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FTD02DS ASSIGN TO FTD02DS.                                    
      *--                                                                       
           SELECT FTD02DS ASSIGN TO FTD02DS                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FTD020E ASSIGN TO FTD020E.                                    
      *--                                                                       
           SELECT FTD020E ASSIGN TO FTD020E                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FTD020S ASSIGN TO FTD020S.                                    
      *--                                                                       
           SELECT FTD020S ASSIGN TO FTD020S                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FTD021  ASSIGN TO FTD021.                                     
      *--                                                                       
           SELECT FTD021  ASSIGN TO FTD021                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FPARAM  ASSIGN TO FPARAM.                                     
      *--                                                                       
           SELECT FPARAM  ASSIGN TO FPARAM                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *--*                Fichier DAF                                           
       FD  FTD02DE RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.           
       01  ENR-FTD02DE PIC X(500).                                              
       FD  FTD02DS RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.           
       01  ENR-FTD02DS PIC X(500).                                              
      *--*                Fichier du BTD020 ( extraction TDxx )                 
       FD  FTD020E RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.           
       01  ENR-FTD020E PIC X(230).                                              
       FD  FTD020S RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.           
       01  ENR-FTD020S PIC X(230).                                              
      *--*                Fichier r�sultat du matching                          
       FD  FTD021 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  ENR-FTD021 PIC X(500).                                               
      *--*                Fichier param�tre du matching                         
       FD  FPARAM RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
      *MW DAR-2                                                                 
       01  MW-FILLER PIC X(80).                                                 
      *----------------------------------------------------------------*        
      * WSS                                                                     
      *----------------------------------------------------------------*        
       WORKING-STORAGE SECTION.                                                 
      * FPARAM contient indice de la cl� de matching                            
       01  W-FPARAM    PIC X(1).                                                
       01  W-IPARAM    PIC 9(1).                                                
      *----------------------------------------------------------------*        
      *            FTD02D ( fichier DAF t�l�phonie )  l = 500                   
      *----------------------------------------------------------------*        
       01 W-FTD02D.                                                             
          02 W-FTD02D-CMARQ         PIC X(5).                                   
          02 W-FTD02D-CLE OCCURS 5.                                             
             05 W-FTD02D-CCTRL      PIC X(5).                                   
             05 W-FTD02D-VCTRL      PIC X(30).                                  
          02 W-FTD02D-PART1-DSI     PIC X(50).                                  
          02 W-FTD02D-PART2-INTEGRATION PIC X(70).                              
          02 W-FTD02D-PART3-UTILISATEUR PIC X(200).                             
       01  CLE-FTD02D PIC X(45).                                                
       01  CLE-FTD02D-ANCIEN PIC X(45).                                         
       01  W-CPT-FTD02D PIC S9(7) PACKED-DECIMAL VALUE 0.                       
       01  FILLER PIC X VALUE SPACE.                                            
           88  FIN-FTD02D VALUE 'O'.                                            
      *----------------------------------------------------------------*        
      *                  FTD020 (fichier in )    l = 230                        
      *----------------------------------------------------------------*        
       01 W-FTD020.                                                             
          02 W-FTD020-CMARQ         PIC X(5).                                   
          02 W-FTD020-CLE OCCURS 5.                                             
             05 W-FTD020-CCTRL      PIC X(5).                                   
             05 W-FTD020-VCTRL      PIC X(30).                                  
          02 W-FTD020-PART1-DSI     PIC X(50).                                  
       01  CLE-FTD020 PIC X(45).                                                
       01  W-CPT-FTD020 PIC S9(7) PACKED-DECIMAL VALUE 0.                       
       01  FILLER PIC X VALUE SPACE.                                            
           88  FIN-FTD020 VALUE 'O'.                                            
      *----------------------------------------------------------------*        
      *                     FTD021 (fichier r�sultat )       l = 500            
      *----------------------------------------------------------------*        
       01  W-FTD021                PIC X(500).                                  
       01  W-CPT-FTD021 PIC S9(7) PACKED-DECIMAL VALUE 0.                       
       01  W-CPT-FTD020-SEUL       PIC S9(7) PACKED-DECIMAL VALUE 0.            
       01  W-CPT-FTD02D-SEUL       PIC S9(7) PACKED-DECIMAL VALUE 0.            
       01  W-CPT-MATCHES           PIC S9(7) PACKED-DECIMAL VALUE 0.            
       01  W-CPT-MAJ               PIC S9(7) PACKED-DECIMAL VALUE 0.            
       01 UPDATETD00               PIC X.                                       
          88 CONTROLE-OK           VALUE '0'.                                   
          88 CONTROLE-KO           VALUE '1'.                                   
      *                                                                         
       01 MESSAGE-ERREUR.                                                       
          02 FILLER          PIC X(9) VALUE 'NVENTE : '.                        
          02 MESS-NVENTE     PIC X(7) VALUE SPACE.                              
          02 FILLER          PIC X(1) value ' '.                                
          02 MESS-ERREUR     PIC X(35) VALUE SPACE.                             
      *----------------------------------------------------------------*        
      *                              DB2                              *         
      *----------------------------------------------------------------*        
       01  MSG-SQL.                                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSG-SQL-LENGTH PIC S9(4) COMP VALUE +240.                         
      *--                                                                       
           02 MSG-SQL-LENGTH PIC S9(4) COMP-5 VALUE +240.                       
      *}                                                                        
           02 MSG-SQL-MSG    PIC X(80) OCCURS 3.                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  MSG-SQL-LRECL PIC S9(8) COMP VALUE +80.                              
      *--                                                                       
       01  MSG-SQL-LRECL PIC S9(8) COMP-5 VALUE +80.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-SQL              PIC S9(4) BINARY.                                 
      *--                                                                       
       01  I-SQL              PIC S9(4) COMP-5.                                 
      *}                                                                        
       01  DSNTIAR PIC X(8) VALUE 'DSNTIAR'.                                    
      *---------------------------------------------------------------*         
      * CODES RETOURS.                                                *         
      *---------------------------------------------------------------*         
      *                                                                         
       01  CODE-RETOUR                 PIC  X(01)  VALUE  '0'.                  
           88  TROUVE                              VALUE  '0'.                  
           88  W-NOT-FIND                          VALUE  '1'.                  
           88  NORMAL                              VALUE  '0'.                  
           88  NON-TROUVE                          VALUE  '1'.                  
           88  ANORMAL                             VALUE  '1'.                  
           88  EXISTE-DEJA                         VALUE  '2'.                  
           88  DOUBLE                              VALUE  '7'.                  
      *----------------------------------------------------------------*        
      * DB2                                                                     
      *----------------------------------------------------------------*        
      *    EXEC SQL INCLUDE SQLCA     END-EXEC.                                 
      *    EXEC SQL INCLUDE RVTD0000  END-EXEC.                                 
           COPY ABENDCOP.                                                       
       01  PIC-EDIT PIC ZZ.ZZZ.ZZ9.                                             
      * pour dire quand est-ce que le programme a �t� compil�                   
       01  W-WHEN-COMPILED PIC X(16).                                           
      *----------------------------------------------------------------*        
      * PD                                                                      
      *----------------------------------------------------------------*        
       PROCEDURE DIVISION.                                                      
           PERFORM MODULE-ENTREE.                                               
           PERFORM MODULE-TRAITEMENT.                                           
           PERFORM MODULE-SORTIE.                                               
      *----------------------------------------------------------------*        
      *                         Entr�e:                                         
      *----------------------------------------------------------------*        
       MODULE-ENTREE            SECTION.                                        
           DISPLAY 'BTD021: Int�gration des financements : DAF Tel'.            
           MOVE WHEN-COMPILED TO W-WHEN-COMPILED.                               
           DISPLAY '        Stamp WHEN-COMPILED: ' W-WHEN-COMPILED(1:8)         
                   ' ' W-WHEN-COMPILED(9:).                                     
           OPEN INPUT  FTD02DE FTD020E FPARAM                                   
           OPEN OUTPUT FTD02DS FTD020S FTD021                                   
      * -> LECTURE DU PARAMETRE : index de la cl� de matching                   
           READ FPARAM INTO W-FPARAM                                            
             AT END                                                             
                MOVE 'ERREUR CARTE FPARAM NON TROUV�E' TO  ABEND-MESS           
                PERFORM ABEND-PROGRAMME                                         
      *         GO TO ABANDON-BATCH                                             
           END-READ                                                             
           CLOSE FPARAM                                                         
           DISPLAY 'BTD021: param�tre = ' W-FPARAM                              
           MOVE W-FPARAM TO W-IPARAM                                            
           .                                                                    
       FIN-MODULE-ENTREE.  EXIT.                                                
      *----------------------------------------------------------------*        
      *                         Traitement:                                     
      *----------------------------------------------------------------*        
       MODULE-TRAITEMENT SECTION.                                               
           PERFORM READ-FTD02DE.                                                
           PERFORM READ-FTD020E.                                                
           PERFORM UNTIL FIN-FTD02D AND FIN-FTD020                              
              IF ((CLE-FTD02D > CLE-FTD020)                                     
                 OR (W-FTD020-VCTRL(W-IPARAM) = SPACE OR LOW-VALUE ))           
                 ADD 1 TO W-CPT-FTD020-SEUL                                     
                 WRITE ENR-FTD020S FROM W-FTD020                                
                 PERFORM READ-FTD020E                                           
              ELSE                                                              
                 IF ((CLE-FTD02D < CLE-FTD020)                                  
                    OR (W-FTD02D-VCTRL(W-IPARAM) = SPACE OR LOW-VALUE ))        
                    ADD 1 TO W-CPT-FTD02D-SEUL                                  
                    WRITE ENR-FTD02DS FROM W-FTD02D                             
                    PERFORM READ-FTD02DE                                        
                 ELSE                                                           
                    MOVE CLE-FTD02D TO CLE-FTD02D-ANCIEN                        
                    PERFORM UNTIL CLE-FTD02D not = CLE-FTD02D-ANCIEN            
                        MOVE W-FTD020-PART1-DSI                                 
                             TO W-FTD02D-PART1-DSI                              
                        MOVE W-IPARAM TO W-FTD02D-PART1-DSI(30:1)               
                        ADD 1 TO W-CPT-MATCHES                                  
                        WRITE ENR-FTD021  FROM W-FTD02D                         
                        MOVE CLE-FTD02D TO CLE-FTD02D-ANCIEN                    
                        PERFORM READ-FTD02DE                                    
                    END-PERFORM                                                 
                    PERFORM READ-FTD020E                                        
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
       FIN-MODULE-TRAITEMENT. EXIT.                                             
      *----------------------------------------------------------------*        
      *                                End                                      
      *----------------------------------------------------------------*        
       MODULE-SORTIE SECTION.                                                   
           CLOSE FTD02DS FTD020S FTD021.                                        
           CLOSE FTD02DE FTD020E.                                               
           DISPLAY 'BTD021: Fin normale de programme'.                          
           MOVE W-CPT-FTD02D TO PIC-EDIT.                                       
           DISPLAY '        ' PIC-EDIT                                          
                   ' enregistrements lus sur FTD02D (Fichier DAF tel)'.         
           MOVE W-CPT-FTD020 TO PIC-EDIT.                                       
           DISPLAY '        ' PIC-EDIT                                          
                   ' enregistrements lus sur FTD020 (Extraction TDxx)'.         
           MOVE W-CPT-FTD02D-SEUL TO PIC-EDIT.                                  
           DISPLAY '        ' PIC-EDIT                                          
                   ' enregistrements non match�s de FTD02D (DAF tel)'.          
           MOVE W-CPT-FTD020-SEUL TO PIC-EDIT.                                  
           DISPLAY '        ' PIC-EDIT                                          
                   ' enregistrements non match�s de FTD020 (SIM)'.              
           MOVE W-CPT-MATCHES TO PIC-EDIT.                                      
           DISPLAY '        ' PIC-EDIT                                          
                   ' enregistrements match�s FTD021'.                           
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-MODULE-SORTIE. EXIT.                                                 
      *----------------------------------------------------------------*        
      *                     S�quentiel FTD02D                                   
      *----------------------------------------------------------------*        
       READ-FTD02DE SECTION.                                                    
           if not FIN-FTD02D                                                    
               READ FTD02DE INTO W-FTD02D                                       
                  AT END MOVE HIGH-VALUE TO W-FTD02D                            
                         SET FIN-FTD02D TO TRUE                                 
                  NOT AT END                                                    
                         ADD 1 TO W-CPT-FTD02D                                  
               END-READ                                                         
               STRING W-FTD02D-CMARQ                                            
                      W-FTD02D-CLE(W-IPARAM)                                    
                      DELIMITED BY SIZE                                         
                      INTO                                                      
                      CLE-FTD02D                                                
               END-STRING                                                       
           end-if                                                               
           .                                                                    
       FIN-READ-FTD02DE.  EXIT.                                                 
      *----------------------------------------------------------------*        
      *                     S�quentiel FTD020                                   
      *----------------------------------------------------------------*        
       READ-FTD020E SECTION.                                                    
           if not FIN-FTD020                                                    
               READ FTD020E INTO W-FTD020                                       
                  AT END MOVE HIGH-VALUE TO W-FTD020                            
                         SET FIN-FTD020 TO TRUE                                 
                  NOT AT END                                                    
                         ADD 1 TO W-CPT-FTD020                                  
               END-READ                                                         
               STRING W-FTD020-CMARQ                                            
                      W-FTD020-CLE(W-IPARAM)                                    
                      DELIMITED BY SIZE                                         
                      INTO                                                      
                      CLE-FTD020                                                
               END-STRING                                                       
           end-if                                                               
           .                                                                    
       FIN-READ-FTD020E. EXIT.                                                  
      *----------------------------------------------------------------*        
      *                         Abend SQL                                       
      *----------------------------------------------------------------*        
      *ABEND-SQL SECTION.                                                       
      *    CALL DSNTIAR USING SQLCA, MSG-SQL, MSG-SQL-LRECL.                    
      *    PERFORM VARYING I-SQL FROM 1 BY 1 UNTIL I-SQL > 3                    
      *       DISPLAY MSG-SQL-MSG (I-SQL) UPON CONSOLE                          
      *       DISPLAY MSG-SQL-MSG (I-SQL)                                       
      *    END-PERFORM.                                                         
      *    PERFORM ABEND-PROGRAMME.                                             
      *FIN-ABEND-SQL. EXIT.                                                     
      *----------------------------------------------------------------*        
      *                         Plantage                                        
      *----------------------------------------------------------------*        
       ABEND-PROGRAMME SECTION.                                                 
           DISPLAY 'BTD021: interruption de traitement'.                        
           MOVE  'BTD021'   TO  ABEND-PROG.                                     
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                        
       FIN-ABEND-PROGRAMME. EXIT.                                               
      *----------------------------------------------------------------*        
      *          M.A.J de la TD00 si controle OK                       *        
      *----------------------------------------------------------------*        
      *MISE-A-JOUR-RTTD00   SECTION.                                            
      *     MOVE W-FTD02D-WARO     TO TD00-WARO                                 
      *     MOVE W-FTD02D-DARO     TO TD00-DARO                                 
      *     MOVE W-FTD020-NSOCIETE TO TD00-NSOCIETE                             
      *     MOVE W-FTD020-NLIEU    TO TD00-NLIEU                                
      *     MOVE W-FTD020-NVENTE   TO TD00-NVENTE                               
      *     MOVE W-FTD020-NDOSSIER TO TD00-NDOSSIER                             
      *     EXEC SQL UPDATE RVTD0000                                            
      *                SET WARO      = :TD00-WARO,                              
      *                    DARO      = :TD00-DARO                               
      *              WHERE NSOCIETE  = :TD00-NSOCIETE                           
      *                AND NLIEU     = :TD00-NLIEU                              
      *                AND NVENTE    = :TD00-NVENTE                             
      *                AND NDOSSIER  = :TD00-NDOSSIER                           
      *     END-EXEC                                                            
      *     IF SQLCODE = 0                                                      
      *         ADD 1 TO W-CPT-MAJ                                              
      *     ELSE                                                                
      *        IF SQLCODE = 100                                                 
      *          MOVE SPACE TO MESS-ERREUR                                      
      *          MOVE 'DOSSIER non pr�sent sur TD00 ' TO MESS-ERREUR            
      *          PERFORM  DISPLAY-MESSAGE-ERREUR                                
      *        ELSE                                                             
      *           IF SQLCODE = -803                                             
      *             MOVE SPACE TO MESS-ERREUR                                   
      *             MOVE 'Doublon sur TD01 ' TO MESS-ERREUR                     
      *             PERFORM  DISPLAY-MESSAGE-ERREUR                             
      *           ELSE                                                          
      *            DISPLAY 'UPDATE RTTD01 : '                                   
      *            PERFORM ABEND-SQL                                            
      *           END-IF                                                        
      *        END-IF                                                           
      *     END-IF                                                              
      *    .                                                                    
      *FIN-MISE-A-JOUR-RTTD00.   EXIT.                                          
