      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BCE610.                                                     
       AUTHOR. JG.                                                              
       ENVIRONMENT DIVISION.                                                    
      ******************************************************************        
      *                                                                         
      *  PHASE 1: GENERATION DU SYSIN POUR FAST UNLOAD DE gb05 gb15             
      ******************************************************************        
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FDATE  ASSIGN TO FDATE.                                      
      *--                                                                       
            SELECT FDATE  ASSIGN TO FDATE                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FSYSIN ASSIGN TO FSYSIN.                                     
      *--                                                                       
            SELECT FSYSIN ASSIGN TO FSYSIN                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FCE610O ASSIGN TO FCE610O.                                   
      *--                                                                       
            SELECT FCE610O ASSIGN TO FCE610O                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FDATE RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.             
       01  MW-FILLER PIC X(80).                                                 
       FD  FSYSIN RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
      *MW DAR-2                                                                 
       01  MW-FILLER PIC X(80).                                                 
       FD  FCE610O RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.           
       01  ENR-FCE610O PIC X(80).                                               
       WORKING-STORAGE SECTION.                                                 
      ***************************************************************           
      * ZONE OUT POUR FCE610O, FLAG POUR FSYSIN                                 
      ***************************************************************           
       01  W-FCE610O PIC X(80).                                                 
       01  FILLER PIC X VALUE SPACE.                                            
           88  FIN-FSYSIN VALUE 'F'.                                            
      ***************************************************************           
      * COMMS AVEC LES SOUS PROGS                                               
      ***************************************************************           
           COPY ABENDCOP.                                                       
       01  BETDATC PIC X(8) VALUE 'BETDATC'.                                    
           COPY WORKDATC.                                                       
      ***************************************************************           
      * DATES CALCULEES                                                         
      * ACHTUNG FLAG: TRAITEMENT QUOTIDIEN, HEBDO OU MENSUEL                    
      *               (QUOTIDIEN ET JOURNALIER C'EST LA MEME CHOSE)             
      ***************************************************************           
       01  W-DDEBUT PIC X(8).                                                   
       01  W-DFIN   PIC X(8).                                                   
       01  W-DMOIS  PIC X(6).                                                   
       01  HEBDO-OU-MENSUEL PIC X.                                              
           88 QUOTIDIEN  VALUE 'Q'.                                             
           88 JOURNALIER VALUE 'Q'.                                             
           88 HEBDO      VALUE 'H'.                                             
           88 MENSUEL    VALUE 'M'.                                             
      ***************************************************************           
      * SOCIETE DE TRAITEMENT                                                   
      ***************************************************************           
       01  W-NSOC PIC XXX.                                                      
      ***************************************************************           
      * LOGISTIQUE                                                              
      ***************************************************************           
       01  W-FLOGIS PIC X(80).                                                  
      ***************************************************************           
      * PROC:                                                                   
      * - LECTURE DATE, FNSOC                                                   
      * - LECTURE FPARAM (TRAITEMENT HEBDO OU MENSUEL)                          
      * - LECTURE FLOGIS ( LOGISTIQUE GROUPE OU NON )                           
      * - CALCUL DES BORNES DE DATES                                            
      * - CHARGEMENT DES CODES REMISE EN TABLEAU                                
      * - GENERATION DE SYSIN                                                   
      * - C'EST TOUT                                                            
      ***************************************************************           
       PROCEDURE DIVISION.                                                      
           DISPLAY 'BCE610: CONSTRUCTION SYSIN POUR UNLOAD'                     
           OPEN INPUT FDATE FSYSIN                                              
           OUTPUT FCE610O.                                                      
           READ FDATE INTO GFJJMMSSAA AT END                            01090024
                MOVE 'FICHIER FDATE VIDE' TO ABEND-MESS                 01100024
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           CLOSE FDATE.                                                 01130024
           MOVE '1'            TO GFDATA.                               00004640
           CALL BETDATC      USING WORK-BETDATC                         00004670
           IF GFVDAT NOT = '1'                                          00004680
              STRING 'BETDATC : ' GF-MESS-ERR                           00004700
              DELIMITED BY SIZE INTO ABEND-MESS                         00004710
              PERFORM ABEND-PROGRAMME                                           
           END-IF.                                                      00004730
           DISPLAY 'BCE610: date param�tre lue sur FDATE: '                     
                    GFSMN-LIB-L ' ' GFJOUR ' '                                  
                    GFMOI-LIB-L ' ' GFSIECLE GFANNEE.                           
      * - FDATE + 1                                                             
           ADD 1 TO GFQNT0                                                      
           MOVE '3'            TO GFDATA.                               00004640
           CALL BETDATC      USING WORK-BETDATC                         00004670
           IF GFVDAT NOT = '1'                                          00004680
              STRING 'BETDATC : ' GF-MESS-ERR                           00004700
              DELIMITED BY SIZE INTO ABEND-MESS                         00004710
              PERFORM ABEND-PROGRAMME                                           
           END-IF.                                                      00004730
           DISPLAY 'BCE610: DATE PARAM�TRE DE TRAVAIL : '                       
                    GFSMN-LIB-L ' ' GFJOUR ' '                                  
                    GFMOI-LIB-L ' ' GFSIECLE GFANNEE.                           
      ******************************************************************        
      * RECOPIE DE FSYSIN                                                       
      ******************************************************************        
           DISPLAY 'BCE610: sysin g�n�r� � partir de FSYSIN:'                   
           PERFORM UNTIL FIN-FSYSIN                                             
              READ FSYSIN INTO W-FCE610O                                        
                 AT END                                                         
                    SET FIN-FSYSIN TO TRUE                                      
                 NOT AT END                                                     
                    INSPECT W-FCE610O REPLACING ALL ':&FDATE:'                  
                    BY GFSAMJ-0                                                 
                    WRITE ENR-FCE610O FROM W-FCE610O                            
                    DISPLAY W-FCE610O                                           
              END-READ                                                          
           END-PERFORM.                                                         
           CLOSE FSYSIN.                                                        
      ******************************************************************        
      * ZIENDE                                                                  
      ******************************************************************        
           CLOSE FCE610O.                                                       
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      ******************************************************************        
      * SCHPLOUF                                                                
      ******************************************************************        
       ABEND-PROGRAMME                 SECTION.                                 
           MOVE  'BCE610'     TO   ABEND-PROG.                                  
           CALL  'ABEND'   USING   ABEND-PROG  ABEND-MESS.                      
