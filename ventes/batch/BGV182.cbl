      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00020001
       PROGRAM-ID.    BGV182.                                           00030001
       AUTHOR.        DSA014.                                           00040001
       ENVIRONMENT    DIVISION.                                         00050001
       CONFIGURATION  SECTION.                                          00060001
      ***************************************************************   00070001
      * F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E *   00080001
      ***************************************************************   00090001
      *                                                             *   00100001
      *  PROJET     : SUIVI CREDIT                                  *   00110001
      *  PROGRAMME  : BGV182                                        *   00120001
      *  CREATION   : OCTOBRE 2002                                  *   00130001
      *  FONCTION   : SELECTION, FORMATAGE, MULTIPLICATION DES ENR  *   00140001
      *               " REMONTEES MGI TRC% "  CA                    *   00160001
      ***************************************************************   00161001
      *                                                                 00162001
       SPECIAL-NAMES.                                                   00170001
           DECIMAL-POINT IS COMMA.                                      00180001
      *                                                                 00181001
       INPUT-OUTPUT   SECTION.                                          00190001
       FILE-CONTROL.                                                    00200001
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FGV182 ASSIGN  TO  FGV182.                            00210001
      *--                                                                       
           SELECT FGV182 ASSIGN  TO  FGV182                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDA189 ASSIGN  TO  FDA189.                            00220001
      *--                                                                       
           SELECT FDA189 ASSIGN  TO  FDA189                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FCR004 ASSIGN  TO  FCR004.                            00220301
      *--                                                                       
           SELECT FCR004 ASSIGN  TO  FCR004                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00221001
       DATA DIVISION.                                                   00230001
       FILE SECTION.                                                    00240001
      ***************************************************************** 00250001
      * FICHIER REMONTEE MGI CA    LG. 40                               00260001
      ***************************************************************** 00270001
       FD  FGV182                                                       00280001
           RECORDING F                                                  00290001
           LABEL RECORD STANDARD                                        00300001
           BLOCK  CONTAINS 0   RECORDS.                                 00310001
       01  FGV182-RECORD      PIC X(40).                                00320001
      *                                                                 00321001
      ***************************************************************** 00322001
      * FICHIER PERIODE SELECTIONNEE                                    00323001
      ***************************************************************** 00324001
       FD  FDA189                                                       00330001
           RECORDING F                                                  00340001
           BLOCK 0 RECORDS                                              00350001
           LABEL RECORD STANDARD.                                       00360001
                                                                        00370001
       01  ENR-FDA189.                                                  00380001
           03  F-DATEDEB.                                               00390001
               05  SS-D              PIC X(02).                         00391001
               05  AA-D              PIC X(02).                         00392001
               05  MM-D              PIC X(02).                         00393001
               05  JJ-D              PIC X(02).                         00394001
           03  F-DATEFIN.                                               00394101
               05  SS-F              PIC X(02).                         00394201
               05  AA-F              PIC X(02).                         00394301
               05  MM-F              PIC X(02).                         00394401
               05  JJ-F              PIC X(02).                         00394501
           03  FILLER                PIC X(64).                         00395001
      *                                                                 00400001
      ***************************************************************** 00401901
      * FICHIER DES MOUVEMENTS FORMATES A TRAITER VERS IGV187  LG 80    00402001
      ***************************************************************** 00402101
       FD  FCR004                                                       00402201
           RECORDING F                                                  00402301
           LABEL RECORD STANDARD                                        00402401
           BLOCK  CONTAINS 0   RECORDS.                                 00402501
       01  FCR004-RECORD      PIC X(80).                                00402601
      *                                                                 00402701
      ***************************************************************** 00403001
      *                 W O R K I N G  S T O R A G E                    00410001
      ***************************************************************** 00420001
      *------------------------                                         00421001
       WORKING-STORAGE SECTION.                                         00430001
      *------------------------                                         00440001
      *****************************************************             00448001
      * COMPTEURS, ZONES ABEND ET INDICATEURS DE DEBUT/FIN              00449001
      *****************************************************             00449101
      *                                                                 00449201
       01  DDEBUT              PIC X(08) VALUE SPACES.                  00449701
       01  DFIN                PIC X(08) VALUE SPACES.                  00449801
      *                                                                 00449901
       01  FILLER.                                                      00460001
           05 CPT-FGV182       PIC 9(06)      VALUE 0.                  00520001
           05 CPT-FCR004       PIC 9(06)      VALUE 0.                  00530201
           05 CPT-ECRIT        PIC 9(06)      VALUE 1.                  00530301
           05 CPT-VOLUME       PIC 9(06)      VALUE 0.                  00530401
      *                                                                 00531001
       01  ABEND-ZONE.                                                  00540001
           05 ABEND-PROG       PIC X(08).                               00550001
           05 ABEND-MESS       PIC X(55).                               00560001
      *                                                                 00561001
       01  TOP-LECTURE-FGV182  PIC 9   VALUE 1.                         00562001
           88  PAS-FIN-FGV182          VALUE 1.                         00563001
           88  FIN-FGV182              VALUE 2.                         00564001
      *                                                                 00564101
       01  TOP-LECTURE-FDA189  PIC 9   VALUE 1.                         00564201
           88  PAS-FIN-FDA189          VALUE 1.                         00564301
           88  FIN-FDA189              VALUE 2.                         00564401
      *                                                                 00564501
      ************************************************************      00565401
      * DESCRIPTION DES FICHIERS "REMONTEE MGI"  LG 40                  00565501
      ************************************************************      00566001
           COPY FGV184.                                                 00567001
      *                                                                 00964101
      ************************************************************      00964201
      * DESCRIPTION DU FICHIER SORTIE  CA        LG 80                  00964301
      ************************************************************      00964401
           COPY FGV190.                                                 00964501
      *                                                                 00964601
      ***************************************************************** 00967401
      *     ZONE  DE  GESTION  DES  ERREURS                           * 00967501
      ***************************************************************** 00967601
       01  ERREUR-MESSAGE.                                              00967701
           05 FILLER                       PIC X(16)                    00968001
              VALUE 'ERREUR DB2 SUR '.                                  00970001
           05 ERR-FONCT                    PIC X(10) VALUE SPACES.      00980001
           05 ERR-TABLE                    PIC X(06) VALUE SPACES.      00990001
           05 FILLER                       PIC X(03)                    01000001
              VALUE ' : '.                                              01010001
           05 ERR-CODE                     PIC -999 .                   01020001
                                                                        01030001
       01  Z-INOUT   PIC X(4096)  VALUE SPACE.                          01040001
            COPY SYBWERRO.                                              01050001
      *                                                                 01060001
      ***************************************************************** 01510001
      *     P R O C E D U R E   D I V I S I O N                       * 01520001
      ***************************************************************** 01530001
       PROCEDURE DIVISION.                                              01540000
      *-------------------                                              01550001
       MODULE-BGV184.                                                   01560001
      *--------------                                                   01570001
           PERFORM MODULE-ENTREE.                                       01580001
           PERFORM MODULE-TRAITEMENT UNTIL FIN-FGV182.                  01590001
           PERFORM MODULE-SORTIE.                                       01600001
                                                                        01610001
      *--------------------------                                       01620001
       MODULE-ENTREE    SECTION.                                        01630001
      *--------------------------                                       01640001
           DISPLAY  '************************************************'  01641001
           DISPLAY  '**              PROGRAMME BGV182              **'  01642001
           DISPLAY  "**  FORMATAGE DES ENREG REMONTEE MGI TRC% CA  **"  01643001
           DISPLAY  '**              SUIVI CREDIT                  **'  01644001
           DISPLAY  '************************************************'  01645001
           DISPLAY '** EXECUTION STARTEE NORMALEMENT           '.       01650001
           OPEN  INPUT  FGV182.                                         01690001
           OPEN  OUTPUT FCR004.                                         01690101
      *                                                                 01691001
      ************************************************                  01700001
      * DETERMINATION PERIODE D'EXTRACTION         ***                  01710001
      ************************************************                  01720001
      *                                                                 01722001
           OPEN INPUT FDA189.                                           01722101
AD01  *    READ FDA189 INTO ENR-FDA189                                  01722201
AD01       READ FDA189                                                  01722301
                AT END SET FIN-FDA189 TO TRUE.                          01722401
           IF FIN-FDA189                                                01722601
              MOVE 'FICHIER DES DATES DE DEBUT/FIN EST VIDE!!!!'        01722701
              TO ABEND-MESS                                             01722801
              DISPLAY ABEND-MESS                                        01722901
              PERFORM PLANTAGE                                          01723001
           ELSE                                                         01723101
              MOVE  F-DATEDEB TO DDEBUT                                 01723201
              MOVE  F-DATEFIN TO DFIN                                   01723301
           END-IF                                                       01723401
      *                                                                 01723501
           PERFORM LECTURE-FGV182.                                      01723601
      *                                                                 01726001
       FIN-MODULE-ENTREE.  EXIT.                                        01726201
      *                                                                 01727001
      ***************************************************************** 01730001
       MODULE-TRAITEMENT   SECTION.                                     01740001
      *---------------------------                                      01750001
      *                                                                 01768001
      *    SELECTION                                                    01769001
           IF  FGV184-CA-DTRAITEMENT >= DDEBUT                          01770501
           AND FGV184-CA-DTRAITEMENT <= DFIN                            01770601
              PERFORM FORMATAGE-80                                      01770701
              PERFORM ECRITURE-FCR004                                   01770801
           END-IF.                                                      01770901
      *                                                                 01771001
           PERFORM LECTURE-FGV182.                                      01771301
      *                                                                 01772001
       FIN-MODULE-TRAITEMENT. EXIT.                                     01780001
                                                                        01790001
      **************************************************************    02410001
       FORMATAGE-80     SECTION.                                        02420001
      *--------------------------                                       02430001
                                                                        02430501
           MOVE FGV184-CA-NSOCIETE   TO FGV190-NSOCIETE.                02430801
           MOVE FGV184-CA-NLIEU      TO FGV190-NLIEU.                   02430901
           MOVE ' '                  TO FGV190-CVENDEUR.                02432301
           MOVE FGV184-CA-CA         TO FGV190-CA.                      02432701
      *                                                                 02433101
       FIN-FORMATAGE-80. EXIT.                                          02433201
                                                                        02433301
      **************************************************************    02437501
       LECTURE-FGV182   SECTION.                                        02437601
      *--------------------------                                       02437701
                                                                        02437801
           INITIALIZE FGV182-RECORD.                                    02438001
           READ FGV182 INTO FGV184-ENREG                                02440001
           AT END SET FIN-FGV182 TO TRUE.                               02441001
           ADD  1  TO CPT-FGV182.                                       02450001
      *                                                                 02451001
       FIN-LECTURE-FGV182. EXIT.                                        02460001
                                                                        02470001
      **************************************************************    02480001
       ECRITURE-FCR004   SECTION.                                       02490001
      *--------------------------                                       02500001
                                                                        02510001
           INITIALIZE FCR004-RECORD.                                    02520001
           WRITE FCR004-RECORD FROM FGV190-ENREG.                       02530001
           ADD  1  TO CPT-FCR004.                                       02541001
      *                                                                 02550001
       FIN-ECRITURE-FCR004. EXIT.                                       02560001
                                                                        02570001
      ***************************************************************** 02700001
      *------------------------------                                   03300001
       PLANTAGE              SECTION.                                   03310001
      *------------------------------                                   03320001
           PERFORM  FIN-ANORMALE.                                       03330001
           MOVE  'BGV182' TO ABEND-PROG.                                03340001
           DISPLAY '*** ABEND ***' ABEND-MESS                           03350001
           CALL  'ABEND' USING ABEND-PROG  ABEND-MESS.                  03360001
      *------------------------------                                   03370001
       FIN-ANORMALE SECTION.                                            03380001
      *------------------------------                                   03390001
           DISPLAY  '***********************************************'.  03400001
           DISPLAY  '***        INTERRUPTION DU PROGRAMME           '.  03410001
           DISPLAY  '***                 BGV182                     '.  03420001
           DISPLAY  '***********************************************'.  03430001
           PERFORM  COMPTE-RENDU.                                       03440001
           CLOSE  FGV182 FDA189 FCR004.                                 03450001
      *------------------------------                                   03460001
       MODULE-SORTIE         SECTION.                                   03470001
      *------------------------------                                   03480001
      *                                                                 03485001
           DISPLAY  '***********************************************'.  03490001
           DISPLAY  '***      EXECUTION TERMINEE NORMALEMENT        '.  03500001
           DISPLAY  '***                BGV182                       '. 03510001
           DISPLAY  '***********************************************'.  03520001
           PERFORM  COMPTE-RENDU.                                       03530001
           CLOSE  FGV182 FDA189 FCR004.                                 03540001
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    03550001
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *------------------------------                                   03560001
       COMPTE-RENDU          SECTION.                                   03570001
      *------------------------------                                   03580001
           DISPLAY  '***********************************************'.  03590001
           DISPLAY  '***  LECTURE DU FICHIER FGV182  : ' CPT-FGV182.    03600001
           DISPLAY  '***'.                                              03610001
           DISPLAY  '***  ECRITURE DU FICHIER FCR004 : ' CPT-FCR004.    03620001
           DISPLAY  '***********************************************'.  03630001
      *                                                                 03631001
                                                                        04160001
                                                                        04170001
