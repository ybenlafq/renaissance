      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00020099
       PROGRAM-ID.    BGV183.                                           00030099
       AUTHOR.        DSA014.                                           00040099
       ENVIRONMENT    DIVISION.                                         00050099
       CONFIGURATION  SECTION.                                          00060099
      ***************************************************************   00070099
      * F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E *   00080099
      ***************************************************************   00090099
      *                                                             *   00100099
      *  PROJET     : SUIVI CREDIT                                  *   00110099
      *  PROGRAMME  : BGV183                                        *   00120099
      *  CREATION   : NOVEMBRE 2001                                 *   00130099
      *  FONCTION   : CUMUL DES DONNEES CREDIT PAR VENDEUR          *   00140099
      ***************************************************************   00161099
      *                                                                 00162099
       SPECIAL-NAMES.                                                   00170099
           DECIMAL-POINT IS COMMA.                                      00180099
      *                                                                 00181099
       INPUT-OUTPUT   SECTION.                                          00190099
       FILE-CONTROL.                                                    00200099
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FGV180 ASSIGN  TO  FGV180.                            00210099
      *--                                                                       
           SELECT FGV180 ASSIGN  TO  FGV180                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FGV183 ASSIGN  TO  FGV183.                            00220099
      *--                                                                       
           SELECT FGV183 ASSIGN  TO  FGV183                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00221099
       DATA DIVISION.                                                   00230099
       FILE SECTION.                                                    00240099
      ***************************************************************** 00250099
      * FICHIER ENTREE                                                  00260099
      ***************************************************************** 00270099
       FD  FGV180                                                       00280099
           RECORDING F                                                  00290099
           LABEL RECORD STANDARD                                        00300099
           BLOCK  CONTAINS 0   RECORDS.                                 00310099
       01  FGV180-RECORD       PIC X(98).                               00320099
                                                                        00320199
      *                                                                 00321099
      ***************************************************************** 00322099
      * FICHIER SORTIE                                                  00323099
      ***************************************************************** 00324099
       FD  FGV183                                                       00325099
           RECORDING F                                                  00326099
           LABEL RECORD STANDARD                                        00327099
           BLOCK  CONTAINS 0   RECORDS.                                 00328099
       01  FGV183-RECORD       PIC X(98).                               00329099
                                                                        00329199
      *                                                                 00329299
                                                                        00402299
      ***************************************************************** 00403099
      *                 W O R K I N G  S T O R A G E                    00410099
      ***************************************************************** 00420099
      *------------------------                                         00421099
       WORKING-STORAGE SECTION.                                         00430099
      *------------------------                                         00440099
       77  WVENDEUR            PIC X(06)  VALUE SPACES.                 00442099
       77  CUMUL-PREGLTVTE     PIC S9(7)V99 COMP-3 VALUE 0.             00443099
      *                                                                 00451099
       01  FILLER.                                                      00460099
           05 CPT-FGV180       PIC 9(06)  VALUE 0.                      00520099
           05 CPT-FGV183       PIC 9(06)  VALUE 0.                      00521099
      *                                                                 00531099
       01  ABEND-ZONE.                                                  00540099
           05 ABEND-PROG       PIC X(08).                               00550099
           05 ABEND-MESS       PIC X(55).                               00560099
      *                                                                 00561099
       01  TOP-LECTURE-FGV180  PIC 9      VALUE 1.                      00562099
           88  PAS-FIN-FGV180             VALUE 1.                      00563099
           88  FIN-FGV180                 VALUE 2.                      00564099
      *                                                                 00565099
                                                                        00568799
       01  TEST-OK             PIC X(01)  VALUE '0'.                    00568899
           88 OK                          VALUE '0'.                    00568999
           88 NOT-OK                      VALUE '1'.                    00569099
      *                                                                 00569199
      ************************************************************      00569299
      * DESCRIPTION DES ZONES DE RESERVES POUR REECRITURE               00569399
      ************************************************************      00569499
       01  RES-ENREG.                                                   00569999
           10  RES-NSOCIETE       PIC  X(03).                           00570099
           10  RES-NLIEU          PIC  X(03).                           00570199
           10  RES-COCRED         PIC  X(01).                           00570299
           10  RES-CVENDEUR       PIC  X(06).                           00570399
           10  RES-WTPCRD         PIC  X(05).                           00570499
           10  RES-NVENTE         PIC  X(07).                           00570599
           10  RES-CFCRED         PIC  X(05).                           00570699
           10  RES-NCREDI         PIC  X(14).                           00570799
           10  RES-CDEV           PIC  X(03).                           00570899
           10  RES-PREGLTVTE      PIC  S9(7)V99 COMP-3.                 00570999
           10  RES-LFRMLE         PIC  X(30).                           00571099
           10  RES-DDEBPER        PIC  X(08).                           00571199
           10  RES-DFINPER        PIC  X(08).                           00571299
      *                                                                 00571399
      *                                                                 00571499
      ************************************************************      00571799
      * DESCRIPTION DES FICHIERS ENTREE ET SORTIE                       00571899
      ************************************************************      00572099
           COPY FGV180.                                                 00580099
      *                                                                 00590099
      *                                                                 00964699
      ***************************************************************** 01510070
      *     P R O C E D U R E   D I V I S I O N                       * 01520080
      ***************************************************************** 01530090
       PROCEDURE DIVISION.                                              01540000
      *-------------------                                              01550010
       MODULE-BGV183.                                                   01560099
      *--------------                                                   01570030
           PERFORM MODULE-ENTREE.                                       01580040
           PERFORM MODULE-TRAITEMENT UNTIL FIN-FGV180.                  01590099
           PERFORM MODULE-SORTIE.                                       01600060
                                                                        01610099
      ***************************************************************** 01620099
       MODULE-ENTREE    SECTION.                                        01630099
      ***************************************************************** 01640099
           DISPLAY  '************************************************'  01641099
           DISPLAY  '**              PROGRAMME BGV183              **'  01642099
           DISPLAY  '**    CUMUL DES MVTS CREDIT COMPTABILISES     **'  01643099
           DISPLAY  '**            SUIVI CREDIT VENDEUR            **'  01644099
           DISPLAY  '************************************************'  01645099
           DISPLAY '** EXECUTION STARTEE NORMALEMENT           '.       01650099
                                                                        01660099
                                                                        01670099
           OPEN INPUT  FGV180.                                          01680099
           OPEN OUTPUT FGV183.                                          01690099
                                                                        01722099
           READ FGV180 INTO FGV183-ENREG AT END                         01722299
              SET FIN-FGV180 TO TRUE                                    01722399
              MOVE '*** FICHIER ENTREE VIDE ***' TO ABEND-MESS          01722499
              DISPLAY ABEND-MESS                                        01722599
              PERFORM PLANTAGE                                          01722699
           END-READ.                                                    01723099
                                                                        01723199
           IF FGV183-NSOCIETE NOT = LOW-VALUE                           01723299
              ADD 1 TO CPT-FGV180                                       01723399
           END-IF.                                                      01723499
                                                                        01723599
           INITIALIZE CUMUL-PREGLTVTE.                                  01723699
                                                                        01723799
           MOVE FGV183-ENREG     TO RES-ENREG.                          01723999
                                                                        01724399
       FIN-MODULE-ENTREE.  EXIT.                                        01726299
                                                                        01727099
      ******************************************************************01730099
       MODULE-TRAITEMENT   SECTION.                                     01740099
      ******************************************************************01750099
                                                                        01770499
           IF FGV183-NSOCIETE NOT = LOW-VALUE                           01771699
              IF FGV183-NSOCIETE = RES-NSOCIETE AND                     01771799
                 FGV183-NLIEU    = RES-NLIEU    AND                     01771899
                 FGV183-CVENDEUR = RES-CVENDEUR AND                     01771999
                 FGV183-NVENTE   = RES-NVENTE                           01772099
                                                                        01772199
                 COMPUTE CUMUL-PREGLTVTE =                              01772299
                                   CUMUL-PREGLTVTE + FGV183-PREGLTVTE   01772399
              ELSE                                                      01772599
                 PERFORM CHARGEMT-FGV183                                01772699
                 PERFORM ECRITURE-FGV183                                01772799
                 INITIALIZE CUMUL-PREGLTVTE                             01772899
                                                                        01772999
                 MOVE FGV183-ENREG  TO RES-ENREG                        01773099
                 COMPUTE CUMUL-PREGLTVTE =                              01773199
                                   CUMUL-PREGLTVTE + FGV183-PREGLTVTE   01773299
              END-IF                                                    01773399
           END-IF.                                                      01773499
                                                                        01774099
           READ FGV180 INTO FGV183-ENREG AT END                         01775099
              SET FIN-FGV180 TO TRUE                                    01776099
           END-READ.                                                    01779199
                                                                        01779299
           IF FGV183-NSOCIETE NOT = LOW-VALUE                           01779399
              ADD 1 TO CPT-FGV180                                       01779499
           END-IF.                                                      01779599
                                                                        01779699
                                                                        01779899
       FIN-MODULE-TRAITEMENT. EXIT.                                     01780099
                                                                        01790099
      ****************************************************************  02443499
                                                                        02443599
       CHARGEMT-FGV183   SECTION.                                       02443799
      *--------------------------                                       02443899
                                                                        02443999
      *    MOVE RES-NSOCIETE    TO FGV183-NSOCIETE.                     02444099
      *    MOVE RES-NLIEU       TO FGV183-NLIEU.                        02445099
      *    MOVE RES-COCRED      TO FGV183-COCRED.                       02446099
      *    MOVE RES-CVENDEUR    TO FGV183-CVENDEUR.                     02447099
      *    MOVE RES-WTPCRD      TO FGV183-WTPCRD.                       02448099
      *    MOVE RES-NVENTE      TO FGV183-NVENTE.                       02449099
      *    MOVE RES-CFCRED      TO FGV183-CFCRED.                       02449199
      *    MOVE RES-NCREDI      TO FGV183-NCREDI.                       02449299
      *    MOVE RES-CDEV        TO FGV183-CDEV.                         02449399
           MOVE CUMUL-PREGLTVTE TO RES-PREGLTVTE.                       02449499
      *    MOVE RES-LFRMLE      TO FGV183-LFRMLE.                       02449599
      *    MOVE RES-DDEBPER     TO FGV183-DDEBPER.                      02449699
      *    MOVE RES-DFINPER     TO FGV183-DFINPER.                      02449799
                                                                        02450099
       FIN-CHARGEMT-FGV183. EXIT.                                       02460099
                                                                        02470099
      ****************************************************************  02471099
                                                                        02472099
       ECRITURE-FGV183   SECTION.                                       02473099
      *--------------------------                                       02474099
           INITIALIZE FGV183-RECORD.                                    02475099
           WRITE FGV183-RECORD FROM RES-ENREG.                          02476099
           ADD  1  TO CPT-FGV183.                                       02477099
       FIN-ECRITURE-FGV183. EXIT.                                       02478099
                                                                        02479099
      ****************************************************************  02480099
      ***************************************************************** 02700099
       PLANTAGE              SECTION.                                   03310099
      *------------------------------                                   03320099
           PERFORM  FIN-ANORMALE.                                       03330099
           MOVE  'BGV183' TO ABEND-PROG.                                03340099
           DISPLAY '*** ABEND ***' ABEND-MESS                           03350099
           CALL  'ABEND' USING ABEND-PROG  ABEND-MESS.                  03360099
      *------------------------------                                   03370099
       FIN-ANORMALE SECTION.                                            03380099
      *------------------------------                                   03390099
           DISPLAY  '***********************************************'.  03400099
           DISPLAY  '***        INTERRUPTION DU PROGRAMME           '.  03410099
           DISPLAY  '***                 BGV183                     '.  03420099
           DISPLAY  '***********************************************'.  03430099
           PERFORM  COMPTE-RENDU.                                       03440099
           CLOSE  FGV180 FGV183.                                        03450099
                                                                        03452099
                                                                        03453099
      *------------------------------                                   03460099
       MODULE-SORTIE         SECTION.                                   03470099
      *------------------------------                                   03480099
                                                                        03489299
           PERFORM CHARGEMT-FGV183                                      03489399
           PERFORM ECRITURE-FGV183                                      03489499
           DISPLAY  '***********************************************'.  03490099
           DISPLAY  '***      EXECUTION TERMINEE NORMALEMENT        '.  03500099
           DISPLAY  '***                BGV183                       '. 03510099
           DISPLAY  '***********************************************'.  03520099
           PERFORM  COMPTE-RENDU.                                       03530099
           CLOSE  FGV180 FGV183.                                        03540099
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    03550099
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        03551099
      *------------------------------                                   03560099
       COMPTE-RENDU          SECTION.                                   03570099
      *------------------------------                                   03580099
           DISPLAY  '***********************************************'.  03590099
           DISPLAY  '***  LECTURE  DU FICHIER FGV180 : ' CPT-FGV180.    03620099
           DISPLAY  '***  ECRITURE DU FICHIER FGV183 : ' CPT-FGV183.    03621099
           DISPLAY  '***********************************************'.  03630099
                                                                        03631099
                                                                        06530099
                                                                        06670099
