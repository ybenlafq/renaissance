      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00010005
       PROGRAM-ID.  BGV810.                                             00020005
       AUTHOR.      DSA019.                                             00030005
      *                                                                         
      * ============================================================= *         
      *                                                               *         
      *     PROJET        :  EDITION DU TABLEAU DE BORD HEBDOMADAIRE  *         
      *                   :  PRIME VENDEUR                            *         
      *     PROGRAMME     :  BGV810.                                  *         
      *                                                               *         
      *     DATE CREATION :  20/11/1992.                              *         
      *                                                               *         
      * ============================================================= *         
      *                                                                         
       ENVIRONMENT DIVISION.                                            00150005
       CONFIGURATION SECTION.                                           00160005
       SPECIAL-NAMES.                                                           
            DECIMAL-POINT IS COMMA.                                             
       INPUT-OUTPUT     SECTION.                                        00170005
       FILE-CONTROL.                                                    00180005
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FDATE          ASSIGN  TO  FDATE.                    00190005
      *--                                                                       
           SELECT  FDATE          ASSIGN  TO  FDATE                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FNSOC          ASSIGN  TO  FNSOC.                    00190005
      *--                                                                       
           SELECT  FNSOC          ASSIGN  TO  FNSOC                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FGV805         ASSIGN  TO  FGV805.                   00190005
      *--                                                                       
           SELECT  FGV805         ASSIGN  TO  FGV805                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  IGV810         ASSIGN  TO  IGV810.                   00190005
      *--                                                                       
           SELECT  IGV810         ASSIGN  TO  IGV810                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
                                                                        00391011
       FD  IGV810                                                       00410005
           RECORDING MODE F                                             00420005
           BLOCK CONTAINS 0 RECORDS                                     00430005
           LABEL RECORD STANDARD.                                       00440005
                                                                        00450005
       01  ENR-IGV810 PIC X(132).                                       00460005
       FD  FNSOC                                                        00280005
           RECORDING F                                                  00290005
           BLOCK CONTAINS 0 RECORDS                                     00300005
           LABEL RECORD STANDARD.                                       00310005
                                                                        00320005
       01  ENR-FNSOC.                                                   00330005
           03 FSOC                  PIC X(03).                                  
           03 FILLER                PIC X(77).                                  
       FD  FDATE                                                        00280005
           RECORDING F                                                  00290005
           BLOCK CONTAINS 0 RECORDS                                     00300005
           LABEL RECORD STANDARD.                                       00310005
                                                                        00320005
       01  ENR-FDATE.                                                   00330005
           03 FDATE-JJMMSSAA.                                                   
              05 FDATE-JJ           PIC X(02).                                  
              05 FDATE-MM           PIC X(02).                                  
              05 FDATE-SS           PIC X(02).                                  
              05 FDATE-AA           PIC X(02).                                  
           03 FILLER                PIC X(72).                                  
                                                                        00391011
       FD   FGV805                                                              
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01  FGV805-RECORD.                                                       
           05 FILLER               PIC  X(180).                                 
           EJECT                                                                
      * ---------------------------------------------------------- *    00500005
      *                W O R K I N G  -  S T O R A G E             *    00510005
      * ---------------------------------------------------------- *    00520005
                                                                        00530005
       WORKING-STORAGE SECTION.                                         00540005
                                                                        00550005
       77  I                     PIC  S9(07) COMP-3 VALUE ZERO.         00590005
       77  IA                    PIC  S9(07) COMP-3 VALUE ZERO.         00590005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-I                   PIC  S9(04) COMP VALUE ZERO.           00590005
      *--                                                                       
       77  W-I                   PIC  S9(04) COMP-5 VALUE ZERO.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  CTR-1                 PIC  S9(04) COMP VALUE ZERO.           00590005
      *--                                                                       
       77  CTR-1                 PIC  S9(04) COMP-5 VALUE ZERO.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  CTR-2                 PIC  S9(04) COMP VALUE ZERO.           00590005
      *--                                                                       
       77  CTR-2                 PIC  S9(04) COMP-5 VALUE ZERO.                 
      *}                                                                        
       77  CPT-FGV805            PIC  99 VALUE ZERO.                    00590005
       77  CPT-LIGNE-IGV810      PIC  99 VALUE 99.                      00590005
       77  CPT-PAGE-IGV810       PIC  999 VALUE ZERO.                   00590005
       77  NB-LIGNE-MAX          PIC  99 VALUE 60.                      00590005
       77  LIGNE-IGV810          PIC  X(132).                           80005   
       77  QUANT1                PIC  9(05) VALUE ZERO.                 80005   
       77  W-ED-DATE             PIC  X(10).                            80005   
       77  W-NSOC                PIC  X(03).                            80005   
       01  TABLE-VALEUR.                                                        
          03  W-PVM            PIC  S9(09)V99 COMP-3.                      80005
          03  W-MBU            PIC  S9(09)V99 COMP-3.                      80005
          03  W-TAUXVOL        PIC  S9(10)V9 COMP-3.                       80005
          03  W-TAUXART        PIC  S9(10)V9 COMP-3.                       80005
          03  W-TOTTAUX        PIC  S9(10)V9 COMP-3.                       80005
                                                                        00644011
       01  BORNE-AN.                                                            
           03  BORNE-SS    PIC 99.                                              
           03  BORNE-AA    PIC 99.                                              
           03  BORNE-WW    PIC 99.                                              
       01  CODE-CAS              PIC X(01) VALUE '0'.                   00699509
           88  NON-CAS                     VALUE '0'.                   00699709
           88  CAS                         VALUE '1'.                   00699709
       01  CODE-FGV805           PIC X(01) VALUE '0'.                   00699509
           88  NON-FIN-FGV805              VALUE '0'.                   00699709
           88  FIN-FGV805                  VALUE '1'.                   00699709
       01  W-ED-DATE1.                                                  00699509
           03  W-ED-DATE1-JJ     PIC X(02).                             00699709
           03  FILLER            PIC X(01) VALUE '/'.                   00699809
           03  W-ED-DATE1-MM     PIC X(02).                             00699909
           03  FILLER            PIC X(01) VALUE '/'.                   00700009
           03  W-ED-DATE1-SS     PIC X(02).                             00700109
           03  W-ED-DATE1-AA     PIC X(02).                             00700109
       01  W-ED-DATE2.                                                  00699509
           03  W-ED-DATE2-JJ     PIC X(02).                             00699709
           03  FILLER            PIC X(01) VALUE '/'.                   00699809
           03  W-ED-DATE2-MM     PIC X(02).                             00699909
           03  FILLER            PIC X(01) VALUE '/'.                   00700009
           03  W-ED-DATE2-SS     PIC X(02).                             00700109
           03  W-ED-DATE2-AA     PIC X(02).                             00700109
       01  DATE-JOU.                                                    00699509
           03  JOUR-SS           PIC 9(02).                             00699709
           03  JOUR-AA           PIC 9(02).                             00699909
           03  JOUR-MM           PIC 9(02).                             00700109
           03  JOUR-JJ           PIC 9(02).                             00700109
       01  FGV805-DSECT.                                                        
           03  F-SOCIETE             PIC  X(03).                                
           03  F-NCODIC              PIC  X(07).                                
           03  F-CFAM                PIC  X(05).                                
           03  F-WSEQRAY             PIC S9(05)    COMP-3.                      
           03  F-CRAYON              PIC  X(05).                                
           03  F-WTYPE               PIC  X(01).                                
           03  F-WSAUT               PIC  X(01).                                
           03  F-SEMAINE-1.                                                     
               04  F-SEM-1-NB        PIC S9(07)    COMP-3.                      
               04  F-SEM-1-CA        PIC S9(09)V99 COMP-3.                      
               04  F-SEM-1-MT        PIC S9(09)V99 COMP-3.                      
               04  F-SEM-PRIM        PIC S9(09)V99 COMP-3.                      
               04  F-SEM-CONB        PIC S9(07)    COMP-3.                      
               04  F-SEM-COCA        PIC S9(09)V99 COMP-3.                      
               04  F-SEM-COMT        PIC S9(09)V99 COMP-3.                      
               04  F-SEM-PVOL        PIC S9(09)V99 COMP-3.                      
           03  F-SEMAINE-2.                                                     
               04  F-SEM-2-NB        PIC S9(07)    COMP-3.                      
               04  F-SEM-2-CA        PIC S9(09)V99 COMP-3.                      
               04  F-SEM-2-MT        PIC S9(09)V99 COMP-3.                      
           03  F-CUMUL-1.                                                       
               04  F-CUM-1-NB        PIC S9(07)    COMP-3.                      
               04  F-CUM-1-CA        PIC S9(09)V99 COMP-3.                      
               04  F-CUM-1-MT        PIC S9(09)V99 COMP-3.                      
               04  F-CUM-PRIM        PIC S9(09)V99 COMP-3.                      
               04  F-CUM-CONB        PIC S9(07)    COMP-3.                      
               04  F-CUM-COCA        PIC S9(09)V99 COMP-3.                      
               04  F-CUM-COMT        PIC S9(09)V99 COMP-3.                      
               04  F-CUM-PVOL        PIC S9(09)V99 COMP-3.                      
           03  F-CUMUL-2.                                                       
               04  F-CUM-2-NB        PIC S9(07)    COMP-3.                      
               04  F-CUM-2-CA        PIC S9(09)V99 COMP-3.                      
               04  F-CUM-2-MT        PIC S9(09)V99 COMP-3.                      
           03  F-LAGREGATED          PIC  X(20).                                
           03  F-CETAT               PIC  X(06).                                
           03  F-CODICGROUPE         PIC  X(01).                                
           03  FILLER                PIC  X(08).                                
      *                                                                         
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *    RENSEIGNEMENT DE LA DATE SYSTEME SPDATDB2                  *         
      *---------------------------------------------------------------*         
      *                                                                         
       01  FILLER                  PIC X(14)  VALUE '*SPDATDB2    *'.           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WRET                    PIC S9(4)  COMP.                             
      *--                                                                       
       01  WRET                    PIC S9(4) COMP-5.                            
      *}                                                                        
       01  DATHEUR                 PIC S9(13) COMP-3.                           
       01  W-MESS-SPDATDB2.                                                     
           05 W-MESS-SPDAT         PIC X(32)                                    
                 VALUE 'RETOUR SPDATDB2 ANORMAL, CODE : '.                      
           05 WRET-PIC             PIC 9(4).                                    
       01  WDATHEUR                PIC S9(13).                                  
       01  WDATHEUR1               REDEFINES WDATHEUR.                          
           05 WJOUR                PIC S9(05).                                  
           05 WHEURE.                                                           
              10 WHH               PIC  9(02).                                  
              10 WMM               PIC  9(02).                                  
           05 WSECONDE             PIC  9(04).                                  
      ** AIDA ************************************************* IRDF **         
      *  DESCRIPTION DE L'ETAT IGV810                                           
      *****************************************************************         
      *                                                                         
       01  IGV810-DSECT.                                                        
      *----------------------------------------------------------               
           05  IGV810-E00.                                                      
               10  FILLER                         PIC X(58) VALUE               
           'IGV810                                                    '.        
               10  FILLER                         PIC X(47) VALUE               
           '                                      EDITE LE '.                   
               10  IGV810-E00-DATEDITE            PIC X(10).                    
               10  FILLER                         PIC X(06).                    
               10  FILLER                         PIC X(06) VALUE               
           ' PAGE '.                                                            
               10  IGV810-E00-NOPAGE              PIC ZZZ.                      
      *----------------------------------------------------------               
           05  IGV810-E05.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '                                           TABLEAU DE BORD'.        
               10  FILLER                         PIC X(13) VALUE               
           ' HEBDOMADAIRE'.                                                     
      *----------------------------------------------------------               
           05  IGV810-E10.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '                                             INTERESSEMENT'.        
               10  FILLER                         PIC X(08) VALUE               
           ' VENDEUR'.                                                          
      *----------------------------------------------------------               
           05  IGV810-E15.                                                      
               10  FILLER                         PIC X(13) VALUE               
           'SEMAINE DU : '.                                                     
               10  IGV810-E15-SEMDEB              PIC X(10).                    
               10  FILLER                         PIC X(82).                    
               10  FILLER                         PIC X(07) VALUE               
            'ETAT : '.                                                          
               10  IGV810-E15-ETAT                PIC X(06).                    
      *----------------------------------------------------------               
           05  IGV810-E20.                                                      
               10  FILLER                         PIC X(13) VALUE               
           '        AU : '.                                                     
               10  IGV810-E20-SEMFIN              PIC X(10).                    
               10  FILLER                         PIC X(82).                    
               10  IGV810-E20-ETATCOD             PIC X(12).                    
      *----------------------------------------------------------               
           05  IGV810-E25.                                                      
               10  FILLER                         PIC X(13) VALUE               
           'SOCIETE    : '.                                                     
               10  IGV810-E25-NSOCIETE            PIC X(03).                    
      *----------------------------------------------------------               
           05  IGV810-E30.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '.---------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(04) VALUE               
           '---.'.                                                              
      *----------------------------------------------------------               
           05  IGV810-E35.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '!  RUBRIQUE PRODUIT    !  NOMBRE  !   C.A.    ! P.V.M. ! M'.        
               10  FILLER                         PIC X(45) VALUE               
           '.B.U. !!  INTER. FAMILLE ! INTER. ARTICLE  ! '.                     
               10  FILLER                         PIC X(17) VALUE               
           ' INTER. TOTAL   !'.                                                 
      *----------------------------------------------------------               
           05  IGV810-E40.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '!                      !          !           !        !  '.        
               10  FILLER                         PIC X(45) VALUE               
           '      !!  TAUX  !  MONT. !  TAUX  !  MONT. !'.                      
               10  FILLER                         PIC X(18) VALUE               
           '  TAUX !  MONT. !'.                                                 
      *----------------------------------------------------------               
           05  IGV810-E45.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '!---------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(04) VALUE               
           '---!'.                                                              
      *----------------------------------------------------------               
           05  IGV810-E50.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '!                      !          !           !        !  '.        
               10  FILLER                         PIC X(45) VALUE               
           '      !!        !        !        !        !'.                      
               10  FILLER                         PIC X(18) VALUE               
           '       !        !'.                                                 
      *----------------------------------------------------------               
           05  IGV810-D05.                                                      
               10  FILLER                         PIC X(02) VALUE               
           '! '.                                                                
               10  IGV810-D05-NRUBR               PIC X(20).                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGV810-D05-NB                  PIC ZZZZZZ9.                  
               10  FILLER                         PIC X(04) VALUE               
           '  ! '.                                                              
               10  IGV810-D05-CA                  PIC ZZZZZZZZ9.                
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGV810-D05-PVM                 PIC ZZZZZ9.                   
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGV810-D05-MBU                 PIC ZZZZZ9.                   
               10  FILLER                         PIC X(04) VALUE               
           ' !!'.                                                               
               10  IGV810-D05-TAUXVOL             PIC ZZZZ,Z.                   
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  IGV810-D05-MONTVOL             PIC ZZZZZZZ.                  
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGV810-D05-TAUXART             PIC ZZZZ,Z.                   
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  IGV810-D05-MONTART             PIC ZZZZZZZ.                  
               10  FILLER                         PIC X(03) VALUE               
           ' !'.                                                                
               10  IGV810-D05-TOTTAUX             PIC ZZZZ,Z.                   
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  IGV810-D05-MONTTOT             PIC ZZZZZZZ.                  
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
      *----------------------------------------------------------               
           05  IGV810-FICHIER-VIDE.                                             
               10  FILLER                         PIC X(58) VALUE               
           '       LE FICHIER EN ENTRE EST VIDE                       '.        
               10  FILLER                        PIC X(74) VALUE SPACES.        
      *----------------------------------------------------------               
      *****************************************************************         
      *  ENTETE PHYSIQUE DE PAGE                                                
      *****************************************************************         
      * ---------------------------------------------------------- *    01290005
      *     D E S C R I P T I O N   A U T R E S   M O D U L E S    *    01300005
      * ---------------------------------------------------------- *    01310005
                                                                        01320005
            COPY ABENDCOP.                                              01340005
            COPY WORKDATC.                                              01340005
                                                                        01350005
      *    STRUCTURE APPEL MODULE DATE HEURE                                    
       01  MESSAGE-SPDATEDB2.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  RET          PIC S9(4) COMP.                                     
      *--                                                                       
           05  RET          PIC S9(4) COMP-5.                                   
      *}                                                                        
           05  DATE-HEURE   PIC S9(13) COMP-3.                                  
           05  W-MESS-SPDATDB2.                                                 
               10  W-MESS-SPDAT  PIC X(33) VALUE                                
                   'RETOUR SPDATEDB2 ANORMAL,CODE : '.                          
               10  W-RET-PIC     PIC 9(4).                                      
      * ---------------------------------------------------------- *    01490005
      *          P R O C E D U R E   D I V I S I O N               *    01500005
      * ---------------------------------------------------------- *    01510005
                                                                        01520005
       PROCEDURE DIVISION.                                              01530005
                                                                        01540005
           PERFORM DEBUT-BGV810.                                        01550005
           PERFORM TRAIT-BGV810.                                        01560005
           PERFORM FIN-BGV810.                                          01570005
                                                                        01580005
      * ---------------------------------------------------------- *    01590005
      *                  D E B U T   B G V 8 1 0                   *    01600005
      * ---------------------------------------------------------- *    01610005
                                                                        01620005
       DEBUT-BGV810 SECTION.                                            01630005
                                                                        01640005
           PERFORM OUVERTURE-FICHIER.                                   01650005
           READ FDATE AT END                                            01660005
                MOVE 'PAS DE DATE EN ENTREE' TO ABEND-MESS              01670005
                PERFORM FIN-ANORMALE                                    01671011
           END-READ.                                                    01680005
           MOVE FDATE-JJ      TO W-ED-DATE1-JJ JOUR-JJ.                         
           MOVE FDATE-MM      TO W-ED-DATE1-MM JOUR-MM.                         
           MOVE FDATE-SS      TO W-ED-DATE1-SS JOUR-SS.                         
           MOVE FDATE-AA      TO W-ED-DATE1-AA JOUR-AA.                         
           MOVE W-ED-DATE1    TO W-ED-DATE.                                     
           DISPLAY 'W-ED-DATE ' W-ED-DATE1.                                     
           INITIALIZE WORK-BETDATC.                                             
           MOVE '5'        TO GFDATA.                                           
           MOVE DATE-JOU   TO GFSAMJ-0.                                         
           CALL 'BETDATC'  USING WORK-BETDATC.                                  
           IF GFVDAT NOT = '1'                                                  
              DISPLAY 'DATE DE TRAITEMENT ANORMALE'                             
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
           COMPUTE QUANT1 = GFQNT0 - 6.                                         
      *--- CALCUL DE LA DATE DE DEBUT DE SEMAINE PRECEDENTE                     
           INITIALIZE WORK-BETDATC.                                             
           MOVE '3' TO GFDATA.                                                  
           MOVE QUANT1 TO GFQNT0.                                               
           CALL 'BETDATC' USING WORK-BETDATC                                    
           IF GFVDAT NOT = '1'                                                  
              DISPLAY 'CALCUL DATE ANORMAL'                                     
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
           MOVE GFWEEK   TO BORNE-AN.                                           
           MOVE '8' TO GFDATA.                                                  
           MOVE BORNE-AN TO GFWEEK.                                             
           CALL 'BETDATC' USING WORK-BETDATC                                    
           IF GFVDAT NOT = '1'                                                  
              DISPLAY 'CALCUL DATE-ED-1 '                                       
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
           MOVE GFSAMJ-0      TO DATE-JOU.                                      
           MOVE JOUR-JJ       TO W-ED-DATE1-JJ.                                 
           MOVE JOUR-MM       TO W-ED-DATE1-MM.                                 
           MOVE JOUR-SS       TO W-ED-DATE1-SS.                                 
           MOVE JOUR-AA       TO W-ED-DATE1-AA.                                 
           DISPLAY 'W-ED-DATE1 ' W-ED-DATE1.                                    
      *--- CALCUL DE LA DATE DE FIN DE SEMAINE PRECEDENTE                       
           COMPUTE QUANT1 = GFQNT0 + 6                                          
           MOVE '3' TO GFDATA                                                   
           MOVE QUANT1 TO GFQNT0                                                
           CALL 'BETDATC' USING WORK-BETDATC                                    
           IF GFVDAT NOT = '1'                                                  
              DISPLAY 'CALCUL DATE-ED-2 '                                       
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
           MOVE GFSAMJ-0      TO DATE-JOU.                                      
           MOVE JOUR-JJ       TO W-ED-DATE2-JJ.                                 
           MOVE JOUR-MM       TO W-ED-DATE2-MM.                                 
           MOVE JOUR-SS       TO W-ED-DATE2-SS.                                 
           MOVE JOUR-AA       TO W-ED-DATE2-AA.                                 
           DISPLAY 'W-ED-DATE2 ' W-ED-DATE2.                                    
           READ FNSOC AT END                                            01660005
                MOVE 'PAS DE SOCIETE EN ENTREE' TO ABEND-MESS           01670005
                PERFORM FIN-ANORMALE                                    01671011
           END-READ.                                                    01680005
           MOVE FSOC        TO W-NSOC.                                          
       FIN-DEBUT-BGV810.  EXIT.                                         01750005
                                                                        01760005
      * ---------------------------------------------------------- *    01770005
                                                                        01780005
       OUVERTURE-FICHIER SECTION.                                       01790005
                                                                        01800005
           OPEN INPUT  FDATE.                                           01810011
           OPEN INPUT  FNSOC.                                           01810011
           OPEN INPUT  FGV805.                                          01810011
           OPEN OUTPUT IGV810.                                          01810011
                                                                        01830005
       FIN-OUVERTURE-FICHIER. EXIT.                                     01840005
                                                                        01780005
      * ---------------------------------------------------------- *    02860005
      *            T R A I T E M E N T   B G V 8 1 0               *    02870005
      * ---------------------------------------------------------- *    02880005
       TRAIT-BGV810 SECTION.                                                    
           PERFORM LECTURE-FGV805.                                              
           IF NON-FIN-FGV805                                                    
              PERFORM EDITION-ENTETE                                            
              PERFORM TRAITE-FICHIER UNTIL FIN-FGV805                           
              MOVE IGV810-E45 TO LIGNE-IGV810                                   
              WRITE ENR-IGV810 FROM LIGNE-IGV810                                
              ADD 1 TO CPT-LIGNE-IGV810                                         
           ELSE                                                                 
              MOVE IGV810-FICHIER-VIDE TO LIGNE-IGV810                          
              WRITE ENR-IGV810 FROM LIGNE-IGV810                                
              ADD 1 TO CPT-LIGNE-IGV810                                         
           END-IF.                                                              
       FIN-TRAIT-BGV810. EXIT.                                          01840005
      * ---------------------------------------------------------- *            
      *                      F I N    B G V 8 1 0                  *    03400005
      * ---------------------------------------------------------- *    03410005
                                                                        03420005
       FIN-BGV810 SECTION.                                              03430005
                                                                        03440005
           CLOSE FDATE.                                                 03890011
           CLOSE FNSOC.                                                 03890011
           CLOSE FGV805.                                                03890011
           CLOSE IGV810.                                                03890011
           PERFORM COMPTE-RENDU.                                        03470005
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    03480005
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        03490005
       FIN-FIN-BGV810. EXIT.                                            03500005
      * ---------------------------------------------------------- *    02860005
      *            T R A I T E     F I C H I E R                   *    02870005
      * ---------------------------------------------------------- *    02880005
       TRAITE-FICHIER  SECTION.                                         02900005
           IF F-WSAUT NUMERIC                                                   
              MOVE F-WSAUT TO CTR-2                                             
              COMPUTE CTR-1 = CPT-LIGNE-IGV810 + CTR-2 + 2                      
           END-IF.                                                              
           IF (CTR-1 > NB-LIGNE-MAX - 4 OR F-WSAUT = 'P')                       
              AND CPT-LIGNE-IGV810 > 12                                         
              MOVE IGV810-E45 TO LIGNE-IGV810                                   
              WRITE ENR-IGV810 FROM LIGNE-IGV810                                
              ADD 1 TO CPT-LIGNE-IGV810                                         
              PERFORM EDITION-ENTETE                                            
           ELSE                                                                 
              PERFORM VARYING W-I FROM 1 BY 1 UNTIL W-I > CTR-2                 
                 MOVE IGV810-E50 TO LIGNE-IGV810                                
                 WRITE ENR-IGV810 FROM LIGNE-IGV810                             
              END-PERFORM                                                       
              ADD CTR-2 TO CPT-LIGNE-IGV810                                     
           END-IF                                                               
           PERFORM ECRIT-D05.                                                   
       FIN-TRAITE-FICHIER. EXIT.                                        02990005
                                                                        03040005
      *----------------------------------------------------------- *    02100005
       ECRIT-D05  SECTION.                                              02900005
      *----------------------------------------------------------- *    02100005
           INITIALIZE TABLE-VALEUR.                                             
           INITIALIZE IGV810-DSECT.                                             
           MOVE F-LAGREGATED TO IGV810-D05-NRUBR.                               
           MOVE F-SEM-1-NB   TO IGV810-D05-NB.                                  
           MOVE F-SEM-1-CA   TO IGV810-D05-CA.                                  
           IF F-SEM-1-NB NOT = ZERO                                             
              COMPUTE W-PVM = F-SEM-1-CA / F-SEM-1-NB                           
              COMPUTE W-MBU = (F-SEM-1-CA - F-SEM-1-MT) / F-SEM-1-NB            
           END-IF.                                                              
           MOVE W-PVM        TO IGV810-D05-PVM.                                 
           MOVE W-MBU        TO IGV810-D05-MBU.                                 
           MOVE F-SEM-PVOL   TO IGV810-D05-MONTVOL.                             
           IF F-SEM-1-CA NOT = ZERO                                             
              COMPUTE W-TAUXVOL = (F-SEM-PVOL * 10000) / F-SEM-1-CA             
              COMPUTE W-TAUXART = (F-SEM-PRIM * 10000) / F-SEM-1-CA             
           END-IF.                                                              
           MOVE W-TAUXVOL    TO IGV810-D05-TAUXVOL.                             
           MOVE W-TAUXART    TO IGV810-D05-TAUXART.                             
           MOVE F-SEM-PRIM   TO IGV810-D05-MONTART.                             
           COMPUTE W-TOTTAUX = (F-SEM-PVOL + F-SEM-PRIM).                       
           MOVE W-TOTTAUX    TO IGV810-D05-MONTTOT.                             
           IF F-SEM-1-CA NOT = ZERO                                             
              COMPUTE W-TOTTAUX = (W-TOTTAUX * 10000) / F-SEM-1-CA              
           END-IF.                                                              
           MOVE W-TOTTAUX    TO IGV810-D05-TOTTAUX.                             
           MOVE IGV810-D05 TO LIGNE-IGV810.                                     
           WRITE ENR-IGV810 FROM LIGNE-IGV810.                                  
           INITIALIZE TABLE-VALEUR.                                             
           MOVE SPACES       TO IGV810-D05-NRUBR.                               
           MOVE F-CUM-1-NB   TO IGV810-D05-NB.                                  
           MOVE F-CUM-1-CA   TO IGV810-D05-CA.                                  
           IF F-CUM-1-NB NOT = ZERO                                             
              COMPUTE W-PVM = F-CUM-1-CA / F-CUM-1-NB                           
              COMPUTE W-MBU = (F-CUM-1-CA - F-CUM-1-MT) / F-CUM-1-NB            
           END-IF.                                                              
           MOVE W-MBU        TO IGV810-D05-MBU.                                 
           MOVE F-CUM-PVOL   TO IGV810-D05-MONTVOL.                             
           IF F-CUM-1-CA NOT = ZERO                                             
              COMPUTE W-TAUXVOL = (F-CUM-PVOL * 10000) / F-CUM-1-CA             
              COMPUTE W-TAUXART = (F-CUM-PRIM * 10000) / F-CUM-1-CA             
           END-IF.                                                              
           MOVE W-TAUXVOL    TO IGV810-D05-TAUXVOL.                             
           MOVE W-PVM        TO IGV810-D05-PVM.                                 
           MOVE F-CUM-PRIM   TO IGV810-D05-MONTART.                             
           MOVE W-TAUXART    TO IGV810-D05-TAUXART.                             
           COMPUTE W-TOTTAUX = (F-CUM-PVOL + F-CUM-PRIM).                       
           MOVE W-TOTTAUX    TO IGV810-D05-MONTTOT.                             
           IF F-CUM-1-CA NOT = ZERO                                             
              COMPUTE W-TOTTAUX = (W-TOTTAUX * 10000) / F-CUM-1-CA              
           END-IF.                                                              
           MOVE W-TOTTAUX  TO IGV810-D05-TOTTAUX.                               
           MOVE IGV810-D05 TO LIGNE-IGV810.                                     
           WRITE ENR-IGV810 FROM LIGNE-IGV810.                                  
      *    MOVE IGV810-D05 TO LIGNE-IGV810.                                     
      *    WRITE ENR-IGV810 FROM LIGNE-IGV810 AFTER 0.                          
           ADD 2 TO CPT-LIGNE-IGV810.                                           
           PERFORM LECTURE-FGV805.                                              
                                                                        02910005
       FIN-ECRIT-D05. EXIT.                                             02990005
      *----------------------------------------------------------- *    02100005
       LECTURE-FGV805            SECTION.                                       
      *----------------------------------------------------------- *    02100005
           READ FGV805  INTO FGV805-DSECT AT END                                
              SET FIN-FGV805  TO TRUE                                           
           END-READ.                                                            
      *    DISPLAY 'FGV805-DSECT ' FGV805-DSECT.                                
           IF NON-FIN-FGV805                                                    
              ADD 1 TO CPT-FGV805                                               
           END-IF.                                                              
       FIN-LECTURE-FGV805. EXIT.                                                
                                                                        02850005
      *----------------------------------------------------------- *    03410005
       EDITION-ENTETE   SECTION.                                        03430005
      *----------------------------------------------------------- *    02100005
                                                                        03440005
           ADD 1 TO CPT-PAGE-IGV810.                                            
           MOVE W-ED-DATE    TO IGV810-E00-DATEDITE.                            
           MOVE W-ED-DATE1   TO IGV810-E15-SEMDEB.                              
           MOVE W-ED-DATE2   TO IGV810-E20-SEMFIN.                              
           MOVE F-CETAT      TO IGV810-E15-ETAT.                                
           IF F-CODICGROUPE = 'O'                                               
              MOVE 'CODIC GROUPE' TO IGV810-E20-ETATCOD                         
           END-IF.                                                              
           MOVE W-NSOC       TO IGV810-E25-NSOCIETE.                            
           MOVE CPT-PAGE-IGV810 TO IGV810-E00-NOPAGE.                           
           MOVE IGV810-E00 TO LIGNE-IGV810.                                     
           IF CPT-PAGE-IGV810 > 1                                               
              WRITE ENR-IGV810 FROM LIGNE-IGV810 AFTER PAGE                     
           ELSE                                                                 
              WRITE ENR-IGV810 FROM LIGNE-IGV810                                
           END-IF.                                                              
           MOVE IGV810-E05 TO LIGNE-IGV810.                                     
           WRITE ENR-IGV810 FROM LIGNE-IGV810.                                  
           MOVE IGV810-E10 TO LIGNE-IGV810.                                     
           WRITE ENR-IGV810 FROM LIGNE-IGV810.                                  
           MOVE IGV810-E15 TO LIGNE-IGV810.                                     
           WRITE ENR-IGV810 FROM LIGNE-IGV810 AFTER 2.                          
           MOVE IGV810-E20 TO LIGNE-IGV810.                                     
           WRITE ENR-IGV810 FROM LIGNE-IGV810.                                  
           MOVE IGV810-E25 TO LIGNE-IGV810.                                     
           WRITE ENR-IGV810 FROM LIGNE-IGV810.                                  
           MOVE IGV810-E30 TO LIGNE-IGV810.                                     
           WRITE ENR-IGV810 FROM LIGNE-IGV810 AFTER 2                           
           MOVE IGV810-E35 TO LIGNE-IGV810.                                     
           WRITE ENR-IGV810 FROM LIGNE-IGV810.                                  
           MOVE IGV810-E40 TO LIGNE-IGV810.                                     
           WRITE ENR-IGV810 FROM LIGNE-IGV810.                                  
           MOVE IGV810-E45 TO LIGNE-IGV810.                                     
           WRITE ENR-IGV810 FROM LIGNE-IGV810.                                  
           MOVE 12 TO CPT-LIGNE-IGV810.                                         
       FIN-EDITION-ENTETE. EXIT.                                                
                                                                        02850005
      *----------------------------------------------------------- *    03520005
       COMPTE-RENDU SECTION.                                            03540005
      *----------------------------------------------------------- *    03520005
                                                                        03550005
           DISPLAY '**'.                                                03560005
           DISPLAY '**       B G V 8 1 0 '.                             03570005
           DISPLAY '**'.                                                03580005
           DISPLAY '** NBRE DE LECTURES FGV805  : ' CPT-FGV805.         03610005
           DISPLAY '** NBRE LIGNES IGV810  : ' CPT-LIGNE-IGV810.        03620005
           DISPLAY '** NBRE PAGES  IGV810  : ' CPT-PAGE-IGV810.         03620005
           DISPLAY '**'.                                                03630005
                                                                        03640005
       FIN-COMPTE-RENDU.  EXIT.                                         03650005
                                                                        03660005
                                                                        03840005
      * ------------------------ FIN-ANORMALE ---------------------- *  03850005
                                                                        03860005
       FIN-ANORMALE SECTION.                                            03870005
                                                                        03880005
           CLOSE FGV805.                                                03890011
           CLOSE IGV810.                                                03890011
           CLOSE FDATE.                                                 03890011
           CLOSE FNSOC.                                                 03890011
           MOVE 'BGV810' TO ABEND-PROG.                                 03900005
           CALL 'ABEND' USING ABEND-PROG ABEND-MESS.                    03910005
                                                                        03920005
       FIN-FIN-ANORMALE. EXIT.                                          03930005
