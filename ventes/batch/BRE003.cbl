      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000010
       PROGRAM-ID.   BRE003.                                            00000020
       AUTHOR.       DSA015.                                            00000030
                                                                        00000040
      * ------------------------------------------------------------ *  00000050
      *                                                              *  00000060
      *   PROJET           :                                         *  00000070
      *   PROGRAMME        : BRE003.                                 *  00000080
      *   PERIODICITE      : HEBDOMADAIRE                            *  00000090
      *   FONCTION         : CUMUL VENTES HEBDO + CALCUL CUMUL ANNUEL*  00000100
      *   DATE DE CREATION : 11/09/1992.                             *  00000110
      *                                                              *  00000120
      * ------------------------------------------------------------ *  00000130
                                                                        00000140
       ENVIRONMENT DIVISION.                                            00000150
       CONFIGURATION SECTION.                                           00000160
       INPUT-OUTPUT SECTION.                                            00000170
       FILE-CONTROL.                                                    00000180
                                                                        00000190
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FRE003   ASSIGN TO  FRE003.                          00000200
      *--                                                                       
           SELECT  FRE003   ASSIGN TO  FRE003                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FRE003R  ASSIGN TO  FRE003R.                         00000200
      *--                                                                       
           SELECT  FRE003R  ASSIGN TO  FRE003R                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FEXTRAC  ASSIGN TO  FEXTRAC.                         00000200
      *--                                                                       
           SELECT  FEXTRAC  ASSIGN TO  FEXTRAC                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FCUMUL   ASSIGN TO  FCUMUL.                          00000210
      *--                                                                       
           SELECT  FCUMUL   ASSIGN TO  FCUMUL                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FCUMULS  ASSIGN TO  FCUMULS.                         00000210
      *--                                                                       
           SELECT  FCUMULS  ASSIGN TO  FCUMULS                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FEXERCIC ASSIGN TO FEXERCIC.                                 
      *--                                                                       
           SELECT  FEXERCIC ASSIGN TO FEXERCIC                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FNSOC    ASSIGN TO  FNSOC.                           00000210
      *--                                                                       
           SELECT  FNSOC    ASSIGN TO  FNSOC                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FDATE    ASSIGN TO  FDATE.                           00000210
      *                                                                         
      *--                                                                       
           SELECT  FDATE    ASSIGN TO  FDATE                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00000220
      *}                                                                        
       DATA DIVISION.                                                   00000230
       FILE SECTION.                                                    00000240
                                                                        00000250
      * ------------------------------------------------------------ *  00000260
      *               D E S C R I P T I O N   F I C H I E R          *  00000270
      * ------------------------------------------------------------ *  00000280
                                                                        00000290
       FD  FRE003                                                       00000300
           RECORDING F                                                  00000310
           BLOCK 0 RECORDS                                              00000320
           LABEL RECORD STANDARD.                                       00000330
       01  ENR-FRE003  PIC X(512).                                      00000340
       FD  FRE003R                                                      00000300
           RECORDING F                                                  00000310
           BLOCK 0 RECORDS                                              00000320
           LABEL RECORD STANDARD.                                       00000330
       01  ENR-FRE003R PIC X(512).                                      00000340
       FD  FEXTRAC                                                      00000300
           RECORDING F                                                  00000310
           BLOCK 0 RECORDS                                              00000320
           LABEL RECORD STANDARD.                                       00000330
       01  ENR-FEXTRAC PIC X(512).                                      00000340
                                                                        00000340
       FD  FNSOC                                                        00000300
           RECORDING F                                                  00000310
           BLOCK 0 RECORDS                                              00000320
           LABEL RECORD STANDARD.                                       00000330
       01  FILE-FNSOC.                                                  00000340
           03  F-NSOC  PIC X(03).                                       00000350
           03  FILLER  PIC X(77).                                       00000350
                                                                        00000360
        FD  FDATE                                                       00000370
            RECORDING F                                                 00000380
            BLOCK 0 RECORDS                                             00000390
            LABEL RECORD STANDARD.                                      00000400
        01  FILE-DATE.                                                  00000410
            03  F-DATE.                                                 00000420
                06   JJ       PIC X(2).                                 00000430
                06   MM       PIC X(2).                                 00000440
                06   SS       PIC X(2).                                 00000450
                06   AA       PIC X(2).                                 00000460
            03  FILLER        PIC X(72).                                00000470
                                                                        00000480
                                                                        00000370
       FD  FCUMUL                                                       00000380
           RECORDING F                                                  00000390
           BLOCK 0 RECORDS                                              00000400
           LABEL RECORD STANDARD.                                       00000410
       01  ENR-CUMUL.                                                   00000420
           02  FILLER PIC X(100).                                       00000430
                                                                        00000440
       FD  FCUMULS                                                      00000380
           RECORDING F                                                  00000390
           BLOCK 0 RECORDS                                              00000400
           LABEL RECORD STANDARD.                                       00000410
       01  ENR-CUMULS.                                                  00000420
           02  FILLER PIC X(100).                                       00000430
                                                                        00000440
                                                                        00000450
       FD  FEXERCIC                                                             
           RECORDING F                                                          
           BLOCK CONTAINS 0 RECORDS                                             
           LABEL RECORD STANDARD.                                               
       01  ENR-FEXERCIC.                                                        
           03  EXER-PARAM   PIC 9(02).                                          
           03  FILLER       PIC X(78).                                          
      * ------------------------------------------------------------ *  00000460
      *                W O R K I N G - S T O R A G E                 *  00000470
      * ------------------------------------------------------------ *  00000480
                                                                        00000490
       WORKING-STORAGE SECTION.                                         00000500
                                                                        00000510
                                                                        00000530
                                                                        00000600
           COPY IRE003.                                                 00000620
       01 DSECT-IRE003-R REDEFINES DSECT-IRE003.                                
          02 FILLER           PIC X(505).                                       
          02 IRE003-CUMUL     PIC X(1).                                         
          02 IRE003-DJOUR     PIC X(6).                                         
           COPY IRECUM.                                                         
                                                                        00000600
      * DESCRITION UTILISEE UNIQUEMENT EN CREATION                              
       01  Z-DSECT-IRECUM.                                                      
           10 Z-CUMUL-NSOCIETE            PIC X(03).                    007  003
           10 Z-CUMUL-NLIEU               PIC X(03).                    007  003
           10 Z-CUMUL-VAL-FORC-P          PIC S9(09) COMP-3.            082  004
           10 Z-CUMUL-VAL-FORC-M          PIC S9(09) COMP-3.            086  004
           10 Z-CUMUL-VAL-REM             PIC S9(09) COMP-3.            090  002
           10 Z-CUMUL-VENTES              PIC S9(11) COMP-3.            092  004
           10 Z-CUMUL-EXERCICE            PIC X(6).                     092  004
           10 Z-CUMUL-CGRPMAG             PIC X(2).                     092  004
           10 FILLER                    PIC X(65).                              
      *                                                                 00000640
       77  CPT-FRE003                PIC 9(07)    VALUE 0.              00000820
       77  CPT-FRE003R               PIC 9(07)    VALUE 0.              00000820
       77  CPT-FEXTRAC               PIC 9(07)    VALUE 0.              00000820
       77  CPT-FCUMUL                PIC 9(07)    VALUE 0.              00000820
       77  CPT-FCUMULS               PIC 9(07)    VALUE 0.              00000820
                                                                        00000980
       01  TEST-FCUMUL               PIC X(01) VALUE '0'.               00000990
           88 PAS-FIN-FCUMUL         VALUE '0'.                         00001000
           88 FIN-FCUMUL             VALUE '1'.                         00001010
                                                                        00001020
       01  TEST-DERNIER              PIC X(01) VALUE '0'.               00000990
           88 NO-DERNIER-CUMUL       VALUE '0'.                         00001000
           88 DERNIER-CUMUL          VALUE '1'.                         00001010
                                                                        00001020
       01  TEST-VENTES               PIC X(01) VALUE '0'.               00000990
           88 MAG-AVEC-VENTE         VALUE '0'.                         00001000
           88 MAG-SANS-VENTE         VALUE '1'.                         00001010
                                                                        00001020
       01  TEST-EXISTE               PIC X(01) VALUE '0'.               00000990
           88 NO-TROUVE-CUMUL        VALUE '0'.                         00001000
           88 TROUVE-CUMUL           VALUE '1'.                         00001000
                                                                        00001020
       01  TEST-HEDBO                PIC X(01) VALUE '0'.                       
           88 AUCUNE-DATE            VALUE '0'.                                 
           88 AU-MOINS-UNE-DATE      VALUE '1'.                                 
       01  TEST-FRE003               PIC X(01) VALUE '0'.               00000990
           88 PAS-FIN-FRE003          VALUE '0'.                        00001000
           88 FIN-FRE003             VALUE '1'.                         00001010
                                                                        00001020
       01  W-SSAAMMJJ.                                                          
           05  W-SSAAMM.                                                        
               10  W-SS2       PIC X(02).                                       
               10  W-AA2       PIC X(02).                                       
               10  W-MM2       PIC X(02).                                       
           05  W-JJ2           PIC X(02).                                       
                                                                        00004150
       01  W-FIN-MOIS                PIC X(6).                                  
       01  W-EXER.                                                              
           02 W-EXERSSAA             PIC 9(4).                                  
           02 W-EXERMM               PIC 9(2).                                  
       01  W-FIN-PERIODE             PIC X(8).                                  
       01  W-DEB-PERIODE.                                                       
           02 SS-DEB-PER             PIC X(2).                                  
           02 W-DEB-PER.                                                        
              05 W-AA-DEB            PIC X(2).                                  
              05 W-MM-DEB            PIC X(2).                                  
              05 W-JJ-DEB            PIC X(2).                                  
       01 W-NSOCIETE                  PIC X(3).                         00001200
       01 SAUV-CLEFIN.                                                          
          02 FIN-NSOCIETE             PIC X(3).                         00001200
          02 FIN-NLIEU                PIC X(3).                         00001200
          02 FIN-DJOUR                PIC X(6).                         00001200
       01 SAUV-CLE.                                                             
          02 SAUV-NSOCIETE                PIC X(3).                     00001200
          02 SAUV-NLIEU                   PIC X(3).                     00001200
          02 SAUV-DJOUR                   PIC X(6).                     00001200
       01 CUMUL-CLE.                                                            
          02 W-CUMUL-NSOCIETE                PIC X(3).                  00001200
          02 W-CUMUL-NLIEU                   PIC X(3).                  00001200
          02 W-CUMUL-EXERCICE                PIC X(6).                  00001200
       01 SAUV-CGRPMAG                    PIC X(2).                     00001200
       01 SAUV-CUMUL                      PIC X(1).                     00001200
       01 W-CUMUL-CGRPMAG                 PIC X(2).                     00001200
       01 SAUV-CUMUL-CGRPMAG              PIC X(2).                     00001200
       01 SAUV-CUMUL-NSOCIETE             PIC X(3).                     00001200
       01 SAUV-CUMUL-NLIEU                PIC X(3).                     00001200
       01 ZONES-CUMULS.                                                         
          02 W-VAL-FORC-M              PIC S9(9)  COMP-3.                       
          02 W-VAL-FORC-P              PIC S9(9)  COMP-3.                       
          02 W-VAL-REM                 PIC S9(9)  COMP-3.                       
          02 W-VENTES1                 PIC S9(11) COMP-3.                       
                                                                        00001210
       01 ZONES-MOIS.                                                           
          02 MOIS-VAL-FORC-M              PIC S9(9)  COMP-3.                    
          02 MOIS-VAL-FORC-P              PIC S9(9)  COMP-3.                    
          02 MOIS-VAL-REM                 PIC S9(9)  COMP-3.                    
          02 MOIS-VENTES1                 PIC S9(11) COMP-3.                    
                                                                        00001210
       01 ZONES-SANS.                                                           
          02 SANS-VAL-FORC-M              PIC S9(9)  COMP-3.                    
          02 SANS-VAL-FORC-P              PIC S9(9)  COMP-3.                    
          02 SANS-VAL-REM                 PIC S9(9)  COMP-3.                    
          02 SANS-VENTES1                 PIC S9(11) COMP-3.                    
                                                                        00001210
       01  DATEDEB         PIC X(6).                                            
       01  DATEDEB-R REDEFINES DATEDEB.                                         
           03  DATEDEBSSAA PIC 9(4).                                            
           03  DATEDEBMM   PIC 9(2).                                            
       01  DATEFIN         PIC X(6).                                            
       01  DATEFIN-R REDEFINES DATEFIN.                                         
           03  DATEFINSSAA PIC X(4).                                            
           03  DATEFINMM   PIC 9(2).                                            
      * ------------------------------------------------------------ *  00001220
      *         DESCRIPTION DES ZONES D'APPEL DU MODULE ABEND        *  00001230
      * ------------------------------------------------------------ *  00001240
                                                                        00001250
           COPY ABENDCOP.                                               00001260
           COPY WORKDATC.                                               00001260
                                                                        00001270
                                                                        00004890
      * ------------------------------------------------------------ *  00001370
      *              P R O C E D U R E     D I V I S I O N           *  00001380
      * ------------------------------------------------------------ *  00001390
       PROCEDURE DIVISION.                                              00001410
                                                                        00001420
      * ------------------------------------------------------------ *  00001430
      *             T R A M E     D U     P R O G R A M M E          *  00001440
      * ------------------------------------------------------------ *  00001450
      *                                                              *  00001460
      *            -----------------------------------------         *  00001470
      *            -  MODULE DE BASE DU PROGRAMME  BRE003  -         *  00001480
      *            -----------------------------------------         *  00001490
      *                               I                              *  00001500
      *            -----------------------------------------         *  00001510
      *            I                  I                    I         *  00001520
      *       -----------      ----------------        ---------     *  00001530
      *       -  DEBUT  -      -  TRAITEMENT  -        -  FIN  -     *  00001540
      *       -----------      ----------------        ---------     *  00001550
      *                                                              *  00001560
      * ------------------------------------------------------------ *  00001570
                                                                        00001580
           PERFORM DEBUT-BRE003.                                        00001590
           PERFORM CALCUL-DATE-HEBDO.                                   00040001
           PERFORM CALCUL-DATEDEBFIN                                            
           PERFORM LECTURE-FCUMUL                                               
           PERFORM LECTURE-FRE003.                                      00001970
           MOVE IRE003-NSOCIETE TO SAUV-NSOCIETE.                               
           MOVE IRE003-NLIEU    TO SAUV-NLIEU                                   
           MOVE IRE003-DJOUR    TO SAUV-DJOUR                                   
           MOVE IRE003-CGRPMAG  TO SAUV-CGRPMAG                         00001200
           MOVE SAUV-CLE        TO SAUV-CLEFIN                                  
           MOVE IRE003-CUMUL   TO SAUV-CUMUL.                                   
           PERFORM TRAIT-BRE003 UNTIL FIN-FRE003.                       00001600
           PERFORM RUPTURE-NLIEU.                                               
           PERFORM FIN-BRE003.                                          00001610
                                                                        00001620
      * ------------------------------------------------------------ *  00001630
      *                     D E B U T    B R E 0 0 3                 *  00001640
      * ------------------------------------------------------------ *  00001650
                                                                        00001660
       DEBUT-BRE003 SECTION.                                            00001670
                                                                        00001680
           PERFORM OUVERTURE-FICHIERS.                                  00001740
                                                                        00001750
           PERFORM LECTURE-PARAMETRES.                                          
                                                                        00001840
       FIN-DEBUT-BRE003. EXIT.                                          00001930
       LECTURE-PARAMETRES SECTION.                                              
           OPEN INPUT FNSOC.                                                    
           READ FNSOC AT END                                                    
              MOVE 'PAS DE FNSOC EN ENTREE' TO ABEND-MESS                       
              PERFORM FIN-ANORMALE                                              
           END-READ.                                                            
           MOVE F-NSOC       TO W-NSOCIETE.                             00003380
           CLOSE FNSOC.                                                         
           OPEN INPUT FDATE.                                                    
           READ FDATE AT END                                                    
              MOVE 'PAS DE DATE EN ENTREE' TO ABEND-MESS                        
              PERFORM FIN-ANORMALE                                              
           END-READ.                                                            
           DISPLAY 'FILE DATE : ' FILE-DATE                                     
           MOVE SS   TO W-SS2                                           00003380
           MOVE AA   TO W-AA2                                           00003380
           MOVE MM   TO W-MM2                                           00003380
           MOVE JJ   TO W-JJ2                                           00003380
           CLOSE FDATE.                                                         
      *                                                                         
      *-- OUVERTURE FEXERCIC                                                    
           OPEN INPUT FEXERCIC .                                                
      *                                                                         
           READ FEXERCIC AT END                                                 
              MOVE 'PAS D''EXERCICE DANS FEXERCIC ' TO ABEND-MESS               
              PERFORM FIN-ANORMALE                                              
           END-READ.                                                            
           IF EXER-PARAM < ZERO OR EXER-PARAM > 12                              
              MOVE 'EXERCICE INVALIDE' TO ABEND-MESS                            
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
      *                                                                         
      *****************************************************************         
       FIN-LECTURE-PARAMETRES. EXIT.                                            
                                                                        00001940
      * -------------------- OUVERTURE-FICHIERS -------------------- *  00002060
                                                                        00002070
       OUVERTURE-FICHIERS SECTION.                                      00002080
                                                                        00002090
           OPEN INPUT   FRE003 FCUMUL                                   00002100
                OUTPUT  FCUMULS FEXTRAC FRE003R.                        00002110
                                                                        00002120
           INITIALIZE ZONES-CUMULS.                                             
           INITIALIZE ZONES-MOIS.                                               
           INITIALIZE ZONES-SANS.                                               
       FIN-OUVERTURE-FICHIERS. EXIT.                                    00002130
                                                                        00002140
                                                                        00002150
      * ------------------------------------------------------------ *  00002160
      *                     T R A I T    B R E 0 0 3                 *  00002170
      * ------------------------------------------------------------ *  00002180
                                                                        00002190
       TRAIT-BRE003 SECTION.                                            00002200
      *                                                                 02390   
           IF AU-MOINS-UNE-DATE                                                 
              IF (IRE003-NSOCIETE NOT = SAUV-NSOCIETE OR                   00001
                 IRE003-NLIEU NOT = SAUV-NLIEU )                           00001
                 MOVE DSECT-IRE003    TO ENR-FRE003                             
                 PERFORM RUPTURE-NLIEU                                          
                 MOVE ENR-FRE003      TO DSECT-IRE003                           
                 MOVE IRE003-NSOCIETE TO SAUV-NSOCIETE                     00001
                 MOVE IRE003-NLIEU    TO SAUV-NLIEU                        00001
                 MOVE IRE003-DJOUR    TO SAUV-DJOUR                             
                 MOVE IRE003-CGRPMAG  TO SAUV-CGRPMAG                      00001
                 MOVE IRE003-CUMUL    TO SAUV-CUMUL                             
                 MOVE SAUV-CLE        TO SAUV-CLEFIN                            
                 SET AUCUNE-DATE      TO TRUE                                   
              ELSE                                                              
                 IF IRE003-DJOUR NOT = SAUV-DJOUR                               
                    PERFORM RUPTURE-DJOUR                                       
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE IRE003-NSOCIETE TO SAUV-NSOCIETE                        00001
              MOVE IRE003-NLIEU    TO SAUV-NLIEU                           00001
              MOVE IRE003-DJOUR    TO SAUV-DJOUR                                
              MOVE IRE003-CGRPMAG  TO SAUV-CGRPMAG                         00001
              MOVE SAUV-CLE        TO SAUV-CLEFIN                               
              MOVE IRE003-CUMUL    TO SAUV-CUMUL                                
           END-IF                                                               
           PERFORM CUMUL-HEBDO                                                  
           PERFORM LECTURE-FRE003.                                      00001970
                                                                        00002520
       FIN-TRAIT-BRE003. EXIT.                                          00002540
      *------------------------------------------------------------*            
      * ON CUMULE LES VENTES AU NIVEAU HEBDO                                    
      * SI LES VENTES CORRESPONDENT A LA PERIODE TRAIT�E ON LES                 
      * RECONDUIT DANS LE FICHIER RELIQUAT AVEC UN FLAG � 'O'                   
      * (POUR UNE REEDITION)                                                    
      *------------------------------------------------------------*            
       CUMUL-HEBDO SECTION.                                                     
           IF IRE003-PERDEB = W-DEB-PERIODE                                     
              SET AU-MOINS-UNE-DATE TO TRUE                                     
              PERFORM PAS-DE-RUPTURE                                            
              MOVE 'O'     TO IRE003-CUMUL                                      
              PERFORM ECRITURE-RELIQUAT                                         
           ELSE                                                                 
              IF IRE003-PERDEB > W-DEB-PERIODE                                  
                 MOVE 'N'     TO IRE003-CUMUL                                   
                 PERFORM ECRITURE-RELIQUAT                                      
              END-IF                                                            
           END-IF.                                                              
       FIN-CUMUL-HEBDO. EXIT.                                                   
      *---------------------------------------------------------*               
      * ON CUMULE TANT QUE PAD DE RUPTURE SUR NLIEU NSOCIETE    *               
      * ON RECONDUIT EN SORTIE TOUS LES ENREGISTREMENTS EN ENTEE*               
      *---------------------------------------------------------*               
       PAS-DE-RUPTURE SECTION.                                                  
           IF IRE003-CUMUL NOT = 'O'        AND                                 
              ((IRE003-DJOUR NOT < DATEDEB    AND                               
              IRE003-DJOUR NOT > DATEFIN)     OR                                
              (IRE003-PERFIN (1:6) = DATEDEB    AND                             
               IRE003-DJOUR < DATEDEB))                                         
              IF (IRE003-PERFIN (1:6) = DATEDEB    AND                          
                  IRE003-DJOUR < DATEDEB)                                       
                 ADD  IRE003-VAL-FORC-M     TO  MOIS-VAL-FORC-M                 
                 ADD  IRE003-VAL-FORC-P     TO  MOIS-VAL-FORC-P                 
                 ADD  IRE003-VAL-REM        TO  MOIS-VAL-REM                    
                 ADD  IRE003-VENTES1        TO  MOIS-VENTES1                    
                 MOVE IRE003-DJOUR          TO  W-FIN-MOIS                      
              ELSE                                                              
                 ADD  IRE003-VAL-FORC-M     TO  W-VAL-FORC-M                    
                                                MOIS-VAL-FORC-M                 
                 ADD  IRE003-VAL-FORC-P     TO  W-VAL-FORC-P                    
                                                MOIS-VAL-FORC-P                 
                 ADD  IRE003-VAL-REM        TO  W-VAL-REM                       
                                                MOIS-VAL-REM                    
                 ADD  IRE003-VENTES1        TO  W-VENTES1                       
                                                MOIS-VENTES1                    
              END-IF                                                            
           END-IF.                                                              
           WRITE ENR-FEXTRAC          FROM DSECT-IRE003.                        
           ADD 1                      TO CPT-FEXTRAC.                           
       FIN-PAS-DE-RUPTURE. EXIT.                                                
       ECRITURE-RELIQUAT  SECTION.                                              
           WRITE ENR-FRE003R          FROM DSECT-IRE003.                        
           ADD 1                      TO CPT-FRE003R.                           
       FIN-ECRITURE-RELIQUAT.                                                   
      *--------------------------------------------------------*                
      * IL Y A CHANGEMENT DE MOIS DANS LA SEMAINE OU IL S'AGIT                  
      * D'UNE RUPTURE AU NIVEAU SUP�RIEUR (I-E SOCIETE - LIEU)                  
      *--------------------------------------------------------*                
       RUPTURE-DJOUR SECTION.                                                   
           PERFORM TRAITEMENT-CUMUL UNTIL FIN-FCUMUL OR                         
                                          CUMUL-CLE > SAUV-CLEFIN.              
           IF NO-TROUVE-CUMUL                                                   
              PERFORM ECRITURE-NOUVEAU-FCUMUL                                   
           END-IF.                                                              
           INITIALIZE ZONES-MOIS.                                               
           MOVE IRE003-DJOUR   TO SAUV-DJOUR FIN-DJOUR.                         
           MOVE IRE003-CUMUL   TO SAUV-CUMUL.                                   
           MOVE SPACES         TO W-FIN-MOIS                                    
           SET NO-TROUVE-CUMUL TO TRUE.                                         
           SET MAG-AVEC-VENTE  TO  TRUE.                                        
       FIN-RUPTURE-DJOUR. EXIT.                                                 
      *--------------------------------------------------------*                
      * RUPTURE SUR SOCIETE                                                     
      *--------------------------------------------------------*                
       TRAITEMENT-CUMUL   SECTION.                                              
           IF CUMUL-NSOCIETE =  SAUV-NSOCIETE AND                               
              CUMUL-NLIEU    =  SAUV-NLIEU    AND                               
              AU-MOINS-UNE-DATE                                                 
              PERFORM CUMUL-FCUMUL                                              
           ELSE                                                                 
              PERFORM TRAITEMENT-SANS-VENTE UNTIL FIN-FCUMUL OR                 
                                        CUMUL-CLE NOT < SAUV-CLEFIN             
           END-IF.                                                              
       FIN-TRAITEMENT-CUMUL. EXIT.                                              
      *------------------------------------------------------*                  
      * ON CUMULE TANT QU'IL S'AGIT DE L'EXERCICE TRAITE                        
      *------------------------------------------------------*                  
       CUMUL-FCUMUL SECTION.                                                    
           IF (CUMUL-EXERCICE NOT < DATEDEB    AND                              
              CUMUL-EXERCICE NOT > DATEFIN ) OR                                 
              CUMUL-EXERCICE = W-FIN-MOIS                                       
              IF CUMUL-EXERCICE NOT = W-FIN-MOIS                                
                 ADD  CUMUL-VAL-FORC-M     TO  W-VAL-FORC-M                     
                 ADD  CUMUL-VAL-FORC-P     TO  W-VAL-FORC-P                     
                 ADD  CUMUL-VAL-REM        TO  W-VAL-REM                        
                 ADD  CUMUL-VENTES         TO  W-VENTES1                        
              END-IF                                                            
              SET TROUVE-CUMUL          TO  TRUE                                
              IF CUMUL-EXERCICE = SAUV-DJOUR AND                                
                 SAUV-CUMUL NOT = 'O'                                           
                 ADD MOIS-VAL-FORC-M     TO    CUMUL-VAL-FORC-M                 
                 ADD MOIS-VAL-FORC-P     TO    CUMUL-VAL-FORC-P                 
                 ADD MOIS-VAL-REM        TO    CUMUL-VAL-REM                    
                 ADD MOIS-VENTES1        TO    CUMUL-VENTES                     
                 INITIALIZE ZONES-MOIS                                          
              END-IF                                                            
           END-IF                                                               
           PERFORM  ECRITURE-FCUMUL-NORMAL                                      
           PERFORM LECTURE-FCUMUL.                                              
       FIN-CUMUL-FCUMUL. EXIT.                                                  
       TRAITEMENT-SANS-VENTE SECTION.                                           
           MOVE CUMUL-NLIEU    TO SAUV-CUMUL-NLIEU                              
           MOVE CUMUL-NSOCIETE TO SAUV-CUMUL-NSOCIETE                           
           MOVE CUMUL-CGRPMAG  TO SAUV-CUMUL-CGRPMAG                            
           PERFORM CUMUL-SANS-VENTE UNTIL FIN-FCUMUL OR                         
                               CUMUL-NSOCIETE NOT = SAUV-CUMUL-NSOCIETE         
                           OR  CUMUL-NLIEU    NOT = SAUV-CUMUL-NLIEU            
           IF MAG-SANS-VENTE                                                    
              PERFORM ECRITURE-CUMUL-SANS                                       
              INITIALIZE ZONES-SANS                                             
              SET MAG-AVEC-VENTE        TO  TRUE                                
           END-IF.                                                              
       FIN-TRAITEMENT-SANS-VENTE. EXIT.                                         
       CUMUL-SANS-VENTE   SECTION.                                              
           IF CUMUL-EXERCICE NOT < DATEDEB    AND                               
              CUMUL-EXERCICE NOT > DATEFIN                                      
              ADD  CUMUL-VAL-FORC-M     TO  SANS-VAL-FORC-M                     
              ADD  CUMUL-VAL-FORC-P     TO  SANS-VAL-FORC-P                     
              ADD  CUMUL-VAL-REM        TO  SANS-VAL-REM                        
              ADD  CUMUL-VENTES         TO  SANS-VENTES1                        
              SET MAG-SANS-VENTE        TO  TRUE                                
           END-IF                                                               
           PERFORM  ECRITURE-FCUMUL-NORMAL                                      
           PERFORM LECTURE-FCUMUL.                                              
       FIN-CUMUL-SANS-VENTE.                                                    
       RUPTURE-NLIEU  SECTION.                                                  
           PERFORM RUPTURE-DJOUR.                                               
           IF AU-MOINS-UNE-DATE                                                 
              PERFORM ECRITURE-CUMUL-FRE003                                     
           END-IF                                                               
           INITIALIZE ZONES-CUMULS.                                             
       FIN-RUPTURE-NLIEU. EXIT.                                                 
       ECRITURE-FCUMUL-NORMAL SECTION.                                          
           WRITE ENR-CUMULS   FROM DSECT-IRECUM.                                
           ADD 1                 TO CPT-FCUMULS.                                
       FIN-ECRITURE-FCUMUL-NORMAL. EXIT.                                        
       ECRITURE-NOUVEAU-FCUMUL SECTION.                                         
           MOVE  SAUV-NSOCIETE   TO Z-CUMUL-NSOCIETE                            
           MOVE  SAUV-NLIEU      TO Z-CUMUL-NLIEU                               
           MOVE  SAUV-CGRPMAG    TO Z-CUMUL-CGRPMAG                             
           MOVE  SAUV-DJOUR      TO Z-CUMUL-EXERCICE                            
           MOVE  MOIS-VAL-FORC-M TO Z-CUMUL-VAL-FORC-M                          
           MOVE  MOIS-VAL-FORC-P TO Z-CUMUL-VAL-FORC-P                          
           MOVE  MOIS-VAL-REM    TO Z-CUMUL-VAL-REM                             
           MOVE  MOIS-VENTES1    TO Z-CUMUL-VENTES                              
           WRITE ENR-CUMULS   FROM Z-DSECT-IRECUM.                              
           ADD 1                 TO CPT-FCUMULS.                                
       FIN-ECRITURE-NOUVEAU-FCUMUL. EXIT.                                       
       ECRITURE-CUMUL-FRE003 SECTION.                                           
           MOVE  SPACES          TO IRE003-JOUR.                                
           MOVE  SAUV-NSOCIETE   TO IRE003-NSOCIETE.                            
           MOVE  SAUV-NLIEU      TO IRE003-NLIEU.                               
           MOVE  SAUV-CGRPMAG    TO IRE003-CGRPMAG.                             
           MOVE  '9'             TO IRE003-NJOUR.                               
           MOVE  0               TO IRE003-VAL-FORC-M                           
           MOVE  0               TO IRE003-VAL-FORC-P                           
           MOVE  0               TO IRE003-VAL-REM                              
           MOVE  0               TO IRE003-VENTES1                              
           MOVE  W-VAL-FORC-M    TO IRE003-TOT-VAL-FORC-M                       
           MOVE  W-VAL-FORC-P    TO IRE003-TOT-VAL-FORC-P                       
           MOVE  W-VAL-REM       TO IRE003-TOT-VAL-REM                          
           MOVE  W-VENTES1       TO IRE003-TOT-VENTES2                          
           WRITE ENR-FEXTRAC   FROM DSECT-IRE003.                               
       FIN-ECRITURE-CUMUL-FRE003. EXIT.                                         
       ECRITURE-CUMUL-SANS SECTION.                                             
           MOVE  SPACES                   TO IRE003-JOUR.                       
           MOVE  SAUV-CUMUL-NSOCIETE      TO IRE003-NSOCIETE.                   
           MOVE  SAUV-CUMUL-NLIEU         TO IRE003-NLIEU.                      
           MOVE  SAUV-CUMUL-CGRPMAG       TO IRE003-CGRPMAG.                    
           MOVE  '9'                      TO IRE003-NJOUR.                      
           MOVE  0                        TO IRE003-VAL-FORC-M                  
           MOVE  0                        TO IRE003-VAL-FORC-P                  
           MOVE  0                        TO IRE003-VAL-REM                     
           MOVE  0                        TO IRE003-VENTES1                     
           MOVE  SANS-VAL-FORC-M          TO IRE003-TOT-VAL-FORC-M              
           MOVE  SANS-VAL-FORC-P          TO IRE003-TOT-VAL-FORC-P              
           MOVE  SANS-VAL-REM             TO IRE003-TOT-VAL-REM                 
           MOVE  SANS-VENTES1             TO IRE003-TOT-VENTES2                 
           WRITE ENR-FEXTRAC   FROM DSECT-IRE003.                               
       FIN-ECRITURE-CUMUL-SANS. EXIT.                                           
      * -------------------- LECTURE FICHIER FRE003 HEBDO----------- *  00001950
                                                                        00001960
       LECTURE-FRE003   SECTION.                                        00001970
                                                                        00001980
           READ FRE003  INTO DSECT-IRE003  AT END                       00001990
                SET FIN-FRE003  TO TRUE                                 00002000
           END-READ.                                                    00002010
           IF PAS-FIN-FRE003                                                    
              ADD 1           TO CPT-FRE003                                     
              IF IRE003-DJOUR = SPACES OR LOW-VALUE                             
                 MOVE DATEDEB  TO IRE003-DJOUR                                  
              END-IF                                                            
           ELSE                                                                 
              MOVE HIGH-VALUE TO SAUV-CLEFIN                                    
           END-IF.                                                              
                                                                        00002030
       FIN-LECTURE-FRE003. EXIT.                                        00002040
      * -------------------- LECTURE FICHIER FCUMUL ANNUEL---------- *  00001950
                                                                        00001960
       LECTURE-FCUMUL   SECTION.                                        00001970
                                                                        00001980
           READ FCUMUL  INTO DSECT-IRECUM AT END                        00001990
                SET FIN-FCUMUL  TO TRUE                                 00002000
           END-READ.                                                    00002010
           IF PAS-FIN-FCUMUL                                                    
              ADD 1      TO CPT-FCUMUL                                          
              MOVE CUMUL-NSOCIETE TO W-CUMUL-NSOCIETE                           
              MOVE CUMUL-NLIEU    TO W-CUMUL-NLIEU                              
              MOVE CUMUL-EXERCICE TO W-CUMUL-EXERCICE                           
           ELSE                                                                 
              MOVE HIGH-VALUE     TO CUMUL-CLE                                  
           END-IF.                                                              
                                                                        00002030
       FIN-LECTURE-FCUMUL. EXIT.                                        00002040
       FIN-ANORMALE SECTION.                                            00004130
                                                                        00004140
           DISPLAY '*****************************************'.         00004150
           DISPLAY '***** FIN ANORMALE DU PROGRAMME *********'.         00004160
           DISPLAY '*****************************************'.         00004170
           CLOSE FRE003 FRE003R FCUMUL FEXTRAC FCUMULS.                 00004180
           CLOSE FEXERCIC .                                                     
           MOVE 'BRE003' TO ABEND-PROG.                                 00004190
           CALL 'ABEND' USING ABEND-PROG ABEND-MESS.                    00004200
                                                                        00004210
       FIN-FIN-ANORMALE. EXIT.                                          00004220
       CALCUL-DATE-HEBDO       SECTION.                                 00040001
                                                                        00050000
           MOVE W-SSAAMMJJ TO GFSAMJ-0.                                 00060001
           MOVE '5'        TO GFDATA.                                   00080000
           CALL 'BETDATC'  USING WORK-BETDATC.                          00090000
           IF GFVDAT NOT = '1'                                          00100000
              DISPLAY 'DATE DE TRAITEMENT ANORMALE : ' W-SSAAMMJJ       00110000
              PERFORM FIN-ANORMALE                                      00120000
           ELSE                                                         00130000
              IF GFSMN = 7                                              00131001
                 MOVE W-SSAAMMJJ TO W-FIN-PERIODE                       00132001
                 MOVE '8'   TO GFDATA                                   00150001
                 CALL 'BETDATC' USING WORK-BETDATC                      00160001
                 IF GFVDAT NOT = '1'                                    00170001
                    DISPLAY 'DATE DE TRAITEMENT ANORMALE ' W-SSAAMMJJ   00180001
                    PERFORM FIN-ANORMALE                                00190001
                 ELSE                                                   00191001
                    MOVE GFSAMJ-0 TO W-DEB-PERIODE                      00192001
                 END-IF                                                 00200001
              ELSE                                                              
                 IF GFSEMNU = 1                                                 
                    SUBTRACT 6 FROM GFQNT0                                      
                    MOVE '3'   TO GFDATA                                00150   
                    CALL 'BETDATC' USING WORK-BETDATC                   00160   
                    IF GFVDAT NOT = '1'                                 00170   
                       DISPLAY 'DATE DE TRAITEMENT ANORMALE ' GFQNT0    00180   
                       PERFORM FIN-ANORMALE                             00190   
                    ELSE                                                00191   
                      MOVE '8'   TO GFDATA                                 00150
                      CALL 'BETDATC' USING WORK-BETDATC                    00160
                      IF GFVDAT NOT = '1'                                  00170
                         DISPLAY 'DATE DE TRAITEMENT ANORMALE'             00180
                         PERFORM FIN-ANORMALE                              00190
                       ELSE                                                00191
                          MOVE GFSAMJ-0 TO W-DEB-PERIODE                   00192
                       END-IF                                                   
                    END-IF                                                 00200
                 ELSE                                                           
                    MOVE '8'   TO GFDATA                                   00150
                    SUBTRACT 1 FROM    GFSEMNU                                  
                    CALL 'BETDATC' USING WORK-BETDATC                      00160
                    IF GFVDAT NOT = '1'                                    00170
                       DISPLAY 'DATE DE TRAITEMENT ANORMALE ' GFSEMNU      00180
                       PERFORM FIN-ANORMALE                                00190
                    ELSE                                                   00191
                       MOVE GFSAMJ-0 TO W-DEB-PERIODE                      00192
                    END-IF                                                 00200
                 END-IF                                                         
                 ADD      6 TO   GFQNT0                                 00140   
                 MOVE '3'   TO GFDATA                                   00150   
                 CALL 'BETDATC' USING WORK-BETDATC                      00160   
                 IF GFVDAT NOT = '1'                                    00170   
                    DISPLAY 'DATE DE TRAITEMENT ANORMALE'               00180   
                    PERFORM FIN-ANORMALE                                00190   
                 ELSE                                                   00191   
                    MOVE GFSAMJ-0 TO W-FIN-PERIODE                      00192   
                 END-IF                                                 00200   
              END-IF                                                    00201001
           END-IF.                                                      00210000
      *    IF W-MM-DEB NOT = W-MM-FIN                                           
      *       MOVE '01'      TO W-JJ-DEB                                        
      *       MOVE W-MM-FIN  TO W-MM-DEB                                        
      *       MOVE W-AA-FIN  TO W-AA-DEB                                        
      *    END-IF.                                                              
           DISPLAY 'NUM-SEM     : ' GFSEMNU.                                    
           DISPLAY 'DEB-PERIODE : ' W-DEB-PERIODE.                      00220001
           DISPLAY 'FIN-PERIODE : ' W-FIN-PERIODE.                      00221001
                                                                        00230000
       FIN-CALCUL-DATE-HEBDO. EXIT.                                     00040001
       CALCUL-DATEDEBFIN        SECTION.                                        
           MOVE W-FIN-PERIODE    TO W-EXER.                                     
           IF W-EXERMM  < EXER-PARAM                                            
              SUBTRACT 1 FROM W-EXERSSAA                                        
           END-IF                                                               
           MOVE W-EXERSSAA TO DATEDEBSSAA.                                      
           IF EXER-PARAM > 1                                                    
              ADD 1 TO W-EXERSSAA                                               
           END-IF.                                                              
           MOVE W-EXERSSAA TO DATEFINSSAA.                                      
           MOVE EXER-PARAM TO DATEDEBMM.                                        
           IF EXER-PARAM = 1                                                    
              MOVE 12 TO DATEFINMM                                              
           ELSE                                                                 
              SUBTRACT 1 FROM EXER-PARAM GIVING DATEFINMM                       
           END-IF.                                                              
           DISPLAY 'DATEDEB EXERCICE ' DATEDEB.                                 
           DISPLAY 'DATEFIN  EXERCICE ' DATEFIN.                                
       FIN-CALCUL-DATEDEBFIN. EXIT.                                             
                                                                        00004230
      * ------------------------------------------------------------ *  00003820
      *                      F I N     B R E 0 0 3                   *  00003830
      * ------------------------------------------------------------ *  00003840
                                                                        00003850
       FIN-BRE003 SECTION.                                              00003860
                                                                        00003870
           CLOSE FCUMUL FRE003R FRE003 FEXTRAC FCUMULS.                 00003920
           CLOSE FEXERCIC .                                                     
           PERFORM COMPTE-RENDU.                                        00003930
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00003940
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        00003950
       FIN-FIN-BRE003. EXIT.                                            00003960
                                                                        00003970
      * ------------------------ COMPTE-RENDU ---------------------- *  00003980
                                                                        00003990
       COMPTE-RENDU SECTION.                                            00004000
                                                                        00004010
           DISPLAY '**'.                                                00004020
           DISPLAY '**          B R E 0 0 3 '.                          00004030
           DISPLAY '**'.                                                00004040
           DISPLAY '** NOMBRE DE LECTURES FCUMUL   : '  CPT-FCUMUL.     00004050
           DISPLAY '** NOMBRE DE LECTURES FRE003   : '  CPT-FRE003.     00004050
           DISPLAY '** NOMBRE D''ECRITURES FRE003R : '  CPT-FRE003R.    00004050
           DISPLAY '** NOMBRE D''ECRITURES FEXTRAC : ' CPT-FEXTRAC.     00004060
           DISPLAY '** NOMBRE D''ECRITURES FCUMULS : ' CPT-FCUMULS.     00004060
           DISPLAY '**'.                                                00004070
                                                                        00004080
       FIN-COMPTE-RENDU. EXIT.                                          00004090
