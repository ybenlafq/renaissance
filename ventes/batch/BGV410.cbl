      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00010000
       PROGRAM-ID.                  BGV410                              00020000
       AUTHOR.    DSA017.                                               00030000
      *------------------------------------------------------------*    00040000
      *                PROGRAMME D'EDITION DE L'ETAT               *    00050000
      *                      DES VENTES PAR SAV                    *    00060001
      *                                                            *    00080000
      * DATE CREATION : 21/03/90                                   *    00090000
      *------------------------------------------------------------*    00100000
       ENVIRONMENT DIVISION.                                            00110000
       CONFIGURATION SECTION.                                           00120000
       SPECIAL-NAMES.                                                   00130000
           DECIMAL-POINT IS COMMA.                                      00140000
       INPUT-OUTPUT SECTION.                                            00150000
       FILE-CONTROL.                                                    00160000
      *                                                                 00170000
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FMOIS  ASSIGN TO FMOIS.                               00180001
      *--                                                                       
           SELECT FMOIS  ASSIGN TO FMOIS                                        
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00190000
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FGV410 ASSIGN TO FGV410.                              00200001
      *--                                                                       
           SELECT FGV410 ASSIGN TO FGV410                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00210001
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IGV410 ASSIGN TO IGV410.                              00220000
      *--                                                                       
           SELECT IGV410 ASSIGN TO IGV410                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00230000
       DATA DIVISION.                                                   00240000
       FILE SECTION.                                                    00250000
      *                                                                 00260000
      *---------------------------------------------------------------* 00270000
      *  D E F I N I T I O N    F I C H I E R    F G V 4 1 0          * 00280000
      *---------------------------------------------------------------* 00290000
      *                                                                 00300000
       FD  FGV410                                                       00310000
           RECORDING F                                                  00320000
           BLOCK 0 RECORDS                                              00330000
           LABEL RECORD STANDARD.                                       00340000
       01  ENR-FGV410.                                                  00350000
           03  FILLER      PIC  X(66).                                  00360000
      *---------------------------------------------------------------* 00361001
      *  D E F I N I T I O N    F I C H I E R    F D A T E            * 00362001
      *---------------------------------------------------------------* 00363001
      *                                                                 00364001
       FD  FMOIS                                                        00365001
           RECORDING F                                                  00366001
           BLOCK 0 RECORDS                                              00367001
           LABEL RECORD STANDARD.                                       00368001
       01  ENR-FMOIS.                                                   00369001
           03  FILLER      PIC  X(80).                                  00369101
      *                                                                 00370000
      *---------------------------------------------------------------* 00380000
      *  D E F I N I T I O N    F I C H I E R    I G V 4 1 0          * 00390000
      *---------------------------------------------------------------* 00400000
      *                                                                 00410000
       FD  IGV410                                                       00420000
           BLOCK 0 RECORDS                                              00430000
           LABEL RECORD STANDARD.                                       00440000
       01  LIGNE-IGV410 PIC X(132).                                     00450000
      *                                                                 00460000
      *---------------------------------------------------------------* 00600000
      *                 W O R K I N G  -  S T O R A G E               * 00610000
      *---------------------------------------------------------------* 00620000
      *                                                                 00630000
       WORKING-STORAGE SECTION.                                         00640000
      *                                                                 00650000
        COPY ABENDCOP.                                                  00660000
           EJECT                                                        00670000
      *                                                                 00680000
        COPY WORKDATC.                                                  00690000
           EJECT                                                        00700000
      *                                                                 00710000
      *---------------------------------------------------------------* 00720000
      *           D E S C R I P T I O N    D E S    Z O N E S         * 00730000
      *        S P E C I F I Q U E S    A U    P R O G R A M M E      * 00740000
      *---------------------------------------------------------------* 00750000
      *                                                                 00760000
       01  FGV410-ENREG.                                                00770000
           03  FGV410-CSECT           PIC X(05).                        00780000
           03  FGV410-CZONESAV        PIC X(05).                        00790000
           03  FGV410-LZONESAV        PIC X(20).                        00800000
           03  FGV410-LFAM            PIC X(20).                        00810000
           03  FGV410-WSEQFAM         PIC S9(5)   COMP-3.               00820000
           03  FGV410-NAGREGAT        PIC X(02).                        00830000
           03  FGV410-QMVT            PIC S9(9)   COMP-3.               00840001
           03  FGV410-QCA             PIC S9(9)V99 COMP-3.              00850001
      *                                                                 00980000
       01  COMPTEURS.                                                   00990000
           03  CTR-FGV410             PIC  S9(05) COMP-3 VALUE -1.      01000000
           03  NOMBRE                 PIC  ZZZZ9.                       01010000
           03  CTR-LIGNES             PIC  9(02) COMP-3 VALUE 99.       01020000
           03  CTR-PAGES              PIC  9(04) COMP-3 VALUE 0.        01030000
      *                                                                 01040000
       01  W-DATE.                                                      01080000
           03  DATAMJ.                                                  01090000
               06  AA PIC 99.                                           01100000
               06  MM PIC 99.                                           01110000
               06  JJ PIC 99.                                           01120000
           03  DATMSA.                                                  01130000
               06  MM PIC 99.                                           01140000
               06  SS PIC 99.                                           01150000
               06  AA PIC 99.                                           01160000
           03  DATEJMA.                                                 01170001
               06  JJ PIC 99.                                           01170401
               06  FILLER PIC X VALUE '/'.                              01170501
               06  MM PIC 99.                                           01170601
               06  FILLER PIC X VALUE '/'.                              01170701
               06  AA PIC 99.                                           01170801
           03  DATSAMJ.                                                 01171001
               06  SS PIC 99.                                           01180000
               06  AA PIC 99.                                           01190000
               06  MM PIC 99.                                           01200000
               06  JJ PIC 99 VALUE 01.                                  01210000
      *                                                                 01220000
       01  CALCUL.                                                      01460001
           03  W-QMVT     PIC  S9(5)  COMP-3.                           01470001
           03  W-QCA      PIC  S9(11) COMP-3.                           01480001
           03  W-STQMVT   PIC  S9(5)  COMP-3.                           01480101
           03  W-STQCA    PIC  S9(11) COMP-3.                           01480401
           03  W-TQMVT    PIC  S9(5)  COMP-3.                           01480501
           03  W-TQCA     PIC  S9(11) COMP-3.                           01481001
           03  W-SQMVT    PIC  S9(5)  COMP-3.                           01482013
           03  W-SQCA     PIC  S9(11) COMP-3.                           01483013
      *                                                                 01490001
       01  ZONE-PRECEDENTE.                                             01500001
           03  W-CSECT    PIC  X(5).                                    01510001
           03  W-CZONESAV PIC  X(5).                                    01520001
           03  W-NAGREGAT PIC  XX.                                      01530001
      *                                                                 01590000
       01  FLAG-FICHIER PIC X VALUE SPACE.                              01600000
           88  EOF      VALUE 'F'.                                      01610000
      *                                                                 01610101
       01  FLAG-AGREGAT PIC X VALUE SPACE.                              01610201
           88  AGREGAT      VALUE 'O'.                                  01610301
           88  NON-AGREGAT  VALUE 'N'.                                  01610401
      *                                                                 01611001
       01  FLAG-TOTAL   PIC X VALUE 'O'.                                01612001
           88  NON-TOTAL VALUE 'N'.                                     01613001
           88      TOTAL VALUE 'O'.                                     01614001
      *                                                                 01620001
       01  FLAG-TRAIT   PIC X VALUE 'O'.                                01630001
           88  NON-TRAIT VALUE 'N'.                                     01640001
           88      TRAIT VALUE 'O'.                                     01650001
      *                                                                 01660001
       01  L-IGV410.                                                    01670001
      ***************************************************************** 01680001
      *  ENTETE PHYSIQUE DE PAGE                                        01690001
      ***************************************************************** 01700001
      ***************************************************************** 01710001
          02 ENTETE01.                                                  01720001
            04 FILLER PIC X(20) VALUE SPACES.                           01730001
            04 FILLER PIC X(20) VALUE SPACES.                           01740001
            04 FILLER PIC X(20) VALUE '       E T A B L I S'.           01750001
            04 FILLER PIC X(20) VALUE ' S E M E N T S    D '.           01760001
            04 FILLER PIC X(20) VALUE 'A R T Y             '.           01770001
            04 FILLER PIC X(20) VALUE SPACES.                           01780001
            04 FILLER PIC X(12) VALUE SPACES.                           01790001
            04 FILLER PIC X(01) VALUE SPACES.                           01800001
      ***************************************************************** 01810001
          02 ENTETE02.                                                  01820001
            04 FILLER PIC X(20) VALUE SPACES.                           01830001
            04 FILLER PIC X(20) VALUE SPACES.                           01840001
            04 FILLER PIC X(20) VALUE '        ------------'.           01850001
            04 FILLER PIC X(20) VALUE '--------------------'.           01860001
            04 FILLER PIC X(20) VALUE '------              '.           01870001
            04 FILLER PIC X(20) VALUE SPACES.                           01880001
            04 FILLER PIC X(7) VALUE ' PAGE :'.                         01890001
            04 FILLER PIC X(01) VALUE SPACES.                           01900001
            04 Z-NPAGE PIC ZZ9 .                                        01910001
            04 FILLER PIC X(01) VALUE SPACES.                           01920001
      ***************************************************************** 01930001
          02 ENTETE03.                                                  01940001
            04 FILLER PIC X(14) VALUE 'SECTEUR      :'.                 01950001
            04 FILLER PIC X(01) VALUE SPACES.                           01960001
            04 Z-CSECTEUR PIC X(5).                                     01970001
            04 FILLER PIC X(01) VALUE SPACES.                           01980001
            04 FILLER PIC X(20) VALUE SPACES.                           01990001
            04 FILLER PIC X(20) VALUE '       ! SUIVI MENSU'.           02000001
            04 FILLER PIC X(20) VALUE 'EL DES VENTES  PAR  '.           02010001
            04 FILLER PIC X(20) VALUE 'SAV !               '.           02020001
            04 FILLER PIC X(13) VALUE SPACES.                           02030001
            04 FILLER PIC X(08) VALUE 'MOIS DE '.                       02040001
            04 Z-MOIS PIC X(10).                                        02041001
            04 FILLER PIC X(01) VALUE SPACES.                           02050001
      ***************************************************************** 02060001
          02 ENTETE04.                                                  02070001
            04 FILLER PIC X(14) VALUE 'LE           :'.                 02080001
            04 FILLER PIC X(01) VALUE SPACES.                           02090001
            04 Z-DATE PIC X(8).                                         02100001
            04 FILLER PIC X(01) VALUE SPACES.                           02110001
            04 FILLER PIC X(20) VALUE SPACES.                           02120001
            04 FILLER PIC X(20) VALUE '    ----------------'.           02130001
            04 FILLER PIC X(20) VALUE '--------------------'.           02140001
            04 FILLER PIC X(20) VALUE '--                  '.           02150001
            04 FILLER PIC X(20) VALUE SPACES.                           02160001
            04 FILLER PIC X(8) VALUE SPACES.                            02170001
            04 FILLER PIC X(01) VALUE SPACES.                           02180001
      ***************************************************************** 02180101
          02 ENTETE05.                                                  02181001
            04 FILLER PIC X(132) VALUE SPACES.                          02182001
      ***************************************************************** 02190001
          02 ENTETE06.                                                  02200001
            04 FILLER PIC X(20) VALUE '--------------------'.           02210001
            04 FILLER PIC X(20) VALUE '--------------------'.           02220001
            04 FILLER PIC X(20) VALUE '--------------------'.           02230001
            04 FILLER PIC X(20) VALUE '--------------------'.           02240001
            04 FILLER PIC X(20) VALUE '--------------------'.           02250001
            04 FILLER PIC X(20) VALUE '--------------------'.           02260001
            04 FILLER PIC X(12) VALUE '------------'.                   02270001
            04 FILLER PIC X(01) VALUE SPACES.                           02280001
      ***************************************************************** 02290001
          02 ENTETE07.                                                  02300001
            04 FILLER PIC X(20) VALUE '!                S A'.           02310001
            04 FILLER PIC X(20) VALUE ' V                ! '.           02320001
            04 FILLER PIC X(20) VALUE '                 FAM'.           02330001
            04 FILLER PIC X(20) VALUE 'ILLE                '.           02340001
            04 FILLER PIC X(20) VALUE '   !          NB    '.           02350001
            04 FILLER PIC X(20) VALUE '                 CA '.           02360001
            04 FILLER PIC X(12) VALUE '           !'.                   02370001
            04 FILLER PIC X(01) VALUE SPACES.                           02380001
      ***************************************************************** 02390001
          02 ENTETE08.                                                  02400001
            04 FILLER PIC X(20) VALUE '--------------------'.           02410001
            04 FILLER PIC X(20) VALUE '--------------------'.           02420001
            04 FILLER PIC X(20) VALUE '--------------------'.           02430001
            04 FILLER PIC X(20) VALUE '--------------------'.           02440001
            04 FILLER PIC X(20) VALUE '--------------------'.           02450001
            04 FILLER PIC X(20) VALUE '--------------------'.           02460001
            04 FILLER PIC X(12) VALUE '------------'.                   02470001
            04 FILLER PIC X(01) VALUE SPACES.                           02480001
      ***************************************************************** 02490001
          02 DETAIL01.                                                  02500001
            04 FILLER PIC X(8) VALUE '!       '.                        02510001
            04 FILLER PIC X(01) VALUE SPACES.                           02520001
            04 Z-LZONESAV PIC X(20).                                    02530001
            04 FILLER PIC X(01) VALUE SPACES.                           02540001
            04 FILLER PIC X(20) VALUE '        !           '.           02550001
            04 FILLER PIC X(01) VALUE SPACES.                           02560001
            04 Z-LFAM PIC X(20).                                        02570001
            04 FILLER PIC X(01) VALUE SPACES.                           02580001
            04 FILLER PIC X(18) VALUE '           !      '.             02590001
            04 FILLER PIC X(01) VALUE SPACES.                           02600001
            04 Z-QMVT PIC Z(7)9.                                        02610001
            04 P-QMVT REDEFINES Z-QMVT PIC -(7)9.                       02611001
            04 FILLER PIC X(01) VALUE SPACES.                           02620001
            04 FILLER PIC X(11) VALUE SPACES.                           02630001
            04 FILLER PIC X(01) VALUE SPACES.                           02640001
            04 Z-QCA PIC Z(10)9.                                        02650001
            04 P-QCA  REDEFINES Z-QCA  PIC -(10)9.                      02651001
            04 FILLER PIC X(01) VALUE SPACES.                           02660001
            04 FILLER PIC X(8) VALUE '       !'.                        02670001
            04 FILLER PIC X(01) VALUE SPACES.                           02680001
      ***************************************************************** 02690001
          02 DETAIL02.                                                  02700001
            04 FILLER PIC X(20) VALUE '!                   '.           02710001
            04 FILLER PIC X(20) VALUE '                  ! '.           02720001
            04 FILLER PIC X(20) VALUE SPACES.                           02730001
            04 FILLER PIC X(20) VALUE SPACES.                           02740001
            04 FILLER PIC X(20) VALUE '   !----------------'.           02750001
            04 FILLER PIC X(20) VALUE '--------------------'.           02760001
            04 FILLER PIC X(12) VALUE '-----------!'.                   02770001
            04 FILLER PIC X(01) VALUE SPACES.                           02780001
      ***************************************************************** 02790001
          02 DETAIL03.                                                  02800001
            04 FILLER PIC X(20) VALUE '!                   '.           02810001
            04 FILLER PIC X(20) VALUE '                  ! '.           02820001
            04 FILLER PIC X(20) VALUE '                 SOU'.           02830001
            04 FILLER PIC X(20) VALUE 'S-TOTAL    :        '.           02840001
            04 FILLER PIC X(10) VALUE '   !      '.                     02850001
            04 FILLER PIC X(01) VALUE SPACES.                           02860001
            04 Z-STQMVT PIC Z(7)9.                                      02870001
            04 P-STQMVT REDEFINES Z-STQMVT  PIC -(7)9.                  02871001
            04 FILLER PIC X(01) VALUE SPACES.                           02880001
            04 FILLER PIC X(11) VALUE SPACES.                           02890001
            04 FILLER PIC X(01) VALUE SPACES.                           02900001
            04 Z-STQCA  PIC Z(10)9.                                     02910001
            04 P-STQCA  REDEFINES Z-STQCA   PIC -(10)9.                 02911001
            04 FILLER PIC X(01) VALUE SPACES.                           02920001
            04 FILLER PIC X(8) VALUE '       !'.                        02930001
            04 FILLER PIC X(01) VALUE SPACES.                           02940001
      ***************************************************************** 02950001
          02 DETAIL04.                                                  02960001
            04 FILLER PIC X(20) VALUE '!                   '.           02970001
            04 FILLER PIC X(20) VALUE '                  !-'.           02980001
            04 FILLER PIC X(20) VALUE '--------------------'.           02990001
            04 FILLER PIC X(20) VALUE '--------------------'.           03000001
            04 FILLER PIC X(20) VALUE '---!----------------'.           03010001
            04 FILLER PIC X(20) VALUE '--------------------'.           03020001
            04 FILLER PIC X(12) VALUE '-----------!'.                   03030001
            04 FILLER PIC X(01) VALUE SPACES.                           03040001
      ***************************************************************** 03050001
          02 DETAIL05.                                                  03060001
            04 FILLER PIC X(20) VALUE '!                   '.           03070001
            04 FILLER PIC X(20) VALUE '                  ! '.           03080001
            04 FILLER PIC X(20) VALUE '          TOTAL GENE'.           03090001
            04 FILLER PIC X(20) VALUE 'RAL SAV    :        '.           03100001
            04 FILLER PIC X(10) VALUE '   !      '.                     03110001
            04 FILLER PIC X(01) VALUE SPACES.                           03120001
            04 Z-TQMVT PIC Z(7)9.                                       03130001
            04 P-TQMVT REDEFINES Z-TQMVT   PIC -(7)9.                   03131001
            04 FILLER PIC X(01) VALUE SPACES.                           03140001
            04 FILLER PIC X(11) VALUE SPACES.                           03150001
            04 FILLER PIC X(01) VALUE SPACES.                           03160001
            04 Z-TQCA PIC Z(10)9.                                       03170001
            04 P-TQCA REDEFINES Z-TQCA    PIC -(10)9.                   03171001
            04 FILLER PIC X(01) VALUE SPACES.                           03180001
            04 FILLER PIC X(8) VALUE '       !'.                        03190001
            04 FILLER PIC X(01) VALUE SPACES.                           03200001
          02 DETAIL06.                                                  03210013
            04 FILLER PIC X(20) VALUE '! TOTAL GENERAL DU S'.           03220018
            04 FILLER PIC X(20) VALUE 'ECTEUR              '.           03230020
            04 FILLER PIC X(20) VALUE '                    '.           03240018
            04 FILLER PIC X(20) VALUE '                    '.           03250018
            04 FILLER PIC X(10) VALUE '   !      '.                     03260013
            04 FILLER PIC X(01) VALUE SPACES.                           03270013
            04 Z-SQMVT PIC Z(7)9.                                       03280013
            04 P-SQMVT REDEFINES Z-SQMVT   PIC -(7)9.                   03290014
            04 FILLER PIC X(01) VALUE SPACES.                           03300013
            04 FILLER PIC X(11) VALUE SPACES.                           03310013
            04 FILLER PIC X(01) VALUE SPACES.                           03320013
            04 Z-SQCA PIC Z(10)9.                                       03330013
            04 P-SQCA REDEFINES Z-SQCA    PIC -(10)9.                   03340014
            04 FILLER PIC X(01) VALUE SPACES.                           03350013
            04 FILLER PIC X(8) VALUE '       !'.                        03360013
            04 FILLER PIC X(01) VALUE SPACES.                           03370013
       PROCEDURE DIVISION.                                              05370000
      ***************************************************************** 05380000
      *              T R A M E   DU   P R O G R A M M E               * 05390000
      ***************************************************************** 05400000
      *                                                               * 05410000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 05420000
      *                                                               * 05430000
      *           ------------------------------------------          * 05440000
      *           --  MODULE DE BASE DU PROGRAMME  BGV410 --          * 05450000
      *           ------------------------------------------          * 05460000
      *                               I                               * 05470000
      *           -----------------------------------------           * 05480000
      *           I                   I                   I           * 05490000
      *   -----------------   -----------------   -----------------   * 05500000
      *   -    DEBUT      -   -  TRAITEMENT   -   -      FIN      -   * 05510000
      *   -----------------   -----------------   -----------------   * 05520000
      *                                                               * 05530000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 05540000
      *                                                                 05550000
       MODULE-BGV410.                                                   05560000
      *                                                                 05570000
           PERFORM  DEBUT-BGV410.                                       05580000
           PERFORM  TRAIT-BGV410.                                       05590000
           PERFORM  FIN-BGV410.                                         05600000
      *                                                                 05610000
       F-MODULE-BGV410. EXIT.                                           05620000
      *                                                                 05630000
           EJECT                                                        05640000
      *                                                                 05650000
      *---------------------------------------------------------------* 05660000
      *                    D E B U T        B G V 4 1 0               * 05670001
      *---------------------------------------------------------------* 05680000
      *                                                                 05690000
       DEBUT-BGV410.                                                    05700000
      *                                                                 05710000
           PERFORM OUVERTURE-FICHIERS.                                  05720000
           PERFORM TRAIT-DATE.                                          05730000
           PERFORM INITIALISATION-ZONES.                                05731001
      *                                                                 05740000
       F-DEBUT-BGV410.                                                  05750000
           EXIT.                                                        05760000
      *                                                                 05770000
           EJECT                                                        05780000
      *                                                                 05790000
      *---------------------------------------------------------------* 05800000
      *                    T R A I T        B G V 4 1 0               * 05810001
      *---------------------------------------------------------------* 05820000
      *                                                                 05830000
       TRAIT-BGV410.                                                    05840000
      *                                                                 05850000
           PERFORM LECTURE-FGV410.                                      05860001
           PERFORM UNTIL EOF                                            05870000
              MOVE FGV410-CSECT           TO W-CSECT                    05880001
              PERFORM ECRITURE-DETAIL0                                  05881001
              PERFORM ECRITURE-DETAIL1                                  05882001
              PERFORM UNTIL W-CSECT       NOT = FGV410-CSECT OR EOF     05890001
                 MOVE FGV410-CZONESAV     TO W-CZONESAV                 05900001
                 MOVE FGV410-NAGREGAT     TO W-NAGREGAT                 05900101
                 MOVE FGV410-CSECT        TO W-CSECT                    05900201
                 PERFORM LECTURE-FGV410                                 05901001
                 SET NON-AGREGAT TO TRUE                                05910001
                 PERFORM UNTIL W-CZONESAV NOT = FGV410-CZONESAV OR      05930001
                               W-CSECT    NOT = FGV410-CSECT    OR      05940001
                               EOF                                      05941001
                    IF W-NAGREGAT NOT = FGV410-NAGREGAT AND NON-AGREGAT 05941101
                       PERFORM ECRITURE-DETAIL2                         05941201
                    END-IF                                              05941301
                    IF W-NAGREGAT = FGV410-NAGREGAT OR NON-AGREGAT      05942001
                       PERFORM ECRITURE-DETAIL1                         05951001
                    END-IF                                              05951101
                    MOVE FGV410-NAGREGAT     TO W-NAGREGAT              05951501
                    MOVE FGV410-CZONESAV     TO W-CZONESAV              05951601
                    MOVE FGV410-CSECT        TO W-CSECT                 05951701
                    PERFORM LECTURE-FGV410                              05951801
                    PERFORM UNTIL W-NAGREGAT NOT = FGV410-NAGREGAT OR   05980001
                                  W-CZONESAV NOT = FGV410-CZONESAV OR   05981001
                                  W-CSECT    NOT = FGV410-CSECT    OR   05982001
                                  EOF                                   05983001
                       PERFORM ECRITURE-DETAIL1                         06020001
                       MOVE FGV410-NAGREGAT     TO W-NAGREGAT           06021001
                       MOVE FGV410-CZONESAV     TO W-CZONESAV           06022001
                       MOVE FGV410-CSECT        TO W-CSECT              06023001
                       PERFORM LECTURE-FGV410                           06030001
                    END-PERFORM                                         06040000
                    SET AGREGAT TO TRUE                                 06041001
                    PERFORM ECRITURE-DETAIL2                            06041101
                    IF (W-NAGREGAT NOT = FGV410-NAGREGAT) AND           06041207
                     ((W-CSECT     = FGV410-CSECT ) AND                 06041410
                      (W-CZONESAV  = FGV410-CZONESAV))                  06041509
                      PERFORM ECRITURE-DETAIL1                          06041607
                    END-IF                                              06041707
                 END-PERFORM                                            06060000
                 IF NON-AGREGAT                                         06061001
                    PERFORM ECRITURE-DETAIL2                            06061101
                 END-IF                                                 06062001
                 PERFORM ECRITURE-DETAIL4                               06070001
                 IF                                                     06070111
                    (W-CZONESAV NOT = FGV410-CZONESAV) AND              06070206
                    (W-CSECT     = FGV410-CSECT )                       06070302
                    PERFORM ECRITURE-DETAIL1                            06070402
                 END-IF                                                 06070502
              END-PERFORM                                               06080000
              PERFORM ECRITURE-DETAIL6                                  06081018
           END-PERFORM.                                                 06090000
      *                                                                 06100000
       F-TRAIT-BGV410.   EXIT.                                          06110000
      *                                                                 06120000
           EJECT                                                        06130000
      *                                                                 06140000
      *---------------------------------------------------------------* 06150000
      *                   F I N        B G V 4 1 0                    * 06160001
      *---------------------------------------------------------------* 06170000
      *                                                                 06180000
       FIN-BGV410.                                                      06190000
      *                                                                 06200000
           IF CTR-FGV410  > 0 AND CTR-LIGNES NOT = 10                   06210001
              PERFORM ECRITURE-FINPAG                                   06230001
           END-IF.                                                      06240000
           CLOSE FGV410 IGV410 FMOIS.                                   06250001
           MOVE CTR-FGV410  TO NOMBRE.                                  06260001
           DISPLAY '**********************************************'.    06270000
           DISPLAY '*    PROGRAMME BGV410  TERMINE NORMALEMENT   *'.    06280000
           DISPLAY '*                                            *'.    06290000
           DISPLAY '*  NOMBRE D''ENREG LUS SUR FGV410    = ' NOMBRE     06300001
                   '  *'.                                               06310000
           DISPLAY '*                                            *'.    06320000
           DISPLAY '**********************************************'.    06330000
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    06340000
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                 06350000
       F-FIN-BGV410.                                                    06360000
           EXIT.                                                        06370000
      *                                                                 06380000
           EJECT                                                        06390000
      *                                                                 06400000
      *---------------------------------------------------------------* 06410000
      *         M O D U L E S    D E B U T  B G V 4 1 0               * 06420001
      *---------------------------------------------------------------* 06430000
      *                                                                 06440000
       OUVERTURE-FICHIERS.                                              06450000
      *-------------------                                              06460000
      *                                                                 06470000
           OPEN INPUT FMOIS FGV410.                                     06480001
           OPEN OUTPUT IGV410.                                          06490001
      *                                                                 06500000
       F-OUVERTURE-FICHIERS.  EXIT.                                     06510000
      *                                                                 06520000
      *                                                                 06530000
       TRAIT-DATE.                                                      06540000
      *-----------                                                      06550000
           MOVE 'FICHIER DATE VIDE' TO ABEND-MESS.                      06560000
           READ FMOIS INTO DATMSA AT END PERFORM FIN-ANORMALE.          06570000
      *                                                                 06580000
      * RECUPERATION DU LIBELLE DU MOIS                                 06590000
      *                                                                 06600000
           MOVE CORR DATMSA TO DATSAMJ.                                 06610000
           MOVE '5' TO GFDATA.                                          06620000
           MOVE DATSAMJ TO GFSAMJ-0.                                    06630000
           CALL 'BETDATC' USING WORK-BETDATC.                           06640000
           IF GFVDAT = 0                                                06650000
              MOVE 'DATE INCORRECTE ' TO ABEND-MESS                     06660000
              GO TO FIN-ANORMALE                                        06670000
           END-IF.                                                      06680000
           MOVE GFMOI-LIB-L TO Z-MOIS.                                  06690001
      *                                                                 06710000
      * RECUPERATION DE LA DATE D'EDITION                               06720000
      *                                                                 06730000
           ACCEPT DATAMJ FROM DATE.                                     06740000
           MOVE CORR DATAMJ TO DATEJMA.                                 06750001
           MOVE DATEJMA     TO Z-DATE.                                  06751001
      *                                                                 06760000
       F-TRAIT-DATE.  EXIT.                                             06770000
      *                                                                 06780000
       INITIALISATION-ZONES.                                            06782001
      *--------------------                                             06783001
      *                                                                 06790000
           MOVE 0 TO W-STQMVT                                           06790101
                     W-TQMVT                                            06790201
                     W-TQCA                                             06790301
                     W-SQMVT                                            06790415
                     W-SQCA                                             06790515
                     W-STQCA.                                           06790601
      *                                                                 06791001
       LECTURE-FGV410.                                                  06800001
      *----------------                                                 06810000
           READ FGV410 INTO FGV410-ENREG AT END SET EOF TO TRUE.        06820001
           ADD 1 TO CTR-FGV410.                                         06830001
      *                                                                 06840000
       F-LECTURE-FGV410.  EXIT.                                         06850001
      *                                                                 06860000
           EJECT                                                        06870000
      *                                                                 06880000
      *---------------------------------------------------------------* 06890000
      *         M O D U L E S    T R A I T    B G V 4 1 0             * 06900001
      *---------------------------------------------------------------* 06910000
      *                                                                 06920000
       ECRITURE-DETAIL0.                                                06930001
      *-----------------                                                06940000
           IF W-CSECT NOT = Z-CSECTEUR                                  07080001
              MOVE W-CSECT TO Z-CSECTEUR                                07110001
              PERFORM ECRITURE-ENTETE                                   07120001
           ELSE                                                         07130001
              IF CTR-LIGNES > 62                                        07131005
                 PERFORM ECRITURE-FINPAG                                07131101
                 PERFORM ECRITURE-ENTETE                                07131201
              END-IF                                                    07132001
           END-IF.                                                      07140001
       F-ECRITURE-DETAIL0.     EXIT.                                    07160001
      *                                                                 07180000
       ECRITURE-DETAIL1.                                                07181001
      *-----------------                                                07182001
           IF CTR-LIGNES > 62                                           07182105
              PERFORM ECRITURE-FINPAG                                   07182201
              PERFORM ECRITURE-ENTETE                                   07182301
           END-IF.                                                      07182401
           PERFORM TR-CALCUL.                                           07183001
           WRITE LIGNE-IGV410 FROM DETAIL01.                            07184001
           SET NON-TRAIT TO TRUE.                                       07184101
           ADD 1 TO CTR-LIGNES.                                         07185001
       F-ECRITURE-DETAIL1.     EXIT.                                    07186001
      *                                                                 07187001
       ECRITURE-DETAIL2.                                                07190000
      *-----------------                                                07200001
           IF CTR-LIGNES > 62                                           07230005
              PERFORM ECRITURE-FINPAG                                   07240001
              PERFORM ECRITURE-ENTETE                                   07250000
           END-IF.                                                      07260000
           PERFORM REMPLISSAGE-STOTAL.                                  07270001
           WRITE LIGNE-IGV410 FROM DETAIL02.                            07271001
           WRITE LIGNE-IGV410 FROM DETAIL03.                            07271101
           WRITE LIGNE-IGV410 FROM DETAIL02.                            07271201
           SET NON-TRAIT TO TRUE.                                       07271301
           PERFORM INITIALISATION-STOTAL.                               07272001
           ADD 3 TO CTR-LIGNES.                                         07280001
       F-ECRITURE-DETAIL2.     EXIT.                                    07310000
      *                                                                 07330000
      *                                                                 07520000
       ECRITURE-DETAIL4.                                                07530000
      *-----------------                                                07540000
           PERFORM REMPLISSAGE-TOTAL.                                   07611001
           WRITE LIGNE-IGV410 FROM DETAIL05.                            07620101
           WRITE LIGNE-IGV410 FROM ENTETE06.                            07620201
           SET TRAIT TO TRUE.                                           07620301
           PERFORM INITIALISATION-TOTAL.                                07621001
           IF (W-CSECT     = FGV410-CSECT )                             07621120
              IF EOF                                                    07621220
                NEXT SENTENCE                                           07621320
              ELSE                                                      07621420
                PERFORM ECRITURE-ENTETE                                 07622020
              END-IF                                                    07623020
           END-IF.                                                      07624020
      *                                                                 07720000
       F-ECRITURE-DETAIL4.     EXIT.                                    07730000
      *                                                                 07740013
       ECRITURE-DETAIL6.                                                07750013
      *-----------------                                                07760013
           PERFORM REMPLISSAGE-SECTEUR.                                 07770013
           WRITE LIGNE-IGV410 FROM DETAIL06.                            07790013
           WRITE LIGNE-IGV410 FROM ENTETE06.                            07800013
           SET TRAIT TO TRUE                                            07810020
           PERFORM INITIALISATION-SECTEUR.                              07820013
      *                                                                 07840013
       F-ECRITURE-DETAIL6.     EXIT.                                    07850014
      *                                                                 11080000
       ECRITURE-ENTETE.                                                 11090000
      *----------------                                                 11100000
           ADD 1 TO CTR-PAGES.                                          11110000
           MOVE CTR-PAGES TO Z-NPAGE.                                   11120001
           WRITE LIGNE-IGV410  FROM ENTETE01 AFTER ADVANCING PAGE.      11130001
           WRITE LIGNE-IGV410 FROM ENTETE02.                            11140001
           WRITE LIGNE-IGV410 FROM ENTETE03.                            11150001
           WRITE LIGNE-IGV410 FROM ENTETE04.                            11160001
           WRITE LIGNE-IGV410 FROM ENTETE05.                            11170001
           WRITE LIGNE-IGV410 FROM ENTETE05.                            11171001
           WRITE LIGNE-IGV410 FROM ENTETE06.                            11180001
           WRITE LIGNE-IGV410 FROM ENTETE07.                            11190001
           WRITE LIGNE-IGV410 FROM ENTETE08.                            11200001
           MOVE  10 TO CTR-LIGNES.                                      11250001
      *                                                                 11260000
       F-ECRITURE-ENTETE.  EXIT.                                        11270000
      *                                                                 11280000
       ECRITURE-FINPAG.                                                 11380000
      *----------------                                                 11390000
           IF NON-TRAIT                                                 11391001
              WRITE LIGNE-IGV410 FROM ENTETE06                          11400001
           END-IF.                                                      11401001
           ADD 1 TO CTR-LIGNES.                                         11410000
      *                                                                 11420000
       F-ECRITURE-FINPAG.  EXIT.                                        11430000
      *                                                                 11440000
       INITIALISATION-STOTAL.                                           11441001
      *---------------------                                            11442001
           MOVE 0  TO W-STQMVT.                                         11443001
           MOVE 0  TO W-STQCA.                                          11444001
      *                                                                 11445001
       INITIALISATION-TOTAL.                                            11446001
      *---------------------                                            11447001
           SET TOTAL TO TRUE.                                           11447101
           MOVE 0  TO W-TQMVT.                                          11448001
           MOVE 0  TO W-TQCA.                                           11449001
      *                                                                 11449101
       INITIALISATION-SECTEUR.                                          11449313
      *---------------------                                            11449413
           MOVE 0  TO W-SQMVT.                                          11449613
           MOVE 0  TO W-SQCA.                                           11449713
      *                                                                 11449813
       REMPLISSAGE-STOTAL.                                              11449901
      *-------------------                                              11450001
           IF W-STQMVT NOT < 0                                          11450101
              MOVE  W-STQMVT TO Z-STQMVT                                11450201
           ELSE                                                         11450301
              MOVE  W-STQMVT TO P-STQMVT                                11450401
           END-IF.                                                      11450501
      *                                                                 11450601
           IF W-STQCA  NOT < 0                                          11451001
              MOVE  W-STQCA  TO Z-STQCA                                 11452001
           ELSE                                                         11453001
              MOVE  W-STQCA  TO P-STQCA                                 11454001
           END-IF.                                                      11455001
      *                                                                 11460000
       REMPLISSAGE-TOTAL.                                               11461001
      *-------------------                                              11462001
           IF W-TQMVT NOT < 0                                           11463001
              MOVE  W-TQMVT TO Z-TQMVT                                  11464001
           ELSE                                                         11465001
              MOVE  W-TQMVT TO P-TQMVT                                  11466001
           END-IF.                                                      11467001
      *                                                                 11468001
           IF W-TQCA  NOT < 0                                           11469001
              MOVE  W-TQCA  TO Z-TQCA                                   11469101
           ELSE                                                         11469201
              MOVE  W-TQCA  TO P-TQCA                                   11469301
           END-IF.                                                      11469401
      *                                                                 11469513
       REMPLISSAGE-SECTEUR.                                             11469613
      *-------------------                                              11469713
           IF W-SQMVT NOT < 0                                           11469813
              MOVE  W-SQMVT TO Z-SQMVT                                  11469913
           ELSE                                                         11470013
              MOVE  W-SQMVT TO P-SQMVT                                  11470113
           END-IF.                                                      11470213
      *                                                                 11470313
           IF W-SQCA  NOT < 0                                           11470413
              MOVE  W-SQCA  TO Z-SQCA                                   11470513
           ELSE                                                         11470613
              MOVE  W-SQCA  TO P-SQCA                                   11470713
           END-IF.                                                      11470813
      *                                                                 11470901
       TR-CALCUL.                                                       11471001
      *-------                                                          11471101
      *                                                                 11471201
           IF TOTAL                                                     11471301
              MOVE FGV410-LZONESAV TO Z-LZONESAV                        11471401
           ELSE                                                         11471501
              MOVE SPACES          TO Z-LZONESAV                        11471601
           END-IF.                                                      11471701
           SET NON-TOTAL    TO TRUE.                                    11471801
           MOVE FGV410-LFAM TO Z-LFAM.                                  11471901
           COMPUTE W-STQMVT ROUNDED = (W-STQMVT + FGV410-QMVT).         11472001
           COMPUTE W-STQCA  ROUNDED = (W-STQCA  + FGV410-QCA).          11472101
           COMPUTE W-TQMVT  ROUNDED = (W-TQMVT  + FGV410-QMVT).         11472201
           COMPUTE W-SQCA   ROUNDED = (W-SQCA   + FGV410-QCA).          11472313
           COMPUTE W-SQMVT  ROUNDED = (W-SQMVT  + FGV410-QMVT).         11472413
           COMPUTE W-TQCA   ROUNDED = (W-TQCA   + FGV410-QCA).          11472513
           COMPUTE W-QCA    ROUNDED = FGV410-QCA.                       11472601
           COMPUTE W-QMVT   ROUNDED = FGV410-QMVT.                      11472701
           IF   W-QMVT < 0                                              11472801
                MOVE W-QMVT TO P-QMVT                                   11472901
           ELSE                                                         11473001
                MOVE W-QMVT TO Z-QMVT                                   11473101
           END-IF.                                                      11473201
           IF   W-QCA  < 0                                              11473301
                MOVE W-QCA  TO P-QCA                                    11473401
           ELSE                                                         11473501
                MOVE W-QCA  TO Z-QCA                                    11473601
           END-IF.                                                      11473701
      *                                                                 11473801
      *---------------------------------------------------------------* 11474000
      *         P A R A G R A P H E   F I N  A N O R M A L E          * 11480000
      *---------------------------------------------------------------* 11490000
      *                                                                 11500000
       FIN-ANORMALE.                                                    11510000
      *-------------                                                    11520000
           MOVE 'BGV410' TO ABEND-PROG.                                 11530000
           CALL 'ABEND' USING                                           11540000
                        ABEND-PROG                                      11550000
                        ABEND-MESS.                                     11560000
      *                                                                 11570000
       F-FIN-ANORMALE.                                                  11580000
           EXIT.                                                        11590000
