      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000010
       PROGRAM-ID.   BRE030.                                            00000020
       AUTHOR.       DSA015.                                            00000030
                                                                        00000040
      * ------------------------------------------------------------ *  00000050
      *                                                              *  00000060
      *   PROJET           :                                         *  00000070
      *   PROGRAMME        : BRE030.                                 *  00000080
      *   PERIODICITE      : MENSUEL                                 *  00000090
      *   FONCTION         : PREPARATION TABLE A LOADER MENSUEL      *  00000100
      *                    :SOC NLIEU DMVENTE VENDEUR CFAM           *  00000100
      *   DATE DE CREATION : 16/09/1992.                             *  00000110
      *                                                              *  00000120
      * ------------------------------------------------------------ *  00000130
                                                                        00000140
       ENVIRONMENT DIVISION.                                            00000150
       CONFIGURATION SECTION.                                           00000160
       INPUT-OUTPUT SECTION.                                            00000170
       FILE-CONTROL.                                                    00000180
                                                                        00000190
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  RTRE00   ASSIGN TO  RTRE00.                          00000210
      *--                                                                       
           SELECT  RTRE00   ASSIGN TO  RTRE00                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FRE030   ASSIGN TO  FRE030.                          00000210
      *--                                                                       
           SELECT  FRE030   ASSIGN TO  FRE030                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FRE031   ASSIGN TO  FRE031.                          00000210
      *--                                                                       
           SELECT  FRE031   ASSIGN TO  FRE031                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FDATE    ASSIGN TO  FDATE.                           00000210
      *--                                                                       
           SELECT  FDATE    ASSIGN TO  FDATE                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FNSOC    ASSIGN TO  FNSOC.                           00000210
      *--                                                                       
           SELECT  FNSOC    ASSIGN TO  FNSOC                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FMOIS    ASSIGN TO  FMOIS.                           00000210
      *                                                                         
      *--                                                                       
           SELECT  FMOIS    ASSIGN TO  FMOIS                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00000220
      *}                                                                        
       DATA DIVISION.                                                   00000230
       FILE SECTION.                                                    00000240
                                                                        00000250
      * ------------------------------------------------------------ *  00000260
      *               D E S C R I P T I O N   F I C H I E R          *  00000270
      * ------------------------------------------------------------ *  00000280
                                                                        00000290
       FD  RTRE00                                                       00000300
           RECORDING F                                                  00000310
           BLOCK 0 RECORDS                                              00000320
           LABEL RECORD STANDARD.                                       00000330
       01  ENR-RTRE00.                                                  00000340
           02  FILLER PIC X(48).                                        00000350
                                                                        00000360
       FD  FDATE                                                        00000300
           RECORDING F                                                  00000310
           BLOCK 0 RECORDS                                              00000320
           LABEL RECORD STANDARD.                                       00000330
       01  FILE-DATE.                                                   00000340
           03  F-DATE.                                                  00000350
               06  JJ  PIC X(02).                                               
               06  MM  PIC X(02).                                               
               06  SS  PIC X(02).                                               
               06  AA  PIC X(02).                                               
           03  FILLER  PIC X(72).                                       00000350
                                                                        00000360
       FD  FMOIS                                                        00000300
           RECORDING F                                                  00000310
           BLOCK 0 RECORDS                                              00000320
           LABEL RECORD STANDARD.                                       00000330
       01  FILE-MOIS.                                                   00000340
           03  F-MOIS.                                                  00000350
               06  M-MM  PIC X(02).                                             
               06  M-SS  PIC X(02).                                             
               06  M-AA  PIC X(02).                                             
           03  FILLER  PIC X(74).                                       00000350
                                                                        00000360
       FD  FNSOC                                                        00000300
           RECORDING F                                                  00000310
           BLOCK 0 RECORDS                                              00000320
           LABEL RECORD STANDARD.                                       00000330
       01  FILE-FNSOC.                                                  00000340
           03  F-NSOC  PIC X(03).                                       00000350
           03  FILLER  PIC X(77).                                       00000350
                                                                        00000360
                                                                        00000370
       FD  FRE030                                                       00000380
           RECORDING F                                                  00000390
           BLOCK 0 RECORDS                                              00000400
           LABEL RECORD STANDARD.                                       00000410
       01  ENR-FRE030.                                                  00000420
           02  FILLER PIC X(48).                                        00000430
                                                                        00000440
                                                                        00000370
       FD  FRE031                                                       00000380
           RECORDING F                                                  00000390
           BLOCK 0 RECORDS                                              00000400
           LABEL RECORD STANDARD.                                       00000410
       01  ENR-FRE031.                                                  00000420
           02  FILLER PIC X(48).                                        00000430
                                                                        00000440
                                                                        00000450
      * ------------------------------------------------------------ *  00000460
      *                W O R K I N G - S T O R A G E                 *  00000470
      * ------------------------------------------------------------ *  00000480
                                                                        00000490
       WORKING-STORAGE SECTION.                                         00000500
                                                                        00000510
                                                                        00000630
           COPY RVRE0000.                                               00000620
                                                                        00000630
      *                                                                 00000640
       77  CPT-FRE030                PIC 9(07)    VALUE 0.              00000820
       77  CPT-FRE031                PIC 9(07)    VALUE 0.              00000820
       77  CPT-RTRE00                PIC 9(07)    VALUE 0.              00000820
                                                                        00000980
       01  TEST-RTRE00               PIC X(01) VALUE ' '.               00000990
           88 PAS-FIN-RTRE00         VALUE '0'.                         00001000
           88 FIN-RTRE00             VALUE '1'.                         00001010
                                                                        00001020
       01  W-DATE.                                                      00001100
           05  W-SSAAMM.                                                        
               10  W-SS               PIC X(02)  VALUE ZEROES.          00001110
               10  W-AA               PIC X(02)  VALUE ZEROES.          00001120
               10  W-MM               PIC X(02)  VALUE ZEROES.          00001130
           05  W-JJ                   PIC X(02)  VALUE ZEROES.          00001130
                                                                        00001140
       01  W-MOIS.                                                      00001100
           10  W-SS-M             PIC X(02)  VALUE ZEROES.              1110    
           10  W-AA-M             PIC X(02)  VALUE ZEROES.              1120    
           10  W-MM-M             PIC X(02)  VALUE ZEROES.              1130    
                                                                        00001140
       01 W-NSOCIETE                PIC X(3).                           00001200
                                                                        00001210
      * ------------------------------------------------------------ *  00001220
      *         DESCRIPTION DES ZONES D'APPEL DU MODULE ABEND        *  00001230
      * ------------------------------------------------------------ *  00001240
                                                                        00001250
           COPY ABENDCOP.                                               00001260
           COPY WORKDATC.                                               00001260
                                                                        00001270
                                                                        00001400
                                                                        00004890
      * ------------------------------------------------------------ *  00001370
      *              P R O C E D U R E     D I V I S I O N           *  00001380
      * ------------------------------------------------------------ *  00001390
       PROCEDURE DIVISION.                                              00001410
                                                                        00001420
      * ------------------------------------------------------------ *  00001430
      *             T R A M E     D U     P R O G R A M M E          *  00001440
      * ------------------------------------------------------------ *  00001450
      *                                                              *  00001460
      *            -----------------------------------------         *  00001470
      *            -  MODULE DE BASE DU PROGRAMME  BRE030  -         *  00001480
      *            -----------------------------------------         *  00001490
      *                               I                              *  00001500
      *            -----------------------------------------         *  00001510
      *            I                  I                    I         *  00001520
      *       -----------      ----------------        ---------     *  00001530
      *       -  DEBUT  -      -  TRAITEMENT  -        -  FIN  -     *  00001540
      *       -----------      ----------------        ---------     *  00001550
      *                                                              *  00001560
      * ------------------------------------------------------------ *  00001570
                                                                        00001580
           PERFORM DEBUT-BRE030.                                        00001590
           PERFORM LECTURE-RTRE00.                                      00001880
           PERFORM TRAIT-BRE030 UNTIL FIN-RTRE00.                       00001600
           PERFORM FIN-BRE030.                                          00001610
                                                                        00001620
      * ------------------------------------------------------------ *  00001630
      *                     D E B U T    B R E 0 3 0                 *  00001640
      * ------------------------------------------------------------ *  00001650
                                                                        00001660
       DEBUT-BRE030 SECTION.                                            00001670
                                                                        00001680
                                                                        00001730
           PERFORM OUVERTURE-FICHIERS.                                  00001740
                                                                        00001750
           PERFORM LECTURE-PARAMETRES.                                          
                                                                        00001840
           PERFORM VALIDITE-DATE                                                
           INITIALIZE RVRE0000.                                                 
                                                                        00001860
           SET PAS-FIN-RTRE00    TO TRUE.                               00001870
                                                                        00001920
       FIN-DEBUT-BRE030. EXIT.                                          00001930
       LECTURE-PARAMETRES SECTION.                                              
           OPEN INPUT FDATE.                                                    
           READ FDATE AT END                                                    
              MOVE 'PAS DE DATE EN ENTREE' TO ABEND-MESS                        
              PERFORM FIN-ANORMALE                                              
           END-READ.                                                            
           DISPLAY 'FILE DATE : ' FILE-DATE                                     
           MOVE SS   TO W-SS                                            00003380
           MOVE AA   TO W-AA                                            00003380
           MOVE MM   TO W-MM                                            00003380
           MOVE JJ   TO W-JJ                                            00003380
           CLOSE FDATE                                                          
           OPEN INPUT FMOIS.                                                    
           READ FMOIS AT END                                                    
              MOVE 'PAS DE MOIS EN ENTREE' TO ABEND-MESS                        
              PERFORM FIN-ANORMALE                                              
           END-READ.                                                            
           DISPLAY 'FILE MOIS : ' FILE-MOIS                                     
           MOVE M-SS   TO W-SS-M                                        00003380
           MOVE M-AA   TO W-AA-M                                        00003380
           MOVE M-MM   TO W-MM-M                                        00003380
           CLOSE FMOIS                                                          
           OPEN INPUT FNSOC.                                                    
           READ FNSOC AT END                                                    
              MOVE 'PAS DE FNSOC EN ENTREE' TO ABEND-MESS                       
              PERFORM FIN-ANORMALE                                              
           END-READ.                                                            
           MOVE F-NSOC       TO W-NSOCIETE.                             00003380
           CLOSE FNSOC.                                                         
       FIN-LECTURE-PARAMETRES. EXIT.                                            
       VALIDITE-DATE SECTION.                                                   
                                                                        00001940
           IF W-MOIS = W-SSAAMM                                         00001100
              PERFORM CALCUL-DERNIER-JOUR                               00040001
           ELSE                                                                 
              IF W-MOIS > W-SSAAMM                                              
              STRING 'MOIS ' W-MOIS ' DATE ' W-DATE ' INCOMPATIBLE '            
                 DELIMITED BY SIZE INTO ABEND-MESS                              
                 PERFORM FIN-ANORMALE                                           
              END-IF                                                            
           END-IF.                                                              
       FIN-VALIDITE-DATE. EXIT.                                                 
       CALCUL-DERNIER-JOUR     SECTION.                                 00040001
                                                                        00050000
           MOVE W-DATE     TO GFSAMJ-0.                                 00060001
           MOVE '5'        TO GFDATA.                                   00080000
           CALL 'BETDATC'  USING WORK-BETDATC.                          00090000
           IF GFVDAT NOT = '1'                                          00100000
              DISPLAY 'DATE DE TRAITEMENT ANORMALE'                     00110000
              PERFORM FIN-ANORMALE                                      00120000
           ELSE                                                         00130000
              ADD      1 TO   GFQNT0                                            
              MOVE '3'   TO GFDATA                                              
              CALL 'BETDATC' USING WORK-BETDATC                                 
              IF GFVDAT NOT = '1'                                               
                 DISPLAY 'DATE DE TRAITEMENT ANORMALE'                          
                 PERFORM FIN-ANORMALE                                           
              ELSE                                                              
                 IF W-MM  =  GFMOIS                                             
                    STRING 'NON FIN DE MOIS  ' W-DATE                           
                    DELIMITED BY SIZE INTO ABEND-MESS                           
                    PERFORM FIN-ANORMALE                                        
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
                                                                        00230000
       FIN-CALCUL-DERNIER-JOUR. EXIT.                                   00040001
                                                                        00001940
      * -------------------- LECTURE FICHIER FCAISSE --------------- *  00001950
                                                                        00001960
       LECTURE-RTRE00   SECTION.                                        00001970
                                                                        00001980
           READ RTRE00  INTO RVRE0000   AT END                          00001990
                SET FIN-RTRE00  TO TRUE                                 00002000
           END-READ.                                                    00002010
           IF PAS-FIN-RTRE00                                            00001000
              ADD 1            TO CPT-RTRE00                            00002020
           END-IF.                                                              
                                                                        00002030
       FIN-LECTURE-RTRE00. EXIT.                                        00002040
                                                                        00002050
      * -------------------- OUVERTURE-FICHIERS -------------------- *  00002060
                                                                        00002070
       OUVERTURE-FICHIERS SECTION.                                      00002080
                                                                        00002090
           OPEN INPUT   RTRE00.                                         00002100
           OPEN OUTPUT  FRE030 FRE031.                                  00002110
                                                                        00002120
       FIN-OUVERTURE-FICHIERS. EXIT.                                    00002130
                                                                        00002140
                                                                        00002150
      * ------------------------------------------------------------ *  00002160
      *                     T R A I T    B R E 0 3 0                 *  00002170
      * ------------------------------------------------------------ *  00002180
                                                                        00002190
       TRAIT-BRE030 SECTION.                                            00002200
                                                                        00002210
      *       TEST PAR RAPPORT AU MOIS EN COURS                         02390   
           IF RE00-DMVENTE = W-MOIS                                     00001880
              PERFORM ECRITURE-MOIS-EN-COURS                                    
              PERFORM ECRITURE-MOIS-SUIVANT                                     
           ELSE                                                                 
              IF RE00-DMVENTE > W-MOIS                                  00001880
                 PERFORM ECRITURE-MOIS-SUIVANT                                  
              END-IF                                                            
           END-IF.                                                              
           PERFORM LECTURE-RTRE00.                                      00002490
      *                                                                 00002520
                                                                        00002530
       FIN-TRAIT-BRE030. EXIT.                                          00002540
      * ------------------------------------------------------------ *  00003360
      *               CHARGEMENT FICHIER POUR LOAD                   *  00003370
      * ------------------------------------------------------------ *  00003380
                                                                        00003390
       ECRITURE-MOIS-EN-COURS SECTION.                                          
                                                                        00003410
           WRITE ENR-FRE030 FROM RVRE0000.                              00003750
                                                                        00003760
           ADD 1 TO CPT-FRE030.                                         00003770
                                                                        00003780
       FIN-ECRITURE-MOIS-EN-COURS. EXIT.                                        
                                                                        00003790
      * ------------------------------------------------------------ *  00003360
      *               CHARGEMENT FICHIER MOIS PROCHAIN               *  00003370
      * ------------------------------------------------------------ *  00003380
                                                                        00003390
       ECRITURE-MOIS-SUIVANT  SECTION.                                          
                                                                        00003410
           WRITE ENR-FRE031 FROM RVRE0000.                              00003750
                                                                        00003760
           ADD 1 TO CPT-FRE031.                                         00003770
                                                                        00003780
       FIN-ECRITURE-MOIS-SUIVANT. EXIT.                                         
                                                                        00003790
      * ------------------------ FIN-ANORMALE ---------------------- *  00004110
                                                                        00004120
       FIN-ANORMALE SECTION.                                            00004130
                                                                        00004140
           DISPLAY '*****************************************'.         00004150
           DISPLAY '***** FIN ANORMALE DU PROGRAMME *********'.         00004160
           DISPLAY '*****************************************'.         00004170
           CLOSE FRE030 FRE031  RTRE00.                                 00004180
           MOVE 'BRE030' TO ABEND-PROG.                                 00004190
           CALL 'ABEND' USING ABEND-PROG ABEND-MESS.                    00004200
                                                                        00004210
       FIN-FIN-ANORMALE. EXIT.                                          00004220
                                                                        00004230
      * ------------------------------------------------------------ *  00003820
      *                      F I N     B R E 0 3 0                   *  00003830
      * ------------------------------------------------------------ *  00003840
                                                                        00003850
       FIN-BRE030 SECTION.                                              00003860
                                                                        00003870
           CLOSE  FRE030 FRE031 RTRE00.                                 00003920
           PERFORM COMPTE-RENDU.                                        00003930
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00003940
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        00003950
       FIN-FIN-BRE030. EXIT.                                            00003960
                                                                        00003970
      * ------------------------ COMPTE-RENDU ---------------------- *  00003980
                                                                        00003990
       COMPTE-RENDU SECTION.                                            00004000
                                                                        00004010
           DISPLAY '**'.                                                00004020
           DISPLAY '**          B R E 0 3 0'.                           00004030
           DISPLAY '**'.                                                00004040
           DISPLAY '** NOMBRE DE LECTURES RTRE00 : '  CPT-RTRE00.       00004050
           DISPLAY '** NOMBRE D''ECRITURES FRE030 : ' CPT-FRE030.       00004060
           DISPLAY '** NOMBRE D''ECRITURES FRE031 : ' CPT-FRE031.       00004060
           DISPLAY '**'.                                                00004070
                                                                        00004080
       FIN-COMPTE-RENDU. EXIT.                                          00004090
