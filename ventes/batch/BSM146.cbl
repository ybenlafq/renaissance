      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.    BSM146                                                    
       DATE-COMPILED.                                                           
       AUTHOR. DSA013.                                                          
      *------------------------------------------------------------*            
      *                PROGRAMME D'EDITION DE L'ETAT               *            
      *                                  ISM145                    *            
      *                                                            *            
      * DATE CREATION : 29/12/2004                                 *            
AL1008* MODIFICATION  : 03/10/2008                                 *            
AL1008*                 AJOUT D'UNE COLONNE MARGE ET TX MARGE      *            
      *------------------------------------------------------------*            
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FSM145  ASSIGN TO FSM145.                                     
      *--                                                                       
           SELECT FSM145  ASSIGN TO FSM145                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE   ASSIGN TO FDATE.                                      
      *--                                                                       
           SELECT FDATE   ASSIGN TO FDATE                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT ISM145GR ASSIGN TO ISM145GR.                                  
      *--                                                                       
           SELECT ISM145GR ASSIGN TO ISM145GR                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT ISM145BR ASSIGN TO ISM145BR.                                  
      *--                                                                       
           SELECT ISM145BR ASSIGN TO ISM145BR                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT ISM145BL ASSIGN TO ISM145BL.                                  
      *--                                                                       
           SELECT ISM145BL ASSIGN TO ISM145BL                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT ISM145AC ASSIGN TO ISM145AC.                                  
      *--                                                                       
           SELECT ISM145AC ASSIGN TO ISM145AC                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      *---------------------------------------------------------------*         
      *  D E F I N I T I O N    F I C H I E R    F D A T E            *         
      *---------------------------------------------------------------*         
      *                                                                         
       FD  FDATE                                                                
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FDATE-ENREG.                                                 00000030
           02 FDATE-JJ              PIC X(02).                                  
           02 FDATE-MM              PIC X(02).                                  
           02 FDATE-SS              PIC X(02).                                  
           02 FDATE-AA              PIC X(02).                                  
           02 FILLER                PIC X(72).                                  
      *                                                                         
      *---------------------------------------------------------------*         
      *  D E F I N I T I O N    F I C H I E R    F S M 1 4 5          *         
      *---------------------------------------------------------------*         
      *                                                                         
       FD  FSM145                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  ENREG-FSM145 PIC X(512).                                             
      *                                                                         
      *---------------------------------------------------------------*         
      *  D E F I N I T I O N    F I C H I E R    I S M 1 4 5          *         
      *---------------------------------------------------------------*         
      *                                                                         
       FD  ISM145GR                                                             
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  LIGNE-ISM145GR.                                                      
           02 SAUTGR     PIC X.                                                 
           02 LIGNEGR    PIC X(198).                                            
      *                                                                         
       FD  ISM145BR                                                             
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  LIGNE-ISM145BR.                                                      
           02 SAUTBR     PIC X.                                                 
           02 LIGNEBR    PIC X(198).                                            
      *                                                                         
       FD  ISM145BL                                                             
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  LIGNE-ISM145BL.                                                      
           02 SAUTBL     PIC X.                                                 
           02 LIGNEBL    PIC X(198).                                            
      *                                                                         
       FD  ISM145AC                                                             
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  LIGNE-ISM145AC.                                                      
           02 SAUTAC     PIC X.                                                 
           02 LIGNEAC    PIC X(198).                                            
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *                 W O R K I N G  -  S T O R A G E               *         
      *---------------------------------------------------------------*         
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
      *                                                                         
        COPY ABENDCOP.                                                          
      *                                                                         
        COPY WORKDATC.                                                          
        COPY  SYBWDIV0.                                                         
      *                                                                         
      *---------------------------------------------------------------*         
      *           D E S C R I P T I O N    D E S    Z O N E S         *         
      *        S P E C I F I Q U E S    A U    P R O G R A M M E      *         
      *---------------------------------------------------------------*         
      *                                                                         
      *                                                                         
       01  LIGNE-ISM145.                                                        
           02 SAUT       PIC X.                                                 
           02 LIGNE      PIC X(198).                                            
       01  W-DATE.                                                              
           03  DATJMSA.                                                         
               05 JJ                  PIC  XX.                                  
               05 MM                  PIC  XX.                                  
               05 SS                  PIC  XX.                                  
               05 AA                  PIC  XX.                                  
           03  W-TIME.                                                          
               05 HH                  PIC  XX.                                  
               05 MN                  PIC  XX.                                  
               05 FILLER              PIC  X(14).                               
       01  JOUR-SEM PIC X.                                                      
       01  W-DATE-TAMP.                                                         
               05 SS                  PIC  XX.                                  
               05 AA                  PIC  XX.                                  
               05 MM                  PIC  XX.                                  
               05 JJ                  PIC  XX.                                  
      *                                                                         
       01  CTR-FSM145  PIC 9(9) VALUE 0.                                        
       01  CTR-GRIS    PIC 9(9) VALUE 0.                                        
       01  CTR-BRUN    PIC 9(9) VALUE 0.                                        
       01  CTR-BLAN    PIC 9(9) VALUE 0.                                        
       01  CTR-ACCE    PIC 9(9) VALUE 0.                                        
      *                                                                         
       01  FLAG-FICHIER PIC X VALUE SPACE.                                      
           88  EOF      VALUE 'F'.                                              
       01  W-FLAGS.                                                             
           05 FLAG-LGDRAYON       PIC 9.                                        
              88 GRIS             VALUE 0.                                      
              88 BRUN             VALUE 1.                                      
              88 BLANC            VALUE 2.                                      
              88 ACCESSOIRE       VALUE 3.                                      
       01  WEDITIOX                  PIC X(8)            VALUE SPACE.           
      *                                                                         
      *ENREGISTREMENT COURANT                                                   
              COPY ISM145 REPLACING ==:ISM145:== BY ==FSM145==.                 
      *                                                                         
      *ENREGISTREMENT PRECEDENT                                                 
              COPY ISM145 REPLACING ==:ISM145:== BY ==W-FSM145==.               
      *--------------------------- CUMUL TSEG --------------------------        
        01 W-TSEG.                                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES1                  PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES1                  PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES2                  PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES2                  PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES3                  PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES3                  PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES4                  PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES4                  PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES5                  PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES5                  PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES6                  PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES6                  PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES7                  PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES7                  PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES8                  PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES8                  PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES9                  PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES9                  PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES10                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES10                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES11                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES11                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES12                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES12                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES13                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES13                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES14                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES14                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES15                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES15                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES16                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES16                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES17                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES17                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES18                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES18                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES19                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES19                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QVTES                   PIC S9(5) COMP.               
      *--                                                                       
               10  W-TSEG-QVTES                   PIC S9(5) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS1             PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS1             PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS2             PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS2             PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS3             PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS3             PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS4             PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS4             PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS5             PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS5             PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS6             PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS6             PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS7             PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS7             PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS8             PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS8             PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS9             PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS9             PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS10            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS10            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS11            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS11            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS12            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS12            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS13            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS13            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS14            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS14            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS15            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS15            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS16            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS16            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS17            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS17            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS18            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS18            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS19            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS19            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKDEPS              PIC S9(5) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKDEPS              PIC S9(5) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TSEG-QSTOCKMAGS              PIC S9(5) COMP.               
      *--                                                                       
               10  W-TSEG-QSTOCKMAGS              PIC S9(5) COMP-5.             
      *}                                                                        
      *--------------------------- CUMUL TFAM --------------------------        
        01 W-TFAM.                                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF1                  PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF1                  PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF2                  PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF2                  PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF3                  PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF3                  PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF4                  PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF4                  PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF5                  PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF5                  PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF6                  PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF6                  PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF7                  PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF7                  PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF8                  PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF8                  PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF9                  PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF9                  PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF10                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF10                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF11                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF11                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF12                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF12                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF13                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF13                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF14                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF14                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF15                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF15                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF16                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF16                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF17                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF17                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF18                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF18                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF19                 PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF19                 PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QVTEF                   PIC S9(5) COMP.               
      *--                                                                       
               10  W-TFAM-QVTEF                   PIC S9(5) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF1             PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF1             PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF2             PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF2             PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF3             PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF3             PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF4             PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF4             PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF5             PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF5             PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF6             PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF6             PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF7             PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF7             PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF8             PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF8             PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF9             PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF9             PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF10            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF10            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF11            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF11            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF12            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF12            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF13            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF13            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF14            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF14            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF15            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF15            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF16            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF16            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF17            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF17            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF18            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF18            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF19            PIC S9(4) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF19            PIC S9(4) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKDEPF              PIC S9(5) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKDEPF              PIC S9(5) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-TFAM-QSTOCKMAGF              PIC S9(5) COMP.               
      *--                                                                       
               10  W-TFAM-QSTOCKMAGF              PIC S9(5) COMP-5.             
      *}                                                                        
      *                                                                         
       01  CTR-PAGE     PIC 9(5) VALUE 0.                                       
       01  CTR-LIGNE    PIC 99   VALUE 0.                                       
       01  MAX-LIGNE1   PIC 99   VALUE 54.                                      
       01  MAX-LIGNE2   PIC 99   VALUE 55.                                      
       01  MAX-LIGNE3   PIC 99   VALUE 57.                                      
           EJECT                                                                
      *                                                                         
       01  ISM145-LIGNES.                                                       
      *--------------------------- LIGNE E01  --------------------------        
           05  ISM145-E01.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+---------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(24) VALUE               
           '-----------------------+'.                                          
      *--------------------------- LIGNE E03  --------------------------        
           05  ISM145-E03.                                                      
               10  FILLER                         PIC X(8) VALUE                
           '! ISM145'.                                                          
               10  ISM145-E03-LGDRAYON            PIC X(02).                    
               10  FILLER                         PIC X(33) VALUE               
           '                       SOCIETE : '.                                 
               10  ISM145-E03-NSOCIETE            PIC X(03).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                          ETABLISSEMENT DA'.        
               10  FILLER                         PIC X(58) VALUE               
           'RTY                                                       '.        
               10  FILLER                         PIC X(25) VALUE               
           '      ZONE DE       PAGE '.                                         
               10  ISM145-E03-NOPAGE              PIC ZZZZZ.                    
               10  FILLER                         PIC X(06) VALUE               
           '     !'.                                                            
      *--------------------------- LIGNE E05  --------------------------        
           05  ISM145-E05.                                                      
               10  FILLER                         PIC X(15) VALUE               
           '! JOURNEE DU : '.                                                   
               10  ISM145-E05-LIBDATE             PIC X(12).                    
               10  FILLER                         PIC X(16) VALUE               
           '      GROUPE  : '.                                                  
               10  ISM145-E05-GRP                 PIC X(02).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                SITUATION DES VENTES ET ST'.        
               10  FILLER                         PIC X(58) VALUE               
           'OCKS PAR MAGASIN                                          '.        
               10  FILLER                         PIC X(14) VALUE               
           '       PRIX : '.                                                    
               10  ISM145-E05-NZONPRIX            PIC X(02).                    
               10  FILLER                         PIC X(21) VALUE               
           '                    !'.                                             
      *--------------------------- LIGNE E15  --------------------------        
           05  ISM145-E15.                                                      
               10  FILLER                         PIC X(43) VALUE               
           '!                                CHEF PR : '.                       
               10  ISM145-E15-CHEFPROD            PIC X(05).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(34) VALUE               
           '                                 !'.                                
      *--------------------------- LIGNE E40  --------------------------        
           05  ISM145-E40.                                                      
               10  FILLER                         PIC X(58) VALUE               
AL1008*  '+-----------------------------------+--------+---------+--'.          
AL1008     '+-----------------------------+--------+---------+--------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '------------------------------------------+-------+-------'.        
AL1008     '------------------------------------+-------+-------------'.        
               10  FILLER                         PIC X(24) VALUE               
AL1008*    '----------+------------+'.                                          
AL1008     '----+-------+----------+'.                                          
      *--------------------------- LIGNE E45  --------------------------        
           05  ISM145-E45.                                                      
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '! FAMILLE / CARACTERISTIQUES        !  RANG  ! O S STA !  '.        
AL1008     '! FAMILLE / CARACTERISTIQUES  !  RANG  ! O S STA !        '.        
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '                   VENTES / STOCKS PAR MAGASIN            '.        
AL1008     '             VENTES / STOCKS PAR MAGASIN                  '.        
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '                                          ! TOTAL ! PRIX D'.        
           '                                    ! TOTAL ! PRIX DE REF '.        
               10  FILLER                         PIC X(24) VALUE               
AL1008*    'E REF NAT !RECEPTION   !'.                                          
AL1008     'NAT ! MARGE !RECEPTION !'.                                          
      *--------------------------- LIGNE E50  --------------------------        
           05  ISM145-E50.                                                      
AL1008*        10  FILLER                         PIC X(58) VALUE               
AL1008*    '! CODIC  MARQUE REFERENCE           !GPE SOC ! A A TUT !  '.        
AL1008         10  FILLER                         PIC X(52) VALUE               
AL1008     '! CODIC  MARQUE REFERENCE     !GPE SOC ! A A TUT !  '.              
               10  ISM145-E50-MAG1                PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-E50-MAG2                PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-E50-MAG3                PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-E50-MAG4                PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-E50-MAG5                PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-E50-MAG6                PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-E50-MAG7                PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-E50-MAG8                PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-E50-MAG9                PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-E50-MAG10               PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-E50-MAG11               PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-E50-MAG12               PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-E50-MAG13               PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-E50-MAG14               PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-E50-MAG15               PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-E50-MAG16               PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-E50-MAG17               PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-E50-MAG18               PIC X(03).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-E50-MAG19               PIC X(03).                    
AL1008*        10  FILLER                         PIC X(47) VALUE               
AL1008*    '  ENT  !  MAG  ! PRIX VTE ZONE   ! DATE QTE   !'.                   
AL1008         10  FILLER                         PIC X(53) VALUE               
AL1008     '  ENT  !  MAG  ! PRIX VTE ZONE   ! TX MA ! DATE QTE !'.             
      *--------------------------- LIGNE E55  --------------------------        
           05  ISM145-E55.                                                      
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '+-----------------------------------+--------+---------+--'.        
AL1008     '+-----------------------------+--------+---------+--------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '------------------------------------------+-------+-------'.        
AL1008     '------------------------------------+-------+-------------'.        
               10  FILLER                         PIC X(24) VALUE               
AL1008*    '----------+------------+'.                                          
AL1008     '----+-------+----------+'.                                          
      *--------------------------- LIGNE E60  --------------------------        
           05  ISM145-E60.                                                      
               10  FILLER                         PIC X(02) VALUE               
           '! '.                                                                
               10  ISM145-E60-LFAM                PIC X(20).                    
AL1008*        10  FILLER                         PIC X(34) VALUE               
AL1008*    '              !        !         !'.                                
AL1008         10  FILLER                         PIC X(28) VALUE               
AL1008     '        !        !         !'.                                      
               10  ISM145-E60-GAMME1              PIC X(05).                    
               10  ISM145-E60-GAMME2              PIC X(05).                    
               10  ISM145-E60-GAMME3              PIC X(05).                    
               10  ISM145-E60-GAMME4              PIC X(05).                    
               10  ISM145-E60-GAMME5              PIC X(05).                    
               10  ISM145-E60-GAMME6              PIC X(05).                    
               10  ISM145-E60-GAMME7              PIC X(05).                    
               10  ISM145-E60-GAMME8              PIC X(05).                    
               10  ISM145-E60-GAMME9              PIC X(05).                    
               10  ISM145-E60-GAMME10             PIC X(05).                    
               10  ISM145-E60-GAMME11             PIC X(05).                    
               10  ISM145-E60-GAMME12             PIC X(05).                    
               10  ISM145-E60-GAMME13             PIC X(05).                    
               10  ISM145-E60-GAMME14             PIC X(05).                    
               10  ISM145-E60-GAMME15             PIC X(05).                    
               10  ISM145-E60-GAMME16             PIC X(05).                    
               10  ISM145-E60-GAMME17             PIC X(05).                    
               10  ISM145-E60-GAMME18             PIC X(05).                    
               10  ISM145-E60-GAMME19             PIC X(05).                    
AL1008*        10  FILLER                         PIC X(47) VALUE               
AL1008*    '       !       !                 !            !'.                   
AL1008         10  FILLER                         PIC X(53) VALUE               
AL1008     '       !       !                 !       !          !'.             
      *--------------------------- LIGNE E61  --------------------------        
           05  ISM145-E61.                                                      
AL1008         10  FILLER                         PIC X(58) VALUE               
AL1008*    '!                                   !        !         !  '.        
AL1008     '!                             !        !         !        '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '                                          !       !       '.        
AL1008     '                                    !       !             '.        
               10  FILLER                         PIC X(24) VALUE               
AL1008*    '          !            !'.                                          
AL1008     '    !       !          !'.                                          
      *--------------------------- LIGNE E62  --------------------------        
           05  ISM145-E62.                                                      
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '!***********************************!        !         !  '.        
AL1008     '!*****************************!        !         !        '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '                                          !       !       '.        
AL1008     '                                    !       !             '.        
               10  FILLER                         PIC X(24) VALUE               
AL1008*    '          !            !'.                                          
AL1008     '    !       !          !'.                                          
      *--------------------------- LIGNE E65  --------------------------        
           05  ISM145-E65.                                                      
               10  FILLER                         PIC X(02) VALUE               
           '! '.                                                                
               10  ISM145-E65-LVMARKET1           PIC X(20).                    
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '              !        !         !                        '.        
AL1008     '        !        !         !                              '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '                    !       !                 !           '.        
AL1008     '              !       !                 !       !         '.        
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
      *--------------------------- LIGNE E66  --------------------------        
           05  ISM145-E66.                                                      
               10  FILLER                         PIC X(02) VALUE               
           '! '.                                                                
               10  ISM145-E66-LVMARKET2           PIC X(20).                    
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '              !        !         !                        '.        
AL1008     '        !        !         !                              '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '                    !       !                 !           '.        
AL1008     '              !       !                 !       !         '.        
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
      *--------------------------- LIGNE E67  --------------------------        
           05  ISM145-E67.                                                      
               10  FILLER                         PIC X(02) VALUE               
           '! '.                                                                
               10  ISM145-E67-LVMARKET3           PIC X(20).                    
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '              !        !         !                        '.        
AL1008     '        !        !         !                              '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '                    !       !                 !           '.        
AL1008     '              !       !                 !       !         '.        
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
      *--------------------------- LIGNE E68  --------------------------        
           05  ISM145-E68.                                                      
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '!***********************************!        !         !  '.        
AL1008     '!*****************************!        !         !        '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '                                          !       !       '.        
AL1008     '                                    !       !             '.        
               10  FILLER                         PIC X(24) VALUE               
AL1008*    '          !            !'.                                          
AL1008     '    !       !          !'.                                          
      *--------------------------- LIGNE D04  --------------------------        
           05  ISM145-D04.                                                      
AL1008         10  FILLER                         PIC X(58) VALUE               
AL1008*    '!                                   !        !         !  '.        
AL1008     '!                             !        !         !        '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '                                          !       !       '.        
AL1008     '                                    !       !             '.        
               10  FILLER                         PIC X(24) VALUE               
AL1008*    '          !            !'.                                          
AL1008     '    !       !          !'.                                          
      *--------------------------- LIGNE D05  --------------------------        
           05  ISM145-D05.                                                      
               10  FILLER                         PIC X(01) VALUE               
           '!'.                                                                 
               10  ISM145-D05-NCODIC              PIC X(07).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-CMARQ               PIC X(05).                    
               10  FILLER                         PIC X(17) VALUE               
           '                !'.                                                 
      *        10  FILLER                         PIC X(01) VALUE               
      *    ' '.                                                                 
      *        10  ISM145-D05-LREFFOURN           PIC X(20).                    
      *        10  FILLER                         PIC X(02) VALUE               
      *    ' !'.                                                                
               10  ISM145-D05-RANGGPEAFF          PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-RANGSOCAFF          PIC X(03).                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  ISM145-D05-WOA                 PIC X(01).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-WSENSAPPRO          PIC X(01).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-LSTATCOMP           PIC X(03).                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  ISM145-D05-QVTE1               PIC ZZZZ.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-QVTE2               PIC ZZZZ.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-QVTE3               PIC ZZZZ.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-QVTE4               PIC ZZZZ.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-QVTE5               PIC ZZZZ.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-QVTE6               PIC ZZZZ.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-QVTE7               PIC ZZZZ.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-QVTE8               PIC ZZZZ.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-QVTE9               PIC ZZZZ.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-QVTE10              PIC ZZZZ.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-QVTE11              PIC ZZZZ.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-QVTE12              PIC ZZZZ.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-QVTE13              PIC ZZZZ.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-QVTE14              PIC ZZZZ.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-QVTE15              PIC ZZZZ.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-QVTE16              PIC ZZZZ.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-QVTE17              PIC ZZZZ.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-QVTE18              PIC ZZZZ.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D05-QVTE19              PIC ZZZZ.                     
               10  FILLER                         PIC X(09) VALUE               
           '       ! '.                                                         
               10  ISM145-D05-QVTETOT             PIC ZZZZ9.                    
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  ISM145-D05-PVREFNAT            PIC ZZZZ9,99.                 
AL1008         10  FILLER                         PIC X(10) VALUE               
AL1008     '         !'.                                                        
AL1008         10  ISM145-D05-MARGE               PIC ---9,9.                   
AL1008         10  FILLER                         PIC X(02) VALUE               
AL1008     ' !'.                                                                
               10  ISM145-D05-LIBELLE             PIC X(10).                    
               10  FILLER                         PIC X(01) VALUE               
           '!'.                                                                 
      *--------------------------- LIGNE D06  --------------------------        
           05  ISM145-D06.                                                      
AL1008*        10  FILLER                         PIC X(57) VALUE               
      *    '!                                   !        !         ! '.         
AL1008         10  FILLER                         PIC X(09) VALUE               
           '!        '.                                                         
               10  ISM145-D06-LREFFOURN           PIC X(20).                    
AL1008         10  FILLER                         PIC X(22) VALUE               
           ' !        !         ! '.                                            
               10  ISM145-D06-QSTOCKMAG1          PIC ZZZ9.                     
               10  ISM145-X1 REDEFINES ISM145-D06-QSTOCKMAG1 PIC X(4).          
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKMAG2          PIC ZZZ9.                     
               10  ISM145-X2 REDEFINES ISM145-D06-QSTOCKMAG2 PIC X(4).          
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKMAG3          PIC ZZZ9.                     
               10  ISM145-X3 REDEFINES ISM145-D06-QSTOCKMAG3 PIC X(4).          
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKMAG4          PIC ZZZ9.                     
               10  ISM145-X4 REDEFINES ISM145-D06-QSTOCKMAG4 PIC X(4).          
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKMAG5          PIC ZZZ9.                     
               10  ISM145-X5 REDEFINES ISM145-D06-QSTOCKMAG5 PIC X(4).          
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKMAG6          PIC ZZZ9.                     
               10  ISM145-X6 REDEFINES ISM145-D06-QSTOCKMAG6 PIC X(4).          
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKMAG7          PIC ZZZ9.                     
               10  ISM145-X7 REDEFINES ISM145-D06-QSTOCKMAG7 PIC X(4).          
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKMAG8          PIC ZZZ9.                     
               10  ISM145-X8 REDEFINES ISM145-D06-QSTOCKMAG8 PIC X(4).          
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKMAG9          PIC ZZZ9.                     
               10  ISM145-X9 REDEFINES ISM145-D06-QSTOCKMAG9 PIC X(4).          
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKMAG10         PIC ZZZ9.                     
               10  ISM145-X10 REDEFINES ISM145-D06-QSTOCKMAG10 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKMAG11         PIC ZZZ9.                     
               10  ISM145-X11 REDEFINES ISM145-D06-QSTOCKMAG11 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKMAG12         PIC ZZZ9.                     
               10  ISM145-X12 REDEFINES ISM145-D06-QSTOCKMAG12 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKMAG13         PIC ZZZ9.                     
               10  ISM145-X13 REDEFINES ISM145-D06-QSTOCKMAG13 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKMAG14         PIC ZZZ9.                     
               10  ISM145-X14 REDEFINES ISM145-D06-QSTOCKMAG14 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKMAG15         PIC ZZZ9.                     
               10  ISM145-X15 REDEFINES ISM145-D06-QSTOCKMAG15 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKMAG16         PIC ZZZ9.                     
               10  ISM145-X16 REDEFINES ISM145-D06-QSTOCKMAG16 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKMAG17         PIC ZZZ9.                     
               10  ISM145-X17 REDEFINES ISM145-D06-QSTOCKMAG17 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKMAG18         PIC ZZZ9.                     
               10  ISM145-X18 REDEFINES ISM145-D06-QSTOCKMAG18 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKMAG19         PIC ZZZ9.                     
               10  ISM145-X19 REDEFINES ISM145-D06-QSTOCKMAG19 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D06-QSTOCKDEP           PIC ZZZ9.                     
               10  FILLER                         PIC X(04) VALUE               
           '  ! '.                                                              
               10  ISM145-D06-QSTOCKMAGTOT        PIC ZZZZ9.                    
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  ISM145-D06-PSTDTTC             PIC ZZZZ9,99.                 
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  ISM145-D06-PRIME               PIC ZZ9,99.                   
AL1008*        10  FILLER                         PIC X(06) VALUE               
AL1008*    ' !    '.                                                            
AL1008         10  FILLER                         PIC X(02) VALUE               
AL1008     ' !'.                                                                
AL1008         10  ISM145-D06-TXMARGE             PIC ---9,9.                   
AL1008         10  FILLER                         PIC X(05) VALUE               
AL1008     ' !   '.                                                             
               10  ISM145-D06-QCDE                PIC ZZZZ9.                    
               10  ISM145-D06-X REDEFINES ISM145-D06-QCDE PIC X(5).             
               10  FILLER                         PIC X(03) VALUE               
           '  !'.                                                               
      *--------------------------- LIGNE D07  --------------------------        
           05  ISM145-D07.                                                      
               10  FILLER                         PIC X(15) VALUE               
           '! CODIC GROUPE '.                                                   
               10  ISM145-D07-NCODICGRP           PIC X(07).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-D07-LSTATCOMP2          PIC X(03).                    
AL1008         10  FILLER                         PIC X(58) VALUE               
AL1008*    '          !        !         !                            '.        
AL1008     '    !        !         !                                  '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
AL1008         10  FILLER                         PIC X(56) VALUE               
AL1008*    '                !       !                 !             '.          
AL1008     '          !       !                 !       !          !'.          
      *--------------------------- LIGNE T06  --------------------------        
           05  ISM145-T06.                                                      
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '!                                   !        !         !  '.        
AL1008     '!                             !        !         !        '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
AL1008     '                                    !       !             '.        
               10  FILLER                         PIC X(24) VALUE               
AL1008     '    !       !          !'.                                          
      *--------------------------- LIGNE T18  --------------------------        
           05  ISM145-T18.                                                      
AL1008*        10  FILLER                         PIC X(57) VALUE               
AL1008*    '!                   TOTAL SEGMENT   !        !         ! '.         
               10  FILLER                         PIC X(51) VALUE               
           '!             TOTAL SEGMENT   !        !         ! '.               
               10  ISM145-T18-QVTES1              PIC ZZZ9.                     
               10  ISM145-T18-X1 REDEFINES ISM145-T18-QVTES1 PIC X(4).          
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T18-QVTES2              PIC ZZZ9.                     
               10  ISM145-T18-X2 REDEFINES ISM145-T18-QVTES2 PIC X(4).          
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T18-QVTES3              PIC ZZZ9.                     
               10  ISM145-T18-X3 REDEFINES ISM145-T18-QVTES3 PIC X(4).          
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T18-QVTES4              PIC ZZZ9.                     
               10  ISM145-T18-X4 REDEFINES ISM145-T18-QVTES4 PIC X(4).          
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T18-QVTES5              PIC ZZZ9.                     
               10  ISM145-T18-X5 REDEFINES ISM145-T18-QVTES5 PIC X(4).          
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T18-QVTES6              PIC ZZZ9.                     
               10  ISM145-T18-X6 REDEFINES ISM145-T18-QVTES6 PIC X(4).          
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T18-QVTES7              PIC ZZZ9.                     
               10  ISM145-T18-X7 REDEFINES ISM145-T18-QVTES7 PIC X(4).          
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T18-QVTES8              PIC ZZZ9.                     
               10  ISM145-T18-X8 REDEFINES ISM145-T18-QVTES8 PIC X(4).          
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T18-QVTES9              PIC ZZZ9.                     
               10  ISM145-T18-X9 REDEFINES ISM145-T18-QVTES9 PIC X(4).          
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T18-QVTES10             PIC ZZZ9.                     
               10  ISM145-T18-X10 REDEFINES ISM145-T18-QVTES10 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T18-QVTES11             PIC ZZZ9.                     
               10  ISM145-T18-X11 REDEFINES ISM145-T18-QVTES11 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T18-QVTES12             PIC ZZZ9.                     
               10  ISM145-T18-X12 REDEFINES ISM145-T18-QVTES12 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T18-QVTES13             PIC ZZZ9.                     
               10  ISM145-T18-X13 REDEFINES ISM145-T18-QVTES13 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T18-QVTES14             PIC ZZZ9.                     
               10  ISM145-T18-X14 REDEFINES ISM145-T18-QVTES14 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T18-QVTES15             PIC ZZZ9.                     
               10  ISM145-T18-X15 REDEFINES ISM145-T18-QVTES15 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T18-QVTES16             PIC ZZZ9.                     
               10  ISM145-T18-X16 REDEFINES ISM145-T18-QVTES16 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T18-QVTES17             PIC ZZZ9.                     
               10  ISM145-T18-X17 REDEFINES ISM145-T18-QVTES17 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T18-QVTES18             PIC ZZZ9.                     
               10  ISM145-T18-X18 REDEFINES ISM145-T18-QVTES18 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T18-QVTES19             PIC ZZZ9.                     
               10  ISM145-T18-X19 REDEFINES ISM145-T18-QVTES19 PIC X(4).        
               10  FILLER                         PIC X(09) VALUE               
           '       ! '.                                                         
               10  ISM145-T18-QVTES               PIC ZZZZ9.                    
AL1008*        10  FILLER                         PIC X(33) VALUE               
AL1008*    ' !                 !            !'.                                 
AL1008         10  FILLER                         PIC X(39) VALUE               
AL1008     ' !                 !       !          !'.                           
      *--------------------------- LIGNE T19  --------------------------        
           05  ISM145-T19.                                                      
AL1008*        10  FILLER                         PIC X(57) VALUE               
AL1008*    '!                                   !        !         ! '.         
AL1008         10  FILLER                         PIC X(51) VALUE               
AL1008     '!                             !        !         ! '.               
               10  ISM145-T19-QSTOCKMAGS1         PIC ZZZ9.                     
               10 ISM145-TX1  REDEFINES ISM145-T19-QSTOCKMAGS1 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKMAGS2         PIC ZZZ9.                     
               10 ISM145-TX2  REDEFINES ISM145-T19-QSTOCKMAGS2 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKMAGS3         PIC ZZZ9.                     
               10 ISM145-TX3  REDEFINES ISM145-T19-QSTOCKMAGS3 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKMAGS4         PIC ZZZ9.                     
               10 ISM145-TX4  REDEFINES ISM145-T19-QSTOCKMAGS4 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKMAGS5         PIC ZZZ9.                     
               10 ISM145-TX5  REDEFINES ISM145-T19-QSTOCKMAGS5 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKMAGS6         PIC ZZZ9.                     
               10 ISM145-TX6  REDEFINES ISM145-T19-QSTOCKMAGS6 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKMAGS7         PIC ZZZ9.                     
               10 ISM145-TX7  REDEFINES ISM145-T19-QSTOCKMAGS7 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKMAGS8         PIC ZZZ9.                     
               10 ISM145-TX8  REDEFINES ISM145-T19-QSTOCKMAGS8 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKMAGS9         PIC ZZZ9.                     
               10 ISM145-TX9  REDEFINES ISM145-T19-QSTOCKMAGS9 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKMAGS10        PIC ZZZ9.                     
              10 ISM145-TX10 REDEFINES ISM145-T19-QSTOCKMAGS10 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKMAGS11        PIC ZZZ9.                     
              10 ISM145-TX11 REDEFINES ISM145-T19-QSTOCKMAGS11 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKMAGS12        PIC ZZZ9.                     
              10 ISM145-TX12 REDEFINES ISM145-T19-QSTOCKMAGS12 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKMAGS13        PIC ZZZ9.                     
              10 ISM145-TX13 REDEFINES ISM145-T19-QSTOCKMAGS13 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKMAGS14        PIC ZZZ9.                     
              10 ISM145-TX14 REDEFINES ISM145-T19-QSTOCKMAGS14 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKMAGS15        PIC ZZZ9.                     
              10 ISM145-TX15 REDEFINES ISM145-T19-QSTOCKMAGS15 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKMAGS16        PIC ZZZ9.                     
              10 ISM145-TX16 REDEFINES ISM145-T19-QSTOCKMAGS16 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKMAGS17        PIC ZZZ9.                     
              10 ISM145-TX17 REDEFINES ISM145-T19-QSTOCKMAGS17 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKMAGS18        PIC ZZZ9.                     
              10 ISM145-TX18 REDEFINES ISM145-T19-QSTOCKMAGS18 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKMAGS19        PIC ZZZ9.                     
              10 ISM145-TX19 REDEFINES ISM145-T19-QSTOCKMAGS19 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T19-QSTOCKDEPS          PIC ZZZZ9.                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  ISM145-T19-QSTOCKMAGS          PIC ZZZZ9.                    
AL1008*        10  FILLER                         PIC X(33) VALUE               
AL1008*    ' !                 !            !'.                                 
AL1008         10  FILLER                         PIC X(39) VALUE               
AL1008     ' !                 !       !          !'.                           
      *--------------------------- LIGNE T21  --------------------------        
           05  ISM145-T21.                                                      
AL1008*        10  FILLER                         PIC X(58) VALUE               
AL1008*    '!                                   !        !         !  '.        
AL1008         10  FILLER                         PIC X(52) VALUE               
AL1008     '!                             !        !         !  '.              
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                          !       !       '.        
AL1008*        10  FILLER                         PIC X(24) VALUE               
AL1008*    '          !            !'.                                          
AL1008         10  FILLER                         PIC X(30) VALUE               
AL1008     '          !       !          !'.                                    
      *--------------------------- LIGNE T22  --------------------------        
           05  ISM145-T22.                                                      
AL1008*        10  FILLER                         PIC X(57) VALUE               
AL1008*    '!                   TOTAL FAMILLE   !        !         ! '.         
AL1008         10  FILLER                         PIC X(51) VALUE               
AL1008     '!             TOTAL FAMILLE   !        !         ! '.               
               10  ISM145-T22-QVTEF1              PIC ZZZ9.                     
               10  ISM145-T22-X1  REDEFINES ISM145-T22-QVTEF1  PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T22-QVTEF2              PIC ZZZ9.                     
               10  ISM145-T22-X2  REDEFINES ISM145-T22-QVTEF2  PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T22-QVTEF3              PIC ZZZ9.                     
               10  ISM145-T22-X3  REDEFINES ISM145-T22-QVTEF3  PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T22-QVTEF4              PIC ZZZ9.                     
               10  ISM145-T22-X4  REDEFINES ISM145-T22-QVTEF4  PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T22-QVTEF5              PIC ZZZ9.                     
               10  ISM145-T22-X5  REDEFINES ISM145-T22-QVTEF5  PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T22-QVTEF6              PIC ZZZ9.                     
               10  ISM145-T22-X6  REDEFINES ISM145-T22-QVTEF6  PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T22-QVTEF7              PIC ZZZ9.                     
               10  ISM145-T22-X7  REDEFINES ISM145-T22-QVTEF7  PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T22-QVTEF8              PIC ZZZ9.                     
               10  ISM145-T22-X8  REDEFINES ISM145-T22-QVTEF8  PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T22-QVTEF9              PIC ZZZ9.                     
               10  ISM145-T22-X9  REDEFINES ISM145-T22-QVTEF9  PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T22-QVTEF10             PIC ZZZ9.                     
               10  ISM145-T22-X10 REDEFINES ISM145-T22-QVTEF10 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T22-QVTEF11             PIC ZZZ9.                     
               10  ISM145-T22-X11 REDEFINES ISM145-T22-QVTEF11 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T22-QVTEF12             PIC ZZZ9.                     
               10  ISM145-T22-X12 REDEFINES ISM145-T22-QVTEF12 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T22-QVTEF13             PIC ZZZ9.                     
               10  ISM145-T22-X13 REDEFINES ISM145-T22-QVTEF13 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T22-QVTEF14             PIC ZZZ9.                     
               10  ISM145-T22-X14 REDEFINES ISM145-T22-QVTEF14 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T22-QVTEF15             PIC ZZZ9.                     
               10  ISM145-T22-X15 REDEFINES ISM145-T22-QVTEF15 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T22-QVTEF16             PIC ZZZ9.                     
               10  ISM145-T22-X16 REDEFINES ISM145-T22-QVTEF16 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T22-QVTEF17             PIC ZZZ9.                     
               10  ISM145-T22-X17 REDEFINES ISM145-T22-QVTEF17 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T22-QVTEF18             PIC ZZZ9.                     
               10  ISM145-T22-X18 REDEFINES ISM145-T22-QVTEF18 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T22-QVTEF19             PIC ZZZ9.                     
               10  ISM145-T22-X19 REDEFINES ISM145-T22-QVTEF19 PIC X(4).        
               10  FILLER                         PIC X(09) VALUE               
           '       ! '.                                                         
               10  ISM145-T22-QVTEF               PIC ZZZZ9.                    
AL1008*        10  FILLER                         PIC X(33) VALUE               
AL1008*    ' !                 !            !'.                                 
AL1008         10  FILLER                         PIC X(39) VALUE               
AL1008     ' !                 !       !          !'.                           
      *--------------------------- LIGNE T23  --------------------------        
           05  ISM145-T23.                                                      
AL1008*        10  FILLER                         PIC X(57) VALUE               
AL1008*    '!                                   !        !         ! '.         
AL1008         10  FILLER                         PIC X(51) VALUE               
AL1008     '!                             !        !         ! '.               
               10  ISM145-T23-QSTOCKMAGF1         PIC ZZZ9.                     
               10  ISM145-1  REDEFINES ISM145-T23-QSTOCKMAGF1  PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKMAGF2         PIC ZZZ9.                     
               10  ISM145-2  REDEFINES ISM145-T23-QSTOCKMAGF2  PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKMAGF3         PIC ZZZ9.                     
               10  ISM145-3  REDEFINES ISM145-T23-QSTOCKMAGF3  PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKMAGF4         PIC ZZZ9.                     
               10  ISM145-4  REDEFINES ISM145-T23-QSTOCKMAGF4  PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKMAGF5         PIC ZZZ9.                     
               10  ISM145-5  REDEFINES ISM145-T23-QSTOCKMAGF5  PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKMAGF6         PIC ZZZ9.                     
               10  ISM145-6  REDEFINES ISM145-T23-QSTOCKMAGF6  PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKMAGF7         PIC ZZZ9.                     
               10  ISM145-7  REDEFINES ISM145-T23-QSTOCKMAGF7  PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKMAGF8         PIC ZZZ9.                     
               10  ISM145-8  REDEFINES ISM145-T23-QSTOCKMAGF8  PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKMAGF9         PIC ZZZ9.                     
               10  ISM145-9  REDEFINES ISM145-T23-QSTOCKMAGF9  PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKMAGF10        PIC ZZZ9.                     
               10  ISM145-10 REDEFINES ISM145-T23-QSTOCKMAGF10 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKMAGF11        PIC ZZZ9.                     
               10  ISM145-11 REDEFINES ISM145-T23-QSTOCKMAGF11 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKMAGF12        PIC ZZZ9.                     
               10  ISM145-12 REDEFINES ISM145-T23-QSTOCKMAGF12 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKMAGF13        PIC ZZZ9.                     
               10  ISM145-13 REDEFINES ISM145-T23-QSTOCKMAGF13 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKMAGF14        PIC ZZZ9.                     
               10  ISM145-14 REDEFINES ISM145-T23-QSTOCKMAGF14 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKMAGF15        PIC ZZZ9.                     
               10  ISM145-15 REDEFINES ISM145-T23-QSTOCKMAGF15 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKMAGF16        PIC ZZZ9.                     
               10  ISM145-16 REDEFINES ISM145-T23-QSTOCKMAGF16 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKMAGF17        PIC ZZZ9.                     
               10  ISM145-17 REDEFINES ISM145-T23-QSTOCKMAGF17 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKMAGF18        PIC ZZZ9.                     
               10  ISM145-18 REDEFINES ISM145-T23-QSTOCKMAGF18 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKMAGF19        PIC ZZZ9.                     
               10  ISM145-19 REDEFINES ISM145-T23-QSTOCKMAGF19 PIC X(4).        
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  ISM145-T23-QSTOCKDEPF          PIC ZZZZ9.                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  ISM145-T23-QSTOCKMAGF          PIC ZZZZ9.                    
AL1008*        10  FILLER                         PIC X(33) VALUE               
AL1008*    ' !                 !            !'.                                 
AL1008         10  FILLER                         PIC X(39) VALUE               
AL1008     ' !                 !       !          !'.                           
      *--------------------------- LIGNE T25  --------------------------        
           05  ISM145-T25.                                                      
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '!                                   !        !         !  '.        
AL1008     '!                             !        !         !        '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '                                          !       !       '.        
AL1008     '                                    !       !             '.        
               10  FILLER                         PIC X(24) VALUE               
AL1008*    '          !            !'.                                          
AL1008     '    !       !          !'.                                          
      *--------------------------- LIGNE B05  --------------------------        
           05  ISM145-B05.                                                      
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '+-----------------------------------+--------+---------+--'.        
AL1008     '+-----------------------------+--------+---------+--------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
AL1008*    '------------------------------------------+-------+-------'.        
AL1008     '------------------------------------+-------+-------------'.        
               10  FILLER                         PIC X(24) VALUE               
AL1008*    '----------+------------+'.                                          
AL1008     '----+-------+----------+'.                                          
      *                                                                         
      *---------------------------------------------------------------*         
      *           D E S C R I P T I O N    D E S    T A B L E S       *         
      *---------------------------------------------------------------*         
      *                                                                         
      *                                                                         
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *****************************************************************         
      *              T R A M E   DU   P R O G R A M M E               *         
      *****************************************************************         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *           ------------------------------------------          *         
      *           --  MODULE DE BASE DU PROGRAMME  BSM146 --          *         
      *           ------------------------------------------          *         
      *                               I                               *         
      *           -----------------------------------------           *         
      *           I                   I                   I           *         
      *   -----------------   -----------------   -----------------   *         
      *   -    DEBUT      -   -  TRAITEMENT   -   -      FIN      -   *         
      *   -----------------   -----------------   -----------------   *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-BSM146 SECTION.                                                   
      *                                                                         
           PERFORM  DEBUT-BSM146.                                               
           PERFORM  TRAIT-BSM146.                                               
           PERFORM  FIN-BSM146.                                                 
      *                                                                         
       F-MODULE-BSM146. EXIT.                                                   
      *                                                                         
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *                    D E B U T        B G P 8 9 0               *         
      *---------------------------------------------------------------*         
      *                                                                         
       DEBUT-BSM146 SECTION.                                                    
      *                                                                         
           DISPLAY '*** DEBUT BSM146 ***'.                                      
           MOVE WHEN-COMPILED   TO Z-WHEN-COMPILED.                             
           STRING Z-WHEN-COMPILED(4:2) '/' Z-WHEN-COMPILED(1:2) '/'             
                  Z-WHEN-COMPILED(7:2) DELIMITED BY SIZE INTO WEDITIOX          
           DISPLAY                                                              
           '**      DATE DE COMPILE (JJ/MM/AA): '   WEDITIOX    '  **'.         
           DISPLAY '** VERSION DU 24/09/03 **'.                                 
           PERFORM OUVERTURE-FICHIERS.                                          
           PERFORM TRAIT-DATE.                                                  
      *                                                                         
       F-DEBUT-BSM146.                                                          
           EXIT.                                                                
      *                                                                         
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *                    T R A I T        B G P 8 9 0               *         
      *---------------------------------------------------------------*         
      *                                                                         
       TRAIT-BSM146 SECTION.                                                    
      *                                                                         
           INITIALIZE W-TSEG W-TFAM                                             
           PERFORM LECTURE-FSM145.                                              
EOF        PERFORM UNTIL EOF                                                    
              PERFORM RUPTURE-ENTETE                                            
ENTETE        PERFORM UNTIL                                                     
                    FSM145-NSOCIETE  NOT = W-FSM145-NSOCIETE                    
                 OR FSM145-GRP       NOT = W-FSM145-GRP                         
                 OR FSM145-CHEFPROD  NOT = W-FSM145-CHEFPROD                    
                 OR FSM145-LGDRAYON  NOT = W-FSM145-LGDRAYON                    
                 OR EOF                                                         
                 PERFORM RUPTURE-FAMILLE                                        
FAMILL           PERFORM UNTIL                                                  
                       FSM145-NSOCIETE  NOT = W-FSM145-NSOCIETE                 
                    OR FSM145-GRP       NOT = W-FSM145-GRP                      
                    OR FSM145-CHEFPROD  NOT = W-FSM145-CHEFPROD                 
                    OR FSM145-LGDRAYON  NOT = W-FSM145-LGDRAYON                 
                    OR FSM145-WSEQFAM   NOT = W-FSM145-WSEQFAM                  
                    OR FSM145-LFAM      NOT = W-FSM145-LFAM                     
                    OR FSM145-CFAM      NOT = W-FSM145-CFAM                     
                    OR EOF                                                      
                    PERFORM RUPTURE-SEGMENT                                     
                    PERFORM ECRITURE-ENTETE                                     
SEGMEN              PERFORM UNTIL                                               
                          FSM145-NSOCIETE  NOT = W-FSM145-NSOCIETE              
                       OR FSM145-GRP       NOT = W-FSM145-GRP                   
                       OR FSM145-CHEFPROD  NOT = W-FSM145-CHEFPROD              
                       OR FSM145-LGDRAYON  NOT = W-FSM145-LGDRAYON              
                       OR FSM145-WSEQFAM   NOT = W-FSM145-WSEQFAM               
                       OR FSM145-LFAM      NOT = W-FSM145-LFAM                  
                       OR FSM145-CFAM      NOT = W-FSM145-CFAM                  
                       OR FSM145-LVMARKET1 NOT = W-FSM145-LVMARKET1             
                       OR FSM145-LVMARKET2 NOT = W-FSM145-LVMARKET2             
                       OR FSM145-LVMARKET3 NOT = W-FSM145-LVMARKET3             
                       OR EOF                                                   
                       PERFORM CUMUL-TOTAUX                                     
                       PERFORM ECRITURE-ORDRE                                   
                       PERFORM RUPTURE-ORDRE                                    
ORDRE                  PERFORM UNTIL                                            
                             FSM145-NSOCIETE   NOT = W-FSM145-NSOCIETE          
                          OR FSM145-GRP        NOT = W-FSM145-GRP               
                          OR FSM145-CHEFPROD   NOT = W-FSM145-CHEFPROD          
                          OR FSM145-LGDRAYON   NOT = W-FSM145-LGDRAYON          
                          OR FSM145-WSEQFAM    NOT = W-FSM145-WSEQFAM           
                          OR FSM145-LFAM       NOT = W-FSM145-LFAM              
                          OR FSM145-CFAM       NOT = W-FSM145-CFAM              
                          OR FSM145-LVMARKET1  NOT = W-FSM145-LVMARKET1         
                          OR FSM145-LVMARKET2  NOT = W-FSM145-LVMARKET2         
                          OR FSM145-LVMARKET3  NOT = W-FSM145-LVMARKET3         
                          OR FSM145-RANGGPE    NOT = W-FSM145-RANGGPE           
                          OR FSM145-ORDRE      NOT = W-FSM145-ORDRE             
                          OR EOF                                                
                             PERFORM ECRITURE-CODICGRP                          
                             PERFORM LECTURE-FSM145                             
ORDRE                  END-PERFORM                                              
SEGMEN              END-PERFORM                                                 
                    PERFORM ECRITURE-TOTAUX-SEG                                 
FAMILL           END-PERFORM                                                    
                 PERFORM ECRITURE-TOTAUX-FAM                                    
ENTETE        END-PERFORM                                                       
EOF        END-PERFORM.                                                         
       FIN-TRAIT-BSM146. EXIT.                                                  
      *                                                                         
       FIN-BSM146 SECTION.                                                      
      *                                                                         
           CLOSE FDATE ISM145GR ISM145BL ISM145BR ISM145AC FSM145 .             
           DISPLAY '**********************************************'.            
           DISPLAY '*    PROGRAMME BSM146  TERMINE NORMALEMENT   *'.            
           DISPLAY '*                                            *'.            
           DISPLAY '*  NOMBRE D''ENREG TRAITES SUR FSM145 = ' CTR-FSM145        
                   ' *'.                                                        
           DISPLAY '*  NOMBRE DE LIGNES ECRITES GRIS      = ' CTR-GRIS          
                   ' *'.                                                        
           DISPLAY '*  NOMBRE DE LIGNES ECRITES BRUN      = ' CTR-BRUN          
                   ' *'.                                                        
           DISPLAY '*  NOMBRE DE LIGNES ECRITES BLANC     = ' CTR-BLAN          
                   ' *'.                                                        
           DISPLAY '*  NOMBRE DE LIGNES ECRITES ACCESSOIRE= ' CTR-ACCE          
                   ' *'.                                                        
           DISPLAY '*                                            *'.            
           DISPLAY '**********************************************'.            
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-FIN-BSM146. EXIT.                                                    
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *         M O D U L E S    D E B U T  B G P 8 9 0               *         
      *---------------------------------------------------------------*         
      *                                                                         
       OUVERTURE-FICHIERS SECTION.                                              
      *-------------------                                                      
      *                                                                         
           OPEN INPUT FDATE FSM145.                                             
           OPEN OUTPUT ISM145GR ISM145BR ISM145BL ISM145AC.                     
       FIN-OUVERTURE-FICHIERS. EXIT.                                            
      *                                                                         
       TRAIT-DATE SECTION.                                                      
      *-----------                                                              
           READ FDATE                                                           
           AT END MOVE 'FICHIER FDATE VIDE ' TO ABEND-MESS                      
                  PERFORM FIN-ANORMALE.                                         
           MOVE '1'                 TO GFDATA.                                  
           MOVE FDATE-ENREG         TO GFJJMMSSAA.                              
           CALL 'BETDATC' USING WORK-BETDATC.                                   
           IF GFVDAT NOT = '1'                                                  
              DISPLAY 'DATE DE TRAITEMENT INVALIDE '                            
              MOVE '** DATE DE TRAITEMENT INVALIDE **' TO ABEND-MESS            
              PERFORM FIN-ANORMALE                                              
           ELSE                                                                 
              DISPLAY '** FDATE  ** ' GFJMSA-5                                  
           END-IF.                                                              
           COMPUTE GFQNT0 = GFQNT0 + 1 .                                        
           MOVE '3' TO GFDATA.                                                  
           CALL 'BETDATC' USING WORK-BETDATC.                                   
           IF GFVDAT NOT = '1'                                                  
              MOVE 'DATE CARTE PARAMETRE ERRONEE ' TO ABEND-MESS                
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
           MOVE GFJJMMSSAA       TO DATJMSA.                                    
           MOVE GFSMN            TO JOUR-SEM.                                   
           DISPLAY 'DATE RECEPTION ETAT DANS BOITE EOS : ' DATJMSA.             
           ACCEPT W-TIME FROM TIME.                                             
       FIN-TRAIT-DATE. EXIT.                                                    
      *                                                                         
      *---------------------------------------------------------------*         
      *         M O D U L E S    T R A I T  B G P 8 7 0               *         
      *---------------------------------------------------------------*         
      *                                                                         
       LECTURE-FSM145 SECTION.                                                  
      *----------------                                                         
           MOVE DSECT-FSM145 TO DSECT-W-FSM145.                                 
           INITIALIZE DSECT-FSM145.                                             
           READ FSM145                                                          
                INTO DSECT-FSM145                                               
                AT END SET EOF TO TRUE                                          
           END-READ                                                             
           IF NOT EOF                                                           
              IF CTR-FSM145 = 0                                                 
                 MOVE DSECT-FSM145 TO DSECT-W-FSM145                            
              END-IF                                                            
              ADD 1 TO CTR-FSM145                                               
           END-IF.                                                              
       FIN-LECTURE-FSM145. EXIT.                                                
      *                                                                         
       RUPTURE-ENTETE   SECTION.                                                
           IF FSM145-NSOCIETE  NOT = W-FSM145-NSOCIETE                          
           OR FSM145-GRP       NOT = W-FSM145-GRP                               
           OR FSM145-LGDRAYON  NOT = W-FSM145-LGDRAYON                          
              MOVE 0           TO CTR-PAGE                                      
           END-IF.                                                              
           MOVE FSM145-NSOCIETE  TO W-FSM145-NSOCIETE                           
           MOVE FSM145-GRP       TO W-FSM145-GRP                                
           MOVE FSM145-CHEFPROD  TO W-FSM145-CHEFPROD.                          
           MOVE FSM145-LGDRAYON  TO W-FSM145-LGDRAYON.                          
       FIN-RUPTURE-ENTETE.    EXIT.                                             
      *                                                                         
       RUPTURE-FAMILLE  SECTION.                                                
           MOVE FSM145-NSOCIETE  TO W-FSM145-NSOCIETE                           
           MOVE FSM145-GRP       TO W-FSM145-GRP                                
           MOVE FSM145-CHEFPROD  TO W-FSM145-CHEFPROD                           
           MOVE FSM145-LGDRAYON  TO W-FSM145-LGDRAYON.                          
           MOVE FSM145-WSEQFAM   TO W-FSM145-WSEQFAM                            
           MOVE FSM145-CFAM      TO W-FSM145-CFAM                               
           MOVE FSM145-LFAM      TO W-FSM145-LFAM.                              
       FIN-RUPTURE-FAMILLE.   EXIT.                                             
      *                                                                         
       RUPTURE-SEGMENT  SECTION.                                                
           MOVE FSM145-NSOCIETE  TO W-FSM145-NSOCIETE                           
           MOVE FSM145-GRP       TO W-FSM145-GRP                                
           MOVE FSM145-CHEFPROD  TO W-FSM145-CHEFPROD                           
           MOVE FSM145-LGDRAYON  TO W-FSM145-LGDRAYON.                          
           MOVE FSM145-WSEQFAM   TO W-FSM145-WSEQFAM                            
           MOVE FSM145-CFAM      TO W-FSM145-CFAM                               
           MOVE FSM145-LFAM      TO W-FSM145-LFAM                               
           MOVE FSM145-LVMARKET1 TO W-FSM145-LVMARKET1                          
           MOVE FSM145-LVMARKET2 TO W-FSM145-LVMARKET2                          
           MOVE FSM145-LVMARKET3 TO W-FSM145-LVMARKET3.                         
       FIN-RUPTURE-SEGMENT.   EXIT.                                             
      *                                                                         
       RUPTURE-ORDRE SECTION.                                                   
           MOVE FSM145-NSOCIETE   TO W-FSM145-NSOCIETE                          
           MOVE FSM145-GRP        TO W-FSM145-GRP                               
           MOVE FSM145-CHEFPROD   TO W-FSM145-CHEFPROD                          
           MOVE FSM145-LGDRAYON   TO W-FSM145-LGDRAYON.                         
           MOVE FSM145-WSEQFAM    TO W-FSM145-WSEQFAM                           
           MOVE FSM145-LFAM       TO W-FSM145-LFAM                              
           MOVE FSM145-CFAM       TO W-FSM145-CFAM                              
           MOVE FSM145-LVMARKET1  TO W-FSM145-LVMARKET1                         
           MOVE FSM145-LVMARKET2  TO W-FSM145-LVMARKET2                         
           MOVE FSM145-LVMARKET3  TO W-FSM145-LVMARKET3.                        
           MOVE FSM145-RANGGPE    TO W-FSM145-RANGGPE                           
           MOVE FSM145-ORDRE      TO W-FSM145-ORDRE.                            
       FIN-RUPTURE-ORDRE. EXIT.                                                 
      *                                                                         
       ECRITURE-ENTETE SECTION.                                                 
      *-----------------                                                        
           IF CTR-PAGE > 0                                                      
              MOVE ' '                   TO SAUT                                
              MOVE ISM145-B05            TO LIGNE                               
              PERFORM WRITE-ISM145                                              
              ADD 1 TO CTR-LIGNE                                                
           END-IF                                                               
      *                                                                         
           ADD 1                      TO CTR-PAGE.                              
           MOVE 0                     TO CTR-LIGNE.                             
      *                                                                         
           MOVE '1'                   TO SAUT.                                  
           MOVE ISM145-E01            TO LIGNE.                                 
           PERFORM WRITE-ISM145.                                                
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE FSM145-NSOCIETE       TO ISM145-E03-NSOCIETE.                   
           MOVE FSM145-LGDRAYON       TO ISM145-E03-LGDRAYON.                   
           MOVE CTR-PAGE              TO ISM145-E03-NOPAGE.                     
           MOVE ' '                   TO SAUT.                                  
           MOVE ISM145-E03            TO LIGNE.                                 
           PERFORM WRITE-ISM145.                                                
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE FSM145-LIBDATE        TO ISM145-E05-LIBDATE.                    
           MOVE FSM145-GRP            TO ISM145-E05-GRP.                        
           MOVE FSM145-NZONPRIX       TO ISM145-E05-NZONPRIX                    
           MOVE ' '                   TO SAUT.                                  
           MOVE ISM145-E05            TO LIGNE.                                 
           PERFORM WRITE-ISM145.                                                
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE FSM145-CHEFPROD       TO ISM145-E15-CHEFPROD                    
           MOVE ' '                   TO SAUT.                                  
           MOVE ISM145-E15            TO LIGNE.                                 
           PERFORM WRITE-ISM145.                                                
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE ' '                   TO SAUT.                                  
           MOVE ISM145-E40            TO LIGNE.                                 
           PERFORM WRITE-ISM145.                                                
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE ' '                   TO SAUT.                                  
           MOVE ISM145-E45            TO LIGNE.                                 
           PERFORM WRITE-ISM145.                                                
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE FSM145-MAG1           TO ISM145-E50-MAG1                        
           MOVE FSM145-MAG2           TO ISM145-E50-MAG2                        
           MOVE FSM145-MAG3           TO ISM145-E50-MAG3                        
           MOVE FSM145-MAG4           TO ISM145-E50-MAG4                        
           MOVE FSM145-MAG5           TO ISM145-E50-MAG5                        
           MOVE FSM145-MAG6           TO ISM145-E50-MAG6                        
           MOVE FSM145-MAG7           TO ISM145-E50-MAG7                        
           MOVE FSM145-MAG8           TO ISM145-E50-MAG8                        
           MOVE FSM145-MAG9           TO ISM145-E50-MAG9                        
           MOVE FSM145-MAG10          TO ISM145-E50-MAG10                       
           MOVE FSM145-MAG11          TO ISM145-E50-MAG11                       
           MOVE FSM145-MAG12          TO ISM145-E50-MAG12                       
           MOVE FSM145-MAG13          TO ISM145-E50-MAG13                       
           MOVE FSM145-MAG14          TO ISM145-E50-MAG14                       
           MOVE FSM145-MAG15          TO ISM145-E50-MAG15                       
           MOVE FSM145-MAG16          TO ISM145-E50-MAG16                       
           MOVE FSM145-MAG17          TO ISM145-E50-MAG17                       
           MOVE FSM145-MAG18          TO ISM145-E50-MAG18                       
           MOVE FSM145-MAG19          TO ISM145-E50-MAG19                       
           MOVE ' '                   TO SAUT.                                  
           MOVE ISM145-E50            TO LIGNE.                                 
           PERFORM WRITE-ISM145.                                                
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE ' '                   TO SAUT.                                  
           MOVE ISM145-E55            TO LIGNE.                                 
           PERFORM WRITE-ISM145.                                                
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE FSM145-LFAM           TO ISM145-E60-LFAM                        
           MOVE FSM145-GAMME1         TO ISM145-E60-GAMME1                      
           MOVE FSM145-GAMME2         TO ISM145-E60-GAMME2                      
           MOVE FSM145-GAMME3         TO ISM145-E60-GAMME3                      
           MOVE FSM145-GAMME4         TO ISM145-E60-GAMME4                      
           MOVE FSM145-GAMME5         TO ISM145-E60-GAMME5                      
           MOVE FSM145-GAMME6         TO ISM145-E60-GAMME6                      
           MOVE FSM145-GAMME7         TO ISM145-E60-GAMME7                      
           MOVE FSM145-GAMME8         TO ISM145-E60-GAMME8                      
           MOVE FSM145-GAMME9         TO ISM145-E60-GAMME9                      
           MOVE FSM145-GAMME10        TO ISM145-E60-GAMME10                     
           MOVE FSM145-GAMME11        TO ISM145-E60-GAMME11                     
           MOVE FSM145-GAMME12        TO ISM145-E60-GAMME12                     
           MOVE FSM145-GAMME13        TO ISM145-E60-GAMME13                     
           MOVE FSM145-GAMME14        TO ISM145-E60-GAMME14                     
           MOVE FSM145-GAMME15        TO ISM145-E60-GAMME15                     
           MOVE FSM145-GAMME16        TO ISM145-E60-GAMME16                     
           MOVE FSM145-GAMME17        TO ISM145-E60-GAMME17                     
           MOVE FSM145-GAMME18        TO ISM145-E60-GAMME18                     
           MOVE FSM145-GAMME19        TO ISM145-E60-GAMME19                     
           MOVE ' '                   TO SAUT.                                  
           MOVE ISM145-E60            TO LIGNE.                                 
           PERFORM WRITE-ISM145.                                                
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           IF FSM145-LVMARKET1 > SPACES                                         
              MOVE ' '                   TO SAUT                                
              MOVE ISM145-E61            TO LIGNE                               
              PERFORM WRITE-ISM145                                              
              ADD 1 TO CTR-LIGNE                                                
           END-IF                                                               
      *                                                                         
           IF FSM145-LVMARKET1 > SPACES                                         
              MOVE ' '                   TO SAUT                                
              MOVE ISM145-E62            TO LIGNE                               
              PERFORM WRITE-ISM145                                              
              ADD 1 TO CTR-LIGNE                                                
           END-IF                                                               
      *                                                                         
           IF FSM145-LVMARKET1 > SPACES                                         
              MOVE FSM145-LVMARKET1      TO ISM145-E65-LVMARKET1                
              MOVE ' '                   TO SAUT                                
              MOVE ISM145-E65            TO LIGNE                               
              PERFORM WRITE-ISM145                                              
              ADD 1 TO CTR-LIGNE                                                
           END-IF                                                               
      *                                                                         
           IF FSM145-LVMARKET2 > SPACES                                         
              MOVE FSM145-LVMARKET2      TO ISM145-E66-LVMARKET2                
              MOVE ' '                   TO SAUT                                
              MOVE ISM145-E66            TO LIGNE                               
              PERFORM WRITE-ISM145                                              
              ADD 1 TO CTR-LIGNE                                                
           END-IF.                                                              
      *                                                                         
           IF FSM145-LVMARKET3 > SPACES                                         
              MOVE FSM145-LVMARKET3      TO ISM145-E67-LVMARKET3                
              MOVE ' '                   TO SAUT                                
              MOVE ISM145-E67            TO LIGNE                               
              PERFORM WRITE-ISM145                                              
              ADD 1 TO CTR-LIGNE                                                
           END-IF.                                                              
      *                                                                         
           IF FSM145-LVMARKET1 > SPACES                                         
              MOVE ' '                   TO SAUT                                
              MOVE ISM145-E68            TO LIGNE                               
              PERFORM WRITE-ISM145                                              
              ADD 1 TO CTR-LIGNE                                                
           END-IF.                                                              
      *                                                                         
       FIN-ECRITURE-ENTETE. EXIT.                                               
      *                                                                         
       ECRITURE-ORDRE      SECTION.                                             
      *-----------------                                                        
      *                                                                         
           IF CTR-LIGNE >  MAX-LIGNE1                                           
              PERFORM ECRITURE-ENTETE                                           
           END-IF                                                               
      *                                                                         
           MOVE ' '                   TO SAUT                                   
           MOVE ISM145-D04            TO LIGNE                                  
           PERFORM WRITE-ISM145                                                 
           ADD 1 TO CTR-LIGNE                                                   
      *                                                                         
           MOVE FSM145-NCODIC         TO ISM145-D05-NCODIC                      
           MOVE FSM145-CMARQ          TO ISM145-D05-CMARQ                       
AL1008*    MOVE FSM145-LREFFOURN      TO ISM145-D05-LREFFOURN                   
           MOVE FSM145-LREFFOURN      TO ISM145-D06-LREFFOURN                   
           MOVE FSM145-RANGGPEAFF     TO ISM145-D05-RANGGPEAFF                  
           MOVE FSM145-RANGSOCAFF     TO ISM145-D05-RANGSOCAFF                  
           MOVE FSM145-WOA            TO ISM145-D05-WOA                         
           MOVE FSM145-WSENSAPPRO     TO ISM145-D05-WSENSAPPRO                  
           MOVE FSM145-LSTATCOMP      TO ISM145-D05-LSTATCOMP                   
           MOVE FSM145-QVTE1          TO ISM145-D05-QVTE1                       
           MOVE FSM145-QVTE2          TO ISM145-D05-QVTE2                       
           MOVE FSM145-QVTE3          TO ISM145-D05-QVTE3                       
           MOVE FSM145-QVTE4          TO ISM145-D05-QVTE4                       
           MOVE FSM145-QVTE5          TO ISM145-D05-QVTE5                       
           MOVE FSM145-QVTE6          TO ISM145-D05-QVTE6                       
           MOVE FSM145-QVTE7          TO ISM145-D05-QVTE7                       
           MOVE FSM145-QVTE8          TO ISM145-D05-QVTE8                       
           MOVE FSM145-QVTE9          TO ISM145-D05-QVTE9                       
           MOVE FSM145-QVTE10         TO ISM145-D05-QVTE10                      
           MOVE FSM145-QVTE11         TO ISM145-D05-QVTE11                      
           MOVE FSM145-QVTE12         TO ISM145-D05-QVTE12                      
           MOVE FSM145-QVTE13         TO ISM145-D05-QVTE13                      
           MOVE FSM145-QVTE14         TO ISM145-D05-QVTE14                      
           MOVE FSM145-QVTE15         TO ISM145-D05-QVTE15                      
           MOVE FSM145-QVTE16         TO ISM145-D05-QVTE16                      
           MOVE FSM145-QVTE17         TO ISM145-D05-QVTE17                      
           MOVE FSM145-QVTE18         TO ISM145-D05-QVTE18                      
           MOVE FSM145-QVTE19         TO ISM145-D05-QVTE19                      
           MOVE FSM145-QVTETOT        TO ISM145-D05-QVTETOT                     
           MOVE FSM145-PVREFNAT       TO ISM145-D05-PVREFNAT                    
AL1008     MOVE FSM145-MARGE          TO ISM145-D05-MARGE                       
           MOVE FSM145-LIBELLE        TO ISM145-D05-LIBELLE                     
           MOVE ' '                   TO SAUT.                                  
           MOVE ISM145-D05            TO LIGNE.                                 
           PERFORM WRITE-ISM145.                                                
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE ' '                   TO SAUT.                                  
           IF FSM145-MAG1 > SPACES                                              
              MOVE FSM145-QSTOCKMAG1     TO ISM145-D06-QSTOCKMAG1               
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X1                           
           END-IF                                                               
           IF FSM145-MAG2 > SPACES                                              
              MOVE FSM145-QSTOCKMAG2     TO ISM145-D06-QSTOCKMAG2               
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X2                           
           END-IF                                                               
           IF FSM145-MAG3 > SPACES                                              
              MOVE FSM145-QSTOCKMAG3     TO ISM145-D06-QSTOCKMAG3               
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X3                           
           END-IF                                                               
           IF FSM145-MAG4 > SPACES                                              
              MOVE FSM145-QSTOCKMAG4     TO ISM145-D06-QSTOCKMAG4               
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X4                           
           END-IF                                                               
           IF FSM145-MAG5 > SPACES                                              
              MOVE FSM145-QSTOCKMAG5     TO ISM145-D06-QSTOCKMAG5               
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X5                           
           END-IF                                                               
           IF FSM145-MAG6 > SPACES                                              
              MOVE FSM145-QSTOCKMAG6     TO ISM145-D06-QSTOCKMAG6               
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X6                           
           END-IF                                                               
           IF FSM145-MAG7 > SPACES                                              
              MOVE FSM145-QSTOCKMAG7     TO ISM145-D06-QSTOCKMAG7               
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X7                           
           END-IF                                                               
           IF FSM145-MAG8 > SPACES                                              
              MOVE FSM145-QSTOCKMAG8     TO ISM145-D06-QSTOCKMAG8               
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X8                           
           END-IF                                                               
           IF FSM145-MAG9 > SPACES                                              
              MOVE FSM145-QSTOCKMAG9     TO ISM145-D06-QSTOCKMAG9               
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X9                           
           END-IF                                                               
           IF FSM145-MAG10 > SPACES                                             
              MOVE FSM145-QSTOCKMAG10    TO ISM145-D06-QSTOCKMAG10              
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X10                          
           END-IF                                                               
           IF FSM145-MAG11 > SPACES                                             
              MOVE FSM145-QSTOCKMAG11    TO ISM145-D06-QSTOCKMAG11              
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X11                          
           END-IF                                                               
           IF FSM145-MAG12 > SPACES                                             
              MOVE FSM145-QSTOCKMAG12    TO ISM145-D06-QSTOCKMAG12              
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X12                          
           END-IF                                                               
           IF FSM145-MAG13 > SPACES                                             
              MOVE FSM145-QSTOCKMAG13    TO ISM145-D06-QSTOCKMAG13              
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X13                          
           END-IF                                                               
           IF FSM145-MAG14 > SPACES                                             
              MOVE FSM145-QSTOCKMAG14    TO ISM145-D06-QSTOCKMAG14              
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X14                          
           END-IF                                                               
           IF FSM145-MAG15 > SPACES                                             
              MOVE FSM145-QSTOCKMAG15    TO ISM145-D06-QSTOCKMAG15              
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X15                          
           END-IF                                                               
           IF FSM145-MAG16 > SPACES                                             
              MOVE FSM145-QSTOCKMAG16    TO ISM145-D06-QSTOCKMAG16              
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X16                          
           END-IF                                                               
           IF FSM145-MAG17 > SPACES                                             
              MOVE FSM145-QSTOCKMAG17    TO ISM145-D06-QSTOCKMAG17              
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X17                          
           END-IF                                                               
           IF FSM145-MAG18 > SPACES                                             
              MOVE FSM145-QSTOCKMAG18    TO ISM145-D06-QSTOCKMAG18              
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X18                          
           END-IF                                                               
           IF FSM145-MAG19 > SPACES                                             
              MOVE FSM145-QSTOCKMAG19    TO ISM145-D06-QSTOCKMAG19              
           ELSE                                                                 
              MOVE SPACES                TO ISM145-X19                          
           END-IF                                                               
           MOVE FSM145-QSTOCKDEP      TO ISM145-D06-QSTOCKDEP                   
           MOVE FSM145-QSTOCKMAGTOT   TO ISM145-D06-QSTOCKMAGTOT                
           MOVE FSM145-PSTDTTC        TO ISM145-D06-PSTDTTC                     
           MOVE FSM145-PRIME          TO ISM145-D06-PRIME                       
AL1008     MOVE FSM145-TXMARGE        TO ISM145-D06-TXMARGE                     
           IF FSM145-WCDE = 'O'                                                 
              MOVE FSM145-QCDE           TO ISM145-D06-QCDE                     
           ELSE                                                                 
              MOVE SPACES                TO ISM145-D06-X                        
           END-IF                                                               
           MOVE ISM145-D06            TO LIGNE.                                 
           PERFORM WRITE-ISM145.                                                
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
      *                                                                         
       FIN-ECRITURE-ORDRE. EXIT.                                                
      *                                                                         
       ECRITURE-CODICGRP   SECTION.                                             
      *-----------------                                                        
      *                                                                         
            IF FSM145-NCODICGRP > SPACES                                        
              IF CTR-LIGNE >  MAX-LIGNE3                                        
                 PERFORM ECRITURE-ENTETE                                        
              END-IF                                                            
      *                                                                         
              MOVE FSM145-NCODICGRP      TO ISM145-D07-NCODICGRP                
              MOVE FSM145-LSTATCOMP2     TO ISM145-D07-LSTATCOMP2               
              MOVE ISM145-D07            TO LIGNE                               
              PERFORM WRITE-ISM145                                              
              ADD 1 TO CTR-LIGNE                                                
           END-IF.                                                              
      *                                                                         
      *                                                                         
       FIN-ECRITURE-CODICGRP. EXIT.                                             
       CUMUL-TOTAUX SECTION.                                                    
      *-----------------                                                        
           ADD FSM145-QVTE1        TO W-TSEG-QVTES1                             
           ADD FSM145-QVTE2        TO W-TSEG-QVTES2                             
           ADD FSM145-QVTE3        TO W-TSEG-QVTES3                             
           ADD FSM145-QVTE4        TO W-TSEG-QVTES4                             
           ADD FSM145-QVTE5        TO W-TSEG-QVTES5                             
           ADD FSM145-QVTE6        TO W-TSEG-QVTES6                             
           ADD FSM145-QVTE7        TO W-TSEG-QVTES7                             
           ADD FSM145-QVTE8        TO W-TSEG-QVTES8                             
           ADD FSM145-QVTE9        TO W-TSEG-QVTES9                             
           ADD FSM145-QVTE10       TO W-TSEG-QVTES10                            
           ADD FSM145-QVTE11       TO W-TSEG-QVTES11                            
           ADD FSM145-QVTE12       TO W-TSEG-QVTES12                            
           ADD FSM145-QVTE13       TO W-TSEG-QVTES13                            
           ADD FSM145-QVTE14       TO W-TSEG-QVTES14                            
           ADD FSM145-QVTE15       TO W-TSEG-QVTES15                            
           ADD FSM145-QVTE16       TO W-TSEG-QVTES16                            
           ADD FSM145-QVTE17       TO W-TSEG-QVTES17                            
           ADD FSM145-QVTE18       TO W-TSEG-QVTES18                            
           ADD FSM145-QVTE19       TO W-TSEG-QVTES19                            
           ADD FSM145-QVTETOT      TO W-TSEG-QVTES                              
           ADD FSM145-QSTOCKMAG1   TO W-TSEG-QSTOCKMAGS1                        
           ADD FSM145-QSTOCKMAG2   TO W-TSEG-QSTOCKMAGS2                        
           ADD FSM145-QSTOCKMAG3   TO W-TSEG-QSTOCKMAGS3                        
           ADD FSM145-QSTOCKMAG4   TO W-TSEG-QSTOCKMAGS4                        
           ADD FSM145-QSTOCKMAG5   TO W-TSEG-QSTOCKMAGS5                        
           ADD FSM145-QSTOCKMAG6   TO W-TSEG-QSTOCKMAGS6                        
           ADD FSM145-QSTOCKMAG7   TO W-TSEG-QSTOCKMAGS7                        
           ADD FSM145-QSTOCKMAG8   TO W-TSEG-QSTOCKMAGS8                        
           ADD FSM145-QSTOCKMAG9   TO W-TSEG-QSTOCKMAGS9                        
           ADD FSM145-QSTOCKMAG10  TO W-TSEG-QSTOCKMAGS10                       
           ADD FSM145-QSTOCKMAG11  TO W-TSEG-QSTOCKMAGS11                       
           ADD FSM145-QSTOCKMAG12  TO W-TSEG-QSTOCKMAGS12                       
           ADD FSM145-QSTOCKMAG13  TO W-TSEG-QSTOCKMAGS13                       
           ADD FSM145-QSTOCKMAG14  TO W-TSEG-QSTOCKMAGS14                       
           ADD FSM145-QSTOCKMAG15  TO W-TSEG-QSTOCKMAGS15                       
           ADD FSM145-QSTOCKMAG16  TO W-TSEG-QSTOCKMAGS16                       
           ADD FSM145-QSTOCKMAG17  TO W-TSEG-QSTOCKMAGS17                       
           ADD FSM145-QSTOCKMAG18  TO W-TSEG-QSTOCKMAGS18                       
           ADD FSM145-QSTOCKMAG19  TO W-TSEG-QSTOCKMAGS19                       
           ADD FSM145-QSTOCKDEP    TO W-TSEG-QSTOCKDEPS                         
           ADD FSM145-QSTOCKMAGTOT TO W-TSEG-QSTOCKMAGS                         
           ADD FSM145-QVTE1        TO W-TFAM-QVTEF1                             
           ADD FSM145-QVTE2        TO W-TFAM-QVTEF2                             
           ADD FSM145-QVTE3        TO W-TFAM-QVTEF3                             
           ADD FSM145-QVTE4        TO W-TFAM-QVTEF4                             
           ADD FSM145-QVTE5        TO W-TFAM-QVTEF5                             
           ADD FSM145-QVTE6        TO W-TFAM-QVTEF6                             
           ADD FSM145-QVTE7        TO W-TFAM-QVTEF7                             
           ADD FSM145-QVTE8        TO W-TFAM-QVTEF8                             
           ADD FSM145-QVTE9        TO W-TFAM-QVTEF9                             
           ADD FSM145-QVTE10       TO W-TFAM-QVTEF10                            
           ADD FSM145-QVTE11       TO W-TFAM-QVTEF11                            
           ADD FSM145-QVTE12       TO W-TFAM-QVTEF12                            
           ADD FSM145-QVTE13       TO W-TFAM-QVTEF13                            
           ADD FSM145-QVTE14       TO W-TFAM-QVTEF14                            
           ADD FSM145-QVTE15       TO W-TFAM-QVTEF15                            
           ADD FSM145-QVTE16       TO W-TFAM-QVTEF16                            
           ADD FSM145-QVTE17       TO W-TFAM-QVTEF17                            
           ADD FSM145-QVTE18       TO W-TFAM-QVTEF18                            
           ADD FSM145-QVTE19       TO W-TFAM-QVTEF19                            
           ADD FSM145-QVTETOT      TO W-TFAM-QVTEF                              
           ADD FSM145-QSTOCKMAG1   TO W-TFAM-QSTOCKMAGF1                        
           ADD FSM145-QSTOCKMAG2   TO W-TFAM-QSTOCKMAGF2                        
           ADD FSM145-QSTOCKMAG3   TO W-TFAM-QSTOCKMAGF3                        
           ADD FSM145-QSTOCKMAG4   TO W-TFAM-QSTOCKMAGF4                        
           ADD FSM145-QSTOCKMAG5   TO W-TFAM-QSTOCKMAGF5                        
           ADD FSM145-QSTOCKMAG6   TO W-TFAM-QSTOCKMAGF6                        
           ADD FSM145-QSTOCKMAG7   TO W-TFAM-QSTOCKMAGF7                        
           ADD FSM145-QSTOCKMAG8   TO W-TFAM-QSTOCKMAGF8                        
           ADD FSM145-QSTOCKMAG9   TO W-TFAM-QSTOCKMAGF9                        
           ADD FSM145-QSTOCKMAG10  TO W-TFAM-QSTOCKMAGF10                       
           ADD FSM145-QSTOCKMAG11  TO W-TFAM-QSTOCKMAGF11                       
           ADD FSM145-QSTOCKMAG12  TO W-TFAM-QSTOCKMAGF12                       
           ADD FSM145-QSTOCKMAG13  TO W-TFAM-QSTOCKMAGF13                       
           ADD FSM145-QSTOCKMAG14  TO W-TFAM-QSTOCKMAGF14                       
           ADD FSM145-QSTOCKMAG15  TO W-TFAM-QSTOCKMAGF15                       
           ADD FSM145-QSTOCKMAG16  TO W-TFAM-QSTOCKMAGF16                       
           ADD FSM145-QSTOCKMAG17  TO W-TFAM-QSTOCKMAGF17                       
           ADD FSM145-QSTOCKMAG18  TO W-TFAM-QSTOCKMAGF18                       
           ADD FSM145-QSTOCKMAG19  TO W-TFAM-QSTOCKMAGF19                       
           ADD FSM145-QSTOCKDEP    TO W-TFAM-QSTOCKDEPF                         
           ADD FSM145-QSTOCKMAGTOT TO W-TFAM-QSTOCKMAGF.                        
       FIN-CUMUL-TOTAUX. EXIT.                                                  
      *                                                                         
       ECRITURE-TOTAUX-SEG SECTION.                                             
      *-----------------                                                        
           IF W-FSM145-LVMARKET1 > SPACES                                       
              IF CTR-LIGNE >  MAX-LIGNE2                                        
                 PERFORM ECRITURE-ENTETE                                        
              END-IF                                                            
              MOVE ' '               TO SAUT                                    
              MOVE ISM145-T06        TO LIGNE                                   
              PERFORM WRITE-ISM145                                              
              ADD 1 TO CTR-LIGNE                                                
      *                                                                         
              MOVE W-TSEG-QVTES1         TO ISM145-T18-QVTES1                   
              MOVE W-TSEG-QVTES2         TO ISM145-T18-QVTES2                   
              MOVE W-TSEG-QVTES3         TO ISM145-T18-QVTES3                   
              MOVE W-TSEG-QVTES4         TO ISM145-T18-QVTES4                   
              MOVE W-TSEG-QVTES5         TO ISM145-T18-QVTES5                   
              MOVE W-TSEG-QVTES6         TO ISM145-T18-QVTES6                   
              MOVE W-TSEG-QVTES7         TO ISM145-T18-QVTES7                   
              MOVE W-TSEG-QVTES8         TO ISM145-T18-QVTES8                   
              MOVE W-TSEG-QVTES9         TO ISM145-T18-QVTES9                   
              MOVE W-TSEG-QVTES10        TO ISM145-T18-QVTES10                  
              MOVE W-TSEG-QVTES11        TO ISM145-T18-QVTES11                  
              MOVE W-TSEG-QVTES12        TO ISM145-T18-QVTES12                  
              MOVE W-TSEG-QVTES13        TO ISM145-T18-QVTES13                  
              MOVE W-TSEG-QVTES14        TO ISM145-T18-QVTES14                  
              MOVE W-TSEG-QVTES15        TO ISM145-T18-QVTES15                  
              MOVE W-TSEG-QVTES16        TO ISM145-T18-QVTES16                  
              MOVE W-TSEG-QVTES17        TO ISM145-T18-QVTES17                  
              MOVE W-TSEG-QVTES18        TO ISM145-T18-QVTES18                  
              MOVE W-TSEG-QVTES19        TO ISM145-T18-QVTES19                  
              MOVE W-TSEG-QVTES          TO ISM145-T18-QVTES                    
              IF W-FSM145-MAG1  NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X1                       
              END-IF                                                            
              IF W-FSM145-MAG2  NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X2                       
              END-IF                                                            
              IF W-FSM145-MAG3  NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X3                       
              END-IF                                                            
              IF W-FSM145-MAG4  NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X4                       
              END-IF                                                            
              IF W-FSM145-MAG5  NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X5                       
              END-IF                                                            
              IF W-FSM145-MAG6  NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X6                       
              END-IF                                                            
              IF W-FSM145-MAG7  NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X7                       
              END-IF                                                            
              IF W-FSM145-MAG8  NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X8                       
              END-IF                                                            
              IF W-FSM145-MAG9  NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X9                       
              END-IF                                                            
              IF W-FSM145-MAG10 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X10                      
              END-IF                                                            
              IF W-FSM145-MAG11 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X11                      
              END-IF                                                            
              IF W-FSM145-MAG12 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X12                      
              END-IF                                                            
              IF W-FSM145-MAG13 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X13                      
              END-IF                                                            
              IF W-FSM145-MAG14 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X14                      
              END-IF                                                            
              IF W-FSM145-MAG15 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X15                      
              END-IF                                                            
              IF W-FSM145-MAG16 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X16                      
              END-IF                                                            
              IF W-FSM145-MAG17 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X17                      
              END-IF                                                            
              IF W-FSM145-MAG18 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X18                      
              END-IF                                                            
              IF W-FSM145-MAG19 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-T18-X19                      
              END-IF                                                            
              MOVE ' '                   TO SAUT                                
              MOVE ISM145-T18            TO LIGNE                               
              PERFORM WRITE-ISM145                                              
              ADD 1 TO CTR-LIGNE                                                
      *                                                                         
              MOVE W-TSEG-QSTOCKMAGS1    TO ISM145-T19-QSTOCKMAGS1              
              MOVE W-TSEG-QSTOCKMAGS2    TO ISM145-T19-QSTOCKMAGS2              
              MOVE W-TSEG-QSTOCKMAGS3    TO ISM145-T19-QSTOCKMAGS3              
              MOVE W-TSEG-QSTOCKMAGS4    TO ISM145-T19-QSTOCKMAGS4              
              MOVE W-TSEG-QSTOCKMAGS5    TO ISM145-T19-QSTOCKMAGS5              
              MOVE W-TSEG-QSTOCKMAGS6    TO ISM145-T19-QSTOCKMAGS6              
              MOVE W-TSEG-QSTOCKMAGS7    TO ISM145-T19-QSTOCKMAGS7              
              MOVE W-TSEG-QSTOCKMAGS8    TO ISM145-T19-QSTOCKMAGS8              
              MOVE W-TSEG-QSTOCKMAGS9    TO ISM145-T19-QSTOCKMAGS9              
              MOVE W-TSEG-QSTOCKMAGS10   TO ISM145-T19-QSTOCKMAGS10             
              MOVE W-TSEG-QSTOCKMAGS11   TO ISM145-T19-QSTOCKMAGS11             
              MOVE W-TSEG-QSTOCKMAGS12   TO ISM145-T19-QSTOCKMAGS12             
              MOVE W-TSEG-QSTOCKMAGS13   TO ISM145-T19-QSTOCKMAGS13             
              MOVE W-TSEG-QSTOCKMAGS14   TO ISM145-T19-QSTOCKMAGS14             
              MOVE W-TSEG-QSTOCKMAGS15   TO ISM145-T19-QSTOCKMAGS15             
              MOVE W-TSEG-QSTOCKMAGS16   TO ISM145-T19-QSTOCKMAGS16             
              MOVE W-TSEG-QSTOCKMAGS17   TO ISM145-T19-QSTOCKMAGS17             
              MOVE W-TSEG-QSTOCKMAGS18   TO ISM145-T19-QSTOCKMAGS18             
              MOVE W-TSEG-QSTOCKMAGS19   TO ISM145-T19-QSTOCKMAGS19             
              MOVE W-TSEG-QSTOCKDEPS     TO ISM145-T19-QSTOCKDEPS               
              MOVE W-TSEG-QSTOCKMAGS     TO ISM145-T19-QSTOCKMAGS               
              IF W-FSM145-MAG1  NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX1                          
              END-IF                                                            
              IF W-FSM145-MAG2  NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX2                          
              END-IF                                                            
              IF W-FSM145-MAG3  NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX3                          
              END-IF                                                            
              IF W-FSM145-MAG4  NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX4                          
              END-IF                                                            
              IF W-FSM145-MAG5  NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX5                          
              END-IF                                                            
              IF W-FSM145-MAG6  NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX6                          
              END-IF                                                            
              IF W-FSM145-MAG7  NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX7                          
              END-IF                                                            
              IF W-FSM145-MAG8  NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX8                          
              END-IF                                                            
              IF W-FSM145-MAG9  NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX9                          
              END-IF                                                            
              IF W-FSM145-MAG10 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX10                         
              END-IF                                                            
              IF W-FSM145-MAG11 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX11                         
              END-IF                                                            
              IF W-FSM145-MAG12 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX12                         
              END-IF                                                            
              IF W-FSM145-MAG13 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX13                         
              END-IF                                                            
              IF W-FSM145-MAG14 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX14                         
              END-IF                                                            
              IF W-FSM145-MAG15 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX15                         
              END-IF                                                            
              IF W-FSM145-MAG16 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX16                         
              END-IF                                                            
              IF W-FSM145-MAG17 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX17                         
              END-IF                                                            
              IF W-FSM145-MAG18 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX18                         
              END-IF                                                            
              IF W-FSM145-MAG19 NOT > SPACES                                    
                 MOVE SPACES             TO ISM145-TX19                         
              END-IF                                                            
              MOVE ' '                   TO SAUT                                
              MOVE ISM145-T19            TO LIGNE                               
              PERFORM WRITE-ISM145                                              
              ADD 1 TO CTR-LIGNE                                                
           END-IF                                                               
      *                                                                         
           INITIALIZE W-TSEG.                                                   
       FIN-ECRITURE-TOTAUX-SEG. EXIT.                                           
      *                                                                         
       ECRITURE-TOTAUX-FAM SECTION.                                             
      *-----------------                                                        
           IF CTR-LIGNE >  MAX-LIGNE2                                           
              PERFORM ECRITURE-ENTETE                                           
           END-IF                                                               
           MOVE ' '               TO SAUT.                                      
           MOVE ISM145-T21        TO LIGNE.                                     
           PERFORM WRITE-ISM145.                                                
           ADD 1 TO CTR-LIGNE.                                                  
           MOVE W-TFAM-QVTEF1         TO ISM145-T22-QVTEF1                      
           MOVE W-TFAM-QVTEF2         TO ISM145-T22-QVTEF2                      
           MOVE W-TFAM-QVTEF3         TO ISM145-T22-QVTEF3                      
           MOVE W-TFAM-QVTEF4         TO ISM145-T22-QVTEF4                      
           MOVE W-TFAM-QVTEF5         TO ISM145-T22-QVTEF5                      
           MOVE W-TFAM-QVTEF6         TO ISM145-T22-QVTEF6                      
           MOVE W-TFAM-QVTEF7         TO ISM145-T22-QVTEF7                      
           MOVE W-TFAM-QVTEF8         TO ISM145-T22-QVTEF8                      
           MOVE W-TFAM-QVTEF9         TO ISM145-T22-QVTEF9                      
           MOVE W-TFAM-QVTEF10        TO ISM145-T22-QVTEF10                     
           MOVE W-TFAM-QVTEF11        TO ISM145-T22-QVTEF11                     
           MOVE W-TFAM-QVTEF12        TO ISM145-T22-QVTEF12                     
           MOVE W-TFAM-QVTEF13        TO ISM145-T22-QVTEF13                     
           MOVE W-TFAM-QVTEF14        TO ISM145-T22-QVTEF14                     
           MOVE W-TFAM-QVTEF15        TO ISM145-T22-QVTEF15                     
           MOVE W-TFAM-QVTEF16        TO ISM145-T22-QVTEF16                     
           MOVE W-TFAM-QVTEF17        TO ISM145-T22-QVTEF17                     
           MOVE W-TFAM-QVTEF18        TO ISM145-T22-QVTEF18                     
           MOVE W-TFAM-QVTEF19        TO ISM145-T22-QVTEF19                     
           MOVE W-TFAM-QVTEF          TO ISM145-T22-QVTEF                       
           IF W-FSM145-MAG1  NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X1                          
           END-IF                                                               
           IF W-FSM145-MAG2  NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X2                          
           END-IF                                                               
           IF W-FSM145-MAG3  NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X3                          
           END-IF                                                               
           IF W-FSM145-MAG4  NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X4                          
           END-IF                                                               
           IF W-FSM145-MAG5  NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X5                          
           END-IF                                                               
           IF W-FSM145-MAG6  NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X6                          
           END-IF                                                               
           IF W-FSM145-MAG7  NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X7                          
           END-IF                                                               
           IF W-FSM145-MAG8  NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X8                          
           END-IF                                                               
           IF W-FSM145-MAG9  NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X9                          
           END-IF                                                               
           IF W-FSM145-MAG10 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X10                         
           END-IF                                                               
           IF W-FSM145-MAG11 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X11                         
           END-IF                                                               
           IF W-FSM145-MAG12 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X12                         
           END-IF                                                               
           IF W-FSM145-MAG13 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X13                         
           END-IF                                                               
           IF W-FSM145-MAG14 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X14                         
           END-IF                                                               
           IF W-FSM145-MAG15 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X15                         
           END-IF                                                               
           IF W-FSM145-MAG16 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X16                         
           END-IF                                                               
           IF W-FSM145-MAG17 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X17                         
           END-IF                                                               
           IF W-FSM145-MAG18 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X18                         
           END-IF                                                               
           IF W-FSM145-MAG19 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-T22-X19                         
           END-IF                                                               
           MOVE ' '                   TO SAUT.                                  
           MOVE ISM145-T22            TO LIGNE.                                 
           PERFORM WRITE-ISM145.                                                
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE W-TFAM-QSTOCKMAGF1    TO ISM145-T23-QSTOCKMAGF1                 
           MOVE W-TFAM-QSTOCKMAGF2    TO ISM145-T23-QSTOCKMAGF2                 
           MOVE W-TFAM-QSTOCKMAGF3    TO ISM145-T23-QSTOCKMAGF3                 
           MOVE W-TFAM-QSTOCKMAGF4    TO ISM145-T23-QSTOCKMAGF4                 
           MOVE W-TFAM-QSTOCKMAGF5    TO ISM145-T23-QSTOCKMAGF5                 
           MOVE W-TFAM-QSTOCKMAGF6    TO ISM145-T23-QSTOCKMAGF6                 
           MOVE W-TFAM-QSTOCKMAGF7    TO ISM145-T23-QSTOCKMAGF7                 
           MOVE W-TFAM-QSTOCKMAGF8    TO ISM145-T23-QSTOCKMAGF8                 
           MOVE W-TFAM-QSTOCKMAGF9    TO ISM145-T23-QSTOCKMAGF9                 
           MOVE W-TFAM-QSTOCKMAGF10   TO ISM145-T23-QSTOCKMAGF10                
           MOVE W-TFAM-QSTOCKMAGF11   TO ISM145-T23-QSTOCKMAGF11                
           MOVE W-TFAM-QSTOCKMAGF12   TO ISM145-T23-QSTOCKMAGF12                
           MOVE W-TFAM-QSTOCKMAGF13   TO ISM145-T23-QSTOCKMAGF13                
           MOVE W-TFAM-QSTOCKMAGF14   TO ISM145-T23-QSTOCKMAGF14                
           MOVE W-TFAM-QSTOCKMAGF15   TO ISM145-T23-QSTOCKMAGF15                
           MOVE W-TFAM-QSTOCKMAGF16   TO ISM145-T23-QSTOCKMAGF16                
           MOVE W-TFAM-QSTOCKMAGF17   TO ISM145-T23-QSTOCKMAGF17                
           MOVE W-TFAM-QSTOCKMAGF18   TO ISM145-T23-QSTOCKMAGF18                
           MOVE W-TFAM-QSTOCKMAGF19   TO ISM145-T23-QSTOCKMAGF19                
           MOVE W-TFAM-QSTOCKDEPF     TO ISM145-T23-QSTOCKDEPF                  
           MOVE W-TFAM-QSTOCKMAGF     TO ISM145-T23-QSTOCKMAGF                  
           IF W-FSM145-MAG1  NOT > SPACES                                       
              MOVE SPACES             TO ISM145-1                               
           END-IF                                                               
           IF W-FSM145-MAG2  NOT > SPACES                                       
              MOVE SPACES             TO ISM145-2                               
           END-IF                                                               
           IF W-FSM145-MAG3  NOT > SPACES                                       
              MOVE SPACES             TO ISM145-3                               
           END-IF                                                               
           IF W-FSM145-MAG4  NOT > SPACES                                       
              MOVE SPACES             TO ISM145-4                               
           END-IF                                                               
           IF W-FSM145-MAG5  NOT > SPACES                                       
              MOVE SPACES             TO ISM145-5                               
           END-IF                                                               
           IF W-FSM145-MAG6  NOT > SPACES                                       
              MOVE SPACES             TO ISM145-6                               
           END-IF                                                               
           IF W-FSM145-MAG7  NOT > SPACES                                       
              MOVE SPACES             TO ISM145-7                               
           END-IF                                                               
           IF W-FSM145-MAG8  NOT > SPACES                                       
              MOVE SPACES             TO ISM145-8                               
           END-IF                                                               
           IF W-FSM145-MAG9  NOT > SPACES                                       
              MOVE SPACES             TO ISM145-9                               
           END-IF                                                               
           IF W-FSM145-MAG10 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-10                              
           END-IF                                                               
           IF W-FSM145-MAG11 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-11                              
           END-IF                                                               
           IF W-FSM145-MAG12 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-12                              
           END-IF                                                               
           IF W-FSM145-MAG13 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-13                              
           END-IF                                                               
           IF W-FSM145-MAG14 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-14                              
           END-IF                                                               
           IF W-FSM145-MAG15 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-15                              
           END-IF                                                               
           IF W-FSM145-MAG16 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-16                              
           END-IF                                                               
           IF W-FSM145-MAG17 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-17                              
           END-IF                                                               
           IF W-FSM145-MAG18 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-18                              
           END-IF                                                               
           IF W-FSM145-MAG19 NOT > SPACES                                       
              MOVE SPACES             TO ISM145-19                              
           END-IF                                                               
           MOVE ' '                   TO SAUT.                                  
           MOVE ISM145-T23            TO LIGNE.                                 
           PERFORM WRITE-ISM145.                                                
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           INITIALIZE W-TFAM.                                                   
       FIN-ECRITURE-TOTAUX-FAM. EXIT.                                           
      *                                                                         
      *                                                                         
       WRITE-ISM145 SECTION.                                                    
      *                                                                         
           EVALUATE (W-FSM145-LGDRAYON)                                         
              WHEN 'GR' SET GRIS       TO TRUE                                  
              WHEN 'BR' SET BRUN       TO TRUE                                  
              WHEN 'BL' SET BLANC      TO TRUE                                  
              WHEN 'AC' SET ACCESSOIRE TO TRUE                                  
              WHEN OTHER SET GRIS      TO TRUE                                  
           END-EVALUATE.                                                        
           IF GRIS                                                              
              MOVE LIGNE        TO LIGNEGR                                      
              MOVE SAUT         TO SAUTGR                                       
              WRITE LIGNE-ISM145GR                                              
              ADD 1 TO CTR-GRIS                                                 
           END-IF                                                               
           IF BRUN                                                              
              MOVE LIGNE        TO LIGNEBR                                      
              MOVE SAUT         TO SAUTBR                                       
              WRITE LIGNE-ISM145BR                                              
              ADD 1 TO CTR-BRUN                                                 
           END-IF                                                               
           IF BLANC                                                             
              MOVE LIGNE        TO LIGNEBL                                      
              MOVE SAUT         TO SAUTBL                                       
              WRITE LIGNE-ISM145BL                                              
              ADD 1 TO CTR-BLAN                                                 
           END-IF                                                               
           IF ACCESSOIRE                                                        
              MOVE LIGNE        TO LIGNEAC                                      
              MOVE SAUT         TO SAUTAC                                       
              WRITE LIGNE-ISM145AC                                              
              ADD 1 TO CTR-ACCE                                                 
           END-IF.                                                              
      *                                                                         
       FIN-WRITE-ISM145. EXIT.                                                  
      *-----------------                                                        
      *---------------------------------------------------------------*         
      *         P A R A G R A P H E   F I N  A N O R M A L E          *         
      *---------------------------------------------------------------*         
      *                                                                         
       FIN-ANORMALE SECTION.                                                    
      *-------------                                                            
           MOVE 'BSM146' TO ABEND-PROG.                                         
           CALL 'ABEND' USING                                                   
                        ABEND-PROG                                              
                        ABEND-MESS.                                             
      *                                                                         
       F-FIN-ANORMALE.                                                          
           EXIT.                                                                
      *                                                                         
           EJECT                                                                
