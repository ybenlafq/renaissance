      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                         00000010
       PROGRAM-ID.   BRE200.                                            00000020
       AUTHOR.       DSA015.                                            00000030
                                                                        00000040
      * ------------------------------------------------------------ *  00000050
      *                                                              *  00000060
      *   PROJET           :                                         *  00000070
      *   PROGRAMME        : BRE200.                                 *  00000080
      *   PERIODICITE      :                                         *  00000090
      *   FONCTION         : EXTRACTION VENTES 36 VENTE EMPORTEE     *  00000100
      *   DATE DE CREATION : 23/11/1992.                             *  00000110
      *                                                              *  00000120
      * ------------------------------------------------------------ *  00000130
                                                                        00000140
       ENVIRONMENT DIVISION.                                            00000150
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                           00000160
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
      *{  Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                     
       SPECIAL-NAMES.                                                           
           COPY MW-COLSEQ.                                                      
      *}                                                                        
                                                                                
      *}                                                                        
       INPUT-OUTPUT SECTION.                                            00000170
       FILE-CONTROL.                                                    00000180
                                                                        00000190
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FEMPORT  ASSIGN TO  FEMPORT.                         00000200
      *--                                                                       
           SELECT  FEMPORT  ASSIGN TO  FEMPORT                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FRE200   ASSIGN TO  FRE200.                          00000210
      *--                                                                       
           SELECT  FRE200   ASSIGN TO  FRE200                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FDATE    ASSIGN TO  FDATE.                           00000210
      *--                                                                       
           SELECT  FDATE    ASSIGN TO  FDATE                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FPARAM   ASSIGN TO  FPARAM.                          00000210
      *--                                                                       
           SELECT  FPARAM   ASSIGN TO  FPARAM                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FNSOC    ASSIGN TO  FNSOC.                           00000210
      *                                                                         
      *--                                                                       
           SELECT  FNSOC    ASSIGN TO  FNSOC                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00000220
      *}                                                                        
       DATA DIVISION.                                                   00000230
       FILE SECTION.                                                    00000240
                                                                        00000250
      * ------------------------------------------------------------ *  00000260
      *               D E S C R I P T I O N   F I C H I E R          *  00000270
      * ------------------------------------------------------------ *  00000280
                                                                        00000290
       FD  FEMPORT                                                      00000300
           RECORDING F                                                  00000310
           BLOCK 0 RECORDS                                              00000320
           LABEL RECORD STANDARD.                                       00000330
       01  ENR-FEMPORT.                                                 00000340
           02  FILLER PIC X(630).                                       00000350
                                                                        00000360
       FD  FPARAM                                                       00000300
           RECORDING F                                                  00000310
           BLOCK 0 RECORDS                                              00000320
           LABEL RECORD STANDARD.                                       00000330
       01  FILE-FPARAM.                                                 00000340
           03  F-PARAM PIC X(01).                                       00000350
           03  FILLER  PIC X(79).                                       00000350
                                                                        00000360
                                                                        00000370
       FD  FDATE                                                        00000300
           RECORDING F                                                  00000310
           BLOCK 0 RECORDS                                              00000320
           LABEL RECORD STANDARD.                                       00000330
       01  FILE-DATE.                                                   00000340
           03  F-DATE.                                                  00000350
               06  JJ  PIC X(02).                                               
               06  MM  PIC X(02).                                               
               06  SS  PIC X(02).                                               
               06  AA  PIC X(02).                                               
           03  FILLER  PIC X(72).                                       00000350
                                                                        00000360
       FD  FNSOC                                                        00000300
           RECORDING F                                                  00000310
           BLOCK 0 RECORDS                                              00000320
           LABEL RECORD STANDARD.                                       00000330
       01  FILE-FNSOC.                                                  00000340
           03  F-NSOC  PIC X(03).                                       00000350
           03  FILLER  PIC X(77).                                       00000350
                                                                        00000360
                                                                        00000370
       FD  FRE200                                                       00000380
           RECORDING F                                                  00000390
           BLOCK 0 RECORDS                                              00000400
           LABEL RECORD STANDARD.                                       00000410
       01  ENR-FRE200.                                                  00000420
           02  FILLER PIC X(100).                                       00000430
                                                                        00000440
                                                                        00000440
                                                                        00000450
      * ------------------------------------------------------------ *  00000460
      *                W O R K I N G - S T O R A G E                 *  00000470
      * ------------------------------------------------------------ *  00000480
                                                                        00000490
       WORKING-STORAGE SECTION.                                         00000500
                                                                        00000510
      *   LONGUEUR 630                                                  00000520
                                                                        00000530
           COPY FEMPORTE.                                               00000550
                                                                        00000600
                                                                        00000600
           COPY IRE000.                                                 00000620
                                                                        00000630
                                                                        00000630
      *                                                                 00000640
       77  CPT-FEMPORT               PIC 9(04)    VALUE 0.              00000810
       77  CPT-FRE200                PIC 9(07)    VALUE 0.              00000820
       77  W-ANNUL                   PIC  X       VALUE SPACE.          00000820
       77  W-BCOM                    PIC  X(9)    VALUE SPACE.          00000820
       77  TOP-PLUS                  PIC  X       VALUE '0'.            00000820
       01  W-CLE.                                                       00000820
           05 FILLER PIC X(18) VALUE SPACE.                                     
           05 W-NUM  PIC 9     VALUE 1.                                         
       01  W-ZONE.                                                      00000820
           05 W-DATTRANS       PIC X(6).                                        
           05 W-CODIC          PIC X(7).                                        
           05 W-QTE            PIC S9(3) COMP-3 VALUE 0.                        
           05 W-VEND           PIC X(4).                                00000980
       01  TEST-FEMPORT              PIC X(01) VALUE ' '.               00000990
           88 PAS-FIN-FEMPORT        VALUE '0'.                         00001000
           88 FIN-FEMPORT            VALUE '1'.                         00001010
                                                                        00001020
       01  TEST-FORCE                PIC X(01) VALUE ' '.               00000990
           88 PAS-EXISTE-FORCE        VALUE '0'.                        00001000
           88 EXISTE-FORCE           VALUE '1'.                         00001010
                                                                        00001020
       01  TEST-REMISE               PIC X(01) VALUE ' '.               00000990
           88 PAS-EXISTE-REMISE      VALUE '0'.                         00001000
           88 EXISTE-REMISE          VALUE '1'.                         00001010
                                                                        00001020
       01  W-DATE1.                                                     00001100
           03  W-AA                PIC X(02)  VALUE ZEROES.             00001130
           03  W-MM                PIC X(02)  VALUE ZEROES.             00001120
           03  W-JJ                PIC X(02)  VALUE ZEROES.             00001110
                                                                        00001140
       01  W-VENDEUR.                                                           
           02 FILLER               PIC X(2)  VALUE '00'.                        
           02 W-VENDEUR4           PIC X(4)  VALUE '00'.                        
       01  W-DATE2.                                                     00001100
           05  W-SSAAMM.                                                        
               10  W-SS2           PIC X(02)  VALUE ZEROES.             00001110
               10  W-AA2           PIC X(02)  VALUE ZEROES.             00001120
               10  W-MM2           PIC X(02)  VALUE ZEROES.             00001130
           05  W-JJ2               PIC X(02)  VALUE ZEROES.             00001130
                                                                        00001140
       01  W-DATE3.                                                     00001100
           05  W-SS3               PIC X(02)  VALUE '19'.               00001110
           05  W-AAMMJJ3.                                                       
               10  W-AA3           PIC X(02)  VALUE ZEROES.             00001120
               10  W-MM3           PIC X(02)  VALUE ZEROES.             00001130
               10  W-JJ3           PIC X(02)  VALUE ZEROES.             00001130
                                                                        00001140
       01  W-DIFFERENCE           PIC S9(7) COMP-3.                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-TAB                  PIC S9(4) COMP  VALUE 0.                      
      *--                                                                       
       01  I-TAB                  PIC S9(4) COMP-5  VALUE 0.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-MAX-ENR1             PIC S9(4) COMP  VALUE 3.                      
      *--                                                                       
       01  I-MAX-ENR1             PIC S9(4) COMP-5  VALUE 3.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-MAX-ENR2             PIC S9(4) COMP  VALUE 7.                      
      *--                                                                       
       01  I-MAX-ENR2             PIC S9(4) COMP-5  VALUE 7.                    
      *}                                                                        
       01  W-QTE-PU               PIC S9(11)V99 COMP-3.                         
       01  W-NSOCIETE             PIC X(3).                             00001200
       01  W-FLAG                 PIC X(1).                             00001200
                                                                        00001210
      * ------------------------------------------------------------ *  00001220
      *         DESCRIPTION DES ZONES D'APPEL DU MODULE ABEND        *  00001230
      * ------------------------------------------------------------ *  00001240
                                                                        00001250
           COPY ABENDCOP.                                               00001260
           COPY WORKDATC.                                               00001260
                                                                        00001270
                                                                        00001400
      * ------------------------------------------------------------ *  00001370
      *              P R O C E D U R E     D I V I S I O N           *  00001380
      * ------------------------------------------------------------ *  00001390
       PROCEDURE DIVISION.                                              00001410
                                                                        00001420
      * ------------------------------------------------------------ *  00001430
      *             T R A M E     D U     P R O G R A M M E          *  00001440
      * ------------------------------------------------------------ *  00001450
      *                                                              *  00001460
      *            -----------------------------------------         *  00001470
      *            -  MODULE DE BASE DU PROGRAMME  BRE200  -         *  00001480
      *            -----------------------------------------         *  00001490
      *                               I                              *  00001500
      *            -----------------------------------------         *  00001510
      *            I                  I                    I         *  00001520
      *       -----------      ----------------        ---------     *  00001530
      *       -  DEBUT  -      -  TRAITEMENT  -        -  FIN  -     *  00001540
      *       -----------      ----------------        ---------     *  00001550
      *                                                              *  00001560
      * ------------------------------------------------------------ *  00001570
                                                                        00001580
           PERFORM DEBUT-BRE200.                                        00001590
           PERFORM TRAIT-BRE200 UNTIL FIN-FEMPORT                       00001600
                                   OR VE-SOC NOT = W-NSOCIETE.                  
           PERFORM FIN-BRE200.                                          00001610
                                                                        00001620
      * ------------------------------------------------------------ *  00001630
      *                     D E B U T    B R E 2 0 0                 *  00001640
      * ------------------------------------------------------------ *  00001650
                                                                        00001660
       DEBUT-BRE200 SECTION.                                            00001670
                                                                        00001680
                                                                        00001730
           PERFORM OUVERTURE-FICHIERS.                                  00001740
                                                                        00001750
           PERFORM LECTURE-PARAMETRES.                                          
                                                                        00001840
           INITIALIZE DSECT-IRE000.                                     00001850
           MOVE 0      TO W-QTE-PU                                              
           SET PAS-EXISTE-FORCE  TO TRUE.                               00001000
           SET PAS-EXISTE-REMISE TO TRUE.                               00001000
           SET PAS-FIN-FEMPORT   TO TRUE.                               00001870
           PERFORM LECTURE-FEMPORT UNTIL (VE-SOC  = W-NSOCIETE AND      00001880
                                        (VE-TYPE  = '1' OR '4')  AND            
                                         VE-ANUL  = ' ')                        
                                          OR FIN-FEMPORT.                       
                                                                        00001890
                                                                        00001920
       FIN-DEBUT-BRE200. EXIT.                                          00001930
       LECTURE-PARAMETRES SECTION.                                              
           OPEN INPUT FDATE.                                                    
           READ FDATE AT END                                                    
              MOVE 'PAS DE DATE EN ENTREE' TO ABEND-MESS                        
              PERFORM FIN-ANORMALE                                              
           END-READ.                                                            
           DISPLAY 'FILE DATE : ' FILE-DATE                                     
           MOVE SS   TO W-SS2                                           00003380
           MOVE AA   TO W-AA  W-AA2                                     00003380
           MOVE MM   TO W-MM  W-MM2.                                    00003380
           MOVE JJ   TO W-JJ  W-JJ2.                                    00003380
           CLOSE FDATE                                                          
           OPEN INPUT FNSOC.                                                    
           READ FNSOC AT END                                                    
              MOVE 'PAS DE FNSOC EN ENTREE' TO ABEND-MESS                       
              PERFORM FIN-ANORMALE                                              
           END-READ.                                                            
           MOVE F-NSOC       TO W-NSOCIETE.                             00003380
           CLOSE FNSOC.                                                         
           OPEN INPUT FPARAM.                                                   
           READ FPARAM AT END                                                   
              MOVE 'PAS DE FPARAM EN ENTREE' TO ABEND-MESS                      
              PERFORM FIN-ANORMALE                                              
           END-READ.                                                            
           MOVE F-PARAM      TO W-FLAG.                                 00003380
           CLOSE FPARAM.                                                        
       FIN-LECTURE-PARAMETRES. EXIT.                                            
                                                                        00001940
      * -------------------- LECTURE FICHIER FEMPORT --------------- *  00001950
                                                                        00001960
       LECTURE-FEMPORT  SECTION.                                        00001970
                                                                        00001980
           READ FEMPORT INTO VE-ARTICLE AT END                          00001990
                SET FIN-FEMPORT TO TRUE                                 00002000
                MOVE HIGH-VALUE TO VE-CLE                                       
           END-READ.                                                    00002010
           ADD 1            TO CPT-FEMPORT.                             00002020
       FIN-LECTURE-FEMPORT. EXIT.                                       00002040
                                                                        00002050
      * -------------------- OUVERTURE-FICHIERS -------------------- *  00002060
                                                                        00002070
       OUVERTURE-FICHIERS SECTION.                                      00002080
                                                                        00002090
           OPEN INPUT   FEMPORT.                                        00002100
           OPEN OUTPUT  FRE200.                                         00002110
                                                                        00002120
       FIN-OUVERTURE-FICHIERS. EXIT.                                    00002130
                                                                        00002140
                                                                        00002150
      * ------------------------------------------------------------ *  00002160
      *                     T R A I T    B R E 2 0 0                 *  00002170
      * ------------------------------------------------------------ *  00002180
                                                                        00002190
       TRAIT-BRE200 SECTION.                                            00002200
                                                                        00002210
      *       SI C'EST UNE VENTE SANS BON DE COMMANDE OU                02390   
      *       VENTE AVEC BON DE COMMANDE ET FLAG A 'O'                  02390   
           IF VE-SOC      = W-NSOCIETE AND                              00001880
             (VE-TYPE  = '1' OR '4')    AND                                     
              VE-ANUL = ' '            AND                                      
              VE-NUM = 0                                                        
              IF W-FLAG = 'O'    OR VE-BCOM NOT >  SPACES                       
                 MOVE VE-DATTRANS TO W-DATTRANS                                 
                 MOVE VE-VEND     TO W-VEND                                     
                 PERFORM RECUP-FORC-ENR1                                   00002
                 INITIALIZE DSECT-IRE000                                   02340
                 SET PAS-EXISTE-FORCE  TO TRUE                             00001
                 SET PAS-EXISTE-REMISE TO TRUE                             00001
                 IF VE-L > I-MAX-ENR1                                           
                    MOVE VE-CLE      TO W-CLE                                   
      *             DISPLAY 'TEST' VE-TRANS ' ' VE-L ' CLEF ' VE-CLE            
                    MOVE '1'         TO W-NUM                                   
                    MOVE VE-BCOM     TO W-BCOM                                  
                    PERFORM LECTURE-FEMPORT                             00002490
                    PERFORM RECUP-FORC-ENR2 UNTIL VE-CLE NOT = W-CLE       00002
                    MOVE '1' TO TOP-PLUS                                        
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                      00002510
      *                                                                 00002520
           IF PAS-FIN-FEMPORT AND TOP-PLUS = '0'                                
              PERFORM LECTURE-FEMPORT                                   00002490
           ELSE                                                                 
              MOVE '0' TO TOP-PLUS                                              
           END-IF.                                                      00002490
      *                                                                 00002520
                                                                        00002530
       FIN-TRAIT-BRE200. EXIT.                                          00002540
                                                                        00002550
      * ------------------------------------------------------------ *  00002560
      *               TRAITEMENT D'UNE VENTE    ENR1                 *  00002570
      * ------------------------------------------------------------ *  00002580
                                                                        00002590
       RECUP-FORC-ENR1   SECTION.                                       00002600
                                                                        00002610
           PERFORM VARYING I-TAB FROM 1 BY 1 UNTIL                      00002620
      *{ reorder-array-condition 1.1                                            
      *                                        VE-CODIC(I-TAB) = SPACES 00002620
      *                                     OR I-TAB > I-MAX-ENR1               
      *--                                                                       
                                               I-TAB > I-MAX-ENR1               
                                            OR VE-CODIC(I-TAB) = SPACES         
      *}                                                                        
      *    SI RAYON 51 (ELA) OR 54 (DACEM) OR 52 (TLM) SANS NUM CDE             
              IF (VE-RAYON(I-TAB) = '51' OR '54')  OR                           
                 (VE-RAYON(I-TAB) = '52' AND VE-BCOM NOT > SPACES)              
                 ADD  VE-MREM   (I-TAB)  TO IRE000-VAL-REMIS                    
                 ADD  VE-RABAIS (I-TAB)  TO IRE000-VAL-REMIS                    
                 IF IRE000-VAL-REMIS NOT = ZERO                                 
                    SET  EXISTE-REMISE    TO TRUE                               
                 END-IF                                                         
                 IF VE-FORCE(I-TAB) = '1'                                       
                    SET EXISTE-FORCE   TO TRUE                                  
                    MOVE VE-QTE-PU (I-TAB)    TO W-QTE-PU                       
                 END-IF                                                         
                 COMPUTE IRE000-VAL-THEOR =                                     
                                     (VE-PUF (I-TAB) * VE-QTE (I-TAB))          
                 IF IRE000-VAL-THEOR > W-QTE-PU                                 
                    AND W-QTE-PU  NOT = ZERO                                    
                    COMPUTE W-DIFFERENCE =                                      
                               IRE000-VAL-THEOR - W-QTE-PU                      
                    MOVE W-DIFFERENCE         TO IRE000-VAL-FORC-M              
                    MOVE 0                    TO W-DIFFERENCE                   
                 END-IF                                                         
                 IF IRE000-VAL-THEOR < W-QTE-PU                                 
                    COMPUTE W-DIFFERENCE =                                      
                               W-QTE-PU - IRE000-VAL-THEOR                      
                    MOVE W-DIFFERENCE         TO IRE000-VAL-FORC-P              
      * FORCAGE PLUS                                                            
                    MOVE W-QTE-PU             TO IRE000-VAL-THEOR               
                    MOVE 0                    TO W-DIFFERENCE                   
                 END-IF                                                         
                 MOVE VE-CODIC(I-TAB) TO W-CODIC                                
                 MOVE VE-QTE  (I-TAB) TO W-QTE                                  
                 PERFORM ECRITURE-FRE200                                        
                 MOVE 0      TO W-QTE-PU                                        
                 INITIALIZE DSECT-IRE000                                   02340
                 SET PAS-EXISTE-FORCE  TO TRUE                             00001
                 SET PAS-EXISTE-REMISE TO TRUE                             00001
              END-IF                                                            
           END-PERFORM.                                                         
                                                                        00003330
       FIN-RECUP-FORC-ENR1. EXIT.                                       00002600
                                                                        00002550
      * ------------------------------------------------------------ *  00002560
      *               TRAITEMENT D'UNE VENTE    ENR2                 *  00002570
      * ------------------------------------------------------------ *  00002580
                                                                        00002590
       RECUP-FORC-ENR2   SECTION.                                       00002600
      *    DISPLAY 'VE-CLE ' VE-CLE.                                    00002610
           PERFORM VARYING I-TAB FROM 1 BY 1 UNTIL                      00002620
      *{ reorder-array-condition 1.1                                            
      *                                     VE-1-CODIC(I-TAB) = SPACES  00002620
      *                                     OR I-TAB > I-MAX-ENR2               
      *--                                                                       
                                            I-TAB > I-MAX-ENR2                  
                                            OR VE-1-CODIC(I-TAB) =              
                                                                  SPACES        
      *}                                                                        
      *    SI RAYON 51 (ELA) OR 54 (DACEM) OR 52 (TLM) SANS NUM CDE             
              IF (VE-1-RAYON(I-TAB) = '51' OR '54')  OR                         
                 (VE-1-RAYON(I-TAB) = '52' AND VE-BCOM NOT > SPACES)            
                 ADD  VE-1-MREM   (I-TAB)  TO IRE000-VAL-REMIS                  
                 ADD  VE-1-RABAIS (I-TAB)  TO IRE000-VAL-REMIS                  
                 IF IRE000-VAL-REMIS NOT = ZERO                                 
                    SET  EXISTE-REMISE    TO TRUE                               
                 END-IF                                                         
                 IF VE-1-FORCE(I-TAB) = '1'                                     
                    SET EXISTE-FORCE   TO TRUE                                  
                    MOVE VE-1-QTE-PU (I-TAB)    TO W-QTE-PU                     
                 END-IF                                                         
                 COMPUTE IRE000-VAL-THEOR =                                     
                              (VE-1-PUF (I-TAB) * VE-1-QTE (I-TAB))             
                 IF IRE000-VAL-THEOR > W-QTE-PU                                 
                    AND W-QTE-PU  NOT = ZERO                                    
                    COMPUTE W-DIFFERENCE =                                      
                               IRE000-VAL-THEOR - W-QTE-PU                      
                    MOVE W-DIFFERENCE         TO IRE000-VAL-FORC-M              
                    MOVE 0                    TO W-DIFFERENCE                   
                 END-IF                                                         
                 IF IRE000-VAL-THEOR < W-QTE-PU                                 
                    COMPUTE W-DIFFERENCE =                                      
                               W-QTE-PU - IRE000-VAL-THEOR                      
                    MOVE W-DIFFERENCE         TO IRE000-VAL-FORC-P              
                    MOVE 0                    TO W-DIFFERENCE                   
                 END-IF                                                         
                 MOVE VE-1-CODIC(I-TAB) TO W-CODIC                              
                 MOVE VE-1-QTE  (I-TAB) TO W-QTE                                
                 PERFORM ECRITURE-FRE200                                        
                 MOVE 0      TO W-QTE-PU                                        
                 INITIALIZE DSECT-IRE000                                   02340
                 SET PAS-EXISTE-FORCE  TO TRUE                             00001
                 SET PAS-EXISTE-REMISE TO TRUE                             00001
              END-IF                                                            
           END-PERFORM.                                                         
           PERFORM LECTURE-FEMPORT.                                     00002490
                                                                        00003330
       FIN-RECUP-FORC-ENR2. EXIT.                                       00002600
                                                                        00003350
      * ------------------------------------------------------------ *  00003360
      *               CHARGEMENT LIGNE FICHIER EXTRACTION            *  00003370
      * ------------------------------------------------------------ *  00003380
                                                                        00003390
       ECRITURE-FRE200 SECTION.                                                 
                                                                        00003410
           MOVE   VE-SOC                   TO    IRE000-NSOCIETE.       00003630
           MOVE   VE-MAG                   TO    IRE000-NLIEU.          00003640
           MOVE   W-VEND                   TO    W-VENDEUR4             00003650
           MOVE   W-VENDEUR                TO    IRE000-VENDEUR         00003650
           MOVE   W-CODIC                  TO    IRE000-NCODIC.         00003650
           MOVE   VE-NUMCAIS               TO    IRE000-CAISSE.                 
           MOVE   VE-TRANS                 TO    IRE000-TRANS.                  
           MOVE   W-QTE                    TO    IRE000-QTE.            00003660
           MOVE   W-DATE2                  TO    IRE000-DATE            00003670
           MOVE   W-DATTRANS               TO W-AAMMJJ3                         
           IF W-AA3 < '93'                                                      
              MOVE '20' TO W-SS3                                                
           ELSE                                                                 
              MOVE '19' TO W-SS3                                                
           END-IF                                                               
           MOVE   W-DATE3                  TO    IRE000-DATE            00003670
                                                                        00003740
           WRITE ENR-FRE200 FROM DSECT-IRE000.                          00003750
                                                                        00003760
           ADD 1 TO CPT-FRE200.                                         00003770
                                                                        00003780
                                                                        00003790
       FIN-ECRITURE-FRE200. EXIT.                                               
      * ------------------------ FIN-ANORMALE ---------------------- *  00004110
                                                                        00004120
       FIN-ANORMALE SECTION.                                            00004130
                                                                        00004140
           DISPLAY '*****************************************'.         00004150
           DISPLAY '***** FIN ANORMALE DU PROGRAMME *********'.         00004160
           DISPLAY '*****************************************'.         00004170
           CLOSE FRE200 FEMPORT.                                        00004180
           MOVE 'BRE200' TO ABEND-PROG.                                 00004190
           CALL 'ABEND' USING ABEND-PROG ABEND-MESS.                    00004200
                                                                        00004210
       FIN-FIN-ANORMALE. EXIT.                                          00004220
                                                                        00004230
      * ------------------------------------------------------------ *  00003820
      *                      F I N     B R E 2 0 0                   *  00003830
      * ------------------------------------------------------------ *  00003840
                                                                        00003850
       FIN-BRE200 SECTION.                                              00003860
                                                                        00003870
           CLOSE FEMPORT FRE200.                                        00003920
           PERFORM COMPTE-RENDU.                                        00003930
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00003940
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        00003950
       FIN-FIN-BRE200. EXIT.                                            00003960
                                                                        00003970
      * ------------------------ COMPTE-RENDU ---------------------- *  00003980
                                                                        00003990
       COMPTE-RENDU SECTION.                                            00004000
                                                                        00004010
           DISPLAY '**'.                                                00004020
           DISPLAY '**          B R E 2 0 0 '.                          00004030
           DISPLAY '**'.                                                00004040
           DISPLAY '** NOMBRE DE LECTURES FEMPORT: '  CPT-FEMPORT.      00004050
           DISPLAY '** NOMBRE D''ECRITURES FRE200 : ' CPT-FRE200.       00004060
           DISPLAY '**'.                                                00004070
                                                                        00004080
       FIN-COMPTE-RENDU. EXIT.                                          00004090
