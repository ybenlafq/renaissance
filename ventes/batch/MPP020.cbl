      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       TITLE 'MODULE DE CALCUL DE L''ENVELOPPE D''UNE CATEGORIE PRIME'.         
       PROGRAM-ID. MPP020.                                                      
       AUTHOR. DSA003.                                                          
      ******************************************************************        
      *  F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E   *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : PREPARATION DE PAYE                              *        
      *  PROGRAMME  : MPP020                                           *        
      *  TITRE      : MODULE DE CALCUL DE L'ENVELOPPE D'UNE CATEGORIE  *        
      *               DE PRIME                                         *        
      *  CREATION   : 17/03/1997                                       *        
      *  FONCTION   : - PONDERATION DES INDICATEURS D'ACTIVITE         *        
      *               - CALCUL GLOBAL DE LA SOMME A REPARTIR           *        
      *                 ENTRE LES MEMBRES DE LA CATEGORIE DE PRIME.    *        
      *                                                                *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       DATA DIVISION.                                                           
      ******************************************************************        
      *                         W O R K I N G                          *        
      ******************************************************************        
       WORKING-STORAGE SECTION.                                                 
       01  FILLER                    PIC X(16) VALUE '*** WORKING  ***'.        
       01  WZONES-OPERATIONS.                                                   
           05 WCOPERATEUR            PIC X                  VALUE SPACE.        
           05 WPOPERANDE1            PIC S9(11)V9(4) COMP-3 VALUE +0.           
              88 WPOPERANDE1-NON-SAISIE        VALUE 99999999999,9999.          
           05 WPOPERANDE2            PIC S9(11)V9(4) COMP-3 VALUE +0.           
              88 WPOPERANDE2-NON-SAISIE        VALUE 99999999999,9999.          
           05 WPRESULTAT-D           PIC S9(13)V9(4)        VALUE +0.           
              88 WPRESULTAT-D-NON-SAISIE       VALUE 99999999999,9999.          
           05 WPRESULTAT-E           PIC S9(13)             VALUE +0.           
              88 WPRESULTAT-E-NON-SAISIE       VALUE 99999999999.               
       01  TOP-PONDERATION           PIC X(01)             VALUE SPACE.         
           88 PAS-DE-PONDERATION                           VALUE SPACE.         
           88 PONDERATION                                  VALUE 'O'.           
       01  FILLER                    PIC X(16) VALUE '***   AIDA   ***'.        
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : PREPARATION DE PAYE                              *        
      *  TRANSACTION: PP00                                             *        
      *  TITRE      : COMMAREA DE LA TRANSACTION PP00                  *        
      *  LONGUEUR   : 4096                                             *        
      *                                                                *        
      * Z-COMMAREA, D' UNE LONGUEUR DE 4096 C, COMPRENANT :            *        
      * 1 - LES ZONES RESERVEES A AIDA                           100 C *        
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS             20 C *        
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT          100 C *        
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP               152 C *        
      * 5 - LES ZONES RESERVEES APPLICATIVES                    3724 C *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
      *--- NOMBRE DE POSTES MAXIMUM POUR TYPE RUBRIQUE ET RUBRIQUE              
      *---(A METTRE A JOUR A CHAQUE CHANGEMENT DU NOMBRE D' OCCURS)             
       01  COMM-PP00-NRUB-MAX           PIC 9(02)            VALUE 14.          
       01  COMM-PP00-NRUB1-MAX          PIC 9(02)            VALUE 10.          
       01  COMM-PP00-NRUB2-MAX          PIC 9(02)            VALUE 02.          
       01  COMM-PP00-NRUB2-FIN          PIC 9(02)            VALUE 12.          
       01  COMM-PP00-NRUB3-MAX          PIC 9(02)            VALUE 02.          
       01  COMM-PP00-NLIGNE             PIC 9(02).                              
           88 COMM-PP00-LIGNE-MOUVT     VALUE 01 THRU 10.                       
           88 COMM-PP00-LIGNE-INTER     VALUE 11 THRU 12.                       
           88 COMM-PP00-LIGNE-INTER1    VALUE 11.                               
           88 COMM-PP00-LIGNE-INTER2    VALUE 12.                               
           88 COMM-PP00-LIGNE-FRANC     VALUE 13 THRU 14.                       
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-PP00-LONG-COMMAREA       PIC S9(4) COMP   VALUE +4096.           
      *--                                                                       
       01  COM-PP00-LONG-COMMAREA       PIC S9(4) COMP-5   VALUE +4096.         
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *--- ZONES RESERVEES A AIDA -------------------------------- 100          
           02 FILLER-COM-AIDA           PIC X(100).                             
      *                                                                         
      *--- ZONES RESERVEES EN PROVENANCE DE CICS ----------------- 020          
           02 COMM-CICS-APPLID          PIC X(08).                              
           02 COMM-CICS-NETNAM          PIC X(08).                              
           02 COMM-CICS-TRANSA          PIC X(04).                              
      *                                                                         
      *--- ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION --- 100          
           02 COMM-DATE-SIECLE          PIC X(02).                              
           02 COMM-DATE-ANNEE           PIC X(02).                              
           02 COMM-DATE-MOIS            PIC X(02).                              
           02 COMM-DATE-JOUR            PIC 9(02).                              
           02 COMM-DATE-QNTA            PIC 9(03).                              
           02 COMM-DATE-QNT0            PIC 9(05).                              
           02 COMM-DATE-BISX            PIC 9(01).                              
           02 COMM-DATE-JSM             PIC 9(01).                              
           02 COMM-DATE-JSM-LC          PIC X(03).                              
           02 COMM-DATE-JSM-LL          PIC X(08).                              
           02 COMM-DATE-MOIS-LC         PIC X(03).                              
           02 COMM-DATE-MOIS-LL         PIC X(09).                              
           02 COMM-DATE-SSAAMMJJ        PIC X(08).                              
           02 COMM-DATE-AAMMJJ          PIC X(06).                              
           02 COMM-DATE-JJMMSSAA        PIC X(08).                              
           02 COMM-DATE-JJMMAA          PIC X(06).                              
           02 COMM-DATE-JJ-MM-AA        PIC X(08).                              
           02 COMM-DATE-JJ-MM-SSAA      PIC X(10).                              
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS        PIC 9(02).                              
              05 COMM-DATE-SEMAA        PIC 9(02).                              
              05 COMM-DATE-SEMNU        PIC 9(02).                              
           02 COMM-DATE-FILLER          PIC X(07).                              
      *                                                                         
      *--- ZONES RESERVEES TRAITEMENT DU SWAP -------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS            PIC S9(4) COMP VALUE -1.                
      *--                                                                       
           02 COMM-SWAP-CURS            PIC S9(4) COMP-5 VALUE -1.              
      *}                                                                        
           02 COMM-SWAP-ATTR            OCCURS 150 PIC X(1).                    
      *                                                                         
      *--- ZONES APPLICATIVES PP00 GENERALES --------------------- 724          
           02 COMM-PP00-APPLI.                                                  
      *-      LIEU DE SAISIE                                                    
              05 COMM-PP00-NLIEUORIG       PIC X(03).                           
              05 COMM-PP00-CTYPSOC         PIC X(03).                           
      *-      MAGASIN DE TRAITEMENT                                             
              05 COMM-PP00-MAGASIN.                                             
                 10 COMM-PP00-NSOCIETE        PIC X(03).                        
                 10 COMM-PP00-NLIEU           PIC X(03).                        
                 10 COMM-PP00-LLIEU           PIC X(20).                        
                 10 COMM-PP00-CGRPMAG         PIC X(02).                        
                 10 COMM-PP00-DMOISPAYE       PIC X(06).                        
                 10 COMM-PP00-DMOISENCOURS    PIC X(06).                        
      *-      DONNEES COMMUNES A TOUS LES PROGRAMMES                            
              05 COMM-PP00-DONNEES.                                             
                 10 COMM-PP00-WDEVERS         PIC X(01).                        
                    88 COMM-PP00-DEVERSEMENT-NON-EFF      VALUE ' '.            
      *{ remove-comma-in-dde 1.5                                                
      *             88 COMM-PP00-DEVERSEMENT-EFFECTUE     VALUE '1','2'.        
      *--                                                                       
                    88 COMM-PP00-DEVERSEMENT-EFFECTUE     VALUE '1' '2'.        
      *}                                                                        
                    88 COMM-PP00-ENVELOPPE-CREEE          VALUE '2'.            
                 10 COMM-PP00-MESSAGE.                                          
                    15 COMM-PP00-CODRET          PIC X(01).                     
                       88 COMM-PP00-CODRET-OK               VALUE ' '.          
                       88 COMM-PP00-CODRET-ERREUR           VALUE '1'.          
                 10 COMM-PP00-LIBERR          PIC X(58).                        
                 10 COMM-PP00-TOP-SORTIE      PIC X(01).                        
                    88 COMM-PP00-SORTIE-NON-CONFIRMEE       VALUE ' '.          
                    88 COMM-PP00-SORTIE-CONFIRMEE           VALUE '1'.          
              05 COMM-PP00-OPTION.                                              
                 10 COMM-PP00-OPTPARAM     PIC X(02).                           
                 10 COMM-PP00-CPRIME       PIC X(05).                           
                 10 COMM-PP00-LOPTION      PIC X(20).                           
              05 COMM-PP00-TAB-NB          PIC 9(02).                           
              05 COMM-PP00-TAB-MENU        OCCURS 16.                           
                 10 COMM-PP00-TAB-COPTION  PIC X(02).                           
                 10 COMM-PP00-TAB-CPRIME   PIC X(05).                           
                 10 COMM-PP00-TAB-LOPTION  PIC X(20).                           
              05 COMM-PP00-DMOISSTAT       PIC X(06).                           
              05 COMM-PP00-FILLER          PIC X(148).                          
      *                                                                         
      *--- ZONES APPLICATIVES PP00 ------------------------------ 3000          
           02 COMM-PP00-REDEFINES          PIC X(3000).                         
      *                                                                         
      *--- ZONES APPLICATIVES PP10 -----------------------------------          
           02 COMM-PP10-APPLI REDEFINES COMM-PP00-REDEFINES.                    
      *-      TABLEAU PRIMES C                                                  
              05 COMM-PP10-TAB-PPPRI       OCCURS 2.                            
                 10 COMM-PP10-CPRIME          PIC X(05).                        
                 10 COMM-PP10-LNOMZONE        PIC X(10).                        
                 10 COMM-PP10-WEXISTENCE      PIC X(01).                        
                    88 COMM-PP10-RTPP00-NON-CREE            VALUE ' '.          
                    88 COMM-PP10-RTPP00-EXISTE              VALUE '1'.          
                 10 COMM-PP10-PENVELOPPE      PIC S9(07)V99 COMP-3.             
      *                                                                         
      *--- ZONES APPLICATIVES PP20 -----------------------------------          
           02 COMM-PP20-APPLI REDEFINES COMM-PP00-REDEFINES PIC X(3000).        
      *                                                                         
      *--- ZONES APPLICATIVES PP30 -----------------------------------          
           02 COMM-PP30-APPLI REDEFINES COMM-PP00-REDEFINES.                    
              05 COMM-PP30-FLAG-PP10       PIC X(01).                           
                 88 COMM-PP30-CREAT-PP10                    VALUE ' '.          
                 88 COMM-PP30-MODIF-PP10                    VALUE '1'.          
              05 COMM-PP30-DB2.                                                 
                 07 COMM-PP30-TABLE           OCCURS 14.                        
                    10 COMM-PP30-NLIGNE          PIC X(02).                     
                    10 COMM-PP30-PART1.                                         
                       15 COMM-PP30-NRUB         PIC X(02).                     
                       15 COMM-PP30-LRUB         PIC X(30).                     
                       15 COMM-PP30-CORIGINEZIN  PIC X(02).                     
                       15 COMM-PP30-CORIGINEZ1   PIC X(02).                     
                       15 COMM-PP30-PMONTANTZ1   PIC S9(11)V9999 COMP-3.        
                       15 COMM-PP30-CORIGINEZ2   PIC X(02).                     
                       15 COMM-PP30-PMONTANTZ2   PIC S9(11)V9999 COMP-3.        
                       15 COMM-PP30-CORIGINEZ3   PIC X(02).                     
                       15 COMM-PP30-PMONTANTZ3   PIC S9(11)V9999 COMP-3.        
                       15 COMM-PP30-COPERATEUR1  PIC X(01).                     
                       15 COMM-PP30-COPERATEUR2  PIC X(01).                     
                       15 COMM-PP30-CPRIORITE    PIC X(01).                     
                       15 COMM-PP30-CCALCUL      PIC X(01).                     
                    10 COMM-PP30-PART2.                                         
                      15 COMM-PP30-PMONTANTZIN PIC S9(11)V9999 COMP-3.          
                 07 COMM-PP30-LTYPRUB            PIC X(30) OCCURS 02.           
      *                                                                         
      *                                                                 00000000
      ***************************************************************** 00000100
      *   SAUVEGARDE  DE ZONES IMPORTANTES EN CAS DE PLANTAGE         * 00000200
      ***************************************************************** 00000300
      *                                                               * 00000400
      * DEBUGGIN EST UNE ZONE DONT LE BUT EST DE...                   * 00000507
      *                                                               * 00000601
      *                                                               * 00000700
      * KONTROL  = 0  PAS D'ERREUR LOGIQUE.                           * 00000800
      *          = 1  ERREUR AVEC INTERRUPTION DU TRAITEMENT.         * 00000900
      *                                                               * 00001000
      * CODE-RETOUR = CODE RETOUR DE LA SECTION QUE L'ON VIENT        * 00001100
      *               D'EXECUTER.                                     * 00001200
      *                                                               * 00001300
      ***************************************************************** 00001400
      *                                                                 00001500
       01  FILLER.                                                      00001600
      *                                                                 00001700
         02  FILLER PIC X(20) VALUE '* WORKING--STORAGE *'.             00001800
      *                                                                 00001900
         02  FILLER      PIC X(13)          VALUE 'DEBUGGIN.....'.      00002008
         02  DEBUGGIN    PIC X(03)          VALUE 'NON'.                00002107
      *                                                                 00004300
         02  FILLER      PIC X(15)          VALUE 'KONTROL........'.    00004400
         02  KONTROL     PIC X(1)           VALUE '0'.                  00004500
           88  ERREUR                     VALUE '1'.                    00004701
           88  OK                         VALUE '0'.                    00004800
      *                                                                 00004900
         02  FILLER      PIC X(15)          VALUE 'CODE-RETOUR....'.    00005000
         02  CODE-RETOUR PIC X(1)           VALUE '0'.                  00005100
           88  TROUVE                     VALUE '0'.                    00005200
           88  NORMAL                     VALUE '0'.                    00005300
           88  DATE-OK                    VALUE '0'.                    00005400
           88  SELECTE                    VALUE '0'.                    00005500
           88  NON-TROUVE                 VALUE '1'.                    00005600
           88  NON-SELECTE                VALUE '1'.                    00005700
           88  ANORMAL                    VALUE '1'.                    00005800
           88  EXISTE-DEJA                VALUE '2'.                    00005900
           88  FIN-FICHIER                VALUE '3'.                    00006000
           88  FIN-DONNEES                VALUE '3'.                    00006101
           88  ERREUR-DATE                VALUE '4'.                    00006200
           88  ERREUR-FORMAT              VALUE '5'.                    00006300
           88  ERREUR-HEURE               VALUE '6'.                    00006400
      *                                                                 00006600
      ***************************************************************** 00008100
      *     INDICES                                                     00008200
      ***************************************************************** 00008300
      *                                                                 00008400
         02  FILLER      PIC X(13)          VALUE 'INDICE-IA....'.      00008501
         02  IA          PIC S9(5) COMP-3   VALUE +0.                   00008601
           88  FIN-BOUCLE-IA              VALUE +999.                   00008700
      *                                                                 00008800
         02  FILLER      PIC X(13)          VALUE 'INDICE-AI....'.      00008901
         02  AI          PIC S9(5) COMP-3   VALUE +0.                   00009001
           88  FIN-BOUCLE-AI              VALUE +999.                   00009100
      *                                                                 00009200
         02  FILLER      PIC X(13)          VALUE 'INDICE-IT....'.      00009301
         02  IT          PIC S9(5) COMP-3   VALUE +0.                   00009401
           88  FIN-TABLE                  VALUE +999.                   00009500
      *                                                                 00009600
         02  FILLER      PIC X(13)          VALUE 'INDICE-LIGNE.'.      00009701
         02  IL          PIC S9(5) COMP-3   VALUE +0.                   00009801
           88  FIN-BOUCLE-IL              VALUE +999.                   00009900
      *                                                                 00010000
         02  FILLER      PIC X(13)          VALUE 'INDICE-PAGE..'.      00010101
         02  IP          PIC S9(5) COMP-3   VALUE +0.                   00010201
      *                                                                 00010300
         02  FILLER            PIC X(15)    VALUE 'ETAT-CONSULT...'.    00010400
         02  ETAT-CONSULTATION PIC X        VALUE '0'.                  00010500
           88  FIN-CONSULTATION           VALUE '1'.                    00010600
      *                                                                 00010700
         02  FILLER            PIC X(15)    VALUE 'ACTIVITE.......'.    00010803
         02  ACTIVITE          PIC X        VALUE '0'.                  00010903
           88  FIN-EXTRACTION             VALUE '1'.                    00011003
      *                                                                 00011103
         02  FILLER      PIC X(7)           VALUE 'NUM-9..'.            00011201
         02  NUM-9       PIC 9(9)           VALUE 0.                    00011301
         02  NUM-8       REDEFINES NUM-9 PIC 9(8).                      00011401
         02  NUM-7       REDEFINES NUM-9 PIC 9(7).                      00011501
         02  NUM-6       REDEFINES NUM-9 PIC 9(6).                      00011601
         02  NUM-5       REDEFINES NUM-9 PIC 9(5).                      00011701
         02  NUM-4       REDEFINES NUM-9 PIC 9(4).                      00011801
         02  NUM-3       REDEFINES NUM-9 PIC 9(3).                      00011901
         02  NUM-2       REDEFINES NUM-9 PIC 9(2).                      00012001
         02  NUM-1       REDEFINES NUM-9 PIC 9(1).                      00012101
      *                                                                 00012200
         02  FILLER      PIC X(12)          VALUE 'COMPTEUR....'.       00012300
         02  COMPTEUR    PIC S9(7) COMP-3   VALUE +0.                   00012400
      *                                                                 00012500
      ***************************************************************** 00012600
      *     CODE ABANDON                                                00012700
      ***************************************************************** 00012800
      *                                                                 00012900
         02  FILLER              PIC X(15) VALUE 'CODE-ABANDON...'.     00013000
         02  CODE-ABANDON        PIC X VALUE SPACE.                     00013100
         88  AIDA-ABANDON            VALUE 'K'.                         00013204
         88  TACHE-ABANDON           VALUE 'T'.                         00013300
         88  BATCH-ABANDON           VALUE 'B'.                         00013401
         88  DL1-ABANDON             VALUE 'D'.                         00013500
      *                                                                 00013600
      ***************************************************************** 00013700
      *     DATE ET HEURE DE COMPILATION                                00013800
      ***************************************************************** 00013900
      *                                                                 00014000
         02  FILLER      PIC X(16)          VALUE 'COMPILATION DU :'.   00014104
         02  Z-WHEN-COMPILED.                                           00014201
             03 Z-WHEN-COMPILED-DATE        PIC X(8)    VALUE SPACE.    00014301
             03 Z-WHEN-COMPILED-HEURE       PIC X(8)    VALUE SPACE.    00014401
      *                                                                 00014500
      ***************************************************************** 00015500
      *     ZONES   FICHIER                                             00015609
      ***************************************************************** 00015700
      *                                                                 00018900
           02  FILE-NAME            PIC X(8)       VALUE SPACE.         00019009
           02  PATH-NAME            PIC X(8)       VALUE SPACE.         00019109
           02  FILE-STATUS          PIC X(2)       VALUE '**'.          00019209
           02  FILE-FUNCTION        PIC X(4)       VALUE '****'.        00019309
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FILE-LONG            PIC S9(4) COMP VALUE +0.            00019509
      *--                                                                       
           02  FILE-LONG            PIC S9(4) COMP-5 VALUE +0.                  
      *}                                                                        
      *                                                                 00019609
      ***************************************************************** 00019709
      *     ZONES   DIVERSES                                            00019809
      ***************************************************************** 00019909
      *                                                                 00020009
         02  CODE-FIN-BOUCLE-IA        PIC S9(3) COMP-3 VALUE +999.     00020100
         02  CODE-FIN-BOUCLE-AI        PIC S9(3) COMP-3 VALUE +999.     00020200
         02  CODE-FIN-TABLE            PIC S9(3) COMP-3 VALUE +999.     00020300
      *                                                                 00020400
         02  CODE-CONSULTATION-EN-COURS    PIC X     VALUE '0'.         00020500
         02  CODE-FIN-CONSULTATION         PIC X     VALUE '1'.         00020600
         02  CODE-FIN-EXTRACTION           PIC X     VALUE '1'.         00020703
      *                                                                 00020800
         02  APOSTROPHE                    PIC X     VALUE QUOTE.       00020900
      *                                                                 00021000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *  02  BINAIRE                       PIC S9(4) COMP VALUE +0.     00021100
      *--                                                                       
         02  BINAIRE                       PIC S9(4) COMP-5 VALUE +0.           
      *}                                                                        
         02  REDEF-BINAIRE REDEFINES BINAIRE PIC X(2).                  00021200
      *                                                                 00022000
      *****************************************************************         
      * ZONES D'INTERFACE POUR LES ACCES AUX DATA BASES DB2/SQL       *         
      ****************************************************** SYKWSQ10 *         
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
           05  TRACE-SQL-MESSAGE.                                               
      *                                                                         
             10  FILLER         PIC  X(4)  VALUE 'TAB='.                        
             10  TABLE-NAME     PIC  X(18)   VALUE SPACES.                      
      *                                                                         
      *      10  FILLER         PIC  X(1)   VALUE SPACES.                       
             10  FILLER         PIC  X(4)  VALUE 'MOD='.                        
             10  MODEL-NAME      PIC  X(6)   VALUE SPACES.                      
      *                                                                         
             10  FILLER         PIC  X(1)   VALUE SPACES.                       
             10  FILLER         PIC  X(5)   VALUE 'FUNC='.                      
             10  TRACE-SQL-FUNCTION  PIC  X(7)  VALUE  SPACES.                  
                 88  CODE-CONNECT               VALUE  'CONNECT'.               
                 88  CODE-SELECT                VALUE  'SELECT'.                
                 88  CODE-INSERT                VALUE  'INSERT'.                
                 88  CODE-UPDATE                VALUE  'UPDATE'.                
                 88  CODE-DELETE                VALUE  'DELETE'.                
                 88  CODE-PREPARE               VALUE  'PREPARE'.               
                 88  CODE-DECLARE               VALUE  'DECLARE'.               
                 88  CODE-OPEN                  VALUE  'OPEN'.                  
                 88  CODE-FETCH                 VALUE  'FETCH'.                 
                 88  CODE-CLOSE                 VALUE  'CLOSE'.                 
      *                                                                         
             10  FILLER         PIC  X(1)   VALUE SPACES.                       
             10  FILLER         PIC  X(8)   VALUE 'SQLCODE='.                   
             10  TRACE-SQL-CODE      PIC  -Z(7)9 VALUE  ZEROS.                  
      *                                                                         
           05  FILLER.                                                          
      *                                                                         
             10  FUNC-SQL       PIC  X(8)   VALUE  SPACES.                      
             10  FUNC-CONNECT   PIC  X(8)   VALUE 'CONNECT'.                    
             10  FUNC-SELECT    PIC  X(8)   VALUE 'SELECT'.                     
             10  FUNC-INSERT    PIC  X(8)   VALUE 'INSERT'.                     
             10  FUNC-UPDATE    PIC  X(8)   VALUE 'UPDATE'.                     
             10  FUNC-DELETE    PIC  X(8)   VALUE 'DELETE'.                     
             10  FUNC-PREPARE   PIC  X(8)   VALUE 'PREPARE'.                    
             10  FUNC-DECLARE   PIC  X(8)   VALUE 'DECLARE'.                    
             10  FUNC-OPEN      PIC  X(8)   VALUE 'OPEN'.                       
             10  FUNC-FETCH     PIC  X(8)   VALUE 'FETCH'.                      
             10  FUNC-CLOSE     PIC  X(8)   VALUE 'CLOSE'.                      
             10  FILLER-SQL     PIC  X(12)  VALUE SPACE.                        
      *                                                                         
           05  FILLER.                                                          
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10  SQL-CODE       PIC  S9(9) COMP-4.                              
      *--                                                                       
             10  SQL-CODE       PIC  S9(9) COMP-5.                              
      *}                                                                        
       EJECT                                                                    
       01  Z-INOUT                   PIC X(236)            VALUE SPACES.        
      *                                                                 00000000
      ***************************************************************** 00000100
      * ZONE D'INTERFACE POUR LA GESTION DES ERREURS NON RECOUVRABLES * 00000202
      ***************************************************************** 00000300
      *                                                                 00000401
       01  Z-ERREUR REDEFINES Z-INOUT.                                  00000503
      *                                                                 00000602
           05  FILLER                   PIC X(256).                     00000703
      *                                                                 00000802
           05  Z-ERREUR-TRACE-MESS      PIC X(80).                      00000903
      *                                                                 00001002
       01  FILLER.                                                      00002003
      *                                                                 00003002
           05  TRACE-MESSAGE.                                           00004003
      *                                                                 00005002
               10  TRACE-MESS.                                          00006003
                   15 MESS1             PIC X(15)       VALUE SPACE.    00007002
                   15 MESS              PIC X(64)       VALUE SPACE.    00008002
      *                                                                 00009002
      ***************************************************************** 00010002
      *                                                                         
       LINKAGE SECTION.                                                         
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : PREPARATION DE PAYE                              *        
      *  PROGRAMME  : MPP20                                            *        
      *  TITRE      : COMMAREA DU MODULE TP DE CALCUL DE               *        
      *               L'ENVELOPPE D'UNE CATEGORIE DE PRIME             *        
      *  LONGUEUR   : 3000 C                                           *        
      *                                                                *        
      *  ENTREE     : COMM-PP20-RTPP00-EXISTE COMM-PP20-LTYPRUB        *        
      *               COMM-PP20-RUBRIQUE                               *        
      *  SORTIE     : COMM-PP20-MESSAGE       COMM-PP20-NLGERR         *        
      *               COMM-PP20-PENVELOPPE    COMM-PP20-PTOTAL         *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
       01  COMM-PP20-DONNEES.                                                   
      *--- CODE RETOUR + MESSAGE ------------------------------------ 62        
           05 COMM-PP20-MESSAGE.                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       10 COMM-PP20-CODRET           PIC S9(04) COMP.                    
      *--                                                                       
              10 COMM-PP20-CODRET           PIC S9(04) COMP-5.                  
      *}                                                                        
              10 COMM-PP20-LIBERR           PIC X(58).                          
           05 COMM-PP20-NLGERR          PIC 9(02).                              
      *--- DONNEES ENVELOPPE (RTPP00) ------------------------------- 17        
           05 COMM-PP20-RTPP00-EXISTE   PIC X(01).                              
              88 COMM-PP20-RTPP00-EXISTENCE                   VALUE '1'.        
           05 COMM-PP20-PENVELOPPE-F    PIC S9(11)V9999 COMP-3.                 
           05 COMM-PP20-PENVELOPPE-E    PIC S9(11)V9999 COMP-3.                 
      *---    DONNEES TYPE RUBRIQUE (1->MVTS 2->INTER 3->FRANCS) --- 111        
           05 COMM-PP20-TYPRUB          OCCURS 3.                               
              10 COMM-PP20-LTYPRUB         PIC X(30).                           
              10 COMM-PP20-PTOTAL          PIC S9(11)V99   COMP-3.              
      *---    DONNEES RUBRIQUE (14 * 127) ------------------------- 1778        
           05 COMM-PP20-RUBRIQUE        OCCURS 14.                              
              10 COMM-PP20-RTPP01-EXISTE   PIC X(01).                           
                 88 COMM-PP20-RTPP01-EXISTENCE                VALUE '1'.        
              10 COMM-PP20-NLIGNE          PIC X(02).                           
                 88 COMM-PP20-LIGNE-MOUVT          VALUE '01' THRU '10'.        
                 88 COMM-PP20-LIGNE-INTER          VALUE '11' THRU '12'.        
                 88 COMM-PP20-LIGNE-INTER1         VALUE '11'.                  
                 88 COMM-PP20-LIGNE-INTER2         VALUE '12'.                  
                 88 COMM-PP20-LIGNE-FRANC          VALUE '13' THRU '14'.        
              10 COMM-PP20-LRUB            PIC X(30).                           
              10 COMM-PP20-CORIGINEZIN     PIC X(02).                           
              10 COMM-PP20-CORIGINEZ1      PIC X(02).                           
              10 COMM-PP20-CORIGINEZ2      PIC X(02).                           
              10 COMM-PP20-CORIGINEZ3      PIC X(02).                           
              10 COMM-PP20-COPERATEUR1     PIC X(01).                           
              10 COMM-PP20-COPERATEUR2     PIC X(01).                           
              10 COMM-PP20-CPRIORITE       PIC X(01).                           
              10 COMM-PP20-CCALCUL         PIC X(01).                           
              10 COMM-PP20-RTPP01-FICHIER  PIC X(41).                           
              10 COMM-PP20-RTPP01-ECRAN.                                        
                 15 COMM-PP20-NRUB            PIC X(02).                        
                 15 COMM-PP20-PMONTANTZIN     PIC S9(11)V9999 COMP-3.           
                 15 COMM-PP20-PMONTANTZ1      PIC S9(11)V9999 COMP-3.           
                   88 COMM-PP20-Z1-NON-SAISIE VALUE 99999999999,9999.           
                 15 COMM-PP20-PMONTANTZ2      PIC S9(11)V9999 COMP-3.           
                   88 COMM-PP20-Z2-NON-SAISIE VALUE 99999999999,9999.           
                 15 COMM-PP20-PMONTANTZ3      PIC S9(11)V9999 COMP-3.           
                   88 COMM-PP20-Z3-NON-SAISIE VALUE 99999999999,9999.           
                 15 COMM-PP20-PRESULTAT       PIC S9(11)V99   COMP-3.           
                   88 COMM-PP20-RES-NON-SAISIE VALUE 99999999999,99.            
      *---    DONNEES SAUVEGARDE ZONE A ET B ------------------------ 17        
           05 COMM-PP20-SAVE.                                                   
              10 COMM-PP20-TOP-PASSAGE     PIC X(01).                           
                 88 COMM-PP20-PREMIER-PASSAGE                 VALUE ' '.        
              10 COMM-PP20-PMONTANT-A      PIC S9(11)V9999 COMP-3.              
              10 COMM-PP20-PMONTANT-B      PIC S9(11)V9999 COMP-3.              
      *--- DISPONIBLES -------------------------------------------- 1013        
           05 COMM-PP20-FILLER          PIC X(1013).                            
       PROCEDURE DIVISION  USING  COMM-PP20-DONNEES.                            
      *                                                                         
      ******************************************************************00001810
      *                      T R A I T E M E N T                       *00001820
      ******************************************************************00001810
      *                                                                         
       MODULE-PP20                    SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           PERFORM MODULE-ENTREE.                                               
      **** TRAITEMENT DES LIGNES DE RUBRIQUES                                   
           PERFORM VARYING IT FROM 1 BY 1 UNTIL IT > COMM-PP00-NRUB-MAX         
              PERFORM TRAITEMENT-LIGNE-RUBRIQUE                                 
           END-PERFORM                                                          
           PERFORM CALCUL-ENVELOPPE.                                            
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
       FIN-MODULE-PP20. EXIT.                                                   
      *                                                                         
       TRAITEMENT-LIGNE-RUBRIQUE      SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** TRAITEMENT D'UNE LIGNE DE RUBRIQUE                                   
           IF COMM-PP20-LIGNE-INTER1 (IT)                                       
              PERFORM CALCUL-B                                                  
           END-IF                                                               
           IF COMM-PP20-LIGNE-INTER2 (IT)                                       
              PERFORM CALCUL-E                                                  
           END-IF                                                               
           IF COMM-PP20-LIGNE-INTER (IT)                                        
           THEN PERFORM CALCUL-OPERATIONS-INTER                                 
           ELSE PERFORM CALCUL-OPERATIONS-LIGNE                                 
           END-IF                                                               
           PERFORM CUMUL-TOTAL-TYPE-RUBRIQUE.                                   
      *                                                                         
       FIN-TRAITEMENT-LIGNE-RUBRIQUE. EXIT.                                     
      *                                                                         
       MODULE-ENTREE                  SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           INITIALIZE COMM-PP20-MESSAGE    COMM-PP20-NLGERR                     
                      COMM-PP20-PTOTAL (1) COMM-PP20-PTOTAL (2)                 
                      COMM-PP20-PTOTAL (3).                                     
      *                                                                         
       FIN-MODULE-ENTREE. EXIT.                                                 
      *                                                                         
       CALCUL-B                       SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** SI LE CODE OPERATEUR POUR CALCUL DE E EST RENSEIGNE ALORS            
      **** CALCUL DE B (B=SEUIL A PARTIR DUQUEL ON EFFECTUE LA PONDERA.)        
      **** SI A (NB TOTAL MVTS) < B                                             
      ****    B=A   SEUIL NON DEPASSE, PAS DE PONDERATION                       
      **** SINON                                                                
      ****    B=B   SEUIL DEPASSE, PONDERATION SUR LES B PREMIERS               
      *                                                                         
           COMPUTE IL = IT + 1                                                  
           IF COMM-PP20-COPERATEUR2 (IL) NOT = SPACE                            
              IF COMM-PP20-PTOTAL (1) < COMM-PP20-PMONTANTZ3 (IT)               
      ****       SI A<B ALORS B=A                                               
                 MOVE COMM-PP20-PTOTAL (1)  TO COMM-PP20-PMONTANTZ1 (IT)        
                 MOVE ZEROES                TO COMM-PP20-PMONTANTZ1 (IL)        
                 SET  PAS-DE-PONDERATION    TO TRUE                             
              ELSE                                                              
                 MOVE COMM-PP20-PMONTANTZ3 (IT)                                 
                                            TO COMM-PP20-PMONTANTZ1 (IT)        
                 SET  PONDERATION           TO TRUE                             
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
       F-CALCUL-B. EXIT.                                                        
      *                                                                         
       CALCUL-E                       SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** OPERATION = CALCUL DU 1ER TERME DU 2EME CALCUL INTERMEDIAIRE         
      ****  (1ER TERME (E) = TOTAL DES LIGNES DE TYPE MOUVEMENTS (A)            
      ****           -,+ 1ER TERME DU 1ER CALCUL TYPE INTERMEDIAIRE (B))        
      *                                                                         
      **** REGLE DE GESTION :                                                   
      **** SI OPERATEUR ('+','-') ENTRE A ET B                                  
      ****    SI CORIGINE DE B = 'P' OU ('S' ET (A OU B CHANGE) )               
      ****    ALORS CALCUL ENTRE A ET B                                         
      **** SINON                                                                
      ****    PAS DE CALCUL                                                     
      *                                                                         
           COMPUTE IL = IT - 1                                                  
           IF PONDERATION AND                                                   
              COMM-PP20-COPERATEUR2 (IT) NOT = SPACE AND                        
             (COMM-PP20-CORIGINEZ1 (IT) = 'P' OR                                
             (COMM-PP20-CORIGINEZ1 (IT) = 'S' AND                               
             (COMM-PP20-PREMIER-PASSAGE       OR                                
             (COMM-PP20-PTOTAL (1) NOT = COMM-PP20-PMONTANT-A OR                
              COMM-PP20-PMONTANTZ1 (IL) NOT = COMM-PP20-PMONTANT-B))))          
              MOVE COMM-PP20-PTOTAL (1)        TO WPOPERANDE1                   
                                                  COMM-PP20-PMONTANT-A          
              MOVE COMM-PP20-COPERATEUR2 (IT)  TO WCOPERATEUR                   
              MOVE COMM-PP20-PMONTANTZ1 (IL)   TO WPOPERANDE2                   
                                                  COMM-PP20-PMONTANT-B          
              PERFORM OPERATION                                                 
              MOVE WPRESULTAT-D          TO COMM-PP20-PMONTANTZ1 (IT)           
           END-IF.                                                              
      *                                                                         
       F-CALCUL-E. EXIT.                                                        
      *                                                                         
       CALCUL-OPERATIONS-INTER        SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** CALCUL DES OPERATIONS D'UNE LIGNE INTERMEDIAIRE                      
      *    LIGNE : Z1 OPERATEUR1 Z2 = RESULTAT                                  
      *                                                                         
           INITIALIZE                    COMM-PP20-PRESULTAT (IT)               
      **** OPERATEUR 1 RENSEIGNE                                                
           IF COMM-PP20-COPERATEUR1 (IT) NOT = SPACE                            
      ****    R = Z1 OPERATEUR1 Z2                                              
              MOVE COMM-PP20-PMONTANTZ1 (IT)  TO WPOPERANDE1                    
              MOVE COMM-PP20-COPERATEUR1(IT)  TO WCOPERATEUR                    
              MOVE COMM-PP20-PMONTANTZ2 (IT)  TO WPOPERANDE2                    
              PERFORM CALCUL-RESULTAT-LIGNE                                     
           END-IF                                                               
      **** OPERATEUR 1 A BLANC                                                  
           IF COMM-PP20-COPERATEUR1 (IT) = SPACE                                
      ****    SI Z1 SAISIE ET DIFF�RENT DE 0                                    
      ****    ALORS R = Z1                                                      
      ****    SINON R = Z2                                                      
              IF COMM-PP20-CORIGINEZ1 (IT) NOT = SPACES AND                     
                 NOT COMM-PP20-Z1-NON-SAISIE (IT)                               
                 MOVE COMM-PP20-PMONTANTZ1 (IT)  TO                             
                      COMM-PP20-PRESULTAT  (IT)                                 
              ELSE                                                              
                 IF COMM-PP20-CORIGINEZ2 (IT) NOT = SPACES AND                  
                    NOT COMM-PP20-Z2-NON-SAISIE (IT)                            
                    MOVE COMM-PP20-PMONTANTZ2 (IT)  TO                          
                         COMM-PP20-PRESULTAT  (IT)                              
                 ELSE                                                           
                    SET  COMM-PP20-RES-NON-SAISIE (IT)  TO TRUE                 
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
       FIN-CALCUL-OPERATIONS-INTER. EXIT.                                       
      *                                                                         
       CALCUL-OPERATIONS-LIGNE        SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** CALCUL DES OPERATIONS D'UNE LIGNE MOUVEMENT OU FRANC                 
      *    LIGNE : Z1 OPERATEUR1 Z2 OPERATEUR2 Z3 = RESULTAT                    
      *                                                                         
           INITIALIZE                    COMM-PP20-PRESULTAT (IT)               
      **** OPERATEUR 1 ET OPERATEUR 2 RENSEIGNE                                 
           IF COMM-PP20-COPERATEUR1 (IT) NOT = SPACE AND                        
              COMM-PP20-COPERATEUR2 (IT) NOT = SPACE                            
      ****    CPRIORITE = 1 OU B ==> RI = Z1 OPERATEUR1 Z2                      
      ****                           R  = RI OPERATEUR2 Z3                      
              IF COMM-PP20-CPRIORITE (IT) = '1' OR                              
                 COMM-PP20-CPRIORITE (IT) = ' '                                 
                 MOVE COMM-PP20-PMONTANTZ1 (IT)  TO WPOPERANDE1                 
                 MOVE COMM-PP20-COPERATEUR1(IT)  TO WCOPERATEUR                 
                 MOVE COMM-PP20-PMONTANTZ2 (IT)  TO WPOPERANDE2                 
                 PERFORM OPERATION                                              
                 MOVE WPRESULTAT-D               TO WPOPERANDE1                 
                 MOVE COMM-PP20-COPERATEUR2(IT)  TO WCOPERATEUR                 
                 MOVE COMM-PP20-PMONTANTZ3 (IT)  TO WPOPERANDE2                 
                 PERFORM CALCUL-RESULTAT-LIGNE                                  
              END-IF                                                            
      ****    CPRIORITE = 2      ==> RI = Z2 OPERATEUR2 Z3                      
      ****                           R  = Z1 OPERATEUR1 RI                      
              IF COMM-PP20-CPRIORITE (IT) = '2'                                 
                 MOVE COMM-PP20-PMONTANTZ2 (IT)  TO WPOPERANDE1                 
                 MOVE COMM-PP20-COPERATEUR2(IT)  TO WCOPERATEUR                 
                 MOVE COMM-PP20-PMONTANTZ3 (IT)  TO WPOPERANDE2                 
                 PERFORM OPERATION                                              
                 MOVE COMM-PP20-PMONTANTZ1 (IT)  TO WPOPERANDE1                 
                 MOVE COMM-PP20-COPERATEUR1(IT)  TO WCOPERATEUR                 
                 MOVE WPRESULTAT-D               TO WPOPERANDE2                 
                 PERFORM CALCUL-RESULTAT-LIGNE                                  
              END-IF                                                            
           END-IF                                                               
      **** OPERATEUR 1 RENSEIGNE ET OPERATEUR 2 A BLANC                         
           IF COMM-PP20-COPERATEUR1 (IT) NOT = SPACE AND                        
              COMM-PP20-COPERATEUR2 (IT) = SPACE                                
      ****    SI Z2 DIFF�RENT DE 0                                              
      ****    ALORS R = Z1 OPERATEUR1 Z2                                        
      ****    SINON R = Z1 OPERATEUR1 Z3                                        
              IF COMM-PP20-CORIGINEZ2 (IT) NOT = SPACES                         
                 MOVE COMM-PP20-PMONTANTZ1 (IT)  TO WPOPERANDE1                 
                 MOVE COMM-PP20-COPERATEUR1(IT)  TO WCOPERATEUR                 
                 MOVE COMM-PP20-PMONTANTZ2 (IT)  TO WPOPERANDE2                 
                 PERFORM CALCUL-RESULTAT-LIGNE                                  
              ELSE                                                              
                 MOVE COMM-PP20-PMONTANTZ1 (IT)  TO WPOPERANDE1                 
                 MOVE COMM-PP20-COPERATEUR1(IT)  TO WCOPERATEUR                 
                 MOVE COMM-PP20-PMONTANTZ3 (IT)  TO WPOPERANDE2                 
                 PERFORM CALCUL-RESULTAT-LIGNE                                  
              END-IF                                                            
           END-IF                                                               
      **** OPERATEUR 1 A BLANC ET OPERATEUR 2 RENSEIGNE                         
           IF COMM-PP20-COPERATEUR1 (IT) = SPACE AND                            
              COMM-PP20-COPERATEUR2 (IT) NOT = SPACE                            
      ****    SI Z1 DIFF�RENT DE 0                                              
      ****    ALORS R = Z1 OPERATEUR2 Z3                                        
      ****    SINON R = Z2 OPERATEUR2 Z3                                        
              IF COMM-PP20-CORIGINEZ1 (IT) NOT = SPACES                         
                 MOVE COMM-PP20-PMONTANTZ1 (IT)  TO WPOPERANDE1                 
                 MOVE COMM-PP20-COPERATEUR2(IT)  TO WCOPERATEUR                 
                 MOVE COMM-PP20-PMONTANTZ3 (IT)  TO WPOPERANDE2                 
                 PERFORM CALCUL-RESULTAT-LIGNE                                  
              ELSE                                                              
                 MOVE COMM-PP20-PMONTANTZ2 (IT)  TO WPOPERANDE1                 
                 MOVE COMM-PP20-COPERATEUR2(IT)  TO WCOPERATEUR                 
                 MOVE COMM-PP20-PMONTANTZ3 (IT)  TO WPOPERANDE2                 
                 PERFORM CALCUL-RESULTAT-LIGNE                                  
              END-IF                                                            
           END-IF                                                               
      **** OPERATEUR 1 A BLANC ET OPERATEUR 2 A BLANC                           
           IF COMM-PP20-COPERATEUR1 (IT) = SPACE AND                            
              COMM-PP20-COPERATEUR2 (IT) = SPACE                                
      ****    SI Z1 SAISIE ET DIFF�RENT DE 0                                    
      ****    ALORS R = Z1                                                      
      ****    SINON                                                             
      ****       SI Z2 SAISIE ET DIFF�RENT DE 0                                 
      ****       ALORS R = Z2                                                   
      ****       SINON R = Z3                                                   
              IF COMM-PP20-CORIGINEZ1 (IT) NOT = SPACES AND                     
                 NOT COMM-PP20-Z1-NON-SAISIE (IT)                               
                 MOVE COMM-PP20-PMONTANTZ1 (IT)  TO                             
                      COMM-PP20-PRESULTAT  (IT)                                 
              ELSE                                                              
                 IF COMM-PP20-CORIGINEZ2 (IT) NOT = SPACES AND                  
                    NOT COMM-PP20-Z2-NON-SAISIE (IT)                            
                    MOVE COMM-PP20-PMONTANTZ2 (IT)  TO                          
                         COMM-PP20-PRESULTAT  (IT)                              
                 ELSE                                                           
                    IF COMM-PP20-CORIGINEZ3 (IT) NOT = SPACES AND               
                       NOT COMM-PP20-Z3-NON-SAISIE (IT)                         
                       MOVE COMM-PP20-PMONTANTZ3 (IT)  TO                       
                            COMM-PP20-PRESULTAT  (IT)                           
                    ELSE                                                        
                       SET  COMM-PP20-RES-NON-SAISIE (IT)  TO TRUE              
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
       FIN-CALCUL-OPERATIONS-LIGNE. EXIT.                                       
      *                                                                         
       OPERATION                      SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** REGLES DE GESTION                                                    
      *                                                                         
      **** OPERATION:                                                           
      **** 1- SI OPERANDE1 ET OPERANDE2 = ZONE SAISIE                           
      ****    => OPERANDE1 OPERATEUR OPERANDE2 = RESULTAT DECIMAL               
      **** 2- SI OPERANDE1 = ZONE SAISIE (S) ET                                 
      ****       OPERANDE2 = ZONE NON SAISIE (R=RIEN)                           
      ****    => S+R=S   S-R=S   S*R=R   S/R=S                                  
      **** 3- SI OPERANDE1 = ZONE NON SAISIE (R=RIEN)                           
      ****       OPERANDE2 = ZONE SAISIE (S) ET                                 
      ****    => R+S=S   R-S=-S  R*S=R   R/S=R                                  
      *                                                                         
           IF NOT WPOPERANDE1-NON-SAISIE AND NOT WPOPERANDE2-NON-SAISIE         
              EVALUATE WCOPERATEUR                                              
              WHEN '+'                                                          
                 ADD      WPOPERANDE2    TO WPOPERANDE1                         
                                     GIVING WPRESULTAT-D ROUNDED                
              WHEN '-'                                                          
                 SUBTRACT WPOPERANDE2  FROM WPOPERANDE1                         
                                     GIVING WPRESULTAT-D ROUNDED                
              WHEN '*'                                                          
                 MULTIPLY WPOPERANDE1    BY WPOPERANDE2                         
                                     GIVING WPRESULTAT-D ROUNDED                
              WHEN '/'                                                          
                 IF WPOPERANDE2 = ZEROES                                        
                 THEN INITIALIZE WPRESULTAT-D                                   
                 ELSE DIVIDE WPOPERANDE1    BY WPOPERANDE2                      
                                        GIVING WPRESULTAT-D ROUNDED             
                 END-IF                                                         
              WHEN OTHER CONTINUE                                               
              END-EVALUATE                                                      
           ELSE                                                                 
              IF WPOPERANDE1-NON-SAISIE AND WPOPERANDE2-NON-SAISIE              
                 SET  WPRESULTAT-D-NON-SAISIE  TO TRUE                          
              ELSE                                                              
                 IF WPOPERANDE2-NON-SAISIE                                      
                    IF WCOPERATEUR = '*'                                        
                    THEN SET  WPRESULTAT-D-NON-SAISIE  TO TRUE                  
                    ELSE MOVE WPOPERANDE1           TO WPRESULTAT-D             
                    END-IF                                                      
                 ELSE                                                           
                    IF WCOPERATEUR = '*' OR '/'                                 
                    THEN SET  WPRESULTAT-D-NON-SAISIE  TO TRUE                  
                    ELSE MOVE WPOPERANDE2           TO WPRESULTAT-D             
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
       FIN-OPERATION. EXIT.                                                     
      *                                                                         
       CALCUL-RESULTAT-LIGNE          SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           PERFORM OPERATION                                                    
      **** LE RESULTAT DE LA LIGNE DOIT ETRE EN ENTIER                          
           IF WPRESULTAT-D-NON-SAISIE                                           
              SET  COMM-PP20-RES-NON-SAISIE (IT)  TO TRUE                       
           ELSE                                                                 
              MULTIPLY WPRESULTAT-D BY 1 GIVING WPRESULTAT-E ROUNDED            
      ****    CONTROLE QUE LE RESULTAT DE LA LIGNE                              
      ****    NE DEPASSE PAS LES LIMITES PREVUES                                
              MOVE WPRESULTAT-E          TO COMM-PP20-PRESULTAT (IT)            
              IF (WPRESULTAT-E NOT <  100000000000 OR                           
                 WPRESULTAT-E NOT > -100000000000) AND                          
                 COMM-PP20-CODRET = ZERO                                        
      ****       SI PLUS DE 11 ENTIERS ALORS AFFICHER MESSAGE ERREUR            
                 MOVE +0100                 TO COMM-PP20-CODRET                 
                 MOVE 'LA VALEUR DE CETTE ZONE EST HORS LIMITE'                 
                                            TO COMM-PP20-LIBERR                 
                 MOVE IT                    TO COMM-PP20-NLGERR                 
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
       FIN-CALCUL-RESULTAT-LIGNE. EXIT.                                         
      *                                                                         
       CUMUL-TOTAL-TYPE-RUBRIQUE      SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           INITIALIZE WPRESULTAT-D                                              
      **** CUMUL DU TOTAL D'UN TYPE DE RUBRIQUE                                 
      **** CALCUL DES OPERATIONS D'UNE LIGNE                                    
           EVALUATE TRUE                                                        
           WHEN COMM-PP20-LIGNE-MOUVT  (IT)                                     
              MOVE 1                     TO IA                                  
              PERFORM CALCUL-OPERATION-TOTAL                                    
           WHEN COMM-PP20-LIGNE-INTER1 (IT)                                     
              MOVE IT                    TO IL                                  
           WHEN COMM-PP20-LIGNE-INTER2 (IT)                                     
      ****    TOTAL-INTER = RESULT-INTER1  CCALCUL-INTER1  RESULT-INTER2        
              MOVE 2                     TO IA                                  
              IF COMM-PP20-RES-NON-SAISIE (IL)                                  
              THEN SET  WPOPERANDE1-NON-SAISIE    TO TRUE                       
              ELSE MOVE COMM-PP20-PRESULTAT (IL)  TO WPOPERANDE1                
              END-IF                                                            
              MOVE COMM-PP20-CCALCUL   (IL)    TO WCOPERATEUR                   
              IF COMM-PP20-RES-NON-SAISIE (IT)                                  
              THEN SET  WPOPERANDE2-NON-SAISIE    TO TRUE                       
              ELSE MOVE COMM-PP20-PRESULTAT (IT)  TO WPOPERANDE2                
              END-IF                                                            
              PERFORM CALCUL-RESULTAT-TOTAL                                     
           WHEN COMM-PP20-LIGNE-FRANC  (IT)                                     
              MOVE 3                     TO IA                                  
              PERFORM CALCUL-OPERATION-TOTAL                                    
           WHEN OTHER CONTINUE                                                  
           END-EVALUATE.                                                        
      *                                                                         
       FIN-CUMUL-TOTAL-TYPE-RUBRIQUE. EXIT.                                     
      *                                                                         
       CALCUL-OPERATION-TOTAL         SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** TOTAL MVT OU FRANC = TOTAL CCALCUL(+,-,' ') RESULTAT                 
           MOVE COMM-PP20-PTOTAL    (IA)    TO WPOPERANDE1 WPRESULTAT-D         
           MOVE COMM-PP20-CCALCUL   (IT)    TO WCOPERATEUR                      
           IF COMM-PP20-RES-NON-SAISIE (IT)                                     
           THEN SET  WPOPERANDE2-NON-SAISIE    TO TRUE                          
           ELSE MOVE COMM-PP20-PRESULTAT (IT)  TO WPOPERANDE2                   
           END-IF                                                               
           PERFORM CALCUL-RESULTAT-TOTAL.                                       
      *                                                                         
       FIN-CALCUL-OPERATION-TOTAL. EXIT.                                        
      *                                                                         
       CALCUL-RESULTAT-TOTAL          SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           PERFORM OPERATION                                                    
      **** LE RESULTAT DE LA LIGNE DOIT ETRE EN ENTIER                          
           IF WPRESULTAT-D-NON-SAISIE                                           
              INITIALIZE WPRESULTAT-E                                           
           ELSE                                                                 
              MULTIPLY WPRESULTAT-D BY 1 GIVING WPRESULTAT-E ROUNDED            
      ****    CONTROLE QUE LE RESULTAT DES TYPES DE RUBRIQUES                   
      ****    NE DEPASSE PAS LES LIMITES PREVUES                                
              MOVE WPRESULTAT-E          TO COMM-PP20-PTOTAL (IA)               
              IF (WPRESULTAT-E NOT <  100000000000 OR                           
                  WPRESULTAT-E NOT > -100000000000) AND                         
                  COMM-PP20-CODRET = ZERO                                       
      ****       SI PLUS DE 11 ENTIERS ALORS AFFICHER MESSAGE ERREUR            
                 MOVE +0101                 TO COMM-PP20-CODRET                 
                 MOVE 'LA VALEUR DE CETTE ZONE EST HORS LIMITE'                 
                                            TO COMM-PP20-LIBERR                 
                 MOVE IT                    TO COMM-PP20-NLGERR                 
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
       FIN-CALCUL-RESULTAT-TOTAL. EXIT.                                         
      *                                                                         
       CALCUL-ENVELOPPE               SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** CALCUL DE L'ENVELOPPE (TOTAL DES FRANCS � REPARTIR) =                
      ****   TOTAL LIGNES EN FRANCS (3)  CCALCUL-INTER2 (+,-,' ')               
      ****   TOTAL LIGNES INTERMEDIAIRE (2)                                     
           MOVE COMM-PP00-NRUB2-FIN   TO IT                                     
           MOVE COMM-PP20-PTOTAL (3)  TO WPOPERANDE1                            
           MOVE COMM-PP20-CCALCUL(IT) TO WCOPERATEUR                            
           MOVE COMM-PP20-PTOTAL (2)  TO WPOPERANDE2                            
           PERFORM OPERATION                                                    
           MULTIPLY WPRESULTAT-D BY 1 GIVING WPRESULTAT-E ROUNDED               
           MOVE WPRESULTAT-E          TO COMM-PP20-PENVELOPPE-E                 
                                         COMM-PP20-PTOTAL (3)                   
           IF COMM-PP20-PENVELOPPE-E NOT < 100000 AND                           
              COMM-PP20-CODRET = ZERO                                           
      ****    SI PLUS DE 5 ENTIERS ALORS AFFICHER MESSAGE ERREUR                
              MOVE +0101                 TO COMM-PP20-CODRET                    
              MOVE 'LA VALEUR DE CETTE ZONE EST HORS LIMITE'                    
                                         TO COMM-PP20-LIBERR                    
              MOVE 3                     TO COMM-PP20-NLGERR                    
           END-IF.                                                              
           IF COMM-PP20-PENVELOPPE-E < ZERO AND COMM-PP20-CODRET = ZERO         
      ****    SI < 0 ALORS AFFICHER MESSAGE ERREUR                              
      ****    ET FORCAGE DE L'ENVELOPPE A ZERO                                  
              MOVE +0102                 TO COMM-PP20-CODRET                    
              MOVE 'LE MONTANT DE L ENVELOPPE EST NEGATIF'                      
                                         TO COMM-PP20-LIBERR                    
              MOVE 3                     TO COMM-PP20-NLGERR                    
              MOVE ZEROES                TO COMM-PP20-PENVELOPPE-E              
              IF COMM-PP20-PREMIER-PASSAGE                                      
                 MOVE ZEROES                TO COMM-PP20-PTOTAL (3)             
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
       FIN-CALCUL-ENVELOPPE. EXIT.                                              
      *                                                                         
       MODULE-SORTIE                  SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           GOBACK.                                                              
      *                                                                         
       FIN-MODULE-SORTIE. EXIT.                                                 
      *                                                                         
