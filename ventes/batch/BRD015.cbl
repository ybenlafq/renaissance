      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.    BRD015.                                                   
       AUTHOR.        DSA016.                                                   
       ENVIRONMENT    DIVISION.                                                 
       CONFIGURATION  SECTION.                                                  
      *****************************************************************         
      *        R E D E V A N C E  A U D I O V I S U E L  SEPT(89)     *         
      *****************************************************************         
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           C01           IS SAUT.                                               
       INPUT-OUTPUT   SECTION.                                                  
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRD007 ASSIGN TO FRD007.                                      
      *--                                                                       
           SELECT FRD007 ASSIGN TO FRD007                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE  ASSIGN TO FDATE.                                       
      *--                                                                       
           SELECT FDATE  ASSIGN TO FDATE                                        
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IRD015 ASSIGN TO IRD015 .                                     
      *--                                                                       
           SELECT IRD015 ASSIGN TO IRD015                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *----------------------------------------------------------------         
      *     DEFINITION DU FICHIER DATE                                          
      *-----------------------------------------------------------------        
       FD   FDATE                                                               
            RECORDING F                                                         
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD                                               
            DATA RECORD DATE-PARM.                                              
       01   DATE-PARM.                                                          
            05  JJ                      PIC XX.                                 
            05  MM                      PIC XX.                                 
            05  MM                      PIC XX.                                 
            05  AA                      PIC XX.                                 
            05  FILLER                  PIC X(72).                              
      *----------------------------------------------------------------         
      *     DEFINITION DU FICHIER EDITION DES ANOMALIES                         
      *-----------------------------------------------------------------        
       FD   FRD007                                                              
            RECORDING F                                                         
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD                                               
            DATA RECORD ENRE-FRD007.                                            
       01  ENRE-FRD007.                                                 01840000
           05  AN-DTRAIT                    PIC X(8).                           
           05  AN-NMAG                      PIC X(3).                           
           05  AN-NVENTE                    PIC X(7).                           
           05  AN-DDELIV                    PIC X(8).                           
           05  AN-FAM                       PIC X(5).                           
           05  AN-MARQ                      PIC X(5).                           
           05  AN-ADRESSE.                                                      
               10 AN-NOM                    PIC X(25).                          
               10 AN-PRENOM                 PIC X(15).                          
               10 AN-TELDOM                 PIC X(10).                          
               10 AN-TELBUR                 PIC X(10).                          
               10 AN-POSTE-BUR              PIC X(5).                           
               10 AN-BAT                    PIC X(3).                           
               10 AN-ESC                    PIC X(3).                           
               10 AN-ETA                    PIC X(3).                           
               10 AN-PTE                    PIC X(3).                           
               10 AN-NVOIE                  PIC X(5).                           
               10 AN-TVOIE                  PIC X(4).                           
               10 AN-NOMVOIE                PIC X(21).                          
               10 AN-COMMUNE                PIC X(32).                          
               10 AN-CPOSTAL                PIC X(5).                           
               10 AN-BURDIS                 PIC X(26).                          
           05  AN-MOTIF-REJET               PIC X(36).                          
           05  AN-MOTIF-REJET-2             PIC X(36).                          
      *----------------------------------------------------------------         
      *     DEFINITION DU FICHIER  D IMPRESSION                                 
      *-----------------------------------------------------------------        
       FD   IRD015                                                              
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD.                                              
       01   LIGNE1.                                                             
      *     05  W-SAUT          PIC X(01).                                      
            05  W-LIGNE-01      PIC X(132).                                     
      *                                                                         
      *****************************************************************         
      *     W O R K I N G  S T O R A G E                                        
      *****************************************************************         
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
      *----------------------------------------------------------------         
      *     DEFINITION DES ZONES SPECIFIQUES AUX PROGRAMMES                     
      *-----------------------------------------------------------------        
       01   CTR-LIGNES          PIC 9(05)  VALUE 0.                             
       01   CTR-PAGE            PIC 9(05)  VALUE 0.                             
      *-----------------------------------------------------------------        
       01   W-CPT-LU            PIC 9(05)  VALUE 0.                             
       01   W-CPT-EDITE         PIC 9(05)  VALUE 0.                             
       01   Z-CPT-LU            PIC ZZZZ9.                                      
       01   Z-CPT-EDITE         PIC ZZZZ9.                                      
      *----------------------------------------------------------------         
       01   CODE-FICHIER        PIC X                VALUE ' '.                 
            88  FIN-FICHIER                          VALUE '1'.                 
            88  NON-FIN-FICHIER                      VALUE '2'.                 
      *----------------------------------------------------------------         
       01   DAT-JMAS.                                                           
                10  D1-JJ      PIC XX.                                          
                10  D1-MM      PIC XX.                                          
                10  D1-SS      PIC XX .                                         
                10  D1-AA      PIC XX.                                          
       01   DAT-JMA.                                                            
                10  D2-JJ      PIC XX.                                          
                10  FILLER     PIC X  VALUE '/'.                                
                10  D2-MM      PIC XX.                                          
                10  FILLER     PIC X  VALUE '/'.                                
                10  D2-AA      PIC XX.                                          
      *----------------------------------------------------------------         
      *     LIGNE EDITION                                                       
      *----------------------------------------------------------------         
       01  IRD015-DSECT.                                                        
      *****************************************************************         
          02 LIGNE-01.                                                          
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     E T A B L I S S'.                   
            04 FILLER PIC X(20) VALUE ' E M E N T S  D A R '.                   
            04 FILLER PIC X(20) VALUE 'T Y                 '.                   
            04 FILLER PIC X(14) VALUE SPACES.                                   
            04 FILLER PIC X(06) VALUE 'IRD015'.                                 
            04 FILLER PIC X(11) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE-02.                                                          
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     ---------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '------              '.                   
            04 FILLER PIC X(14) VALUE SPACES.                                   
            04 FILLER PIC X(07) VALUE 'PAGE : '.                                
            04 ED-PAGE PIC ZZ9.                                           .     
            04 FILLER PIC X(07) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE-03.                                                          
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     !   EDITION DES'.                   
            04 FILLER PIC X(20) VALUE ' REJETS DU TRAITEMEN'.                   
            04 FILLER PIC X(20) VALUE 'T    !              '.                   
            04 FILLER PIC X(14) VALUE SPACES.                                   
            04 FILLER PIC X(09) VALUE 'EDITE LE '.                              
            04 ED-JOUR  PIC X(8).                                               
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE-04.                                                          
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     !DU FICHIER DES'.                   
            04 FILLER PIC X(20) VALUE ' DECLARATION DE REDE'.                   
            04 FILLER PIC X(20) VALUE 'VANCE!              '.                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(11) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE-05.                                                          
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     !        TRAITE'.                   
            04 FILLER PIC X(7) VALUE 'MENT DU'.                                 
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-DTRAIT    PIC X(8).                                           
            04 FILLER PIC X(17) VALUE '         !       '.                      
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(18) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE-06.                                                          
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     ---------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '------              '.                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(11) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE-07.                                                          
            04 FILLER PIC X(30) VALUE '------------------------------'.         
            04 FILLER PIC X(30) VALUE '------------------------------'.         
            04 FILLER PIC X(30) VALUE '------------------------------'.         
            04 FILLER PIC X(30) VALUE '------------------------------'.         
            04 FILLER PIC X(11) VALUE '-----------'.                            
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE-08.                                                          
            04 FILLER PIC X(03) VALUE '!  '.                                    
            04 FILLER PIC X(15) VALUE ' NMAG/NVENTE   '.                        
            04 FILLER PIC X(08) VALUE 'DATE    '.                               
            04 FILLER PIC X(11) VALUE 'FAM.       '.                            
            04 FILLER PIC X(20) VALUE 'NOM / PRENOM        '.                   
            04 FILLER PIC X(19) VALUE '            ADRESSE'.                    
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(22) VALUE '     MOTIF(S) DE REJET'.                 
            04 FILLER PIC X(12) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE '!'.                                      
      *****************************************************************         
          02 LIGNE-09.                                                          
            04 FILLER PIC X(03) VALUE '!  '.                                    
            04 FILLER PIC X(14) VALUE SPACES.                                   
            04 FILLER PIC X(08) VALUE 'DELIV.  '.                               
            04 FILLER PIC X(10) VALUE 'MQUE.     '.                             
            04 FILLER PIC X(17) VALUE 'TEL DOM ET BUREAU'.                      
            04 FILLER PIC X(78) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE '!'.                                      
      *****************************************************************         
          02 LIGNE-10.                                                          
            04 FILLER PIC X(30) VALUE '------------------------------'.         
            04 FILLER PIC X(30) VALUE '------------------------------'.         
            04 FILLER PIC X(30) VALUE '------------------------------'.         
            04 FILLER PIC X(30) VALUE '------------------------------'.         
            04 FILLER PIC X(11) VALUE '-----------'.                            
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE-11.                                                          
            04 FILLER PIC X(04) VALUE '!   '.                                   
            04 ED-NMAG    PIC X(3).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-NVENTE  PIC X(7).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-DDELIV  PIC X(8).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-FAM    PIC X(5).                                              
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-NOM    PIC X(25).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(04) VALUE 'BAT:'.                                   
            04 ED-BAT     PIC X(03).                                            
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(04) VALUE 'ESC:'.                                   
            04 ED-ESC      PIC X(03).                                           
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(04) VALUE 'ETA:'.                                   
            04 ED-ETA      PIC X(03).                                           
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(04) VALUE 'PTE:'.                                   
            04 ED-PTE      PIC X(3).                                            
            04 FILLER PIC X(02) VALUE SPACES.                                   
            04 ED-MOTIF-REJET PIC X(36).                                        
            04 FILLER PIC X(05) VALUE '    !'.                                  
      *****************************************************************         
          02 LIGNE-12.                                                          
            04 FILLER PIC X(03) VALUE '!  '.                                    
            04 FILLER PIC X(22) VALUE SPACES.                                   
            04 ED-MARQ    PIC X(5).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-PRENOM  PIC X(15).                                            
            04 FILLER PIC X(11) VALUE SPACES.                                   
            04 ED-NVOIE  PIC X(5).                                              
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-TVOIE  PIC X(4).                                              
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-NOMVOIE PIC X(21).                                            
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-MOTIF-REJET-2 PIC X(36).                                      
            04 FILLER PIC X(05) VALUE '    !'.                                  
      *****************************************************************         
          02 LIGNE-13.                                                          
            04 FILLER PIC X(03) VALUE '!  '.                                    
            04 FILLER PIC X(28) VALUE SPACES.                                   
            04 ED-TELDOM  PIC X(10).                                            
            04 FILLER PIC X(16) VALUE SPACES.                                   
            04 ED-CPOSTAL PIC X(05).                                            
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-BURDIS  PIC X(26).                                            
            04 FILLER PIC X(37) VALUE SPACES.                                   
            04 FILLER PIC X(05) VALUE '    !'.                                  
      *****************************************************************         
          02 LIGNE-14.                                                          
            04 FILLER PIC X(03) VALUE '!  '.                                    
            04 FILLER PIC X(28) VALUE SPACES.                                   
            04 ED-TELBUR  PIC X(10).                                            
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-POSTE-BUR PIC X(05).                                          
            04 FILLER PIC X(10) VALUE SPACES.                                   
            04 ED-COMMUNE   PIC X(32).                                          
            04 FILLER PIC X(37) VALUE SPACES.                                   
            04 FILLER PIC X(05) VALUE '    !'.                                  
      *                                                                 02990000
      *                                                                 03000000
      **************************************************                03015000
      *  DESCRIPTION DES ZONES D'APPEL AU MODULE ABEND *                03020000
      **************************************************                03030000
           COPY ABENDCOP.                                               03040000
                                                                        03050000
      *****************************************************************         
      *     P R O C E D U R E  D I V I S I O N                                  
      *****************************************************************         
      *-----------------------------------------------------------------        
      *     TRAME DU PROGRAMME BRD015                                           
      *-----------------------------------------------------------------        
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
       MODULE-BRD015 SECTION.                                                   
      *                                                                         
           PERFORM DEBUT-BRD015.                                                
           PERFORM TRAITEMENT-BRD015.                                           
           PERFORM FIN-BRD015.                                                  
      *                                                                         
       F-MODULE-BRD015 .EXIT.                                                   
      *                                                                         
      *-----------------------------------------------------------------        
      *           DEBUT DU BRD015                                               
      *-----------------------------------------------------------------        
      *                                                                         
       DEBUT-BRD015 SECTION.                                                    
      *                                                                         
           OPEN INPUT  FRD007.                                                  
           OPEN INPUT  FDATE.                                                   
           OPEN OUTPUT IRD015.                                                  
      *                                                                         
           MOVE 0 TO W-CPT-LU.                                                  
           MOVE 0 TO W-CPT-EDITE.                                               
      *                                                                         
           READ FDATE AT END                                                    
                MOVE 'DATE PARAMETRE ABSENTE '      TO ABEND-MESS               
                PERFORM ABEND-PROGRAMME.                                        
           MOVE  DATE-PARM       TO DAT-JMAS.                                   
           MOVE  D1-JJ           TO D2-JJ.                                      
           MOVE  D1-MM           TO D2-MM.                                      
           MOVE  D1-AA           TO D2-AA.                                      
           MOVE DAT-JMA          TO ED-JOUR.                                    
      *                                                                         
       F-DEBUT-BRD015. EXIT.                                                    
      *                                                                         
      *-----------------------------------------------------------------        
      *           TRAITEMENT DU BRD015                                          
      *-----------------------------------------------------------------        
      *                                                                         
       TRAITEMENT-BRD015 SECTION.                                               
      *                                                                         
           SET NON-FIN-FICHIER TO TRUE.                                         
           PERFORM LECTURE-EDANO.                                               
           PERFORM EDITION-ENT.                                                 
           PERFORM UNTIL   FIN-FICHIER                                          
                   ADD 1   TO W-CPT-LU                                          
      *            DISPLAY 'T2. CPT-LU = ' W-CPT-LU                             
                   PERFORM EDITION-MVT                                          
                   PERFORM LECTURE-EDANO                                        
           END-PERFORM.                                                         
      *                                                                         
       F-TRAITEMENT-BRD015. EXIT.                                               
      *                                                                         
      *-----------------------------------------------------------------        
      *           LECTURE DU EDANO                                              
      *-----------------------------------------------------------------        
      *                                                                         
       LECTURE-EDANO  SECTION.                                                  
           READ FRD007 AT END SET FIN-FICHIER TO TRUE.                          
       F-LECTURE-EDANO. EXIT.                                                   
      *                                                                         
      *-----------------------------------------------------------------        
      *           EDITION DES MOUVEMENTS                                        
      *-----------------------------------------------------------------        
       EDITION-MVT   SECTION.                                                   
      *                                                                         
           IF CTR-LIGNES > 55                                                   
      *    DISPLAY 'T1. CONTROLE LIGNE = ' CTR-LIGNES                           
              MOVE 0       TO   CTR-LIGNES                                      
              PERFORM EDITION-ENT                                               
           END-IF.                                                              
           PERFORM EDITION-DETAIL.                                              
       F-EDITION-MVT. EXIT.                                                     
      *                                                                         
      *                                                                         
      *-----------------------------------------------------------------        
      *           EDITION DE L ENTETE DU TABLEAU                                
      *-----------------------------------------------------------------        
      *                                                                         
       EDITION-ENT SECTION.                                                     
      *                                                                         
           ADD +1               TO CTR-PAGE.                                    
           MOVE CTR-PAGE        TO ED-PAGE.                                     
           MOVE AN-DTRAIT       TO ED-DTRAIT.                                   
      *                                                                         
           WRITE LIGNE1         FROM LIGNE-01 AFTER ADVANCING SAUT.             
           WRITE LIGNE1         FROM LIGNE-02.                                  
           WRITE LIGNE1         FROM LIGNE-03.                                  
           WRITE LIGNE1         FROM LIGNE-04.                                  
           WRITE LIGNE1         FROM LIGNE-05.                                  
           WRITE LIGNE1         FROM LIGNE-06.                                  
           WRITE LIGNE1         FROM LIGNE-07.                                  
           WRITE LIGNE1         FROM LIGNE-08.                                  
           WRITE LIGNE1         FROM LIGNE-09.                                  
           WRITE LIGNE1         FROM LIGNE-10.                                  
           WRITE LIGNE1         FROM LIGNE-10.                                  
      *                                                                         
           ADD +11               TO CTR-LIGNES.                                 
      *                                                                         
       F-EDITION-ENT. EXIT.                                                     
      *                                                                         
      *-----------------------------------------------------------------        
      *           EDITION D UNE LIGNE DE DETAIL                                 
      *-----------------------------------------------------------------        
      *                                                                         
       EDITION-DETAIL SECTION.                                                  
      *                                                                         
           MOVE AN-NMAG         TO  ED-NMAG.                                    
           MOVE AN-NVENTE       TO  ED-NVENTE.                                  
           MOVE AN-DDELIV       TO  ED-DDELIV                                   
           MOVE AN-FAM          TO  ED-FAM.                                     
           MOVE AN-MARQ         TO  ED-MARQ.                                    
           MOVE AN-NOM          TO  ED-NOM.                                     
           MOVE AN-PRENOM       TO  ED-PRENOM.                                  
           MOVE AN-TELDOM       TO  ED-TELDOM.                                  
           MOVE AN-TELBUR       TO  ED-TELBUR.                                  
           MOVE AN-POSTE-BUR    TO  ED-POSTE-BUR.                               
           MOVE AN-BAT          TO  ED-BAT.                                     
           MOVE AN-ESC          TO  ED-ESC.                                     
           MOVE AN-ETA          TO  ED-ETA.                                     
           MOVE AN-PTE          TO  ED-PTE.                                     
           MOVE AN-NVOIE        TO  ED-NVOIE.                                   
           MOVE AN-TVOIE        TO  ED-TVOIE.                                   
           MOVE AN-NOMVOIE      TO  ED-NOMVOIE.                                 
           MOVE AN-COMMUNE      TO  ED-COMMUNE.                                 
           MOVE AN-CPOSTAL      TO  ED-CPOSTAL.                                 
           MOVE AN-BURDIS       TO  ED-BURDIS.                                  
           MOVE AN-MOTIF-REJET  TO  ED-MOTIF-REJET.                             
           MOVE AN-MOTIF-REJET-2  TO  ED-MOTIF-REJET-2.                         
      *                                                                         
           WRITE LIGNE1         FROM LIGNE-11.                                  
           WRITE LIGNE1         FROM LIGNE-12.                                  
           WRITE LIGNE1         FROM LIGNE-13.                                  
           WRITE LIGNE1         FROM LIGNE-14.                                  
           WRITE LIGNE1         FROM LIGNE-10.                                  
      *                                                                         
           ADD +5               TO CTR-LIGNES.                                  
           ADD +1               TO W-CPT-EDITE.                                 
      *                                                                         
       F-EDITION-DETAIL. EXIT.                                                  
      *                                                                         
      *-----------------------------------------------------------------        
      *           FIN DU BRD015                                                 
      *-----------------------------------------------------------------        
      *                                                                         
       FIN-BRD015 SECTION.                                                      
      *                                                                         
           PERFORM FERMETURE-FICHIER.                                           
           PERFORM FIN-PROGRAMME.                                               
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       F-FIN-BRD015. EXIT.                                                      
      *                                                                         
      *-----------------------------------------------------------------        
      *           FERMETURE DES FICHIERS                                        
      *-----------------------------------------------------------------        
      *                                                                         
       FERMETURE-FICHIER    SECTION.                                            
           CLOSE IRD015 .                                                       
           CLOSE FRD007.                                                        
           CLOSE FDATE.                                                         
       FIN-FERMETURE-FICHIER. EXIT.                                             
      *                                                                         
       FIN-PROGRAMME        SECTION.                                            
           MOVE W-CPT-LU    TO   Z-CPT-LU.                                      
           MOVE W-CPT-EDITE TO   Z-CPT-EDITE.                                   
           DISPLAY ' '.                                                         
           DISPLAY '***************************************'.                   
           DISPLAY '* PROGRAMME BRD015     : COMPTE RENDU * '.                  
           DISPLAY '***************************************'.                   
           DISPLAY '* ENREGISTREMENTS LUS ...: ' Z-CPT-LU '      *'             
           DISPLAY '* ENREGISTREMENTS EDITES : ' Z-CPT-EDITE '      *'          
           DISPLAY '***************************************'.                   
       F-FIN-PROGRAMME. EXIT.                                                   
      *                                                                         
       ABEND-PROGRAMME       SECTION.                                   14420000
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                14430000
       FIN-ABEND-PROGRAMME.       EXIT.                                 14420000
      *                                                                 14400000
                                                                        14410000
