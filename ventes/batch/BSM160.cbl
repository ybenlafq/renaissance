      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. BSM160.                                                      
      *****************************************************************         
      * 25/01/2005 : PROGRAMME RELATIFS AUX ETATS ISM160 ISM165                 
      *              ON UTILISE 2 FICHIERS FSM160 FSM165 CONSTRUITS             
      *              PAR LE BSM144                                              
      *              ON EN RESSORT DEUX FICHIERS IDENTIQUES QU ON               
      *              INCREMENTE DES DONNEES D 'ACIVATION MESSAGE ALERTE         
      *              POUR L ETAT ISM160 (UNIQUEMENT EXPO M )                    
      *              ET DE CALCUL DE COUVERTURE DE STOCK POUR L'ETAT            
      *              ISM165                                                     
      *****************************************************************         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FSM165   ASSIGN TO FSM165.                                   
      *--                                                                       
            SELECT FSM165   ASSIGN TO FSM165                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FSM160   ASSIGN TO FSM160.                                   
      *--                                                                       
            SELECT FSM160   ASSIGN TO FSM160                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT ISM160   ASSIGN TO ISM160.                                   
      *--                                                                       
            SELECT ISM160   ASSIGN TO ISM160                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT ISM165   ASSIGN TO ISM165.                                   
      *--                                                                       
            SELECT ISM165   ASSIGN TO ISM165                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FSM165 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  RECORD-FSM165 PIC X(512).                                            
       FD  FSM160 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  RECORD-FSM160 PIC X(512).                                            
       FD  ISM160 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  RECORD-ISM160 PIC X(512).                                            
       FD  ISM165 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  RECORD-ISM165 PIC X(512).                                            
       WORKING-STORAGE SECTION.                                                 
      **************************************************************            
      *   ZONE D'APPEL DES MODULES GERANT LES SQLCODES                          
      **************************************************************            
      *                                                                         
           COPY ABENDCOP.                                                       
           COPY WORKDATC.                                                       
           COPY SYKWERRO.                                                       
      *                                                                         
      ***************************************************************           
      * ZONE DE TRAVAIL                                                         
      ***************************************************************           
      *                                                                         
           COPY ISM160.                                                         
           COPY ISM165.                                                         
       01  W-TAUX                  PIC S9(3)V999 COMP-3.                        
       01  W-C                     PIC  9(4)     COMP-3.                        
       01  W-TAMP9                 PIC  9(4).                                   
       01  W-TAMPZ                 PIC  ZZZ9.                                   
       01 TAMPON1-VENTES.                                                       
          05 W-VENTES              PIC S9(5) COMP-3 OCCURS 19.                  
       01 TAMPON2-VENTES REDEFINES TAMPON1-VENTES .                             
          05 W-TAMPON1             PIC X(30).                                   
          05 W-TAMPON2             PIC X(27).                                   
       01  COMPTEURS.                                                   00001010
           03  CTR-FSM165             PIC  S9(07) COMP-3 VALUE +0.      00001040
           03  CTR-FSM160             PIC  S9(07) COMP-3 VALUE +0.      00001040
           03  CTR-ISM160             PIC  S9(07) COMP-3 VALUE +0.      00001040
           03  CTR-ISM165             PIC  S9(07) COMP-3 VALUE +0.      00001040
      *                                                                         
       01  W-FLAGS.                                                             
           05 FICHIER-FLAGS       PIC X VALUE 'O'.                              
              88 PAS-FIN-FIC            VALUE 'O'.                              
              88 FIN-FIC                VALUE 'N'.                              
      *===============================================================*         
       PROCEDURE DIVISION.                                                      
      *===============================================================*         
      *                                                                         
      *****************************************************************         
       MODULE-BSM160 SECTION.                                                   
      *****************************************************************         
      *                                                                         
           PERFORM DEBUT.                                                       
           SET  PAS-FIN-FIC TO TRUE                                             
           PERFORM LECTURE-FSM160.                                              
           PERFORM TRAITEMENT-ISM160 UNTIL FIN-FIC.                             
           SET  PAS-FIN-FIC TO TRUE                                             
           PERFORM LECTURE-FSM165.                                              
           PERFORM TRAITEMENT-ISM165 UNTIL FIN-FIC.                             
           PERFORM FIN.                                                         
      *                                                                         
       FIN-MODULE-BSM160. EXIT.                                                 
      *                                                                         
      *****************************************************************         
       DEBUT SECTION.                                                           
      *****************************************************************         
      *                                                                         
           OPEN INPUT FSM160 FSM165.                                            
           OPEN OUTPUT ISM160 ISM165.                                           
      *                                                                         
       FIN-DEBUT. EXIT.                                                         
      *                                                                         
      *****************************************************************         
       TRAITEMENT-ISM160 SECTION.                                               
      *****************************************************************         
      *                                                                         
           IF ISM160-LIBELLE = 'EXPO M'                                         
              IF ISM160-TAUX NOT = 0                                            
                 MOVE SPACES  TO ISM160-COMMENTAIRE                             
                 COMPUTE W-TAUX = ( ISM160-NBREF * ISM160-TAUX ) / 100          
                 IF ISM160-MAG1 > SPACES AND ISM160-QNBR1 < W-TAUX              
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
                 IF ISM160-MAG2 > SPACES AND ISM160-QNBR2 < W-TAUX              
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
                 IF ISM160-MAG3 > SPACES AND ISM160-QNBR3 < W-TAUX              
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
                 IF ISM160-MAG4 > SPACES AND ISM160-QNBR4 < W-TAUX              
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
                 IF ISM160-MAG5 > SPACES AND ISM160-QNBR5 < W-TAUX              
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
                 IF ISM160-MAG6 > SPACES AND ISM160-QNBR6 < W-TAUX              
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
                 IF ISM160-MAG7 > SPACES AND ISM160-QNBR7 < W-TAUX              
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
                 IF ISM160-MAG8 > SPACES AND ISM160-QNBR8 < W-TAUX              
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
                 IF ISM160-MAG9 > SPACES AND ISM160-QNBR9 < W-TAUX              
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
                 IF ISM160-MAG10 > SPACES AND ISM160-QNBR10 < W-TAUX            
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
                 IF ISM160-MAG11 > SPACES AND ISM160-QNBR11 < W-TAUX            
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
                 IF ISM160-MAG12 > SPACES AND ISM160-QNBR12 < W-TAUX            
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
                 IF ISM160-MAG13 > SPACES AND ISM160-QNBR13 < W-TAUX            
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
                 IF ISM160-MAG14 > SPACES AND ISM160-QNBR14 < W-TAUX            
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
                 IF ISM160-MAG15 > SPACES AND ISM160-QNBR15 < W-TAUX            
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
                 IF ISM160-MAG16 > SPACES AND ISM160-QNBR16 < W-TAUX            
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
                 IF ISM160-MAG17 > SPACES AND ISM160-QNBR17 < W-TAUX            
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
                 IF ISM160-MAG18 > SPACES AND ISM160-QNBR18 < W-TAUX            
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
                 IF ISM160-MAG19 > SPACES AND ISM160-QNBR19 < W-TAUX            
                    MOVE 'FAIB' TO ISM160-COMMENTAIRE                           
                 END-IF                                                         
              ELSE                                                              
                 MOVE SPACES  TO ISM160-COMMENTAIRE                             
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES  TO ISM160-COMMENTAIRE                                
           END-IF                                                               
           PERFORM ECRITURE-ISM160.                                             
           PERFORM LECTURE-FSM160.                                              
      *                                                                         
       FIN-TRAITEMENT-ISM160. EXIT.                                             
      *                                                                         
      *****************************************************************         
       TRAITEMENT-ISM165 SECTION.                                               
      *****************************************************************         
      *                                                                         
           MOVE ISM165-TAMPON1 TO W-TAMPON1                                     
           MOVE ISM165-TAMPON2 TO W-TAMPON2                                     
           IF ISM165-MAG1 > SPACES                                              
              IF W-VENTES ( 1 ) > 0                                             
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK1 / W-VENTES ( 1 )          
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX1                                   
              ELSE                                                              
                 IF ISM165-QSTOCK1 NOT > 0                                      
                    MOVE '   0' TO ISM165-TAUX1                                 
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX1                                 
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX1                                    
           END-IF                                                               
           IF ISM165-MAG2 > SPACES                                              
              IF W-VENTES ( 2 ) > 0                                             
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK2 / W-VENTES ( 2 )          
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX2                                   
              ELSE                                                              
                 IF ISM165-QSTOCK2 NOT > 0                                      
                    MOVE '   0' TO ISM165-TAUX2                                 
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX2                                 
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX2                                    
           END-IF                                                               
           IF ISM165-MAG3 > SPACES                                              
              IF W-VENTES ( 3 ) > 0                                             
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK3 / W-VENTES ( 3 )          
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX3                                   
              ELSE                                                              
                 IF ISM165-QSTOCK3 NOT > 0                                      
                    MOVE '   0' TO ISM165-TAUX3                                 
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX3                                 
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX3                                    
           END-IF                                                               
           IF ISM165-MAG4 > SPACES                                              
              IF W-VENTES ( 4 ) > 0                                             
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK4 / W-VENTES ( 4 )          
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX4                                   
              ELSE                                                              
                 IF ISM165-QSTOCK4 NOT > 0                                      
                    MOVE '   0' TO ISM165-TAUX4                                 
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX4                                 
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX4                                    
           END-IF                                                               
           IF ISM165-MAG5 > SPACES                                              
              IF W-VENTES ( 5 ) > 0                                             
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK5 / W-VENTES ( 5 )          
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX5                                   
              ELSE                                                              
                 IF ISM165-QSTOCK5 NOT > 0                                      
                    MOVE '   0' TO ISM165-TAUX5                                 
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX5                                 
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX5                                    
           END-IF                                                               
           IF ISM165-MAG6 > SPACES                                              
              IF W-VENTES ( 6 ) > 0                                             
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK6 / W-VENTES ( 6 )          
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX6                                   
              ELSE                                                              
                 IF ISM165-QSTOCK6 NOT > 0                                      
                    MOVE '   0' TO ISM165-TAUX6                                 
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX6                                 
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX6                                    
           END-IF                                                               
           IF ISM165-MAG7 > SPACES                                              
              IF W-VENTES ( 7 ) > 0                                             
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK7 / W-VENTES ( 7 )          
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX7                                   
              ELSE                                                              
                 IF ISM165-QSTOCK7 NOT > 0                                      
                    MOVE '   0' TO ISM165-TAUX7                                 
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX7                                 
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX7                                    
           END-IF                                                               
           IF ISM165-MAG8 > SPACES                                              
              IF W-VENTES ( 8 ) > 0                                             
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK8 / W-VENTES ( 8 )          
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX8                                   
              ELSE                                                              
                 IF ISM165-QSTOCK8 NOT > 0                                      
                    MOVE '   0' TO ISM165-TAUX8                                 
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX8                                 
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX8                                    
           END-IF                                                               
           IF ISM165-MAG9 > SPACES                                              
              IF W-VENTES ( 9 ) > 0                                             
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK9 / W-VENTES ( 9 )          
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX9                                   
              ELSE                                                              
                 IF ISM165-QSTOCK9 NOT > 0                                      
                    MOVE '   0' TO ISM165-TAUX9                                 
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX9                                 
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX9                                    
           END-IF                                                               
           IF ISM165-MAG10 > SPACES                                             
              IF W-VENTES ( 10 ) > 0                                            
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK10 / W-VENTES ( 10 )        
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX10                                  
              ELSE                                                              
                 IF ISM165-QSTOCK10 NOT > 0                                     
                    MOVE '   0' TO ISM165-TAUX10                                
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX10                                
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX10                                   
           END-IF                                                               
           IF ISM165-MAG11 > SPACES                                             
              IF W-VENTES ( 11 ) > 0                                            
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK11 / W-VENTES ( 11 )        
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX11                                  
              ELSE                                                              
                 IF ISM165-QSTOCK11 NOT > 0                                     
                    MOVE '   0' TO ISM165-TAUX11                                
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX11                                
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX11                                   
           END-IF                                                               
           IF ISM165-MAG12 > SPACES                                             
              IF W-VENTES ( 12 ) > 0                                            
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK12 / W-VENTES ( 12 )        
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX12                                  
              ELSE                                                              
                 IF ISM165-QSTOCK12 NOT > 0                                     
                    MOVE '   0' TO ISM165-TAUX12                                
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX12                                
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX12                                   
           END-IF                                                               
           IF ISM165-MAG13 > SPACES                                             
              IF W-VENTES ( 13 ) > 0                                            
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK13 / W-VENTES ( 13 )        
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX13                                  
              ELSE                                                              
                 IF ISM165-QSTOCK13 NOT > 0                                     
                    MOVE '   0' TO ISM165-TAUX13                                
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX13                                
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX13                                   
           END-IF                                                               
           IF ISM165-MAG14 > SPACES                                             
              IF W-VENTES ( 14 ) > 0                                            
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK14 / W-VENTES ( 14 )        
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX14                                  
              ELSE                                                              
                 IF ISM165-QSTOCK14 NOT > 0                                     
                    MOVE '   0' TO ISM165-TAUX14                                
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX14                                
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX14                                   
           END-IF                                                               
           IF ISM165-MAG15 > SPACES                                             
              IF W-VENTES ( 15 ) > 0                                            
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK15 / W-VENTES ( 15 )        
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX15                                  
              ELSE                                                              
                 IF ISM165-QSTOCK15 NOT > 0                                     
                    MOVE '   0' TO ISM165-TAUX15                                
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX15                                
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX15                                   
           END-IF                                                               
           IF ISM165-MAG16 > SPACES                                             
              IF W-VENTES ( 16 ) > 0                                            
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK16 / W-VENTES ( 16 )        
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX16                                  
              ELSE                                                              
                 IF ISM165-QSTOCK16 NOT > 0                                     
                    MOVE '   0' TO ISM165-TAUX16                                
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX16                                
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX16                                   
           END-IF                                                               
           IF ISM165-MAG17 > SPACES                                             
              IF W-VENTES ( 17 ) > 0                                            
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK17 / W-VENTES ( 17 )        
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX17                                  
              ELSE                                                              
                 IF ISM165-QSTOCK17 NOT > 0                                     
                    MOVE '   0' TO ISM165-TAUX17                                
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX17                                
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX17                                   
           END-IF                                                               
           IF ISM165-MAG18 > SPACES                                             
              IF W-VENTES ( 18 ) > 0                                            
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK18 / W-VENTES ( 18 )        
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX18                                  
              ELSE                                                              
                 IF ISM165-QSTOCK18 NOT > 0                                     
                    MOVE '   0' TO ISM165-TAUX18                                
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX18                                
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX18                                   
           END-IF                                                               
           IF ISM165-MAG19 > SPACES                                             
              IF W-VENTES ( 19 ) > 0                                            
                 COMPUTE W-C ROUNDED = ISM165-QSTOCK19 / W-VENTES ( 19 )        
                 MOVE W-C     TO W-TAMP9                                        
                 MOVE W-TAMP9 TO W-TAMPZ                                        
                 MOVE W-TAMPZ TO ISM165-TAUX19                                  
              ELSE                                                              
                 IF ISM165-QSTOCK19 NOT > 0                                     
                    MOVE '   0' TO ISM165-TAUX19                                
                 ELSE                                                           
                    MOVE 'INF.' TO ISM165-TAUX19                                
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES    TO ISM165-TAUX19                                   
           END-IF                                                               
           PERFORM ECRITURE-ISM165.                                             
           PERFORM LECTURE-FSM165.                                              
      *                                                                         
       FIN-TRAITEMENT-ISM165. EXIT.                                             
      *                                                                         
      *****************************************************************         
       FIN SECTION.                                                             
      *****************************************************************         
      *                                                                         
           CLOSE FSM165 FSM160 ISM160 ISM165                                    
           DISPLAY '*************************************************'          
           DISPLAY '* LE PROGRAMME S''EST TERMINE NORMALEMENT       *'          
           DISPLAY '*************************************************'          
           DISPLAY '*  NOMBRE DE LECTURE FICHIER FSM165: '              00002980
                   CTR-FSM165 '  *'.                                    00002990
           DISPLAY '*  NOMBRE D ECRITURE FICHIER FSM160: '              00002980
                   CTR-FSM160 '  *'.                                    00002990
           DISPLAY '*  NOMBRE D ECRITURE FICHIER ISM160: '              00002980
                   CTR-ISM160 '  *'.                                    00002990
           DISPLAY '*  NOMBRE D ECRITURE FICHIER ISM165: '              00002980
                   CTR-ISM165 '  *'.                                    00002990
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-FIN. EXIT.                                                           
      *                                                                         
      *****************************************************************         
       LECTURE-FSM160 SECTION.                                                  
      *****************************************************************         
      *                                                                         
            READ FSM160 INTO DSECT-ISM160                                       
                 AT END SET FIN-FIC TO TRUE                                     
                 NOT AT END ADD 1 TO CTR-FSM160                                 
            END-READ.                                                           
      *                                                                         
       FIN-LECTURE-FSM160. EXIT.                                                
      *                                                                         
      *****************************************************************         
       LECTURE-FSM165 SECTION.                                                  
      *****************************************************************         
      *                                                                         
            READ FSM165 INTO DSECT-ISM165                                       
                 AT END SET FIN-FIC TO TRUE                                     
                 NOT AT END ADD 1 TO CTR-FSM165                                 
            END-READ.                                                           
      *                                                                         
       FIN-LECTURE-FSM165. EXIT.                                                
      *                                                                         
      *****************************************************************         
       ECRITURE-ISM160 SECTION.                                                 
      *****************************************************************         
      *                                                                         
           WRITE RECORD-ISM160       FROM DSECT-ISM160.                         
           ADD 1 TO CTR-ISM160.                                                 
      *                                                                         
       FIN-ECRITURE-ISM160. EXIT.                                               
      *                                                                         
      *****************************************************************         
       ECRITURE-ISM165 SECTION.                                                 
      *****************************************************************         
      *                                                                         
           WRITE RECORD-ISM165       FROM DSECT-ISM165.                         
           ADD 1 TO CTR-ISM165.                                                 
      *                                                                         
       FIN-ECRITURE-ISM165. EXIT.                                               
      *                                                                         
      *****************************************************************         
       PLANTAGE                     SECTION.                                    
      *****************************************************************         
      *                                                                         
           CLOSE  FSM165 FSM160 ISM160 ISM165                                   
           DISPLAY '*--------------------*'                                     
           DISPLAY '   P L A N T A G E    '                                     
           DISPLAY '*--------------------*'                                     
           MOVE 'BSM160'   TO ABEND-PROG.                                       
           CALL 'ABEND' USING ABEND-PROG ABEND-MESS.                            
      *                                                                         
       FIN-PLANTAGE. EXIT.                                                      
      *                                                                         
