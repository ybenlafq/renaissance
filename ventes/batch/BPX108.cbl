      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BPX108.                                                     
       AUTHOR.      DSA019.                                                     
      * ---------------------------------------------------------- *            
      *                                                            *            
      *    PROJET         : PAIES VENDEURS.                        *            
      *    PROGRAMME      : BPX108                                 *            
      *    PERIODICITE    : JOURNALIERE                            *            
      *    FONCTION       : EXTRACTION FICHIER HISTORIQUE          *            
      *    DATE DE CREAT. : 19/03/1992                             *            
      *                                                            *            
      * ---------------------------------------------------------- *            
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       INPUT-OUTPUT     SECTION.                                                
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FNSOC          ASSIGN  TO  FNSOC.                            
      *--                                                                       
           SELECT  FNSOC          ASSIGN  TO  FNSOC                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FMOIS          ASSIGN  TO  FMOIS.                            
      *--                                                                       
           SELECT  FMOIS          ASSIGN  TO  FMOIS                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FPX100H        ASSIGN  TO  FPX100H.                          
      *--                                                                       
           SELECT  FPX100H        ASSIGN  TO  FPX100H                           
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FPX108M        ASSIGN  TO  FPX108M.                          
      *--                                                                       
           SELECT  FPX108M        ASSIGN  TO  FPX108M                           
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FPX100H                                                              
           RECORDING F                                                          
           BLOCK CONTAINS 0 RECORDS                                             
           LABEL RECORD STANDARD.                                               
       01  ENR-FPX100H PIC X(250).                                              
       FD  FPX108M                                                              
           RECORDING F                                                          
           BLOCK CONTAINS 0 RECORDS                                             
           LABEL RECORD STANDARD.                                               
       01  ENR-FPX108M PIC X(250).                                              
       FD  FMOIS                                                                
           RECORDING F                                                          
           BLOCK CONTAINS 0 RECORDS                                             
           LABEL RECORD STANDARD.                                               
       01  FILE-MOIS.                                                           
           03 FILE-SSAAMM.                                                      
              05 FILE-MM              PIC  X(02).                               
              05 FILE-SS              PIC  X(02).                               
              05 FILE-AA              PIC  X(02).                               
           03 FILLER                  PIC  X(74).                               
      *                                                                         
       FD  FNSOC                                                                
           RECORDING F                                                          
           BLOCK CONTAINS 0 RECORDS                                             
           LABEL RECORD STANDARD.                                               
       01  ENR-FNSOC.                                                           
           03 FSOC        PIC X(03).                                            
           03 FILLER      PIC X(77).                                            
      * ---------------------------------------------------------- *            
      *                W O R K I N G  -  S T O R A G E             *            
      * ---------------------------------------------------------- *            
       WORKING-STORAGE SECTION.                                                 
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *77  ABEND                 PIC X(08) VALUE 'ABEND'.                       
      *--                                                                       
       77  MW-ABEND                 PIC X(08) VALUE 'ABEND'.                    
      *}                                                                        
       77  I                     PIC  S9(07) COMP-3 VALUE ZERO.                 
       77  W-NSOC                PIC  X(03)  VALUE SPACES.                      
       77  W-MOIS                PIC  X(06)  VALUE SPACES.                      
       77  CPT-FPX100H           PIC  S9(07) COMP-3 VALUE ZERO.                 
       77  CPT-FPX108M           PIC  S9(07) COMP-3 VALUE ZERO.                 
       77  CPT-REJET             PIC  S9(07) COMP-3 VALUE ZERO.                 
       01  CODE-FPX100H          PIC X(01) VALUE '0'.                           
           88  NON-FIN-FPX100H             VALUE '0'.                           
           88  FIN-FPX100H                 VALUE '1'.                           
      *----------------------------------------------------------- *            
           COPY SWPX100.                                                        
           COPY ABENDCOP.                                                       
      *----------------------------------------------------------- *            
      *          P R O C E D U R E   D I V I S I O N               *            
      *----------------------------------------------------------- *            
       PROCEDURE DIVISION.                                                      
           PERFORM DEBUT-BPX108.                                                
           PERFORM TRAIT-BPX108 UNTIL FIN-FPX100H.                              
           PERFORM FIN-BPX108.                                                  
      *----------------------------------------------------------- *            
      *                  D E B U T   B P X 1 0 8                   *            
      *----------------------------------------------------------- *            
       DEBUT-BPX108 SECTION.                                                    
           PERFORM OUVERTURE-FICHIER.                                           
           READ FMOIS AT END                                                    
                MOVE 'PAS DE MOIS EN ENTREE' TO ABEND-MESS                      
                PERFORM FIN-ANORMALE                                            
           END-READ.                                                            
           STRING FILE-SS FILE-AA FILE-MM                                       
                  DELIMITED SIZE INTO W-MOIS.                                   
           READ FNSOC AT END                                                    
                MOVE 'PAS DE SOCIETE EN ENTREE' TO ABEND-MESS                   
                PERFORM FIN-ANORMALE                                            
           END-READ.                                                            
           MOVE FSOC     TO  W-NSOC.                                            
           PERFORM LECTURE-FPX100H.                                             
       FIN-DEBUT-BPX108.  EXIT.                                                 
      * ---------------------------------------------------------- *            
       OUVERTURE-FICHIER SECTION.                                               
           OPEN INPUT  FMOIS FNSOC FPX100H                                      
                OUTPUT FPX108M.                                                 
       FIN-OUVERTURE-FICHIER. EXIT.                                             
      * ---------------------------------------------------------- *            
      *            T R A I T E M E N T   B P X 1 0 8               *            
      * ---------------------------------------------------------- *            
       TRAIT-BPX108 SECTION.                                                    
           IF   FPX100-DMVENTE    = W-MOIS                                      
              PERFORM ECRITURE-FPX108M                                          
           ELSE                                                                 
              ADD 1 TO CPT-REJET                                                
           END-IF.                                                              
           PERFORM LECTURE-FPX100H.                                             
       FIN-TRAIT-BPX108. EXIT.                                                  
      * ---------------------------------------------------------- *            
      *                      F I N    B P X 1 0 8                  *            
      * ---------------------------------------------------------- *            
       FIN-BPX108 SECTION.                                                      
           CLOSE FMOIS FNSOC FPX100H FPX108M.                                   
           PERFORM COMPTE-RENDU.                                                
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-FIN-BPX108. EXIT.                                                    
      *----------------------------------------------------------- *            
       LECTURE-FPX100H            SECTION.                                      
      *----------------------------------------------------------- *            
           READ FPX100H INTO FPX100-DSECT AT END                                
              SET FIN-FPX100H TO TRUE                                           
           END-READ.                                                            
           IF NON-FIN-FPX100H                                                   
              ADD 1 TO CPT-FPX100H                                              
           END-IF.                                                              
       FIN-LECTURE-FPX100H. EXIT.                                               
      *----------------------------------------------------------- *            
       ECRITURE-FPX108M           SECTION.                                      
      *----------------------------------------------------------- *            
           ADD 1 TO CPT-FPX108M.                                                
           MOVE  FPX100-DSECT TO  ENR-FPX108M.                                  
           WRITE ENR-FPX108M.                                                   
       FIN-ECRITURE-FPX108M. EXIT.                                              
      *----------------------------------------------------------- *            
       COMPTE-RENDU SECTION.                                                    
      *----------------------------------------------------------- *            
           DISPLAY '**'.                                                        
           DISPLAY '**       B P X 1 0 8 '.                                     
           DISPLAY '**'.                                                        
           DISPLAY '** NBRE DE LECTURES  FPX100H : ' CPT-FPX100H.               
           DISPLAY '** NBRE D''ECRITURES FPX108M : ' CPT-FPX108M.               
           DISPLAY '** NBRE DE REJETS            : ' CPT-REJET.                 
           DISPLAY '**'.                                                        
       FIN-COMPTE-RENDU.  EXIT.                                                 
      * ------------------------ FIN-ANORMALE ---------------------- *          
       FIN-ANORMALE SECTION.                                                    
           CLOSE FMOIS FNSOC FPX100H FPX108M.                                   
           MOVE 'BPX108' TO ABEND-PROG.                                         
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL  ABEND  USING ABEND-PROG ABEND-MESS.                            
      *--                                                                       
           CALL  MW-ABEND  USING ABEND-PROG ABEND-MESS.                         
      *}                                                                        
       FIN-FIN-ANORMALE. EXIT.                                                  
