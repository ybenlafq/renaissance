      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000020
       PROGRAM-ID.                BPV053.                               00000030
       AUTHOR. DSA019.                                                  00000040
      *===============================================================* 00000050
      *                        BPV053                                 * 00000060
      *  PROGRAMME D'EDITION DES STATS DE VENTE PAR RUBRIQUE PRODUIT  * 00000070
      *   DATE DE CREATION : 25/11/93.                                * 00000090
      *===============================================================* 00000100
       ENVIRONMENT DIVISION.                                            00000110
       CONFIGURATION SECTION.                                           00000120
       SPECIAL-NAMES.                                                   00000130
           DECIMAL-POINT IS COMMA.                                      00000140
       INPUT-OUTPUT SECTION.                                            00000150
       FILE-CONTROL.                                                    00000160
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FMOIS  ASSIGN TO  FMOIS.                             00000170
      *--                                                                       
            SELECT FMOIS  ASSIGN TO  FMOIS                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FNSOC  ASSIGN TO  FNSOC.                             00000170
      *--                                                                       
            SELECT FNSOC  ASSIGN TO  FNSOC                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FDATE  ASSIGN TO  FDATE.                             00000170
      *--                                                                       
            SELECT FDATE  ASSIGN TO  FDATE                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FPV050 ASSIGN TO  FPV050.                            00000180
      *--                                                                       
            SELECT FPV050 ASSIGN TO  FPV050                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT IPV052 ASSIGN TO  IPV052.                            00000180
      *--                                                                       
            SELECT IPV052 ASSIGN TO  IPV052                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                   00000190
       FILE SECTION.                                                    00000200
      *                                                                 00000210
       FD   FMOIS                                                       00000220
            RECORDING F                                                 00000230
            BLOCK 0 RECORDS                                             00000240
            LABEL RECORD STANDARD.                                      00000250
       01  PARAMETRE-MOIS.                                              00000260
           03  ENR-FMOIS.                                               00000270
               05 FMOIS-MM          PIC  X(02).                                 
               05 FMOIS-SS          PIC  X(02).                                 
               05 FMOIS-AA          PIC  X(02).                                 
           03  FILLER               PIC  X(74).                         00000320
      *                                                                 00000210
       FD   FNSOC                                                       00000220
            RECORDING F                                                 00000230
            BLOCK 0 RECORDS                                             00000240
            LABEL RECORD STANDARD.                                      00000250
       01  PARAMETRE-SOC.                                               00000260
           03  FNSOCIETE            PIC  X(03).                         00000270
           03  FILLER               PIC  X(77).                         00000320
      *                                                                 00000210
       FD   FDATE                                                       00000220
            RECORDING F                                                 00000230
            BLOCK 0 RECORDS                                             00000240
            LABEL RECORD STANDARD.                                      00000250
       01  PARAMETRE-FDATE.                                             00000260
           03  FDATE-JJ             PIC  X(02).                         00000270
           03  FDATE-MM             PIC  X(02).                         00000270
           03  FDATE-SS             PIC  X(02).                         00000270
           03  FDATE-AA             PIC  X(02).                         00000270
           03  FILLER               PIC  X(72).                         00000320
      *                                                                 00000330
       FD   FPV050                                                      00000340
            RECORDING F                                                 00000350
            BLOCK 0 RECORDS                                             00000360
            LABEL RECORD STANDARD.                                      00000370
      *-- LONGUEUR DE 180 C.                                                    
       COPY SWPV050.                                                    00000390
       FD   IPV052                                                      00000340
            RECORDING F                                                 00000350
            BLOCK 0 RECORDS                                             00000360
            LABEL RECORD STANDARD.                                      00000370
       01  IPV052-ENREG.                                                00000260
           03  IPV052-ASA           PIC  X(01).                         00000270
           03  IPV052-LIG           PIC  X(132).                        00000320
                                                                        00000390
      *                                                                 00000400
      *================================================================*00000410
       WORKING-STORAGE         SECTION.                                 00000420
      *================================================================*00000430
      *                                                                 00000440
       77  BETDATC            PIC X(08)  VALUE 'BETDATC'.               00000470
       77  W-FNSOC            PIC X(03)  VALUE SPACES.                  00000470
       77  W-NMAG             PIC X(03)  VALUE SPACES.                  00000470
       77  W-CVENDEUR         PIC X(06)  VALUE SPACES.                  00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-NB-FPV050        PIC S9(06) COMP VALUE ZERO.               00000470
      *--                                                                       
       77  W-NB-FPV050        PIC S9(06) COMP-5 VALUE ZERO.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  CPT-DETAIL         PIC S9(04) COMP VALUE ZERO.               00000470
      *--                                                                       
       77  CPT-DETAIL         PIC S9(04) COMP-5 VALUE ZERO.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  CPT-LIGNE          PIC S9(04) COMP VALUE ZERO.               00000470
      *--                                                                       
       77  CPT-LIGNE          PIC S9(04) COMP-5 VALUE ZERO.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-NLIGNE-MAX       PIC S9(04) COMP VALUE 60.                 00000470
      *--                                                                       
       77  W-NLIGNE-MAX       PIC S9(04) COMP-5 VALUE 60.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  CPT-PAGE           PIC S9(04) COMP VALUE ZERO.               00000470
      *--                                                                       
       77  CPT-PAGE           PIC S9(04) COMP-5 VALUE ZERO.                     
      *}                                                                        
       77  W-MTREM            PIC S9(09)V99  COMP-3 VALUE ZERO.         00000470
       77  W-QVENDUESV        PIC S9(09)V9   COMP-3 VALUE ZERO.         00000470
       77  W-PMTCA            PIC S9(09)     COMP-3 VALUE ZERO.         00000470
       77  W-MTCOMM           PIC S9(09)V9   COMP-3 VALUE ZERO.         00000470
       77  W-QNBPSE           PIC S9(07)V9   COMP-3 VALUE ZERO.         00000470
      *                                                                 00000830
      *                                                                 00000830
      *                                                                 00000830
      *-- INDICE 1 = EDITION "AUTRES"                                           
      *-- INDICE 2 = TOTAL DU MOIS                                              
      *-- INDICE 3 = CUMUL EXERCICE                                             
       01  W-FPV050-DSECT.                                                      
           03  W-FPV050-TAB-VENTES OCCURS 3.                                    
              05  W-FPV050-QVENDUE    PIC S9(07)    COMP-3.                     
              05  W-FPV050-QVENDUESV  PIC S9(07)    COMP-3.                     
              05  W-FPV050-QNBPSE     PIC S9(07)    COMP-3.                     
              05  W-FPV050-QNBPSAB    PIC S9(07)    COMP-3.                     
              05  W-FPV050-PMTCA      PIC S9(09)    COMP-3.                     
              05  W-FPV050-MTREM      PIC S9(09)V99 COMP-3.                     
              05  W-FPV050-MTCOMM     PIC S9(09)V99 COMP-3.                     
              05  W-FPV050-PMTCAPSE   PIC S9(09)V99 COMP-3.                     
                                                                        00000830
       01  W-MOIS.                                                              
           05  W-MOIS-MM      PIC X(02).                                        
           05  FILLER         PIC X(01)  VALUE '/'.                             
           05  W-MOIS-SS      PIC X(02).                                        
           05  W-MOIS-AA      PIC X(02).                                        
                                                                        00000830
       01  W-DATE-SSAAMMJJ.                                                     
           05  W-DATE-SS      PIC X(02).                                        
           05  W-DATE-AA      PIC X(02).                                        
           05  W-DATE-MM      PIC X(02).                                        
           03  W-DATE-JJ      PIC X(02).                                        
                                                                        00000830
       01  W-DATE-ED.                                                           
           05  W-DATE-JJ-ED   PIC X(02).                                        
           05  FILLER         PIC X(01) VALUE '/'.                              
           05  W-DATE-MM-ED   PIC X(02).                                        
           05  FILLER         PIC X(01) VALUE '/'.                              
           05  W-DATE-SS-ED   PIC X(02).                                        
           03  W-DATE-AA-ED   PIC X(02).                                        
                                                                        00000830
       01  CODE-FPV050 PIC X(01) VALUE '0'.                             00001010
           88 NON-FIN-FPV050     VALUE '0'.                             00001020
           88 FIN-FPV050         VALUE '1'.                             00001030
       01  CODE-AUTRE  PIC X(01) VALUE '0'.                             00001010
           88 NON-EDIT-AUTRE     VALUE '0'.                             00001020
           88 EDIT-AUTRE         VALUE '1'.                             00001030
      *---------------------------------------------------------------  00001040
      *  DSECT DE L'EDITION                                                     
      *---------------------------------------------------------------          
       01  IPV052-DSECT.                                                        
           05  E00.                                                             
               10  E00-ASA PIC X VALUE '1'.                                     
               10  FILLER                         PIC X(58) VALUE               
           'IPV052                               STATISTIQUES  DE  VEN'.        
               10  FILLER                         PIC X(47) VALUE               
           'TES  PAR  VENDEUR                     EDITE LE '.                   
               10  E00-DATEDITE                   PIC 99/99/9999.               
               10  FILLER                         PIC X(08).                    
               10  FILLER                         PIC X(06) VALUE               
           ' PAGE '.                                                            
               10  E00-NOPAGE                     PIC ZZZ.                      
           05  E01.                                                             
               10  E01-ASA PIC X VALUE '0'.                                     
               10  FILLER                         PIC X(13) VALUE               
           'MOIS       : '.                                                     
               10  E01-MOIS                       PIC X(07).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(56) VALUE               
           '                                                        '.          
           05  E02.                                                             
               10  E02-ASA PIC X VALUE ' '.                                     
               10  FILLER                         PIC X(13) VALUE               
           'MAGASIN    : '.                                                     
               10  E02-NMAG                       PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  E02-LMAG                       PIC X(20).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(37) VALUE               
           '                                     '.                             
           05  E03.                                                             
               10  E03-ASA PIC X VALUE '-'.                                     
               10  FILLER                         PIC X(58) VALUE               
           '.---------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(16) VALUE               
           '---------------.'.                                                  
           05  E04.                                                             
               10  E04-ASA PIC X VALUE ' '.                                     
               10  FILLER                         PIC X(12) VALUE               
           '! VENDEUR : '.                                                      
               10  E04-LVENDEUR                   PIC X(20).                    
               10  FILLER                         PIC X(07) VALUE               
           'CODE : '.                                                           
               10  E04-CVENDEUR                   PIC X(06).                    
               10  FILLER                         PIC X(05) VALUE               
           '     '.                                                             
               10  E04-SUITE                      PIC X(07).                    
               10  FILLER                         PIC X(36) VALUE               
           '                                    '.                              
               10  FILLER                         PIC X(06) VALUE               
           '      '.                                                            
               10  FILLER                         PIC X(33) VALUE               
           '                                !'.                                 
           05  E05.                                                             
               10  E05-ASA PIC X VALUE ' '.                                     
               10  FILLER                         PIC X(58) VALUE               
           '!---------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(16) VALUE               
           '---------------!'.                                                  
           05  E06.                                                             
               10  E06-ASA PIC X VALUE ' '.                                     
               10  FILLER                         PIC X(58) VALUE               
           '!                      !                C U M U L   D U   '.        
               10  FILLER                         PIC X(58) VALUE               
           'M O I S            !                  C U M U L   E X E R '.        
               10  FILLER                         PIC X(16) VALUE               
           'C I C E        !'.                                                  
           05  E07.                                                             
               10  E07-ASA PIC X VALUE ' '.                                     
               10  FILLER                         PIC X(58) VALUE               
           '!                      !----------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '-------------------!--------------------------------------'.        
               10  FILLER                         PIC X(16) VALUE               
           '---------------!'.                                                  
           05  E08.                                                             
               10  E08-ASA PIC X VALUE ' '.                                     
               10  FILLER                         PIC X(58) VALUE               
           '!                      !                   PRODUITS       '.        
               10  FILLER                         PIC X(58) VALUE               
           '      !    PSE     !               PRODUITS               '.        
               10  FILLER                         PIC X(16) VALUE               
           '  !     PSE    !'.                                                  
           05  E09.                                                             
               10  E09-ASA PIC X VALUE ' '.                                     
               10  FILLER                         PIC X(58) VALUE               
           '!----------------------!----------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '------!------------!--------------------------------------'.        
               10  FILLER                         PIC X(16) VALUE               
           '--!------------!'.                                                  
           05  E10.                                                             
               10  E10-ASA PIC X VALUE ' '.                                     
               10  FILLER                         PIC X(58) VALUE               
           '! RUBRIQUE PRODUIT     !     NB    C.A.   REM   PVM     % '.        
               10  FILLER                         PIC X(58) VALUE               
           '    % !   NB   %   !    NB     C.A.   REM   PVM     %    '.         
               10  FILLER                         PIC X(16) VALUE               
           '% !   NB     % !'.                                                  
           05  E11.                                                             
               10  E11-ASA PIC X VALUE ' '.                                     
               10  FILLER                         PIC X(58) VALUE               
           '!                      !  TOTAL   TOTAL               COM '.        
               10  FILLER                         PIC X(58) VALUE               
           ' O.A. !  PSE   PSE ! TOTAL    TOTAL               COM  O.A'.        
               10  FILLER                         PIC X(16) VALUE               
           '. !   PSE  PSE !'.                                                  
           05  D05.                                                             
               10  D05-ASA PIC X VALUE ' '.                                     
               10  FILLER                         PIC X(02) VALUE               
           '! '.                                                                
               10  D05-LAGREGATED                 PIC X(20).                    
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  D05-NB                         PIC -(6).                     
               10  D05-CA                         PIC -(9).                     
               10  D05-REM                        PIC --9,99                    
                                                  BLANK WHEN ZERO.              
               10  D05-PVM                        PIC ------.                   
               10  D05-COM-PCT                    PIC ---9,9                    
                                                  BLANK WHEN ZERO.              
               10  D05-OA-PCT                     PIC ---9,9                    
                                                  BLANK WHEN ZERO.              
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  D05-NB-PSE                     PIC -----.                    
               10  D05-PSE-PCT                    PIC ---9,9                    
                                                  BLANK WHEN ZERO.              
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  D05-NB-TOT                     PIC -(6).                     
               10  D05-CA-TOT                     PIC -(9).                     
               10  D05-REM-TOT                    PIC --9,99                    
                                                  BLANK WHEN ZERO.              
               10  D05-PVM-TOT                    PIC ------.                   
               10  D05-COM-TOT-PCT                PIC ---9,9                    
                                                  BLANK WHEN ZERO.              
               10  D05-OA-TOT-PCT                 PIC ---9,9                    
                                                  BLANK WHEN ZERO.              
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  D05-NB-PSE-TOT                 PIC -----.                    
               10  D05-PSE-TOT-PCT                PIC ---9,9                    
                                                  BLANK WHEN ZERO.              
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
           05  T02.                                                             
               10  T02-ASA PIC X VALUE ' '.                                     
               10  FILLER                         PIC X(24) VALUE               
           '!      TOTAL  VENDEUR  !'.                                          
               10  T02-NB                         PIC ------.                   
               10  T02-CA                         PIC ---------.                
               10  T02-REM                        PIC --9,99                    
                                                  BLANK WHEN ZERO.              
               10  T02-PVM                        PIC ------.                   
               10  T02-COM-PCT                    PIC ---9,9                    
                                                  BLANK WHEN ZERO.              
               10  T02-OA-PCT                     PIC ---9,9                    
                                                  BLANK WHEN ZERO.              
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  T02-NB-PSE                     PIC -----.                    
               10  T02-PSE-PCT                    PIC ---9,9                    
                                                  BLANK WHEN ZERO.              
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  T02-NB-TOT                     PIC ------.                   
               10  T02-CA-TOT                     PIC ---------.                
               10  T02-REM-TOT                    PIC --9,99                    
                                                  BLANK WHEN ZERO.              
               10  T02-PVM-TOT                    PIC ------.                   
               10  T02-COM-TOT-PCT                PIC ---9,9                    
                                                  BLANK WHEN ZERO.              
               10  T02-OA-TOT-PCT                 PIC ---9,9                    
                                                  BLANK WHEN ZERO.              
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  T02-NB-PSE-TOT                 PIC -----.                    
               10  T02-PSE-TOT-PCT                PIC ---9,9                    
                                                  BLANK WHEN ZERO.              
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
           05  T03.                                                             
               10  T03-ASA PIC X VALUE ' '.                                     
               10  FILLER                         PIC X(58) VALUE               
           '!---------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(16) VALUE               
           '---------------!'.                                                  
      *================================================================*00001950
      *       DESCRIPTION DES ZONES D'APPEL AU MODULE ABEND            *00001960
      *================================================================*00001970
                                                                        00001980
            COPY  ABENDCOP.                                             00001990
                                                                        00002120
                                                                        00002060
      *================================================================*00002070
      *       DESCRIPTION DES ZONES D'APPEL AU MODULE BETDATC          *00002080
      *================================================================*00002090
                                                                        00002100
            COPY  WORKDATC.                                             00002110
                                                                        00002850
      *================================================================*00002860
       PROCEDURE               DIVISION.                                00002870
      *================================================================*00002880
                                                                        00002890
      *===============================================================* 00002900
      *              T R A M E   DU   P R O G R A M M E               * 00002910
      *===============================================================* 00002920
      *                                                                 00002930
      *===============================================================* 00002940
      *                                                               * 00002950
      *           -----------------------------------------           * 00002960
      *           -           MODULE    BPV053            -           * 00002970
      *           -----------------------------------------           * 00002980
      *                               I                               * 00002990
      *           -----------------------------------------           * 00003000
      *           I                   I                   I           * 00003010
      *   -----------------   -----------------   -----------------   * 00003020
      *   -    ENTREE     -   -  TRAITEMENT   -   -     SORTIE    -   * 00003030
      *   -----------------   -----------------   -----------------   * 00003040
      *                                                               * 00003050
      *===============================================================* 00003060
      *                                                                 00003070
      *===============================================================* 00003080
       MODULE-BPV053           SECTION.                                 00003090
      *===============================================================* 00003100
      *                                                                 00003110
           PERFORM ENTREE.                                              00003120
           PERFORM TRAITEMENT UNTIL FIN-FPV050.                         00003130
           PERFORM SORTIE.                                              00003140
      *                                                                 00003150
       FIN-MODULE-BPV053.  EXIT.                                        00003160
        EJECT                                                           00003170
      *                                                                 00003180
      *===============================================================* 00003190
       ENTREE                     SECTION.                              00003200
      *===============================================================* 00003210
      *                                                                 00003220
           OPEN INPUT  FMOIS.                                           00003230
           OPEN INPUT  FDATE.                                           00003230
           OPEN INPUT  FNSOC.                                           00003230
           OPEN INPUT  FPV050.                                          00003240
           OPEN OUTPUT IPV052.                                          00003240
           READ FMOIS AT END                                            00003250
             MOVE 'ABSENCE DE CARTE PARAMETRE FMOIS' TO ABEND-MESS      00003260
             PERFORM PLANTAGE                                           00003270
           END-READ.                                                    00003280
           MOVE FMOIS-MM TO W-MOIS-MM.                                          
           MOVE FMOIS-SS TO W-MOIS-SS.                                          
           MOVE FMOIS-AA TO W-MOIS-AA.                                          
           READ FDATE AT END                                            00003250
             MOVE 'ABSENCE DE CARTE PARAMETRE FDATE' TO ABEND-MESS      00003260
             PERFORM PLANTAGE                                           00003270
           END-READ.                                                    00003280
           MOVE FDATE-JJ        TO W-DATE-JJ W-DATE-JJ-ED.                      
           MOVE FDATE-MM        TO W-DATE-MM W-DATE-MM-ED.                      
           MOVE FDATE-SS        TO W-DATE-SS W-DATE-SS-ED.                      
           MOVE FDATE-AA        TO W-DATE-AA W-DATE-AA-ED.                      
           INITIALIZE WORK-BETDATC.                                     00003540
           MOVE '5'             TO GFDATA.                              00003550
           MOVE W-DATE-SSAAMMJJ TO GFSAMJ-0.                            00003560
           PERFORM APPEL-BETDATC.                                       00003570
                                                                        00003590
           READ FNSOC AT END                                            00003250
             MOVE 'ABSENCE DE CARTE PARAMETRE' TO ABEND-MESS            00003260
             PERFORM PLANTAGE                                           00003270
           END-READ.                                                    00003280
           MOVE FNSOCIETE  TO W-FNSOC.                                          
           READ FPV050 AT END                                           00003250
             DISPLAY 'FICHIER FPV050 VIDE '                             00003260
             PERFORM FERMETURE-FICHIERS                                 00003270
             PERFORM FIN-PROGRAMME                                      00003270
           END-READ.                                                    00003280
           ADD 1 TO W-NB-FPV050.                                                
           INITIALIZE W-FPV050-DSECT.                                           
           INITIALIZE IPV052-ENREG.                                             
      *                                                                 00003340
       FIN-ENTREE. EXIT.                                                00003350
        EJECT                                                           00003360
      *                                                                 00003370
      *===============================================================* 00003900
       APPEL-BETDATC              SECTION.                              00003910
      *===============================================================* 00003920
      *                                                                 00003930
           CALL BETDATC USING WORK-BETDATC.                             00003940
           IF GFVDAT = ZERO                                             00003950
              MOVE GF-MESS-ERR TO ABEND-MESS                            00003960
              PERFORM PLANTAGE                                          00003970
           END-IF.                                                      00003980
      *                                                                 00003990
       FIN-APPEL-BETDATC. EXIT.                                         00004000
                    EJECT                                               00004010
      *                                                                 00005300
      *===============================================================* 00005310
       TRAITEMENT                 SECTION.                              00005320
      *===============================================================* 00005330
      *                                                                 00005340
      *-- SI 1ERE PAGE, OU CHGT MAGASIN OU PLUS DE PLACE SUR PAGE               
      *-- ALORS EDITION DE L'ENTETE                                             
           IF (CPT-LIGNE = 0 AND CPT-PAGE = 0)                          00005370
              OR (CPT-LIGNE > W-NLIGNE-MAX - 3                                  
                  AND W-CVENDEUR = FPV050-CVENDEUR)                             
              OR (CPT-LIGNE > W-NLIGNE-MAX - 14                                 
                  AND W-CVENDEUR NOT = FPV050-CVENDEUR)                         
              OR W-NMAG NOT = FPV050-NMAG                                       
              PERFORM TRAIT-ENTETE                                              
           END-IF.                                                              
      *                                                                 00005340
      *-- SI CHGT D'AGREGAT, SI NB-LIGNE <= NLIGNE-MAX - 14                     
      *-- ALORS ENTETE-REDUITE                                                  
           IF W-CVENDEUR NOT = FPV050-CVENDEUR                          00005370
              AND (CPT-LIGNE NOT > W-NLIGNE-MAX - 14)                           
              PERFORM ENTETE-REDUITE                                            
           END-IF.                                                              
      *-- SI VENDEUR A VENDU DANS LE MOIS ALORS EDITER LIGNE,                   
      *-- SINON CUMULER DANS 'AUTRES VENDEURS'.                                 
           INITIALIZE D05.                                                      
           IF FPV050-QVENDUE(1) NOT = 0                                         
              IF W-CVENDEUR NOT = FPV050-CVENDEUR                               
                 PERFORM ENTETE-REDUITE                                         
              END-IF                                                            
              MOVE FPV050-LAGREGATED TO D05-LAGREGATED                          
              MOVE FPV050-QVENDUE(1) TO D05-NB                                  
              MOVE FPV050-PMTCA(1)   TO D05-CA                                  
              COMPUTE W-MTREM = FPV050-MTREM(1) + FPV050-PMTCA(1)               
              IF W-MTREM NOT = 0                                                
                 COMPUTE W-MTREM ROUNDED =                                      
                                   (FPV050-MTREM(1) * 100) / W-MTREM            
                 MOVE W-MTREM           TO D05-REM                              
              END-IF                                                            
              IF FPV050-QVENDUE(1) NOT = 0                                      
                 COMPUTE W-PMTCA ROUNDED = FPV050-PMTCA(1)                      
                                         / FPV050-QVENDUE(1)                    
                 MOVE W-PMTCA            TO D05-PVM                             
              END-IF                                                            
              IF FPV050-PMTCA(1) NOT = 0                                        
                 COMPUTE W-MTCOMM ROUNDED = (FPV050-MTCOMM(1) * 10000)          
                                          / FPV050-PMTCA(1)                     
                 MOVE W-MTCOMM     TO D05-COM-PCT                               
              END-IF                                                            
              IF FPV050-QVENDUE(1) NOT = 0                                      
                 COMPUTE W-QVENDUESV ROUNDED = (FPV050-QVENDUESV(1)             
                                        * 100) / FPV050-QVENDUE(1)              
                 MOVE W-QVENDUESV   TO D05-OA-PCT                               
              END-IF                                                            
              MOVE FPV050-QNBPSE(1) TO D05-NB-PSE                               
              IF FPV050-QNBPSAB(1) NOT = 0                                      
                 COMPUTE W-QNBPSE ROUNDED  = (FPV050-QNBPSE(1) * 100)           
                                           / FPV050-QNBPSAB(1)                  
                 MOVE W-QNBPSE       TO D05-PSE-PCT                             
              END-IF                                                            
              MOVE FPV050-QVENDUE(2) TO D05-NB-TOT                              
              MOVE FPV050-PMTCA(2)   TO D05-CA-TOT                              
              COMPUTE W-MTREM = FPV050-MTREM(2) + FPV050-PMTCA(2)               
              IF W-MTREM NOT = 0                                                
                 COMPUTE W-MTREM ROUNDED =                                      
                                   (FPV050-MTREM(2) * 100) / W-MTREM            
                 MOVE W-MTREM           TO D05-REM-TOT                          
              END-IF                                                            
              IF FPV050-QVENDUE(2) NOT = 0                                      
                 COMPUTE W-PMTCA ROUNDED =                                      
                                 FPV050-PMTCA(2) / FPV050-QVENDUE(2)            
                 MOVE W-PMTCA           TO D05-PVM-TOT                          
              END-IF                                                            
              IF FPV050-PMTCA(2) NOT = 0                                        
                 COMPUTE W-MTCOMM ROUNDED  = (FPV050-MTCOMM(2) * 10000)         
                                           / FPV050-PMTCA(2)                    
                 MOVE W-MTCOMM     TO D05-COM-TOT-PCT                           
              END-IF                                                            
              IF FPV050-QVENDUE(2) NOT = 0                                      
                 COMPUTE W-QVENDUESV ROUNDED = (FPV050-QVENDUESV(2)             
                                        * 100) / FPV050-QVENDUE(2)              
                 MOVE W-QVENDUESV  TO D05-OA-TOT-PCT                            
              END-IF                                                            
              MOVE FPV050-QNBPSE(2) TO D05-NB-PSE-TOT                           
              IF FPV050-QNBPSAB(2) NOT = 0                                      
                 COMPUTE W-QNBPSE ROUNDED  = (FPV050-QNBPSE(2) * 100)           
                                            / FPV050-QNBPSAB(2)                 
                 MOVE W-QNBPSE      TO D05-PSE-TOT-PCT                          
              END-IF                                                            
              MOVE D05  TO IPV052-ENREG                                         
              WRITE IPV052-ENREG                                                
              ADD 1 TO CPT-LIGNE CPT-DETAIL                                     
           ELSE                                                                 
      *-- ON RENSEIGNE LA RUBRIQUE 'AUTRES VENDEURS'                            
              IF FPV050-QVENDUE(2) NOT = 0                                      
                 SET  EDIT-AUTRE TO TRUE                                        
                 ADD FPV050-QVENDUE   (2) TO W-FPV050-QVENDUE   (1)             
                 ADD FPV050-QVENDUESV (2) TO W-FPV050-QVENDUESV (1)             
                 ADD FPV050-PMTCA     (2) TO W-FPV050-PMTCA     (1)             
                 ADD FPV050-MTREM     (2) TO W-FPV050-MTREM     (1)             
                 ADD FPV050-MTCOMM    (2) TO W-FPV050-MTCOMM    (1)             
                 ADD FPV050-QNBPSE    (2) TO W-FPV050-QNBPSE    (1)             
                 ADD FPV050-QNBPSAB   (2) TO W-FPV050-QNBPSAB   (1)             
                 ADD FPV050-PMTCAPSE  (2) TO W-FPV050-PMTCAPSE  (1)             
              END-IF                                                            
           END-IF.                                                              
      *-- ON CUMULE AFIN DE RENSEIGNER LA LIGNE TOTALE                          
           ADD FPV050-QVENDUE(1)     TO W-FPV050-QVENDUE   (2).                 
           ADD FPV050-QVENDUESV(1)   TO W-FPV050-QVENDUESV (2).                 
           ADD FPV050-PMTCA  (1)     TO W-FPV050-PMTCA     (2).                 
           ADD FPV050-MTREM  (1)     TO W-FPV050-MTREM     (2).                 
           ADD FPV050-MTCOMM (1)     TO W-FPV050-MTCOMM    (2).                 
           ADD FPV050-QNBPSE (1)     TO W-FPV050-QNBPSE    (2).                 
           ADD FPV050-QNBPSAB(1)     TO W-FPV050-QNBPSAB   (2).                 
           ADD FPV050-PMTCAPSE(1)    TO W-FPV050-PMTCAPSE  (2).                 
           ADD FPV050-QVENDUE(2)     TO W-FPV050-QVENDUE   (3).                 
           ADD FPV050-QVENDUESV(2)   TO W-FPV050-QVENDUESV (3).                 
           ADD FPV050-PMTCA  (2)     TO W-FPV050-PMTCA     (3).                 
           ADD FPV050-MTREM  (2)     TO W-FPV050-MTREM     (3).                 
           ADD FPV050-MTCOMM (2)     TO W-FPV050-MTCOMM    (3).                 
           ADD FPV050-QNBPSE (2)     TO W-FPV050-QNBPSE    (3).                 
           ADD FPV050-QNBPSAB(2)     TO W-FPV050-QNBPSAB   (3).                 
           ADD FPV050-PMTCAPSE(2)    TO W-FPV050-PMTCAPSE  (3).                 
           PERFORM LECTURE-FPV050.                                              
      *                                                                 00005550
       FIN-TRAITEMENT. EXIT.                                            00005560
                    EJECT                                               00005570
      *                                                                 00006240
      *===============================================================* 00006250
       TRAIT-ENTETE            SECTION.                                 00006260
      *===============================================================* 00006270
      *                                                                 00006280
           IF (W-NMAG NOT = FPV050-NMAG                                         
              OR W-CVENDEUR   NOT = FPV050-CVENDEUR)                            
              AND EDIT-AUTRE                                                    
              AND CPT-DETAIL > 0                                                
              PERFORM EDITION-AUTRE                                             
           END-IF.                                                              
           IF CPT-PAGE > 0                                                      
              IF FPV050-CVENDEUR NOT = W-CVENDEUR                               
                 PERFORM EDITION-TOTAL                                          
              ELSE                                                              
                 MOVE T03         TO IPV052-ENREG                               
                 WRITE IPV052-ENREG                                             
              END-IF                                                            
           END-IF.                                                              
           ADD  1                 TO CPT-PAGE.                                  
           MOVE W-DATE-ED         TO E00-DATEDITE.                              
           MOVE CPT-PAGE          TO E00-NOPAGE.                                
           MOVE E00               TO IPV052-ENREG                               
           WRITE IPV052-ENREG.                                                  
           MOVE W-MOIS            TO E01-MOIS.                                  
           MOVE E01               TO IPV052-ENREG.                              
           WRITE IPV052-ENREG.                                                  
           MOVE FPV050-NMAG       TO E02-NMAG.                                  
           MOVE FPV050-LMAG       TO E02-LMAG.                                  
           MOVE E02               TO IPV052-ENREG.                              
           WRITE IPV052-ENREG.                                                  
           MOVE 3                 TO CPT-LIGNE.                                 
           PERFORM ENTETE-REDUITE.                                              
      *                                                                 00006400
       FIN-TRAIT-ENTETE. EXIT.                                          00006410
        EJECT                                                           00006420
      *                                                                 00006240
      *===============================================================* 00006250
       ENTETE-REDUITE          SECTION.                                 00006260
      *===============================================================* 00006270
      *                                                                 00006280
      *-- CAS OU L'ON CHANGE D'AGREGAT ET QU'IL EXISTE DES VENTES               
      *-- D'AUTRES VENDEURS SUR LES MOIS PRECEDENTS                             
           IF W-CVENDEUR NOT = FPV050-CVENDEUR                                  
              AND EDIT-AUTRE                                                    
              AND CPT-DETAIL > 0                                                
              PERFORM EDITION-AUTRE                                             
              MOVE T03               TO IPV052-ENREG                            
              WRITE IPV052-ENREG                                                
           END-IF.                                                              
      *-- CAS OU L'ON IMPRIME LA LIGNE TOTAL VENDEUR                            
           IF CPT-PAGE > 0 AND CPT-LIGNE > 3                                    
              AND FPV050-CVENDEUR NOT = W-CVENDEUR                              
              PERFORM EDITION-TOTAL                                             
           END-IF.                                                              
      *-- CAS OU UNE RUBRIQUE NE PEUT S'EDITER EN INTEGRALITE SUR 1 PAGE        
           IF CPT-PAGE > 0 AND CPT-LIGNE = 3                                    
              AND FPV050-CVENDEUR = W-CVENDEUR                                  
              MOVE '(SUITE)'      TO E04-SUITE                                  
           ELSE                                                                 
              MOVE SPACES         TO E04-SUITE                                  
           END-IF.                                                              
           MOVE FPV050-NMAG       TO W-NMAG                                     
           MOVE FPV050-CVENDEUR   TO W-CVENDEUR                                 
           MOVE E03               TO IPV052-ENREG.                              
           WRITE IPV052-ENREG                                                   
           MOVE FPV050-LVENDEUR   TO E04-LVENDEUR.                              
           MOVE FPV050-CVENDEUR   TO E04-CVENDEUR.                              
           MOVE E04               TO IPV052-ENREG.                              
           WRITE IPV052-ENREG.                                                  
           MOVE E05               TO IPV052-ENREG.                              
           WRITE IPV052-ENREG.                                                  
           MOVE E06               TO IPV052-ENREG.                              
           WRITE IPV052-ENREG.                                                  
           MOVE E07               TO IPV052-ENREG.                              
           WRITE IPV052-ENREG.                                                  
           MOVE E08               TO IPV052-ENREG.                              
           WRITE IPV052-ENREG.                                                  
           MOVE E09               TO IPV052-ENREG.                              
           WRITE IPV052-ENREG.                                                  
           MOVE E10               TO IPV052-ENREG.                              
           WRITE IPV052-ENREG.                                                  
           MOVE E11               TO IPV052-ENREG.                              
           WRITE IPV052-ENREG.                                                  
           MOVE E09               TO IPV052-ENREG.                              
           WRITE IPV052-ENREG.                                                  
           ADD 12                 TO CPT-LIGNE.                                 
      *                                                                 00006400
       FIN-ENTETE-REDUITE. EXIT.                                        00006410
        EJECT                                                           00006420
      *                                                                 00006240
      *===============================================================* 00006250
       EDITION-AUTRE           SECTION.                                 00006260
      *===============================================================* 00006270
                                                                        00006280
           INITIALIZE D05.                                                      
           MOVE 'AUTRES RUBRIQUES'    TO D05-LAGREGATED.                        
           MOVE W-FPV050-QVENDUE(1)   TO D05-NB-TOT.                            
           MOVE W-FPV050-PMTCA(1)     TO D05-CA-TOT.                            
           COMPUTE W-MTREM = W-FPV050-MTREM(1) + W-FPV050-PMTCA(1)              
           IF W-MTREM NOT = 0                                                   
              COMPUTE W-FPV050-MTREM(1) ROUNDED =                               
                               (W-FPV050-MTREM(1) * 100) / W-MTREM              
              MOVE W-FPV050-MTREM(1)     TO D05-REM-TOT                         
           END-IF.                                                              
           IF W-FPV050-QVENDUE(1) NOT = 0                                       
              COMPUTE W-PMTCA ROUNDED = W-FPV050-PMTCA(1)                       
                                      / W-FPV050-QVENDUE(1)                     
              MOVE W-PMTCA            TO D05-PVM-TOT                            
           END-IF.                                                              
           IF W-FPV050-PMTCA(1) NOT = 0                                         
              COMPUTE W-MTCOMM ROUNDED = (W-FPV050-MTCOMM(1) * 10000)           
                                       / W-FPV050-PMTCA(1)                      
              MOVE W-MTCOMM           TO D05-COM-TOT-PCT                        
           END-IF.                                                              
           IF W-FPV050-QVENDUE(1) NOT = 0                                       
              COMPUTE W-QVENDUESV ROUNDED = (W-FPV050-QVENDUESV(1)              
                                         * 100) / W-FPV050-QVENDUE(1)           
              MOVE W-QVENDUESV        TO D05-OA-TOT-PCT                         
           END-IF.                                                              
           MOVE W-FPV050-QNBPSE(1)    TO D05-NB-PSE-TOT.                        
           IF W-FPV050-QNBPSAB(1) > 0                                           
              COMPUTE W-QNBPSE ROUNDED = (W-FPV050-QNBPSE(1) * 100)             
                                         / W-FPV050-QNBPSAB(1)                  
              MOVE W-QNBPSE           TO D05-PSE-TOT-PCT                        
           END-IF.                                                              
           MOVE D05                   TO IPV052-ENREG.                          
           WRITE IPV052-ENREG.                                                  
           ADD 1                      TO CPT-LIGNE.                             
      *                                                                 00006400
       FIN-EDITION-AUTRE. EXIT.                                         00006410
        EJECT                                                           00006420
      *                                                                 00006240
      *===============================================================* 00006250
       EDITION-TOTAL           SECTION.                                 00006260
      *===============================================================* 00006270
                                                                        00006280
           INITIALIZE T02.                                                      
           MOVE W-FPV050-QVENDUE(2)   TO T02-NB.                                
           MOVE W-FPV050-PMTCA(2)     TO T02-CA.                                
           COMPUTE W-MTREM = W-FPV050-MTREM(2) + W-FPV050-PMTCA(2)              
           IF W-MTREM NOT = 0                                                   
              COMPUTE W-FPV050-MTREM(2) ROUNDED =                               
                               (W-FPV050-MTREM(2) * 100) / W-MTREM              
              MOVE W-FPV050-MTREM(2)     TO T02-REM                             
           END-IF.                                                              
           IF W-FPV050-QVENDUE(2) NOT = 0                                       
              COMPUTE W-PMTCA ROUNDED = W-FPV050-PMTCA(2)                       
                                      / W-FPV050-QVENDUE(2)                     
              MOVE W-PMTCA            TO T02-PVM                                
           END-IF.                                                              
           IF W-FPV050-PMTCA(2) NOT = 0                                         
              COMPUTE W-MTCOMM ROUNDED = (W-FPV050-MTCOMM(2) * 10000)           
                                       / W-FPV050-PMTCA(2)                      
              MOVE W-MTCOMM           TO T02-COM-PCT                            
           END-IF.                                                              
           IF W-FPV050-QVENDUE(2) NOT = 0                                       
              COMPUTE W-QVENDUESV ROUNDED = (W-FPV050-QVENDUESV(2)              
                                      * 100) / W-FPV050-QVENDUE(2)              
              MOVE W-QVENDUESV        TO T02-OA-PCT                             
           END-IF.                                                              
           MOVE W-FPV050-QNBPSE(2)    TO T02-NB-PSE.                            
           IF W-FPV050-QNBPSAB(2) NOT = 0                                       
              COMPUTE W-QNBPSE ROUNDED = (W-FPV050-QNBPSE(2) * 100)             
                                         / W-FPV050-QNBPSAB(2)                  
              MOVE W-QNBPSE           TO T02-PSE-PCT                            
           END-IF.                                                              
           MOVE W-FPV050-QVENDUE(3)   TO T02-NB-TOT.                            
           MOVE W-FPV050-PMTCA(3)     TO T02-CA-TOT.                            
           COMPUTE W-MTREM = W-FPV050-MTREM(3) + W-FPV050-PMTCA(3)              
           IF W-MTREM NOT = 0                                                   
              COMPUTE W-MTREM ROUNDED =                                         
                               (W-FPV050-MTREM(3) * 100) / W-MTREM              
              MOVE W-MTREM            TO T02-REM-TOT                            
           END-IF.                                                              
           IF W-FPV050-QVENDUE(3) NOT = 0                                       
              COMPUTE W-PMTCA ROUNDED = W-FPV050-PMTCA(3)                       
                                      / W-FPV050-QVENDUE(3)                     
              MOVE W-PMTCA            TO T02-PVM-TOT                            
           END-IF                                                               
           IF W-FPV050-PMTCA(3) NOT = 0                                         
              COMPUTE W-MTCOMM ROUNDED = (W-FPV050-MTCOMM(3) * 10000)           
                                       / W-FPV050-PMTCA(3)                      
              MOVE W-MTCOMM           TO T02-COM-TOT-PCT                        
           END-IF.                                                              
           IF W-FPV050-QVENDUE(3) NOT = 0                                       
              COMPUTE W-QVENDUESV ROUNDED = (W-FPV050-QVENDUESV(3)              
                                 * 100)  / W-FPV050-QVENDUE(3)                  
              MOVE W-QVENDUESV        TO T02-OA-TOT-PCT                         
           END-IF.                                                              
           MOVE W-FPV050-QNBPSE(3)  TO T02-NB-PSE-TOT.                          
           IF W-FPV050-QNBPSAB(3) NOT = 0                                       
              COMPUTE W-QNBPSE ROUNDED = (W-FPV050-QNBPSE(3) * 100)             
                                         / W-FPV050-QNBPSAB(3)                  
              MOVE W-QNBPSE           TO T02-PSE-TOT-PCT                        
           END-IF.                                                              
           MOVE T02                   TO IPV052-ENREG.                          
           WRITE IPV052-ENREG.                                                  
           MOVE T03                   TO IPV052-ENREG.                          
           WRITE IPV052-ENREG.                                                  
           ADD 2                      TO CPT-LIGNE.                             
           INITIALIZE W-FPV050-DSECT.                                           
           MOVE  0                    TO CPT-DETAIL.                            
           MOVE '0'                   TO CODE-AUTRE.                            
      *                                                                 00006400
       FIN-EDITION-TOTAL. EXIT.                                         00006410
        EJECT                                                           00006420
      *                                                                 00006240
      *===============================================================* 00006250
       SORTIE                  SECTION.                                 00006260
      *===============================================================* 00006270
      *                                                                 00006280
           IF CPT-PAGE > 0 AND EDIT-AUTRE AND CPT-DETAIL > 0                    
              PERFORM EDITION-AUTRE                                             
           END-IF.                                                              
           PERFORM EDITION-TOTAL.                                               
           DISPLAY '*********** BPV053/EXTRACTION ******************'.  00006310
           DISPLAY '** NB LECTURES FPV050   :' W-NB-FPV050.             00006340
           DISPLAY '** NB PAGES EDITEES     :' CPT-PAGE.                00006340
           DISPLAY '** NB LIGNES SUR DERN. PAGE :' CPT-LIGNE.           00006340
           DISPLAY '************************************************'.  00006330
           PERFORM FERMETURE-FICHIERS.                                  00006380
           PERFORM FIN-PROGRAMME.                                       00006390
      *                                                                 00006400
       FIN-SORTIE. EXIT.                                                00006410
        EJECT                                                           00006420
      *                                                                 00005300
      *===============================================================* 00005310
       LECTURE-FPV050             SECTION.                              00005320
      *===============================================================* 00005330
      *                                                                 00005340
           READ FPV050 AT END                                           00005370
             SET FIN-FPV050 TO TRUE                                             
           END-READ.                                                    00005530
           ADD 1 TO W-NB-FPV050.                                                
      *                                                                 00005550
       FIN-LECTURE-FPV050. EXIT.                                        00005560
                    EJECT                                               00005570
      *                                                                 00006430
      *================================================================*00006440
       FERMETURE-FICHIERS       SECTION.                                00006450
      *================================================================*00006460
      *                                                                 00006470
           CLOSE FMOIS.                                                 00006480
           CLOSE FDATE.                                                 00006480
           CLOSE FNSOC.                                                 00006480
           CLOSE FPV050.                                                00006490
           CLOSE IPV052.                                                00006490
      *                                                                 00006500
       F-FERMETURE-FICHIERS. EXIT.                                      00006510
        EJECT                                                           00006520
      *                                                                 00006530
      *================================================================*00006540
       FIN-PROGRAMME          SECTION.                                  00006550
      *================================================================*00006560
      *                                                                 00006570
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP  RUN.                                                   00006580
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                 00006590
       FIN-FIN-PROGRAMME. EXIT.                                         00006600
        EJECT                                                           00006610
      *                                                                 00006620
      *================================================================*00006630
       PLANTAGE                  SECTION.                               00006640
      *================================================================*00006650
      *                                                                 00006660
           PERFORM  FIN-ANORMALE.                                       00006670
           MOVE  'BPV053' TO ABEND-PROG.                                00006680
           CALL  'ABEND' USING ABEND-PROG  ABEND-MESS.                  00006690
      *                                                                 00006700
       FIN-PLANTAGE. EXIT.                                              00006710
        EJECT                                                           00006720
      *                                                                 00006730
      *================================================================*00006740
       FIN-ANORMALE              SECTION.                               00006750
      *================================================================*00006760
      *                                                                 00006770
           DISPLAY  '**********************************************'.   00006780
           DISPLAY  '***      INTERRUPTION DU PROGRAMME         ***'.   00006790
           DISPLAY  '***              BPV053                    ***'.   00006800
           DISPLAY  '**********************************************'.   00006810
                                                                        00006820
           PERFORM  FERMETURE-FICHIERS.                                 00006830
      *                                                                 00006840
       FIN-FIN-ANORMALE. EXIT.                                          00006850
        EJECT                                                           00006860
