      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.            BPV105.                                           
       AUTHOR. DSA011-FC.                                                       
      *                                                                         
      * ============================================================= *         
      *                                                               *         
      *       PROJET        :  TRAITEMENT DES PRIMES VENDEURS         *         
      *       PROGRAMME     :  BPV105.                                *         
      *       PERIODICITE   :  MENSUEL.                               *         
      *       FONCTION      :  EPURATION DU FICHIER HISTORIQUES       *         
      *                                                               *         
      *       DATE CREATION :  28/12/1992.                            *         
      * ============================================================= *         
      *                                                                         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
      *{ Tr-Source-Computer-Bis 1.3                                             
      *SOURCE-COMPUTER. IBM-370.                                                
      *--                                                                       
       SOURCE-COMPUTER. UNIX-MF.                                                
      *}                                                                        
      *SOURCE-COMPUTER. IBM-370 WITH DEBUGGING MODE.                            
      *{ Tr-Object-Computer-Bis 1.1                                             
      *OBJECT-COMPUTER. IBM-370.                                                
      *--                                                                       
        OBJECT-COMPUTER. UNIX-MF.                                               
      *}                                                                        
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
      *                                                                         
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FMOIS         ASSIGN  TO  FMOIS.                            
      *--                                                                       
            SELECT  FMOIS         ASSIGN  TO  FMOIS                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FPV100H       ASSIGN  TO  FPV100H.                          
      *--                                                                       
            SELECT  FPV100H       ASSIGN  TO  FPV100H                           
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FPV100HN      ASSIGN  TO  FPV100HN.                         
      *--                                                                       
            SELECT  FPV100HN      ASSIGN  TO  FPV100HN                          
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      *                                                                         
      *                                                                         
      * ============================================================= *         
      *  D E F I N I T I O N    F I C H I E R S                       *         
      * ============================================================= *         
      *                                                                         
       FD   FPV100H                                                             
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01  FPV100H-ENREG           PIC  X(250).                                 
      *                                                                         
       FD   FPV100HN                                                            
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01  FPV100HN-ENREG          PIC  X(250).                                 
      *                                                                         
       FD   FMOIS                                                               
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01  FMOIS-ENREG                PIC  X(80).                               
      *                                                                         
      * ============================================================= *         
      *                 W O R K I N G  -  S T O R A G E               *         
      * ============================================================= *         
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
      *                                                                         
      *                                                                         
      * MODULES APPELES PAR CALL                                                
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *77  ABEND                   PIC X(08) VALUE 'ABEND   '.                  
      *--                                                                       
       77  MW-ABEND                   PIC X(08) VALUE 'ABEND   '.               
      *}                                                                        
       77  BETDATC                 PIC X(08) VALUE 'BETDATC '.                  
      *                                                                         
       01  ETAT-FPV100H            PIC  X(01)  VALUE '0'.                       
           88  FIN-FPV100H         VALUE  '1'.                                  
      *                                                                         
       01  COMPTEURS.                                                           
           05  ZCPT                  PIC  -(9)9.                                
           05  W-FPV100H-LUS         PIC  S9(09)  COMP-3  VALUE  +0.            
           05  W-FPV100HN-ECRITS     PIC  S9(09)  COMP-3  VALUE  +0.            
      *                                                                         
      *                                                                         
       01  W-MOIS.                                                              
           05 W-MOIS-SSAA            PIC 9999.                                  
           05 W-MOIS-MM              PIC 99.                                    
           COPY ABENDCOP.                                                       
           COPY WORKDATC.                                                       
           COPY SWPV100.                                                        
       PROCEDURE DIVISION.                                                      
      *                                                                         
      * ============================================================= *         
      *              T R A M E   DU   P R O G R A M M E               *         
      * ============================================================= *         
      *                                                               *         
      * ============================================================= *         
      *                                                               *         
      *           ------------------------------------------          *         
      *           --  MODULE DE BASE DU PROGRAMME  BPV105 --          *         
      *           ------------------------------------------          *         
      *                               I                               *         
      *           -----------------------------------------           *         
      *           I                   I                   I           *         
      *   -----------------   -----------------   -----------------   *         
      *   -    DEBUT      -   -  TRAITEMENT   -   -      FIN      -   *         
      *   -----------------   -----------------   -----------------   *         
      *                                                               *         
      * ============================================================= *         
      *                                                                         
       MODULE-BPV105 SECTION.                                                   
      *                                                                         
           PERFORM  DEBUT-BPV105.                                               
      *                                                                         
      *    TRAITEMENT                                                           
           PERFORM  TRAIT-FPV100H        UNTIL  FIN-FPV100H.                    
      *                                                                         
      *    FIN DU TRAITEMENT                                                    
           PERFORM  COMPTE-RENDU                                                
           PERFORM  FERMETURE-FICHIERS                                          
           PERFORM  FIN-PROGRAMME.                                              
       MODULE-BPV105-FIN. EXIT.                                                 
      **-------------------------------------------------------------**         
      *                                                                         
      **-------------------------------------------------------------**         
       DEBUT-BPV105 SECTION.                                                    
           OPEN  INPUT   FPV100H  FMOIS                                         
                 OUTPUT  FPV100HN.                                              
      *                                                                         
      *    LECTURE FICHIER FMOIS                                                
           READ FMOIS INTO W-MOIS                                               
                END   MOVE 'PAS DE FMOIS EN ENTREE' TO ABEND-MESS               
                      PERFORM FIN-ANORMALE                                      
           END-READ.                                                            
           STRING '01' W-MOIS DELIMITED SIZE INTO GFJJMMSSAA                    
           MOVE '1' TO GFDATA                                                   
           CALL BETDATC USING WORK-BETDATC                                      
           IF GFVDAT NOT = '1'                                                  
              MOVE 'FMOIS REJETE PAR BETDATC' TO ABEND-MESS                     
              PERFORM FIN-ANORMALE                                              
           END-IF                                                               
           MOVE GFSAMJ-0(1:6) TO W-MOIS                                         
      *                                                                         
      *    CALCUL MOIS - 2                                                      
           IF W-MOIS-MM > 2                                                     
              COMPUTE W-MOIS-MM   = W-MOIS-MM   - 2                             
           ELSE                                                                 
              COMPUTE W-MOIS-MM   = W-MOIS-MM   + 10                            
              COMPUTE W-MOIS-SSAA = W-MOIS-SSAA - 1                             
           END-IF                                                               
      *                                                                         
      *    LECTURE INITIALE FPV100H                                             
           PERFORM  LECTURE-FPV100H.                                            
       FIN-DEBUT-BPV105. EXIT.                                                  
      **-------------------------------------------------------------**         
      *                                                                         
      **-------------------------------------------------------------**         
      **-------------------------------------------------------------**         
       TRAIT-FPV100H SECTION.                                                   
           ADD 1   TO W-FPV100H-LUS                                             
           IF      FPV100-DMVENTE > W-MOIS                                      
                   WRITE FPV100HN-ENREG FROM FPV100-DSECT                       
                   ADD 1 TO W-FPV100HN-ECRITS                                   
           END-IF                                                               
           PERFORM LECTURE-FPV100H.                                             
       FIN-TRAIT-FPV100H. EXIT.                                                 
      **-------------------------------------------------------------**         
      *                                                                         
      **-------------------------------------------------------------**         
      *                                                                         
       LECTURE-FPV100H         SECTION.                                         
           READ FPV100H INTO FPV100-DSECT AT END                                
              SET  FIN-FPV100H       TO TRUE                                    
           END-READ.                                                            
       FIN-LECTURE-FPV100H. EXIT.                                               
      **-------------------------------------------------------------**         
      *                                                                         
      *                                                                         
      *====>  C O M P T E  -  R E N D U.                                        
      *                                                                         
       COMPTE-RENDU      SECTION.                                               
           DISPLAY  '**'.                                                       
           DISPLAY  '**           B P V 1 0 5 '.                                
           DISPLAY  '**'.                                                       
           DISPLAY  '** ' W-FPV100H-LUS ' LUS DANS FICHIER FPV100H ;'           
           DISPLAY  '**'.                                                       
           DISPLAY  '** ' W-FPV100HN-ECRITS ' ECRITS DANS FICHIER'              
                        ' FPV100HN ;'.                                          
       FIN-COMPTE-RENDU.        EXIT.                                           
      **-------------------------------------------------------------**         
      *                                                                         
      **-------------------------------------------------------------**         
       FERMETURE-FICHIERS    SECTION.                                           
           CLOSE  FPV100H FPV100HN FMOIS.                                       
       FIN-FERMETURE-FICHIERS.    EXIT.                                         
      **-------------------------------------------------------------**         
      *                                                                         
      **-------------------------------------------------------------**         
       FIN-PROGRAMME        SECTION.                                            
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP  RUN.                                                           
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      **--------------------------------------------------------------**        
       FIN-ANORMALE    SECTION.                                                 
           PERFORM  FERMETURE-FICHIERS.                                         
           MOVE  'BPV105'                  TO  ABEND-PROG.                      
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL  ABEND  USING  ABEND-PROG    ABEND-MESS.                        
      *--                                                                       
           CALL  MW-ABEND  USING  ABEND-PROG    ABEND-MESS.                     
      *}                                                                        
       FIN-FIN-ANORMALE.    EXIT.                                               
