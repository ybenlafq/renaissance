      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.      BCE620.                                                 
       AUTHOR.              AD.                                                 
       ENVIRONMENT DIVISION.                                                    
      ******************************************************************        
      *                                                                         
      *  PHASE 1: GENERATION DU SYSIN POUR FAST UNLOAD AVEC UNE DATE            
      *           A J                                                           
      ******************************************************************        
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FDATE  ASSIGN TO FDATE.                                      
      *--                                                                       
            SELECT FDATE  ASSIGN TO FDATE                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FSYSIN ASSIGN TO FSYSIN.                                     
      *--                                                                       
            SELECT FSYSIN ASSIGN TO FSYSIN                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FCE620O ASSIGN TO FCE620O.                                   
      *--                                                                       
            SELECT FCE620O ASSIGN TO FCE620O                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FDATE RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.             
       01  MW-FILLER PIC X(80).                                                 
       FD  FSYSIN RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
      *MW DAR-2                                                                 
       01  MW-FILLER PIC X(80).                                                 
       FD  FCE620O RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.           
       01  ENR-FCE620O PIC X(80).                                               
       WORKING-STORAGE SECTION.                                                 
      ***************************************************************           
      * ZONE OUT POUR FCE620O, FLAG POUR FSYSIN                                 
      ***************************************************************           
       01  W-FCE620O PIC X(80).                                                 
       01  FILLER PIC X VALUE SPACE.                                            
           88  FIN-FSYSIN VALUE 'F'.                                            
      ***************************************************************           
      * COMMS AVEC LES SOUS PROGS                                               
      ***************************************************************           
           COPY ABENDCOP.                                                       
       01  BETDATC PIC X(8) VALUE 'BETDATC'.                                    
           COPY WORKDATC.                                                       
      ***************************************************************           
      * PROC:                                                                   
      * - LECTURE DATE                                                          
      * - GENERATION DE SYSIN                                                   
      * - C'EST TOUT                                                            
      ***************************************************************           
       PROCEDURE DIVISION.                                                      
           DISPLAY 'BCE620: CONSTRUCTION SYSIN POUR UNLOAD'                     
           OPEN INPUT FDATE FSYSIN                                              
           OUTPUT FCE620O.                                                      
           READ FDATE INTO GFJJMMSSAA AT END                            01090024
                MOVE 'FICHIER FDATE VIDE' TO ABEND-MESS                 01100024
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           CLOSE FDATE.                                                 01130024
           MOVE '1'            TO GFDATA.                               00004640
           CALL BETDATC      USING WORK-BETDATC                         00004670
           IF GFVDAT NOT = '1'                                          00004680
              STRING 'BETDATC : ' GF-MESS-ERR                           00004700
              DELIMITED BY SIZE INTO ABEND-MESS                         00004710
              PERFORM ABEND-PROGRAMME                                           
           END-IF.                                                      00004730
           DISPLAY 'BCE620: date param�tre lue sur FDATE: '                     
                    GFSMN-LIB-L ' ' GFJOUR ' '                                  
                    GFMOI-LIB-L ' ' GFSIECLE GFANNEE.                           
      ******************************************************************        
      * RECOPIE DE FSYSIN                                                       
      ******************************************************************        
           DISPLAY 'BCE620: sysin g�n�r� � partir de FSYSIN:'                   
           PERFORM UNTIL FIN-FSYSIN                                             
              READ FSYSIN INTO W-FCE620O                                        
                 AT END                                                         
                    SET FIN-FSYSIN TO TRUE                                      
                 NOT AT END                                                     
                    INSPECT W-FCE620O REPLACING ALL ':&FDATE:'                  
                    BY GFSAMJ-0                                                 
                    WRITE ENR-FCE620O FROM W-FCE620O                            
                    DISPLAY W-FCE620O                                           
              END-READ                                                          
           END-PERFORM.                                                         
           CLOSE FSYSIN.                                                        
      ******************************************************************        
      * ZIENDE                                                                  
      ******************************************************************        
           CLOSE FCE620O.                                                       
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      ******************************************************************        
      * SCHPLOUF                                                                
      ******************************************************************        
       ABEND-PROGRAMME                 SECTION.                                 
           MOVE  'BCE620'     TO   ABEND-PROG.                                  
           CALL  'ABEND'   USING   ABEND-PROG  ABEND-MESS.                      
