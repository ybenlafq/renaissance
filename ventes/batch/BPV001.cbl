      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                     BPV001.                                  
       AUTHOR. DSA051.                                                          
      ******************************************************************        
      *                                                                *        
      *      EXTRACTION DES ARTICLES DONT LE PRIX EST INFERIEUR AUX    *        
      *                  CONCURRENTS DE SA ZONE DE PRIX                *        
      *                                                                *        
      ******************************************************************        
      *     M O D I F I C A T I O N S  /  M A I N T E N A N C E S      *        
      ******************************************************************        
      *  DATE       :                                                 *         
      *  AUTEUR     :                                                 *         
      *  REPAIRE    :                                                 *         
      *  OBJET      :                                                           
      *****************************************************************         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                            00000180
       FILE-CONTROL.                                                    00000190
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT IPV002   ASSIGN TO IPV002.                                   
      *--                                                                       
            SELECT IPV002   ASSIGN TO IPV002                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT IPV001   ASSIGN TO IPV001.                                   
      *--                                                                       
            SELECT IPV001   ASSIGN TO IPV001                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                   00000240
       FILE SECTION.                                                    00000250
      *                                                                 00000260
       FD  IPV002                                                       00000270
           RECORDING F                                                  00000280
           BLOCK 0 RECORDS                                              00000290
           LABEL RECORD STANDARD.                                       00000300
       01  IPV002-ENR        PIC X(512).                                00000310
      *                                                                 00000260
       FD  IPV001                                                       00000270
           RECORDING F                                                  00000280
           BLOCK 0 RECORDS                                              00000290
           LABEL RECORD STANDARD.                                       00000300
       01  IPV001-ENR        PIC X(512).                                00000310
      *                                                                 00000260
       WORKING-STORAGE SECTION.                                                 
                  COPY  IPV002.                                                 
                  COPY  IPV001.                                                 
                  COPY  SYKWDIV0.                                               
                  COPY  ABENDCOP.                                               
                  COPY  WORKDATC.                                               
           EJECT                                                                
      *****************************************************************         
      *   DECRIRE   ICI   LES   ZONES   SPECIFIQUES   AU   PROGRAMME  *         
      *****************************************************************         
      *                                                                         
       01  W-FLAG.                                                              
           05  FLAG-IPV002         PIC X VALUE 'O'.                             
               88 NON-FIN-IPV002         VALUE 'O'.                             
               88 FIN-IPV002             VALUE 'N'.                             
      *                                                                         
       01  CTR-IPV002 PIC 9(6) VALUE ZERO.                                      
       01  CTR-IPV001 PIC 9(6) VALUE ZERO.                                      
      *                                                                 00019300
       PROCEDURE DIVISION.                                                      
      *****************************************************************         
      *              T R A M E   DU   P R O G R A M M E               *         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *           -----------------------------------------           *         
      *           -   MODULE DE BASE DU PROGRAMME BPV001  -           *         
      *           -----------------------------------------           *         
      *                               I                               *         
      *           -----------------------------------------           *         
      *           I                   I                   I           *         
      *   -----------------   -----------------   -----------------   *         
      *   -    ENTREE     -   -  TRAITEMENT   -   -     SORTIE    -   *         
      *   -----------------   -----------------   -----------------   *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-BPV001                  SECTION.                                  
      *                                                                         
           DISPLAY '>>>>>> DEBUT'                                               
           PERFORM MODULE-ENTREE.                                               
           DISPLAY '>>>>>> TRAITEMENT'                                          
           PERFORM MODULE-TRAITEMENT UNTIL FIN-IPV002.                          
           DISPLAY '>>>>>> SORTIE'                                              
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
       FIN-MODULE-BPV001.  EXIT.                                                
                    EJECT                                                       
      *                                                                         
       MODULE-ENTREE              SECTION.                                      
      *                                                                         
           OPEN INPUT  IPV002.                                                  
           OPEN OUTPUT IPV001.                                                  
           PERFORM LECTURE-IPV002.                                              
      *                                                                         
      *                                                                         
       FIN-MODULE-ENTREE.   EXIT.                                               
                    EJECT                                                       
      ***********************************************************               
       MODULE-TRAITEMENT          SECTION.                                      
      ***********************************************************               
           INITIALIZE DSECT-IPV001.                                             
           MOVE 'IPV001'                  TO NOMETAT-IPV001.                    
           MOVE  1                        TO IPV001-SEQUENCE                    
           MOVE  IPV002-FDATE             TO IPV001-FDATE                       
           MOVE  IPV002-NSOCIETE          TO IPV001-NSOCIETE                    
           MOVE  IPV002-CHEFPROD          TO IPV001-CHEFPROD                    
           MOVE  IPV002-CMARQ             TO IPV001-CMARQ                       
           MOVE  IPV002-WSEQFAM           TO IPV001-WSEQFAM                     
           MOVE  IPV002-LREFFOURN         TO IPV001-LREFFOURN                   
           MOVE  IPV002-NLIEU             TO IPV001-NLIEU                       
           MOVE  IPV002-NCODIC            TO IPV001-NCODIC                      
           MOVE  IPV002-CFAM              TO IPV001-CFAM                        
           MOVE  IPV002-CPROGRAMME        TO IPV001-CPROGRAMME                  
           MOVE  IPV002-LENSCONC          TO IPV001-LENSCONC                    
           MOVE  IPV002-NCONC             TO IPV001-NCONC                       
           MOVE  IPV002-NZONPRIX          TO IPV001-NZONPRIX                    
           MOVE  IPV002-DEFFET            TO IPV001-DEFFET                      
           MOVE  IPV002-DFINEFFET         TO IPV001-DFINEFFET                   
           MOVE  IPV002-PEXPTTC           TO IPV001-PEXPTTC                     
           MOVE  IPV002-PSTDTTC           TO IPV001-PSTDTTC                     
           WRITE IPV001-ENR   FROM DSECT-IPV001                                 
           ADD 1 TO CTR-IPV001                                                  
           PERFORM    LECTURE-IPV002.                                           
      *                                                                         
      *------------------------------------------                               
       LECTURE-IPV002  SECTION.                                                 
      *------------------------------------------                               
           READ IPV002 INTO DSECT-IPV002 AT END                                 
              SET FIN-IPV002 TO TRUE                                            
           END-READ.                                                            
           IF NON-FIN-IPV002                                                    
              ADD 1 TO CTR-IPV002                                               
           END-IF.                                                              
      *                                                                         
       FIN-LECTURE-IPV002. EXIT.                                                
      *-----------------------------------------                                
       MODULE-SORTIE              SECTION.                                      
      *-----------------------------------------                                
           CLOSE IPV002 IPV001.                                                 
           DISPLAY '****************************'.                              
           DISPLAY ' IPV002 LUS    = ' CTR-IPV002.                              
           DISPLAY ' IPV001 ECRITS = ' CTR-IPV001.                              
           DISPLAY '     FIN NORMALE BPV001     '.                              
           DISPLAY '****************************'.                              
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-MODULE-SORTIE. EXIT.                                                 
                    EJECT                                                       
      *                                                                 00039300
      ** AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00039400
      *     M O D U L E     D ' A B A N D O N                         * 00039500
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00039600
      *                                                                 00039700
