      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000010
       PROGRAM-ID.   BRE002.                                            00000020
       AUTHOR.       DSA015.                                            00000030
                                                                        00000040
      * ------------------------------------------------------------ *  00000050
      *                                                              *  00000060
      *   PROJET           :                                         *  00000070
      *   PROGRAMME        : BRE002.                                 *  00000080
      *   PERIODICITE      : MENSUEL                                 *  00000090
      *   FONCTION         : PREPARATION FICHIER MENSUEL +           *  00000100
      *                    : FICHIER RELIQUAT                        *  00000100
      *   DATE DE CREATION : 18/01/1993.                             *  00000110
                                                                     *  00000120
      * ------------------------------------------------------------ *  00000130
                                                                        00000140
       ENVIRONMENT DIVISION.                                            00000150
       CONFIGURATION SECTION.                                           00000160
       INPUT-OUTPUT SECTION.                                            00000170
       FILE-CONTROL.                                                    00000180
                                                                        00000190
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FRE002   ASSIGN TO  FRE002.                          00000210
      *--                                                                       
           SELECT  FRE002   ASSIGN TO  FRE002                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FEXTRAC  ASSIGN TO  FEXTRAC.                         00000210
      *--                                                                       
           SELECT  FEXTRAC  ASSIGN TO  FEXTRAC                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FRE002S  ASSIGN TO  FRE002S.                         00000210
      *--                                                                       
           SELECT  FRE002S  ASSIGN TO  FRE002S                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FMOIS    ASSIGN TO  FMOIS.                           00000210
      *--                                                                       
           SELECT  FMOIS    ASSIGN TO  FMOIS                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FNSOC    ASSIGN TO  FNSOC.                           00000210
      *                                                                         
      *--                                                                       
           SELECT  FNSOC    ASSIGN TO  FNSOC                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00000220
      *}                                                                        
       DATA DIVISION.                                                   00000230
       FILE SECTION.                                                    00000240
                                                                        00000250
      * ------------------------------------------------------------ *  00000260
      *               D E S C R I P T I O N   F I C H I E R          *  00000270
      * ------------------------------------------------------------ *  00000280
                                                                        00000290
       FD  FRE002                                                       00000300
           RECORDING F                                                  00000310
           BLOCK 0 RECORDS                                              00000320
           LABEL RECORD STANDARD.                                       00000330
       01  ENR-FRE002.                                                  00000340
           02  FILLER PIC X(512).                                       00000350
                                                                        00000360
       FD  FMOIS                                                        00000300
           RECORDING F                                                  00000310
           BLOCK 0 RECORDS                                              00000320
           LABEL RECORD STANDARD.                                       00000330
       01  FILE-MOIS.                                                   00000340
           03  F-MOIS.                                                  00000350
               06  MM  PIC X(02).                                               
               06  SS  PIC X(02).                                               
               06  AA  PIC X(02).                                               
           03  FILLER  PIC X(74).                                       00000350
                                                                        00000360
       FD  FNSOC                                                        00000300
           RECORDING F                                                  00000310
           BLOCK 0 RECORDS                                              00000320
           LABEL RECORD STANDARD.                                       00000330
       01  FILE-FNSOC.                                                  00000340
           03  F-NSOC  PIC X(03).                                       00000350
           03  FILLER  PIC X(77).                                       00000350
                                                                        00000360
                                                                        00000370
       FD  FEXTRAC                                                      00000380
           RECORDING F                                                  00000390
           BLOCK 0 RECORDS                                              00000400
           LABEL RECORD STANDARD.                                       00000410
       01  ENR-FEXTRAC.                                                 00000420
           02  FILLER PIC X(512).                                       00000430
                                                                        00000440
                                                                        00000370
       FD  FRE002S                                                      00000380
           RECORDING F                                                  00000390
           BLOCK 0 RECORDS                                              00000400
           LABEL RECORD STANDARD.                                       00000410
       01  ENR-FRE002S.                                                 00000420
           02  FILLER PIC X(512).                                       00000430
                                                                        00000440
                                                                        00000450
      * ------------------------------------------------------------ *  00000460
      *                W O R K I N G - S T O R A G E                 *  00000470
      * ------------------------------------------------------------ *  00000480
                                                                        00000490
       WORKING-STORAGE SECTION.                                         00000500
                                                                        00000510
                                                                        00000630
           COPY IRE002.                                                 00000620
                                                                        00000630
      *                                                                 00000640
       77  CPT-FRE002                PIC S9(07) COMP-3   VALUE 0.       00000820
       77  CPT-FEXTRAC               PIC S9(07) COMP-3   VALUE 0.       00000820
       77  CPT-FRE002S               PIC S9(07) COMP-3   VALUE 0.       00000820
                                                                        00000980
       01  TEST-FRE002               PIC X(01) VALUE ' '.               00000990
           88 PAS-FIN-FRE002         VALUE '0'.                         00001000
           88 FIN-FRE002             VALUE '1'.                         00001010
                                                                        00001020
       01  W-SSAAMM.                                                            
           05  W-SS               PIC X(02)  VALUE ZEROES.              00001110
           05  W-AA               PIC X(02)  VALUE ZEROES.              00001120
           05  W-MM               PIC X(02)  VALUE ZEROES.              00001130
                                                                        00001140
       01  2-DATE.                                                      00001100
           05  2-SSAAMM.                                                        
               10  2-SS               PIC X(02)  VALUE ZEROES.          00001110
               10  2-AA               PIC X(02)  VALUE ZEROES.          00001120
               10  2-MM               PIC X(02)  VALUE ZEROES.          00001130
           05  2-JJ                   PIC X(02)  VALUE ZEROES.          00001130
                                                                        00001140
       01 W-NSOCIETE                PIC X(3).                           00001200
                                                                        00001210
      * ------------------------------------------------------------ *  00001220
      *         DESCRIPTION DES ZONES D'APPEL DU MODULE ABEND        *  00001230
      * ------------------------------------------------------------ *  00001240
                                                                        00001250
           COPY ABENDCOP.                                               00001260
           COPY WORKDATC.                                               00001260
                                                                        00001270
                                                                        00001400
                                                                        00004890
      * ------------------------------------------------------------ *  00001370
      *              P R O C E D U R E     D I V I S I O N           *  00001380
      * ------------------------------------------------------------ *  00001390
       PROCEDURE DIVISION.                                              00001410
                                                                        00001420
      * ------------------------------------------------------------ *  00001430
      *             T R A M E     D U     P R O G R A M M E          *  00001440
      * ------------------------------------------------------------ *  00001450
      *                                                              *  00001460
      *            -----------------------------------------         *  00001470
      *            -  MODULE DE BASE DU PROGRAMME  BRE002  -         *  00001480
      *            -----------------------------------------         *  00001490
      *                               I                              *  00001500
      *            -----------------------------------------         *  00001510
      *            I                  I                    I         *  00001520
      *       -----------      ----------------        ---------     *  00001530
      *       -  DEBUT  -      -  TRAITEMENT  -        -  FIN  -     *  00001540
      *       -----------      ----------------        ---------     *  00001550
      *                                                              *  00001560
      * ------------------------------------------------------------ *  00001570
                                                                        00001580
           PERFORM DEBUT-BRE002.                                        00001590
           PERFORM LECTURE-FRE002.                                      00001880
           PERFORM TRAIT-BRE002 UNTIL FIN-FRE002.                       00001600
           PERFORM FIN-BRE002.                                          00001610
                                                                        00001620
      * ------------------------------------------------------------ *  00001630
      *                     D E B U T    B R E 0 0 2                 *  00001640
      * ------------------------------------------------------------ *  00001650
                                                                        00001660
       DEBUT-BRE002 SECTION.                                            00001670
                                                                        00001680
                                                                        00001730
           PERFORM OUVERTURE-FICHIERS.                                  00001740
                                                                        00001750
           PERFORM LECTURE-PARAMETRES.                                          
                                                                        00001840
                                                                        00001860
           SET PAS-FIN-FRE002    TO TRUE.                               00001870
                                                                        00001920
       FIN-DEBUT-BRE002. EXIT.                                          00001930
       LECTURE-PARAMETRES SECTION.                                              
           OPEN INPUT FMOIS.                                                    
           READ FMOIS AT END                                                    
              MOVE 'PAS DE DATE EN ENTREE' TO ABEND-MESS                        
              PERFORM FIN-ANORMALE                                              
           END-READ.                                                            
           DISPLAY 'FILE MOIS : ' FILE-MOIS                                     
           MOVE SS   TO W-SS                                            00003380
           MOVE AA   TO W-AA                                            00003380
           MOVE MM   TO W-MM                                            00003380
           CLOSE FMOIS                                                          
           OPEN INPUT FNSOC.                                                    
           READ FNSOC AT END                                                    
              MOVE 'PAS DE FNSOC EN ENTREE' TO ABEND-MESS                       
              PERFORM FIN-ANORMALE                                              
           END-READ.                                                            
           MOVE F-NSOC       TO W-NSOCIETE.                             00003380
           CLOSE FNSOC.                                                         
       FIN-LECTURE-PARAMETRES. EXIT.                                            
                                                                        00001940
      * -------------------- LECTURE FICHIER FCAISSE --------------- *  00001950
                                                                        00001960
       LECTURE-FRE002   SECTION.                                        00001970
                                                                        00001980
           READ FRE002  INTO DSECT-IRE002 AT END                        00001990
                SET FIN-FRE002  TO TRUE                                 00002000
           END-READ.                                                    00002010
           IF PAS-FIN-FRE002                                            00001000
              ADD 1            TO CPT-FRE002                            00002020
           END-IF.                                                              
                                                                        00002030
       FIN-LECTURE-FRE002. EXIT.                                        00002040
                                                                        00002050
      * -------------------- OUVERTURE-FICHIERS -------------------- *  00002060
                                                                        00002070
       OUVERTURE-FICHIERS SECTION.                                      00002080
                                                                        00002090
           OPEN INPUT   FRE002.                                         00002100
           OPEN OUTPUT  FEXTRAC FRE002S.                                00002110
                                                                        00002120
       FIN-OUVERTURE-FICHIERS. EXIT.                                    00002130
                                                                        00002140
                                                                        00002150
      * ------------------------------------------------------------ *  00002160
      *                     T R A I T    B R E 0 0 2                 *  00002170
      * ------------------------------------------------------------ *  00002180
                                                                        00002190
       TRAIT-BRE002 SECTION.                                            00002200
                                                                        00002210
      *       TEST PAR RAPPORT AU MOIS EN COURS                         02390   
           MOVE  IRE002-FMOIS    TO 2-DATE                              00001880
           IF 2-SSAAMM    = W-SSAAMM                                    00001880
              PERFORM ECRITURE-MOIS-EN-COURS                                    
           END-IF                                                               
           IF 2-SSAAMM NOT < W-SSAAMM                                           
              PERFORM ECRITURE-MOIS-SUIVANT                                     
           END-IF.                                                              
           PERFORM LECTURE-FRE002.                                      00002490
      *                                                                 00002520
                                                                        00002530
       FIN-TRAIT-BRE002. EXIT.                                          00002540
      * ------------------------------------------------------------ *  00003360
      *               CHARGEMENT FICHIER POUR EDITION                *  00003370
      * ------------------------------------------------------------ *  00003380
                                                                        00003390
       ECRITURE-MOIS-EN-COURS SECTION.                                          
                                                                        00003410
           WRITE ENR-FEXTRAC FROM DSECT-IRE002.                         00003750
                                                                        00003760
           ADD 1 TO CPT-FEXTRAC.                                        00003770
                                                                        00003780
       FIN-ECRITURE-MOIS-EN-COURS. EXIT.                                        
                                                                        00003790
      * ------------------------------------------------------------ *  00003360
      *               CHARGEMENT FICHIER MOIS PROCHAIN               *  00003370
      * ------------------------------------------------------------ *  00003380
                                                                        00003390
       ECRITURE-MOIS-SUIVANT  SECTION.                                          
                                                                        00003410
           WRITE ENR-FRE002S FROM DSECT-IRE002.                         00003750
                                                                        00003760
           ADD 1 TO CPT-FRE002S.                                        00003770
                                                                        00003780
       FIN-ECRITURE-MOIS-SUIVANT. EXIT.                                         
                                                                        00003790
      * ------------------------ FIN-ANORMALE ---------------------- *  00004110
                                                                        00004120
       FIN-ANORMALE SECTION.                                            00004130
                                                                        00004140
           DISPLAY '*****************************************'.         00004150
           DISPLAY '***** FIN ANORMALE DU PROGRAMME *********'.         00004160
           DISPLAY '*****************************************'.         00004170
           CLOSE FRE002 FRE002S FEXTRAC.                                00004180
           MOVE 'BRE002' TO ABEND-PROG.                                 00004190
           CALL 'ABEND' USING ABEND-PROG ABEND-MESS.                    00004200
                                                                        00004210
       FIN-FIN-ANORMALE. EXIT.                                          00004220
                                                                        00004230
      * ------------------------------------------------------------ *  00003820
      *                      F I N     B R E 0 0 2                   *  00003830
      * ------------------------------------------------------------ *  00003840
                                                                        00003850
       FIN-BRE002 SECTION.                                              00003860
                                                                        00003870
           CLOSE FRE002 FRE002S FEXTRAC.                                00004180
           PERFORM COMPTE-RENDU.                                        00003930
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00003940
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        00003950
       FIN-FIN-BRE002. EXIT.                                            00003960
                                                                        00003970
      * ------------------------ COMPTE-RENDU ---------------------- *  00003980
                                                                        00003990
       COMPTE-RENDU SECTION.                                            00004000
                                                                        00004010
           DISPLAY '**'.                                                00004020
           DISPLAY '**          B R E 0 0 2'.                           00004030
           DISPLAY '**'.                                                00004040
           DISPLAY '** NOMBRE DE LECTURES FRE002   : '  CPT-FRE002.     00004050
           DISPLAY '** NOMBRE D''ECRITURES FEXTRAC : ' CPT-FEXTRAC.     00004060
           DISPLAY '** NOMBRE D''ECRITURES FRE002S : ' CPT-FRE002S.     00004060
           DISPLAY '**'.                                                00004070
                                                                        00004080
       FIN-COMPTE-RENDU. EXIT.                                          00004090
