      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00020000
       PROGRAM-ID.            BPV405.                                   00030000
       AUTHOR. DSA049-MH.                                               00040000
      *                                                                 00050000
      * ============================================================= * 00060000
      *                                                               * 00070000
      *       PROJET        :  TRAITEMENT DES PRIMES VENDEURS         * 00080000
      *       PROGRAMME     :  BPV405.                                * 00090000
      *       PERIODICITE   :  MENSUEL.                               * 00100000
      *       FONCTION      :  EPURATION DU FICHIER HISTORIQUES OFFRES* 00110000
      *                        POUR LA PAYE VENDEURS (DATE FMOIS - 2) * 00120000
      *       DATE CREATION :  28/12/1992.                            * 00130000
      * ============================================================= * 00140000
      *                                                                 00150000
       ENVIRONMENT DIVISION.                                            00160000
       CONFIGURATION SECTION.                                           00170000
      *{ Tr-Source-Computer-Bis 1.3                                             
      *SOURCE-COMPUTER. IBM-370.                                        00180000
      *--                                                                       
       SOURCE-COMPUTER. UNIX-MF.                                                
      *}                                                                        
      *{ Tr-Object-Computer-Bis 1.1                                             
      *OBJECT-COMPUTER. IBM-370.                                        00190000
      *--                                                                       
        OBJECT-COMPUTER. UNIX-MF.                                               
      *}                                                                        
       SPECIAL-NAMES.                                                   00200000
           DECIMAL-POINT IS COMMA.                                      00210000
       INPUT-OUTPUT SECTION.                                            00220000
      *                                                                 00230000
       FILE-CONTROL.                                                    00240000
      *                                                                 00250000
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FMOIS         ASSIGN  TO  FMOIS.                    00260000
      *                                                                         
      *--                                                                       
            SELECT  FMOIS         ASSIGN  TO  FMOIS                             
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00270000
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FPV400H       ASSIGN  TO  FPV400H.                  00280000
      *                                                                         
      *--                                                                       
            SELECT  FPV400H       ASSIGN  TO  FPV400H                           
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00290000
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FPV400HN      ASSIGN  TO  FPV400HN.                 00300000
      *--                                                                       
            SELECT  FPV400HN      ASSIGN  TO  FPV400HN                          
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00310000
       DATA DIVISION.                                                   00320000
       FILE SECTION.                                                    00330000
      *                                                                 00340000
      *                                                                 00350000
      *                                                                 00360000
      * ============================================================= * 00370000
      *  D E F I N I T I O N    F I C H I E R S                       * 00380000
      * ============================================================= * 00390000
      *                                                                 00400000
       FD   FPV400H                                                     00410000
            RECORDING F                                                 00420000
            BLOCK 0 RECORDS                                             00430000
            LABEL RECORD STANDARD.                                      00440000
                                                                        00450000
       01  FPV400H-ENREG           PIC  X(210).                         00460000
      *                                                                 00470000
       FD   FPV400HN                                                    00480000
            RECORDING F                                                 00490000
            BLOCK 0 RECORDS                                             00500000
            LABEL RECORD STANDARD.                                      00510000
                                                                        00520000
       01  FPV400HN-ENREG          PIC  X(210).                         00530000
      *                                                                 00540000
       FD   FMOIS                                                       00550000
            RECORDING F                                                 00560000
            BLOCK 0 RECORDS                                             00570000
            LABEL RECORD STANDARD.                                      00580000
                                                                        00590000
       01  FMOIS-ENREG                PIC  X(80).                       00600000
      *                                                                 00610000
      * ============================================================= * 00620000
      *                 W O R K I N G  -  S T O R A G E               * 00630000
      * ============================================================= * 00640000
      *                                                                 00650000
       WORKING-STORAGE SECTION.                                         00660000
      *                                                                 00670000
      *                                                                 00680000
      * MODULES APPELES PAR CALL                                        00690000
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *77  ABEND                   PIC X(08) VALUE 'ABEND   '.          00700000
      *--                                                                       
       77  MW-ABEND                   PIC X(08) VALUE 'ABEND   '.               
      *}                                                                        
       77  BETDATC                 PIC X(08) VALUE 'BETDATC '.          00710000
      *                                                                 00720000
       01  ETAT-FPV400H            PIC  X(01)  VALUE '0'.               00730000
           88  FIN-FPV400H         VALUE  '1'.                          00740000
      *                                                                 00750000
       01  COMPTEURS.                                                   00760000
           05  ZCPT                  PIC  -(9)9.                        00770000
           05  W-FPV400H-LUS         PIC  S9(09)  COMP-3  VALUE  +0.    00780000
           05  W-FPV400HN-ECRITS     PIC  S9(09)  COMP-3  VALUE  +0.    00790000
      *                                                                 00800000
      *                                                                 00810000
       01  W-MOIS.                                                      00820000
           05 W-MOIS-SSAA            PIC 9999.                          00830000
           05 W-MOIS-MM              PIC 99.                            00840000
                                                                        00850000
           COPY ABENDCOP.                                               00860000
           COPY WORKDATC.                                               00870000
           COPY SWPV100.                                                00880000
                                                                        00890000
                                                                        00900000
                                                                        00910000
                                                                        00920000
                                                                        00930000
       PROCEDURE DIVISION.                                              00940000
      *                                                                 00950000
      * ============================================================= * 00960000
      *              T R A M E   DU   P R O G R A M M E               * 00970000
      * ============================================================= * 00980000
      *                                                               * 00990000
      * ============================================================= * 01000000
      *                                                               * 01010000
      *           ------------------------------------------          * 01020000
      *           --  MODULE DE BASE DU PROGRAMME  BPV405 --          * 01030000
      *           ------------------------------------------          * 01040000
      *                               I                               * 01050000
      *           -----------------------------------------           * 01060000
      *           I                   I                   I           * 01070000
      *   -----------------   -----------------   -----------------   * 01080000
      *   -    DEBUT      -   -  TRAITEMENT   -   -      FIN      -   * 01090000
      *   -----------------   -----------------   -----------------   * 01100000
      *                                                               * 01110000
      * ============================================================= * 01120000
      *                                                                 01130000
       MODULE-BPV405 SECTION.                                           01140000
      *                                                                 01150000
           PERFORM  DEBUT-BPV405.                                       01160000
      *                                                                 01170000
      *    TRAITEMENT                                                   01180000
           PERFORM  TRAIT-FPV400H        UNTIL  FIN-FPV400H.            01190000
      *                                                                 01200000
      *    FIN DU TRAITEMENT                                            01210000
           PERFORM  COMPTE-RENDU                                        01220000
           PERFORM  FERMETURE-FICHIERS                                  01230000
           PERFORM  FIN-PROGRAMME.                                      01240000
       MODULE-BPV405-FIN. EXIT.                                         01250000
      **-------------------------------------------------------------** 01260000
      *                                                                 01270000
      **-------------------------------------------------------------** 01280000
       DEBUT-BPV405 SECTION.                                            01290000
           OPEN  INPUT   FPV400H  FMOIS                                 01300000
                 OUTPUT  FPV400HN.                                      01310000
      *                                                                 01320000
      *    LECTURE FICHIER FMOIS                                        01330000
           READ FMOIS INTO W-MOIS                                       01340000
                END   MOVE 'PAS DE FMOIS EN ENTREE' TO ABEND-MESS       01350000
                      PERFORM FIN-ANORMALE                              01360000
           END-READ.                                                    01370000
           STRING '01' W-MOIS DELIMITED SIZE INTO GFJJMMSSAA            01380000
           MOVE '1' TO GFDATA                                           01390000
           CALL BETDATC USING WORK-BETDATC                              01400000
           IF GFVDAT NOT = '1'                                          01410000
              MOVE 'FMOIS REJETE PAR BETDATC' TO ABEND-MESS             01420000
              PERFORM FIN-ANORMALE                                      01430000
           END-IF                                                       01440000
           MOVE GFSAMJ-0(1:6) TO W-MOIS                                 01450000
      *                                                                 01460000
      *    CALCUL MOIS - 2                                              01470000
           IF W-MOIS-MM > 2                                             01480000
              COMPUTE W-MOIS-MM   = W-MOIS-MM   - 2                     01490000
           ELSE                                                         01500000
              COMPUTE W-MOIS-MM   = W-MOIS-MM   + 10                    01510000
              COMPUTE W-MOIS-SSAA = W-MOIS-SSAA - 1                     01520000
           END-IF                                                       01530000
      *                                                                 01540000
      *    LECTURE INITIALE FPV400H                                     01550000
           PERFORM  LECTURE-FPV400H.                                    01560000
       FIN-DEBUT-BPV405. EXIT.                                          01570000
      **-------------------------------------------------------------** 01580000
      *                                                                 01590000
      **-------------------------------------------------------------** 01600000
      **-------------------------------------------------------------** 01610000
       TRAIT-FPV400H SECTION.                                           01620000
           ADD 1   TO W-FPV400H-LUS                                     01630000
           IF      FPV100-DMVENTE > W-MOIS                              01640000
                   WRITE FPV400HN-ENREG FROM FPV100-DSECT               01650000
                   ADD 1 TO W-FPV400HN-ECRITS                           01660000
           END-IF                                                       01670000
           PERFORM LECTURE-FPV400H.                                     01680000
       FIN-TRAIT-FPV400H. EXIT.                                         01690000
      **-------------------------------------------------------------** 01700000
      *                                                                 01710000
      **-------------------------------------------------------------** 01720000
      *                                                                 01730000
       LECTURE-FPV400H         SECTION.                                 01740000
           READ FPV400H INTO FPV100-DSECT AT END                        01750000
              SET  FIN-FPV400H       TO TRUE                            01760000
           END-READ.                                                    01770000
       FIN-LECTURE-FPV400H. EXIT.                                       01780000
      **-------------------------------------------------------------** 01790000
      *                                                                 01800000
      *                                                                 01810000
      *====>  C O M P T E  -  R E N D U.                                01820000
      *                                                                 01830000
       COMPTE-RENDU      SECTION.                                       01840000
                                                                        01850000
           DISPLAY  '**'.                                               01860000
           DISPLAY  '**           B P V 1 0 5 '.                        01870000
           DISPLAY  '**'.                                               01880000
           DISPLAY  '** ' W-FPV400H-LUS ' LUS DANS FICHIER FPV400H ;'   01890000
           DISPLAY  '**'.                                               01900000
           DISPLAY  '** ' W-FPV400HN-ECRITS ' ECRITS DANS FICHIER'      01910000
                        ' FPV400HN ;'.                                  01920000
       FIN-COMPTE-RENDU.        EXIT.                                   01930000
      **-------------------------------------------------------------** 01940000
      *                                                                 01950000
      **-------------------------------------------------------------** 01960000
       FERMETURE-FICHIERS    SECTION.                                   01970000
           CLOSE  FPV400H FPV400HN FMOIS.                               01980000
       FIN-FERMETURE-FICHIERS.    EXIT.                                 01990000
      **-------------------------------------------------------------** 02000000
      *                                                                 02010000
      **-------------------------------------------------------------** 02020000
       FIN-PROGRAMME        SECTION.                                    02030000
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP  RUN.                                                   02040000
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      **--------------------------------------------------------------**02050000
       FIN-ANORMALE    SECTION.                                         02060000
           PERFORM  FERMETURE-FICHIERS.                                 02070000
           MOVE  'BPV405'                  TO  ABEND-PROG.              02080000
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL  ABEND  USING  ABEND-PROG    ABEND-MESS.                02090000
      *--                                                                       
           CALL  MW-ABEND  USING  ABEND-PROG    ABEND-MESS.                     
      *}                                                                        
       FIN-FIN-ANORMALE.    EXIT.                                       02100000
