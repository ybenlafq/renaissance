      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BPV212.                                                     
       AUTHOR. DSA015.                                                          
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
      *                                                                         
      ***************************************************************           
      *  CREATION DU FICHIER DES MISES A JOUR                       *           
      *  DU FICHIER DES PRODUITS EN OFFRE ACTIVE EN MAGASIN         *           
      *                                                             *           
      *  DATE DE CREATION : SEPTEMBRE 1993 (CHRISTOPHE)             *           
      ***************************************************************           
      *                                                                         
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT    FDATE    ASSIGN  TO  FDATE.                               
      *--                                                                       
            SELECT    FDATE    ASSIGN  TO  FDATE                                
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT    FPV212   ASSIGN  TO  FPV212.                              
      *--                                                                       
            SELECT    FPV212   ASSIGN  TO  FPV212                               
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT    FPV220N  ASSIGN  TO  FPV220B.                             
      *--                                                                       
            SELECT    FPV220N  ASSIGN  TO  FPV220B                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT    FPV220A  ASSIGN  TO  FPV220O.                             
      *--                                                                       
            SELECT    FPV220A  ASSIGN  TO  FPV220O                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
        FD            FDATE                                                     
                      RECORDING F                                               
                      BLOCK 0 RECORDS                                           
                      LABEL RECORD STANDARD                                     
                      DATA RECORD DATE-PARM.                                    
       01  DATE-PARM.                                                           
           02 DATE-JMSA        PIC X(8).                                        
           02 FILLER           PIC X(72).                                       
       FD         FPV212                                                        
                  RECORDING F                                                   
                  BLOCK 0 RECORDS                                               
                  LABEL RECORD STANDARD.                                        
       01         FPV212-ENREG           PIC X(140).                            
       FD         FPV220A                                                       
                  RECORDING F                                                   
                  BLOCK 0 RECORDS                                               
                  LABEL RECORD STANDARD.                                        
       01 FPV220A-ENREG.                                                        
          03 FPV220A-NSOCIETE             PIC X(0003).                          
          03 FPV220A-NLIEU.                                                     
             05 FPV220A-NZONPRIX             PIC XX.                            
             05 FPV220A-BBTE                 PIC X.                             
          03 FPV220A-WSEQFAM              PIC X(0005).                          
          03 FPV220A-CODPROF              PIC X(0010).                          
          03 FPV220A-CMARKETING           PIC X(0005).                          
          03 FPV220A-CVMARKETING          PIC X(0005).                          
          03 FPV220A-PVENTE               PIC 9(0005).                          
          03 FPV220A-CMARQ                PIC X(0005).                          
          03 FPV220A-NCODIC               PIC X(0007).                          
          03 FPV220A-CFAM                 PIC X(0005).                          
NT        03 FPV220A-INTER                PIC X(0003).                          
          03 FPV220A-VARINTER             PIC X(0001).                          
          03 FPV220A-DATVARINTER          PIC X(0008).                          
          03 FPV220A-LSTATCOMP            PIC X(0003).                          
          03 FPV220A-LIBPROFIL            PIC X(0030).                          
          03 FPV220A-LMARKETING           PIC X(0020).                          
          03 FPV220A-LVMARKETING          PIC X(0020).                          
          03 FPV220A-LREFFOURN            PIC X(0020).                          
          03 FPV220A-VAR                  PIC X(0001).                          
          03 FPV220A-OFFRE                PIC X(0001).                          
          03 FPV220A-DATOFFRE             PIC X(0008).                          
NT        03 FPV220A-FILLER               PIC X(0012).                          
       FD         FPV220N                                                       
                  RECORDING F                                                   
                  BLOCK 0 RECORDS                                               
                  LABEL RECORD STANDARD.                                        
       01 FPV220N-ENREG.                                                        
          03 FPV220N-NSOCIETE             PIC X(0003).                          
          03 FPV220N-NLIEU.                                                     
             05 FPV220N-NZONPRIX          PIC XX.                               
             05 FPV220N-BBTE              PIC X.                                
          03 FPV220N-WSEQFAM              PIC X(0005).                          
          03 FPV220N-CODPROF              PIC X(0010).                          
          03 FPV220N-CMARKETING           PIC X(0005).                          
          03 FPV220N-CVMARKETING          PIC X(0005).                          
          03 FPV220N-PVENTE               PIC 9(0005).                          
          03 FPV220N-CMARQ                PIC X(0005).                          
          03 FPV220N-NCODIC               PIC X(0007).                          
          03 FPV220N-CFAM                 PIC X(0005).                          
NT        03 FPV220N-INTER                PIC X(0003).                          
          03 FPV220N-VARINTER             PIC X(0001).                          
          03 FPV220N-DATVARINTER          PIC X(0008).                          
          03 FPV220N-LSTATCOMP            PIC X(0003).                          
          03 FPV220N-LIBPROFIL            PIC X(0030).                          
          03 FPV220N-LMARKETING           PIC X(0020).                          
          03 FPV220N-LVMARKETING          PIC X(0020).                          
          03 FPV220N-LREFFOURN            PIC X(0020).                          
          03 FPV220N-VAR                  PIC X(0001).                          
          03 FPV220N-OFFRE                PIC X(0001).                          
          03 FPV220N-DATOFFRE             PIC X(0008).                          
NT        03 FPV220N-FILLER               PIC X(0012).                          
      *------------------------*                                                
       WORKING-STORAGE SECTION.                                                 
      *------------------------*                                                
       COPY FPV212.                                                             
       01 RUPTURES.                                                             
          02 RUPTUREA.                                                          
             03 RUPT-A-NSOCIETE          PIC X(3).                              
             03 RUPT-A-NLIEU             PIC X(3).                              
             03 RUPT-A-NCODIC            PIC X(7).                              
          02 RUPTUREN.                                                          
             03 RUPT-N-NSOCIETE          PIC X(3).                              
             03 RUPT-N-NLIEU             PIC X(3).                              
             03 RUPT-N-NCODIC            PIC X(7).                              
       01 LECTURE-FPV220A                PIC X VALUE '0'.                       
          88 NON-FIN-FPV220A                   VALUE '0'.                       
          88 FIN-FPV220A                       VALUE '1'.                       
       01 LECTURE-FPV220N                PIC X VALUE '0'.                       
          88 NON-FIN-FPV220N                   VALUE '0'.                       
          88 FIN-FPV220N                       VALUE '1'.                       
       77 CPT-FPV212                      PIC 9(8) VALUE 0.                     
       77 CPT-FPV220A                     PIC 9(8) VALUE 0.                     
       77 CPT-FPV220N                     PIC 9(8) VALUE 0.                     
      *                                                                         
      *************************************************                         
      * MODULE DE CONTROLE ET DE TRAITEMENT DES DATES *                         
      *************************************************                         
      *                                                                         
           COPY WORKDATC.                                                       
      *                                                                         
      **************************************************                        
      *  DESCRIPTION DES ZONES D'APPEL AU MODULE ABEND *                        
      **************************************************                        
      *                                                                         
           COPY ABENDCOP.                                                       
      *                                                                         
      ***************************************************************           
      *            P   R   O   C   E   D   U   R   E                *           
      ***************************************************************           
      *                                                                         
       PROCEDURE DIVISION.                                                      
           MOVE   'BPV212'     TO   ABEND-PROG.                                 
      ***************************************************************           
      *         T R A I T E M E N T     P R I N C I P A L           *           
      ***************************************************************           
           OPEN OUTPUT FPV212.                                                  
           OPEN INPUT  FPV220A FPV220N FDATE.                                   
           PERFORM LIRE-DATE-JOUR.                                              
           PERFORM LIRE-FPV220A.                                                
           PERFORM LIRE-FPV220N.                                                
           PERFORM UNTIL (FIN-FPV220A AND FIN-FPV220N)                          
              IF RUPTUREA = RUPTUREN                                            
                 IF FPV220N-VARINTER = SPACE                                    
                    MOVE SPACE            TO FPV220A-VARINTER                   
                 END-IF                                                         
                 IF FPV220N-DATVARINTER = SPACE                                 
                    MOVE SPACE            TO FPV220A-DATVARINTER                
                 END-IF                                                         
                 IF FPV220N-VAR         = SPACE                                 
                    MOVE SPACE            TO FPV220A-VAR                        
                 END-IF                                                         
                 IF FPV220N-DATOFFRE    = SPACE                                 
                    MOVE SPACE            TO FPV220A-DATOFFRE                   
                 END-IF                                                         
                 IF FPV220A-ENREG NOT = FPV220N-ENREG                           
                    MOVE 'M' TO FPV212-CMAJ                                     
                    PERFORM REMPLIR-A-PARTIR-FPV220N                            
                    PERFORM ECRIRE-FPV212                                       
                 END-IF                                                         
                 PERFORM LIRE-FPV220A                                           
                 PERFORM LIRE-FPV220N                                           
              ELSE                                                              
                 IF RUPTUREA > RUPTUREN                                         
                    MOVE 'C' TO FPV212-CMAJ                                     
                    PERFORM REMPLIR-A-PARTIR-FPV220N                            
                    PERFORM ECRIRE-FPV212                                       
                    PERFORM LIRE-FPV220N                                        
                 ELSE                                                           
                    MOVE 'S' TO FPV212-CMAJ                                     
                    PERFORM REMPLIR-A-PARTIR-FPV220A                            
                    PERFORM ECRIRE-FPV212                                       
                    PERFORM LIRE-FPV220A                                        
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
           PERFORM TERMINER.                                                    
           GOBACK.                                                              
      ***************************************************************           
      *                P A R A G R A P H E S                        *           
      ***************************************************************           
      ***************************************************************           
       LIRE-DATE-JOUR                              SECTION.                     
      ***************************************************************           
      *                                                                         
           READ FDATE AT END                                                    
              MOVE 'DATE PARAMETRE FDATE ABSENTE ' TO ABEND-MESS                
              CLOSE FDATE                                                       
              PERFORM ABEND-PROGRAMME.                                          
           MOVE  '1'         TO    GFDATA.                                      
           MOVE  DATE-JMSA   TO    GFJJMMSSAA.                                  
           CALL 'BETDATC'    USING WORK-BETDATC.                                
           IF GFVDAT = 0                                                        
              DISPLAY ' DATE PARAMETRE FPARAM ' DATE-JMSA                       
              MOVE 'DATE PARAMETRE NON COHERENTE ' TO ABEND-MESS                
              CLOSE FDATE                                                       
              PERFORM ABEND-PROGRAMME                                           
           END-IF.                                                              
           MOVE GFSAMJ-0     TO FPV212-DMAJ.                                    
           DISPLAY 'DATE PARAMETRE : ' FPV212-DMAJ.                             
           CLOSE FDATE.                                                         
      *                                                                         
       FIN-LIRE-DATE-JOUR.                         EXIT.                        
      ***************************************************************           
       LIRE-FPV220A                                SECTION.                     
      ***************************************************************           
      *                                                                         
           IF NON-FIN-FPV220A                                                   
              READ FPV220A AT END                                               
                  SET FIN-FPV220A TO TRUE.                                      
           IF NON-FIN-FPV220A                                                   
              ADD 1 TO CPT-FPV220A                                              
              MOVE FPV220A-NSOCIETE TO RUPT-A-NSOCIETE                          
              MOVE FPV220A-NLIEU    TO RUPT-A-NLIEU                             
              MOVE FPV220A-NCODIC   TO RUPT-A-NCODIC                            
           ELSE                                                                 
              MOVE HIGH-VALUE       TO RUPT-A-NSOCIETE                          
                                       RUPT-A-NLIEU                             
                                       RUPT-A-NCODIC                            
           END-IF.                                                              
      *                                                                         
       FIN-LIRE-FPV220A.                           EXIT.                        
      ***************************************************************           
       LIRE-FPV220N                                SECTION.                     
      ***************************************************************           
      *                                                                         
           IF NON-FIN-FPV220N                                                   
              READ FPV220N AT END                                               
                  SET FIN-FPV220N TO TRUE.                                      
           IF NON-FIN-FPV220N                                                   
              ADD 1 TO CPT-FPV220N                                              
              MOVE FPV220N-NSOCIETE TO RUPT-N-NSOCIETE                          
              MOVE FPV220N-NLIEU    TO RUPT-N-NLIEU                             
              MOVE FPV220N-NCODIC   TO RUPT-N-NCODIC                            
           ELSE                                                                 
              MOVE HIGH-VALUE       TO RUPT-N-NSOCIETE                          
                                       RUPT-N-NLIEU                             
                                       RUPT-N-NCODIC                            
           END-IF.                                                              
      *                                                                         
       FIN-LIRE-FPV220N.                           EXIT.                        
      ***************************************************************           
       REMPLIR-A-PARTIR-FPV220A                    SECTION.                     
      ***************************************************************           
      *                                                                         
           MOVE FPV220A-NSOCIETE          TO FPV212-NSOCIETE.                   
           MOVE FPV220A-NLIEU             TO FPV212-NLIEU.                      
           MOVE FPV220A-CODPROF           TO FPV212-CODPROF.                    
           MOVE FPV220A-CMARKETING        TO FPV212-CMARKETING.                 
           MOVE FPV220A-CVMARKETING       TO FPV212-CVMARKETING.                
           MOVE FPV220A-NCODIC            TO FPV212-NCODIC.                     
           MOVE FPV220A-INTER             TO FPV212-INTER.                      
           MOVE FPV220A-VARINTER          TO FPV212-VARINTER.                   
           MOVE FPV220A-DATVARINTER       TO FPV212-DATINTER.                   
           MOVE FPV220A-LSTATCOMP         TO FPV212-LSTATCOMP.                  
           MOVE FPV220A-LIBPROFIL         TO FPV212-LIBPROFIL.                  
           MOVE FPV220A-LMARKETING        TO FPV212-LMARKETING.                 
           MOVE FPV220A-LVMARKETING       TO FPV212-LVMARKETING.                
           MOVE FPV220A-VAR               TO FPV212-VAR.                        
           MOVE FPV220A-OFFRE             TO FPV212-OFFRE.                      
           MOVE FPV220A-DATOFFRE          TO FPV212-DATOFFRE.                   
           MOVE LOW-VALUE                 TO FPV212-FILLER.                     
      *                                                                         
       FIN-REMPLIR-A-PARTIR-FPV220A.               EXIT.                        
      ***************************************************************           
       REMPLIR-A-PARTIR-FPV220N                    SECTION.                     
      ***************************************************************           
      *                                                                         
           MOVE FPV220N-NSOCIETE          TO FPV212-NSOCIETE.                   
           MOVE FPV220N-NLIEU             TO FPV212-NLIEU.                      
           MOVE FPV220N-CODPROF           TO FPV212-CODPROF.                    
           MOVE FPV220N-CMARKETING        TO FPV212-CMARKETING.                 
           MOVE FPV220N-CVMARKETING       TO FPV212-CVMARKETING.                
           MOVE FPV220N-NCODIC            TO FPV212-NCODIC.                     
           MOVE FPV220N-INTER             TO FPV212-INTER.                      
           MOVE FPV220N-VARINTER          TO FPV212-VARINTER.                   
           MOVE FPV220N-DATVARINTER       TO FPV212-DATINTER.                   
           MOVE FPV220N-LSTATCOMP         TO FPV212-LSTATCOMP.                  
           MOVE FPV220N-LIBPROFIL         TO FPV212-LIBPROFIL.                  
           MOVE FPV220N-LMARKETING        TO FPV212-LMARKETING.                 
           MOVE FPV220N-LVMARKETING       TO FPV212-LVMARKETING.                
           MOVE FPV220N-VAR               TO FPV212-VAR.                        
           MOVE FPV220N-OFFRE             TO FPV212-OFFRE.                      
           MOVE FPV220N-DATOFFRE          TO FPV212-DATOFFRE.                   
           MOVE LOW-VALUE                 TO FPV212-FILLER.                     
      *                                                                         
       FIN-REMPLIR-A-PARTIR-FPV220N.               EXIT.                        
      ***************************************************************** 10480000
       ECRIRE-FPV212                               SECTION.             10490003
      ***************************************************************** 10500000
      *                                                                         
           ADD  1 TO CPT-FPV212.                                                
           WRITE FPV212-ENREG FROM DSECT-FPV212.                                
      *                                                                         
       FIN-ECRIRE-FPV212.                          EXIT.                11000003
      ***************************************************************** 10500000
      ***************************************************************** 10480000
      *                        T E R M I N E R                        * 10480000
      ***************************************************************** 10480000
       TERMINER                                    SECTION.                     
           CLOSE FPV212 FPV220A FPV220N.                                        
           DISPLAY '********************************************'.              
           DISPLAY '*      FIN NORMAL DU PROGRAMME BPV212      *'.              
           DISPLAY '********************************************'.              
           DISPLAY '* LECTURES  SUR L''ANCIEN   FICHIER :' CPT-FPV220A.         
           DISPLAY '* LECTURES  SUR LE NOUVEAU FICHIER :'  CPT-FPV220N.         
           DISPLAY '* ECRITURES SUR LE FICHIER FPV212  :'  CPT-FPV212.          
           DISPLAY '********************************************'.              
       FIN-TERMINER.                               EXIT.                        
      ***************************************************************** 10480000
       ABEND-PROGRAMME                             SECTION.                     
      *                                                                         
           CLOSE FPV212 FPV220A FPV220N.                                        
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                        
