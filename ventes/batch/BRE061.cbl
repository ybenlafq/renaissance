      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BRE061.                                                     
       AUTHOR. JG.                                                              
       ENVIRONMENT DIVISION.                                                    
      ******************************************************************        
      *                                                                         
      *  ETAT DES REMISES (EN LIVRE)                                            
      *  PHASE 2: MATCHING DES FICHIERS D'UNLOAD (TRIES PAR SOC/LIEU/VT)        
      *           DE RTGV10 ET RTGV11 (POUR AVOIR LES VENTES EXPORT)            
      *           EST CREE LE FICHIER FRE061 (=GV11 + GV10.WEXPORT)             
      *                                                                         
      *  --------------------------------------------------------------         
      *  - MAINTENANCES -                                                       
      *  ---------------------------------------------------------------        
      *  AL2307 : NOUVELLE ALIMENTATION, TRAITEMENT VENTES B&S.                 
      *           AJOUT MPRIMCLI POUR PRESTATION DE CALCUL.                     
      *           AJOUT FILLER POUR ANTICIPATION ENRICHISSEMENT.                
      *  ---------------------------------------------------------------        
      *                                                                         
      *                                                                         
      ******************************************************************        
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT RTGV10 ASSIGN TO RTGV10.                                     
      *--                                                                       
            SELECT RTGV10 ASSIGN TO RTGV10                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT RTGV11 ASSIGN TO RTGV11.                                     
      *--                                                                       
            SELECT RTGV11 ASSIGN TO RTGV11                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FRE061 ASSIGN TO FRE061.                                     
      *--                                                                       
            SELECT FRE061 ASSIGN TO FRE061                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  RTGV10 RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  MW-FILLER PIC X(11).                                                 
       FD  RTGV11 RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
      *MW DAR-2                                                                 
       01  MW-FILLER PIC X(94).                                                 
       FD  FRE061 RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  ENR-FRE061 PIC X(110).                                               
       WORKING-STORAGE SECTION.                                                 
      ***************************************************************           
      * ZONE IN POUR RTGV11, FLAG DE FIN                                        
      ***************************************************************           
       01  RVGV1100.                                                            
  1        02  GV11-NLIEU     PIC X(0003).                                      
  4        02  GV11-NVENTE    PIC X(0007).                                      
 11        02  GV11-NLIGNE    PIC X(0002).                                      
 13        02  GV11-NCODIC    PIC X(0007).                                      
 20        02  GV11-CVENDEUR  PIC X(0006).                                      
 26        02  GV11-CTYPENREG PIC X(0001).                                      
 27        02  GV11-CENREG    PIC X(0005).                                      
 32        02  GV11-QVENDUE   PIC S9(5) COMP-3.                                 
 35        02  GV11-PVUNIT    PIC S9(7)V9(0002) COMP-3.                         
 40        02  GV11-PVUNITF   PIC S9(7)V9(0002) COMP-3.                         
 45        02  GV11-PVTOTAL   PIC S9(7)V9(0002) COMP-3.                         
 50        02  GV11-CMODDEL   PIC X(0003).                                      
 53        02  GV11-DCREATION PIC X(0008).                                      
 61        02  GV11-NCODICGRP PIC X(0007).                                      
 68        02  GV11-DDELIV    PIC X(0008).                                      
 76        02  GV11-DCOMPTA   PIC X(0008).                                      
AL2207                                                                          
 84        02  GV11-MPRIMCLI  PIC S9(7)V9(0002) COMP-3.                         
 84        02  GV11-NSEQREF   PIC S9(5)         COMP-3.                         
 84        02  GV11-NSEQENS   PIC S9(5)         COMP-3.                         
       01  CLE-FICHIER-RTGV11 REDEFINES RVGV1100 PIC X(10).                     
       01  FILLER PIC X VALUE SPACE.                                            
           88  FIN-RTGV11 VALUE 'F'.                                            
       01  CPT-RTGV11         PIC S9(7) PACKED-DECIMAL VALUE ZERO.              
      ***************************************************************           
      * ZONE IN POUR RTGV10                                                     
      ***************************************************************           
       01  RVGV1000.                                                            
  1        02  GV10-NLIEU     PIC X(0003).                                      
  4        02  GV10-NVENTE    PIC X(0007).                                      
 11        02  GV10-WEXPORT   PIC X(0001).                                      
       01  CLE-FICHIER-RTGV10 REDEFINES RVGV1000 PIC X(10).                     
       01  CPT-RTGV10         PIC S9(7) PACKED-DECIMAL VALUE ZERO.              
      ***************************************************************           
      * ZONE OUT POUR FRE061                                                    
      ***************************************************************           
       01  W-FRE061.                                                            
  1        02  RE61-NCODIC    PIC X(0007).                                      
  8        02  RE61-NLIEU     PIC X(0003).                                      
 11        02  RE61-NVENTE    PIC X(0007).                                      
 18        02  RE61-CVENDEUR  PIC X(0006).                                      
 24        02  RE61-CTYPENREG PIC X(0001).                                      
 25        02  RE61-CENREG    PIC X(0005).                                      
 30        02  RE61-QVENDUE   PIC S9(5) COMP-3.                                 
 33        02  RE61-PVUNIT    PIC S9(7)V9(0002) COMP-3.                         
 38        02  RE61-PVUNITF   PIC S9(7)V9(0002) COMP-3.                         
 43        02  RE61-PVTOTAL   PIC S9(7)V9(0002) COMP-3.                         
 48        02  RE61-CMODDEL   PIC X(0003).                                      
 51        02  RE61-DCREATION PIC X(0008).                                      
 59        02  RE61-NCODICGRP PIC X(0007).                                      
 66        02  RE61-DDELIV    PIC X(0008).                                      
 74        02  RE61-DCOMPTA   PIC X(0008).                                      
 82        02  RE61-WEXPORT   PIC X(0001).                                      
 83        02  RE61-PDIFF     PIC S9(7)V9(0002) COMP-3.                         
 83        02  RE61-NSEQREF   PIC S9(5)         COMP-3.                         
 83        02  RE61-NSEQENS   PIC S9(5)         COMP-3.                         
           02  FILLER         PIC X(17).                                        
       01  CPT-FRE061         PIC S9(7) PACKED-DECIMAL VALUE ZERO.              
      ***************************************************************           
      * OUORQUE                                                                 
      ***************************************************************           
       01  PIC-EDIT PIC ZZZ.ZZZ.ZZ9.                                            
      ***************************************************************           
      * COMMS AVEC LES SOUS PROGS                                               
      ***************************************************************           
           COPY ABENDCOP.                                                       
      ***************************************************************           
      * PROC:                                                                   
      * - MATCHING DES DEUX FICHIERS                                            
      *   (POUR RECUPERER GV10-WEXPORT)                                         
      ***************************************************************           
       PROCEDURE DIVISION.                                                      
           DISPLAY 'BRE061: Base Stats en Livr� 2/3'.                           
           DISPLAY '        Matching GV10/GV11'.                                
           OPEN INPUT RTGV10 RTGV11 OUTPUT FRE061.                              
           MOVE LOW-VALUE TO CLE-FICHIER-RTGV10.                                
           PERFORM LECTURE-RTGV11.                                              
           PERFORM UNTIL FIN-RTGV11                                             
              PERFORM LECTURE-RTGV10                                            
                UNTIL CLE-FICHIER-RTGV10 >= CLE-FICHIER-RTGV11                  
              PERFORM ECRITURE-FRE061                                           
              PERFORM LECTURE-RTGV11                                            
           END-PERFORM.                                                         
      ******************************************************************        
      * ZIENDE                                                                  
      ******************************************************************        
           CLOSE RTGV10 RTGV11 FRE061.                                          
           MOVE CPT-RTGV10 TO PIC-EDIT.                                         
           DISPLAY '        Nombre de RTGV10 lus : ' PIC-EDIT.                  
           MOVE CPT-RTGV11 TO PIC-EDIT.                                         
           DISPLAY '        Nombre de RTGV11 lus : ' PIC-EDIT.                  
           MOVE CPT-FRE061 TO PIC-EDIT.                                         
           DISPLAY '        Nombre de FRE061 �crits : ' PIC-EDIT.               
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      ******************************************************************        
      *  LECTURE DE GV10                                                        
      ******************************************************************        
       LECTURE-RTGV10 SECTION.                                                  
           READ RTGV10 INTO RVGV1000                                            
             AT END MOVE HIGH-VALUE TO CLE-FICHIER-RTGV10                       
             NOT AT END ADD 1 TO CPT-RTGV10                                     
           END-READ.                                                            
      ******************************************************************        
      *  LECTURE DE GV11                                                        
      ******************************************************************        
       LECTURE-RTGV11 SECTION.                                                  
           READ RTGV11 INTO RVGV1100                                            
             AT END SET FIN-RTGV11 TO TRUE                                      
             NOT AT END ADD 1 TO CPT-RTGV11                                     
           END-READ.                                                            
      ******************************************************************        
      * ECRITURE DE FRE061 (=GV11+GV10-WEXPORT)                                 
      *    (LA CLE CHANGE, NCODIC EST MIS EN TETE)                              
      ******************************************************************        
       ECRITURE-FRE061 SECTION.                                                 
           MOVE GV11-NLIEU     TO RE61-NLIEU                                    
           MOVE GV11-NVENTE    TO RE61-NVENTE                                   
      * SI REPRISE DU MEME CODIC, ON TOUCHE PAS A LA DATE DE CREATION           
      * Impossible a faire car GS40 n'a pas la date du mvt d'origine,           
      * GV11 ne pourrait pas �tre rapproch� de GS40. Mis en comment.            
      *    IF (GV11-CTYPENREG = '1' AND GV11-CENREG = ' ')                      
      *       IF NOT (    (GV11-CMODDEL(1:1) = 'R')                             
      *               AND (GV11-NCODIC = RE61-NCODIC)                           
      *               AND (GV11-NCODICGRP = RE61-NCODICGRP))                    
      *          MOVE GV11-DCREATION TO RE61-DCREATION                          
      *       END-IF                                                            
      *    ELSE                                                                 
              MOVE GV11-DCREATION TO RE61-DCREATION                             
      *    END-IF.                                                              
      * SI RACHAT PSE, ON TOUCHE PAS AU CODIC                                   
           IF NOT ((GV11-CTYPENREG = '1') AND (GV11-CENREG > ' '))              
              MOVE GV11-NCODIC    TO RE61-NCODIC                                
              MOVE GV11-NCODICGRP TO RE61-NCODICGRP                             
           END-IF.                                                              
           MOVE GV11-CVENDEUR  TO RE61-CVENDEUR                                 
           MOVE GV11-CTYPENREG TO RE61-CTYPENREG                                
           MOVE GV11-CENREG    TO RE61-CENREG                                   
           MOVE GV11-QVENDUE   TO RE61-QVENDUE                                  
           MOVE GV11-PVUNIT    TO RE61-PVUNIT                                   
           MOVE GV11-PVUNITF   TO RE61-PVUNITF                                  
           MOVE GV11-PVTOTAL   TO RE61-PVTOTAL                                  
           MOVE GV11-CMODDEL   TO RE61-CMODDEL                                  
           MOVE GV11-DDELIV    TO RE61-DDELIV                                   
           MOVE GV11-DCOMPTA   TO RE61-DCOMPTA                                  
AL2307     MOVE GV11-MPRIMCLI  TO RE61-PDIFF                                    
AL2307     MOVE GV11-NSEQREF   TO RE61-NSEQREF                                  
AL2307     MOVE GV11-NSEQENS   TO RE61-NSEQENS                                  
           IF CLE-FICHIER-RTGV10 = CLE-FICHIER-RTGV11                           
              MOVE GV10-WEXPORT TO RE61-WEXPORT                                 
           ELSE                                                                 
              DISPLAY 'GV10 NON TROUVE ' CLE-FICHIER-RTGV11                     
              MOVE ' '          TO RE61-WEXPORT                                 
           END-IF.                                                              
           WRITE ENR-FRE061 FROM W-FRE061.                                      
           ADD 1 TO CPT-FRE061.                                                 
      ******************************************************************        
      * SCHPLOUF                                                                
      ******************************************************************        
       ABEND-PROGRAMME SECTION.                                                 
           MOVE  'BRE061'     TO   ABEND-PROG.                                  
           CALL  'ABEND'   USING   ABEND-PROG  ABEND-MESS.                      
