      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.    BRD050.                                                   
       AUTHOR.        DSA016.                                                   
       ENVIRONMENT    DIVISION.                                                 
       CONFIGURATION  SECTION.                                                  
      *****************************************************************         
      *        R E D E V A N C E  A U D I O V I S U E L               *         
      *        ETAT DES CODIC NONDECLARABLES DE FAMILLES DECLARABLE   *         
      *****************************************************************         
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           C01           IS SAUT.                                               
       INPUT-OUTPUT   SECTION.                                                  
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRD050 ASSIGN TO FRD050.                                      
      *--                                                                       
           SELECT FRD050 ASSIGN TO FRD050                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRD051 ASSIGN TO FRD051.                                      
      *--                                                                       
           SELECT FRD051 ASSIGN TO FRD051                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRD052 ASSIGN TO FRD052.                                      
      *--                                                                       
           SELECT FRD052 ASSIGN TO FRD052                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE  ASSIGN TO FDATE.                                       
      *--                                                                       
           SELECT FDATE  ASSIGN TO FDATE                                        
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IRD050 ASSIGN TO IRD050 .                                     
      *--                                                                       
           SELECT IRD050 ASSIGN TO IRD050                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IRD051 ASSIGN TO IRD051 .                                     
      *--                                                                       
           SELECT IRD051 ASSIGN TO IRD051                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IRD052 ASSIGN TO IRD052 .                                     
      *--                                                                       
           SELECT IRD052 ASSIGN TO IRD052                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *----------------------------------------------------------------         
      *     DEFINITION DU FICHIER DATE                                          
      *-----------------------------------------------------------------        
       FD   FDATE                                                               
            RECORDING F                                                         
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD                                               
            DATA RECORD DATE-PARM.                                              
       01   DATE-PARM.                                                          
            05  JJ                      PIC XX.                                 
            05  MM                      PIC XX.                                 
            05  SS                      PIC XX.                                 
            05  AA                      PIC XX.                                 
            05  FILLER                  PIC X(72).                              
      *----------------------------------------------------------------         
      *     DEFINITION DU FICHIER DES DECLARATIONS A EDITER                     
      * CE FICHIER DOIT ETRE TRIE SUR SOC/LIEU/DATE/CFAMILLE                    
      *               ET CUMUL DES QTE SUR CES CRITERES DE TRI                  
      *-----------------------------------------------------------------        
       FD   FRD050                                                              
            RECORDING F                                                         
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD                                               
            DATA RECORD ENREG-FRD050.                                           
       01  ENREG-FRD050  PIC X(75).                                             
      *-----------------------------------------------------------------        
      * CE FICHIER DOIT ETRE TRIE SUR SOCGRP/DATE/CFAMILLE                      
      *    ET CUMUL DES QTE SUR CES CRITERES DE TRI                             
      *-----------------------------------------------------------------        
       FD   FRD051                                                              
            RECORDING F                                                         
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD                                               
            DATA RECORD ENREG-FRD051.                                           
       01  ENREG-FRD051  PIC X(75).                                             
      *-----------------------------------------------------------------        
      * CE FICHIER DOIT ETRE TRIE SUR DATE/CFAMILLE                             
      *     ET CUMUL DES QTE SUR CES CRITERES DE TRI                            
      *-----------------------------------------------------------------        
       FD   FRD052                                                              
            RECORDING F                                                         
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD                                               
            DATA RECORD ENREG-FRD052.                                           
       01  ENREG-FRD052  PIC X(75).                                             
      *----------------------------------------------------------------         
      *     DEFINITION DU FICHIER  D IMPRESSION                                 
      * SAUT DE PAGE POUR SOC/MAG  ET DATE SSAAMM                               
      *                                                                         
      *-----------------------------------------------------------------        
       FD   IRD050                                                              
            RECORDING F                                                         
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD.                                              
       01   LIGNE0.                                                             
            05  W-SAUT0         PIC X(01).                                      
            05  W-LIGNE-00      PIC X(132).                                     
      *-----------------------------------------------------------------        
       FD   IRD051                                                              
            RECORDING F                                                         
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD.                                              
       01   LIGNE1.                                                             
      *     05  W-SAUT1         PIC X(01).                                      
            05  W-LIGNE-01      PIC X(132).                                     
      *-----------------------------------------------------------------        
       FD   IRD052                                                              
            RECORDING F                                                         
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD.                                              
       01   LIGNE2.                                                             
            05  W-SAUT2         PIC X(01).                                      
            05  W-LIGNE-02      PIC X(132).                                     
      *                                                                         
      *****************************************************************         
      *     W O R K I N G  S T O R A G E                                        
      *****************************************************************         
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
      *                                                                         
      * DESCRIPTION DE FRD050 FRD051 FRD052                                     
       01  ENRE-FRD050.                                                         
           03  RD050-NSOCGRP                PIC X(03).                          
           03  RD050-NSOCIETE               PIC X(03).                          
           03  RD050-NLIEU                  PIC X(03).                          
           03  RD050-DATE-MVT.                                                  
               05   RD050-DATE-MVT-SS       PIC X(02).                          
               05   RD050-DATE-MVT-AA       PIC X(02).                          
               05   RD050-DATE-MVT-MM       PIC X(02).                          
           03  RD050-CFAM                   PIC X(05).                          
           03  RD050-CMARQ                  PIC X(05).                          
           03  RD050-NCODIC                 PIC X(07).                          
           03  RD050-LREFFOURN              PIC X(20).                          
           03  RD050-QTE-VTE                PIC S9(11) COMP-3.                  
           03  RD050-NVENTE                 PIC X(07).                          
           03  RD050-FILLER                 PIC X(10).                          
      *                                                                         
       01  LIGNE                        PIC X(132).                             
      *                                                                         
           COPY WORKDATC.                                                       
      *    COPY SYKWDIV0.                                                       
      *    EXEC SQL INCLUDE SQLCA    END-EXEC.                                  
      *    EXEC SQL INCLUDE RVGA1005 END-EXEC.                                  
      *                                                                         
      *----------------------------------------------------------------         
      *     DEFINITION DES ZONES SPECIFIQUES AUX PROGRAMMES                     
      *-----------------------------------------------------------------        
       01   CTR-LIGNES          PIC 9(05)  VALUE 0.                             
       01   CTR-PAGE            PIC 9(05)  VALUE 0.                             
      *-----------------------------------------------------------------        
       01   W-CPT-LU0           PIC 9(05)  VALUE 0.                             
       01   W-CPT-FAUX0         PIC 9(05)  VALUE 0.                             
       01   Z-CPT-LU0           PIC ZZZZ9.                                      
       01   Z-CPT-FAUX0         PIC ZZZZ9.                                      
      *                                                                         
       01   W-CPT-LU1           PIC 9(05)  VALUE 0.                             
       01   W-CPT-FAUX1         PIC 9(05)  VALUE 0.                             
       01   Z-CPT-LU1           PIC ZZZZ9.                                      
       01   Z-CPT-FAUX1         PIC ZZZZ9.                                      
       01   W-CPT-LU2           PIC 9(05)  VALUE 0.                             
       01   W-CPT-FAUX2         PIC 9(05)  VALUE 0.                             
       01   Z-CPT-LU2           PIC ZZZZ9.                                      
       01   Z-CPT-FAUX2         PIC ZZZZ9.                                      
      *----------------------------------------------------------------         
       01   CODE-FICHIER        PIC X                VALUE ' '.                 
            88  OUI-FIN-FICHIER                      VALUE '1'.                 
            88  NON-FIN-FICHIER                      VALUE '2'.                 
      *----------------------------------------------------------------         
       01   DAT-JMAS.                                                           
                10  D1-JJ      PIC XX.                                          
                10  D1-MM      PIC XX.                                          
                10  D1-SS      PIC XX .                                         
                10  D1-AA      PIC XX.                                          
       01   DAT-JMA.                                                            
                10  D2-JJ      PIC XX.                                          
                10  FILLER     PIC X     VALUE '/'.                             
                10  D2-MM      PIC XX.                                          
                10  FILLER     PIC X     VALUE '/'.                             
                10  D2-AA      PIC XX.                                          
       01  W-RUPT.                                                              
           03  W-RUPT1.                                                         
               09  W-NSOCGRP    PIC XXX.                                        
               09  W-NSOCIETE   PIC XXX.                                        
               09  W-NLIEU      PIC XXX.                                        
           03  W-RUPT2.                                                         
               09  W-MOIS       PIC XX.                                         
               09  W-AN         PIC XX.                                         
       01  WA-RUPT.                                                             
           03  WA-RUPT1.                                                        
               09  WA-NSOCGRP    PIC XXX.                                       
               09  WA-NSOCIETE   PIC XXX.                                       
               09  WA-NLIEU      PIC XXX.                                       
           03  WA-RUPT2.                                                        
               09  WA-MOIS       PIC XX.                                        
               09  WA-AN         PIC XX.                                        
       01  W-LSOCIETE           PIC X(30).                                      
       01  W-LLIEU              PIC X(30).                                      
       01  W-QTE.                                                               
           03  W-QTE-VTE           PIC S9(11) COMP-3.                           
           03  W-QTE-NONDECVTE     PIC S9(11) COMP-3.                           
           03  W-QTE-NONDECREP     PIC S9(11) COMP-3.                           
      *01  TL-MOIS.                                                             
      *  02  T-L-MOIS.                                                          
      *    03 FILLER PIC X(09) VALUE 'JANVIER  '.                               
      *    03 FILLER PIC X(09) VALUE 'FEVRIER  '.                               
      *    03 FILLER PIC X(09) VALUE 'M A R S  '.                               
      *    03 FILLER PIC X(09) VALUE 'A V R I L'.                               
      *    03 FILLER PIC X(09) VALUE 'M A I    '.                               
      *    03 FILLER PIC X(09) VALUE 'J U I N  '.                               
      *    03 FILLER PIC X(09) VALUE 'JUILLET  '.                               
      *    03 FILLER PIC X(09) VALUE 'A O U T  '.                               
      *    03 FILLER PIC X(09) VALUE 'SEPTEMBRE'.                               
      *    03 FILLER PIC X(09) VALUE 'OCTOBRE  '.                               
      *    03 FILLER PIC X(09) VALUE 'NOVEMBRE '.                               
      *    03 FILLER PIC X(09) VALUE 'DECEMBRE '.                               
      *  02  T-MOIS REDEFINES T-L-MOIS.                                         
      *    03  T-LMOIS OCCURS 12.                                               
      *        05  LMOIS        PIC X(09).                                      
      *01  IND-MOIS       PIC 99.                                               
       01  W-LMOIS        PIC X(09).                                            
      *                                                                         
       01  I              PIC 999.                                              
      *01  T-LIEU.                                                              
      *    03  T-LLIEU OCCURS 200.                                              
      *        05  CLIEU.                                                       
      *        07  E-CSOC       PIC X(3).                                       
      *        07  E-CMAG       PIC X(3).                                       
      *        05  E-LLIEU      PIC X(30).                                      
      *                                                                         
      *01  RESP-SQL    PIC 9.                                                   
      *88  RESP-OK   VALUE 1.                                                   
      *88  RESP-NOK  VALUE 0.                                                   
      *                                                                         
       01  TYPE-LIEU   PIC 9.                                                   
       88  TYPE-NUL  VALUE 0.                                                   
       88  TYPE-GRP  VALUE 1.                                                   
       88  TYPE-SOC  VALUE 2.                                                   
       88  TYPE-MAG  VALUE 3.                                                   
      *----------------------------------------------------------------         
      *     LIGNE EDITION                                                       
      *----------------------------------------------------------------         
      *****************************************************************         
       01 LIGNE-01.                                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 L01-EDIT PIC  X(06).                                             
            04 FILLER PIC X(33) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     E T A B L I S S'.                   
            04 FILLER PIC X(20) VALUE ' E M E N T S  D A R '.                   
            04 FILLER PIC X(20) VALUE 'T Y                 '.                   
            04 FILLER PIC X(14) VALUE SPACES.                                   
            04 FILLER PIC X(17) VALUE SPACES.                                   
      *****************************************************************         
       01 LIGNE-02.                                                             
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     ---------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '---                 '.                   
            04 FILLER PIC X(14) VALUE SPACES.                                   
            04 FILLER PIC X(07) VALUE 'PAGE : '.                                
            04 ED-PAGE PIC ZZ9.                                           .     
            04 FILLER PIC X(07) VALUE SPACES.                                   
      *****************************************************************         
       01 LIGNE-03.                                                             
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     !    EDITION DE'.                   
            04 FILLER PIC X(20) VALUE ' LISTE DE CONTROLE  '.                   
            04 FILLER PIC X(20) VALUE '  !                 '.                   
            04 FILLER PIC X(14) VALUE SPACES.                                   
            04 FILLER PIC X(09) VALUE 'EDITE LE '.                              
            04 ED-JOUR  PIC X(8).                                               
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
       01 LIGNE-04.                                                             
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     !DECLARATION RE'.                   
            04 FILLER PIC X(20) VALUE 'DEVANCE AUDIOVISUELL'.                   
            04 FILLER PIC X(20) VALUE 'E !                '.                    
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(11) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
       01 LIGNE-05.                                                             
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     !       TRAITEM'.                   
            04 FILLER PIC X(6) VALUE 'ENT DU'.                                  
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-DTRAIT    PIC X(8).                                           
            04 FILLER PIC X(18) VALUE '       !         '.                      
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(18) VALUE SPACES.                                   
      *****************************************************************         
       01 LIGNE-06.                                                             
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     ---------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '---                 '.                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(11) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
       01 LIGNE-07.                                                             
          02 FILLER PIC X(03) VALUE SPACES.                                     
          02 FILLER PIC X(30) VALUE '------------------------------'.           
          02 FILLER PIC X(30) VALUE '------------------------------'.           
          02 FILLER PIC X(30) VALUE '------------------------'.                 
          02 FILLER PIC X(39) VALUE SPACE.                                      
       01 LIGNE-08A.                                                            
          02 FILLER PIC X(03) VALUE SPACES.                                     
          02 FILLER PIC X(30) VALUE '!         !         !         '.           
          02 FILLER PIC X(30) VALUE '             !        !       '.           
          02 FILLER PIC X(30) VALUE '     ! VENTE INTROUVEE !'.                 
          02 FILLER PIC X(39) VALUE SPACE.                                      
       01 LIGNE-08.                                                             
          02 FILLER PIC X(03) VALUE SPACES.                                     
          02 FILLER PIC X(30) VALUE '! FAMILLE !  CODIC  !   R E F '.           
          02 FILLER PIC X(30) VALUE 'E R E N C E  ! MARQUE ! QUANTI'.           
          02 FILLER PIC X(30) VALUE 'TE   !  ABS ! N� GS040 !'.                 
          02 FILLER PIC X(39) VALUE SPACE.                                      
       01 LIGNE-11.                                                             
          02 FILLER PIC X(03) VALUE SPACES.                                     
          02 FILLER PIC X(30) VALUE '!         !         !         '.           
          02 FILLER PIC X(30) VALUE '             !        !       '.           
          02 FILLER PIC X(30) VALUE '     !      !          !'.                 
          02 FILLER PIC X(39) VALUE SPACE.                                      
       01 LIGNE-12.                                                             
      *   02 FILLER PIC X(03) VALUE SPACES.                                     
      *   02 FILLER PIC X(30) VALUE '!  XXXXX  ! XXXXXXX ! XXXXXXXX'.           
      *   02 FILLER PIC X(30) VALUE 'XXXXXXXXXXXX ! XXXXX  ! ZZZZZZ'.           
      *   02 FILLER PIC X(30) VALUE 'ZZZ9 !   X  !  XXXXXXX !'.                 
      *   02 FILLER PIC X(39) VALUE SPACE.                                      
          02 FILLER   PIC X(03) VALUE SPACES.                                   
          02 FILLER   PIC X(03) VALUE  '!  '.                                   
          02 L12-CFAM PIC X(05).                                                
          02 FILLER   PIC X(04) VALUE  '  ! '.                                  
          02 L12-NCODIC PIC X(07).                                              
          02 FILLER   PIC X(03) VALUE  ' ! '.                                   
          02 L12-LREFFOURN PIC X(20).                                           
          02 FILLER   PIC X(03) VALUE  ' ! '.                                   
          02 L12-CMARQ     PIC X(05).                                           
          02 FILLER   PIC X(03) VALUE  '  !'.                                   
          02 L12-QTE-VTE PIC -(10)9.                                            
          02 FILLER   PIC X(05) VALUE  ' !   '.                                 
          02 L12-ABS     PIC X.                                                 
          02 FILLER   PIC X(05) VALUE  '  !  '.                                 
          02 L12-NVENTE  PIC X(07).                                             
          02 FILLER   PIC X(02) VALUE  ' !'.                                    
          02 FILLER   PIC X(20) VALUE SPACES.                                   
       01 LIGNE-13.                                                             
          02 FILLER   PIC X(03) VALUE SPACES.                                   
          02 FILLER   PIC X(17) VALUE ' S O C I E T E : '.                      
          02 L13-SOC  PIC X(03).                                                
          02 FILLER   PIC X(03) VALUE SPACES.                                   
          02 L13-LSOC PIC X(30).                                                
          02 FILLER PIC X(76) VALUE SPACE.                                      
       01 LIGNE-14.                                                             
          02 FILLER   PIC X(03) VALUE SPACES.                                   
          02 FILLER   PIC X(19) VALUE ' LIEU DE VENTE   : '.                    
          02 L14-SOC  PIC X(03).                                                
          02 FILLER   PIC X(02) VALUE SPACES.                                   
          02 L14-MAG  PIC X(03).                                                
          02 FILLER   PIC X(03) VALUE SPACES.                                   
          02 L14-LMAG PIC X(30).                                                
          02 FILLER PIC X(69) VALUE SPACE.                                      
       01 LIGNE-15.                                                             
          02 FILLER   PIC X(03) VALUE SPACES.                                   
          02 FILLER   PIC X(28) VALUE ' G R O U P E   D A R T Y  : '.           
          02 L15-SOC  PIC X(03).                                                
          02 FILLER   PIC X(03) VALUE SPACES.                                   
          02 L15-LSOC PIC X(30).                                                
          02 FILLER PIC X(65) VALUE SPACE.                                      
       01 LIGNE-16.                                                             
          02 FILLER PIC X(03) VALUE SPACES.                                     
          02 FILLER PIC X(30) VALUE '!   T O T A L                 '.           
          02 FILLER PIC X(23) VALUE '                      !'.                  
          02 L16-QTE-VTE PIC -(10)9.                                            
          02 FILLER PIC X(30) VALUE ' !      !          !'.                     
          02 FILLER PIC X(10) VALUE SPACE.                                      
      *                                                                 03000000
       01 LIGNE-16A.                                                            
          02 FILLER PIC X(03) VALUE SPACES.                                     
          02 FILLER PIC X(30) VALUE '!   TOTAL VENTES NON TROUVEES '.           
          02 FILLER PIC X(15) VALUE '----> VENTES : '.                          
          02 L16-QTE-NONDECVTE PIC -(10)9.                                      
          02 FILLER PIC X(15) VALUE '    REPRISES : '.                          
          02 L16-QTE-NONDECREP PIC -(10)9.                                      
          02 FILLER PIC X(02) VALUE ' !'.                                       
          02 FILLER PIC X(10) VALUE SPACE.                                      
      *                                                                 03000000
       01 LIGNE-17.                                                             
          02 FILLER   PIC X(03) VALUE SPACES.                                   
          02 FILLER   PIC X(21) VALUE ' MOIS  DECLARATION : '.                  
          02 L17-LM   PIC X(07).                                                
          02 FILLER   PIC X(100) VALUE SPACES.                                  
      *                                                                 03000000
      *                                                                 03000000
      **************************************************                03010000
      *  DESCRIPTION DES ZONES D'APPEL AU MODULE ABEND *                03020000
      **************************************************                03030000
           COPY ABENDCOP.                                               03040000
                                                                        03050000
      *****************************************************************         
      *     P R O C E D U R E  D I V I S I O N                                  
      *****************************************************************         
      *-----------------------------------------------------------------        
      *     TRAME DU PROGRAMME BRD050                                           
      *-----------------------------------------------------------------        
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
       MODULE-BRD050 SECTION.                                                   
      *                                                                         
           PERFORM DEBUT-BRD050.                                                
           PERFORM TRAITEMENT-BRD050.                                           
           PERFORM FIN-BRD050.                                                  
      *                                                                         
       F-MODULE-BRD050. EXIT.                                                   
      *                                                                         
      *-----------------------------------------------------------------        
      *           DEBUT DU BRD010                                               
      *-----------------------------------------------------------------        
      *                                                                         
       DEBUT-BRD050 SECTION.                                                    
      *                                                                         
           OPEN INPUT  FRD050.                                                  
           OPEN INPUT  FRD051.                                                  
           OPEN INPUT  FRD052.                                                  
           OPEN INPUT  FDATE.                                                   
           OPEN OUTPUT IRD050.                                                  
           OPEN OUTPUT IRD051.                                                  
           OPEN OUTPUT IRD052.                                                  
      *                                                                         
           MOVE 0 TO W-CPT-LU0    W-CPT-LU1   W-CPT-LU2.                        
           MOVE 0 TO W-CPT-FAUX0  W-CPT-FAUX1 W-CPT-FAUX2.                      
      *                                                                         
           READ FDATE AT END                                                    
                MOVE 'DATE PARAMETRE ABSENTE '      TO ABEND-MESS               
                PERFORM ABEND-PROGRAMME.                                        
           MOVE  DATE-PARM       TO DAT-JMAS.                                   
           MOVE  D1-JJ           TO D2-JJ.                                      
           MOVE  D1-MM           TO D2-MM.                                      
           MOVE  D1-AA           TO D2-AA.                                      
           MOVE DAT-JMA          TO ED-JOUR.                                    
           MOVE DAT-JMA          TO ED-DTRAIT.                                  
      *                                                                         
      *    PERFORM RECHERCHE-TLIEU.                                             
      *                                                                         
       F-DEBUT-BRD050. EXIT.                                                    
      *                                                                         
      *-----------------------------------------------------------------        
      *           TRAITEMENT DU BRD050                                          
      *-----------------------------------------------------------------        
      *                                                                         
       TRAITEMENT-BRD050 SECTION.                                               
      *                                                                         
           MOVE 3 TO TYPE-LIEU.                                                 
           PERFORM UNTIL TYPE-NUL                                               
                IF TYPE-MAG                                                     
                                   MOVE 'IRD050' TO L01-EDIT                    
                ELSE IF TYPE-SOC                                                
                                   MOVE 'IRD051' TO L01-EDIT                    
                     ELSE IF TYPE-GRP                                           
                                   MOVE 'IRD052' TO L01-EDIT                    
                          END-IF                                                
                     END-IF                                                     
                END-IF                                                          
                SET NON-FIN-FICHIER TO TRUE                                     
                PERFORM LECTURE-EDRED                                           
                MOVE SPACE TO W-RUPT WA-RUPT                                    
                MOVE 0     TO CTR-PAGE                                          
                INITIALIZE W-QTE                                                
                PERFORM UNTIL   OUI-FIN-FICHIER                                 
                IF TYPE-MAG                                                     
                        ADD 1   TO W-CPT-LU0                                    
                ELSE IF TYPE-SOC                                                
                        ADD 1   TO W-CPT-LU1                                    
                     ELSE IF TYPE-GRP                                           
                        ADD 1   TO W-CPT-LU2                                    
                          END-IF                                                
                     END-IF                                                     
                END-IF                                                          
                        PERFORM TEST-SECTION                                    
                        PERFORM LECTURE-EDRED                                   
                END-PERFORM                                                     
                PERFORM EDITION-FIN                                             
                SUBTRACT 1 FROM TYPE-LIEU                                       
           END-PERFORM.                                                         
      *                                                                         
       F-TRAITEMENT-BRD050. EXIT.                                               
      *                                                                         
      *-----------------------------------------------------------------        
      *           LECTURE DU EDRED                                              
      *-----------------------------------------------------------------        
      *                                                                         
       LECTURE-EDRED  SECTION.                                                  
                IF TYPE-MAG                                                     
                            READ FRD050 INTO ENRE-FRD050                        
                            AT END SET OUI-FIN-FICHIER TO TRUE                  
                ELSE IF TYPE-SOC                                                
                            READ FRD051 INTO ENRE-FRD050                        
                            AT END SET OUI-FIN-FICHIER TO TRUE                  
                     ELSE IF TYPE-GRP                                           
                            READ FRD052 INTO ENRE-FRD050                        
                            AT END SET OUI-FIN-FICHIER TO TRUE                  
                          END-IF                                                
                     END-IF                                                     
                END-IF.                                                         
       F-LECTURE-EDRED. EXIT.                                                   
      *                                                                         
      *-----------------------------------------------------------------        
      *           TEST SUR LA SECTION                                           
      *-----------------------------------------------------------------        
      *                                                                         
       TEST-SECTION      SECTION.                                               
                IF TYPE-MAG                                                     
                        MOVE RD050-NLIEU       TO WA-NLIEU                      
                        MOVE RD050-NSOCIETE    TO WA-NSOCIETE                   
                        MOVE RD050-NSOCGRP     TO WA-NSOCGRP                    
                        MOVE RD050-DATE-MVT-MM TO WA-MOIS                       
                        MOVE RD050-DATE-MVT-SS TO WA-AN                         
                ELSE IF TYPE-SOC                                                
                        MOVE RD050-NSOCGRP     TO WA-NSOCGRP                    
                        MOVE RD050-DATE-MVT-MM TO WA-MOIS                       
                        MOVE RD050-DATE-MVT-SS TO WA-AN                         
                     ELSE IF TYPE-GRP                                           
                        MOVE RD050-DATE-MVT-MM TO WA-MOIS                       
                        MOVE RD050-DATE-MVT-SS TO WA-AN                         
                          END-IF                                                
                     END-IF                                                     
                END-IF.                                                         
                IF TYPE-MAG                                                     
                        ADD 1   TO W-CPT-FAUX0                                  
                ELSE IF TYPE-SOC                                                
                        ADD 1   TO W-CPT-FAUX1                                  
                     ELSE IF TYPE-GRP                                           
                        ADD 1   TO W-CPT-FAUX2                                  
                          END-IF                                                
                     END-IF                                                     
                END-IF.                                                         
           PERFORM EDITION-MVT.                                                 
       F-TEST-SECTION. EXIT.                                                    
      *                                                                         
      *-----------------------------------------------------------------        
      *           IMPRESSION DES MOUVEMENTS                                     
      *-----------------------------------------------------------------        
      *                                                                         
       EDITION-MVT SECTION.                                                     
      *                                                                         
           IF CTR-LIGNES > 56 OR   W-RUPT NOT = WA-RUPT                         
              IF CTR-PAGE > 0   PERFORM EDITION-FIN                             
              END-IF                                                            
              PERFORM EDITION-ENT                                               
           END-IF.                                                              
           IF RD050-CFAM NOT = SPACE                                            
                       PERFORM EDITION-DETAIL                                   
           END-IF.                                                              
                IF TYPE-MAG                                                     
                        MOVE RD050-NSOCIETE    TO W-NSOCIETE                    
                        MOVE RD050-NSOCGRP     TO W-NSOCGRP                     
                        MOVE RD050-NLIEU       TO W-NLIEU                       
                        MOVE RD050-DATE-MVT-MM TO W-MOIS                        
                        MOVE RD050-DATE-MVT-SS TO W-AN                          
                ELSE IF TYPE-SOC                                                
                        MOVE RD050-NSOCGRP     TO W-NSOCGRP                     
                        MOVE RD050-DATE-MVT-MM TO W-MOIS                        
                        MOVE RD050-DATE-MVT-SS TO W-AN                          
                     ELSE IF TYPE-GRP                                           
                        MOVE RD050-DATE-MVT-MM TO W-MOIS                        
                        MOVE RD050-DATE-MVT-SS TO W-AN                          
                          END-IF                                                
                     END-IF                                                     
                END-IF.                                                         
       F-EDITION-MVT. EXIT.                                                     
      *                                                                         
      *-----------------------------------------------------------------        
      *           EDITION DE LA FERMETURE DU TABLEAU                            
      *-----------------------------------------------------------------        
      *                                                                         
       EDITION-FIN SECTION.                                                     
      *                                                                         
      *    MOVE 0           TO   CTR-LIGNES.                                    
           IF  W-RUPT NOT = WA-RUPT OR OUI-FIN-FICHIER                          
           MOVE W-QTE-VTE        TO  L16-QTE-VTE                                
           MOVE W-QTE-NONDECVTE  TO  L16-QTE-NONDECVTE                          
           MOVE W-QTE-NONDECREP  TO  L16-QTE-NONDECREP                          
           MOVE  LIGNE-07  TO LIGNE PERFORM WL-EDIT                             
           MOVE  LIGNE-16  TO LIGNE PERFORM WL-EDIT                             
           MOVE  LIGNE-16A TO LIGNE PERFORM WL-EDIT                             
           INITIALIZE W-QTE                                                     
           END-IF.                                                              
           MOVE  LIGNE-07  TO LIGNE PERFORM WL-EDIT.                            
      *                                                                         
       F-EDITION-FIN. EXIT.                                                     
      *                                                                         
      *-----------------------------------------------------------------        
      *           EDITION DE L ENTETE DU TABLEAU                                
      *-----------------------------------------------------------------        
      *                                                                         
       EDITION-ENT SECTION.                                                     
      *                                                                         
           MOVE 0           TO   CTR-LIGNES.                                    
      *                                                                         
           IF  W-RUPT1 NOT = WA-RUPT1                                           
                              PERFORM RECHERCHE-LIEU                            
           END-IF.                                                              
           IF  W-RUPT2 NOT = WA-RUPT2                                           
                              PERFORM RECHERCHE-MOIS                            
           END-IF.                                                              
           ADD +1               TO CTR-PAGE.                                    
           MOVE CTR-PAGE        TO ED-PAGE.                                     
      *                                                                         
           MOVE  SPACE     TO LIGNE PERFORM WP-EDIT.                            
           MOVE  LIGNE-01  TO LIGNE PERFORM WL-EDIT.                            
           MOVE  LIGNE-02  TO LIGNE PERFORM WL-EDIT.                            
           MOVE  LIGNE-03  TO LIGNE PERFORM WL-EDIT.                            
           MOVE  LIGNE-04  TO LIGNE PERFORM WL-EDIT.                            
           MOVE  LIGNE-05  TO LIGNE PERFORM WL-EDIT.                            
           MOVE  LIGNE-06  TO LIGNE PERFORM WL-EDIT.                            
           MOVE  LIGNE-17  TO LIGNE PERFORM WL-EDIT.                            
           IF TYPE-MAG                                                          
                     MOVE  LIGNE-14  TO LIGNE PERFORM WL-EDIT                   
           ELSE IF TYPE-SOC                                                     
                     MOVE  LIGNE-13  TO LIGNE PERFORM WL-EDIT                   
                ELSE IF TYPE-GRP                                                
                     MOVE  LIGNE-15  TO LIGNE PERFORM WL-EDIT                   
                     END-IF                                                     
                END-IF                                                          
           END-IF.                                                              
           MOVE  LIGNE-07  TO LIGNE PERFORM WL-EDIT.                            
           MOVE  LIGNE-08A TO LIGNE PERFORM WL-EDIT.                            
           MOVE  LIGNE-08  TO LIGNE PERFORM WL-EDIT.                            
           MOVE  LIGNE-07  TO LIGNE PERFORM WL-EDIT.                            
           MOVE  LIGNE-11  TO LIGNE PERFORM WL-EDIT.                            
      *                                                                         
           ADD +14               TO CTR-LIGNES.                                 
      *                                                                         
       F-EDITION-ENT. EXIT.                                                     
      *-----------------------------------------------------------------        
       WP-EDIT        SECTION.                                                  
      *                                                                         
           IF TYPE-MAG                                                          
                     WRITE LIGNE0 FROM  LIGNE AFTER ADVANCING SAUT              
                                              MOVE SPACE TO LIGNE               
           ELSE IF TYPE-SOC                                                     
                     WRITE LIGNE1 FROM  LIGNE AFTER ADVANCING SAUT              
                                              MOVE SPACE TO LIGNE               
                ELSE IF TYPE-GRP                                                
                     WRITE LIGNE2 FROM  LIGNE AFTER ADVANCING SAUT              
                                              MOVE SPACE TO LIGNE               
                     END-IF                                                     
                END-IF                                                          
           END-IF.                                                              
       F-WP-EDIT. EXIT.                                                         
      *                                                                         
       WL-EDIT        SECTION.                                                  
      *                                                                         
           IF TYPE-MAG                                                          
                     WRITE LIGNE0 FROM  LIGNE  MOVE SPACE TO LIGNE              
           ELSE IF TYPE-SOC                                                     
                     WRITE LIGNE1 FROM  LIGNE  MOVE SPACE TO LIGNE              
                ELSE IF TYPE-GRP                                                
                     WRITE LIGNE2 FROM  LIGNE  MOVE SPACE TO LIGNE              
                     END-IF                                                     
                END-IF                                                          
           END-IF.                                                              
       F-WL-EDIT. EXIT.                                                         
      *                                                                         
      *-----------------------------------------------------------------        
      *           EDITION D UNE LIGNE DE DETAIL                                 
      *-----------------------------------------------------------------        
      *                                                                         
       EDITION-DETAIL SECTION.                                                  
      *                                                                         
           INITIALIZE   LIGNE-12.                                               
           MOVE RD050-CFAM           TO  L12-CFAM.                              
           MOVE RD050-CMARQ          TO  L12-CMARQ.                             
           MOVE RD050-NCODIC         TO  L12-NCODIC.                            
           MOVE RD050-LREFFOURN      TO  L12-LREFFOURN.                         
           MOVE RD050-QTE-VTE        TO  L12-QTE-VTE.                           
           IF RD050-NVENTE NOT = SPACE                                          
                  MOVE RD050-NVENTE  TO  L12-NVENTE                             
                  MOVE 'X'           TO  L12-ABS                                
                  IF   RD050-QTE-VTE > 0                                        
                       ADD  RD050-QTE-VTE TO  W-QTE-NONDECVTE                   
                  ELSE                                                          
                       ADD  RD050-QTE-VTE TO  W-QTE-NONDECREP                   
                  END-IF                                                        
           ELSE                                                                 
                  MOVE SPACE         TO  L12-NVENTE                             
                                         L12-ABS                                
           END-IF.                                                              
      *                                                                         
           ADD  RD050-QTE-VTE       TO  W-QTE-VTE.                              
      *                                                                         
           MOVE  LIGNE-12  TO LIGNE PERFORM WL-EDIT.                            
      *                                                                         
           ADD +1               TO CTR-LIGNES.                                  
      *                                                                         
       F-EDITION-MVT. EXIT.                                                     
      *                                                                         
       RECHERCHE-MOIS SECTION.                                                  
             STRING     RD050-DATE-MVT-MM '/'                                   
                        RD050-DATE-MVT-SS                                       
                        RD050-DATE-MVT-AA                                       
             DELIMITED SIZE INTO W-LMOIS.                                       
           MOVE W-LMOIS              TO  L17-LM.                                
       F-RECHERCHE-MOIS.   EXIT.                                                
      *                                                                         
       RECHERCHE-LIEU SECTION.                                                  
      *      PERFORM VARYING I FROM 1 BY 1 UNTIL  CLIEU(I) = WA-RUPT1           
      *                                 OR I > 200                              
      *      END-PERFORM.                                                       
      *      MOVE SPACE                 TO L14-LMAG.                            
      *      IF I < 200                                                         
      *                 MOVE E-LLIEU(I) TO L14-LMAG                             
      *      END-IF.                                                            
             MOVE SPACE                 TO L14-LMAG.                            
             MOVE SPACE                 TO L13-LSOC.                            
             MOVE WA-NSOCGRP            TO L13-SOC.                             
             MOVE WA-NSOCIETE           TO L14-SOC.                             
             MOVE WA-NLIEU              TO L14-MAG.                             
       F-RECHERCHE-LIEU.   EXIT.                                                
      *                                                                         
      *----------------------- LECTURE  TABLE LIEU                              
      *----------------------- RECHERCHE LIEU MAG MGC                           
      *RECHERCHE-TLIEU    SECTION.                                              
      *                                                                         
      *    EXEC SQL DECLARE  CURS01 CURSOR FOR                                  
      *             SELECT                                                      
      *                   NSOCIETE,                                             
      *                   NLIEU,                                                
      *                   LLIEU                                                 
      *             FROM  RVGA1005                                              
      *             WHERE                                                       
      *                   CTYPSOC = 'MGI'                                       
      *             OR    CTYPSOC = 'SOC'                                       
      *             OR    CTYPLIEU = '3'                                        
      *             ORDER BY  NSOCIETE, NLIEU                                   
      *             END-EXEC.                                                   
      *                                                                         
      *    PERFORM TEST-CODE-RETOUR-SQL.                                        
      *-------------                                                            
      *                                                                         
      *    EXEC SQL OPEN   CURS01                                               
      *             END-EXEC.                                                   
      *    PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
      *-------------                                                            
      *                                                                         
      *    MOVE 0 TO I.                                                         
      *    MOVE 1 TO RESP-SQL.                                                  
      *    PERFORM UNTIL RESP-NOK                                               
      *                                                                         
      *    EXEC SQL FETCH   CURS01                                              
      *             INTO                                                        
      *                  :GA10-NSOCIETE,                                        
      *                  :GA10-NLIEU,                                           
      *                  :GA10-LLIEU                                            
      *             END-EXEC                                                    
      *                                                                         
      *    PERFORM TEST-CODE-RETOUR-SQL                                         
      *                                                                         
      *-------------------                                                      
      *                                                                         
      *    POUR CODE-RETOUR = '1' ET SQL-CODE = +100                            
      *    IF NON-TROUVE                                                        
      *         MOVE 0 TO RESP-SQL                                              
      *    ELSE                                                                 
      *       ADD  1 TO I                                                       
      *       MOVE GA10-NSOCIETE  TO E-CSOC(I)                                  
      *       MOVE GA10-NLIEU     TO E-CMAG(I)                                  
      *       MOVE GA10-LLIEU     TO E-LLIEU(I)                                 
      *    END-IF                                                               
      * FIN DU FETCH                                                            
      *    END-PERFORM.                                                         
      *                                                                         
      *    EXEC SQL CLOSE   CURS01                                              
      *             END-EXEC.                                                   
      *F-RECHERCHE-TLIEU. EXIT.                                                 
      *                                                                         
      *TEST-CODE-RETOUR-SQL   SECTION.                                          
      *    MOVE 0 TO CODE-RETOUR.                                               
      *    EVALUATE SQLCODE                                                     
      *       WHEN  0   SET TROUVE      TO TRUE                                 
      *       WHEN  100 SET NON-TROUVE  TO TRUE                                 
      *       WHEN -803 SET EXISTE-DEJA TO TRUE                                 
      *       WHEN OTHER                                                        
      *            MOVE 'PB DB2'      TO ABEND-MESS                             
      *            MOVE SQLCODE       TO ABEND-CODE                             
      *            GO TO ABEND-PROGRAMME                                        
      *       END-EVALUATE.                                                     
      *F-TEST-CODE-RETOUR-SQL. EXIT.                                            
      *----------------------------------------------------------------         
      *-----------------------------------------------------------------        
      *           FIN DU BRD010                                                 
      *-----------------------------------------------------------------        
      *                                                                         
       FIN-BRD050 SECTION.                                                      
      *                                                                         
           PERFORM FERMETURE-FICHIER.                                           
           PERFORM FIN-PROGRAMME.                                               
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       F-FIN-BRD050. EXIT.                                                      
      *                                                                         
      *-----------------------------------------------------------------        
      *           FERMETURE DES FICHIERS                                        
      *-----------------------------------------------------------------        
      *                                                                         
       FERMETURE-FICHIER    SECTION.                                            
           CLOSE IRD050.                                                        
           CLOSE IRD051.                                                        
           CLOSE IRD052.                                                        
           CLOSE FRD050.                                                        
           CLOSE FRD051.                                                        
           CLOSE FRD052.                                                        
           CLOSE FDATE.                                                         
       FIN-FERMETURE-FICHIER. EXIT.                                             
      *                                                                         
       FIN-PROGRAMME        SECTION.                                            
           MOVE W-CPT-LU0   TO   Z-CPT-LU0.                                     
           MOVE W-CPT-FAUX0 TO   Z-CPT-FAUX0.                                   
           MOVE W-CPT-LU1   TO   Z-CPT-LU1.                                     
           MOVE W-CPT-FAUX1 TO   Z-CPT-FAUX1.                                   
           MOVE W-CPT-LU2   TO   Z-CPT-LU2.                                     
           MOVE W-CPT-FAUX2 TO   Z-CPT-FAUX2.                                   
           DISPLAY ' '.                                                         
           DISPLAY '***************************************'.                   
           DISPLAY '* PROGRAMME BRD050     : COMPTE RENDU * '.                  
           DISPLAY '***************************************'.                   
           DISPLAY '* ENREG FRD050    LUS ...: ' Z-CPT-LU0 '        *'          
           DISPLAY '* ENREG FRD050    EDITES : ' Z-CPT-FAUX0 '      *'          
           DISPLAY '* ENREG FRD051    LUS ...: ' Z-CPT-LU1 '        *'          
           DISPLAY '* ENREG FRD051    EDITES : ' Z-CPT-FAUX1 '      *'          
           DISPLAY '* ENREG FRD052    LUS ...: ' Z-CPT-LU2 '        *'          
           DISPLAY '* ENREG FRD052    EDITES : ' Z-CPT-FAUX2 '      *'          
           DISPLAY '***************************************'.                   
       F-FIN-PROGRAMME. EXIT.                                                   
      *                                                                         
       ABEND-PROGRAMME       SECTION.                                   14420000
           CLOSE IRD050.                                                        
           CLOSE IRD051.                                                        
           CLOSE IRD052.                                                        
           CLOSE FRD050.                                                        
           CLOSE FRD051.                                                        
           CLOSE FRD052.                                                        
           CLOSE FDATE.                                                         
           MOVE 'BRD050' TO ABEND-PROG.                                         
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                14430000
       FIN-ABEND-PROGRAMME.       EXIT.                                 14420000
      *                                                                 14400000
                                                                        14410000
