      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. MECNM.                                                       
       AUTHOR. JG.                                                              
      ******************************************************************        
      *  Mise en format d'�dition d'une zone num�rique                          
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       DATA DIVISION.                                                           
      *****************************************************************         
      *  OUORQUE                                                                
      *****************************************************************         
       WORKING-STORAGE SECTION.                                                 
      ** Zones de r�ception d�cimale                                            
       01  DEC-18    PIC S9(18) PACKED-DECIMAL.                                 
       01  DEC-CHAR  REDEFINES DEC-18   PIC X(10).                              
      ** Zone de r�ception �tendu                                               
       01  PIC-18    PIC S9(18).                                                
       01  PIC-17V1  REDEFINES PIC-18   PIC S9(17)V9(1).                        
       01  PIC-16V2  REDEFINES PIC-18   PIC S9(16)V9(2).                        
       01  PIC-15V3  REDEFINES PIC-18   PIC S9(15)V9(3).                        
       01  PIC-14V4  REDEFINES PIC-18   PIC S9(14)V9(4).                        
       01  PIC-13V5  REDEFINES PIC-18   PIC S9(13)V9(5).                        
       01  PIC-12V6  REDEFINES PIC-18   PIC S9(12)V9(6).                        
       01  PIC-CHAR  REDEFINES PIC-18   PIC X(18).                              
      ** Zone d'Edition                                                         
       01  EDT-CHAR  PIC X(20).                                                 
       01  EDT-18    REDEFINES EDT-CHAR PIC -(18)9.                             
       01  EDT-17V1  REDEFINES EDT-CHAR PIC -(17)9,9(1).                        
       01  EDT-16V2  REDEFINES EDT-CHAR PIC -(16)9,9(2).                        
       01  EDT-15V3  REDEFINES EDT-CHAR PIC -(15)9,9(3).                        
       01  EDT-14V4  REDEFINES EDT-CHAR PIC -(14)9,9(4).                        
       01  EDT-13V5  REDEFINES EDT-CHAR PIC -(13)9,9(5).                        
       01  EDT-12V6  REDEFINES EDT-CHAR PIC -(12)9,9(6).                        
      ** Position: ouorque                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-POS                 PIC S9(4) COMP.                                
      *--                                                                       
       01  W-POS                 PIC S9(4) COMP-5.                              
      *}                                                                        
      *****************************************************************         
      *  LINKAGE                                                                
      *****************************************************************         
       LINKAGE SECTION.                                                         
           COPY MECNMC.                                                         
      *****************************************************************         
      *  PROCEDURE DIVISION                                                     
      *  - Contr�le des param�tres d'appel                                      
      *  - Mise en forme de la valeur num�rique                                 
      *****************************************************************         
       PROCEDURE DIVISION USING Z-COMM-MECNM.                                   
           PERFORM CONTROLE-PARAMETRES.                                         
           IF MECNM-OK                                                          
              PERFORM MISE-EN-FORME-VALEUR                                      
           END-IF.                                                              
           GOBACK.                                                              
      *****************************************************************         
      *  Contr�le des param�tres d'appel                                        
      *****************************************************************         
       CONTROLE-PARAMETRES SECTION.                                             
           SET MECNM-OK      TO TRUE.                                           
      **> Num�rique ou Pack�                                                    
           IF MECNM-NUM-PACK NOT = 'N' AND 'P'                                  
              SET MECNM-KO TO TRUE                                              
           END-IF.                                                              
      **> Nombre de D�cimales                                                   
           IF MECNM-NBDEC NOT NUMERIC                                           
              SET MECNM-KO TO TRUE                                              
           ELSE                                                                 
              IF MECNM-NBDEC < 0 OR > 6                                         
                 SET MECNM-KO TO TRUE                                           
              END-IF                                                            
           END-IF.                                                              
      **> Longueur de la Zone                                                   
           IF MECNM-LONG-IN  NOT NUMERIC                                        
              SET MECNM-KO TO TRUE                                              
           ELSE                                                                 
              IF MECNM-LONG-IN < 0 OR > 18                                      
                 SET MECNM-KO TO TRUE                                           
              END-IF                                                            
           END-IF.                                                              
      *****************************************************************         
      *  Mise en forme de la valeur num�rique                                   
      *****************************************************************         
       MISE-EN-FORME-VALEUR SECTION.                                            
           IF MECNM-PACKE                                                       
              COMPUTE MECNM-LONG-FIC ROUNDED = (MECNM-LONG-IN + 1) / 2          
              COMPUTE W-POS = 11 - MECNM-LONG-FIC                               
              MOVE 0 TO DEC-18                                                  
              MOVE MECNM-INPUT TO DEC-CHAR(W-POS:)                              
              MOVE DEC-18 TO PIC-18                                             
           ELSE                                                                 
              COMPUTE MECNM-LONG-FIC = MECNM-LONG-IN                            
              COMPUTE W-POS = 19 - MECNM-LONG-FIC                               
              MOVE 0 TO PIC-18                                                  
              MOVE MECNM-INPUT TO PIC-CHAR(W-POS:)                              
           END-IF.                                                              
           MOVE SPACES TO EDT-CHAR.                                             
           EVALUATE MECNM-NBDEC                                                 
              WHEN 0 MOVE      PIC-18   TO   EDT-18                             
              WHEN 1 MOVE      PIC-17V1 TO   EDT-17V1                           
              WHEN 2 MOVE      PIC-16V2 TO   EDT-16V2                           
              WHEN 3 MOVE      PIC-15V3 TO   EDT-15V3                           
              WHEN 4 MOVE      PIC-14V4 TO   EDT-14V4                           
              WHEN 5 MOVE      PIC-13V5 TO   EDT-13V5                           
              WHEN 6 MOVE      PIC-12V6 TO   EDT-12V6                           
           END-EVALUATE.                                                        
           PERFORM VARYING W-POS FROM 1 BY 1                                    
             UNTIL EDT-CHAR(W-POS:1) > ' '                                      
           END-PERFORM.                                                         
           MOVE EDT-CHAR(W-POS:) TO MECNM-OUTPUT.                               
           PERFORM VARYING MECNM-LONG-OUT FROM 20 BY -1                         
             UNTIL MECNM-OUTPUT(MECNM-LONG-OUT:) > ' '                          
           END-PERFORM.                                                         
