      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.    BRD010.                                                   
       AUTHOR.        DSA016.                                                   
       ENVIRONMENT    DIVISION.                                                 
       CONFIGURATION  SECTION.                                                  
      *****************************************************************         
      *        R E D E V A N C E  A U D I O V I S U E L  SEPT(89)     *         
      *****************************************************************         
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           C01           IS SAUT.                                               
       INPUT-OUTPUT   SECTION.                                                  
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRD006 ASSIGN TO FRD006.                                      
      *--                                                                       
           SELECT FRD006 ASSIGN TO FRD006                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE  ASSIGN TO FDATE.                                       
      *--                                                                       
           SELECT FDATE  ASSIGN TO FDATE                                        
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IRD010 ASSIGN TO IRD010 .                                     
      *--                                                                       
           SELECT IRD010 ASSIGN TO IRD010                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *----------------------------------------------------------------         
      *     DEFINITION DU FICHIER DATE                                          
      *-----------------------------------------------------------------        
       FD   FDATE                                                               
            RECORDING F                                                         
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD                                               
            DATA RECORD DATE-PARM.                                              
       01   DATE-PARM.                                                          
            05  JJ                      PIC XX.                                 
            05  MM                      PIC XX.                                 
            05  MM                      PIC XX.                                 
            05  AA                      PIC XX.                                 
            05  FILLER                  PIC X(72).                              
      *----------------------------------------------------------------         
      *     DEFINITION DU FICHIER EDITION DES ANOMALIES                         
      *-----------------------------------------------------------------        
       FD   FRD006                                                              
            RECORDING F                                                         
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD                                               
            DATA RECORD ENRE-FRD006.                                            
       01  ENRE-FRD006.                                                 01840000
           05  RE-DTRAIT                    PIC X(8).                   01850000
           05  RE-DDELIV                    PIC X(8).                   01910000
           05  RE-NMAG                      PIC X(3).                   01970000
           05  RE-NVENTE                    PIC X(7).                   01980000
           05  RE-NOM                       PIC X(25).                  01990000
           05  RE-PRENOM                    PIC X(15).                  02000000
           05  RE-TELDOM                    PIC X(10).                  02010000
           05  RE-TELBUR                    PIC X(10).                  02020000
           05  RE-POSTEBUR                  PIC X(5).                   02030000
           05  RE-FAM                       PIC X(5).                   02040000
           05  RE-MARQ                      PIC X(5).                   02050000
           05  RE-NDECLARATION              PIC 9(11).                  02060000
           05  RE-ANNUL                     PIC X.                      02070000
           05  RE-VOIE.                                                         
               10 RE-NVOIE                  PIC X(5).                           
               10 RE-TVOIE                  PIC X(15).                          
               10 RE-NOMVOIE                PIC X(21).                          
           05  RE-COMMUNE                   PIC X(32).                          
           05  RE-CPOSTAL                   PIC 9(5).                           
           05  RE-DEPT-NAISS                PIC X(02).                          
           05  RE-COMMUNE-NAISS             PIC X(30).                          
           05  RE-D-NAISS                   PIC X(08).                          
      *----------------------------------------------------------------         
      *     DEFINITION DU FICHIER  D IMPRESSION                                 
      *-----------------------------------------------------------------        
       FD   IRD010                                                              
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD.                                              
       01   LIGNE1.                                                             
      *     05  W-SAUT          PIC X(01).                                      
            05  W-LIGNE-01      PIC X(132).                                     
      *                                                                         
      *****************************************************************         
      *     W O R K I N G  S T O R A G E                                        
      *****************************************************************         
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
      *----------------------------------------------------------------         
      *     DEFINITION DES ZONES SPECIFIQUES AUX PROGRAMMES                     
      *-----------------------------------------------------------------        
       01   CTR-LIGNES          PIC 9(05)  VALUE 0.                             
       01   CTR-PAGE            PIC 9(05)  VALUE 0.                             
       01   W-CPT-ANNUL         PIC 9(05)  VALUE 0.                             
       01   W-CPT-NON-ANN       PIC 9(05)  VALUE 0.                             
      *-----------------------------------------------------------------        
       01   W-CPT-LU            PIC 9(05)  VALUE 0.                             
       01   W-CPT-FAUX          PIC 9(05)  VALUE 0.                             
       01   Z-CPT-LU            PIC ZZZZ9.                                      
       01   Z-CPT-FAUX          PIC ZZZZ9.                                      
      *----------------------------------------------------------------         
       01   CODE-FICHIER        PIC X                VALUE ' '.                 
            88  FIN-FICHIER                          VALUE '1'.                 
            88  NON-FIN-FICHIER                      VALUE '2'.                 
      *----------------------------------------------------------------         
       01   DAT-JMAS.                                                           
                10  D1-JJ      PIC XX.                                          
                10  D1-MM      PIC XX.                                          
                10  D1-SS      PIC XX .                                         
                10  D1-AA      PIC XX.                                          
       01   DAT-JMA.                                                            
                10  D2-JJ      PIC XX.                                          
                10  FILLER     PIC X     VALUE '/'.                             
                10  D2-MM      PIC XX.                                          
                10  FILLER     PIC X     VALUE '/'.                             
                10  D2-AA      PIC XX.                                          
       01  W-MAG               PIC XXX.                                         
      * TABLEAU CONCATENATION                                                   
       01  TA.                                                                  
           02  A               PIC X   OCCURS 85.                               
       01   I                  PIC 9(02)  VALUE 0.                              
       01   T                  PIC 9(02)  VALUE 0.                              
       01   J                  PIC 9(02)  VALUE 0.                              
       01   B                  PIC 9(02)  VALUE 0.                              
      *----------------------------------------------------------------         
      *     LIGNE EDITION                                                       
      *----------------------------------------------------------------         
       01  IRD010-DSECT.                                                        
      *****************************************************************         
          02 LIGNE-01.                                                          
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     E T A B L I S S'.                   
            04 FILLER PIC X(20) VALUE ' E M E N T S  D A R '.                   
            04 FILLER PIC X(20) VALUE 'T Y                 '.                   
            04 FILLER PIC X(14) VALUE SPACES.                                   
            04 FILLER PIC X(06) VALUE 'IRD010'.                                 
            04 FILLER PIC X(11) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE-02.                                                          
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     ---------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '---                 '.                   
            04 FILLER PIC X(14) VALUE SPACES.                                   
            04 FILLER PIC X(07) VALUE 'PAGE : '.                                
            04 ED-PAGE PIC ZZ9.                                           .     
            04 FILLER PIC X(07) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE-03.                                                          
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     !  EDITION DE L'.                   
            04 FILLER PIC X(20) VALUE 'ISTE DE CONTROLE    '.                   
            04 FILLER PIC X(20) VALUE '   !                '.                   
            04 FILLER PIC X(14) VALUE SPACES.                                   
            04 FILLER PIC X(09) VALUE 'EDITE LE '.                              
            04 ED-JOUR  PIC X(8).                                               
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE-04.                                                          
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     !DECLARATION RE'.                   
            04 FILLER PIC X(20) VALUE 'DEVANCE AUDIOVISUEL '.                   
            04 FILLER PIC X(20) VALUE '   !                '.                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(11) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE-05.                                                          
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     !     TRAITEMEN'.                   
            04 FILLER PIC X(4) VALUE 'T DU'.                                    
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-DTRAIT    PIC X(8).                                           
            04 FILLER PIC X(20) VALUE '          !         '.                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(18) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE-06.                                                          
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '     ---------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '---                 '.                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(11) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE-07.                                                          
            04 FILLER PIC X(30) VALUE '------------------------------'.         
            04 FILLER PIC X(30) VALUE '------------------------------'.         
            04 FILLER PIC X(30) VALUE '------------------------------'.         
            04 FILLER PIC X(30) VALUE '------------------------------'.         
            04 FILLER PIC X(11) VALUE '-----------'.                            
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE-08.                                                          
            04 FILLER PIC X(02) VALUE '! '.                                     
            04 FILLER PIC X(09) VALUE 'D.DELIV  '.                              
            04 FILLER PIC X(11) VALUE 'NMAG/NVENTE'.                            
            04 FILLER PIC X(01) VALUE SPACE.                                    
            04 FILLER PIC X(14) VALUE 'NOM           '.                         
            04 FILLER PIC X(16) VALUE '    PRENOM    '.                         
            04 FILLER PIC X(11) VALUE ' TEL.DOM.  '.                            
            04 FILLER PIC X(11) VALUE 'TEL.BUREAU '.                            
            04 FILLER PIC X(06) VALUE SPACES.                                   
            04 FILLER PIC X(11) VALUE ' FAM  MQUE '.                            
            04 FILLER PIC X(16) VALUE 'N�DECLARATION  A'.                       
            04 FILLER PIC X(12) VALUE '          ! '.                           
      *****************************************************************         
          02 LIGNE-14.                                                          
            04 FILLER PIC X(02) VALUE '! '.                                     
            04 FILLER PIC X(26) VALUE 'DATE    /LIEU DE NAISSANCE'.             
            04 FILLER PIC X(17) VALUE SPACES.                                   
            04 FILLER PIC X(14) VALUE 'A D R E S S E '.                         
            04 FILLER PIC X(16) VALUE '              '.                         
            04 FILLER PIC X(55) VALUE SPACES.                                   
            04 FILLER PIC X(02) VALUE '! '.                                     
      *****************************************************************         
          02 LIGNE-15.                                                          
            04 FILLER PIC X(02) VALUE '! '.                                     
            04 ED-DDN PIC X(08).                                                
            04 FILLER PIC X(01) VALUE ' '.                                      
            04 ED-RDN PIC X(02).                                                
            04 FILLER PIC X(01) VALUE ' '.                                      
            04 ED-LDN PIC X(30).                                                
            04 FILLER PIC X(01) VALUE SPACE.                                    
            04 ED-ADR PIC X(85).                                                
            04 FILLER PIC X(06) VALUE '! '.                                     
      *****************************************************************         
          02 LIGNE-09.                                                          
            04 FILLER PIC X(30) VALUE '------------------------------'.         
            04 FILLER PIC X(30) VALUE '------------------------------'.         
            04 FILLER PIC X(30) VALUE '------------------------------'.         
            04 FILLER PIC X(30) VALUE '------------------------------'.         
            04 FILLER PIC X(11) VALUE '-----------'.                            
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE-10.                                                          
            04 FILLER PIC X(02) VALUE '! '.                                     
            04 ED-DDELIV  PIC X(8).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-NMAG    PIC X(3).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-NVENTE  PIC X(7).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-NOM     PIC X(25).                                            
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-PRENOM PIC X(15).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-TELDOM PIC X(10).                                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-TELBUR  PIC X(10).                                            
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-POSTEBUR PIC X(05).                                           
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-FAM      PIC X(5).                                            
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-MARQ  PIC X(5).                                               
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-NDECLARATION PIC X(13).                                       
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 ED-ANNUL  PIC X(01).                                             
            04 FILLER PIC X(12) VALUE '          ! '.                           
      **************************************************                03030000
          02 LIGNE-12.                                                          
            04 FILLER        PIC X(47) VALUE SPACES.                            
            04 FILLER        PIC X(20) VALUE 'NOMBRE DECLARATIONS '.            
            04 FILLER        PIC X(13) VALUE 'ANNULATIONS: '.                   
            04 Z-CPT-ANNUL   PIC ZZZZ9 VALUE SPACES.                            
            04 FILLER        PIC X(47) VALUE SPACES.                            
      **************************************************                03030000
          02 LIGNE-13.                                                          
            04 FILLER        PIC X(47) VALUE SPACES.                            
            04 FILLER        PIC X(20) VALUE 'NOMBRE DECLARATIONS '.            
            04 FILLER        PIC X(13) VALUE ' VENTES    : '.                   
            04 Z-CPT-NON-ANN PIC ZZZZ9 VALUE SPACES.                            
            04 FILLER        PIC X(47) VALUE SPACES.                            
      *                                                                 02990000
      *                                                                 03000000
      **************************************************                03010000
      *  DESCRIPTION DES ZONES D'APPEL AU MODULE ABEND *                03020000
      **************************************************                03030000
           COPY ABENDCOP.                                               03040000
                                                                        03050000
      *****************************************************************         
      *     P R O C E D U R E  D I V I S I O N                                  
      *****************************************************************         
      *-----------------------------------------------------------------        
      *     TRAME DU PROGRAMME BRD010                                           
      *-----------------------------------------------------------------        
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
       MODULE-BRD010 SECTION.                                                   
      *                                                                         
           PERFORM DEBUT-BRD010.                                                
           PERFORM TRAITEMENT-BRD010.                                           
           PERFORM FIN-BRD010.                                                  
      *                                                                         
       F-MODULE-BRD010 .EXIT.                                                   
      *                                                                         
      *-----------------------------------------------------------------        
      *           DEBUT DU BRD010                                               
      *-----------------------------------------------------------------        
      *                                                                         
       DEBUT-BRD010 SECTION.                                                    
      *                                                                         
           OPEN INPUT  FRD006.                                                  
           OPEN INPUT  FDATE.                                                   
           OPEN OUTPUT IRD010.                                                  
      *                                                                         
           MOVE 0 TO W-CPT-LU.                                                  
           MOVE 0 TO W-CPT-FAUX.                                                
      *                                                                         
           READ FDATE AT END                                                    
                MOVE 'DATE PARAMETRE ABSENTE '      TO ABEND-MESS               
                PERFORM ABEND-PROGRAMME.                                        
           MOVE  DATE-PARM       TO DAT-JMAS.                                   
           MOVE  D1-JJ           TO D2-JJ.                                      
           MOVE  D1-MM           TO D2-MM.                                      
           MOVE  D1-AA           TO D2-AA.                                      
           MOVE DAT-JMA          TO ED-JOUR.                                    
      *                                                                         
       F-DEBUT-BRD010. EXIT.                                                    
      *                                                                         
      *-----------------------------------------------------------------        
      *           TRAITEMENT DU BRD010                                          
      *-----------------------------------------------------------------        
      *                                                                         
       TRAITEMENT-BRD010 SECTION.                                               
      *                                                                         
           SET NON-FIN-FICHIER TO TRUE.                                         
           PERFORM LECTURE-EDRED.                                               
           MOVE RE-NMAG TO W-MAG.                                               
           PERFORM EDITION-ENT.                                                 
           PERFORM UNTIL   FIN-FICHIER                                          
                   ADD 1   TO W-CPT-LU                                          
                   PERFORM TEST-SECTION                                         
                   PERFORM LECTURE-EDRED                                        
           END-PERFORM.                                                         
           PERFORM EDITION-FIN.                                                 
      *                                                                         
       F-TRAITEMENT-BRD010. EXIT.                                               
      *                                                                         
      *-----------------------------------------------------------------        
      *           LECTURE DU EDRED                                              
      *-----------------------------------------------------------------        
      *                                                                         
       LECTURE-EDRED  SECTION.                                                  
           READ FRD006 AT END SET FIN-FICHIER TO TRUE.                          
           IF NOT FIN-FICHIER                                                   
              IF   RE-ANNUL = 'A'                                               
                   ADD 1 TO W-CPT-ANNUL                                         
              ELSE                                                              
                   ADD 1 TO W-CPT-NON-ANN                                       
              END-IF                                                            
           END-IF.                                                              
       F-LECTURE-EDRED. EXIT.                                                   
      *                                                                         
      *-----------------------------------------------------------------        
      *           TEST SUR LA SECTION                                           
      *-----------------------------------------------------------------        
      *                                                                         
       TEST-SECTION      SECTION.                                               
             ADD 1 TO W-CPT-FAUX.                                               
             PERFORM EDITION-MVT.                                               
       F-TEST-SECTION. EXIT.                                                    
      *                                                                         
      *-----------------------------------------------------------------        
      *           IMPRESSION DES MOUVEMENTS                                     
      *-----------------------------------------------------------------        
      *                                                                         
       EDITION-MVT SECTION.                                                     
           IF CTR-LIGNES > 56 OR RE-NMAG NOT = W-MAG                            
              PERFORM EDITION-FIN                                               
              PERFORM EDITION-ENT                                               
           END-IF.                                                              
           PERFORM EDITION-DETAIL.                                              
           MOVE RE-NMAG TO W-MAG.                                               
       F-EDITION-MVT. EXIT.                                                     
      *                                                                         
      *-----------------------------------------------------------------        
      *           EDITION DE LA FERMETURE DU TABLEAU                            
      *-----------------------------------------------------------------        
      *                                                                         
       EDITION-FIN SECTION.                                                     
      *                                                                         
           MOVE 0           TO   CTR-LIGNES.                                    
           WRITE LIGNE1     FROM LIGNE-09.                                      
      *                                                                         
       F-EDITION-FIN. EXIT.                                                     
      *                                                                         
      *-----------------------------------------------------------------        
      *           EDITION DE L ENTETE DU TABLEAU                                
      *-----------------------------------------------------------------        
      *                                                                         
       EDITION-ENT SECTION.                                                     
      *                                                                         
           ADD +1               TO CTR-PAGE.                                    
           MOVE CTR-PAGE        TO ED-PAGE.                                     
           MOVE RE-DTRAIT       TO ED-DTRAIT.                                   
      *                                                                         
           WRITE LIGNE1         FROM LIGNE-01 AFTER ADVANCING SAUT.             
           WRITE LIGNE1         FROM LIGNE-02.                                  
           WRITE LIGNE1         FROM LIGNE-03.                                  
           WRITE LIGNE1         FROM LIGNE-04.                                  
           WRITE LIGNE1         FROM LIGNE-05.                                  
           WRITE LIGNE1         FROM LIGNE-06.                                  
           WRITE LIGNE1         FROM LIGNE-07.                                  
           WRITE LIGNE1         FROM LIGNE-08.                                  
           WRITE LIGNE1         FROM LIGNE-14.                                  
           WRITE LIGNE1         FROM LIGNE-09.                                  
      *                                                                         
           ADD +10               TO CTR-LIGNES.                                 
      *                                                                         
       F-EDITION-ENT. EXIT.                                                     
      *                                                                         
      *-----------------------------------------------------------------        
      *           EDITION D UNE LIGNE DE DETAIL                                 
      *-----------------------------------------------------------------        
      *                                                                         
       EDITION-DETAIL SECTION.                                                  
      *                                                                         
           MOVE RE-DDELIV       TO  ED-DDELIV                                   
           MOVE RE-NMAG         TO  ED-NMAG.                                    
           MOVE RE-NVENTE       TO  ED-NVENTE.                                  
           MOVE RE-NOM          TO  ED-NOM.                                     
           MOVE RE-PRENOM       TO  ED-PRENOM.                                  
           MOVE RE-TELDOM       TO  ED-TELDOM.                                  
           MOVE RE-TELBUR       TO  ED-TELBUR.                                  
           MOVE RE-POSTEBUR     TO  ED-POSTEBUR.                                
           MOVE RE-FAM          TO  ED-FAM.                                     
           MOVE RE-MARQ         TO  ED-MARQ.                                    
           MOVE RE-NDECLARATION TO  ED-NDECLARATION.                            
           MOVE RE-ANNUL        TO  ED-ANNUL.                                   
      *                                                                         
           WRITE LIGNE1         FROM LIGNE-10.                                  
      *                                                                         
      *                                                                         
           MOVE SPACE TO TA.                                                    
           STRING  RE-NVOIE  ' '  RE-TVOIE   ' '  RE-NOMVOIE                    
                             ' '  RE-CPOSTAL ' '  RE-COMMUNE                    
           DELIMITED SIZE INTO TA.                                              
           PERFORM CONCA.                                                       
           MOVE TA          TO ED-ADR.                                          
           IF RE-D-NAISS = '99999999'                                           
              MOVE RE-D-NAISS TO ED-DDN                                         
           ELSE                                                                 
              STRING RE-D-NAISS (1:2) '/' RE-D-NAISS (3:2) '/'                  
                     RE-D-NAISS (7:2) DELIMITED BY SIZE                         
                                   INTO ED-DDN                                  
           END-IF                                                               
           MOVE RE-DEPT-NAISS     TO ED-RDN                                     
           MOVE RE-COMMUNE-NAISS  TO ED-LDN                                     
      *                                                                         
           WRITE LIGNE1         FROM LIGNE-15.                                  
      *                                                                         
           ADD +2               TO CTR-LIGNES.                                  
      *                                                                         
       F-EDITION-MVT. EXIT.                                                     
      *                                                                         
       CONCA    SECTION.                                                        
      *                                                                         
                MOVE 1 TO B. MOVE 85 TO T. MOVE 1  TO I.                        
                MOVE 0 TO J.                                                    
      *                                                                         
                PERFORM UNTIL I > T                                             
                    IF A(I) = ' ' ADD 1   TO B                                  
                    ELSE          MOVE 0  TO B                                  
                    END-IF                                                      
                    IF B < 2 ADD 1 TO J                                         
                       IF I > J  MOVE A(I)  TO A(J)                             
                                 MOVE ' '   TO A(I)                             
                       END-IF                                                   
                    END-IF                                                      
                ADD 1 TO I                                                      
                END-PERFORM.                                                    
       F-CONCA. EXIT.                                                           
      *                                                                         
      *-----------------------------------------------------------------        
      *           FIN DU BRD010                                                 
      *-----------------------------------------------------------------        
      *                                                                         
       FIN-BRD010 SECTION.                                                      
      *                                                                         
           PERFORM FERMETURE-FICHIER.                                           
           PERFORM FIN-PROGRAMME.                                               
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       F-FIN-BRD010. EXIT.                                                      
      *                                                                         
      *-----------------------------------------------------------------        
      *           FERMETURE DES FICHIERS                                        
      *-----------------------------------------------------------------        
      *                                                                         
       FERMETURE-FICHIER    SECTION.                                            
           MOVE W-CPT-LU        TO   Z-CPT-LU.                                  
           MOVE W-CPT-FAUX      TO   Z-CPT-FAUX.                                
           MOVE W-CPT-ANNUL     TO   Z-CPT-ANNUL.                               
           MOVE W-CPT-NON-ANN   TO   Z-CPT-NON-ANN.                             
           IF CTR-LIGNES > 50                                                   
              PERFORM EDITION-FIN                                               
              ADD +1               TO CTR-PAGE                                  
              MOVE CTR-PAGE        TO ED-PAGE                                   
              MOVE RE-DTRAIT       TO ED-DTRAIT                                 
              WRITE LIGNE1         FROM LIGNE-01 AFTER ADVANCING SAUT           
              WRITE LIGNE1         FROM LIGNE-02                                
              WRITE LIGNE1         FROM LIGNE-03                                
              WRITE LIGNE1         FROM LIGNE-04                                
              WRITE LIGNE1         FROM LIGNE-05                                
              WRITE LIGNE1         FROM LIGNE-06                                
           END-IF.                                                              
              WRITE LIGNE1         FROM LIGNE-12 AFTER 8.                       
              WRITE LIGNE1         FROM LIGNE-13 AFTER 2.                       
           CLOSE IRD010 .                                                       
           CLOSE FRD006.                                                        
           CLOSE FDATE.                                                         
       FIN-FERMETURE-FICHIER. EXIT.                                             
      *                                                                         
       FIN-PROGRAMME        SECTION.                                            
           MOVE W-CPT-LU    TO   Z-CPT-LU.                                      
           MOVE W-CPT-FAUX  TO   Z-CPT-FAUX.                                    
           DISPLAY ' '.                                                         
           DISPLAY '***************************************'.                   
           DISPLAY '* PROGRAMME BRD010     : COMPTE RENDU * '.                  
           DISPLAY '***************************************'.                   
           DISPLAY '* ENREGISTREMENTS LUS ...: ' Z-CPT-LU '        *'           
           DISPLAY '* ENREGISTREMENTS EDITES : ' Z-CPT-FAUX '      *'           
           DISPLAY '***************************************'.                   
       F-FIN-PROGRAMME. EXIT.                                                   
      *                                                                         
       ABEND-PROGRAMME       SECTION.                                   14420000
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                14430000
       FIN-ABEND-PROGRAMME.       EXIT.                                 14420000
      *                                                                 14400000
                                                                        14410000
