      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BRE060.                                                     
       AUTHOR. JG.                                                              
       ENVIRONMENT DIVISION.                                                    
      ******************************************************************        
      *                                                                         
      *  CONSTITUTION D'UNE BASE STATS LIVRE                                    
      *  PHASE 1: GENERATION DU SYSIN POUR FAST UNLOAD DE GS40, GV10            
      *           ET GV11                                                       
      ******************************************************************        
      * si la filiale est logistique : ajout du prefixe PDARTY pour             
      * la table RTGS40 .                                                       
      * Ajout de FLOGIS, qui contient LGTG ( logistique groupe ) ou             
      * HLGTG ( hors logistique groupe )                                        
      ******************************************************************        
      * modif ap dsa005 - le 17/08  ==> ref 'ap1'                               
      * ne plus prendre en compte les vtes inexistantes de gv11, i.e.           
      * vente avec dcreation > dcompta                                          
      ******************************************************************        
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FDATE  ASSIGN TO FDATE.                                      
      *--                                                                       
            SELECT FDATE  ASSIGN TO FDATE                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FNSOC  ASSIGN TO FNSOC.                                      
      *--                                                                       
            SELECT FNSOC  ASSIGN TO FNSOC                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FPARAM ASSIGN TO FPARAM.                                     
      *--                                                                       
            SELECT FPARAM ASSIGN TO FPARAM                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FSYSIN ASSIGN TO FSYSIN.                                     
      *--                                                                       
            SELECT FSYSIN ASSIGN TO FSYSIN                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FRE060 ASSIGN TO FRE060.                                     
      *--                                                                       
            SELECT FRE060 ASSIGN TO FRE060                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
AL    *     SELECT FLOGIS ASSIGN TO FLOGIS.                                     
      *--                                                                       
            SELECT FLOGIS ASSIGN TO FLOGIS                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
AL     FD  FLOGIS RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  MW-FILLER PIC X(80).                                                 
       FD  FDATE RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.             
       01  MW-FILLER PIC X(80).                                                 
       FD  FNSOC RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.             
       01  MW-FILLER PIC X(80).                                                 
       FD  FPARAM RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  MW-FILLER PIC X(80).                                                 
       FD  FSYSIN RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
      *MW DAR-2                                                                 
       01  MW-FILLER PIC X(80).                                                 
       FD  FRE060 RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  ENR-FRE060 PIC X(80).                                                
       WORKING-STORAGE SECTION.                                                 
      ***************************************************************           
      * ZONE OUT POUR FRE060, FLAG POUR FSYSIN                                  
      ***************************************************************           
       01  W-FRE060 PIC X(80).                                                  
       01  FILLER PIC X VALUE SPACE.                                            
           88  FIN-FSYSIN VALUE 'F'.                                            
      ***************************************************************           
      * COMMS AVEC LES SOUS PROGS                                               
      ***************************************************************           
           COPY ABENDCOP.                                                       
       01  BETDATC PIC X(8) VALUE 'BETDATC'.                                    
           COPY WORKDATC.                                                       
      ***************************************************************           
      * DATES CALCULEES                                                         
      * ACHTUNG FLAG: TRAITEMENT QUOTIDIEN, HEBDO OU MENSUEL                    
      *               (QUOTIDIEN ET JOURNALIER C'EST LA MEME CHOSE)             
      ***************************************************************           
       01  W-DDEBUT PIC X(8).                                                   
       01  W-DFIN   PIC X(8).                                                   
       01  W-DMOIS  PIC X(6).                                                   
       01  HEBDO-OU-MENSUEL PIC X.                                              
           88 QUOTIDIEN  VALUE 'Q'.                                             
           88 JOURNALIER VALUE 'Q'.                                             
           88 HEBDO      VALUE 'H'.                                             
           88 MENSUEL    VALUE 'M'.                                             
      ***************************************************************           
      * SOCIETE DE TRAITEMENT                                                   
      ***************************************************************           
       01  W-NSOC PIC XXX.                                                      
      ***************************************************************           
      * LOGISTIQUE                                                              
      ***************************************************************           
       01  W-FLOGIS PIC X(80).                                                  
      ***************************************************************           
      * PROC:                                                                   
      * - LECTURE DATE, FNSOC                                                   
      * - LECTURE FPARAM (TRAITEMENT HEBDO OU MENSUEL)                          
      * - LECTURE FLOGIS ( LOGISTIQUE GROUPE OU NON )                           
      * - CALCUL DES BORNES DE DATES                                            
      * - CHARGEMENT DES CODES REMISE EN TABLEAU                                
      * - GENERATION DE SYSIN                                                   
      * - C'EST TOUT                                                            
      ***************************************************************           
       PROCEDURE DIVISION.                                                      
           DISPLAY 'BRE060: Base Stats en Livr� 1/3'                            
           OPEN INPUT FDATE FNSOC FPARAM FSYSIN FLOGIS                          
           OUTPUT FRE060.                                                       
           READ FDATE INTO GFJJMMSSAA AT END                            01090024
                MOVE 'FICHIER FDATE VIDE' TO ABEND-MESS                 01100024
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           CLOSE FDATE.                                                 01130024
           MOVE '1'            TO GFDATA.                               00004640
           CALL BETDATC      USING WORK-BETDATC                         00004670
           IF GFVDAT NOT = '1'                                          00004680
              STRING 'BETDATC : ' GF-MESS-ERR                           00004700
              DELIMITED BY SIZE INTO ABEND-MESS                         00004710
              PERFORM ABEND-PROGRAMME                                           
           END-IF.                                                      00004730
           DISPLAY 'BRE060: date param�tre lue sur FDATE: '                     
                    GFSMN-LIB-L ' ' GFJOUR ' '                                  
                    GFMOI-LIB-L ' ' GFSIECLE GFANNEE.                           
           MOVE GFSAMJ-0 TO W-DFIN W-DMOIS.                                     
      ***************************************************************           
      * SOCIETE                                                                 
      ***************************************************************           
           READ FNSOC INTO W-NSOC AT END                                01090024
                MOVE 'FICHIER FNSOC VIDE' TO ABEND-MESS                 01100024
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           CLOSE FNSOC.                                                 01130024
           DISPLAY 'BRE060: Soci�t� lue sur FNSOC: ' W-NSOC.                    
      ***************************************************************           
      * ESKE L'ON TOURNE EN HEBDO OU EN MENSUEL?                                
      ***************************************************************           
           READ FPARAM INTO HEBDO-OU-MENSUEL AT END                     01090024
              DISPLAY 'BRE060: fichier FPARAM vide'                             
           END-READ.                                                            
           CLOSE FPARAM.                                                01130024
           IF HEBDO                                                             
              DISPLAY 'BRE060: traitement HEBDO'                                
           ELSE                                                                 
              IF QUOTIDIEN OR JOURNALIER                                        
                 DISPLAY 'BRE060: traitement QUOTIDIEN'                         
              ELSE                                                              
                 SET MENSUEL TO TRUE                                            
                 DISPLAY 'BRE060: traitement MENSUEL'                           
              END-IF                                                            
           END-IF.                                                              
AL    ***************************************************************           
AL    * LA FILIALE EST-ELLE LOGISTIQUE GROUPE ?                                 
AL    ***************************************************************           
AL                                                                              
AL         READ FLOGIS INTO W-FLOGIS                                            
AL         END-READ.                                                            
AL                                                                              
AL         CLOSE FLOGIS .                                                       
      ***************************************************************           
      * KALKUL DES BORGNES (QUE POUR HEBDO)                                     
      ***************************************************************           
           IF HEBDO                                                             
              SUBTRACT 6 FROM GFQNT0                                            
              MOVE '3'            TO GFDATA                             00004640
              CALL BETDATC      USING WORK-BETDATC                      00004670
              IF GFVDAT NOT = '1'                                       00004680
                 STRING 'BETDATC : ' GF-MESS-ERR                        00004700
                 DELIMITED BY SIZE INTO ABEND-MESS                      00004710
                 PERFORM ABEND-PROGRAMME                                        
              END-IF                                                    00004730
              DISPLAY 'BRE060: date d�but de recherche: '                       
                       GFSMN-LIB-L ' ' GFJOUR ' '                               
                       GFMOI-LIB-L ' ' GFSIECLE GFANNEE                         
           END-IF.                                                              
           MOVE GFSAMJ-0 TO W-DDEBUT.                                           
      ******************************************************************        
      * RECOPIE DE FSYSIN                                                       
      ******************************************************************        
           DISPLAY 'BRE060: sysin g�n�r� � partir de FSYSIN:'                   
           PERFORM UNTIL FIN-FSYSIN                                             
              READ FSYSIN INTO W-FRE060                                         
                 AT END                                                         
                    SET FIN-FSYSIN TO TRUE                                      
                 NOT AT END                                                     
                    WRITE ENR-FRE060 FROM W-FRE060                              
                    DISPLAY W-FRE060                                            
              END-READ                                                          
           END-PERFORM.                                                         
           CLOSE FSYSIN.                                                        
      ******************************************************************        
      * GENERATION DE L'ORDRE SQL GS40                                          
      ******************************************************************        
           DISPLAY 'BRE060: sysin g�n�r� pour RTGS40:'                          
           MOVE '   SELECT NCODIC, NLIEUVTE, NORIGINE, CVENDEUR,'               
                TO W-FRE060.                                                    
           WRITE ENR-FRE060 FROM W-FRE060.                                      
           DISPLAY W-FRE060.                                                    
           MOVE '          NLIEUORIG, NLIEUDEST, CPROG, PVTOTAL,'               
                TO W-FRE060.                                                    
           WRITE ENR-FRE060 FROM W-FRE060.                                      
           DISPLAY W-FRE060.                                                    
           MOVE '          PVTOTALSR, QMVT, DOPER, DCREATION, '                 
                TO W-FRE060.                                                    
           WRITE ENR-FRE060 FROM W-FRE060.                                      
           DISPLAY W-FRE060.                                                    
           MOVE '          NCODICGRP, CMODDEL'                                  
                TO W-FRE060.                                                    
           WRITE ENR-FRE060 FROM W-FRE060.                                      
           DISPLAY W-FRE060.                                                    
AL         IF W-FLOGIS = 'LGTG'                                                 
AL           MOVE '     FROM PDARTY.RTGS40 '                                    
AL              TO W-FRE060                                                     
AL         ELSE                                                                 
AL           MOVE '     FROM RTGS40 '                                           
AL              TO W-FRE060                                                     
AL         END-IF.                                                              
           WRITE ENR-FRE060 FROM W-FRE060.                                      
           DISPLAY W-FRE060.                                                    
           MOVE '    WHERE NSOCVTE = '''                                        
                TO W-FRE060.                                                    
           MOVE W-NSOC                                                          
                TO W-FRE060(22:).                                               
           MOVE ''''                                                            
                TO W-FRE060(25:).                                               
           WRITE ENR-FRE060 FROM W-FRE060.                                      
           DISPLAY W-FRE060.                                                    
           MOVE '      AND (NLIEUORIG = ''VEN'''                                
                TO W-FRE060.                                                    
           WRITE ENR-FRE060 FROM W-FRE060.                                      
           DISPLAY W-FRE060.                                                    
           MOVE '            OR NLIEUDEST = ''VEN'')'                           
                TO W-FRE060.                                                    
           WRITE ENR-FRE060 FROM W-FRE060.                                      
           DISPLAY W-FRE060.                                                    
      **** TRAITEMENT HEBDO                                                     
           IF HEBDO                                                             
              MOVE '      AND DOPER >= ''' TO W-FRE060                          
              MOVE W-DDEBUT TO W-FRE060(21:)                                    
              MOVE '''' TO W-FRE060(29:)                                        
              WRITE ENR-FRE060 FROM W-FRE060                                    
              DISPLAY W-FRE060                                                  
              MOVE '      AND DOPER <= ''' TO W-FRE060                          
              MOVE W-DFIN TO W-FRE060(21:)                                      
              MOVE ''' ;' TO W-FRE060(29:)                                      
              WRITE ENR-FRE060 FROM W-FRE060                                    
              DISPLAY W-FRE060                                                  
           ELSE IF MENSUEL                                                      
      **** TRAITEMENT MENSUEL                                                   
                 MOVE '      AND DSTAT  = ''' TO W-FRE060                       
                 MOVE W-DMOIS TO W-FRE060(21:)                                  
                 MOVE ''' ;' TO W-FRE060(27:)                                   
                 WRITE ENR-FRE060 FROM W-FRE060                                 
                 DISPLAY W-FRE060                                               
      **** SINON, C'EST QUOTIDIEN (EN CUMUL MOIS)                               
              ELSE                                                              
                 MOVE '      AND DSTAT  = ''' TO W-FRE060                       
                 MOVE ''' ;' TO W-FRE060(27:)                                   
                 WRITE ENR-FRE060 FROM W-FRE060                                 
                 DISPLAY W-FRE060                                               
              END-IF                                                            
           END-IF.                                                              
      ******************************************************************        
      * GENERATION DE L'ORDRE SQL GV10                                          
      ******************************************************************        
           DISPLAY 'BRE060: sysin g�n�r� pour RTGV10:'                          
           MOVE '   SELECT NLIEU, NVENTE, WEXPORT'                              
                TO W-FRE060.                                                    
           WRITE ENR-FRE060 FROM W-FRE060.                                      
           DISPLAY W-FRE060.                                                    
           MOVE '     FROM RTGV10 '                                             
                TO W-FRE060.                                                    
           WRITE ENR-FRE060 FROM W-FRE060.                                      
           DISPLAY W-FRE060.                                                    
           MOVE '    WHERE NSOCIETE = '''                                       
                TO W-FRE060.                                                    
           MOVE W-NSOC                                                          
                TO W-FRE060(23:).                                               
           MOVE ''';'                                                           
                TO W-FRE060(26:).                                               
           WRITE ENR-FRE060 FROM W-FRE060.                                      
           DISPLAY W-FRE060.                                                    
      ******************************************************************        
      * GENERATION DE L'ORDRE SQL GV11                                          
      ******************************************************************        
           DISPLAY 'BRE060: sysin g�n�r� pour RTGV11:'                          
           MOVE '   SELECT NLIEU, NVENTE, NLIGNE, NCODIC, CVENDEUR,'            
                TO W-FRE060.                                                    
           WRITE ENR-FRE060 FROM W-FRE060.                                      
           DISPLAY W-FRE060.                                                    
           MOVE '          CTYPENREG, CENREG, QVENDUE, PVUNIT, PVUNITF,'        
                TO W-FRE060.                                                    
           WRITE ENR-FRE060 FROM W-FRE060.                                      
           DISPLAY W-FRE060.                                                    
           MOVE '          PVTOTAL, CMODDEL, DCREATION, NCODICGRP,'             
                TO W-FRE060.                                                    
           WRITE ENR-FRE060 FROM W-FRE060.                                      
           DISPLAY W-FRE060.                                                    
           MOVE '          DDELIV, DCOMPTA, MPRIMECLI, NSEQREF, NSEQENS'        
                TO W-FRE060.                                                    
           WRITE ENR-FRE060 FROM W-FRE060.                                      
           DISPLAY W-FRE060.                                                    
           MOVE '     FROM RVGV1106 ' TO W-FRE060.                              
           WRITE ENR-FRE060 FROM W-FRE060.                                      
           DISPLAY W-FRE060.                                                    
           MOVE '    WHERE NSOCIETE = ''' TO W-FRE060.                          
           MOVE W-NSOC TO W-FRE060(23:).                                        
           MOVE '''' TO W-FRE060(26:).                                          
           WRITE ENR-FRE060 FROM W-FRE060.                                      
           DISPLAY W-FRE060.                                                    
           MOVE '      AND WTOPELIVRE = ''O'' ' TO W-FRE060.                    
           WRITE ENR-FRE060 FROM W-FRE060.                                      
           DISPLAY W-FRE060.                                                    
      *{Post-translation Correct-Sign-not
      *    MOVE '      and ((DCREATION ^> DCOMPTA)  OR' TO W-FRE060.            
ap1        MOVE '      and ((DCREATION <= DCOMPTA)  OR' TO W-FRE060.            
      *}
ap1        WRITE ENR-FRE060 FROM W-FRE060.                                      
ap1        DISPLAY W-FRE060.                                                    
ap1        MOVE '            (DCOMPTA = '' '' )) ;' TO W-FRE060.                
ap1        WRITE ENR-FRE060 FROM W-FRE060.                                      
ap1        DISPLAY W-FRE060.                                                    
      ******************************************************************        
      * ZIENDE                                                                  
      ******************************************************************        
           CLOSE FRE060.                                                        
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      ******************************************************************        
      * SCHPLOUF                                                                
      ******************************************************************        
       ABEND-PROGRAMME                 SECTION.                                 
           MOVE  'BRE060'     TO   ABEND-PROG.                                  
           CALL  'ABEND'   USING   ABEND-PROG  ABEND-MESS.                      
