      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                         00000020
       PROGRAM-ID.  BIT011.                                             00000030
       AUTHOR.      DSA011-FC.                                          00000040
                                                                        00000050
      *-----------------------------------------------------------      00000060
      *                                                                 00000070
      *    PROJET         : INVENTAIRE TOURNANT                         00000080
      *    PROGRAMME      : BIT011                                      00000090
      *    FONCTION       : EDITION DU SUPPORT DE COMPTAGE              00000100
      *    DATE DE CREAT. : 18/06/1992                                  00000110
      *                                                                 00000120
      *-----------------------------------------------------------      00000130
      * 20/10/92 DSA011-FC : ECRITURE DANS FICHIER IMPR IIT01           00000140
      *                      SEULEMENT SI CODE LIEU >= '090'            00000150
      *                      OU        SI SOCIETE    = '907'            00000160
      *-----------------------------------------------------------      00000170
                                                                        00000180
       ENVIRONMENT DIVISION.                                            00000190
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                           00000200
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
      *{  Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                     
       SPECIAL-NAMES.                                                           
           COPY MW-COLSEQ.                                                      
      *}                                                                        
                                                                                
      *}                                                                        
       INPUT-OUTPUT     SECTION.                                        00000210
       FILE-CONTROL.                                                    00000220
      *    SELECT  FNSOC          ASSIGN  TO  FNSOC.                    00000230
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FDATE          ASSIGN  TO  FDATE.                    00000240
      *--                                                                       
           SELECT  FDATE          ASSIGN  TO  FDATE                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FIT011         ASSIGN  TO  FIT011.                   00000250
      *--                                                                       
           SELECT  FIT011         ASSIGN  TO  FIT011                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  IIT011         ASSIGN  TO  IIT011.                   00000260
      *--                                                                       
           SELECT  IIT011         ASSIGN  TO  IIT011                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FEDG36         ASSIGN  TO  FEDG36.                   00000270
      *--                                                                       
           SELECT  FEDG36         ASSIGN  TO  FEDG36                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                   00000280
       FILE SECTION.                                                    00000290
                                                                        00000300
      * ----------------------------------------------------------      00000310
      *            D E S C R I P T I O N     F I C H I E R              00000320
      * ----------------------------------------------------------      00000330
                                                                        00000340
      *FD  FNSOC                                                        00000350
      *    RECORDING F                                                  00000360
      *    BLOCK CONTAINS 0 RECORDS                                     00000370
      *    LABEL RECORD STANDARD.                                       00000380
      *                                                                 00000390
      *01  ENR-FNSOC.                                                   00000400
      *    03  FNSOC-NSOC        PIC X(03).                             00000410
      *    03  FILLER            PIC X(77).                             00000420
                                                                        00000430
       FD  FDATE                                                        00000440
           RECORDING F                                                  00000450
           BLOCK CONTAINS 0 RECORDS                                     00000460
           LABEL RECORD STANDARD.                                       00000470
                                                                        00000480
       01  ENR-FDATE.                                                   00000490
           03  FDATE-JJ          PIC X(02).                             00000500
           03  FDATE-MM          PIC X(02).                             00000510
           03  FDATE-SS          PIC X(02).                             00000520
           03  FDATE-AA          PIC X(02).                             00000530
           03  FILLER            PIC X(72).                             00000540
                                                                        00000550
       FD  FIT011                                                       00000560
           RECORDING F                                                  00000570
           BLOCK CONTAINS 0 RECORDS                                     00000580
           LABEL RECORD STANDARD.                                       00000590
       01  FIT011-ENREG          PIC X(90).                             00000600
                                                                        00000610
       FD  IIT011                                                       00000620
           RECORDING F                                                  00000630
           BLOCK CONTAINS 0 RECORDS                                     00000640
           LABEL RECORD STANDARD.                                       00000650
       01  IIT011-ENREG.                                                00000660
           05     IIT011-ASA     PIC X(001).                            00000670
           05     IIT011-DATA    PIC X(132).                            00000680
                                                                        00000690
       FD  FEDG36                                                       00000700
           RECORDING F                                                  00000710
           BLOCK CONTAINS 0 RECORDS                                     00000720
           LABEL RECORD STANDARD.                                       00000730
       01  FEDG36-ENREG     PIC X(156).                                 00000740
                                                                        00000750
                                                                        00000760
      * ----------------------------------------------------------      00000770
      *                W O R K I N G  -  S T O R A G E                  00000780
      * ----------------------------------------------------------      00000790
                                                                        00000800
       WORKING-STORAGE SECTION.                                         00000810
                                                                        00000820
       77  W-NPAGE                       PIC S9(4) COMP-3  VALUE 0.     00000830
       77  W-NLIG                        PIC S9(4) COMP-3  VALUE 0.     00000840
       77  W-NLIG-MAX                    PIC S9(4) COMP-3  VALUE 62.    00000850
                                                                        00000860
       77  W-NSOC                        PIC X(03)         VALUE SPACE. 00000870
       77  W-CFAM                        PIC X(05)         VALUE SPACE. 00000880
       77  W-CMARQ                       PIC X(05)         VALUE SPACE. 00000890
                                                                        00000900
       77  W-LUS-FIT011                  PIC S9(7) COMP-3  VALUE ZERO.  00000910
       77  W-ECRITS-IIT011               PIC S9(7) COMP-3  VALUE ZERO.  00000920
       77  W-ECRITS-FEDG36               PIC S9(7) COMP-3  VALUE ZERO.  00000930
       77  W-PAGE-IIT011                 PIC S9(7) COMP-3  VALUE ZERO.  00000940
                                                                        00000950
           COPY SWIT010.                                                00000960
                                                                        00000970
           COPY ABENDCOP.                                               00000980
                                                                        00000990
           COPY WORKDATC.                                               00001000
                                                                        00001010
       01  FEDG36-WORK.                                                 00001020
           03  FEDG36-CETAT      PIC X(08).                             00001030
           03  FEDG36-NSEQ       PIC S9(07) COMP-3.                     00001040
           03  FEDG36-NSOC       PIC X(03).                             00001050
           03  FEDG36-NLIEU      PIC X(03).                             00001060
           03  FEDG36-CRIT       PIC X(05).                             00001070
           03  FEDG36-ASA        PIC X(01).                             00001080
           03  FEDG36-DATA       PIC X(132).                            00001090
                                                                        00001100
                                                                        00001110
       01  ETAT-FIT011                   PIC  X(1)         VALUE '0'.   00001120
           88   NON-FIN-FIT011                             VALUE '0'.   00001130
           88       FIN-FIT011                             VALUE '1'.   00001140
                                                                        00001150
       01  W-SSAAMMJJ.                                                  00001160
           02 W-SS                       PIC X(2).                      00001170
           02 W-AA                       PIC X(2).                      00001180
           02 W-MM                       PIC X(2).                      00001190
           02 W-JJ                       PIC X(2).                      00001200
       01  W-JJ-MM-AA                    PIC X(8).                      00001210
                                                                        00001220
      *----------------------------------------------------------------*00001230
      *                                                                 00001240
      * DEFINITION DES LIGNES D'EDITION                                 00001250
      *                                                                 00001260
      *-------------------------123456789012345678901234567890---------*00001270
      *                                                                 00001280
       01  LIG-01.                                                      00001290
           5 F PIC X(30) VALUE 'IIT011                        '.        00001300
           5 F PIC X(30) VALUE '                        ETABLI'.        00001310
           5 F PIC X(30) VALUE 'SSEMENTS DARTY                '.        00001320
           5 F PIC X(30) VALUE '                              '.        00001330
           5 F PIC X(08) VALUE '  PAGE :'.                              00001340
           5 L01-NPAGE   PIC ZZZ9.                                      00001350
      *                                                                 00001360
       01  LIG-02.                                                      00001370
           5 F PIC X(30) VALUE '                              '.        00001380
           5 F PIC X(30) VALUE '          SUPPORT DE COMPTAGE '.        00001390
           5 F PIC X(22) VALUE 'INVENTAIRE TOURNANT : '.                00001400
           5 L02-NINVENTAIRE PIC X(05).                                 00001410
      *                                                                 00001420
       01  LIG-03.                                                      00001430
           5 F PIC X(09) VALUE 'LIEU  : '.                              00001440
           5 L03-NSOC    PIC X(03).                                     00001450
           5 F PIC X(02) VALUE '  '.                                    00001460
           5 L03-NLIEU   PIC X(03).                                     00001470
      *                                                                 00001480
       01  LIG-04.                                                      00001490
           5 F PIC X(32) VALUE 'EDITE LE                      : '.      00001500
           5 L04-DEDITSUPPORT  PIC X(08).                               00001510
      *                                                                 00001520
       01  LIG-05.                                                      00001530
           5 F PIC X(32) VALUE 'DATE DE COMPTAGE              : '.      00001540
           5 L05-DCOMPTAGE     PIC X(08).                               00001550
      *                                                                 00001560
       01  LIG-06.                                                      00001570
           5 F PIC X(32) VALUE 'RAPPROCHEMENT STOCK THEORIQUE : '.      00001580
           5 L06-DSTOCKTHEO    PIC X(08).                               00001590
      *                                                                 00001600
       01  LIG-07.                                                      00001610
           5 F PIC X(30) VALUE '   FAMILLE   MARQUE       REFE'.        00001620
           5 F PIC X(30) VALUE 'RENCE            CODIC     !  '.        00001630
           5 F PIC X(32) VALUE '         STOCK COMPTE          !'.      00001640
      *                                                                 00001650
       01  LIG-08.                                                      00001660
           5 F PIC X(30) VALUE '                              '.        00001670
           5 F PIC X(30) VALUE '                           !  '.        00001680
           5 F PIC X(32) VALUE 'TOTAL   !  RESERVE  !   EXPO   !'.      00001690
      *                                                                 00001700
       01  LIG-08PARIS.                                                 00001710
           5 F PIC X(30) VALUE '                              '.        00001720
           5 F PIC X(30) VALUE '                           !  '.        00001730
           5 F PIC X(32) VALUE 'TOTAL   !  RESERVE  !   EXPO   !'.      00001740
           5 F PIC X(32) VALUE ' HS SAV !HS MAGASIN !   PRET   !'.      00001750
      *                                                                 00001760
       01  LIG-09.                                                      00001770
           5 F PIC X(30) VALUE '                              '.        00001780
           5 F PIC X(30) VALUE '                           !  '.        00001790
           5 F PIC X(32) VALUE '        !           !          !'.      00001800
      *                                                                 00001810
       01  LIG-09PARIS.                                                 00001820
           5 F PIC X(30) VALUE '                              '.        00001830
           5 F PIC X(30) VALUE '                           !  '.        00001840
           5 F PIC X(32) VALUE '        !           !          !'.      00001850
           5 F PIC X(32) VALUE '        !           !          !'.      00001860
      *                                                                 00001870
       01  LIG-10.                                                      00001880
           5 FILLER            PIC X(04).                               00001890
           5 L10-CFAM          PIC X(05).                               00001900
           5 FILLER            PIC X(04).                               00001910
           5 L10-CMARQ         PIC X(05).                               00001920
           5 FILLER            PIC X(04).                               00001930
           5 L10-LREFFOURN     PIC X(20).                               00001940
           5 FILLER            PIC X(04).                               00001950
           5 L10-NCODIC        PIC X(07).                               00001960
           5 FILLER            PIC X(71).                               00001970
                                                                        00001980
                                                                        00001990
       PROCEDURE DIVISION.                                              00002000
                                                                        00002010
      *---------------------------------------------------------------  00002020
      * SQUELETTE DU PROGRAMME                                          00002030
      *---------------------------------------------------------------  00002040
                                                                        00002050
           PERFORM DEBUT-PGM-BIT011.                                    00002060
                                                                        00002070
           PERFORM TRAIT-FIT011 UNTIL FIN-FIT011.                       00002080
                                                                        00002090
           PERFORM FIN-PGM-BIT011.                                      00002100
                                                                        00002110
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00002120
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        00002130
      *---------------------------------------------------------------  00002140
      *                                                                 00002150
      *                                                                 00002160
      *---------------------------------------------------------------  00002170
      *                  D E B U T   B I T 0 1 1                        00002180
      *---------------------------------------------------------------  00002190
       DEBUT-PGM-BIT011.                                                00002200
      *    OPEN INPUT  FNSOC  FDATE FIT011                              00002210
           OPEN INPUT         FDATE FIT011                              00002220
                OUTPUT IIT011 FEDG36.                                   00002230
                                                                        00002240
      *    READ FNSOC AT END                                            00002250
      *         MOVE 'PAS DE SOCIETE EN ENTREE' TO ABEND-MESS           00002260
      *         PERFORM FIN-ANORMALE                                    00002270
      *    END-READ.                                                    00002280
      *    MOVE FNSOC-NSOC TO W-NSOC.                                   00002290
           READ FDATE INTO GFJJMMSSAA                                   00002300
                END   MOVE 'PAS DE DATE EN ENTREE' TO ABEND-MESS        00002310
                      PERFORM FIN-ANORMALE                              00002320
           END-READ.                                                    00002330
                                                                        00002340
           MOVE '1' TO GFDATA                                           00002350
           CALL 'BETDATC' USING WORK-BETDATC                            00002360
                                                                        00002370
           IF GFVDAT NOT = '1'                                          00002380
              MOVE 'DATE PARAM. REJETEE PAR BETDATC' TO ABEND-MESS      00002390
              PERFORM FIN-ANORMALE                                      00002400
           END-IF                                                       00002410
                                                                        00002420
           DISPLAY '*** DATE EN ENTREE '                                00002430
                   FDATE-JJ '/' FDATE-MM '/'                            00002440
                   FDATE-SS     FDATE-AA.                               00002450
                                                                        00002460
      *--> +1 SUR DATE EN ENTREE                                        00002470
           ADD   1  TO GFQNT0.                                          00002480
           MOVE '3' TO GFDATA.                                          00002490
           CALL 'BETDATC' USING WORK-BETDATC                            00002500
                                                                        00002510
           DISPLAY '*** EDITION DES SUPPORTS DE COMPTAGE DU '           00002520
                   GFJJ  '/' GFMM '/' GFSS GFAA.                        00002530
                                                                        00002540
      *    IF W-NSOC = '907'                                            00002550
      *       MOVE  LIG-09PARIS     TO LIG-10                           00002560
      *    ELSE                                                         00002570
      *       MOVE  LIG-09           TO LIG-10                          00002580
      *    END-IF.                                                      00002590
      *    MOVE FNSOC-NSOC TO L03-NSOC.                                 00002600
                                                                        00002610
           INITIALIZE         FEDG36-WORK.                              00002620
      *    MOVE FNSOC-NSOC TO FEDG36-NSOC.                              00002630
           MOVE 'IIT011'   TO FEDG36-CETAT.                             00002640
       FIN-DEBUT-PGM-BIT011.  EXIT.                                     00002650
                                                                        00002660
                                                                        00002670
      *---------------------------------------------------------------  00002680
      * FIN DU PROGRAMME / COMPTE RENDU                                 00002690
      *---------------------------------------------------------------  00002700
       FIN-PGM-BIT011.                                                  00002710
           PERFORM FERMETURE-FICHIERS.                                  00002720
                                                                        00002730
           DISPLAY '*------------------------------------------------'  00002740
           DISPLAY '*'                                                  00002750
           DISPLAY '*   P R O G R A M M E      B I T 0 1 1  :   O  K '  00002760
           DISPLAY '*'                                                  00002770
           DISPLAY '*'                                                  00002780
           DISPLAY '* ' W-LUS-FIT011    ' LUS    DANS FICHIER FIT011;.' 00002790
           DISPLAY '*'                                                  00002800
           DISPLAY '* ' W-ECRITS-IIT011 ' ECRITS DANS FICHIER IIT011 '  00002810
           DISPLAY '*'                                                  00002820
           DISPLAY '* ' W-ECRITS-FEDG36 ' ECRITS DANS FICHIER FEDG36;.' 00002830
           DISPLAY '*'                                                  00002840
           DISPLAY '* (' W-PAGE-IIT011  ' PAGES AU TOTAL);'             00002850
           DISPLAY '*'                                                  00002860
           DISPLAY '*------------------------------------------------'. 00002870
       FIN-FIN-PGM-BIT011. EXIT.                                        00002880
                                                                        00002890
       FERMETURE-FICHIERS.                                              00002900
      *    CLOSE  FNSOC FDATE FIT011 IIT011 FEDG36.                     00002910
           CLOSE        FDATE FIT011 IIT011 FEDG36.                     00002920
       FIN-FERMETURE-FICHIERS. EXIT.                                    00002930
      *---------------------------------------------------------------  00002940
      *                                                                 00002950
      *                                                                 00002960
      *---------------------------------------------------------------  00002970
      * LECTURE DES ENREGISTREMENTS.                                    00002980
      *---------------------------------------------------------------* 00002990
       TRAIT-FIT011.                                                    00003000
                                                                        00003010
           PERFORM LECTURE.                                             00003020
      *---                                                              00003030
           MOVE FIT010-NSOCIETE TO W-NSOC.                              00003040
                                                                        00003050
           IF W-NSOC = '907'                                            00003060
              MOVE  LIG-09PARIS     TO LIG-10                           00003070
           ELSE                                                         00003080
              MOVE  LIG-09           TO LIG-10                          00003090
           END-IF.                                                      00003100
                                                                        00003110
           MOVE W-NSOC TO L03-NSOC.                                     00003120
           MOVE W-NSOC TO FEDG36-NSOC.                                  00003130
      *---                                                              00003140
           IF  NON-FIN-FIT011                                           00003150
               ADD 1 TO W-LUS-FIT011                                    00003160
                                                                        00003170
               IF       FIT010-NLIEU     NOT =  FEDG36-NLIEU            00003180
                        MOVE    0            TO FEDG36-NSEQ             00003190
                        MOVE    FIT010-NLIEU TO FEDG36-NLIEU            00003200
               END-IF                                                   00003210
                                                                        00003220
               IF       FIT010-NLIEU       NOT = L03-NLIEU              00003230
                     OR FIT010-NINVENTAIRE NOT = L02-NINVENTAIRE        00003240
                                                                        00003250
                        MOVE     FIT010-NLIEU       TO L03-NLIEU        00003260
                        MOVE     FIT010-NINVENTAIRE TO L02-NINVENTAIRE  00003270
                        MOVE     GFJMA-4            TO L04-DEDITSUPPORT 00003280
                                                                        00003290
                        MOVE     FIT010-DCOMPTAGE   TO W-SSAAMMJJ       00003300
                        PERFORM  T-DATE                                 00003310
                        MOVE     W-JJ-MM-AA         TO L05-DCOMPTAGE    00003320
                                                                        00003330
                        MOVE     FIT010-DSTOCKTHEO  TO W-SSAAMMJJ       00003340
                        PERFORM  T-DATE                                 00003350
                        MOVE     W-JJ-MM-AA         TO L06-DSTOCKTHEO   00003360
                        MOVE     0                  TO W-NPAGE          00003370
                        MOVE     99                 TO W-NLIG           00003380
               END-IF                                                   00003390
                                                                        00003400
               IF       W-NLIG    > W-NLIG-MAX                          00003410
                        PERFORM EDIT-ENTETE                             00003420
                        MOVE      FIT010-CFAM        TO W-CFAM          00003430
                                                        L10-CFAM        00003440
                        MOVE      FIT010-CMARQ       TO W-CMARQ         00003450
                                                        L10-CMARQ       00003460
               END-IF                                                   00003470
                                                                        00003480
               IF       FIT010-CFAM   NOT = W-CFAM                      00003490
                        MOVE      FIT010-CFAM        TO W-CFAM          00003500
                                                        L10-CFAM        00003510
                        MOVE      SPACE              TO W-CMARQ         00003520
               END-IF                                                   00003530
                                                                        00003540
               IF       FIT010-CMARQ  NOT = W-CMARQ                     00003550
                        MOVE     FIT010-CMARQ       TO W-CMARQ          00003560
                                                       L10-CMARQ        00003570
                        MOVE     '0'                TO IIT011-ASA       00003580
               ELSE                                                     00003590
                        MOVE     ' '                TO IIT011-ASA       00003600
               END-IF                                                   00003610
                                                                        00003620
               MOVE     FIT010-LREFFOURN TO L10-LREFFOURN               00003630
               MOVE     FIT010-NCODIC    TO L10-NCODIC                  00003640
               MOVE     LIG-10           TO IIT011-DATA                 00003650
               PERFORM  EDIT-LIGNE                                      00003660
               IF W-NSOC = '907'                                        00003670
                  MOVE  LIG-09PARIS      TO LIG-10                      00003680
               ELSE                                                     00003690
                  MOVE  LIG-09           TO LIG-10                      00003700
               END-IF                                                   00003710
           END-IF.                                                      00003720
                                                                        00003730
       LECTURE.                                                         00003740
                                                                        00003750
           READ     FIT011   INTO FIT010-DSECT                          00003760
                    AT END   SET FIN-FIT011 TO TRUE                     00003770
           END-READ.                                                    00003780
                                                                        00003790
      *-----------------------------------------------------------------00003800
      * EDITION DE L'ENTETE DU DOCUMENT                                 00003810
      *-----------------------------------------------------------------00003820
       EDIT-ENTETE.                                                     00003830
               ADD   1 TO W-NPAGE                                       00003840
               MOVE       W-NPAGE TO L01-NPAGE.                         00003850
               MOVE '1'                  TO IIT011-ASA.                 00003860
               MOVE LIG-01               TO IIT011-DATA.                00003870
               PERFORM EDIT-LIGNE.                                      00003880
                                                                        00003890
               MOVE '0'                  TO IIT011-ASA.                 00003900
               MOVE LIG-02               TO IIT011-DATA.                00003910
               PERFORM EDIT-LIGNE.                                      00003920
                                                                        00003930
               MOVE ' '                  TO IIT011-ASA.                 00003940
               MOVE LIG-03               TO IIT011-DATA.                00003950
               PERFORM EDIT-LIGNE.                                      00003960
                                                                        00003970
               MOVE '0'                  TO IIT011-ASA.                 00003980
               MOVE LIG-04               TO IIT011-DATA.                00003990
               PERFORM EDIT-LIGNE.                                      00004000
                                                                        00004010
               MOVE ' '                  TO IIT011-ASA.                 00004020
               MOVE LIG-05               TO IIT011-DATA.                00004030
               PERFORM EDIT-LIGNE.                                      00004040
                                                                        00004050
               MOVE LIG-06               TO IIT011-DATA.                00004060
               PERFORM EDIT-LIGNE.                                      00004070
                                                                        00004080
               MOVE '-'                  TO IIT011-ASA.                 00004090
               MOVE LIG-07               TO IIT011-DATA.                00004100
               PERFORM EDIT-LIGNE.                                      00004110
                                                                        00004120
               MOVE ' '                  TO IIT011-ASA.                 00004130
               IF W-NSOC = '907'                                        00004140
                  MOVE LIG-08PARIS       TO IIT011-DATA                 00004150
               ELSE                                                     00004160
                  MOVE LIG-08            TO IIT011-DATA                 00004170
               END-IF.                                                  00004180
               PERFORM EDIT-LIGNE.                                      00004190
                                                                        00004200
               IF W-NSOC = '907'                                        00004210
                  MOVE LIG-09PARIS       TO IIT011-DATA                 00004220
               ELSE                                                     00004230
                  MOVE LIG-09            TO IIT011-DATA                 00004240
               END-IF.                                                  00004250
               PERFORM EDIT-LIGNE.                                      00004260
                                                                        00004270
      *-----------------------------------------------------------------00004280
      * EDITION D'UNE LIGNE                                             00004290
      *-----------------------------------------------------------------00004300
       EDIT-LIGNE.                                                      00004310
      *                                                                 00004320
      * MODIF DU 21/05/2001 M.BRAULT                                    00004330
      *                                                                 00004340
      * ---> ON NE PASSE PLUS PAR LA PROCEDURE FEGD36 POUR LES FILIALES 00004350
      *                                                                 00004360
      *                                                                 00004370
           WRITE      IIT011-ENREG.                                     00004380
           ADD 1 TO W-ECRITS-IIT011.                                    00004390
      *    SI ENTREPOT OU SOCIETE 907 ECRIT SUR IIT011                  00004400
      *    IF FEDG36-NLIEU NOT < '090'                                  00004410
      *    OR W-NSOC         =   '907'                                  00004420
      *       WRITE      IIT011-ENREG                                   00004430
      *       ADD 1 TO W-ECRITS-IIT011                                  00004440
      *    ELSE                                                         00004450
      *                                                                 00004460
      *    SINON       ECRIT SUR FEDG36 FICHIER EDITION 36              00004470
      *                                                                 00004480
      *       ADD  1             TO FEDG36-NSEQ                         00004490
      *       MOVE IIT011-ASA    TO FEDG36-ASA                          00004500
      *       MOVE IIT011-DATA   TO FEDG36-DATA                         00004510
      *       WRITE                 FEDG36-ENREG FROM FEDG36-WORK       00004520
      *       ADD  1             TO W-ECRITS-FEDG36                     00004530
      *    END-IF.                                                      00004540
           EVALUATE IIT011-ASA                                          00004550
              WHEN '1'        ADD  1 TO W-PAGE-IIT011                   00004560
                              MOVE 1 TO W-NLIG                          00004570
              WHEN ' '        ADD  1 TO W-NLIG                          00004580
              WHEN '0'        ADD  2 TO W-NLIG                          00004590
              WHEN '-'        ADD  3 TO W-NLIG                          00004600
           END-EVALUATE.                                                00004610
       FIN-EDIT-LIGNE. EXIT.                                            00004620
      *-----------------------------------------------------------------00004630
      * INVERSION DATE                                                  00004640
      *-----------------------------------------------------------------00004650
       T-DATE.                                                          00004660
           STRING W-JJ '/' W-MM '/' W-AA '/'                            00004670
                  DELIMITED SIZE INTO W-JJ-MM-AA.                       00004680
       FIN-T-DATE. EXIT.                                                00004690
                                                                        00004700
      *-----------------------------------------------------------------00004710
      * FIN POUR ABEND                                                  00004720
      *-----------------------------------------------------------------00004730
       FIN-ANORMALE.                                                    00004740
           PERFORM  FERMETURE-FICHIERS.                                 00004750
           MOVE  'BIT011'                  TO  ABEND-PROG.              00004760
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                00004770
       FIN-FIN-ANORMALE.    EXIT.                                       00004780
