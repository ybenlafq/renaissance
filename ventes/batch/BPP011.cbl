      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       ID DIVISION.                                                             
       PROGRAM-ID. BPP011.                                                      
      *****************************************************************         
      *** - CUMUL DE IPP010A ET IPP010B EN IPP010                     *         
      ***      ET DE IPP015A ET IPP015B EN IPP015                     *         
      *****************************************************************         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       INPUT-OUTPUT  SECTION.                                                   
       FILE-CONTROL.                                                            
      *    FICHIER D'ENTETE A DE DE IPP01XB                                     
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IPP01XA ASSIGN TO IPP01XA.                                    
      *--                                                                       
           SELECT IPP01XA ASSIGN TO IPP01XA                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *    FICHIER A COMPLETER                                                  
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IPP01XB ASSIGN TO IPP01XB.                                    
      *--                                                                       
           SELECT IPP01XB ASSIGN TO IPP01XB                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *    FICHIER DE SORTIE = IPP010                                           
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IPP01X  ASSIGN TO IPP01X.                                     
      *--                                                                       
           SELECT IPP01X  ASSIGN TO IPP01X                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  IPP01XA                                                              
           RECORDING MODE IS F                                                  
           BLOCK CONTAINS 0                                                     
           LABEL RECORD STANDARD                                                
           DATA RECORD IPP01XA-DSECT.                                           
       01  IPP01XA-DSECT    PIC X(80).                                          
      *                                                                         
       FD  IPP01XB                                                              
           RECORDING MODE IS F                                                  
           BLOCK CONTAINS 0                                                     
           LABEL RECORD STANDARD                                                
           DATA RECORD IPP01XB-DSECT.                                           
       01  IPP01XB-DSECT    PIC X(512).                                         
      *                                                                         
       FD  IPP01X                                                               
           RECORDING MODE IS F                                                  
           BLOCK CONTAINS 0                                                     
           LABEL RECORD STANDARD                                                
           DATA RECORD IPP01X-DSECT.                                            
       01  IPP01X-DSECT     PIC X(512).                                         
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
       01  W-DONNEES.                                                           
           02 NOM-MODULE            PIC X(8).                                   
           02 CTR-MASQUE            PIC ZZZZ9.                                  
           02 CTR-IPP01XA           PIC S9(5) COMP-3.                           
           02 CTR-IPP01XB           PIC S9(5) COMP-3.                           
           02 CTR-IPP01X            PIC S9(5) COMP-3.                           
      *                                                                         
           02 IPP01XA-FLAG          PIC 9.                                      
              88 IPP01XA-NON-FIN          VALUE 0.                              
              88 IPP01XA-FIN              VALUE 1.                              
           02 IPP01XB-FLAG          PIC 9.                                      
              88 IPP01XB-NON-FIN          VALUE 0.                              
              88 IPP01XB-FIN              VALUE 1.                              
           02 ETAT-FLAG             PIC 9 VALUE 0.                              
              88 ETAT-IPP010              VALUE 1.                              
              88 ETAT-IPP015              VALUE 2.                              
      *                                                                         
      *****DSECT DE IPP01XA                                                     
       01  DSECT-IPP01XA.                                                       
           05 IPP01XA-NSOCMAG       PIC X(6).                                   
           05 IPP01XA-NOMETAT       PIC X(6).                                   
           05 FILLER                PIC X(68).                                  
      *                                                                         
      *****DSECT DE IPP010A                                                     
       01  DSECT-IPP010A.                                                       
           05 IPP010A-NSOCTITRE     PIC X(3).                                   
           05 IPP010A-NMAGTITRE     PIC X(3).                                   
           05 IPP010A-NOMETAT       PIC X(6).                                   
           05 IPP010A-CGRPMAG       PIC X(2).                                   
           05 IPP010A-LLIEU         PIC X(20).                                  
           05 IPP010A-PAY-ARTI      PIC S9(7) COMP-3.                           
           05 IPP010A-PAY-PRES      PIC S9(7) COMP-3.                           
           05 IPP010A-PAY-CONT      PIC S9(7) COMP-3.                           
           05 IPP010A-PAY-PAMM      PIC S9(7) COMP-3.                           
           05 IPP010A-GEN-ARTI      PIC S9(7) COMP-3.                           
           05 IPP010A-GEN-PRES      PIC S9(7) COMP-3.                           
           05 IPP010A-GEN-CONT      PIC S9(7) COMP-3.                           
           05 IPP010A-GEN-PAMM      PIC S9(7) COMP-3.                           
           05 IPP010A-MMSSAA        PIC X(8).                                   
           05 FILLER                PIC X(6).                                   
      *                                                                         
      *****DSECT DE IPP015A                                                     
       01  DSECT-IPP015A.                                                       
           05 IPP015A-NSOCTITRE     PIC X(3).                                   
           05 IPP015A-NMAGTITRE     PIC X(3).                                   
           05 IPP015A-NOMETAT       PIC X(6).                                   
           05 IPP015A-CGRPMAG       PIC X(2).                                   
           05 IPP015A-LLIEU         PIC X(20).                                  
           05 IPP015A-PAY-VOLU      PIC S9(7) COMP-3.                           
           05 IPP015A-PAY-CA        PIC S9(7) COMP-3.                           
           05 IPP015A-PAY-TOT       PIC S9(7) COMP-3.                           
           05 IPP015A-GEN-VOLU      PIC S9(7) COMP-3.                           
           05 IPP015A-GEN-CA        PIC S9(7) COMP-3.                           
           05 IPP015A-GEN-TOT       PIC S9(7) COMP-3.                           
           05 IPP015A-MMSSAA        PIC X(8).                                   
           05 FILLER                PIC X(14).                                  
      *                                                                         
      *****DSECT DE IPP01XB                                                     
       01  DSECT-IPP01XB.                                                       
           05 IPP01XB-NOMETAT       PIC X(6).                                   
           05 IPP01XB-NSOCTITRE     PIC X(3).                                   
           05 FILLER                PIC X(2).                                   
           05 IPP01XB-NMAGTITRE     PIC X(3).                                   
           05 FILLER                PIC X(498).                                 
      *EN PLUS                                                                  
       01  IPP01XB-NSOCMAG.                                                     
           05 IPP01XB-NSOC          PIC X(3).                                   
           05 IPP01XB-NMAG          PIC X(3).                                   
      *                                                                         
      *****DSECT DE IPP010                                                      
           COPY IPP010.                                                         
      *                                                                         
      *****DSECT DE IPP015                                                      
           COPY IPP015.                                                         
      *                                                                         
      *****ZONES D'APPEL AU MODULE D'ABEND                                      
           COPY ABENDCOP.                                                       
           COPY WORKDATC.                                                       
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
       MODULE-BPP011    SECTION.                                                
      *-------------                                                            
           PERFORM MODULE-ENTREE.                                               
           PERFORM MODULE-TRAITEMENT.                                           
           PERFORM MODULE-SORTIE.                                               
           GOBACK.                                                              
      *                                                                         
      *================================================================*        
       MODULE-ENTREE        SECTION.                                            
      *================================================================*        
           DISPLAY '*--- DEBUT DU PROGRAMME BPP011 ------------------*'         
           INITIALIZE W-DONNEES                                                 
                      DSECT-IPP01XA DSECT-IPP010A DSECT-IPP015A                 
                      DSECT-IPP01XB                                             
                                    DSECT-IPP010  DSECT-IPP015                  
      *                                                                         
           OPEN INPUT   IPP01XA IPP01XB                                         
                OUTPUT  IPP01X.                                                 
      *                                                                         
           SET IPP01XA-NON-FIN TO TRUE                                          
           PERFORM LECTURE-IPP01XA                                              
           SET IPP01XB-NON-FIN TO TRUE                                          
           PERFORM LECTURE-IPP01XB                                              
      *                                                                         
           IF (IPP01XA-NOMETAT = 'IPP010' AND                                   
               IPP01XB-NOMETAT = 'IPP010')                OR                    
              (IPP01XA-NOMETAT = 'IPP010' AND IPP01XB-FIN) OR                   
              (IPP01XA-FIN AND IPP01XB-NOMETAT = 'IPP010')                      
              SET ETAT-IPP010 TO TRUE                                           
           ELSE                                                                 
              IF (IPP01XA-NOMETAT = 'IPP015' AND                                
                  IPP01XB-NOMETAT = 'IPP015')                OR                 
                 (IPP01XA-NOMETAT = 'IPP015' AND IPP01XB-FIN) OR                
                 (IPP01XA-FIN AND IPP01XB-NOMETAT = 'IPP015')                   
                 SET ETAT-IPP015 TO TRUE                                        
              ELSE                                                              
                 MOVE 'NOM D''ETAT INCORRECT' TO ABEND-MESS                     
                 PERFORM PLANTAGE                                               
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
      *================================================================*        
       MODULE-TRAITEMENT        SECTION.                                        
      *================================================================*        
           PERFORM UNTIL IPP01XA-FIN                                            
              IF IPP01XB-NON-FIN                                                
                 IF IPP01XA-NSOCMAG < IPP01XB-NSOCMAG                           
                    PERFORM WRITE-IPP01X-A                                      
                 ELSE                                                           
                    PERFORM UNTIL IPP01XB-FIN OR                                
                                  IPP01XB-NSOCMAG > IPP01XA-NSOCMAG             
                       IF IPP01XB-NSOCMAG NOT = IPP01XA-NSOCMAG                 
                          DISPLAY 'NSOC NON EGALES : ' IPP01XA-NSOCMAG          
                          ' ET ' IPP01XB-NSOCMAG                                
                          STRING  'ERREUR PGM,NON EGAL'                         
                          IPP01XA-NSOCMAG '/' IPP01XB-NSOCMAG                   
                          DELIMITED BY SIZE INTO ABEND-MESS                     
                          PERFORM PLANTAGE                                      
                       END-IF                                                   
                       PERFORM WRITE-IPP01X-A-B                                 
                       PERFORM LECTURE-IPP01XB                                  
                    END-PERFORM                                                 
                 END-IF                                                         
              ELSE                                                              
                 PERFORM WRITE-IPP01X-A                                         
              END-IF                                                            
      *                                                                         
              PERFORM LECTURE-IPP01XA                                           
           END-PERFORM.                                                         
      *                                                                         
      ******************************************************************        
      *=============== O R D R E S   E / S ============================*        
      ******************************************************************        
       LECTURE-IPP01XA   SECTION.                                               
      *--------------                                                           
           READ IPP01XA INTO DSECT-IPP01XA AT END                               
              SET IPP01XA-FIN TO TRUE                                           
           END-READ.                                                            
           IF IPP01XA-NON-FIN                                                   
              ADD 1 TO CTR-IPP01XA                                              
           END-IF.                                                              
      *                                                                         
       LECTURE-IPP01XB   SECTION.                                               
      *--------------                                                           
           READ IPP01XB INTO DSECT-IPP01XB AT END                               
              SET IPP01XB-FIN TO TRUE                                           
           END-READ.                                                            
           IF IPP01XB-NON-FIN                                                   
              ADD 1 TO CTR-IPP01XB                                              
           END-IF.                                                              
           MOVE IPP01XB-NSOCTITRE TO IPP01XB-NSOC                               
           MOVE IPP01XB-NMAGTITRE TO IPP01XB-NMAG.                              
      *                                                                         
       WRITE-IPP01X-A     SECTION.                                              
      *--------------                                                           
           IF ETAT-IPP010                                                       
              PERFORM WRITE-IPP010-A                                            
           ELSE                                                                 
              PERFORM WRITE-IPP015-A                                            
           END-IF.                                                              
           ADD 1 TO CTR-IPP01X.                                                 
      *                                                                         
       WRITE-IPP01X-A-B   SECTION.                                              
      *----------------                                                         
           IF ETAT-IPP010                                                       
              PERFORM WRITE-IPP010-A-B                                          
           ELSE                                                                 
              PERFORM WRITE-IPP015-A-B                                          
           END-IF.                                                              
           ADD 1 TO CTR-IPP01X.                                                 
      *                                                                         
       WRITE-IPP010-A    SECTION.                                               
      *--------------                                                           
           INITIALIZE DSECT-IPP010                                              
           MOVE DSECT-IPP01XA TO DSECT-IPP010A                                  
           MOVE 'IPP010'          TO NOMETAT-IPP010                             
           MOVE 1                 TO IPP010-SEQUENCE                            
           MOVE IPP010A-MMSSAA    TO IPP010-MMSSAA                              
      *                                                                         
           MOVE IPP010A-NSOCTITRE     TO IPP010-NSOCTITRE                       
           MOVE IPP010A-CGRPMAG       TO IPP010-CGRPMAG                         
           MOVE IPP010A-NMAGTITRE     TO IPP010-NMAGTITRE                       
           MOVE IPP010A-LLIEU         TO IPP010-LLIEU                           
      *                                                                         
           MOVE IPP010A-PAY-ARTI      TO IPP010-PAY-ARTI                        
           MOVE IPP010A-PAY-PRES      TO IPP010-PAY-PRES                        
           MOVE IPP010A-PAY-CONT      TO IPP010-PAY-CONT                        
           MOVE IPP010A-PAY-PAMM      TO IPP010-PAY-PAMM                        
           MOVE IPP010A-GEN-ARTI      TO IPP010-GEN-ARTI                        
           MOVE IPP010A-GEN-PRES      TO IPP010-GEN-PRES                        
           MOVE IPP010A-GEN-CONT      TO IPP010-GEN-CONT                        
           MOVE IPP010A-GEN-PAMM      TO IPP010-GEN-PAMM                        
           WRITE IPP01X-DSECT FROM DSECT-IPP010.                                
      *                                                                         
       WRITE-IPP015-A    SECTION.                                               
      *--------------                                                           
           INITIALIZE DSECT-IPP015                                              
           MOVE DSECT-IPP01XA TO DSECT-IPP015A                                  
           MOVE 'IPP015'          TO NOMETAT-IPP015                             
           MOVE 1                 TO IPP015-SEQUENCE                            
           MOVE IPP015A-MMSSAA    TO IPP015-MMSSAA                              
      *                                                                         
           MOVE IPP015A-NSOCTITRE     TO IPP015-NSOCTITRE                       
           MOVE IPP015A-CGRPMAG       TO IPP015-CGRPMAG                         
           MOVE IPP015A-NMAGTITRE     TO IPP015-NMAGTITRE                       
           MOVE IPP015A-LLIEU         TO IPP015-LLIEU                           
      *                                                                         
           MOVE IPP015A-PAY-VOLU      TO IPP015-PAY-VOLU                        
           MOVE IPP015A-PAY-CA        TO IPP015-PAY-CA                          
           MOVE IPP015A-PAY-TOT       TO IPP015-PAY-TOT                         
           MOVE IPP015A-GEN-VOLU      TO IPP015-GEN-VOLU                        
           MOVE IPP015A-GEN-CA        TO IPP015-GEN-CA                          
           MOVE IPP015A-GEN-TOT       TO IPP015-GEN-TOT                         
           WRITE IPP01X-DSECT FROM DSECT-IPP015.                                
      *                                                                         
       WRITE-IPP010-A-B   SECTION.                                              
      *----------------                                                         
           INITIALIZE DSECT-IPP010                                              
           MOVE DSECT-IPP01XB TO DSECT-IPP010                                   
      *                                                                         
           MOVE DSECT-IPP01XA TO DSECT-IPP010A                                  
           MOVE IPP010A-CGRPMAG       TO IPP010-CGRPMAG                         
           MOVE IPP010A-LLIEU         TO IPP010-LLIEU                           
      *                                                                         
           MOVE IPP010A-PAY-ARTI      TO IPP010-PAY-ARTI                        
           MOVE IPP010A-PAY-PRES      TO IPP010-PAY-PRES                        
           MOVE IPP010A-PAY-CONT      TO IPP010-PAY-CONT                        
           MOVE IPP010A-PAY-PAMM      TO IPP010-PAY-PAMM                        
           MOVE IPP010A-GEN-ARTI      TO IPP010-GEN-ARTI                        
           MOVE IPP010A-GEN-PRES      TO IPP010-GEN-PRES                        
           MOVE IPP010A-GEN-CONT      TO IPP010-GEN-CONT                        
           MOVE IPP010A-GEN-PAMM      TO IPP010-GEN-PAMM                        
           WRITE IPP01X-DSECT FROM DSECT-IPP010.                                
      *                                                                         
       WRITE-IPP015-A-B    SECTION.                                             
      *----------------                                                         
           INITIALIZE DSECT-IPP015                                              
           MOVE DSECT-IPP01XB TO DSECT-IPP015                                   
      *                                                                         
           MOVE DSECT-IPP01XA TO DSECT-IPP015A                                  
           MOVE IPP015A-CGRPMAG       TO IPP015-CGRPMAG                         
           MOVE IPP015A-LLIEU         TO IPP015-LLIEU                           
      *                                                                         
           MOVE IPP015A-PAY-VOLU      TO IPP015-PAY-VOLU                        
           MOVE IPP015A-PAY-CA        TO IPP015-PAY-CA                          
           MOVE IPP015A-PAY-TOT       TO IPP015-PAY-TOT                         
           MOVE IPP015A-GEN-VOLU      TO IPP015-GEN-VOLU                        
           MOVE IPP015A-GEN-CA        TO IPP015-GEN-CA                          
           MOVE IPP015A-GEN-TOT       TO IPP015-GEN-TOT                         
           WRITE IPP01X-DSECT FROM DSECT-IPP015.                                
      *                                                                         
      *================================================================*        
       MODULE-SORTIE      SECTION.                                              
      *================================================================*        
           CLOSE  IPP01XA IPP01XB IPP01X.                                       
           IF ETAT-IPP010                                                       
              MOVE CTR-IPP01XA TO CTR-MASQUE                                    
              DISPLAY '*--- NBRE LECTURES  IPP010A   : ' CTR-MASQUE             
              MOVE CTR-IPP01XB TO CTR-MASQUE                                    
              DISPLAY '*--- NBRE LECTURES  IPP010B   : ' CTR-MASQUE             
              MOVE CTR-IPP01X TO CTR-MASQUE                                     
              DISPLAY '*--- NBRE ECRITURES IPP010   : ' CTR-MASQUE              
           END-IF.                                                              
           IF ETAT-IPP015                                                       
              MOVE CTR-IPP01XA TO CTR-MASQUE                                    
              DISPLAY '*--- NBRE LECTURES  IPP015A   : ' CTR-MASQUE             
              MOVE CTR-IPP01XB TO CTR-MASQUE                                    
              DISPLAY '*--- NBRE LECTURES  IPP015B   : ' CTR-MASQUE             
              MOVE CTR-IPP01X TO CTR-MASQUE                                     
              DISPLAY '*--- NBRE ECRITURES IPP015   : ' CTR-MASQUE              
           END-IF.                                                              
           DISPLAY '*--- FIN   DU PROGRAMME BPP011 ------------------*'.        
      *                                                                         
       PLANTAGE   SECTION.                                                      
      *--------                                                                 
           CLOSE  IPP01XA IPP01XB IPP01X.                                       
           IF CTR-IPP01XA > 0                                                   
              DISPLAY 'LORS DU PLANTAGE,NUM LIGNE IPP01XA :' CTR-IPP01XA        
              DISPLAY 'LORS DU PLANTAGE,NUM LIGNE IPP01XB :'                    
                                                  CTR-IPP01XB                   
           END-IF                                                               
           DISPLAY '*--------------------*'                                     
           DISPLAY '   P L A N T A G E    '                                     
           DISPLAY '*--------------------*'                                     
           MOVE 'BPP011'   TO ABEND-PROG.                                       
           MOVE 'ABEND'    TO NOM-MODULE.                                       
           CALL NOM-MODULE USING ABEND-PROG ABEND-MESS.                         
      *                                                                 00000000
