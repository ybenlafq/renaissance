      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.      BCE618.                                                 
       AUTHOR.     AD - APSIDE.                                                 
      ******************************************************************        
      ******************************************************************        
      *               F I C H E   D E S C R I P T I V E                *        
      ******************************************************************        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : ETIQUETTE - ASSORTIMENT                         *        
      *  PROGRAMME   : BCE618                                          *        
      *  CREATION    : 01/02/2012                                      *        
      *  FONCTION    : CONTITUTION DU FICHIER D'ASSORTIMENT            *        
      *                                                                *        
      *  EN ENTREE   :                                                 *        
      *                FCE618S : FICHIER D'ASSORTIMENT DU STOCK ET     *        
      *                          TRANSIT ISSU DE LA KLEE3              *        
      *                FCE618D : FICHIER D'ASSORTIMENT DU DEMO         *        
      *                          ( UNLOAD SL11/CE13)                   *        
      *                                                                *        
      *  EN SORTIE   :                                                 *        
      *                FCE618  : FICHIER D'ASSORTIMENT ENVOYE          *        
      *                          DANS LA KLEE6                         *        
      *                                                                *        
      ******************************************************************        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT  SECTION.                                                   
      *                                                                         
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FCE618S       ASSIGN  TO  FCE618S.                           
      *--                                                                       
           SELECT  FCE618S       ASSIGN  TO  FCE618S                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FCE618D       ASSIGN  TO  FCE618D.                           
      *--                                                                       
           SELECT  FCE618D       ASSIGN  TO  FCE618D                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FCE618        ASSIGN  TO  FCE618.                            
      *--                                                                       
           SELECT  FCE618        ASSIGN  TO  FCE618                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      *--- FICHIER EN ENTREE                                                    
      *                                                                         
      *------ FICHIER FCE618S                                                   
       FD  FCE618S                                                              
           RECORDING F                                                          
           BLOCK 0  RECORDS                                                     
           LABEL RECORD STANDARD.                                               
       01  FCE618S-ENREG                PIC X(14).                              
      *                                                                         
      *------ FICHIER FCE618D                                                   
       FD  FCE618D                                                              
           RECORDING F                                                          
           BLOCK 0  RECORDS                                                     
           LABEL RECORD STANDARD.                                               
       01  FCE618D-ENREG                PIC X(14).                              
      *                                                                         
      *--- FICHIER EN SORTIE                                                    
      *                                                                         
      *------ FICHIER FCE618                                                    
       FD  FCE618                                                               
           RECORDING F                                                          
           BLOCK 0  RECORDS                                                     
           LABEL RECORD STANDARD.                                               
       01  FCE618-ENREG                  PIC X(15).                             
      *                                                                         
      *---------------------------------------------------------------*         
      *                 W O R K I N G  -  S T O R A G E               *         
      *---------------------------------------------------------------*         
       WORKING-STORAGE SECTION.                                                 
           COPY SYBWDIV0.                                                       
      *--------------------------------------------------------------*          
      *  ZONES D'INTERFACE POUR LES ACCES AUX DATA BASES DB2/SQL     *          
      *--------------------------------------------------------------*          
      *    COPY SYKWSQ10.                                                       
      *    COPY ZLIBERRG.                                                       
      *                                                                         
      *    EXEC SQL INCLUDE SQLCA     END-EXEC.                                 
      *--- TABLE DE PARAMETRAGE DES LIEUX                                       
      *    EXEC SQL INCLUDE RVLI0000  END-EXEC.                                 
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES SPECIFIQUES PROGRAMME                                 *          
      *--------------------------------------------------------------*          
      *                                                                         
      *----DSECT FICHIER                                                        
      *                                                                         
       01 W-FCE618-ENREG.                                                       
          03 W-FCE618-CLE               PIC X(13).                              
          03 W-FCE618-WSTOCK            PIC X(01).                              
          03 W-FCE618-WDEMO             PIC X(01).                              
      *                                                                         
       01 W-FCE618S-ENREG.                                                      
          05 W-FCE618S-CLE              PIC X(13).                              
          05 W-FCE618S-WSTOCK           PIC X(01).                              
      *                                                                         
       01 W-FCE618D-ENREG.                                                      
          05 W-FCE618D-CLE              PIC X(13).                              
          05 W-FCE618D-WDEMO            PIC X(01).                              
      *----COMPTEURS D'ENREGISTREMENTS                                          
      *                                                                         
       01 CPT-GENERAL.                                                          
          03 CPT-CE18S-LU        PIC 9(08) COMP-3   VALUE ZERO.                 
          03 CPT-CE18D-LU        PIC 9(08) COMP-3   VALUE ZERO.                 
          03 CPT-ECRIT           PIC 9(08) COMP-3   VALUE ZERO.                 
      *                                                                         
       01  PIC-EDIT              PIC Z(07)9.                                    
      *                                                                         
      *--- VARIABLES                                                            
      *                                                                         
       01  W-DATE-DU-JOUR        PIC X(08) VALUE SPACES.                        
      *                                                                         
      *--- STATISTIQUES DE TRAITEMENT                                           
      *                                                                         
       01 CR-STAT.                                                              
          03 CR-TIME.                                                           
             05 CR-HEURE         PIC 9(2).                                      
             05 CR-MINUTE        PIC 9(2).                                      
             05 CR-SECONDE       PIC 9(2).                                      
             05 FILLER           PIC 9(2).                                      
          03 CR-DUREE            PIC 9(8).                                      
          03 CR-DATE             PIC 9(6).                                      
          03 CR-JOUR-DEBUT       PIC 9(5).                                      
          03 CR-JOUR-FIN         PIC 9(5).                                      
      *                                                                         
      *--- FLAG                                                                 
      *                                                                         
       01 F-FCE618S          PIC 9     VALUE 0.                                 
          88 DEBUT-CE618S              VALUE 0.                                 
          88 FIN-CE618S                VALUE 1.                                 
      *                                                                         
       01 F-FCE618D          PIC 9     VALUE 0.                                 
          88 DEBUT-CE618D              VALUE 0.                                 
          88 FIN-CE618D                VALUE 1.                                 
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES BUFFER D'ENTREE/SORTIE                                *          
      *--------------------------------------------------------------*          
       01 FILLER  PIC X(16)   VALUE '*** Z-INOUT ****'.                         
       01 Z-INOUT PIC X(4096) VALUE SPACE.                                      
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES DE GESTION DES ERREURS                                *          
      *--------------------------------------------------------------*          
           COPY SYBWERRO.                                                       
      *                                                                         
      *--------------------------------------------------------------*          
      *  MODULE ABEND / BETDATC / BFTNPER                            *          
      *--------------------------------------------------------------*          
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *01  ABEND  PIC X(8) VALUE 'ABEND'.                                       
      *--                                                                       
       01  MW-ABEND  PIC X(8) VALUE 'ABEND'.                                    
      *}                                                                        
           COPY ABENDCOP.                                                       
      *                                                                         
       01 W-SQL-MESSAGE.                                                        
          03 W-SQL-TABLE              PIC X(06)      VALUE SPACES.              
          03 FILLER                   PIC X(03)      VALUE ' : '.               
          03 W-SQL-FONCTION           PIC X(06)      VALUE SPACES.              
          03 FILLER                   PIC X(03)      VALUE ' : '.               
          03 W-SQL-CODE               PIC +ZZ9.                                 
          03 FILLER                   PIC X(03)      VALUE ' : '.               
          03 W-SQL-CLEF               PIC X(55)      VALUE SPACES.              
       01 W-SQL-MESS.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 W-SQL-LRECL              PIC S9(8) COMP VALUE +72.                 
      *--                                                                       
          05 W-SQL-LRECL              PIC S9(8) COMP-5 VALUE +72.               
      *}                                                                        
          05 W-SQL-MSGS.                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10  W-SQL-LGTH           PIC S9(4) COMP VALUE +288.                
      *--                                                                       
             10  W-SQL-LGTH           PIC S9(4) COMP-5 VALUE +288.              
      *}                                                                        
             10  W-SQL-MSG            PIC X(72) OCCURS 4.                       
      *                                                                         
       01  BETDATC PIC X(10) VALUE 'BETDATC'.                                   
           COPY WORKDATC.                                                       
      *                                                                         
      *****************************************************************         
      *             P R O C E D U R E    D I V I S I O N              *         
      *****************************************************************         
       PROCEDURE DIVISION.                                                      
      *-------------------                                                      
       MODULE-BCE618.                                                           
      *--------------                                                           
      *                                                                         
           PERFORM MODULE-ENTREE                                                
           PERFORM TRAITEMENT-ASSORTIMENT                                       
           PERFORM AFFICHAGE-COMPTEUR                                           
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
       FIN-MODULE-BCE618.                 EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       MODULE-ENTREE                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-          BIENVENUE DANS LE PROGRAMME          -'          
           DISPLAY '-                    BCE618                     -'          
           DISPLAY '-------------------------------------------------'          
      *                                                                         
           MOVE 'BCE618' TO  ABEND-PROG                                         
      *                                                                         
           ACCEPT CR-DATE FROM DATE                                             
           ACCEPT CR-TIME FROM TIME                                             
           ACCEPT CR-JOUR-DEBUT FROM DAY                                        
      *                                                                         
           COMPUTE CR-DUREE  = CR-HEURE * 3600                                  
                             + CR-MINUTE * 60 + CR-SECONDE                      
      *                                                                         
           DISPLAY '-  NOUS SOMMES LE ' CR-DATE(5:2) '/' CR-DATE(3:2)           
                                       '/' CR-DATE(1:2) '  '                    
                   '-  IL EST ' CR-HEURE ':' CR-MINUTE ':' CR-SECONDE           
                   '  -'                                                        
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-                                               -'          
      *                                                                         
      *----OUVERTURE DES FICHIERS                                               
           OPEN INPUT   FCE618S   FCE618D.                                      
           OPEN OUTPUT  FCE618.                                                 
      *                                                                         
       FIN-MODULE-ENTREE.                 EXIT.                                 
      *                                                                         
      *****************************************************************         
      *             M O D U L E - T R A I T E M E N T                 *         
      *****************************************************************         
      *----------------------------------------                                 
       TRAITEMENT-ASSORTIMENT          SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
      * VOILA CE QUE LE TRAITEMENT EST CENSE FAIRE :                            
      *   ON COMPARE LE FICHIER DE LA STRUCTURE J-1 AVEC LE                     
      *   FICHIER DE LA STRUCTURE J. POUR CHAQUE CHANGEMENT ON                  
      *   ALIMENTE LE FICHIER DE SORTIE                                         
      *                                                                         
           SET DEBUT-CE618S TO TRUE                                             
           SET DEBUT-CE618D TO TRUE                                             
      *                                                                         
           INITIALIZE W-FCE618S-ENREG                                           
                      W-FCE618D-ENREG                                           
                      W-FCE618-ENREG                                            
      *                                                                         
           PERFORM LECTURE-FCE618S                                              
           PERFORM LECTURE-FCE618D                                              
      *                                                                         
           IF FIN-CE618S THEN                                                   
             DISPLAY '-------------------------------------------------'        
             DISPLAY '- !!! ATTENTION !!! FICHIER FCE18S VIDE !!!     -'        
             DISPLAY '-------------------------------------------------'        
             MOVE 'FICHIER FCE18S VIDE' TO ABEND-MESS                           
             PERFORM SORTIE-ANORMALE                                            
           END-IF                                                               
      *                                                                         
           IF FIN-CE618D THEN                                                   
             DISPLAY '-------------------------------------------------'        
             DISPLAY '- !!! ATTENTION !!! FICHIER FCE18D VIDE !!!     -'        
             DISPLAY '-------------------------------------------------'        
      *                                                                         
      *----  ON ALIMENTE LE FICHIER D'ASSORTIMENT AVEC LE FICHIER STOCK         
             PERFORM UNTIL FIN-CE618S                                           
                MOVE W-FCE618S-CLE     TO  W-FCE618-CLE                         
                MOVE W-FCE618S-WSTOCK  TO  W-FCE618-WSTOCK                      
                PERFORM ECRITURE-ASSORTIMENT                                    
                PERFORM LECTURE-FCE618S                                         
             END-PERFORM                                                        
           END-IF                                                               
      *                                                                         
           PERFORM UNTIL FIN-CE618S                                             
              IF W-FCE618S-CLE = W-FCE618D-CLE THEN                             
                 MOVE W-FCE618S-CLE     TO  W-FCE618-CLE                        
                 MOVE W-FCE618S-WSTOCK  TO  W-FCE618-WSTOCK                     
                 MOVE W-FCE618D-WDEMO   TO  W-FCE618-WDEMO                      
                 PERFORM ECRITURE-ASSORTIMENT                                   
                 PERFORM LECTURE-FCE618S                                        
                 PERFORM LECTURE-FCE618D                                        
              END-IF                                                            
      *                                                                         
              IF W-FCE618S-CLE > W-FCE618D-CLE AND NOT FIN-CE618S THEN          
                 MOVE W-FCE618D-CLE     TO  W-FCE618-CLE                        
                 MOVE W-FCE618D-WDEMO   TO  W-FCE618-WDEMO                      
                 PERFORM ECRITURE-ASSORTIMENT                                   
                 PERFORM LECTURE-FCE618D                                        
              END-IF                                                            
      *                                                                         
              IF W-FCE618S-CLE < W-FCE618D-CLE AND NOT FIN-CE618S THEN          
                 MOVE W-FCE618S-CLE     TO  W-FCE618-CLE                        
                 MOVE W-FCE618S-WSTOCK  TO  W-FCE618-WSTOCK                     
                 PERFORM ECRITURE-ASSORTIMENT                                   
                 PERFORM LECTURE-FCE618S                                        
              END-IF                                                            
      *                                                                         
           END-PERFORM                                                          
      *                                                                         
      *--- IL FAUT TRAITER LE RESTE EVENTUEL DU FICHIER DEMO                    
           PERFORM UNTIL FIN-CE618D                                             
              MOVE W-FCE618D-CLE     TO  W-FCE618-CLE                           
              MOVE W-FCE618D-WDEMO   TO  W-FCE618-WDEMO                         
              PERFORM ECRITURE-ASSORTIMENT                                      
              PERFORM LECTURE-FCE618S                                           
           END-PERFORM.                                                         
      *                                                                         
       FIN-TRAITEMENT-ASSORTIMENT.        EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       ECRITURE-ASSORTIMENT            SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           ADD  1  TO  CPT-ECRIT                                                
           WRITE FCE618-ENREG FROM W-FCE618-ENREG                               
           INITIALIZE W-FCE618-ENREG.                                           
      *                                                                         
       FIN-ECRITURE-ASSORTIMENT.          EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       LECTURE-FCE618S                 SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           INITIALIZE W-FCE618S-ENREG                                           
      *                                                                         
           READ FCE618S                                                         
              AT END SET FIN-CE618S TO TRUE                                     
                     MOVE HIGH-VALUE TO FCE618S-ENREG W-FCE618S-ENREG           
           END-READ                                                             
      *                                                                         
           IF F-FCE618S = 0 THEN                                                
              ADD 1 TO CPT-CE18S-LU                                             
              MOVE FCE618S-ENREG      TO  W-FCE618S-ENREG                       
           END-IF.                                                              
      *                                                                         
       FIN-LECTURE-FCE618S.               EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       LECTURE-FCE618D                 SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           INITIALIZE W-FCE618D-ENREG                                           
      *                                                                         
           READ FCE618D                                                         
              AT END SET FIN-CE618D TO TRUE                                     
                     MOVE HIGH-VALUE TO FCE618D-ENREG W-FCE618D-ENREG           
           END-READ                                                             
      *                                                                         
           IF F-FCE618D = 0 THEN                                                
              ADD 1 TO CPT-CE18D-LU                                             
              MOVE FCE618D-ENREG      TO  W-FCE618D-ENREG                       
           END-IF.                                                              
      *                                                                         
       FIN-LECTURE-FCE618D.               EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       AFFICHAGE-COMPTEUR              SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           DISPLAY '-------------------------------------------------'          
           DISPLAY '- AFFICHAGE DES COMPTEURS :                     -'          
           MOVE  CPT-CE18S-LU      TO  PIC-EDIT                                 
           DISPLAY '-  NBRE DE LG ASSORTIMENT STOCK  '                          
                   PIC-EDIT '       -'                                          
           MOVE  CPT-CE18D-LU    TO  PIC-EDIT                                   
           DISPLAY '-  NBRE DE LG ASSORTIMENT DEMO   '                          
                   PIC-EDIT '       -'                                          
           MOVE  CPT-ECRIT        TO  PIC-EDIT                                  
           DISPLAY '-  NBRE DE LG FCE618 ECRITE      '                          
                   PIC-EDIT '       -'.                                         
      *                                                                         
       FIN-AFFICHAGE-COMPTEUR.            EXIT.                                 
      *                                                                         
      *                                                                         
      *----------------------------------------                                 
       MODULE-SORTIE                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           CLOSE  FCE618S FCE618D FCE618.                                       
      *                                                                         
           ACCEPT CR-TIME FROM TIME                                             
           ACCEPT CR-DATE FROM DATE                                             
           DISPLAY '-                                               -'          
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-  NOUS SOMMES LE ' CR-DATE(5:2) '/' CR-DATE(3:2)           
                                       '/' CR-DATE(1:2) '  '                    
                   '-  IL EST ' CR-HEURE ':' CR-MINUTE ':' CR-SECONDE           
                   '  -'                                                        
           COMPUTE CR-DUREE  = CR-DUREE - ( CR-HEURE * 3600                     
                             - CR-MINUTE * 60 - CR-SECONDE )                    
           ACCEPT CR-JOUR-FIN FROM DAY                                          
           IF CR-JOUR-FIN > CR-JOUR-DEBUT                                       
              COMPUTE CR-DUREE  = CR-DUREE                                      
                                + (CR-JOUR-FIN - CR-JOUR-DEBUT) * 86400         
           END-IF                                                               
           DIVIDE CR-DUREE  BY 3600 GIVING CR-HEURE  REMAINDER CR-MINUTE        
           DIVIDE CR-MINUTE BY 60 GIVING CR-MINUTE REMAINDER CR-SECONDE         
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-       DUREE TOTALE DU PROGRAMME '                         
                   CR-HEURE ':' CR-MINUTE ':' CR-SECONDE '      -'              
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-       CETTE NUIT TOUT S''EST BIEN PASSE        -'         
           DISPLAY '-                   A DEMAIN                    -'          
           DISPLAY '-------------------------------------------------'          
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-MODULE-SORTIE.                 EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       SORTIE-ANORMALE                 SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           DISPLAY '><><><><><><><><><><><><><><><><><><><><><><><><'           
           DISPLAY '><                    ' ABEND-PROG(1:6)                     
                                               '                  ><'           
           DISPLAY '><     FIN ANORMALE PROVOQUEE PAR PROGRAMME   ><'           
           DISPLAY '><><><><><><><><><><><><><><><><><><><><><><><><'           
           DISPLAY ABEND-MESS                                                   
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL ABEND USING ABEND-PROG ABEND-MESS.                              
      *--                                                                       
           CALL MW-ABEND USING ABEND-PROG ABEND-MESS.                           
      *}                                                                        
      *                                                                         
       FIN-SORTIE-ANORMALE.               EXIT.                                 
      *                                                                         
      *                                                                         
      ******************************************************************        
      ******************************************************************        
      **                       FIN                                    **        
      ******************************************************************        
      ******************************************************************        
