      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00020000
       PROGRAM-ID.                BPV800.                               00030000
       AUTHOR.                    DSA044.                               00040000
      * ************************************************************** +00050000
      *    + ------------------------------------------------------ +  *00060000
      *    !            AJOUT PRIMES OPERATEURS SUR RTPV00          !  *00070000
      *    + ------------------------------------------------------ +  *00080000
      * ************************************************************** +00090000
      *                                                                *00100000
      *  PROJET     : BIRD ENVOI DU FICHIER DES PRIMES ET COMMISSIONS  *00110000
      *  PROGRAMME  : BPV800                                           *00120000
      *  CREATION   : 22/07/09                                         *00130000
      *  FONCTION   : EXTRACTION DEPUIS LE FICHIER CREE PAR LE BPV507  *00140000
      *                                                                *00150000
      ******************************************************************00160000
      *  MAINTENANCES :                                                 00170000
      * --------------------------------------------------------------- 00180000
      * DATE          : 02-09-2010                                      00190001
      * USER          : DSA030 JACQUES BICHY                            00200001
      * MARQUE        : JB0209                                          00210001
      * LIBELLE       : MODIFICATION DU FILTRE POUR INTéGRER LES CO, SE 00220001
      ******************************************************************00230000
       ENVIRONMENT DIVISION.                                            00240000
       CONFIGURATION SECTION.                                           00250000
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                           00260000
       INPUT-OUTPUT SECTION.                                            00270000
                                                                        00280000
       FILE-CONTROL.                                                    00290000
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FPV500I ASSIGN TO FPV500I FILE STATUS IS ST-FPV500I. 00300000
      *--                                                                       
            SELECT FPV500I ASSIGN TO FPV500I FILE STATUS IS ST-FPV500I          
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FPV800O ASSIGN TO FPV800O FILE STATUS IS ST-FPV800O. 00310000
      *                                                                         
      *--                                                                       
            SELECT FPV800O ASSIGN TO FPV800O FILE STATUS IS ST-FPV800O          
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00320000
      *}                                                                        
       DATA DIVISION.                                                   00330000
       FILE SECTION.                                                    00340000
                                                                        00350000
       FD  FPV500I                                                      00360000
           RECORDING F                                                  00370000
           BLOCK 0 RECORDS                                              00380000
           LABEL RECORD STANDARD.                                       00390000
       01  FPV500I-ENR     PIC X(147).                                  00400000
                                                                        00410000
                                                                        00420000
       FD  FPV800O                                                      00430000
           RECORDING F                                                  00440000
           BLOCK 0 RECORDS                                              00450000
           LABEL RECORD STANDARD.                                       00460000
       01  FPV800O-ENR     PIC X(219).                                  00470000
                                                                        00480000
       WORKING-STORAGE SECTION.                                         00490000
      *========================                                         00500000
                                                                        00510000
       77 FILLER                            PIC X(10) VALUE '$WORKING$'.00520000
       77 FILLER                            PIC X(10) VALUE '$COMPTEU$'.00530000
                                                                        00540000
       77 W-CPT-LEC-PV500                   PIC 9(06) VALUE 0.          00550000
       77 W-CPT-ECRITURES                   PIC 9(06) VALUE 0.          00560000
                                                                        00570000
      * - ZONE RECEPTION POUR CALCULS INTERMéDIAIRES                    00580000
       01  W-MONTANT-HT             PIC S9(13)V9(02) COMP-3 VALUE ZEROS.00590000
       01  W-MONTANT-TTC            PIC S9(13)V9(02) COMP-3 VALUE ZEROS.00600000
                                                                        00610000
       01  W-MONTANT-REFIN-HT       PIC S9(13)V9(02) COMP-3.            00620000
                                                                        00630000
      *--  GESTION CARTE FPV500                                         00640000
                                                                        00650000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WW-FPV500                 PIC S9(7) COMP      VALUE ZERO.    00660000
      *--                                                                       
       01  WW-FPV500                 PIC S9(7) COMP-5      VALUE ZERO.          
      *}                                                                        
           COPY  FPV500.                                                00670000
                                                                        00680000
       01  ST-FPV500I                PIC 99              VALUE ZERO.    00690000
                                                                        00700000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WW-FPV800                 PIC S9(7) COMP      VALUE ZERO.    00710000
      *--                                                                       
       01  WW-FPV800                 PIC S9(7) COMP-5      VALUE ZERO.          
      *}                                                                        
           COPY  FPV800.                                                00720000
                                                                        00730000
       01  ST-FPV800O                PIC 99              VALUE ZERO.    00740000
                                                                        00750000
       01 ETAT-FICHIER-FPV500 PIC X VALUE '1'.                          00760000
           88 NON-FIN-FPV500  VALUE '1'.                                00770000
           88 FIN-FPV500      VALUE '0'.                                00780000
                                                                        00790000
                                                                        00800000
       01 Z-INOUT                    PIC X(236) VALUE SPACES.           00810000
           COPY SYBWERRO.                                               00820000
           COPY ABENDCOP.                                               00830000
                                                                        00840000
      ****************************************************************  00850000
      *                   DEBUT DU PROGRAMME                         *  00860000
      ****************************************************************  00870000
       PROCEDURE DIVISION.                                              00880000
      *===================                                              00890000
                                                                        00900000
           PERFORM INITIALISATION                                       00910000
              THRU FIN-INITIALISATION.                                  00920000
                                                                        00930000
           PERFORM TRAITEMENT-BPV800.                                   00940000
                                                                        00950000
           PERFORM TRAITEMENT-SORTIE                                    00960000
              THRU FIN-TRAITEMENT-SORTIE.                               00970000
                                                                        00980000
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00990000
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        01000000
      * //////////////////////////////////////////////////////////// *  01010000
      *                  SECTIONS DE LA BOUCLE PRINCIPALE            *  01020000
      * //////////////////////////////////////////////////////////// *  01030000
       INITIALISATION SECTION.                                          01040000
      *=======================                                          01050000
                                                                        01060000
           DISPLAY ' * ------------------------------------ *'.         01070000
           DISPLAY ' * INITIALISATION TRAITEMENT BPV800     *'.         01080000
           DISPLAY ' * ------------------------------------ *'.         01090000
                                                                        01100000
           OPEN INPUT FPV500I.                                          01110000
           OPEN OUTPUT FPV800O.                                         01120000
                                                                        01130000
      *    1ERE LECTURE SUR FPV500I                                     01140000
           READ FPV500I INTO W-ENR-FPV500                               01150000
                AT END SET FIN-FPV500 TO TRUE                           01160000
           IF ST-FPV500I = 10                                           01170000
              DISPLAY 'FICHIER FPV500 VIDE : SORTIE PGM'                01180000
              PERFORM TRAITEMENT-SORTIE                                 01190000
           END-IF.                                                      01200000
                                                                        01210000
       FIN-INITIALISATION.  EXIT.                                       01220000
      *-------------------                                              01230000
                                                                        01240000
                                                                        01250000
       TRAITEMENT-BPV800 SECTION.                                       01260000
      *==========================                                       01270000
                                                                        01280000
           SET NON-FIN-FPV500 TO TRUE                                   01290000
                                                                        01300000
           PERFORM UNTIL FIN-FPV500                                     01310000
                                                                        01320000
              ADD 1               TO W-CPT-LEC-PV500                    01330000
                                                                        01340000
JB0209        IF FPV500-CTYPENT = 'PS' OR 'SE'  OR 'CO'                 01350001
                 PERFORM ECRITURE-FPV800O                               01360000
              END-IF                                                    01410000
                                                                        01420000
              PERFORM LECTURE-FPV500                                    01430000
                                                                        01440000
           END-PERFORM.                                                 01450000
                                                                        01460000
                                                                        01470000
       FIN-TRAITEMENT-BPV800.   EXIT.                                   01480000
      *----------------------                                           01490000
                                                                        01500000
                                                                        01510000
       LECTURE-FPV500 SECTION.                                          01520000
      *=======================                                          01530000
                                                                        01540000
                                                                        01550000
           READ FPV500I INTO W-ENR-FPV500                               01560000
                AT END SET FIN-FPV500 TO TRUE.                          01570000
                                                                        01580000
       FIN-LECTURE-FPV500 . EXIT.                                       01590000
      *--------------------                                             01600000
                                                                        01610000
                                                                        01620000
       ECRITURE-FPV800O SECTION.                                        01630000
      *==========================                                       01640000
                                                                        01650000
           MOVE FPV500-NSOCIETE  TO FPV800-NSOCIETE                     01660000
           MOVE FPV500-NLIEU     TO FPV800-NLIEU                        01670000
           MOVE FPV500-NVENTE    TO FPV800-NVENTE                       01680000
           MOVE FPV500-NSEQNQ    TO FPV800-NSEQNQ                       01690000
           MOVE FPV500-CVENDEUR  TO FPV800-CVENDEUR                     01700000
           MOVE FPV500-DTOPE     TO FPV800-DTOPE                        01710000
           MOVE FPV500-DDELIV    TO FPV800-DDELIV                       01720000
           MOVE FPV500-DCREATION TO FPV800-DCREATION                    01730000
           MOVE FPV500-WOFFRE    TO FPV800-WOFFRE                       01740000
           MOVE FPV500-NCODICGRP TO FPV800-NCODICGRP                    01750000
           MOVE FPV500-CTYPENT   TO FPV800-CTYPENT                      01760000
           MOVE FPV500-WREMISE   TO FPV800-WREMISE                      01770000
           MOVE FPV500-NCODIC    TO FPV800-NCODIC                       01780000
           MOVE FPV500-CENREG    TO FPV800-CENREG                       01790000
           MOVE FPV500-WEP       TO FPV800-WEP                          01800000
           MOVE FPV500-WOA       TO FPV800-WOA                          01810000
           MOVE FPV500-WPSAB     TO FPV800-WPSAB                        01820000
           MOVE FPV500-WREMVTE   TO FPV800-WREMVTE                      01830000
           MOVE FPV500-NSEQENT   TO FPV800-NSEQENT                      01840000
           MOVE FPV500-QTVEND    TO FPV800-QTVEND                       01850000
           MOVE FPV500-QTCODIG   TO FPV800-QTCODIG                      01860000
           MOVE FPV500-MTVTEHT   TO FPV800-MTVTEHT                      01870000
           MOVE FPV500-MTVTETVA  TO FPV800-MTVTETVA                     01880000
           MOVE FPV500-MTFORHT   TO FPV800-MTFORHT                      01890000
           MOVE FPV500-MTFORTVA  TO FPV800-MTFORTVA                     01900000
           MOVE FPV500-MTDIFFHT  TO FPV800-MTDIFFHT                     01910000
           MOVE FPV500-MTDIFFTVA TO FPV800-MTDIFFTVA                    01920000
           MOVE FPV500-MTPRMP    TO FPV800-MTPRMP                       01930000
           MOVE FPV500-MTPRIME   TO FPV800-MTPRIME                      01940000
           MOVE FPV500-MTPRIMEO  TO FPV800-MTPRIMEO                     01950000
           MOVE FPV500-MTPRIMEV  TO FPV800-MTPRIMEV                     01960000
           IF FPV500-MTREFINOP NOT NUMERIC                              01970000
              MOVE ZERO TO FPV500-MTREFINOP                             01980000
           END-IF                                                       01990000
           COMPUTE FPV500-MTCOMMOP = FPV500-MTCOMMOP - FPV500-MTREFINOP 02000000
           MOVE FPV500-MTCOMMOP  TO FPV800-MTCOMMOP                     02010000
           MOVE FPV500-TYPVTE    TO FPV800-TYPVTE                       02020000
           MOVE FPV500-WTABLE    TO FPV800-WTABLE                       02030000
           MOVE FPV500-NZONPRIX  TO FPV800-NZONPRIX                     02040000
           MOVE FPV500-MTREFINOP TO FPV800-MTREFINOP                    02050000
                                                                        02060000
           WRITE FPV800O-ENR FROM W-ENR-FPV800                          02070000
           ADD 1 TO W-CPT-ECRITURES.                                    02080000
                                                                        02090000
       FIN-ECRITURE-FPV800O. EXIT.                                      02100000
      *---------------------                                            02110000
                                                                        02120000
                                                                        02130000
       TRAITEMENT-SORTIE SECTION.                                       02140000
      *==========================                                       02150000
                                                                        02160000
           CLOSE FPV500I FPV800O.                                       02170000
                                                                        02180000
           DISPLAY ' * ------------------------------------ *'.         02190000
           DISPLAY ' *       FIN D''EXECUTION BPV800         *'.        02200000
           DISPLAY ' * ------------------------------------ *'.         02210000
           DISPLAY '    -  NBRE DE LECTURE PV500I   : ' W-CPT-LEC-PV500.02220000
           DISPLAY '    -  NBRE D''ECRITURES PV800O  : '                02230000
                   W-CPT-ECRITURES.                                     02240000
                                                                        02250000
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    02260000
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        02270000
       FIN-TRAITEMENT-SORTIE.     EXIT.                                 02280000
      *----------------------                                           02290000
                                                                        02300000
                                                                        02310000
