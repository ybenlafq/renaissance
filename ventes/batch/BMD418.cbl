      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       ID  DIVISION.                                                    00020001
       PROGRAM-ID.    BMD418.                                           00030001
       AUTHOR.        DSA024.                                           00040001
       DATE-WRITTEN.  29/06/2001.                                       00050001
                                                                        00060001
      * -------------------------------------------------------------- *00070001
      *  FONCTION : PROGRAMME D'EDITION                                *00080000
      *             EDITION MENSUELLE DES DEGRADATIONS DES MARGES.     *00090000
      *             DESCRITPION PAR MAG/FAMILLES ET CODES REGULS       *00100000
      *            + CONSO SUR MAG/FAMILLE + CONSO SUR SOCIETE/FAMILLE *00110000
      *                                                                *00120000
      *  AUTEUR   : DSA024.AL                                          *00130001
      *  DATE     : 17/12/2004                                         *00140001
      * -------------------------------------------------------------- *00150001
CL0510*  LE 11/05/2010 - DSA065  (C. LAVAURE)                           00150101
      *              AUGMENTATION DE LA ZONE CUMUL DES QUANTITES        00150201
      * -------------------------------------------------------------- *00151001
                                                                        00160001
       ENVIRONMENT DIVISION.                                            00170001
       CONFIGURATION SECTION.                                           00180001
       SPECIAL-NAMES.                                                   00190001
           DECIMAL-POINT IS COMMA.                                      00200001
                                                                        00210001
       INPUT-OUTPUT SECTION.                                            00220000
       FILE-CONTROL.                                                    00230001
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE      ASSIGN TO FDATE.                           00240001
      *--                                                                       
           SELECT FDATE      ASSIGN TO FDATE                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FC-BMD418I ASSIGN TO BMD418I STATUS IS FS-BMD418I.    00250001
      *--                                                                       
           SELECT FC-BMD418I ASSIGN TO BMD418I STATUS IS FS-BMD418I             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FC-BMD418O ASSIGN TO BMD418O.                         00260001
      *                                                                         
      *--                                                                       
           SELECT FC-BMD418O ASSIGN TO BMD418O                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00270001
      *}                                                                        
       DATA DIVISION.                                                   00280001
                                                                        00290001
       FILE SECTION.                                                    00300001
                                                                        00310001
      * - DATE DU JOUR DE TRAITEMENT.                                   00320001
        FD  FDATE                                                       00330001
            RECORDING F                                                 00340001
            BLOCK 0 RECORDS                                             00350001
            LABEL RECORD STANDARD.                                      00360001
        01  FILE-DATE.                                                  00370001
            05  F-DTRT   PIC X(8).                                      00380001
            05  FILLER   PIC X(72).                                     00390000
                                                                        00400001
      * - FICHIER PERMANENT EN ENTREE.                                  00410001
        FD  FC-BMD418I                                                  00420001
            RECORDING F                                                 00430001
            BLOCK 0 RECORDS                                             00440001
            LABEL RECORD STANDARD.                                      00450001
        01  FD-BMD418I     PIC X(167).                                  00460001
                                                                        00470001
      * - FICHIER EN SORTIE.                                            00480001
        FD  FC-BMD418O                                                  00490001
            RECORDING F                                                 00500001
            BLOCK 0 RECORDS                                             00510001
            LABEL RECORD STANDARD.                                      00520001
        01  FD-BMD418O.                                                 00530001
            05  FD-ASA         PIC X.                                   00540001
            05  ENR-BMD418O    PIC X(132).                              00550001
                                                                        00560000
       WORKING-STORAGE SECTION.                                         00570001
                                                                        00580001
      * - ZONE DE CONTROLE DE FICHIERS.                                 00590001
       77  FS-BMD418I                 PIC 9(02).                        00600001
                                                                        00610001
       01   W-CALLNAME                 PIC  X(8)   VALUE  SPACES.       00620001
       01   W-SS-TABLE                 PIC  X(5)   VALUE  SPACES.       00630001
                                                                        00640001
       01  DDEBMOIS.                                                    00650001
         05 FILLER                    PIC X(06).                        00660001
         05 JJDEBMOIS                 PIC X(02).                        00670001
                                                                        00680001
       01  DFINMOIS                   PIC X(08).                        00690001
      *                                                                 00700001
      * ---------------------- *                                        00710001
      * TABLES INTERNES        *                                        00720001
      * ---------------------- *                                        00730001
                                                                        00740001
       01  I                           PIC 99999.                       00750001
       01  J                           PIC 99999.                       00760001
                                                                        00770001
      * - ZONE TAMPON LORS DE L'�DITION DE L'ENTETE                     00771001
       01  WZONE-TAMPON133             PIC X(133).                      00772001
                                                                        00780001
      * - GESTION DU DEROULEMENT DU TRAITEMENT.                         00790001
       01  ETAT-TRAITEMENT            PIC 9.                            00800001
         88 TRT-OK                              VALUE 0.                00810001
         88 TRT-ERREUR                          VALUE 1.                00820001
                                                                        00830001
       01  ETAT-BMD418I                PIC 9.                           00840001
         88 LECTURE-OK                          VALUE 0.                00850001
         88 LECTURE-FIN                         VALUE 1.                00860001
                                                                        00870001
      * - COMPTEURS.                                                    00880001
       01  W-INDICE                    PIC 9(3).                        00890001
                                                                        00900001
      * - DETAIL DE LA PAGE D'IMPRESSION                                00910001
       77 WLONGUEUR-PAGE                PIC 9(03) VALUE 55.             00921001
       77 WLARGEUR-PAGE                 PIC 9(03) VALUE 132.            00930001
                                                                        00940001
      * - COMPTEUR D'ECRITURE.                                          00950001
       77 WCPT-LIGNE                    PIC 9(02) VALUE 0.              00960001
       77 WCPT-PAGE                     PIC 9(02) VALUE 0.              00970001
                                                                        00980001
       01 JEDITEQUOI                    PIC 9  VALUE 0.                 00990001
          88 EDIT-TOTAL-FAM                    VALUE 1.                 01000101
          88 EDIT-OPE                          VALUE 2.                 01001001
          88 EDIT-SOC                          VALUE 3.                 01010001
          88 EDIT-ENTETE                       VALUE 4.                 01020001
          88 EDIT-TOTAL-MAG                    VALUE 5.                 01021001
                                                                        01030001
       01 QUELLERUPTURES                PIC 9  VALUE 0.                 01040001
          88 RUPTURE-FAM                       VALUE 0.                 01050001
          88 RUPTURE-MAG                       VALUE 1.                 01060001
      * - ZONES DE RUPTURES FAMILLE, MAGASIN                            01070001
                                                                        01080001
       01 WCLE-RUPTURES.                                                01090001
          05 WCLE-NLIEU        PIC X(03).                               01100001
          05 WCLE-CFAM         PIC X(05).                               01110001
          05 WCLE-LLIEU        PIC X(20).                               01112001
                                                                        01120001
      * - TABLEAU CODE OP� POUR R��CRITURE � RUPTURE FAMILLE.           01120101
       01 WTOTAL-MAG.                                                   01120201
          05  TMAG-PCESSIONCUM          PIC S9(11)V9(2) COMP-3.         01120301
          05  TMAG-PCESSIONCUM-EXC      PIC S9(11)V9(2) COMP-3.         01120401
          05  TMAG-PRMPCUM              PIC S9(11)V9(2) COMP-3.         01120501
          05  TMAG-PRMPCUM-EXC          PIC S9(09)V9(2) COMP-3.         01120601
          05  TMAG-QTECUM               PIC S9(06)      COMP-3.         01120701
          05  TMAG-QTECUM-EXC           PIC S9(06)      COMP-3.         01120801
                                                                        01120901
       01  WDETAIL-FAMILLE-OPERATION.                                   01121001
          05  FAM-CFAM                 PIC X(05).                       01121101
          05  FAM-LFAM                 PIC X(20).                       01121201
          05  FAM-PCESSIONCUM          PIC S9(11)V9(2) COMP-3.          01121301
          05  FAM-PCESSIONCUM-EXC      PIC S9(11)V9(2) COMP-3.          01121401
          05  FAM-PRMPCUM              PIC S9(11)V9(2) COMP-3.          01121501
          05  FAM-PRMPCUM-EXC          PIC S9(09)V9(2) COMP-3.          01121601
          05  FAM-QTECUM               PIC S9(06)      COMP-3.          01121701
          05  FAM-QTECUM-EXC           PIC S9(06)      COMP-3.          01121801
                                                                        01121901
          05  WCPT-CODOP                 PIC 9(04) VALUE 0.             01122001
          05  WCPT-CODOP-MAX             PIC 9(04) VALUE 1000.          01122101
          05  WTAB-CODOP.                                               01122201
          10  WPOSTE-CODOP OCCURS 100.                                  01122301
              15 CODOP-COPER             PIC X(10).                     01122401
              15 CODOP-PCESSIONCUM       PIC S9(11)V9(2) COMP-3.        01122501
              15 CODOP-PCESSIONCUM-EXC   PIC S9(11)V9(2) COMP-3.        01122601
              15 CODOP-PRMPCUM           PIC S9(11)V9(2) COMP-3.        01122701
              15 CODOP-PRMPCUM-EXC       PIC S9(09)V9(2) COMP-3.        01122801
              15 CODOP-QTECUM            PIC S9(06)      COMP-3.        01122901
              15 CODOP-QTECUM-EXC        PIC S9(06)      COMP-3.        01123001
                                                                        01124001
       01 WCPT-CUMUL-SOCF-MAX               PIC 9(05) VALUE 0999.       01130001
       01 WCPT-CUMUL-SOCF                   PIC 9(05) VALUE 0.          01140001
       01 WCPT-CUMUL-SOCO-MAX               PIC 9(04) VALUE 100.        01141001
       01 WCPT-CUMUL-SOCO                   PIC 9(04) VALUE 0.          01142001
                                                                        01143001
       01 WTAB-CUMUL-SOCIETE.                                           01150001
         02  WPOSTE-CUMUL-SOCIETE  OCCURS 1000.                         01160001
          05 WTAB-SOCF-CFAM                 PIC X(05).                  01170001
          05 WTAB-SOCF-LFAM                 PIC X(20).                  01180001
          05 WTAB-SOCF-WSEQFAM              PIC 9(05).                  01180101
          05 WTAB-SOCF-PCESSIONCUM          PIC S9(11)V9(2) COMP-3.     01180201
          05 WTAB-SOCF-PCESSIONCUM-EXC      PIC S9(11)V9(2) COMP-3.     01180301
          05 WTAB-SOCF-PRMPCUM              PIC S9(11)V9(2) COMP-3.     01180401
          05 WTAB-SOCF-PRMPCUM-EXC          PIC S9(09)V9(2) COMP-3.     01180501
          05 WTAB-SOCF-QTECUM               PIC S9(06)      COMP-3.     01180601
          05 WTAB-SOCF-QTECUM-EXC           PIC S9(06)      COMP-3.     01180701
          05 WTAB-OPER OCCURS 100.                                      01180801
             10 WTAB-SOCO-COPER                PIC X(10).               01181001
             10 WTAB-SOCO-PCESSIONCUM          PIC S9(11)V9(2) COMP-3.  01190001
             10 WTAB-SOCO-PCESSIONCUM-EXC      PIC S9(11)V9(2) COMP-3.  01200001
             10 WTAB-SOCO-PRMPCUM              PIC S9(11)V9(2) COMP-3.  01210001
             10 WTAB-SOCO-PRMPCUM-EXC          PIC S9(09)V9(2) COMP-3.  01220001
             10 WTAB-SOCO-QTECUM               PIC S9(06)      COMP-3.  01230001
             10 WTAB-SOCO-QTECUM-EXC           PIC S9(06)      COMP-3.  01240001
                                                                        01250001
      * --------------------------------------------------------- *     01260001
      * --------------------------------------------------------- *     01270001
                                                                        01280001
       01  DESC-JMD418.                                                 01520001
      * - DESCRIPTION DE L'ENTETE                                       01530001
      *                               123546879012345678901234567890    01540001
         05  LIGNE-TIRET.                                               01550001
           10  F     PIC X(01)  VALUE     '+'.                          01560001
           10  F     PIC X(130) VALUE ALL '-'.                          01570001
           10  F     PIC X(01)  VALUE     '+'.                          01580001
         05  LIGNE-TIRET2.                                              01590001
           10  F  PIC X(30)  VALUE   '+-----------------------------'.  01600001
           10  F  PIC X(30)  VALUE   '--------------+---------+-----'.  01610001
           10  F  PIC X(30)  VALUE   '---------+-----------------+--'.  01620001
           10  F  PIC X(30)  VALUE   '---------+--------------+-----'.  01630001
           10  F  PIC X(12)  VALUE   '-----------+'.                    01640001
                                                                        01650001
         05  LIGNE-BLANCHE.                                             01660001
           10  F     PIC X(30)  VALUE '!                             '. 01670001
           10  F     PIC X(30)  VALUE '              !         !     '. 01680001
           10  F     PIC X(30)  VALUE '         !                 !  '. 01690001
           10  F     PIC X(30)  VALUE '         !              !     '. 01700001
           10  F     PIC X(12)  VALUE '           !'.                   01710001
                                                                        01720001
         05  ENTETE-LIB.                                                01730001
           10  F     PIC X(30)  VALUE '!JMD418                       '. 01740001
           10  F     PIC X(30)  VALUE '   DEGRADATION DE MARGES CONST'. 01750001
           10  F     PIC X(30)  VALUE 'ATEES PAR FAMILLE ET CODE OPER'. 01760001
           10  F     PIC X(30)  VALUE 'ATION                         '. 01770001
           10  F     PIC X(12)  VALUE '           !'.                   01780001
                                                                        01790001
         05  ENTETE-LIB-SOC.                                            01800001
           10  F     PIC X(30)  VALUE '!JMD418                       '. 01810001
           10  F     PIC X(30)  VALUE '   DEGRADATION DE MARGES CONST'. 01820001
           10  F     PIC X(30)  VALUE 'ATEES SUR LA SOCIETE          '. 01830001
           10  F     PIC X(30)  VALUE '                              '. 01840001
           10  F     PIC X(12)  VALUE '           !'.                   01850001
                                                                        01860001
         05  ENTETE-MAG.                                                01870001
1          10  F     PIC X(12)  VALUE '! SOCIETE : '.                   01880001
13         10  ENTETE-FIL   PIC X(03).                                  01890001
16         10  F     PIC X(11)  VALUE ' MAGASIN : '.                    01900001
26         10  ENTETE-LIEU  PIC X(03).                                  01910001
           10  F            PIC X(01) VALUE ' '.                        01920001
           10  ENTETE-LLIEU PIC X(20).                                  01930001
           10  F            PIC X(18) VALUE '       PERIODE DU '.       01940001
           10  ENTETE-DDEB  PIC X(10).                                  01950001
           10  F            PIC X(04) VALUE ' AU '.                     01960001
           10  ENTETE-DFIN  PIC X(10).                                  01970001
           10  F     PIC X(30) VALUE '                              '.  01980001
           10  F     PIC X(06) VALUE ' PAGE '.                          01990001
           10  ENTETE-PAGE PIC 999.                                     02000001
           10  F     PIC X(01)        VALUE '!'.                        02010001
      *                                123546879012345678901234567890   02010201
         05  ENTETE-SOC.                                                02011001
1          10  F     PIC X(30)  VALUE '!                             '. 02012001
1          10  F     PIC X(20)  VALUE '                    '.           02012101
           10  F            PIC X(18) VALUE '       PERIODE DU '.       02018001
           10  ENTETE-SOC-DDEB  PIC X(10).                              02019001
           10  F            PIC X(04) VALUE ' AU '.                     02019101
           10  ENTETE-SOC-DFIN  PIC X(10).                              02019201
           10  F     PIC X(30) VALUE '                              '.  02019301
           10  F     PIC X(06) VALUE ' PAGE '.                          02019401
           10  ENTETE-SOC-PAGE PIC 999.                                 02019501
           10  F     PIC X(01)        VALUE '!'.                        02019601
                                                                        02020001
      *                                123546879012345678901234567890   02030001
         05  CORPS-LIB.                                                 02040001
           10  F     PIC X(30)  VALUE '! FAM   LIBELLE FAMILLE       '. 02050001
           10  F     PIC X(30)  VALUE ' OPERATION    ! QTE     ! VALE'. 02060001
           10  F     PIC X(30)  VALUE 'UR PRMP  ! PRIX CESSION    ! C'. 02070001
           10  F     PIC X(30)  VALUE 'UMUL QTE ! CUMUL PRMP   ! CUMU'. 02080001
           10  F     PIC X(12)  VALUE 'L CESSION  !'.                   02090001
                                                                        02100001
         05  CORPS-LIB-FAMILLE.                                         02180001
           10  F                              PIC X(02)  VALUE '! '.    02190001
           10  CLF-CFAM                       PIC X(05).                02200001
           10  F                              PIC X(01)  VALUE ' '.     02210001
           10  CLF-LFAM                       PIC X(20).                02220001
           10  F     PIC X(02)  VALUE '  '.                             02230001
           10  F     PIC X(30)  VALUE '              !         !     '. 02240001
           10  F     PIC X(30)  VALUE '         !                 !  '. 02250001
           10  F     PIC X(30)  VALUE '         !              !     '. 02260001
           10  F     PIC X(12)  VALUE '           !'.                   02270001
      *                                123546879012345678901234567890   02280001
         05  CORPS-DETAIL-FAMILLE.                                      02290001
           10 F                   PIC X(02)  VALUE '! '.                02291001
           10  CDF-CFAM           PIC X(05).                            02292001
           10  F                  PIC X(01)  VALUE ' '.                 02293001
           10  CDF-LFAM           PIC X(20).                            02294001
           10  F                  PIC X(18)  VALUE '                ! '.02295001
           10  CDF-QTECUM          PIC -----9         BLANK WHEN ZERO.  02330001
           10  F                   PIC X(04)  VALUE '  ! '.             02340001
           10  CDF-PRMPCUM         PIC -------9,99    BLANK WHEN ZERO.  02350001
           10  F                   PIC X(04)  VALUE '  ! '.             02360001
           10  CDF-PCESSIONCUM     PIC ----------9,99 BLANK WHEN ZERO.  02370001
           10  F                   PIC X(04)  VALUE '  ! '.             02380001
CL0510*    10  CDF-QTECUM-EXC      PIC -----9         BLANK WHEN ZERO.  02390001
CL0510     10  CDF-QTECUM-EXC      PIC ------9        BLANK WHEN ZERO.  02391001
CL0510*    10  F                   PIC X(06)  VALUE '    ! '.           02400001
CL0510     10  F                   PIC X(05)  VALUE '   ! '.            02401001
           10  CDF-PRMPCUM-EXC     PIC -------9,99    BLANK WHEN ZERO.  02410001
           10  F                   PIC X(04)  VALUE '  ! '.             02420001
           10  CDF-PCESSIONCUM-EXC PIC ----------9,99 BLANK WHEN ZERO.  02430001
           10  F                   PIC X(02)  VALUE ' !'.               02440001
                                                                        02450001
      *                                123546879012345678901234567890   02460001
         05  CORPS-DETAIL-OPERATION.                                    02470001
           10  F     PIC X(30)  VALUE '!                             '. 02480001
           10  F     PIC X(01)  VALUE SPACE.                            02490001
           10  CDO-COPER            PIC X(10).                          02500001
           10  F                    PIC X(05) VALUE '   ! '.            02510001
           10  CDO-QTECUM           PIC -----9         BLANK WHEN ZERO. 02520001
           10  F                    PIC X(04)  VALUE '  ! '.            02530001
           10  CDO-PRMPCUM          PIC -------9,99    BLANK WHEN ZERO. 02540001
           10  F                    PIC X(04)  VALUE '  ! '.            02550001
           10  CDO-PCESSIONCUM      PIC ----------9,99 BLANK WHEN ZERO. 02560001
           10  F                    PIC X(04)  VALUE '  ! '.            02570001
CL0510*    10  CDO-QTECUM-EXC       PIC -----9         BLANK WHEN ZERO. 02580001
CL0510     10  CDO-QTECUM-EXC       PIC ------9        BLANK WHEN ZERO. 02581001
CL0510*    10  F                    PIC X(06)  VALUE '    ! '.          02590001
CL0510     10  F                    PIC X(05)  VALUE '   ! '.           02591001
           10  CDO-PRMPCUM-EXC      PIC -------9,99    BLANK WHEN ZERO. 02600001
           10  F                    PIC X(04)  VALUE '  ! '.            02610001
           10  CDO-PCESSIONCUM-EXC  PIC ----------9,99 BLANK WHEN ZERO. 02620001
           10  F                    PIC X(02)  VALUE ' !'.              02630001
                                                                        02640001
      *                                123546879012345678901234567890   02650001
         05 SOC-DETAIL-FAM.                                             02820001
           10 F               PIC X(02)  VALUE '! '.                    02830001
           10 WSOCF-CFAM      PIC X(05).                                02840001
           10  F                  PIC X(01)  VALUE ' '.                 02850001
           10  WSOCF-LFAM         PIC X(20).                            02860001
           10  F                  PIC X(18)  VALUE '                ! '.02870001
           10  WSOCF-QTECUM          PIC -----9         BLANK WHEN ZERO.02880001
           10  F                     PIC X(04)  VALUE '  ! '.           02890001
           10  WSOCF-PRMPCUM         PIC -------9,99    BLANK WHEN ZERO.02900001
           10  F                     PIC X(04)  VALUE '  ! '.           02910001
           10  WSOCF-PCESSIONCUM     PIC ----------9,99 BLANK WHEN ZERO.02920001
           10  F                     PIC X(04)  VALUE '  ! '.           02930001
CL0510*    10  WSOCF-QTECUM-EXC      PIC -----9         BLANK WHEN ZERO.02940001
CL0510     10  WSOCF-QTECUM-EXC      PIC ------9        BLANK WHEN ZERO.02941001
CL0510*    10  F                     PIC X(06)  VALUE '    ! '.         02950001
CL0510     10  F                     PIC X(05)  VALUE '   ! '.          02951001
           10  WSOCF-PRMPCUM-EXC     PIC -------9,99    BLANK WHEN ZERO.02960001
           10  F                     PIC X(04)  VALUE '  ! '.           02970001
           10  WSOCF-PCESSIONCUM-EXC                                    02980001
                                     PIC ----------9,99 BLANK WHEN ZERO.02981001
           10  F                     PIC X(02)  VALUE ' !'.             02990001
                                                                        03000001
         05  SOC-DETAIL-OPERATION.                                      03001001
           10  F     PIC X(30)  VALUE '!                             '. 03002001
           10  F     PIC X(01)  VALUE SPACE.                            03003001
           10  WSOCO-COPER            PIC X(10).                        03004001
           10  F                    PIC X(05) VALUE '   ! '.            03005001
           10  WSOCO-QTECUM          PIC -----9         BLANK WHEN ZERO.03006001
           10  F                    PIC X(04)  VALUE '  ! '.            03007001
           10  WSOCO-PRMPCUM         PIC -------9,99    BLANK WHEN ZERO.03008001
           10  F                    PIC X(04)  VALUE '  ! '.            03009001
           10  WSOCO-PCESSIONCUM     PIC ----------9,99 BLANK WHEN ZERO.03009101
           10  F                    PIC X(04)  VALUE '  ! '.            03009201
CL0510*    10  WSOCO-QTECUM-EXC     PIC -----9         BLANK WHEN ZERO. 03009301
CL0510     10  WSOCO-QTECUM-EXC     PIC ------9        BLANK WHEN ZERO. 03009401
CL0510*    10  F                    PIC X(06)  VALUE '    ! '.          03009501
CL0510     10  F                    PIC X(05)  VALUE '   ! '.           03009601
           10  WSOCO-PRMPCUM-EXC     PIC -------9,99    BLANK WHEN ZERO.03009701
           10  F                    PIC X(04)  VALUE '  ! '.            03009801
           10  WSOCO-PCESSIONCUM-EXC PIC ----------9,99 BLANK WHEN ZERO.03009901
           10  F                    PIC X(02)  VALUE ' !'.              03010001
                                                                        03010101
         05  TOTAL-MAG-SOCIETE.                                         03010201
           10  F          PIC X(08)  VALUE '! TOTAL '.                  03010301
           10  TMS-LIB    PIC X(10)  VALUE SPACES.                      03010401
           10  TMS-NLIEU  PIC X(03)  VALUE SPACES.                      03010501
           10  F          PIC X(10)  VALUE SPACE.                       03010601
           10  F          PIC X(10)  VALUE SPACES.                      03010701
           10  F                     PIC X(05) VALUE '   ! '.           03010801
           10  TMS-QTECUM            PIC -----9         BLANK WHEN ZERO.03010901
           10  F                     PIC X(04)  VALUE '  ! '.           03011001
           10  TMS-PRMPCUM           PIC -------9,99    BLANK WHEN ZERO.03011101
           10  F                     PIC X(04)  VALUE '  ! '.           03011201
           10  TMS-PCESSIONCUM       PIC ----------9,99 BLANK WHEN ZERO.03011301
           10  F                     PIC X(04)  VALUE '  ! '.           03011401
CL0510*    10  TMS-QTECUM-EXC        PIC -----9         BLANK WHEN ZERO.03011501
CL0510     10  TMS-QTECUM-EXC        PIC ------9        BLANK WHEN ZERO.03011601
CL0510*    10  F                     PIC X(06)  VALUE '    ! '.         03011701
CL0510     10  F                     PIC X(05)  VALUE '   ! '.          03011801
           10  TMS-PRMPCUM-EXC       PIC -------9,99    BLANK WHEN ZERO.03011901
           10  F                     PIC X(04)  VALUE '  ! '.           03012001
           10  TMS-PCESSIONCUM-EXC   PIC ----------9,99 BLANK WHEN ZERO.03012101
           10  F                     PIC X(02)  VALUE ' !'.             03013001
                                                                        03020001
           COPY  ABENDCOP.                                              03030001
                                                                        03040001
           COPY  WORKDATC.                                              03050001
                                                                        03060001
           COPY  COMMMG01.                                              03070001
                                                                        03080001
           COPY  FMD417.                                                03090001
                                                                        03100001
      * - ZONES RETOUR.                                                 03120001
           COPY SYBWDIV0.                                               03130001
                                                                        03140001
      * -----------------------------------------  *                    03150000
      * - DESCRIPTION DES TABLES UTILIS�ES         *                    03160001
      * ------------------------------------------ *                    03170000
                                                                        03180000
           COPY RVGA01YS.                                               03190001
           COPY RVGA01YT.                                               03200001
                                                                        03210001
                                                                        03240001
       PROCEDURE  DIVISION.                                             03250001
                                                                        03260000
      * ------------------------------------------------------- *       03270001
      *              BOUCLE PRINCIPALE                          *       03280001
      * ------------------------------------------------------- *       03290001
           PERFORM  INITIALISATION.                                     03300001
                                                                        03310000
           PERFORM  TRAITEMENT-BMD418  UNTIL  LECTURE-FIN.              03320001
                                                                        03330001
           PERFORM EDITION-TOTAL-FAMILLE                                03340001
           PERFORM EDITION-OPERATION.                                   03341001
           PERFORM EDITION-TOTAL-MAG.                                   03342001
                                                                        03350001
      * - ECRITURE CUMUL SUR SOCIETE.                                   03360001
           PERFORM TRI-FAMILLE.                                         03361001
           PERFORM EDITION-SOCIETE.                                     03370001
                                                                        03380001
           PERFORM  FIN-BMD418.                                         03390001
                                                                        03400001
      * ------------------------------------------------------- *       03410001
      *              FIN BOUCLE PRINCIPALE                      *       03420001
      * ------------------------------------------------------- *       03430001
                                                                        03440001
       INITIALISATION    SECTION.                                       03450001
                                                                        03460001
           DISPLAY  '+ -------------------------------------------- +'. 03470001
           DISPLAY  '! EXTRACTION - DEGRADATION DES MARGES          !'. 03480000
           DISPLAY  '! -------------------------------------------- !'. 03490001
           DISPLAY  '!    PROGRAMME BMD418                          !'. 03500001
           DISPLAY  '!    ETAPE 2 EDITION DE L''�TAT JMD418         !'. 03510001
           DISPLAY  '* -------------------------------------------- *'. 03520001
                                                                        03530001
           SET   TRT-OK    TO TRUE.                                     03540001
                                                                        03550001
      * - RECUPERATION DATE DE TRAITEMENT                               03560001
           DISPLAY '- LECTURE DE LA DATE DU JOUR...'.                   03570001
                                                                        03580001
           OPEN  INPUT  FDATE.                                          03590001
                                                                        03600001
           READ  FDATE                                                  03610001
             AT END                                                     03620001
                MOVE  2                            TO  ABEND-CODE       03630001
                MOVE '- ERREUR - DATE ABSENTE    ' TO  ABEND-MESS       03640001
                CLOSE FDATE                                             03650000
                SET TRT-ERREUR TO TRUE                                  03660001
                GO TO FIN-BMD418                                        03670001
           END-READ.                                                    03680001
                                                                        03690001
           DISPLAY 'FDATE : ' F-DTRT.                                   03700001
           MOVE F-DTRT     TO GFJJMMSSAA                                03710001
           CLOSE  FDATE.                                                03720001
                                                                        03730001
           PERFORM CONTROLE-DATE.                                       03740001
           MOVE  GFSAMJ-0  TO DFINMOIS.                                 03750001
                                                                        03760001
           MOVE  DFINMOIS  TO DDEBMOIS.                                 03770001
           MOVE '01'       TO JJDEBMOIS.                                03780001
                                                                        03790001
      * - OUVERTURE DES FICHIERS                                        03800001
                                                                        03810001
           OPEN OUTPUT  FC-BMD418O.                                     03820001
           OPEN INPUT   FC-BMD418I.                                     03840001
                                                                        03850001
      * - INITIALISATIONS ZONES DE CUMUL                                03860001
           PERFORM RAZ-ZONES-CUMUL.                                     03870001
           PERFORM RAZ-WTAB-CUMUL-SOC.                                  03880001
           INITIALIZE     WTOTAL-MAG.                                   03880101
                                                                        03890001
           SET LECTURE-OK TO TRUE.                                      03900001
                                                                        03910001
           PERFORM LECTURE-FICHIER.                                     03920001
                                                                        03930001
           IF LECTURE-FIN                                               03940001
              DISPLAY 'AUCUNE LIGNE A TRAITER DS LE FICHIER BMD418I'    03950001
              PERFORM FIN-BMD418                                        03960001
           ELSE                                                         03970001
             STRING FMD417-DDEB (7:2) '/'                               03980001
                    FMD417-DDEB (5:2) '/'                               03990001
                    FMD417-DDEB (1:4) DELIMITED BY SIZE                 04000001
               INTO ENTETE-DDEB                                         04010001
                                                                        04011001
             MOVE ENTETE-DDEB TO ENTETE-SOC-DDEB                        04020001
                                                                        04021001
             STRING FMD417-DFIN (7:2) '/'                               04030001
                    FMD417-DFIN (5:2) '/'                               04040001
                    FMD417-DFIN (1:4) DELIMITED BY SIZE                 04050001
               INTO ENTETE-DFIN                                         04060001
                                                                        04060101
             MOVE ENTETE-DFIN TO ENTETE-SOC-DFIN                        04061001
                                                                        04070001
              MOVE FMD417-NMAG     TO WCLE-NLIEU                        04080001
              MOVE FMD417-CFAM     TO WCLE-CFAM                         04090001
              MOVE FMD417-LNMAG    TO WCLE-LLIEU                        04090201
                                                                        04091001
              PERFORM  EDITION-ENTETE                                   04100001
           END-IF.                                                      04120001
                                                                        04130001
       FIN-INITIALISATION.                                              04140001
           EXIT.                                                        04150001
                                                                        04160001
       TRAITEMENT-BMD418      SECTION.                                  04170001
                                                                        04180001
      * CONTROLE RUPTURES                                               04190001
                                                                        04200001
           IF FMD417-NMAG NOT = WCLE-NLIEU                              04210001
                                                                        04220001
              PERFORM EDITION-TOTAL-FAMILLE                             04250001
              PERFORM EDITION-OPERATION                                 04251001
                                                                        04251101
              PERFORM EDITION-TOTAL-MAG                                 04252001
              INITIALIZE     WTOTAL-MAG                                 04252101
                                                                        04252201
              MOVE FMD417-NMAG     TO WCLE-NLIEU                        04253001
              MOVE FMD417-CFAM     TO WCLE-CFAM                         04254001
              MOVE FMD417-LNMAG    TO WCLE-LLIEU                        04255001
                                                                        04256001
              PERFORM EDITION-ENTETE                                    04260001
                                                                        04270001
              PERFORM RAZ-ZONES-CUMUL                                   04280001
                                                                        04330001
           END-IF.                                                      04360001
                                                                        04370001
           IF FMD417-CFAM  NOT = WCLE-CFAM                              04380001
                                                                        04390001
              PERFORM EDITION-TOTAL-FAMILLE                             04410001
              PERFORM EDITION-OPERATION                                 04411001
              PERFORM RAZ-ZONES-CUMUL                                   04420001
                                                                        04430101
              MOVE FMD417-CFAM TO WCLE-CFAM                             04431001
           END-IF.                                                      04440001
                                                                        04450001
           PERFORM CUMUL-DES-LIGNES.                                    04460001
                                                                        04460101
           PERFORM LECTURE-FICHIER.                                     04490001
                                                                        04500001
       FIN-TRAITEMENT-BMD418.                                           04510001
           EXIT.                                                        04520001
                                                                        04530001
      * ----------------------------- *                                 04540001
      * - SECTION ACCES AUX FICHIERS. *                                 04550001
      * ----------------------------- *                                 04560001
       LECTURE-FICHIER SECTION.                                         04570001
                                                                        04580001
           READ FC-BMD418I INTO ENR-FMD417.                             04590001
                                                                        04600001
           EVALUATE TRUE                                                04610001
               WHEN FS-BMD418I = '10'                                   04620001
                    SET LECTURE-FIN TO TRUE                             04630001
                    GO TO FIN-LECTURE-FICHIER                           04640001
               WHEN FS-BMD418I = '00'                                   04650001
                    CONTINUE                                            04660001
               WHEN OTHER                                               04670001
                    DISPLAY 'ERREUR LECTURE' FS-BMD418I                 04680001
                    SET TRT-ERREUR TO TRUE                              04690001
                    SET LECTURE-FIN TO TRUE                             04700001
                    GO TO FIN-LECTURE-FICHIER                           04710001
           END-EVALUATE.                                                04720001
                                                                        04730001
       FIN-LECTURE-FICHIER.                                             04740001
           EXIT.                                                        04750001
                                                                        04760001
       CUMUL-DES-LIGNES SECTION.                                        04770001
                                                                        04780001
           MOVE 1 TO I.                                                 04790001
           PERFORM VARYING I FROM 1 BY 1                                04791001
             UNTIL I > WCPT-CODOP                                       04792001
                OR I > WCPT-CODOP-MAX                                   04792101
           END-PERFORM.                                                 04794001
                                                                        04800001
           IF I > WCPT-CODOP-MAX                                        04801001
              DISPLAY 'ERR..  TABLEAU CODOP TROP PETIT...I=' I          04801101
              MOVE    'ERR: CODOP TROP PETIT' TO ABEND-MESS             04801301
              PERFORM FIN-ANORMALE                                      04801401
           ELSE                                                         04803001
               MOVE I TO WCPT-CODOP                                     04804001
           END-IF.                                                      04805001
                                                                        04806001
      * - SAUVEGARDE SUR OPERATION ET CUMUL SUR FAMILLE                 04810001
                                                                        04840001
           MOVE  FMD417-COPER           TO CODOP-COPER           (I)    04841001
           MOVE  FMD417-PCESSIONCUM     TO CODOP-PCESSIONCUM     (I)    04850001
           MOVE  FMD417-PCESSIONCUM-EXC TO CODOP-PCESSIONCUM-EXC (I)    04860001
           MOVE  FMD417-PRMPCUM         TO CODOP-PRMPCUM         (I)    04870001
           MOVE  FMD417-PRMPCUM-EXC     TO CODOP-PRMPCUM-EXC     (I)    04880001
           MOVE  FMD417-QTECUM          TO CODOP-QTECUM          (I)    04890001
           MOVE  FMD417-QTECUM-EXC      TO CODOP-QTECUM-EXC      (I)    04900001
                                                                        04900101
           MOVE  FMD417-CFAM            TO FAM-CFAM                     04900201
           MOVE  FMD417-LFAM            TO FAM-LFAM                     04900301
           ADD   FMD417-PCESSIONCUM     TO FAM-PCESSIONCUM              04901001
           ADD   FMD417-PCESSIONCUM-EXC TO FAM-PCESSIONCUM-EXC          04902001
           ADD   FMD417-PRMPCUM         TO FAM-PRMPCUM                  04903001
           ADD   FMD417-PRMPCUM-EXC     TO FAM-PRMPCUM-EXC              04904001
           ADD   FMD417-QTECUM          TO FAM-QTECUM                   04905001
           ADD   FMD417-QTECUM-EXC      TO FAM-QTECUM-EXC               04906001
                                                                        04906101
      * - CUMUL SUR MAG                                                 04906201
                                                                        04906301
           ADD   FMD417-PCESSIONCUM     TO TMAG-PCESSIONCUM             04907001
           ADD   FMD417-PCESSIONCUM-EXC TO TMAG-PCESSIONCUM-EXC         04908001
           ADD   FMD417-PRMPCUM         TO TMAG-PRMPCUM                 04909001
           ADD   FMD417-PRMPCUM-EXC     TO TMAG-PRMPCUM-EXC             04909101
           ADD   FMD417-QTECUM          TO TMAG-QTECUM                  04909201
           ADD   FMD417-QTECUM-EXC      TO TMAG-QTECUM-EXC              04909301
                                                                        04910001
      * - CUMUL SUR SOCIETE                                             05010001
                                                                        05011001
           PERFORM VARYING I  FROM 1 BY 1                               05030001
      *{ reorder-array-condition 1.1                                            
      *    UNTIL  FMD417-CFAM = WTAB-SOCF-CFAM (I)                      05031001
      *        OR WTAB-SOCF-CFAM (I) <= SPACES                          05040001
      *        OR I > WCPT-CUMUL-SOCF-MAX                               05060001
      *--                                                                       
           UNTIL  I > WCPT-CUMUL-SOCF-MAX                                       
               OR FMD417-CFAM = WTAB-SOCF-CFAM (I)                              
               OR WTAB-SOCF-CFAM (I) <= SPACES                                  
      *}                                                                        
           END-PERFORM.                                                 05080001
                                                                        05090001
           IF I > WCPT-CUMUL-SOCF-MAX                                   05100001
              DISPLAY 'ERR..  TABLEAU CUMUL SOC TROP PETIT...I=' I      05110001
              DISPLAY ' !' WCPT-CUMUL-SOCF-MAX                          05120001
              MOVE 'TAB : WCPT-CUMUL-SOCF  TROP PETIT' TO ABEND-MESS    05130001
              PERFORM FIN-ANORMALE                                      05140001
           END-IF.                                                      05150001
                                                                        05160001
           IF I > WCPT-CUMUL-SOCF                                       05170001
              MOVE I TO WCPT-CUMUL-SOCF                                 05180001
           END-IF                                                       05190001
                                                                        05200001
           MOVE FMD417-CFAM            TO WTAB-SOCF-CFAM    (I)         05210001
           MOVE FMD417-LFAM            TO WTAB-SOCF-LFAM    (I)         05220001
           MOVE FMD417-WSEQFAM         TO WTAB-SOCF-WSEQFAM (I)         05221001
                                                                        05230001
           ADD FMD417-PCESSIONCUM      TO WTAB-SOCF-PCESSIONCUM (I)     05240001
           ADD FMD417-PCESSIONCUM-EXC  TO WTAB-SOCF-PCESSIONCUM-EXC (I) 05250001
           ADD FMD417-PRMPCUM          TO WTAB-SOCF-PRMPCUM     (I)     05260001
           ADD FMD417-PRMPCUM-EXC      TO WTAB-SOCF-PRMPCUM-EXC (I)     05270001
           ADD FMD417-QTECUM           TO WTAB-SOCF-QTECUM      (I)     05280001
           ADD FMD417-QTECUM-EXC       TO WTAB-SOCF-QTECUM-EXC  (I).    05290001
                                                                        05290101
      * - AJOUT DU CODE OPERATION                                       05291001
                                                                        05291201
           PERFORM VARYING J FROM 1 BY 1                                05292001
      *{ reorder-array-condition 1.1                                            
      *         UNTIL FMD417-COPER = WTAB-SOCO-COPER (I, J)             05293001
      *               OR WTAB-SOCO-COPER (I, J) <= SPACES               05294001
      *               OR J > WCPT-CUMUL-SOCO-MAX                        05296001
      *--                                                                       
                UNTIL J > WCPT-CUMUL-SOCO-MAX                                   
                      OR FMD417-COPER = WTAB-SOCO-COPER (I, J)                  
                      OR WTAB-SOCO-COPER (I, J) <= SPACES                       
      *}                                                                        
           END-PERFORM.                                                 05298001
                                                                        05298101
           IF J > WCPT-CUMUL-SOCO-MAX                                   05299001
              DISPLAY 'ERR..  TABLEAU CUMUL SOC TROP PETIT...I=' J      05299101
              DISPLAY ' !' WCPT-CUMUL-SOCF-MAX                          05299201
              MOVE 'TAB : WCPT-CUMUL-SOCF  TROP PETIT' TO ABEND-MESS    05299301
              PERFORM FIN-ANORMALE                                      05299401
           END-IF.                                                      05299501
                                                                        05299601
           MOVE FMD417-COPER           TO WTAB-SOCO-COPER (I, J)        05299901
                                                                        05300101
           ADD FMD417-PCESSIONCUM      TO WTAB-SOCO-PCESSIONCUM (I, J)  05300201
           ADD FMD417-PCESSIONCUM-EXC                                   05300301
                                   TO WTAB-SOCO-PCESSIONCUM-EXC (I, J)  05300401
           ADD FMD417-PRMPCUM          TO WTAB-SOCO-PRMPCUM     (I, J)  05300501
           ADD FMD417-PRMPCUM-EXC      TO WTAB-SOCO-PRMPCUM-EXC (I, J)  05300601
           ADD FMD417-QTECUM           TO WTAB-SOCO-QTECUM      (I, J)  05300701
           ADD FMD417-QTECUM-EXC       TO WTAB-SOCO-QTECUM-EXC  (I, J). 05300801
                                                                        05301001
       FIN-CUMUL-DES-LIGNES. EXIT.                                      05310001
                                                                        05320001
       EDITION-TOTAL-FAMILLE SECTION.                                   05330001
                                                                        05340001
           IF NOT EDIT-ENTETE                                           05350001
              IF WCPT-LIGNE  =   WLONGUEUR-PAGE - 2                     05351001
                 ADD 1 TO WCPT-LIGNE                                    05351101
              END-IF                                                    05351201
              MOVE ' '          TO FD-ASA                               05352001
              MOVE LIGNE-TIRET2 TO ENR-BMD418O                          05353001
              PERFORM ECRITURE-ETAT                                     05354001
           END-IF.                                                      05360001
                                                                        05370001
           MOVE FAM-CFAM                     TO CDF-CFAM                05410001
           MOVE FAM-LFAM                     TO CDF-LFAM                05411001
           MOVE FAM-PCESSIONCUM              TO CDF-PCESSIONCUM         05420001
           MOVE FAM-PCESSIONCUM-EXC          TO CDF-PCESSIONCUM-EXC     05430001
           MOVE FAM-PRMPCUM                  TO CDF-PRMPCUM             05440001
           MOVE FAM-PRMPCUM-EXC              TO CDF-PRMPCUM-EXC         05450001
           MOVE FAM-QTECUM                   TO CDF-QTECUM              05460001
           MOVE FAM-QTECUM-EXC               TO CDF-QTECUM-EXC          05470001
                                                                        05480001
           MOVE CORPS-DETAIL-FAMILLE         TO ENR-BMD418O.            05490001
           MOVE ' ' TO FD-ASA.                                          05491001
                                                                        05500001
           PERFORM ECRITURE-ETAT.                                       05510001
                                                                        05511001
           SET EDIT-TOTAL-FAM TO TRUE.                                  05520001
                                                                        05580001
       FIN-EDITION-TOTAL-FAMILLE. EXIT.                                 05590001
                                                                        05680001
       EDITION-OPERATION SECTION.                                       05710001
                                                                        05720001
           MOVE ' ' TO FD-ASA.                                          05720101
                                                                        05720201
           PERFORM VARYING I FROM 1 BY 1                                05720301
             UNTIL I > WCPT-CODOP                                       05720401
                                                                        05722001
               MOVE CODOP-COPER           (I) TO CDO-COPER              05730001
               MOVE CODOP-PCESSIONCUM     (I) TO CDO-PCESSIONCUM        05750001
               MOVE CODOP-PCESSIONCUM-EXC (I) TO CDO-PCESSIONCUM-EXC    05760001
               MOVE CODOP-PRMPCUM         (I) TO CDO-PRMPCUM            05770001
               MOVE CODOP-PRMPCUM-EXC     (I) TO CDO-PRMPCUM-EXC        05780001
               MOVE CODOP-QTECUM          (I) TO CDO-QTECUM             05790001
               MOVE CODOP-QTECUM-EXC      (I) TO CDO-QTECUM-EXC         05800001
                                                                        05810001
               MOVE CORPS-DETAIL-OPERATION    TO ENR-BMD418O            05820001
                                                                        05830001
               PERFORM ECRITURE-ETAT                                    05840001
                                                                        05840101
           END-PERFORM.                                                 05841001
                                                                        05841101
           SET EDIT-OPE TO TRUE.                                        05842001
                                                                        05850001
       FIN-EDITION-OPERATION. EXIT.                                     05860001
                                                                        05860101
       EDITION-TOTAL-MAG SECTION.                                       05861001
                                                                        05861101
           IF  WCPT-LIGNE NOT =   WLONGUEUR-PAGE - 2                    05861301
               MOVE LIGNE-TIRET TO ENR-BMD418O                          05861401
               PERFORM ECRITURE-ETAT                                    05861601
           ELSE                                                         05861701
               ADD 1 TO WCPT-LIGNE                                      05861801
           END-IF.                                                      05861901
                                                                        05862001
           MOVE 'MAGASIN'                TO TMS-LIB                     05862101
           MOVE WCLE-NLIEU               TO TMS-NLIEU                   05862201
           MOVE TMAG-PCESSIONCUM         TO TMS-PCESSIONCUM             05862301
           MOVE TMAG-PCESSIONCUM-EXC     TO TMS-PCESSIONCUM-EXC         05862401
           MOVE TMAG-PRMPCUM             TO TMS-PRMPCUM                 05862501
           MOVE TMAG-PRMPCUM-EXC         TO TMS-PRMPCUM-EXC             05862601
           MOVE TMAG-QTECUM              TO TMS-QTECUM                  05862701
           MOVE TMAG-QTECUM-EXC          TO TMS-QTECUM-EXC              05862801
                                                                        05862901
           MOVE ' ' TO FD-ASA.                                          05863001
                                                                        05863101
           MOVE TOTAL-MAG-SOCIETE TO ENR-BMD418O.                       05863201
           PERFORM ECRITURE-ETAT.                                       05863301
                                                                        05863401
       FIN-EDITION-TOTAL-MAG. EXIT.                                     05864001
                                                                        05870001
       TRI-FAMILLE SECTION.                                             05871001
                                                                        05872201
           PERFORM VARYING I FROM 1 BY 1                                05872301
             UNTIL I > WCPT-CUMUL-SOCF                                  05872401
                                                                        05872501
             PERFORM VARYING J FROM 2 BY 1                              05872601
             UNTIL J > WCPT-CUMUL-SOCF                                  05872701
                                                                        05872801
                 IF WTAB-SOCF-WSEQFAM (J - 1 )                          05872901
                    > WTAB-SOCF-WSEQFAM (J)                             05873001
                                                                        05873101
                    MOVE WPOSTE-CUMUL-SOCIETE (J)                       05873201
                     TO  WPOSTE-CUMUL-SOCIETE (1000)                    05873301
                    MOVE WPOSTE-CUMUL-SOCIETE (J - 1)                   05873401
                     TO  WPOSTE-CUMUL-SOCIETE (J)                       05873501
                    MOVE WPOSTE-CUMUL-SOCIETE (1000)                    05873601
                     TO  WPOSTE-CUMUL-SOCIETE (J - 1)                   05873701
                                                                        05873801
                    INITIALIZE WPOSTE-CUMUL-SOCIETE (1000)              05873901
                 END-IF                                                 05874001
                                                                        05874201
             END-PERFORM                                                05874301
                                                                        05874401
           END-PERFORM.                                                 05874501
                                                                        05877401
       FIN-TRI-FAMILLE. EXIT.                                           05877501
                                                                        05878001
       EDITION-SOCIETE SECTION.                                         05880001
                                                                        05890001
              INITIALIZE     WTOTAL-MAG.                                05890101
                                                                        05890201
              MOVE ' ' TO FD-ASA.                                       05891001
                                                                        05892001
      * - EDITION DE L'ENTETE                                           05900001
              MOVE 0       TO WCPT-LIGNE.                               05910001
                                                                        05912001
              PERFORM EDITION-ENTETE-SOC                                05920001
                                                                        05921001
              SET EDIT-ENTETE TO TRUE.                                  05922001
                                                                        05923001
              PERFORM VARYING I FROM 1 BY 1                             05930001
                UNTIL I > WCPT-CUMUL-SOCF-MAX                           05940001
                   OR WTAB-SOCF-CFAM (I) <= SPACES                      05940101
                                                                        05940201
                   IF NOT EDIT-ENTETE                                   05941001
                   AND WCPT-LIGNE <   WLONGUEUR-PAGE - 3                05941101
                      MOVE ' '          TO FD-ASA                       05942001
                      MOVE LIGNE-TIRET2 TO ENR-BMD418O                  05943001
                      PERFORM ECRITURE-ETAT                             05944001
                   END-IF                                               05945001
      * - FOR�AGE DE RUPTURE PAGE                                       05945101
                   IF WCPT-LIGNE = WLONGUEUR-PAGE - 3                   05946001
                      ADD 1 TO WCPT-LIGNE                               05947001
                   END-IF                                               05948001
                                                                        05950001
                   MOVE WTAB-SOCF-CFAM            (I) TO WSOCF-CFAM     05960001
                   MOVE WTAB-SOCF-LFAM            (I) TO WSOCF-LFAM     05970001
                                                                        05971001
                   MOVE WTAB-SOCF-PCESSIONCUM     (I)                   05980001
                     TO WSOCF-PCESSIONCUM                               05981001
                   MOVE WTAB-SOCF-PCESSIONCUM-EXC (I)                   05990001
                     TO WSOCF-PCESSIONCUM-EXC                           05991001
                   MOVE WTAB-SOCF-PRMPCUM        (I) TO WSOCF-PRMPCUM   06000001
                   MOVE WTAB-SOCF-PRMPCUM-EXC    (I)                    06010001
                     TO WSOCF-PRMPCUM-EXC                               06011001
                   MOVE WTAB-SOCF-QTECUM         (I) TO WSOCF-QTECUM    06020001
                   MOVE WTAB-SOCF-QTECUM-EXC     (I)                    06030001
                     TO WSOCF-QTECUM-EXC                                06030101
                                                                        06031001
                   MOVE SOC-DETAIL-FAM               TO ENR-BMD418O     06040001
                                                                        06040101
                   SET EDIT-SOC TO TRUE                                 06040201
                                                                        06040301
                  MOVE ' '                          TO FD-ASA           06041001
                  PERFORM ECRITURE-ETAT                                 06050001
                                                                        06060001
                  PERFORM VARYING J FROM 1 BY 1                         06060101
      *{ reorder-array-condition 1.1                                            
      *              UNTIL WTAB-SOCO-COPER (I, J) <= SPACES             06060201
      *                 OR J > WCPT-CUMUL-SOCO-MAX                      06060301
      *                                                                         
      *--                                                                       
                     UNTIL J > WCPT-CUMUL-SOCO-MAX                              
                                                                        06060401
                        OR WTAB-SOCO-COPER (I, J) <= SPACES                     
      *}                                                                        
                     MOVE WTAB-SOCO-COPER           (I, J)              06061001
                       TO WSOCO-COPER                                   06062001
                                                                        06062101
                     MOVE WTAB-SOCO-PCESSIONCUM     (I, J)              06063001
                       TO WSOCO-PCESSIONCUM                             06063101
                     MOVE WTAB-SOCO-PCESSIONCUM-EXC (I, J)              06064001
                       TO WSOCO-PCESSIONCUM-EXC                         06065001
                     MOVE WTAB-SOCO-PRMPCUM         (I, J)              06066001
                       TO WSOCO-PRMPCUM                                 06066101
                     MOVE WTAB-SOCO-PRMPCUM-EXC     (I, J)              06067001
                       TO WSOCO-PRMPCUM-EXC                             06067101
                     MOVE WTAB-SOCO-QTECUM          (I, J)              06068001
                       TO WSOCO-QTECUM                                  06068101
                     MOVE WTAB-SOCO-QTECUM-EXC      (I, J)              06069001
                       TO WSOCO-QTECUM-EXC                              06069101
                                                                        06069201
                     MOVE SOC-DETAIL-OPERATION TO ENR-BMD418O           06069301
                     MOVE ' '                          TO FD-ASA        06069401
                     PERFORM ECRITURE-ETAT                              06069501
                                                                        06069601
                      ADD WTAB-SOCO-PCESSIONCUM (I, J)                  06069901
                      TO TMAG-PCESSIONCUM                               06070001
                      ADD WTAB-SOCO-PCESSIONCUM-EXC (I, J)              06070101
                      TO TMAG-PCESSIONCUM-EXC                           06070201
                      ADD WTAB-SOCO-PRMPCUM (I, J)                      06070301
                      TO TMAG-PRMPCUM                                   06070401
                      ADD WTAB-SOCO-PRMPCUM-EXC (I, J)                  06070501
                      TO TMAG-PRMPCUM-EXC                               06070601
                      ADD WTAB-SOCO-QTECUM (I, J)                       06070701
                      TO TMAG-QTECUM                                    06070801
                      ADD WTAB-SOCO-QTECUM-EXC (I, J)                   06070901
                      TO TMAG-QTECUM-EXC                                06071001
                                                                        06071101
              END-PERFORM                                               06071201
           END-PERFORM.                                                 06071301
                                                                        06071401
           IF WCPT-LIGNE < WLONGUEUR-PAGE      - 2                      06071501
              MOVE LIGNE-TIRET TO ENR-BMD418O                           06072001
              PERFORM ECRITURE-ETAT                                     06073001
           END-IF.                                                      06074001
                                                                        06074101
           PERFORM EDITION-TOTAL-SOC.                                   06074201
                                                                        06074301
           IF WCPT-LIGNE < WLONGUEUR-PAGE      - 2                      06075001
              MOVE LIGNE-TIRET TO ENR-BMD418O                           06076001
              PERFORM ECRITURE-ETAT                                     06077001
           END-IF.                                                      06078001
                                                                        06080001
       FIN-EDITION-SOCIETE. EXIT.                                       06130001
                                                                        06140001
       EDITION-TOTAL-SOC SECTION.                                       06141001
                                                                        06142001
           MOVE 'SOCIETE'                TO TMS-LIB                     06143001
           MOVE ENTETE-FIL               TO TMS-NLIEU                   06144001
           MOVE TMAG-PCESSIONCUM         TO TMS-PCESSIONCUM             06145001
           MOVE TMAG-PCESSIONCUM-EXC     TO TMS-PCESSIONCUM-EXC         06146001
           MOVE TMAG-PRMPCUM             TO TMS-PRMPCUM                 06147001
           MOVE TMAG-PRMPCUM-EXC         TO TMS-PRMPCUM-EXC             06148001
           MOVE TMAG-QTECUM              TO TMS-QTECUM                  06149001
           MOVE TMAG-QTECUM-EXC          TO TMS-QTECUM-EXC              06149101
                                                                        06149201
           MOVE ' ' TO FD-ASA.                                          06149301
                                                                        06149401
           MOVE TOTAL-MAG-SOCIETE TO ENR-BMD418O.                       06149501
           PERFORM ECRITURE-ETAT.                                       06149601
                                                                        06149701
       FIN-EDITION-TOTAL-SOC. EXIT.                                     06149901
                                                                        06150001
       RAZ-ZONES-CUMUL SECTION.                                         06151001
                                                                        06160001
           MOVE 1 TO I.                                                 06170001
                                                                        06180001
           MOVE SPACES TO FAM-CFAM                                      06190001
                          FAM-LFAM                                      06200001
                                                                        06201001
           MOVE 0      TO FAM-PCESSIONCUM                               06210001
                          FAM-PCESSIONCUM-EXC                           06220001
                          FAM-PRMPCUM                                   06230001
                          FAM-PRMPCUM-EXC                               06240001
                          FAM-QTECUM                                    06250001
                          FAM-QTECUM-EXC.                               06260001
                                                                        06261001
           MOVE 0      TO WCPT-CODOP                                    06262001
                                                                        06262101
           INITIALIZE     WTAB-CODOP.                                   06263001
                                                                        06270001
       FIN-RAZ-ZONES-CUMUL. EXIT.                                       06280001
                                                                        06400001
       RAZ-WTAB-CUMUL-SOC SECTION.                                      06410001
                                                                        06420001
           INITIALIZE  WTAB-CUMUL-SOCIETE.                              06421001
                                                                        06530001
       FIN-RAZ-WTAB-CUMUL-SOC. EXIT.                                    06540001
                                                                        06550001
       ECRITURE-ETAT SECTION.                                           06560001
                                                                        06570301
           IF WCPT-LIGNE >= WLONGUEUR-PAGE      - 1                     06571001
              MOVE 0               TO WCPT-LIGNE                        06571301
              MOVE FD-BMD418O      TO WZONE-TAMPON133                   06571401
                                                                        06571701
              IF EDIT-SOC                                               06571801
                 PERFORM EDITION-ENTETE-SOC                             06571901
              ELSE                                                      06572001
                 PERFORM EDITION-ENTETE                                 06572101
              END-IF                                                    06572201
                                                                        06572301
              IF WZONE-TAMPON133 (2:2) = '+-'                           06572401
                 GO TO FIN-ECRITURE-ETAT                                06572501
              END-IF                                                    06572601
                                                                        06572701
              MOVE WZONE-TAMPON133 TO FD-BMD418O                        06572801
           END-IF                                                       06572901
                                                                        06610401
           ADD 1 TO WCPT-LIGNE.                                         06630001
           WRITE  FD-BMD418O.                                           06631001
                                                                        06670001
       FIN-ECRITURE-ETAT. EXIT.                                         06680001
                                                                        06690001
       FIN-BMD418    SECTION.                                           06700001
                                                                        06710001
           CLOSE FC-BMD418O                                             06720001
                 FC-BMD418I.                                            06730001
                                                                        06740001
           IF TRT-ERREUR                                                06750001
              PERFORM FIN-ANORMALE                                      06760001
           END-IF.                                                      06770001
                                                                        06780001
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    06790001
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        06800001
       FIN-FIN-BMD418.                                                  06810001
           EXIT.                                                        06820001
                                                                        06830001
       EDITION-ENTETE SECTION.                                          06840001
                                                                        06851001
           MOVE ' ' TO FD-ASA.                                          06852001
                                                                        06870001
           MOVE 0   TO WCPT-LIGNE.                                      06880001
           ADD  1   TO WCPT-PAGE.                                       06890001
                                                                        06900001
           MOVE WCPT-PAGE TO ENTETE-PAGE.                               06910001
                                                                        06920001
           IF WCPT-PAGE  > 1                                            06930001
              MOVE LIGNE-TIRET TO ENR-BMD418O                           06940001
              PERFORM ECRITURE-ETAT                                     06950001
           END-IF.                                                      06960001
                                                                        06970001
      * SAUT DE PAGE                                                    06980001
                                                                        06990001
           MOVE '1' TO FD-ASA.                                          07000001
           MOVE LIGNE-TIRET TO ENR-BMD418O.                             07010001
           PERFORM ECRITURE-ETAT.                                       07020001
                                                                        07030001
           MOVE ' ' TO FD-ASA.                                          07040001
           MOVE ENTETE-LIB  TO ENR-BMD418O.                             07050001
           PERFORM ECRITURE-ETAT.                                       07060001
                                                                        07070001
           MOVE ' ' TO FD-ASA.                                          07080001
           MOVE FMD417-NSOC     TO ENTETE-FIL                           07090001
           MOVE WCLE-NLIEU      TO ENTETE-LIEU                          07100001
           MOVE WCLE-LLIEU      TO ENTETE-LLIEU                         07110001
                                                                        07120001
           MOVE ENTETE-MAG  TO ENR-BMD418O.                             07130001
           PERFORM ECRITURE-ETAT.                                       07140001
                                                                        07150001
           MOVE ' ' TO FD-ASA.                                          07160001
           MOVE LIGNE-TIRET TO ENR-BMD418O.                             07170001
           PERFORM ECRITURE-ETAT.                                       07180001
                                                                        07190001
           MOVE ' ' TO FD-ASA.                                          07200001
           MOVE CORPS-LIB   TO ENR-BMD418O.                             07210001
           PERFORM ECRITURE-ETAT.                                       07220001
                                                                        07230001
           MOVE ' ' TO FD-ASA.                                          07240001
           MOVE LIGNE-TIRET TO ENR-BMD418O.                             07250001
           PERFORM ECRITURE-ETAT.                                       07260001
                                                                        07270001
           SET EDIT-ENTETE TO TRUE.                                     07280001
                                                                        07290001
       FIN-EDITION-ENTETE. EXIT.                                        07300001
                                                                        07310001
       EDITION-ENTETE-SOC SECTION.                                      07320001
                                                                        07330001
           ADD 1 TO WCPT-PAGE.                                          07340001
                                                                        07341001
           MOVE WCPT-PAGE TO ENTETE-SOC-PAGE.                           07342001
                                                                        07350001
           IF WCPT-PAGE  > 1                                            07360001
              MOVE LIGNE-TIRET TO ENR-BMD418O                           07370001
              PERFORM ECRITURE-ETAT                                     07380001
           END-IF.                                                      07390001
                                                                        07400001
      * SAUT DE PAGE                                                    07410001
                                                                        07420001
           MOVE '1' TO FD-ASA.                                          07430001
           MOVE LIGNE-TIRET TO ENR-BMD418O.                             07440001
           PERFORM ECRITURE-ETAT.                                       07450001
                                                                        07460001
           MOVE ' ' TO FD-ASA.                                          07470001
           MOVE ENTETE-LIB-SOC TO ENR-BMD418O.                          07480001
           PERFORM ECRITURE-ETAT.                                       07490001
                                                                        07500001
           MOVE ' ' TO FD-ASA.                                          07541001
           MOVE ENTETE-SOC  TO ENR-BMD418O.                             07542001
           PERFORM ECRITURE-ETAT.                                       07543001
                                                                        07544001
           MOVE ' ' TO FD-ASA.                                          07545001
           MOVE LIGNE-TIRET TO ENR-BMD418O.                             07546001
           PERFORM ECRITURE-ETAT.                                       07547001
                                                                        07548001
           MOVE ' ' TO FD-ASA.                                          07550001
           MOVE CORPS-LIB      TO ENR-BMD418O.                          07560001
           PERFORM ECRITURE-ETAT.                                       07570001
                                                                        07610501
           MOVE ' ' TO FD-ASA.                                          07610601
           MOVE LIGNE-TIRET TO ENR-BMD418O.                             07610701
                                                                        07610801
           PERFORM ECRITURE-ETAT.                                       07610901
           SET EDIT-SOC TO TRUE.                                        07611001
                                                                        07620001
       FIN-EDITION-ENTETE-SOC. EXIT.                                    07630001
                                                                        07640001
       CONTROLE-DATE SECTION.                                           07730001
                                                                        07740001
           MOVE  '1'         TO  GFDATA.                                07750001
           MOVE  'BETDATC'   TO  W-CALLNAME.                            07760001
                                                                        07770001
           CALL  W-CALLNAME  USING  WORK-BETDATC.                       07780001
                                                                        07790001
           IF  GFVDAT  NOT  =  '1'                                      07800000
               MOVE   3                                  TO ABEND-CODE  07810001
               MOVE  '- ERREUR - FORMAT DE DATE ERRON�.' TO ABEND-MESS  07820001
               SET TRT-ERREUR TO TRUE                                   07830001
               PERFORM   FIN-BMD418                                     07840001
           END-IF.                                                      07850001
                                                                        07860001
       FIN-CONTROLE-DATE.                                               07870001
           EXIT.                                                        07880001
                                                                        07890001
       FIN-ANORMALE     SECTION.                                        07900001
                                                                        07910001
           DISPLAY '***************************************'.           07920001
           DISPLAY '*** PROGRAMME TERMINE ANORMALEMENT  ***'.           07930001
           DISPLAY '***************************************'.           07940001
                                                                        07950001
           MOVE  'BMD418'  TO    ABEND-PROG.                            07960001
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                07970001
                                                                        07980001
       FIN-FIN-ANORMALE.                                                07990000
           EXIT.                                                        08000001
                                                                        08280001
