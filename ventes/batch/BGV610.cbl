      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                  BGV610.                                     
       AUTHOR. FLORENCE.                                                        
      *------------------------------------------------------------*            
      *                      PROGRAMME                             *            
      *            D EDITION DES VENTES NON TOPEES                 *            
      *         ET DONT LA DATE DE LIVRAISON EST ANTERIEURE        *            
      *            A CELLE DU SAMEDI PRECEDEMENT                   *            
      *                                                            *            
      * DATE CREATION : 03:10:90                                   *            
      *------------------------------------------------------------*            
      * MODIFICATION LE 15/09/95 POUR AFFICHER LA ZONE WINTMAJ= 'M'*            
      * DANS LE CAS DE L'EMD.                                      *            
      *------------------------------------------------------------*            
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA                                               
      *    C01           IS SAUT.                                               
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           C01           IS SAUT                                                
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      *-----------FICHIER EN ENTREE---------.                                   
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FGV600   ASSIGN TO FGV600                                     
      *           ACCESS          IS SEQUENTIAL.                                
      *--                                                                       
           SELECT FGV600   ASSIGN TO FGV600                                     
      *{ Tr-Select-Org-Sequential-Bis 1.1                                       
      *           ORGANIZATION    IS SEQUENTIAL                                 
      *}                                                                        
                  ACCESS          IS SEQUENTIAL                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *-----------FICHIER DE L IMPRIMANTE                                       
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FIMP     ASSIGN TO FIMP.                                      
      *--                                                                       
           SELECT FIMP     ASSIGN TO FIMP                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *-----------FICHIER DE FDATE                                              
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE    ASSIGN TO FDATE.                                     
      *--                                                                       
           SELECT FDATE    ASSIGN TO FDATE                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      *---------------------------------------------------------------* 00590000
      *  D E F I N I T I O N    F I C H I E R    F D A T E            * 00600000
      *---------------------------------------------------------------* 00610000
      *                                                                 00620000
       FD  FDATE                                                        00630001
           RECORDING F                                                  00630001
           BLOCK 0 RECORDS                                              00640000
           LABEL RECORD STANDARD.                                       00650000
       01  ENREG-FDATE.                                                 00660000
           03  FILLER PIC X(80).                                                
      *                                                                         
      *---------------------------------------------------------------* 00590000
      *  D E F I N I T I O N    F I C H I E R    I M P R              * 00600000
      *---------------------------------------------------------------* 00610000
      *                                                                 00620000
       FD  FIMP                                                         00630001
           RECORDING F                                                  00630001
           BLOCK 0 RECORDS                                              00640000
           LABEL RECORD STANDARD.                                       00650000
       01  LIGNE1.                                                      00660000
           03  FILLER PIC X(132).                                               
      *                                                                         
      *                                                                 00700000
      *---------------------------------------------------------------* 00590000
      *  D E F I N I T I O N    F I C H I E R    E N  E N T R E E    *  00600000
      *---------------------------------------------------------------* 00610000
      *                                                                 00620000
       FD  FGV600                                                       00630001
           RECORDING F                                                  00630001
           BLOCK 0 RECORDS                                              00640000
           LABEL RECORD STANDARD.                                       00650000
       01  ENREG-FGV600.                                                00660000
           03  FGV600-NLIEU          PIC XXX.                                   
           03  FGV600-NVENTE         PIC X(7).                                  
           03  FGV600-NCODIC         PIC X(7).                                  
           03  FGV600-DDELIV         PIC X(8).                                  
           03  FGV600-DCREATION      PIC X(8).                                  
           03  FGV600-CMODDEL        PIC XXX.                                   
           03  FGV600-QVENDUE        PIC S9(5) COMP-3.                          
           03  FGV600-WINTMAJ        PIC X(1).                                  
      *                                                                 00700000
      *---------------------------------------------------------------* 00730000
      *                 W O R K I N G  -  S T O R A G E               * 00740000
      *---------------------------------------------------------------* 00750000
      *                                                                 00760000
       WORKING-STORAGE SECTION.                                         00770000
      *                                                                 00780000
        COPY ABENDCOP.                                                  00781001
      *                                                                 00782001
      *                                                                 00782001
      *---------------------------------------------------------------* 00790000
      *           D E S C R I P T I O N    D E S    Z O N E S         * 00800000
      *        S P E C I F I Q U E S    A U    P R O G R A M M E      * 00810000
      *---------------------------------------------------------------* 00820000
      *                                                                 00782001
      *                                                                 01160000
       01  CODE-FICHIER      PIC X     VALUE '0'.                       01160000
           88  FIN-FICHIER             VALUE '1'.                       01160000
      *                                                                 01160000
       01  CODE-TOTAL        PIC X     VALUE ' '.                       01160000
           88  TOTAL                   VALUE '0'.                       01160000
           88  NON-TOTAL               VALUE '1'.                       01160000
      *                                                                 01160000
       01  W-DATE.                                                      01160000
           03  W-JJ          PIC 99    VALUE 0.                         01190000
           03  W-MM          PIC 99    VALUE 0.                         01190000
           03  W-SS          PIC 99    VALUE 0.                         01190000
           03  W-AA          PIC 99    VALUE 0.                         01190000
      *                                                                 01160000
       01  ZONES.                                                       01160000
           03  CTR-PAGE      PIC 99    VALUE 0.                         01190000
           03  CTR-LIGNE     PIC 99    VALUE 0.                         01190000
           03  W-PREC-NMAG   PIC X(3).                                  01190000
           03  W-CUMUL-MAG   PIC S9(7) COMP-3.                          01190000
           03  W-CUMUL-TOT   PIC S9(7) COMP-3.                          01190000
      *                                                                         
      ** AIDA ************************************************* IRDF **         
      *  DESCRIPTION DE L'ETAT IGV600                                           
      *****************************************************************         
      *                                                                         
       01  IGV600.                                                              
      *****************************************************************         
      *  ENTETE PHYSIQUE DE PAGE                                                
      *****************************************************************         
      *****************************************************************         
          02 LIGNE01 .                                                          
            04 FILLER PIC X(20) VALUE '  I G V 6 1 0       '.                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE 'E T A B L I S S E M '.                   
            04 FILLER PIC X(20) VALUE 'E N T S    D A R T Y'.                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(17) VALUE '           PAGE  '.                      
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 Z-NPAGE PIC Z(3)9.                                               
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE02 .                                                          
            04 FILLER PIC X(11) VALUE 'EDITE LE : '.                            
            04 Z-DJJ  PIC X(02).                                                
            04 FILLER PIC X(01) VALUE '/'.                                      
            04 Z-DMM  PIC X(02).                                                
            04 FILLER PIC X(01) VALUE '/'.                                      
            04 Z-DAA  PIC X(02).                                                
            04 FILLER PIC X(21) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(11) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE03 .                                                          
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '!   ETAT DES VENTES '.                   
            04 FILLER PIC X(20) VALUE 'NON TOPEES,DONT    !'.                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(11) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE04 .                                                          
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '! LA DATE DE LIVRAIS'.                   
            04 FILLER PIC X(20) VALUE 'ON EST ANTERIEURE  !'.                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(11) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE05 .                                                          
            04 FILLER PIC X(12) VALUE '  MAGASIN : '.                           
            04 L5-MAG PIC X(03) VALUE SPACES.                                   
            04 FILLER PIC X(05) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '!          A CELLE D'.                   
            04 FILLER PIC X(10) VALUE 'U SAMEDI: '.                             
            04 Z-JJ   PIC X(2).                                                 
            04 FILLER PIC X(1)  VALUE '/'.                                      
            04 Z-MM   PIC X(2).                                                 
            04 FILLER PIC X(1)  VALUE '/'.                                      
            04 Z-AA   PIC X(2).                                                 
            04 FILLER PIC X(2)  VALUE ' !'.                                     
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(11) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE06 .                                                          
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(11) VALUE SPACES.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE07 .                                                          
            04 FILLER PIC X(20) VALUE '+-------------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(20) VALUE '--------------------'.                   
            04 FILLER PIC X(11) VALUE '--+        '.                            
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE08 .                                                          
            04 FILLER PIC X(20) VALUE '!                !  '.                   
            04 FILLER PIC X(20) VALUE '              !     '.                   
            04 FILLER PIC X(20) VALUE '    !               '.                   
            04 FILLER PIC X(20) VALUE '    !               '.                   
            04 FILLER PIC X(20) VALUE '    !               '.                   
            04 FILLER PIC X(20) VALUE '    !               '.                   
            04 FILLER PIC X(11) VALUE '  !        '.                            
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE09 .                                                          
            04 FILLER PIC X(20) VALUE '! N � DE MAGASIN ! N'.                   
            04 FILLER PIC X(20) VALUE '� DE COMMANDE !  COD'.                   
            04 FILLER PIC X(20) VALUE 'IC  ! DATE DE LIVRAI'.                   
            04 FILLER PIC X(20) VALUE 'SON ! MODE DE DELIVR'.                   
            04 FILLER PIC X(20) VALUE 'ANCE! DATE DE CREATI'.                   
            04 FILLER PIC X(20) VALUE 'ON  ! QUANTITE VENDU'.                   
            04 FILLER PIC X(11) VALUE 'E !        '.                            
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE10 .                                                          
            04 FILLER PIC X(20) VALUE '!                !  '.                   
            04 FILLER PIC X(20) VALUE '              !     '.                   
            04 FILLER PIC X(20) VALUE '    !               '.                   
            04 FILLER PIC X(20) VALUE '    !               '.                   
            04 FILLER PIC X(20) VALUE '    !               '.                   
            04 FILLER PIC X(20) VALUE '    !               '.                   
            04 FILLER PIC X(10) VALUE '  !       '.                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE11 .                                                          
            04 FILLER PIC X(20) VALUE '!----------------+--'.                   
            04 FILLER PIC X(20) VALUE '--------------+-----'.                   
            04 FILLER PIC X(20) VALUE '----+---------------'.                   
            04 FILLER PIC X(20) VALUE '----+---------------'.                   
            04 FILLER PIC X(20) VALUE '----+---------------'.                   
            04 FILLER PIC X(20) VALUE '----+---------------'.                   
            04 FILLER PIC X(11) VALUE '--!        '.                            
            04 FILLER PIC X(01) VALUE SPACES.                                   
      *****************************************************************         
          02 LIGNE12 .                                                          
            04 FILLER PIC X(7) VALUE '!      '.                                 
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 Z-NMAG PIC X(3).                                                 
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(10) VALUE '     !    '.                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 Z-NVENTE PIC X(7).                                               
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(4) VALUE '   !'.                                    
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 Z-NCODIC PIC X(7).                                               
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(5) VALUE '!    '.                                   
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 Z-JJ1    PIC X(2).                                               
            04 FILLER PIC X(01) VALUE '/'.                                      
            04 Z-MM1    PIC X(2).                                               
            04 FILLER PIC X(01) VALUE '/'.                                      
            04 Z-AA1    PIC X(2).                                               
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(12) VALUE '     !      '.                           
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 Z-CMODDEL PIC X(3).                                              
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 Z-WINTMAJ PIC X(1).                                              
            04 FILLER PIC X(13) VALUE '       !     '.                          
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 Z-JJ2    PIC X(2).                                               
            04 FILLER PIC X(01) VALUE '/'.                                      
            04 Z-MM2    PIC X(2).                                               
            04 FILLER PIC X(01) VALUE '/'.                                      
            04 Z-AA2    PIC X(2).                                               
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(10) VALUE '    !     '.                             
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 Z-QVENDUE PIC Z(4)9.                                             
            04 FILLER PIC X(07) VALUE '      !'.                                
      *****************************************************************         
          02 LIGNE13 .                                                          
            04 FILLER PIC X(20) VALUE '! TOTAL PAR MAGASIN '.                   
            04 FILLER PIC X(2) VALUE ': '.                                      
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 Z-TMAG2 PIC X(3).                                                
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(1) VALUE SPACES.                                    
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 Z-QTOTAL2 PIC Z(6)9.                                             
            04 FILLER PIC X(07) VALUE '      !'.                                
      *****************************************************************         
          02 LIGNE14 .                                                          
            04 FILLER PIC X(20) VALUE '! TOTAL GENERAL     '.                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(20) VALUE SPACES.                                   
            04 FILLER PIC X(8) VALUE SPACES.                                    
            04 FILLER PIC X(01) VALUE SPACES.                                   
            04 Z-QTOTAL3 PIC Z(6)9.                                             
            04 FILLER PIC X(07) VALUE '      !'.                                
      *                                                                 02700000
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
      *****************************************************************         
      *              T R A M E   DU   P R O G R A M M E               *         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *           ------------------------------------------          *         
      *           -- MODULE DE BASE DU PROGRAMME  BGV610  --          *         
      *           ------------------------------------------          *         
      *                               I                               *         
      *           -----------------------------------------           *         
      *           I                   I                   I           *         
      *   -----------------   -----------------   -----------------   *         
      *   -    ENTREE     -   -  TRAITEMENT   -   -     SORTIE    -   *         
      *   -----------------   -----------------   -----------------   *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-BGV610.                                                           
      *                                                                         
           PERFORM DEBUT-BGV610.                                                
           PERFORM TRAIT-BGV610.                                                
           PERFORM FIN-BGV610.                                                  
      *                                                                         
       F-MODULE-BGV610.  EXIT.                                                  
      *                                                                         
               EJECT                                                            
      *                                                                         
      *---------------------------------------------------------------*         
      *              D E B U T   B G V 6 1 0                          *         
      *---------------------------------------------------------------*         
      *                                                                         
       DEBUT-BGV610.                                                            
      *                                                                         
           PERFORM OUVERTURE-FICHIER.                                           
           PERFORM LECTURE-FDATE.                                               
           PERFORM LECTURE-FGV600.                                              
           MOVE ENREG-FGV600 TO W-DATE.                                         
           MOVE W-JJ         TO Z-JJ.                                           
           MOVE W-MM         TO Z-MM.                                           
           MOVE W-AA         TO Z-AA.                                           
      *                                                                         
       F-DEBUT-GGV610. EXIT.                                                    
      *                                                                         
               EJECT                                                            
      *                                                                         
      *---------------------------------------------------------------*         
      *              T R A I T   B G V 6 1 0                          *         
      *---------------------------------------------------------------*         
      *                                                                         
       TRAIT-BGV610.                                                            
      *                                                                         
           PERFORM LECTURE-FGV600.                                              
           PERFORM UNTIL FIN-FICHIER                                            
                   PERFORM SAUVEGARDE-LECTURE                                   
                   PERFORM EDITION-LIGNE                                        
                   PERFORM LECTURE-FGV600                                       
                   PERFORM TEST-EGALITE                                         
             END-PERFORM.                                                       
      *                                                                         
      *---------------------------------------------------------------*         
      *              L E C T U R E    F G V 6 1 0                     *         
      *---------------------------------------------------------------*         
      *                                                                         
       LECTURE-FGV600.                                                          
      *                                                                         
           READ FGV600 NEXT AT END SET FIN-FICHIER TO TRUE.                     
      *                                                                         
      *---------------------------------------------------------------*         
      *              S A U V E G A R D E                              *         
      *---------------------------------------------------------------*         
      *                                                                         
       SAUVEGARDE-LECTURE.                                                      
      *                                                                         
           MOVE FGV600-NLIEU TO W-PREC-NMAG.                                    
      *                                                                         
      *---------------------------------------------------------------*         
      *               T E S T                                         *         
      *---------------------------------------------------------------*         
      *                                                                         
       TEST-EGALITE.                                                            
      *                                                                         
           IF  (FGV600-NLIEU NOT = W-PREC-NMAG ) OR FIN-FICHIER                 
                PERFORM EDITION-TOTAL                                           
                PERFORM     TRAIT-FIN-TAB                               06880030
                PERFORM     TRAIT-ENT-TAB                               06890030
           END-IF.                                                              
      *                                                                         
      *---------------------------------------------------------------*         
      *              F I N     B G V 6 1 0                            *         
      *---------------------------------------------------------------*         
      *                                                                         
       FIN-BGV610.                                                              
      *                                                                         
           PERFORM FIN-PROGRAMME.                                               
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       F-FIN-BGV610.  EXIT.                                                     
      *                                                                         
         EJECT                                                                  
      *                                                                         
      *---------------------------------------------------------------*         
      *       M O D U L E S    D E B U T  B G V 6 1 0                 *         
      *---------------------------------------------------------------*         
      *                                                                         
       OUVERTURE-FICHIER.                                                       
      *------------------                                                       
           OPEN INPUT  FGV600.                                                  
           OPEN INPUT  FDATE.                                                   
           OPEN OUTPUT FIMP.                                                    
           MOVE 0 TO  W-CUMUL-MAG.                                      01190000
           MOVE 0 TO  W-CUMUL-TOT.                                      01190000
      *                                                                         
      *---------------------------------------------------------------*         
      *              L E C T U R E    F D A T E                       *         
      *---------------------------------------------------------------*         
      *                                                                         
       LECTURE-FDATE.                                                           
      *                                                                         
           READ FDATE INTO W-DATE  AT END DISPLAY 'FDATE VIDE'                  
                                   GO TO FIN-PROGRAMME.                         
           MOVE W-JJ TO Z-DJJ.                                                  
           MOVE W-MM TO Z-DMM.                                                  
           MOVE W-AA TO Z-DAA.                                                  
      *                                                                         
      *---------------------------------------------------------------*         
      *       M O D U L E S    E D I T I O N                          *         
      *---------------------------------------------------------------*         
      *                                                                         
       EDITION-LIGNE.                                                           
      *------------------                                                       
      *                                                                 06820029
           IF CTR-PAGE = 0                                              06830030
              PERFORM     TRAIT-ENT-TAB                                 06840030
           END-IF.                                                      06850031
      *                                                                 06860030
           IF CTR-LIGNE > 56                                            06870030
              PERFORM     TRAIT-FIN-TAB                                 06880030
              PERFORM     TRAIT-ENT-TAB                                 06890030
           END-IF.                                                      06900030
      *                                                                 06910030
           MOVE FGV600-NLIEU   TO Z-NMAG.                               06960030
           MOVE FGV600-NVENTE  TO Z-NVENTE.                             06960030
           MOVE FGV600-NCODIC  TO Z-NCODIC.                             06960030
           MOVE FGV600-DDELIV  TO W-DATE .                              06960030
           MOVE W-MM           TO Z-AA1.                                        
           MOVE W-SS           TO Z-MM1.                                        
           MOVE W-AA           TO Z-JJ1.                                        
           MOVE FGV600-DCREATION TO W-DATE .                            06960030
           MOVE W-MM           TO Z-AA2.                                        
           MOVE W-SS           TO Z-MM2.                                        
           MOVE W-AA           TO Z-JJ2.                                        
           MOVE FGV600-CMODDEL TO Z-CMODDEL.                            06960030
           MOVE FGV600-QVENDUE TO Z-QVENDUE.                            06960030
           COMPUTE W-CUMUL-MAG = FGV600-QVENDUE + W-CUMUL-MAG           06960030
           COMPUTE W-CUMUL-TOT = FGV600-QVENDUE + W-CUMUL-TOT           06960030
           MOVE SPACES         TO Z-WINTMAJ.                            06960030
           IF FGV600-CMODDEL = 'EMD'                                    06960030
              IF FGV600-WINTMAJ = 'M'                                   06960030
                 MOVE FGV600-WINTMAJ  TO Z-WINTMAJ                      06970035
              END-IF                                                    06970035
           END-IF                                                       06970035
           WRITE LIGNE1        FROM LIGNE12.                            06970035
           ADD 1               TO CTR-LIGNE.                            06980030
           SET NON-TOTAL TO TRUE.                                               
      *                                                                 06990029
      *---------------------------------------------------------------* 07000029
      *         ENTETE DE LA PAGE                                     * 07010030
      *---------------------------------------------------------------* 07020029
      *                                                                 07030029
       TRAIT-ENT-TAB.                                                   07040031
      *---------------                                                  07050030
      *                                                                 07060030
           ADD 1               TO CTR-PAGE.                             07070030
           MOVE  CTR-PAGE      TO Z-NPAGE.                              07120033
      *                                                                 07130033
           SET NON-TOTAL TO TRUE.                                               
           WRITE LIGNE1        FROM LIGNE01 AFTER ADVANCING SAUT.       07140030
           WRITE LIGNE1        FROM LIGNE02.                            07150030
           WRITE LIGNE1        FROM LIGNE03.                            07160030
           WRITE LIGNE1        FROM LIGNE04.                            07170030
           MOVE FGV600-NLIEU   TO L5-MAG.                                       
           IF FIN-FICHIER                                                       
           MOVE '000'          TO L5-MAG.                                       
           WRITE LIGNE1        FROM LIGNE05.                            07180030
           WRITE LIGNE1        FROM LIGNE06.                            07190035
           WRITE LIGNE1        FROM LIGNE07.                            07200035
           WRITE LIGNE1        FROM LIGNE08.                            07210035
           WRITE LIGNE1        FROM LIGNE09.                            07210035
           WRITE LIGNE1        FROM LIGNE10.                            07210035
           WRITE LIGNE1        FROM LIGNE11.                            07210035
           MOVE  11            TO CTR-LIGNE.                            07220035
      *                                                                 07230030
      *---------------------------------------------------------------* 07240030
      *         FIN    DE LA PAGE                                     * 07250030
      *---------------------------------------------------------------* 07260030
      *                                                                 07270030
       TRAIT-FIN-TAB.                                                   07280031
      *---------------                                                  07290030
      *                                                                 07300030
           IF NON-TOTAL                                                         
              WRITE LIGNE1        FROM LIGNE11                          07210035
           END-IF.                                                              
      *                                                                 07230030
      *---------------------------------------------------------------* 07240030
      *         EDITION TOTAL   MAGASIN                               * 07250030
      *---------------------------------------------------------------* 07260030
      *                                                                 07270030
       EDITION-TOTAL.                                                   07280031
      *---------------                                                  07290030
      *                                                                 07300030
           MOVE W-CUMUL-MAG TO Z-QTOTAL2.                                       
           MOVE W-PREC-NMAG TO Z-TMAG2.                                         
           SET TOTAL TO TRUE.                                                   
           WRITE LIGNE1        FROM LIGNE11.                            07210035
           WRITE LIGNE1        FROM LIGNE13.                            07210035
           WRITE LIGNE1        FROM LIGNE11.                            07210035
           ADD 3               TO CTR-LIGNE.                            06980030
           MOVE 0 TO  W-CUMUL-MAG .                                     06960030
      *                                                                 07230030
      *---------------------------------------------------------------* 07240030
      *         FIN DU PROGRAMME                                      * 07250030
      *---------------------------------------------------------------* 07260030
      *                                                                 07270030
       FIN-PROGRAMME.                                                   07280031
      *---------------                                                  07290030
      *                                                                 07300030
           MOVE W-CUMUL-TOT TO Z-QTOTAL3.                                       
           MOVE W-PREC-NMAG TO Z-TMAG2.                                         
           SET TOTAL TO TRUE.                                                   
           WRITE LIGNE1        FROM LIGNE14.                            07210035
           WRITE LIGNE1        FROM LIGNE11.                            07210035
           CLOSE FIMP.                                                  07210035
           CLOSE FDATE.                                                 07210035
           CLOSE FGV600.                                                06960030
