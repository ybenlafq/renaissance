      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000010
       PROGRAM-ID.    BGV004.                                           00000020
       AUTHOR.                                                          00000030
      *                                                                 00000040
      * ============================================================= * 00000050
      *                                                               * 00000060
      *       PROJET        :  STATISTIQUE COMMERCIALE                * 00000070
      *       PROGRAMME     :  BGV004.                                * 00000080
      *       FONCTION      :  EXTRACTION MAX DATE DE VENTE           * 00000090
      *                                                               * 00000110
      * ============================================================= * 00000120
      *                                                                 00000130
       ENVIRONMENT DIVISION.                                            00000140
       CONFIGURATION SECTION.                                           00000150
       SPECIAL-NAMES.                                                   00000160
           DECIMAL-POINT IS COMMA.                                      00000170
       INPUT-OUTPUT SECTION.                                            00000180
      *                                                                 00000190
       FILE-CONTROL.                                                    00000200
      *                                                                 00000210
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FHV02         ASSIGN  TO  FHV02.                    00000240
      *                                                                         
      *--                                                                       
            SELECT  FHV02         ASSIGN  TO  FHV02                             
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00000250
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FICDAT        ASSIGN  TO  FICDAT.                   00000240
      *                                                                         
      *--                                                                       
            SELECT  FICDAT        ASSIGN  TO  FICDAT                            
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00000250
      *}                                                                        
      *                                                                 00000260
       DATA DIVISION.                                                   00000270
       FILE SECTION.                                                    00000280
      *                                                                 00000290
      * ============================================================= * 00000450
      *  D E F I N I T I O N    F I C H I E R    E N T R E E          * 00000460
      * ============================================================= * 00000470
      *                                                                 00000480
       FD   FHV02                                                       00000490
            RECORDING F                                                 00000350
            BLOCK 0 RECORDS                                             00000360
            LABEL RECORD STANDARD.                                      00000500
JA                                                                      00000510
JA    **********************************************************        00000510
JA    *  DESCRIPTION FICHIER CORRESPONDANT TABLE HV02          *        00000510
JA    **********************************************************        00000510
JA     01  HV02-DSECT.                                                  00003770
JA         03  HV02-NSOCIETE           PIC  X(03).                      00003780
JA         03  HV02-NCODIC             PIC  X(07).                      00003790
JA         03  HV02-DVENTECIALE        PIC  X(08).                      00003800
JA         03  HV02-QPIECES            PIC  S9(05)     COMP-3.          00003810
JA         03  HV02-PCA                PIC  S9(07)V99  COMP-3.          00003820
JA         03  HV02-PMTACHATS          PIC  S9(07)V99  COMP-3.          00003830
JA         03  HV02-QPIECES-EMP        PIC  S9(05)     COMP-3.          00003850
JA         03  HV02-PCA-EMP            PIC  S9(07)V99  COMP-3.          00003860
JA         03  HV02-PMTACHATS-EMP      PIC  S9(07)V99  COMP-3.          00003870
JA         03  HV02-QPIECES-COM        PIC  S9(05)     COMP-3.          00003850
JA         03  HV02-PCA-COM            PIC  S9(07)V99  COMP-3.          00003860
JA         03  HV02-PMTACHATS-COM      PIC  S9(07)V99  COMP-3.          00003870
JA         03  HV02-PMTPRIMES          PIC  S9(07)V99  COMP-3.          00003860
JA         03  HV02-PMTPRIMVOL         PIC  S9(07)V99  COMP-3.          00003860
JA         03  HV02-QPIECESEXC         PIC  S9(05)     COMP-3.          00003600
JA         03  HV02-PCAEXC             PIC  S9(07)V99  COMP-3.          00003650
JA         03  HV02-PMTACHATSEXC       PIC  S9(07)V99  COMP-3.          00003640
JA         03  HV02-QPIECESEXE         PIC  S9(05)     COMP-3.          00003600
JA         03  HV02-PCAEXE             PIC  S9(07)V99  COMP-3.          00003650
JA         03  HV02-PMTACHATSEXE       PIC  S9(07)V99  COMP-3.          00003640
      * ============================================================= * 00000450
      *  D E F I N I T I O N    F I C H I E R    S O R T I E          * 00000460
      * ============================================================= * 00000470
      *                                                                 00000480
       FD   FICDAT                                                      00000490
            RECORDING F                                                 00000350
            BLOCK 0 RECORDS                                             00000360
            LABEL RECORD STANDARD.                                      00000500
                                                                        00000510
       01   ENREG          PIC X(80).                                   00000390
      *                                                                 00000640
      * ============================================================= * 00000650
      *                 W O R K I N G  -  S T O R A G E               * 00000660
      * ============================================================= * 00000670
      *                                                                 00000680
       WORKING-STORAGE SECTION.                                         00000690
      *                                                                 00000700
       01  FILLER.                                                      00001130
           02  CODE-HV02                 PIC  9      VALUE 0.           00000530
               88  NON-FIN-HV02                      VALUE 0.           00000540
               88  FIN-HV02                          VALUE 1.           00000550
                                                                        00000560
      *================================================================*00001180
      *       DESCRIPTION DE LA ZONE D'ENREGISTREMENT (FICDAT)         *00001190
      *================================================================*00001200
                                                                        00001210
           02  FICDAT-DSECT.                                            00001220
               03  F-DVENTECIALE         PIC  X(08).                    00001230
               03  FILLER                PIC  X(72).                    00001230
      *                                                                 00001260
      * ============================================================= * 00001270
      *        D E S C R I P T I O N      D E S      Z O N E S        * 00001280
      *           D ' A P P E L    M O D U L E    A B E N D           * 00001290
      * ============================================================= * 00001300
      *                                                                 00001310
           COPY  ABENDCOP.                                              00001320
      *                                                                 00001330
       PROCEDURE DIVISION.                                              00001340
      *                                                                 00001350
                                                                        00001530
           PERFORM  DEBUT-BGV004.                                       00001540
           IF NON-FIN-HV02                                                      
              PERFORM  TRAIT-BGV004                                     00001550
           END-IF                                                               
           PERFORM  FIN-BGV004.                                         00001560
                                                                        00001570
      * ============================================================= * 00001580
      *                  D E B U T        B G V 0 0 5                 * 00001590
      * ============================================================= * 00001600
      *                                                                 00001610
       DEBUT-BGV004      SECTION.                                       00001620
                                                                        00001630
           OPEN  INPUT  FHV02                                           00002070
           OPEN  OUTPUT FICDAT.                                         00002080
           READ FHV02 AT END                                            00001660
                DISPLAY 'FICHIER HV02 VIDE'                                     
                SET FIN-HV02 TO TRUE                                            
           END-READ.                                                            
      *                                                                 00001700
      * ============================================================= * 00001710
      *                    T R A I T        B G V 3 0 5               * 00001720
      * ============================================================= * 00001730
      *                                                                 00001740
       TRAIT-BGV004     SECTION.                                        00001750
                                                                        00001760
           INITIALIZE FICDAT-DSECT.                                             
           MOVE HV02-DVENTECIALE TO F-DVENTECIALE.                              
           PERFORM  ECRITURE-FICDAT.                                    00001800
      *                                                                 00001840
      * ============================================================= * 00001850
      *                      F I N        B G V 0 0 4                 * 00001860
      * ============================================================= * 00001870
      *                                                                 00001880
       FIN-BGV004     SECTION.                                          00001890
                                                                        00001900
           CLOSE  FICDAT FHV02.                                         00004260
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP  RUN.                                                   00004370
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                 00004890
      *-----------------------------------------------------------------00003760
      *          E C R I T U R E                                      * 00003770
      *-----------------------------------------------------------------00003780
      *                                                                 00003790
       ECRITURE-FICDAT SECTION.                                         00003800
                                                                        00003810
           WRITE ENREG FROM FICDAT-DSECT.                               00003820
                                                                        00003840
      *-----------------------------------------------------------------00003900
      *       F I N  -  A N O R M A L E.                                00003910
      *-----------------------------------------------------------------00003920
      *                                                                 00003930
       FIN-ANORMALE    SECTION.                                         00003940
                                                                        00003950
           MOVE  'BGV004'                  TO  ABEND-PROG.              00003960
                                                                        00003970
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                00003980
                                                                        00003990
