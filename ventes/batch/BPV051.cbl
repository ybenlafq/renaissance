      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                BPV051.                                       
       AUTHOR. DSA019.                                                          
      *===============================================================*         
      *                        BPV051                                 *         
      *  PROGRAMME D'EDITION DES STATS DE VENTE PAR RUBRIQUE PRODUIT  *         
      *   DATE DE CREATION : 19/11/93.                                *         
      *===============================================================*         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FMOIS  ASSIGN TO  FMOIS.                                     
      *--                                                                       
            SELECT FMOIS  ASSIGN TO  FMOIS                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FNSOC  ASSIGN TO  FNSOC.                                     
      *--                                                                       
            SELECT FNSOC  ASSIGN TO  FNSOC                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FDATE  ASSIGN TO  FDATE.                                     
      *--                                                                       
            SELECT FDATE  ASSIGN TO  FDATE                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FPV050 ASSIGN TO  FPV050.                                    
      *--                                                                       
            SELECT FPV050 ASSIGN TO  FPV050                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT IPV050 ASSIGN TO  IPV050.                                    
      *--                                                                       
            SELECT IPV050 ASSIGN TO  IPV050                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
       FD   FMOIS                                                               
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01  PARAMETRE-MOIS.                                                      
           03  ENR-FMOIS.                                                       
               05 FMOIS-MM          PIC  X(02).                                 
               05 FMOIS-SS          PIC  X(02).                                 
               05 FMOIS-AA          PIC  X(02).                                 
           03  FILLER               PIC  X(74).                                 
      *                                                                         
       FD   FNSOC                                                               
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01  PARAMETRE-SOC.                                                       
           03  FNSOCIETE            PIC  X(03).                                 
           03  FILLER               PIC  X(77).                                 
      *                                                                         
       FD   FDATE                                                               
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01  PARAMETRE-FDATE.                                                     
           03  FDATE-JJ             PIC  X(02).                                 
           03  FDATE-MM             PIC  X(02).                                 
           03  FDATE-SS             PIC  X(02).                                 
           03  FDATE-AA             PIC  X(02).                                 
           03  FILLER               PIC  X(72).                                 
      *                                                                         
       FD   FPV050                                                              
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
      *-- LONGUEUR DE 180 C.                                                    
       COPY SWPV050.                                                            
       FD   IPV050                                                              
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01  IPV050-ENREG.                                                        
           03  IPV050-ASA           PIC  X(1).                                  
           03  IPV050-LIG           PIC  X(132).                                
      *                                                                         
      *================================================================*        
       WORKING-STORAGE         SECTION.                                         
      *================================================================*        
      *                                                                         
       77  BETDATC            PIC X(08)  VALUE 'BETDATC'.                       
       77  W-FNSOC            PIC X(03)  VALUE SPACES.                          
       77  W-NMAG             PIC X(03)  VALUE SPACES.                          
       77  W-LAGREGATED       PIC X(20)  VALUE SPACES.                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-NB-FPV050        PIC S9(06) COMP VALUE ZERO.                       
      *--                                                                       
       77  W-NB-FPV050        PIC S9(06) COMP-5 VALUE ZERO.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  CPT-DETAIL         PIC S9(04) COMP VALUE ZERO.                       
      *--                                                                       
       77  CPT-DETAIL         PIC S9(04) COMP-5 VALUE ZERO.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  CPT-LIGNE          PIC S9(04) COMP VALUE ZERO.                       
      *--                                                                       
       77  CPT-LIGNE          PIC S9(04) COMP-5 VALUE ZERO.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-NLIGNE-MAX       PIC S9(04) COMP VALUE 60.                         
      *--                                                                       
       77  W-NLIGNE-MAX       PIC S9(04) COMP-5 VALUE 60.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  CPT-PAGE           PIC S9(04) COMP VALUE ZERO.                       
      *--                                                                       
       77  CPT-PAGE           PIC S9(04) COMP-5 VALUE ZERO.                     
      *}                                                                        
       77  W-MTREM            PIC S9(09)V99 COMP-3 VALUE ZERO.                  
       77  W-PMTCA            PIC S9(07)    COMP-3 VALUE ZERO.                  
       77  W-MTCOMM           PIC S9(09)V999 COMP-3 VALUE ZERO.                 
       77  W-QNBPSE           PIC S9(07)V9  COMP-3 VALUE ZERO.                  
      *-- INDICE 1 = EDITION "AUTRES"                                           
      *-- INDICE 2 = TOTAL DU MOIS                                              
      *-- INDICE 3 = CUMUL EXERCICE                                             
       01  W-FPV050-DSECT.                                                      
           03  W-FPV050-TAB-VENTES OCCURS 3.                                    
              05  W-FPV050-QVENDUE  PIC S9(07)    COMP-3.                       
              05  W-FPV050-PMTCA    PIC S9(09)    COMP-3.                       
              05  W-FPV050-MTREM    PIC S9(09)V99 COMP-3.                       
              05  W-FPV050-MTCOMM   PIC S9(09)V99 COMP-3.                       
              05  W-FPV050-QNBPSE   PIC S9(07)    COMP-3.                       
              05  W-FPV050-QNBPSAB  PIC S9(07)    COMP-3.                       
              05  W-FPV050-PMTCAPSE PIC S9(09)V99 COMP-3.                       
       01  W-MOIS.                                                              
           05  W-MOIS-MM      PIC X(02).                                        
           05  FILLER         PIC X(01)  VALUE '/'.                             
           05  W-MOIS-SS      PIC X(02).                                        
           05  W-MOIS-AA      PIC X(02).                                        
       01  W-DATE-SSAAMMJJ.                                                     
           05  W-DATE-SS      PIC X(02).                                        
           05  W-DATE-AA      PIC X(02).                                        
           05  W-DATE-MM      PIC X(02).                                        
           03  W-DATE-JJ      PIC X(02).                                        
       01  W-DATE-ED.                                                           
           05  W-DATE-JJ-ED   PIC X(02).                                        
           05  FILLER         PIC X(01) VALUE '/'.                              
           05  W-DATE-MM-ED   PIC X(02).                                        
           05  FILLER         PIC X(01) VALUE '/'.                              
           05  W-DATE-SS-ED   PIC X(02).                                        
           03  W-DATE-AA-ED   PIC X(02).                                        
       01  CODE-FPV050 PIC X(01) VALUE '0'.                                     
           88 NON-FIN-FPV050     VALUE '0'.                                     
           88 FIN-FPV050         VALUE '1'.                                     
       01  CODE-AUTRE  PIC X(01) VALUE '0'.                                     
           88 NON-EDIT-AUTRE     VALUE '0'.                                     
           88 EDIT-AUTRE         VALUE '1'.                                     
      *---------------------------------------------------------------          
      *  DSECT DE L'EDITION                                                     
      *---------------------------------------------------------------          
       01  IPV050-DSECT.                                                        
           05  E00.                                                             
               10  E00-ASA PIC X(1) VALUE '1'.                                  
               10  FILLER                         PIC X(58) VALUE               
           'IPV050                                           VENTES  P'.        
               10  FILLER                         PIC X(47) VALUE               
           'AR  RUBRIQUES  PRODUITS               EDITE LE '.                   
               10  E00-DATEDITE                   PIC 99/99/9999.               
               10  FILLER                         PIC X(08).                    
               10  FILLER                         PIC X(06) VALUE               
           ' PAGE '.                                                            
               10  E00-NOPAGE                     PIC ZZZ.                      
           05  E01.                                                             
               10  E01-ASA PIC X(1) VALUE ' '.                                  
               10  FILLER                         PIC X(13) VALUE               
           'MOIS       : '.                                                     
               10  E01-MOIS                       PIC X(07).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(56) VALUE               
           '                                                        '.          
           05  E02.                                                             
               10  E02-ASA PIC X(1) VALUE ' '.                                  
               10  FILLER                         PIC X(13) VALUE               
           'MAGASIN    : '.                                                     
               10  E02-NMAG                       PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  E02-LMAG                       PIC X(20).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(37) VALUE               
           '                                     '.                             
           05  E03.                                                             
               10  E03-ASA PIC X(1) VALUE '-'.                                  
               10  FILLER                         PIC X(58) VALUE               
           '.---------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(16) VALUE               
           '---------------.'.                                                  
           05  E04.                                                             
               10  E04-ASA PIC X(1) VALUE ' '.                                  
               10  FILLER                         PIC X(21) VALUE               
           '! RUBRIQUE PRODUIT : '.                                             
               10  E04-LAGREGATED                 PIC X(20).                    
               10  E04-SUITE                      PIC X(07).                    
               10  FILLER                         PIC X(36) VALUE               
           '                                    '.                              
               10  FILLER                         PIC X(15) VALUE               
           '               '.                                                   
               10  FILLER                         PIC X(33) VALUE               
           '                                !'.                                 
           05  E05.                                                             
               10  E05-ASA PIC X(1) VALUE ' '.                                  
               10  FILLER                         PIC X(58) VALUE               
           '!---------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(16) VALUE               
           '---------------!'.                                                  
           05  E06.                                                             
               10  E06-ASA PIC X(1) VALUE ' '.                                  
               10  FILLER                         PIC X(58) VALUE               
           '!                            !             CUMUL  DU  MOIS'.        
               10  FILLER                         PIC X(58) VALUE               
           '                      !                   CUMUL  EXERCICE '.        
               10  FILLER                         PIC X(16) VALUE               
           '               !'.                                                  
           05  E07.                                                             
               10  E07-ASA PIC X(1) VALUE ' '.                                  
               10  FILLER                         PIC X(58) VALUE               
           '!                            !----------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------!-----------------------------------'.        
               10  FILLER                         PIC X(16) VALUE               
           '---------------!'.                                                  
           05  E08.                                                             
               10  E08-ASA PIC X(1) VALUE ' '.                                  
               10  FILLER                         PIC X(58) VALUE               
           '!                            !             PRODUITS       '.        
               10  FILLER                         PIC X(58) VALUE               
           '        !      PSE    !            PRODUITS               '.        
               10  FILLER                         PIC X(16) VALUE               
           ' !     PSE     !'.                                                  
           05  E09.                                                             
               10  E09-ASA PIC X(1) VALUE ' '.                                  
               10  FILLER                         PIC X(58) VALUE               
           '!----------------------------!----------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '--------!-------------!-----------------------------------'.        
               10  FILLER                         PIC X(16) VALUE               
           '-!-------------!'.                                                  
           05  E10.                                                             
               10  E10-ASA PIC X(1) VALUE ' '.                                  
               10  FILLER                         PIC X(58) VALUE               
           '! VENDEUR                    !   NB      C.A.    REM  PVM '.        
               10  FILLER                         PIC X(58) VALUE               
           '  %COM  !   NB     %  !    NB      C.A.   REM   PVM  %COM '.        
               10  FILLER                         PIC X(16) VALUE               
           ' ! NB      %   !'.                                                  
           05  E11.                                                             
               10  E11-ASA PIC X(1) VALUE ' '.                                  
               10  FILLER                         PIC X(58) VALUE               
           '!                            ! TOTAL    TOTAL     %       '.        
               10  FILLER                         PIC X(58) VALUE               
           '  TOTAL !  PSE    PSE !   TOTAL    TOTAL   %         TOTAL'.        
               10  FILLER                         PIC X(16) VALUE               
           ' ! PSE     PSE !'.                                                  
           05  D05.                                                             
               10  D05-ASA PIC X(1) VALUE ' '.                                  
               10  FILLER                         PIC X(02) VALUE               
           '! '.                                                                
               10  D05-CVENDEUR                   PIC X(06).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  D05-LVENDEUR                   PIC X(20).                    
               10  FILLER                         PIC X(01) VALUE               
           '!'.                                                                 
               10  D05-NB                         PIC -(7).                     
               10  D05-CA                         PIC -(10).                    
               10  D05-REM                        PIC --9,99                    
                                                  BLANK WHEN ZERO.              
               10  D05-PVM                        PIC ------.                   
               10  D05-COM-PCT                    PIC -9,999                    
                                                  BLANK WHEN ZERO.              
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  D05-NB-PSE                     PIC ------.                   
               10  D05-PSE-PCT                    PIC ---9,9                    
                                                  BLANK WHEN ZERO.              
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  D05-NB-TOT                     PIC -(7).                     
               10  D05-CA-TOT                     PIC -(10).                    
               10  D05-REM-TOT                    PIC --9,99                    
                                                  BLANK WHEN ZERO.              
               10  D05-PVM-TOT                    PIC ------.                   
               10  D05-COM-TOT-PCT                PIC -9,999                    
                                                  BLANK WHEN ZERO.              
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  D05-NB-PSE-TOT                 PIC ------.                   
               10  D05-PSE-TOT-PCT                PIC ---9,9                    
                                                  BLANK WHEN ZERO.              
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
           05  T02.                                                             
               10  T02-ASA PIC X(1) VALUE ' '.                                  
               10  FILLER                         PIC X(30) VALUE               
           '!        TOTAL RUBRIQUE      !'.                                    
               10  T02-NB                         PIC -------.                  
               10  T02-CA                         PIC ----------.               
               10  T02-REM                        PIC --9,99                    
                                                  BLANK WHEN ZERO.              
               10  T02-PVM                        PIC ------.                   
               10  T02-COM-PCT                    PIC -9,999                    
                                                  BLANK WHEN ZERO.              
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  T02-NB-PSE                     PIC ------.                   
               10  T02-PSE-PCT                    PIC ---9,9                    
                                                  BLANK WHEN ZERO.              
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  T02-NB-TOT                     PIC -------.                  
               10  T02-CA-TOT                     PIC ----------.               
               10  T02-REM-TOT                    PIC --9,99                    
                                                  BLANK WHEN ZERO.              
               10  T02-PVM-TOT                    PIC ------.                   
               10  T02-COM-TOT-PCT                PIC -9,999                    
                                                  BLANK WHEN ZERO.              
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  T02-NB-PSE-TOT                 PIC ------.                   
               10  T02-PSE-TOT-PCT                PIC ---9,9                    
                                                  BLANK WHEN ZERO.              
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
           05  T03.                                                             
               10  T03-ASA PIC X(1) VALUE ' '.                                  
               10  FILLER                         PIC X(58) VALUE               
           '!---------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(16) VALUE               
           '---------------!'.                                                  
      *================================================================*        
      *       DESCRIPTION DES ZONES D'APPEL AU MODULE ABEND            *        
      *================================================================*        
            COPY  ABENDCOP.                                                     
      *================================================================*        
      *       DESCRIPTION DES ZONES D'APPEL AU MODULE BETDATC          *        
      *================================================================*        
            COPY  WORKDATC.                                                     
      *================================================================*        
       PROCEDURE               DIVISION.                                        
      *================================================================*        
      *===============================================================*         
      *              T R A M E   DU   P R O G R A M M E               *         
      *===============================================================*         
      *                                                                         
      *===============================================================*         
      *                                                               *         
      *           -----------------------------------------           *         
      *           -           MODULE    BPV051            -           *         
      *           -----------------------------------------           *         
      *                               I                               *         
      *           -----------------------------------------           *         
      *           I                   I                   I           *         
      *   -----------------   -----------------   -----------------   *         
      *   -    ENTREE     -   -  TRAITEMENT   -   -     SORTIE    -   *         
      *   -----------------   -----------------   -----------------   *         
      *                                                               *         
      *===============================================================*         
      *                                                                         
      *===============================================================*         
       MODULE-BPV051           SECTION.                                         
      *===============================================================*         
      *                                                                         
           PERFORM ENTREE.                                                      
           PERFORM TRAITEMENT UNTIL FIN-FPV050.                                 
           PERFORM SORTIE.                                                      
      *                                                                         
       FIN-MODULE-BPV051.  EXIT.                                                
        EJECT                                                                   
      *                                                                         
      *===============================================================*         
       ENTREE                     SECTION.                                      
      *===============================================================*         
      *                                                                         
           OPEN INPUT  FMOIS.                                                   
           OPEN INPUT  FDATE.                                                   
           OPEN INPUT  FNSOC.                                                   
           OPEN INPUT  FPV050.                                                  
           OPEN OUTPUT IPV050.                                                  
           READ FMOIS AT END                                                    
             MOVE 'ABSENCE DE CARTE PARAMETRE FMOIS' TO ABEND-MESS              
             PERFORM PLANTAGE                                                   
           END-READ.                                                            
           MOVE FMOIS-MM TO W-MOIS-MM.                                          
           MOVE FMOIS-SS TO W-MOIS-SS.                                          
           MOVE FMOIS-AA TO W-MOIS-AA.                                          
           READ FDATE AT END                                                    
             MOVE 'ABSENCE DE CARTE PARAMETRE FDATE' TO ABEND-MESS              
             PERFORM PLANTAGE                                                   
           END-READ.                                                            
           MOVE FDATE-JJ        TO W-DATE-JJ W-DATE-JJ-ED.                      
           MOVE FDATE-MM        TO W-DATE-MM W-DATE-MM-ED.                      
           MOVE FDATE-SS        TO W-DATE-SS W-DATE-SS-ED.                      
           MOVE FDATE-AA        TO W-DATE-AA W-DATE-AA-ED.                      
           INITIALIZE WORK-BETDATC.                                             
           MOVE '5'             TO GFDATA.                                      
           MOVE W-DATE-SSAAMMJJ TO GFSAMJ-0.                                    
           PERFORM APPEL-BETDATC.                                               
           READ FNSOC AT END                                                    
             MOVE 'ABSENCE DE CARTE PARAMETRE' TO ABEND-MESS                    
             PERFORM PLANTAGE                                                   
           END-READ.                                                            
           MOVE FNSOCIETE  TO W-FNSOC.                                          
           READ FPV050 AT END                                                   
             DISPLAY 'FICHIER FPV050 VIDE '                                     
             PERFORM FERMETURE-FICHIERS                                         
             PERFORM FIN-PROGRAMME                                              
           END-READ.                                                            
           ADD 1 TO W-NB-FPV050.                                                
           INITIALIZE W-FPV050-DSECT.                                           
      *                                                                         
       FIN-ENTREE. EXIT.                                                        
        EJECT                                                                   
      *                                                                         
      *===============================================================*         
       APPEL-BETDATC              SECTION.                                      
      *===============================================================*         
      *                                                                         
           CALL BETDATC USING WORK-BETDATC.                                     
           IF GFVDAT = ZERO                                                     
              MOVE GF-MESS-ERR TO ABEND-MESS                                    
              PERFORM PLANTAGE                                                  
           END-IF.                                                              
      *                                                                         
       FIN-APPEL-BETDATC. EXIT.                                                 
                    EJECT                                                       
      *                                                                         
      *===============================================================*         
       TRAITEMENT                 SECTION.                                      
      *===============================================================*         
      *                                                                         
      *-- SI 1ERE PAGE, OU CHGT MAGASIN OU PLUS DE PLACE SUR PAGE               
      *-- ALORS EDITION DE L'ENTETE                                             
           IF (CPT-LIGNE = 0 AND CPT-PAGE = 0)                                  
              OR (CPT-LIGNE > W-NLIGNE-MAX - 3                                  
                  AND W-LAGREGATED = FPV050-LAGREGATED)                         
              OR (CPT-LIGNE > W-NLIGNE-MAX - 14                                 
                  AND W-LAGREGATED NOT = FPV050-LAGREGATED)                     
              OR W-NMAG NOT = FPV050-NMAG                                       
              PERFORM TRAIT-ENTETE                                              
           END-IF.                                                              
      *                                                                         
      *-- SI CHGT D'AGREGAT, SI NB-LIGNE <= NLIGNE-MAX - 14                     
      *-- ALORS ENTETE-REDUITE                                                  
           IF W-LAGREGATED NOT = FPV050-LAGREGATED                              
              AND (CPT-LIGNE NOT > W-NLIGNE-MAX - 14)                           
              PERFORM ENTETE-REDUITE                                            
           END-IF.                                                              
      *-- SI VENDEUR A VENDU DANS LE MOIS ALORS EDITER LIGNE,                   
      *-- SINON CUMULER DANS 'AUTRES VENDEURS'.                                 
           INITIALIZE D05.                                                      
           IF FPV050-QVENDUE(1) NOT = 0                                         
              IF W-LAGREGATED NOT = FPV050-LAGREGATED                           
                 PERFORM ENTETE-REDUITE                                         
              END-IF                                                            
              MOVE FPV050-CVENDEUR   TO D05-CVENDEUR                            
              MOVE FPV050-LVENDEUR   TO D05-LVENDEUR                            
              MOVE FPV050-QVENDUE(1) TO D05-NB                                  
              MOVE FPV050-PMTCA(1)   TO D05-CA                                  
              COMPUTE W-MTREM = FPV050-MTREM(1) + FPV050-PMTCA(1)               
              IF W-MTREM NOT = 0                                                
                 COMPUTE W-MTREM ROUNDED =                                      
                                   (FPV050-MTREM(1) * 100) / W-MTREM            
                 MOVE W-MTREM           TO D05-REM                              
              END-IF                                                            
              IF FPV050-QVENDUE(1) NOT = 0                                      
                 COMPUTE W-PMTCA ROUNDED = FPV050-PMTCA(1)                      
                                         / FPV050-QVENDUE(1)                    
                 MOVE W-PMTCA            TO D05-PVM                             
              ELSE                                                              
                 MOVE 0 TO D05-PVM                                              
              END-IF                                                            
              IF FPV050-PMTCA(1) NOT = 0                                        
                 COMPUTE W-MTCOMM ROUNDED = FPV050-MTCOMM(1) * 100              
                                          / FPV050-PMTCA(1)                     
                 MOVE W-MTCOMM     TO D05-COM-PCT                               
              ELSE                                                              
                 MOVE 0 TO D05-COM-PCT                                          
              END-IF                                                            
              MOVE FPV050-QNBPSE(1) TO D05-NB-PSE                               
              IF FPV050-QNBPSAB(1) NOT = 0                                      
                 COMPUTE W-QNBPSE ROUNDED  = FPV050-QNBPSE(1) * 100             
                                           / FPV050-QNBPSAB(1)                  
                 MOVE W-QNBPSE       TO D05-PSE-PCT                             
              ELSE                                                              
                 MOVE 0 TO D05-PSE-PCT                                          
              END-IF                                                            
              MOVE FPV050-QVENDUE(2) TO D05-NB-TOT                              
              MOVE FPV050-PMTCA(2)   TO D05-CA-TOT                              
              COMPUTE W-MTREM = FPV050-MTREM(2) + FPV050-PMTCA(2)               
              IF W-MTREM NOT = 0                                                
                 COMPUTE W-MTREM ROUNDED =                                      
                                   (FPV050-MTREM(2) * 100) / W-MTREM            
                 MOVE W-MTREM           TO D05-REM-TOT                          
              END-IF                                                            
              IF FPV050-QVENDUE(2) NOT = 0                                      
                 COMPUTE W-PMTCA ROUNDED =                                      
                                 FPV050-PMTCA(2) / FPV050-QVENDUE(2)            
                 MOVE W-PMTCA           TO D05-PVM-TOT                          
              ELSE                                                              
                 MOVE 0 TO D05-PVM-TOT                                          
              END-IF                                                            
              IF FPV050-PMTCA(2) NOT = 0                                        
                 COMPUTE W-MTCOMM ROUNDED  = FPV050-MTCOMM(2) * 100             
                                             / FPV050-PMTCA(2)                  
                 MOVE W-MTCOMM     TO D05-COM-TOT-PCT                           
              ELSE                                                              
                 MOVE 0 TO D05-COM-TOT-PCT                                      
              END-IF                                                            
              MOVE FPV050-QNBPSE(2) TO D05-NB-PSE-TOT                           
              IF FPV050-QNBPSAB(2) NOT = 0                                      
                 COMPUTE W-QNBPSE ROUNDED  = FPV050-QNBPSE(2) * 100             
                                            / FPV050-QNBPSAB(2)                 
                 MOVE W-QNBPSE      TO D05-PSE-TOT-PCT                          
              ELSE                                                              
                 MOVE 0 TO D05-PSE-TOT-PCT                                      
              END-IF                                                            
              MOVE D05  TO IPV050-ENREG                                         
              WRITE IPV050-ENREG                                                
              ADD 1 TO CPT-LIGNE CPT-DETAIL                                     
           ELSE                                                                 
      *-- ON RENSEIGNE LA RUBRIQUE 'AUTRES VENDEURS'                            
              IF FPV050-QVENDUE(2) NOT = 0                                      
                 SET  EDIT-AUTRE TO TRUE                                        
                 ADD FPV050-QVENDUE (2) TO W-FPV050-QVENDUE (1)                 
                 ADD FPV050-PMTCA   (2) TO W-FPV050-PMTCA   (1)                 
                 ADD FPV050-MTREM   (2) TO W-FPV050-MTREM   (1)                 
                 ADD FPV050-MTCOMM  (2) TO W-FPV050-MTCOMM  (1)                 
                 ADD FPV050-QNBPSE  (2) TO W-FPV050-QNBPSE  (1)                 
                 ADD FPV050-QNBPSAB (2) TO W-FPV050-QNBPSAB (1)                 
                 ADD FPV050-PMTCAPSE(2) TO W-FPV050-PMTCAPSE(1)                 
              END-IF                                                            
           END-IF.                                                              
      *-- ON CUMULE AFIN DE RENSEIGNER LA LIGNE TOTALE                          
           ADD FPV050-QVENDUE(1)     TO W-FPV050-QVENDUE (2).                   
           ADD FPV050-PMTCA  (1)     TO W-FPV050-PMTCA   (2).                   
           ADD FPV050-MTREM  (1)     TO W-FPV050-MTREM   (2).                   
           ADD FPV050-MTCOMM (1)     TO W-FPV050-MTCOMM  (2).                   
           ADD FPV050-QNBPSE (1)     TO W-FPV050-QNBPSE  (2).                   
           ADD FPV050-QNBPSAB(1)     TO W-FPV050-QNBPSAB (2).                   
           ADD FPV050-PMTCAPSE(1)    TO W-FPV050-PMTCAPSE(2).                   
           ADD FPV050-QVENDUE(2)     TO W-FPV050-QVENDUE (3).                   
           ADD FPV050-PMTCA  (2)     TO W-FPV050-PMTCA   (3).                   
           ADD FPV050-MTREM  (2)     TO W-FPV050-MTREM   (3).                   
           ADD FPV050-MTCOMM (2)     TO W-FPV050-MTCOMM  (3).                   
           ADD FPV050-QNBPSE (2)     TO W-FPV050-QNBPSE  (3).                   
           ADD FPV050-QNBPSAB(2)     TO W-FPV050-QNBPSAB (3).                   
           ADD FPV050-PMTCAPSE(2)    TO W-FPV050-PMTCAPSE(3).                   
           PERFORM LECTURE-FPV050.                                              
      *                                                                         
       FIN-TRAITEMENT. EXIT.                                                    
                    EJECT                                                       
      *                                                                         
      *===============================================================*         
       TRAIT-ENTETE            SECTION.                                         
      *===============================================================*         
      *                                                                         
           IF (W-NMAG NOT = FPV050-NMAG                                         
              OR W-LAGREGATED NOT = FPV050-LAGREGATED)                          
              AND EDIT-AUTRE                                                    
              AND CPT-DETAIL > 0                                                
              PERFORM EDITION-AUTRE                                             
           END-IF.                                                              
           IF CPT-PAGE > 0                                                      
              IF FPV050-LAGREGATED NOT = W-LAGREGATED                           
                 PERFORM EDITION-TOTAL                                          
              ELSE                                                              
                 MOVE T03         TO IPV050-ENREG                               
                 WRITE IPV050-ENREG                                             
              END-IF                                                            
           END-IF.                                                              
           ADD  1                 TO CPT-PAGE.                                  
           MOVE W-DATE-ED         TO E00-DATEDITE.                              
           MOVE CPT-PAGE          TO E00-NOPAGE.                                
           MOVE E00               TO IPV050-ENREG.                              
           WRITE IPV050-ENREG.                                                  
           MOVE W-MOIS            TO E01-MOIS.                                  
           MOVE E01               TO IPV050-ENREG.                              
           WRITE IPV050-ENREG.                                                  
           MOVE FPV050-NMAG       TO E02-NMAG.                                  
           MOVE FPV050-LMAG       TO E02-LMAG.                                  
           MOVE E02               TO IPV050-ENREG.                              
           WRITE IPV050-ENREG.                                                  
           MOVE 3                 TO CPT-LIGNE.                                 
           PERFORM ENTETE-REDUITE.                                              
      *                                                                         
       FIN-TRAIT-ENTETE. EXIT.                                                  
        EJECT                                                                   
      *                                                                         
      *===============================================================*         
       ENTETE-REDUITE          SECTION.                                         
      *===============================================================*         
      *                                                                         
      *-- CAS OU L'ON CHANGE D'AGREGAT ET QU'IL EXISTE DES VENTES               
      *-- D'AUTRES VENDEURS SUR LES MOIS PRECEDENTS                             
           IF W-LAGREGATED NOT = FPV050-LAGREGATED                              
              AND EDIT-AUTRE                                                    
              AND CPT-DETAIL > 0                                                
              PERFORM EDITION-AUTRE                                             
              MOVE T03               TO IPV050-ENREG                            
              WRITE IPV050-ENREG                                                
           END-IF.                                                              
      *-- CAS OU L'ON IMPRIME LA LIGNE TOTAL VENDEUR                            
           IF CPT-PAGE > 0 AND CPT-LIGNE > 3                                    
              AND FPV050-LAGREGATED NOT = W-LAGREGATED                          
              PERFORM EDITION-TOTAL                                             
           END-IF.                                                              
      *-- CAS OU UNE RUBRIQUE NE PEUT S'EDITER EN INTEGRALITE SUR 1 PAGE        
           IF CPT-PAGE > 0 AND CPT-LIGNE = 3                                    
              AND FPV050-LAGREGATED = W-LAGREGATED                              
              MOVE '(SUITE)'      TO E04-SUITE                                  
           ELSE                                                                 
              MOVE SPACES         TO E04-SUITE                                  
           END-IF.                                                              
           MOVE FPV050-NMAG       TO W-NMAG                                     
           MOVE FPV050-LAGREGATED TO W-LAGREGATED                               
           MOVE E03               TO IPV050-ENREG.                              
           WRITE IPV050-ENREG.                                                  
           MOVE FPV050-LAGREGATED TO E04-LAGREGATED.                            
           MOVE E04               TO IPV050-ENREG.                              
           WRITE IPV050-ENREG.                                                  
           MOVE E05               TO IPV050-ENREG.                              
           WRITE IPV050-ENREG.                                                  
           MOVE E06               TO IPV050-ENREG.                              
           WRITE IPV050-ENREG.                                                  
           MOVE E07               TO IPV050-ENREG.                              
           WRITE IPV050-ENREG.                                                  
           MOVE E08               TO IPV050-ENREG.                              
           WRITE IPV050-ENREG.                                                  
           MOVE E09               TO IPV050-ENREG.                              
           WRITE IPV050-ENREG.                                                  
           MOVE E10               TO IPV050-ENREG.                              
           WRITE IPV050-ENREG.                                                  
           MOVE E11               TO IPV050-ENREG.                              
           WRITE IPV050-ENREG.                                                  
           MOVE E09               TO IPV050-ENREG.                              
           WRITE IPV050-ENREG.                                                  
           ADD 12                 TO CPT-LIGNE.                                 
      *                                                                         
       FIN-ENTETE-REDUITE. EXIT.                                                
        EJECT                                                                   
      *                                                                         
      *===============================================================*         
       EDITION-AUTRE           SECTION.                                         
      *===============================================================*         
           INITIALIZE D05.                                                      
           MOVE 'AUTRES VENDEURS'     TO D05-LVENDEUR.                          
           MOVE W-FPV050-QVENDUE(1)   TO D05-NB-TOT.                            
           MOVE W-FPV050-PMTCA(1)     TO D05-CA-TOT.                            
           COMPUTE W-MTREM = W-FPV050-MTREM(1) + W-FPV050-PMTCA(1)              
           IF W-MTREM NOT = 0                                                   
              COMPUTE W-FPV050-MTREM(1) ROUNDED =                               
                               (W-FPV050-MTREM(1) * 100) / W-MTREM              
              MOVE W-FPV050-MTREM(1)     TO D05-REM-TOT                         
           END-IF.                                                              
           IF W-FPV050-QVENDUE(1) NOT = 0                                       
              COMPUTE W-PMTCA ROUNDED = W-FPV050-PMTCA(1)                       
                                      / W-FPV050-QVENDUE(1)                     
              MOVE W-PMTCA            TO D05-PVM-TOT                            
           END-IF.                                                              
           IF W-FPV050-PMTCA(1) NOT = 0                                         
              COMPUTE W-MTCOMM ROUNDED = W-FPV050-MTCOMM(1)                     
                                         * 100 / W-FPV050-PMTCA(1)              
              MOVE W-MTCOMM           TO D05-COM-TOT-PCT                        
           END-IF.                                                              
           MOVE W-FPV050-QNBPSE(1)    TO D05-NB-PSE-TOT.                        
           IF W-FPV050-QNBPSAB(1) > 0                                           
              COMPUTE W-QNBPSE ROUNDED = W-FPV050-QNBPSE(1) * 100               
                                         / W-FPV050-QNBPSAB(1)                  
              MOVE W-QNBPSE           TO D05-PSE-TOT-PCT                        
           END-IF.                                                              
           MOVE D05                   TO IPV050-ENREG.                          
           WRITE IPV050-ENREG.                                                  
           ADD 1                      TO CPT-LIGNE.                             
      *                                                                         
       FIN-EDITION-AUTRE. EXIT.                                                 
        EJECT                                                                   
      *                                                                         
      *===============================================================*         
       EDITION-TOTAL           SECTION.                                         
      *===============================================================*         
           INITIALIZE T02.                                                      
           MOVE W-FPV050-QVENDUE(2)   TO T02-NB.                                
           MOVE W-FPV050-PMTCA(2)     TO T02-CA.                                
           COMPUTE W-MTREM = W-FPV050-MTREM(2) + W-FPV050-PMTCA(2)              
           IF W-MTREM NOT = 0                                                   
              COMPUTE W-FPV050-MTREM(2) ROUNDED =                               
                               (W-FPV050-MTREM(2) * 100) / W-MTREM              
              MOVE W-FPV050-MTREM(2)     TO T02-REM                             
           END-IF.                                                              
           IF W-FPV050-QVENDUE(2) NOT = 0                                       
              COMPUTE W-PMTCA ROUNDED = W-FPV050-PMTCA(2)                       
                                      / W-FPV050-QVENDUE(2)                     
              MOVE W-PMTCA            TO T02-PVM                                
           END-IF.                                                              
           IF W-FPV050-PMTCA(2) NOT = 0                                         
              COMPUTE W-MTCOMM ROUNDED = W-FPV050-MTCOMM(2)                     
                                       * 100 / W-FPV050-PMTCA(2)                
              MOVE W-MTCOMM           TO T02-COM-PCT                            
           END-IF.                                                              
           MOVE W-FPV050-QNBPSE(2)    TO T02-NB-PSE.                            
           IF W-FPV050-QNBPSAB(2) NOT = 0                                       
              COMPUTE W-QNBPSE ROUNDED = W-FPV050-QNBPSE(2) * 100               
                                         / W-FPV050-QNBPSAB(2)                  
              MOVE W-QNBPSE           TO T02-PSE-PCT                            
           END-IF.                                                              
           MOVE W-FPV050-QVENDUE(3)   TO T02-NB-TOT.                            
           MOVE W-FPV050-PMTCA(3)     TO T02-CA-TOT.                            
           COMPUTE W-MTREM = W-FPV050-MTREM(3) + W-FPV050-PMTCA(3)              
           IF W-MTREM NOT = 0                                                   
              COMPUTE W-MTREM ROUNDED =                                         
                               (W-FPV050-MTREM(3) * 100) / W-MTREM              
              MOVE W-MTREM            TO T02-REM-TOT                            
           END-IF.                                                              
           IF W-FPV050-QVENDUE(3) NOT = 0                                       
              COMPUTE W-PMTCA ROUNDED = W-FPV050-PMTCA(3)                       
                                      / W-FPV050-QVENDUE(3)                     
              MOVE W-PMTCA            TO T02-PVM-TOT                            
           END-IF                                                               
           IF W-FPV050-PMTCA(3) NOT = 0                                         
              COMPUTE W-MTCOMM ROUNDED = W-FPV050-MTCOMM(3)                     
                                 * 100  / W-FPV050-PMTCA(3)                     
              MOVE W-MTCOMM           TO T02-COM-TOT-PCT                        
           END-IF.                                                              
           MOVE W-FPV050-QNBPSE(3)  TO T02-NB-PSE-TOT.                          
           IF W-FPV050-QNBPSAB(3) NOT = 0                                       
              COMPUTE W-QNBPSE ROUNDED = W-FPV050-QNBPSE(3) * 100               
                                         / W-FPV050-QNBPSAB(3)                  
              MOVE W-QNBPSE           TO T02-PSE-TOT-PCT                        
           END-IF.                                                              
           MOVE T02                   TO IPV050-ENREG.                          
           WRITE IPV050-ENREG.                                                  
           MOVE T03                   TO IPV050-ENREG.                          
           WRITE IPV050-ENREG.                                                  
           ADD 2                      TO CPT-LIGNE.                             
           INITIALIZE W-FPV050-DSECT.                                           
           MOVE 0                     TO CPT-DETAIL.                            
           MOVE '0'                   TO CODE-AUTRE.                            
      *                                                                         
       FIN-EDITION-TOTAL. EXIT.                                                 
        EJECT                                                                   
      *                                                                         
      *===============================================================*         
       SORTIE                  SECTION.                                         
      *===============================================================*         
      *                                                                         
           IF CPT-PAGE > 0 AND EDIT-AUTRE AND CPT-DETAIL > 0                    
              PERFORM EDITION-AUTRE                                             
           END-IF.                                                              
           PERFORM EDITION-TOTAL.                                               
           DISPLAY '*********** BPV051/EXTRACTION ******************'.          
           DISPLAY '** NB LECTURES FPV050   :' W-NB-FPV050.                     
           DISPLAY '** NB PAGES EDITEES     :' CPT-PAGE.                        
           DISPLAY '** NB LIGNES SUR DERN. PAGE :' CPT-LIGNE.                   
           DISPLAY '************************************************'.          
           PERFORM FERMETURE-FICHIERS.                                          
           PERFORM FIN-PROGRAMME.                                               
      *                                                                         
       FIN-SORTIE. EXIT.                                                        
        EJECT                                                                   
      *                                                                         
      *===============================================================*         
       LECTURE-FPV050             SECTION.                                      
      *===============================================================*         
      *                                                                         
           READ FPV050 AT END                                                   
             SET FIN-FPV050 TO TRUE                                             
           END-READ.                                                            
           ADD 1 TO W-NB-FPV050.                                                
      *                                                                         
       FIN-LECTURE-FPV050. EXIT.                                                
                    EJECT                                                       
      *                                                                         
      *================================================================*        
       FERMETURE-FICHIERS       SECTION.                                        
      *================================================================*        
      *                                                                         
           CLOSE FMOIS.                                                         
           CLOSE FDATE.                                                         
           CLOSE FNSOC.                                                         
           CLOSE FPV050.                                                        
           CLOSE IPV050.                                                        
      *                                                                         
       F-FERMETURE-FICHIERS. EXIT.                                              
        EJECT                                                                   
      *                                                                         
      *================================================================*        
       FIN-PROGRAMME          SECTION.                                          
      *================================================================*        
      *                                                                         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP  RUN.                                                           
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-FIN-PROGRAMME. EXIT.                                                 
        EJECT                                                                   
      *                                                                         
      *================================================================*        
       PLANTAGE                  SECTION.                                       
      *================================================================*        
      *                                                                         
           PERFORM  FIN-ANORMALE.                                               
           MOVE  'BPV051' TO ABEND-PROG.                                        
           CALL  'ABEND' USING ABEND-PROG  ABEND-MESS.                          
      *                                                                         
       FIN-PLANTAGE. EXIT.                                                      
        EJECT                                                                   
      *                                                                         
      *================================================================*        
       FIN-ANORMALE              SECTION.                                       
      *================================================================*        
      *                                                                         
           DISPLAY  '**********************************************'.           
           DISPLAY  '***      INTERRUPTION DU PROGRAMME         ***'.           
           DISPLAY  '***              BPV051                    ***'.           
           DISPLAY  '**********************************************'.           
           PERFORM  FERMETURE-FICHIERS.                                         
      *                                                                         
       FIN-FIN-ANORMALE. EXIT.                                                  
        EJECT                                                                   
