      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 20/09/2016 1        
      *{     Tr-Collating-Sequence-EBCDIC 1.0                                   
      * OBJECT-COMPUTER.                                                        
      *    PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
      *--                                                                       
       IDENTIFICATION DIVISION.                                         00000200
       PROGRAM-ID.                     MVT49.                           00000300
      *                                                                 00000400
      ***************************************************************** 00000500
      *  F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E  * 00000600
      ***************************************************************** 00000700
      *                                                               * 00000800
      *  PROJET     : APPLICATION  GESTION DES HISTORIQUES VENTES     * 00000900
      *  PROGRAMME  : MVT49                                           * 00001000
      *  CREATION   : JUIN 1997.                                      * 00001100
      *  AUTEUR     : JEAN-CLAUDE FELICIANO.                          * 00001200
      *                                                               * 00001300
      *  FONCTION   : MODULE DE RECHERCHE DE LA TRANSACTION � AFFICHER* 00001400
      *             :                                                 * 00001500
      *             :                                                 * 00001600
      *             : ENTR�E � PARTIR DU THV42 POUR LES VENTES        * 00001700
      *                                  THV46, THV47, THV48, THV49   * 00001800
      *                                  POUR LES TRANSACTIONS.       * 00001900
      *                                                               * 00002000
      ***************************************************************** 00002100
      *  FICHIER ACC�D�S:                                             * 00002200
      *                                                               * 00002300
      *                                                               * 00002400
      ***************************************************************** 00002500
      *     M O D I F I C A T I O N S  /  M A I N T E N A N C E S     * 00002600
      ***************************************************************** 00002700
      *  DATE       : ...               - AUTEUR : ...                * 00002800
      *  OBJET      : ...                                             * 00002900
      *---------------------------------------------------------------* 00003000
      *  DATE       : ...               - AUTEUR : ...                * 00003100
      *  OBJET      : ...                                             * 00003200
      ***************************************************************** 00003300
       ENVIRONMENT DIVISION.                                            00003400
       CONFIGURATION SECTION.                                           00003500
      *{ Tr-Collating-Sequence-EBCDIC 1.0                                       
      *SPECIAL-NAMES.                                                   00003600
      *    DECIMAL-POINT IS COMMA.                                      00003700
      *--                                                                       
        OBJECT-COMPUTER.                                                        
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
       DATA DIVISION.                                                   00003800
       WORKING-STORAGE SECTION.                                         00003900
                  COPY  SYKWDIV0.                                       00004000
       EJECT                                                            00004100
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00004200
      * ZONES DE WORKING SPECIFIQUES AU PROGRAMME                     * 00004300
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00004400
      *                                                                 00004500
       01  FILLER     PIC X(16) VALUE 'WORKING-STORAGE:'.               00004600
      *                                                                 00004700
      *---------------------------------------------------------------* 00004800
      * NOMBRES LIMITES DES TABLES EN COMMAR�A                        * 00004900
      *---------------------------------------------------------------* 00005000
      *                                                                 00005100
      *                                                                 00005200
      *---------------------------------------------------------------* 00005300
      * COMPTEURS DIVERS.                                             * 00005400
      *---------------------------------------------------------------* 00005500
      *                                                                 00005600
      *                                                                 00005700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
B&S   *77  WS-A                             PIC S9(9) COMP   VALUE ZERO.00005800
      *--                                                                       
       77  WS-A                             PIC S9(9) COMP-5   VALUE            
                                                                   ZERO.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  WS-I                             PIC S9(9) COMP   VALUE ZERO.00005900
      *--                                                                       
       77  WS-I                             PIC S9(9) COMP-5   VALUE            
                                                                   ZERO.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  WS-T                             PIC S9(9) COMP   VALUE ZERO.00006000
      *--                                                                       
       77  WS-T                             PIC S9(9) COMP-5   VALUE            
                                                                   ZERO.        
      *}                                                                        
B&S    77  WS-T-12                          PIC  9(9)        VALUE ZERO.00006100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
B&S   *77  WS-COMPT                         PIC S9(9) COMP   VALUE ZERO.00006200
      *--                                                                       
       77  WS-COMPT                         PIC S9(9) COMP-5   VALUE            
                                                                   ZERO.        
      *}                                                                        
       77  WS-NOITEM                        PIC S9(4) COMP-3 VALUE ZERO.00006300
      *                                                                 00006400
      *---------------------------------------------------------------* 00006500
      * ZONES DE TRAVAIL DIVERSES.                                    * 00006600
      *---------------------------------------------------------------* 00006700
       01  WS-FLAG-TRANS-NON-SELECT               PIC X VALUE ' '.      00006800
           88  WS-PREMIERE-NON-SELECT                   VALUE '1'.      00006900
           88  WS-DERNIERE-NON-SELECT                   VALUE '2'.      00007000
      *                                                                 00007100
B&S    01  WS-SAVE-NTRANS                         PIC X(08).            00007200
B&S    01  WS-SAVE-NVENTE                         PIC X(07).            00007300
B&S    01  WS-SAVE-NOINDICE                       PIC S9(3) COMP-3.     00007400
B&S    01  WS-SAVE-NOPAGE                         PIC 9(03).            00007500
      *                                                                 00007600
B&S    01  WS-NTRANS-AFFICHEE                     PIC X(08).            00007700
B&S    01  WS-NTRANS-A-AFFICHER                   PIC X(08).            00007800
      *                                                                 00007900
B&S    01  WS-TOP-SELECTION                       PIC X VALUE '1'.      00008000
B&S        88  WS-UNE-SELECTION                         VALUE '1'.      00008100
B&S        88  WS-PLUSIEURS-SELECTIONS                  VALUE '2'.      00008200
      *                                                                 00008300
      *                                                                 00008400
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00008500
      * DESCRIPTION DE LA TS DE PAGINATION (TAILLE MAXI.: 999 PAGES)  * 00008600
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00008700
      *                                                                 00008800
       01  FILLER     PIC X(16) VALUE '**** TSEM46 ****'.               00008900
      *                                                                 00009000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00010001
      * >>>>>>>>>>>>>>>>> DESCRIPTION DE LA TSEM46 <<<<<<<<<<<<<<<<<< * 00020001
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00030001
      *                                                               * 00040001
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00050001
      *         MODIFICATIONS POUR PASSAGE A BIENS ET SERVICES          00060001
      *  -> NTRANS PASSE DE X(4) � X(8)                                 00070001
      *  -> NOPERATEUR PASSE DE X(4) � X(7)                             00080001
      *  -> CMODPAIMT (1 � 6)  PASSE DE X(1) � X(5)                     00090001
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00100001
      *                                                               * 00110001
       01  TSEM46-IDENTIFICATEUR.                                       00120001
           05  TSEM46-TRANSID               PIC X(04) VALUE 'EM46'.     00130001
           05  TSEM46-NUMERO-DE-TERMINAL    PIC X(04) VALUE SPACES.     00140001
      *                                                               * 00150001
       01  TSEM46-ITEM.                                                 00160001
      *    05  TSEM46-LONGUEUR              PIC S9(4) VALUE +2730.      00170001
      * AJOUT SOC/LIEU DE VENTE 6 * 12 = + 72                           00180001
           05  TSEM46-LONGUEUR              PIC S9(4) VALUE +2802.      00190001
           05  TSEM46-DATAS.                                            00200001
               10  TSEM46-LIGNE             OCCURS 12.                  00210001
      *      PREMIERE PARTIE = 037 OCTETS PAR OCCURENCE               * 00220000
                   15  TSEM46-CTYPE            PIC X(01).               00230001
                   15  TSEM46-NSOCIETE         PIC X(03).               00240001
                   15  TSEM46-NLIEU            PIC X(03).               00250001
      * SOC / LIEU DE VENTE                                             00260001
                   15  TSEM46-NSOCVENTE        PIC X(03).               00270001
                   15  TSEM46-NLIEUVENTE       PIC X(03).               00280001
      * DATE DE CAISSE                                                  00290001
                   15  TSEM46-DCAISSE.                                  00300001
                       25  TSEM46-DCAISSE-SSAA PIC X(04).               00310001
                       25  TSEM46-DCAISSE-MM   PIC X(02).               00320001
                       25  TSEM46-DCAISSE-JJ   PIC X(02).               00330001
      * DATE DE VENTE                                                   00340001
                   15  TSEM46-DVENTE.                                   00350001
                       25  TSEM46-DVENTE-SSAA  PIC X(04).               00360001
                       25  TSEM46-DVENTE-MM    PIC X(02).               00370001
                       25  TSEM46-DVENTE-JJ    PIC X(02).               00380001
                   15  TSEM46-NCAISSE          PIC X(04).               00390001
                   15  TSEM46-NTRANS           PIC X(08).               00400001
                                                                        00410001
      * CAISSE ET TRANSACTION DE CONTREPARTIE                           00420001
                   15  TSEM46-NCAISC           PIC X(04).               00430001
                   15  TSEM46-NTRCPT           PIC X(08).               00440001
                                                                        00450001
                   15  TSEM46-FLAG-DONNEES-ENTETE PIC X.                00460001
                       88 TSEM46-ACCES-EM51-PAS-EFFECTUE VALUE ' '.     00470001
                       88 TSEM46-ACCES-EM51-EFFECTUE     VALUE 'O'.     00480001
                   15  TSEM46-NTRANS-ANNULEE   PIC X(04).               00490001
                   15  TSEM46-ANNULEE          PIC X VALUE ' '.         00500001
                       88 TSEM46-TRANSAC-RIEN        VALUE ' '.         00510001
                       88 TSEM46-TRANSAC-ANNULEE     VALUE '1'.         00520001
                       88 TSEM46-TRANSAC-ANNULANTE   VALUE '2'.         00530001
                   15  TSEM46-TTRANS           PIC X(03).               00540001
                   15  TSEM46-NTYPVENTE        PIC X(03).               00550001
                   15  TSEM46-APVF             PIC X(01).               00560001
                   15  TSEM46-CODE-APVF        PIC X(04).               00570001
                   15  TSEM46-CODE-TRANSACTION PIC 9(01).               00580001
                       88 TSEM46-TRANS-DECOMPTE     VALUE 1.            00590001
                       88 TSEM46-TRANS-APP-RETRAIT  VALUE 2.            00600001
                       88 TSEM46-AUTRE-TRANSACTION  VALUE 3.            00610001
                   15  TSEM46-NVENTE           PIC X(07).               00620001
                   15  TSEM46-NEMPORT          PIC X(08).               00630001
                   15  TSEM46-CNATVENTE        PIC X(03).               00640001
                   15  TSEM46-NFACT            PIC X(06).               00650001
                   15  TSEM46-NOPERATEUR       PIC X(07).               00660001
                   15  TSEM46-NVENDEUR         PIC X(07).               00670001
      * HEURE DE PAIEMENT                                               00680001
                   15  TSEM46-DHMPAIMT.                                 00690001
                       25  TSEM46-DHPAIMT      PIC X(02).               00700001
                       25  TSEM46-DMPAIMT      PIC X(02).               00710001
      * HEURE DE VENTE                                                  00720001
                   15  TSEM46-DHMVENTE.                                 00730001
                       25  TSEM46-DHVENTE      PIC X(02).               00740001
                       25  TSEM46-DMVENTE      PIC X(02).               00750001
                   15  TSEM46-PTTVENTE         PIC S9(7)V99 COMP-3.     00760001
                   15  TSEM46-PTTTRANS         PIC S9(7)V99 COMP-3.     00770001
                   15  TSEM46-PDEDUCT          PIC S9(7)V99 COMP-3.     00780001
                   15  TSEM46-PVERSE           PIC S9(7)V99 COMP-3.     00790001
                   15  TSEM46-PCOMPT           PIC S9(7)V99 COMP-3.     00800001
                   15  TSEM46-PLIVR            PIC S9(7)V99 COMP-3.     00810001
                   15  TSEM46-PDIFFERE         PIC S9(7)V99 COMP-3.     00820001
                   15  TSEM46-PRFACT           PIC S9(7)V99 COMP-3.     00830001
                   15  TSEM46-CORGORED         PIC X(05).               00840001
                   15  TSEM46-CMODPAIMT1       PIC X(5).                00850001
                   15  TSEM46-CMODPAIMT2       PIC X(5).                00860001
                   15  TSEM46-CMODPAIMT3       PIC X(5).                00870001
                   15  TSEM46-CMODPAIMT4       PIC X(5).                00880001
                   15  TSEM46-CMODPAIMT5       PIC X(5).                00890001
                   15  TSEM46-CMODPAIMT6       PIC X(5).                00900001
                   15  TSEM46-DMODPAIMT1       PIC X.                   00910001
                   15  TSEM46-DMODPAIMT2       PIC X.                   00920001
                   15  TSEM46-DMODPAIMT3       PIC X.                   00930001
                   15  TSEM46-DMODPAIMT4       PIC X.                   00940001
                   15  TSEM46-DMODPAIMT5       PIC X.                   00950001
                   15  TSEM46-DMODPAIMT6       PIC X.                   00960001
                   15  TSEM46-DEVENT           PIC X.                   00970001
                   15  TSEM46-DEVCRED          PIC X.                   00980001
                   15  TSEM46-CAPRET           PIC X.                   00990001
                   15  TSEM46-PREGLEM          PIC S9(7)V99.            01000001
                   15  TSEM46-DEVREGL          PIC X.                   01010001
                   15  TSEM46-NLIGNE           PIC X(03).               01020001
                   15  TSEM46-NOLIGNE          PIC 9(3).                01030001
                   15  TSEM46-NENVLP           PIC X(10).               01040001
                   15  TSEM46-PMTENV           PIC S9(7)V99 COMP-3.     01050001
               10  TSEM46-TOP-PF9              PIC 9(01).               01060001
      *   SI TSEM46-CODE-ECRAN = 0 ==> ECRAN RENSEIGNE                  01070001
      *   SI TSEM46-CODE-ECRAN = 9 ==> ECRAN NON RENSEIGNE              01080001
               10  TSEM46-CODE-ECRAN           PIC 9(01).               01090001
      *                                                                 01100001
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 01110001
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< * 01120001
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 01130001
      *                                                                 00009200
      *                                                                 00009300
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00009400
      * DESCRIPTION DES ZONES D'INTERFACE DB2                         * 00009500
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00009600
      *                                                                 00009700
      *****************************************************************         
      * ZONES D'INTERFACE POUR LES ACCES AUX DATA BASES DB2/SQL       *         
      ****************************************************** SYKWSQ10 *         
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
           05  TRACE-SQL-MESSAGE.                                               
      *                                                                         
             10  FILLER         PIC  X(4)  VALUE 'TAB='.                        
             10  TABLE-NAME     PIC  X(18)   VALUE SPACES.                      
      *                                                                         
      *      10  FILLER         PIC  X(1)   VALUE SPACES.                       
             10  FILLER         PIC  X(4)  VALUE 'MOD='.                        
             10  MODEL-NAME      PIC  X(6)   VALUE SPACES.                      
      *                                                                         
             10  FILLER         PIC  X(1)   VALUE SPACES.                       
             10  FILLER         PIC  X(5)   VALUE 'FUNC='.                      
             10  TRACE-SQL-FUNCTION  PIC  X(7)  VALUE  SPACES.                  
                 88  CODE-CONNECT               VALUE  'CONNECT'.               
                 88  CODE-SELECT                VALUE  'SELECT'.                
                 88  CODE-INSERT                VALUE  'INSERT'.                
                 88  CODE-UPDATE                VALUE  'UPDATE'.                
                 88  CODE-DELETE                VALUE  'DELETE'.                
                 88  CODE-PREPARE               VALUE  'PREPARE'.               
                 88  CODE-DECLARE               VALUE  'DECLARE'.               
                 88  CODE-OPEN                  VALUE  'OPEN'.                  
                 88  CODE-FETCH                 VALUE  'FETCH'.                 
                 88  CODE-CLOSE                 VALUE  'CLOSE'.                 
      *                                                                         
             10  FILLER         PIC  X(1)   VALUE SPACES.                       
             10  FILLER         PIC  X(8)   VALUE 'SQLCODE='.                   
             10  TRACE-SQL-CODE      PIC  -Z(7)9 VALUE  ZEROS.                  
      *                                                                         
           05  FILLER.                                                          
      *                                                                         
             10  FUNC-SQL       PIC  X(8)   VALUE  SPACES.                      
             10  FUNC-CONNECT   PIC  X(8)   VALUE 'CONNECT'.                    
             10  FUNC-SELECT    PIC  X(8)   VALUE 'SELECT'.                     
             10  FUNC-INSERT    PIC  X(8)   VALUE 'INSERT'.                     
             10  FUNC-UPDATE    PIC  X(8)   VALUE 'UPDATE'.                     
             10  FUNC-DELETE    PIC  X(8)   VALUE 'DELETE'.                     
             10  FUNC-PREPARE   PIC  X(8)   VALUE 'PREPARE'.                    
             10  FUNC-DECLARE   PIC  X(8)   VALUE 'DECLARE'.                    
             10  FUNC-OPEN      PIC  X(8)   VALUE 'OPEN'.                       
             10  FUNC-FETCH     PIC  X(8)   VALUE 'FETCH'.                      
             10  FUNC-CLOSE     PIC  X(8)   VALUE 'CLOSE'.                      
             10  FILLER-SQL     PIC  X(12)  VALUE SPACE.                        
      *                                                                         
           05  FILLER.                                                          
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10  SQL-CODE       PIC  S9(9) COMP-4.                              
      *--                                                                       
             10  SQL-CODE       PIC  S9(9) COMP-5.                              
      *}                                                                        
       EJECT                                                                    
       01  MESSAGE-ERREUR.                                              00000100
           05 ERREUR-UTILISATEUR   PIC X(4).                            00000200
           05 FILLER               PIC X          VALUE SPACES.         00000300
           05 LIBERR-MESSAGE       PIC X(53)      VALUE SPACES.         00000400
       01  MESSAGE-VARIABLE        PIC X(53)      VALUE SPACES.         00000500
       01  W-CODE-RETOUR-MLIBERRG  PIC X(1).                            00000600
       EJECT                                                            00010000
           COPY SYKWZCMD.                                               00010100
      *                                                                 00010200
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00010300
      * ZONES GENERALES OBLIGATOIRES                                  * 00010400
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00010500
           COPY  SYKWEIB0.                                              00010600
           COPY  SYKWDATH.                                              00010700
           COPY  SYKWCWA0.                                              00010800
           COPY  SYKWTCTU.                                              00010900
           COPY  SYKWECRA.                                              00011000
      *                                                                 00011100
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00011200
      * ZONES DE CONTROLE ET DE TRAITEMENT                            * 00011300
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00011400
           COPY  SYKWAGE0.                                              00011500
           COPY  SYKWDATE.                                              00011600
           COPY  SYKWDECI.                                              00011700
           COPY  SYKWVALN.                                              00011800
           COPY  SYKWTIME.                                              00011900
      *                                                                         
      *==> DARTY ******************************************************         
      * ZONES DE TRAVAIL POUR LES TRAITEMENTS DES MONTANTS SAISIS     *         
      ****************************************************** SYKWMONT *         
      *                                                                         
       01  FILLER PIC X(16) VALUE '**** W-MONT ****'.                           
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
         02  FILLER.                                                            
      *                                                                         
           05  W-MONT-USER             PIC X(20).                               
           05  FILLER                  REDEFINES W-MONT-USER.                   
             10  FILLER                OCCURS 20.                               
               15  W-MONT-USER-CAR     PIC X.                                   
               15  W-MONT-USER-CAR9    REDEFINES W-MONT-USER-CAR PIC 9.         
      *                                                                         
           05  W-MONT-FILE             PIC S9(9)V9(9) VALUE +0.                 
      *                                                                         
           05  W-MONT-NB-ENT-USER      PIC S9(3) COMP-3 VALUE +0.               
           05  W-MONT-NB-DEC-USER      PIC S9(3) COMP-3 VALUE +0.               
      *                                                                         
         02  FILLER.                                                            
      *                                                                         
           05  W-MONT-NUMERIC         PIC  X             VALUE SPACE.           
           05  W-MONT-POS-DEB         PIC S9(3)   COMP-3 VALUE +0.              
           05  W-MONT-POS-VIR         PIC S9(3)   COMP-3 VALUE +0.              
           05  W-MONT-NB-ENT          PIC S9(3)   COMP-3 VALUE +0.              
           05  W-MONT-NB-DEC          PIC S9(3)   COMP-3 VALUE +0.              
           05  W-MONT-LONG            PIC S9(3)   COMP-3 VALUE +20.             
           05  W-MONT-I               PIC S9(3)   COMP-3 VALUE +0.              
           05  W-MONT-J               PIC S9(3)   COMP-3 VALUE +0.              
           05  W-MONT-POIDS           PIC S9V9(9) COMP-3 VALUE +0.              
           05  W-MONT-SIGNE           PIC X            VALUE ' '.               
           05  W-MONT-FLAG            PIC X            VALUE SPACE.             
           05  W-MONT-EDIT            PIC -(9)9,9(9)   VALUE ZERO.              
           05  W-MONT-REEDIT REDEFINES W-MONT-EDIT.                             
             10  FILLER               OCCURS 20.                                
               15  W-MONT-EDIT-CAR    PIC X.                                    
           05  W-MONT-TAMPON.                                                   
             10  W-MONT-TAMPON1       PIC X            VALUE SPACE.             
             10  W-MONT-TAMPON2       PIC X(19)        VALUE SPACE.             
      *                                                                         
             EJECT                                                              
       EJECT                                                            00012100
      *                                                                 00012200
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00012300
      * ZONES DE COMMAREA                                             * 00012400
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00012500
      *                                                                 00012600
       01  FILLER     PIC X(16) VALUE '*** COMMAREA ***'.               00012700
      *                                                                 00012800
          01 COMM-HV49-DONNEES.                                         00010001
             10 COMM-HV49-FONCTIONS               PIC X(01).            00020001
                88 COMM-HV49-TOUCHE-PF7           VALUE '1'.            00030001
                88 COMM-HV49-TOUCHE-PF8           VALUE '2'.            00040001
                88 COMM-HV49-TOUCHE-PF10          VALUE '5'.            00050001
                88 COMM-HV49-TOUCHE-PF11          VALUE '6'.            00060001
                88 COMM-HV49-DEBUT                VALUE '3'.            00070001
             10 COMM-HV49-CODRET                  PIC 9(03) COMP-3.     00080001
                88 COMM-HV49-BOUCLE               VALUE 555.            00090001
                88 COMM-HV49-ERREUR-MANIP         VALUE 999.            00100001
             10 COMM-HV49-LIBERR                  PIC X(25).            00110001
             10 COMM-HV49-EHV-NBLG                PIC 9(02).            00120001
             10 COMM-HV49-DONNEES-COMMUNE.                              00130001
                15 COMM-HV49-NOPAGE                  PIC 9(03).         00140001
      *-->         NOMBRE DE PROPOSITIONS TROUV�ES                      00150001
                15 COMM-HV49-NBPROPO                 PIC 9(03).         00160001
                15 COMM-HV49-TAB-SELECTION.                             00170001
                   20 COMM-HV49-SELECTION OCCURS 250 PIC S9(3) COMP-3.  00180001
                   20 COMM-HV49-MAX-SELECT           PIC S9(3) COMP-3.  00190001
                   20 COMM-HV49-DERN-SELECT          PIC S9(3) COMP-3.  00200001
                      88 COMM-HV49-PAS-EN-SELECTION    VALUE 0.         00210001
                      88 COMM-HV49-EN-SELECTION        VALUE 1 THRU 999.00220001
      *-->         N� DE LA PROPOSITION AFFICH�E                        00230001
                15 COMM-HV49-NOPROPO                PIC S9(3) COMP-3.   00240001
      *-->         N� INDICE DE LA PROPOSITION AFFICH�E DANS LE TABLEAU 00250001
                15 COMM-HV49-NOINDICE               PIC S9(3) COMP-3.   00260001
      *-->         NB DE PROPOSITIONS D�J� AFFICH�ES                    00270001
                15 COMM-HV49-NBPROPO-AFFICHES       PIC S9(3) COMP-3.   00280000
      *                                                                 00013000
      *    COPY  SYKWSTAR.                                              00013100
       EJECT                                                            00013200
      *                                                                 00013300
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00013400
      * ZONE DE COMMAREA POUR PROGRAMME APPELE PAR LINK               * 00013500
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00013600
      *                                                                 00013700
       01  FILLER     PIC X(16)  VALUE 'Z-COMMAREA-LINK'.               00013800
      *                                                                 00013900
       01  Z-COMMAREA-LINK    PIC X(700).                               00014000
      *                                                                 00014100
           EJECT                                                        00000100
      *==> DARTY ****************************************************** 00010001
      *          * COMMAREA TRAITEMENT DATE - PGM CICS TETDATC        * 00020001
      ****************************************************** COMMDATC * 00030001
      *                                                                 00040001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-DATC-LONG-COMMAREA PIC S9(4) COMP VALUE +163.           00050001
      *--                                                                       
       01  COMM-DATC-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +163.                 
      *}                                                                        
      *                                                                 00060001
       01  Z-COMMAREA-TETDATC.                                          00070001
      *                                                                 00080001
          02 GFDATA                      PIC X.                         00090001
      *                                                                 00100001
          02 GFJJMMSSAA.                                                00110001
             05 GFJOUR                   PIC XX.                        00120001
             05 GFJJ REDEFINES GFJOUR    PIC 99.                        00130001
             05 GFMOIS                   PIC XX.                        00140001
             05 GFMM REDEFINES GFMOIS    PIC 99.                        00150001
             05 GFSIECLE                 PIC XX.                        00160001
             05 GFSS REDEFINES GFSIECLE  PIC 99.                        00170001
             05 GFANNEE                  PIC XX.                        00180001
             05 GFAA REDEFINES GFANNEE   PIC 99.                        00190001
      *                                                                 00200001
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00210003
          02 GFQNTA                      PIC 999.                       00220001
          02 GFQNT0                      PIC 9(5).                      00230001
      *                                                                 00240001
      *   RESULTATS SEMAINE                                             00250003
          02 GFSMN                       PIC 9.                         00260001
          02 GFSMN-LIB-C                 PIC X(3).                      00270001
          02 GFSMN-LIB-L                 PIC X(8).                      00280001
      *                                                                 00290001
      *   LIBELLE MOIS                                                  00300003
          02 GFMOI-LIB-C                 PIC X(3).                      00310001
          02 GFMOI-LIB-L                 PIC X(9).                      00320001
      *                                                                 00330001
      * DIFFERENTES EXPRESSIONS DES DATES                               00340003
      *   FORME SSAAMMJJ                                                00350003
          02 GFSAMJ-0                    PIC X(8).                      00360003
      *   FORME AAMMJJ                                                  00370003
          02 GFAMJ-1                     PIC X(6).                      00380003
      *   FORME JJMMSSAA                                                00390003
          02 GFJMSA-2                    PIC X(8).                      00400003
      *   FORME JJMMAA                                                  00410003
          02 GFJMA-3                     PIC X(6).                      00420003
      *   FORME JJ/MM/AA                                                00430003
          02 GFJMA-4                     PIC X(8).                      00440003
      *   FORME JJ/MM/SSAA                                              00450003
          02 GFJMSA-5                    PIC X(10).                     00460003
      *                                                                 00470001
          02 GFVDAT                      PIC X.                         00480002
          02 GFBISS                      PIC 9.                         00490001
      *                                                                 00500001
          02 GFWEEK.                                                    00510001
             05 GFSEMSS                  PIC 99.                        00520001
             05 GFSEMAA                  PIC 99.                        00530001
             05 GFSEMNU                  PIC 99.                        00540001
      *                                                                 00550001
          02 GFAJOUX                     PIC XX.                        00560001
          02 GFAJOUP REDEFINES GFAJOUX   PIC 99.                        00560002
          02 FILLER                      PIC X(6).                      00560003
      *                                                                 00570001
          02 GF-MESS-ERR                 PIC X(60).                     00580001
       EJECT                                                            00014300
      *                                                                 00014400
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00014500
      * ZONE BUFFER D'ENTREE-SORTIE                                   * 00014600
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00014700
           COPY  SYKWZINO.                                              00014800
       EJECT                                                            00014900
      *                                                                 00015000
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00015100
      * ZONE DE GESTION DES ERREURS                                   * 00015200
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00015300
      ******************************************************* 14-06-89 *        
      * ZONE D'INTERFACE POUR LA GESTION DES ERREURS NON RECOUVRABLES           
      ******************************************************* SYKWERRO *        
       01  Z-ERREUR.                                                            
           05  FILLER                 PIC X(256).                               
           05  Z-ERREUR-TRACE-MESS    PIC X(80).                                
           05  Z-ERREUR-EIBLK         PIC X(66).                                
       01  FILLER.                                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRACE-MESSAGE-LONG PIC S9(4) COMP VALUE +84.                     
      *--                                                                       
           05  TRACE-MESSAGE-LONG PIC S9(4) COMP-5 VALUE +84.                   
      *}                                                                        
           05  TRACE-MESSAGE.                                                   
      *                                     START-BUFFER-ADDRESS                
             10  FILLER         PIC  X      VALUE ''.                          
      *                                     23EME LIGNE 1ERE COLONNE            
             10  FILLER         PIC  X(2)   VALUE '$-'.                         
      *                                     START-FIELD                         
             10  FILLER         PIC  X      VALUE ''.                          
      *                                     ATTRIBUT                            
             10  FILLER         PIC  X      VALUE 'Y'.                          
             10  TRACE-MESS.                                                    
               15  MESS1          PIC  X(15)  VALUE SPACE.                      
               15  MESS           PIC  X(64)  VALUE SPACE.                      
               15  FILLER REDEFINES MESS.                                       
                 20  MESS-ABCODE  PIC  X(4).                                    
                 20  FILL         PIC  X(60).                                   
       EJECT                                                                    
       EJECT                                                            00015500
      *                                                                 00015600
      *                                                                 00015700
      *                                                                 00015800
       LINKAGE SECTION.                                                 00015900
      *                    DFHCOMMAREA                                  00016000
       01  DFHCOMMAREA.                                                 00016100
           05  FILLER PIC X OCCURS 10000 DEPENDING ON EIBCALEN.         00016200
           COPY  SYKLINKB.                                              00016300
       EJECT                                                            00016400
      *                                                                 00016500
      *                                                                 00016600
       PROCEDURE DIVISION.                                              00016700
      *                                                                 00016800
      ***************************************************************** 00016900
      *              T R A M E   DU   P R O G R A M M E               * 00017000
      ***************************************************************** 00017100
      *                                                                 00017200
       MODULE-MVT49                    SECTION.                         00017300
      *----------------------------------------                         00017400
      *                                                                 00017500
           MOVE LENGTH OF COMM-HV49-DONNEES TO LONG-COMMAREA.           00017600
           IF EIBCALEN NOT = LONG-COMMAREA                              00017700
              MOVE -001                           TO COMM-HV49-CODRET   00017800
              MOVE 'EIBCALEN NOT = LONG-COMMAREA' TO COMM-HV49-LIBERR   00017900
              PERFORM MODULE-SORTIE                                     00018000
           END-IF.                                                      00018100
      *                                                                 00018200
           MOVE DFHCOMMAREA           TO COMM-HV49-DONNEES.             00018300
      *                                                                 00018400
           MOVE 0 TO  COMM-HV49-CODRET.                                 00018500
      *                                                                 00018600
      *    DETERMINE LE NO DE L'ITEM DE LA TS ACTIVE              *     00018700
           IF COMM-HV49-DEBUT                                           00018800
              IF COMM-HV49-DERN-SELECT > 0                              00018900
      *-->       LISTE DES VENTES SELECTIONN�ES                         00019000
                 MOVE 1     TO   COMM-HV49-DERN-SELECT                  00019100
                                 COMM-HV49-NBPROPO-AFFICHES             00019200
                 MOVE COMM-HV49-SELECTION(COMM-HV49-DERN-SELECT)        00019300
                              TO COMM-HV49-NOPROPO                      00019400
              ELSE                                                      00019500
      *-->       LISTE DES VENTES NON SELECTIONN�ES                     00019600
                 SET COMM-HV49-PAS-EN-SELECTION TO TRUE                 00019700
                 MOVE 1 TO COMM-HV49-NOPROPO COMM-HV49-NBPROPO-AFFICHES 00019800
               END-IF                                                   00019900
               PERFORM DETERMINE-NOPAGE                                 00020000
           ELSE                                                         00020100
               PERFORM DETERMINE-POSITION                               00020200
           END-IF.                                                      00020300
           PERFORM MODULE-SORTIE.                                       00020400
      *                                                                 00020500
       FIN-MODULE-MVT49. EXIT.                                          00020600
      *                                                                 00020700
       MODULE-SORTIE                  SECTION.                          00020800
      *---------------------------------------                          00020900
      *                                                                 00021000
           MOVE COMM-HV49-DONNEES     TO DFHCOMMAREA.                   00021100
           EXEC CICS RETURN                                             00021200
           END-EXEC.                                                    00021300
      *                                                                 00021400
       FIN-MODULE-SORTIE. EXIT.                                         00021500
      *                                                                 00021600
      *                                                                 00021700
       FIN-INIT-USER.                  EXIT.                            00021800
      *------------------------------------------------- FIN DE SECTION 00021900
       EJECT                                                            00022000
      *                                                                 00022100
      *                                                                 00022200
       DETERMINE-POSITION              SECTION.                         00022300
      *----------------------------------------                         00022400
           IF COMM-HV49-EN-SELECTION                                    00022500
B&S           IF COMM-HV49-TOUCHE-PF8 OR COMM-HV49-TOUCHE-PF11          00022600
                 EVALUATE TRUE                                          00022700
                 WHEN COMM-HV49-NBPROPO-AFFICHES  >= COMM-HV49-NBPROPO  00022800
      *-->             DERNI�RE VENTE D�J� AFFICH�E                     00022900
                       SET COMM-HV49-ERREUR-MANIP TO TRUE               00023000
                                                                        00023100
                 WHEN COMM-HV49-DERN-SELECT < COMM-HV49-MAX-SELECT      00023200
      *-->          LISTE DES VENTES SELECTIONN�ES                      00023300
                       ADD 1      TO   COMM-HV49-DERN-SELECT            00023400
                                       COMM-HV49-NBPROPO-AFFICHES       00023500
                       MOVE COMM-HV49-SELECTION(COMM-HV49-DERN-SELECT)  00023600
                                  TO   COMM-HV49-NOPROPO                00023700
                                                                        00023800
B&S                 IF COMM-HV49-TOUCHE-PF8                             00023900
                       PERFORM DETERMINE-NOPAGE                         00024000
                                                                        00024100
B&S                 ELSE                                                00024200
 "                     IF COMM-HV49-TOUCHE-PF11                         00024300
 "                        SET WS-PLUSIEURS-SELECTIONS TO TRUE           00024400
 "    * RECUPERATION DU N� TRANS DE LA TRANSACTION AFFICHEE             00024500
 "                        PERFORM LECTURE-TSEM46                        00024600
 "                        MOVE TSEM46-NTRANS (COMM-HV49-NOINDICE)       00024700
 "                                TO   WS-NTRANS-AFFICHEE               00024800
 "                        MOVE  COMM-HV49-NOPAGE   TO  WS-SAVE-NOPAGE   00024900
 "                        MOVE  COMM-HV49-NOINDICE TO  WS-SAVE-NOINDICE 00025000
 "    * RECUPERATION DU N� TRANS DE LA TRANSACTION SUIVANTE SELECTIONNEE00025100
 "                        PERFORM DETERMINE-NOPAGE                      00025200
 "                                                                      00025300
 "                        IF WS-SAVE-NOPAGE NOT = COMM-HV49-NOPAGE      00025400
 "    * LECTURE DE LA TS POUR LA TRANSACTION SUIVANTE SELECTIONNEE      00025500
 "                           PERFORM LECTURE-TSEM46                     00025600
 "                           MOVE TSEM46-NTRANS (COMM-HV49-NOINDICE)    00025700
 "                                TO  WS-NTRANS-A-AFFICHER              00025800
 "                        ELSE                                          00025900
 "                           MOVE TSEM46-NTRANS (COMM-HV49-NOINDICE)    00026000
 "                                TO  WS-NTRANS-A-AFFICHER              00026100
 "                        END-IF                                        00026200
 "                                                                      00026300
 "                        IF                                            00026400
 "                        WS-NTRANS-A-AFFICHER NOT = WS-NTRANS-AFFICHEE 00026500
 "                           CONTINUE                                   00026600
 "                        ELSE                                          00026700
 "    * PERMET DE SORTIR DE MVT49 ET DE RETOURNER AU PGM APPELANT       00026800
 "    * POUR RECUPERER LA LIGNE SELECTIONNEE SUIVANTE SI IL Y A         00026900
 "                           SET COMM-HV49-BOUCLE TO TRUE               00027000
 "                        END-IF                                        00027100
 "                                                                      00027200
 "                     END-IF                                           00027300
B&S                 END-IF                                              00027400
                                                                        00027500
                 WHEN OTHER                                             00027600
      *-->          DEBUT DE LA LISTE DES VENTES NON SELECTIONN�ES      00027700
                    SET COMM-HV49-PAS-EN-SELECTION TO TRUE              00027800
B&S                 IF COMM-HV49-TOUCHE-PF8                             00027900
                       MOVE 0 TO COMM-HV49-NOPROPO                      00028000
B&S                 ELSE                                                00028100
 "                     IF COMM-HV49-TOUCHE-PF11                         00028200
 "                        MOVE 1 TO COMM-HV49-NOPROPO                   00028300
 "                     END-IF                                           00028400
B&S                 END-IF                                              00028500
                    PERFORM RECHERCHE-TRANS-NON-SELECT                  00028600
                    PERFORM DETERMINE-NOPAGE                            00028700
                 END-EVALUATE                                           00028800
              ELSE                                                      00028900
B&S              IF COMM-HV49-TOUCHE-PF7 OR COMM-HV49-TOUCHE-PF10       00029000
                    IF COMM-HV49-DERN-SELECT > 1                        00029100
      *-->          LISTE DES VENTES SELECTIONN�ES                      00029200
                       SUBTRACT 1 FROM COMM-HV49-DERN-SELECT            00029300
                                       COMM-HV49-NBPROPO-AFFICHES       00029400
                       MOVE COMM-HV49-SELECTION(COMM-HV49-DERN-SELECT)  00029500
                                    TO COMM-HV49-NOPROPO                00029600
      *                PERFORM DETERMINE-NOPAGE                         00029700
                                                                        00029800
B&S                 IF COMM-HV49-TOUCHE-PF7                             00029900
                       PERFORM DETERMINE-NOPAGE                         00030000
                                                                        00030100
B&S                 ELSE                                                00030200
 "                     IF COMM-HV49-TOUCHE-PF10                         00030300
 "                        SET WS-PLUSIEURS-SELECTIONS TO TRUE           00030400
 "    * RECUPERATION DU N� TRANS DE LA TRANSACTION AFFICHEE             00030500
 "                        PERFORM LECTURE-TSEM46                        00030600
 "                        MOVE TSEM46-NTRANS (COMM-HV49-NOINDICE)       00030700
 "                                TO   WS-NTRANS-AFFICHEE               00030800
 "                        MOVE  COMM-HV49-NOPAGE   TO  WS-SAVE-NOPAGE   00030900
 "                        MOVE  COMM-HV49-NOINDICE TO  WS-SAVE-NOINDICE 00031000
 "    * RECUPERATION DU N� TRANS DE LA TRANSACTION SUIVANTE SELECTIONNEE00031100
 "                        PERFORM DETERMINE-NOPAGE                      00031200
 "                                                                      00031300
 "                        IF WS-SAVE-NOPAGE NOT = COMM-HV49-NOPAGE      00031400
 "    * LECTURE DE LA TS POUR LA TRANSACTION SUIVANTE SELECTIONNEE      00031500
 "                           PERFORM LECTURE-TSEM46                     00031600
 "                           MOVE TSEM46-NTRANS (COMM-HV49-NOINDICE)    00031700
 "                                TO  WS-NTRANS-A-AFFICHER              00031800
 "                        ELSE                                          00031900
 "                           MOVE TSEM46-NTRANS (COMM-HV49-NOINDICE)    00032000
 "                                TO  WS-NTRANS-A-AFFICHER              00032100
 "                        END-IF                                        00032200
 "                                                                      00032300
 "                        IF                                            00032400
 "                        WS-NTRANS-A-AFFICHER NOT = WS-NTRANS-AFFICHEE 00032500
 "                           CONTINUE                                   00032600
 "                        ELSE                                          00032700
 "    * PERMET DE SORTIR DE MVT49 ET DE RETOURNER AU PGM APPELANT       00032800
 "    * POUR RECUPERER LA LIGNE SELECTIONNEE SUIVANTE SI IL Y A         00032900
 "                           SET COMM-HV49-BOUCLE TO TRUE               00033000
 "                        END-IF                                        00033100
 "                                                                      00033200
 "                     END-IF                                           00033300
B&S                 END-IF                                              00033400
                    ELSE                                                00033500
      *--> CAS PARTICULIER DE LA TRANSACTION SELECTIONNEE + PF10 :      00033600
      *--> PERMET DE PASSER A LA TRANSACTION PRECEDENTE                 00033700
B&S                    IF  COMM-HV49-DERN-SELECT = 1                    00033800
 "                     AND COMM-HV49-TOUCHE-PF10                        00033900
TEST  *                    SET COMM-HV49-PAS-EN-SELECTION TO TRUE       00034000
                           SET COMM-HV49-ERREUR-MANIP TO TRUE           00034100
 "    *                    GO TO DETERMINE-POSITION                     00034200
B&S                    ELSE                                             00034300
      *-->                 PREMIERE VENTE SELECTIONN�E D�J� AFFICH�E    00034400
                           SET COMM-HV49-ERREUR-MANIP TO TRUE           00034500
B&S                    END-IF                                           00034600
                    END-IF                                              00034700
B&S              END-IF                                                 00034800
              END-IF                                                    00034900
           ELSE                                                         00035000
      *-->    LISTE DES VENTES NON SELECTIONN�ES                        00035100
B&S           IF COMM-HV49-TOUCHE-PF8 OR COMM-HV49-TOUCHE-PF11          00035200
                 IF COMM-HV49-NOPROPO < COMM-HV49-NBPROPO               00035300
                    PERFORM RECHERCHE-TRANS-NON-SELECT                  00035400
                    IF WS-DERNIERE-NON-SELECT                           00035500
      *-->             DERNI�RE VENTE SELECTIONN�E D�J� AFFICH�E        00035600
                       SET COMM-HV49-ERREUR-MANIP TO TRUE               00035700
                    ELSE                                                00035800
                       PERFORM DETERMINE-NOPAGE                         00035900
                    END-IF                                              00036000
                 ELSE                                                   00036100
      *-->          DERNI�RE VENTE SELECTIONN�E D�J� AFFICH�E           00036200
                    SET COMM-HV49-ERREUR-MANIP TO TRUE                  00036300
                 END-IF                                                 00036400
              ELSE                                                      00036500
      *-->       TOUCHE PF7 / PF10 ACTIV�E                              00036600
B&S              IF COMM-HV49-TOUCHE-PF7 OR COMM-HV49-TOUCHE-PF10       00036700
                    IF COMM-HV49-NOPROPO > 1                            00036800
      *-->             LISTE DES VENTES NON SELECTIONN�ES               00036900
                       PERFORM RECHERCHE-TRANS-NON-SELECT               00037000
                       IF  WS-PREMIERE-NON-SELECT                       00037100
      *-->                PREMIERE VENTE NON SELECTIONN�E D�J� AFFICH�E 00037200
      *-->                FIN LISTE DES VENTES SELECTIONN�ES            00037300
                          MOVE COMM-HV49-MAX-SELECT                     00037400
                                                TO COMM-HV49-DERN-SELECT00037500
                         MOVE COMM-HV49-SELECTION(COMM-HV49-DERN-SELECT)00037600
                                                TO COMM-HV49-NOPROPO    00037700
                          SUBTRACT 1 FROM COMM-HV49-NBPROPO-AFFICHES    00037800
                       END-IF                                           00037900
                       PERFORM DETERMINE-NOPAGE                         00038000
                    ELSE                                                00038100
      *-->             FIN DE LA LISTE DES VENTES SELECTIONN�ES         00038200
                       IF COMM-HV49-MAX-SELECT > 0                      00038300
                         MOVE COMM-HV49-MAX-SELECT                      00038400
                                                TO COMM-HV49-DERN-SELECT00038500
                         MOVE COMM-HV49-SELECTION(COMM-HV49-DERN-SELECT)00038600
                                                TO COMM-HV49-NOPROPO    00038700
                         SUBTRACT 1 FROM COMM-HV49-NBPROPO-AFFICHES     00038800
B&S                      IF COMM-HV49-TOUCHE-PF10                       00038900
 "                          SET WS-PLUSIEURS-SELECTIONS TO  TRUE        00039000
B&S                      END-IF                                         00039100
                         PERFORM DETERMINE-NOPAGE                       00039200
                       ELSE                                             00039300
      *-->               DERNI�RE VENTE SELECTIONN�E D�J� AFFICH�E      00039400
                         SET COMM-HV49-ERREUR-MANIP  TO  TRUE           00039500
                       END-IF                                           00039600
B&S                 END-IF                                              00039700
                 END-IF                                                 00039800
              END-IF.                                                   00039900
      *                                                                 00040000
       FIN-DETERMINE-POSITION.         EXIT.                            00040100
      *------------------------------------------------- FIN DE SECTION 00040200
      *                                                                 00040300
      *                                                                 00040400
      *----------------------------------------*                        00040500
       RECHERCHE-TRANS-NON-SELECT      SECTION.                         00040600
      *----------------------------------------*                        00040700
      * RECHERCHE LA PROCHAINE VENTE NON SELECTIONNEE          *        00040800
      *                                                                 00040900
      * EN ENTREE COMM-HV49-NOPROPO = NOITEM DE D�BUT DE RECHERCHE      00041000
      *                                                                 00041100
           SET TROUVE TO TRUE.                                          00041200
           INITIALIZE WS-FLAG-TRANS-NON-SELECT.                         00041300
B&S        IF COMM-HV49-TOUCHE-PF8 OR COMM-HV49-TOUCHE-PF11             00041400
              COMPUTE WS-NOITEM = COMM-HV49-NOPROPO + 1                 00041500
      *                                                                 00041600
B&S           IF  COMM-HV49-TOUCHE-PF8                                  00041700
                  PERFORM VARYING WS-I FROM WS-NOITEM BY 1              00041800
                      UNTIL NON-TROUVE OR WS-I > COMM-HV49-NBPROPO      00041900
                      PERFORM TESTE-TRANS-SELECTIONNEE                  00042000
                                                                        00042100
                      IF TROUVE                                         00042200
                         ADD 1 TO WS-NOITEM                             00042300
                      END-IF                                            00042400
                  END-PERFORM                                           00042500
B&S           ELSE                                                      00042600
      *                                                                 00042700
B&S               IF  COMM-HV49-TOUCHE-PF11                             00042800
                      PERFORM TESTE-TRANS-SELECTIONNEE-ASC              00042900
B&S               END-IF                                                00043000
B&S           END-IF                                                    00043100
      *                                                                 00043200
                                                                        00043300
              IF WS-I > COMM-HV49-NBPROPO AND TROUVE                    00043400
                 SET WS-DERNIERE-NON-SELECT TO TRUE                     00043500
              ELSE                                                      00043600
B&S   * ==> SI TRANSACTION NON TROUVEE --> ERREUR MANIP                 00043700
B&S              IF  COMM-HV49-TOUCHE-PF11  AND  NON-TROUVE             00043800
                     SET COMM-HV49-ERREUR-MANIP TO TRUE                 00043900
B&S              ELSE                                                   00044000
                     ADD 1 TO COMM-HV49-NBPROPO-AFFICHES                00044100
                     MOVE COMM-HV49-NBPROPO-AFFICHES                    00044200
                                    TO  COMM-HV49-NOPROPO               00044300
B&S              END-IF                                                 00044400
              END-IF                                                    00044500
      *                                                                 00044600
           ELSE                                                         00044700
B&S           IF COMM-HV49-TOUCHE-PF7 OR COMM-HV49-TOUCHE-PF10          00044800
                 COMPUTE WS-NOITEM = COMM-HV49-NOPROPO - 1              00044900
      *                                                                 00045000
B&S              IF COMM-HV49-TOUCHE-PF7                                00045100
                    PERFORM VARYING WS-I FROM WS-NOITEM BY -1           00045200
                      UNTIL NON-TROUVE OR WS-I < 1                      00045300
                         PERFORM TESTE-TRANS-SELECTIONNEE               00045400
                                                                        00045500
B&S                   IF TROUVE AND COMM-HV49-TOUCHE-PF7                00045600
                         SUBTRACT 1 FROM WS-NOITEM                      00045700
                      END-IF                                            00045800
                    END-PERFORM                                         00045900
B&S              ELSE                                                   00046000
      *                                                                 00046100
B&S                 IF COMM-HV49-TOUCHE-PF10                            00046200
B&S                    COMPUTE WS-COMPT = COMM-HV49-NOPROPO - 1         00046300
                       PERFORM TESTE-TRANS-SELECTIONNEE-DESC            00046400
                                                                        00046500
TEST                   PERFORM VARYING WS-I FROM WS-NOITEM BY -1        00046600
TEST                     UNTIL NON-TROUVE OR WS-I < 1                   00046700
TEST                        PERFORM TESTE-TRANS-SELECTIONNEE            00046800
TEST                   END-PERFORM                                      00046900
                                                                        00047000
B&S                 END-IF                                              00047100
B&S           END-IF                                                    00047200
      *                                                                 00047300
B&S           IF  COMM-HV49-TOUCHE-PF7                                  00047400
B&S              IF  WS-I < 1 AND TROUVE                                00047500
                     SET WS-PREMIERE-NON-SELECT TO TRUE                 00047600
                 ELSE                                                   00047700
                     SUBTRACT 1 FROM COMM-HV49-NBPROPO-AFFICHES         00047800
                     MOVE WS-NOITEM TO COMM-HV49-NOPROPO                00047900
B&S              END-IF                                                 00048000
              END-IF                                                    00048100
      *                                                                 00048200
TEST          IF  COMM-HV49-TOUCHE-PF10                                 00048300
TEST             IF  WS-I < 1 AND TROUVE                                00048400
TEST  *          IF  WS-A = 1 AND TROUVE                                00048500
TEST                 SET WS-PREMIERE-NON-SELECT TO TRUE                 00048600
TEST             ELSE                                                   00048700
TEST                 SUBTRACT 1 FROM COMM-HV49-NBPROPO-AFFICHES         00048800
TEST                 MOVE WS-NOITEM TO COMM-HV49-NOPROPO                00048900
TEST             END-IF                                                 00049000
TEST          END-IF                                                    00049100
TEST  *       IF  COMM-HV49-TOUCHE-PF10                                 00049200
TEST  *       AND WS-I < 1 AND TROUVE                                   00049300
TEST  *           SUBTRACT 1 FROM COMM-HV49-NBPROPO-AFFICHES            00049400
TEST  *           MOVE WS-NOITEM TO COMM-HV49-NOPROPO                   00049500
TEST  *       END-IF                                                    00049600
           END-IF.                                                      00049700
      *                                                                 00049800
       FIN-RECHERCHE-TRANS-NON-SELECT.  EXIT.                           00049900
      *------------------------------------------------- FIN DE SECTION 00050000
      *                                                                 00050100
       TESTE-TRANS-SELECTIONNEE         SECTION.                        00050200
      *----------------------------------------                         00050300
      *                                                                 00050400
      * SI VENTE SELECTIONNE =>     TROUVE                              00050500
      * SINON                => NON TROUVE                              00050600
           SET NON-TROUVE TO TRUE                                       00050700
           PERFORM VARYING WS-T FROM 1 BY 1                             00050800
                   UNTIL TROUVE OR WS-T > COMM-HV49-MAX-SELECT          00050900
                   IF COMM-HV49-SELECTION(WS-T) = WS-NOITEM             00051000
                      SET TROUVE TO TRUE                                00051100
                   END-IF                                               00051200
           END-PERFORM.                                                 00051300
                                                                        00051400
      *                                                                 00051500
       FIN-TESTE-TRANS-SELECTIONNEE. EXIT.                              00051600
      *------------------------------------------------- FIN DE SECTION 00051700
      *                                                                 00051800
       TESTE-TRANS-SELECTIONNEE-ASC    SECTION.                         00051900
      *----------------------------------------                         00052000
B&S   * LECTURE TS DE LA PAGE EN COURS POUR RECUPERER N� TRANSACTIONS   00052100
B&S        PERFORM LECTURE-TSEM46.                                      00052200
      *                                                                 00052300
      * SI VENTE SELECTIONNEE =>     TROUVE                             00052400
      * SINON                 => NON TROUVE                             00052500
           SET NON-TROUVE TO TRUE.                                      00052600
B&S        MOVE   COMM-HV49-NOINDICE    TO  WS-COMPT                    00052700
B&S                                         WS-NOITEM.                  00052800
                                                                        00052900
B&S   * ==> PASSAGE DE LA PAGE N A N+1                                  00053000
           IF ( COMM-HV49-NOINDICE = 12                                 00053100
            OR  COMM-HV49-NOPROPO  = 12  )                              00053200
                                                                        00053300
B&S   * --> SAUVEGARDE DES NTRANS ET NVENTE DE LA PAGE N                00053400
B&S            MOVE TSEM46-NTRANS (12)  TO  WS-SAVE-NTRANS              00053500
B&S            MOVE TSEM46-NVENTE (12)  TO  WS-SAVE-NVENTE              00053600
                                                                        00053700
B&S   * --> LECTURE DE LA PAGE SUIVANTE (N+1)                           00053800
               COMPUTE COMM-HV49-NOPAGE = COMM-HV49-NOPAGE + 1          00053900
B&S            PERFORM READ-TSEM46                                      00054000
                                                                        00054100
B&S   * --> COMPARAISON DE LA TRANSACTION (PAGE N, N� 12)               00054200
 "    * --> AVEC LA TRANSACTION (PAGE N+1, N� 1)                        00054300
 "             IF  WS-SAVE-NTRANS NOT = TSEM46-NTRANS (1)               00054400
 "                 SET TROUVE TO TRUE                                   00054500
 "                 MOVE  1    TO WS-NOITEM                              00054600
 "                               COMM-HV49-NOINDICE                     00054700
 "             ELSE                                                     00054800
 "                 IF  WS-SAVE-NTRANS  = TSEM46-NTRANS (1)              00054900
 "                 AND WS-SAVE-NVENTE NOT = TSEM46-NVENTE (1)           00055000
 "                     ADD 1    TO  COMM-HV49-NBPROPO-AFFICHES          00055100
B&S                    PERFORM SELECTION-TRANSACTION-ASC                00055200
                   END-IF                                               00055300
               END-IF                                                   00055400
B&S        ELSE                                                         00055500
 "             PERFORM SELECTION-TRANSACTION-ASC                        00055600
 "    * --> SI MEME TRANSACTION EN PAGE N, LIGNES N� 11 ET 12)          00055700
 "    * --> ON PASSE � LA PAGE N+1                                      00055800
 "             IF ( NON-TROUVE AND WS-COMPT = 11 )                      00055900
 "                ADD 1         TO  COMM-HV49-NOINDICE                  00056000
 "                GO TO  TESTE-TRANS-SELECTIONNEE-ASC                   00056100
 "             END-IF                                                   00056200
B&S        END-IF.                                                      00056300
      *                                                                 00056400
B&S        IF ( NON-TROUVE AND WS-COMPT NOT = 11 )                      00056500
 "             SET COMM-HV49-ERREUR-MANIP TO TRUE                       00056600
B&S        END-IF.                                                      00056700
      *                                                                 00056800
       FIN-TRANS-SELECTIONNEE-ASC. EXIT.                                00056900
      *------------------------------------------------- FIN DE SECTION 00057000
      *                                                                 00057100
      *------------------------------------*                            00057200
B&S    SELECTION-TRANSACTION-ASC   SECTION.                             00057300
      *------------------------------------*                            00057400
B&S        PERFORM VARYING WS-T FROM WS-COMPT  BY 1                     00057500
             UNTIL TROUVE OR WS-T >= 12                                 00057600
B&S            COMPUTE WS-A = (WS-T) + 1                                00057700
               IF  TSEM46-NTRANS(WS-A) = LOW-VALUE OR SPACE             00057800
B&S                MOVE 15    TO WS-T                                   00057900
 "             ELSE                                                     00058000
 "                 IF  TSEM46-NTRANS(WS-T) NOT = TSEM46-NTRANS(WS-A)    00058100
 "                    SET TROUVE TO TRUE                                00058200
 "                    MOVE WS-A  TO WS-NOITEM                           00058300
 "                                  COMM-HV49-NOINDICE                  00058400
 "                 ELSE                                                 00058500
 "                    IF  TSEM46-NTRANS(WS-T) = TSEM46-NTRANS(WS-A)     00058600
 "                    AND TSEM46-NVENTE(WS-T) NOT = TSEM46-NVENTE(WS-A) 00058700
 "                      ADD 1    TO  COMM-HV49-NBPROPO-AFFICHES         00058800
 "                      MOVE TSEM46-NTRANS(WS-A) TO WS-SAVE-NTRANS      00058900
B&S                     MOVE TSEM46-NVENTE(WS-A) TO WS-SAVE-NVENTE      00059000
                      END-IF                                            00059100
                   END-IF                                               00059200
               END-IF                                                   00059300
           END-PERFORM.                                                 00059400
      *                                                                 00059500
B&S    FIN-SELECTION-TRANSACTION-ASC. EXIT.                             00059600
      *                                                                 00059700
      *----------------------------------------*                        00059800
       TESTE-TRANS-SELECTIONNEE-DESC   SECTION.                         00059900
      *----------------------------------------*                        00060000
B&S   * LECTURE TS POUR RECUPERER LES N� TRANSACTIONS                   00060100
B&S        PERFORM LECTURE-TSEM46.                                      00060200
      *                                                                 00060300
      * SI VENTE SELECTIONNEE =>     TROUVE                             00060400
      * SINON                 => NON TROUVE                             00060500
           SET NON-TROUVE TO TRUE.                                      00060600
B&S        MOVE   COMM-HV49-NOPROPO  TO WS-COMPT                        00060700
                                        WS-NOITEM.                      00060800
B&S        MOVE   COMM-HV49-NOINDICE TO WS-COMPT.                       00060900
                                                                        00061000
B&S        IF COMM-HV49-NOINDICE = 1                                    00061100
                                                                        00061200
B&S   * --> SAUVEGARDE DES NTRANS ET NVENTE DE LA PAGE N                00061300
 "             MOVE TSEM46-NTRANS (1)  TO WS-SAVE-NTRANS                00061400
 "             MOVE TSEM46-NVENTE (1)  TO WS-SAVE-NVENTE                00061500
                                                                        00061600
B&S   * --> LECTURE DE LA PAGE PRECEDENTE (N-1)                         00061700
 "             COMPUTE COMM-HV49-NOPAGE = COMM-HV49-NOPAGE - 1          00061800
 "             PERFORM LECTURE-TSEM46                                   00061900
                                                                        00062000
B&S   * --> COMPARAISON DE LA TRANSACTION (PAGE N, N� 1)                00062100
 "    * --> AVEC LA TRANSACTION (PAGE N-1, N� 12)                       00062200
 "             IF  WS-SAVE-NTRANS NOT = TSEM46-NTRANS (12)              00062300
 "                 SET TROUVE TO TRUE                                   00062400
 "                 MOVE  12   TO WS-NOITEM                              00062500
 "                               COMM-HV49-NOINDICE                     00062600
 "             ELSE                                                     00062700
 "                 IF  WS-SAVE-NTRANS  = TSEM46-NTRANS (12)             00062800
 "                 AND WS-SAVE-NVENTE NOT = TSEM46-NVENTE (12)          00062900
 "                     SUBTRACT 1 FROM  COMM-HV49-NBPROPO-AFFICHES      00063000
 "                     PERFORM SELECTION-TRANSACTION-DESC               00063100
                   END-IF                                               00063200
               END-IF                                                   00063300
B&S        ELSE                                                         00063400
 "             PERFORM SELECTION-TRANSACTION-DESC                       00063500
B&S        END-IF.                                                      00063600
      *                                                                 00063700
B&S        IF  NON-TROUVE                                               00063800
 "             SET COMM-HV49-ERREUR-MANIP TO TRUE                       00063900
B&S        END-IF.                                                      00064000
      *                                                                 00064100
       FIN-TRANS-SELECTIONNEE-DESC. EXIT.                               00064200
      *                                                                 00064300
      *-------------------------------------*                           00064400
B&S    SELECTION-TRANSACTION-DESC   SECTION.                            00064500
      *-------------------------------------*                           00064600
B&S        PERFORM VARYING WS-T FROM WS-COMPT  BY -1                    00064700
                   UNTIL TROUVE OR WS-T = 1                             00064800
                   COMPUTE WS-A = (WS-T) - 1                            00064900
B&S                IF TSEM46-NTRANS(WS-T) NOT = TSEM46-NTRANS(WS-A)     00065000
 "                    SET TROUVE TO TRUE                                00065100
 "                    MOVE WS-A  TO WS-NOITEM                           00065200
B&S                   MOVE 1     TO WS-I                                00065300
                   ELSE                                                 00065400
                      IF  TSEM46-NTRANS(WS-T) = TSEM46-NTRANS(WS-A)     00065500
                      AND TSEM46-NVENTE(WS-T) NOT = TSEM46-NVENTE(WS-A) 00065600
                        SUBTRACT 1 FROM  COMM-HV49-NBPROPO-AFFICHES     00065700
                      END-IF                                            00065800
                   END-IF                                               00065900
           END-PERFORM.                                                 00066000
      *                                                                 00066100
B&S    FIN-SELECTION-TRANSACTION-DESC. EXIT.                            00066200
      *                                                                 00066300
B&S    LECTURE-TSEM46                  SECTION.                         00066400
      *----------------------------------------                         00066500
B&S        MOVE EIBTRMID               TO TSEM46-NUMERO-DE-TERMINAL.    00066600
           MOVE COMM-HV49-NOPAGE       TO RANG-TS.                      00066700
           PERFORM READ-TSEM46.                                         00066800
      *                                                                 00066900
B&S    FIN-LECTURE-TSEM46.             EXIT.                            00067000
      *------------------------------------------------- FIN DE SECTIO  00067100
      *                                                                 00067200
B&S    READ-TSEM46                     SECTION.                         00067300
      *----------------------------------------                         00067400
           MOVE TSEM46-IDENTIFICATEUR  TO IDENT-TS.                     00067500
           MOVE TSEM46-LONGUEUR        TO LONG-TS.                      00067600
           PERFORM READ-TS.                                             00067700
           MOVE Z-INOUT                TO TSEM46-DATAS.                 00067800
      *                                                                 00067900
B&S    FIN-READ-TSEM46.                EXIT.                            00068000
      *------------------------------------------------- FIN DE SECTION 00068100
      *                                                                 00068200
       DETERMINE-NOPAGE                SECTION.                         00068300
      *----------------------------------------                         00068400
      * DETERMINE LE NO DE PAGE PAR RAPPORT � CELUI DE L'ITEM  *        00068500
      * --> GARDE L'INCREMENTATION DU NOPAGE FAITE DANS -ASC / -DESC    00068600
B&S        IF ( ( COMM-HV49-TOUCHE-PF10                                 00068700
 "          OR    COMM-HV49-TOUCHE-PF11 )                               00068800
 "          AND ( WS-UNE-SELECTION       ) )                            00068900
B&S            MOVE WS-NOITEM TO COMM-HV49-NOINDICE                     00069000
               COMPUTE COMM-HV49-NBPROPO-AFFICHES =                     00069100
                  ( COMM-HV49-NOPAGE - 1 ) * 12                         00069200
                +   COMM-HV49-NOINDICE                                  00069300
               END-COMPUTE                                              00069400
               MOVE  COMM-HV49-NBPROPO-AFFICHES TO COMM-HV49-NOPROPO    00069500
                                                                        00069600
B&S        ELSE                                                         00069700
               COMPUTE COMM-HV49-NOPAGE =                               00069800
                  ((COMM-HV49-NOPROPO + COMM-HV49-EHV-NBLG - 1 ) /      00069900
                                        COMM-HV49-EHV-NBLG)             00070000
               END-COMPUTE                                              00070100
      *                                                                 00070200
               COMPUTE COMM-HV49-NOINDICE = COMM-HV49-NOPROPO -         00070300
                  ((COMM-HV49-NOPAGE - 1) * COMM-HV49-EHV-NBLG)         00070400
               END-COMPUTE                                              00070500
B&S        END-IF.                                                      00070600
      *                                                                 00070700
       FIN-DETERMINE-NOPAGE. EXIT.                                      00070800
      *------------------------------------------------- FIN DE SECTION 00070900
      *                                                                 00071000
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00071100
B&S   * DESCRIPTION DES BRIQUES AIDA TEMPORARY STORAGE (TS)           * 00071200
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00071300
      *                                                                 00071400
      *TEMP01-COPY SECTION. CONTINUE. COPY  SYKCTSDE.                   00071500
       TEMP02-COPY SECTION. CONTINUE. COPY  SYKCTSRD.                   00071600
      *TEMP03-COPY SECTION. CONTINUE. COPY  SYKCTSRW.                   00071700
      *TEMP04-COPY SECTION. CONTINUE. COPY  SYKCTSWR.                   00071800
      *                                                                 00071900
      *                                                                 00072000
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00072100
B&S   * MODULES DE CONTROLES ET DE TRAITEMENTS STANDARDISES           * 00072200
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00072300
      *                                                                 00072400
      *T01-COPY SECTION. CONTINUE. COPY SYKPAGE0.                       00072500
      *T02-COPY SECTION. CONTINUE. COPY SYKPDATE.                       00072600
      *T03-COPY SECTION. CONTINUE. COPY SYKPDECI.                       00072700
      *T04-COPY SECTION. CONTINUE. COPY SYKPVALN.                       00072800
      *T05-COPY SECTION. CONTINUE. COPY SYKPTIME.                       00072900
      *T06-COPY SECTION. CONTINUE. COPY SYKPMONT.                       00073000
B&S    99-COPY  SECTION. CONTINUE. COPY SYKCERRO.                       00073100
      *                                                                 00073200
      ***************************************************************** 00073300
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<* 00073400
      *>>>>>>>>>>>>>>>> FIN DU PROGRAMME SOURCE MVT49 <<<<<<<<<<<<<<<<* 00073500
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<* 00073600
      ***************************************************************** 00073700
