      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. TVT43.                                                       
       AUTHOR. DSA049.                                                          
      ******************************************************************        
      *  F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E   *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  PROGRAMME  : TVT43                                            *        
      *  TITRE      : CONSULTATION DES QUOTAS PAR :                    *        
      *               - PAR DATE           (SUIVANTE F08, PRECED F07)  *        
      *               - MODE DE DELIVRANCE (SUIVANTE F10, PRECED F09)  *        
      *               - PERIMETRE DE LIVRAISON (SUIV F12, PRECED F11)  *        
      *              (APPEL A MGV23 --> RECHERCHE DES QUOTAS)          *        
      *                                                                *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       DATA DIVISION.                                                           
       WORKING-STORAGE SECTION.
      *{Post-translation Correct-Val-pointeur
         77  NULLV PIC S9(8) COMP-5 VALUE -16777216.
         77  NULLP REDEFINES NULLV USAGE IS POINTER.
      *}
      ******************************************************************        
      *                         W O R K I N G                          *        
      ******************************************************************        
      *                                                                         
       77  I                          PIC S9(5) COMP-3     VALUE 0.             
       77  J                          PIC S9(5) COMP-3     VALUE 0.             
       77  K                          PIC S9(5) COMP-3     VALUE 0.             
       77  L                          PIC S9(5) COMP-3     VALUE 0.             
       77  ZQUOTA                     PIC ZZZZZ.                                
       01  WCMODDEL                   PIC X(03)            VALUE SPACES.        
       01  FILLER                     PIC X(15) VALUE '*****  TS *****'.        
      **** DESCRIPTION DE LA TSGV01 : TS MAJ TABLE RTGV11                       
           COPY TSGV01.                                                         
      *                                                                         
      **** DESCRIPTION DE LA TSGV23 : TS D'APPEL A MGV23                        
       01  ZONE-TS-ENTREE.                                                      
           03 TSEN-ENTETE.                                                      
              05 TSEN-NOMPROG         PIC X(08)            VALUE SPACES.        
              05 TSEN-REQUETE         PIC X(04)            VALUE SPACES.        
              05 TSEN-CODRET          PIC 9(04)            VALUE ZEROES.        
              05 TSEN-NBITEM          PIC 9(04)            VALUE ZEROES.        
              05 TSEN-LGDONNEES       PIC 9(04)            VALUE ZEROES.        
           03 TSEN-DONNEES.                                                     
              05 TSEN-NSOCIETE        PIC X(03)            VALUE SPACES.        
              05 TSEN-NLIEU           PIC X(03)            VALUE SPACES.        
              05 TSEN-CMODDEL         PIC X(03)            VALUE SPACES.        
              05 TSEN-CINSEE          PIC X(05)            VALUE SPACES.        
              05 TSEN-CPOSTAL         PIC X(05)            VALUE SPACES.        
              05 TSEN-DATA-PACK.                                                
                 10 TSEN-DATA            OCCURS 27.                             
                    15 TSEN-CFAM            PIC  X(5).                          
      *                                                                         
       01  ZONE-TS-SORTIE.                                                      
           03 TSSO-ENTETE.                                                      
              05 TSSO-NOMPROG         PIC X(08)            VALUE SPACES.        
              05 TSSO-REQUETE         PIC X(04)            VALUE SPACES.        
              05 TSSO-CODRET          PIC 9(04)            VALUE ZEROES.        
              05 TSSO-NBITEM          PIC 9(04)            VALUE ZEROES.        
              05 TSSO-LGDONNEES       PIC 9(04)            VALUE ZEROES.        
           03 TSSO-DATA.                                                        
              05 TSSO-CMODDEL         PIC X(03)            VALUE SPACES.        
              05 TSSO-ZONEPAGE.                                                 
                 06 TSSO-CZONLIV         PIC X(05)         VALUE SPACES.        
                 06 TSSO-HAUT            OCCURS 20.                             
                    11 TSSO-CPLAGE          PIC X(02).                          
                    11 TSSO-LPLAGE          PIC X(10).                          
                 06 TSSO-DONNEES         OCCURS 30.                             
                    11 TSSO-DATE            PIC  X(08).                         
                    11 TSSO-INFO            OCCURS 20.                          
                       16 TSSO-QUOTA           PIC  9(05).                      
                 06 TSSO-CEQUIPE         PIC X(05)         VALUE SPACES.        
      *                                                                         
      **** GESTION TS-EGV43 PAGINATION    : ECRAN CONSULT. QUOTA                
       01  TS-DATA.                                                             
           02 TS-CMODDEL              PIC X(03).                                
           02 TS-ZONE                 OCCURS 2.                                 
              03 TS-CZONLIV              PIC X(05).                             
              03 TS-HAUT                 OCCURS 20.                             
                 04 TS-CPLAGE               PIC X(02).                          
                 04 TS-LPLAGE               PIC X(10).                          
              03 TS-DONNEES                 OCCURS 30.                          
                 04 TS-DATE                    PIC X(08).                       
                 04 TS-QUOTA-PACK              OCCURS 20.                       
                    05 TS-QUOTA                   PIC 9(05).                    
              03 TS-CEQUIPE         PIC X(05)         VALUE SPACES.             
      *                                                                         
       01  FILLER                     PIC X(15) VALUE '*** MAP VT43 **'.        
           COPY EVT43 REPLACING EVT43I BY Z-MAP.                                
       01  FILLER                     PIC X(15) VALUE '*** COMMAREA **'.        
 MH        COPY COMMGV00.                                                       
           COPY SYKWCOMM.                                                       
           COPY SYKWSTAR.                                                       
       01  COMMGV23.                                                            
           05 COMM-NOM-TS             PIC X(08)            VALUE SPACES.        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 COMM-NB-ITEM            PIC S9(04) COMP      VALUE ZEROES.        
      *--                                                                       
           05 COMM-NB-ITEM            PIC S9(04) COMP-5      VALUE              
                                                                 ZEROES.        
      *}                                                                        
       01  FILLER                     PIC X(15) VALUE 'Z-COMMAREA-LINK'.        
       01  Z-COMMAREA-LINK            PIC X(200)           VALUE SPACES.        
           COPY COMMDATC.                                                       
      *                                                                         
       01  FILLER                     PIC X(15) VALUE '** AIDA ZONES *'.        
           COPY SYKWDIV0.                                                       
           COPY SYKWEIB0.                                                       
           COPY SYKWDATH.                                                       
           COPY SYKWCWA0.                                                       
           COPY SYKWTCTU.                                                       
           COPY SYKWECRA.                                                       
           COPY SYKWZINO.                                                       
           COPY SYKWERRO.                                                       
           COPY SYKWZCMD.                                                       
      *                                                                         
       LINKAGE SECTION.                                                         
       01  DFHCOMMAREA.                                                         
           05  FILLER PIC X OCCURS 9096 DEPENDING ON EIBCALEN.                  
           COPY SYKLINKB.                                                       
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
      *****************************************************************         
      *              T R A M E   DU   P R O G R A M M E               *         
      *****************************************************************         
      *                                                                         
       MODULE-GV43                    SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           PERFORM MODULE-ENTREE.                                               
           IF TRAITEMENT                                                        
              PERFORM MODULE-TRAITEMENT                                         
           END-IF.                                                              
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
       FIN-MODULE-GV43. EXIT.                                                   
      *                                                                         
      ******************************************************************        
      *                          E N T R E E                           *        
      ******************************************************************        
      *                                                                         
       MODULE-ENTREE                  SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           PERFORM INIT-HANDLE.                                                 
           PERFORM INIT-ADDRESS.                                                
           PERFORM INIT-USER.                                                   
           PERFORM RECEPTION-MESSAGE.                                           
      *                                                                         
       FIN-MODULE-ENTREE. EXIT.                                                 
      *                                                                         
       E01-COPY SECTION. CONTINUE. COPY SYKCHAND.                               
      *                                                                         
       INIT-USER                      SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           MOVE LOW-VALUE             TO Z-MAP.                                 
           MOVE 'EVT43'               TO NOM-MAP.                               
           MOVE 'EVT43'               TO NOM-MAPSET.                            
           MOVE 'TVT43'               TO NOM-PROG.                              
           MOVE  'VT43'               TO NOM-TACHE.                             
           MOVE  'VT40'               TO NOM-LEVEL-MAX.                         
           MOVE   'NON'               TO DEBUGGIN.                              
           MOVE LENGTH OF Z-COMMAREA  TO LONG-COMMAREA LONG-START.              
           IF LONG-COMMAREA LESS THAN 220                                       
              MOVE 'COMMAREA PLUS PETITE QUE 220'  TO MESS                      
              GO TO ABANDON-TACHE                                               
           END-IF.                                                              
           IF LONG-COMMAREA GREATER THAN 9096                                   
              MOVE 'COMMAREA PLUS GRANDE QUE 9096' TO MESS                      
              GO TO ABANDON-TACHE                                               
           END-IF.                                                              
           IF EIBCALEN NOT = 0                                                  
              MOVE DFHCOMMAREA           TO Z-COMMAREA                          
           ELSE                                                                 
              MOVE SPACES                TO Z-COMMAREA                          
              MOVE 'TVT40'               TO NOM-PROG-XCTL                       
              PERFORM XCTL-NO-COMMAREA                                          
           END-IF.                                                              
      *                                                                         
       FIN-INIT-USER. EXIT.                                                     
      *                                                                         
       E02-COPY SECTION. CONTINUE. COPY SYKCADDR.                               
      *                                                                         
       RECEPTION-MESSAGE              SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           SET  PAS-DE-DEBRANCHEMENT  TO TRUE.                                  
           PERFORM POSIT-ATTR-INITIAL.                                          
      **** AUTRE TACHE                                                          
           IF EIBTRNID NOT = NOM-TACHE                                          
              MOVE CODE-TRAITEMENT-AUTOMATIQUE TO FONCTION                      
              GO TO FIN-RECEPTION-MESSAGE                                       
           END-IF.                                                              
      **** FAST PATH                                                            
           IF EIBTRNID  = Z-COMMAREA-TACHE-JUMP                                 
              MOVE SPACES TO Z-COMMAREA-TACHE-JUMP                              
              MOVE CODE-TRAITEMENT-AUTOMATIQUE TO FONCTION                      
              GO TO FIN-RECEPTION-MESSAGE                                       
           END-IF.                                                              
           PERFORM RECEIVE-MAP.                                                 
           MOVE SPACES                TO MLIBERRI.                              
           MOVE EIBAID                TO WORKAID.                               
      **** TRAITEMENT NORMAL                                                    
           IF TOUCHE-ENTER MOVE CODE-TRAITEMENT-NORMAL   TO FONCTION.           
      **** RETOUR NIVEAU SUPERIEUR                                              
           IF TOUCHE-PF3   MOVE CODE-LEVEL-MAX           TO FONCTION.           
           IF TOUCHE-PF4   MOVE CODE-LEVEL-MAX           TO FONCTION.           
      **** PAGINATION                                                           
      *    PAR DATE PAGINATION SUIVANTE 08                                      
           IF TOUCHE-PF8                                                        
              IF COMM-GV03-ITEM-PAGE < 2                                        
                 MOVE 'PF8'                 TO COMM-GV03-T-FONCTION             
                 MOVE CODE-SUIVANTE         TO FONCTION                         
              END-IF                                                            
           END-IF.                                                              
      *    PAGINATION PRECEDENTE 07                                             
           IF TOUCHE-PF7                                                        
              IF COMM-GV03-ITEM-PAGE > 1                                        
                 MOVE 'PF7'                 TO COMM-GV03-T-FONCTION             
                 MOVE CODE-PRECEDENTE       TO FONCTION                         
              END-IF                                                            
           END-IF.                                                              
      *    MODE DELIVRANCE    PAGINATION SUIVANTE 10                            
           IF TOUCHE-PF10                                                       
              IF COMM-GV03-ITEM < COMM-GV03-DELIVR-MAX                          
                 MOVE 'PF10'                TO COMM-GV03-T-FONCTION             
                 MOVE CODE-SUIVANTE         TO FONCTION                         
              END-IF                                                            
           END-IF.                                                              
      *    MODE DELIVRANCE    PAGINATION PRECEDENTE 09                          
           IF TOUCHE-PF9                                                        
              IF COMM-GV03-ITEM > 1                                             
                 MOVE 'PF9'                 TO COMM-GV03-T-FONCTION             
                 MOVE CODE-PRECEDENTE       TO FONCTION                         
              END-IF                                                            
           END-IF.                                                              
      *    PERIMETRE LIVRAISON    PAGINATION SUIVANTE 12                        
           IF TOUCHE-PF12                                                       
              IF COMM-GV03-ITEM-OCCURS < COMM-GV03-OCCURS-MAX                   
                 MOVE 'PF12'                TO COMM-GV03-T-FONCTION             
                 MOVE CODE-SUIVANTE         TO FONCTION                         
              END-IF                                                            
           END-IF.                                                              
      *    PERIMETRE LIVRAISON    PAGINATION PRECEDENTE 11                      
           IF TOUCHE-PF11                                                       
              IF COMM-GV03-ITEM-OCCURS > 1                                      
                 MOVE 'PF11'                TO COMM-GV03-T-FONCTION             
                 MOVE CODE-PRECEDENTE       TO FONCTION                         
              END-IF                                                            
           END-IF.                                                              
           IF TOUCHE-PF20                                                       
              IF COMM-IT-PLAGES  < 1                                            
                 MOVE 'PF20'                 TO COMM-GV03-T-FONCTION            
                 MOVE CODE-SUIVANTE         TO FONCTION                         
              END-IF                                                            
           END-IF.                                                              
      *    PAGINATION PRECEDENTE 07                                             
           IF TOUCHE-PF19                                                       
              IF COMM-IT-PLAGES  > 0                                            
                 MOVE 'PF19'                 TO COMM-GV03-T-FONCTION            
                 MOVE CODE-PRECEDENTE       TO FONCTION                         
              END-IF                                                            
           END-IF.                                                              
      **** DEBRANCHEMENT FENETRES DE VENTE                                      
           IF TOUCHE-PF14  SET  CONSULTATION-STOCK             TO TRUE.         
           IF TOUCHE-PF15  SET  CONSULTATION-CARACTERISTIQUE   TO TRUE.         
           IF TOUCHE-PF16  SET  CONSULTATION-COMMENTAIRE       TO TRUE.         
           IF TOUCHE-PF18  SET  CONSULTATION-ADRESSE           TO TRUE.         
      *    IF TOUCHE-PF20  SET  CONSULTATION-ADR-BENEFICIAIRE  TO TRUE.         
      **** ZONE COMMANDE                                                        
           IF MZONCMDI NOT = LOW-VALUE AND SPACES                               
              MOVE 1                     TO DISPATCH-LEVEL                      
              PERFORM RECEPTION-ZONE-COMMANDE                                   
           END-IF.                                                              
      **** MAPFAIL                                                              
           IF ECRAN-MAPFAIL                                                     
              MOVE LOW-VALUE             TO Z-MAP                               
              MOVE CODE-TRAITEMENT-AUTOMATIQUE TO FONCTION                      
           END-IF.                                                              
           PERFORM POSIT-ATTR-INITIAL.                                          
      *                                                                         
       FIN-RECEPTION-MESSAGE. EXIT.                                             
      *                                                                         
       E03-COPY SECTION. CONTINUE. COPY SYKCRECV.                               
       E04-COPY SECTION. CONTINUE. COPY SYKCZCMD.                               
      *                                                                         
       POSIT-ATTR-INITIAL             SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           MOVE NOR-PRO-FSET          TO MLIBERRA.                              
           MOVE DFH-RED               TO MLIBERRC.                              
           MOVE DFH-REVRS             TO MLIBERRH.                              
      *                                                                         
       FIN-POSIT-ATTR-INITIAL. EXIT.                                            
      *                                                                         
      ******************************************************************        
      *                      T R A I T E M E N T                       *        
      ******************************************************************        
      *                                                                         
       MODULE-TRAITEMENT              SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           IF TRAITEMENT-NORMAL                                                 
              PERFORM MODULE-TRAITEMENT-NORMAL                                  
           END-IF.                                                              
           IF TRAITEMENT-AUTOMATIQUE                                            
              PERFORM MODULE-TRAITEMENT-AUTOMATIQUE                             
           END-IF.                                                              
      *                                                                         
       FIN-MODULE-TRAITEMENT. EXIT.                                             
      *                                                                         
      ******************************************************************        
      *          T R A I T E M E N T   A U T O M A T I Q U E           *        
      ******************************************************************        
      *                                                                         
       MODULE-TRAITEMENT-AUTOMATIQUE  SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      *    EXEC CICS DELAY REQID ('POSI FIC') END-EXEC                          
           IF COMM-GV03-FLAG-PAGINATION NOT = 'O'                               
              INITIALIZE  COMM-GV03-TRI  COMM-GV03-TABLE-PASSAGE-TRI            
                 COMM-GV03-TABLE-PACK    COMM-GV03-ETAT-FETCH                   
                 COMM-GV03-ADR   COMMGV23                                       
                 MOVE 1 TO COMM-IT-PLAGES-MAX                                   
              PERFORM DELETE-TS-EGV03                                           
              MOVE 'O'                   TO COMM-GV03-FLAG-PAGINATION           
              MOVE  0                    TO COMM-GV03-ETAT-FETCH                
              MOVE  0                    TO J                                   
      ******* TRAITEMENT DES ADRESSES                                           
              IF PRESENCE-ADRESSE-TYPE-A                                        
                 MOVE 'A'                   TO COMM-GV03-WTYPEADR (1)           
                 MOVE COMM-GV00-CPOSTAL-A   TO COMM-GV03-CPOSTAL  (1)           
                 MOVE COMM-GV00-CINSEE-A    TO COMM-GV03-CINSEE   (1)           
              END-IF                                                            
              IF PRESENCE-ADRESSE-TYPE-B                                        
                 MOVE 'B'                   TO COMM-GV03-WTYPEADR (2)           
                 MOVE COMM-GV00-CPOSTAL-B   TO COMM-GV03-CPOSTAL  (2)           
                 MOVE COMM-GV00-CINSEE-B    TO COMM-GV03-CINSEE   (2)           
              END-IF                                                            
              IF PRESENCE-ADRESSE-TYPE-C                                        
                 MOVE 'C'                   TO COMM-GV03-WTYPEADR (3)           
                 MOVE COMM-GV00-CPOSTAL-C   TO COMM-GV03-CPOSTAL  (3)           
                 MOVE COMM-GV00-CINSEE-C    TO COMM-GV03-CINSEE   (3)           
              END-IF                                                            
      *                                                                         
              MOVE 0                     TO J                                   
              PERFORM POSITIONNEMENT-FICHIER                                    
              MOVE 1                     TO COMM-GV03-ITEM                      
              MOVE 1                     TO COMM-GV03-ITEM-PAGE                 
              MOVE 1                     TO COMM-GV03-ITEM-OCCURS               
              PERFORM READ-TS-EGV03                                             
              IF TS-CZONLIV (2) = SPACES OR LOW-VALUE                           
              THEN MOVE 1                     TO COMM-GV03-OCCURS-MAX           
              ELSE MOVE 2                     TO COMM-GV03-OCCURS-MAX           
              END-IF                                                            
           ELSE                                                                 
              EVALUATE COMM-GV03-T-FONCTION                                     
                 WHEN 'PF8'                                                     
                    ADD  1                     TO COMM-GV03-ITEM-PAGE           
                 WHEN 'PF7'                                                     
                    SUBTRACT 1               FROM COMM-GV03-ITEM-PAGE           
                 WHEN 'PF10'                                                    
                    ADD  1                     TO COMM-GV03-ITEM                
                    MOVE 1                     TO COMM-GV03-ITEM-PAGE           
                    MOVE 1                     TO COMM-GV03-ITEM-OCCURS         
                 WHEN 'PF9'                                                     
                    SUBTRACT 1               FROM COMM-GV03-ITEM                
                    MOVE 1                     TO COMM-GV03-ITEM-PAGE           
                    MOVE 1                     TO COMM-GV03-ITEM-OCCURS         
                 WHEN 'PF12'                                                    
                    ADD  1                     TO COMM-GV03-ITEM-OCCURS         
                 WHEN 'PF11'                                                    
                    SUBTRACT 1 FROM COMM-GV03-ITEM-OCCURS                       
                 WHEN 'PF19'                                                    
                    SUBTRACT 1 FROM COMM-IT-PLAGES                              
                 WHEN 'PF20'                                                    
                    ADD      1 TO   COMM-IT-PLAGES                              
              END-EVALUATE                                                      
              PERFORM READ-TS-EGV03                                             
              IF COMM-GV03-T-FONCTION = 'PF10' OR 'PF9'                         
                 IF TS-CZONLIV (2) = SPACES OR LOW-VALUE                        
                 THEN MOVE 1                     TO COMM-GV03-OCCURS-MAX        
                 ELSE MOVE 2                     TO COMM-GV03-OCCURS-MAX        
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
           MOVE SPACES                TO COMM-GV03-T-FONCTION                   
           PERFORM REMPLISSAGE-FORMAT-ECRAN                                     
           PERFORM SAVE-POSITIONNEMENT.                                         
      *                                                                         
       FIN-MODULE-TRAITEMENT-AUTOMAT. EXIT.                                     
      *                                                                         
       POSITIONNEMENT-FICHIER         SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           PERFORM VARYING ICOMM FROM 1 BY 1                                    
      *{ reorder-array-condition 1.1                                            
      *              UNTIL KOM-LIGNE-INEXISTANTE (ICOMM)                        
      *                 OR ICOMM > COMM-GV01-NLIGNE-MAX                         
      *--                                                                       
                     UNTIL ICOMM > COMM-GV01-NLIGNE-MAX                         
                        OR KOM-LIGNE-INEXISTANTE (ICOMM)                        
      *}                                                                        
              IF KOM-LIGNE-REPRISE (ICOMM) OR KOM-LIGNE-VENTE(ICOMM)            
                 IF KOM-NON-TOPE-LIVRE (ICOMM) AND                              
                    NOT KOM-LIGNE-ANNULEE (ICOMM)                               
                    PERFORM READ-TSGV01                                         
                    MOVE Z-INOUT               TO TS01-DONNEES                  
                    IF NOT (COMM-GV00-MAGASIN AND TS01-EN-LIVRAISON)            
                       MOVE TS01-CMODDEL          TO WCMODDEL                   
                       IF WCMODDEL(1:2) = 'LD' OR 'RD' OR 'ED'                  
                          IF WCMODDEL(1:2) = 'RD' AND                           
                             WCMODDEL(3:1) NOT = ' '                            
                             MOVE 'LD'               TO WCMODDEL(1:2)           
                          END-IF                                                
                          IF WCMODDEL = 'RD '                                   
                             MOVE 'ED '              TO WCMODDEL                
                          END-IF                                                
                          MOVE WCMODDEL              TO TS01-CMODDEL            
                          PERFORM REMPLISSAGE-TABLE-PASSAGE                     
                       ELSE                                                     
                          PERFORM REMPLISSAGE-TABLE-PASSAGE                     
                       END-IF                                                   
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
           MOVE SPACES                TO COMM-GV03-TABLE-PASSAGE-TRI            
                                         COMM-GV03-TRI                          
           MOVE 0                     TO I J K                                  
      *                                                                         
      **** TRI DE LA TABLE INTERNE                                              
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > 9                            
              IF COMM-GV03-T-CLE (I) = SPACES OR LOW-VALUE                      
                 MOVE HIGH-VALUE            TO COMM-GV03-T-CLE (I)              
              END-IF                                                            
           END-PERFORM.                                                         
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > 9                            
              COMPUTE J = I + 1                                                 
              PERFORM VARYING J FROM J BY 1 UNTIL J > 9                         
                IF COMM-GV03-TAB-PASSE(I)  >  COMM-GV03-TAB-PASSE(J)            
                   MOVE COMM-GV03-TAB-PASSE(I) TO COMM-GV03-TRI                 
                   MOVE COMM-GV03-TAB-PASSE(J) TO COMM-GV03-TAB-PASSE(I)        
                   MOVE COMM-GV03-TRI          TO COMM-GV03-TAB-PASSE(J)        
                END-IF                                                          
              END-PERFORM                                                       
           END-PERFORM.                                                         
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > 9                            
              IF COMM-GV03-T-CLE (I)  = HIGH-VALUE                              
                 MOVE SPACES                TO COMM-GV03-T-CLE (I)              
              END-IF                                                            
           END-PERFORM.                                                         
      *                                                                         
      **** PASSAGE D'UN ITEM DE TABLE AU MODULE MGV23 PAR LINK. PUIS            
      **** ECRITURE DE 'ZONE-TS-SORTIE' DANS LA TS03. 1 ITEM = 30 LIGNES        
           INITIALIZE                    COMM-GV03-USER                         
           MOVE 0                     TO I J                                    
           PERFORM VARYING I FROM 1 BY 1 UNTIL ( I > 9 )                        
                        OR ( COMM-GV03-T-CLE (I) = LOW-VALUE OR SPACES )        
      ******* ITEM = CMODDEL / CINSEE / CPOSTAL                                 
              MOVE 'TVT43'               TO TSEN-NOMPROG                        
              MOVE SPACES                TO TSEN-REQUETE                        
              MOVE 0                     TO TSEN-CODRET                         
              MOVE 1                     TO TSEN-NBITEM COMM-NB-ITEM            
              MOVE 93                    TO TSEN-LGDONNEES                      
              MOVE COMM-GV00-NSOCLIVR    TO TSEN-NSOCIETE                       
              MOVE COMM-GV00-NDEPOT      TO TSEN-NLIEU                          
              MOVE COMM-GV03-T-CMODDEL (I)  TO TSEN-CMODDEL                     
              MOVE COMM-GV03-T-CINSEE (I)   TO TSEN-CINSEE                      
              MOVE COMM-GV03-T-CPOSTAL (I)  TO TSEN-CPOSTAL                     
              MOVE COMM-GV03-T-DATA (I)     TO TSEN-DATA-PACK                   
              PERFORM DELETE-TS-MGV23                                           
      ******* ECRITURE TSGV23                                                   
              PERFORM WRITE-TS-MGV23                                            
              MOVE IDENT-TS              TO COMM-NOM-TS                         
              IF (TSEN-CMODDEL = 'ED ' OR 'RD ') OR                             
                 (COMM-GV03-WTYPEADR (1) NOT = SPACES AND LOW-VALUE)            
      ********** LINK MGV23                                                     
                 EXEC CICS LINK PROGRAM ('MGV23')                               
                                COMMAREA(COMMGV23)                              
                                LENGTH(10)                                      
                                NOHANDLE                                        
                 END-EXEC                                                       
      ********** LECTURE TSGV23                                                 
                 PERFORM READ-TS-MGV23                                          
                 IF TSSO-CODRET = '1000'                                        
                    MOVE 'PROBLEME DANS MGV23' TO MESS                          
                    GO TO ABANDON-TACHE                                         
                 END-IF                                                         
              ELSE                                                              
                 INITIALIZE TSSO-ZONEPAGE                                       
              END-IF                                                            
      ******* ECRITURE TS PAGINATION TSEGV03                                    
              IF I = 1                                                          
                 MOVE 0                     TO COMM-GV03-ITEM                   
                 MOVE TSSO-CMODDEL          TO TS-CMODDEL                       
                 MOVE TSSO-CZONLIV          TO TS-CZONLIV (1)                   
                 MOVE TSSO-CEQUIPE          TO TS-CEQUIPE (1)                   
                 MOVE TSSO-ZONEPAGE         TO TS-ZONE (1)                      
                 PERFORM WRITE-TS-EGV03                                         
              ELSE                                                              
                 IF TS-CMODDEL = TSSO-CMODDEL                                   
                    IF TSSO-CZONLIV NOT = TS-CZONLIV (1)                        
                                      AND TS-CZONLIV (2)                        
                       MOVE TSSO-CZONLIV          TO TS-CZONLIV (2)             
                       MOVE TSSO-CEQUIPE          TO TS-CEQUIPE (2)             
                       MOVE TSSO-ZONEPAGE         TO TS-ZONE (2)                
                       PERFORM REWRITE-TS-EGV03                                 
                    END-IF                                                      
                 ELSE                                                           
                    INITIALIZE TS-DATA                                          
                    MOVE TSSO-CMODDEL          TO TS-CMODDEL                    
                    MOVE TSSO-CZONLIV          TO TS-CZONLIV (1)                
                    MOVE TSSO-CEQUIPE          TO TS-CEQUIPE (1)                
                    MOVE TSSO-ZONEPAGE         TO TS-ZONE (1)                   
                    PERFORM WRITE-TS-EGV03                                      
                 END-IF                                                         
              END-IF                                                            
              PERFORM DELETE-TS-MGV23                                           
           END-PERFORM.                                                         
           MOVE COMM-GV03-ITEM        TO COMM-GV03-DELIVR-MAX.                  
      *                                                                         
       FIN-POSITIONNEMENT-FICHIER.    EXIT.                                     
      *                                                                         
       REMPLISSAGE-TABLE-PASSAGE      SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           IF KOM-LIGNE-REPRISE (ICOMM)                                         
              IF COMM-GV03-WTYPEADR (3) NOT = SPACES AND LOW-VALUE              
                 MOVE COMM-GV03-CPOSTAL (3) TO COMM-GV03-T-CPOSTAL-T            
                 MOVE COMM-GV03-CINSEE  (3) TO COMM-GV03-T-CINSEE-T             
              ELSE                                                              
                 MOVE COMM-GV03-CPOSTAL (1) TO COMM-GV03-T-CPOSTAL-T            
                 MOVE COMM-GV03-CINSEE  (1) TO COMM-GV03-T-CINSEE-T             
              END-IF                                                            
           ELSE                                                                 
              IF COMM-GV03-WTYPEADR (2) NOT = SPACES AND LOW-VALUE              
                 MOVE COMM-GV03-CPOSTAL (2) TO COMM-GV03-T-CPOSTAL-T            
                 MOVE COMM-GV03-CINSEE  (2) TO COMM-GV03-T-CINSEE-T             
              ELSE                                                              
                 MOVE COMM-GV03-CPOSTAL (1) TO COMM-GV03-T-CPOSTAL-T            
                 MOVE COMM-GV03-CINSEE  (1) TO COMM-GV03-T-CINSEE-T             
              END-IF                                                            
           END-IF.                                                              
           MOVE TS01-CMODDEL          TO COMM-GV03-T-CMODDEL-T                  
           PERFORM VARYING J FROM 1 BY 1 UNTIL J > 9                            
              IF COMM-GV03-T-CLE (J) = SPACES OR LOW-VALUE                      
                 MOVE COMM-GV03-T-CLE-T     TO COMM-GV03-T-CLE (J)              
                 MOVE TS01-CFAM             TO COMM-GV03-T-CFAM (J , 1)         
                 MOVE 10                    TO J                                
              ELSE                                                              
                 IF COMM-GV03-T-CLE (J) = COMM-GV03-T-CLE-T                     
                    PERFORM VARYING K FROM 1 BY 1 UNTIL K > 27                  
                       IF COMM-GV03-T-CFAM (J , K) = SPACES OR LOW-VALUE        
                          MOVE TS01-CFAM    TO COMM-GV03-T-CFAM (J , K)         
                          MOVE 28           TO K                                
                       END-IF                                                   
                    END-PERFORM                                                 
                    MOVE 10                    TO J                             
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
       F-REMPLISSAGE-TABLE-PASSAGE.          EXIT.                              
      *                                                                         
      ******************************************************************        
      *          T R A I T E M E N T   A U T O M A T I Q U E           *        
      ******************************************************************        
      *                                                                         
       REMPLISSAGE-FORMAT-ECRAN       SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
            PERFORM REMPLISSAGE-ZONES-OBLIGATOIRES.                             
            PERFORM REMPLISSAGE-ZONES-PROTEGEES.                                
      *                                                                         
       FIN-REMPLISSAGE-FORMAT-ECRAN. EXIT.                                      
      *                                                                         
       REMPLISSAGE-ZONES-OBLIGATOIRES SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           MOVE COMM-CICS-APPLID      TO MCICSI                                 
           MOVE COMM-CICS-NETNAM      TO MNETNAMI                               
           MOVE COMM-CICS-TRANSA      TO MCODTRAI                               
           MOVE EIBTRMID              TO MSCREENI                               
           MOVE COMM-DATE-JJ-MM-SSAA  TO MDATJOUI                               
           MOVE Z-TIMER-TIMJOU        TO MTIMJOUI                               
           MOVE COMM-GV03-ITEM-PAGE   TO COMM-GV03-NPAGE                        
           MOVE COMM-GV03-NPAGE       TO MWPAGEI                                
           IF COMM-GV03-ITEM-PAGE = 1                                           
              MOVE                                                              
           'F8/7 PAGINE PAR DATE, F10/9 DELIVRANCE, F12/11 PERIMETRE'           
                                         TO MLIBERRI                            
           ELSE                                                                 
              MOVE 'DERNIERE PAGE'       TO MLIBERRI                            
           END-IF.                                                              
      *                                                                         
       FIN-REMP-ZONES-OBL.            EXIT.                                     
      *                                                                         
       REMPLISSAGE-ZONES-PROTEGEES    SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           MOVE TS-CMODDEL                             TO MCMODDELI             
           MOVE TS-CZONLIV(COMM-GV03-ITEM-OCCURS)      TO MCZONLIVI             
           MOVE TS-CEQUIPE(COMM-GV03-ITEM-OCCURS)      TO MCEQUIPEI             
           COMPUTE  L = COMM-IT-PLAGES * 10 + 1                                 
           MOVE TS-CPLAGE (COMM-GV03-ITEM-OCCURS , L)      TO MCPLAG1I          
           MOVE TS-CPLAGE (COMM-GV03-ITEM-OCCURS , L + 1)  TO MCPLAG2I          
           MOVE TS-CPLAGE (COMM-GV03-ITEM-OCCURS , L + 2)  TO MCPLAG3I          
           MOVE TS-CPLAGE (COMM-GV03-ITEM-OCCURS , L + 3)  TO MCPLAG4I          
           MOVE TS-CPLAGE (COMM-GV03-ITEM-OCCURS , L + 4)  TO MCPLAG5I          
           MOVE TS-CPLAGE (COMM-GV03-ITEM-OCCURS , L + 5)  TO MCPLAG6I          
           MOVE TS-CPLAGE (COMM-GV03-ITEM-OCCURS , L + 6)  TO MCPLAG7I          
           MOVE TS-CPLAGE (COMM-GV03-ITEM-OCCURS , L + 7)  TO MCPLAG8I          
           MOVE TS-CPLAGE (COMM-GV03-ITEM-OCCURS , L + 8)  TO MCPLAG9I          
           MOVE TS-CPLAGE (COMM-GV03-ITEM-OCCURS , L + 9)  TO MCPLAG0I          
           MOVE TS-LPLAGE (COMM-GV03-ITEM-OCCURS , L)      TO MLPLAG1I          
           MOVE TS-LPLAGE (COMM-GV03-ITEM-OCCURS , L + 1)  TO MLPLAG2I          
           MOVE TS-LPLAGE (COMM-GV03-ITEM-OCCURS , L + 2)  TO MLPLAG3I          
           MOVE TS-LPLAGE (COMM-GV03-ITEM-OCCURS , L + 3)  TO MLPLAG4I          
           MOVE TS-LPLAGE (COMM-GV03-ITEM-OCCURS , L + 4)  TO MLPLAG5I          
           MOVE TS-LPLAGE (COMM-GV03-ITEM-OCCURS , L + 5)  TO MLPLAG6I          
           MOVE TS-LPLAGE (COMM-GV03-ITEM-OCCURS , L + 6)  TO MLPLAG7I          
           MOVE TS-LPLAGE (COMM-GV03-ITEM-OCCURS , L + 7)  TO MLPLAG8I          
           MOVE TS-LPLAGE (COMM-GV03-ITEM-OCCURS , L + 8)  TO MLPLAG9I          
           MOVE TS-LPLAGE (COMM-GV03-ITEM-OCCURS , L + 9)  TO MLPLAG0I          
      *                                                                         
           IF COMM-GV03-ITEM-PAGE = 1                                           
              PERFORM VARYING I FROM 1 BY 1 UNTIL I > 15                        
                 MOVE I                     TO J                                
                 PERFORM REMPLISSAGE-LIGNE-DATE                                 
              END-PERFORM                                                       
           ELSE                                                                 
              PERFORM VARYING I FROM 16 BY 1 UNTIL I > 30                       
                 COMPUTE J = I - 15                                             
                 PERFORM REMPLISSAGE-LIGNE-DATE                                 
              END-PERFORM                                                       
           END-IF.                                                              
      *                                                                         
       FIN-REMP-ZONES-PROT. EXIT.                                               
      *                                                                         
       REMPLISSAGE-LIGNE-DATE         SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           PERFORM TRAITEMENT-DATE-QUOTA                                        
           MOVE TS-QUOTA (COMM-GV03-ITEM-OCCURS, I , L) TO ZQUOTA               
           MOVE ZQUOTA                TO MQUOTA1I (J)                           
           MOVE TS-QUOTA (COMM-GV03-ITEM-OCCURS, I , L + 1) TO ZQUOTA           
           MOVE ZQUOTA                TO MQUOTA2I (J)                           
           MOVE TS-QUOTA (COMM-GV03-ITEM-OCCURS, I , L + 2) TO ZQUOTA           
           MOVE ZQUOTA                TO MQUOTA3I (J)                           
           MOVE TS-QUOTA (COMM-GV03-ITEM-OCCURS, I , L + 3) TO ZQUOTA           
           MOVE ZQUOTA                TO MQUOTA4I (J)                           
           MOVE TS-QUOTA (COMM-GV03-ITEM-OCCURS, I , L + 4) TO ZQUOTA           
           MOVE ZQUOTA                TO MQUOTA5I (J)                           
           MOVE TS-QUOTA (COMM-GV03-ITEM-OCCURS, I , L + 5) TO ZQUOTA           
           MOVE ZQUOTA                TO MQUOTA6I (J)                           
           MOVE TS-QUOTA (COMM-GV03-ITEM-OCCURS, I , L + 6) TO ZQUOTA           
           MOVE ZQUOTA                TO MQUOTA7I (J)                           
           MOVE TS-QUOTA (COMM-GV03-ITEM-OCCURS, I , L + 7) TO ZQUOTA           
           MOVE ZQUOTA                TO MQUOTA8I (J)                           
           MOVE TS-QUOTA (COMM-GV03-ITEM-OCCURS, I , L + 8) TO ZQUOTA           
           MOVE ZQUOTA                TO MQUOTA9I (J)                           
           MOVE TS-QUOTA (COMM-GV03-ITEM-OCCURS, I , L + 9) TO ZQUOTA           
           MOVE ZQUOTA                TO MQUOTA0I (J).                          
      *                                                                         
       FIN-REMPLISSAGE-LIGNE-DATE.    EXIT.                                     
      *                                                                         
       TRAITEMENT-DATE-QUOTA          SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** CONVERSION DE LA DATE DE JJMMSSAA EN JJ/MM/SSAA                      
      **** ET AFFICHAGE DU LIBELLE DU JOUR                                      
           MOVE TS-DATE (COMM-GV03-ITEM-OCCURS, I)  TO GFJJMMSSAA               
           MOVE '1'                      TO GFDATA                              
           MOVE COMM-DATC-LONG-COMMAREA  TO LONG-COMMAREA-LINK                  
           MOVE Z-COMMAREA-TETDATC       TO Z-COMMAREA-LINK                     
           MOVE 'TETDATC'                TO NOM-PROG-LINK                       
           PERFORM LINK-PROG                                                    
           MOVE Z-COMMAREA-LINK       TO Z-COMMAREA-TETDATC.                    
           IF NOT GFVDAT = '1'                                                  
              INITIALIZE                 MDATEI (J)  MLIBCI (J)                 
           ELSE                                                                 
              MOVE GFJMSA-5              TO MDATEI (J)                          
              IF GFSMN-LIB-C = 'DIM'                                            
                 INITIALIZE                 MLIBCI (J)                          
              ELSE                                                              
                 MOVE GFSMN-LIB-C        TO MLIBCI (J)                          
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
       FIN-TRAITEMENT-DATE-QUOTA. EXIT.                                         
      *                                                                         
       SAVE-POSITIONNEMENT            SECTION.                                  
       FIN-SAVE-POSITIONNEMENT.       EXIT.                                     
      *                                                                         
       MODULE-TRAITEMENT-NORMAL       SECTION.                                  
       FIN-MODULE-TRAITEMENT-NORM.    EXIT.                                     
      *                                                                         
      *****************************************************************         
      *                          S O R T I E                          *         
      *****************************************************************         
      *                                                                         
       MODULE-SORTIE                  SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           IF TRAITEMENT-AUTOMATIQUE  PERFORM SORTIE-AFFICHAGE-FORMAT.          
           IF NOT OK                  PERFORM SORTIE-ERREUR.                    
           IF DEMANDE-DE-DEBRANCHEMENT PERFORM SORTIE-AUTRE-TRANSACTION.        
           IF TRAITEMENT-NORMAL       PERFORM SORTIE-SUITE.                     
           IF LEVEL-MAX OR JUMP       PERFORM SORTIE-LEVEL-MAX.                 
           IF ERREUR-MANIPULATION     PERFORM SORTIE-ERREUR-MANIP.              
           IF HELP                    PERFORM SORTIE-HELP.                      
      **** ABANDON                                                              
           MOVE 'ERREUR CODE FONCTION DANS MODULE SORTIE' TO MESS               
           GO TO ABANDON-TACHE.                                                 
      *                                                                         
       FIN-MODULE-SORTIE. EXIT.                                                 
      *                                                                         
       SORTIE-AFFICHAGE-FORMAT        SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           MOVE CURSEUR               TO MZONCMDL                               
           PERFORM SEND-MAP                                                     
           MOVE SPACES                TO Z-COMMAREA-TACHE-JUMP.                 
           MOVE NOM-TACHE             TO NOM-TACHE-RETOUR.                      
           PERFORM RETOUR-COMMAREA.                                             
      *                                                                         
       FIN-SORTIE-AFFICHAGE-FORMAT. EXIT.                                       
      *                                                                         
       SORTIE-ERREUR                  SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** AFFICHAGE DE LA MAP EN ERREUR, EN CONFIRMATION                       
      **** ET RETURN AU MEME PROGRAMME                                          
           PERFORM SEND-MAP-SORTIE-ERREUR.                                      
           MOVE NOM-TACHE             TO NOM-TACHE-RETOUR.                      
           PERFORM RETOUR-COMMAREA.                                             
      *                                                                         
       FIN-SORTIE-ERREUR. EXIT.                                                 
      *                                                                         
       SORTIE-AUTRE-TRANSACTION       SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           PERFORM DELETE-TS-EGV03                                              
           INITIALIZE                    COMM-GV03-APPLI                        
           MOVE COMM-GV00-DEBRANCHEMENT  TO  NOM-PROG-XCTL                      
           PERFORM XCTL-PROG-COMMAREA.                                          
      *                                                                         
       FIN-SORTIE-AUTRE-TRANSACTION. EXIT.                                      
      *                                                                         
       SORTIE-SUITE                   SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** AFFICHAGE DE LA MAP SUIVANTE ET RETURN AU PROGRAMME SUIVANT          
           PERFORM SEND-MAP-DATA-ONLY.                                          
           MOVE EIBTRNID              TO NOM-TACHE-RETOUR.                      
           PERFORM RETOUR-COMMAREA.                                             
      *                                                                         
       FIN-SORTIE-SUITE. EXIT.                                                  
      *                                                                         
       SORTIE-LEVEL-MAX               SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** RETOUR APRES PF3 OU PF4 AU LEVEL SUPERIEUR                           
           PERFORM DELETE-TS-EGV03                                              
           INITIALIZE                    COMM-GV03-APPLI                        
           MOVE COMM-GV00-CPROG       TO NOM-PROG-XCTL                          
           PERFORM XCTL-PROG-COMMAREA.                                          
      *                                                                         
       FIN-SORTIE-LEVEL-MAX. EXIT.                                              
      *                                                                         
       SORTIE-ERREUR-MANIP            SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** SORTIE ERREUR MANIPULATION                                           
           MOVE 'K017 : ERREUR MANIPULATION' TO MLIBERRI.                       
           MOVE 'K017'                       TO COM-CODERR.                     
           EVALUATE WORKAID                                                     
             WHEN '8'                                                           
                 MOVE 'PAGINATION PAR DATE TERMINEE' TO MLIBERRI                
             WHEN '7'                                                           
                 MOVE 'PAGINATION PAR DATE TERMINEE' TO MLIBERRI                
             WHEN ':'                                                           
                 MOVE 'PAGINATION PAR MODE DE DELIVRANCE TERMINEE'              
                 TO MLIBERRI                                                    
             WHEN '9'                                                           
                 MOVE 'PAGINATION PAR MODE DE DELIVRANCE TERMINEE'              
                 TO MLIBERRI                                                    
             WHEN '�'                                                           
                 MOVE 'PAGINATION PAR PERIMETRE TERMINEE' TO MLIBERRI           
             WHEN '�'                                                           
                 MOVE 'PAGINATION PAR PERIMETRE TERMINEE' TO MLIBERRI           
             WHEN OTHER                                                         
                 MOVE 'K017 : ERREUR MANIPULATION' TO MLIBERRI                  
                 MOVE 'K017'                       TO COM-CODERR                
           END-EVALUATE                                                         
           MOVE CURSEUR               TO MZONCMDL.                              
           PERFORM SEND-MAP-ERREUR-MANIP.                                       
           MOVE NOM-TACHE             TO NOM-TACHE-RETOUR.                      
           PERFORM RETOUR-COMMAREA.                                             
      *                                                                         
       FIN-SORTIE-ERREUR-MANIP. EXIT.                                           
      *                                                                         
       SAVE-SWAP-ATTR                 SECTION.                                  
       FIN-SAVE-SWAP-ATTR.            EXIT.                                     
      *                                                                         
       REST-SWAP-ATTR                 SECTION.                                  
       FIN-REST-SWAP-ATTR.            EXIT.                                     
      *                                                                         
      ******************************************************************        
      *                    M O D U L E S   A I D A                     *        
      ******************************************************************        
      *                                                                         
      *DESCRIPTION DES BRIQUES AIDA CICS                                        
       S01-COPY    SECTION. CONTINUE. COPY SYKCHELP.                            
       S02-COPY    SECTION. CONTINUE. COPY SYKCRTCO.                            
       S03-COPY    SECTION. CONTINUE. COPY SYKCRT00.                            
       S04-COPY    SECTION. CONTINUE. COPY SYKCSMDO.                            
       S05-COPY    SECTION. CONTINUE. COPY SYKCSMEM.                            
       S06-COPY    SECTION. CONTINUE. COPY SYKCSMSE.                            
       S07-COPY    SECTION. CONTINUE. COPY SYKCSM00.                            
       S08-COPY    SECTION. CONTINUE. COPY SYKCXCTL.                            
       S09-COPY    SECTION. CONTINUE. COPY SYKCLINK.                            
       S10-COPY    SECTION. CONTINUE. COPY SYKCSWAP.                            
       S11-COPY    SECTION. CONTINUE. COPY SYKXSWAP.                            
      *DESCRIPTION DES BRIQUES AIDA TEMP                                        
       TEMP01-COPY SECTION. CONTINUE. COPY SYKCTSDE.                            
       TEMP02-COPY SECTION. CONTINUE. COPY SYKCTSRD.                            
       TEMP03-COPY SECTION. CONTINUE. COPY SYKCTSRW.                            
       TEMP04-COPY SECTION. CONTINUE. COPY SYKCTSWR.                            
      *MODULE D'ABANDON                                                         
       99-COPY     SECTION. CONTINUE. COPY SYKCERRO.                            
      *                                                                         
      ******************************************************************        
      *                     G E S T I O N   T S                        *        
      ******************************************************************        
      *                                                                         
      **** GESTION DE LA TSGV23 : TS D'APPEL A MGV23                            
       DELETE-TS-MGV23                SECTION.                                  
      *---------------------------------------                                  
           STRING EIBTRMID 'E' 'GV3'                                            
              DELIMITED BY SIZE        INTO IDENT-TS                            
           END-STRING                                                           
           PERFORM DELETE-TS                                                    
           STRING EIBTRMID 'R' 'GV3'                                            
              DELIMITED BY SIZE        INTO IDENT-TS                            
           END-STRING                                                           
           PERFORM DELETE-TS.                                                   
       FIN-DELETE-TS-MGV23. EXIT.                                               
      *                                                                         
       WRITE-TS-MGV23                 SECTION.                                  
      *---------------------------------------                                  
           STRING EIBTRMID 'E' 'GV3'                                            
              DELIMITED BY SIZE        INTO IDENT-TS                            
           END-STRING                                                           
           MOVE 178                   TO LONG-TS                                
           MOVE 1                     TO RANG-TS                                
           MOVE ZONE-TS-ENTREE        TO Z-INOUT                                
           PERFORM WRITE-TS.                                                    
       FIN-WRITE-TS-MGV23. EXIT.                                                
      *                                                                         
       READ-TS-MGV23                  SECTION.                                  
      *---------------------------------------                                  
           STRING EIBTRMID 'R' 'GV3'                                            
              DELIMITED BY SIZE        INTO IDENT-TS                            
           END-STRING                                                           
      *    MOVE 1897                  TO LONG-TS                                
           MOVE 3517                  TO LONG-TS                                
           MOVE 1                     TO RANG-TS                                
           PERFORM READ-TS                                                      
           MOVE Z-INOUT               TO ZONE-TS-SORTIE.                        
       FIN-READ-TS-MGV23. EXIT.                                                 
      *                                                                         
      **** GESTION TS-EGV03 PAGINATION    : ECRAN CONSULT. QUOTA                
       READ-TS-EGV03                  SECTION.                                  
      *---------------------------------------                                  
           STRING 'GV03' EIBTRMID                                               
              DELIMITED BY SIZE        INTO IDENT-TS                            
           END-STRING                                                           
           MOVE LENGTH OF TS-DATA     TO LONG-TS                                
           MOVE COMM-GV03-ITEM        TO RANG-TS                                
           PERFORM READ-TS                                                      
           MOVE Z-INOUT               TO TS-DATA.                               
       F-READ-TS-EGV03. EXIT.                                                   
      *                                                                         
       WRITE-TS-EGV03                 SECTION.                                  
      *---------------------------------------                                  
           ADD  1                     TO COMM-GV03-ITEM                         
           STRING 'GV03' EIBTRMID                                               
              DELIMITED BY SIZE        INTO IDENT-TS                            
           END-STRING                                                           
           MOVE LENGTH OF TS-DATA     TO LONG-TS                                
           MOVE COMM-GV03-ITEM        TO RANG-TS                                
           MOVE TS-DATA               TO Z-INOUT                                
           PERFORM WRITE-TS.                                                    
       F-WRITE-TS-EGV03. EXIT.                                                  
      *                                                                         
       REWRITE-TS-EGV03               SECTION.                                  
      *---------------------------------------                                  
           STRING 'GV03' EIBTRMID                                               
              DELIMITED BY SIZE        INTO IDENT-TS                            
           END-STRING                                                           
           MOVE LENGTH OF TS-DATA     TO LONG-TS                                
           MOVE COMM-GV03-ITEM        TO RANG-TS                                
           MOVE TS-DATA               TO Z-INOUT                                
           PERFORM REWRITE-TS.                                                  
       F-REWRITE-TS-EGV03. EXIT.                                                
      *                                                                         
       DELETE-TS-EGV03                SECTION.                                  
      *---------------------------------------                                  
           STRING 'GV03' EIBTRMID                                               
              DELIMITED BY SIZE        INTO IDENT-TS                            
           END-STRING                                                           
           MOVE LENGTH OF TS-DATA     TO LONG-TS                                
           PERFORM DELETE-TS.                                                   
       FIN-DELETE-TS-EGV03. EXIT.                                               
      *                                                                         
      **** GESTION DE LA TSGV01 : TS MAJ TABLE RTGV11                           
       READ-TSGV01                    SECTION.                                  
      *---------------------------------------                                  
           MOVE KOM-ITEM (ICOMM)      TO RANG-TS                                
           STRING 'GV01' EIBTRMID                                               
              DELIMITED BY SIZE        INTO IDENT-TS                            
           END-STRING                                                           
           MOVE LENGTH OF TS01-DONNEES   TO LONG-TS                             
           PERFORM READ-TS                                                      
           IF TROUVE                                                            
           THEN MOVE Z-INOUT               TO TS01-DONNEES                      
           ELSE INITIALIZE                    TS01-DONNEES                      
           END-IF.                                                              
       FIN-READ-TSGV01. EXIT.                                                   
