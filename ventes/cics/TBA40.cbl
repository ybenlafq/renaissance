      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *{ Pre-Catalog Suppression_OPTS                                           
      *OPTS(CICS,DLI)                                                           
      *} Pre-Catalog                                                            
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. TBA40.                                                       
       AUTHOR. DSA003.                                                          
      *---------------------------------------------------------------*         
      *    APPLICATION   : BONS D'ACHAT                               *         
      *    PROGRAMME     : GESTION DES CARTES PARAMETRES DES          *         
      *                    PROGRAMMES D'EDITION DE L'APPLICATION      *         
      *                    BONS D'ACHAT                               *         
      *    DESCRIPTION   : CE PROGRAMME A POUR FONCTION DE CREER OU   *         
      *                    DE METTRE A JOUR  UN ENREGISTREMENT DANS   *         
      *                    LE FICHIER DES PARAMETRES D'EXPLOITATION.  *         
      *---------------------------------------------------------------*         
       ENVIRONMENT DIVISION.                                                    
       DATA DIVISION.                                                           
       WORKING-STORAGE SECTION.                                                 
      *                                                                         
      *---------------------------------------------------------------*         
      *    ZONES SPECIFIQUES AU PROGRAMME                             *         
      *---------------------------------------------------------------*         
      *                                                                         
       01  Z-COMMAREA-LINK           PIC X(120)        VALUE SPACES.            
       01  Z-SAVE-PARAM              PIC X(080)        VALUE SPACES.            
      *                                                                         
      *---------------------------------------------------------------*         
      *    LISTE DES COPY                                             *         
      *---------------------------------------------------------------*         
      *                                                                         
      *    COPY  COMMEX00.                                                      
      *                                                                         
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *           COMMAREA DU MODULE DE MAJ DU FICHIER                 *        
      *           DES PARAMETRES D'EXPLOITATION (TEX00)                *        
      *                                                                *        
      *           COMM-EX00-LONG-COMMAREA A UNE VALUE DE +120          *        
      *           Z-COMMAREA-EX00 = 200 C                              *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-EX00-LONG-COMMAREA      PIC S9(4) COMP   VALUE +120.            
      *--                                                                       
       01  COMM-EX00-LONG-COMMAREA      PIC S9(4) COMP-5   VALUE +120.          
      *}                                                                        
      *                                                                 00730918
       01  Z-COMMAREA-EX00.                                             00731018
      *                                 ZONE CLE                        00731709
           05 COMM-EX00-CLE.                                            00731818
      *                                 DATE DE PASSAGE PREVU DU STEP   00731909
              10 COMM-EX00-DATE.                                        00732018
                 15 COMM-EX00-AA        PIC X(02).                      00732218
                 15 COMM-EX00-MM        PIC X(02).                      00732418
                 15 COMM-EX00-JJ        PIC X(02).                      00732618
      *                                 NOM DU PROGRAMME CICS APPELANT  00732709
              10 COMM-EX00-NOMPGM       PIC X(05).                      00732818
      *                                 NUMERO DE TACHE                 00732909
              10 COMM-EX00-NUMTAC       PIC X(02).                      00733018
      *                                 NUMERO DE CARTE                 00733109
              10 COMM-EX00-NUMCAR       PIC X(02).                      00733218
      *                                 CODE MISE A JOUR (G/I/D/R)      00733309
           05 COMM-EX00-CODMAJ          PIC X(01).                      00733418
      *                                 CODE RETOUR (00/99/98)          00733509
           05 COMM-EX00-CODRET          PIC X(02).                      00733618
      *                                 ZONE INUTILISEE                 00733509
           05 COMM-EX00-FILLER1         PIC X(15).                      00733618
      *                                 ZONE PARAMETRE                  00733709
           05 COMM-EX00-ZPARAM          PIC X(80).                      00733818
      *                                 LIBRE                           00733509
           05 COMM-EX00-LIBRE           PIC X(87).                              
           COPY SYKWDIV0.                                                       
           COPY SYKWEIB0.                                                       
           COPY SYKWECRA.                                                       
      ******************************************************* 14-06-89 *        
      * ZONE D'INTERFACE POUR LA GESTION DES ERREURS NON RECOUVRABLES           
      ******************************************************* SYKWERRO *        
       01  Z-ERREUR.                                                            
           05  FILLER                 PIC X(256).                               
           05  Z-ERREUR-TRACE-MESS    PIC X(80).                                
           05  Z-ERREUR-EIBLK         PIC X(66).                                
       01  FILLER.                                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRACE-MESSAGE-LONG PIC S9(4) COMP VALUE +84.                     
      *--                                                                       
           05  TRACE-MESSAGE-LONG PIC S9(4) COMP-5 VALUE +84.                   
      *}                                                                        
           05  TRACE-MESSAGE.                                                   
      *                                     START-BUFFER-ADDRESS                
             10  FILLER         PIC  X      VALUE ''.                          
      *                                     23EME LIGNE 1ERE COLONNE            
             10  FILLER         PIC  X(2)   VALUE '$-'.                         
      *                                     START-FIELD                         
             10  FILLER         PIC  X      VALUE ''.                          
      *                                     ATTRIBUT                            
             10  FILLER         PIC  X      VALUE 'Y'.                          
             10  TRACE-MESS.                                                    
               15  MESS1          PIC  X(15)  VALUE SPACE.                      
               15  MESS           PIC  X(64)  VALUE SPACE.                      
               15  FILLER REDEFINES MESS.                                       
                 20  MESS-ABCODE  PIC  X(4).                                    
                 20  FILL         PIC  X(60).                                   
       EJECT                                                                    
      *                                                                         
       LINKAGE SECTION.                                                         
       01  DFHCOMMAREA.                                                         
           05  FILLER                PIC X(0200).                               
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
       MODULE-BA40                   SECTION.                                   
      *--------------------------------------                                   
      *                                                                         
      *--- RECEPTION COMMAREA                                                   
      *                                                                         
           MOVE DFHCOMMAREA          TO Z-COMMAREA-EX00                         
           MOVE '200'                TO LONG-COMMAREA                           
           IF EIBCALEN  NOT =   LONG-COMMAREA                                   
              MOVE 'EIBCALEN NOT = LONG-COMMAREA' TO MESS                       
              GO TO ABANDON-TACHE                                               
           END-IF                                                               
      *                                                                         
           PERFORM TRAITEMENT-FICHIER-FEX000.                                   
      *                                                                         
       MODULE-SORTIE.                                                           
           MOVE Z-COMMAREA-EX00      TO DFHCOMMAREA                             
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS RETURN END-EXEC                                            
      *--                                                                       
           EXEC CICS RETURN                                                     
           END-EXEC                                                             
      *}                                                                        
           GOBACK.                                                              
      *                                                                         
       FIN-MODULE-BA40. EXIT.                                                   
      *                                                                         
       TRAITEMENT-FICHIER-FEX000     SECTION.                                   
      *--------------------------------------                                   
      *                                                                         
      *--- SAUVEGARDE DE LA ZONE PARAMETRE                                      
      *                                                                         
           MOVE COMM-EX00-ZPARAM     TO Z-SAVE-PARAM                            
      *                                                                         
      *--- LECTURE DU FICHIER DES PARAMETRES D'EXPLOITATION                     
      *                                                                         
           MOVE 'TBA00'              TO COMM-EX00-NOMPGM                        
           MOVE '01'                 TO COMM-EX00-NUMCAR                        
           MOVE 'G'                  TO COMM-EX00-CODMAJ                        
           PERFORM LINK-TEX00                                                   
      *                                                                         
      *--- MISE � JOUR DU FICHIER DES PARAMETRES D'EXPLOITATION                 
      *                                                                         
           IF COMM-EX00-CODRET = 'GE'                                           
           THEN MOVE 'I'                TO COMM-EX00-CODMAJ                     
           ELSE MOVE 'R'                TO COMM-EX00-CODMAJ                     
           END-IF                                                               
           MOVE Z-SAVE-PARAM         TO COMM-EX00-ZPARAM                        
           PERFORM LINK-TEX00                                                   
           IF COMM-EX00-CODRET = 'GE'                                           
              IF COMM-EX00-CODMAJ = 'I' MOVE                                    
                'CREATION ENR. FICHIER PARAMETRE EXISTE DEJA' TO MESS           
              END-IF                                                            
              IF COMM-EX00-CODMAJ = 'R' MOVE                                    
                'ENR. FICHIER PARAMETRE NON TROUVE EN MODIF' TO MESS            
              END-IF                                                            
              GO TO ABANDON-TACHE                                               
           ELSE                                                                 
              MOVE 'DEMANDE PRISE EN COMPTE'                                    
                                        TO COMM-EX00-LIBRE(8:56)                
           END-IF.                                                              
      *                                                                 00017200
       FIN-TRAITEMENT-FICHIER-FEX000. EXIT.                             00017230
      *                                                                 00000400
       LINK-TEX00                    SECTION.                                   
      *--------------------------------------                                   
      *                                                                 00000400
           MOVE '00'                 TO COMM-EX00-CODRET                        
      *                                                                 00000400
           MOVE COMM-EX00-LONG-COMMAREA TO LONG-COMMAREA-LINK                   
           MOVE 'TEX00'              TO NOM-PROG-LINK                   00000700
           MOVE Z-COMMAREA-EX00      TO Z-COMMAREA-LINK                 00000700
           PERFORM LINK-PROG                                            00000800
           MOVE Z-COMMAREA-LINK      TO Z-COMMAREA-EX00.                00000700
      *                                                                 00000400
           EVALUATE COMM-EX00-CODRET                                            
              WHEN '00' CONTINUE                                                
              WHEN 'GE' CONTINUE                                                
              WHEN '99'                                                         
              MOVE 'FICHIER DES PARAMETRES FERME, PREVENIR BONDY'               
                                        TO COMM-EX00-LIBRE(8:56)                
              PERFORM MODULE-SORTIE                                             
              WHEN OTHER MOVE 'PROBLEME D''ACCES AU PROGRAMME TEX00'            
                                        TO COMM-EX00-LIBRE(8:56)                
              PERFORM MODULE-SORTIE                                             
           END-EVALUATE.                                                        
      *                                                                 00000400
       FIN-LINK-TEX00. EXIT.                                                    
      *                                                                         
      *---------------------------------------------------------------*         
      *        DESCRIPTION DES BRIQUES AIDA CICS                      *         
      *---------------------------------------------------------------*         
      *                                                                         
       S15-COPY SECTION. CONTINUE. COPY SYKCLINK.                               
      *                                                                         
       SECTION-ABANDON-TACHE         SECTION.                                   
      *--------------------------------------                                   
      *                                                                         
       ABANDON-ABEND.                                                           
           MOVE 'ABANDON-ABEND :'    TO MESS1                                   
           GO TO SEND-MESSAGE-ABANDON.                                          
       ABANDON-AIDA.                                                            
           MOVE 'ABANDON-AIDA  :'    TO MESS1                                   
           GO TO SEND-MESSAGE-ABANDON.                                          
       ABANDON-TACHE.                                                           
           MOVE 'ABANDON-TACHE :'    TO MESS1                                   
           GO TO SEND-MESSAGE-ABANDON.                                          
       ABANDON-CICS.                                                            
           MOVE 'ABANDON-CICS :'     TO MESS1                                   
           GO TO SEND-MESSAGE-ABANDON.                                          
       ABANDON-DL1.                                                             
           MOVE 'ABANDON-DL1   :'    TO MESS1                                   
           GO TO SEND-MESSAGE-ABANDON.                                          
       ABANDON-SQL.                                                             
           MOVE 'ABANDON-SQL   :'    TO MESS1                                   
           GO TO SEND-MESSAGE-ABANDON.                                          
      *                                                                         
       SEND-MESSAGE-ABANDON.                                                    
      *                                                                         
           EXEC CICS SEND                                                       
                 FROM    (TRACE-MESSAGE)                                        
                 LENGTH  (TRACE-MESSAGE-LONG)                                   
                 CTLCHAR (WCC-SCREEN)                                           
                 WAIT                                                           
                 NOHANDLE                                                       
           END-EXEC.                                                            
      *                                                                         
           EXEC  CICS  ABEND  CANCEL  NOHANDLE                                  
           END-EXEC.                                                            
      *                                                                         
