      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. TGV06.                                                       
       AUTHOR. DSA003.                                                          
      ******************************************************************        
      *  F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E   *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  PROGRAMME  : TGV06                                            *        
      *  TITRE      : CONSULTATION DES STOCKS ET DES RESERVATIONS      *        
      *              (APPEL A MGV20 --> RECHERCHE DES STOCKS EN        *        
      *                                 FONCTION DU MODE DE DELIVRANCE)*        
      *                                                                *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                           00000100
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                   00000110
      *    DECIMAL-POINT IS COMMA.                                      00000120
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       DATA DIVISION.                                                   00000130
      ******************************************************************        
      *                         W O R K I N G                          *        
      ******************************************************************        
       WORKING-STORAGE SECTION.
      *{Post-translation Correct-Val-pointeur
         77  NULLV PIC S9(8) COMP-5 VALUE -16777216.
         77  NULLP REDEFINES NULLV USAGE IS POINTER.
      *}
       01  FILLER                     PIC X(15) VALUE '*** WORKING ***'.        
      *                                                                 00000250
       01  WQUANTITE-SP               PIC -----.                        00000420
       01  IMAP                       PIC 9(02) VALUE ZEROES.           00000780
           88 FIN-BOUCLE-IMAP                   VALUE 09 THRU 99.               
      *                                                                 00000810
       01  FILLER                     PIC X(15) VALUE '*****  TS *****'.        
      **** DESCRIPTION DE LA TSGV01 : TS MAJ TABLE RTGV11                       
           COPY TSGV01.                                                 00001200
      *                                                                 00000810
      **** DESCRIPTION DE LA TSGV20 : TS D'APPEL A MGV20                        
       01  ZONE-TS-ENTREE.                                              00001530
           05 TSEN-ENTETE.                                              00001540
              10 TSEN-NOMPROG            PIC X(08).                     00001550
              10 TSEN-REQUETE            PIC X(04).                     00001560
              10 TSEN-CODRET             PIC 9(04).                     00001570
              10 TSEN-NBITEM             PIC 9(04).                     00001580
              10 TSEN-LGDONNEES          PIC 9(04).                     00001590
           05 TSEN-DATA.                                                00001600
              10 TSEN-CODTRAV            PIC X(01).                     00001610
              10 TSEN-FLAGDEM            PIC X(01).                     00001620
              10 TSEN-NSOCIETE           PIC X(03).                     00001630
              10 TSEN-NLIEU              PIC X(03).                     00001640
              10 TSEN-CINSEE             PIC X(05).                     00001640
              10 TSEN-NLIGNE             PIC 9(02).                     00001650
              10 TSEN-DONNEES            OCCURS 80.                     00001660
                 15 TSEN-NCODIC             PIC X(07).                  00001670
                 15 TSEN-CMODDEL            PIC X(03).                  00001680
                 15 TSEN-QVENDUE            PIC 9(03).                  00001690
           05 FILLER                  PIC X(3017).                      00001700
      *                                                                 00001710
       01  ZONE-TS-SORTIE.                                              00001720
           05 TSSO-ENTETE.                                              00001730
              10 TSSO-NOMPROG            PIC X(08).                     00001740
              10 TSSO-REQUETE            PIC X(04).                     00001750
              10 TSSO-CODRET             PIC 9(04).                     00001760
              10 TSSO-NBITEM             PIC 9(04).                     00001770
              10 TSSO-LGDONNEES          PIC 9(04).                     00001780
           05 TSSO-DATA.                                                00001790
              10 TSSO-DONNEES            OCCURS 80.                     00001800
                 15 TSSO-NCODIC             PIC X(07).                  00001810
                 15 TSSO-QDISPO             PIC 9(05).                  00001820
                 15 TSSO-WCQERESF           PIC X(01).                  00001830
                 15 TSSO-DCQERESF           PIC 9(08).                  00001840
              10 FILLER                  PIC X(2392).                   00001850
      *                                                                 00001710
       01  FILLER                     PIC X(15) VALUE '*** MAP GV06 **'.        
           COPY EGV06 REPLACING EGV06I BY Z-MAP.                        00002220
       01  FILLER                     PIC X(15) VALUE '*** COMMAREA **'.        
           COPY COMMGV00.                                               00002280
           COPY SYKWCOMM.                                               00002300
           COPY SYKWSTAR.                                               00002310
       01  COMM-MGV20-APPLI.                                            00002330
           05 COMM-MGV20-NOMTS        PIC X(08).                        00002340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 COMM-MGV20-NBITEM       PIC S9(04)  COMP.                 00002350
      *--                                                                       
           05 COMM-MGV20-NBITEM       PIC S9(04) COMP-5.                        
      *}                                                                        
           05 FILLER                  PIC X(01).                        00002360
           05 COMM-MGV20-CODRET       PIC X(01).                        00002370
              88 COMM-MGV20-ERREUR                VALUE 'N'.                    
           05 COMM-MGV20-LIBERR       PIC X(80).                        00002380
           05 FILLER                  PIC X(108).                       00002390
       01  FILLER                     PIC X(15)  VALUE 'ZCOMMAREA-LINK'.00002450
       01  Z-COMMAREA-LINK            PIC X(200).                       00002460
           COPY COMMDATC.                                                       
      *                                                                 00002000
       01  FILLER                     PIC X(15) VALUE '** AIDA ZONES *'.        
           COPY SYKWDIV0.                                               00000150
           COPY SYKWEIB0.                                               00002050
           COPY SYKWDATH.                                               00002060
           COPY SYKWCWA0.                                               00002070
           COPY SYKWTCTU.                                               00002080
           COPY SYKWECRA.                                               00002090
           COPY SYKWZINO.                                               00002540
           COPY SYKWERRO.                                               00002600
           COPY SYKWZCMD.                                               00001930
       01  FILLER                     PIC X(15) VALUE '* ZONE MODULE *'.        
           COPY SYKWMONT.                                               00002150
           COPY WGV00.                                                  00001990
      *                                                                 00002320
       LINKAGE SECTION.                                                 00002920
       01  DFHCOMMAREA.                                                 00002940
           05  FILLER PIC X OCCURS 9096 DEPENDING ON EIBCALEN.          00002950
           COPY SYKLINKB.                                               00002960
       PROCEDURE DIVISION.                                              00003010
      *                                                                 00003020
      ***************************************************************** 00003030
      *              T R A M E   DU   P R O G R A M M E               * 00003040
      ***************************************************************** 00003050
      *                                                                 00003200
       MODULE-GV06                    SECTION.                          00003210
      *---------------------------------------                          00003220
      *                                                                 00003230
           PERFORM MODULE-ENTREE.                                       00003240
           IF TRAITEMENT                                                00003260
              PERFORM MODULE-TRAITEMENT                                 00003270
           END-IF.                                                      00003280
           PERFORM MODULE-SORTIE.                                       00003300
      *                                                                         
       FIN-MODULE-GV06. EXIT.                                                   
      *                                                                         
      ******************************************************************00001810
      *                          E N T R E E                           *00001820
      ******************************************************************00001810
      *                                                                 00003580
       MODULE-ENTREE                  SECTION.                          00003590
      *---------------------------------------                          00003600
      *                                                                 00003610
           PERFORM INIT-HANDLE.                                         00003620
           PERFORM INIT-ADDRESS.                                        00003640
           PERFORM INIT-USER.                                           00003660
           PERFORM RECEPTION-MESSAGE.                                   00003680
      *                                                                         
       FIN-MODULE-ENTREE. EXIT.                                                 
      *                                                                 00003690
       E01-COPY SECTION. CONTINUE. COPY SYKCHAND.                       00003700
      *                                                                 00003710
       INIT-USER                      SECTION.                          00003760
      *---------------------------------------                          00003770
      *                                                                 00003780
           MOVE  LOW-VALUE            TO Z-MAP.                         00003790
           MOVE 'EGV06'               TO NOM-MAP.                       00003800
           MOVE 'EGV06'               TO NOM-MAPSET.                    00003810
           MOVE 'TGV06'               TO NOM-PROG.                      00003820
           MOVE  'GV06'               TO NOM-TACHE.                     00003830
           MOVE  'GV00'               TO NOM-LEVEL-MAX.                 00003840
           MOVE   'NON'               TO DEBUGGIN.                      00003850
           MOVE LENGTH OF Z-COMMAREA  TO LONG-COMMAREA LONG-START.      00003880
           IF LONG-COMMAREA LESS THAN 220                               00003900
              MOVE 'COMMAREA PLUS PETITE QUE 220'  TO MESS              00003910
              GO TO ABANDON-TACHE                                       00003920
           END-IF.                                                      00003930
           IF LONG-COMMAREA GREATER THAN 9096                           00003950
              MOVE 'COMMAREA PLUS GRANDE QUE 9096' TO MESS              00003960
              GO TO ABANDON-TACHE                                       00003970
           END-IF.                                                      00003980
           IF EIBCALEN NOT = 0                                          00004000
              MOVE DFHCOMMAREA           TO Z-COMMAREA                  00004010
           ELSE                                                                 
              MOVE SPACES                TO Z-COMMAREA                  00004020
              MOVE 'TGV00'               TO NOM-PROG-XCTL               00004030
              PERFORM XCTL-NO-COMMAREA                                  00004040
           END-IF.                                                      00004050
      *                                                                         
       FIN-INIT-USER. EXIT.                                                     
      *                                                                 00006420
       E02-COPY SECTION. CONTINUE. COPY SYKCADDR.                       00006430
      *                                                                 00006440
       RECEPTION-MESSAGE              SECTION.                          00007010
      *---------------------------------------                          00007020
      *                                                                 00007030
           PERFORM POSIT-ATTR-INITIAL.                                  00007040
      **** AUTRE TACHE                                                  00007050
           IF EIBTRNID NOT = NOM-TACHE                                  00007060
              MOVE CODE-TRAITEMENT-AUTOMATIQUE TO FONCTION              00007070
              GO TO FIN-RECEPTION-MESSAGE                               00007080
           END-IF.                                                      00007090
      **** FAST PATH                                                    00007100
           IF EIBTRNID  = Z-COMMAREA-TACHE-JUMP                         00007110
              MOVE SPACES TO Z-COMMAREA-TACHE-JUMP                      00007120
              MOVE CODE-TRAITEMENT-AUTOMATIQUE TO FONCTION              00007130
              GO TO FIN-RECEPTION-MESSAGE                               00007140
           END-IF.                                                      00007150
      *                                                                         
 NV01 * ��� POUR EVITER LA TOUCHE PF2,                                          
  '   * SI ON NE MET PAS CELA, ON PART DANS LES MODULES @ MAURICE               
  '   * ET CELA BEUGGGG                                                         
 NV01      IF  EIBAID = '2' OR '>'                                              
  '            MOVE  '1' TO EIBAID                                              
 NV01      END-IF                                                               
      *                                                                         
           PERFORM RECEIVE-MAP.                                         00007170
                           MOVE SPACES                   TO MLIBERRI.   00007730
                           MOVE EIBAID                   TO WORKAID.    00007190
                           SET  PAS-DE-DEBRANCHEMENT     TO TRUE.               
      **** TRAITEMENT NORMAL                                            00007200
           IF TOUCHE-ENTER MOVE CODE-TRAITEMENT-NORMAL   TO FONCTION.   00007220
      **** RETOUR NIVEAU SUPERIEUR                                      00007240
           IF TOUCHE-PF3   MOVE CODE-LEVEL-MAX           TO FONCTION.   00007260
           IF TOUCHE-PF4   MOVE CODE-LEVEL-MAX           TO FONCTION.   00007260
      **** PAGINATION   ARRIERE(PF7),AVANT(PF8)                                 
           IF TOUCHE-PF7                                                00007290
              IF KOM-PAGE > 1                                           00007300
                 MOVE CODE-PRECEDENTE          TO FONCTION              00007310
                 SUBTRACT 1                  FROM KOM-PAGE              00007320
              ELSE                                                      00007330
                 MOVE CODE-ERREUR-MANIPULATION TO FONCTION              00007340
              END-IF                                                    00007350
           END-IF.                                                      00007360
           IF TOUCHE-PF8                                                00007380
              COMPUTE ICOMM = (KOM-PAGE * 8) + 1                        00007860
              IF KOM-LIGNE-INEXISTANTE(ICOMM) OR KOM-DERNIERE-PAGE      00007390
                 MOVE CODE-ERREUR-MANIPULATION TO FONCTION              00007400
              ELSE                                                      00007410
                 MOVE CODE-SUIVANTE            TO FONCTION              00007420
                 ADD  1                        TO KOM-PAGE              00007430
              END-IF                                                    00007440
           END-IF.                                                      00007450
      **** DEBRANCHEMENT FENETRES DE VENTE                              00007460
           IF NOT COMM-SAISIE-PRE-RESERVATION                           00007470
              IF TOUCHE-PF15                                            00007480
                 SET  DEBRANCHE-CARACTERISTIQUE   TO TRUE               00007480
              END-IF                                                            
              IF TOUCHE-PF16                                            00007510
                 SET  DEBRANCHE-COMMENTAIRE       TO TRUE               00007520
              END-IF                                                    00007530
              IF TOUCHE-PF18                                            00007590
                 SET  DEBRANCHE-ADRESSE           TO TRUE               00007600
              END-IF                                                    00007610
           END-IF.                                                      00007490
           IF TOUCHE-PF17  SET  DEBRANCHE-QUOTA          TO TRUE.       00007560
      **** ZONE COMMANDE                                                        
           IF MZONCMDI NOT = LOW-VALUE AND SPACES                       00007630
              MOVE 1                     TO DISPATCH-LEVEL              00007640
              PERFORM RECEPTION-ZONE-COMMANDE                           00007650
           END-IF.                                                      00007660
      **** MAPFAIL                                                              
           IF ECRAN-MAPFAIL                                             00007680
              MOVE LOW-VALUE             TO Z-MAP                       00007690
              MOVE CODE-TRAITEMENT-AUTOMATIQUE TO FONCTION              00007700
           END-IF.                                                      00007710
           PERFORM POSIT-ATTR-INITIAL.                                  00007740
      *                                                                 00007750
       FIN-RECEPTION-MESSAGE. EXIT.                                     00007760
      *                                                                 00007770
       E03-COPY SECTION. CONTINUE. COPY SYKCRECV.                       00007780
       E04-COPY SECTION. CONTINUE. COPY SYKCZCMD.                       00007790
      *                                                                 00007800
       POSIT-ATTR-INITIAL             SECTION.                          00007810
       FIN-POSIT-ATTR-INITIAL.        EXIT.                                     
      *                                                                 00001800
      ******************************************************************00001810
      *                      T R A I T E M E N T                       *00001820
      ******************************************************************00001810
      *                                                                         
       MODULE-TRAITEMENT              SECTION.                          00008520
      *---------------------------------------                          00008530
      *                                                                 00008530
           IF TRAITEMENT-NORMAL                                         00008540
              PERFORM MODULE-TRAITEMENT-NORMAL                          00008550
           END-IF.                                                      00008560
           IF TRAITEMENT-AUTOMATIQUE                                    00008580
              PERFORM MODULE-TRAITEMENT-AUTOMATIQUE                     00008590
           END-IF.                                                      00008600
      *                                                                 00008610
       FIN-MODULE-TRAITEMENT. EXIT.                                     00008620
      *                                                                         
      ******************************************************************        
      *          T R A I T E M E N T   A U T O M A T I Q U E           *        
      ******************************************************************        
      *                                                                 00008850
       MODULE-TRAITEMENT-AUTOMATIQUE  SECTION.                          00008860
      *---------------------------------------                          00008870
      *                                                                 00008900
           IF EIBTRNID NOT = NOM-TACHE                                  00004100
      ******* PREMIER PASSAGE                                           00004110
              PERFORM POSITIONNEMENT-FICHIER                            00004120
           END-IF.                                                      00004130
           PERFORM REMPLISSAGE-ZONES-OBLIGATOIRES.                      00008930
           PERFORM REMPLISSAGE-ZONES-PROTEGEES.                                 
           PERFORM REMPLISSAGE-ZONES-NO-PROTEGEES.                              
      *                                                                 00009140
       FIN-MODULE-TRAITEMENT-AUTOMAT. EXIT.                                     
      *                                                                         
      ******************************************************************        
      *          P O S I T I O N N E M E N T   F I C H I E R           *        
      ******************************************************************        
      *                                                                         
       POSITIONNEMENT-FICHIER         SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** REMPLISSAGE DE LA TS EN ENTREE DU MGV20                      00004290
           PERFORM DELETE-TS-MGV20                                              
           INITIALIZE                    IL ZONE-TS-ENTREE              00005400
           PERFORM VARYING ICOMM FROM 1 BY 1                            00004320
      *{ reorder-array-condition 1.1                                            
      *              UNTIL KOM-LIGNE-INEXISTANTE (ICOMM)                00004330
      *                 OR ICOMM > COMM-GV01-NLIGNE-MAX                 00004350
      *--                                                                       
                     UNTIL ICOMM > COMM-GV01-NLIGNE-MAX                         
                        OR KOM-LIGNE-INEXISTANTE (ICOMM)                        
      *}                                                                        
              PERFORM READ-TSGV01                                       00004740
      ******* ON NE PASSE AU MODULE QUE                                         
      ******* LES LIGNES DE VENTES NON ANNULEES                                 
              IF KOM-LIGNE-VENTE(ICOMM) AND KOM-LIGNE-NORMALE(ICOMM)    00004770
                 PERFORM REMPLISSAGE-TS-ENTREE-MGV20                    00004800
              END-IF                                                    00004820
           END-PERFORM                                                  00004370
      *                                                                 00004290
      **** SI AU MOINS UNE LIGNE DE VENTE NON ANNULEE => ACCES A MGV20  00004390
      **** MODULE DE RECHERCHE DES STOCKS MAGASINS OU ENTREPOTS         00004390
           IF TSEN-NOMPROG = 'MGV20'                                    00004400
              PERFORM WRITE-TS-MGV20                                            
              PERFORM ACCES-MODULE-MGV20                                00004410
           END-IF.                                                      00004420
      *                                                                         
       FIN-POSITIONNEMENT-FICHIER. EXIT.                                        
      *                                                                 00005520
       REMPLISSAGE-TS-ENTREE-MGV20    SECTION.                          00005530
      *---------------------------------------                          00005540
      *                                                                 00005550
           MOVE 'MGV20'               TO TSEN-NOMPROG                   00005570
           MOVE SPACES                TO TSEN-REQUETE                   00005580
           MOVE ZEROES                TO TSEN-CODRET                    00005590
           MOVE 1                     TO TSEN-NBITEM                    00005600
           MOVE ZEROES                TO TSEN-LGDONNEES                 00005610
           MOVE SPACES                TO TSEN-CODTRAV                   00005620
           MOVE 'E'                   TO TSEN-FLAGDEM                   00005630
           MOVE COMM-GV00-NSOCLIVR    TO TSEN-NSOCIETE                  00005640
           MOVE COMM-GV00-NDEPOT      TO TSEN-NLIEU                     00005650
           IF PRESENCE-ADRESSE-TYPE-B                                           
           THEN MOVE COMM-GV00-CINSEE-B    TO TSEN-CINSEE                       
           ELSE MOVE COMM-GV00-CINSEE-A    TO TSEN-CINSEE                       
           END-IF                                                               
           ADD  1                     TO IL                             00005560
           MOVE IL                    TO TSEN-NLIGNE                    00005700
      *                                                                 00005550
           MOVE TS01-NCODIC           TO TSEN-NCODIC (IL)               00005660
           MOVE TS01-CMODDEL          TO TSEN-CMODDEL (IL)              00005670
           MOVE TS01-MQTE             TO TSEN-QVENDUE (IL).             00005680
      *                                                                         
       FIN-REMPL-TS-ENTREE-MGV20. EXIT.                                         
      *                                                                 00005720
       ACCES-MODULE-MGV20             SECTION.                          00005730
      *---------------------------------------                          00005740
      *                                                                 00005750
           INITIALIZE                    COMM-MGV20-APPLI                       
           MOVE IDENT-TS              TO COMM-MGV20-NOMTS               00005770
           MOVE LENGTH OF COMM-MGV20-APPLI  TO LONG-COMMAREA-LINK               
           MOVE COMM-MGV20-APPLI      TO Z-COMMAREA-LINK                        
           MOVE 'MGV20'               TO NOM-PROG-LINK                          
           PERFORM LINK-PROG                                                    
           MOVE Z-COMMAREA-LINK       TO COMM-MGV20-APPLI                       
           IF COMM-MGV20-ERREUR                                         00005890
              MOVE COMM-MGV20-LIBERR     TO MLIBERRI                    00005900
           END-IF.                                                      00005920
      *                                                                         
       FIN-ACCES-MODULE-MGV20. EXIT.                                            
      *                                                                 00009170
       REMPLISSAGE-ZONES-OBLIGATOIRES SECTION.                          00009180
      *---------------------------------------                          00009190
      *                                                                 00009200
           MOVE COMM-CICS-APPLID      TO MCICSI.                        00009210
           MOVE COMM-CICS-NETNAM      TO MNETNAMI.                      00009220
           MOVE COMM-CICS-TRANSA      TO MCODTRAI.                      00009230
           MOVE EIBTRMID              TO MSCREENI.                      00009240
           MOVE COMM-DATE-JJ-MM-SSAA  TO MDATJOUI.                      00009250
           MOVE Z-TIMER-TIMJOU        TO MTIMJOUI.                      00009260
      *                                                                 00009900
       FIN-REMP-ZONES-OBL. EXIT.                                                
      *                                                                         
       REMPLISSAGE-ZONES-PROTEGEES    SECTION.                          00009910
      *---------------------------------------                          00009920
      *                                                                 00009930
      **** CHARGEMENT DES ZONES ENTETE ET PIED DE VENTE                 00009940
           MOVE 'CONSULTATION DES STOCKS'   TO MTITREI                  00009280
           MOVE KOM-PAGE              TO MWPAGEI                        00009960
           MOVE COMM-GV00-NLIEU       TO MNLIEUI                        00009970
           IF COMM-GV00-NVENTE NUMERIC                                  00009980
           THEN MOVE COMM-GV00-NVENTE    TO MNVENTEI                    00009990
           ELSE MOVE SPACES              TO MNVENTEI                    00010000
           END-IF                                                       00010010
           STRING COMM-GV00-DVENTE(7:2) '/' COMM-GV00-DVENTE(5:2)               
              '/' COMM-GV00-DVENTE(3:2)                                         
              DELIMITED BY SIZE        INTO MDVENTEI                            
           END-STRING                                                           
           MOVE COMM-GV00-NORDRE      TO MNORDREI                       00010020
           IF COMM-GV00-DMODIFVTE NOT = SPACES                          04970034
              STRING COMM-GV00-DMODIFVTE(7:2) '/'                               
                 COMM-GV00-DMODIFVTE(5:2) '/' COMM-GV00-DMODIFVTE(3:2)          
                 DELIMITED BY SIZE     INTO MDMODIFVTEI                         
              END-STRING                                                        
           ELSE                                                                 
              MOVE SPACES                TO MDMODIFVTEI                 04970034
           END-IF                                                               
      *                                                                 00010220
           MOVE COMM-GV00-ECMODDEL    TO MECMODDELI                     00010230
           MOVE COMM-GV00-EDDELIV     TO MEDDELIVI                      00010240
           MOVE COMM-GV00-ECPLAGE     TO MECPLAGEI                      00010250
           MOVE COMM-GV00-CVENDEUR(1) TO MCVENDEUR1I                    00010270
           MOVE COMM-GV00-LVENDEUR(1) TO MLVENDEUR1I                    00010280
           MOVE COMM-GV00-CVENDEUR(2) TO MCVENDEUR2I                    00010290
           MOVE COMM-GV00-LVENDEUR(2) TO MLVENDEUR2I                    00010300
           MOVE COMM-GV00-CVENDEUR(3) TO MCVENDEUR3I                    00010310
           MOVE COMM-GV00-LVENDEUR(3) TO MLVENDEUR3I                    00010320
           MOVE COMM-GV00-CVENDEUR(4) TO MCVENDEUR4I                    00010330
           MOVE COMM-GV00-LVENDEUR(4) TO MLVENDEUR4I                    00010340
           MOVE COMM-GV00-LCOMVTE1    TO MLCOMVTE1I                     00010360
           MOVE COMM-GV00-LCOMVTE2    TO MLCOMVTE2I                     00010370
           MOVE COMM-GV00-LCOMVTE3    TO MLCOMVTE3I                     00010380
           MOVE COMM-GV00-LCOMVTE4    TO MLCOMVTE4I                     00010390
           INITIALIZE                    WS-ALPHA                       00010410
           MOVE COMM-GV00-PTTVENTE    TO WS-DECIMAL-7S                  00010420
           PERFORM REMPLISSAGE-ZONE-NUMERIQUE                           00010430
           PERFORM TRAITEMENT-ZONE-NUMERIQUE                            00010440
           MOVE WS-ALPHA              TO MPTTVENTEI                     00010450
           INITIALIZE                    WS-ALPHA                       00010470
           MOVE COMM-GV00-PCOMPT      TO WS-DECIMAL-7S                  00010480
           PERFORM REMPLISSAGE-ZONE-NUMERIQUE                           00010490
           PERFORM TRAITEMENT-ZONE-NUMERIQUE                            00010500
           MOVE WS-ALPHA              TO MPCOMPTI                       00010510
           INITIALIZE                    WS-ALPHA                       00010530
           MOVE COMM-GV00-PLIVR       TO WS-DECIMAL-7S                  00010540
           PERFORM REMPLISSAGE-ZONE-NUMERIQUE                           00010550
           PERFORM TRAITEMENT-ZONE-NUMERIQUE                            00010560
           MOVE WS-ALPHA              TO MPLIVRI                        00010570
           INITIALIZE                    WS-ALPHA                       00010590
           MOVE COMM-GV00-PDIFFERE    TO WS-DECIMAL-7S                  00010600
           PERFORM REMPLISSAGE-ZONE-NUMERIQUE                           00010610
           PERFORM TRAITEMENT-ZONE-NUMERIQUE                            00010620
           MOVE WS-ALPHA               TO MPDIFFEREI                    00010630
           INITIALIZE                     WS-ALPHA                      00010650
           MOVE COMM-GV00-PRFACT       TO WS-DECIMAL-7S                 00010660
           PERFORM REMPLISSAGE-ZONE-NUMERIQUE                           00010670
           PERFORM TRAITEMENT-ZONE-NUMERIQUE                            00010680
           MOVE WS-ALPHA               TO MPRFACTI                      00010690
      **** GV10-LAUTORM CONTIENT LE TERMINAL: NE PAS AFFICHER                   
           MOVE SPACES                 TO MLAUTORMI                     00029070
      *    MOVE COMM-GV00-NAUTORD      TO MNAUTORDI                     00010720
           MOVE COMM-GV00-WFACTURE     TO MWFACTUREI                    00010740
           MOVE COMM-GV00-WDETAXEC     TO MWDETAXECI                    00010750
           MOVE COMM-GV00-WEXPORT      TO MWEXPORTI                     00010760
           MOVE COMM-GV00-WBLSP        TO MWBLSPI.                      00010770
           IF MLIBERRI = SPACES OR LOW-VALUE                                    
              MOVE 'ENTER:CONTROLE   FP3/4:RETOUR   FP7/8:PAGINATION '  00009290
                                          TO MLIBERRI                   00009300
           END-IF                                                               
           IF KOM-PAS-ITEM                                              00009320
              MOVE 1                      TO KONTROL                    00009330
              MOVE 'AUCUNE LIGNE DE VENTE ' TO MLIBERRI                 00009340
           END-IF.                                                      00009350
      *                                                                         
       FIN-REMP-ZONES-PROT. EXIT.                                               
      *                                                                         
       REMPLISSAGE-ZONES-NO-PROTEGEES SECTION.                          00009910
      *---------------------------------------                          00009920
      *                                                                 00009930
      **** LECTURE DE LA TS RETOUR DE MGV20                             00009930
           PERFORM READ-TS-MGV20                                                
           COMPUTE ICOMM = (KOM-PAGE * 8) - 7                           00008910
           PERFORM VARYING IMAP FROM 1 BY 1 UNTIL FIN-BOUCLE-IMAP       00008980
              IF KOM-LIGNE-INEXISTANTE(ICOMM)                           00009020
                 MOVE SPACES                TO MCMARQI(IMAP)            00009400
                      MLREFFOURNI(IMAP)        MNCODICI(IMAP)           00009420
                      MQVENDUEI(IMAP)          MQDISPODEPI(IMAP)        00009440
                      MQDISPOMAGI(IMAP)        MDRESFI(IMAP)            00009460
                      MDCQEI(IMAP)             MNAUTORI(IMAP)           00009480
              ELSE                                                      00009050
                 PERFORM READ-TSGV01                                    00009030
                 MOVE TS01-CMARQ            TO MCMARQI(IMAP)            00009540
                 MOVE TS01-LREFFOURN        TO MLREFFOURNI(IMAP)        00009550
                 MOVE TS01-NCODIC           TO MNCODICI(IMAP)           00009560
                 MOVE TS01-MQTE             TO WS-QUANTITE              00009570
                 MOVE WS-QUANTITE           TO MQVENDUEI(IMAP)          00009580
                 IF KOM-LIGNE-VENTE(ICOMM) AND KOM-LIGNE-NORMALE(ICOMM) 00004760
      ************* COMPTER LES LIGNES DE VENTE DANS LA COMMGV01                
      ************* JUSQU' A LA LIGNE TRAITEE, LE NOMBRE DE LIGNE               
      ************* TROUVE DONNE LE NUM. DE LIGNE DANS LA TS DE MGV20           
                    INITIALIZE                    IL                            
                    PERFORM VARYING ICOMM1 FROM 1 BY 1                          
                              UNTIL ICOMM1 > ICOMM                              
                       IF KOM-LIGNE-VENTE(ICOMM1) AND                           
                          KOM-LIGNE-NORMALE(ICOMM1)                             
                          ADD  1                     TO IL                      
                       END-IF                                                   
                    END-PERFORM                                                 
                    PERFORM REMPLISSAGE-ZONES-STOCK                             
                 END-IF                                                         
              END-IF                                                    00009080
              ADD  1                     TO ICOMM                       00009090
           END-PERFORM.                                                 00009110
           PERFORM POSIT-ATTR-INITIAL.                                  00009130
      *                                                                         
       FIN-REMP-ZONES-NO-PROT. EXIT.                                            
      *                                                                         
       REMPLISSAGE-ZONES-STOCK        SECTION.                          00009910
      *---------------------------------------                          00009920
      *                                                                 00009930
           MOVE TSSO-QDISPO(IL)       TO WQUANTITE-SP                   00009600
           IF TS01-STOCK-MAGASIN                                        00004790
              MOVE WQUANTITE-SP          TO MQDISPOMAGI(IMAP)           00009630
              MOVE SPACES                TO MQDISPODEPI(IMAP)           00009630
           ELSE                                                                 
              MOVE SPACES                TO MQDISPOMAGI(IMAP)           00009630
              MOVE WQUANTITE-SP          TO MQDISPODEPI(IMAP)           00009610
           END-IF                                                               
           IF TSSO-WCQERESF(IL) = 'F'                                   00006250
              MOVE TSSO-DCQERESF(IL)    TO GFSAMJ-0                     00006260
              STRING GFSAMJ-0(7:2) GFSAMJ-0(5:2) GFSAMJ-0(3:2)          00006190
                 DELIMITED BY SIZE     INTO MDRESFI(IMAP)               00006200
              END-STRING                                                00006210
           ELSE                                                                 
              MOVE SPACES                TO MDRESFI(IMAP)                       
           END-IF                                                       00006310
           IF TSSO-WCQERESF(IL) = 'Q'                                   00006170
              MOVE TSSO-DCQERESF(IL)    TO GFSAMJ-0                     00006180
              STRING GFSAMJ-0(7:2) GFSAMJ-0(5:2) GFSAMJ-0(3:2)          00006190
                 DELIMITED BY SIZE     INTO MDCQEI(IMAP)                00006200
              END-STRING                                                00006210
           ELSE                                                                 
              MOVE SPACES                TO MDCQEI(IMAP)                00009660
           END-IF                                                       00006230
           MOVE SPACES                TO MNAUTORI(IMAP).                00009660
      *                                                                         
       FIN-REMPLISSAGE-ZONES-STOCK. EXIT.                                       
      *                                                                 00010960
       MODULE-TRAITEMENT-NORMAL       SECTION.                          00010970
       FIN-MODULE-TRAITEMENT-NORM.    EXIT.                                     
      *                                                                         
      *****************************************************************         
      *                          S O R T I E                          * 00001820
      *****************************************************************         
      *                                                                 00013330
       MODULE-SORTIE                  SECTION.                          00013340
      *---------------------------------------                                  
      *                                                                 00013350
           IF TRAITEMENT-AUTOMATIQUE  PERFORM SORTIE-AFFICHAGE-FORMAT.  00013380
           IF NOT OK                  PERFORM SORTIE-ERREUR.            00013430
           IF DEBRANCHEMENT-FENETRES  PERFORM SORTIE-AUTRE-TRANSACTION. 00013520
           IF TRAITEMENT-NORMAL       PERFORM SORTIE-SUITE.             00013560
           IF LEVEL-MAX OR JUMP       PERFORM SORTIE-LEVEL-MAX.         00013600
           IF ERREUR-MANIPULATION     PERFORM SORTIE-ERREUR-MANIP.      00013650
           IF HELP                    PERFORM SORTIE-HELP.              00013700
      **** ABANDON                                                      00013730
           MOVE 'ERREUR CODE FONCTION DANS MODULE SORTIE' TO MESS       00013750
           GO TO ABANDON-TACHE.                                         00013760
      *                                                                 00013770
       FIN-MODULE-SORTIE. EXIT.                                                 
      *                                                                         
       SORTIE-AFFICHAGE-FORMAT        SECTION.                          00013780
      *---------------------------------------                          00013790
      *                                                                 00013800
           MOVE CURSEUR               TO MZONCMDL.                      00015070
           PERFORM SEND-MAP.                                            00013820
           MOVE SPACES                TO Z-COMMAREA-TACHE-JUMP.         00013830
           MOVE NOM-TACHE             TO NOM-TACHE-RETOUR.              00013840
           PERFORM RETOUR-COMMAREA.                                     00013850
      *                                                                 00013770
       FIN-SORTIE-AFFICHAGE-FORMAT. EXIT.                                       
      *                                                                 00013860
       SORTIE-ERREUR                  SECTION.                          00013870
      *---------------------------------------                          00013880
      *                                                                 00013890
      **** AFFICHAGE DE LA MAP EN ERREUR, EN CONFIRMATION               00013900
      **** ET RETURN AU MEME PROGRAMME                                  00013910
           PERFORM SEND-MAP-SORTIE-ERREUR.                              00013930
           MOVE NOM-TACHE             TO NOM-TACHE-RETOUR.              00013940
           PERFORM RETOUR-COMMAREA.                                     00013950
      *                                                                 00013770
       FIN-SORTIE-ERREUR. EXIT.                                                 
      *                                                                 00014200
       SORTIE-AUTRE-TRANSACTION       SECTION.                          00014210
      *---------------------------------------                          00014220
      *                                                                 00014230
      **** DEBRANCHEMENT VERS LES FENETRES                              00012430
           PERFORM DELETE-TS-MGV20                                              
           MOVE COMM-GV00-DEBRANCHEMENT  TO NOM-PROG-XCTL               00014250
           PERFORM XCTL-PROG-COMMAREA .                                 00014260
      *                                                                 00013770
       FIN-SORTIE-AUTRE-TRANSACTION. EXIT.                                      
      *                                                                 00013960
       SORTIE-SUITE                   SECTION.                          00013970
      *----------------------------------------                         00013980
      *                                                                 00013990
      **** AFFICHAGE DE LA MAP SUIVANTE ET RETURN AU PROGRAMME SUIVANT  00014000
           IF MLIBERRI = SPACES OR LOW-VALUE                            00014020
              MOVE                                                      00014030
             'ENTER:CTL  FP5:VALIDATION  FP3/4:RETOUR  FP7/8:PAGINATION'00014040
                                      TO MLIBERRI                       00014050
           END-IF                                                       00014060
           MOVE CURSEUR               TO MZONCMDL.                      00015070
           PERFORM SEND-MAP-DATA-ONLY.                                  00014090
           MOVE EIBTRNID              TO NOM-TACHE-RETOUR.              00014100
           PERFORM RETOUR-COMMAREA.                                     00014110
      *                                                                 00013770
       FIN-SORTIE-SUITE. EXIT.                                                  
      *                                                                 00014120
       SORTIE-LEVEL-MAX               SECTION.                          00014130
      *---------------------------------------                          00014140
      *                                                                 00014150
      **** RETOUR APRES PF3 OU PF4 AU MENU                              00014160
           PERFORM DELETE-TS-MGV20                                              
           MOVE COMM-GV00-CPROG       TO NOM-PROG-XCTL                  00014180
           PERFORM XCTL-PROG-COMMAREA.                                  00014190
      *                                                                 00013770
       FIN-SORTIE-LEVEL-MAX. EXIT.                                              
      *                                                                 00014270
       SORTIE-ERREUR-MANIP            SECTION.                          00014280
      *----------------------------------------                         00014290
      *                                                                 00014300
      **** SORTIE ERREUR MANIPULATION                                   00014310
           MOVE 'K017 : ERREUR MANIPULATION' TO MLIBERRI.               00014330
           MOVE 'K017'                       TO COM-CODERR.             00014340
           IF TOUCHE-PF7 MOVE 'K017 : PREMIERE PAGE   ' TO MLIBERRI.    00014370
           IF TOUCHE-PF8 MOVE 'K017 : DERNIERE PAGE   ' TO MLIBERRI.    00014410
           MOVE CURSEUR               TO MZONCMDL.                      00014440
           PERFORM SEND-MAP-ERREUR-MANIP.                               00014450
           MOVE NOM-TACHE             TO NOM-TACHE-RETOUR.              00014460
           PERFORM RETOUR-COMMAREA.                                     00014470
      *                                                                 00014270
       FIN-SORTIE-ERREUR-MANIP. EXIT.                                           
      *                                                                         
       SAVE-SWAP-ATTR                 SECTION.                                  
       FIN-SAVE-SWAP-ATTR.            EXIT.                                     
      *                                                                         
       REST-SWAP-ATTR                 SECTION.                                  
       FIN-REST-SWAP-ATTR.            EXIT.                                     
      *                                                                         
      ******************************************************************00001810
      *                    M O D U L E S   A I D A                     *00001820
      ******************************************************************00001810
      *                                                                 00015160
      *DESCRIPTION DES BRIQUES AIDA CICS                                        
       S01-COPY    SECTION. CONTINUE. COPY SYKCHELP.                    00015210
       S02-COPY    SECTION. CONTINUE. COPY SYKCRTCO.                    00015220
       S03-COPY    SECTION. CONTINUE. COPY SYKCRT00.                    00015230
       S04-COPY    SECTION. CONTINUE. COPY SYKCSMDO.                    00015240
       S05-COPY    SECTION. CONTINUE. COPY SYKCSMEM.                    00015250
       S06-COPY    SECTION. CONTINUE. COPY SYKCSMSE.                    00015260
       S07-COPY    SECTION. CONTINUE. COPY SYKCSM00.                    00015270
       S08-COPY    SECTION. CONTINUE. COPY SYKCXCTL.                    00015280
       S09-COPY    SECTION. CONTINUE. COPY SYKCLINK.                    00015290
       S10-COPY    SECTION. CONTINUE. COPY SYKCSWAP.                    00015300
       S11-COPY    SECTION. CONTINUE. COPY SYKXSWAP.                    00015310
      *MODULES DE CONTROLES ET DE TRAITEMENTS STANDARDISES              00015120
       T06-COPY    SECTION. CONTINUE. COPY SYKPMONT.                    00015150
      *DESCRIPTION DES BRIQUES AIDA TEMP                                        
       TEMP01-COPY SECTION. CONTINUE. COPY SYKCTSDE.                    00015370
       TEMP02-COPY SECTION. CONTINUE. COPY SYKCTSRD.                    00015380
       TEMP03-COPY SECTION. CONTINUE. COPY SYKCTSRW.                    00015390
       TEMP04-COPY SECTION. CONTINUE. COPY SYKCTSWR.                    00015400
      *MODULE D'ABANDON                                                         
       99-COPY     SECTION. CONTINUE. COPY SYKCERRO.                    00015560
      *DESCRIPTION DES MODULES COMMUNS AUX PROGRAMMES DE VENTE          00015590
          COPY MGV00.                                                   00015620
      *                                                                         
      ******************************************************************00001810
      *                     G E S T I O N   T S                        *00001820
      ******************************************************************00001810
      *                                                                         
      **** GESTION DE LA TSGV01 : TS MAJ TABLE RTGV11                           
       READ-TSGV01                    SECTION.                          00015680
      *---------------------------------------                          00015690
           MOVE KOM-ITEM (ICOMM)      TO RANG-TS                        00015710
           STRING 'GV01' EIBTRMID                                               
              DELIMITED BY SIZE        INTO IDENT-TS                            
           END-STRING                                                           
           MOVE LENGTH OF TS01-DONNEES   TO LONG-TS                     00015730
           PERFORM READ-TS                                              00015740
           MOVE Z-INOUT               TO TS01-DONNEES.                  00015750
       FIN-READ-TSGV01. EXIT.                                           00017180
      *                                                                 00015850
      **** GESTION DE LA TSGV20 : TS D'APPEL A MGV20                            
       DELETE-TS-MGV20                SECTION.                          00015860
      *---------------------------------------                          00015870
           STRING EIBTRMID 'E' 'GV6'                                            
              DELIMITED BY SIZE        INTO IDENT-TS                            
           END-STRING                                                           
           PERFORM DELETE-TS                                            00005280
      *                                                                 00005290
           STRING EIBTRMID 'R' 'GV6'                                            
              DELIMITED BY SIZE        INTO IDENT-TS                            
           END-STRING                                                           
           PERFORM DELETE-TS.                                           00005340
       FIN-DELETE-TS-MGV20. EXIT.                                       00017180
      *                                                                 00015850
       WRITE-TS-MGV20                 SECTION.                          00015860
      *---------------------------------------                          00015870
           MOVE 1                     TO RANG-TS                        00005800
           STRING EIBTRMID 'E' 'GV6'                                            
              DELIMITED BY SIZE        INTO IDENT-TS                            
           END-STRING                                                           
           MOVE ZONE-TS-ENTREE        TO Z-INOUT                        00005780
           MOVE LENGTH OF ZONE-TS-ENTREE TO LONG-TS                     00005790
           PERFORM WRITE-TS.                                            00005810
       FIN-WRITE-TS-MGV20. EXIT.                                        00017180
      *                                                                 00015850
       READ-TS-MGV20                  SECTION.                          00015860
      *---------------------------------------                          00015870
           MOVE 1                     TO RANG-TS                        00005970
           STRING EIBTRMID 'R' 'GV6'                                            
              DELIMITED BY SIZE        INTO IDENT-TS                            
           END-STRING                                                           
           MOVE LENGTH OF ZONE-TS-SORTIE TO LONG-TS                     00005960
           PERFORM READ-TS                                              00005980
           IF TROUVE                                                            
           THEN MOVE Z-INOUT               TO ZONE-TS-SORTIE            00005990
           ELSE INITIALIZE                    ZONE-TS-SORTIE                    
           END-IF.                                                              
       FIN-READ-TS-MGV20. EXIT.                                         00017180
      *                                                                 00015850
