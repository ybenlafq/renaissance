      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. MGV32.                                                       
       AUTHOR. DSA003.                                                          
      ******************************************************************        
      *  F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E   *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  PROGRAMME  : MGV32                                            *        
      *  TITRE      : REMPLISSAGE DE L'ECRAN                           *        
      *                                                                *        
      *               MGV32 EST APPELE PAR LES PROGRAMMES :            *        
      *               TGV01 - TGV10 - TGV14 - TGV16 ET TGV17           *        
      *                                                                *        
      ******************************************************************        
      *     M O D I F I C A T I O N S  /  M A I N T E N A N C E S      *        
      ******************************************************************        
      *  DATE       : 16/03/1999                                       *        
      *  AUTEUR     : DSA003                                           *        
      *  REPAIRE    : NEM                                              *        
      *  OBJET      : ENCHAINEMENT POUR MAGASIN NEM OU MAGASIN NON NEM *        
      ******************************************************************        
      *  DATE       : 17/01/2011                                       *        
      *  AUTEUR     : FT                                               *        
      *  REPERE     : V34R4                                            *        
      *  OBJET      : INTERDIRE MODIF VENTE EN COURS EXPEDITION        *        
      ******************************************************************        
      *  MODIFICATION                                                  *  416104
V41R0 *  J. BICHY   :      08/02/2011                                  *  416204
      *  AJOUT KIALA                                                   *  416304
      ******************************************************************  416404
INTERF*  OBJET      : TRAITEMENT VENTES INTERFILIALES                  *        
      * 23/06/2016  :                                                  *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       DATA DIVISION.                                                           
      ******************************************************************        
      *                         W O R K I N G                          *        
      ******************************************************************        
       WORKING-STORAGE SECTION.                                                 
       01  FILLER                    PIC X(16) VALUE '*** WORKING  ***'.        
       01  WS-PVTOTAL                PIC Z(8)9             VALUE ZEROES.        
V34R4  01  WS-EXPED                  PIC X.                                     
       01  FILLER                    PIC X(16) VALUE '***  INDICE  ***'.        
      *01  ICOMM                     PIC 9(02)             VALUE ZEROES.        
       01  IMAP                      PIC 9(02)             VALUE ZEROES.        
           88 FIN-BOUCLE-IMAP                     VALUE 09 THRU 99.             
      *                                                               *         
       01  FILLER                    PIC X(16) VALUE '*****  TS  *****'.        
      **** DESCRIPTION DE LA TSGV01 - LIGNES DE VENTES                          
           COPY TSGV01.                                                         
      **** DESCRIPTION DE LA TSGV30                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS30-LONG                 PIC S9(4) COMP        VALUE  +2000.        
      *--                                                                       
       01  TS30-LONG                 PIC S9(4) COMP-5        VALUE              
                                                                  +2000.        
      *}                                                                        
       01  FILLER                    PIC X(16) VALUE '*** MAP GV01 ***'.        
           COPY EGV01                REPLACING EGV01I BY Z-MAP.                 
       01  FILLER                    PIC X(16) VALUE '*** COMMAREA ***'.        
           COPY COMMGV00.                                                       
           COPY SYKWCOMM.                                                       
           COPY SYKWSTAR.                                                       
       01  Z-COMMAREA-LINK           PIC X(163).                                
           COPY COMMDATC.                                                       
       01  FILLER                    PIC X(16) VALUE '** AIDA ZONES **'.        
           COPY SYKWDIV0.                                                       
           COPY SYKWEIB0.                                                       
           COPY SYKWECRA.                                                       
           COPY SYKWZINO.                                                       
           COPY SYKWERRO.                                                       
       01  FILLER                    PIC X(16) VALUE '* ZONES MODULE *'.        
           COPY SYKWDATE.                                                       
           COPY SYKWMONT.                                                       
           COPY WGV00.                                                          
V41R0      COPY             WKTENVOI.                                           
      *                                                                         
interf     COPY TSGVVI.                                                         
      *                                                                         
       LINKAGE                        SECTION.                                  
       01  DFHCOMMAREA.                                                 03660000
           05  FILLER                 PIC X(9096).                      03670000
       PROCEDURE DIVISION.                                                      
      *                                                                         
      *****************************************************************         
      *              T R A M E   DU   P R O G R A M M E               *         
      *****************************************************************         
      *                                                                         
       MODULE-MGV32                    SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           PERFORM MODULE-ENTREE                                                
           PERFORM MODULE-TRAITEMENT                                            
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
       FIN-MODULE-MGV32. EXIT.                                                  
      *                                                                         
      *------------------------------------------------ STRUCTURE ---*          
      *--------------------- NIVEAU 1 -------------------------------*          
      *                                                                         
       MODULE-TRAITEMENT               SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
                    COMPUTE ICOMM = (KOM-PAGE * 8) - 7                          
           PERFORM  REMPLISSAGE-ZONES-OBLIGATOIRES                              
           PERFORM  REMPLISSAGE-ZONES-ENTETE                                    
V34R4      MOVE 'N' TO WS-EXPED                                                 
           PERFORM  VARYING  IMAP  FROM 1 BY 1  UNTIL  FIN-BOUCLE-IMAP          
              PERFORM INITIALISER-LIGNE                                         
              IF KOM-LIGNE-INEXISTANTE (ICOMM)                                  
                 IF COMM-CONSULTATION-VENTE                                     
V34R4            OR COMM-VENTE-EN-COURS-EXPEDITION                              
                    MOVE 'NNNNNNNNNNNNN'  TO                            00009720
                          KOM-ZONE-LIBRE-EN-SAISIE (ICOMM)              00009720
                 ELSE                                                           
                    MOVE 'OOOOOONOOOOOO'  TO                                    
                          KOM-ZONE-LIBRE-EN-SAISIE (ICOMM)                      
                 END-IF                                                         
              ELSE                                                              
                 PERFORM CHARGER-LIGNE                                          
              END-IF                                                            
              ADD  1                   TO ICOMM                                 
           END-PERFORM.                                                         
      *                                                                         
       FIN-MODULE-TRAITEMENT. EXIT.                                             
      *                                                                         
      *--------------------- NIVEAU 2 -------------------------------*          
      *                                                                         
       CHARGER-LIGNE                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
                    PERFORM READ-TSGV01                                         
           PERFORM  CHARGER-LIGNE-TOUT-TYPE                                     
           EVALUATE TRUE                                                        
              WHEN  KOM-LIGNE-VENTE(ICOMM)                                      
                OR  KOM-LIGNE-REPRISE(ICOMM)                                    
                    PERFORM CHARGER-LIGNE-VENTE-REPRISE                         
              WHEN  KOM-LIGNE-RACHAT-PSE(ICOMM)                                 
                OR  KOM-LIGNE-GARANTIE(ICOMM)                                   
                    MOVE TS01-MPVUNIT-X         TO MPVUNITI(IMAP)               
           END-EVALUATE.                                                        
      *                                                                         
       FIN-CHARGER-LIGNE. EXIT.                                                 
      *                                                                         
      *----------------------------------------------- TRAITEMENT ---*          
      *--------------------------------------------------------------*          
      *                                                                 03680000
       MODULE-ENTREE                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           PERFORM INIT-HANDLE.                                                 
       E01-COPY SECTION. CONTINUE. COPY SYKCHAND.                               
      *                                                                         
           EXEC  CICS  HANDLE  CONDITION                                03820000
                       ITEMERR (ABANDON-CICS)                           03830000
                       QIDERR  (ABANDON-CICS)                           03840000
           END-EXEC                                                     03850000
           MOVE DFHCOMMAREA            TO Z-COMMAREA                    03800000
           MOVE LOW-VALUE              TO Z-MAP                         03870000
      **** EXEC CICS ENTER TRACEID(001)   FROM(TS30-IDENTIFICATEUR)     03902055
      **** END-EXEC                                                     03903055
           PERFORM READ-TSGV30.                                                 
      *                                                                         
       FIN-MODULE-ENTREE. EXIT.                                                 
      *                                                                         
       REMPLISSAGE-ZONES-OBLIGATOIRES  SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           EVALUATE TRUE                                                        
              WHEN COMM-SAISIE-VENTE                                            
                OR COMM-REPRISE-PRE-RESERVATION                                 
MGD15            IF COMM-GV00-TYPVTE = 'O'                                      
                     MOVE DFH-BLUE  TO MTITREC                                  
                     MOVE DFH-REVRS TO MTITREH                                  
                     MOVE '   SAISIE DE VENTE MGD  ' TO MTITREI                 
                 ELSE                                                           
                     MOVE '     SAISIE  DE  VENTE   ' TO MTITREI                
                 END-IF                                                         
                 IF MLIBERRI = SPACES OR LOW-VALUE MOVE                         
           'ENTER:CTL FP5:ENCAI FP6:VALID+BC FP3/4:RETOUR FP7/8:PAGIN'          
                                       TO MLIBERRI                              
                 END-IF                                                         
              WHEN COMM-SAISIE-PRE-RESERVATION                                  
MGD15            IF COMM-GV00-TYPVTE = 'O'                                      
                    MOVE DFH-BLUE  TO MTITREC                                   
                    MOVE DFH-REVRS TO MTITREH                                   
                    MOVE '   PRE-RESERVATION MGD   ' TO MTITREI                 
                 ELSE                                                           
                    MOVE '      PRE-RESERVATION    ' TO MTITREI                 
                 END-IF                                                         
                 IF MLIBERRI = SPACES OR LOW-VALUE MOVE                         
           'ENTER:CTL  FP5:VALIDATION  FP3/4:RETOUR  FP7/8:PAGINATION'          
                                       TO MLIBERRI                              
                 END-IF                                                         
              WHEN COMM-SAISIE-INEXISTANTE                                      
MGD15            IF COMM-GV00-TYPVTE = 'O'                                      
                    MOVE DFH-BLUE  TO MTITREC                                   
                    MOVE DFH-REVRS TO MTITREH                                   
                    MOVE ' SAISIE VENTE INEXISTANTE MGD ' TO MTITREI            
                 ELSE                                                           
                    MOVE ' SAISIE VENTE INEXISTANTE' TO MTITREI                 
                 END-IF                                                         
                 IF MLIBERRI = SPACES OR LOW-VALUE MOVE                         
           'ENTER:CTL  FP5:VALIDATION  FP3/4:RETOUR  FP7/8:PAGINATION'          
                                       TO MLIBERRI                              
                 END-IF                                                         
              WHEN COMM-MODIFICATION-VENTE                                      
MGD15            IF COMM-GV00-TYPVTE = 'O'                                      
                    MOVE DFH-BLUE  TO MTITREC                                   
                    MOVE DFH-REVRS TO MTITREH                                   
                    MOVE 'MODIFICATION DE VENTE MGD ' TO MTITREI                
                 ELSE                                                           
                    MOVE '  MODIFICATION  DE  VENTE' TO MTITREI                 
                 END-IF                                                         
                 IF MLIBERRI = SPACES OR LOW-VALUE                              
                    MOVE                                                        
           'ENTER:CTL  FP5:VALIDATION  FP3/4:RETOUR  FP7/8:PAGINATION'          
                                       TO MLIBERRI                              
                 END-IF                                                         
              WHEN COMM-MODIFICATION-INEXISTANTE                                
                 MOVE '  MODIF VENTE INEXISTANTE' TO MTITREI                    
                 IF MLIBERRI = SPACES OR LOW-VALUE                              
                    MOVE                                                        
           'ENTER:CTL  FP5:VALIDATION  FP3/4:RETOUR  FP7/8:PAGINATION'          
                                       TO MLIBERRI                              
                 END-IF                                                         
              WHEN COMM-CONSULTATION-VENTE                                      
MGD15            IF COMM-GV00-TYPVTE = 'O'                                      
                    MOVE DFH-BLUE  TO MTITREC                                   
                    MOVE DFH-REVRS TO MTITREH                                   
                    MOVE '  CONSULTATION VENTE MGD '  TO MTITREI                
                 ELSE                                                           
                    MOVE '  CONSULTATION  VENTE  '  TO MTITREI                  
                 END-IF                                                         
                 IF MLIBERRI = SPACES OR LOW-VALUE MOVE                         
           'ENTER:CTL  FP5:VALIDATION  FP3/4:RETOUR  FP7/8:PAGINATION'          
                                       TO MLIBERRI                              
                 END-IF                                                         
           END-EVALUATE.                                                        
      *                                                                         
       FIN-REMPLISSAGE-ZONES-OBLIGAT. EXIT.                                     
      *                                                                         
      *****************************************************************         
      *                    REMPLISSAGE ENTETE VENTE                   *         
      *****************************************************************         
      *                                                               *         
      *    CE PARAGRAPHE CONTIENT LE CHARGEMENT DES ZONES ENTETES     *         
      *    IDENTIQUES AUX PROGRAMMES DE VENTE ET AUX FENETRES.        *         
      *                                                               *         
      *    CE PARAGRAPHE EST APPELE DANS LES PROGRAMMES SUIVANTS :    *         
      *       - TGV06 - TGV07 - TGV08  (SAISIE VENTE)                 *         
      *       - TGV46 - TGV47 - TGV48  (CONSULTATION VENTE)           *         
      *       - MGV32  (MODULE DE CHARGEMENT ECRAN)                   *         
      *         MGV32 EST APPELE DANS TGV01 , TGV10 ,  TGV14          *         
      *                               TGV16 , TGV17 ET TGV41.         *         
      *                                                               *         
      *    ATTENTION : CHAQUE MODIFICATION, SUR CE PARAGRAPHE         *         
      *    =========   DANS UN DES PROGRAMMES, DOIT ETRE REPORTEE     *         
      *                DANS TOUS LES AUTRES PROGRAMMES.               *         
      *                                                               *         
      *****************************************************************         
      *                                                                         
       REMPLISSAGE-ZONES-ENTETE       SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** CHARGEMENT DES ZONES ENTETE ET PIED DE VENTE                         
           MOVE KOM-PAGE              TO MWPAGEI                                
           MOVE COMM-GV00-NLIEU       TO MMAGI                                  
           IF COMM-GV00-NVENTE NUMERIC                                          
           THEN MOVE COMM-GV00-NVENTE    TO MNVENTEI                            
           ELSE MOVE SPACES              TO MNVENTEI                            
           END-IF                                                               
           MOVE COMM-GV00-NSOCP       TO MNSOCPI                                
           MOVE COMM-GV00-NLIEUP      TO MNLIEUPI                               
           STRING COMM-GV00-DVENTE(7:2) '/' COMM-GV00-DVENTE(5:2)               
              '/' COMM-GV00-DVENTE(3:2)                                         
              DELIMITED BY SIZE        INTO MDVENTEI                            
           END-STRING                                                           
           MOVE COMM-GV00-NORDRE      TO MNORDREI                               
           MOVE COMM-GV00-CIMPRIM     TO MCIMPRIMI                              
           IF COMM-GV00-DMODIFVTE NOT = SPACES AND LOW-VALUE            04970034
              STRING COMM-GV00-DMODIFVTE(7:2) '/'                               
                 COMM-GV00-DMODIFVTE(5:2) '/' COMM-GV00-DMODIFVTE(3:2)          
                 DELIMITED BY SIZE     INTO MDMODIFI                            
              END-STRING                                                        
           ELSE                                                                 
              MOVE SPACES                TO MDMODIFI                    04970034
           END-IF                                                               
      *                                                                         
           MOVE COMM-GV00-ECMODDEL    TO MEDELI                                 
           MOVE COMM-GV00-EDDELIV     TO MEDATEI                                
           MOVE COMM-GV00-ECPLAGE     TO MEPLI                                  
           MOVE COMM-GV00-CVENDEUR(1) TO MCVEND1I                               
           MOVE COMM-GV00-LVENDEUR(1)    TO MLVEND1I                            
           MOVE COMM-GV00-CVENDEUR(2) TO MCVEND2I                               
           MOVE COMM-GV00-LVENDEUR(2)    TO MLVEND2I                            
           MOVE COMM-GV00-CVENDEUR(3) TO MCVEND3I                               
           MOVE COMM-GV00-LVENDEUR(3)    TO MLVEND3I                            
           MOVE COMM-GV00-CVENDEUR(4) TO MCVEND4I                               
           MOVE COMM-GV00-LVENDEUR(4)    TO MLVEND4I                            
           MOVE COMM-GV00-LCOMVTE1    TO MLCOMM1I                               
           MOVE COMM-GV00-LCOMVTE2    TO MLCOMM2I                               
           MOVE COMM-GV00-LCOMVTE3    TO MLCOMM3I                               
           MOVE COMM-GV00-LCOMVTE4    TO MLCOMM4I                               
interf     IF COMM-GV00-INTERFIL     > SPACE                                    
  "           PERFORM READ-TSGVVI                                               
  "           MOVE TSGVVI-CVENDEURG  TO MCVEND1I                                
  "           MOVE TSGVVI-LVENDEURG  TO MLVEND1I                                
interf     END-If                                                               
           INITIALIZE                    WS-ALPHA                               
           MOVE COMM-GV00-PTTVENTE     TO WS-DECIMAL-7S                         
           PERFORM REMPLISSAGE-ZONE-NUMERIQUE                                   
           PERFORM TRAITEMENT-ZONE-NUMERIQUE                                    
           MOVE WS-ALPHA              TO MQVENTEI                               
           INITIALIZE                    WS-ALPHA                               
           MOVE COMM-GV00-PCOMPT      TO WS-DECIMAL-7S                          
           PERFORM REMPLISSAGE-ZONE-NUMERIQUE                                   
           PERFORM TRAITEMENT-ZONE-NUMERIQUE                                    
           MOVE WS-ALPHA              TO MQCOMPTI                               
           INITIALIZE                    WS-ALPHA                               
           MOVE COMM-GV00-PLIVR       TO WS-DECIMAL-7S                          
           PERFORM REMPLISSAGE-ZONE-NUMERIQUE                                   
           PERFORM TRAITEMENT-ZONE-NUMERIQUE                                    
           MOVE WS-ALPHA              TO MQLIVRI                                
           INITIALIZE                    WS-ALPHA                               
           MOVE COMM-GV00-PDIFFERE    TO WS-DECIMAL-7S                          
           PERFORM REMPLISSAGE-ZONE-NUMERIQUE                                   
           PERFORM TRAITEMENT-ZONE-NUMERIQUE                                    
           MOVE WS-ALPHA              TO MQDIFFI                                
           INITIALIZE                    WS-ALPHA                               
           MOVE COMM-GV00-PRFACT      TO WS-DECIMAL-7S                          
           PERFORM REMPLISSAGE-ZONE-NUMERIQUE                                   
           PERFORM TRAITEMENT-ZONE-NUMERIQUE                                    
           MOVE WS-ALPHA              TO MQRECFI                                
      **** GV10-LAUTORM CONTIENT LE TERMINAL: NE PAS AFFICHER                   
           MOVE SPACES                TO MLAUTORMI                      00029070
      *    MOVE COMM-GV00-NAUTORD     TO MNAUTORDI                              
           MOVE COMM-GV00-WFACTURE    TO MCFACI                                 
           MOVE COMM-GV00-WDETAXEC    TO MCCEEI                                 
           MOVE COMM-GV00-WDETAXEHC   TO MCBLSPI.                               
      **** CETTE ZONE EST RENSEIGNE EN TRAITEMENT AUTOMATIQUE                   
      **** DANS TGV01 TGV10 TGV14 TGV16 ET TGV17                                
      **** MOVE COMM-GV00-WEXPORT     TO MCEXPI.                                
      *                                                                         
       FIN-REMPLISSAGE-ZONES-ENTETE. EXIT.                                      
      *                                                                         
       INITIALISER-LIGNE              SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** REMISE A BLANC DES ZONES DE LA LIGNE DE L' ECRAN                     
           MOVE SPACES                 TO MMQEI(IMAP)                           
                                          MREFMQEI(IMAP)                        
                                          MNCODICI(IMAP)                        
                                          MQTEI(IMAP)                           
                                          MTYPEI(IMAP)                          
                                          MPVUNITI(IMAP)                        
                                          MPVTOTI(IMAP)                         
                                          MDELI(IMAP)                           
                                          MRESERVI(IMAP)                        
                                          MDATEI(IMAP)                          
                                          MPLI(IMAP)                            
                                          MNVENDI(IMAP).                        
      *                                                                         
       FIN-INITIALISER-LIGNE. EXIT.                                             
      *                                                                         
       CHARGER-LIGNE-TOUT-TYPE        SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           MOVE TS01-CMARQ            TO MMQEI(IMAP)                            
           MOVE TS01-LREFFOURN        TO MREFMQEI(IMAP)                         
           MOVE TS01-NCODIC           TO MNCODICI(IMAP)                         
           MOVE TS01-MQTE             TO WS-QUANTITE                            
           MOVE WS-QUANTITE           TO MQTEI(IMAP)                            
           MOVE TS01-MTYPE            TO MTYPEI(IMAP)                           
AL1802*    IF TS01-MPVTOT = ZEROES                                              
     |*       MOVE TS01-MPVTOT           TO WS-PVTOTAL                          
     |*       MOVE WS-PVTOTAL            TO MPVTOTI(IMAP)                       
     |*    ELSE                                                                 
     |*       MOVE TS01-MPVTOT           TO WS-DECIMAL-7S                       
     |*       PERFORM REMPLISSAGE-ZONE-NUMERIQUE                                
     |*       PERFORM TRAITEMENT-ZONE-NUMERIQUE                                 
     |*       MOVE WS-ALPHA              TO MPVTOTI(IMAP)                       
     |*    END-IF                                                               
     |     IF TS01-MPVTOT - TS01-MPRIMECLI = ZEROES                             
     |        COMPUTE WS-PVTOTAL = TS01-MPVTOT - TS01-MPRIMECLI                 
     |        MOVE WS-PVTOTAL            TO MPVTOTI(IMAP)                       
     |     ELSE                                                                 
     |        COMPUTE WS-DECIMAL-7S = TS01-MPVTOT - TS01-MPRIMECLI              
     |        PERFORM REMPLISSAGE-ZONE-NUMERIQUE                                
     |        PERFORM TRAITEMENT-ZONE-NUMERIQUE                                 
     |        MOVE WS-ALPHA              TO MPVTOTI(IMAP)                       
     |     END-IF                                                               
           MOVE TS01-CMODDEL          TO MDELI(IMAP)                            
           MOVE TS01-MRESERV          TO MRESERVI(IMAP)                         
           MOVE TS01-MDATE            TO MDATEI(IMAP)                           
           MOVE TS01-MPL              TO MPLI(IMAP)                             
           MOVE TS01-MNVEND           TO WS-VENDEUR                             
           MOVE WS-VENDEUR            TO MNVENDI(IMAP).                         
V34R4      IF TS01-CTYPENREG = '1'                                              
 '           IF TS01-CMODDEL = 'EMR'                                            
V41R0 *      AND (TS01-MTYPE = 'X' OR TS01-MTYPE = 'P' )                        
    |            SET WC-TSTENVOI-SUITE     TO TRUE                              
    |            PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                  
    |            UNTIL WC-TSTENVOI-FIN                                          
    |               PERFORM LECTURE-TSTENVOI                                    
    |               IF  WC-TSTENVOI-SUITE                                       
    |               AND TSTENVOI-NSOCZP      = '00000'                          
    |               AND TSTENVOI-CTRL(1:1)   = 'A'                              
    |               AND TSTENVOI-WPARAM(4:1) = TS01-MTYPE                       
 '                      MOVE 'O' TO WS-EXPED                                    
    |                   SET WC-TSTENVOI-FIN TO TRUE                             
    |               END-IF                                                      
V41R0            END-PERFORM                                                    
 '           ELSE                                                               
 '              MOVE 'N' TO WS-EXPED                                            
 '           END-IF                                                             
 '         END-IF                                                               
 '         IF COMM-VENTE-EN-COURS-EXPEDITION                                    
V41R0 *    AND ( (TS01-MTYPE = 'X' OR TS01-MTYPE = 'P')                         
    |            SET WC-TSTENVOI-SUITE     TO TRUE                              
    |            PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                  
    |            UNTIL WC-TSTENVOI-FIN                                          
    |               PERFORM LECTURE-TSTENVOI                                    
    |               IF ((WC-TSTENVOI-SUITE                                      
    |               AND TSTENVOI-NSOCZP      = '00000'                          
    |               AND TSTENVOI-CTRL(1:1)   = 'A'                              
    |               AND TSTENVOI-WPARAM(4:1) = TS01-MTYPE )                     
 '                  OR  ( WS-EXPED = 'O' ) )                                    
 '                       MOVE 'NNNNNNNNNNNNN'  TO                       00009720
 '                             KOM-ZONE-LIBRE-EN-SAISIE (ICOMM)         00009720
    |                   SET WC-TSTENVOI-FIN TO TRUE                             
    |               END-IF                                                      
V41R0            END-PERFORM                                                    
 '         END-IF.                                                              
      *                                                                         
       FIN-CHARGER-LIGNE-TOUT-TYPE. EXIT.                                       
      *                                                                         
       CHARGER-LIGNE-VENTE-REPRISE    SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           IF TS01-MPVUNIT = ZEROES OR TS01-MPRIMECLI NOT = ZERO                
              MOVE SPACES                TO MPVUNITI(IMAP)                      
           ELSE                                                                 
              MOVE TS01-MPVUNIT          TO WS-DECIMAL-7S                       
              PERFORM REMPLISSAGE-ZONE-NUMERIQUE                                
              PERFORM TRAITEMENT-ZONE-NUMERIQUE                                 
              MOVE WS-ALPHA              TO MPVUNITI(IMAP)                      
           END-IF.                                                              
      *                                                                         
       FIN-CHARGER-LIGNE-VENTE. EXIT.                                           
      *                                                                         
       MODULE-SORTIE                  SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** PASSAGE DE LA COMMAREA - REECRITURE TS GV30 - RETOUR                 
           PERFORM REWRITE-TSGV30.                                              
           MOVE Z-COMMAREA            TO DFHCOMMAREA.                   04730000
           EXEC CICS RETURN                                             04740000
           END-EXEC.                                                    04740000
           GOBACK.                                                      04750000
      *                                                                         
       FIN-MODULE-SORTIE. EXIT.                                                 
      *                                                                         
      ******************************************************************00001810
      *                     G E S T I O N   T S                        *00001820
      ******************************************************************00001810
      *                                                                         
       READ-TSGV01                    SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           MOVE KOM-ITEM (ICOMM)      TO RANG-TS                                
           STRING 'GV01' EIBTRMID                                               
              DELIMITED BY SIZE        INTO IDENT-TS                            
           END-STRING                                                           
      *    MOVE TS01-LONG             TO LONG-TS                                
           MOVE LENGTH OF TS01-DONNEES          TO LONG-TS                      
           PERFORM READ-TS                                                      
           MOVE Z-INOUT               TO TS01-DONNEES.                          
      *                                                                         
       FIN-READ-TSGV01. EXIT.                                                   
      *                                                                         
       READ-TSGV30                    SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           STRING 'GV32' EIBTRMID                                               
              DELIMITED BY SIZE        INTO IDENT-TS                            
           END-STRING                                                           
           MOVE 1                     TO RANG-TS                                
           MOVE TS30-LONG             TO LONG-TS                                
           PERFORM READ-TS                                                      
           MOVE Z-INOUT               TO Z-MAP.                                 
      *                                                                         
       FIN-READ-TSGV30. EXIT.                                                   
      *                                                                         
       REWRITE-TSGV30                 SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           STRING 'GV32' EIBTRMID                                               
              DELIMITED BY SIZE        INTO IDENT-TS                            
           END-STRING                                                           
           MOVE 1                     TO RANG-TS                                
           MOVE TS30-LONG             TO LONG-TS                                
           MOVE Z-MAP                 TO Z-INOUT                                
           PERFORM REWRITE-TS.                                                  
      *                                                                         
       FIN-REWRITE-TSGV30. EXIT.                                                
      *                                                                         
interf READ-TSGVVI                    SECTION.                                  
           MOVE 1                        TO RANG-TS                             
           STRING 'GVVI' EIBTRMID                                               
              DELIMITED BY SIZE        INTO IDENT-TS                            
           END-STRING                                                           
           MOVE LENGTH OF TSGVVI-DONNEES TO LONG-TS                             
           PERFORM READ-TS                                                      
           IF TROUVE                                                            
              MOVE Z-INOUT               TO TSGVVI-DONNEES                      
           END-IF.                                                              
interf FIN-READ-TSGVVI. EXIT.                                                   
V41R0                                                                           
    | *-----------------------------------------------------------------        
    |  LECTURE-TSTENVOI SECTION.                                                
      *{ normalize-exec-xx 1.5                                                  
    | *    EXEC CICS                                                            
    | *         READQ TS                                                        
    | *         QUEUE  (WC-ID-TSTENVOI)                                         
    | *         INTO   (TS-TSTENVOI-DONNEES)                                    
    | *         LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
    | *         ITEM   (WP-RANG-TSTENVOI)                                       
    | *         NOHANDLE                                                        
    | *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS READQ TS                                                   
                QUEUE  (WC-ID-TSTENVOI)                                         
                INTO   (TS-TSTENVOI-DONNEES)                                    
                LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
                ITEM   (WP-RANG-TSTENVOI)                                       
                NOHANDLE                                                        
           END-EXEC.                                                            
      *}                                                                        
    |                                                                           
    |      EVALUATE EIBRESP                                                     
    |      WHEN 0                                                               
    |         SET WC-TSTENVOI-SUITE      TO TRUE                                
    |         SET WC-XCTRL-EXPDEL-TROUVE TO TRUE                                
    |      WHEN 26                                                              
    |      WHEN 44                                                              
    |         SET WC-TSTENVOI-FIN        TO TRUE                                
    |      WHEN OTHER                                                           
    |         SET WC-TSTENVOI-ERREUR     TO TRUE                                
    |      END-EVALUATE.                                                        
    |                                                                           
    |      IF WC-TSTENVOI-ERREUR                                                
    |         MOVE 'TSTENVOI INVALIDE ' TO MESS                                 
    |         GO   TO ABANDON-CICS                                              
    |      END-IF.                                                              
    |                                                                           
    |  FIN-LECT-TSTENVOI. EXIT.                                                 
    | *                                                                         
V41R0                                                                           
      *                                                                         
      ******************************************************************00001810
      *                    M O D U L E S   A I D A                     *00001820
      ******************************************************************00001810
      *                                                                         
      *DESCRIPTION DES BRIQUES AIDA CICS                                        
       S15-COPY SECTION. CONTINUE. COPY SYKCLINK.                               
      *DESCRIPTION DES BRIQUES AIDA TEMP                                        
       TEMP01-COPY SECTION. CONTINUE. COPY SYKCTSRD.                            
       TEMP02-COPY SECTION. CONTINUE. COPY SYKCTSRW.                            
      *MODULE D'ABANDON                                                         
       99-COPY SECTION. CONTINUE. COPY SYKCERRO.                                
      *DESCRIPTION DES MODULES DE CONTROLES ET DE TRAITEMENTS STANDARDS         
       T06-COPY SECTION. CONTINUE. COPY SYKPMONT.                               
       COPY MGV00.                                                              
      *                                                                         
