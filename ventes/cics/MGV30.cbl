      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. MGV30.                                                       
       AUTHOR. DSA003.                                                          
      ******************************************************************        
      *  F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E   *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  PROGRAMME  : MGV30                                            *        
      *  TITRE      : MODULE DE POSITIONNEMENT DES ATTRIBUTS DE LA MAP *        
      *                                                                *        
      ******************************************************************        
M8656 * 08/02/2012 - L.ARMANT                                          *        
      * EVOLUTION : MANTIS 08656                                       *        
      *  OBJET    : PROTEGER LE CODE VENDEUR POUR LEQUEL UNE LIGNE DE  *        
M8656 *             VENTE EST ASSOCIEE                                 *        
      ******************************************************************        
MGD14 * 04/03/2014 - DE02009                                           *        
      * EVOLUTION : EN CAS DE MODIF DE VENTE MGD, PROT�GER LES ZONES   *        
      *             NSOCP/NLIEUP, FORCER � NSOC/240                    *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       DATA DIVISION.                                                           
      ******************************************************************        
      *                         W O R K I N G                          *        
      ******************************************************************        
       WORKING-STORAGE SECTION.                                                 
       01  FILLER                    PIC X(16) VALUE '*** WORKING  ***'.        
       01  WS-COULEUR-LIB            PIC X                 VALUE SPACE.         
       01  WS-COULEUR-PRO            PIC X                 VALUE SPACE.         
      *                                                                         
       01  FILLER                    PIC X(16) VALUE '***  INDICE  ***'.        
       01  IMAP                      PIC 9(02)             VALUE ZEROES.        
           88 FIN-BOUCLE-IMAP            VALUE 09 THRU 99.                      
      *                                                                         
      **** DESCRIPTION DE LA TSGV30                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS30-LONG                 PIC S9(4) COMP        VALUE +2000.         
      *--                                                                       
       01  TS30-LONG                 PIC S9(4) COMP-5        VALUE              
                                                                  +2000.        
      *}                                                                        
      **** DESCRIPTION DE LA TS CONTENANT LES INFORMATIONS DES          00001910
      **** TABLES OU SOUS-TABLES COMMUNES A TOUS LES MAGASINS           00001920
           COPY TSGV00.                                                         
      *                                                                         
       01  FILLER                    PIC X(16) VALUE '*** MAP GV01 ***'.        
           COPY EGV01                REPLACING EGV01I BY Z-MAP.                 
       01  FILLER                    PIC X(16) VALUE '*** COMMAREA ***'.        
           COPY COMMGV00.                                                       
           COPY SYKWCOMM.                                                       
           COPY SYKWSTAR.                                                       
       01  Z-COMMAREA-LINK           PIC X(163).                                
       01  FILLER                    PIC X(16) VALUE '** AIDA ZONES **'.        
           COPY SYKWDIV0.                                                       
           COPY SYKWEIB0.                                                       
           COPY SYKWECRA.                                                       
           COPY SYKWZINO.                                                       
           COPY SYKWERRO.                                                       
      *                                                                         
       LINKAGE                        SECTION.                                  
       01  DFHCOMMAREA.                                                 03660000
           05  FILLER                 PIC X(9096).                      03670000
       PROCEDURE DIVISION.                                                      
      *                                                                         
      *****************************************************************         
      *              T R A M E   DU   P R O G R A M M E               *         
      *****************************************************************         
      *                                                                         
       MODULE-MGV30                   SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           PERFORM MODULE-ENTREE                                                
           PERFORM MODULE-TRAITEMENT                                            
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
       FIN-MODULE-MGV30. EXIT.                                                  
      *                                                                         
      *------------------------------------------------ STRUCTURE ---*          
      *--------------------- NIVEAU 1 -------------------------------*          
      *                                                                         
       MODULE-TRAITEMENT              SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           PERFORM TRAITEMENT-LIGNES                                            
           IF COMM-CONSULTATION-VENTE                                           
NEM        THEN PERFORM TRAIT-SPE-LIEU-PAIEMENT                                 
           ELSE PERFORM TRAITEMENT-BAS-FACTURE                                  
           END-IF.                                                              
      *                                                                         
       FIN-MODULE-TRAITEMENT. EXIT.                                             
      *                                                                         
       TRAITEMENT-LIGNES              SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           COMPUTE ICOMM = (KOM-PAGE * 8) - 7                                   
           PERFORM VARYING  IMAP  FROM 1 BY 1  UNTIL FIN-BOUCLE-IMAP            
      ****    EN CONSULTATION DE VENTE, LES LG ANNULEES APPARAISSENT            
      ****    EN TURQUOISE, PAS EN BLEU                                         
              IF KOM-LIGNE-ANNULEE(ICOMM) AND                                   
                 NOT COMM-CONSULTATION-VENTE                                    
                 PERFORM TRAITEMENT-LIGNE-ANNULEE                               
              END-IF                                                            
              IF KOM-LIGNE-ACTIVE(ICOMM) OR                                     
              (KOM-LIGNE-ANNULEE(ICOMM) AND COMM-CONSULTATION-VENTE)            
                 PERFORM TRAITEMENT-LIGNE-ACTIVE                                
              END-IF                                                            
              ADD 1                      TO ICOMM                               
           END-PERFORM.                                                         
      *                                                                         
       FIN-TRAITEMENT-LIGNES. EXIT.                                             
      *                                                                         
       TRAITEMENT-BAS-FACTURE         SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           PERFORM TRAITEMENT-CAS-GENERAL                                       
NEM        IF NOT COMM-FONCTION-VENTE                                           
 "            PERFORM TRAIT-SPE-LIEU-PAIEMENT                                   
NEM        END-IF.                                                              
           IF COMM-SAISIE-PRE-RESERVATION                                       
           OR COMM-VENTE-EN-COURS-LIVRAISON                                     
NEM        OR COMM-MODE-DEGRADE-NEM                                             
              PERFORM TRAIT-SPE-MONTANTS-VENTE                                  
           END-IF                                                               
           IF COMM-VENTE-INEXISTANTE                                            
              PERFORM TRAIT-SPE-VENTE-INEXISTANTE                               
           END-IF                                                               
      **** LE MODE DEGRADE SE COMPORTE COMME UNE SAISIE DE VENTE                
      **** POUR LES ZONES TRAITEES DANS CE PARAGRAPHE                           
           IF COMM-SAISIE-VENTE                                                 
           OR COMM-REPRISE-PRE-RESERVATION                                      
NEM        OR COMM-MODE-DEGRADE-NEM                                             
              PERFORM TRAIT-SPE-SAISIE-VENTE                                    
           END-IF                                                               
           IF COMM-SAISIE-VENTE                                                 
           OR COMM-SAISIE-PRE-RESERVATION                                       
           OR COMM-SAISIE-INEXISTANTE                                           
           OR COMM-REPRISE-PRE-RESERVATION                                      
NEM        OR COMM-MODE-DEGRADE-NEM                                             
              PERFORM TRAIT-SPE-SAISIE-OU-REPRISE                               
           END-IF                                                               
           IF (COMM-MODIFICATION-VENTE OR COMM-MODIFICATION-INEXISTANTE)        
NEM        AND NOT COMM-MODE-DEGRADE-NEM                                        
              PERFORM TRAIT-SPE-MODIFICATION                                    
           END-IF                                                               
M8656      IF COMM-REPRISE-PRE-RESERVATION                                      
M8656         PERFORM TRAIT-SPE-PRE-RESERV                                      
M8656      END-IF                                                               
           IF NOT COMM-SAISIE-PRE-RESERVATION                                   
              PERFORM TRAIT-SPE-AUTRE-QUE-PRE-RESERV                            
           END-IF.                                                              
      * POUR LES VENTES MGD:                                                    
      *  -   PROT�GER LES ZONES NSOCP/NLIEUP                                    
      *  -   FORCER LE NSOCP/NLIEUP A NSOCIETE/NLIEU DE VENTE                   
MGD14      IF COMM-GV00-MGD = 'M'                                               
 "            MOVE NOR-PRO-FSET          TO MNSOCPA                             
 "            MOVE DFH-TURQ              TO MNSOCPC                             
 "            MOVE DFH-BASE              TO MNSOCPH                             
 "            MOVE NOR-PRO-FSET          TO MNLIEUPA                            
 "            MOVE DFH-TURQ              TO MNLIEUPC                            
 "            MOVE DFH-BASE              TO MNLIEUPH                            
 "            MOVE COMM-GV00-NSOCIETE    TO COMM-GV00-NSOCP                     
 "            MOVE COMM-GV00-NLIEU       TO COMM-GV00-NLIEUP                    
MGD14      END-IF.                                                              
      *                                                                         
       FIN-TRAITEMENT-BAS-FACTURE. EXIT.                                        
      *                                                                         
      *----------------------------------------------- TRAITEMENT ---*          
      *--------------------------------------------------------------*          
      *                                                                 03680000
       MODULE-ENTREE                  SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           PERFORM INIT-HANDLE.                                                 
       E01-COPY SECTION. CONTINUE. COPY SYKCHAND.                               
           EXEC  CICS  HANDLE  CONDITION                                03820000
                       ITEMERR (ABANDON-CICS)                           03830000
                       QIDERR  (ABANDON-CICS)                           03840000
           END-EXEC                                                     03850000
           MOVE DFHCOMMAREA           TO Z-COMMAREA                     03800000
           PERFORM READ-TSGV30.                                                 
      *                                                                         
       FIN-MODULE-ENTREE. EXIT.                                                 
      *                                                                         
       TRAITEMENT-LIGNE-ANNULEE       SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      ****  LES ATTRIBUTS SONT POSITIONNES EN FONCTION DE LA                    
      ****  VALEUR DU TOP ATTRIBUT POUR CHACUNE DES ZONES DE LA MAP.            
      **** -SI LA VALEUR EST 'O' ALORS LA ZONE EST DEPROTEGEE                   
      **** -SI LA VALEUR EST 'N' ALORS LA ZONE EST PROTEGEE                     
      *                                                                         
      ****  CES TOPS SONT POSITIONNES DANS :                                    
      ****      MGV31 - CHARGEMENT DES DONNEES                                  
      ****      MGV33 - CONTROLE DES DEMANDES                                   
      ****      MGV35 - FORMATAGE ET SAUVEGARDE DE L' ECRAN                     
      ****      MGV36 - CONTROLE SYNTAXIQUE                                     
      *                                                                         
      ****  POSITIONNEMENT DES ATTRIBUTS EN PROTEGE BLEU                        
      ****- POUR LES LIGNES ANNULEES CONSECUTIVEMENT A UNE                      
      ****  DEMANDE D'ANNULATION D' UNE LIGNE TOPEE LIVREE                      
      ****  OU UNE LIGNE DEJA ANNULEE                                           
           MOVE NOR-PRO-FSET          TO MMQEA   (IMAP)                         
           MOVE DFH-BLUE              TO MMQEC   (IMAP)                         
           MOVE DFH-BASE              TO MMQEH   (IMAP)                         
           MOVE NOR-PRO-FSET          TO MREFMQEA(IMAP)                         
           MOVE DFH-BLUE              TO MREFMQEC(IMAP)                         
           MOVE DFH-BASE              TO MREFMQEH(IMAP)                         
           MOVE NOR-PRO-FSET          TO MNCODICA(IMAP)                         
           MOVE DFH-BLUE              TO MNCODICC(IMAP)                         
           MOVE DFH-BASE              TO MNCODICH(IMAP)                         
           MOVE NOR-PRO-FSET          TO MQTEA   (IMAP)                         
           MOVE DFH-BLUE              TO MQTEC   (IMAP)                         
           MOVE DFH-BASE              TO MQTEH   (IMAP)                         
           MOVE NOR-PRO-FSET          TO MTYPEA  (IMAP)                         
           MOVE DFH-BLUE              TO MTYPEC  (IMAP)                         
           MOVE DFH-BASE              TO MTYPEH  (IMAP)                         
           MOVE NOR-PRO-FSET          TO MPVUNITA(IMAP)                         
           MOVE DFH-BLUE              TO MPVUNITC(IMAP)                         
           MOVE DFH-BASE              TO MPVUNITH(IMAP)                         
           MOVE NOR-PRO-FSET          TO MPVTOTA (IMAP)                         
           MOVE DFH-BLUE              TO MPVTOTC (IMAP)                         
           MOVE DFH-BASE              TO MPVTOTH (IMAP)                         
           MOVE NOR-PRO-FSET          TO MDELA   (IMAP)                         
           MOVE DFH-BLUE              TO MDELC   (IMAP)                         
           MOVE DFH-BASE              TO MDELH   (IMAP)                         
           MOVE NOR-PRO-FSET          TO MRESERVA(IMAP)                         
           MOVE DFH-BLUE              TO MRESERVC(IMAP)                         
           IF KOM-LIGNE-ANNULEE(ICOMM) AND                                      
             (KOM-LIGNE-VENTE(ICOMM)                                            
           OR KOM-LIGNE-REPRISE(ICOMM)                                          
           OR KOM-LIGNE-RACHAT-PSE(ICOMM))                                      
              MOVE DFH-REVRS             TO MRESERVH(IMAP)                      
           ELSE                                                                 
              MOVE DFH-BASE              TO MRESERVH(IMAP)                      
           END-IF                                                               
           MOVE NOR-PRO-FSET          TO MDATEA (IMAP)                          
           MOVE DFH-BLUE              TO MDATEC (IMAP)                          
           MOVE DFH-BASE              TO MDATEH (IMAP)                          
           MOVE NOR-PRO-FSET          TO MPLA   (IMAP)                          
           MOVE DFH-BLUE              TO MPLC   (IMAP)                          
           MOVE DFH-BASE              TO MPLH   (IMAP)                          
           MOVE NOR-PRO-FSET          TO MNVENDA(IMAP)                          
           MOVE DFH-BLUE              TO MNVENDC(IMAP)                          
           MOVE DFH-BASE              TO MNVENDH(IMAP).                         
      *                                                                         
       FIN-TRAITEMENT-LIGNE-ANNULEE. EXIT.                                      
      *                                                                         
       TRAITEMENT-LIGNE-ACTIVE        SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      ****  POSITIONNEMENT DES ATTRIBUTS POUR LES LIGNES ACTIVES                
      **** (NON ANNULEES SAUF CONSULTATION DE VENTE)                            
      ****  LES TOPS ATTRIBUTS SONT POSITIONNES SELON L' ETAT                   
      ****  DE LA LIGNE :                                                       
      ****  - LIGNE TOPEE LIVREE : LIGNE PROTEGEE OU EN LIVRAISON               
      ****  - LIGNE NON TOPEE LIVREE : LIGNE DEPROTEGEE                         
      *                                                                         
      ****  COULEURS POUR LES ZONES LIBRES    :                                 
      ****     - ROSE  : CODIC EMD MUTE EN MAGASIN                              
      ****     - BLANC : CODIC-ELEMENT NON TOPE LIVRE                           
      ****     - JAUNE : AUTRES CAS                                             
           IF KOM-MUTE(ICOMM)                                                   
           THEN MOVE DFH-PINK            TO WS-COULEUR-LIB                      
           ELSE                                                                 
              IF KOM-ART-CODIC-GROUPE(ICOMM)                                    
              THEN MOVE DFH-NEUTR           TO WS-COULEUR-LIB                   
              ELSE MOVE DFH-YELLO           TO WS-COULEUR-LIB                   
              END-IF                                                            
           END-IF                                                               
      **** COULEURS POUR LES ZONES PROTEGEES :                                  
      ****    - ROSE      : CODIC EMD MUTE EN MAGASIN                           
      ****    - TURQUOISE : AUTRES CAS                                          
           IF KOM-MUTE(ICOMM)                                                   
           THEN MOVE DFH-PINK            TO WS-COULEUR-PRO                      
           ELSE MOVE DFH-TURQ            TO WS-COULEUR-PRO                      
           END-IF                                                               
           MOVE NOR-PRO-FSET          TO MMQEA(IMAP)                            
           MOVE DFH-TURQ              TO MMQEC(IMAP)                            
           MOVE DFH-BASE              TO MMQEH(IMAP)                            
           MOVE NOR-PRO-FSET          TO MREFMQEA(IMAP)                         
           MOVE DFH-TURQ              TO MREFMQEC(IMAP)                         
           MOVE DFH-BASE              TO MREFMQEH(IMAP)                         
           IF KOM-CODIC-LIBRE(ICOMM)                                            
              MOVE NOR-ALP-FSET          TO MNCODICA(IMAP)                      
              MOVE WS-COULEUR-LIB        TO MNCODICC(IMAP)                      
              MOVE DFH-UNDLN             TO MNCODICH(IMAP)                      
           ELSE                                                                 
              MOVE NOR-PRO-FSET          TO MNCODICA(IMAP)                      
              MOVE WS-COULEUR-PRO        TO MNCODICC(IMAP)                      
              MOVE DFH-BASE              TO MNCODICH(IMAP)                      
           END-IF                                                               
           IF KOM-TYPE-LIBRE(ICOMM)                                             
              MOVE NOR-ALP-FSET          TO MTYPEA(IMAP)                        
              MOVE WS-COULEUR-LIB        TO MTYPEC(IMAP)                        
              MOVE DFH-UNDLN             TO MTYPEH(IMAP)                        
           ELSE                                                                 
              MOVE NOR-PRO-FSET          TO MTYPEA(IMAP)                        
              MOVE WS-COULEUR-PRO        TO MTYPEC(IMAP)                        
              MOVE DFH-BASE              TO MTYPEH(IMAP)                        
           END-IF                                                               
           IF KOM-QTE-LIBRE(ICOMM)                                              
              MOVE NOR-NUM-FSET          TO MQTEA(IMAP)                         
              MOVE WS-COULEUR-LIB        TO MQTEC(IMAP)                         
              MOVE DFH-UNDLN             TO MQTEH(IMAP)                         
           ELSE                                                                 
              MOVE NOR-PRO-FSET          TO MQTEA(IMAP)                         
              MOVE WS-COULEUR-PRO        TO MQTEC(IMAP)                         
              MOVE DFH-BASE              TO MQTEH(IMAP)                         
           END-IF                                                               
           IF KOM-PVUNIT-LIBRE(ICOMM)                                           
              MOVE NOR-ALP-FSET          TO MPVUNITA(IMAP)                      
              MOVE WS-COULEUR-LIB        TO MPVUNITC(IMAP)                      
              MOVE DFH-UNDLN             TO MPVUNITH(IMAP)                      
           ELSE                                                                 
              MOVE NOR-PRO-FSET          TO MPVUNITA(IMAP)                      
              MOVE WS-COULEUR-PRO        TO MPVUNITC(IMAP)                      
              MOVE DFH-BASE              TO MPVUNITH(IMAP)                      
           END-IF                                                               
           IF KOM-PVTOTAL-LIBRE(ICOMM)                                          
              MOVE NOR-NUM-FSET          TO MPVTOTA(IMAP)                       
              MOVE WS-COULEUR-LIB        TO MPVTOTC(IMAP)                       
              MOVE DFH-UNDLN             TO MPVTOTH(IMAP)                       
           ELSE                                                                 
              MOVE NOR-PRO-FSET          TO MPVTOTA(IMAP)                       
              MOVE WS-COULEUR-PRO        TO MPVTOTC(IMAP)                       
              MOVE DFH-BASE              TO MPVTOTH(IMAP)                       
           END-IF                                                               
           IF KOM-DEL-LIBRE(ICOMM)                                              
              MOVE NOR-ALP-FSET          TO MDELA(IMAP)                         
              MOVE WS-COULEUR-LIB        TO MDELC(IMAP)                         
              MOVE DFH-UNDLN             TO MDELH(IMAP)                         
           ELSE                                                                 
              MOVE NOR-PRO-FSET          TO MDELA(IMAP)                         
              MOVE WS-COULEUR-PRO        TO MDELC(IMAP)                         
              MOVE DFH-BASE              TO MDELH(IMAP)                         
           END-IF                                                               
           MOVE NOR-PRO-FSET          TO MRESERVA(IMAP)                         
           MOVE DFH-TURQ              TO MRESERVC(IMAP)                         
           IF KOM-TOPE-LIVRE(ICOMM)                                             
              IF KOM-LIGNE-VENTE(ICOMM)                                         
              OR KOM-LIGNE-REPRISE(ICOMM)                                       
              OR KOM-LIGNE-RACHAT-PSE(ICOMM)                                    
                 MOVE DFH-REVRS             TO MRESERVH(IMAP)                   
              ELSE                                                              
                 MOVE DFH-BASE              TO MRESERVH(IMAP)                   
              END-IF                                                            
           END-IF                                                               
           IF KOM-DATE-LIBRE(ICOMM)                                             
              MOVE NOR-NUM-FSET          TO MDATEA(IMAP)                        
              MOVE WS-COULEUR-LIB        TO MDATEC(IMAP)                        
              MOVE DFH-UNDLN             TO MDATEH(IMAP)                        
           ELSE                                                                 
              MOVE NOR-PRO-FSET          TO MDATEA(IMAP)                        
              MOVE WS-COULEUR-PRO        TO MDATEC(IMAP)                        
              MOVE DFH-BASE              TO MDATEH(IMAP)                        
           END-IF                                                               
           IF KOM-PLAGE-LIBRE(ICOMM)                                            
              MOVE NOR-ALP-FSET          TO MPLA(IMAP)                          
              MOVE WS-COULEUR-LIB        TO MPLC(IMAP)                          
              MOVE DFH-UNDLN             TO MPLH(IMAP)                          
           ELSE                                                                 
              MOVE NOR-PRO-FSET          TO MPLA(IMAP)                          
              MOVE WS-COULEUR-PRO        TO MPLC(IMAP)                          
              MOVE DFH-BASE              TO MPLH(IMAP)                          
           END-IF                                                               
           IF KOM-VENDEUR-LIBRE(ICOMM)                                          
              MOVE NOR-ALP-FSET          TO MNVENDA(IMAP)                       
              MOVE WS-COULEUR-LIB        TO MNVENDC(IMAP)                       
              MOVE DFH-UNDLN             TO MNVENDH(IMAP)                       
           ELSE                                                                 
              MOVE NOR-PRO-FSET          TO MNVENDA(IMAP)                       
              MOVE WS-COULEUR-PRO        TO MNVENDC(IMAP)                       
              MOVE DFH-BASE              TO MNVENDH(IMAP)                       
           END-IF.                                                              
      *                                                                         
       FIN-TRAITEMENT-LIGNE-ACTIVE. EXIT.                                       
      *                                                                         
       TRAITEMENT-CAS-GENERAL         SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
NEM        MOVE NOR-ALP-FSET          TO MNSOCPA                                
 "         MOVE DFH-YELLO             TO MNSOCPC                                
 "         MOVE DFH-UNDLN             TO MNSOCPH                                
 "         MOVE NOR-ALP-FSET          TO MNLIEUPA                               
 "         MOVE DFH-YELLO             TO MNLIEUPC                               
 "         MOVE DFH-UNDLN             TO MNLIEUPH                               
 "         MOVE NOR-ALP-FSET          TO MCIMPRIMA                              
 "         MOVE DFH-YELLO             TO MCIMPRIMC                              
NEM        MOVE DFH-UNDLN             TO MCIMPRIMH                              
           MOVE NOR-ALP-FSET          TO MEDELA                                 
           MOVE DFH-YELLO             TO MEDELC                                 
           MOVE DFH-UNDLN             TO MEDELH                                 
           MOVE NOR-ALP-FSET          TO MEDATEA                                
           MOVE DFH-YELLO             TO MEDATEC                                
           MOVE DFH-UNDLN             TO MEDATEH                                
           MOVE NOR-ALP-FSET          TO MEPLA                                  
           MOVE DFH-YELLO             TO MEPLC                                  
           MOVE DFH-UNDLN             TO MEPLH                                  
           MOVE NOR-ALP-FSET          TO MCVEND1A                               
           MOVE DFH-YELLO             TO MCVEND1C                               
           MOVE DFH-UNDLN             TO MCVEND1H                               
           MOVE NOR-ALP-FSET          TO MCVEND2A                               
           MOVE DFH-YELLO             TO MCVEND2C                               
           MOVE DFH-UNDLN             TO MCVEND2H                               
           MOVE NOR-ALP-FSET          TO MCVEND3A                               
           MOVE DFH-YELLO             TO MCVEND3C                               
           MOVE DFH-UNDLN             TO MCVEND3H                               
           MOVE NOR-ALP-FSET          TO MCVEND4A                               
           MOVE DFH-YELLO             TO MCVEND4C                               
           MOVE DFH-UNDLN             TO MCVEND4H                               
           MOVE NOR-ALP-FSET          TO MLAUTORMA                              
           MOVE DFH-YELLO             TO MLAUTORMC                              
           MOVE DFH-UNDLN             TO MLAUTORMH                              
           MOVE NOR-ALP-FSET          TO MNAUTORDA                              
           MOVE DFH-YELLO             TO MNAUTORDC                              
           MOVE DFH-UNDLN             TO MNAUTORDH                              
           MOVE NOR-PRO-FSET          TO MQVENTEA                               
           MOVE DFH-TURQ              TO MQVENTEC                               
           MOVE DFH-BASE              TO MQVENTEH                               
           MOVE NOR-NUM-FSET          TO MQCOMPTA                               
           MOVE DFH-YELLO             TO MQCOMPTC                               
           MOVE DFH-UNDLN             TO MQCOMPTH                               
           MOVE NOR-NUM-FSET          TO MQLIVRA                                
           MOVE DFH-YELLO             TO MQLIVRC                                
           MOVE DFH-UNDLN             TO MQLIVRH                                
           MOVE NOR-NUM-FSET          TO MQDIFFA                                
           MOVE DFH-YELLO             TO MQDIFFC                                
           MOVE DFH-UNDLN             TO MQDIFFH                                
           MOVE NOR-NUM-FSET          TO MQRECFA                                
           MOVE DFH-YELLO             TO MQRECFC                                
           MOVE DFH-UNDLN             TO MQRECFH                                
           MOVE NOR-ALP-FSET          TO MCFACA                                 
           MOVE DFH-YELLO             TO MCFACC                                 
           MOVE DFH-BASE              TO MCFACH                                 
           MOVE NOR-ALP-FSET          TO MCBLSPA.                               
           MOVE DFH-YELLO             TO MCBLSPC.                               
           MOVE DFH-BASE              TO MCBLSPH.                               
      *                                                                         
       FIN-TRAITEMENT-CAS-GENERAL. EXIT.                                        
      *                                                                         
NEM    TRAIT-SPE-LIEU-PAIEMENT        SECTION.                                  
 "    *---------------------------------------                                  
 "    *                                                                         
 "    **** LES ZONES LIEU DE PAIEMENT SONT PROTEGEES                            
 "    **** POUR LES FONCTIONS AUTRES QUE LA SAISIE/MODIF DE VENTE               
 "    **** (VENTE INEXISTANTE, PRE-RESERVATION, MODE DEGRADE)                   
 "         MOVE DRK-PRO-FSET          TO MNSOCPA                                
 "         MOVE DFH-NEUTR             TO MNSOCPC                                
 "         MOVE DFH-BASE              TO MNSOCPH                                
 "         MOVE DRK-PRO-FSET          TO MNLIEUPA                               
 "         MOVE DFH-NEUTR             TO MNLIEUPC                               
 "         MOVE DFH-BASE              TO MNLIEUPH                               
 "         MOVE DRK-PRO-FSET          TO MCIMPRIMA                              
 "         MOVE DFH-NEUTR             TO MCIMPRIMC                              
 "         MOVE DFH-BASE              TO MCIMPRIMH.                             
 "    *                                                                         
NEM    FIN-TRAIT-SPE-LIEU-PAIEMENT. EXIT.                                       
      *                                                                         
       TRAIT-SPE-MONTANTS-VENTE       SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** LES ZONES MTS A ENCAISSER SONT PROTEGEES POUR LES FONCTIONS :        
      **** - SAISIE PRE-RESERVATION                                             
      **** - VENTE EST EN COURS DE LIVRAISON                                    
      **** - MODE DEGRADE NEM                                                   
           MOVE NOR-PRO-FSET          TO MQCOMPTA                               
           MOVE DFH-TURQ              TO MQCOMPTC                               
           MOVE DFH-BASE              TO MQCOMPTH                               
           MOVE NOR-PRO-FSET          TO MQLIVRA                                
           MOVE DFH-TURQ              TO MQLIVRC                                
           MOVE DFH-BASE              TO MQLIVRH                                
           MOVE NOR-PRO-FSET          TO MQDIFFA                                
           MOVE DFH-TURQ              TO MQDIFFC                                
           MOVE DFH-BASE              TO MQDIFFH                                
           MOVE NOR-PRO-FSET          TO MQRECFA                                
           MOVE DFH-TURQ              TO MQRECFC                                
           MOVE DFH-BASE              TO MQRECFH.                               
      *                                                                         
       FIN-TRAIT-SPE-MONTANTS-VENTE. EXIT.                                      
      *                                                                         
       TRAIT-SPE-VENTE-INEXISTANTE    SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** TRAITEMENT SPECIFIQUE AUX FONCTIONS :                                
      **** - SAISIE VENTE INEXISTANTE                                           
      **** - MODIFICATION VENTE INEXISTANTE                                     
           MOVE NOR-PRO-FSET          TO MLAUTORMA                              
           MOVE DFH-TURQ              TO MLAUTORMC                              
           MOVE DFH-BASE              TO MLAUTORMH                              
           MOVE NOR-PRO-FSET          TO MNAUTORDA                              
           MOVE DFH-TURQ              TO MNAUTORDC                              
           MOVE DFH-BASE              TO MNAUTORDH                              
           MOVE NOR-PRO-FSET          TO MQLIVRA                                
           MOVE DFH-TURQ              TO MQLIVRC                                
           MOVE DFH-BASE              TO MQLIVRH                                
           MOVE NOR-PRO-FSET          TO MQDIFFA                                
           MOVE DFH-TURQ              TO MQDIFFC                                
           MOVE DFH-BASE              TO MQDIFFH                                
           MOVE NOR-PRO-FSET          TO MQRECFA                                
           MOVE DFH-TURQ              TO MQRECFC                                
           MOVE DFH-BASE              TO MQRECFH.                               
      *                                                                         
       FIN-TRAIT-SPE-VENTE-INEXIST. EXIT.                                       
      *                                                                         
       TRAIT-SPE-SAISIE-VENTE         SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** TRAITEMENT SPECIFIQUE AUX FONCTIONS :                                
      **** - SAISIE VENTE                                                       
      **** - REPRISE PRE-RESERVATION                                            
           MOVE NOR-ALP-FSET          TO MCEXPA                                 
           MOVE DFH-YELLO             TO MCEXPC                                 
           MOVE DFH-BASE              TO MCEXPH.                                
      *                                                                         
       FIN-TRAIT-SPE-SAISIE-VENTE. EXIT.                                        
      *                                                                         
       TRAIT-SPE-SAISIE-OU-REPRISE    SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** TRAITEMENT SPECIFIQUE AUX FONCTIONS :                                
      **** - SAISIE VENTE                                                       
      **** - SAISIE PRE-RESERVATION                                             
      **** - SAISIE VENTE INEXISTANTE                                           
      **** - REPRISE PRE-RESERVATION                                            
           MOVE NOR-PRO-FSET          TO MDVENTEA                               
           MOVE DFH-TURQ              TO MDVENTEC                               
           MOVE DFH-BASE              TO MDVENTEH                               
           MOVE NOR-PRO-FSET          TO MDMODIFA                               
           MOVE DFH-TURQ              TO MDMODIFC                               
           MOVE DFH-BASE              TO MDMODIFH                               
           MOVE NOR-ALP-FSET          TO MCCEEA                                 
           MOVE DFH-YELLO             TO MCCEEC                                 
           MOVE DFH-BASE              TO MCCEEH.                                
      *                                                                         
       FIN-TRAIT-SPE-SAISIE-OU-REP. EXIT.                                       
      *                                                                         
       TRAIT-SPE-MODIFICATION         SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** TRAITEMENT SPECIFIQUE AUX FONCTIONS :                                
      **** - MODIFICATION VENTE                                                 
      **** - MODIFICATION VENTE INEXISTANTE                                     
           MOVE NOR-PRO-FSET          TO MDVENTEA                               
           MOVE DFH-TURQ              TO MDVENTEC                               
           MOVE DFH-BASE              TO MDVENTEH                               
           MOVE NOR-PRO-FSET          TO MDMODIFA                               
           MOVE DFH-TURQ              TO MDMODIFC                               
           MOVE DFH-BASE              TO MDMODIFH                               
           MOVE NOR-PRO-FSET          TO MEDELA                                 
           MOVE DFH-TURQ              TO MEDELC                                 
           MOVE DFH-BASE              TO MEDELH                                 
           MOVE NOR-PRO-FSET          TO MEDATEA                                
           MOVE DFH-TURQ              TO MEDATEC                                
           MOVE DFH-BASE              TO MEDATEH                                
           MOVE NOR-PRO-FSET          TO MEPLA                                  
           MOVE DFH-TURQ              TO MEPLC                                  
           MOVE DFH-BASE              TO MEPLH                                  
           IF COMM-CVENDEUR-FREE (1)                                            
              MOVE NOR-ALP-FSET          TO MCVEND1A                            
              MOVE DFH-YELLO             TO MCVEND1C                            
              MOVE DFH-UNDLN             TO MCVEND1H                            
           ELSE                                                                 
              MOVE NOR-PRO-FSET          TO MCVEND1A                            
              MOVE DFH-TURQ              TO MCVEND1C                            
              MOVE DFH-BASE              TO MCVEND1H                            
           END-IF                                                               
           IF COMM-CVENDEUR-FREE (2)                                            
              MOVE NOR-ALP-FSET          TO MCVEND2A                            
              MOVE DFH-YELLO             TO MCVEND2C                            
              MOVE DFH-UNDLN             TO MCVEND2H                            
           ELSE                                                                 
              MOVE NOR-PRO-FSET          TO MCVEND2A                            
              MOVE DFH-TURQ              TO MCVEND2C                            
              MOVE DFH-BASE              TO MCVEND2H                            
           END-IF                                                               
           IF COMM-CVENDEUR-FREE (3)                                            
              MOVE NOR-ALP-FSET          TO MCVEND3A                            
              MOVE DFH-YELLO             TO MCVEND3C                            
              MOVE DFH-UNDLN             TO MCVEND3H                            
           ELSE                                                                 
              MOVE NOR-PRO-FSET          TO MCVEND3A                            
              MOVE DFH-TURQ              TO MCVEND3C                            
              MOVE DFH-BASE              TO MCVEND3H                            
           END-IF                                                               
           IF COMM-CVENDEUR-FREE(4)                                             
              MOVE NOR-ALP-FSET          TO MCVEND4A                            
              MOVE DFH-YELLO             TO MCVEND4C                            
              MOVE DFH-UNDLN             TO MCVEND4H                            
           ELSE                                                                 
              MOVE NOR-PRO-FSET          TO MCVEND4A                            
              MOVE DFH-TURQ              TO MCVEND4C                            
              MOVE DFH-BASE              TO MCVEND4H                            
           END-IF                                                               
           IF KOM-DETAXE-FREE                                                   
              MOVE NOR-ALP-FSET          TO MCCEEA                              
              MOVE DFH-YELLO             TO MCCEEC                              
              MOVE DFH-BASE              TO MCCEEH                              
              MOVE NOR-ALP-FSET          TO MCEXPA                              
              MOVE DFH-YELLO             TO MCEXPC                              
              MOVE DFH-BASE              TO MCEXPH                              
           ELSE                                                                 
              MOVE NOR-PRO-FSET          TO MCCEEA                              
              MOVE DFH-TURQ              TO MCCEEC                              
              MOVE DFH-BASE              TO MCCEEH                              
              MOVE NOR-PRO-FSET          TO MCEXPA                              
              MOVE DFH-TURQ              TO MCEXPC                              
              MOVE DFH-BASE              TO MCEXPH                              
           END-IF.                                                              
      *                                                                         
       FIN-TRAIT-SPE-MODIFICATION. EXIT.                                        
      *                                                                         
       TRAIT-SPE-AUTRE-QUE-PRE-RESERV SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** TRAITEMENT POUR TOUTES LES FONCTIONS                                 
      **** SAUF POUR LA SAISIE DE PRE-RESERVATION                               
           MOVE NOR-ALP-FSET          TO MLCOMM1A                               
           MOVE DFH-YELLO             TO MLCOMM1C                               
           MOVE DFH-UNDLN             TO MLCOMM1H                               
           MOVE NOR-ALP-FSET          TO MLCOMM2A                               
           MOVE DFH-YELLO             TO MLCOMM2C                               
           MOVE DFH-UNDLN             TO MLCOMM2H                               
           MOVE NOR-ALP-FSET          TO MLCOMM3A                               
           MOVE DFH-YELLO             TO MLCOMM3C                               
           MOVE DFH-UNDLN             TO MLCOMM3H                               
           MOVE NOR-ALP-FSET          TO MLCOMM4A                               
           MOVE DFH-YELLO             TO MLCOMM4C                               
           MOVE DFH-UNDLN             TO MLCOMM4H.                              
      *                                                                         
       FIN-TRAIT-SPE-AUTRE. EXIT.                                               
      *                                                                 03680000
      *                                                                         
M8656  TRAIT-SPE-PRE-RESERV           SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** TRAITEMENT SPECIFIQUE A LA FONCTION :                                
      **** - REPRISE PRE RESERVATION                                            
           IF COMM-CVENDEUR-FREE (1)                                            
              MOVE NOR-ALP-FSET          TO MCVEND1A                            
              MOVE DFH-YELLO             TO MCVEND1C                            
              MOVE DFH-UNDLN             TO MCVEND1H                            
           ELSE                                                                 
              MOVE NOR-PRO-FSET          TO MCVEND1A                            
              MOVE DFH-TURQ              TO MCVEND1C                            
              MOVE DFH-BASE              TO MCVEND1H                            
           END-IF                                                               
           IF COMM-CVENDEUR-FREE (2)                                            
              MOVE NOR-ALP-FSET          TO MCVEND2A                            
              MOVE DFH-YELLO             TO MCVEND2C                            
              MOVE DFH-UNDLN             TO MCVEND2H                            
           ELSE                                                                 
              MOVE NOR-PRO-FSET          TO MCVEND2A                            
              MOVE DFH-TURQ              TO MCVEND2C                            
              MOVE DFH-BASE              TO MCVEND2H                            
           END-IF                                                               
           IF COMM-CVENDEUR-FREE (3)                                            
              MOVE NOR-ALP-FSET          TO MCVEND3A                            
              MOVE DFH-YELLO             TO MCVEND3C                            
              MOVE DFH-UNDLN             TO MCVEND3H                            
           ELSE                                                                 
              MOVE NOR-PRO-FSET          TO MCVEND3A                            
              MOVE DFH-TURQ              TO MCVEND3C                            
              MOVE DFH-BASE              TO MCVEND3H                            
           END-IF                                                               
           IF COMM-CVENDEUR-FREE(4)                                             
              MOVE NOR-ALP-FSET          TO MCVEND4A                            
              MOVE DFH-YELLO             TO MCVEND4C                            
              MOVE DFH-UNDLN             TO MCVEND4H                            
           ELSE                                                                 
              MOVE NOR-PRO-FSET          TO MCVEND4A                            
              MOVE DFH-TURQ              TO MCVEND4C                            
              MOVE DFH-BASE              TO MCVEND4H                            
           END-IF                                                               
           .                                                                    
      *                                                                         
M8656  FIN-TRAIT-SPE-PRE-RESERV.   EXIT.                                        
      *                                                                         
       MODULE-SORTIE                  SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      **** PASSAGE DE LA COMMAREA - REECRITURE TS GV30 - RETOUR                 
           PERFORM REWRITE-TSGV30.                                              
           MOVE Z-COMMAREA            TO DFHCOMMAREA.                   04730000
           EXEC CICS RETURN                                             04740000
           END-EXEC.                                                    04740000
           GOBACK.                                                      04750000
      *                                                                         
       FIN-MODULE-SORTIE. EXIT.                                                 
      *                                                                         
      *DESCRIPTION DES BRIQUES AIDA TEMP                                        
       TEMP01-COPY SECTION. CONTINUE. COPY SYKCTSRD.                            
       TEMP02-COPY SECTION. CONTINUE. COPY SYKCTSRW.                            
       99-COPY SECTION. CONTINUE. COPY SYKCERRO.                                
      *                                                                         
      ******************************************************************        
      *                     G E S T I O N   T S                        *        
      ******************************************************************        
      *                                                                         
       READ-TSGV30                    SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           STRING 'GV30' EIBTRMID                                               
              DELIMITED BY SIZE          INTO IDENT-TS                          
           END-STRING                                                           
           MOVE 1                     TO RANG-TS                                
           MOVE TS30-LONG             TO LONG-TS                                
           PERFORM READ-TS                                                      
           MOVE Z-INOUT               TO Z-MAP.                                 
      *                                                                         
       FIN-READ-TSGV30. EXIT.                                                   
      *                                                                         
       REWRITE-TSGV30                 SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           STRING 'GV30' EIBTRMID                                               
              DELIMITED BY SIZE          INTO IDENT-TS                          
           END-STRING                                                           
           MOVE 1                     TO RANG-TS                                
           MOVE TS30-LONG             TO LONG-TS                                
           MOVE Z-MAP                 TO Z-INOUT                                
           PERFORM REWRITE-TS.                                                  
      *                                                                         
       FIN-REWRITE-TSGV30. EXIT.                                                
