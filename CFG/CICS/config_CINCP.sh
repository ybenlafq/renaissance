#!/bin/ksh
# Configuration file for region : CINCP
CICS_REGION=CINCP

cicscp stop region ${CICS_REGION} 2>/dev/null
cicscp destroy region ${CICS_REGION} -f 2>/dev/null 

cat >/tmp/DestroySfs <<!eof
${CICS_REGION}cicsnlqfile
${CICS_REGION}cicsnrectsqfil
${CICS_REGION}cicsplqfile
${CICS_REGION}cicsrectsqfile
${CICS_REGION}cicstdqlgfile
${CICS_REGION}cicstdqnofile
${CICS_REGION}cicstdqphfile
FIGPRT
!eof

cat /tmp/DestroySfs | while read fic 
do
  sfsadmin destroy file -server /.:/cics/sfs/pilote $fic
done 2>/dev/null

echo ----- Setting ENV variables: LANG -----
export LANG=fr_FR@euro

echo ----- ${CICS_REGION} Region creation -----
cicscp -v create region ${CICS_REGION} DefaultFileServer=/.:/cics/sfs/pilote

echo ----- Transactions definition -----
echo ----- FG10 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG10
cicsadd -c td -r ${CICS_REGION} FG10 ProgName="TFG10"
cicsadd -c pd -r ${CICS_REGION} TFG10 PathName="TFG10.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG10 ProgType=map PathName="EFG10.map" RSLKey=public                               

echo ----- FG11 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG11
cicsadd -c td -r ${CICS_REGION} FG11 ProgName="TFG11"
cicsadd -c pd -r ${CICS_REGION} TFG11 PathName="TFG11.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG11 ProgType=map PathName="EFG11.map" RSLKey=public 

echo ----- FG12 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG12
cicsadd -c td -r ${CICS_REGION} FG12 ProgName="TFG12"
cicsadd -c pd -r ${CICS_REGION} TFG12 PathName="TFG12.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG12 ProgType=map PathName="EFG12.map" RSLKey=public 

echo ----- FG13 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG13
cicsadd -c td -r ${CICS_REGION} FG13 ProgName="TFG13"
cicsadd -c pd -r ${CICS_REGION} TFG13 PathName="TFG13.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG13 ProgType=map PathName="EFG13.map" RSLKey=public 

echo ----- FG14 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG14
cicsadd -c td -r ${CICS_REGION} FG14 ProgName="TFG14"
cicsadd -c pd -r ${CICS_REGION} TFG14 PathName="TFG14.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG14 ProgType=map PathName="EFG14.map" RSLKey=public 

echo ----- FG20 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG20
cicsadd -c td -r ${CICS_REGION} FG20 ProgName="TFG20" TWASize=500 
cicsadd -c pd -r ${CICS_REGION} TFG20 PathName="TFG20.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} SYRD PathName="SYRD.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG20 ProgType=map PathName="EFG20.map" RSLKey=public 

echo ----- FG21 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG21
cicsadd -c td -r ${CICS_REGION} FG21 ProgName="TFG21" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG21 PathName="TFG21.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG21 ProgType=map PathName="EFG21.map" RSLKey=public 

echo ----- FG22 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG22
cicsadd -c td -r ${CICS_REGION} FG22 ProgName="TFG22" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG22 PathName="TFG22.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG22 ProgType=map PathName="EFG22.map" RSLKey=public 

echo ----- FG23 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG23
cicsadd -c td -r ${CICS_REGION} FG23 ProgName="TFG23" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG23 PathName="TFG23.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG23 ProgType=map PathName="EFG23.map" RSLKey=public 

echo ----- FG24 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG24
cicsadd -c td -r ${CICS_REGION} FG24 ProgName="TFG24" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG24 PathName="TFG24.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG24 ProgType=map PathName="EFG24.map" RSLKey=public 

echo ----- FG25 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG25
cicsadd -c td -r ${CICS_REGION} FG25 ProgName="TFG25" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG25 PathName="TFG25.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG25 ProgType=map PathName="EFG25.map" RSLKey=public

echo ----- FG27 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG27
cicsadd -c td -r ${CICS_REGION} FG27 ProgName="TFG27" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG27 PathName="TFG27.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG27 ProgType=map PathName="EFG27.map" RSLKey=public

echo ----- FG28 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG28
cicsadd -c td -r ${CICS_REGION} FG28 ProgName="TFG28" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG28 PathName="TFG28.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG28 ProgType=map PathName="EFG28.map" RSLKey=public

echo ----- FG29 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG29
cicsadd -c td -r ${CICS_REGION} FG29 ProgName="TFG29" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG29 PathName="TFG29.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG29 ProgType=map PathName="EFG29.map" RSLKey=public

echo ----- FG30 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG30
cicsadd -c td -r ${CICS_REGION} FG30 ProgName="MFG30" TWASize=500
cicsadd -c pd -r ${CICS_REGION} MFG30 PathName="MFG30.so" RSLKey=public

echo ----- FG50 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG50
cicsadd -c td -r ${CICS_REGION} FG50 ProgName="TFG50" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG50 PathName="TFG50.so" RSLKey=public

echo ----- FG51 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG51
cicsadd -c td -r ${CICS_REGION} FG51 ProgName="TFG51" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG51 PathName="TFG51.so" RSLKey=public


echo ----- RM00 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM00
cicsadd -c td -r ${CICS_REGION} RM00 ProgName="TRM00" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM00 PathName="TRM00.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM00 ProgType=map PathName="ERM00.map" RSLKey=public

echo ----- RM01 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM01
cicsadd -c td -r ${CICS_REGION} RM01 ProgName="TRM01" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM01 PathName="TRM01.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM01 ProgType=map PathName="ERM01.map" RSLKey=public

echo ----- RM02 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM02
cicsadd -c td -r ${CICS_REGION} RM02 ProgName="TRM02" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM02 PathName="TRM02.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM02 ProgType=map PathName="ERM02.map" RSLKey=public

echo ----- RM03 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM03
cicsadd -c td -r ${CICS_REGION} RM03 ProgName="TRM03" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM03 PathName="TRM03.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM03 ProgType=map PathName="ERM03.map" RSLKey=public

echo ----- RM04 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM04
cicsadd -c td -r ${CICS_REGION} RM04 ProgName="TRM04" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM04 PathName="TRM04.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM04 ProgType=map PathName="ERM04.map" RSLKey=public

echo ----- RM05 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM05
cicsadd -c td -r ${CICS_REGION} RM05 ProgName="TRM05" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM05 PathName="TRM05.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM05 ProgType=map PathName="ERM05.map" RSLKey=public

echo ----- RM06 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM06
cicsadd -c td -r ${CICS_REGION} RM06 ProgName="TRM06" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM06 PathName="TRM06.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM06 ProgType=map PathName="ERM06.map" RSLKey=public

echo ----- RM07 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM07
cicsadd -c td -r ${CICS_REGION} RM07 ProgName="TRM07" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM07 PathName="TRM07.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM07 ProgType=map PathName="ERM07.map" RSLKey=public

echo ----- RM08 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM08
cicsadd -c td -r ${CICS_REGION} RM08 ProgName="TRM08" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM08 PathName="TRM08.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM08 ProgType=map PathName="ERM08.map" RSLKey=public

echo ----- RM09 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM09
cicsadd -c td -r ${CICS_REGION} RM09 ProgName="TRM09" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM09 PathName="TRM09.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM09 ProgType=map PathName="ERM09.map" RSLKey=public

echo ----- RM12 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM12
cicsadd -c td -r ${CICS_REGION} RM12 ProgName="TRM12" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM12 PathName="TRM12.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM12 ProgType=map PathName="ERM12.map" RSLKey=public

echo ----- RM13 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM13
cicsadd -c td -r ${CICS_REGION} RM13 ProgName="TRM13" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM13 PathName="TRM13.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM13 ProgType=map PathName="ERM13.map" RSLKey=public

echo ----- RM14 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM14
cicsadd -c td -r ${CICS_REGION} RM14 ProgName="TRM14" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM14 PathName="TRM14.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM14 ProgType=map PathName="ERM14.map" RSLKey=public

echo ----- RM15 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM15
cicsadd -c td -r ${CICS_REGION} RM15 ProgName="TRM15" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM15 PathName="TRM15.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM15 ProgType=map PathName="ERM15.map" RSLKey=public

echo ----- RM16 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM16
cicsadd -c td -r ${CICS_REGION} RM16 ProgName="TRM16" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM16 PathName="TRM16.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM16 ProgType=map PathName="ERM16.map" RSLKey=public

echo ----- RM60 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM60
cicsadd -c td -r ${CICS_REGION} RM60 ProgName="TRM60" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM60 PathName="TRM60.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM60 ProgType=map PathName="ERM60.map" RSLKey=public

echo ----- RM61 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM61
cicsadd -c td -r ${CICS_REGION} RM61 ProgName="TRM61" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM61 PathName="TRM61.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM61 ProgType=map PathName="ERM61.map" RSLKey=public

echo ----- RM10 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM10
cicsadd -c td -r ${CICS_REGION} RM10 ProgName="TRM10" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM10 PathName="TRM10.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM10 ProgType=map PathName="ERM10.map" RSLKey=public

cicsadd -c pd -r ${CICS_REGION} TETDATC PathName="TETDATC.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} TETDATI PathName="TETDATI.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} MFFNUM  PathName="MFFNUM .so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} MFL10 PathName="MFL10.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} MPTMG PathName="MPTMG.so" RSLKey=public

echo ----- RM30 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM30
cicsadd -c td -r ${CICS_REGION} RM30 ProgName="TRM30" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM30 PathName="TRM30.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM30 ProgType=map PathName="ERM30.map" RSLKey=public

echo ----- RM31 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM31
cicsadd -c td -r ${CICS_REGION} RM31 ProgName="TRM31" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM31 PathName="TRM31.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM31 ProgType=map PathName="ERM31.map" RSLKey=public

echo ----- RM32 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM32
cicsadd -c td -r ${CICS_REGION} RM32 ProgName="TRM32" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM32 PathName="TRM32.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM32 ProgType=map PathName="ERM32.map" RSLKey=public

echo ----- RM33 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM33
cicsadd -c td -r ${CICS_REGION} RM33 ProgName="TRM33" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM33 PathName="TRM33.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM33 ProgType=map PathName="ERM33.map" RSLKey=public

echo ----- RM34 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM34
cicsadd -c td -r ${CICS_REGION} RM34 ProgName="TRM34" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM34 PathName="TRM34.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM34 ProgType=map PathName="ERM34.map" RSLKey=public

echo ----- RM35 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM35
cicsadd -c td -r ${CICS_REGION} RM35 ProgName="TRM35" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM35 PathName="TRM35.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM35 ProgType=map PathName="ERM35.map" RSLKey=public

echo ----- RM36 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM36
cicsadd -c td -r ${CICS_REGION} RM36 ProgName="TRM36" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM36 PathName="TRM36.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM36 ProgType=map PathName="ERM36.map" RSLKey=public

echo ----- RM37 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM37
cicsadd -c td -r ${CICS_REGION} RM37 ProgName="TRM37" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM37 PathName="TRM37.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM37 ProgType=map PathName="ERM37.map" RSLKey=public

echo ----- RM38 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM38
cicsadd -c td -r ${CICS_REGION} RM38 ProgName="TRM38" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM38 PathName="TRM38.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM38 ProgType=map PathName="ERM38.map" RSLKey=public

echo ----- RM40 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM40
cicsadd -c td -r ${CICS_REGION} RM40 ProgName="TRM40" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM40 PathName="TRM40.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM40 ProgType=map PathName="ERM40.map" RSLKey=public

echo ----- RM41 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM41
cicsadd -c td -r ${CICS_REGION} RM41 ProgName="TRM41" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM41 PathName="TRM41.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM41 ProgType=map PathName="ERM41.map" RSLKey=public

echo ----- RM42 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM42
cicsadd -c td -r ${CICS_REGION} RM42 ProgName="TRM42" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM42 PathName="TRM42.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM42 ProgType=map PathName="ERM42.map" RSLKey=public

echo ----- RM43 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM43
cicsadd -c td -r ${CICS_REGION} RM43 ProgName="TRM43" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM43 PathName="TRM43.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM43 ProgType=map PathName="ERM43.map" RSLKey=public

echo ----- RM44 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM44
cicsadd -c td -r ${CICS_REGION} RM44 ProgName="TRM44" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM44 PathName="TRM44.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM44 ProgType=map PathName="ERM44.map" RSLKey=public

echo ----- RM45 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM45
cicsadd -c td -r ${CICS_REGION} RM45 ProgName="TRM45" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM45 PathName="TRM45.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM45 ProgType=map PathName="ERM45.map" RSLKey=public

echo ----- RM47 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM47
cicsadd -c td -r ${CICS_REGION} RM47 ProgName="TRM47" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM47 PathName="TRM47.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM47 ProgType=map PathName="ERM47.map" RSLKey=public

echo ----- RM48 -----
cicsadd -c tsd -r ${CICS_REGION} TMPRM48
cicsadd -c td -r ${CICS_REGION} RM48 ProgName="TRM48" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TRM48 PathName="TRM48.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} ERM48 ProgType=map PathName="ERM48.map" RSLKey=public

# File definition 
echo ----- File definition -----
#sfs_srv=$(uname -n)
cicsadd -c fd -r ${CICS_REGION} FIGPRT RSLKey=public EnableStatus=enabled OpenStatus=open BaseName="FIGPRT" FileServer="/.:/cics/sfs/pilote" IndexName="pri_0" ActivateOnStartup=yes KeyLen=12 RecordSize=88
#sfsadmin create clusteredfile FIGPRT 2 CLE string 12 DATA string 88 CLE -unique 1 CLE sfs_Spilote -server /.:/cics/sfs/pilote -preallocate 2

# Create cics user 
cicsadd -r ${CICS_REGION} -c ud IE01019 CICSPassword="IE01019" TSLKeyList=5
cicsadd -r ${CICS_REGION} -c ud IE01093 CICSPassword="IE01093" TSLKeyList=5

# Define XA Conenction
echo ----- XA Connection definition -----
#cicsadd -c xad -r ${CICS_REGION} XADdef SwitchLoadFile=db2xa XAOpen="pilote,dbuser,dbuser"
cicsadd -c xad -r ${CICS_REGION} XADdef SwitchLoadFile=db2xa XAOpen="${MT_DB_NAME},${MT_DB_USER},${MT_DB_PASSWD}"

# Opening port for remote connections 
echo ----- Opening 60180 TCP port -----
iptables -I INPUT -p tcp --dport 60180 --syn -j ACCEPT

echo ----- Copie des binaires sous /var/cics_regions/${CICS_REGION}/bin -----
cp $ROOT_DIR/EXE/*.so /var/cics_regions/${CICS_REGION}/bin/
cp $ROOT_DIR/MAP/*.map /var/cics_regions/${CICS_REGION}/bin/
cp $CICSPATH/maps/fr_FR/dfhcesn.map /var/cics_regions/${CICS_REGION}/bin/

#echo '>>>> WARNING: 2 actions sont a faire pour completer la configuration  <<<<'
#echo '   1. Ajouter les deux variables ci-dessous au niveau du fichier /var/cics_regions/${CICS_REGION}/environment:
#		CICS_PROGRAM_PATH=/opt/ibm/cics/samples/ivp/COBOLIT:/opt/ibm/cics/bin:/var/cics_regions/${CICS_REGION}/bin
#		COB_LIBRARY_PATH=/opt/ibm/cics/samples/ivp/COBOLIT:/opt/ibm/cics/bin:/var/cics_regions/${CICS_REGION}/bin'
echo "CICS_PROGRAM_PATH=/opt/ibm/cics/samples/ivp/COBOLIT:/opt/ibm/cics/bin:/var/cics_regions/${CICS_REGION}/bin" >> /var/cics_regions/${CICS_REGION}/environment
echo "COB_LIBRARY_PATH=/opt/ibm/cics/samples/ivp/COBOLIT:/opt/ibm/cics/bin:/var/cics_regions/${CICS_REGION}/bin" >> /var/cics_regions/${CICS_REGION}/environment

#echo '   2. Mettre a jour le fichier /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza
#	        Entry MD16:
#		        La valeur de TCTUALen devient 255 au lieu de 0 (TCTUALen = 255 )'

awk '/MD16/,/TCTUALen/ {if ( $0 ~ /TCTUALen/) {sub("= 0","= 255",$0);print;next}}{print}' /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza > /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza.awk
mv /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza.awk /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza
# Convert all received data from terminal to UPPERCASE (Attribute UCTranFlag of terminal definition)
awk '/MD16/,/UCTranFlag/ {if ( $0 ~ /UCTranFlag/) {sub("= no", "= yes",$0);print;next}}{print}' /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza > /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza.awk
mv /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza.awk /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza
chmod 660 /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza
chown cics:cics /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza

#cicscp start region ${CICS_REGION} StartType=cold
