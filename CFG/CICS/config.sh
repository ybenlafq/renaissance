#!/bin/ksh

cicscp stop region CICSA 2>/dev/null
cicscp destroy region CICSA -f 2>/dev/null 

cat >/tmp/DestroySfs <<!eof
CICSAcicsnlqfile
CICSAcicsnrectsqfil
CICSAcicsplqfile
CICSAcicsrectsqfile
CICSAcicstdqlgfile
CICSAcicstdqnofile
CICSAcicstdqphfile
FIGPRT
!eof

cat /tmp/DestroySfs | while read fic 
do
  sfsadmin destroy file -server /.:/cics/sfs/pilote $fic
done 2>/dev/null

echo ----- Setting ENV variables: LANG -----
export LANG=fr_FR@euro

echo ----- CICSA Region creation -----
cicscp -v create region CICSA DefaultFileServer=/.:/cics/sfs/pilote

echo ----- Transactions definition -----
echo ----- FG10 -----
cicsadd -c tsd -r CICSA TMPFG10
cicsadd -c td -r CICSA FG10 ProgName="TFG10"
cicsadd -c pd -r CICSA TFG10 PathName="TFG10.so" RSLKey=public
cicsadd -c pd -r CICSA EFG10 ProgType=map PathName="EFG10.map" RSLKey=public                               

echo ----- FG11 -----
cicsadd -c tsd -r CICSA TMPFG11
cicsadd -c td -r CICSA FG11 ProgName="TFG11"
cicsadd -c pd -r CICSA TFG11 PathName="TFG11.so" RSLKey=public
cicsadd -c pd -r CICSA EFG11 ProgType=map PathName="EFG11.map" RSLKey=public 

echo ----- FG12 -----
cicsadd -c tsd -r CICSA TMPFG12
cicsadd -c td -r CICSA FG12 ProgName="TFG12"
cicsadd -c pd -r CICSA TFG12 PathName="TFG12.so" RSLKey=public
cicsadd -c pd -r CICSA EFG12 ProgType=map PathName="EFG12.map" RSLKey=public 

echo ----- FG13 -----
cicsadd -c tsd -r CICSA TMPFG13
cicsadd -c td -r CICSA FG13 ProgName="TFG13"
cicsadd -c pd -r CICSA TFG13 PathName="TFG13.so" RSLKey=public
cicsadd -c pd -r CICSA EFG13 ProgType=map PathName="EFG13.map" RSLKey=public 

echo ----- FG14 -----
cicsadd -c tsd -r CICSA TMPFG14
cicsadd -c td -r CICSA FG14 ProgName="TFG14"
cicsadd -c pd -r CICSA TFG14 PathName="TFG14.so" RSLKey=public
cicsadd -c pd -r CICSA EFG14 ProgType=map PathName="EFG14.map" RSLKey=public 

echo ----- FG20 -----
cicsadd -c tsd -r CICSA TMPFG20
cicsadd -c td -r CICSA FG20 ProgName="TFG20" TWASize=500 
cicsadd -c pd -r CICSA TFG20 PathName="TFG20.so" RSLKey=public
cicsadd -c pd -r CICSA SYRD PathName="SYRD.so" RSLKey=public
cicsadd -c pd -r CICSA EFG20 ProgType=map PathName="EFG20.map" RSLKey=public 

echo ----- FG21 -----
cicsadd -c tsd -r CICSA TMPFG21
cicsadd -c td -r CICSA FG21 ProgName="TFG21" TWASize=500
cicsadd -c pd -r CICSA TFG21 PathName="TFG21.so" RSLKey=public
cicsadd -c pd -r CICSA EFG21 ProgType=map PathName="EFG21.map" RSLKey=public 

echo ----- FG22 -----
cicsadd -c tsd -r CICSA TMPFG22
cicsadd -c td -r CICSA FG22 ProgName="TFG22" TWASize=500
cicsadd -c pd -r CICSA TFG22 PathName="TFG22.so" RSLKey=public
cicsadd -c pd -r CICSA EFG22 ProgType=map PathName="EFG22.map" RSLKey=public 

echo ----- FG23 -----
cicsadd -c tsd -r CICSA TMPFG23
cicsadd -c td -r CICSA FG23 ProgName="TFG23" TWASize=500
cicsadd -c pd -r CICSA TFG23 PathName="TFG23.so" RSLKey=public
cicsadd -c pd -r CICSA EFG23 ProgType=map PathName="EFG23.map" RSLKey=public 

echo ----- FG24 -----
cicsadd -c tsd -r CICSA TMPFG24
cicsadd -c td -r CICSA FG24 ProgName="TFG24" TWASize=500
cicsadd -c pd -r CICSA TFG24 PathName="TFG24.so" RSLKey=public
cicsadd -c pd -r CICSA EFG24 ProgType=map PathName="EFG24.map" RSLKey=public 

echo ----- FG25 -----
cicsadd -c tsd -r CICSA TMPFG25
cicsadd -c td -r CICSA FG25 ProgName="TFG25" TWASize=500
cicsadd -c pd -r CICSA TFG25 PathName="TFG25.so" RSLKey=public
cicsadd -c pd -r CICSA EFG25 ProgType=map PathName="EFG25.map" RSLKey=public

echo ----- FG27 -----
cicsadd -c tsd -r CICSA TMPFG27
cicsadd -c td -r CICSA FG27 ProgName="TFG27" TWASize=500
cicsadd -c pd -r CICSA TFG27 PathName="TFG27.so" RSLKey=public
cicsadd -c pd -r CICSA EFG27 ProgType=map PathName="EFG27.map" RSLKey=public

echo ----- FG28 -----
cicsadd -c tsd -r CICSA TMPFG28
cicsadd -c td -r CICSA FG28 ProgName="TFG28" TWASize=500
cicsadd -c pd -r CICSA TFG28 PathName="TFG28.so" RSLKey=public
cicsadd -c pd -r CICSA EFG28 ProgType=map PathName="EFG28.map" RSLKey=public

echo ----- FG29 -----
cicsadd -c tsd -r CICSA TMPFG29
cicsadd -c td -r CICSA FG29 ProgName="TFG29" TWASize=500
cicsadd -c pd -r CICSA TFG29 PathName="TFG29.so" RSLKey=public
cicsadd -c pd -r CICSA EFG29 ProgType=map PathName="EFG29.map" RSLKey=public

echo ----- FG30 -----
cicsadd -c tsd -r CICSA TMPFG30
cicsadd -c td -r CICSA FG30 ProgName="MFG30" TWASize=500
cicsadd -c pd -r CICSA MFG30 PathName="MFG30.so" RSLKey=public

echo ----- FG50 -----
cicsadd -c tsd -r CICSA TMPFG50
cicsadd -c td -r CICSA FG50 ProgName="TFG50" TWASize=500
cicsadd -c pd -r CICSA TFG50 PathName="TFG50.so" RSLKey=public

echo ----- FG51 -----
cicsadd -c tsd -r CICSA TMPFG51
cicsadd -c td -r CICSA FG51 ProgName="TFG51" TWASize=500
cicsadd -c pd -r CICSA TFG51 PathName="TFG51.so" RSLKey=public

cicsadd -c pd -r CICSA TETDATC PathName="TETDATC.so" RSLKey=public

echo ----- RM30 -----
cicsadd -c tsd -r CINCP TMPRM30
cicsadd -c td -r CINCP RM30 ProgName="TRM30" TWASize=500
cicsadd -c pd -r CINCP TRM30 PathName="TRM30.so" RSLKey=public
cicsadd -c pd -r CINCP ERM30 ProgType=map PathName="ERM30.map" RSLKey=public
echo ----- RM31 -----
cicsadd -c tsd -r CINCP TMPRM31
cicsadd -c td -r CINCP RM31 ProgName="TRM31" TWASize=500
cicsadd -c pd -r CINCP TRM31 PathName="TRM31.so" RSLKey=public
cicsadd -c pd -r CINCP ERM31 ProgType=map PathName="ERM31.map" RSLKey=public
echo ----- RM32 -----
cicsadd -c tsd -r CINCP TMPRM32
cicsadd -c td -r CINCP RM32 ProgName="TRM32" TWASize=500
cicsadd -c pd -r CINCP TRM32 PathName="TRM32.so" RSLKey=public
cicsadd -c pd -r CINCP ERM32 ProgType=map PathName="ERM32.map" RSLKey=public
echo ----- RM33 -----
cicsadd -c tsd -r CINCP TMPRM33
cicsadd -c td -r CINCP RM33 ProgName="TRM33" TWASize=500
cicsadd -c pd -r CINCP TRM33 PathName="TRM33.so" RSLKey=public
cicsadd -c pd -r CINCP ERM33 ProgType=map PathName="ERM33.map" RSLKey=public
echo ----- RM34 -----
cicsadd -c tsd -r CINCP TMPRM34
cicsadd -c td -r CINCP RM34 ProgName="TRM34" TWASize=500
cicsadd -c pd -r CINCP TRM34 PathName="TRM34.so" RSLKey=public
cicsadd -c pd -r CINCP ERM34 ProgType=map PathName="ERM34.map" RSLKey=public
echo ----- RM35 -----
cicsadd -c tsd -r CINCP TMPRM35
cicsadd -c td -r CINCP RM35 ProgName="TRM35" TWASize=500
cicsadd -c pd -r CINCP TRM35 PathName="TRM35.so" RSLKey=public
cicsadd -c pd -r CINCP ERM35 ProgType=map PathName="ERM35.map" RSLKey=public
echo ----- RM36 -----
cicsadd -c tsd -r CINCP TMPRM36
cicsadd -c td -r CINCP RM36 ProgName="TRM36" TWASize=500
cicsadd -c pd -r CINCP TRM36 PathName="TRM36.so" RSLKey=public
cicsadd -c pd -r CINCP ERM36 ProgType=map PathName="ERM36.map" RSLKey=public
echo ----- RM37 -----
cicsadd -c tsd -r CINCP TMPRM37
cicsadd -c td -r CINCP RM37 ProgName="TRM37" TWASize=500
cicsadd -c pd -r CINCP TRM37 PathName="TRM37.so" RSLKey=public
cicsadd -c pd -r CINCP ERM37 ProgType=map PathName="ERM37.map" RSLKey=public
echo ----- RM38 -----
cicsadd -c tsd -r CINCP TMPRM38
cicsadd -c td -r CINCP RM38 ProgName="TRM38" TWASize=500
cicsadd -c pd -r CINCP TRM38 PathName="TRM38.so" RSLKey=public
cicsadd -c pd -r CINCP ERM38 ProgType=map PathName="ERM38.map" RSLKey=public
echo ----- RM40 -----
cicsadd -c tsd -r CINCP TMPRM40
cicsadd -c td -r CINCP RM40 ProgName="TRM40" TWASize=500
cicsadd -c pd -r CINCP TRM40 PathName="TRM40.so" RSLKey=public
cicsadd -c pd -r CINCP ERM40 ProgType=map PathName="ERM40.map" RSLKey=public
echo ----- RM41 -----
cicsadd -c tsd -r CINCP TMPRM41
cicsadd -c td -r CINCP RM41 ProgName="TRM41" TWASize=500
cicsadd -c pd -r CINCP TRM41 PathName="TRM41.so" RSLKey=public
cicsadd -c pd -r CINCP ERM41 ProgType=map PathName="ERM41.map" RSLKey=public
echo ----- RM42 -----
cicsadd -c tsd -r CINCP TMPRM42
cicsadd -c td -r CINCP RM42 ProgName="TRM42" TWASize=500
cicsadd -c pd -r CINCP TRM42 PathName="TRM42.so" RSLKey=public
cicsadd -c pd -r CINCP ERM42 ProgType=map PathName="ERM42.map" RSLKey=public
echo ----- RM43 -----
cicsadd -c tsd -r CINCP TMPRM43
cicsadd -c td -r CINCP RM43 ProgName="TRM43" TWASize=500
cicsadd -c pd -r CINCP TRM43 PathName="TRM43.so" RSLKey=public
cicsadd -c pd -r CINCP ERM43 ProgType=map PathName="ERM43.map" RSLKey=public
echo ----- RM44 -----
cicsadd -c tsd -r CINCP TMPRM44
cicsadd -c td -r CINCP RM44 ProgName="TRM44" TWASize=500
cicsadd -c pd -r CINCP TRM44 PathName="TRM44.so" RSLKey=public
cicsadd -c pd -r CINCP ERM44 ProgType=map PathName="ERM44.map" RSLKey=public
echo ----- RM45 -----
cicsadd -c tsd -r CINCP TMPRM45
cicsadd -c td -r CINCP RM45 ProgName="TRM45" TWASize=500
cicsadd -c pd -r CINCP TRM45 PathName="TRM45.so" RSLKey=public
cicsadd -c pd -r CINCP ERM45 ProgType=map PathName="ERM45.map" RSLKey=public
echo ----- RM47 -----
cicsadd -c tsd -r CINCP TMPRM47
cicsadd -c td -r CINCP RM47 ProgName="TRM47" TWASize=500
cicsadd -c pd -r CINCP TRM47 PathName="TRM47.so" RSLKey=public
cicsadd -c pd -r CINCP ERM47 ProgType=map PathName="ERM47.map" RSLKey=public
echo ----- RM48 -----
cicsadd -c tsd -r CINCP TMPRM48
cicsadd -c td -r CINCP RM48 ProgName="TRM48" TWASize=500
cicsadd -c pd -r CINCP TRM48 PathName="TRM48.so" RSLKey=public
cicsadd -c pd -r CINCP ERM48 ProgType=map PathName="ERM48.map" RSLKey=public


# File definition 
echo ----- File definition -----
#sfs_srv=$(uname -n)
cicsadd -c fd -r CICSA FIGPRT RSLKey=public EnableStatus=enabled OpenStatus=open BaseName="FIGPRT" FileServer="/.:/cics/sfs/pilote" IndexName="pri_0" ActivateOnStartup=yes KeyLen=12 RecordSize=88
#sfsadmin create clusteredfile FIGPRT 2 CLE string 12 DATA string 88 CLE -unique 1 CLE sfs_Spilote -server /.:/cics/sfs/pilote -preallocate 2

# Create cics user 
cicsadd -r CICSA -c ud IE01019 CICSPassword="IE01019" TSLKeyList=5

# Define XA Conenction
echo ----- XA Connection definition -----
cicsadd -c xad -r CICSA XADdef SwitchLoadFile=db2xa XAOpen="pilote,dbuser,dbuser"

# Opening port for remote connections 
echo ----- Opening 60180 TCP port -----
iptables -I INPUT -p tcp --dport 60180 --syn -j ACCEPT

echo ----- Copie des binaires sous /var/cics_regions/CICSA/bin -----
cp $ROOT_DIR/EXE/*.so /var/cics_regions/CICSA/bin/ 
cp $ROOT_DIR/MAP/*.map /var/cics_regions/CICSA/bin/
cp $CICSPATH/maps/fr_FR/dfhcesn.map /var/cics_regions/CICSA/bin/

#echo '>>>> WARNING: 2 actions sont a faire pour completer la configuration  <<<<'
#echo '   1. Ajouter les deux variables ci-dessous au niveau du fichier /var/cics_regions/CICSA/environment:
#		CICS_PROGRAM_PATH=/opt/ibm/cics/samples/ivp/COBOLIT:/opt/ibm/cics/bin:/var/cics_regions/CICSA/bin
#		COB_LIBRARY_PATH=/opt/ibm/cics/samples/ivp/COBOLIT:/opt/ibm/cics/bin:/var/cics_regions/CICSA/bin'
echo "CICS_PROGRAM_PATH=/opt/ibm/cics/samples/ivp/COBOLIT:/opt/ibm/cics/bin:/var/cics_regions/CICSA/bin" >> /var/cics_regions/CICSA/environment
echo "COB_LIBRARY_PATH=/opt/ibm/cics/samples/ivp/COBOLIT:/opt/ibm/cics/bin:/var/cics_regions/CICSA/bin" >> /var/cics_regions/CICSA/environment

#echo '   2. Mettre a jour le fichier /var/cics_regions/CICSA/database/WD/WD.stanza
#	        Entry MD16:
#		        La valeur de TCTUALen devient 255 au lieu de 0 (TCTUALen = 255 )'

awk '/MD16/,/TCTUALen/ {if ( $0 ~ /TCTUALen/) {sub("= 0","= 255",$0);print;next}}{print}' /var/cics_regions/CICSA/database/WD/WD.stanza > /var/cics_regions/CICSA/database/WD/WD.stanza.awk
mv /var/cics_regions/CICSA/database/WD/WD.stanza.awk /var/cics_regions/CICSA/database/WD/WD.stanza
# Convert all received data from terminal to UPPERCASE (Attribute UCTranFlag of terminal definition) 
awk '/MD16/,/UCTranFlag/ {if ( $0 ~ /UCTranFlag/) {sub("= no", "= yes",$0);print;next}}{print}' /var/cics_regions/CICSA/database/WD/WD.stanza > /var/cics_regions/CICSA/database/WD/WD.stanza.awk
mv /var/cics_regions/CICSA/database/WD/WD.stanza.awk /var/cics_regions/CICSA/database/WD/WD.stanza
chmod 660 /var/cics_regions/CICSA/database/WD/WD.stanza
chown cics:cics /var/cics_regions/CICSA/database/WD/WD.stanza

#cicscp start region CICSA StartType=cold
