#!/bin/ksh
# Configuration file for region : CICSA 
CICS_REGION=CICSA

cicscp stop region ${CICS_REGION} 2>/dev/null
cicscp destroy region ${CICS_REGION} -f 2>/dev/null 

cat >/tmp/DestroySfs <<!eof
${CICS_REGION}cicsnlqfile
${CICS_REGION}cicsnrectsqfil
${CICS_REGION}cicsplqfile
${CICS_REGION}cicsrectsqfile
${CICS_REGION}cicstdqlgfile
${CICS_REGION}cicstdqnofile
${CICS_REGION}cicstdqphfile
FIGPRT
!eof

cat /tmp/DestroySfs | while read fic 
do
  sfsadmin destroy file -server /.:/cics/sfs/pilote $fic
done 2>/dev/null

echo ----- Setting ENV variables: LANG -----
export LANG=fr_FR@euro

echo ----- ${CICS_REGION} Region creation -----
cicscp -v create region ${CICS_REGION} DefaultFileServer=/.:/cics/sfs/pilote

echo ----- Transactions definition -----
echo ----- FG10 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG10
cicsadd -c td -r ${CICS_REGION} FG10 ProgName="TFG10"
cicsadd -c pd -r ${CICS_REGION} TFG10 PathName="TFG10.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG10 ProgType=map PathName="EFG10.map" RSLKey=public                               

echo ----- FG11 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG11
cicsadd -c td -r ${CICS_REGION} FG11 ProgName="TFG11"
cicsadd -c pd -r ${CICS_REGION} TFG11 PathName="TFG11.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG11 ProgType=map PathName="EFG11.map" RSLKey=public 

echo ----- FG12 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG12
cicsadd -c td -r ${CICS_REGION} FG12 ProgName="TFG12"
cicsadd -c pd -r ${CICS_REGION} TFG12 PathName="TFG12.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG12 ProgType=map PathName="EFG12.map" RSLKey=public 

echo ----- FG13 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG13
cicsadd -c td -r ${CICS_REGION} FG13 ProgName="TFG13"
cicsadd -c pd -r ${CICS_REGION} TFG13 PathName="TFG13.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG13 ProgType=map PathName="EFG13.map" RSLKey=public 

echo ----- FG14 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG14
cicsadd -c td -r ${CICS_REGION} FG14 ProgName="TFG14"
cicsadd -c pd -r ${CICS_REGION} TFG14 PathName="TFG14.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG14 ProgType=map PathName="EFG14.map" RSLKey=public 

echo ----- FG20 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG20
cicsadd -c td -r ${CICS_REGION} FG20 ProgName="TFG20" TWASize=500 
cicsadd -c pd -r ${CICS_REGION} TFG20 PathName="TFG20.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} SYRD PathName="SYRD.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG20 ProgType=map PathName="EFG20.map" RSLKey=public 

echo ----- FG21 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG21
cicsadd -c td -r ${CICS_REGION} FG21 ProgName="TFG21" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG21 PathName="TFG21.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG21 ProgType=map PathName="EFG21.map" RSLKey=public 

echo ----- FG22 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG22
cicsadd -c td -r ${CICS_REGION} FG22 ProgName="TFG22" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG22 PathName="TFG22.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG22 ProgType=map PathName="EFG22.map" RSLKey=public 

echo ----- FG23 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG23
cicsadd -c td -r ${CICS_REGION} FG23 ProgName="TFG23" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG23 PathName="TFG23.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG23 ProgType=map PathName="EFG23.map" RSLKey=public 

echo ----- FG24 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG24
cicsadd -c td -r ${CICS_REGION} FG24 ProgName="TFG24" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG24 PathName="TFG24.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG24 ProgType=map PathName="EFG24.map" RSLKey=public 

echo ----- FG25 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG25
cicsadd -c td -r ${CICS_REGION} FG25 ProgName="TFG25" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG25 PathName="TFG25.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG25 ProgType=map PathName="EFG25.map" RSLKey=public

echo ----- FG27 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG27
cicsadd -c td -r ${CICS_REGION} FG27 ProgName="TFG27" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG27 PathName="TFG27.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG27 ProgType=map PathName="EFG27.map" RSLKey=public

echo ----- FG28 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG28
cicsadd -c td -r ${CICS_REGION} FG28 ProgName="TFG28" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG28 PathName="TFG28.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG28 ProgType=map PathName="EFG28.map" RSLKey=public

echo ----- FG29 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG29
cicsadd -c td -r ${CICS_REGION} FG29 ProgName="TFG29" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG29 PathName="TFG29.so" RSLKey=public
cicsadd -c pd -r ${CICS_REGION} EFG29 ProgType=map PathName="EFG29.map" RSLKey=public

echo ----- FG30 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG30
cicsadd -c td -r ${CICS_REGION} FG30 ProgName="MFG30" TWASize=500
cicsadd -c pd -r ${CICS_REGION} MFG30 PathName="MFG30.so" RSLKey=public

echo ----- FG50 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG50
cicsadd -c td -r ${CICS_REGION} FG50 ProgName="TFG50" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG50 PathName="TFG50.so" RSLKey=public

echo ----- FG51 -----
cicsadd -c tsd -r ${CICS_REGION} TMPFG51
cicsadd -c td -r ${CICS_REGION} FG51 ProgName="TFG51" TWASize=500
cicsadd -c pd -r ${CICS_REGION} TFG51 PathName="TFG51.so" RSLKey=public

cicsadd -c pd -r ${CICS_REGION} TETDATC PathName="TETDATC.so" RSLKey=public


# File definition 
echo ----- File definition -----
#sfs_srv=$(uname -n)
cicsadd -c fd -r ${CICS_REGION} FIGPRT RSLKey=public EnableStatus=enabled OpenStatus=open BaseName="FIGPRT" FileServer="/.:/cics/sfs/pilote" IndexName="pri_0" ActivateOnStartup=yes KeyLen=12 RecordSize=88
#sfsadmin create clusteredfile FIGPRT 2 CLE string 12 DATA string 88 CLE -unique 1 CLE sfs_Spilote -server /.:/cics/sfs/pilote -preallocate 2

# Create cics user 
cicsadd -r ${CICS_REGION} -c ud IE01019 CICSPassword="IE01019" TSLKeyList=5

# Define XA Conenction
echo ----- XA Connection definition -----
cicsadd -c xad -r ${CICS_REGION} XADdef SwitchLoadFile=db2xa XAOpen="pilote,dbuser,dbuser"

# Opening port for remote connections 
echo ----- Opening 60180 TCP port -----
iptables -I INPUT -p tcp --dport 60180 --syn -j ACCEPT

echo ----- Copie des binaires sous /var/cics_regions/${CICS_REGION}/bin -----
cp $ROOT_DIR/EXE/*.so /var/cics_regions/${CICS_REGION}/bin/ 
cp $ROOT_DIR/MAP/*.map /var/cics_regions/${CICS_REGION}/bin/
cp $CICSPATH/maps/fr_FR/dfhcesn.map /var/cics_regions/${CICS_REGION}/bin/

#echo '>>>> WARNING: 2 actions sont a faire pour completer la configuration  <<<<'
#echo '   1. Ajouter les deux variables ci-dessous au niveau du fichier /var/cics_regions/${CICS_REGION}/environment:
#		CICS_PROGRAM_PATH=/opt/ibm/cics/samples/ivp/COBOLIT:/opt/ibm/cics/bin:/var/cics_regions/${CICS_REGION}/bin
#		COB_LIBRARY_PATH=/opt/ibm/cics/samples/ivp/COBOLIT:/opt/ibm/cics/bin:/var/cics_regions/${CICS_REGION}/bin'
echo "CICS_PROGRAM_PATH=/opt/ibm/cics/samples/ivp/COBOLIT:/opt/ibm/cics/bin:/var/cics_regions/${CICS_REGION}/bin" >> /var/cics_regions/${CICS_REGION}/environment
echo "COB_LIBRARY_PATH=/opt/ibm/cics/samples/ivp/COBOLIT:/opt/ibm/cics/bin:/var/cics_regions/${CICS_REGION}/bin" >> /var/cics_regions/${CICS_REGION}/environment

#echo '   2. Mettre a jour le fichier /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza
#	        Entry MD16:
#		        La valeur de TCTUALen devient 255 au lieu de 0 (TCTUALen = 255 )'

awk '/MD16/,/TCTUALen/ {if ( $0 ~ /TCTUALen/) {sub("= 0","= 255",$0);print;next}}{print}' /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza > /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza.awk
mv /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza.awk /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza
# Convert all received data from terminal to UPPERCASE (Attribute UCTranFlag of terminal definition) 
awk '/MD16/,/UCTranFlag/ {if ( $0 ~ /UCTranFlag/) {sub("= no", "= yes",$0);print;next}}{print}' /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza > /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza.awk
mv /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza.awk /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza
chmod 660 /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza
chown cics:cics /var/cics_regions/${CICS_REGION}/database/WD/WD.stanza

#cicscp start region ${CICS_REGION} StartType=cold
