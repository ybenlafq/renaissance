#!/usr/bin/env bash    
echo === Resource definition for region CONVERT ===
echo === Autoinstall configuration for Programs and MAPs ===
cd /opt/ibm/cics/samples/userexit
make 
cp cicsuxit /var/cics_regions/CONVERT/bin 
cicsadd -c pd -r CONVERT UXIT PathName="cicsuxit" UserExitNumber=13
echo === TRANSACTIONs Definition ===
echo === TERMINALs Definition ===
echo === FILEs Definition ===
echo === TDQUEUEs Definition ===
echo === TRANCLASSes Definition ===
echo === TSMODELs Definition ===
echo === JOURNALMODELs Definition ===
