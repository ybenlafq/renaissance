#!/usr/bin/env bash

QMgrName=MQ1
MQResources=MQDEFS
QAlias=QALIAS
# File for test configuration: Replace QREMOTE => QLOCAL
MQ_Test=MQ_Test

echo Creating Queue Manager : $QMgrName
crtmqm $QMgrName

echo Starting Queue Manager : $QMgrName
strmqm $QMgrName

echo Defining resources in Queue Manager : $QMgrName
runmqsc $QMgrName < $MQResources > MQDEFS.log

echo Defining other QALIASes in Queue Manager : $QMgrName 
runmqsc $QMgrName < $QAlias > QALIAS.log
arg=$(echo $1 | awk '{print toupper($0)}')

if [[ "$arg" = "TEST" ]] ; then
  echo Adapting MQ configuration for test: QREMOTE =\> QLOCAL
  runmqsc $QMgrName < $MQ_Test  > MQ_Test.log
fi
