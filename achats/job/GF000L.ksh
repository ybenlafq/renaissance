#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GF000L.ksh                       --- VERSION DU 07/10/2016 22:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGF000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/12/17 AT 14.12.46 BY BURTECN                      
#    STANDARDS: P  JOBSET: GF000L                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='LILLE'                                                             
# ********************************************************************         
#  B G F 0 0 0                                                                 
# ********************************************************************         
#   LISTE DES COMMANDES NON-SOLDEES TRIEES PAR ENTITE DE COMMANDE              
#   INTERLOCUTEUR N DE COMMANDE FAMILLE MARQUE ARTICLE                         
#   TRAITEMENT DU DIMANCHE                                                     
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GF000LA
       ;;
(GF000LA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       RUN=${RUN}
       JUMP_LABEL=GF000LAA
       ;;
(GF000LAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  LISTE DES COMMANDES NON-SOLDEES                                      
       m_OutputAssign -c 9 -w BGF000 SYSCONTL
# ******  COMMANDES FOURNISSEUR                                                
#    RSGF10L  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10L /dev/null
# ******  LIGNES COMMANDES FOURNISSEUR                                         
#    RSGF20L  : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20L /dev/null
# ******  LIGNES DE COMMANDES ECHEANCEES                                       
#    RSGF30L  : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30L /dev/null
# ******  LIGNES DE COMMANDES DEMANDEURS                                       
#    RSGF15L  : NAME=RSGF15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF15L /dev/null
# ******  ARTICLES                                                             
#    RSGA00L  : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
# ******  ENTITES DE COMMANDES                                                 
#    RSGA06L  : NAME=RSGA06L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06L /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10L  : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10L /dev/null
# ******* SPECIFIQUE ARTICLE GERES PAR ENTREPOT EXTERIEUR                      
#    RSFL50L  : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50L /dev/null
#                                                                              
#                                                                              
# ******  PARAMETRE SOCIETE : 961                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF000 
       JUMP_LABEL=GF000LAB
       ;;
(GF000LAB)
       m_CondExec 04,GE,GF000LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G F 0 1 5                                                                 
# ********************************************************************         
#  VENTILATION DES NOMBRES D U O PRIS OU A PRENDRE                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF000LAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GF000LAD
       ;;
(GF000LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  LISTE DE LA VENTILATION DES NOMBRES D U O                            
       m_OutputAssign -c 9 -w BGF015 IMPR
# ******  COMMANDES FOURNISSEUR                                                
#    RSGF10L  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10L /dev/null
# ******  LIGNES DE COMMANDES ECHEANCEES                                       
#    RSGF30L  : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30L /dev/null
# ******  PLANNING DE LIVRAISON COMMANDES FOURNISSEURS                         
#    RSGF35L  : NAME=RSGF35L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGF35L /dev/null
# ******  ARTICLES                                                             
#    RSGA00L  : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
# ******  CHEFS PRODUITS                                                       
#    RSGA02L  : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02L /dev/null
# ******  LIEUX                                                                
#    RSGA10L  : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10L /dev/null
# ******  RECEPTIONS                                                           
#    RSGR00L  : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR00L /dev/null
# ******  LIGNES DE RECEPTION ECHEANCEES                                       
#    RSGR10L  : NAME=RSGR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR10L /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE : 961                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF015 
       JUMP_LABEL=GF000LAE
       ;;
(GF000LAE)
       m_CondExec 04,GE,GF000LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G F 0 1 0                                                                 
# ********************************************************************         
#  CREATION DE L ETAT DESCRIPTIF DE COMMANDE                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF000LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GF000LAG
       ;;
(GF000LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  COMMANDES FOURNISSEUR                                                
#    RSGF10L  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10L /dev/null
# ******  LIGNES COMMANDES FOURNISSEUR DEMANDEURS                              
#    RSGF15L  : NAME=RSGF15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF15L /dev/null
# ******  LIGNES DE COMMANDES ECHEANCEES                                       
#    RSGF30L  : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30L /dev/null
# ******  MVTS DE CREATION ET DE MAJ DE COMMANDES                              
#    RSGF45L  : NAME=RSGF45,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF45L /dev/null
# ******  ARTICLES                                                             
#    RSGA00L  : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10L  : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10L /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE : 961                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  LISTE DE L ETAT DESCRIPTIF DES COMMANDES                             
       m_OutputAssign -c 9 -w BGF010 IMPR
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF010 
       JUMP_LABEL=GF000LAH
       ;;
(GF000LAH)
       m_CondExec 04,GE,GF000LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G F 0 2 0                                                                 
# ********************************************************************         
#  CREATION DE L ETAT VALORISATION DE COMMANDE                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF000LAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GF000LAJ
       ;;
(GF000LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  COMMANDES FOURNISSEUR                                                
#    RSGF10L  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10L /dev/null
# ******  LIGNES DE COMMANDES FOURNISSEUR                                      
#    RSGF20L  : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20L /dev/null
# ******  RISTOURNES SUR LES LIGNES DE COMMANDES FOURNISSEUR                   
#    RSGF25L  : NAME=RSGF25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF25L /dev/null
# ******  MVTS DE CREATION ET DE MAJ DE COMMANDES                              
#    RSGF45L  : NAME=RSGF45,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF45L /dev/null
# ******  VALORISATION DES ESCLAVES LIBRE DES COMMANDES FOURNI                 
#    RSGF50L  : NAME=RSGF50L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGF50L /dev/null
# ******  ARTICLES                                                             
#    RSGA00L  : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10L  : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10L /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE : 961                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  LISTE DE L ETAT VALORISATION DE COMMANDES                            
       m_OutputAssign -c 9 -w BGF020 IMPR
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF020 
       JUMP_LABEL=GF000LAK
       ;;
(GF000LAK)
       m_CondExec 04,GE,GF000LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
