#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GR002D.ksh                       --- VERSION DU 17/10/2016 18:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGR002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/03/17 AT 10.14.52 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GR002D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ******M*************************************************************         
#  QUIESCE DES TABLESPACES RSGG50D,RSGG60D,RSGG70D                             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GR002DA
       ;;
(GR002DA)
#
#GR002DAA
#GR002DAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GR002DAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GR002DAD
       ;;
(GR002DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES RECEPTIONS FOURNISSEURS                                    
#    RSGR00D  : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR00D /dev/null
# ******  TABLE DES LIGNES DE RECEPTIONS FOURNISSEURS PAR SOCIETES /           
#    RSGR15D  : NAME=RSGR15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR15D /dev/null
# ******  TABLE DES LIGNES DE COMMANDES FOURNISSEURS                           
#    RSGF20D  : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20D /dev/null
# ******  TABLE DES ARTICLES                                                   
#    RSGA00D  : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00D /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# *******                                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *******                                                                      
# ******  CREATION DU FIC-EXTRACTION DES RECEPTIONS                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB07 ${DATA}/PTEM/GR002DAD.BGB007AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR002 
       JUMP_LABEL=GR002DAE
       ;;
(GR002DAE)
       m_CondExec 04,GE,GR002DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 0 6                                                                 
# ********************************************************************         
#   SELECTION DES M.A.J EFFECTUEES SUR LES RECEPTIONS QUI NE                   
#      DATENT PAS DE LA JOURNEE DE TRAITEMENT                                  
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GR002DAG
       ;;
(GR002DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES RECEPTIONS FOURNISSEURS                                    
#    RSGR00D  : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR00D /dev/null
# ******  TABLE DES LIGNES DE RECEPTIONS FOURNISSEURS PAR SOCIETES,LIE         
#    RSGR15D  : NAME=RSGR15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR15D /dev/null
# ******  TABLE DES MOUVEMENTS DE RECEPTION DU JOUR                            
#    RSGR20D  : NAME=RSGR20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR20D /dev/null
# ******  TABLE DES LIGNES DE COMMANDES FOURNISSEURS                           
#    RSGF20D  : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20D /dev/null
# ******  TABLE DES HISTORIQUES DE RECEPTION                                   
#    RSGG70D  : NAME=RSGG70D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70D /dev/null
# ******  TABLE DES ARTICLES                                                   
#    RSGA00D  : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00D /dev/null
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CREATION DU FICHIER DES M.A.J EFFECTUEES                             
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB08 ${DATA}/PTEM/GR002DAG.BGB008AD
#                                                                              
# ******  PARAMETRE SOCIETE : 991                                              
       m_FileAssign -d SHR FSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR006 
       JUMP_LABEL=GR002DAH
       ;;
(GR002DAH)
       m_CondExec 04,GE,GR002DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PAR CODIC,TYPE ENREGISTREMENT,NUM-RECEPTION,NUM-COMMANDE ET             
#    DATE MODIFICATION  POUR CREATION DE FGB10                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GR002DAJ
       ;;
(GR002DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS EXTRACTIONS ET FIC M.A.J                                    
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GR002DAD.BGB007AD
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/GR002DAG.BGB008AD
# ******  FICHIER SORTIE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/GR002DAJ.BGB010AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_1 1 CH 1
 /FIELDS FLD_CH_59_6 59 CH 6
 /FIELDS FLD_CH_24_7 24 CH 7
 /FIELDS FLD_CH_3_7 3 CH 7
 /FIELDS FLD_CH_31_7 31 CH 7
 /KEYS
   FLD_CH_3_7 ASCENDING,
   FLD_CH_1_1 ASCENDING,
   FLD_CH_24_7 ASCENDING,
   FLD_CH_31_7 ASCENDING,
   FLD_CH_59_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002DAK
       ;;
(GR002DAK)
       m_CondExec 00,EQ,GR002DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 2                                                                 
# ********************************************************************         
#   EXTRACTION DES DEMANDES DE RECYCLAGE DE PRMP                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GR002DAM
       ;;
(GR002DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES                                                   
#    RSGA00D  : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00D /dev/null
# ******  TABLE DES DEMANDES DE RECYCLAGES DE PRA                              
#    RSGG60D  : NAME=RSGG60D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG60D /dev/null
# ******  TABLE DES HISTORIQUES DE RECEPTIONS                                  
#    RSGG70D  : NAME=RSGG70D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70D /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER EXTRACTION DES DEMANDES DE RECYCLAGE DE PRMP                 
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB09 ${DATA}/PTEM/GR002DAM.BGB009AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR012 
       JUMP_LABEL=GR002DAN
       ;;
(GR002DAN)
       m_CondExec 04,GE,GR002DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 3                                                                 
# ********************************************************************         
#  CALCUL DU STOCK SOCIETE POUR LES RECEPTIONS DU JOUR                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002DAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GR002DAQ
       ;;
(GR002DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISE : SOUS TABLE EXMAG                                  
#    RSGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01D /dev/null
# ******  TABLE DES STOCKS DE L ENTREPOT                                       
#    RSGS10D  : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10D /dev/null
# ******  TABLE DES STOCKS SOUS LIEUX ENTREPOT                                 
#    RSGS30D  : NAME=RSGS30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30D /dev/null
# ******  TABLE DES MVTS DE RECEPTION DU JOUR                                  
#    RSGR20D  : NAME=RSGR20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR20D /dev/null
# ******  FICHIER EXTRACTION                                                   
       m_FileAssign -d SHR -g ${G_A3} FGB09 ${DATA}/PTEM/GR002DAM.BGB009AD
# ******  FICHIER EXTRACTIONS TRIE                                             
       m_FileAssign -d SHR -g ${G_A4} FGB10 ${DATA}/PTEM/GR002DAJ.BGB010AD
# ******  FICHIER STOCK SOCIETE POUR RECEP DU JOUR                             
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB11 ${DATA}/PTEM/GR002DAQ.BGB011AD
#                                                                              
# *******                                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR013 
       JUMP_LABEL=GR002DAR
       ;;
(GR002DAR)
       m_CondExec 04,GE,GR002DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER STOCK SOCIETE                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002DAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GR002DAT
       ;;
(GR002DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER EXTRACTION                                                   
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GR002DAQ.BGB011AD
# ******  FICHIER EXTRACTIONS TRIES                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PGB991/F91.BGB013AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_1 1 CH 1
 /FIELDS FLD_CH_16_8 16 CH 8
 /FIELDS FLD_CH_3_7 3 CH 7
 /FIELDS FLD_CH_24_14 24 CH 14
 /KEYS
   FLD_CH_3_7 ASCENDING,
   FLD_CH_16_8 ASCENDING,
   FLD_CH_1_1 ASCENDING,
   FLD_CH_24_14 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002DAU
       ;;
(GR002DAU)
       m_CondExec 00,EQ,GR002DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 4                                                                 
# ********************************************************************         
#   RECALCUL DU PRMP ET MAJ DES TABLES RTGG50 RTGG60 RTGG70                    
#   REPRISE: OUI SI ABEND                                                      
#          : NON SI FIN NORMALE FAIRE UN RECOVER TO RBA DES TABLES             
#            RTGG50 - RTGG60 - RTGG70 A PARTIR DU QUIESCE DU DEBUT DE          
#            LA CHAINE                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002DAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GR002DAX
       ;;
(GR002DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE DES PRMP DU JOUR                                               
#    RSGG50D  : NAME=RSGG50D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50D /dev/null
#    RSGA06D  : NAME=RSGA06D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06D /dev/null
#    RSGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01D /dev/null
# ******  TABLE DES DEMANDES DE RECYCLAGES DE PRA                              
#    RSGG60D  : NAME=RSGG60D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGG60D /dev/null
# ******  TABLE DES HISTORIQUES DE RECEPTIONS                                  
#    RSGG70D  : NAME=RSGG70D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70D /dev/null
#                                                                              
# ******  FICHIER EXTRACTION CONTROLE TRIE                                     
       m_FileAssign -d SHR -g ${G_A6} FGB13 ${DATA}/PGB991/F91.BGB013AD
# ******  FICHIER DES RECYCLAGE DE PRMP                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB16 ${DATA}/PTEM/GR002DAX.BGB016AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR014 
       JUMP_LABEL=GR002DAY
       ;;
(GR002DAY)
       m_CondExec 04,GE,GR002DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#   EASYTREAVE                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002DBA PGM=EZTPA00    ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GR002DBA
       ;;
(GR002DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#  FICHIER DES RECYCLAGE DE PRMP                                               
       m_FileAssign -d SHR -g ${G_A7} FILEA ${DATA}/PTEM/GR002DAX.BGB016AD
       m_FileAssign -d NEW,CATLG,DELETE -r 108 -g +1 FILEB ${DATA}/PTEM/GR002DBA.BGR002AD
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX4/GR002DBA
       m_ProgramExec GR002DBA
# ********************************************************************         
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  TRI DU FICHIER ISSU DE L'EASYTRIEVE                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002DBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GR002DBD
       ;;
(GR002DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER EXTRACTION                                                          
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GR002DBA.BGR002AD
#  FICHIER EXTRACTIONS TRIES                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GR002DBD.BGR002BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_9_1 09 CH 01
 /FIELDS FLD_BI_15_7 15 CH 07
 /FIELDS FLD_BI_10_6 10 CH 06
 /FIELDS FLD_BI_7_2 07 CH 02
 /KEYS
   FLD_BI_7_2 ASCENDING,
   FLD_BI_9_1 ASCENDING,
   FLD_BI_10_6 ASCENDING,
   FLD_BI_15_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002DBE
       ;;
(GR002DBE)
       m_CondExec 00,EQ,GR002DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002DBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GR002DBG
       ;;
(GR002DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******************** TABLES DU GENERATEUR D'ETATS                            
#    RTEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/GR002DBD.BGR002BD
# *********************************** FICHIER FCUMULS                          
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GR002DBG.BGR002CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GR002DBH
       ;;
(GR002DBH)
       m_CondExec 04,GE,GR002DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  TRI DU FICHIER FCUMUL                                                       
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002DBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GR002DBJ
       ;;
(GR002DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMUL                           
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/GR002DBG.BGR002CD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GR002DBJ.BGR002DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002DBK
       ;;
(GR002DBK)
       m_CondExec 00,EQ,GR002DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IGR016                                                 
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002DBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GR002DBM
       ;;
(GR002DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A11} FEXTRAC ${DATA}/PTEM/GR002DBD.BGR002BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FCUMULS ${DATA}/PTEM/GR002DBJ.BGR002DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG132 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w BGR016 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GR002DBN
       ;;
(GR002DBN)
       m_CondExec 04,GE,GR002DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GR002DZA
       ;;
(GR002DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GR002DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
