#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GK004F.ksh                       --- VERSION DU 09/10/2016 05:55
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFGK004 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/12/29 AT 10.15.32 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GK004F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BGK141  : ENRICHISSEMENT DU FICHIER ACHAT KESAA                             
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GK004FA
       ;;
(GK004FA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GK004FAA
       ;;
(GK004FAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
# *****   ACHAT AS400 DACEM                                                    
       m_FileAssign -d SHR FGK140 /dev/null
# *****   PRODUITS CONSOLIDES                                                  
       m_FileAssign -d SHR -g +0 FGK020 ${DATA}/PXX0/F99.BGK130BP
#                                                                              
# *****   FICHIER ACHAT DACEM ENRICHI                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -g +1 FGK141 ${DATA}/PXX0/F99.BGK141AP
       m_ProgramExec BGK141 
# ********************************************************************         
#  MERGE FICHIER  ACHAT DACEM AVEC BGK140BP (CONSO GROUPE KESA)                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK004FAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GK004FAD
       ;;
(GK004FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F99.BGK140BP
       m_FileAssign -d SHR -g ${G_A1} -C ${DATA}/PXX0/F99.BGK141AP
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -g +1 SORTOUT ${DATA}/PXX0/F99.BGK140DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_25_7 25 CH 7
 /KEYS
   FLD_CH_25_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK004FAE
       ;;
(GK004FAE)
       m_CondExec 00,EQ,GK004FAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PASSE BGK050AP AU FORMAT KESA FOURNISSEUR                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK004FAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GK004FAG
       ;;
(GK004FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BGK050AP
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 SORTOUT ${DATA}/PTEM/GK004FAG.BGK050BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK004FAH
       ;;
(GK004FAH)
       m_CondExec 00,EQ,GK004FAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  MERGE FICHIER FOURNISSEUR  DACEM AVEC BGK050AP (CONSO GROUPE KESA)          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK004FAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GK004FAJ
       ;;
(GK004FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GK004FAG.BGK050BP
# ****    FILE  NAME=KESAFB,MODE=I                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 SORTOUT ${DATA}/PXX0/F99.BGK050DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK004FAK
       ;;
(GK004FAK)
       m_CondExec 00,EQ,GK004FAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK135 : AJOUT DU PRMP DACEM SUR LE FICHIER CONSO KESA DES VENTES           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK004FAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GK004FAM
       ;;
(GK004FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  DATE  JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  ARTICLES                                                             
#    RSGA00B  : NAME=RSGA00B,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00B /dev/null
# ******  PRMP                                                                 
#    RSGG50B  : NAME=RSGG50B,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50B /dev/null
#                                                                              
# ******  FIC   CUMUL VENTES LIVREES FILIALES  CONSOLIDEES DANS GK003          
       m_FileAssign -d SHR -g +0 FGK130I ${DATA}/PXX0/F99.BGK130CP
# ******  FIC   VENTES LIVREES FILIALES AVEC PRMP DACEM                        
       m_FileAssign -d NEW,CATLG,DELETE -r 78 -g +1 FGK130O ${DATA}/PXX0/F99.BGK130DP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGK135 
       JUMP_LABEL=GK004FAN
       ;;
(GK004FAN)
       m_CondExec 04,GE,GK004FAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DELETE DES FIC A DESTINATION DE KESA                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK004FAQ PGM=IDCAMS     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GK004FAQ
       ;;
(GK004FAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK004FAQ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GK004FAR
       ;;
(GK004FAR)
       m_CondExec 16,NE,GK004FAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO SUR SEQUENTIEL DES FIC A DESTINATION DE KESA                          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK004FAT PGM=IDCAMS     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GK004FAT
       ;;
(GK004FAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A3} IN1 ${DATA}/PXX0/F99.BGK140DP
       m_FileAssign -d SHR -g ${G_A4} IN2 ${DATA}/PXX0/F99.BGK130DP
       m_FileAssign -d SHR -g +0 IN3 ${DATA}/PXX0/F99.BGK120BP
       m_FileAssign -d SHR -g +0 IN4 ${DATA}/PXX0/F99.BGK010AP
       m_FileAssign -d SHR -g ${G_A5} IN5 ${DATA}/PXX0/F99.BGK050DP
       m_FileAssign -d NEW,CATLG,DELETE -r 71 OUT1 ${DATA}/PXX0.F99.BGK140KP
       m_FileAssign -d NEW,CATLG,DELETE -r 78 OUT2 ${DATA}/PXX0.F99.BGK130KP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 OUT3 ${DATA}/PXX0.F99.BGK120KP
       m_FileAssign -d NEW,CATLG,DELETE -r 29 OUT4 ${DATA}/PXX0.F99.BGK010KP
       m_FileAssign -d NEW,CATLG,DELETE -r 27 OUT5 ${DATA}/PXX0.F99.BGK050KP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK004FAT.sysin
       m_UtilityExec
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GK004FAU
       ;;
(GK004FAU)
       m_CondExec 16,NE,GK004FAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GK004FZA
       ;;
(GK004FZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK004FZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
