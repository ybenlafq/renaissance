#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GK002L.ksh                       --- VERSION DU 09/10/2016 05:24
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGK002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/11/30 AT 12.12.36 BY BURTECR                      
#    STANDARDS: P  JOBSET: GK002L                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  TRI DU FIC HISTO DES VENTES                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GK002LA
       ;;
(GK002LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GK002LAA
       ;;
(GK002LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F61.HV01AL
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PXX0/GK002LAA.BGK030FL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_14_6 14 CH 6
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_14_6 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK002LAB
       ;;
(GK002LAB)
       m_CondExec 00,EQ,GK002LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK030F: CREATION DU FICHIER STATS DE VENTES LIVREES A PARTIR DE HV         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK002LAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GK002LAD
       ;;
(GK002LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* PRMP                                                                 
#    RSGG50L  : NAME=RSGG50L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50L /dev/null
# ******* ZONE DE PRIX                                                         
#    RSGA59L  : NAME=RSGA59L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59L /dev/null
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DATE MMSSAA                                                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******* SOCIETE                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
#                                                                              
# ******* FIC HISTO STAT DE VENTE LIVREE                                       
       m_FileAssign -d SHR -g ${G_A1} RTHV01 ${DATA}/PXX0/GK002LAA.BGK030FL
# ******* FIC STAT DE VENTE LIVREE                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -g +1 FGK030 ${DATA}/PXX0/F61.BGK030BL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGK030F 
       JUMP_LABEL=GK002LAE
       ;;
(GK002LAE)
       m_CondExec 04,GE,GK002LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK045 : CREATION DU FICHIER DES PRA                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK002LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GK002LAG
       ;;
(GK002LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* PRA                                                                  
#    RSGG70L  : NAME=RSGG70L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70L /dev/null
#                                                                              
# ******* DATE MMSSAA                                                          
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ******* FIC STAT DE VENTE LIVREE                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 13 -g +1 FGK045 ${DATA}/PXX0/F61.BGK045BL
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGK045 
       JUMP_LABEL=GK002LAH
       ;;
(GK002LAH)
       m_CondExec 04,GE,GK002LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GK002LZA
       ;;
(GK002LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK002LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
