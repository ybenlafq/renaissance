#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GK001O.ksh                       --- VERSION DU 09/10/2016 05:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGK001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/02/11 AT 14.03.44 BY BURTECN                      
#    STANDARDS: P  JOBSET: GK001O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BRM000 : EXTRACTION DES DONNEES ARTICLE-FAMILLE                             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GK001OA
       ;;
(GK001OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GK001OAA
       ;;
(GK001OAA)
       m_CondExec ${EXAAA},NE,YES 
#                                                                              
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* ARTICLE                                                              
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
# ******* COCIC / VALEUR CODE MARKETING                                        
#    RSGA09O  : NAME=RSGA09O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09O /dev/null
# ******* LIEUX                                                                
#    RSGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10O /dev/null
# ******* RELATION ETAT / FAMILLE RAYON                                        
#    RSGA11O  : NAME=RSGA11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11O /dev/null
# ******* RELATION FAMILLE / CODE MARKETING                                    
#    RSGA12O  : NAME=RSGA12O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12O /dev/null
# ******* TABLE FAMILLES                                                       
#    RSGA14O  : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14O /dev/null
# ******* LIBELLES IMBRICATIONS DES CODES MARKETING                            
#    RSGA29O  : NAME=RSGA29O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA29O /dev/null
# ******* RELATION ARTICLE MODES DE DELIVRANCES                                
#    RSGA64O  : NAME=RSGA64O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA64O /dev/null
# ******* RELATION ARTICLE RAYON CODE MARKETING                                
#    RSGA83O  : NAME=RSGA83O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA83O /dev/null
#                                                                              
# ******* SOCIETE = 916                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* PARAMETRE EDITION= 'KESA'                                            
       m_FileAssign -d SHR FETAT ${DATA}/CORTEX4.P.MTXTFIX1/BRM000ET
#                                                                              
# ******* DONNEES MUTATIONS                                                    
       m_FileAssign -d SHR FMU235 /dev/null
# ******* DONNEES DESTINEES AU LOAD RTRM60                                     
       m_FileAssign -d SHR RTRM60 /dev/null
# ******* DONNEES DESTINEES AU LOAD RTRM65                                     
       m_FileAssign -d SHR RTRM65 /dev/null
# ******* FIC ARTICLE ET FAMILLE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FRMCOM ${DATA}/PTEM/GK001OAA.BRM001EO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM000 
       JUMP_LABEL=GK001OAB
       ;;
(GK001OAB)
       m_CondExec 04,GE,GK001OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK020 : CREATION DU FICHIER COMPLET ARTICLES                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK001OAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GK001OAD
       ;;
(GK001OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* ARTICLE                                                              
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
# ******* MARQUES                                                              
#    RSGA22O  : NAME=RSGA22O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22O /dev/null
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FIC ARTICLE ET FAMILLE                                               
       m_FileAssign -d SHR -g ${G_A1} FRMCOM ${DATA}/PTEM/GK001OAA.BRM001EO
# ******* DATE DE DERNIER PASSAGE                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/BGK020O
#                                                                              
# ******* FIC ARTICLE COMPLET                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 113 -g +1 FGK020 ${DATA}/PXX0/F16.BGK020AO
# ******* FIC ARTICLE-MAJ COMPLET                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 FGK120 ${DATA}/PXX0/F16.BGK120AO
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGK020 
       JUMP_LABEL=GK001OAE
       ;;
(GK001OAE)
       m_CondExec 04,GE,GK001OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GK001OZA
       ;;
(GK001OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK001OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
