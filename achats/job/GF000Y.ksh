#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GF000Y.ksh                       --- VERSION DU 09/10/2016 05:40
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGF000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/12/17 AT 14.16.16 BY BURTECN                      
#    STANDARDS: P  JOBSET: GF000Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='LYON'                                                              
# ********************************************************************         
#  B G F 0 0 0                                                                 
# ********************************************************************         
#   LISTE DES COMMANDES NON-SOLDEES TRIEES PAR ENTITE DE COMMANDE              
#   INTERLOCUTEUR N DE COMMANDE FAMILLE MARQUE ARTICLE                         
#   TRAITEMENT DU DIMANCHE                                                     
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GF000YA
       ;;
(GF000YA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       RUN=${RUN}
       JUMP_LABEL=GF000YAA
       ;;
(GF000YAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  LISTE DES COMMANDES NON-SOLDEES                                      
       m_OutputAssign -c 9 -w BGF000 SYSCONTL
# ******  COMMANDES FOURNISSEUR                                                
#    RSGF10Y  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10Y /dev/null
# ******  LIGNES COMMANDES FOURNISSEUR                                         
#    RSGF20Y  : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20Y /dev/null
# ******  LIGNES DE COMMANDES ECHEANCEES                                       
#    RSGF30Y  : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30Y /dev/null
# ******  LIGNES DE COMMANDES DEMANDEURS                                       
#    RSGF15Y  : NAME=RSGF15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF15Y /dev/null
# ******  ARTICLES                                                             
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ******  ENTITES DE COMMANDES                                                 
#    RSGA06Y  : NAME=RSGA06Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06Y /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******* SPECIFIQUE ARTICLE GERES PAR ENTREPOT EXTERIEUR                      
#    RSFL50Y  : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50Y /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE : 945                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF000 
       JUMP_LABEL=GF000YAB
       ;;
(GF000YAB)
       m_CondExec 04,GE,GF000YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G F 0 1 5                                                                 
# ********************************************************************         
#  VENTILATION DES NOMBRES D U O PRIS OU A PRENDRE                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF000YAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GF000YAD
       ;;
(GF000YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  LISTE DE LA VENTILATION DES NOMBRES D U O                            
# ******  LISTE PLUS  UTILISEE CGA LE 041198                                   
# IMPR     REPORT SYSOUT=*                                                     
       m_OutputAssign -c 9 -w BGF015 IMPR
# ******  COMMANDES FOURNISSEUR                                                
#    RSGF10Y  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10Y /dev/null
# ******  LIGNES DE COMMANDES ECHEANCEES                                       
#    RSGF30Y  : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30Y /dev/null
# ******  PLANNING DE LIVRAISON COMMANDES FOURNISSEURS                         
#    RSGF35Y  : NAME=RSGF35Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGF35Y /dev/null
# ******  ARTICLES                                                             
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ******  CHEFS PRODUITS                                                       
#    RSGA02Y  : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02Y /dev/null
# ******  LIEUX                                                                
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******  RECEPTIONS                                                           
#    RSGR00Y  : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR00Y /dev/null
# ******  LIGNES DE RECEPTION ECHEANCEES                                       
#    RSGR10Y  : NAME=RSGR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR10Y /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE : 945                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF015 
       JUMP_LABEL=GF000YAE
       ;;
(GF000YAE)
       m_CondExec 04,GE,GF000YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G F 0 1 0                                                                 
# ********************************************************************         
#  CREATION DE L ETAT DESCRIPTIF DE COMMANDE                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF000YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GF000YAG
       ;;
(GF000YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  COMMANDES FOURNISSEUR                                                
#    RSGF10Y  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10Y /dev/null
# ******  LIGNES COMMANDES FOURNISSEUR DEMANDEURS                              
#    RSGF15Y  : NAME=RSGF15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF15Y /dev/null
# ******  LIGNES DE COMMANDES ECHEANCEES                                       
#    RSGF30Y  : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30Y /dev/null
# ******  MVTS DE CREATION ET DE MAJ DE COMMANDES                              
#    RSGF45Y  : NAME=RSGF45,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF45Y /dev/null
# ******  ARTICLES                                                             
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE : 945                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  LISTE DE L ETAT DESCRIPTIF DES COMMANDES                             
       m_OutputAssign -c 9 -w BGF010 IMPR
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF010 
       JUMP_LABEL=GF000YAH
       ;;
(GF000YAH)
       m_CondExec 04,GE,GF000YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G F 0 2 0                                                                 
# ********************************************************************         
#  CREATION DE L ETAT VALORISATION DE COMMANDE                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF000YAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GF000YAJ
       ;;
(GF000YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  COMMANDES FOURNISSEUR                                                
#    RSGF10Y  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10Y /dev/null
# ******  LIGNES DE COMMANDES FOURNISSEUR                                      
#    RSGF20Y  : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20Y /dev/null
# ******  RISTOURNES SUR LES LIGNES DE COMMANDES FOURNISSEUR                   
#    RSGF25Y  : NAME=RSGF25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF25Y /dev/null
# ******  MVTS DE CREATION ET DE MAJ DE COMMANDES                              
#    RSGF45Y  : NAME=RSGF45,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF45Y /dev/null
# ******  VALORISATION DES ESCLAVES LIBRE DES COMMANDES FOURNI                 
#    RSGF50Y  : NAME=RSGF50Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGF50Y /dev/null
# ******  ARTICLES                                                             
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE : 945                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  LISTE DE L ETAT VALORISATION DE COMMANDES                            
       m_OutputAssign -c 9 -w BGF020 IMPR
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF020 
       JUMP_LABEL=GF000YAK
       ;;
(GF000YAK)
       m_CondExec 04,GE,GF000YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
