#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FK210F.ksh                       --- VERSION DU 17/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFFK210 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/27 AT 10.50.13 BY BURTECA                      
#    STANDARDS: P  JOBSET: FK210F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  DELETE AVANT TRAITEMENT DES FICHIERS NON GDG                                
#         POUR RECR�ATION ET ENVOI VERS GFK.                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FK210FA
       ;;
(FK210FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FK210FAA
       ;;
(FK210FAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :            **                                       
#    OBLIGATOIRE POUR LOGIQUE APPL    **                                       
# **************************************                                       
# ******* FICHIER POUR GFK (CODIC/NSOCVTE/NLIEUVTE)                            
#    IN1      : NAME=BFK220AP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR IN1 /dev/null
#    IN2      : NAME=BFK220BP,MODE=I - DYNAM=YES                               
       m_FileAssign -d SHR IN2 /dev/null
# ******  FIC SORTIE DU PKZIP (ENVOI VERS GFK)                                 
#    IN3      : NAME=GFK000F,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR IN3 /dev/null
# ******  FIC SORTIE DU PKZIP (ENVOI VERS FRSECURESER@CONTEXTWORLD.COM         
# ******  SEVERINE.BAUZEDAT@DARTY.FR)                                          
#    IN4      : NAME=CONTEXT,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FK210FAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FK210FAB
       ;;
(FK210FAB)
       m_CondExec 16,NE,FK210FAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BFK210                                                                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FK210FAD PGM=BFK210     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FK210FAD
       ;;
(FK210FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER DE CONSTRUCTION DU SELECT                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FFK210 ${DATA}/PTEM/FK210FAD.BFK210AP
# ******* SYSIN DES FILIALES A UNLOADER                                        
       m_FileAssign -d SHR FMGI ${DATA}/CORTEX4.P.MTXTFIX1/FK201F1
# ******* PREPARATION DU FAST UNLOAD.                                          
       m_FileAssign -d SHR FSYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FK201F2.sysin
       m_ProgramExec BFK210 
# ********************************************************************         
#  FAST UNLOAD DE RTGS40                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FK210FAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FK210FAG
       ;;
(FK210FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
# ******* TABLES EN LECTURE                                                    
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
# ******* FICHIERS D'UNLOAD DE LA RTGS40P ET DES RTGS42                        
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/FK210FAG.GS40UNFP
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SYSREC02 ${DATA}/PTEM/FK210FAG.GS42UNFD
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SYSREC03 ${DATA}/PTEM/FK210FAG.GS42UNFL
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SYSREC04 ${DATA}/PTEM/FK210FAG.GS42UNFP
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SYSREC05 ${DATA}/PTEM/FK210FAG.GS42UNFY
# ******* SYSIN D'UNLOAD                                                       
       m_FileAssign -d SHR -g ${G_A1} SYSIN ${DATA}/PTEM/FK210FAD.BFK210AP
       
       #Untranslated utility: PTLDRIVM##Untranslate PTLDRIVM SYSIN

#                                                                              
# ********************************************************************         
#  PGM : SORT                                                                  
#  FICHIERS FAST UNLOAD TRIE (TS LES MVTS GS40 CONFONDUS).                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FK210FAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FK210FAJ
       ;;
(FK210FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* RESULTATS DES UNLOAD PR�C�DENTS                                      
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/FK210FAG.GS40UNFP
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/FK210FAG.GS42UNFD
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/FK210FAG.GS42UNFL
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/FK210FAG.GS42UNFP
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/FK210FAG.GS42UNFY
# ******* FICHIER DE CUMUL DES UNLOAD TRI�S                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FK210FAJ.BFK210BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_22_6 22 CH 6
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_22_6 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FK210FAK
       ;;
(FK210FAK)
       m_CondExec 00,EQ,FK210FAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BFK200                                                                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FK210FAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FK210FAM
       ;;
(FK210FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ***** TABLES EN LECTURE                                                      
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA11 /dev/null
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA22 /dev/null
# ******* CONDTITUTION DU FICHIER DES FAMILLES DE PRODUITS                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FFK200 ${DATA}/PTEM/FK210FAM.BFK200AP
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******* SOCIET� D'ETUDE                                                      
       m_FileAssign -d SHR FETAT ${DATA}/CORTEX4.P.MTXTFIX1/FK210FAM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFK200 
       JUMP_LABEL=FK210FAN
       ;;
(FK210FAN)
       m_CondExec 04,GE,FK210FAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
#  TRI PAR CODICS DU FICHIER FAMILLE DE PRODUITS                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FK210FAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FK210FAQ
       ;;
(FK210FAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER DES FAMILLES DE PRODUITS                                     
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/FK210FAM.BFK200AP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FK210FAQ.BFK200BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FK210FAR
       ;;
(FK210FAR)
       m_CondExec 00,EQ,FK210FAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BFK220                                                                
#  CONSTITUTION DU FICHIER _A.ENVOYER _A GFK                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FK210FAT PGM=BFK220     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=FK210FAT
       ;;
(FK210FAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
# ******* FICHIER DES FAMILLES DE PRODUITS TRI� (NCODIC)                       
       m_FileAssign -d SHR -g ${G_A8} FFK200 ${DATA}/PTEM/FK210FAQ.BFK200BP
# ******* FICHIER DE CUMUL DES UNLOAD TRI�S                                    
       m_FileAssign -d SHR -g ${G_A9} RTGS40 ${DATA}/PTEM/FK210FAJ.BFK210BP
# ******* FICHIER POUR GFK (CODIC/NSOCVTE/NLIEUVTE)                            
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ FFK220 ${DATA}/PXX0.F07.BFK220AP
       m_ProgramExec BFK220 
#                                                                              
# ********************************************************************         
#   COMPRESS DU FICHIER BFK220AP SUR GFK000F                                   
#   REPRISE : OUI  MIS EN COMMENTAIRE LE 20042015                              
# ********************************************************************         
# ABJ      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TEXT                                                                       
#  -ARCHIVE(":GFK000F"")                                                       
#  -ARCHUNIT(SEM350)                                                           
#  -ZIPPED_DSN(":BFK220AP"",GFK000F.TXT)                                       
#  ":BFK220AP""                                                                
#          DATAEND                                                             
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FK210FAX PGM=JVMLDM76   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=FK210FAX
       ;;
(FK210FAX)
       m_CondExec ${EXABJ},NE,YES 
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ FICZIP ${DATA}/PXX0.F07.GFK000F
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FK210FAX.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  ENVOI DU FICHIER VERS GFK VIA GATEWAY                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
# ABO      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=GFK000F,                                                            
#      FNAME=":GFK000F""                                                       
#          DATAEND                                                             
# ********************************************************************         
#  PGM : SORT                                                                  
# ********************************************************************         
# ABT      SORT                                                                
# ******* FICHIER DES FAMILLES DE PRODUITS                                     
# SORTIN   FILE  NAME=BFK220AP,MODE=I                                          
# SORTOUT  FILE  NAME=BFK220BP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(1,12,A,110,12,A),FORMAT=CH                                     
# INCLUDE COND=((8,5,CH,EQ,C'MONIT'),OR,(8,5,CH,EQ,C'MONIF'),OR,               
#               (8,5,CH,EQ,C'UNCEN'),OR,(8,5,CH,EQ,C'IMPMF'))                  
#         DATAEND                                                              
# ********************************************************************         
#   COMPRESS DU FICHIER BFK220BP SUR GFK010F                                   
#   REPRISE : OUI                                                              
# ********************************************************************         
# ABY      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TEXT                                                                       
#  -ARCHIVE(":CONTEXT"")                                                       
#  -ARCHUNIT(SEM350)                                                           
#  -ZIPPED_DSN(":BFK220BP"",CONTEXT.TXT)                                       
#  ":BFK220BP""                                                                
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI DU FICHIER VERS GFK VIA GATEWAY                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
# *ACD      STEP  PGM=IEFBR14,PATTERN=CFT                                      
# *CFTIN    DATA  *                                                            
# *SEND PART=XFBPRO,                                                           
# *     IDF=CONTEXT,                                                           
# *     FNAME=":CONTEXT""                                                      
# *         DATAEND                                                            
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTFK210F                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FK210FBA PGM=FTP        ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=FK210FBA
       ;;
(FK210FBA)
       m_CondExec ${EXABO},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/FK210FBA.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP FK210FBD PGM=EZACFSM1   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=FK210FBD
       ;;
(FK210FBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FK210FBD.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FK210FZA
       ;;
(FK210FZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FK210FZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
