#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GR050L.ksh                       --- VERSION DU 17/10/2016 18:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGR050 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/12/16 AT 11.51.31 BY BURTECN                      
#    STANDARDS: P  JOBSET: GR050L                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#                                                                              
# ********************************************************************         
#  B G R 0 5 0                                                                 
# ********************************************************************         
#  EXTRACTION PERMETTANT L'EDITION DES BONS DE RECEPTION VALORISEES            
#  DEPUIS LE DEBUT DU MOIS                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GR050LA
       ;;
(GR050LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GR050LAA
       ;;
(GR050LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    POUR AVOIR LE PRMP A JOUR(RTGG50) *                                       
#  POUR AVOIR TOUTES LES RECEPTIONS                                            
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME 2 JJMMSSAA                         
#        DATE DE FIN DE SELECTION                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/DAGR050L
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES PRMP                                                              
#    RSGG50   : NAME=RSGG50L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#  TABLE DES NUMEROS DE RECEPT                                                 
#    RSGG70   : NAME=RSGG70L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70 /dev/null
#  TABLE DES RECEPTIONS COMPTABLES (GENERALITES)                               
#    RSGR30   : NAME=RSGR30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGR30 /dev/null
#  TABLE DES RECEPTIONS COMPTABLES (DETAIL)                                    
#    RSGR35   : NAME=RSGF35L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGR35 /dev/null
#  TABLE DES CDES FOURNISSEURS                                                 
#    RSGF10   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10 /dev/null
#  TABLE DES LIGNES DE CDES FOURNISSEURS                                       
#    RSGF20   : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20 /dev/null
#  TABLE DES FAMILLES                                                          
#    RSGA14   : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#  TABLE DES ARTICLES                                                          
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#  TABLE DES ANOMALIES                                                         
#    RSAN00   : NAME=RSAN00L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
#  FICHIER D'EXTRACTION : NEW LRECL 125                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -g +1 FGR050 ${DATA}/PTEM/GR050LAA.FGR050AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR050 
       JUMP_LABEL=GR050LAB
       ;;
(GR050LAB)
       m_CondExec 04,GE,GR050LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  S O R T                                                                     
# ********************************************************************         
#   TRI DU FICHIER DE RECEPTIONS VALORISEES POUR PGM BGR052                    
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR050LAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GR050LAD
       ;;
(GR050LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM DU JOUR                                          
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GR050LAA.FGR050AL
#  FICHIER DES RECEPTIONS ADM DU JOUR TRIE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -g +1 SORTOUT ${DATA}/PTEM/GR050LAD.FGR052AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_11 1 CH 11
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_27_24 27 CH 24
 /FIELDS FLD_CH_15_12 15 CH 12
 /KEYS
   FLD_CH_1_11 ASCENDING,
   FLD_CH_27_24 ASCENDING,
   FLD_CH_12_3 ASCENDING,
   FLD_CH_15_12 ASCENDING
 /* Record Type = F  Record Length = 127 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR050LAE
       ;;
(GR050LAE)
       m_CondExec 00,EQ,GR050LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  S O R T                                                                     
# ********************************************************************         
#   TRI DU FICHIER DE RECEPTIONS VALORISEES POUR PGM BGR051                    
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR050LAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GR050LAG
       ;;
(GR050LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM DU JOUR                                          
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GR050LAA.FGR050AL
#  FICHIER DES RECEPTIONS ADM DU JOUR TRIE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -g +1 SORTOUT ${DATA}/PTEM/GR050LAG.FGR051AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_11 1 CH 11
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_15_36 15 CH 36
 /KEYS
   FLD_CH_1_11 ASCENDING,
   FLD_CH_12_3 ASCENDING,
   FLD_CH_15_36 ASCENDING
 /* Record Type = F  Record Length = 127 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR050LAH
       ;;
(GR050LAH)
       m_CondExec 00,EQ,GR050LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 5 1                                                                 
# ********************************************************************         
#  EDITION DES BONS DE RECEPTION VALORISEES                                    
#  ETAT IGR051                                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR050LAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GR050LAJ
       ;;
(GR050LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME 2 JJMMSSAA                         
#        DATE DE FIN DE SELECTION                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/DAGR050L
#                                                                              
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  SOCIETE                                                                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/GR050LAJ
#  TABLE DES CHEFS PRODUIT                                                     
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
#  TABLE DES ASSOCIATIONS ENTITES DE CDE INTERLOC COMMERCIALE                  
#    RSGA08   : NAME=RSGA08L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA08 /dev/null
#  TABLE DES ENTITES DE CDES                                                   
#    RSGA06   : NAME=RSGA06L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06 /dev/null
#  TABLE DES ANOMALIES                                                         
#    RSAN00   : NAME=RSAN00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
#  FICHIER D'EXTRACTION TRIE                                                   
       m_FileAssign -d SHR -g ${G_A3} FGR050 ${DATA}/PTEM/GR050LAG.FGR051AL
#  ETAT DES RECEPTIONS VALORISEES                                              
       m_OutputAssign -c 9 -w IGR051 IGR051
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR051 
       JUMP_LABEL=GR050LAK
       ;;
(GR050LAK)
       m_CondExec 04,GE,GR050LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 5 2                                                                 
# ********************************************************************         
#  EDITION DES BONS DE RECEPTION VALORISEES                                    
#  ETAT IGR052                                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR050LAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GR050LAM
       ;;
(GR050LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME 2 JJMMSSAA                         
#        DATE DE FIN DE SELECTION                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/DAGR050L
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/GR050LAM
#  TABLE DES CHEFS PRODUIT                                                     
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
#  TABLE DES ASSOCIATIONS ENTITES DE CDE INTERLOC COMMERCIALE                  
#    RSGA08   : NAME=RSGA08L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA08 /dev/null
#  TABLE DES ENTITES DE CDES                                                   
#    RSGA06   : NAME=RSGA06L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06 /dev/null
#  TABLE DES ANOMALIES                                                         
#    RSAN00   : NAME=RSAN00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
#  FICHIER D'EXTRACTION TRIE                                                   
       m_FileAssign -d SHR -g ${G_A4} FGR050 ${DATA}/PTEM/GR050LAD.FGR052AL
#  ETAT DES RECEPTIONS VALORISEES                                              
       m_OutputAssign -c 9 -w IGR052 IGR052
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR052 
       JUMP_LABEL=GR050LAN
       ;;
(GR050LAN)
       m_CondExec 04,GE,GR050LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GR050LZA
       ;;
(GR050LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GR050LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
