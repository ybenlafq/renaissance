#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GK200M.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGK200 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 05/08/02 AT 13.55.33 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GK200M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BGK200 : GENERATION DU FICHIER PRODUITS A TRANSMETTRE                       
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GK200MA
       ;;
(GK200MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GK200MAA
       ;;
(GK200MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
       m_FileAssign -d SHR FETAT ${DATA}/CORTEX4.P.MTXTFIX1/KRISP01
#                                                                              
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA09   : NAME=RSGA09M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09 /dev/null
#    RSGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA11   : NAME=RSGA11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11 /dev/null
#    RSGA12   : NAME=RSGA12M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA22   : NAME=RSGA22M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22 /dev/null
#    RSGA29   : NAME=RSGA29M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA29 /dev/null
#    RSGA64   : NAME=RSGA64M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA64 /dev/null
#    RSGA83   : NAME=RSGA83M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA83 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 FGK200 ${DATA}/PTEM/GK200MAA.BGK200AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGK200 
       JUMP_LABEL=GK200MAB
       ;;
(GK200MAB)
       m_CondExec 04,GE,GK200MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PAR CODIC                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK200MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GK200MAD
       ;;
(GK200MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GK200MAA.BGK200AM
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 SORTOUT ${DATA}/PTEM/GK200MAD.BGK200BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK200MAE
       ;;
(GK200MAE)
       m_CondExec 00,EQ,GK200MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK210 : GENERATION DE SYSIN POUR FASTUNLOAD DES DONNEES PRODUIT            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK200MAG PGM=BGK210     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GK200MAG
       ;;
(GK200MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GK200M1
#                                                                              
       m_FileAssign -d SHR FSYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK200MAG.sysin
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FGK210 ${DATA}/PTEM/GK200MAG.BGK210AM
       m_ProgramExec BGK210 
# ********************************************************************         
#  FASTUNLOAD DES TABLES RTGS10, RTGS30, RTGG31, RTGS40,                       
#                        RTGS42 ET RTGG70                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK200MAJ PGM=PTLDRIVM   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GK200MAJ
       ;;
(GK200MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
#                                                                              
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGS30   : NAME=RSGS30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
#    RSGS31   : NAME=RSGS31M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS31 /dev/null
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSGS42   : NAME=RSGS42M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS42 /dev/null
#    RSGG70   : NAME=RSGG70M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -g +1 SYSREC01 ${DATA}/PTEM/GK200MAJ.GS40UNAM
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -g +1 SYSREC02 ${DATA}/PTEM/GK200MAJ.GS42UNAM
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 SYSREC03 ${DATA}/PTEM/GK200MAJ.GG70UNAM
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SYSREC04 ${DATA}/PTEM/GK200MAJ.GS10UNAM
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SYSREC05 ${DATA}/PTEM/GK200MAJ.GS30UNAM
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SYSREC06 ${DATA}/PTEM/GK200MAJ.GS31UNAM
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} SYSIN ${DATA}/PTEM/GK200MAG.BGK210AM
       
       #Untranslated utility: PTLDRIVM##Untranslate PTLDRIVM SYSIN

# ********************************************************************         
#  FUSION DES DONNEES DES TABLES RTGS40 ET RTGS42                              
# ********************************************************************         
#  TRI PAR CODIC                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK200MAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GK200MAM
       ;;
(GK200MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GK200MAJ.GS40UNAM
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/GK200MAJ.GS42UNAM
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -g +1 SORTOUT ${DATA}/PTEM/GK200MAM.BGK220AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK200MAN
       ;;
(GK200MAN)
       m_CondExec 00,EQ,GK200MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PAR CODIC/DATE DES DONNEES DE LA TABLE RTGG70                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK200MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GK200MAQ
       ;;
(GK200MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GK200MAJ.GG70UNAM
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 SORTOUT ${DATA}/PTEM/GK200MAQ.BGK220BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_8_8 8 CH 8
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_8 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK200MAR
       ;;
(GK200MAR)
       m_CondExec 00,EQ,GK200MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PAR CODIC DES DONNEES DE LA TABLE RTGS10                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK200MAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GK200MAT
       ;;
(GK200MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/GK200MAJ.GS10UNAM
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SORTOUT ${DATA}/PTEM/GK200MAT.BGK220CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK200MAU
       ;;
(GK200MAU)
       m_CondExec 00,EQ,GK200MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FUSION DES DONNEES DES TABLES RTGS30 ET RTGS31                              
# ********************************************************************         
#  TRI PAR CODIC                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK200MAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GK200MAX
       ;;
(GK200MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/GK200MAJ.GS30UNAM
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SORTOUT ${DATA}/PTEM/GK200MAX.BGK220DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK200MAY
       ;;
(GK200MAY)
       m_CondExec 00,EQ,GK200MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK220 : MISE EN FORME DES DONNEES PRODUITS                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK200MBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GK200MBA
       ;;
(GK200MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
#    RSGA59   : NAME=RSGA59M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
#    RSGF10   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10 /dev/null
#    RSGF30   : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30 /dev/null
#    RSGG50   : NAME=RSGG50M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#                                                                              
       m_FileAssign -d SHR -g ${G_A8} FGK200 ${DATA}/PTEM/GK200MAD.BGK200BM
       m_FileAssign -d SHR -g ${G_A9} RTGS40 ${DATA}/PTEM/GK200MAM.BGK220AM
       m_FileAssign -d SHR -g ${G_A10} RTGG70 ${DATA}/PTEM/GK200MAQ.BGK220BM
       m_FileAssign -d SHR -g ${G_A11} RTGS10 ${DATA}/PTEM/GK200MAT.BGK220CM
       m_FileAssign -d SHR -g ${G_A12} RTGS30 ${DATA}/PTEM/GK200MAX.BGK220DM
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 1000 -g +1 FGK220 ${DATA}/PXX0/F89.BGK220EM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGK220 
       JUMP_LABEL=GK200MBB
       ;;
(GK200MBB)
       m_CondExec 04,GE,GK200MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GK200MZA
       ;;
(GK200MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK200MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
