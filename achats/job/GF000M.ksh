#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GF000M.ksh                       --- VERSION DU 09/10/2016 05:33
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGF000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/12/17 AT 14.13.21 BY BURTECN                      
#    STANDARDS: P  JOBSET: GF000M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  B G F 0 0 0                                                                 
# ********************************************************************         
#   LISTE DES COMMANDES NON-SOLDEES TRIEES PAR ENTITE DE COMMANDE              
#   INTERLOCUTEUR N DE COMMANDE FAMILLE MARQUE ARTICLE                         
#   TRAITEMENT DU DIMANCHE                                                     
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GF000MA
       ;;
(GF000MA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       RUN=${RUN}
       JUMP_LABEL=GF000MAA
       ;;
(GF000MAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  LISTE DES COMMANDES NON-SOLDEES                                      
       m_OutputAssign -c 9 -w BGF000 SYSCONTL
# ******  COMMANDES FOURNISSEUR                                                
#    RSGF10M  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10M /dev/null
# ******  LIGNES COMMANDES FOURNISSEUR                                         
#    RSGF20M  : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20M /dev/null
# ******  LIGNES DE COMMANDES ECHEANCEES                                       
#    RSGF30M  : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30M /dev/null
# ******  LIGNES DE COMMANDES DEMANDEURS                                       
#    RSGF15M  : NAME=RSGF15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF15M /dev/null
# ******  ARTICLES                                                             
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ******  ENTITES DE COMMANDES                                                 
#    RSGA06M  : NAME=RSGA06M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06M /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ******* SPECIFIQUE ARTICLE GERES PAR ENTREPOT EXTERIEUR                      
#    RSFL50M  : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50M /dev/null
#                                                                              
#                                                                              
# ******  PARAMETRE SOCIETE : 989                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF000 
       JUMP_LABEL=GF000MAB
       ;;
(GF000MAB)
       m_CondExec 04,GE,GF000MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G F 0 1 5                                                                 
#  B G F 0 1 5     MISE SYSOUT=*  CAR ETAT NON UTILISE                         
# ********************************************************************         
#  VENTILATION DES NOMBRES D U O PRIS OU A PRENDRE                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF000MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GF000MAD
       ;;
(GF000MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  LISTE DE LA VENTILATION DES NOMBRES D U O                            
# IMPR     REPORT SYSOUT=*                                                     
       m_OutputAssign -c 9 -w BGF015 IMPR
# ******  COMMANDES FOURNISSEUR                                                
#    RSGF10M  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10M /dev/null
# ******  LIGNES DE COMMANDES ECHEANCEES                                       
#    RSGF30M  : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30M /dev/null
# ******  PLANNING DE LIVRAISON COMMANDES FOURNISSEURS                         
#    RSGF35M  : NAME=RSGF35M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGF35M /dev/null
# ******  ARTICLES                                                             
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ******  CHEFS PRODUITS                                                       
#    RSGA02M  : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02M /dev/null
# ******  LIEUX                                                                
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ******  RECEPTIONS                                                           
#    RSGR00M  : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR00M /dev/null
# ******  LIGNES DE RECEPTION ECHEANCEES                                       
#    RSGR10M  : NAME=RSGR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR10M /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE : 989                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF015 
       JUMP_LABEL=GF000MAE
       ;;
(GF000MAE)
       m_CondExec 04,GE,GF000MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G F 0 1 0                                                                 
#  B G F 0 1 0     MISE SYSOUT=*  CAR ETAT NON UTILISE                         
# ********************************************************************         
#  CREATION DE L ETAT DESCRIPTIF DE COMMANDE                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF000MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GF000MAG
       ;;
(GF000MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  COMMANDES FOURNISSEUR                                                
#    RSGF10M  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10M /dev/null
# ******  LIGNES COMMANDES FOURNISSEUR DEMANDEURS                              
#    RSGF15M  : NAME=RSGF15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF15M /dev/null
# ******  LIGNES DE COMMANDES ECHEANCEES                                       
#    RSGF30M  : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30M /dev/null
# ******  MVTS DE CREATION ET DE MAJ DE COMMANDES                              
#    RSGF45M  : NAME=RSGF45,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF45M /dev/null
# ******  ARTICLES                                                             
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE : 989                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  LISTE DE L ETAT DESCRIPTIF DES COMMANDES                             
# IMPR     REPORT SYSOUT=(9,BGF010),SPIN=UNALLOC                               
       m_OutputAssign -c "*" IMPR
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF010 
       JUMP_LABEL=GF000MAH
       ;;
(GF000MAH)
       m_CondExec 04,GE,GF000MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G F 0 2 0    MISE COMMENT CAR ETAT PLUS UTILISE A DAL  CGA                
# ********************************************************************         
#  CREATION DE L ETAT VALORISATION DE COMMANDE                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAP      STEP PGM=IKJEFT01,LANG=UTIL                                         
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *******  COMMANDES FOURNISSEUR                                               
# RSGF10M  FILE  DYNAM=YES,NAME=RSGF10,MODE=I                                  
# *******  LIGNES DE COMMANDES FOURNISSEUR                                     
# RSGF20M  FILE  DYNAM=YES,NAME=RSGF20,MODE=I                                  
# *******  RISTOURNES SUR LES LIGNES DE COMMANDES FOURNISSEUR                  
# RSGF25M  FILE  DYNAM=YES,NAME=RSGF25,MODE=I                                  
# *******  MVTS DE CREATION ET DE MAJ DE COMMANDES                             
# RSGF45M  FILE  DYNAM=YES,NAME=RSGF45,MODE=I                                  
# *******  VALORISATION DES ESCLAVES LIBRE DES COMMANDES FOURNI                
# RSGF50M  FILE  DYNAM=YES,NAME=RSGF50M,MODE=I                                 
# *******  ARTICLES                                                            
# RSGA00M  FILE  DYNAM=YES,NAME=RSGA00M,MODE=I                                 
# *******  TABLE LIEUX                                                         
# RSGA10M  FILE  DYNAM=YES,NAME=RSGA10M,MODE=I                                 
# *                                                                            
# *******  PARAMETRE SOCIETE : 989                                             
# FNSOC    DATA CLASS=FIX1,MBR=SOCDAL                                          
# ******  DATE JJMMSSAA                                                        
# FDATE    DATA CLASS=VAR,PARMS=FDATE,MBR=FDATE                                
# *******  LISTE DE L ETAT VALORISATION DE COMMANDES                           
# IMPR     REPORT SYSOUT=(9,BGF020),SPIN=UNALLOC                               
# *                                                                            
# SYSTSIN  DATA *,CLASS=FIX2                                                   
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BGF020) PLAN(BGF020M)                                           
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
