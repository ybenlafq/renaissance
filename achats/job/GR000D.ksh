#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GR000D.ksh                       --- VERSION DU 17/10/2016 18:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGR000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/12/17 AT 13.58.16 BY BURTECN                      
#    STANDARDS: P  JOBSET: GR000D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#                                                                              
# ********************************************************************         
#  B G R 0 0 0                                                                 
# ********************************************************************         
#   EXTRACTION DES RECEPTIONS ADMINISTRATIVES DU JOUR VALIDEES                 
#   EXTRACTION DES RECEPTIONS ADMINISTRATIVES DU JOUR PAR FOURNISSEURS         
#   SEULES LES QUANTITES REELLEMENT COMMANDEES SONT TRAITEES                   
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GR000DA
       ;;
(GR000DA)
#
#GR000DAX
#GR000DAX Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GR000DAX
#
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RABO=${RABO:-GR000DBA}
       RUN=${RUN}
       JUMP_LABEL=GR000DAA
       ;;
(GR000DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    POUR AVOIR LE PRMP A JOUR(RTGG50) *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  CARTE SOCIETE                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#  FICHIER DES RECEPTIONS ADM DU JOUR                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 FGR001 ${DATA}/PTEM/GR000DAA.GR0001AD
#  FICHIER DES RECEPTIONS ADM DU JOUR PAR FOURNISSEURS                         
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 FGR000 ${DATA}/PTEM/GR000DAA.GR0000AD
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME JJMMSSAA                           
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES ARTICLES                                                          
#    TABLE    : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES ENTITES DE COMMANDES                                              
#    TABLEA   : NAME=RSGA06D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEA /dev/null
#  TABLE DES FAMILLES                                                          
#    TABLEB   : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEB /dev/null
#  TABLE DES COMMANDES FOURNISSEURS                                            
#    TABLEC   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEC /dev/null
#  TABLE DES RECEPTIONS                                                        
#    TABLED   : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLED /dev/null
#  TABLE DES MVTS DE RECEPTIONS DU JOUR                                        
#    TABLEE   : NAME=RSGR20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEE /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR000 
       JUMP_LABEL=GR000DAB
       ;;
(GR000DAB)
       m_CondExec 04,GE,GR000DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  S O R T                                                                     
#   TRI DU FICHIER DE RECEPTIONS DU JOUR VALIDEES                              
#   1,3 CODE SOCIETE;4,3 CODE DEPOT; 7,7 NDE RECEPTION ADM;                    
#   56,7 CODIC         ; 29,7 NUM DE COMMANDE                                  
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GR000DAD
       ;;
(GR000DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM DU JOUR                                          
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GR000DAA.GR0001AD
#  FICHIER DES RECEPTIONS ADM DU JOUR TRIE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 SORTOUT ${DATA}/PTEM/GR000DAD.GR0001BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_29_7 29 CH 7
 /FIELDS FLD_CH_56_7 56 CH 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_56_7 ASCENDING,
   FLD_CH_29_7 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR000DAE
       ;;
(GR000DAE)
       m_CondExec 00,EQ,GR000DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 0 5                                                                 
# ********************************************************************         
#  EDITION DES RECEPTIONS VALIDEES DU JOUR                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GR000DAG
       ;;
(GR000DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM DU JOUR VALIDEES                                 
       m_FileAssign -d SHR -g ${G_A2} FGR001 ${DATA}/PTEM/GR000DAD.GR0001BD
#  LISTE DES RECPTIONS VALIDEES DU JOUR IGR005                                 
       m_OutputAssign -c 9 -w BGR005 IGR005
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME                                    
#                 JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#  TABLE DES LIGNES DE RECEPTIONS ECHEANCEES                                   
#    RTGR10   : NAME=RSGR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGR10 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR005 
       JUMP_LABEL=GR000DAH
       ;;
(GR000DAH)
       m_CondExec 04,GE,GR000DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 0                                                                 
# ********************************************************************         
#  EXTRACTION DES RECEPTIONS MODIFIEES DU JOUR                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000DAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GR000DAJ
       ;;
(GR000DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FNSOC                                                                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#  FICHIER DES MODIFICATIONS DES RECEPTIONS DU JOUR                            
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -g +1 FGR011 ${DATA}/PTEM/GR000DAJ.GR0011AD
#  FICHIER DES MODIFICATION DES REEPTIONS DU JOUR PAR FOURNISSEURS             
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 FGR000 ${DATA}/PTEM/GR000DAJ.GR0000BD
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME JJMMSSAA                           
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES ARTICLES                                                          
#    TABLE    : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES ENTITES DE COMMANDE                                               
#    TABLEA   : NAME=RSGA06D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEA /dev/null
#  TABLE DES COMMANDES FOURNISSEURS                                            
#    TABLEB   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEB /dev/null
#  TABLE DES RECEPTIONS                                                        
#    TABLEC   : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEC /dev/null
#  TABLE DES RECEPTIONS PAR ARTICLES                                           
#    TABLED   : NAME=RSGR05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLED /dev/null
#  TABLE DES LIGNES DE RECEPTIONS ECHEANCEES                                   
#    TABLEE   : NAME=RSGR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEE /dev/null
#  TABLE DES MVTS DE RECEPTIONS DU JOUR                                        
#    TABLEF   : NAME=RSGR20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEF /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR010 
       JUMP_LABEL=GR000DAK
       ;;
(GR000DAK)
       m_CondExec 04,GE,GR000DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  S O R T                                                                     
#   TRI DU FICHIER DES RECEPTIONS MODIFIEES DU JOUR                            
#   1,3 CODE SOCIETE;4,3 CODE DEPOT;7,7 N DE RECEPTION ADM;                    
#   44,5 CODE FAMILLE ; 49,5 CODE MARQUE; 74,8 N DE COMMANDE                   
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000DAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GR000DAM
       ;;
(GR000DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM MODIFIEES DU JOUR                                
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GR000DAJ.GR0011AD
#  FICHIER DES RECEPTIONS ADM MODIFIEES DU JOUR TRIE                           
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -g +1 SORTOUT ${DATA}/PTEM/GR000DAM.GR0011BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_44_5 44 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_74_8 74 CH 8
 /FIELDS FLD_CH_49_5 49 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_44_5 ASCENDING,
   FLD_CH_49_5 ASCENDING,
   FLD_CH_74_8 ASCENDING
 /* Record Type = F  Record Length = 135 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR000DAN
       ;;
(GR000DAN)
       m_CondExec 00,EQ,GR000DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 5                                                                 
# ********************************************************************         
#  EDITION DES RECEPTIONS MODIFIEES DU JOUR                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000DAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GR000DAQ
       ;;
(GR000DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM MODIFIEES DU JOUR TRIE                           
       m_FileAssign -d SHR -g ${G_A4} FGR011 ${DATA}/PTEM/GR000DAM.GR0011BD
#  LISTE DES RECEPTIONS MODIFIEES DU JOUR                                      
       m_OutputAssign -c 9 -w BGR015 IGR015
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME                                    
#                 JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES LIGNES DE RECEPTIONS ECHEANCEES                                   
#    RTPT03   : NAME=RSPT03D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTPT03 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR015 
       JUMP_LABEL=GR000DAR
       ;;
(GR000DAR)
       m_CondExec 04,GE,GR000DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  S O R T                                                                     
#   TRI DU FICHIER DES RECEPTIONS DU JOUR PAR FOURNISSEURS                     
#   TRI DU FICHIER DES RECEPTIONS DU JOUR MAJ PAR FOURNISSEURS                 
#   POUR NE FAIRE Q UN SEUL FICHIER                                            
#   122,3 TYPE MVT; 86,1 TYPE CODIC; 1,7 N DE RECEPTION ADM;                   
#   31,5 N DE FOURNISSEURS; 125,1 CODE MVT                                     
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000DAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GR000DAT
       ;;
(GR000DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM DU JOUR PAR FOURNISSEURS                         
#  FICHIER DES RECEPTIONS ADM MODIFIEES DU JOUR PAR FOURNISSEURS               
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GR000DAA.GR0000AD
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/GR000DAJ.GR0000BD
#  FICHIER DES RECEPTIONS ADM DU JOUR TRIE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 SORTOUT ${DATA}/PTEM/GR000DAT.GR0000CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_125_1 125 CH 1
 /FIELDS FLD_CH_137_10 137 CH 10
 /FIELDS FLD_CH_122_3 122 CH 3
 /FIELDS FLD_CH_31_5 31 CH 5
 /FIELDS FLD_CH_86_1 86 CH 1
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_122_3 ASCENDING,
   FLD_CH_86_1 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_31_5 ASCENDING,
   FLD_CH_137_10 ASCENDING,
   FLD_CH_125_1 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR000DAU
       ;;
(GR000DAU)
       m_CondExec 00,EQ,GR000DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QUIESCE DES TABLES RTGR30  ET RTGR35   AVANT MAJ                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000DAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GR000DBA
       ;;
(GR000DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS DU JOUR CREE OU MAJ                                  
       m_FileAssign -d SHR -g ${G_A7} FGR000 ${DATA}/PTEM/GR000DAT.GR0000CD
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME JJMMSSAA                           
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES RECPTIONS COMPTABLES                                              
#    TABLE    : NAME=RSGR30D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR TABLE /dev/null
#    TABLEA   : NAME=RSGR35D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR TABLEA /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR020 
       JUMP_LABEL=GR000DBB
       ;;
(GR000DBB)
       m_CondExec 04,GE,GR000DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI CODE RETOUR DIFFERENT DE 0 ABEND                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000DBD PGM=ZUTABEND   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GR000DBD
       ;;
(GR000DBD)
       m_CondExec ${EXABT},NE,YES 0,EQ,$[RABO] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# ********************************************************************         
#  B G R 0 2 5                                                                 
# ********************************************************************         
#  LISTE DES RECEPTIONS DU JOUR                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000DBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GR000DBG
       ;;
(GR000DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME JJMMSSAA                           
       m_FileAssign -i FDATE
$FDATE
_end
#  LISTE DES RECEPTIONS DU JOUR                                                
       m_OutputAssign -c 9 -w BGR025 IGR025
#  TABLE DES RECEPTIONS COMPTABLES                                             
#    TABLE    : NAME=RSGR30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES LIGNES DE RECEPTIONS COMPTABLES                                   
#    TABLEA   : NAME=RSGR35D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEA /dev/null
       m_FileAssign -d SHR FDARDAC ${DATA}/CORTEX4.P.MTXTFIX1/GR000DBG
# ******  CARTE SOCIETE                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR025 
       JUMP_LABEL=GR000DBH
       ;;
(GR000DBH)
       m_CondExec 04,GE,GR000DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 3 0                                                                 
# ********************************************************************         
#  LISTE DES BONS DE RECEPTIONS VALORISES DU JOUR                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000DBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GR000DBJ
       ;;
(GR000DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME JJMMSSAA                           
       m_FileAssign -i FDATE
$FDATE
_end
#  LISTE DES BONS DE RECEPTIONS VALORISES DU JOUR                              
       m_OutputAssign -c 9 -w BGR030 IGR030
#  LISTE DES BONS DE RECEPTIONS VALORISES DU JOUR/PAR FOURNISSEUR              
       m_FileAssign -d NEW,CATLG,DELETE -r 153 -g +1 FGR030 ${DATA}/PTEM/GR000DBJ.FGR030AD
#  TABLE DES ARTICLES                                                          
#    TABLE    : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES LIENS ARTICLES                                                    
#    TABLEA   : NAME=RSGA58D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEA /dev/null
#  TABLE DES LIGNES DE COMMANDES FOURNISSEURS                                  
#    TABLEB   : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEB /dev/null
#  TABLE DES RISTOURNES SUR LIGNES DE COMMANDES FOURNISSEURS                   
#    TABLEC   : NAME=RSGF25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEC /dev/null
#  TABLE DES PRIX MOYENS PONDERES (PRMP DU JOUR)                               
#    TABLED   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLED /dev/null
#  TABLE DES RECEPTIONS COMPTABLES                                             
#    TABLEE   : NAME=RSGR30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEE /dev/null
#  TABLE DES LIGNES DE RECEPTIONS COMPTABLE                                    
#    TABLEF   : NAME=RSGR35D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEF /dev/null
       m_FileAssign -d SHR FDARDAC ${DATA}/CORTEX4.P.MTXTFIX1/GR000DBJ
# ******  CARTE SOCIETE                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR030 
       JUMP_LABEL=GR000DBK
       ;;
(GR000DBK)
       m_CondExec 04,GE,GR000DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FGR030AD POUR CREATION D UNE EDITION                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000DBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GR000DBM
       ;;
(GR000DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GR000DBJ.FGR030AD
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 SORTOUT ${DATA}/PTEM/GR000DBM.FGR030BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_20 1 CH 20
 /FIELDS FLD_CH_21_133 21 CH 133
 /KEYS
   FLD_BI_1_20 ASCENDING
 /* Record Type = F  Record Length = 133 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_21_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GR000DBN
       ;;
(GR000DBN)
       m_CondExec 00,EQ,GR000DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEBGENER POUR CREATION D UNE EDITION IGR030B                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000DBQ PGM=IEBGENER   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GR000DBQ
       ;;
(GR000DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SYSUT1 ${DATA}/PTEM/GR000DBM.FGR030BD
       m_OutputAssign -c 9 -w IGR030B SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GR000DBR
       ;;
(GR000DBR)
       m_CondExec 00,EQ,GR000DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 3 5                                                                 
# ********************************************************************         
#  LISTE DES RECEPTIONS MODIFIEES VALORISEES DU JOUR                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000DBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GR000DBT
       ;;
(GR000DBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME                                    
#                 JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
#  LISTE DES RECEPTIONS MODIFIEES VALORISEES DU JOUR                           
       m_OutputAssign -c 9 -w BGR035 IGR035
#  TABLE DES ARTICLES                                                          
#    TABLE    : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES LIENS ARTICLES                                                    
#    TABLEA   : NAME=RSGA58D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEA /dev/null
#  TABLE DES LIGNES DE COMMANDES FOURNISSEURS                                  
#    TABLEB   : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEB /dev/null
#  TABLE DES RISTOURNES SUR LIGNES DE COMMANDES FOURNISSEURS                   
#    TABLEC   : NAME=RSGF25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEC /dev/null
#  TABLE DES PRIX MOYENS PONDERES (PRMP DU JOUR)                               
#    TABLED   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLED /dev/null
#  TABLE DES RECEPTIONS COMPTABLES                                             
#    TABLEE   : NAME=RSGR30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEE /dev/null
#  TABLE DES LIGNES DE RECEPTIONS COMPTABLE                                    
#    TABLEF   : NAME=RSGR35D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEF /dev/null
# ******  CARTE SOCIETE                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR035 
       JUMP_LABEL=GR000DBU
       ;;
(GR000DBU)
       m_CondExec 04,GE,GR000DBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GR000DZA
       ;;
(GR000DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GR000DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
