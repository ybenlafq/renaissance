#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GR074P.ksh                       --- VERSION DU 17/10/2016 18:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGR074 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 05/03/09 AT 17.22.39 BY BURTEC7                      
#    STANDARDS: P  JOBSET: GR074P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  *****     STEP PERMETTANT UNE EXTRACTION DU FICHIER SYSIN FGR074            
#  **    A L AIDE DU PARAMETRE FPARAM (SOIT P OU F OU SPACE )                  
#  **    A L AIDE DU PARAMETRE FNSOC  (SOIT 907   OU SPACE )                   
#  **    A L AIDE DU PARAMETRE FDATDEB (DEBUT DE L EXTRACTION DEMAND�)         
#  *******                                                                     
#  ******* TRAITEMENT JOURNALIER = IGR074                                      
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GR074PA
       ;;
(GR074PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GR074PAA
       ;;
(GR074PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUA
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ****** CARTE FDATE                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** CARTE PARAMETRE SOCIETE                                               
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/NSOC05
#                                                                              
# ****** CARTE PARAMETRE DE DEBUT DE L EXTRACTION                              
# ****** QMF023 (DATE DE DEBUT DE MOIS )                                       
       m_FileAssign -i FDATDEB
$VDATEJ__01MMAA
_end
#                                                                              
# ****** CARTE FPARAM .QMF0233 (VALEUR A SPACE )                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/FPARAM05
#                                                                              
# ****** CARTE FSYSIN                                                          
       m_FileAssign -d SHR FSYSIN ${SYSIN}/CORTEX4.P.MTXTFIX2/EXTRAC05.sysin
#                                                                              
# ****** FICHIER DE RECUP DE SYSIN (80 DE LONG)                                
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FGR070 ${DATA}/PTEM/GR074PAA.FGR074AP
       m_ProgramExec BGR070 
#                                                                              
# ********************************************************************         
#  AVEC LA SYSIN PRECEDEMENT OBTENU .UNLOAD DE TABLE                           
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR074PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GR074PAD
       ;;
(GR074PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
#                                                                              
# RECEPTION DE L UNLOAD DE TABLE                                               
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC01 ${DATA}/PTEM/GR074PAD.FGR074BP
#                                                                              
# SYSIN PRECEDEMMENT OBTENU                                                    
       m_FileAssign -d SHR -g ${G_A1} SYSIN ${DATA}/PTEM/GR074PAA.FGR074AP
       
       #Untranslated utility: PTLDRIVM##Untranslate PTLDRIVM SYSIN

# ********************************************************************         
#  TRI DU FIC FGR070U                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR074PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GR074PAG
       ;;
(GR074PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GR074PAD.FGR074BP
       m_FileAssign -d NEW,CATLG,DELETE -r 87 -g +1 SORTOUT ${DATA}/PTEM/GR074PAG.FGR074CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_51_25 51 CH 25
 /FIELDS FLD_CH_24_3 24 CH 3
 /FIELDS FLD_CH_50_1 50 CH 1
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_82_6 82 PD 6
 /FIELDS FLD_CH_27_23 27 CH 23
 /FIELDS FLD_PD_76_6 76 PD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_50_1 ASCENDING,
   FLD_CH_24_3 ASCENDING,
   FLD_CH_27_23 ASCENDING,
   FLD_CH_51_25 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_76_6,
    TOTAL FLD_PD_82_6
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR074PAH
       ;;
(GR074PAH)
       m_CondExec 00,EQ,GR074PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC FGR070U POUR OBTENIR UN FICHIER FNDEPOT POUR L ENTREE AU         
#  PGM BGR071 (ON OBTIENT UN FICHIER DE 03 DE LONG)                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR074PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GR074PAJ
       ;;
(GR074PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GR074PAD.FGR074BP
       m_FileAssign -d NEW,CATLG,DELETE -r 3 -g +1 SORTOUT ${DATA}/PTEM/GR074PAJ.FGR074DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_47_3 47 CH 3
 /KEYS
   FLD_CH_47_3 ASCENDING
 /SUMMARIZE
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_47_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GR074PAK
       ;;
(GR074PAK)
       m_CondExec 00,EQ,GR074PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGR071                                                                      
#  EDITION A PARTIR DES DIFFERENTS PARAMETRES OBTENUS PRECEDEMMENT             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR074PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GR074PAM
       ;;
(GR074PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#  FICHIER EN ENTRE ISSU DU TRI FGR073                                         
       m_FileAssign -d SHR -g ${G_A4} FGR071 ${DATA}/PTEM/GR074PAG.FGR074CP
#                                                                              
#  FICHIER EN ENTRE ISSU DU TRI FGR074  (03 LRECL)                             
       m_FileAssign -d SHR -g ${G_A5} FNDEPOT ${DATA}/PTEM/GR074PAJ.FGR074DP
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GR070P05
#                                                                              
# ******  FICHIER D'EDITION.                                                   
       m_OutputAssign -c 9 -w IGR074 IGR071
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR071 
       JUMP_LABEL=GR074PAN
       ;;
(GR074PAN)
       m_CondExec 04,GE,GR074PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GR074PZA
       ;;
(GR074PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GR074PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
