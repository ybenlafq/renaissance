#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GK001Y.ksh                       --- VERSION DU 08/10/2016 13:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGK001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/02/07 AT 10.13.27 BY BURTEC5                      
#    STANDARDS: P  JOBSET: GK001Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BRM000 : EXTRACTION DES DONNEES ARTICLE-FAMILLE                             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GK001YA
       ;;
(GK001YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GK001YAA
       ;;
(GK001YAA)
       m_CondExec ${EXAAA},NE,YES 
#                                                                              
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* ARTICLE                                                              
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ******* COCIC / VALEUR CODE MARKETING                                        
#    RSGA09Y  : NAME=RSGA09Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09Y /dev/null
# ******* LIEUX                                                                
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******* RELATION ETAT / FAMILLE RAYON                                        
#    RSGA11Y  : NAME=RSGA11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11Y /dev/null
# ******* RELATION FAMILLE / CODE MARKETING                                    
#    RSGA12Y  : NAME=RSGA12Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12Y /dev/null
# ******* TABLE FAMILLES                                                       
#    RSGA14Y  : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14Y /dev/null
# ******* LIBELLES IMBRICATIONS DES CODES MARKETING                            
#    RSGA29Y  : NAME=RSGA29Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA29Y /dev/null
# ******* RELATION ARTICLE MODES DE DELIVRANCES                                
#    RSGA64Y  : NAME=RSGA64Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA64Y /dev/null
# ******* RELATION ARTICLE RAYON CODE MARKETING                                
#    RSGA83Y  : NAME=RSGA83Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA83Y /dev/null
#                                                                              
# ******* SOCIETE = 945                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* PARAMETRE EDITION= 'KESA'                                            
       m_FileAssign -d SHR FETAT ${DATA}/CORTEX4.P.MTXTFIX1/BRM000ET
#                                                                              
# ******* DONNEES MUTATIONS                                                    
       m_FileAssign -d SHR FMU235 /dev/null
# ******* DONNEES DESTINEES AU LOAD RTRM60                                     
       m_FileAssign -d SHR RTRM60 /dev/null
# ******* DONNEES DESTINEES AU LOAD RTRM65                                     
       m_FileAssign -d SHR RTRM65 /dev/null
# ******* FIC ARTICLE ET FAMILLE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FRMCOM ${DATA}/PXX0/GK001YAA.BRM001EY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM000 
       JUMP_LABEL=GK001YAB
       ;;
(GK001YAB)
       m_CondExec 04,GE,GK001YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK020 : CREATION DU FICHIER COMPLET ARTICLES                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK001YAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GK001YAD
       ;;
(GK001YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* ARTICLE                                                              
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ******* MARQUES                                                              
#    RSGA22Y  : NAME=RSGA22Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22Y /dev/null
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FIC ARTICLE ET FAMILLE                                               
       m_FileAssign -d SHR -g ${G_A1} FRMCOM ${DATA}/PXX0/GK001YAA.BRM001EY
# ******* DATE DE DERNIER PASSAGE                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/BGK020Y
#                                                                              
# ******* FIC ARTICLE COMPLET                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 113 -g +1 FGK020 ${DATA}/PXX0/F45.BGK020AY
# ******* FIC ARTICLE-MAJ COMPLET                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 FGK120 ${DATA}/PXX0/F45.BGK120AY
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGK020 
       JUMP_LABEL=GK001YAE
       ;;
(GK001YAE)
       m_CondExec 04,GE,GK001YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GK001YZA
       ;;
(GK001YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK001YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
