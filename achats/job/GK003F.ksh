#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GK003F.ksh                       --- VERSION DU 08/10/2016 14:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFGK003 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/12/06 AT 11.22.22 BY BURTECA                      
#    STANDARDS: P  JOBSET: GK003F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  CUMUL FICHIERS VENTES LIVREES ISSUS DE GK002 FILIALES                       
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GK003FA
       ;;
(GK003FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GK003FAA
       ;;
(GK003FAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F96.BGK030BB
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BGK030BD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BGK030BL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BGK030BM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BGK030BO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BGK030BP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BGK030BY
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GK003FAA.BGK130AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_4_7 4 CH 7
 /KEYS
   FLD_BI_4_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK003FAB
       ;;
(GK003FAB)
       m_CondExec 00,EQ,GK003FAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CUMUL FICHIERS PRODUITS ISSUS DE GK001 FILIALES                             
#  REPRISE: OUI   FAC BGK020AB MIS EN DERNIER                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK003FAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GK003FAD
       ;;
(GK003FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BGK020AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BGK020AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BGK020AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BGK020AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BGK020AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BGK020AY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F96.BGK020AB
       m_FileAssign -d NEW,CATLG,DELETE -r 113 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.BGK130BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_51_7 51 CH 7
 /KEYS
   FLD_BI_51_7 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK003FAE
       ;;
(GK003FAE)
       m_CondExec 00,EQ,GK003FAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK130 : CREATION FIC STAT VENTES FILIALES (CONSO)                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK003FAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GK003FAG
       ;;
(GK003FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  DATE  JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FIC   CUMUL VENTES LIVREES FILIALES                                  
       m_FileAssign -d SHR -g ${G_A1} FGK030 ${DATA}/PTEM/GK003FAA.BGK130AP
# ******  FIC   CUMUL PRODUITS FILIALES                                        
       m_FileAssign -d SHR -g ${G_A2} FGK020 ${DATA}/PXX0/F99.BGK130BP
# ******  SOUS TABLE TVA                                                       
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  ZONE DE PRIX                                                         
#    RSGA59   : NAME=RSGA59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA59 /dev/null
# ******  PRMP                                                                 
#    RSGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG50 /dev/null
#                                                                              
# ******* FIC   DE STAT VENTES LIVREES DESTINE A KESA                          
       m_FileAssign -d NEW,CATLG,DELETE -r 78 -t LSEQ -g +1 FGK130 ${DATA}/PXX0/F99.BGK130CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGK130 
       JUMP_LABEL=GK003FAH
       ;;
(GK003FAH)
       m_CondExec 04,GE,GK003FAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CUMUL FICHIERS PRA ISSUS DE GK002 FILIALES                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK003FAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GK003FAJ
       ;;
(GK003FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F96.BGK045BB
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BGK045BD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BGK045BL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BGK045BM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BGK045BO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BGK045BY
       m_FileAssign -d NEW,CATLG,DELETE -r 13 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GK003FAJ.BGK045XP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_7 1 CH 7
 /KEYS
   FLD_BI_1_7 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK003FAK
       ;;
(GK003FAK)
       m_CondExec 00,EQ,GK003FAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI   FICHIERS ACHATS ISSUS DE GK002 IDF                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK003FAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GK003FAM
       ;;
(GK003FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BGK040AP
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GK003FAM.BGK140AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_3 1 CH 3
 /FIELDS FLD_BI_9_7 9 CH 7
 /FIELDS FLD_BI_4_5 4 CH 5
 /FIELDS FLD_BI_16_6 16 CH 6
 /KEYS
   FLD_BI_9_7 ASCENDING,
   FLD_BI_4_5 ASCENDING,
   FLD_BI_16_6 ASCENDING,
   FLD_BI_1_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK003FAN
       ;;
(GK003FAN)
       m_CondExec 00,EQ,GK003FAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK140 : CREATION FIC STAT ACHATS FILIALES (CONSO)                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK003FAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GK003FAQ
       ;;
(GK003FAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* HISTO PRA                                                            
#    RSGG70   : NAME=RSGG70,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG70 /dev/null
#                                                                              
# ******  DATE  JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FIC   CUMUL ACHATS FILIALES                                          
       m_FileAssign -d SHR -g ${G_A3} FGK040 ${DATA}/PTEM/GK003FAM.BGK140AP
# ******  FIC   CUMUL PRODUITS FILIALES                                        
       m_FileAssign -d SHR -g ${G_A4} FGK020 ${DATA}/PXX0/F99.BGK130BP
# ******  FIC   CUMUL PRA FILIALES                                             
       m_FileAssign -d SHR -g ${G_A5} FGK045 ${DATA}/PTEM/GK003FAJ.BGK045XP
#                                                                              
# ******* FIC   DE STAT VENTES LIVREES DESTINE A KESA                          
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -t LSEQ -g +1 FGK140 ${DATA}/PXX0/F99.BGK140BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGK140 
       JUMP_LABEL=GK003FAR
       ;;
(GK003FAR)
       m_CondExec 04,GE,GK003FAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK010 : CREATION FIC TICKET D'HORODATAGE                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK003FAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GK003FAT
       ;;
(GK003FAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* DATE MMSSAA                                                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  DATE  JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  SOUS TABLE EUROP (DEVISE UTILISEE)                                   
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# ******* FIC   TICKET D'HORODATAGE DESTINE A KESA                             
       m_FileAssign -d NEW,CATLG,DELETE -r 29 -t LSEQ -g +1 FGK010 ${DATA}/PXX0/F99.BGK010AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGK010 
       JUMP_LABEL=GK003FAU
       ;;
(GK003FAU)
       m_CondExec 04,GE,GK003FAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CUMUL FICHIERS PRODUITS ISSUS DE GK001 FILIALES                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK003FAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GK003FAX
       ;;
(GK003FAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F96.BGK120AB
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BGK120AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BGK120AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BGK120AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BGK120AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BGK120AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BGK120AY
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.BGK120BP
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_55_7 55 CH 7
 /KEYS
   FLD_BI_55_7 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK003FAY
       ;;
(GK003FAY)
       m_CondExec 00,EQ,GK003FAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GK003FZA
       ;;
(GK003FZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK003FZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
