#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GK201M.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGK201 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/04/22 AT 09.14.35 BY PREPA2                       
#    STANDARDS: P  JOBSET: GK201M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BGK200 : GENERATION DU FICHIER PRODUITS A TRANSMETTRE                       
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GK201MA
       ;;
(GK201MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GK201MAA
       ;;
(GK201MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
       m_FileAssign -d SHR FETAT ${DATA}/CORTEX4.P.MTXTFIX1/KRISP01
#                                                                              
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA09   : NAME=RSGA09M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09 /dev/null
#    RSGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA11   : NAME=RSGA11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11 /dev/null
#    RSGA12   : NAME=RSGA12M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA22   : NAME=RSGA22M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22 /dev/null
#    RSGA29   : NAME=RSGA29M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA29 /dev/null
#    RSGA64   : NAME=RSGA64M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA64 /dev/null
#    RSGA83   : NAME=RSGA83M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA83 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 FGK200 ${DATA}/PTEM/GK201MAA.BGK201AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGK200 
       JUMP_LABEL=GK201MAB
       ;;
(GK201MAB)
       m_CondExec 04,GE,GK201MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PAR CODIC                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK201MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GK201MAD
       ;;
(GK201MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GK201MAA.BGK201AM
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 SORTOUT ${DATA}/PTEM/GK201MAD.BGK201BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK201MAE
       ;;
(GK201MAE)
       m_CondExec 00,EQ,GK201MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK211 : GENERATION DE SYSIN POUR FASTUNLOAD DES DONNEES PRODUIT            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK201MAG PGM=BGK211     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GK201MAG
       ;;
(GK201MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GK201M
#                                                                              
       m_FileAssign -d SHR FSYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK201MAG.sysin
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FGK211 ${DATA}/PTEM/GK201MAG.BGK211AM
       m_ProgramExec BGK211 
# ********************************************************************         
#  FASTUNLOAD DES TABLES RTGS10, RTGS30, RTGG31, RTGS40,                       
#                        RTGS42 ET RTGG70                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK201MAJ PGM=PTLDRIVM   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GK201MAJ
       ;;
(GK201MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
#                                                                              
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGS30   : NAME=RSGS30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
#    RSGS31   : NAME=RSGS31M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS31 /dev/null
#    RSGG70   : NAME=RSGG70M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70 /dev/null
#    RSHV02   : NAME=RSHV02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV02 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 SYSREC01 ${DATA}/PTEM/GK200MAJ.GG70UNAM
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SYSREC02 ${DATA}/PTEM/GK200MAJ.GS10UNAM
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SYSREC03 ${DATA}/PTEM/GK200MAJ.GS30UNAM
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SYSREC04 ${DATA}/PTEM/GK200MAJ.GS31UNAM
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -g +1 SYSREC05 ${DATA}/PTEM/GK201MAJ.HV02UNAM
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} SYSIN ${DATA}/PTEM/GK201MAG.BGK211AM
       
       #Untranslated utility: PTLDRIVM##Untranslate PTLDRIVM SYSIN

# ********************************************************************         
#  TRI PAR CODIC DES DONNEES DE LA TABLE RTHV02                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK201MAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GK201MAM
       ;;
(GK201MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GK201MAJ.HV02UNAM
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -g +1 SORTOUT ${DATA}/PTEM/GK201MAM.BGK221AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK201MAN
       ;;
(GK201MAN)
       m_CondExec 00,EQ,GK201MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PAR CODIC/DATE DES DONNEES DE LA TABLE RTGG70                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK201MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GK201MAQ
       ;;
(GK201MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GK200MAJ.GG70UNAM
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 SORTOUT ${DATA}/PTEM/GK201MAQ.BGK221BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_8_8 8 CH 8
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_8 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK201MAR
       ;;
(GK201MAR)
       m_CondExec 00,EQ,GK201MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PAR CODIC DES DONNEES DE LA TABLE RTGS10                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK201MAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GK201MAT
       ;;
(GK201MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GK200MAJ.GS10UNAM
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SORTOUT ${DATA}/PTEM/GK201MAT.BGK221CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK201MAU
       ;;
(GK201MAU)
       m_CondExec 00,EQ,GK201MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FUSION DES DONNEES DES TABLES RTGS30 ET RTGS31                              
#  TRI PAR CODIC                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK201MAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GK201MAX
       ;;
(GK201MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/GK200MAJ.GS30UNAM
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SORTOUT ${DATA}/PTEM/GK201MAX.BGK221DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK201MAY
       ;;
(GK201MAY)
       m_CondExec 00,EQ,GK201MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK221 : MISE EN FORME DES DONNEES PRODUITS                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK201MBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GK201MBA
       ;;
(GK201MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
#    RSGA59   : NAME=RSGA59M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
#    RSGF10   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10 /dev/null
#    RSGF30   : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30 /dev/null
#    RSGG50   : NAME=RSGG50M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#                                                                              
       m_FileAssign -d SHR -g ${G_A7} FGK200 ${DATA}/PTEM/GK201MAD.BGK201BM
       m_FileAssign -d SHR -g ${G_A8} RTHV02 ${DATA}/PTEM/GK201MAM.BGK221AM
       m_FileAssign -d SHR -g ${G_A9} RTGG70 ${DATA}/PTEM/GK201MAQ.BGK221BM
       m_FileAssign -d SHR -g ${G_A10} RTGS10 ${DATA}/PTEM/GK201MAT.BGK221CM
       m_FileAssign -d SHR -g ${G_A11} RTGS30 ${DATA}/PTEM/GK201MAX.BGK221DM
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 1000 -g +1 FGK221 ${DATA}/PXX0/F89.BGK221EM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGK221 
       JUMP_LABEL=GK201MBB
       ;;
(GK201MBB)
       m_CondExec 04,GE,GK201MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GK201MZA
       ;;
(GK201MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK201MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
