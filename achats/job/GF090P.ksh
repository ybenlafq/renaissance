#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GF090P.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGF090 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/05/30 AT 14.55.54 BY BURTEC7                      
#    STANDARDS: P  JOBSET: GF090P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  SORT DU FIC BGF090AP TRI DU FICHIER DE LA VEILLE (LRECL = 50)               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GF090PA
       ;;
(GF090PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GF090PAA
       ;;
(GF090PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ** PREDS ************                                                        
# *********************                                                        
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGP/F07.BGF090AP
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PTEM/GF090PAA.BGF090BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "B"
 /FIELDS FLD_CH_1_1 1 CH 1
 /CONDITION CND_1 FLD_CH_1_1 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GF090PAB
       ;;
(GF090PAB)
       m_CondExec 00,EQ,GF090PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGF090                                                                      
#  EXTRACT DU FICHIER FGF090                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF090PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GF090PAD
       ;;
(GF090PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ****** TABLE                                                                 
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ****** TABLE                                                                 
#    RSGF01   : NAME=RSGF01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF01 /dev/null
# ****** TABLE                                                                 
#    RSGF30   : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30 /dev/null
# ****** TABLE                                                                 
#    RSGR00   : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR00 /dev/null
# ****** TABLE                                                                 
#    RSGR05   : NAME=RSGR05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR05 /dev/null
#                                                                              
# ****** CARTE DATE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ****** FICHIER FGF090                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FGF090 ${DATA}/PTEM/GF090PAD.BGF090CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF090 
       JUMP_LABEL=GF090PAE
       ;;
(GF090PAE)
       m_CondExec 04,GE,GF090PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC FGF090                                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF090PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GF090PAG
       ;;
(GF090PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GF090PAD.BGF090CP
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PTEM/GF090PAG.BGF090DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_9_7 9 CH 7
 /FIELDS FLD_CH_31_8 31 CH 8
 /FIELDS FLD_CH_46_7 46 CH 7
 /FIELDS FLD_CH_2_7 02 CH 7
 /FIELDS FLD_CH_1_1 1 CH 1
 /FIELDS FLD_CH_25_6 25 CH 6
 /KEYS
   FLD_CH_2_7 ASCENDING,
   FLD_CH_9_7 ASCENDING,
   FLD_CH_25_6 ASCENDING,
   FLD_CH_31_8 ASCENDING,
   FLD_CH_46_7 ASCENDING,
   FLD_CH_1_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GF090PAH
       ;;
(GF090PAH)
       m_CondExec 00,EQ,GF090PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     E A S Y T R I E V E                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF090PAJ PGM=EZTPA00    ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GF090PAJ
       ;;
(GF090PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} OLDFILE ${DATA}/PTEM/GF090PAA.BGF090BP
       m_FileAssign -d SHR -g ${G_A3} NEWFILE ${DATA}/PTEM/GF090PAG.BGF090DP
       m_FileAssign -d NEW,CATLG,DELETE -r 104 -g +1 SORTIE ${DATA}/PTEM/GF090PAJ.BGF091AP
#                                                                              
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/GF090PAJ
       m_ProgramExec GF090PAJ
# ********************************************************************         
#  BGF091                                                                      
#  EXTRACT DU FICHIER FGF092                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF090PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GF090PAM
       ;;
(GF090PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ****** TABLE                                                                 
#                                                                              
# ****** CARTE DATE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ****** CARTE FXML                                                            
       m_FileAssign -d SHR -g +0 FXML ${DATA}/PNCGP/F07.BGF090PP
# **//FXML     DD DSN=PEXP.KOTTRPA.XML,DISP=SHR                                
# ****** FICHIER ISSU DE L EASYTRIEVE                                          
       m_FileAssign -d SHR -g ${G_A4} FGF091 ${DATA}/PTEM/GF090PAJ.BGF091AP
# ****** FICHIER FGF092                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 276 -g +1 FGF092 ${DATA}/PTEM/GF090PAM.BGF092AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF091 
       JUMP_LABEL=GF090PAN
       ;;
(GF090PAN)
       m_CondExec 04,GE,GF090PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BXML00                                                                      
#  ENVOI VERS MQ                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF090PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GF090PAQ
       ;;
(GF090PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ****** TABLE                                                                 
#                                                                              
# ****** CARTE DATE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** CARTE FPARAM                                                          
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GF090PAQ
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ****** FICHIER FGF092                                                        
       m_FileAssign -d SHR -g ${G_A5} FGF090 ${DATA}/PTEM/GF090PAM.BGF092AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BXML00 
       JUMP_LABEL=GF090PAR
       ;;
(GF090PAR)
       m_CondExec 04,GE,GF090PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO EN FIN DE CHAINE DU FICHIER BGF090DP SUR BGF090AP POUR LENDEM         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF090PAT PGM=IDCAMS     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GF090PAT
       ;;
(GF090PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A6} INPUT ${DATA}/PTEM/GF090PAG.BGF090DP
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 OUTPUT ${DATA}/PNCGP/F07.BGF090AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/REPRO.sysin
       m_UtilityExec
# ***                                                                          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GF090PAU
       ;;
(GF090PAU)
       m_CondExec 16,NE,GF090PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GF090PZA
       ;;
(GF090PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GF090PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
