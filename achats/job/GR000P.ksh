#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GR000P.ksh                       --- VERSION DU 17/10/2016 18:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGR000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/09/14 AT 13.47.21 BY BURTECA                      
#    STANDARDS: P  JOBSET: GR000P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
# ********************************************************************         
#  B G R 0 0 0                                                                 
# ********************************************************************         
#   EXTRACTION DES RECEPTIONS ADMINISTRATIVES DU JOUR VALIDEES                 
#   EXTRACTION DES RECEPTIONS ADMINISTRATIVES DU JOUR PAR FOURNISSEURS         
#   SEULES LES QUANTITES REELLEMENT COMMANDEES SONT TRAITEES                   
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GR000PA
       ;;
(GR000PA)
#
#GR000PAX
#GR000PAX Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GR000PAX
#
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RABO=${RABO:-GR000PBA}
       RUN=${RUN}
       JUMP_LABEL=GR000PAA
       ;;
(GR000PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    POUR AVOIR LE PRMP A JOUR         *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRE SOCIETE : 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#  FICHIER DES RECEPTIONS ADM DU JOUR                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 FGR001 ${DATA}/PTEM/GR000PAA.GR0001AP
#  FICHIER DES RECEPTIONS ADM DU JOUR PAR FOURNISSEURS                         
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 FGR000 ${DATA}/PTEM/GR000PAA.GR0000AP
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME                                    
#                 JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES ARTICLES                                                          
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLE    : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES ENTITES DE COMMANDES                                              
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEA   : NAME=RSGA06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEA /dev/null
#  TABLE DES FAMILLES                                                          
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEB   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEB /dev/null
#  TABLE DES COMMANDES FOURNISSEURS                                            
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEC   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEC /dev/null
#  TABLE DES RECEPTIONS                                                        
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLED   : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLED /dev/null
#  TABLE DES MVTS DE RECEPTIONS DU JOUR                                        
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEE   : NAME=RSGR20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEE /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR000 
       JUMP_LABEL=GR000PAB
       ;;
(GR000PAB)
       m_CondExec 04,GE,GR000PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER DE RECEPTIONS DU JOUR VALIDEES                              
#   1,3 CODE SOCIETE;4,3 CODE DEPOT; 7,7 NDE RECEPTION ADM;                    
#   56,7 CODIC         ; 29,7 NUM DE COMMANDE                                  
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GR000PAD
       ;;
(GR000PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM DU JOUR                                          
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GR000PAA.GR0001AP
#  FICHIER DES RECEPTIONS ADM DU JOUR TRIE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 SORTOUT ${DATA}/PTEM/GR000PAD.GR0001BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_56_7 56 CH 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_29_7 29 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_7_7 7 CH 7
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_56_7 ASCENDING,
   FLD_CH_29_7 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR000PAE
       ;;
(GR000PAE)
       m_CondExec 00,EQ,GR000PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 0 5                                                                 
# ********************************************************************         
#  EDITION DES RECEPTIONS VALIDEES DU JOUR                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GR000PAG
       ;;
(GR000PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM DU JOUR VALIDEES                                 
       m_FileAssign -d SHR -g ${G_A2} FGR001 ${DATA}/PTEM/GR000PAD.GR0001BP
#  LISTE DES RECPTIONS VALIDEES DU JOUR IGR005                                 
       m_OutputAssign -c 9 -w BGR005 IGR005
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME                                    
#                 JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#  TABLE DES LIGNES DE RECEPTIONS ECHEANCEES                                   
#    RTGR10   : NAME=RSGR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGR10 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR005 
       JUMP_LABEL=GR000PAH
       ;;
(GR000PAH)
       m_CondExec 04,GE,GR000PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 0                                                                 
# ********************************************************************         
#  EXTRACTION DES RECEPTIONS MODIFIEES DU JOUR                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GR000PAJ
       ;;
(GR000PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRE SOCIETE : 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#  FICHIER DES MODIFICATIONS DES RECEPTIONS DU JOUR                            
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -g +1 FGR011 ${DATA}/PTEM/GR000PAJ.GR0011AP
#  FICHIER DES MODIFICATION DES REEPTIONS DU JOUR PAR FOURNISSEURS             
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 FGR000 ${DATA}/PTEM/GR000PAJ.GR0000BP
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME                                    
#                 JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES ARTICLES                                                          
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLE    : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES ENTITES DE COMMANDE                                               
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEA   : NAME=RSGA06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEA /dev/null
#  TABLE DES COMMANDES FOURNISSEURS                                            
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEB   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEB /dev/null
#  TABLE DES RECEPTIONS                                                        
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEC   : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEC /dev/null
#  TABLE DES RECEPTIONS PAR ARTICLES                                           
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLED   : NAME=RSGR05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLED /dev/null
#  TABLE DES LIGNES DE RECEPTIONS ECHEANCEES                                   
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEE   : NAME=RSGR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEE /dev/null
#  TABLE DES MVTS DE RECEPTIONS DU JOUR                                        
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEF   : NAME=RSGR20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEF /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR010 
       JUMP_LABEL=GR000PAK
       ;;
(GR000PAK)
       m_CondExec 04,GE,GR000PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER DES RECEPTIONS MODIFIEES DU JOUR                            
#   1,3 CODE SOCIETE;4,3 CODE DEPOT;7,7 N DE RECEPTION ADM;                    
#   44,5 CODE FAMILLE ; 49,5 CODE MARQUE; 74,8 N DE COMMANDE                   
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GR000PAM
       ;;
(GR000PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM MODIFIEES DU JOUR                                
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GR000PAJ.GR0011AP
#  FICHIER DES RECEPTIONS ADM MODIFIEES DU JOUR TRIE                           
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -g +1 SORTOUT ${DATA}/PTEM/GR000PAM.GR0011BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_74_8 74 CH 8
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_44_5 44 CH 5
 /FIELDS FLD_CH_49_5 49 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_44_5 ASCENDING,
   FLD_CH_49_5 ASCENDING,
   FLD_CH_74_8 ASCENDING
 /* Record Type = F  Record Length = 135 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR000PAN
       ;;
(GR000PAN)
       m_CondExec 00,EQ,GR000PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 5                                                                 
# ********************************************************************         
#  EDITION DES RECEPTIONS MODIFIEES DU JOUR                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GR000PAQ
       ;;
(GR000PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM MODIFIEES DU JOUR TRIE                           
       m_FileAssign -d SHR -g ${G_A4} FGR011 ${DATA}/PTEM/GR000PAM.GR0011BP
#  LISTE DES RECEPTIONS MODIFIEES DU JOUR                                      
       m_OutputAssign -c 9 -w BGR015 IGR015
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME                                    
#                 JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES LIGNES DE RECEPTIONS ECHEANCEES                                   
#    RTPT03   : NAME=RSPT03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPT03 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR015 
       JUMP_LABEL=GR000PAR
       ;;
(GR000PAR)
       m_CondExec 04,GE,GR000PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER DES RECEPTIONS DU JOUR PAR FOURNISSEURS                     
#   TRI DU FICHIER DES RECEPTIONS DU JOUR MAJ PAR FOURNISSEURS                 
#   POUR NE FAIRE Q UN SEUL FICHIER                                            
#   122,3 TYPE MVT; 86,1 TYPE CODIC; 1,7 N DE RECEPTION ADM;                   
#   31,5 N DE FOURNISSEURS; 137,10 NUM DE BL                                   
#   125,1 CODE MVT                                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GR000PAT
       ;;
(GR000PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM DU JOUR PAR FOURNISSEURS                         
#  FICHIER DES RECEPTIONS ADM MODIFIEES DU JOUR PAR FOURNISSEURS               
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GR000PAA.GR0000AP
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/GR000PAJ.GR0000BP
#  FICHIER DES RECEPTIONS ADM DU JOUR TRIE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 SORTOUT ${DATA}/PTEM/GR000PAT.GR0000CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_122_3 122 CH 3
 /FIELDS FLD_CH_86_1 86 CH 1
 /FIELDS FLD_CH_125_1 125 CH 1
 /FIELDS FLD_CH_137_10 137 CH 10
 /FIELDS FLD_CH_31_5 31 CH 5
 /KEYS
   FLD_CH_122_3 ASCENDING,
   FLD_CH_86_1 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_31_5 ASCENDING,
   FLD_CH_137_10 ASCENDING,
   FLD_CH_125_1 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR000PAU
       ;;
(GR000PAU)
       m_CondExec 00,EQ,GR000PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QUIESCE DES TABLES RTGR30 ET RTGR35 AVANT MAJ                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000PAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GR000PBA
       ;;
(GR000PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS DU JOUR CREE OU MAJ                                  
       m_FileAssign -d SHR -g ${G_A7} FGR000 ${DATA}/PTEM/GR000PAT.GR0000CP
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME                                    
#                 JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES RECPTIONS COMPTABLES                                              
#    TABLE    : NAME=RSGR30,MODE=U - DYNAM=YES                                 
# -X-RSGR30   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR TABLE /dev/null
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEA   : NAME=RSGR35,MODE=U - DYNAM=YES                                 
# -X-RSGR35   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR TABLEA /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR020 
       JUMP_LABEL=GR000PBB
       ;;
(GR000PBB)
       m_CondExec 04,GE,GR000PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI CODE RETOUR DIFFERENT DE 0 ABEND                                         
# --------------------------------------------------------------------         
#                                                                              
# ***********************************                                          
# *   STEP GR000PBD PGM=ZUTABEND   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GR000PBD
       ;;
(GR000PBD)
       m_CondExec ${EXABT},NE,YES 0,EQ,$[RABO] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# ********************************************************************         
#  B G R 0 2 5                                                                 
# ********************************************************************         
#  LISTE DES RECEPTIONS DU JOUR                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GR000PBG
       ;;
(GR000PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME                                    
#                 JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
#  LISTE DES RECEPTIONS DU JOUR                                                
       m_OutputAssign -c 9 -w BGR025 IGR025
#  TABLE DES RECEPTIONS COMPTABLES                                             
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLE    : NAME=RSGR30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES LIGNES DE RECEPTIONS COMPTABLES                                   
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEA   : NAME=RSGR35,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEA /dev/null
       m_FileAssign -d SHR FDARDAC ${DATA}/CORTEX4.P.MTXTFIX1/GR000PBG
# ******  PARAMETRE SOCIETE : 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR025 
       JUMP_LABEL=GR000PBH
       ;;
(GR000PBH)
       m_CondExec 04,GE,GR000PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 3 0                                                                 
# ********************************************************************         
#  LISTE DES BONS DE RECEPTIONS VALORISES DU JOUR                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000PBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GR000PBJ
       ;;
(GR000PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME                                    
#                 JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
#  LISTE DES BONS DE RECEPTIONS VALORISES DU JOUR/PAR N�RECEP.CPT              
       m_OutputAssign -c 9 -w BGR030 IGR030
#  LISTE DES BONS DE RECEPTIONS VALORISES DU JOUR/PAR FOURNISSEUR              
       m_FileAssign -d NEW,CATLG,DELETE -r 153 -g +1 FGR030 ${DATA}/PTEM/GR000PBJ.FGR030AP
#  TABLE DES ARTICLES                                                          
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLE    : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES LIENS ARTICLES                                                    
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEA   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEA /dev/null
#  TABLE DES LIGNES DE COMMANDES FOURNISSEURS                                  
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEB   : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEB /dev/null
#  TABLE DES RISTOURNES SUR LIGNES DE COMMANDES FOURNISSEURS                   
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEC   : NAME=RSGF25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEC /dev/null
#  TABLE DES PRIX MOYENS PONDERES (PRMP DU JOUR)                               
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLED   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLED /dev/null
#  TABLE DES RECEPTIONS COMPTABLES                                             
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEE   : NAME=RSGR30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEE /dev/null
#  TABLE DES LIGNES DE RECEPTIONS COMPTABLE                                    
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEF   : NAME=RSGR35,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEF /dev/null
       m_FileAssign -d SHR FDARDAC ${DATA}/CORTEX4.P.MTXTFIX1/GR000PBJ
# ******  PARAMETRE SOCIETE : 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR030 
       JUMP_LABEL=GR000PBK
       ;;
(GR000PBK)
       m_CondExec 04,GE,GR000PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FGR030AP POUR CREATION D UNE EDITION                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GR000PBM
       ;;
(GR000PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GR000PBJ.FGR030AP
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 SORTOUT ${DATA}/PTEM/GR000PBM.FGR030BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_20 1 CH 20
 /FIELDS FLD_CH_21_133 21 CH 133
 /KEYS
   FLD_BI_1_20 ASCENDING
 /* Record Type = F  Record Length = 133 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_21_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GR000PBN
       ;;
(GR000PBN)
       m_CondExec 00,EQ,GR000PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEBGENER POUR CREATION D UNE EDITION IGR030B                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000PBQ PGM=IEBGENER   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GR000PBQ
       ;;
(GR000PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SYSUT1 ${DATA}/PTEM/GR000PBM.FGR030BP
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GR000PBR
       ;;
(GR000PBR)
       m_CondExec 00,EQ,GR000PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 3 5                                                                 
# ********************************************************************         
#  LISTE DES RECEPTIONS MODIFIEES VALORISEES DU JOUR                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000PBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GR000PBT
       ;;
(GR000PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME                                    
#                 JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
#  LISTE DES RECEPTIONS MODIFIEES VALORISEES DU JOUR                           
       m_OutputAssign -c 9 -w BGR035 IGR035
#  TABLE DES ARTICLES                                                          
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLE    : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES LIENS ARTICLES                                                    
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEA   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEA /dev/null
#  TABLE DES LIGNES DE COMMANDES FOURNISSEURS                                  
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEB   : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEB /dev/null
#  TABLE DES RISTOURNES SUR LIGNES DE COMMANDES FOURNISSEURS                   
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEC   : NAME=RSGF25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEC /dev/null
#  TABLE DES PRIX MOYENS PONDERES (PRMP DU JOUR)                               
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLED   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLED /dev/null
#  TABLE DES RECEPTIONS COMPTABLES                                             
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEE   : NAME=RSGR30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEE /dev/null
#  TABLE DES LIGNES DE RECEPTIONS COMPTABLE                                    
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLEF   : NAME=RSGR35,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEF /dev/null
# ******  PARAMETRE SOCIETE : 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR035 
       JUMP_LABEL=GR000PBU
       ;;
(GR000PBU)
       m_CondExec 04,GE,GR000PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GR000PZA
       ;;
(GR000PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GR000PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
