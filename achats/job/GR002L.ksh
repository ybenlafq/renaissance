#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GR002L.ksh                       --- VERSION DU 17/10/2016 18:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGR002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/03/17 AT 10.15.42 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GR002L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLESPACES RSGG50L,RSGG60L,RSGG70L                             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GR002LA
       ;;
(GR002LA)
#
#GR002LAA
#GR002LAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GR002LAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GR002LAD
       ;;
(GR002LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES RECEPTIONS FOURNISSEURS                                    
#    RSGR00L  : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR00L /dev/null
# ******  TABLE DES LIGNES DE RECEPTIONS FOURNISSEURS PAR SOCIETES /           
#    RSGR15L  : NAME=RSGR15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR15L /dev/null
# ******  TABLE DES LIGNES DE COMMANDES FOURNISSEURS                           
#    RSGF20L  : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20L /dev/null
# ******  TABLE DES ARTICLES                                                   
#    RSGA00L  : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
# *******                                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *******                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CREATION DU FIC-EXTRACTION DES RECEPTIONS                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB07 ${DATA}/PTEM/GR002LAD.BGB007AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR002 
       JUMP_LABEL=GR002LAE
       ;;
(GR002LAE)
       m_CondExec 04,GE,GR002LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 0 6                                                                 
# ********************************************************************         
#   SELECTION DES M.A.J EFFECTUEES SUR LES RECEPTIONS QUI NE                   
#      DATENT PAS DE LA JOURNEE DE TRAITEMENT                                  
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GR002LAG
       ;;
(GR002LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES RECEPTIONS FOURNISSEURS                                    
#    RSGR00L  : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR00L /dev/null
# ******  TABLE DES LIGNES DE RECEPTIONS FOURNISSEURS PAR SOCIETES,LIE         
#    RSGR15L  : NAME=RSGR15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR15L /dev/null
# ******  TABLE DES MOUVEMENTS DE RECEPTION DU JOUR                            
#    RSGR20L  : NAME=RSGR20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR20L /dev/null
# ******  TABLE DES LIGNES DE COMMANDES FOURNISSEURS                           
#    RSGF20L  : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20L /dev/null
# ******  TABLE DES HISTORIQUES DE RECEPTION                                   
#    RSGG70L  : NAME=RSGG70L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70L /dev/null
# ******  TABLE DES ARTICLES                                                   
#    RSGA00L  : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CREATION DU FICHIER DES M.A.J EFFECTUEES                             
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB08 ${DATA}/PTEM/GR002LAG.BGB008AL
#                                                                              
# ******  PARAMETRE SOCIETE : 961                                              
       m_FileAssign -d SHR FSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR006 
       JUMP_LABEL=GR002LAH
       ;;
(GR002LAH)
       m_CondExec 04,GE,GR002LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PAR CODIC,TYPE ENREGISTREMENT,NUM-RECEPTION,NUM-COMMANDE ET             
#    DATE MODIFICATION  POUR CREATION DE FGB10                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002LAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GR002LAJ
       ;;
(GR002LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS EXTRACTIONS ET FIC M.A.J                                    
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GR002LAD.BGB007AL
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/GR002LAG.BGB008AL
# ******  FICHIER SORTIE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/GR002LAJ.BGB010AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_7 24 CH 7
 /FIELDS FLD_CH_31_7 31 CH 7
 /FIELDS FLD_CH_3_7 3 CH 7
 /FIELDS FLD_CH_1_1 1 CH 1
 /FIELDS FLD_CH_59_6 59 CH 6
 /KEYS
   FLD_CH_3_7 ASCENDING,
   FLD_CH_1_1 ASCENDING,
   FLD_CH_24_7 ASCENDING,
   FLD_CH_31_7 ASCENDING,
   FLD_CH_59_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002LAK
       ;;
(GR002LAK)
       m_CondExec 00,EQ,GR002LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 2                                                                 
# ********************************************************************         
#   EXTRACTION DES DEMANDES DE RECYCLAGE DE PRMP                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002LAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GR002LAM
       ;;
(GR002LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES                                                   
#    RSGA00L  : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
# ******  TABLE DES DEMANDES DE RECYCLAGES DE PRA                              
#    RSGG60L  : NAME=RSGG60L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG60L /dev/null
# ******  TABLE DES HISTORIQUES DE RECEPTIONS                                  
#    RSGG70L  : NAME=RSGG70L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70L /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER EXTRACTION DES DEMANDES DE RECYCLAGE DE PRMP                 
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB09 ${DATA}/PTEM/GR002LAM.BGB009AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR012 
       JUMP_LABEL=GR002LAN
       ;;
(GR002LAN)
       m_CondExec 04,GE,GR002LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 3                                                                 
# ********************************************************************         
#  CALCUL DU STOCK SOCIETE POUR LES RECEPTIONS DU JOUR                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002LAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GR002LAQ
       ;;
(GR002LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISE : SOUS TABLE EXMAG                                  
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
# ******  TABLE DES STOCKS DE L ENTREPOT                                       
#    RSGS10L  : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10L /dev/null
# ******  TABLE DES STOCKS SOUS LIEUX ENTREPOT                                 
#    RSGS30L  : NAME=RSGS30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30L /dev/null
# ******  TABLE DES MVTS DE RECEPTION DU JOUR                                  
#    RSGR20L  : NAME=RSGR20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR20L /dev/null
# ******  FICHIER EXTRACTION                                                   
       m_FileAssign -d SHR -g ${G_A3} FGB09 ${DATA}/PTEM/GR002LAM.BGB009AL
# ******  FICHIER EXTRACTIONS TRIE                                             
       m_FileAssign -d SHR -g ${G_A4} FGB10 ${DATA}/PTEM/GR002LAJ.BGB010AL
# ******  FICHIER STOCK SOCIETE POUR RECEP DU JOUR                             
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB11 ${DATA}/PTEM/GR002LAQ.BGB011AL
#                                                                              
# *******                                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR013 
       JUMP_LABEL=GR002LAR
       ;;
(GR002LAR)
       m_CondExec 04,GE,GR002LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER STOCK SOCIETE                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002LAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GR002LAT
       ;;
(GR002LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER EXTRACTION                                                   
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GR002LAQ.BGB011AL
# ******  FICHIER EXTRACTIONS TRIES                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PGB961/F61.BGB013AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_7 3 CH 7
 /FIELDS FLD_CH_24_14 24 CH 14
 /FIELDS FLD_CH_1_1 1 CH 1
 /FIELDS FLD_CH_16_8 16 CH 8
 /KEYS
   FLD_CH_3_7 ASCENDING,
   FLD_CH_16_8 ASCENDING,
   FLD_CH_1_1 ASCENDING,
   FLD_CH_24_14 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002LAU
       ;;
(GR002LAU)
       m_CondExec 00,EQ,GR002LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 4                                                                 
# ********************************************************************         
#   RECALCUL DU PRMP ET MAJ DES TABLES RTGG50 RTGG60 RTGG70                    
#   REPRISE: OUI SI ABEND                                                      
#          : NON SI FIN NORMALE FAIRE UN RECOVER TO RBA DES TABLES             
#            RTGG50 - RTGG60 - RTGG70 A PARTIR DU QUIESCE DU DEBUT DE          
#            LA CHAINE                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002LAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GR002LAX
       ;;
(GR002LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#    RSGA06L  : NAME=RSGA06L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06L /dev/null
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
# ******  TABLE DES PRMP DU JOUR                                               
#    RSGG50L  : NAME=RSGG50L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50L /dev/null
# ******  TABLE DES DEMANDES DE RECYCLAGES DE PRA                              
#    RSGG60L  : NAME=RSGG60L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGG60L /dev/null
# ******  TABLE DES HISTORIQUES DE RECEPTIONS                                  
#    RSGG70L  : NAME=RSGG70L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70L /dev/null
#                                                                              
# ******  FICHIER EXTRACTION CONTROLE TRIE                                     
       m_FileAssign -d SHR -g ${G_A6} FGB13 ${DATA}/PGB961/F61.BGB013AL
# ******  FICHIER DES RECYCLAGE DE PRMP                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB16 ${DATA}/PTEM/GR002LAX.BGB016AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR014 
       JUMP_LABEL=GR002LAY
       ;;
(GR002LAY)
       m_CondExec 04,GE,GR002LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#   EASYTREAVE                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002LBA PGM=EZTPA00    ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GR002LBA
       ;;
(GR002LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#  FICHIER DES RECYCLAGE DE PRMP                                               
       m_FileAssign -d SHR -g ${G_A7} FILEA ${DATA}/PTEM/GR002LAX.BGB016AL
       m_FileAssign -d NEW,CATLG,DELETE -r 108 -g +1 FILEB ${DATA}/PTEM/GR002LBA.BGR002AL
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX4/GR002LBA
       m_ProgramExec GR002LBA
# ********************************************************************         
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  TRI DU FICHIER ISSU DE L'EASYTRIEVE                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002LBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GR002LBD
       ;;
(GR002LBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER EXTRACTION                                                          
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GR002LBA.BGR002AL
#  FICHIER EXTRACTIONS TRIES                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GR002LBD.BGR002BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_9_1 09 CH 01
 /FIELDS FLD_BI_15_7 15 CH 07
 /FIELDS FLD_BI_10_6 10 CH 06
 /FIELDS FLD_BI_7_2 07 CH 02
 /KEYS
   FLD_BI_7_2 ASCENDING,
   FLD_BI_9_1 ASCENDING,
   FLD_BI_10_6 ASCENDING,
   FLD_BI_15_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002LBE
       ;;
(GR002LBE)
       m_CondExec 00,EQ,GR002LBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002LBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GR002LBG
       ;;
(GR002LBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******************** TABLES DU GENERATEUR D'ETATS                            
#    RTEG00   : NAME=RSEG00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/GR002LBD.BGR002BL
# *********************************** FICHIER FCUMULS                          
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GR002LBG.BGR002CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GR002LBH
       ;;
(GR002LBH)
       m_CondExec 04,GE,GR002LBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  TRI DU FICHIER FCUMUL                                                       
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002LBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GR002LBJ
       ;;
(GR002LBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMUL                           
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/GR002LBG.BGR002CL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GR002LBJ.BGR002DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002LBK
       ;;
(GR002LBK)
       m_CondExec 00,EQ,GR002LBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IGR016                                                 
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002LBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GR002LBM
       ;;
(GR002LBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A11} FEXTRAC ${DATA}/PTEM/GR002LBD.BGR002BL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FCUMULS ${DATA}/PTEM/GR002LBJ.BGR002DL
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG132 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w BGR016 FEDITION
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GR002LBN
       ;;
(GR002LBN)
       m_CondExec 04,GE,GR002LBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GR002LZA
       ;;
(GR002LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GR002LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
