#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GK002O.ksh                       --- VERSION DU 08/10/2016 12:37
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGK002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/03/13 AT 15.21.50 BY BURTECN                      
#    STANDARDS: P  JOBSET: GK002O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  TRI DU FIC HISTO DES VENTES                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GK002OA
       ;;
(GK002OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GK002OAA
       ;;
(GK002OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGV0/F16.HV01AO
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PTEM/GK002OAA.BGK030FO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_14_6 14 CH 6
 /FIELDS FLD_CH_1_10 1 CH 10
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_14_6 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK002OAB
       ;;
(GK002OAB)
       m_CondExec 00,EQ,GK002OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK030F: CREATION DU FICHIER STATS DE VENTES LIVREES A PARTIR DE HV         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK002OAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GK002OAD
       ;;
(GK002OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* PRMP                                                                 
#    RSGG50O  : NAME=RSGG50O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50O /dev/null
# ******* ZONE DE PRIX                                                         
#    RSGA59O  : NAME=RSGA59O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59O /dev/null
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DATE MMSSAA                                                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******* SOCIETE                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FIC HISTO STAT DE VENTE LIVREE                                       
       m_FileAssign -d SHR -g ${G_A1} RTHV01 ${DATA}/PTEM/GK002OAA.BGK030FO
# ******* FIC STAT DE VENTE LIVREE                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -g +1 FGK030 ${DATA}/PXX0/F16.BGK030BO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGK030F 
       JUMP_LABEL=GK002OAE
       ;;
(GK002OAE)
       m_CondExec 04,GE,GK002OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK045 : CREATION DU FICHIER DES PRA                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK002OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GK002OAG
       ;;
(GK002OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* PRA                                                                  
#    RSGG70O  : NAME=RSGG70O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70O /dev/null
#                                                                              
# ******* DATE MMSSAA                                                          
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ******* FIC STAT DE VENTE LIVREE                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 13 -g +1 FGK045 ${DATA}/PXX0/F16.BGK045BO
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGK045 
       JUMP_LABEL=GK002OAH
       ;;
(GK002OAH)
       m_CondExec 04,GE,GK002OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GK002OZA
       ;;
(GK002OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK002OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
