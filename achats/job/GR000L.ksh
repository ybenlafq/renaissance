#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GR000L.ksh                       --- VERSION DU 17/10/2016 18:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGR000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/12/17 AT 13.59.12 BY BURTECN                      
#    STANDARDS: P  JOBSET: GR000L                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#                                                                              
# ********************************************************************         
#  B G R 0 0 0                                                                 
# ********************************************************************         
#   EXTRACTION DES RECEPTIONS ADMINISTRATIVES DU JOUR VALIDEES                 
#   EXTRACTION DES RECEPTIONS ADMINISTRATIVES DU JOUR PAR FOURNISSEURS         
#   SEULES LES QUANTITES REELLEMENT COMMANDEES SONT TRAITEES                   
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GR000LA
       ;;
(GR000LA)
#
#GR000LAX
#GR000LAX Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GR000LAX
#
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RABO=${RABO:-GR000LBA}
       RUN=${RUN}
       JUMP_LABEL=GR000LAA
       ;;
(GR000LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    POUR AVOIR LE PRMP A JOUR(RTGG50) *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FNSOC                                                                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
#  FICHIER DES RECEPTIONS ADM DU JOUR                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 FGR001 ${DATA}/PTEM/GR000LAA.GR0001AL
#  FICHIER DES RECEPTIONS ADM DU JOUR PAR FOURNISSEURS                         
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 FGR000 ${DATA}/PTEM/GR000LAA.GR0000AL
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME JJMMSSAA                           
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES ARTICLES                                                          
#    TABLE    : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES ENTITES DE COMMANDES                                              
#    TABLEA   : NAME=RSGA06L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEA /dev/null
#  TABLE DES FAMILLES                                                          
#    TABLEB   : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEB /dev/null
#  TABLE DES COMMANDES FOURNISSEURS                                            
#    TABLEC   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEC /dev/null
#  TABLE DES RECEPTIONS                                                        
#    TABLED   : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLED /dev/null
#  TABLE DES MVTS DE RECEPTIONS DU JOUR                                        
#    TABLEE   : NAME=RSGR20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEE /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR000 
       JUMP_LABEL=GR000LAB
       ;;
(GR000LAB)
       m_CondExec 04,GE,GR000LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  S O R T                                                                     
#   TRI DU FICHIER DE RECEPTIONS DU JOUR VALIDEES                              
#   1,3 CODE SOCIETE;4,3 CODE DEPOT; 7,7 NDE RECEPTION ADM;                    
#   56,7 CODIC         ; 29,7 NUM DE COMMANDE                                  
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000LAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GR000LAD
       ;;
(GR000LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM DU JOUR                                          
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GR000LAA.GR0001AL
#  FICHIER DES RECEPTIONS ADM DU JOUR TRIE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 SORTOUT ${DATA}/PTEM/GR000LAD.GR0001BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_56_7 56 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_29_7 29 CH 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_7 7 CH 7
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_56_7 ASCENDING,
   FLD_CH_29_7 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR000LAE
       ;;
(GR000LAE)
       m_CondExec 00,EQ,GR000LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 0 5                                                                 
# ********************************************************************         
#  EDITION DES RECEPTIONS VALIDEES DU JOUR                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GR000LAG
       ;;
(GR000LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM DU JOUR VALIDEES                                 
       m_FileAssign -d SHR -g ${G_A2} FGR001 ${DATA}/PTEM/GR000LAD.GR0001BL
#  LISTE DES RECPTIONS VALIDEES DU JOUR IGR005                                 
       m_OutputAssign -c 9 -w BGR005 IGR005
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME                                    
#  JJMMSSAA                                                                    
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#  TABLE DES LIGNES DE RECEPTIONS ECHEANCEES                                   
#    RTGR10   : NAME=RSGR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGR10 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR005 
       JUMP_LABEL=GR000LAH
       ;;
(GR000LAH)
       m_CondExec 04,GE,GR000LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 0                                                                 
# ********************************************************************         
#  EXTRACTION DES RECEPTIONS MODIFIEES DU JOUR                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000LAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GR000LAJ
       ;;
(GR000LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FNSOC                                                                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
#  FICHIER DES MODIFICATIONS DES RECEPTIONS DU JOUR                            
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -g +1 FGR011 ${DATA}/PTEM/GR000LAJ.GR0011AL
#  FICHIER DES MODIFICATION DES REEPTIONS DU JOUR PAR FOURNISSEURS             
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 FGR000 ${DATA}/PTEM/GR000LAJ.GR0000BL
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME JJMMSSAA                           
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES ARTICLES                                                          
#    TABLE    : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES ENTITES DE COMMANDE                                               
#    TABLEA   : NAME=RSGA06L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEA /dev/null
#  TABLE DES COMMANDES FOURNISSEURS                                            
#    TABLEB   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEB /dev/null
#  TABLE DES RECEPTIONS                                                        
#    TABLEC   : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEC /dev/null
#  TABLE DES RECEPTIONS PAR ARTICLES                                           
#    TABLED   : NAME=RSGR05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLED /dev/null
#  TABLE DES LIGNES DE RECEPTIONS ECHEANCEES                                   
#    TABLEE   : NAME=RSGR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEE /dev/null
#  TABLE DES MVTS DE RECEPTIONS DU JOUR                                        
#    TABLEF   : NAME=RSGR20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEF /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR010 
       JUMP_LABEL=GR000LAK
       ;;
(GR000LAK)
       m_CondExec 04,GE,GR000LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  S O R T                                                                     
#   TRI DU FICHIER DES RECEPTIONS MODIFIEES DU JOUR                            
#   1,3 CODE SOCIETE;4,3 CODE DEPOT;7,7 N DE RECEPTION ADM;                    
#   44,5 CODE FAMILLE ; 49,5 CODE MARQUE; 74,8 N DE COMMANDE                   
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000LAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GR000LAM
       ;;
(GR000LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM MODIFIEES DU JOUR                                
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GR000LAJ.GR0011AL
#  FICHIER DES RECEPTIONS ADM MODIFIEES DU JOUR TRIE                           
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -g +1 SORTOUT ${DATA}/PTEM/GR000LAM.GR0011BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_49_5 49 CH 5
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_44_5 44 CH 5
 /FIELDS FLD_CH_74_8 74 CH 8
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_44_5 ASCENDING,
   FLD_CH_49_5 ASCENDING,
   FLD_CH_74_8 ASCENDING
 /* Record Type = F  Record Length = 135 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR000LAN
       ;;
(GR000LAN)
       m_CondExec 00,EQ,GR000LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 5                                                                 
# ********************************************************************         
#  EDITION DES RECEPTIONS MODIFIEES DU JOUR                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000LAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GR000LAQ
       ;;
(GR000LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM MODIFIEES DU JOUR TRIE                           
       m_FileAssign -d SHR -g ${G_A4} FGR011 ${DATA}/PTEM/GR000LAM.GR0011BL
#  LISTE DES RECEPTIONS MODIFIEES DU JOUR                                      
       m_OutputAssign -c 9 -w BGR015 IGR015
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME                                    
#                 JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES LIGNES DE RECEPTIONS ECHEANCEES                                   
#    RTPT03   : NAME=RSPT03L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTPT03 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR015 
       JUMP_LABEL=GR000LAR
       ;;
(GR000LAR)
       m_CondExec 04,GE,GR000LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  S O R T                                                                     
#   TRI DU FICHIER DES RECEPTIONS DU JOUR PAR FOURNISSEURS                     
#   TRI DU FICHIER DES RECEPTIONS DU JOUR MAJ PAR FOURNISSEURS                 
#   POUR NE FAIRE Q UN SEUL FICHIER                                            
#   122,3 TYPE MVT; 86,1 TYPE CODIC; 1,7 N DE RECEPTION ADM;                   
#   31,5 N DE FOURNISSEURS; 125,1 CODE MVT                                     
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000LAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GR000LAT
       ;;
(GR000LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM DU JOUR PAR FOURNISSEURS                         
#  FICHIER DES RECEPTIONS ADM MODIFIEES DU JOUR PAR FOURNISSEURS               
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GR000LAA.GR0000AL
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/GR000LAJ.GR0000BL
#  FICHIER DES RECEPTIONS ADM DU JOUR TRIE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 SORTOUT ${DATA}/PTEM/GR000LAT.GR0000CL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_31_5 31 CH 5
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_125_1 125 CH 1
 /FIELDS FLD_CH_137_10 137 CH 10
 /FIELDS FLD_CH_86_1 86 CH 1
 /FIELDS FLD_CH_122_3 122 CH 3
 /KEYS
   FLD_CH_122_3 ASCENDING,
   FLD_CH_86_1 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_31_5 ASCENDING,
   FLD_CH_137_10 ASCENDING,
   FLD_CH_125_1 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR000LAU
       ;;
(GR000LAU)
       m_CondExec 00,EQ,GR000LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QUIESCE DES TABLES RTGR30  ET RTGR35   AVANT MAJ                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000LAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GR000LBA
       ;;
(GR000LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS DU JOUR CREE OU MAJ                                  
       m_FileAssign -d SHR -g ${G_A7} FGR000 ${DATA}/PTEM/GR000LAT.GR0000CL
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME JJMMSSAA                           
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES RECPTIONS COMPTABLES                                              
#    TABLE    : NAME=RSGR30L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR TABLE /dev/null
#    TABLEA   : NAME=RSGR35L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR TABLEA /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR020 
       JUMP_LABEL=GR000LBB
       ;;
(GR000LBB)
       m_CondExec 04,GE,GR000LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI CODE RETOUR DIFFERENT DE 0 ABEND                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000LBD PGM=ZUTABEND   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GR000LBD
       ;;
(GR000LBD)
       m_CondExec ${EXABT},NE,YES 0,EQ,$[RABO] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# ********************************************************************         
#  B G R 0 2 5                                                                 
# ********************************************************************         
#  LISTE DES RECEPTIONS DU JOUR                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000LBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GR000LBG
       ;;
(GR000LBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME JJMMSSAA                           
       m_FileAssign -i FDATE
$FDATE
_end
#  LISTE DES RECEPTIONS DU JOUR                                                
       m_OutputAssign -c 9 -w BGR025 IGR025
#  TABLE DES RECEPTIONS COMPTABLES                                             
#    TABLE    : NAME=RSGR30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES LIGNES DE RECEPTIONS COMPTABLES                                   
#    TABLEA   : NAME=RSGR35L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEA /dev/null
       m_FileAssign -d SHR FDARDAC ${DATA}/CORTEX4.P.MTXTFIX1/GR000LBG
# ******  CARTE SOCIETE                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR025 
       JUMP_LABEL=GR000LBH
       ;;
(GR000LBH)
       m_CondExec 04,GE,GR000LBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 3 0                                                                 
# ********************************************************************         
#  LISTE DES BONS DE RECEPTIONS VALORISES DU JOUR                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000LBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GR000LBJ
       ;;
(GR000LBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME JJMMSSAA                           
       m_FileAssign -i FDATE
$FDATE
_end
#  LISTE DES BONS DE RECEPTIONS VALORISES DU JOUR                              
       m_OutputAssign -c 9 -w BGR030 IGR030
#  LISTE DES BONS DE RECEPTIONS VALORISES DU JOUR/PAR FOURNISSEUR              
       m_FileAssign -d NEW,CATLG,DELETE -r 153 -g +1 FGR030 ${DATA}/PTEM/GR000LBJ.FGR030AL
#  TABLE DES ARTICLES                                                          
#    TABLE    : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES LIENS ARTICLES                                                    
#    TABLEA   : NAME=RSGA58L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEA /dev/null
#  TABLE DES LIGNES DE COMMANDES FOURNISSEURS                                  
#    TABLEB   : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEB /dev/null
#  TABLE DES RISTOURNES SUR LIGNES DE COMMANDES FOURNISSEURS                   
#    TABLEC   : NAME=RSGF25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEC /dev/null
#  TABLE DES PRIX MOYENS PONDERES (PRMP DU JOUR)                               
#    TABLED   : NAME=RSGG50L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLED /dev/null
#  TABLE DES RECEPTIONS COMPTABLES                                             
#    TABLEE   : NAME=RSGR30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEE /dev/null
#  TABLE DES LIGNES DE RECEPTIONS COMPTABLE                                    
#    TABLEF   : NAME=RSGR35L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEF /dev/null
       m_FileAssign -d SHR FDARDAC ${DATA}/CORTEX4.P.MTXTFIX1/GR000LBJ
# ******  CARTE SOCIETE                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR030 
       JUMP_LABEL=GR000LBK
       ;;
(GR000LBK)
       m_CondExec 04,GE,GR000LBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FGR030AL POUR CREATION D UNE EDITION                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000LBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GR000LBM
       ;;
(GR000LBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GR000LBJ.FGR030AL
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 SORTOUT ${DATA}/PTEM/GR000LBM.FGR030BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_21_133 21 CH 133
 /FIELDS FLD_BI_1_20 1 CH 20
 /KEYS
   FLD_BI_1_20 ASCENDING
 /* Record Type = F  Record Length = 133 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_21_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GR000LBN
       ;;
(GR000LBN)
       m_CondExec 00,EQ,GR000LBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEBGENER POUR CREATION D UNE EDITION IGR030B                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000LBQ PGM=IEBGENER   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GR000LBQ
       ;;
(GR000LBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SYSUT1 ${DATA}/PTEM/GR000LBM.FGR030BL
       m_OutputAssign -c 9 -w IGR030B SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GR000LBR
       ;;
(GR000LBR)
       m_CondExec 00,EQ,GR000LBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 3 5                                                                 
# ********************************************************************         
#  LISTE DES RECEPTIONS MODIFIEES VALORISEES DU JOUR                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR000LBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GR000LBT
       ;;
(GR000LBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME                                    
#           JJMMSSAA                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#  LISTE DES RECEPTIONS MODIFIEES VALORISEES DU JOUR                           
       m_OutputAssign -c 9 -w BGR035 IGR035
#  TABLE DES ARTICLES                                                          
#    TABLE    : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES LIENS ARTICLES                                                    
#    TABLEA   : NAME=RSGA58L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEA /dev/null
#  TABLE DES LIGNES DE COMMANDES FOURNISSEURS                                  
#    TABLEB   : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEB /dev/null
#  TABLE DES RISTOURNES SUR LIGNES DE COMMANDES FOURNISSEURS                   
#    TABLEC   : NAME=RSGF25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEC /dev/null
#  TABLE DES PRIX MOYENS PONDERES (PRMP DU JOUR)                               
#    TABLED   : NAME=RSGG50L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLED /dev/null
#  TABLE DES RECEPTIONS COMPTABLES                                             
#    TABLEE   : NAME=RSGR30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEE /dev/null
#  TABLE DES LIGNES DE RECEPTIONS COMPTABLE                                    
#    TABLEF   : NAME=RSGR35L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLEF /dev/null
# ******  CARTE SOCIETE                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR035 
       JUMP_LABEL=GR000LBU
       ;;
(GR000LBU)
       m_CondExec 04,GE,GR000LBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GR000LZA
       ;;
(GR000LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GR000LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
