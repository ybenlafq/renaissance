#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GF000D.ksh                       --- VERSION DU 08/10/2016 22:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGF000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/12/17 AT 14.12.12 BY BURTECN                      
#    STANDARDS: P  JOBSET: GF000D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='DPM'                                                               
# ********************************************************************         
#  B G F 0 0 0                                                                 
# ********************************************************************         
#   LISTE DES COMMANDES NON-SOLDEES TRIEES PAR ENTITE DE COMMANDE              
#   INTERLOCUTEUR N DE COMMANDE FAMILLE MARQUE ARTICLE                         
#   TRAITEMENT DU DIMANCHE                                                     
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GF000DA
       ;;
(GF000DA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       GF000DA=${VENDRED}
       RUN=${RUN}
       JUMP_LABEL=GF000DAA
       ;;
(GF000DAA)
       m_CondExec ${EXAAA},NE,YES 1,NE,$[GF000DA] 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  LISTE DES COMMANDES NON-SOLDEES                                      
       m_OutputAssign -c 9 -w BGF000 SYSCONTL
# ******  COMMANDES FOURNISSEUR                                                
#    RSGF10D  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10D /dev/null
# ******  LIGNES COMMANDES FOURNISSEUR                                         
#    RSGF20D  : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20D /dev/null
# ******  LIGNES DE COMMANDES ECHEANCEES                                       
#    RSGF30D  : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30D /dev/null
# ******  LIGNES DE COMMANDES DEMANDEURS                                       
#    RSGF15D  : NAME=RSGF15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF15D /dev/null
# ******  ARTICLES                                                             
#    RSGA00D  : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00D /dev/null
# ******  ENTITES DE COMMANDES                                                 
#    RSGA06D  : NAME=RSGA06D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06D /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10D  : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
# ******* SPECIFIQUE ARTICLE GERES PAR ENTREPOT EXTERIEUR                      
#    RSFL50D  : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50D /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE : 991                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF000 
       JUMP_LABEL=GF000DAB
       ;;
(GF000DAB)
       m_CondExec 04,GE,GF000DAA ${EXAAA},NE,YES 1,NE,$[GF000DA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G F 0 1 5                                                                 
# ********************************************************************         
#  VENTILATION DES NOMBRES D U O PRIS OU A PRENDRE                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF000DAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GF000DAD
       ;;
(GF000DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  LISTE DE LA VENTILATION DES NOMBRES D U O                            
       m_OutputAssign -c 9 -w BGF015 IMPR
# ******  COMMANDES FOURNISSEUR                                                
#    RSGF10D  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10D /dev/null
# ******  LIGNES DE COMMANDES ECHEANCEES                                       
#    RSGF30D  : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30D /dev/null
# ******  PLANNING DE LIVRAISON COMMANDES FOURNISSEURS                         
#    RSGF35D  : NAME=RSGF35D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGF35D /dev/null
# ******  ARTICLES                                                             
#    RSGA00D  : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00D /dev/null
# ******  CHEFS PRODUITS                                                       
#    RSGA02D  : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02D /dev/null
# ******  LIEUX                                                                
#    RSGA10D  : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
# ******  RECEPTIONS                                                           
#    RSGR00D  : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR00D /dev/null
# ******  LIGNES DE RECEPTION ECHEANCEES                                       
#    RSGR10D  : NAME=RSGR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR10D /dev/null
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CODE SOCIETE (991)                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF015 
       JUMP_LABEL=GF000DAE
       ;;
(GF000DAE)
       m_CondExec 04,GE,GF000DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G F 0 1 0                                                                 
# ********************************************************************         
#  CREATION DE L ETAT DESCRIPTIF DE COMMANDE                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF000DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GF000DAG
       ;;
(GF000DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  COMMANDES FOURNISSEUR                                                
#    RSGF10D  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10D /dev/null
# ******  LIGNES COMMANDES FOURNISSEUR DEMANDEURS                              
#    RSGF15D  : NAME=RSGF15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF15D /dev/null
# ******  LIGNES DE COMMANDES ECHEANCEES                                       
#    RSGF30D  : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30D /dev/null
# ******  MVTS DE CREATION ET DE MAJ DE COMMANDES                              
#    RSGF45D  : NAME=RSGF45,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF45D /dev/null
# ******  ARTICLES                                                             
#    RSGA00D  : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00D /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10D  : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CODE SOCIETE (991)                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******  LISTE DE L ETAT DESCRIPTIF DES COMMANDES                             
       m_OutputAssign -c 9 -w BGF010 IMPR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF010 
       JUMP_LABEL=GF000DAH
       ;;
(GF000DAH)
       m_CondExec 04,GE,GF000DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G F 0 2 0                                                                 
# ********************************************************************         
#  CREATION DE L ETAT VALORISATION DE COMMANDE                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF000DAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GF000DAJ
       ;;
(GF000DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  COMMANDES FOURNISSEUR                                                
#    RSGF10D  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10D /dev/null
# ******  LIGNES DE COMMANDES FOURNISSEUR                                      
#    RSGF20D  : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20D /dev/null
# ******  RISTOURNES SUR LES LIGNES DE COMMANDES FOURNISSEUR                   
#    RSGF25D  : NAME=RSGF25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF25D /dev/null
# ******  MVTS DE CREATION ET DE MAJ DE COMMANDES                              
#    RSGF45D  : NAME=RSGF45,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF45D /dev/null
# ******  VALORISATION DES ESCLAVES LIBRE DES COMMANDES FOURNI                 
#    RSGF50D  : NAME=RSGF50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGF50D /dev/null
# ******  ARTICLES                                                             
#    RSGA00D  : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00D /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10D  : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CODE SOCIETE (991)                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******  LISTE DE L ETAT VALORISATION DE COMMANDES                            
       m_OutputAssign -c 9 -w BGF020 IMPR
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF020 
       JUMP_LABEL=GF000DAK
       ;;
(GF000DAK)
       m_CondExec 04,GE,GF000DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
