#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GK321G.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PGGK321 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/05/28 AT 11.02.01 BY BURTECA                      
#    STANDARDS: P  JOBSET: GK321G                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DES FICHIERS ARTICLES BGK220                                            
#  TRI PAR CODIC/SOCIETE                                                       
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GK321GA
       ;;
(GK321GA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GK321GAA
       ;;
(GK321GAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F96.BGK221EB
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BGK221ED
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BGK221EL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BGK221EM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BGK221EO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BGK221EP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BGK221EY
       m_FileAssign -d NEW,CATLG,DELETE -r 1000 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GK321GAA.BGK321AG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_10 1 CH 10
 /KEYS
   FLD_CH_1_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK321GAB
       ;;
(GK321GAB)
       m_CondExec 00,EQ,GK321GAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK321 : FICHIER PRODUITS                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK321GAD PGM=BGK321     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GK321GAD
       ;;
(GK321GAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} FGK221 ${DATA}/PTEM/GK321GAA.BGK321AG
       m_FileAssign -d SHR -g +0 FGK321 ${DATA}/PXX0/F99.BGK321BG
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FGK322 ${DATA}/PXX0/F99.BGK321BG
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FGK32C ${DATA}/PTEM/GK321GAD.BGK321CG
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FGK32T ${DATA}/PTEM/GK321GAD.BGK321DG
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FGK32P ${DATA}/PTEM/GK321GAD.BGK321EG
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FGK32V ${DATA}/PTEM/GK321GAD.BGK321FG
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FGK32A ${DATA}/PTEM/GK321GAD.BGK321GG
       m_ProgramExec BGK321 
# ********************************************************************         
#  BGK331 : FICHIER DE REFERENCE FOURNISSEURS                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK321GAG PGM=BGK331     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GK321GAG
       ;;
(GK321GAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR -g +0 FGK230 ${DATA}/PXX0/F07.BGK230BP
       m_FileAssign -d SHR -g +0 FGK331 ${DATA}/PXX0/F99.BGK331AG
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 160 -t LSEQ -g +1 FGK332 ${DATA}/PXX0/F99.BGK331AG
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FGK33C ${DATA}/PTEM/GK321GAG.BGK331BG
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FGK33F ${DATA}/PTEM/GK321GAG.BGK331CG
       m_ProgramExec BGK331 
# ********************************************************************         
#  BGK340 : MISE EN FORME, FICHIER EN-TETE                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK321GAJ PGM=BGK340     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GK321GAJ
       ;;
(GK321GAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* PREPARATION SYSIN CFT                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDCFT ${DATA}/PXX0/F99.BGKCFTBP
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GK321G2
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} FGK32C ${DATA}/PTEM/GK321GAD.BGK321CG
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/GK321GAG.BGK331BG
       m_FileAssign -d SHR -g ${G_A4} FGK32T ${DATA}/PTEM/GK321GAD.BGK321DG
       m_FileAssign -d SHR -g ${G_A5} FGK32P ${DATA}/PTEM/GK321GAD.BGK321EG
       m_FileAssign -d SHR -g ${G_A6} FGK33F ${DATA}/PTEM/GK321GAG.BGK331CG
       m_FileAssign -d SHR -g ${G_A7} FGK32V ${DATA}/PTEM/GK321GAD.BGK321FG
       m_FileAssign -d SHR -g ${G_A8} FGK32A ${DATA}/PTEM/GK321GAD.BGK321GG
       m_FileAssign -d SHR -g +0 FGK340 ${DATA}/PXX0/F99.BGK341NG
#                                                                              
       m_FileAssign -d SHR FGK34E ${DATA}/PXX0.F99.BGK341AG
       m_FileAssign -d SHR FGK34C ${DATA}/PXX0.F99.BGK341BG
       m_FileAssign -d SHR FGK34T ${DATA}/PXX0.F99.BGK341CG
       m_FileAssign -d SHR FGK34P ${DATA}/PXX0.F99.BGK341DG
       m_FileAssign -d SHR FGK34F ${DATA}/PXX0.F99.BGK341EG
       m_FileAssign -d SHR FGK34V ${DATA}/PXX0.F99.BGK341FG
       m_FileAssign -d SHR FGK34A ${DATA}/PXX0.F99.BGK341GG
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -t LSEQ -g +1 FGK341 ${DATA}/PXX0/F99.BGK341NG
       m_ProgramExec BGK340 
# ********************************************************************         
#   DELETE DU FICHIER ARCHIVE                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK321GAM PGM=IDCAMS     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GK321GAM
       ;;
(GK321GAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR KRISP ${DATA}/PXX0.F99.FKRISP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK321GAM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GK321GAN
       ;;
(GK321GAN)
       m_CondExec 16,NE,GK321GAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK351 : CREATION DU FICHIER ARCHIVE                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK321GAQ PGM=BGK351T    ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GK321GAQ
       ;;
(GK321GAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR FGK34C ${DATA}/PXX0.F99.BGK341BG
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FPKZIP ${DATA}/PXX0/F99.BGK351TG
       m_FileAssign -d SHR FARCH ${DATA}/CORTEX4.P.MTXTFIX1/GK321G3
       m_FileAssign -d SHR FDDDSN ${DATA}/CORTEX4.P.MTXTFIX1/GK321G4
       m_ProgramExec BGK351T 
# ********************************************************************         
#  ZIP DU FICHIER _A ENVOYER                                                    
# ********************************************************************         
# ABE      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSIN    FILE  NAME=BGK351TG,MODE=I                                          
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK321GAT PGM=JVMLDM76   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GK321GAT
       ;;
(GK321GAT)
       m_CondExec ${EXABE},NE,YES 
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 6160 -t LSEQ FICZIP ${DATA}/PXX0.F99.FKRISPW
# *                                                                            
       m_FileAssign -d SHR -g ${G_A9} SYSIN ${DATA}/PXX0/F99.BGK351TG
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  Migration ATOS ENVOI FICHIER VIA GATEWAY                                    
#  REPRISE :                                                                   
# ********************************************************************         
# ABJ      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    FILE  NAME=BGKCFTBP,MODE=I                                          
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY                                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK321GAX PGM=FTP        ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GK321GAX
       ;;
(GK321GAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR -g ${G_A10} SYSIN ${DATA}/PXX0/F99.BGKCFTBP
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GK321GZA
       ;;
(GK321GZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK321GZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
