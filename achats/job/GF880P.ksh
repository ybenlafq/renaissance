#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GF880P.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGF880 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/09/23 AT 11.19.40 BY BURTEC3                      
#    STANDARDS: P  JOBSET: GF880P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TEST SI FICHIER VIDE SI RC=03 FICHIER VIDE                                  
#  TEST SI FICHIER PLEIN SI RC=00 FICHIER PLEIN                                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GF880PA
       ;;
(GF880PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RAAA=${RAAA:-GF880PAA}
       RUN=${RUN}
       JUMP_LABEL=GF880PAA
       ;;
(GF880PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 IN1 ${DATA}/PXX0/FTP.F07.BGF880AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GF880PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GF880PAB
       ;;
(GF880PAB)
       m_CondExec 16,NE,GF880PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORCAGE DU STATUT SOUS PLAN . MISE EN "JOB CANCELED" VOLONTAIRE             
#  SI RC EMIS PAR STEP AAA N EST PAS EGAL _A 03 JE N EXECUTE STEP AAF           
#  SI RC EMIS PAR STEP AAA EST EGAL _A 03 JE N EXECUTE QUE LE STEP AAF          
#  SI RC EMIS PAR STEP AAA EGAL _A 00 JE N EXECUTE STEP AAF MAIS                
#  J EXECUTE TOUS LE RESTE DE LA CHAINE                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF880PAD PGM=CZX2PTRT   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GF880PAG
       ;;
(GF880PAG)
       m_CondExec ${EXAAK},NE,YES 3,EQ,$[RAAA] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g +0 FPRODUI ${DATA}/PXX0/FTP.F07.BGF880AP
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ************** TABLE DES ENCAISSEMENTS                                       
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA31   : NAME=RSGA31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA31 /dev/null
#    RTGA55   : NAME=RSGA55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA55 /dev/null
# ************** FICHIER                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 69 -t LSEQ -g +1 FGF88 ${DATA}/PTEM/GF880PAG.BGF880BP
       m_FileAssign -d NEW,CATLG,DELETE -r 149 -t LSEQ -g +1 FGF89 ${DATA}/PTEM/GF880PAG.BGF880CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF880 
       JUMP_LABEL=GF880PAH
       ;;
(GF880PAH)
       m_CondExec 04,GE,GF880PAG ${EXAAK},NE,YES 3,EQ,$[RAAA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGF88                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF880PAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GF880PAJ
       ;;
(GF880PAJ)
       m_CondExec ${EXAAP},NE,YES 3,EQ,$[RAAA] 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ****** FIC DE LOAD                                                           
       m_FileAssign -d SHR -g ${G_A1} SYSREC ${DATA}/PTEM/GF880PAG.BGF880BP
#    RSGF88   : NAME=RSGF88,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGF88 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GF880PAJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GF880P_GF880PAJ_RTGF88.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GF880PAK
       ;;
(GF880PAK)
       m_CondExec 04,GE,GF880PAJ ${EXAAP},NE,YES 3,EQ,$[RAAA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD RTGF89                                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF880PAM PGM=PTLDRIVM   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GF880PAM
       ;;
(GF880PAM)
       m_CondExec ${EXAAU},NE,YES 3,EQ,$[RAAA] 
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/GF880PAM.UNGF89AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GF880PAM.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   LOAD DE LA TABLE RTGF89                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF880PAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GF880PAQ
       ;;
(GF880PAQ)
       m_CondExec ${EXAAZ},NE,YES 3,EQ,$[RAAA] 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ****** FIC DE LOAD                                                           
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PTEM/GF880PAM.UNGF89AP
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/GF880PAG.BGF880CP
#    RSGF89   : NAME=RSGF89,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGF89 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GF880PAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GF880P_GF880PAQ_RTGF89.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GF880PAR
       ;;
(GF880PAR)
       m_CondExec 04,GE,GF880PAQ ${EXAAZ},NE,YES 3,EQ,$[RAAA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GF880PZA
       ;;
(GF880PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GF880PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
