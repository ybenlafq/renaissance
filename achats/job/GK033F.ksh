#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GK033F.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFGK033 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/06/19 AT 16.57.45 BY BURTECA                      
#    STANDARDS: P  JOBSET: GK033F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  CUMUL FICHIERS VENTES LIVREES ISSUS DE GK002 FILIALES                       
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GK033FA
       ;;
(GK033FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GK033FAA
       ;;
(GK033FAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F96.BGK030BB
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BGK030BD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BGK030BL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BGK030BM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BGK030BO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BGK030BP
       m_FileAssign -d SHR -g +0 -C ${DATA}/P908/SEM.BGK030BX
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BGK030BY
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GK033FAA.FGK030AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_4_7 4 CH 7
 /KEYS
   FLD_BI_4_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK033FAB
       ;;
(GK033FAB)
       m_CondExec 00,EQ,GK033FAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CUMUL FICHIERS PRODUITS ISSUS DE GK001 FILIALES                             
#  REPRISE: OUI   FAC BGK020AB MIS EN DERNIER                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK033FAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GK033FAD
       ;;
(GK033FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BGK020AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BGK020AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BGK020AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BGK020AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BGK020AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/P908/SEM.BGK020AX
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BGK020AY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F96.BGK020AB
       m_FileAssign -d NEW,CATLG,DELETE -r 113 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GK033FAD.FGK020AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_51_7 51 CH 7
 /KEYS
   FLD_BI_51_7 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK033FAE
       ;;
(GK033FAE)
       m_CondExec 00,EQ,GK033FAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CUMUL FICHIERS STOCKS RTGS30 DE GK022  FILIALES                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK033FAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GK033FAG
       ;;
(GK033FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F96.GS30UN0B
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.GS30UN0D
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.GS30UN0L
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.GS30UN0M
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.GS30UN0O
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.GS30UN0P
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F08.GS30UN0X
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.GS30UN0Y
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GK033FAG.GS30UN0F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK033FAH
       ;;
(GK033FAH)
       m_CondExec 00,EQ,GK033FAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CUMUL FICHIERS STOCKS RTGS31 DE GK022  FILIALES                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK033FAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GK033FAJ
       ;;
(GK033FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F96.GS31UN0B
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.GS31UN0D
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.GS31UN0L
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.GS31UN0M
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.GS31UN0O
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.GS31UN0P
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F08.GS31UN0X
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.GS31UN0Y
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GK033FAJ.GS31UN0F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK033FAK
       ;;
(GK033FAK)
       m_CondExec 00,EQ,GK033FAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FUSION DES FICHIERS STOCKS RTGS30 RTGS31                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK033FAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GK033FAM
       ;;
(GK033FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GK033FAG.GS30UN0F
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/GK033FAJ.GS31UN0F
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GK033FAM.GS30AF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK033FAN
       ;;
(GK033FAN)
       m_CondExec 00,EQ,GK033FAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FASTUNLOAD DE RTGS10                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK033FAQ PGM=PTLDRIVM   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GK033FAQ
       ;;
(GK033FAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/GK033FAQ.GS10UN0F
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK033FAQ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI FICHIERS STOCKS RTGS10                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK033FAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GK033FAT
       ;;
(GK033FAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GK033FAQ.GS10UN0F
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GK033FAT.RTGS10F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK033FAU
       ;;
(GK033FAU)
       m_CondExec 00,EQ,GK033FAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK131 : CREATION FIC STAT VENTES FILIALES (CONSO)                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK033FAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GK033FAX
       ;;
(GK033FAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  DATE  JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  SOUS TABLE TVA                                                       
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  ZONE DE PRIX                                                         
#    RSGA59   : NAME=RSGA59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA59 /dev/null
# ******  PRMP                                                                 
#    RSGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG50 /dev/null
#                                                                              
# ******  FIC   STOCKS ENTREPOT                                                
       m_FileAssign -d SHR -g ${G_A4} RTGS10 ${DATA}/PTEM/GK033FAT.RTGS10F
# ******  FIC   STOCKS MAGS                                                    
       m_FileAssign -d SHR -g ${G_A5} RTGS30 ${DATA}/PTEM/GK033FAM.GS30AF
# ******  FIC   CUMUL VENTES LIVREES FILIALES                                  
       m_FileAssign -d SHR -g ${G_A6} FGK030 ${DATA}/PTEM/GK033FAA.FGK030AP
# ******  FIC   CUMUL PRODUITS FILIALES                                        
       m_FileAssign -d SHR -g ${G_A7} FGK020 ${DATA}/PTEM/GK033FAD.FGK020AP
#                                                                              
# ******* FIC   DE STAT VENTES LIVREES DESTINE A KESA                          
       m_FileAssign -d NEW,CATLG,DELETE -r 112 -t LSEQ -g +1 FGK131 ${DATA}/PXX0/F99.BGK131AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGK131 
       JUMP_LABEL=GK033FAY
       ;;
(GK033FAY)
       m_CondExec 04,GE,GK033FAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CUMUL FICHIERS PRA ISSUS DE GK002 FILIALES                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK033FBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GK033FBA
       ;;
(GK033FBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F96.BGK045BB
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BGK045BD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BGK045BL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BGK045BM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BGK045BO
       m_FileAssign -d SHR -g +0 -C ${DATA}/P908/SEM.BGK045BX
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BGK045BY
       m_FileAssign -d NEW,CATLG,DELETE -r 13 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GK003FAJ.BGK045BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_7 1 CH 7
 /KEYS
   FLD_BI_1_7 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK033FBB
       ;;
(GK033FBB)
       m_CondExec 00,EQ,GK033FBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI   FICHIERS ACHATS ISSUS DE GK002 IDF                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAU      SORT                                                                
# *                                                                            
# SORTIN   FILE  NAME=BGK040AP,MODE=I                                          
# SORTOUT  FILE  NAME=BGK140AP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(9,7,BI,A,4,5,BI,A,16,6,BI,A,1,3,BI,A)                          
#          DATAEND                                                             
# ********************************************************************         
#  BGK140 : CREATION FIC STAT ACHATS FILIALES (CONSO)                          
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAZ      STEP  PGM=IKJEFT01                                                  
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *                                                                            
# ******** HISTO PRA                                                           
# RSGG70   FILE  NAME=RSGG70,MODE=I,DYNAM=YES                                  
# *                                                                            
# *******  DATE  JJMMSSAA                                                      
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# *******  FIC   CUMUL ACHATS FILIALES                                         
# FGK040   FILE  NAME=BGK140AP,MODE=I                                          
# *******  FIC   CUMUL PRODUITS FILIALES                                       
# FGK020   FILE  NAME=BGK130BP,MODE=I                                          
# *******  FIC   CUMUL PRA FILIALES                                            
# FGK045   FILE  NAME=BGK045BP,MODE=I                                          
# *                                                                            
# ******** FIC   DE STAT VENTES LIVREES DESTINE A KESA                         
# FGK140   FILE  NAME=BGK140BP,MODE=O                                          
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BGK140) PLAN(BGK140)                                            
#  END                                                                         
#          DATAEND                                                             
# *                                                                            
#                                                                              
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GK033FZA
       ;;
(GK033FZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK033FZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
