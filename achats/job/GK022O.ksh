#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GK022O.ksh                       --- VERSION DU 08/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGK022 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/02/16 AT 10.37.10 BY BURTEC5                      
#    STANDARDS: P  JOBSET: GK022O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  TRI DU FIC HISTO DES VENTES                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAA      SORT                                                                
#                                                                              
# PRED     LINK  NAME=$EX010O,MODE=I                                           
# PRED     LINK  NAME=$STAT0O,MODE=I                                           
# PRED     LINK  NAME=$PHV00O,MODE=I                                           
# PRED     LINK  NAME=$GK002O,MODE=I                                           
# *                                                                            
# SORTIN   FILE  NAME=HV01AO,MODE=I                                            
# SORTOUT  FILE  NAME=BGK030FO,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(1,10,A,14,6,A),FORMAT=CH                                       
#          DATAEND                                                             
# ********************************************************************         
#  BGK030F: CREATION DU FICHIER STATS DE VENTES LIVREES A PARTIR DE HV         
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAF      STEP  PGM=IKJEFT01                                                  
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *                                                                            
# ******** PRMP                                                                
# RSGG50D  FILE  DYNAM=YES,NAME=RSGG50O,MODE=I                                 
# ******** ZONE DE PRIX                                                        
# RSGA59D  FILE  DYNAM=YES,NAME=RSGA59O,MODE=I                                 
# *                                                                            
# ******** DATE JJMMSSAA                                                       
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# ******** DATE MMSSAA                                                         
# FMOIS    DATA  CLASS=VAR,PARMS=FMOISP,MBR=FMOISP                             
# ******** SOCIETE                                                             
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDIF                                         
# *                                                                            
# ******** FIC HISTO STAT DE VENTE LIVREE                                      
# RTHV01   FILE  NAME=BGK030FO,MODE=I                                          
# ******** FIC STAT DE VENTE LIVREE                                            
# FGK030   FILE  NAME=BGK030BO,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BGK030F) PLAN(BGK030FO)                                         
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  BGK045 : CREATION DU FICHIER DES PRA                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAK      STEP  PGM=IKJEFT01                                                  
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *                                                                            
# ******** PRA                                                                 
# RSGG70D  FILE  DYNAM=YES,NAME=RSGG70O,MODE=I                                 
# *                                                                            
# ******** DATE MMSSAA                                                         
# FMOIS    DATA  CLASS=VAR,PARMS=FMOISP,MBR=FMOISP                             
# *                                                                            
# ******** FIC STAT DE VENTE LIVREE                                            
# FGK045   FILE  NAME=BGK045BO,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BGK045) PLAN(BGK045O)                                           
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  BGK060 : GENERATION DE SYSIN POUR FASTUNLOAD DES DONNEES PRODUIT            
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GK022OA
       ;;
(GK022OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GK022OAA
       ;;
(GK022OAA)
       m_CondExec ${EXAAA},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* DATE  JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DATE  MMSSAA                                                         
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GK022O1
#                                                                              
       m_FileAssign -d SHR FSYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK022OAA.sysin
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FGK060 ${DATA}/PTEM/GK022OAA.BGK060AO
       m_ProgramExec BGK060 
# ********************************************************************         
#  FASTUNLOAD DES TABLES RTGS30, RTGS31                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK022OAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GK022OAD
       ;;
(GK022OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
#                                                                              
#    RSGS30   : NAME=RSGS30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
#    RSGS31   : NAME=RSGS31O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS31 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SYSREC01 ${DATA}/PXX0/F16.GS30UN0O
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SYSREC02 ${DATA}/PXX0/F16.GS31UN0O
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} SYSIN ${DATA}/PTEM/GK022OAA.BGK060AO
       
       #Untranslated utility: PTLDRIVM##Untranslate PTLDRIVM SYSIN

#                                                                              
#                                                                              
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GK022OZA
       ;;
(GK022OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK022OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
