#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GK022P.ksh                       --- VERSION DU 08/10/2016 22:18
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGK022 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/02/16 AT 10.37.44 BY BURTEC5                      
#    STANDARDS: P  JOBSET: GK022P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  TRI DU FIC HISTO DES VENTES                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAA      SORT                                                                
#                                                                              
# PRED     LINK  NAME=$EX010P,MODE=I                                           
# PRED     LINK  NAME=$STAT0P,MODE=I                                           
# PRED     LINK  NAME=$PHV00P,MODE=I                                           
# PRED     LINK  NAME=$GK002P,MODE=I                                           
# *                                                                            
# SORTIN   FILE  NAME=HV01AP,MODE=I                                            
# SORTOUT  FILE  NAME=BGK030FP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(1,10,A,14,6,A),FORMAT=CH                                       
#          DATAEND                                                             
# ********************************************************************         
#  BGK030F: CREATION DU FICHIER STATS DE VENTES LIVREES A PARTIR DE HV         
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAF      STEP  PGM=IKJEFT01                                                  
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *                                                                            
# ******** PRMP                                                                
# RSGG50D  FILE  DYNAM=YES,NAME=RSGG50,MODE=I                                  
# ******** ZONE DE PRIX                                                        
# RSGA59D  FILE  DYNAM=YES,NAME=RSGA59,MODE=I                                  
# *                                                                            
# ******** DATE JJMMSSAA                                                       
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# ******** DATE MMSSAA                                                         
# FMOIS    DATA  CLASS=VAR,PARMS=FMOISP,MBR=FMOISP                             
# ******** SOCIETE                                                             
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDIF                                         
# *                                                                            
# ******** FIC HISTO STAT DE VENTE LIVREE                                      
# RTHV01   FILE  NAME=BGK030FP,MODE=I                                          
# ******** FIC STAT DE VENTE LIVREE                                            
# FGK030   FILE  NAME=BGK030BP,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BGK030F) PLAN(BGK030F)                                          
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  BGK030P: CREATION DU FICHIER STATS DE VENTES LIVREES TYPE MGI               
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAK      STEP  PGM=IKJEFT01                                                  
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *                                                                            
# ******** PRMP                                                                
# RSGG50   FILE  DYNAM=YES,NAME=RSGG50,MODE=I                                  
# ******** ZONE DE PRIX                                                        
# RSGA59   FILE  DYNAM=YES,NAME=RSGA59,MODE=I                                  
# ******** HISTO VENTE                                                         
# RSGV51   FILE  DYNAM=YES,NAME=RSGV51,MODE=I                                  
# ******** HISTO VENTE                                                         
# RSGV52   FILE  DYNAM=YES,NAME=RSGV52,MODE=I                                  
# ******** HISTO VENTE                                                         
# RSGV53   FILE  DYNAM=YES,NAME=RSGV53,MODE=I                                  
# ******** HISTO VENTE                                                         
# RSGV54   FILE  DYNAM=YES,NAME=RSGV54,MODE=I                                  
# ******** HISTO VENTE                                                         
# RSGV55   FILE  DYNAM=YES,NAME=RSGV55,MODE=I                                  
# ******** HISTO VENTE                                                         
# RSGV56   FILE  DYNAM=YES,NAME=RSGV56,MODE=I                                  
# *                                                                            
# ******** DATE JJMMSSAA                                                       
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# ******** DATE MMSSAA                                                         
# FMOIS    DATA  CLASS=VAR,PARMS=FMOISP,MBR=FMOISP                             
# ******** SOCIETE                                                             
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDIF                                         
# *                                                                            
# ******** FIC STAT DE VENTE LIVREE                                            
# FGK030   FILE  NAME=BGK030CP,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BGK030P) PLAN(BGK030P)                                          
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  CUMUL FGK030 DE  BGK030F ET BGK030P                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAP      SORT                                                                
# SORTIN   FILE  NAME=BGK030BP,MODE=I                                          
#          FILE  NAME=BGK030CP,MODE=I                                          
# SORTOUT  FILE  NAME=BGK030BP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  MERGE FIELDS=COPY                                                           
#          DATAEND                                                             
# ********************************************************************         
#  BGK040 : CREATION DU FICHIER STATS ACHAT                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAU      STEP  PGM=IKJEFT01                                                  
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *                                                                            
# ******** PRMP                                                                
# RSGG50   FILE  DYNAM=YES,NAME=RSGG50,MODE=I                                  
# ******** HISTO VENTES SOC/CODIC/MOIS                                         
# RSHV70   FILE  DYNAM=YES,NAME=RSHV70,MODE=I                                  
# *                                                                            
# ******** DATE JJMMSSAA                                                       
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# ******** DATE MMSSAA                                                         
# FMOIS    DATA  CLASS=VAR,PARMS=FMOISP,MBR=FMOISP                             
# *                                                                            
# ******** FIC STAT ACHAT                                                      
# FGK040   FILE  NAME=BGK040AP,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BGK040) PLAN(BGK040)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  BGK050 : CREATION DU FICHIER ENTITE DE CMD FOURNISSEUR                      
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAZ      STEP  PGM=IKJEFT01                                                  
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *                                                                            
# ******** ENTITE DE CMD FOURNISSEUR                                           
# RSGA06   FILE  DYNAM=YES,NAME=RSGA06,MODE=I                                  
# ******** ARTICLE / FOURNISSEUR                                               
# RSGA55   FILE  DYNAM=YES,NAME=RSGA55,MODE=I                                  
# *                                                                            
# ******** DATE JJMMSSAA                                                       
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# *                                                                            
# ******** FIC ENTITE CMD FOURNISSEUR                                          
# FGK050   FILE  NAME=BGK050AP,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BGK050) PLAN(BGK050)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  BGK060 : GENERATION DE SYSIN POUR FASTUNLOAD DES DONNEES PRODUIT            
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GK022PA
       ;;
(GK022PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GK022PAA
       ;;
(GK022PAA)
       m_CondExec ${EXAAA},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* DATE  JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DATE  MMSSAA                                                         
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GK022P1
#                                                                              
       m_FileAssign -d SHR FSYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK022PAA.sysin
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FGK060 ${DATA}/PTEM/GK022PAA.BGK060AP
       m_ProgramExec BGK060 
# ********************************************************************         
#  FASTUNLOAD DES TABLES RTGS30, RTGS31                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK022PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GK022PAD
       ;;
(GK022PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
#                                                                              
#    RSGS30   : NAME=RSGS30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS30 /dev/null
#    RSGS31   : NAME=RSGS31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS31 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SYSREC01 ${DATA}/PXX0/F07.GS30UN0P
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SYSREC02 ${DATA}/PXX0/F07.GS31UN0P
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} SYSIN ${DATA}/PTEM/GK022PAA.BGK060AP
       
       #Untranslated utility: PTLDRIVM##Untranslate PTLDRIVM SYSIN

#                                                                              
#                                                                              
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GK022PZA
       ;;
(GK022PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK022PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
