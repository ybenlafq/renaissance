#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GK201D.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGK201 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/04/22 AT 09.14.07 BY PREPA2                       
#    STANDARDS: P  JOBSET: GK201D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BGK200 : GENERATION DU FICHIER PRODUITS A TRANSMETTRE                       
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GK201DA
       ;;
(GK201DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GK201DAA
       ;;
(GK201DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
       m_FileAssign -d SHR FETAT ${DATA}/CORTEX4.P.MTXTFIX1/KRISP01
#                                                                              
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA09   : NAME=RSGA09D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09 /dev/null
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA11   : NAME=RSGA11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11 /dev/null
#    RSGA12   : NAME=RSGA12D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA14   : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA22   : NAME=RSGA22D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22 /dev/null
#    RSGA29   : NAME=RSGA29D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA29 /dev/null
#    RSGA64   : NAME=RSGA64D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA64 /dev/null
#    RSGA83   : NAME=RSGA83D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA83 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 FGK200 ${DATA}/PTEM/GK201DAA.BGK201AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGK200 
       JUMP_LABEL=GK201DAB
       ;;
(GK201DAB)
       m_CondExec 04,GE,GK201DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PAR CODIC                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK201DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GK201DAD
       ;;
(GK201DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GK201DAA.BGK201AD
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 SORTOUT ${DATA}/PTEM/GK201DAD.BGK201BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK201DAE
       ;;
(GK201DAE)
       m_CondExec 00,EQ,GK201DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK211 : GENERATION DE SYSIN POUR FASTUNLOAD DES DONNEES PRODUIT            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK201DAG PGM=BGK211     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GK201DAG
       ;;
(GK201DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GK201D
#                                                                              
       m_FileAssign -d SHR FSYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK201DAG.sysin
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FGK211 ${DATA}/PTEM/GK201DAG.BGK211AD
       m_ProgramExec BGK211 
# ********************************************************************         
#  FASTUNLOAD DES TABLES RTGS10, RTGS30, RTGG31, RTGS40,                       
#                        RTGS42 ET RTGG70                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK201DAJ PGM=PTLDRIVM   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GK201DAJ
       ;;
(GK201DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
#                                                                              
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGS30   : NAME=RSGS30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
#    RSGS31   : NAME=RSGS31D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS31 /dev/null
#    RSGG70   : NAME=RSGG70D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70 /dev/null
#    RSHV02   : NAME=RSHV02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV02 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 SYSREC01 ${DATA}/PTEM/GK200DAJ.GG70UNAD
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SYSREC02 ${DATA}/PTEM/GK200DAJ.GS10UNAD
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SYSREC03 ${DATA}/PTEM/GK200DAJ.GS30UNAD
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SYSREC04 ${DATA}/PTEM/GK200DAJ.GS31UNAD
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -g +1 SYSREC05 ${DATA}/PTEM/GK201DAJ.HV02UNAD
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} SYSIN ${DATA}/PTEM/GK201DAG.BGK211AD
       
       #Untranslated utility: PTLDRIVM##Untranslate PTLDRIVM SYSIN

# ********************************************************************         
#  TRI PAR CODIC DES DONNEES DE LA TABLE RTHV02                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK201DAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GK201DAM
       ;;
(GK201DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GK201DAJ.HV02UNAD
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -g +1 SORTOUT ${DATA}/PTEM/GK201DAM.BGK221AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK201DAN
       ;;
(GK201DAN)
       m_CondExec 00,EQ,GK201DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PAR CODIC/DATE DES DONNEES DE LA TABLE RTGG70                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK201DAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GK201DAQ
       ;;
(GK201DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GK200DAJ.GG70UNAD
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 SORTOUT ${DATA}/PTEM/GK201DAQ.BGK221BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_8_8 8 CH 8
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_8 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK201DAR
       ;;
(GK201DAR)
       m_CondExec 00,EQ,GK201DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PAR CODIC DES DONNEES DE LA TABLE RTGS10                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK201DAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GK201DAT
       ;;
(GK201DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GK200DAJ.GS10UNAD
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SORTOUT ${DATA}/PTEM/GK201DAT.BGK221CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK201DAU
       ;;
(GK201DAU)
       m_CondExec 00,EQ,GK201DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FUSION DES DONNEES DES TABLES RTGS30 ET RTGS31                              
#  TRI PAR CODIC                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK201DAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GK201DAX
       ;;
(GK201DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/GK200DAJ.GS30UNAD
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SORTOUT ${DATA}/PTEM/GK201DAX.BGK221DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GK201DAY
       ;;
(GK201DAY)
       m_CondExec 00,EQ,GK201DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK221 : MISE EN FORME DES DONNEES PRODUITS                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK201DBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GK201DBA
       ;;
(GK201DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
#    RSGA59   : NAME=RSGA59D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
#    RSGF10   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10 /dev/null
#    RSGF30   : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30 /dev/null
#    RSGG50   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#                                                                              
       m_FileAssign -d SHR -g ${G_A7} FGK200 ${DATA}/PTEM/GK201DAD.BGK201BD
       m_FileAssign -d SHR -g ${G_A8} RTHV02 ${DATA}/PTEM/GK201DAM.BGK221AD
       m_FileAssign -d SHR -g ${G_A9} RTGG70 ${DATA}/PTEM/GK201DAQ.BGK221BD
       m_FileAssign -d SHR -g ${G_A10} RTGS10 ${DATA}/PTEM/GK201DAT.BGK221CD
       m_FileAssign -d SHR -g ${G_A11} RTGS30 ${DATA}/PTEM/GK201DAX.BGK221DD
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 1000 -g +1 FGK221 ${DATA}/PXX0/F91.BGK221ED
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGK221 
       JUMP_LABEL=GK201DBB
       ;;
(GK201DBB)
       m_CondExec 04,GE,GK201DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GK201DZA
       ;;
(GK201DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK201DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
