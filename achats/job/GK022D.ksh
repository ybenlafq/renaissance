#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GK022D.ksh                       --- VERSION DU 08/10/2016 17:23
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGK022 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/02/16 AT 10.35.40 BY BURTEC5                      
#    STANDARDS: P  JOBSET: GK022D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  TRI DU FIC HISTO DES VENTES                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAA      SORT                                                                
#                                                                              
# PRED     LINK  NAME=$EX010D,MODE=I                                           
# PRED     LINK  NAME=$STAT0D,MODE=I                                           
# PRED     LINK  NAME=$PHV00D,MODE=I                                           
# PRED     LINK  NAME=$GK002D,MODE=I                                           
# *                                                                            
# SORTIN   FILE  NAME=HV01AD,MODE=I                                            
# SORTOUT  FILE  NAME=BGK030FD,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(1,10,A,14,6,A),FORMAT=CH                                       
#          DATAEND                                                             
# ********************************************************************         
#  BGK030F: CREATION DU FICHIER STATS DE VENTES LIVREES A PARTIR DE HV         
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAF      STEP  PGM=IKJEFT01                                                  
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *                                                                            
# ******** PRMP                                                                
# RSGG50D  FILE  DYNAM=YES,NAME=RSGG50D,MODE=I                                 
# ******** ZONE DE PRIX                                                        
# RSGA59D  FILE  DYNAM=YES,NAME=RSGA59D,MODE=I                                 
# *                                                                            
# ******** DATE JJMMSSAA                                                       
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# ******** DATE MMSSAA                                                         
# FMOIS    DATA  CLASS=VAR,PARMS=FMOISP,MBR=FMOISP                             
# ******** SOCIETE                                                             
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDPM                                         
# *                                                                            
# ******** FIC HISTO STAT DE VENTE LIVREE                                      
# RTHV01   FILE  NAME=BGK030FD,MODE=I                                          
# ******** FIC STAT DE VENTE LIVREE                                            
# FGK030   FILE  NAME=BGK030BD,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BGK030F) PLAN(BGK030FD)                                         
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  BGK045 : CREATION DU FICHIER DES PRA                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAK      STEP  PGM=IKJEFT01                                                  
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *                                                                            
# ******** PRA                                                                 
# RSGG70D  FILE  DYNAM=YES,NAME=RSGG70D,MODE=I                                 
# *                                                                            
# ******** DATE MMSSAA                                                         
# FMOIS    DATA  CLASS=VAR,PARMS=FMOISP,MBR=FMOISP                             
# *                                                                            
# ******** FIC STAT DE VENTE LIVREE                                            
# FGK045   FILE  NAME=BGK045BD,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BGK045) PLAN(BGK045D)                                           
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  BGK060 : GENERATION DE SYSIN POUR FASTUNLOAD DES DONNEES PRODUIT            
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GK022DA
       ;;
(GK022DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GK022DAA
       ;;
(GK022DAA)
       m_CondExec ${EXAAA},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* DATE  JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DATE  MMSSAA                                                         
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GK022D1
#                                                                              
       m_FileAssign -d SHR FSYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK022DAA.sysin
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FGK060 ${DATA}/PTEM/GK022DAA.BGK060AD
       m_ProgramExec BGK060 
# ********************************************************************         
#  FASTUNLOAD DES TABLES RTGS30, RTGS31                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK022DAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GK022DAD
       ;;
(GK022DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
#                                                                              
#    RSGS30   : NAME=RSGS30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
#    RSGS31   : NAME=RSGS31D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS31 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SYSREC01 ${DATA}/PXX0/F91.GS30UN0D
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SYSREC02 ${DATA}/PXX0/F91.GS31UN0D
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} SYSIN ${DATA}/PTEM/GK022DAA.BGK060AD
       
       #Untranslated utility: PTLDRIVM##Untranslate PTLDRIVM SYSIN

#                                                                              
#                                                                              
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GK022DZA
       ;;
(GK022DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK022DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
