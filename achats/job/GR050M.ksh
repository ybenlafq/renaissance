#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GR050M.ksh                       --- VERSION DU 09/10/2016 05:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGR050 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/03/29 AT 15.16.43 BY BURTECA                      
#    STANDARDS: P  JOBSET: GR050M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  B G R 0 5 0                                                                 
# ********************************************************************         
#  EXTRACTION PERMETTANT L'EDITION DES BONS DE RECEPTION VALORISEES            
#  DEPUIS LE DEBUT DU MOIS                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GR050MA
       ;;
(GR050MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GR050MAA
       ;;
(GR050MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    POUR AVOIR LE PRMP A JOUR(RTGG50) *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME 2 JJMMSSAA                         
#        DATE DE FIN DE SELECTION                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/DAGR050M
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES PRMP                                                              
#    RSGG50M  : NAME=RSGG50M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGG50M /dev/null
#  TABLE DES NUMEROS DE RECEPT                                                 
#    RSGG70M  : NAME=RSGG70M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70M /dev/null
#  TABLE DES RECEPTIONS COMPTABLES (GENERALITES)                               
#    RSGR30M  : NAME=RSGR30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGR30M /dev/null
#  TABLE DES RECEPTIONS COMPTABLES (DETAIL)                                    
#    RSGR35M  : NAME=RSGR35M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGR35M /dev/null
#  TABLE DES CDES FOURNISSEURS                                                 
#    RSGF10M  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10M /dev/null
#  TABLE DES LIGNES DE CDES FOURNISSEURS                                       
#    RSGF20M  : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20M /dev/null
#  TABLE DES FAMILLES                                                          
#    RSGA14M  : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14M /dev/null
#  TABLE DES ARTICLES                                                          
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
#  TABLE DES ANOMALIES                                                         
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES                                
# -X-RSAN00M  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSAN00M /dev/null
#  FICHIER D'EXTRACTION                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -t LSEQ -g +1 FGR050 ${DATA}/PTEM/GR050MAA.FGR050AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR050 
       JUMP_LABEL=GR050MAB
       ;;
(GR050MAB)
       m_CondExec 04,GE,GR050MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  S O R T                                                                     
# ********************************************************************         
#   TRI DU FICHIER DE RECEPTIONS VALORISEES POUR PGM BGR052                    
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR050MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GR050MAD
       ;;
(GR050MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM DU JOUR                                          
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GR050MAA.FGR050AM
#  FICHIER DES RECEPTIONS ADM DU JOUR TRIE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GR050MAD.FGR052AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_27_24 27 CH 24
 /FIELDS FLD_CH_1_11 1 CH 11
 /FIELDS FLD_CH_15_12 15 CH 12
 /FIELDS FLD_CH_12_3 12 CH 3
 /KEYS
   FLD_CH_1_11 ASCENDING,
   FLD_CH_27_24 ASCENDING,
   FLD_CH_12_3 ASCENDING,
   FLD_CH_15_12 ASCENDING
 /* Record Type = F  Record Length = 127 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR050MAE
       ;;
(GR050MAE)
       m_CondExec 00,EQ,GR050MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 5 2                                                                 
# ********************************************************************         
#  EDITION DES BONS DE RECEPTION VALORISEES                                    
#  ETAT IGR052                                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR050MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GR050MAG
       ;;
(GR050MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME 2 JJMMSSAA                         
#        DATE DE FIN DE SELECTION                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/DAGR050M
#                                                                              
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#  TABLE DES CHEFS PRODUIT                                                     
#    RSGA02M  : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02M /dev/null
#  TABLE DES ASSOCIATIONS ENTITES DE CDE INTERLOC COMMERCIALE                  
#    RSGA08M  : NAME=RSGA08M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA08M /dev/null
#  TABLE DES ENTITES DE CDES                                                   
#    RSGA06M  : NAME=RSGA06M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06M /dev/null
#  TABLE DES ANOMALIES                                                         
#    RSAN00M  : NAME=RSAN00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
#  FICHIER D'EXTRACTION TRIE                                                   
       m_FileAssign -d SHR -g ${G_A2} FGR050 ${DATA}/PTEM/GR050MAD.FGR052AM
#  ETAT DES RECEPTIONS VALORISEES                                              
       m_OutputAssign -c 9 -w IGR052 IGR052
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR052 
       JUMP_LABEL=GR050MAH
       ;;
(GR050MAH)
       m_CondExec 04,GE,GR050MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GR050MZA
       ;;
(GR050MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GR050MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
