#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GR050Y.ksh                       --- VERSION DU 17/10/2016 18:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGR050 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/12/16 AT 11.50.20 BY BURTECN                      
#    STANDARDS: P  JOBSET: GR050Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#                                                                              
# ********************************************************************         
#  B G R 0 5 0                                                                 
# ********************************************************************         
#  EXTRACTION PERMETTANT L'EDITION DES BONS DE RECEPTION VALORISEES            
#  DEPUIS LE DEBUT DU MOIS                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GR050YA
       ;;
(GR050YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GR050YAA
       ;;
(GR050YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    POUR AVOIR LE PRMP A JOUR(RTGG50) *                                       
#  POUR AVOIR TOUTES LES RECEPTIONS                                            
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME 2 JJMMSSAA                         
#        DATE DE FIN DE SELECTION                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/DAGR050Y
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES PRMP                                                              
#    RSGG50   : NAME=RSGG50Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#  TABLE DES NUMEROS DE RECEPT                                                 
#    RSGG70   : NAME=RSGG70Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70 /dev/null
#  TABLE DES RECEPTIONS COMPTABLES (GENERALITES)                               
#    RSGR30   : NAME=RSGR30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGR30 /dev/null
#  TABLE DES RECEPTIONS COMPTABLES (DETAIL)                                    
#    RSGR35   : NAME=RSGF35Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGR35 /dev/null
#  TABLE DES CDES FOURNISSEURS                                                 
#    RSGF10   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10 /dev/null
#  TABLE DES LIGNES DE CDES FOURNISSEURS                                       
#    RSGF20   : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20 /dev/null
#  TABLE DES FAMILLES                                                          
#    RSGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#  TABLE DES ARTICLES                                                          
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#  TABLE DES ANOMALIES                                                         
#    RSAN00   : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
# -X-RSAN00Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSAN00 /dev/null
#  FICHIER D'EXTRACTION : NEW LRECL 127                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -g +1 FGR050 ${DATA}/PTEM/GR050YAA.FGR050AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR050 
       JUMP_LABEL=GR050YAB
       ;;
(GR050YAB)
       m_CondExec 04,GE,GR050YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  S O R T                                                                     
# ********************************************************************         
#   TRI DU FICHIER DE RECEPTIONS VALORISEES POUR PGM BGR052                    
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR050YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GR050YAD
       ;;
(GR050YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM DU JOUR                                          
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GR050YAA.FGR050AY
#  FICHIER DES RECEPTIONS ADM DU JOUR TRIE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -g +1 SORTOUT ${DATA}/PTEM/GR050YAD.FGR052AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_15_12 15 CH 12
 /FIELDS FLD_CH_27_24 27 CH 24
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_1_11 1 CH 11
 /KEYS
   FLD_CH_1_11 ASCENDING,
   FLD_CH_27_24 ASCENDING,
   FLD_CH_12_3 ASCENDING,
   FLD_CH_15_12 ASCENDING
 /* Record Type = F  Record Length = 127 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR050YAE
       ;;
(GR050YAE)
       m_CondExec 00,EQ,GR050YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  S O R T                                                                     
# ********************************************************************         
#   TRI DU FICHIER DE RECEPTIONS VALORISEES POUR PGM BGR051                    
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR050YAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GR050YAG
       ;;
(GR050YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES RECEPTIONS ADM DU JOUR                                          
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GR050YAA.FGR050AY
#  FICHIER DES RECEPTIONS ADM DU JOUR TRIE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -g +1 SORTOUT ${DATA}/PTEM/GR050YAG.FGR051AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_15_36 15 CH 36
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_1_11 1 CH 11
 /KEYS
   FLD_CH_1_11 ASCENDING,
   FLD_CH_12_3 ASCENDING,
   FLD_CH_15_36 ASCENDING
 /* Record Type = F  Record Length = 127 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR050YAH
       ;;
(GR050YAH)
       m_CondExec 00,EQ,GR050YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 5 1                                                                 
# ********************************************************************         
#  EDITION DES BONS DE RECEPTION VALORISEES                                    
#  ETAT IGR051                                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR050YAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GR050YAJ
       ;;
(GR050YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME 2 JJMMSSAA                         
#        DATE DE FIN DE SELECTION                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/DAGR050Y
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  SOCIETE                                                                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/GR050YAJ
#  TABLE DES CHEFS PRODUIT                                                     
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
#  TABLE DES ASSOCIATIONS ENTITES DE CDE INTERLOC COMMERCIALE                  
#    RSGA08   : NAME=RSGA08Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA08 /dev/null
#  TABLE DES ENTITES DE CDES                                                   
#    RSGA06   : NAME=RSGA06Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06 /dev/null
#  TABLE DES ANOMALIES                                                         
#    RSAN00   : NAME=RSAN00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
#  FICHIER D'EXTRACTION TRIE                                                   
       m_FileAssign -d SHR -g ${G_A3} FGR050 ${DATA}/PTEM/GR050YAG.FGR051AY
#  ETAT DES RECEPTIONS VALORISEES                                              
       m_OutputAssign -c 9 -w IGR051 IGR051
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR051 
       JUMP_LABEL=GR050YAK
       ;;
(GR050YAK)
       m_CondExec 04,GE,GR050YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 5 2                                                                 
# ********************************************************************         
#  EDITION DES BONS DE RECEPTION VALORISEES                                    
#  ETAT IGR052                                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR050YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GR050YAM
       ;;
(GR050YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  CARTE PARAMETRE TRAITEMENT SOUS LA FORME 2 JJMMSSAA                         
#        DATE DE FIN DE SELECTION                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/DAGR050Y
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/GR050YAM
#  TABLE DES CHEFS PRODUIT                                                     
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
#  TABLE DES ASSOCIATIONS ENTITES DE CDE INTERLOC COMMERCIALE                  
#    RSGA08   : NAME=RSGA08Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA08 /dev/null
#  TABLE DES ENTITES DE CDES                                                   
#    RSGA06   : NAME=RSGA06Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06 /dev/null
#  TABLE DES ANOMALIES                                                         
#    RSAN00   : NAME=RSAN00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
#  FICHIER D'EXTRACTION TRIE                                                   
       m_FileAssign -d SHR -g ${G_A4} FGR050 ${DATA}/PTEM/GR050YAD.FGR052AY
#  ETAT DES RECEPTIONS VALORISEES                                              
       m_OutputAssign -c 9 -w IGR052 IGR052
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR052 
       JUMP_LABEL=GR050YAN
       ;;
(GR050YAN)
       m_CondExec 04,GE,GR050YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GR050YZA
       ;;
(GR050YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GR050YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
