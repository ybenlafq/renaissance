#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GF030P.ksh                       --- VERSION DU 08/10/2016 23:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGF030 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/06/13 AT 11.31.11 BY BURTECB                      
#    STANDARDS: P  JOBSET: GF030P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
# ********************************************************************         
#  B G F 0 3 0                                                                 
# ********************************************************************         
#   PASSAGE EN RELIQUAT NON-DATE DES LIGNES DE COMMANDES POUR                  
#   LESQUELLES UNE LIVRAISON ETAIT ATTENDUE ET QUI N ONT PAS ETE               
#   RECEPTIONNEES DANS LE COURANT DU MOIS PRECEDENT                            
#   REPRISE: OUI SI FIN ANORMALE                                               
#   REPRISE: NON SI FIN NORMALE MAJ SUR LA TABLE DES LIGNES DE COMMAND         
#   ECHEANCEES                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GF030PA
       ;;
(GF030PA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2000/06/13 AT 11.31.04 BY BURTECB                
# *    JOBSET INFORMATION:    NAME...: GF030P                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'RELIQUAT NON DATE'                     
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GF030PAA
       ;;
(GF030PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  LISTE DES LIGNES DE COMMANDES PASSEES EN RELIQUAT NON DATEE                 
       m_OutputAssign -c 9 -w BGF030 EGF030
#  TABLE DES LIGNES DE COMMANDES ECHEANCEES                                    
#  DD SERVANT POUR LES CROSS REF                                               
#    TABLE    : NAME=RSGF30,MODE=U - DYNAM=YES                                 
# -X-RSGF30   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR TABLE /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE COMMANDES FOURNISSEUR                                          
#    RSGF10   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10 /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE : 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF030 
       JUMP_LABEL=GF030PAB
       ;;
(GF030PAB)
       m_CondExec 04,GE,GF030PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
