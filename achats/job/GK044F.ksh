#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GK044F.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFGK044 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/05/28 AT 10.58.08 BY BURTECA                      
#    STANDARDS: P  JOBSET: GK044F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  BGK150    CREATION DES FICHIERS FGK15                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GK044FA
       ;;
(GK044FA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GK044FAA
       ;;
(GK044FAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  DATE  JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DATE  MMSSAA                                                         
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ******  INPUT                                                                
       m_FileAssign -d SHR -g +0 FGK020 ${DATA}/PXX0/F99.BGK130BP
       m_FileAssign -d SHR -g +0 FGK050 ${DATA}/PXX0/F99.BGK050DP
       m_FileAssign -d SHR -g +0 FGK131 ${DATA}/PXX0/F99.BGK131AP
       m_FileAssign -d SHR -g +0 FGK140 ${DATA}/PXX0/F99.BGK140DP
#                                                                              
# ******  OUTPUT                                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FGK15C ${DATA}/PXX0/F99.BGK150AP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FGK15T ${DATA}/PXX0/F99.BGK150BP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FGK15P ${DATA}/PXX0/F99.BGK150CP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FGK15F ${DATA}/PXX0/F99.BGK150DP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FGK15V ${DATA}/PXX0/F99.BGK150EP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FGK15A ${DATA}/PXX0/F99.BGK150FP
       m_ProgramExec BGK150 
# ********************************************************************         
#  BGK160    CREATION DES FICHIERS FGK16                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK044FAD PGM=BGK160     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GK044FAD
       ;;
(GK044FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  DATE  JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DATE  MMSSAA                                                         
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******* PREPARATION SYSIN CFT                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDCFT ${DATA}/PXX0/F99.BGKCFTAP
# ******* DATE  MMSSAA                                                         
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GK044F1
#                                                                              
# ******  INPUT                                                                
       m_FileAssign -d SHR -g ${G_A1} FGK15C ${DATA}/PXX0/F99.BGK150AP
       m_FileAssign -d SHR -g ${G_A2} FGK15T ${DATA}/PXX0/F99.BGK150BP
       m_FileAssign -d SHR -g ${G_A3} FGK15P ${DATA}/PXX0/F99.BGK150CP
       m_FileAssign -d SHR -g ${G_A4} FGK15F ${DATA}/PXX0/F99.BGK150DP
       m_FileAssign -d SHR -g ${G_A5} FGK15V ${DATA}/PXX0/F99.BGK150EP
       m_FileAssign -d SHR -g ${G_A6} FGK15A ${DATA}/PXX0/F99.BGK150FP
       m_FileAssign -d SHR -g +0 FGK160 ${DATA}/PXX0/F99.BGK160AP
#                                                                              
# ******  OUTPUT                                                               
       m_FileAssign -d SHR -g +0 FGK161 ${DATA}/PXX0/F99.BGK160AP
       m_FileAssign -d SHR FGK16E ${DATA}/PXX0.F99.BGK160BP
       m_FileAssign -d SHR FGK16C ${DATA}/PXX0.F99.BGK160CP
       m_FileAssign -d SHR FGK16T ${DATA}/PXX0.F99.BGK160DP
       m_FileAssign -d SHR FGK16P ${DATA}/PXX0.F99.BGK160EP
       m_FileAssign -d SHR FGK16F ${DATA}/PXX0.F99.BGK160FP
       m_FileAssign -d SHR FGK16V ${DATA}/PXX0.F99.BGK160GP
       m_FileAssign -d SHR FGK16A ${DATA}/PXX0.F99.BGK160HP
       m_ProgramExec BGK160 
#                                                                              
# ********************************************************************         
#   DELETE DU FICHIER ARCHIVE                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK044FAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GK044FAG
       ;;
(GK044FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR KRISP ${DATA}/PXX0.F99.FKRISPK
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK044FAG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GK044FAH
       ;;
(GK044FAH)
       m_CondExec 16,NE,GK044FAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK170 : CREATION DU FICHIER ARCHIVE                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK044FAJ PGM=BGK170     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GK044FAJ
       ;;
(GK044FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR FGK16C ${DATA}/PXX0.F99.BGK160CP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FPKZIP ${DATA}/PXX0/F99.BGK170TG
       m_FileAssign -d SHR FARCH ${DATA}/CORTEX4.P.MTXTFIX1/GK044G3
       m_FileAssign -d SHR FDDDSN ${DATA}/CORTEX4.P.MTXTFIX1/GK044G4
       m_ProgramExec BGK170 
# ********************************************************************         
#  ZIP DU FICHIER _A ENVOYER                                                    
# ********************************************************************         
# AAU      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSIN    FILE  NAME=BGK170TG,MODE=I                                          
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK044FAM PGM=JVMLDM76   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GK044FAM
       ;;
(GK044FAM)
       m_CondExec ${EXAAU},NE,YES 
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 6160 -t LSEQ FICZIP ${DATA}/PXX0.F99.FKRISPK
# *                                                                            
       m_FileAssign -d SHR -g ${G_A7} SYSIN ${DATA}/PXX0/F99.BGK170TG
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  Migration ATOS ENVOI FICHIER VIA GATEWAY                                    
#  REPRISE :                                                                   
# ********************************************************************         
# AAZ      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    FILE  NAME=BGKCFTAP,MODE=I                                          
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY                                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK044FAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GK044FAQ
       ;;
(GK044FAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR -g ${G_A8} SYSIN ${DATA}/PXX0/F99.BGKCFTAP
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
