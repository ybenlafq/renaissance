#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GR002M.ksh                       --- VERSION DU 17/10/2016 18:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGR002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/03/17 AT 10.16.33 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GR002M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLESPACES RSGG50M,RSGG60M,RSGG70M                             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GR002MA
       ;;
(GR002MA)
#
#GR002MAA
#GR002MAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GR002MAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GR002MAD
       ;;
(GR002MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES RECEPTIONS FOURNISSEURS                                    
#    RSGR00M  : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR00M /dev/null
# ******  TABLE DES LIGNES DE RECEPTIONS FOURNISSEURS PAR SOCIETES /           
#    RSGR15M  : NAME=RSGR15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR15M /dev/null
# ******  TABLE DES LIGNES DE COMMANDES FOURNISSEURS                           
#    RSGF20M  : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20M /dev/null
# ******  TABLE DES ARTICLES                                                   
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# *******                                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *******                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CREATION DU FIC-EXTRACTION DES RECEPTIONS                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB07 ${DATA}/PTEM/GR002MAD.BGB007AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR002 
       JUMP_LABEL=GR002MAE
       ;;
(GR002MAE)
       m_CondExec 04,GE,GR002MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGR006 : SELECTION DES M.A.J EFFECTUEES SUR LES RECEPTIONS QUI NE           
#           DATENT PAS DE LA JOURNEE DE TRAITEMENT                             
#   REPRISE:OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GR002MAG
       ;;
(GR002MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES RECEPTIONS FOURNISSEURS                                    
#    RSGR00M  : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR00M /dev/null
# ******  TABLE DES LIGNES DE RECEPTIONS FOURNISSEURS PAR SOCIETES,LIE         
#    RSGR15M  : NAME=RSGR15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR15M /dev/null
# ******  TABLE DES MOUVEMENTS DE RECEPTION DU JOUR                            
#    RSGR20M  : NAME=RSGR20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR20M /dev/null
# ******  TABLE DES LIGNES DE COMMANDES FOURNISSEURS                           
#    RSGF20M  : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20M /dev/null
# ******  TABLE DES HISTORIQUES DE RECEPTION                                   
#    RSGG70M  : NAME=RSGG70M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70M /dev/null
# ******  TABLE DES ARTICLES                                                   
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CREATION DU FICHIER DES M.A.J EFFECTUEES                             
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB08 ${DATA}/PTEM/GR002MAG.BGB008AM
#                                                                              
# ******  PARAMETRE SOCIETE : 989                                              
       m_FileAssign -d SHR FSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR006 
       JUMP_LABEL=GR002MAH
       ;;
(GR002MAH)
       m_CondExec 04,GE,GR002MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PAR CODIC,TYPE ENREGISTREMENT,NUM-RECEPTION,NUM-COMMANDE ET             
#    DATE MODIFICATION  POUR CREATION DE FGB10                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GR002MAJ
       ;;
(GR002MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS EXTRACTIONS ET FIC M.A.J                                    
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GR002MAD.BGB007AM
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/GR002MAG.BGB008AM
# ******  FICHIER SORTIE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/GR002MAJ.BGB010AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_31_7 31 CH 7
 /FIELDS FLD_CH_1_1 1 CH 1
 /FIELDS FLD_CH_24_7 24 CH 7
 /FIELDS FLD_CH_3_7 3 CH 7
 /FIELDS FLD_CH_59_6 59 CH 6
 /KEYS
   FLD_CH_3_7 ASCENDING,
   FLD_CH_1_1 ASCENDING,
   FLD_CH_24_7 ASCENDING,
   FLD_CH_31_7 ASCENDING,
   FLD_CH_59_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002MAK
       ;;
(GR002MAK)
       m_CondExec 00,EQ,GR002MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 2                                                                 
# ********************************************************************         
#   EXTRACTION DES DEMANDES DE RECYCLAGE DE PRMP                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002MAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GR002MAM
       ;;
(GR002MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES                                                   
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ******  TABLE DES DEMANDES DE RECYCLAGES DE PRA                              
#    RSGG60M  : NAME=RSGG60M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG60M /dev/null
# ******  TABLE DES HISTORIQUES DE RECEPTIONS                                  
#    RSGG70M  : NAME=RSGG70M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70M /dev/null
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER EXTRACTION DES DEMANDES DE RECYCLAGE DE PRMP                 
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB09 ${DATA}/PTEM/GR002MAM.BGB009AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR012 
       JUMP_LABEL=GR002MAN
       ;;
(GR002MAN)
       m_CondExec 04,GE,GR002MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 3                                                                 
# ********************************************************************         
#  CALCUL DU STOCK SOCIETE POUR LES RECEPTIONS DU JOUR                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002MAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GR002MAQ
       ;;
(GR002MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISE : SOUS TABLE EXMAG                                  
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ******  TABLE DES STOCKS DE L ENTREPOT                                       
#    RSGS10M  : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10M /dev/null
# ******  TABLE DES STOCKS SOUS LIEUX ENTREPOT                                 
#    RSGS30M  : NAME=RSGS30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30M /dev/null
# ******  TABLE DES MVTS DE RECEPTION DU JOUR                                  
#    RSGR20M  : NAME=RSGR20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR20M /dev/null
# ******  FICHIER EXTRACTION                                                   
       m_FileAssign -d SHR -g ${G_A3} FGB09 ${DATA}/PTEM/GR002MAM.BGB009AM
# ******  FICHIER EXTRACTIONS TRIE                                             
       m_FileAssign -d SHR -g ${G_A4} FGB10 ${DATA}/PTEM/GR002MAJ.BGB010AM
# ******  FICHIER STOCK SOCIETE POUR RECEP DU JOUR                             
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB11 ${DATA}/PTEM/GR002MAQ.BGB011AM
#                                                                              
# *******                                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR013 
       JUMP_LABEL=GR002MAR
       ;;
(GR002MAR)
       m_CondExec 04,GE,GR002MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER STOCK SOCIETE                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002MAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GR002MAT
       ;;
(GR002MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER EXTRACTION                                                   
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GR002MAQ.BGB011AM
# ******  FICHIER EXTRACTIONS TRIES                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PGB989/F89.BGB013AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_7 3 CH 7
 /FIELDS FLD_CH_1_1 1 CH 1
 /FIELDS FLD_CH_24_14 24 CH 14
 /FIELDS FLD_CH_16_8 16 CH 8
 /KEYS
   FLD_CH_3_7 ASCENDING,
   FLD_CH_16_8 ASCENDING,
   FLD_CH_1_1 ASCENDING,
   FLD_CH_24_14 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002MAU
       ;;
(GR002MAU)
       m_CondExec 00,EQ,GR002MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 4                                                                 
# ********************************************************************         
#   RECALCUL DU PRMP ET MAJ DES TABLES RTGG50 RTGG60 RTGG70                    
#   REPRISE: OUI SI ABEND                                                      
#          : NON SI FIN NORMALE FAIRE UN RECOVER TO RBA DES TABLES             
#            RTGG50 - RTGG60 - RTGG70 A PARTIR DU QUIESCE DU DEBUT DE          
#            LA CHAINE                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002MAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GR002MAX
       ;;
(GR002MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE DES PRMP DU JOUR                                               
#    RSGG50M  : NAME=RSGG50M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGG50M /dev/null
# ******  TABLE DES DEMANDES DE RECYCLAGES DE PRA                              
#    RSGG60M  : NAME=RSGG60M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGG60M /dev/null
# ******  TABLE DES HISTORIQUES DE RECEPTIONS                                  
#    RSGG70M  : NAME=RSGG70M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGG70M /dev/null
#    RSGA06M  : NAME=RSGA06M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06M /dev/null
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
#                                                                              
# ******  FICHIER EXTRACTION CONTROLE TRIE                                     
       m_FileAssign -d SHR -g ${G_A6} FGB13 ${DATA}/PGB989/F89.BGB013AM
# ******  FICHIER DES RECYCLAGE DE PRMP                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB16 ${DATA}/PTEM/GR002MAX.BGB016AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR014 
       JUMP_LABEL=GR002MAY
       ;;
(GR002MAY)
       m_CondExec 04,GE,GR002MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#   EASYTREAVE                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002MBA PGM=EZTPA00    ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GR002MBA
       ;;
(GR002MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#  FICHIER DES RECYCLAGE DE PRMP                                               
       m_FileAssign -d SHR -g ${G_A7} FILEA ${DATA}/PTEM/GR002MAX.BGB016AM
       m_FileAssign -d NEW,CATLG,DELETE -r 108 -g +1 FILEB ${DATA}/PTEM/GR002MBA.BGR002AM
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX4/GR002MBA
       m_ProgramExec GR002MBA
# ********************************************************************         
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  TRI DU FICHIER ISSU DE L'EASYTRIEVE                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002MBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GR002MBD
       ;;
(GR002MBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER EXTRACTION                                                          
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GR002MBA.BGR002AM
#  FICHIER EXTRACTIONS TRIES                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GR002MBD.BGR002BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_10_6 10 CH 06
 /FIELDS FLD_BI_15_7 15 CH 07
 /FIELDS FLD_BI_7_2 07 CH 02
 /FIELDS FLD_BI_9_1 09 CH 01
 /KEYS
   FLD_BI_7_2 ASCENDING,
   FLD_BI_9_1 ASCENDING,
   FLD_BI_10_6 ASCENDING,
   FLD_BI_15_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002MBE
       ;;
(GR002MBE)
       m_CondExec 00,EQ,GR002MBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002MBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GR002MBG
       ;;
(GR002MBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******************** TABLES DU GENERATEUR D'ETATS                            
#    RTEG00   : NAME=RSEG00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/GR002MBD.BGR002BM
# *********************************** FICHIER FCUMULS                          
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GR002MBG.BGR002CM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GR002MBH
       ;;
(GR002MBH)
       m_CondExec 04,GE,GR002MBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  TRI DU FICHIER FCUMUL                                                       
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002MBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GR002MBJ
       ;;
(GR002MBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMUL                           
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/GR002MBG.BGR002CM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GR002MBJ.BGR002DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002MBK
       ;;
(GR002MBK)
       m_CondExec 00,EQ,GR002MBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IGR016                                                 
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002MBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GR002MBM
       ;;
(GR002MBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A11} FEXTRAC ${DATA}/PTEM/GR002MBD.BGR002BM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FCUMULS ${DATA}/PTEM/GR002MBJ.BGR002DM
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG132 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w BGR016 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GR002MBN
       ;;
(GR002MBN)
       m_CondExec 04,GE,GR002MBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GR002MZA
       ;;
(GR002MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GR002MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
