#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GF000P.ksh                       --- VERSION DU 08/10/2016 12:34
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGF000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/12/17 AT 14.14.03 BY BURTECN                      
#    STANDARDS: P  JOBSET: GF000P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='PREPA'                                                             
# ********************************************************************         
#  B G F 0 0 0                                                                 
# ********************************************************************         
#   LISTE DES COMMANDES NON-SOLDEES TRIEES PAR ENTITE DE COMMANDE              
#   INTERLOCUTEUR N DE COMMANDE FAMILLE MARQUE ARTICLE                         
#   REPRISE: OUI                                                               
#   TRAITEMENT DIMANCHE                                                        
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GF000PA
       ;;
(GF000PA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       GF000PA=${VENDRED}
       GF000PB=${SAMEDI}
       RUN=${RUN}
       JUMP_LABEL=GF000PAA
       ;;
(GF000PAA)
       m_CondExec ${EXAAA},NE,YES 1,NE,$[GF000PA] 1,NE,$[GF000PB] 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  LISTE DES COMMANDES NON-SOLDEES                                             
       m_OutputAssign -c 9 -w BGF000 SYSCONTL
#  TABLE DES COMMANDES FOURNISSEUR                                             
#    TABLE    : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES LIGNES COMMANDES FOURNISSEUR                                      
#    TABLEA   : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEA /dev/null
#  TABLE DES LIGNES DE COMMANDES ECHEANCEES                                    
#    TABLEB   : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEB /dev/null
#  TABLE DES LIGNES DE COMMANDES DEMANDEURS                                    
#    RSGF15   : NAME=RSGF15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF15 /dev/null
#  TABLE DES ARTICLES                                                          
#    TABLEC   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEC /dev/null
#  TABLE DES ENTITES DE COMMANDES                                              
#    TABLED   : NAME=RSGA06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLED /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* SPECIFIQUE ARTICLE GERES PAR ENTREPOT EXTERIEUR                      
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE = 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF000 
       JUMP_LABEL=GF000PAB
       ;;
(GF000PAB)
       m_CondExec 04,GE,GF000PAA ${EXAAA},NE,YES 1,NE,$[GF000PA] 1,NE,$[GF000PB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G F 0 1 5                                                                 
# ********************************************************************         
#  VENTILATION DES NOMBRES D U O PRIS OU A PRENDRE                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF000PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GF000PAD
       ;;
(GF000PAD)
       m_CondExec ${EXAAF},NE,YES 1,NE,$[GF000PB] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  LISTE DE LA VENTILATION DES NOMBRES D U O                                   
       m_OutputAssign -c 9 -w BGF015 IMPR
#  TABLE DES COMMANDES FOURNISSEUR                                             
#    TABLE    : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES LIGNES DE COMMANDES ECHEANCEES                                    
#    TABLEB   : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEB /dev/null
#  TABLE DES PLANNING DE LIVRAISON COMMANDES FOURNISSEURS                      
#    TABLEC   : NAME=RSGF35,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEC /dev/null
#  TABLE DES ARTICLES                                                          
#    TABLED   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLED /dev/null
#  TABLE DES CHEFS PRODUITS                                                    
#    TABLEE   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEE /dev/null
#  TABLE DES LIEUX                                                             
#    TABLEF   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEF /dev/null
#  TABLE DES RECEPTIONS                                                        
#    TABLEG   : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEG /dev/null
#  TABLE DES LIGNES DE RECEPTION ECHEANCEES                                    
#    TABLEH   : NAME=RSGR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEH /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE = 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ****** CARTE PARAMETRE TRAITEMENT SOUS LA FORME                              
# ******          JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF015 
       JUMP_LABEL=GF000PAE
       ;;
(GF000PAE)
       m_CondExec 04,GE,GF000PAD ${EXAAF},NE,YES 1,NE,$[GF000PB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G F 0 1 0                                                                 
# ********************************************************************         
#  CREATION DE L ETAT DESCRIPTIF DE COMMANDE                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF000PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GF000PAG
       ;;
(GF000PAG)
       m_CondExec ${EXAAK},NE,YES 1,NE,$[GF000PB] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES COMMANDES FOURNISSEUR                                             
#    TABLE    : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES LIGNES COMMANDES FOURNISSEUR DEMANDEURS                           
#    RSGF15   : NAME=RSGF15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF15 /dev/null
#  TABLE DES LIGNES DE COMMANDES ECHEANCEES                                    
#    TABLEB   : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEB /dev/null
#  TABLE DES MVTS DE CREATION ET DE MAJ DE COMMANDES                           
#    TABLEC   : NAME=RSGF45,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEC /dev/null
#  TABLE DES ARTICLES                                                          
#    TABLED   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLED /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE = 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ****** CARTE PARAMETRE TRAITEMENT SOUS LA FORME                              
# ******          JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ****** LISTE DE L ETAT DESCRIPTIF DES COMMANDES                              
       m_OutputAssign -c 9 -w BGF010 IMPR
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF010 
       JUMP_LABEL=GF000PAH
       ;;
(GF000PAH)
       m_CondExec 04,GE,GF000PAG ${EXAAK},NE,YES 1,NE,$[GF000PB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G F 0 2 0                                                                 
# ********************************************************************         
#  CREATION DE L ETAT VALORISATION DE COMMANDE                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GF000PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GF000PAJ
       ;;
(GF000PAJ)
       m_CondExec ${EXAAP},NE,YES 1,NE,$[GF000PB] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES COMMANDES FOURNISSEUR                                             
#    TABLE    : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLE /dev/null
#  TABLE DES LIGNES DE COMMANDES FOURNISSEUR                                   
#    TABLEA   : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEA /dev/null
#  TABLE DES RISTOURNES SUR LES LIGNES DE COMMANDES FOURNISSEUR                
#    TABLEB   : NAME=RSGF25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEB /dev/null
#  TABLE DES MVTS DE CREATION ET DE MAJ DE COMMANDES                           
#    TABLEC   : NAME=RSGF45,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEC /dev/null
#  TABLE DES VALORISATION DES ESCLAVES LIBRE DES COMMANDES FOURNI              
#    TABLED   : NAME=RSGF50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLED /dev/null
#  TABLE DES ARTICLES                                                          
#    TABLEE   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEE /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE = 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ****** CARTE PARAMETRE TRAITEMENT SOUS LA FORME                              
# ******          JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ****** LISTE DE L ETAT VALORISATION DE COMMANDES                             
       m_OutputAssign -c 9 -w BGF020 IMPR
#                                                                              
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGF020 
       JUMP_LABEL=GF000PAK
       ;;
(GF000PAK)
       m_CondExec 04,GE,GF000PAJ ${EXAAP},NE,YES 1,NE,$[GF000PB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
