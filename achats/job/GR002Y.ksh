#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GR002Y.ksh                       --- VERSION DU 17/10/2016 18:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGR002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/03/17 AT 10.19.21 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GR002Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  RECALCUL DES PRMPS                                                          
# ********************************************************************         
#  QUIESCE DES TABLESPACES RSGG50Y,RSGG60Y,RSGG70Y                             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GR002YA
       ;;
(GR002YA)
#
#GR002YAA
#GR002YAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GR002YAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GR002YAD
       ;;
(GR002YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES RECEPTIONS FOURNISSEURS                                    
#    RSGR00Y  : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR00Y /dev/null
# ******  TABLE DES LIGNES DE RECEPTIONS FOURNISSEURS PAR SOCIETES /           
#    RSGR15Y  : NAME=RSGR15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR15Y /dev/null
# ******  TABLE DES LIGNES DE COMMANDES FOURNISSEURS                           
#    RSGF20Y  : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20Y /dev/null
# ******  TABLE DES ARTICLES                                                   
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# *******                                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *******                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CREATION DU FIC-EXTRACTION DES RECEPTIONS                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB07 ${DATA}/PTEM/GR002YAD.BGB007AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR002 
       JUMP_LABEL=GR002YAE
       ;;
(GR002YAE)
       m_CondExec 04,GE,GR002YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 0 6                                                                 
# ********************************************************************         
#   SELECTION DES M.A.J EFFECTUEES SUR LES RECEPTIONS QUI NE                   
#      DATENT PAS DE LA JOURNEE DE TRAITEMENT                                  
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GR002YAG
       ;;
(GR002YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES RECEPTIONS FOURNISSEURS                                    
#    RSGR00Y  : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR00Y /dev/null
# ******  TABLE DES LIGNES DE RECEPTIONS FOURNISSEURS PAR SOCIETES,LIE         
#    RSGR15Y  : NAME=RSGR15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR15Y /dev/null
# ******  TABLE DES MOUVEMENTS DE RECEPTION DU JOUR                            
#    RSGR20Y  : NAME=RSGR20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR20Y /dev/null
# ******  TABLE DES LIGNES DE COMMANDES FOURNISSEURS                           
#    RSGF20Y  : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20Y /dev/null
# ******  TABLE DES HISTORIQUES DE RECEPTION                                   
#    RSGG70Y  : NAME=RSGG70Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70Y /dev/null
# ******  TABLE DES ARTICLES                                                   
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CREATION DU FICHIER DES M.A.J EFFECTUEES                             
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB08 ${DATA}/PTEM/GR002YAG.BGB008AY
#                                                                              
# ******  PARAMETRE SOCIETE : 945                                              
       m_FileAssign -d SHR FSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR006 
       JUMP_LABEL=GR002YAH
       ;;
(GR002YAH)
       m_CondExec 04,GE,GR002YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PAR CODIC,TYPE ENREGISTREMENT,NUM-RECEPTION,NUM-COMMANDE ET             
#    DATE MODIFICATION  POUR CREATION DE FGB10                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GR002YAJ
       ;;
(GR002YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS EXTRACTIONS ET FIC M.A.J                                    
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GR002YAD.BGB007AY
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/GR002YAG.BGB008AY
# ******  FICHIER SORTIE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/GR002YAJ.BGB010AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_7 24 CH 7
 /FIELDS FLD_CH_59_6 59 CH 6
 /FIELDS FLD_CH_1_1 1 CH 1
 /FIELDS FLD_CH_31_7 31 CH 7
 /FIELDS FLD_CH_3_7 3 CH 7
 /KEYS
   FLD_CH_3_7 ASCENDING,
   FLD_CH_1_1 ASCENDING,
   FLD_CH_24_7 ASCENDING,
   FLD_CH_31_7 ASCENDING,
   FLD_CH_59_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002YAK
       ;;
(GR002YAK)
       m_CondExec 00,EQ,GR002YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 2                                                                 
# ********************************************************************         
#   EXTRACTION DES DEMANDES DE RECYCLAGE DE PRMP                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GR002YAM
       ;;
(GR002YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES                                                   
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ******  TABLE DES DEMANDES DE RECYCLAGES DE PRA                              
#    RSGG60Y  : NAME=RSGG60Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG60Y /dev/null
# ******  TABLE DES HISTORIQUES DE RECEPTIONS                                  
#    RSGG70Y  : NAME=RSGG70Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70Y /dev/null
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER EXTRACTION DES DEMANDES DE RECYCLAGE DE PRMP                 
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB09 ${DATA}/PTEM/GR002YAM.BGB009AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR012 
       JUMP_LABEL=GR002YAN
       ;;
(GR002YAN)
       m_CondExec 04,GE,GR002YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 3                                                                 
# ********************************************************************         
#  CALCUL DU STOCK SOCIETE POUR LES RECEPTIONS DU JOUR                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002YAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GR002YAQ
       ;;
(GR002YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISE : SOUS TABLE EXMAG                                  
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******  TABLE DES STOCKS DE L ENTREPOT                                       
#    RSGS10Y  : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10Y /dev/null
# ******  TABLE DES STOCKS SOUS LIEUX ENTREPOT                                 
#    RSGS30Y  : NAME=RSGS30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30Y /dev/null
# ******  TABLE DES MVTS DE RECEPTION DU JOUR                                  
#    RSGR20Y  : NAME=RSGR20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR20Y /dev/null
# ******  FICHIER EXTRACTION                                                   
       m_FileAssign -d SHR -g ${G_A3} FGB09 ${DATA}/PTEM/GR002YAM.BGB009AY
# ******  FICHIER EXTRACTIONS TRIE                                             
       m_FileAssign -d SHR -g ${G_A4} FGB10 ${DATA}/PTEM/GR002YAJ.BGB010AY
# ******  FICHIER STOCK SOCIETE POUR RECEP DU JOUR                             
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB11 ${DATA}/PTEM/GR002YAQ.BGB011AY
#                                                                              
# *******                                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR013 
       JUMP_LABEL=GR002YAR
       ;;
(GR002YAR)
       m_CondExec 04,GE,GR002YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER STOCK SOCIETE                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002YAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GR002YAT
       ;;
(GR002YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER EXTRACTION                                                   
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GR002YAQ.BGB011AY
# ******  FICHIER EXTRACTIONS TRIES                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PGB945/F45.BGB013AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_14 24 CH 14
 /FIELDS FLD_CH_3_7 3 CH 7
 /FIELDS FLD_CH_16_8 16 CH 8
 /FIELDS FLD_CH_1_1 1 CH 1
 /KEYS
   FLD_CH_3_7 ASCENDING,
   FLD_CH_16_8 ASCENDING,
   FLD_CH_1_1 ASCENDING,
   FLD_CH_24_14 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002YAU
       ;;
(GR002YAU)
       m_CondExec 00,EQ,GR002YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 4                                                                 
# ********************************************************************         
#   RECALCUL DU PRMP ET MAJ DES TABLES RTGG50 RTGG60 RTGG70                    
#   REPRISE: OUI SI ABEND                                                      
#          : NON SI FIN NORMALE FAIRE UN RECOVER TO RBA DES TABLES             
#            RTGG50 - RTGG60 - RTGG70 A PARTIR DU QUIESCE DU DEBUT DE          
#            LA CHAINE                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002YAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GR002YAX
       ;;
(GR002YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#    RSGA06Y  : NAME=RSGA06Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06Y /dev/null
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******  TABLE DES PRMP DU JOUR                                               
#    RSGG50Y  : NAME=RSGG50Y,MODE=U - DYNAM=YES                                
# -X-RSGG50Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSGG50Y /dev/null
# ******  TABLE DES DEMANDES DE RECYCLAGES DE PRA                              
#    RSGG60Y  : NAME=RSGG60Y,MODE=U - DYNAM=YES                                
# -X-RSGG60Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSGG60Y /dev/null
# ******  TABLE DES HISTORIQUES DE RECEPTIONS                                  
#    RSGG70Y  : NAME=RSGG70Y,MODE=U - DYNAM=YES                                
# -X-RSGG70Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSGG70Y /dev/null
#                                                                              
# ******  FICHIER EXTRACTION CONTROLE TRIE                                     
       m_FileAssign -d SHR -g ${G_A6} FGB13 ${DATA}/PGB945/F45.BGB013AY
# ******  FICHIER DES RECYCLAGE DE PRMP                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB16 ${DATA}/PTEM/GR002YAX.BGB016AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR014 
       JUMP_LABEL=GR002YAY
       ;;
(GR002YAY)
       m_CondExec 04,GE,GR002YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#   EASYTREAVE                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002YBA PGM=EZTPA00    ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GR002YBA
       ;;
(GR002YBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#  FICHIER DES RECYCLAGE DE PRMP                                               
       m_FileAssign -d SHR -g ${G_A7} FILEA ${DATA}/PTEM/GR002YAX.BGB016AY
       m_FileAssign -d NEW,CATLG,DELETE -r 108 -g +1 FILEB ${DATA}/PTEM/GR002YBA.BGR002AY
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX4/GR002YBA
       m_ProgramExec GR002YBA
# ********************************************************************         
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  TRI DU FICHIER ISSU DE L'EASYTRIEVE                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002YBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GR002YBD
       ;;
(GR002YBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER EXTRACTION                                                          
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GR002YBA.BGR002AY
#  FICHIER EXTRACTIONS TRIES                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GR002YBD.BGR002BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_15_7 15 CH 07
 /FIELDS FLD_BI_7_2 07 CH 02
 /FIELDS FLD_BI_9_1 09 CH 01
 /FIELDS FLD_BI_10_6 10 CH 06
 /KEYS
   FLD_BI_7_2 ASCENDING,
   FLD_BI_9_1 ASCENDING,
   FLD_BI_10_6 ASCENDING,
   FLD_BI_15_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002YBE
       ;;
(GR002YBE)
       m_CondExec 00,EQ,GR002YBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002YBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GR002YBG
       ;;
(GR002YBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******************** TABLES DU GENERATEUR D'ETATS                            
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/GR002YBD.BGR002BY
# *********************************** FICHIER FCUMULS                          
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GR002YBG.BGR002CY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GR002YBH
       ;;
(GR002YBH)
       m_CondExec 04,GE,GR002YBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  TRI DU FICHIER FCUMUL                                                       
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002YBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GR002YBJ
       ;;
(GR002YBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMUL                           
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/GR002YBG.BGR002CY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GR002YBJ.BGR002DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002YBK
       ;;
(GR002YBK)
       m_CondExec 00,EQ,GR002YBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IGR016                                                 
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002YBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GR002YBM
       ;;
(GR002YBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A11} FEXTRAC ${DATA}/PTEM/GR002YBD.BGR002BY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FCUMULS ${DATA}/PTEM/GR002YBJ.BGR002DY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG132 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w BGR016 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GR002YBN
       ;;
(GR002YBN)
       m_CondExec 04,GE,GR002YBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GR002YZA
       ;;
(GR002YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GR002YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
