#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GK001M.ksh                       --- VERSION DU 09/10/2016 05:55
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGK001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/02/07 AT 10.09.13 BY BURTEC5                      
#    STANDARDS: P  JOBSET: GK001M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BRM000 : EXTRACTION DES DONNEES ARTICLE-FAMILLE                             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GK001MA
       ;;
(GK001MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GK001MAA
       ;;
(GK001MAA)
       m_CondExec ${EXAAA},NE,YES 
#                                                                              
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* ARTICLE                                                              
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ******* COCIC / VALEUR CODE MARKETING                                        
#    RSGA09M  : NAME=RSGA09M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09M /dev/null
# ******* LIEUX                                                                
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ******* RELATION ETAT / FAMILLE RAYON                                        
#    RSGA11M  : NAME=RSGA11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11M /dev/null
# ******* RELATION FAMILLE / CODE MARKETING                                    
#    RSGA12M  : NAME=RSGA12M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12M /dev/null
# ******* TABLE FAMILLES                                                       
#    RSGA14M  : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14M /dev/null
# ******* LIBELLES IMBRICATIONS DES CODES MARKETING                            
#    RSGA29M  : NAME=RSGA29M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA29M /dev/null
# ******* RELATION ARTICLE MODES DE DELIVRANCES                                
#    RSGA64M  : NAME=RSGA64M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA64M /dev/null
# ******* RELATION ARTICLE RAYON CODE MARKETING                                
#    RSGA83M  : NAME=RSGA83M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA83M /dev/null
#                                                                              
# ******* SOCIETE = 989                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******* PARAMETRE EDITION= 'KESA'                                            
       m_FileAssign -d SHR FETAT ${DATA}/CORTEX4.P.MTXTFIX1/BRM000ET
#                                                                              
# ******* DONNEES MUTATIONS                                                    
       m_FileAssign -d SHR FMU235 /dev/null
# ******* DONNEES DESTINEES AU LOAD RTRM60                                     
       m_FileAssign -d SHR RTRM60 /dev/null
# ******* DONNEES DESTINEES AU LOAD RTRM65                                     
       m_FileAssign -d SHR RTRM65 /dev/null
# ******* FIC ARTICLE ET FAMILLE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FRMCOM ${DATA}/PXX0/GK001MAA.BRM001EM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM000 
       JUMP_LABEL=GK001MAB
       ;;
(GK001MAB)
       m_CondExec 04,GE,GK001MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGK020 : CREATION DU FICHIER COMPLET ARTICLES                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GK001MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GK001MAD
       ;;
(GK001MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* ARTICLE                                                              
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ******* MARQUES                                                              
#    RSGA22M  : NAME=RSGA22M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22M /dev/null
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FIC ARTICLE ET FAMILLE                                               
       m_FileAssign -d SHR -g ${G_A1} FRMCOM ${DATA}/PXX0/GK001MAA.BRM001EM
# ******* DATE DE DERNIER PASSAGE                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/BGK020M
#                                                                              
# ******* FIC ARTICLE COMPLET                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 113 -g +1 FGK020 ${DATA}/PXX0/F89.BGK020AM
# ******* FIC ARTICLE-MAJ COMPLET                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 FGK120 ${DATA}/PXX0/F89.BGK120AM
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGK020 
       JUMP_LABEL=GK001MAE
       ;;
(GK001MAE)
       m_CondExec 04,GE,GK001MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GK001MZA
       ;;
(GK001MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GK001MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
