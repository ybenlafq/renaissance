#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GR002P.ksh                       --- VERSION DU 17/10/2016 18:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGR002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/03/23 AT 13.54.22 BY BURTECA                      
#    STANDARDS: P  JOBSET: GR002P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLES RTGG50,RTGG60,RTGG70                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GR002PA
       ;;
(GR002PA)
#
#GR002PAA
#GR002PAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GR002PAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GR002PAD
       ;;
(GR002PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES RECEPTIONS FOURNISSEURS                                           
#    RSGR00   : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR00 /dev/null
#  TABLE DES LIGNES DE RECEPTIONS FOURNISSEURS PAR SOCIETES / LIEUX            
#    RSGR15   : NAME=RSGR15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR15 /dev/null
#  TABLE DES LIGNES DE COMMANDES FOURNISSEURS                                  
#    RSGF20   : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20 /dev/null
#  TABLE DES ARTICLES                                                          
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******                                                                       
# ******  PARAMETRE SOCIETE = 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******                                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#  CREATION DU FIC-EXTRACTION DES RECEPTIONS                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB07 ${DATA}/PTEM/GR002PAD.BGB007AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR002 
       JUMP_LABEL=GR002PAE
       ;;
(GR002PAE)
       m_CondExec 04,GE,GR002PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 0 6                                                                 
# ********************************************************************         
#   SELECTION DES M.A.J EFFECTUEES SUR LES RECEPTIONS NCP QUI NE               
#      DATENT PAS DE LA JOURNEE DE TRAITEMENT                                  
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GR002PAG
       ;;
(GR002PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES RECEPTIONS FOURNISSEURS                                           
#    RSGR00   : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR00 /dev/null
#  TABLE DES LIGNES DE RECEPTIONS FOURNISSEURS PAR SOCIETES / LIEUX            
#    RSGR15   : NAME=RSGR15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR15 /dev/null
#  TABLE DES MOUVEMENTS DE RECEPTION DU JOUR                                   
#    RSGR20   : NAME=RSGR20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR20 /dev/null
#  TABLE DES LIGNES DE COMMANDES FOURNISSEURS                                  
#    RSGF20   : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20 /dev/null
#  TABLE DES HISTORIQUES DE RECEPTION                                          
#    RSGG70   : NAME=RSGG70,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG70 /dev/null
#  TABLE DES ARTICLES                                                          
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
       m_FileAssign -i FDATE
$FDATE
_end
#  CREATION DU FICHIER DES M.A.J  EFFECTUEES                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB08 ${DATA}/PTEM/GR002PAG.BGB008AP
#                                                                              
# ******  PARAMETRE SOCIETE : 907                                              
       m_FileAssign -d SHR FSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR006 
       JUMP_LABEL=GR002PAH
       ;;
(GR002PAH)
       m_CondExec 04,GE,GR002PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PAR CODIC,TYPE ENREGISTREMENT,NUM-RECEPTION,NUM-COMMANDE ET             
#    DATE MODIFICATION  POUR CREATION DE FGB10                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GR002PAJ
       ;;
(GR002PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIERS EXTRACTIONS ET FIC  M.A.J                                          
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GR002PAD.BGB007AP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/GR002PAG.BGB008AP
#  FICHIER                                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/GR002PAJ.BGB010AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_1 1 CH 1
 /FIELDS FLD_CH_31_7 31 CH 7
 /FIELDS FLD_CH_24_7 24 CH 7
 /FIELDS FLD_CH_3_7 3 CH 7
 /FIELDS FLD_CH_59_6 59 CH 6
 /KEYS
   FLD_CH_3_7 ASCENDING,
   FLD_CH_1_1 ASCENDING,
   FLD_CH_24_7 ASCENDING,
   FLD_CH_31_7 ASCENDING,
   FLD_CH_59_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002PAK
       ;;
(GR002PAK)
       m_CondExec 00,EQ,GR002PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 2                                                                 
# ********************************************************************         
#   EXTRACTION DES DEMANDES DE RECYCLAGE DE PRMP                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GR002PAM
       ;;
(GR002PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    TABLEA   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEA /dev/null
#  TABLE DES DEMANDES DE RECYCLAGES DE PRA                                     
#    TABLEB   : NAME=RSGG60,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEB /dev/null
#  TABLE DES HISTORIQUES DE RECEPTIONS                                         
#    TABLEB   : NAME=RSGG70,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEB /dev/null
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER EXTRACTION DES DEMANDES DE RECYCLAGE DE PRMP                        
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB09 ${DATA}/PTEM/GR002PAM.BGB009AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR012 
       JUMP_LABEL=GR002PAN
       ;;
(GR002PAN)
       m_CondExec 04,GE,GR002PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 3                                                                 
# ********************************************************************         
#  CALCUL DU STOCK SOCIETE POUR LES RECEPTIONS DU JOUR                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GR002PAQ
       ;;
(GR002PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISE : SOUS TABLE EXMAG                                         
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#  TABLE DES STOCKS DE L ENTREPOT                                              
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#  TABLE DES STOCKS SOUS LIEUX ENTREPOT                                        
#    RSGS30   : NAME=RSGS30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS30 /dev/null
#  TABLE DES MVTS DE RECEPTION DU JOUR                                         
#    RSGR20   : NAME=RSGR20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR20 /dev/null
#  FICHIER EXTRACTION                                                          
       m_FileAssign -d SHR -g ${G_A3} FGB09 ${DATA}/PTEM/GR002PAM.BGB009AP
#  FICHIER EXTRACTIONS TRIE                                                    
       m_FileAssign -d SHR -g ${G_A4} FGB10 ${DATA}/PTEM/GR002PAJ.BGB010AP
#  FICHIER STOCK SOCIETE POUR RECEP DU JOUR                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB11 ${DATA}/PTEM/GR002PAQ.BGB011AP
#                                                                              
# ******  PARAMETRE SOCIETE = 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR013 
       JUMP_LABEL=GR002PAR
       ;;
(GR002PAR)
       m_CondExec 04,GE,GR002PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER STOCK SOCIETE                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GR002PAT
       ;;
(GR002PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER EXTRACTION                                                          
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GR002PAQ.BGB011AP
#  FICHIER EXTRACTIONS TRIES                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PGB0/F07.BGB013AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_1 1 CH 1
 /FIELDS FLD_CH_16_8 16 CH 8
 /FIELDS FLD_CH_3_7 3 CH 7
 /FIELDS FLD_CH_24_14 24 CH 14
 /KEYS
   FLD_CH_3_7 ASCENDING,
   FLD_CH_16_8 ASCENDING,
   FLD_CH_1_1 ASCENDING,
   FLD_CH_24_14 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002PAU
       ;;
(GR002PAU)
       m_CondExec 00,EQ,GR002PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G R 0 1 4                                                                 
# ********************************************************************         
#   RECALCUL DU PRMP ET MAJ DES TABLES RTGG50 RTGG60 RTGG70                    
#   REPRISE: OUI SI ABEND                                                      
#          : NON SI FIN NORMALE FAIRE UN RECOVER TO RBA DES TABLES             
#            RTGG50 - RTGG60 - RTGG70 A PARTIR DU QUIESCE DU DEBUT DE          
#            LA CHAINE                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GR002PAX
       ;;
(GR002PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#    RSGA06   : NAME=RSGA06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA06 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSSP10   : NAME=RSSP10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSP10 /dev/null
# ****** TABLE DES PRMP DU JOUR                                                
#    RSGG50   : NAME=RSGG50,MODE=(U,N) - DYNAM=YES                             
# -X-RSGG50   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGG50 /dev/null
# ****** TABLE DES DEMANDES DE RECYCLAGES DE PRA                               
#    RSGG60   : NAME=RSGG60,MODE=(U,N) - DYNAM=YES                             
# -X-RSGG60   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGG60 /dev/null
# ****** TABLE DES HISTORIQUES DE RECEPTIONS                                   
#    RSGG70   : NAME=RSGG70,MODE=(U,N) - DYNAM=YES                             
# -X-RSGG70   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGG70 /dev/null
#                                                                              
#  FICHIER EXTRACTION CONTROLE TRIE                                            
       m_FileAssign -d SHR -g ${G_A6} FGB13 ${DATA}/PGB0/F07.BGB013AP
#  FICHIER DES RECYCLAGE DE PRMP                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB16 ${DATA}/PXX0/F07.BGB016AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR014 
       JUMP_LABEL=GR002PAY
       ;;
(GR002PAY)
       m_CondExec 04,GE,GR002PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  EASYTREAVE                                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002PBA PGM=EZTPA00    ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GR002PBA
       ;;
(GR002PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#  FICHIER DES RECYCLAGE DE PRMP                                               
       m_FileAssign -d SHR -g ${G_A7} FILEA ${DATA}/PXX0/F07.BGB016AP
       m_FileAssign -d NEW,CATLG,DELETE -r 108 -g +1 FILEB ${DATA}/PTEM/GR002PBA.BGR002AP
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX4/GR002PBA
       m_ProgramExec GR002PBA
# ********************************************************************         
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  TRI DU FICHIER ISSU DE L'EASYTRIEVE                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GR002PBD
       ;;
(GR002PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER EXTRACTION                                                          
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GR002PBA.BGR002AP
#  FICHIER EXTRACTIONS TRIES                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GR002PBD.BGR002BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_15_7 15 CH 07
 /FIELDS FLD_BI_7_2 07 CH 02
 /FIELDS FLD_BI_9_1 09 CH 01
 /FIELDS FLD_BI_10_6 10 CH 06
 /KEYS
   FLD_BI_7_2 ASCENDING,
   FLD_BI_9_1 ASCENDING,
   FLD_BI_10_6 ASCENDING,
   FLD_BI_15_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002PBE
       ;;
(GR002PBE)
       m_CondExec 00,EQ,GR002PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GR002PBG
       ;;
(GR002PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******************** TABLES DU GENERATEUR D'ETATS                            
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/GR002PBD.BGR002BP
# *********************************** FICHIER FCUMULS                          
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GR002PBG.BGR002CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GR002PBH
       ;;
(GR002PBH)
       m_CondExec 04,GE,GR002PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  TRI DU FICHIER FCUMUL                                                       
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GR002PBJ
       ;;
(GR002PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMUL                           
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/GR002PBG.BGR002CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GR002PBJ.BGR002DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR002PBK
       ;;
(GR002PBK)
       m_CondExec 00,EQ,GR002PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IGR016                                                 
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR002PBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GR002PBM
       ;;
(GR002PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A11} FEXTRAC ${DATA}/PTEM/GR002PBD.BGR002BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FCUMULS ${DATA}/PTEM/GR002PBJ.BGR002DP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG132 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w BGR016 FEDITION
# FEDITION FILE  NAME=BGR016AP,MODE=O                                          
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GR002PBN
       ;;
(GR002PBN)
       m_CondExec 04,GE,GR002PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#         FUSION DU FICHIER RECYCLAGE POUR SAUVE CDROM MICROLIST               
#  REPRISE: OUI                                                                
# ********************************************************************         
# ACX      SORT                                                                
# SORTIN   FILE  NAME=BGR016BP,MODE=I                                          
#          FILE  NAME=BGR016AP,MODE=I                                          
# *******  EDITION DESTINE AUX MICRO-FICHES SUR 1 MOIS                         
# SORTOUT  FILE  NAME=BGR016BP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  MERGE FIELDS=COPY                                                           
#          DATAEND                                                             
# ********************************************************************         
#  SI PLANTAGE _A CE STEP ON MET LA CHAINE TERMINEE                             
# ********************************************************************         
#  EDITION BGR016  ETAT DES RECYCLAGES SUR ET HORS FACTURES                    
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# ADC      STEP  PGM=IEBGENER,LANG=UTIL                                        
#                                                                              
# SYSUT1   FILE  NAME=BGR016AP,MODE=I                                          
# SYSUT2   REPORT SYSOUT=(9,BGR016)                                            
# //SYSIN    DD DUMMY                                                          
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GR002PZA
       ;;
(GR002PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GR002PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
