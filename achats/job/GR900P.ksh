#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GR900P.ksh                       --- VERSION DU 17/10/2016 18:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGR900 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/02/01 AT 10.53.15 BY BURTEC7                      
#    STANDARDS: P  JOBSET: GR900P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BGR903                                                                
# ********************************************************************         
#  SELECTION DES RECEPTIONS A EPURER                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GR900PA
       ;;
(GR900PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GR900PAA
       ;;
(GR900PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RSGF00   : NAME=RSGF01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF00 /dev/null
#    RSGF10   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10 /dev/null
#    RSGR30   : NAME=RSGR30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR30 /dev/null
#    RSGR35   : NAME=RSGR35,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR35 /dev/null
#                                                                              
#    RSGF20   : NAME=RSGF20,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGF20 /dev/null
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER VENANT DE SAP LRECL 07                                       
       m_FileAssign -d SHR -g +0 FGR903 ${DATA}/PXX0/F07.FGR903AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR903 
       JUMP_LABEL=GR900PAB
       ;;
(GR900PAB)
       m_CondExec 04,GE,GR900PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGR900                                                                
# ********************************************************************         
#  SELECTION DES RECEPTIONS A EPURER                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR900PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GR900PAD
       ;;
(GR900PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RSGR00   : NAME=RSGR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR00 /dev/null
#    RSGR10   : NAME=RSGR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR10 /dev/null
#    RSGR30   : NAME=RSGR30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR30 /dev/null
#    RSGF20   : NAME=RSGF20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20 /dev/null
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CARTE PARAMETRE (DELAI EN MOIS POUR PURGE )                          
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GR900PAD
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -g +1 FGR900 ${DATA}/PTEM/GR900PAD.FGR900AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR900 
       JUMP_LABEL=GR900PAE
       ;;
(GR900PAE)
       m_CondExec 04,GE,GR900PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGR901                                                                
# ********************************************************************         
#  SELECTION DES COMMANDES A EPURER                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR900PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GR900PAG
       ;;
(GR900PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RSGF10   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10 /dev/null
#    RSGF30   : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30 /dev/null
#    RSGR10   : NAME=RSGR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR10 /dev/null
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CARTE PARAMETRE (DELAI EN MOIS POUR PURGE )                          
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GR900PAG
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -g +1 FGF900 ${DATA}/PTEM/GR900PAG.FGF900AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR901 
       JUMP_LABEL=GR900PAH
       ;;
(GR900PAH)
       m_CondExec 04,GE,GR900PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER SERVANT A LOADER LA RTGR90                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR900PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GR900PAJ
       ;;
(GR900PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GR900PAD.FGR900AP
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -g +1 SORTOUT ${DATA}/PGR0/F07.RELOAD.GR90RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_14 1 CH 14
 /KEYS
   FLD_CH_1_14 ASCENDING
 /* Record Type = F  Record Length = 14 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR900PAK
       ;;
(GR900PAK)
       m_CondExec 00,EQ,GR900PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER SERVANT A LOADER LA RTGF90                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR900PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GR900PAM
       ;;
(GR900PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GR900PAG.FGF900AP
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -g +1 SORTOUT ${DATA}/PGF0/F07.RELOAD.GF90RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_14 1 CH 14
 /KEYS
   FLD_CH_1_14 ASCENDING
 /* Record Type = F  Record Length = 14 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GR900PAN
       ;;
(GR900PAN)
       m_CondExec 00,EQ,GR900PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE RTGR90                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR900PAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GR900PAQ
       ;;
(GR900PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ******  FIC DE LOAD DE LA RTGS40                                             
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PGR0/F07.RELOAD.GR90RP
# ******  MVTS DE STOCKS                                                       
#    RSGR90   : NAME=RSGR90,MODE=(U,N) - DYNAM=YES                             
# -X-RSGR90   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGR90 /dev/null
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GR900PAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GR900P_GR900PAQ_RTGR90.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GR900PAR
       ;;
(GR900PAR)
       m_CondExec 04,GE,GR900PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE RTGF90                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR900PAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GR900PAT
       ;;
(GR900PAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ******  FIC DE LOAD DE LA RTGF90                                             
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PGF0/F07.RELOAD.GF90RP
# ******  MVTS DE STOCKS                                                       
#    RSGF90   : NAME=RSGF90,MODE=(U,N) - DYNAM=YES                             
# -X-RSGF90   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGF90 /dev/null
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GR900PAT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GR900P_GR900PAT_RTGF90.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GR900PAU
       ;;
(GR900PAU)
       m_CondExec 04,GE,GR900PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGR902                                                                
# ********************************************************************         
#  ELIMINE LES LIGNES COMMUNES DANS RTGR90 ET RTGF90                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR900PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GR900PAX
       ;;
(GR900PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RSGR90   : NAME=RSGR90,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR90 /dev/null
#    RSGF90   : NAME=RSGF90,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF90 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR902 
       JUMP_LABEL=GR900PAY
       ;;
(GR900PAY)
       m_CondExec 04,GE,GR900PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD                                                                      
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR900PBA PGM=PTLDRIVM   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GR900PBA
       ;;
(GR900PBA)
       m_CondExec ${EXABO},NE,YES 
#  TABLE DES VENTES DETAIL                                                     
#    RSGR90   : NAME=RSGR90,MODE=I - DYNAM=YES                                 
# **********  FICHIER D UNLOAD                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -g +1 SYSREC01 ${DATA}/PXX0/F07.FGR906AP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GR900PBA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  PGM : BGR907                                                                
# ********************************************************************         
#  EPURATION DES COMMANDES ET RECEPTIONS FOURNISSEUR                           
#  REPRISE: NON (METTRE TERMIN� LA CHAINE ENVOY� MAIL AU BT)                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR900PBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GR900PBD
       ;;
(GR900PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RSGF10   : NAME=RSGF10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10 /dev/null
#    RSGF15   : NAME=RSGF15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF15 /dev/null
#    RSGF20   : NAME=RSGF20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20 /dev/null
#    RSGF25   : NAME=RSGF25,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF25 /dev/null
#    RSGR00   : NAME=RSGR00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR00 /dev/null
#    RSGR05   : NAME=RSGR05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR05 /dev/null
#    RSGR10   : NAME=RSGR10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR10 /dev/null
#    RSGR15   : NAME=RSGR15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR15 /dev/null
#    RSGR30   : NAME=RSGR30,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR30 /dev/null
#    RSGR35   : NAME=RSGR35,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGR35 /dev/null
# ******  FICHIER D'UNLOAD DE RTGR90                                           
       m_FileAssign -d SHR -g ${G_A5} FGR906 ${DATA}/PXX0/F07.FGR906AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR907 
       JUMP_LABEL=GR900PBE
       ;;
(GR900PBE)
       m_CondExec 04,GE,GR900PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGR908                                                                
#  EPURATION GESTION DE COMMENTAIRES PAR LIGNES DE COMMANDE                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR900PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GR900PBG
       ;;
(GR900PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLES EN ENTREE                                                     
#    RSGF01   : NAME=RSGF10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF01 /dev/null
#    RSGF20   : NAME=RSGF20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF20 /dev/null
#    RSGF70   : NAME=RSGF70,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF70 /dev/null
# ******  FICHIER DE LOAD DE RTGF70                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g +1 FGF70 ${DATA}/PTEM/GR900PBG.BGR908AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR908 
       JUMP_LABEL=GR900PBH
       ;;
(GR900PBH)
       m_CondExec 04,GE,GR900PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE  RTGF70 GESTION DE COMMENTAIRES PAR LIGNES DE COMMANDE              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR900PBJ PGM=DSNUTILB   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GR900PBJ
       ;;
(GR900PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ******  FIC DE LOAD DE LA RTGF70                                             
       m_FileAssign -d SHR -g ${G_A6} SYSREC ${DATA}/PTEM/GR900PBG.BGR908AP
# ******  GESTION DE COMMENTAIRES PAR LIGNES DE COMMANDE                       
#    RSGF70   : NAME=RSGF70,MODE=(U,N) - DYNAM=YES                             
# -X-RSGF70   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGF70 /dev/null
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GR900PBJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GR900P_GR900PBJ_RTGF70.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GR900PBK
       ;;
(GR900PBK)
       m_CondExec 04,GE,GR900PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGR909  : PGM A SUPPRIMER APRES LE PASSGE DU 12/02/2011               
# ********************************************************************         
#  EPURATION DES COMMANDES ET RECEPTIONS FOURNISSEUR                           
#  REPRISE: OUI                                                                
# ********************************************************************         
# ACD      STEP  PGM=IKJEFT01                                                  
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ******  PARAMETRE DATE                                                       
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# ******  TABLES EN MAJ POUR SUPPRESSION                                       
# RSGF01   FILE  DYNAM=YES,NAME=RSGF01,MODE=U,REST=NO                          
# RSGF10   FILE  DYNAM=YES,NAME=RSGF10,MODE=U,REST=NO                          
# RSGF15   FILE  DYNAM=YES,NAME=RSGF15,MODE=U,REST=NO                          
# RSGF20   FILE  DYNAM=YES,NAME=RSGF20,MODE=U,REST=NO                          
# RSGF30   FILE  DYNAM=YES,NAME=RSGF30,MODE=U,REST=NO                          
# RSGR00   FILE  DYNAM=YES,NAME=RSGR00,MODE=U,REST=NO                          
# RSGR05   FILE  DYNAM=YES,NAME=RSGR05,MODE=U,REST=NO                          
# RSGR10   FILE  DYNAM=YES,NAME=RSGR10,MODE=U,REST=NO                          
# RSGR15   FILE  DYNAM=YES,NAME=RSGR15,MODE=U,REST=NO                          
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BGR909) PLAN(BGR909)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  REMISE _A ZERO DU FICHIER RECEPTION VENANT DE SAP                            
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GR900PBM PGM=IDCAMS     ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GR900PBM
       ;;
(GR900PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR IN1 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 7 -g +1 OUT1 ${DATA}/PXX0/F07.FGR903AP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GR900PBM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GR900PBN
       ;;
(GR900PBN)
       m_CondExec 16,NE,GR900PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GR900PZA
       ;;
(GR900PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GR900PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
