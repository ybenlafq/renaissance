#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GR092P.ksh                       --- VERSION DU 08/10/2016 12:46
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGR092 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/28 AT 13.49.32 BY OPERAT1                      
#    STANDARDS: P  JOBSET: GR092P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BGR092 : RéCEPTION ADMINISTRATIVE DES RECEPTIONS _A QUAI                     
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GR092PA
       ;;
(GR092PA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2015/04/28 AT 13.49.32 BY OPERAT1                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: GR092P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ARTICLE FILIALES'                      
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GR092PAA
       ;;
(GR092PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# PRED     LINK  NAME=$EX010P,MODE=I   *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLES EN LECTURE                                                           
#    RTLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTLI00 /dev/null
#    RTLW99   : NAME=RSLW99,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTLW99 /dev/null
#  TABLES EN MAJ                                                               
#    RTGR00   : NAME=RSGR00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTGR00 /dev/null
#    RTGR05   : NAME=RSGR05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTGR05 /dev/null
#    RTGR92   : NAME=RSGR92,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTGR92 /dev/null
#    RTGR95   : NAME=RSGR95,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTGR95 /dev/null
#    RTLV30   : NAME=RSLV30,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTLV30 /dev/null
#    RTLV32   : NAME=RSLV32,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTLV32 /dev/null
#    RTLV40   : NAME=RSLV40,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTLV40 /dev/null
#    RTLW32   : NAME=RSLW32,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTLW32 /dev/null
#    RTLW40   : NAME=RSLW40,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTLW40 /dev/null
#    RTAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTAN00 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR092 
       JUMP_LABEL=GR092PAB
       ;;
(GR092PAB)
       m_CondExec 04,GE,GR092PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
