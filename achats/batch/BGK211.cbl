      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BGK211.                                                     
       AUTHOR. JG.                                                              
       ENVIRONMENT DIVISION.                                                    
      ******************************************************************        
      *                                                                         
      *  Ventes Krisp:                                                          
      *  G�n�ration du SYSIN pour Fast Unload de GS40, GS42, GG70               
      *                                                                         
      ******************************************************************        
      * Ajout du param�tre FPARAM qui contient LGTG si la filiale fait          
      * partie de la logistique groupe.                                         
      * Dans ce cas, il faut faire en sorte que le r�sultat soit                
      * vide, sans que cela plante la cha�ne ( les tables filiales              
      * n'existant plus).                                                       
      ******************************************************************        
      * BGK211 DEVIENT UNIQUEMENT                                               
      *  G�N�RATION DU SYSIN POUR FAST UNLOAD DE GS30 ET HV02                   
      * (suppression de gs40)                                                   
      ******************************************************************        
      * FMOIS DOIT ETRE LA DATE DE DERNIER JOUR DU MOIS                         
      ******************************************************************        
      * DSA015 14/05/2012 : on n'extrait pas le stock de la soc 996             
      *                     dans la table RTGS10                                
      * signet 0512                                                             
      ******************************************************************        
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FDATE   ASSIGN TO FDATE.                                     
      *--                                                                       
            SELECT FDATE   ASSIGN TO FDATE                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
AL    *     SELECT FPARAM  ASSIGN TO FPARAM.                                    
      *--                                                                       
            SELECT FPARAM  ASSIGN TO FPARAM                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
JB    *     SELECT FNSOC   ASSIGN TO FNSOC.                                     
      *--                                                                       
            SELECT FNSOC   ASSIGN TO FNSOC                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FSYSIN  ASSIGN TO FSYSIN.                                    
      *--                                                                       
            SELECT FSYSIN  ASSIGN TO FSYSIN                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FGK211  ASSIGN TO FGK211.                                    
      *--                                                                       
            SELECT FGK211  ASSIGN TO FGK211                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FDATE  RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  MW-FILLER PIC X(80).                                                 
AL     FD  FPARAM RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  MW-FILLER PIC X(80).                                                 
       FD  FNSOC  RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  MW-FILLER PIC X(80).                                                 
       FD  FSYSIN RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
      *MW DAR-2                                                                 
       01  MW-FILLER PIC X(80).                                                 
       FD  FGK211 RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  ENR-FGK211 PIC X(80).                                                
       WORKING-STORAGE SECTION.                                                 
      ***************************************************************           
      * ZONE POUR FPARAM                                                        
      ***************************************************************           
AL     01  W-FPARAM  PIC X(80) .                                                
      ***************************************************************           
      * ZONE OUT POUR FGK211, FLAG POUR FSYSIN                                  
      ***************************************************************           
       01  W-FGK211 PIC X(80).                                                  
       01  FILLER PIC X VALUE SPACE.                                            
           88  FIN-FSYSIN VALUE 'F'.                                            
      ***************************************************************           
      * COMMS AVEC LES SOUS PROGS                                               
      ***************************************************************           
           COPY ABENDCOP.                                                       
       01  BETDATC PIC X(8) VALUE 'BETDATC'.                                    
           COPY WORKDATC.                                                       
      ***************************************************************           
      * DATES CALCULEES                                                         
      ***************************************************************           
       01  W-DDEBUT PIC X(8).                                                   
       01  W-DFIN   PIC X(8).                                                   
      * ============================================================= *         
      *                 DESCRIPTION FICHIER FNSOC                     *         
      * ============================================================= *         
      *                                                                         
       01  FNSOC-ENR.                                                           
           05  FNSOC-NSOCIETE       PIC  X(03)    VALUE SPACES.                 
           05  FNSOC-FILLER         PIC  X(77)    VALUE SPACES.                 
      *                                                                         
      ***************************************************************           
      * PROC:                                                                   
      * - LECTURE DATE                                                          
      * - CALCUL DES BORNES DE DATES                                            
      * - CHARGEMENT DES CODES REMISE EN TABLEAU                                
      * - GENERATION DE SYSIN                                                   
      * - C'EST TOUT                                                            
      ***************************************************************           
       PROCEDURE DIVISION.                                                      
           DISPLAY 'BGK211: Ventes Hebdo pour KRISP'                            
           OPEN INPUT FDATE FSYSIN FPARAM FNSOC                                 
           OUTPUT FGK211.                                                       
           READ FDATE INTO GFJJMMSSAA AT END                            01090024
                MOVE 'FICHIER FDATE VIDE' TO ABEND-MESS                 01100024
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           CLOSE FDATE.                                                 01130024
AL         READ FPARAM INTO W-FPARAM                                            
AL         END-READ.                                                            
AL         CLOSE FPARAM.                                                        
           MOVE '1'            TO GFDATA.                               00004640
           CALL BETDATC      USING WORK-BETDATC                         00004670
           IF GFVDAT NOT = '1'                                          00004680
              STRING 'BETDATC : ' GF-MESS-ERR                           00004700
              DELIMITED BY SIZE INTO ABEND-MESS                         00004710
              PERFORM ABEND-PROGRAMME                                           
           END-IF.                                                      00004730
           DISPLAY 'BGK211: date param�tre lue sur FDATE: '                     
                    GFSMN-LIB-L ' ' GFJOUR ' '                                  
                    GFMOI-LIB-L ' ' GFSIECLE GFANNEE.                           
           MOVE GFSAMJ-0 TO W-DFIN.                                             
      ***************************************************************           
      * KALKUL DES BORGNES HEBDO                                                
      ***************************************************************           
           SUBTRACT 6 FROM GFQNT0                                               
           MOVE '3'            TO GFDATA                                00004640
           CALL BETDATC      USING WORK-BETDATC                         00004670
           IF GFVDAT NOT = '1'                                          00004680
              STRING 'BETDATC : ' GF-MESS-ERR                           00004700
              DELIMITED BY SIZE INTO ABEND-MESS                         00004710
              PERFORM ABEND-PROGRAMME                                           
           END-IF.                                                      00004730
           DISPLAY 'BGK211: date d�but de recherche: '                          
                    GFSMN-LIB-L ' ' GFJOUR ' '                                  
                    GFMOI-LIB-L ' ' GFSIECLE GFANNEE.                           
           MOVE GFSAMJ-0 TO W-DDEBUT.                                           
      ******************************************************************        
      * RECOPIE DE FSYSIN                                                       
      ******************************************************************        
           DISPLAY 'BGK211: sysin g�n�r� � partir de FSYSIN:'                   
           PERFORM UNTIL FIN-FSYSIN                                             
              READ FSYSIN INTO W-FGK211                                         
                 AT END                                                         
                    SET FIN-FSYSIN TO TRUE                                      
                 NOT AT END                                                     
                    WRITE ENR-FGK211 FROM W-FGK211                              
                    DISPLAY W-FGK211                                            
              END-READ                                                          
           END-PERFORM.                                                         
           CLOSE FSYSIN.                                                        
           READ FNSOC INTO FNSOC-ENR   AT END                                   
                MOVE 'FICHIER FNSOC VIDE' TO ABEND-MESS                         
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           DISPLAY  'SOCIETE  : '   FNSOC-NSOCIETE                              
           CLOSE FNSOC.                                                         
      ******************************************************************        
      * GENERATION DE L'ORDRE SQL GS40                                          
      ******************************************************************        
      *                                                                         
      * Ordre 'bidon' permettant d'obtenir un r�sultat vide pour les            
      * filiales qui ont int�gr� la logistique groupe .                         
AL    *    IF  W-FPARAM    = 'LGTG'                                             
JB... *      DISPLAY 'BGK211: sysin g�n�r� pour RTGS40:'                        
      *      MOVE '   SELECT NCODIC, NLIEUORIG, NLIEUDEST, ' TO W-FGK211.       
      *      WRITE ENR-FGK211 FROM W-FGK211                                     
      *      DISPLAY W-FGK211                                                   
      *      MOVE '          PVTOTALSR, QMVT' TO W-FGK211                       
      *      WRITE ENR-FGK211 FROM W-FGK211                                     
      *      DISPLAY W-FGK211                                                   
      *      MOVE '     FROM RTGS42 ' TO W-FGK211                               
      *      WRITE ENR-FGK211 FROM W-FGK211                                     
      *      DISPLAY W-FGK211                                                   
AL    *      MOVE "   WHERE NCODIC = 'BIDON';"  TO W-FGK211                     
AL    *      WRITE ENR-FGK211 FROM W-FGK211                                     
AL    *      DISPLAY W-FGK211                                                   
      *                                                                         
AL    *    ELSE                                                                 
      *      DISPLAY 'BGK211: sysin g�n�r� pour RTGS40:'                        
      *      MOVE '   SELECT NCODIC, NLIEUORIG, NLIEUDEST, ' TO W-FGK211.       
      *      WRITE ENR-FGK211 FROM W-FGK211                                     
      *      DISPLAY W-FGK211                                                   
      *      MOVE '          PVTOTALSR, QMVT' TO W-FGK211                       
      *      WRITE ENR-FGK211 FROM W-FGK211                                     
      *      DISPLAY W-FGK211                                                   
      *      MOVE '     FROM RTGS40 ' TO W-FGK211                               
      *      WRITE ENR-FGK211 FROM W-FGK211                                     
      *      DISPLAY W-FGK211                                                   
      *      MOVE '    WHERE (NLIEUORIG = ''VEN''' TO W-FGK211                  
      *      WRITE ENR-FGK211 FROM W-FGK211                                     
      *      DISPLAY W-FGK211                                                   
      *      MOVE '            OR NLIEUDEST = ''VEN'')' TO W-FGK211             
      *      WRITE ENR-FGK211 FROM W-FGK211                                     
      *      DISPLAY W-FGK211                                                   
      *      MOVE '      AND DMVT  >= ''' TO W-FGK211                           
      *      MOVE W-DDEBUT TO W-FGK211(21:)                                     
      *      MOVE '''' TO W-FGK211(29:)                                         
      *      WRITE ENR-FGK211 FROM W-FGK211                                     
      *      DISPLAY W-FGK211                                                   
      *      MOVE '      AND DMVT  <= ''' TO W-FGK211                           
      *      MOVE W-DFIN TO W-FGK211(21:)                                       
      *      MOVE ''' ;' TO W-FGK211(29:)                                       
      *      WRITE ENR-FGK211 FROM W-FGK211                                     
      *      DISPLAY W-FGK211                                                   
      *    END-IF.                                                              
      *                                                                         
...JB *                                                                         
      ******************************************************************        
      * GENERATION DE L'ORDRE SQL GS42                                          
      ******************************************************************        
JB... *    DISPLAY 'BGK211: sysin g�n�r� pour RTGS42:'                          
      *    MOVE '   SELECT NCODIC, NLIEUORIG, NLIEUDEST, ' TO W-FGK211.         
      *    WRITE ENR-FGK211 FROM W-FGK211.                                      
      *    DISPLAY W-FGK211.                                                    
      *    MOVE '          PVTOTALSR, QMVT' TO W-FGK211.                        
      *    WRITE ENR-FGK211 FROM W-FGK211.                                      
      *    DISPLAY W-FGK211.                                                    
      *    MOVE '     FROM RTGS42 ' TO W-FGK211.                                
      *    WRITE ENR-FGK211 FROM W-FGK211.                                      
      *    DISPLAY W-FGK211.                                                    
      *    MOVE '    WHERE (NLIEUORIG = ''VEN''' TO W-FGK211.                   
      *    WRITE ENR-FGK211 FROM W-FGK211.                                      
      *    DISPLAY W-FGK211.                                                    
      *    MOVE '            OR NLIEUDEST = ''VEN'')' TO W-FGK211.              
      *    WRITE ENR-FGK211 FROM W-FGK211.                                      
      *    DISPLAY W-FGK211.                                                    
      *    MOVE '      AND DMVT  >= ''' TO W-FGK211.                            
      *    MOVE W-DDEBUT TO W-FGK211(21:).                                      
      *    MOVE '''' TO W-FGK211(29:).                                          
      *    WRITE ENR-FGK211 FROM W-FGK211.                                      
      *    DISPLAY W-FGK211.                                                    
      *    MOVE '      AND DMVT  <= ''' TO W-FGK211.                            
      *    MOVE W-DFIN TO W-FGK211(21:).                                        
      *    MOVE ''' ;' TO W-FGK211(29:).                                        
      *    WRITE ENR-FGK211 FROM W-FGK211.                                      
...JB *    DISPLAY W-FGK211.                                                    
      ******************************************************************        
      * GENERATION DE L'ORDRE SQL GG70                                          
      ******************************************************************        
           DISPLAY 'BGK211: sysin g�n�r� pour RTGG70:'                          
           MOVE '   SELECT NCODIC, DREC, NCDE, PRA, QTEREC' TO W-FGK211.        
           WRITE ENR-FGK211 FROM W-FGK211.                                      
           DISPLAY W-FGK211.                                                    
           MOVE '     FROM RTGG70 ' TO W-FGK211.                                
           WRITE ENR-FGK211 FROM W-FGK211.                                      
           DISPLAY W-FGK211.                                                    
           MOVE '    WHERE WFICTIF <> ''O''' TO W-FGK211.                       
           WRITE ENR-FGK211 FROM W-FGK211.                                      
           DISPLAY W-FGK211.                                                    
           MOVE '      AND DREC  <= ''' TO W-FGK211.                            
           MOVE W-DFIN   TO W-FGK211(21:).                                      
           MOVE ''';' TO W-FGK211(29:).                                         
           WRITE ENR-FGK211 FROM W-FGK211.                                      
           DISPLAY W-FGK211.                                                    
      ******************************************************************        
      * GENERATION DE L'ORDRE SQL GS10                                          
      ******************************************************************        
           DISPLAY 'BGK211: sysin g�n�r� pour RTGS10:'                          
AL         IF  W-FPARAM    = 'LGTG'                                             
AL           MOVE '   SELECT NCODIC, QSTOCK' TO W-FGK211                        
AL           WRITE ENR-FGK211 FROM W-FGK211                                     
AL           DISPLAY W-FGK211                                                   
AL           MOVE '     FROM RTGS30' TO W-FGK211                                
AL           WRITE ENR-FGK211 FROM W-FGK211                                     
AL           DISPLAY W-FGK211                                                   
AL           MOVE "   WHERE NCODIC = 'BIDON';" TO W-FGK211                      
AL           WRITE ENR-FGK211 FROM W-FGK211                                     
AL           DISPLAY W-FGK211                                                   
AL         ELSE                                                                 
             MOVE '   SELECT NCODIC, QSTOCK' TO W-FGK211                        
             WRITE ENR-FGK211 FROM W-FGK211                                     
             DISPLAY W-FGK211                                                   
0512  *      MOVE '     FROM RTGS10;' TO W-FGK211                               
0512         MOVE '     FROM RTGS10 ' TO W-FGK211                               
             WRITE ENR-FGK211 FROM W-FGK211                                     
             DISPLAY W-FGK211                                                   
0512         MOVE "    WHERE NSOCDEPOT <> '996' ;" TO W-FGK211                  
0512         WRITE ENR-FGK211 FROM W-FGK211                                     
0512         DISPLAY W-FGK211                                                   
AL         END-IF.                                                              
      ******************************************************************        
      * GENERATION DE L'ORDRE SQL GS30                                          
      ******************************************************************        
           DISPLAY 'BGK211: sysin g�n�r� pour RTGS30:'                          
           MOVE '   SELECT NCODIC, QSTOCK' TO W-FGK211.                         
           WRITE ENR-FGK211 FROM W-FGK211.                                      
           DISPLAY W-FGK211.                                                    
           MOVE '     FROM RTGS30;' TO W-FGK211.                                
           WRITE ENR-FGK211 FROM W-FGK211.                                      
           DISPLAY W-FGK211.                                                    
      ******************************************************************        
      * GENERATION DE L'ORDRE SQL GS31                                          
      ******************************************************************        
           DISPLAY 'BGK211: sysin g�n�r� pour RTGS31:'                          
           MOVE '   SELECT NCODIC, QSTOCK' TO W-FGK211.                         
           WRITE ENR-FGK211 FROM W-FGK211.                                      
           DISPLAY W-FGK211.                                                    
           MOVE '     FROM RTGS31;' TO W-FGK211.                                
           WRITE ENR-FGK211 FROM W-FGK211.                                      
           DISPLAY W-FGK211.                                                    
      ******************************************************************        
      * GENERATION DE L'ORDRE SQL HV02                                          
      ******************************************************************        
           DISPLAY 'BGK211: SYSIN G�N�R� POUR RTHV02:'                          
           MOVE '   SELECT NCODIC, QPIECES, PCA, PMTACHATS '                    
                                     TO W-FGK211.                               
           WRITE ENR-FGK211 FROM W-FGK211.                                      
           DISPLAY W-FGK211.                                                    
      *    MOVE '   PCA, PMTACHATS ' TO W-FGK211.                               
      *    WRITE ENR-FGK211 FROM W-FGK211.                                      
      *    DISPLAY W-FGK211.                                                    
           MOVE '     FROM RTHV02 ' TO W-FGK211.                                
           WRITE ENR-FGK211 FROM W-FGK211.                                      
           DISPLAY W-FGK211.                                                    
           MOVE '     WHERE NSOCIETE  = ''' TO W-FGK211.                        
           MOVE FNSOC-NSOCIETE TO W-FGK211(25:).                                
           MOVE ''' ' TO W-FGK211(28:).                                         
           WRITE ENR-FGK211 FROM W-FGK211.                                      
           DISPLAY W-FGK211.                                                    
           MOVE '      AND DVENTECIALE  >= ''' TO W-FGK211.                     
           MOVE W-DDEBUT   TO W-FGK211(28:).                                    
           MOVE ''' ' TO W-FGK211(36:).                                         
           WRITE ENR-FGK211 FROM W-FGK211.                                      
           DISPLAY W-FGK211.                                                    
           MOVE '      AND DVENTECIALE  <= ''' TO W-FGK211.                     
           MOVE W-DFIN   TO W-FGK211(28:).                                      
           MOVE ''';' TO W-FGK211(36:).                                         
           WRITE ENR-FGK211 FROM W-FGK211.                                      
           DISPLAY W-FGK211.                                                    
      ******************************************************************        
      * ZIENDE                                                                  
      ******************************************************************        
           CLOSE FGK211.                                                        
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      ******************************************************************        
      * SCHPLOUF                                                                
      ******************************************************************        
       ABEND-PROGRAMME                 SECTION.                                 
           MOVE  'BGK211'     TO   ABEND-PROG.                                  
           CALL  'ABEND'   USING   ABEND-PROG  ABEND-MESS.                      
