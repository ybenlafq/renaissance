      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. BGK340.                                                      
       AUTHOR. JG.                                                              
      ******************************************************************        
      *                                                                         
      *  Mise en forme de tous les fichiers � envoyer � Krisp et                
      *  g�n�ration du fichier ent�te FGK34E.                                   
      *  - Contr�le (FGK32C+FGK33C) -> FGK34C                                   
      *  - Typo (FGK32T) -> FGK34T                                              
      *  - Produits (FGK32P) -> FGK34P                                          
      *  - Fournisseurs (FGK33F) -> FGK34F                                      
      *  - Ventes (FGK32V) -> FGK34V                                            
      *  - Achats (FGK32A) -> FGK34A                                            
      *                                                                         
      ******************************************************************        
      * MODIFICATIONS :                                                *        
      * ---------------                                                *        
      * A. DURAND - 16/02/2005 : CALCUL DE LA SEMAINE ERRONE           *        
      *                          REFERENCE 'AD05' EN COL 1             *        
      ******************************************************************        
      * Modif le 18022008 FQ : DSA026                                           
      * la r�gle de calcul de la 1�re semaine de                                
      * l'exercice n'est pas valable pour cette ann�e ( FQ en col 1 )           
      ******************************************************************        
      * Modif le 05052008 FQ : DSA026                                           
      * la r�gle de calcul de la 1�re semaine de                                
      * change car le d�but d'exercice est le 01 mai  ( FQ2 en col 1 )          
      ******************************************************************        
      * modif le 18052009 ec : DSA015                                           
      * erreur sur le n� de semaine calcul�    EC en col1                       
      ******************************************************************        
      * DSA015 - 13/02/12 : ajout du n� de semaine dans nom de l'archive        
      *                     envoy�e par CFT => creation d'une sysin             
      *                     utilis�e dans le step de send cft                   
      ******************************************************************        
      * DSA015 - 03/07/13 : passage de CFT � FTP                                
      *                          => modification de la sysin                    
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                                   
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FDATE   ASSIGN TO  FDATE.                                   
      *--                                                                       
            SELECT  FDATE   ASSIGN TO  FDATE                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FPARAM  ASSIGN TO  FPARAM.                                  
      *--                                                                       
            SELECT  FPARAM  ASSIGN TO  FPARAM                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK32C  ASSIGN TO  FGK32C.                                  
      *--                                                                       
            SELECT  FGK32C  ASSIGN TO  FGK32C                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK32T  ASSIGN TO  FGK32T.                                  
      *--                                                                       
            SELECT  FGK32T  ASSIGN TO  FGK32T                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK32P  ASSIGN TO  FGK32P.                                  
      *--                                                                       
            SELECT  FGK32P  ASSIGN TO  FGK32P                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK33F  ASSIGN TO  FGK33F.                                  
      *--                                                                       
            SELECT  FGK33F  ASSIGN TO  FGK33F                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK32V  ASSIGN TO  FGK32V.                                  
      *--                                                                       
            SELECT  FGK32V  ASSIGN TO  FGK32V                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK32A  ASSIGN TO  FGK32A.                                  
      *--                                                                       
            SELECT  FGK32A  ASSIGN TO  FGK32A                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK340  ASSIGN TO  FGK340.                                  
      *--                                                                       
            SELECT  FGK340  ASSIGN TO  FGK340                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK341  ASSIGN TO  FGK341.                                  
      *--                                                                       
            SELECT  FGK341  ASSIGN TO  FGK341                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK34E  ASSIGN TO  FGK34E.                                  
      *--                                                                       
            SELECT  FGK34E  ASSIGN TO  FGK34E                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK34C  ASSIGN TO  FGK34C.                                  
      *--                                                                       
            SELECT  FGK34C  ASSIGN TO  FGK34C                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK34T  ASSIGN TO  FGK34T.                                  
      *--                                                                       
            SELECT  FGK34T  ASSIGN TO  FGK34T                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK34P  ASSIGN TO  FGK34P.                                  
      *--                                                                       
            SELECT  FGK34P  ASSIGN TO  FGK34P                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK34F  ASSIGN TO  FGK34F.                                  
      *--                                                                       
            SELECT  FGK34F  ASSIGN TO  FGK34F                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK34V  ASSIGN TO  FGK34V.                                  
      *--                                                                       
            SELECT  FGK34V  ASSIGN TO  FGK34V                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK34A  ASSIGN TO  FGK34A.                                  
      *--                                                                       
            SELECT  FGK34A  ASSIGN TO  FGK34A                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
0212  *     SELECT  FDCFT   ASSIGN TO  FDCFT.                                   
      *--                                                                       
            SELECT  FDCFT   ASSIGN TO  FDCFT                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      ******************************************************************        
       DATA DIVISION.                                                           
      ******************************************************************        
       FILE SECTION.                                                            
      * Parms...                                                                
       FD  FDATE RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.             
       01  MW-FILLER        PIC X(80).                                          
       FD  FPARAM RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER        PIC X(80).                                          
      * Input...                                                                
       FD  FGK32C RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER        PIC X(200).                                         
       FD  FGK32T RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER        PIC X(200).                                         
       FD  FGK32P RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER        PIC X(200).                                         
       FD  FGK33F RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER        PIC X(200).                                         
       FD  FGK32V RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER        PIC X(200).                                         
       FD  FGK32A RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER        PIC X(200).                                         
       FD  FGK340 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
      *MW DAR-2                                                                 
       01  MW-FILLER        PIC X(10).                                          
      * Output...                                                               
       FD  FGK341 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  ENR-FGK341    PIC X(10).                                             
       FD  FGK34E RECORDING V BLOCK 0 RECORDS                                   
           RECORD VARYING FROM 10 TO 196 DEPENDING ON FGK34O-LENGTH             
           LABEL RECORD STANDARD.                                               
       01  ENR-FGK34E    PIC X(196).                                            
       FD  FGK34C RECORDING V BLOCK 0 RECORDS                                   
           RECORD VARYING FROM 10 TO 196 DEPENDING ON FGK34O-LENGTH             
           LABEL RECORD STANDARD.                                               
       01  ENR-FGK34C    PIC X(196).                                            
       FD  FGK34T RECORDING V BLOCK 0 RECORDS                                   
           RECORD VARYING FROM 10 TO 196 DEPENDING ON FGK34O-LENGTH             
           LABEL RECORD STANDARD.                                               
       01  ENR-FGK34T    PIC X(196).                                            
       FD  FGK34P RECORDING V BLOCK 0 RECORDS                                   
           RECORD VARYING FROM 10 TO 196 DEPENDING ON FGK34O-LENGTH             
           LABEL RECORD STANDARD.                                               
       01  ENR-FGK34P    PIC X(196).                                            
       FD  FGK34V RECORDING V BLOCK 0 RECORDS                                   
           RECORD VARYING FROM 10 TO 196 DEPENDING ON FGK34O-LENGTH             
           LABEL RECORD STANDARD.                                               
       01  ENR-FGK34V    PIC X(196).                                            
       FD  FGK34F RECORDING V BLOCK 0 RECORDS                                   
           RECORD VARYING FROM 10 TO 196 DEPENDING ON FGK34O-LENGTH             
           LABEL RECORD STANDARD.                                               
       01  ENR-FGK34F    PIC X(196).                                            
       FD  FGK34A RECORDING V BLOCK 0 RECORDS                                   
           RECORD VARYING FROM 10 TO 196 DEPENDING ON FGK34O-LENGTH             
           LABEL RECORD STANDARD.                                               
       01  ENR-FGK34A    PIC X(196).                                            
0212   FD  FDCFT  RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
0212   01  ENR-FDCFT     PIC X(80).                                             
      ******************************************************************        
       WORKING-STORAGE SECTION.                                                 
      ******************************************************************        
      * quelques zones... qui servent - on s'en doute - � quelque chose         
      ******************************************************************        
       01  W-SEMAINE      PIC X(6)     VALUE SPACES.                            
       01  PIC-EDITZ9     PIC ----.---.--9.                                     
       01  PIC-EDITV9     PIC ----.---.--9,99.                                  
       01  FPARAM-ENR.                                                          
           02  FPARAM-CDEVISE PIC X(05).                                        
           02  FPARAM-LDEVISE PIC X(50).                                        
      ******************************************************************        
      * FGK340/341: dernier envoi                                               
      ******************************************************************        
       01  FGK340-ENR.                                                          
   1       02  FGK340-SEMAINE         PIC X(06).                                
   7       02  FGK340-VERSION         PIC 9(02).                                
      ******************************************************************        
      * FGK34E: Ent�te (6 champs)                                               
      ******************************************************************        
       01  FGK34E-ENR.                                                          
   1       02  FGK34E-NSOC            PIC X(03) VALUE '004'.                    
           02  FILLER                 PIC X(02) VALUE ',"'.                     
   2       02  FGK34E-LSOC            PIC X(20) VALUE 'DARTY'.                  
           02  FILLER                 PIC X(03) VALUE '","'.                    
   3       02  FGK34E-DATETIME        PIC X(20).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
   4       02  FGK34E-DATE            PIC X(07).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
   5       02  FGK34E-CDEVISE         PIC X(05).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
   6       02  FGK34E-LDEVISE         PIC X(50).                                
           02  FILLER                 PIC X(01) VALUE '"'.                      
       01  CPT-FGK34E  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  W-DATE      PIC 9(6).                                                
       01  W-TIME      PIC 9(8).                                                
0212  ******************************************************************        
"     * FDCFT                                                                   
"     ******************************************************************        
0212   01  FDCFT-ENR         PIC X(80).                                         
      ******************************************************************        
      * TOUS FICHIERS IO.                                                       
      ******************************************************************        
       01  FGK34I-ENR   PIC X(200).                                             
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FGK32C VALUE 'F'.                                             
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FGK32T VALUE 'F'.                                             
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FGK32P VALUE 'F'.                                             
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FGK33F VALUE 'F'.                                             
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FGK32V VALUE 'F'.                                             
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FGK32A VALUE 'F'.                                             
       01  CPT-FGK32C  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK32T  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK32P  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK33F  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK32V  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK32A  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  FGK34O-ENR   PIC X(200).                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  FGK34O-LENGTH PIC 9(4) COMP.                                         
      *--                                                                       
       01  FGK34O-LENGTH PIC 9(4) COMP-5.                                       
      *}                                                                        
       01  CPT-FGK34C  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK34T  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK34P  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK34F  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK34V  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK34A  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-MOV-CHAMP PIC 9(4) COMP.                                           
      *--                                                                       
       01  I-MOV-CHAMP PIC 9(4) COMP-5.                                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-DEB-CHAMP PIC 9(4) COMP.                                           
      *--                                                                       
       01  I-DEB-CHAMP PIC 9(4) COMP-5.                                         
      *}                                                                        
      ******************************************************************        
      * ABEND et BETDATC                                                        
      ******************************************************************        
           COPY ABENDCOP.                                                       
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *01  ABEND   PIC X(8) VALUE 'ABEND  '.                                    
      *--                                                                       
       01  MW-ABEND   PIC X(8) VALUE 'ABEND  '.                                 
      *}                                                                        
           COPY WORKDATC.                                                       
       01  BETDATC PIC X(8) VALUE 'BETDATC'.                                    
      ******************************************************************        
      * zones pour calcul de semaine KRISP:                                     
      * la date doit �tre fournie dans W-SEM-FDATE, format JJMMSSAA             
      * la semaine KRISP est rendue dans W-SEMAINE-KRISP, format SSAASE         
      ******************************************************************        
       01  W-SEM-FDATE   PIC X(8)     VALUE SPACES.                             
       01  W-SEMAINE-FDATE     PIC X(6).                                        
       01  FILLER REDEFINES W-SEMAINE-FDATE.                                    
           02  P-SEMAINE-FDATE-SSAA PIC 9999.                                   
           02  P-SEMAINE-FDATE-SEM  PIC 99.                                     
       01  W-SEMAINE-1ER-J-MAI PIC X(6).                                        
       01  FILLER REDEFINES W-SEMAINE-1ER-J-MAI.                                
           02  P-SEMAINE-1ER-J-MAI-SSAA PIC 9999.                               
           02  P-SEMAINE-1ER-J-MAI-SEM  PIC 99.                                 
       01  W-SEMAINE-KRISP     PIC X(6).                                        
       01  FILLER REDEFINES W-SEMAINE-KRISP.                                    
           02  P-SEMAINE-KRISP-SSAA PIC 9999.                                   
           02  P-SEMAINE-KRISP-SEM  PIC 99.                                     
       01  W-SEMAINE-DERN-SEM PIC X(6).                                         
       01  FILLER REDEFINES W-SEMAINE-DERN-SEM.                                 
           02  P-SEMAINE-DERN-SEM-SSAA PIC 9999.                                
           02  P-SEMAINE-DERN-SEM-SEM  PIC 99.                                  
       01  X-SEM-SMN PIC XX.                                                    
       01  P-SEM-SMN REDEFINES X-SEM-SMN PIC 99.                                
       01  FILLER PIC X.                                                        
           88 A-1-BISS     VALUE 'O'.                                           
           88 A-1-NON-BISS VALUE 'N'.                                           
      ******************************************************************        
      * Proc: D�but, Milieu, Fin.                                               
      ******************************************************************        
       PROCEDURE DIVISION.                                                      
           PERFORM  DEBUT-BGK340.                                               
           PERFORM  TRAIT-BGK340.                                               
           PERFORM  FIN-BGK340.                                                 
      ******************************************************************        
      * Ouverture de fichiers, param�tres: FDATE.                               
      ******************************************************************        
       DEBUT-BGK340 SECTION.                                                    
           DISPLAY 'BGK340: Fichiers pour KRISP'                                
           OPEN INPUT FDATE FPARAM FGK340                                       
                             FGK32C FGK32T FGK32P FGK33F FGK32V FGK32A          
               OUTPUT FGK341                                                    
                      FGK34E FGK34C FGK34T FGK34P FGK34F FGK34V FGK34A          
0212                  FDCFT.                                                    
           READ FDATE INTO GFJJMMSSAA AT END                            01090024
                MOVE 'FICHIER FDATE VIDE' TO ABEND-MESS                 01100024
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           CLOSE FDATE.                                                 01130024
           MOVE '1'            TO GFDATA.                               00004640
           CALL BETDATC      USING WORK-BETDATC                         00004670
           IF GFVDAT NOT = '1'                                          00004680
              STRING 'BETDATC : ' GF-MESS-ERR                           00004700
              DELIMITED BY SIZE INTO ABEND-MESS                         00004710
              PERFORM ABEND-PROGRAMME                                           
           END-IF.                                                      00004730
           DISPLAY 'BGK340: date param�tre lue sur FDATE: '                     
                    GFSMN-LIB-L ' ' GFJOUR ' '                                  
                    GFMOI-LIB-L ' ' GFSIECLE GFANNEE.                           
      ******************************************************************        
      * Calcul de la semaine KRISP.                                             
      ******************************************************************        
           MOVE GFJJMMSSAA TO W-SEM-FDATE.                                      
           PERFORM CALCUL-SEMAINE-KRISP.                                        
           MOVE W-SEMAINE-KRISP TO W-SEMAINE.                                   
           DISPLAY '        semaine: ' W-SEMAINE(5:2)                           
                   ', ann�e: ' W-SEMAINE(1:4).                                  
0212  ******************************************************************        
"     * Cr�ation de la sysin pour CFT                                           
"     ******************************************************************        
"     *    MOVE 'SEND PART=XFBPRO,' TO FDCFT-ENR                                
"     *    WRITE ENR-FDCFT FROM FDCFT-ENR                                       
"     *    MOVE SPACES TO FDCFT-ENR                                             
"     *    MOVE '  IDF=FKRISPW,' TO FDCFT-ENR                                   
"     *    WRITE ENR-FDCFT FROM FDCFT-ENR                                       
"     *    MOVE SPACES TO FDCFT-ENR                                             
"     *    MOVE '  FNAME=PXX0.F99.FKRISPW,' TO FDCFT-ENR                        
"     *    WRITE ENR-FDCFT FROM FDCFT-ENR                                       
"     *    MOVE SPACES TO FDCFT-ENR                                             
"     *    STRING '  NFNAME=WN_004_'                                            
"     *           P-SEMAINE-KRISP-SEM                                           
"     *           P-SEMAINE-KRISP-SSAA                                          
"     *           '.ZIP'                                                        
"     *    DELIMITED BY SIZE INTO FDCFT-ENR                                     
0212  *    WRITE ENR-FDCFT FROM FDCFT-ENR                                       
0713       MOVE '10.135.2.114'       TO FDCFT-ENR                               
"          WRITE ENR-FDCFT FROM FDCFT-ENR                                       
"          MOVE SPACES TO FDCFT-ENR                                             
"          MOVE 'IPO1GTW'            TO FDCFT-ENR                               
"          WRITE ENR-FDCFT FROM FDCFT-ENR                                       
"          MOVE SPACES TO FDCFT-ENR                                             
"          MOVE 'IPO1GTW'            TO FDCFT-ENR                               
"          WRITE ENR-FDCFT FROM FDCFT-ENR                                       
"          MOVE SPACES TO FDCFT-ENR                                             
"          MOVE 'LOCSITE SBD=TCPIP1.XLATE2.TCPXLBIN' TO FDCFT-ENR               
"          WRITE ENR-FDCFT FROM FDCFT-ENR                                       
"          MOVE SPACES TO FDCFT-ENR                                             
"          MOVE 'BIN'                TO FDCFT-ENR                               
"          WRITE ENR-FDCFT FROM FDCFT-ENR                                       
"          MOVE SPACES TO FDCFT-ENR                                             
"          STRING 'PUT ''PXX0.F99.FKRISPW'''                                    
"                 ' FKRISPW_WN_004_'                                            
"                 P-SEMAINE-KRISP-SEM                                           
"                 P-SEMAINE-KRISP-SSAA                                          
"                 '.ZIP'                                                        
"          DELIMITED BY SIZE INTO FDCFT-ENR                                     
"          WRITE ENR-FDCFT FROM FDCFT-ENR                                       
"          MOVE SPACES TO FDCFT-ENR                                             
"          MOVE 'QUIT'               TO FDCFT-ENR                               
0713       WRITE ENR-FDCFT FROM FDCFT-ENR                                       
      ******************************************************************        
      * FPARAM: devise                                                          
      ******************************************************************        
           READ FPARAM INTO FPARAM-ENR AT END                           01090024
                MOVE 'Fichier FPARAM vide' TO ABEND-MESS                01100024
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           CLOSE FPARAM.                                                01130024
           DISPLAY 'BGK340: Code devise lu sur FPARAM: '                        
                    FPARAM-CDEVISE ' ' FPARAM-LDEVISE.                          
      ******************************************************************        
      * Num�ro de version.                                                      
      ******************************************************************        
           READ FGK340 INTO FGK340-ENR AT END                           01090024
               MOVE W-SEMAINE TO FGK340-SEMAINE                                 
               MOVE ZERO      TO FGK340-VERSION                                 
           END-READ.                                                            
           IF W-SEMAINE = FGK340-SEMAINE                                        
              ADD 1 TO FGK340-VERSION                                           
           ELSE                                                                 
              MOVE 1 TO FGK340-VERSION                                          
              MOVE W-SEMAINE TO FGK340-SEMAINE                                  
           END-IF.                                                              
           WRITE ENR-FGK341 FROM FGK340-ENR.                            01090024
           CLOSE FGK340 FGK341.                                                 
      ******************************************************************        
      * Programme:                                                              
      * - G�n�ration du Fichier ent�te                                          
      * - Recopie de tous les fichiers                                          
      ******************************************************************        
       TRAIT-BGK340 SECTION.                                                    
           PERFORM ECRITURE-FGK34E.                                             
           PERFORM LECTURE-FGK32C.                                              
           PERFORM UNTIL FIN-FGK32C                                             
              PERFORM ECRITURE-FGK34C                                           
              PERFORM LECTURE-FGK32C                                            
           END-PERFORM.                                                         
           PERFORM LECTURE-FGK32T.                                              
           PERFORM UNTIL FIN-FGK32T                                             
              PERFORM ECRITURE-FGK34T                                           
              PERFORM LECTURE-FGK32T                                            
           END-PERFORM.                                                         
           PERFORM LECTURE-FGK32P.                                              
           PERFORM UNTIL FIN-FGK32P                                             
              PERFORM ECRITURE-FGK34P                                           
              PERFORM LECTURE-FGK32P                                            
           END-PERFORM.                                                         
           PERFORM LECTURE-FGK33F.                                              
           PERFORM UNTIL FIN-FGK33F                                             
              PERFORM ECRITURE-FGK34F                                           
              PERFORM LECTURE-FGK33F                                            
           END-PERFORM.                                                         
           PERFORM LECTURE-FGK32V.                                              
           PERFORM UNTIL FIN-FGK32V                                             
              PERFORM ECRITURE-FGK34V                                           
              PERFORM LECTURE-FGK32V                                            
           END-PERFORM.                                                         
           PERFORM LECTURE-FGK32A.                                              
           PERFORM UNTIL FIN-FGK32A                                             
              PERFORM ECRITURE-FGK34A                                           
              PERFORM LECTURE-FGK32A                                            
           END-PERFORM.                                                         
      ******************************************************************        
      * Ecriture sur FGK34E: entete                                             
      ******************************************************************        
       ECRITURE-FGK34E SECTION.                                                 
           ACCEPT W-DATE FROM DATE.                                             
           ACCEPT W-TIME FROM TIME.                                             
           MOVE SPACES TO FGK34E-DATETIME.                                      
           STRING W-DATE(5:2) '/' W-DATE(3:2) '/20' W-DATE(1:2)                 
                  ' ' W-TIME(1:2) ':' W-TIME(3:2) ':' W-TIME(5:2)               
                 DELIMITED BY SIZE INTO FGK34E-DATETIME.                        
           STRING W-SEMAINE(5:2) '-' W-SEMAINE(1:4)                             
                 DELIMITED BY SIZE INTO FGK34E-DATE.                            
           MOVE FPARAM-CDEVISE TO FGK34E-CDEVISE.                               
           MOVE FPARAM-LDEVISE TO FGK34E-LDEVISE.                               
           MOVE FGK34E-ENR     TO FGK34I-ENR.                                   
           PERFORM CONDENSATION-FGK34O.                                         
           WRITE ENR-FGK34E FROM FGK34O-ENR.                                    
           ADD 1 TO CPT-FGK34E.                                                 
      ******************************************************************        
      * lecture FGK32C: Contr�le.                                               
      ******************************************************************        
       LECTURE-FGK32C SECTION.                                                  
           READ FGK32C INTO FGK34I-ENR                                          
             AT END                                                             
                SET FIN-FGK32C TO TRUE                                          
             NOT AT END                                                         
                ADD 1 TO CPT-FGK32C                                             
           END-READ.                                                            
      ******************************************************************        
      * lecture FGK32T: Typologie.                                              
      ******************************************************************        
       LECTURE-FGK32T SECTION.                                                  
           READ FGK32T INTO FGK34I-ENR                                          
             AT END                                                             
                SET FIN-FGK32T TO TRUE                                          
             NOT AT END                                                         
                ADD 1 TO CPT-FGK32T                                             
           END-READ.                                                            
      ******************************************************************        
      * lecture FGK32P: Produits.                                               
      ******************************************************************        
       LECTURE-FGK32P SECTION.                                                  
           READ FGK32P INTO FGK34I-ENR                                          
             AT END                                                             
                SET FIN-FGK32P TO TRUE                                          
             NOT AT END                                                         
                ADD 1 TO CPT-FGK32P                                             
           END-READ.                                                            
      ******************************************************************        
      * lecture FGK33F: Fournisseurs.                                           
      ******************************************************************        
       LECTURE-FGK33F SECTION.                                                  
           READ FGK33F INTO FGK34I-ENR                                          
             AT END                                                             
                SET FIN-FGK33F TO TRUE                                          
             NOT AT END                                                         
                ADD 1 TO CPT-FGK33F                                             
           END-READ.                                                            
      ******************************************************************        
      * lecture FGK32V: Ventes.                                                 
      ******************************************************************        
       LECTURE-FGK32V SECTION.                                                  
           READ FGK32V INTO FGK34I-ENR                                          
             AT END                                                             
                SET FIN-FGK32V TO TRUE                                          
             NOT AT END                                                         
                ADD 1 TO CPT-FGK32V                                             
           END-READ.                                                            
      ******************************************************************        
      * lecture FGK32A: Achats.                                                 
      ******************************************************************        
       LECTURE-FGK32A SECTION.                                                  
           READ FGK32A INTO FGK34I-ENR                                          
             AT END                                                             
                SET FIN-FGK32A TO TRUE                                          
             NOT AT END                                                         
                ADD 1 TO CPT-FGK32A                                             
           END-READ.                                                            
      ******************************************************************        
      * Ecriture sur FGK34C: Contr�le                                           
      ******************************************************************        
       ECRITURE-FGK34C SECTION.                                                 
           PERFORM VARYING I-DEB-CHAMP FROM 1 BY 1                              
             UNTIL I-DEB-CHAMP > 50                                             
               IF FGK34I-ENR(I-DEB-CHAMP:7) = '_01.txt'                         
                  ADD 1 TO I-DEB-CHAMP                                          
                  MOVE FGK340-VERSION TO FGK34I-ENR(I-DEB-CHAMP:2)              
               END-IF                                                           
           END-PERFORM.                                                         
           PERFORM CONDENSATION-FGK34O.                                         
           WRITE ENR-FGK34C FROM FGK34O-ENR.                                    
           ADD 1 TO CPT-FGK34C.                                                 
      ******************************************************************        
      * Ecriture sur FGK34T: Typologie                                          
      ******************************************************************        
       ECRITURE-FGK34T SECTION.                                                 
           PERFORM CONDENSATION-FGK34O.                                         
           WRITE ENR-FGK34T FROM FGK34O-ENR.                                    
           ADD 1 TO CPT-FGK34T.                                                 
      ******************************************************************        
      * Ecriture sur FGK34P: Produits                                           
      ******************************************************************        
       ECRITURE-FGK34P SECTION.                                                 
           PERFORM CONDENSATION-FGK34O.                                         
           WRITE ENR-FGK34P FROM FGK34O-ENR.                                    
           ADD 1 TO CPT-FGK34P.                                                 
      ******************************************************************        
      * Ecriture sur FGK34F: Fournisseurs                                       
      ******************************************************************        
       ECRITURE-FGK34F SECTION.                                                 
           PERFORM CONDENSATION-FGK34O.                                         
           WRITE ENR-FGK34F FROM FGK34O-ENR.                                    
           ADD 1 TO CPT-FGK34F.                                                 
      ******************************************************************        
      * Ecriture sur FGK34V: Ventes                                             
      ******************************************************************        
       ECRITURE-FGK34V SECTION.                                                 
           PERFORM CONDENSATION-FGK34O.                                         
           WRITE ENR-FGK34V FROM FGK34O-ENR.                                    
           ADD 1 TO CPT-FGK34V.                                                 
      ******************************************************************        
      * Ecriture sur FGK34A: Achats                                             
      ******************************************************************        
       ECRITURE-FGK34A SECTION.                                                 
           PERFORM CONDENSATION-FGK34O.                                         
           WRITE ENR-FGK34A FROM FGK34O-ENR.                                    
           ADD 1 TO CPT-FGK34A.                                                 
      ******************************************************************        
      * Condensation de FGK34O-ENR (a partir de FGK34I-ENR).                    
      ******************************************************************        
       CONDENSATION-FGK34O SECTION.                                             
           MOVE FGK34I-ENR TO FGK34O-ENR.                                       
           MOVE 1 TO I-DEB-CHAMP.                                               
           PERFORM UNTIL FGK34O-ENR(I-DEB-CHAMP:) = ' '                         
              IF FGK34O-ENR(I-DEB-CHAMP:1) = ','                                
                 ADD 1 TO I-DEB-CHAMP                                           
              ELSE                                                              
                 IF FGK34O-ENR(I-DEB-CHAMP:1) = '"'                             
                    PERFORM CONDENSATION-FGK34O-ALPHA                           
                 ELSE                                                           
                    PERFORM CONDENSATION-FGK34O-NUM                             
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
           COMPUTE FGK34O-LENGTH = I-DEB-CHAMP - 1.                             
       CONDENSATION-FGK34O-ALPHA SECTION.                                       
     *** On vire les espaces � gauche...                                        
           ADD 1 TO I-DEB-CHAMP.                                                
           PERFORM UNTIL FGK34O-ENR(I-DEB-CHAMP:1) > ' '                        
              COMPUTE I-MOV-CHAMP = I-DEB-CHAMP + 1                             
              MOVE FGK34O-ENR(I-MOV-CHAMP:) TO FGK34I-ENR(I-DEB-CHAMP:)         
              MOVE FGK34I-ENR TO FGK34O-ENR                                     
           END-PERFORM.                                                         
     *** ...on cherche la fin du champ...                                       
           PERFORM VARYING I-DEB-CHAMP FROM I-DEB-CHAMP BY 1                    
             UNTIL FGK34O-ENR(I-DEB-CHAMP:1) = '"'                              
           END-PERFORM.                                                         
     *** ...on cherche le dernier caract�re � droite...                         
           SUBTRACT 1 FROM I-DEB-CHAMP.                                         
           PERFORM VARYING I-DEB-CHAMP FROM I-DEB-CHAMP BY -1                   
             UNTIL FGK34O-ENR(I-DEB-CHAMP:1) > ' '                              
           END-PERFORM.                                                         
           ADD 1 TO I-DEB-CHAMP.                                                
     *** ...on vire les espaces � droite...                                     
           PERFORM UNTIL FGK34O-ENR(I-DEB-CHAMP:1) > ' '                        
              COMPUTE I-MOV-CHAMP = I-DEB-CHAMP + 1                             
              MOVE FGK34O-ENR(I-MOV-CHAMP:) TO FGK34I-ENR(I-DEB-CHAMP:)         
              MOVE FGK34I-ENR TO FGK34O-ENR                                     
           END-PERFORM.                                                         
     *** ...et on se positionne sur le prochain champ                           
           ADD 1 TO I-DEB-CHAMP.                                                
       CONDENSATION-FGK34O-NUM SECTION.                                         
     *** On vire les espaces � gauche...                                        
           PERFORM UNTIL FGK34O-ENR(I-DEB-CHAMP:1) > ' '                        
              COMPUTE I-MOV-CHAMP = I-DEB-CHAMP + 1                             
              MOVE FGK34O-ENR(I-MOV-CHAMP:) TO FGK34I-ENR(I-DEB-CHAMP:)         
              MOVE FGK34I-ENR TO FGK34O-ENR                                     
           END-PERFORM.                                                         
     *** ...on cherche la fin du champ...                                       
           PERFORM VARYING I-DEB-CHAMP FROM I-DEB-CHAMP BY 1                    
             UNTIL FGK34O-ENR(I-DEB-CHAMP:1) = ','                              
                OR FGK34O-ENR(I-DEB-CHAMP:) = ' '                               
           END-PERFORM.                                                         
     *** ...on cherche le dernier caract�re � droite...                         
           SUBTRACT 1 FROM I-DEB-CHAMP.                                         
           PERFORM VARYING I-DEB-CHAMP FROM I-DEB-CHAMP BY -1                   
             UNTIL FGK34O-ENR(I-DEB-CHAMP:1) > ' '                              
           END-PERFORM.                                                         
     *** ...et on vire les espaces � droite...                                  
           ADD 1 TO I-DEB-CHAMP.                                                
           PERFORM UNTIL FGK34O-ENR(I-DEB-CHAMP:1) > ' '                        
                      OR FGK34O-ENR(I-DEB-CHAMP:) = ' '                         
              COMPUTE I-MOV-CHAMP = I-DEB-CHAMP + 1                             
              MOVE FGK34O-ENR(I-MOV-CHAMP:) TO FGK34I-ENR(I-DEB-CHAMP:)         
              MOVE FGK34I-ENR TO FGK34O-ENR                                     
           END-PERFORM.                                                         
     *** (on est positionn� sur la virgule de fin de champ)                     
      ******************************************************************        
      * C'est la fin des Haricots.                                              
      ******************************************************************        
       FIN-BGK340 SECTION.                                                      
           CLOSE        FGK32C FGK32T FGK32P FGK33F FGK32V FGK32A               
                 FGK34E FGK34C FGK34T FGK34P FGK34F FGK34V FGK34A.              
           DISPLAY 'BGK340: Fin normale de programme'.                          
           MOVE CPT-FGK32C TO PIC-EDITZ9.                                       
           DISPLAY '        Nb. enregs lus sur FGK32C: ' PIC-EDITZ9.            
           MOVE CPT-FGK32T TO PIC-EDITZ9.                                       
           DISPLAY '        Nb. enregs lus sur FGK32T: ' PIC-EDITZ9.            
           MOVE CPT-FGK32T TO PIC-EDITZ9.                                       
           DISPLAY '        Nb. enregs lus sur FGK32P: ' PIC-EDITZ9.            
           MOVE CPT-FGK33F TO PIC-EDITZ9.                                       
           DISPLAY '        Nb. enregs lus sur FGK33F: ' PIC-EDITZ9.            
           MOVE CPT-FGK32V TO PIC-EDITZ9.                                       
           DISPLAY '        Nb. enregs lus sur FGK32V: ' PIC-EDITZ9.            
           MOVE CPT-FGK32A TO PIC-EDITZ9.                                       
           DISPLAY '        Nb. enregs lus sur FGK32A: ' PIC-EDITZ9.            
           MOVE CPT-FGK34E TO PIC-EDITZ9.                                       
           DISPLAY '        Nb. enregs �crits sur FGK34E: ' PIC-EDITZ9.         
           MOVE CPT-FGK34C TO PIC-EDITZ9.                                       
           DISPLAY '        Nb. enregs �crits sur FGK34C: ' PIC-EDITZ9.         
           MOVE CPT-FGK34T TO PIC-EDITZ9.                                       
           DISPLAY '        Nb. enregs �crits sur FGK34T: ' PIC-EDITZ9.         
           MOVE CPT-FGK34P TO PIC-EDITZ9.                                       
           DISPLAY '        Nb. enregs �crits sur FGK34P: ' PIC-EDITZ9.         
           MOVE CPT-FGK34F TO PIC-EDITZ9.                                       
           DISPLAY '        Nb. enregs �crits sur FGK34F: ' PIC-EDITZ9.         
           MOVE CPT-FGK34V TO PIC-EDITZ9.                                       
           DISPLAY '        Nb. enregs �crits sur FGK34V: ' PIC-EDITZ9.         
           MOVE CPT-FGK34A TO PIC-EDITZ9.                                       
           DISPLAY '        Nb. enregs �crits sur FGK34A: ' PIC-EDITZ9.         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      ******************************************************************        
      * Proc de calcul de semaine KRISP:                                        
      * la date doit �tre fournie dans W-SEM-FDATE, format JJMMSSAA             
      * la semaine KRISP est rendue dans W-SEMAINE-KRISP, format SSAASE         
      * remarque: BETDATC est appel� plusieurs fois, mais WORKDATC est          
      *           restaur�e � la fin � la date W-SEM-FDATE.                     
      ******************************************************************        
      ******************************************************************        
      * >>>>>>>>>>>>>>>>>>>>>> D�but modifs AD05 <<<<<<<<<<<<<<<<<<<<<<         
      ******************************************************************        
      *CALCUL-SEMAINE-KRISP SECTION.                                            
      * Calcul de la date FDATE                                                 
      *    MOVE W-SEM-FDATE TO GFJJMMSSAA.                                      
      *    PERFORM APPEL-BETDATC-SEM.                                           
      *    MOVE GFWEEK TO W-SEMAINE-FDATE.                                      
      * Est-ce que l'ann�e pr�c�dente �tait bissextile?                         
      *    PERFORM APPEL-BETDATC-A-1.                                           
      * Calcul du 1er jeudi de f�vrier de l'ann�e de la semaine FDATE           
      *    MOVE W-SEMAINE-FDATE TO GFJJMMSSAA(5:).                              
      *    MOVE '0102' TO GFJJMMSSAA(1:4).                                      
      *    MOVE ZERO TO GFSMN.                                                  
      *    PERFORM VARYING P-SEM-SMN FROM 1 BY 1                                
      *              UNTIL (P-SEM-SMN > 7) OR (GFSMN = 4)                       
      *       MOVE X-SEM-SMN TO GFJJMMSSAA(1:2)                                 
      *       PERFORM APPEL-BETDATC-SEM                                         
      *    END-PERFORM.                                                         
      *    MOVE GFWEEK TO W-SEMAINE-1ER-J-FEV.                                  
      * Si l'ann�e -1 �tait bissextile, on recherche le 2�me jeudi              
      *    IF A-1-BISS                                                          
      *       ADD 1 TO P-SEM-SMN                                                
      *       MOVE ZERO TO GFSMN                                                
      *       PERFORM VARYING P-SEM-SMN FROM P-SEM-SMN BY 1                     
      *                 UNTIL (P-SEM-SMN > 15) OR (GFSMN = 4)                   
      *          MOVE X-SEM-SMN TO GFJJMMSSAA(1:2)                              
      *          PERFORM APPEL-BETDATC-SEM                                      
      *       END-PERFORM                                                       
      *       MOVE GFWEEK TO W-SEMAINE-1ER-J-FEV                                
      *    END-IF.                                                              
      * si la semaine FDATE >= 1er jeudi (ou 2�me), la semaine anglaise         
      * est �gale �: SemFDATE - Sem1erJeudi + 1                                 
      *    IF W-SEMAINE-FDATE >= W-SEMAINE-1ER-J-FEV                            
      *       MOVE W-SEMAINE-FDATE TO W-SEMAINE-KRISP                           
      *       COMPUTE P-SEMAINE-KRISP-SEM =                                     
      *               P-SEMAINE-FDATE-SEM                                       
      *             - P-SEMAINE-1ER-J-FEV-SEM                                   
      *             + 1                                                         
      *    ELSE                                                                 
      * sinon, les choses se corsent....                                        
      *       PERFORM CALCUL-SEMAINE-ANNEE-MOINS-UN                             
      *    END-IF.                                                              
      * reset de WORKDATC � W-SEM-FDATE.                                        
      *    MOVE W-SEM-FDATE TO GFJJMMSSAA.                                      
      *    PERFORM APPEL-BETDATC-SEM.                                           
      ***************************************************************           
      * Calcul alors qu'on est d�cal� d'un an...                                
      ***************************************************************           
      *CALCUL-SEMAINE-ANNEE-MOINS-UN SECTION.                                   
      * On se positionne au 1er f�vrier de l'ann�e -1                           
      *    MOVE GFWEEK TO W-SEMAINE-1ER-J-FEV.                                  
      *    SUBTRACT 1 FROM P-SEMAINE-1ER-J-FEV-SSAA                             
      *    MOVE W-SEMAINE-1ER-J-FEV TO GFJJMMSSAA(5:).                          
      *    MOVE '0102' TO GFJJMMSSAA(1:4).                                      
      *    PERFORM APPEL-BETDATC-SEM                                            
      * Est-ce que l'ann�e pr�c�dente (donc -2) �tait bissextile?               
      *    PERFORM APPEL-BETDATC-A-1.                                           
      * Calcul du 1er jeudi de f�vrier de l'ann�e -1                            
      *    MOVE W-SEMAINE-1ER-J-FEV TO GFJJMMSSAA(5:).                          
      *    MOVE '0102' TO GFJJMMSSAA(1:4).                                      
      *    MOVE ZERO TO GFSMN.                                                  
      *    PERFORM VARYING P-SEM-SMN FROM 1 BY 1                                
      *              UNTIL (P-SEM-SMN > 7) OR (GFSMN = 4)                       
      *       MOVE X-SEM-SMN TO GFJJMMSSAA(1:2)                                 
      *       PERFORM APPEL-BETDATC-SEM                                         
      *    END-PERFORM.                                                         
      *    MOVE GFWEEK TO W-SEMAINE-1ER-J-FEV.                                  
      * Si l'ann�e -2 �tait bissextile, on recherche le 2�me jeudi              
      *    IF A-1-BISS                                                          
      *       ADD 1 TO P-SEM-SMN                                                
      *       MOVE ZERO TO GFSMN                                                
      *       PERFORM VARYING P-SEM-SMN FROM P-SEM-SMN BY 1                     
      *                 UNTIL (P-SEM-SMN > 15) OR (GFSMN = 4)                   
      *          MOVE X-SEM-SMN TO GFJJMMSSAA(1:2)                              
      *          PERFORM APPEL-BETDATC-SEM                                      
      *       END-PERFORM                                                       
      *       MOVE GFWEEK TO W-SEMAINE-1ER-J-FEV                                
      *    END-IF.                                                              
      * Calcul de la derni�re semaine de l'ann�e                                
      *    MOVE SPACES TO GFWEEK.                                               
      *    MOVE 12     TO GFMM.                                                 
      *    PERFORM VARYING GFJJ FROM 31 BY -1                                   
      *              UNTIL (GFSEMSS = GFSS) AND (GFSEMAA = GFAA)                
      *       PERFORM APPEL-BETDATC-SEM                                         
      *    END-PERFORM.                                                         
      *    MOVE GFWEEK TO W-SEMAINE-DERN-SEM.                                   
      * la semaine anglaise est �gale �:                                        
      *   SemFDATE + DernSem - Sem1erJeudi + 1                                  
      *    MOVE W-SEMAINE-1ER-J-FEV TO W-SEMAINE-KRISP.                         
      *    COMPUTE P-SEMAINE-KRISP-SEM =                                        
      *            P-SEMAINE-FDATE-SEM                                          
      *          + P-SEMAINE-DERN-SEM-SEM                                       
      *          - P-SEMAINE-1ER-J-FEV-SEM                                      
      *          + 1.                                                           
      *                                                                         
       CALCUL-SEMAINE-KRISP SECTION.                                            
      * Calcul de la date FDATE                                                 
           MOVE W-SEM-FDATE TO GFJJMMSSAA.                                      
           PERFORM APPEL-BETDATC-SEM.                                           
           MOVE GFWEEK TO W-SEMAINE-FDATE.                                      
      * Calcul du 1er jeudi de f�vrier de l'ann�e de la semaine FDATE           
           MOVE W-SEMAINE-FDATE TO GFJJMMSSAA(5:).                              
           MOVE '0105' TO GFJJMMSSAA(1:4).                                      
           MOVE ZERO TO GFSMN.                                                  
           PERFORM VARYING P-SEM-SMN FROM 1 BY 1                                
                     UNTIL (P-SEM-SMN > 7) OR (GFSMN = 4)                       
              MOVE X-SEM-SMN TO GFJJMMSSAA(1:2)                                 
              PERFORM APPEL-BETDATC-SEM                                         
           END-PERFORM.                                                         
           MOVE GFWEEK TO W-SEMAINE-1ER-J-MAI.                                  
      *                                                                         
      * Si la semaine FDATE >= 1er jeudi, la semaine anglaise                   
      * est �gale �: SemFDATE - Sem1erJeudi + 1                                 
FQ    * la r�gle de calcul de la 1�re semaine de                                
FQ    * l'exercice n'est pas valable pour cette ann�e                           
FQ    * SemFDATE - Sem1erJeudi + 2                                              
           IF W-SEMAINE-FDATE >= W-SEMAINE-1ER-J-MAI                            
              MOVE W-SEMAINE-FDATE TO W-SEMAINE-KRISP                           
              COMPUTE P-SEMAINE-KRISP-SEM =                                     
                      P-SEMAINE-FDATE-SEM                                       
                    - P-SEMAINE-1ER-J-MAI-SEM                                   
EC                  + 1                                                         
FQ2   *             + 2                                                         
FQ                                                                              
FQ2           IF P-SEMAINE-KRISP-SEM = 53                                       
FQ               MOVE 01 TO P-SEMAINE-KRISP-SEM                                 
FQ               ADD 1 TO  P-SEMAINE-KRISP-SSAA                                 
FQ            END-IF                                                            
           ELSE                                                                 
      * sinon, les choses se corsent....                                        
              PERFORM CALCUL-SEMAINE-ANNEE-MOINS-UN                             
           END-IF.                                                              
      * reset de WORKDATC � W-SEM-FDATE.                                        
           MOVE W-SEM-FDATE TO GFJJMMSSAA.                                      
           PERFORM APPEL-BETDATC-SEM.                                           
      ***************************************************************           
      * Calcul alors qu'on est d�cal� d'un an...                                
      ***************************************************************           
       CALCUL-SEMAINE-ANNEE-MOINS-UN SECTION.                                   
      * On se positionne au 1er f�vrier de l'ann�e -1                           
           MOVE GFWEEK TO W-SEMAINE-1ER-J-MAI.                                  
           SUBTRACT 1 FROM P-SEMAINE-1ER-J-MAI-SSAA                             
           MOVE W-SEMAINE-1ER-J-MAI TO GFJJMMSSAA(5:).                          
           MOVE '0105' TO GFJJMMSSAA(1:4).                                      
           PERFORM APPEL-BETDATC-SEM                                            
      * Calcul du 1er jeudi de f�vrier de l'ann�e -1                            
           MOVE W-SEMAINE-1ER-J-MAI TO GFJJMMSSAA(5:).                          
           MOVE '0105' TO GFJJMMSSAA(1:4).                                      
           MOVE ZERO TO GFSMN.                                                  
           PERFORM VARYING P-SEM-SMN FROM 1 BY 1                                
                     UNTIL (P-SEM-SMN > 7) OR (GFSMN = 4)                       
              MOVE X-SEM-SMN TO GFJJMMSSAA(1:2)                                 
              PERFORM APPEL-BETDATC-SEM                                         
           END-PERFORM.                                                         
           MOVE GFWEEK TO W-SEMAINE-1ER-J-MAI.                                  
      * Calcul de la derni�re semaine de l'ann�e                                
           MOVE SPACES TO GFWEEK.                                               
           MOVE 12     TO GFMM.                                                 
           PERFORM VARYING GFJJ FROM 31 BY -1                                   
                     UNTIL (GFSEMSS = GFSS) AND (GFSEMAA = GFAA)                
              PERFORM APPEL-BETDATC-SEM                                         
           END-PERFORM.                                                         
           MOVE GFWEEK TO W-SEMAINE-DERN-SEM.                                   
      * La semaine anglaise est �gale �:                                        
      *   SemFDATE + DernSem - Sem1erJeudi + 1                                  
FQ    * la r�gle de calcul de la 1�re semaine de                                
FQ    * l'exercice n'est pas valable pour cette ann�e                           
FQ    * SemFDATE - Sem1erJeudi + 2                                              
           MOVE W-SEMAINE-1ER-J-MAI TO W-SEMAINE-KRISP.                         
           COMPUTE P-SEMAINE-KRISP-SEM =                                        
                   P-SEMAINE-FDATE-SEM                                          
                 + P-SEMAINE-DERN-SEM-SEM                                       
                 - P-SEMAINE-1ER-J-MAI-SEM                                      
EC               + 1                                                            
FQ2   *          + 2                                                            
FQ                                                                              
FQ2        IF P-SEMAINE-KRISP-SEM = 53                                          
FQ            MOVE 01 TO P-SEMAINE-KRISP-SEM                                    
FQ            ADD 1 TO  P-SEMAINE-KRISP-SSAA                                    
FQ         END-IF                                                               
           .                                                                    
      ******************************************************************        
      * >>>>>>>>>>>>>>>>>>>>>>> Fin modifs AD05 <<<<<<<<<<<<<<<<<<<<<<<         
      ******************************************************************        
      ***************************************************************           
      * Appel BETDATC                                                           
      ***************************************************************           
       APPEL-BETDATC-SEM SECTION.                                               
           MOVE '1'            TO GFDATA.                               00004640
           CALL BETDATC      USING WORK-BETDATC                         00004670
           IF GFVDAT NOT = '1'                                          00004680
              DISPLAY 'BETDATC : ' GF-MESS-ERR                          00004700
           END-IF.                                                      00004730
      ***************************************************************           
      * Appel BETDATC Ann�e -1                                                  
      ***************************************************************           
       APPEL-BETDATC-A-1 SECTION.                                               
           MOVE 'B'            TO GFDATA.                               00004640
           MOVE '12'           TO GFAJOUX.                              00004640
           CALL BETDATC      USING WORK-BETDATC                         00004670
           IF GFVDAT NOT = '1'                                          00004680
              DISPLAY 'BETDATC : ' GF-MESS-ERR                          00004700
           END-IF.                                                      00004730
           IF GFBISS = 1                                                        
              SET A-1-BISS TO TRUE                                              
           ELSE                                                                 
              SET A-1-NON-BISS TO TRUE                                          
           END-IF.                                                      00004730
      ******************************************************************        
      * BadaBoum                                                                
      ******************************************************************        
       ABEND-PROGRAMME SECTION.                                                 
           DISPLAY ABEND-MESS                                                   
           MOVE 'BGK340' TO ABEND-PROG.                                         
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL ABEND USING ABEND-PROG ABEND-MESS.                              
      *--                                                                       
           CALL MW-ABEND USING ABEND-PROG ABEND-MESS.                           
      *}                                                                        
