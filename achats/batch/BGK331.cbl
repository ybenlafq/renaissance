      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. BGK331.                                                      
       AUTHOR. JG.                                                              
      ******************************************************************        
      *                                                                         
      *  Fournisseurs a envoyer a KRISP:                                        
      *                                                                         
      ******************************************************************        
      *  Modif JB (DSA053) le 23/12/03 on n'envoie plus que le d�but du         
      *                    fichier (nentcde et lentcde)                         
      ******************************************************************        
      * MODIFICATIONS :                                                *        
      * ---------------                                                *        
      * A. DURAND - 16/02/2005 : CALCUL DE LA SEMAINE ERRONE           *        
      *                          REFERENCE 'AD05' EN COL 1             *        
      ******************************************************************        
      * Modif le 18022008 FQ : DSA026                                           
      * la r�gle de calcul de la 1�re semaine de                                
      * l'exercice n'est pas valable pour cette ann�e ( FQ en col 1 )           
      ******************************************************************        
      * Modif le 05052008 FQ : DSA026                                           
      * la r�gle de calcul de la 1�re semaine de                                
      * change car le d�but d'exercice est le 01 mai  ( FQ2 en col 1 )          
      ******************************************************************        
      * modif le 18052009 ec : DSA015                                           
      * erreur sur le n� de semaine calcul�    EC en col1                       
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                                   
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FDATE   ASSIGN TO  FDATE.                                   
      *--                                                                       
            SELECT  FDATE   ASSIGN TO  FDATE                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK230  ASSIGN TO  FGK230.                                  
      *--                                                                       
            SELECT  FGK230  ASSIGN TO  FGK230                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK331  ASSIGN TO  FGK331.                                  
      *--                                                                       
            SELECT  FGK331  ASSIGN TO  FGK331                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK332  ASSIGN TO  FGK332.                                  
      *--                                                                       
            SELECT  FGK332  ASSIGN TO  FGK332                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK33C  ASSIGN TO  FGK33C.                                  
      *--                                                                       
            SELECT  FGK33C  ASSIGN TO  FGK33C                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK33F  ASSIGN TO  FGK33F.                                  
      *--                                                                       
            SELECT  FGK33F  ASSIGN TO  FGK33F                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      ******************************************************************        
       DATA DIVISION.                                                           
      ******************************************************************        
       FILE SECTION.                                                            
       FD  FDATE RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.             
       01  MW-FILLER        PIC X(80).                                          
       FD  FGK230 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER        PIC X(160).                                         
       FD  FGK331 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
      *MW DAR-2                                                                 
       01  MW-FILLER        PIC X(160).                                         
       FD  FGK332 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  ENR-FGK332    PIC X(160).                                            
       FD  FGK33C RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  ENR-FGK33C    PIC X(200).                                            
       FD  FGK33F RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  ENR-FGK33F    PIC X(200).                                            
      ******************************************************************        
       WORKING-STORAGE SECTION.                                                 
      ******************************************************************        
      * quelques zones... qui servent - on s'en doute - � quelque chose         
      ******************************************************************        
       01  W-SEMAINE     PIC X(6)     VALUE SPACES.                             
       01  W-MOIS        PIC X(6)     VALUE SPACES.                             
       01  PIC-EDITZ9    PIC ----.---.--9.                                      
       01  PIC-EDITV9    PIC ----.---.--9,99.                                   
      **********************************************************                
      * Fichier FGK230                                                          
      **********************************************************                
           COPY FGK230.                                                         
       01  CPT-FGK230  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FGK230 VALUE 'F'.                                             
      ******************************************************************        
      * FGK331, FGK332: COPIE DE FGK230                                         
      ******************************************************************        
       01  FGK331-ENR.                                                          
   1       02  FGK331-NENTCDE         PIC X(06).                                
   7       02  FGK331-LENTCDE         PIC X(20).                                
  27  *    02  FGK331-LENTCDEADR1     PIC X(32).                                
  59  *    02  FGK331-LENTCDEADR2     PIC X(32).                                
  91  *    02  FGK331-NENTCDEADR3     PIC X(05).                                
  96  *    02  FGK331-LENTCDEADR4     PIC X(26).                                
 122  *    02  FGK331-NENTCDETEL      PIC X(15).                                
 137  *    02  FGK331-CENTCDETELX     PIC X(15).                                
 152  *    02  FILLER                 PIC X(09).                                
 152       02  FILLER                 PIC X(134).                               
 161 *** LRECL=160                                                              
       01  CPT-FGK331  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK332  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FGK331 VALUE 'F'.                                             
      ******************************************************************        
      * FGK33C: Contr�le (19 champs)                                            
      ******************************************************************        
       01  FGK33C-ENR.                                                          
   1       02  FGK33C-NOM             PIC X(40).                                
           02  FILLER                 PIC X(01) VALUE ','.                      
   2       02  FGK33C-NBENR           PIC Z(7)9.                                
3-19       02  FILLER                 PIC X(18) VALUE ALL ','.                  
       01  CPT-FGK33C  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
      ******************************************************************        
      * FGK33F: Fournisseurs (10 champs)                                        
      ******************************************************************        
       01  FGK33F-ENR.                                                          
           02  FILLER                 PIC X(01) VALUE '"'.                      
   1       02  FGK33F-NENTCDE         PIC X(06).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
   2       02  FGK33F-LENTCDE         PIC X(20).                                
JB...      02  FILLER                 PIC X(02) VALUE '",'.                     
...JB      02  FILLER                 PIC X(07) VALUE ',,,,,,,'.                
      *    02  FILLER                 PIC X(03) VALUE '","'.                    
   3  *    02  FGK33F-ADDRESS         PIC X(98).                                
      *    02  FILLER                 PIC X(03) VALUE '","'.                    
   4  *    02  FGK33F-NENTCDETEL      PIC X(15).                                
      *    02  FILLER                 PIC X(03) VALUE '","'.                    
   5  *    02  FGK33F-CENTCDETELX     PIC X(15).                                
      *    02  FILLER                 PIC X(07) VALUE '",,,,,,'.                
       01  CPT-FGK33F  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
      ******************************************************************        
      * ABEND et BETDATC                                                        
      ******************************************************************        
           COPY ABENDCOP.                                                       
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *01  ABEND   PIC X(8) VALUE 'ABEND  '.                                    
      *--                                                                       
       01  MW-ABEND   PIC X(8) VALUE 'ABEND  '.                                 
      *}                                                                        
           COPY WORKDATC.                                                       
       01  BETDATC PIC X(8) VALUE 'BETDATC'.                                    
      ******************************************************************        
      * zones pour calcul de semaine KRISP:                                     
      * la date doit �tre fournie dans W-SEM-FDATE, format JJMMSSAA             
      * la semaine KRISP est rendue dans W-SEMAINE-KRISP, format SSAASE         
      ******************************************************************        
       01  W-SEM-FDATE   PIC X(8)     VALUE SPACES.                             
       01  W-SEMAINE-FDATE     PIC X(6).                                        
       01  FILLER REDEFINES W-SEMAINE-FDATE.                                    
           02  P-SEMAINE-FDATE-SSAA PIC 9999.                                   
           02  P-SEMAINE-FDATE-SEM  PIC 99.                                     
       01  W-SEMAINE-1ER-J-MAI PIC X(6).                                        
       01  FILLER REDEFINES W-SEMAINE-1ER-J-MAI.                                
           02  P-SEMAINE-1ER-J-MAI-SSAA PIC 9999.                               
           02  P-SEMAINE-1ER-J-MAI-SEM  PIC 99.                                 
       01  W-SEMAINE-KRISP     PIC X(6).                                        
       01  FILLER REDEFINES W-SEMAINE-KRISP.                                    
           02  P-SEMAINE-KRISP-SSAA PIC 9999.                                   
           02  P-SEMAINE-KRISP-SEM  PIC 99.                                     
       01  W-SEMAINE-DERN-SEM PIC X(6).                                         
       01  FILLER REDEFINES W-SEMAINE-DERN-SEM.                                 
           02  P-SEMAINE-DERN-SEM-SSAA PIC 9999.                                
           02  P-SEMAINE-DERN-SEM-SEM  PIC 99.                                  
       01  X-SEM-SMN PIC XX.                                                    
       01  P-SEM-SMN REDEFINES X-SEM-SMN PIC 99.                                
       01  FILLER PIC X.                                                        
           88 A-1-BISS     VALUE 'O'.                                           
           88 A-1-NON-BISS VALUE 'N'.                                           
      ******************************************************************        
      * Proc: D�but, Milieu, Fin.                                               
      ******************************************************************        
       PROCEDURE DIVISION.                                                      
           PERFORM  DEBUT-BGK331.                                               
           PERFORM  TRAIT-BGK331.                                               
           PERFORM  FIN-BGK331.                                                 
      ******************************************************************        
      * Ouverture de fichiers, param�tres: FDATE.                               
      ******************************************************************        
       DEBUT-BGK331 SECTION.                                                    
           DISPLAY 'BGK331: Fournisseurs pour KRISP'                            
           OPEN INPUT FDATE FGK230 FGK331                                       
               OUTPUT FGK332 FGK33C FGK33F.                                     
           READ FDATE INTO GFJJMMSSAA AT END                            01090024
                MOVE 'FICHIER FDATE VIDE' TO ABEND-MESS                 01100024
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           CLOSE FDATE.                                                 01130024
           MOVE '1'            TO GFDATA.                               00004640
           CALL BETDATC      USING WORK-BETDATC                         00004670
           IF GFVDAT NOT = '1'                                          00004680
              STRING 'BETDATC : ' GF-MESS-ERR                           00004700
              DELIMITED BY SIZE INTO ABEND-MESS                         00004710
              PERFORM ABEND-PROGRAMME                                           
           END-IF.                                                      00004730
           DISPLAY 'BGK331: date param�tre lue sur FDATE: '                     
                    GFSMN-LIB-L ' ' GFJOUR ' '                                  
                    GFMOI-LIB-L ' ' GFSIECLE GFANNEE.                           
           MOVE GFJJMMSSAA TO W-SEM-FDATE.                                      
           PERFORM CALCUL-SEMAINE-KRISP.                                        
           MOVE W-SEMAINE-KRISP TO W-SEMAINE.                                   
           DISPLAY '        semaine: ' W-SEMAINE(5:2)                           
                   ', ann�e: ' W-SEMAINE(1:4).                                  
      ******************************************************************        
      * Programme:                                                              
      * - Matching des fichiers FGK230 (ce sont les nouveaux fourn.)            
      *   et FGK331 (fournisseurs lors du dernier passage):                     
      *   - s'il n'y a aucune diff�rence entre les deux, on n'envoie            
      *     rien � KRISP                                                        
      *   - s'il y a une diff�rence on envoie le nouvel enr. dans               
      *     le fichier FGK33F.                                                  
      *   - s'il y a un enr. dans FGK230 sans correspondance sur FGK331,        
      *     c'est un nouveau fournisseur: on l'envoie sur FGK33F.               
      *????Le cas suivant n'existe pas: on ne delete pas de fournisseur.        
      *   - s'il y a un enr. dans FGK331 sans correspondance sur FGK230,        
      *     c'est un fournisseur � deleter: on l'envoie                         
      *     avec un FLAG delete � 'Y'.                                          
      *   Quelque soit le cas, le fichier FGK230 est recopi� sur le             
      *   fichier FGK332 (qui sera le FGK331 du prochain passage).              
      ******************************************************************        
       TRAIT-BGK331 SECTION.                                                    
           PERFORM LECTURE-FGK230.                                              
           PERFORM LECTURE-FGK331.                                              
           PERFORM UNTIL FIN-FGK230 AND FIN-FGK331                              
              IF FGK230-NENTCDE = FGK331-NENTCDE                                
                 PERFORM RECHERCHE-DES-DIFFERENCES                              
                 PERFORM LECTURE-FGK230                                         
                 PERFORM LECTURE-FGK331                                         
              ELSE IF FGK230-NENTCDE > FGK331-NENTCDE                           
                    PERFORM FOURN-A-DELETER                                     
                    PERFORM LECTURE-FGK331                                      
                 ELSE                                                           
                    PERFORM FOURN-A-CREER                                       
                    PERFORM LECTURE-FGK230                                      
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
      ******************************************************************        
      * Differences entre FGK230 et FGK331                                      
      ******************************************************************        
       RECHERCHE-DES-DIFFERENCES SECTION.                                       
           IF (FGK230-LENTCDE     NOT = FGK331-LENTCDE    )                     
JB... *    OR (FGK230-LENTCDEADR1 NOT = FGK331-LENTCDEADR1)                     
      *    OR (FGK230-LENTCDEADR2 NOT = FGK331-LENTCDEADR2)                     
      *    OR (FGK230-NENTCDEADR3 NOT = FGK331-NENTCDEADR3)                     
      *    OR (FGK230-LENTCDEADR4 NOT = FGK331-LENTCDEADR4)                     
      *    OR (FGK230-NENTCDETEL  NOT = FGK331-NENTCDETEL )                     
...JB *    OR (FGK230-CENTCDETELX NOT = FGK331-CENTCDETELX)                     
              PERFORM ECRITURE-FGK33F                                           
           END-IF.                                                              
      ******************************************************************        
      * Fournisseur a Virer: on ne fait rien ???(c'est fait expr�s)             
      ******************************************************************        
       FOURN-A-DELETER SECTION.                                                 
           DISPLAY 'FOURN-A-DELETER: ' FGK331-NENTCDE.                          
      ******************************************************************        
      * Fournisseur a Cr�er                                                     
      ******************************************************************        
       FOURN-A-CREER SECTION.                                                   
           PERFORM ECRITURE-FGK33F.                                             
      ******************************************************************        
      * lecture FGK230                                                          
      ******************************************************************        
       LECTURE-FGK230 SECTION.                                                  
           READ FGK230 INTO FGK230-ENR                                          
             AT END                                                             
                SET FIN-FGK230 TO TRUE                                          
                MOVE HIGH-VALUE TO FGK230-NENTCDE                               
             NOT AT END                                                         
                ADD 1 TO CPT-FGK230                                             
                INSPECT FGK230-LENTCDE     REPLACING ALL '"' BY ''''            
JB... *         INSPECT FGK230-LENTCDEADR1 REPLACING ALL '"' BY ''''            
      *         INSPECT FGK230-LENTCDEADR2 REPLACING ALL '"' BY ''''            
      *         INSPECT FGK230-NENTCDEADR3 REPLACING ALL '"' BY ''''            
      *         INSPECT FGK230-LENTCDEADR4 REPLACING ALL '"' BY ''''            
      *         INSPECT FGK230-NENTCDETEL  REPLACING ALL '"' BY ''''            
...JB *         INSPECT FGK230-CENTCDETELX REPLACING ALL '"' BY ''''            
                PERFORM ECRITURE-FGK332                                         
           END-READ.                                                            
      ******************************************************************        
      * lecture FGK331                                                          
      ******************************************************************        
       LECTURE-FGK331 SECTION.                                                  
           READ FGK331 INTO FGK331-ENR                                          
             AT END                                                             
                SET FIN-FGK331 TO TRUE                                          
                MOVE HIGH-VALUE TO FGK331-NENTCDE                               
             NOT AT END                                                         
                ADD 1 TO CPT-FGK331                                             
           END-READ.                                                            
      ******************************************************************        
      * Ecriture sur FGK332.                                                    
      ******************************************************************        
       ECRITURE-FGK332 SECTION.                                                 
           WRITE ENR-FGK332 FROM FGK230-ENR.                                    
           ADD 1 TO CPT-FGK332.                                                 
      ******************************************************************        
      * Ecriture sur FGK33F: Fournisseur                                        
      ******************************************************************        
       ECRITURE-FGK33F SECTION.                                                 
           MOVE FGK230-NENTCDE     TO FGK33F-NENTCDE.                           
           MOVE FGK230-LENTCDE     TO FGK33F-LENTCDE.                           
JB... *    MOVE SPACES             TO FGK33F-ADDRESS.                           
      *    STRING FGK230-LENTCDEADR1 ' '                                        
      *           FGK230-LENTCDEADR2 ' '                                        
      *           FGK230-NENTCDEADR3 ' '                                        
      *           FGK230-LENTCDEADR4                                            
      *           DELIMITED BY SIZE INTO FGK33F-ADDRESS.                        
      *    MOVE FGK230-NENTCDETEL  TO FGK33F-NENTCDETEL.                        
...JB *    MOVE FGK230-CENTCDETELX TO FGK33F-CENTCDETELX.                       
           WRITE ENR-FGK33F FROM FGK33F-ENR.                                    
           ADD 1 TO CPT-FGK33F.                                                 
      ******************************************************************        
      * C'est la fin des Haricots. Mais avant, on �crit un enreg.               
      * uniquement avec le nom du fichier et le nombre d'enregs.                
      ******************************************************************        
       FIN-BGK331 SECTION.                                                      
           MOVE SPACES TO FGK33C-NOM.                                           
           STRING 'WN_Supplier_004_'                                            
                   W-SEMAINE(5:2) W-SEMAINE(1:4)                                
                   '_01.txt'                                                    
                   DELIMITED BY SIZE INTO  FGK33C-NOM.                          
           MOVE CPT-FGK33F TO FGK33C-NBENR.                                     
           WRITE ENR-FGK33C FROM FGK33C-ENR.                                    
           ADD 1 TO CPT-FGK33C.                                                 
           CLOSE FGK230 FGK331 FGK332 FGK33C FGK33F.                            
           DISPLAY 'BGK331: Fin normale de programme'.                          
           MOVE CPT-FGK230 TO PIC-EDITZ9.                                       
           DISPLAY '        Nb. enregs lus sur FGK230: ' PIC-EDITZ9.            
           MOVE CPT-FGK331 TO PIC-EDITZ9.                                       
           DISPLAY '        Nb. enregs lus sur FGK331: ' PIC-EDITZ9.            
           MOVE CPT-FGK332 TO PIC-EDITZ9.                                       
           DISPLAY '        Nb. enregs �crits sur FGK332: ' PIC-EDITZ9.         
           MOVE CPT-FGK33C TO PIC-EDITZ9.                                       
           DISPLAY '        Nb. enregs �crits sur FGK33C: ' PIC-EDITZ9.         
           MOVE CPT-FGK33F TO PIC-EDITZ9.                                       
           DISPLAY '        Nb. enregs �crits sur FGK33F: ' PIC-EDITZ9.         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      ******************************************************************        
      * Proc de calcul de semaine KRISP:                                        
      * la date doit �tre fournie dans W-SEM-FDATE, format JJMMSSAA             
      * la semaine KRISP est rendue dans W-SEMAINE-KRISP, format SSAASE         
      * remarque: BETDATC est appel� plusieurs fois, mais WORKDATC est          
      *           restaur�e � la fin � la date W-SEM-FDATE.                     
      ******************************************************************        
      * >>>>>>>>>>>>>>>>>>>>>> D�but modifs AD05 <<<<<<<<<<<<<<<<<<<<<<         
      ******************************************************************        
      *CALCUL-SEMAINE-KRISP SECTION.                                            
      * Calcul de la date FDATE                                                 
      *    MOVE W-SEM-FDATE TO GFJJMMSSAA.                                      
      *    PERFORM APPEL-BETDATC-SEM.                                           
      *    MOVE GFWEEK TO W-SEMAINE-FDATE.                                      
      * Est-ce que l'ann�e pr�c�dente �tait bissextile?                         
      *    PERFORM APPEL-BETDATC-A-1.                                           
      * Calcul du 1er jeudi de f�vrier de l'ann�e de la semaine FDATE           
      *    MOVE W-SEMAINE-FDATE TO GFJJMMSSAA(5:).                              
      *    MOVE '0102' TO GFJJMMSSAA(1:4).                                      
      *    MOVE ZERO TO GFSMN.                                                  
      *    PERFORM VARYING P-SEM-SMN FROM 1 BY 1                                
      *              UNTIL (P-SEM-SMN > 7) OR (GFSMN = 4)                       
      *       MOVE X-SEM-SMN TO GFJJMMSSAA(1:2)                                 
      *       PERFORM APPEL-BETDATC-SEM                                         
      *    END-PERFORM.                                                         
      *    MOVE GFWEEK TO W-SEMAINE-1ER-J-FEV.                                  
      * Si l'ann�e -1 �tait bissextile, on recherche le 2�me jeudi              
      *    IF A-1-BISS                                                          
      *       ADD 1 TO P-SEM-SMN                                                
      *       MOVE ZERO TO GFSMN                                                
      *       PERFORM VARYING P-SEM-SMN FROM P-SEM-SMN BY 1                     
      *                 UNTIL (P-SEM-SMN > 15) OR (GFSMN = 4)                   
      *          MOVE X-SEM-SMN TO GFJJMMSSAA(1:2)                              
      *          PERFORM APPEL-BETDATC-SEM                                      
      *       END-PERFORM                                                       
      *       MOVE GFWEEK TO W-SEMAINE-1ER-J-FEV                                
      *    END-IF.                                                              
      * si la semaine FDATE >= 1er jeudi (ou 2�me), la semaine anglaise         
      * est �gale �: SemFDATE - Sem1erJeudi + 1                                 
      *    IF W-SEMAINE-FDATE >= W-SEMAINE-1ER-J-FEV                            
      *       MOVE W-SEMAINE-FDATE TO W-SEMAINE-KRISP                           
      *       COMPUTE P-SEMAINE-KRISP-SEM =                                     
      *               P-SEMAINE-FDATE-SEM                                       
      *             - P-SEMAINE-1ER-J-FEV-SEM                                   
      *             + 1                                                         
      *    ELSE                                                                 
      * sinon, les choses se corsent....                                        
      *       PERFORM CALCUL-SEMAINE-ANNEE-MOINS-UN                             
      *    END-IF.                                                              
      * reset de WORKDATC � W-SEM-FDATE.                                        
      *    MOVE W-SEM-FDATE TO GFJJMMSSAA.                                      
      *    PERFORM APPEL-BETDATC-SEM.                                           
      ***************************************************************           
      * Calcul alors qu'on est d�cal� d'un an...                                
      ***************************************************************           
      *CALCUL-SEMAINE-ANNEE-MOINS-UN SECTION.                                   
      * On se positionne au 1er f�vrier de l'ann�e -1                           
      *    MOVE GFWEEK TO W-SEMAINE-1ER-J-FEV.                                  
      *    SUBTRACT 1 FROM P-SEMAINE-1ER-J-FEV-SSAA                             
      *    MOVE W-SEMAINE-1ER-J-FEV TO GFJJMMSSAA(5:).                          
      *    MOVE '0102' TO GFJJMMSSAA(1:4).                                      
      *    PERFORM APPEL-BETDATC-SEM                                            
      * Est-ce que l'ann�e pr�c�dente (donc -2) �tait bissextile?               
      *    PERFORM APPEL-BETDATC-A-1.                                           
      * Calcul du 1er jeudi de f�vrier de l'ann�e -1                            
      *    MOVE W-SEMAINE-1ER-J-FEV TO GFJJMMSSAA(5:).                          
      *    MOVE '0102' TO GFJJMMSSAA(1:4).                                      
      *    MOVE ZERO TO GFSMN.                                                  
      *    PERFORM VARYING P-SEM-SMN FROM 1 BY 1                                
      *              UNTIL (P-SEM-SMN > 7) OR (GFSMN = 4)                       
      *       MOVE X-SEM-SMN TO GFJJMMSSAA(1:2)                                 
      *       PERFORM APPEL-BETDATC-SEM                                         
      *    END-PERFORM.                                                         
      *    MOVE GFWEEK TO W-SEMAINE-1ER-J-FEV.                                  
      * Si l'ann�e -2 �tait bissextile, on recherche le 2�me jeudi              
      *    IF A-1-BISS                                                          
      *       ADD 1 TO P-SEM-SMN                                                
      *       MOVE ZERO TO GFSMN                                                
      *       PERFORM VARYING P-SEM-SMN FROM P-SEM-SMN BY 1                     
      *                 UNTIL (P-SEM-SMN > 15) OR (GFSMN = 4)                   
      *          MOVE X-SEM-SMN TO GFJJMMSSAA(1:2)                              
      *          PERFORM APPEL-BETDATC-SEM                                      
      *       END-PERFORM                                                       
      *       MOVE GFWEEK TO W-SEMAINE-1ER-J-FEV                                
      *    END-IF.                                                              
      * Calcul de la derni�re semaine de l'ann�e                                
      *    MOVE SPACES TO GFWEEK.                                               
      *    MOVE 12     TO GFMM.                                                 
      *    PERFORM VARYING GFJJ FROM 31 BY -1                                   
      *              UNTIL (GFSEMSS = GFSS) AND (GFSEMAA = GFAA)                
      *       PERFORM APPEL-BETDATC-SEM                                         
      *    END-PERFORM.                                                         
      *    MOVE GFWEEK TO W-SEMAINE-DERN-SEM.                                   
      * la semaine anglaise est �gale �:                                        
      *   SemFDATE + DernSem - Sem1erJeudi + 1                                  
      *    MOVE W-SEMAINE-1ER-J-FEV TO W-SEMAINE-KRISP.                         
      *    COMPUTE P-SEMAINE-KRISP-SEM =                                        
      *            P-SEMAINE-FDATE-SEM                                          
      *          + P-SEMAINE-DERN-SEM-SEM                                       
      *          - P-SEMAINE-1ER-J-FEV-SEM                                      
      *          + 1.                                                           
      *                                                                         
       CALCUL-SEMAINE-KRISP SECTION.                                            
      * Calcul de la date FDATE                                                 
           MOVE W-SEM-FDATE TO GFJJMMSSAA.                                      
           PERFORM APPEL-BETDATC-SEM.                                           
           MOVE GFWEEK TO W-SEMAINE-FDATE.                                      
      * Calcul du 1er jeudi de f�vrier de l'ann�e de la semaine FDATE           
           MOVE W-SEMAINE-FDATE TO GFJJMMSSAA(5:).                              
           MOVE '0105' TO GFJJMMSSAA(1:4).                                      
           MOVE ZERO TO GFSMN.                                                  
           PERFORM VARYING P-SEM-SMN FROM 1 BY 1                                
                     UNTIL (P-SEM-SMN > 7) OR (GFSMN = 4)                       
              MOVE X-SEM-SMN TO GFJJMMSSAA(1:2)                                 
              PERFORM APPEL-BETDATC-SEM                                         
           END-PERFORM.                                                         
           MOVE GFWEEK TO W-SEMAINE-1ER-J-MAI.                                  
      *                                                                         
      * Si la semaine FDATE >= 1er jeudi, la semaine anglaise                   
      * est �gale �: SemFDATE - Sem1erJeudi + 1                                 
FQ    * la r�gle de calcul de la 1�re semaine de                                
FQ    * l'exercice n'est pas valable pour cette ann�e                           
FQ    * SemFDATE - Sem1erJeudi + 2                                              
           IF W-SEMAINE-FDATE >= W-SEMAINE-1ER-J-MAI                            
              MOVE W-SEMAINE-FDATE TO W-SEMAINE-KRISP                           
              COMPUTE P-SEMAINE-KRISP-SEM =                                     
                      P-SEMAINE-FDATE-SEM                                       
                    - P-SEMAINE-1ER-J-MAI-SEM                                   
EC                  + 1                                                         
FQ2   *             + 2                                                         
FQ                                                                              
FQ2           IF P-SEMAINE-KRISP-SEM = 53                                       
FQ               MOVE 01 TO P-SEMAINE-KRISP-SEM                                 
FQ               ADD 1 TO  P-SEMAINE-KRISP-SSAA                                 
FQ            END-IF                                                            
           ELSE                                                                 
      * sinon, les choses se corsent....                                        
              PERFORM CALCUL-SEMAINE-ANNEE-MOINS-UN                             
           END-IF.                                                              
      * reset de WORKDATC � W-SEM-FDATE.                                        
           MOVE W-SEM-FDATE TO GFJJMMSSAA.                                      
           PERFORM APPEL-BETDATC-SEM.                                           
      ***************************************************************           
      * Calcul alors qu'on est d�cal� d'un an...                                
      ***************************************************************           
       CALCUL-SEMAINE-ANNEE-MOINS-UN SECTION.                                   
      * On se positionne au 1er f�vrier de l'ann�e -1                           
           MOVE GFWEEK TO W-SEMAINE-1ER-J-MAI.                                  
           SUBTRACT 1 FROM P-SEMAINE-1ER-J-MAI-SSAA                             
           MOVE W-SEMAINE-1ER-J-MAI TO GFJJMMSSAA(5:).                          
           MOVE '0105' TO GFJJMMSSAA(1:4).                                      
           PERFORM APPEL-BETDATC-SEM                                            
      * Calcul du 1er jeudi de f�vrier de l'ann�e -1                            
           MOVE W-SEMAINE-1ER-J-MAI TO GFJJMMSSAA(5:).                          
           MOVE '0105' TO GFJJMMSSAA(1:4).                                      
           MOVE ZERO TO GFSMN.                                                  
           PERFORM VARYING P-SEM-SMN FROM 1 BY 1                                
                     UNTIL (P-SEM-SMN > 7) OR (GFSMN = 4)                       
              MOVE X-SEM-SMN TO GFJJMMSSAA(1:2)                                 
              PERFORM APPEL-BETDATC-SEM                                         
           END-PERFORM.                                                         
           MOVE GFWEEK TO W-SEMAINE-1ER-J-MAI.                                  
      * Calcul de la derni�re semaine de l'ann�e                                
           MOVE SPACES TO GFWEEK.                                               
           MOVE 12     TO GFMM.                                                 
           PERFORM VARYING GFJJ FROM 31 BY -1                                   
                     UNTIL (GFSEMSS = GFSS) AND (GFSEMAA = GFAA)                
              PERFORM APPEL-BETDATC-SEM                                         
           END-PERFORM.                                                         
           MOVE GFWEEK TO W-SEMAINE-DERN-SEM.                                   
      * La semaine anglaise est �gale �:                                        
      *   SemFDATE + DernSem - Sem1erJeudi + 1                                  
FQ    * la r�gle de calcul de la 1�re semaine de                                
FQ    * l'exercice n'est pas valable pour cette ann�e                           
FQ    * SemFDATE - Sem1erJeudi + 2                                              
           MOVE W-SEMAINE-1ER-J-MAI TO W-SEMAINE-KRISP.                         
           COMPUTE P-SEMAINE-KRISP-SEM =                                        
                   P-SEMAINE-FDATE-SEM                                          
                 + P-SEMAINE-DERN-SEM-SEM                                       
                 - P-SEMAINE-1ER-J-MAI-SEM                                      
EC               + 1                                                            
FQ2   *          + 2                                                            
FQ                                                                              
FQ2        IF P-SEMAINE-KRISP-SEM = 53                                          
FQ            MOVE 01 TO P-SEMAINE-KRISP-SEM                                    
FQ            ADD 1 TO  P-SEMAINE-KRISP-SSAA                                    
FQ         END-IF                                                               
           .                                                                    
      ******************************************************************        
      * >>>>>>>>>>>>>>>>>>>>>>> Fin modifs AD05 <<<<<<<<<<<<<<<<<<<<<<<         
      ******************************************************************        
      ***************************************************************           
      * Appel BETDATC                                                           
      ***************************************************************           
       APPEL-BETDATC-SEM SECTION.                                               
           MOVE '1'            TO GFDATA.                               00004640
           CALL BETDATC      USING WORK-BETDATC                         00004670
           IF GFVDAT NOT = '1'                                          00004680
              DISPLAY 'BETDATC : ' GF-MESS-ERR                          00004700
           END-IF.                                                      00004730
      ***************************************************************           
      * Appel BETDATC Ann�e -1                                                  
      ***************************************************************           
       APPEL-BETDATC-A-1 SECTION.                                               
           MOVE 'B'            TO GFDATA.                               00004640
           MOVE '12'           TO GFAJOUX.                              00004640
           CALL BETDATC      USING WORK-BETDATC                         00004670
           IF GFVDAT NOT = '1'                                          00004680
              DISPLAY 'BETDATC : ' GF-MESS-ERR                          00004700
           END-IF.                                                      00004730
           IF GFBISS = 1                                                        
              SET A-1-BISS TO TRUE                                              
           ELSE                                                                 
              SET A-1-NON-BISS TO TRUE                                          
           END-IF.                                                      00004730
      ******************************************************************        
      * BadaBoum                                                                
      ******************************************************************        
       ABEND-PROGRAMME SECTION.                                                 
           DISPLAY ABEND-MESS                                                   
           MOVE 'BGK331' TO ABEND-PROG.                                         
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL ABEND USING ABEND-PROG ABEND-MESS.                              
      *--                                                                       
           CALL MW-ABEND USING ABEND-PROG ABEND-MESS.                           
      *}                                                                        
