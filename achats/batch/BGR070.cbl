      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BGR070.                                                     
       AUTHOR. JB.                                                              
       ENVIRONMENT DIVISION.                                                    
      ******************************************************************        
      *                                                                         
      *  REMPLACEMENT DE LA REQUETE Q023                                        
      *  GENERATION DU SYSIN POUR FAST UNLOAD                                   
      *                                                                         
C0503 ******************************************************************        
      * DE02005 29/12/09 SUPPORT MAINTENANCE                                    
      *                  PB PERF DB2                                            
      *                  UTILISE DJRECQUAI DE RTGR05 AU LIEU DE RTGR00          
      ******************************************************************        
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FDATE   ASSIGN TO FDATE.                                     
      *--                                                                       
            SELECT FDATE   ASSIGN TO FDATE                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FNSOC   ASSIGN TO FNSOC.                                     
      *--                                                                       
            SELECT FNSOC   ASSIGN TO FNSOC                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FSYSIN  ASSIGN TO FSYSIN.                                    
      *--                                                                       
            SELECT FSYSIN  ASSIGN TO FSYSIN                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FPARAM  ASSIGN TO FPARAM.                                    
      *--                                                                       
            SELECT FPARAM  ASSIGN TO FPARAM                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FDATDEB ASSIGN TO FDATDEB.                                   
      *--                                                                       
            SELECT FDATDEB ASSIGN TO FDATDEB                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FGR070  ASSIGN TO FGR070.                                    
      *--                                                                       
            SELECT FGR070  ASSIGN TO FGR070                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FDATE  RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  MW-FILLER PIC X(80).                                                 
       FD  FNSOC  RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  MW-FILLER PIC X(80).                                                 
       FD  FSYSIN RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  MW-FILLER PIC X(80).                                                 
       FD  FPARAM RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  MW-FILLER PIC X(80).                                                 
       FD  FDATDEB RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.           
      *MW DAR-2                                                                 
       01  MW-FILLER PIC X(80).                                                 
       FD  FGR070 RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  ENR-FGR070 PIC X(80).                                                
       WORKING-STORAGE SECTION.                                                 
      ***************************************************************           
      * ZONE OUT POUR FGR070, FLAG POUR FSYSIN                                  
      ***************************************************************           
       01  W-FGR070 PIC X(80).                                                  
       01  FILLER PIC X VALUE SPACE.                                            
           88  FIN-FSYSIN VALUE 'F'.                                            
      *                                                                         
      * ============================================================= *         
      *                 DESCRIPTION FICHIER FNSOC                     *         
      * ============================================================= *         
      *                                                                         
       01  FNSOC-ENR.                                                           
           05  FNSOC-NSOC           PIC  X(03)    VALUE SPACES.                 
      *                                                                         
      * ============================================================= *         
      *                 DESCRIPTION FICHIER FPARAM                    *         
      * ============================================================= *         
      *                                                                         
       01  FPARAM-ENR.                                                          
           03 FPARAM-PARAM         PIC  X(01).                                  
           03 FILLER               PIC  X(79).                                  
      *                                                                         
       01 FLAG-FPARAM              PIC X     VALUE ' '.                         
           88 W-PARAM-VIDE                   VALUE ' '.                         
           88 W-PARAM-P                      VALUE 'P'.                         
           88 W-PARAM-F                      VALUE 'F'.                         
      ***************************************************************           
      * COMMS AVEC LES SOUS PROGS                                               
      ***************************************************************           
           COPY ABENDCOP.                                                       
       01  BETDATI    PIC X(8) VALUE 'BETDATI'.                                 
           COPY WORKDATI.                                                       
      * CODE LANGUE ET CODE PICTURE                                             
       77  WS-CODLANG              PIC X(2) VALUE 'FR'.                         
       77  WS-CODPIC               PIC X(2) VALUE '01'.                         
      ***************************************************************           
      * SOCIETE                                                                 
      ***************************************************************           
       01  W-NSOC-5.                                                            
      *{ Tr-Hexa-Map 1.5                                                        
      *  05  FILLER         PIC X(1) VALUE X'7D'.                               
      *--                                                                       
         05  FILLER         PIC X(1) VALUE X'27'.                               
      *}                                                                        
         05  W-NSOC         PIC X(3).                                           
      *{ Tr-Hexa-Map 1.5                                                        
      *  05  FILLER         PIC X(1) VALUE X'7D'.                               
      *--                                                                       
         05  FILLER         PIC X(1) VALUE X'27'.                               
      *}                                                                        
      ***************************************************************           
      * DATES CALCULEES                                                         
      ***************************************************************           
       01  W-DATE-DEB-10.                                                       
      *{ Tr-Hexa-Map 1.5                                                        
      *  05  FILLER         PIC X(1) VALUE X'7D'.                               
      *--                                                                       
         05  FILLER         PIC X(1) VALUE X'27'.                               
      *}                                                                        
         05  W-DATE-DEB     PIC X(8).                                           
      *{ Tr-Hexa-Map 1.5                                                        
      *  05  FILLER         PIC X(1) VALUE X'7D'.                               
      *--                                                                       
         05  FILLER         PIC X(1) VALUE X'27'.                               
      *}                                                                        
       01  W-DATE-FIN-10.                                                       
      *{ Tr-Hexa-Map 1.5                                                        
      *  05  FILLER         PIC X(1) VALUE X'7D'.                               
      *--                                                                       
         05  FILLER         PIC X(1) VALUE X'27'.                               
      *}                                                                        
         05  W-DATE-FIN     PIC X(8).                                           
      *{ Tr-Hexa-Map 1.5                                                        
      *  05  FILLER         PIC X(1) VALUE X'7D'.                               
      *--                                                                       
         05  FILLER         PIC X(1) VALUE X'27'.                               
      *}                                                                        
      *---------------------------------------------------------------*         
      *           D E S C R I P T I O N    D E S    Z O N E S         *         
      *                   S P E C I F I Q U E S    D B 2              *         
      *                    I N F O R M A T I V E S  I C I             *         
      *---------------------------------------------------------------*         
      *******TABLE PARAM ETAT  *****                                            
           COPY RVPARME.                                                        
      *                                                                         
      ***************************************************************           
      * PROC:                                                                   
      * - LECTURE DATE                                                          
      * - CALCUL DES BORNES DE DATES                                            
      * - GENERATION DE SYSIN                                                   
      * - C'EST TOUT                                                            
      ***************************************************************           
       PROCEDURE DIVISION.                                                      
           DISPLAY 'BGR070: EXTRACTION DES STATS DE RECEPTION.'.                
           OPEN INPUT FDATE FDATDEB FSYSIN FNSOC FPARAM                         
           OUTPUT FGR070.                                                       
           READ FDATE INTO GFJJMMSSAA AT END                            01090024
                MOVE 'FICHIER FDATE VIDE' TO ABEND-MESS                 01100024
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           CLOSE FDATE.                                                 01130024
           MOVE '1'            TO GFDATA.                               00004640
           PERFORM  CALL-BETDATI.                                       00004670
           DISPLAY 'BGR070: DATE PARAMETRE LUE SUR FDATE: '                     
                    GFSMN-LIB-L ' ' GFJOUR ' '                                  
                    GFMOI-LIB-L ' ' GFSIECLE GFANNEE.                           
           MOVE GFSAMJ-0 TO W-DATE-FIN.                                         
           READ FDATDEB INTO GFJMA-3 AT END                                     
                MOVE 'FICHIER FDATDEB  VIDE' TO ABEND-MESS                      
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           MOVE '6'            TO GFDATA.                               00004640
           PERFORM  CALL-BETDATI.                                       00004670
           DISPLAY 'BGR070: DATE PARAMETRE LUE SUR FDATDEB: '                   
                    GFSMN-LIB-L ' ' GFJOUR ' '                                  
                    GFMOI-LIB-L ' ' GFSIECLE GFANNEE.                           
           MOVE GFSAMJ-0 TO W-DATE-DEB.                                         
           CLOSE FDATDEB.                                                       
           READ FNSOC  INTO FNSOC-ENR   AT END                                  
                DISPLAY 'FICHIER FNSOC VIDE'                                    
                MOVE SPACES TO FNSOC-NSOC                                       
           END-READ.                                                            
           DISPLAY  'SOCIETE DE TRAITEMENT: '   FNSOC-NSOC.                     
           MOVE FNSOC-NSOC TO W-NSOC.                                           
           CLOSE FNSOC.                                                         
           READ FPARAM INTO FPARAM-ENR   AT END                                 
                DISPLAY 'FICHIER FPARAM VIDE'                                   
                MOVE SPACES TO FPARAM-PARAM                                     
           END-READ.                                                            
           DISPLAY  'CODE PARAMETRE       : '   FPARAM-PARAM.                   
           MOVE FPARAM-PARAM TO FLAG-FPARAM                                     
           CLOSE FPARAM.                                                        
      ******************************************************************        
      * RECOPIE DE FSYSIN                                                       
      ******************************************************************        
           DISPLAY 'BGR070: SYSIN GENERE A PARTIR DE FSYSIN:'                   
           PERFORM UNTIL FIN-FSYSIN                                             
              READ FSYSIN INTO W-FGR070                                         
                 AT END                                                         
                    SET FIN-FSYSIN TO TRUE                                      
                 NOT AT END                                                     
                    WRITE ENR-FGR070 FROM W-FGR070                              
                    DISPLAY W-FGR070                                            
              END-READ                                                          
           END-PERFORM.                                                         
           CLOSE FSYSIN.                                                        
      ******************************************************************        
      * GENERATION DE L'ORDRE SQL                                               
      ******************************************************************        
           DISPLAY 'BGR070: SYSIN GENERE :'                                     
           MOVE '     SELECT E.NSOCLIVR,                  ' TO W-FGR070.        
           MOVE W-DATE-DEB-10                       TO W-FGR070 (25:10).        
           MOVE ', '                                TO W-FGR070 (35:01).        
           MOVE W-DATE-FIN-10                       TO W-FGR070 (36:10).        
           MOVE ', '                                TO W-FGR070 (46:).          
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     C.WSEQFAM, C.LFAM, E.NDEPOT,        ' TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     SUBSTR(D.LVPARAM, 1, 1),            ' TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     SUBSTR(D.LVPARAM, 3, 3),            ' TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     SUBSTR(D.LVPARAM, 7, 5),            ' TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     SUBSTR(D.LVPARAM, 13, 7),           ' TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     CASE WHEN H.WMODSTOCK1 = ''O''      ' TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     THEN H.CMODSTOCK1                   ' TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     ELSE CASE  WHEN H.WMODSTOCK2 = ''O''' TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     THEN H.CMODSTOCK2                   ' TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     ELSE CASE  WHEN H.WMODSTOCK3 = ''O''' TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     THEN H.CMODSTOCK3  ELSE ''XXXXX''   ' TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     END END END,  VALUE(I.WFLAG, ''N''),' TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           IF W-PARAM-P OR W-PARAM-F                                            
              MOVE '     VALUE(A.QRECQUAI, 0),            ' TO W-FGR070         
              WRITE ENR-FGR070 FROM W-FGR070                                    
              DISPLAY W-FGR070                                                  
              MOVE '     VALUE(A.QRECQUAI, 0)             ' TO W-FGR070         
              WRITE ENR-FGR070 FROM W-FGR070                                    
              DISPLAY W-FGR070                                                  
           ELSE                                                                 
              MOVE '     CASE WHEN F.NCODIC IS NULL       ' TO W-FGR070         
              WRITE ENR-FGR070 FROM W-FGR070                                    
              DISPLAY W-FGR070                                                  
              MOVE '     THEN A.QRECQUAI ELSE 0 END,      ' TO W-FGR070         
              WRITE ENR-FGR070 FROM W-FGR070                                    
              DISPLAY W-FGR070                                                  
              MOVE '     VALUE(F.QREC, 0)                 ' TO W-FGR070         
              WRITE ENR-FGR070 FROM W-FGR070                                    
              DISPLAY W-FGR070                                                  
           END-IF                                                               
           MOVE '     FROM ( RVGR0500 A                   ' TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     INNER JOIN RVGA0000 B ON A.NCODIC    = B.NCODIC'          
                                                            TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     INNER JOIN RVGA1400 C ON B.CFAM      = C.CFAM  '          
                                                            TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     INNER JOIN RVGA3000 D ON C.CFAM      = D.CFAM  '          
                                                            TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     INNER JOIN RVGR0000 E ON A.NRECQUAI  = E.NRECQUAI'        
                                                            TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     INNER JOIN RVFL9003 G ON E.NSOCLIVR = G.NSOCDEPOT'        
                                                            TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '                          AND E.NDEPOT   = G.NDEPOT'           
                                                            TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     INNER JOIN RVFL5001 H ON A.NCODIC   = H.NCODIC'           
                                                            TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '                  AND G.NSOCDEPOTRT = H.NSOCDEPOT '            
                                                            TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '                  AND G.NDEPOTRT    = H.NDEPOT )  '            
                                                            TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           IF W-PARAM-VIDE                                                      
              MOVE '     LEFT JOIN  RVGR1500 F ON A.NREC     = F.NREC  '        
                                                            TO W-FGR070         
              WRITE ENR-FGR070 FROM W-FGR070                                    
              DISPLAY W-FGR070                                                  
              MOVE '                         AND A.NCODIC    = F.NCODIC'        
                                                            TO W-FGR070         
              WRITE ENR-FGR070 FROM W-FGR070                                    
              DISPLAY W-FGR070                                                  
      *{Post-translation Correct-Sign-not
      *       MOVE '                         AND F.NSOCIETE ^= ''801'' '        
              MOVE '                         AND F.NSOCIETE <> ''801'' '        
      *}
                                                            TO W-FGR070         
              WRITE ENR-FGR070 FROM W-FGR070                                    
              DISPLAY W-FGR070                                                  
           END-IF.                                                              
           MOVE '       LEFT JOIN  RVPARME  I ON B.CFAM      = I.CFAM  '        
                                                            TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '                       AND H.NSOCDEPOT  || H.NDEPOT '          
                                                            TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '                       =   I.NSOCLIEU             '            
                                                            TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '                   AND I.CPARAM = ''GR7''' TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '                AND I.CETAT  = ''IGR071''' TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
C0503 *    MOVE '     WHERE E.DJRECQUAI >=                ' TO W-FGR070.        
C0503      MOVE '     WHERE A.DJRECQUAI >=                ' TO W-FGR070.        
           MOVE  W-DATE-DEB-10   TO W-FGR070 (28:).                             
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
C0503 *    MOVE '     AND E.DJRECQUAI <=                  ' TO W-FGR070.        
C0503      MOVE '     AND A.DJRECQUAI <=                  ' TO W-FGR070.        
           MOVE  W-DATE-FIN-10   TO W-FGR070 (28:).                             
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     AND D.CPARAM = ''AGRE1''            ' TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           MOVE '     AND B.WDACEM = ''N''                ' TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
           IF ( W-NSOC > SPACES )                                               
           AND NOT W-PARAM-F                                                    
              MOVE '     AND E.NSOCLIVR =               '   TO W-FGR070         
              MOVE  W-NSOC-5   TO   W-FGR070 (26:)                              
              WRITE ENR-FGR070 FROM W-FGR070                                    
              DISPLAY W-FGR070                                                  
           END-IF.                                                              
           MOVE '     WITH UR ;                           ' TO W-FGR070.        
           WRITE ENR-FGR070 FROM W-FGR070.                                      
           DISPLAY W-FGR070.                                                    
      ******************************************************************        
      * FIN                                                                     
      ******************************************************************        
           CLOSE FGR070.                                                        
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      ******************************************************************        
      * ABEND                                                                   
      ******************************************************************        
       ABEND-PROGRAMME                 SECTION.                                 
           MOVE  'BGR070'     TO   ABEND-PROG.                                  
           CALL  'ABEND'   USING   ABEND-PROG  ABEND-MESS.                      
      ******************************************************************        
      * BETDATI                                                                 
      ******************************************************************        
       CALL-BETDATI                    SECTION.                                 
           CALL BETDATI      USING WORK-BETDATC WS-CODLANG              00004670
           IF GFVDAT NOT = '1'                                          00004680
              STRING 'BETDATI : ' GF-MESS-ERR                           00004700
              DELIMITED BY SIZE INTO ABEND-MESS                         00004710
              PERFORM ABEND-PROGRAMME                                           
           END-IF.                                                      00004730
