      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. BGK150.                                                      
       AUTHOR. JB.                                                              
      ******************************************************************        
      * ATTENTION LOWER CASE OBLIGATOIRE POUR LE NOM DES FICHIERS               
      ******************************************************************        
      *  FORMATAGE DES FICHIERS ALLIACOM AU FORMAT KRISP                        
      *                                                                         
      *  FICHIERS ALLIACOM :                                                    
      *  - PRODUITS (FGK020)                                                    
      *  - FOURNISSEURS (FGK050)                                                
      *  - VENTES (FGK131)                                                      
      *  - ACHATS (FGK140)                                                      
      *                                                                         
      *  CONSOLIDATION PRODUITS POUR KRISP:                                     
      *  - TYPOLOGIE (FGK15T)                                                   
      *  - PRODUITS (FGK15P)                                                    
      *  - VENTES (FGK15V)                                                      
      *  - ACHATS (FGK15A)                                                      
      *                                                                         
      *  - FOURNISSEUR (FGK15F)                                                 
      *                                                                         
      *  - CONTR�LE (FGK15C)                                                    
      *                                                                         
      *****************************************************************         
      * MODIF DSA015 / NOVEMBRE 2010                                            
      *  CORRECTION DES CALCULS DU INVENTORY QUANTITY ET VALUE                  
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                                   
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FDATE   ASSIGN TO  FDATE.                                   
      *--                                                                       
            SELECT  FDATE   ASSIGN TO  FDATE                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FMOIS   ASSIGN TO  FMOIS.                                   
      *--                                                                       
            SELECT  FMOIS   ASSIGN TO  FMOIS                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK020  ASSIGN TO  FGK020.                                  
      *--                                                                       
            SELECT  FGK020  ASSIGN TO  FGK020                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK050  ASSIGN TO  FGK050.                                  
      *--                                                                       
            SELECT  FGK050  ASSIGN TO  FGK050                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK131  ASSIGN TO  FGK131.                                  
      *--                                                                       
            SELECT  FGK131  ASSIGN TO  FGK131                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK140  ASSIGN TO  FGK140.                                  
      *--                                                                       
            SELECT  FGK140  ASSIGN TO  FGK140                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK15C  ASSIGN TO  FGK15C.                                  
      *--                                                                       
            SELECT  FGK15C  ASSIGN TO  FGK15C                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK15T  ASSIGN TO  FGK15T.                                  
      *--                                                                       
            SELECT  FGK15T  ASSIGN TO  FGK15T                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK15P  ASSIGN TO  FGK15P.                                  
      *--                                                                       
            SELECT  FGK15P  ASSIGN TO  FGK15P                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK15V  ASSIGN TO  FGK15V.                                  
      *--                                                                       
            SELECT  FGK15V  ASSIGN TO  FGK15V                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK15A  ASSIGN TO  FGK15A.                                  
      *--                                                                       
            SELECT  FGK15A  ASSIGN TO  FGK15A                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK15F  ASSIGN TO  FGK15F.                                  
      *--                                                                       
            SELECT  FGK15F  ASSIGN TO  FGK15F                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      ******************************************************************        
       DATA DIVISION.                                                           
      ******************************************************************        
       FILE SECTION.                                                            
       FD  FDATE  RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER        PIC X(80).                                          
       FD  FMOIS  RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER        PIC X(80).                                          
       FD  FGK020 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER        PIC X(113).                                         
       FD  FGK050 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER        PIC X(27).                                          
       FD  FGK131 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER        PIC X(112).                                         
       FD  FGK140 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
      *MW DAR-2                                                                 
       01  MW-FILLER        PIC X(71).                                          
       FD  FGK15C RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  ENR-FGK15C    PIC X(200).                                            
       FD  FGK15T RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  ENR-FGK15T    PIC X(200).                                            
       FD  FGK15P RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  ENR-FGK15P    PIC X(200).                                            
       FD  FGK15V RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  ENR-FGK15V    PIC X(200).                                            
       FD  FGK15A RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  ENR-FGK15A    PIC X(200).                                            
       FD  FGK15F RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  ENR-FGK15F    PIC X(200).                                            
      ******************************************************************        
       WORKING-STORAGE SECTION.                                                 
      ******************************************************************        
      * QUELQUES ZONES... QUI SERVENT - ON S'EN DOUTE - � QUELQUE CHOSE         
      ******************************************************************        
       01  PIC-EDITZ9     PIC ----.---.--9.                                     
       01  PIC-EDITV9     PIC ----.---.--9,9999.                                
      *                                                                         
       01  W-LIBELLE-MOIS PIC X(03).                                            
       01  W-PRAM         PIC S9(10)V99     VALUE  0.                           
       01  W-PRA          PIC S9(10)V99     VALUE  0.                           
       01  W-QREC-VALO    PIC S9(10)V99     VALUE  0.                           
       01  W-QREC         PIC S9(10)        VALUE  0.                           
ap0904 01  W-FGK050-DACEM.                                                      
ap0904     05 W-FGK050-NENTCDE    PIC X(06).                                    
ap0904     05 FILLER              PIC X(01).                                    
ap0904     05 W-FGK050-LENTCDE    PIC X(20).                                    
      *                                                                         
       01  ENR-FMOIS.                                                           
           03 W-MOIS.                                                           
               05 W-MOIS-MM  PIC X(02).                                         
               05 W-MOIS-SS  PIC X(02).                                         
               05 W-MOIS-AA  PIC X(02).                                         
           03 FILLER         PIC X(74).                                         
      ******************************************************************        
      * ...INTRODUCING THE FAMOUS TAUX...                                       
      ******************************************************************        
       01  W-TAUXEURO          PIC S9(1)V9(5) COMP-3 VALUE +6,55957.            
       01  W-GFSAMJ-0          PIC X(8).                                        
      **********************************************************                
      * FICHIER FGK020, FGK131 , FGK140                                         
      **********************************************************                
           COPY FGK020.                                                         
           COPY FGK050.                                                         
           05  FILLER               PIC X(01)     VALUE ' '.                    
           COPY FGK131.                                                         
           COPY FGK140.                                                         
       01  CPT-FGK020  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FGK020 VALUE 'F'.                                             
       01  CPT-FGK050  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FGK050 VALUE 'F'.                                             
       01  CPT-FGK131  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FGK131 VALUE 'F'.                                             
       01  CPT-FGK140  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FGK140 VALUE 'F'.                                             
      ******************************************************************        
      * FGK15C: CONTR�LE (18 CHAMPS)                                            
      *   -TPCE-: POUR TYPO, PRODUITS, CONTR�LE ET ENT�TE                       
      *    -V-  : POUR VENTES                                                   
      *    -C-  : POUR ACHATS                                                   
      ******************************************************************        
       01  FGK15C-TPCE-ENR.                                                     
           02  FGK15C-TPCE-NOM        PIC X(40).                                
           02  FILLER                 PIC X(01) VALUE ','.                      
           02  FGK15C-TPCE-NBENR      PIC Z(7)9.                                
           02  FILLER                 PIC X(17) VALUE ALL ','.                  
       01  FGK15C-V-ENR.                                                        
           02  FGK15C-V-NOM           PIC X(40).                                
           02  FILLER                 PIC X(01) VALUE ','.                      
           02  FGK15C-V-NBENR         PIC Z(7)9.                                
           02  FILLER                 PIC X(01) VALUE ','.                      
           02  FGK15C-V-PVUHT         PIC -(12)9.V9999.                         
           02  FILLER                 PIC X(01) VALUE ','.                      
JB1812*    02  FGK15C-V-PVMHT         PIC -(12)9.V9999.                         
JB1812*    02  FILLER                 PIC X(01) VALUE ','.                      
JB1812     02  FILLER                 PIC X(01) VALUE ','.                      
           02  FGK15C-V-QVENDUE       PIC -(09)9.                               
           02  FILLER                 PIC X(01) VALUE ','.                      
           02  FGK15C-V-STOCK         PIC -(09)9.                               
JB1812*    02  FILLER                 PIC X(09) VALUE ALL ','.                  
JB1812     02  FILLER                 PIC X(09) VALUE ',,,,,,,,,'.              
JB1812*    02  FGK15C-VPSTDTTC        PIC -(12)9.V9999.                         
           02  FGK15C-V-PX-VTE-HT     PIC -(12)9.V9999.                         
           02  FILLER                 PIC X(01) VALUE ','.                      
           02  FGK15C-V-MARGE-NET     PIC -(12)9.V9999.                         
           02  FILLER                 PIC X(01) VALUE ','.                      
           02  FGK15C-V-VALOPRMP      PIC -(12)9.V9999.                         
           02  FILLER                 PIC X(01) VALUE ','.                      
JB1812*    02  FGK15C-V-STOCKMAG      PIC -(09)9.                               
JB1812*    02  FILLER                 PIC X(01) VALUE ','.                      
JB1812*    02  FGK15C-V-STOCKDEP      PIC -(09)9.                               
JB1812*    02  FILLER                 PIC X(02) VALUE ',,'.                     
       01  W-TOTAL-PVUHT       PIC S9(11)V9999 PACKED-DECIMAL VALUE 0.          
JB1812*01  W-TOTAL-PVMHT     PIC S9(11)V9999 PACKED-DECIMAL VALUE 0.            
       01  W-TOTAL-QVENDUE     PIC S9(9)       PACKED-DECIMAL VALUE 0.          
       01  W-TOTAL-STOCK       PIC S9(9)       PACKED-DECIMAL VALUE 0.          
JB1812*01  W-TOTAL-PSTDTTC   PIC S9(11)V9999 PACKED-DECIMAL VALUE 0.            
JB1812 01  W-TOTAL-PX-VTE-HT   PIC S9(11)V9999 PACKED-DECIMAL VALUE 0.          
JB1812 01  W-TOTAL-MARGE-NET   PIC S9(11)V9999 PACKED-DECIMAL VALUE 0.          
JB1812 01  W-TOTAL-VALOPRMP    PIC S9(11)V9999 PACKED-DECIMAL VALUE 0.          
JB1812*01  W-TOTAL-STOCKMAG  PIC S9(9)       PACKED-DECIMAL VALUE 0.            
JB1812*01  W-TOTAL-STOCKDEP  PIC S9(9)       PACKED-DECIMAL VALUE 0.            
       01  FGK15C-A-ENR.                                                        
           02  FGK15C-A-NOM           PIC X(40).                                
           02  FILLER                 PIC X(01) VALUE ','.                      
           02  FGK15C-A-NBENR         PIC Z(7)9.                                
JB1812*    02  FILLER                 PIC X(07) VALUE ',,,,,0,'.                
JB1812     02  FILLER                 PIC X(10) VALUE ',,,,,,,,,,'.             
           02  FGK15C-A-PRA           PIC -(12)9.V9999.                         
JB1812*    02  FILLER                 PIC X(09) VALUE ',0,0,0,0,'.              
JB1812*    02  FGK15C-A-PRAM          PIC -(12)9.V9999.                         
JB1812     02  FILLER                 PIC X(02) VALUE ',,'.                     
JB1812*    02  FILLER                 PIC X(01) VALUE ','.                      
           02  FGK15C-A-QREC          PIC -(09)9.                               
           02  FILLER                 PIC X(04) VALUE ',,,,'.                   
JB1812*    02  FGK15C-A-QCDE          PIC -(09)9.                               
JB1812     02  FGK15C-A-QREC-VALO     PIC -(12)9.V9999.                         
       01  W-TOTAL-PRA       PIC S9(11)V9999 PACKED-DECIMAL VALUE 0.            
JB1812*01  W-TOTAL-PRAM      PIC S9(11)V9999 PACKED-DECIMAL VALUE 0.            
       01  W-TOTAL-QREC      PIC S9(9)       PACKED-DECIMAL VALUE 0.            
JB1812 01  W-TOTAL-QREC-VALO PIC S9(11)V9999 PACKED-DECIMAL VALUE 0.            
JB1812*01  W-TOTAL-QCDE      PIC S9(9)       PACKED-DECIMAL VALUE 0.            
       01  FGK15C-F-ENR.                                                        
           02  FGK15C-F-NOM            PIC X(40).                               
           02  FILLER                 PIC X(01) VALUE ','.                      
           02  FGK15C-F-NBENR          PIC Z(7)9.                               
           02  FILLER                 PIC X(16) VALUE ALL ','.                  
JB1812*    02  FILLER                 PIC X(18) VALUE ALL ','.                  
       01  CPT-FGK15C  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
      ******************************************************************        
      * FGK15T: TYPO PRODUITS (18 CHAMPS)                                       
      ******************************************************************        
       01  FGK15T-ENR.                                                          
           02  FILLER                 PIC X(01) VALUE '"'.                      
           02  FGK15T-NCODIC          PIC X(07).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
           02  FGK15T-DCREATION       PIC X(10).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
           02  FGK15T-CMKT            PIC X(05).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
           02  FGK15T-LMKT            PIC X(20).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
           02  FGK15T-CFAM            PIC X(05).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
           02  FGK15T-LFAM            PIC X(20).                                
           02  FILLER                 PIC X(14) VALUE '",,,,,,,,,,,,,'.         
       01  CPT-FGK15T  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
      ******************************************************************        
      * FGK15P: PRODUITS (14 CHAMPS)                                            
      ******************************************************************        
       01  FGK15P-ENR.                                                          
           02  FILLER                 PIC X(01) VALUE '"'.                      
           02  FGK15P-NCODIC          PIC X(07).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
           02  FGK15P-DCREATION       PIC X(10).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
           02  FGK15P-LREFFOURN       PIC X(20).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
           02  FGK15P-CMARQ           PIC X(05).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
           02  FGK15P-LMARQ           PIC X(20).                                
JB1812*    02  FILLER                 PIC X(03) VALUE '","'.                    
JB1812*    02  FGK15P-NEAN            PIC X(13).                                
JB1812*    02  FILLER                 PIC X(08) VALUE '",,,,,,,'.               
JB1812*    02  FGK15P-TAUXTVA         PIC ZZ9.V999.                             
JB1812*    02  FILLER                 PIC X(02) VALUE ',"'.                     
JB1812*    02  FGK15P-DELETED         PIC X(01).                                
JB1812*    02  FILLER                 PIC X(01) VALUE '"'.                      
JB1812     02  FILLER                 PIC X(10) VALUE '",,,,,,,,,'.             
       01  CPT-FGK15P  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
      ******************************************************************        
      * FGK15V: VENTES (13 CHAMPS)                                              
      ******************************************************************        
       01  FGK15V-ENR.                                                          
           02  FILLER                 PIC X(01) VALUE '"'.                      
           02  FGK15V-NCODIC          PIC X(07).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
           02  FGK15V-DCREATION       PIC X(10).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
           02  FGK15V-DATE            PIC X(08).                                
JB1812*    02  FILLER                 PIC X(03) VALUE '","'.                    
           02  FILLER                 PIC X(03) VALUE '",,'.                    
JB1812*    02  FGK15V-CDEVISE         PIC X(05) VALUE SPACES.                   
JB1812*    02  FILLER                 PIC X(02) VALUE '",'.                     
           02  FGK15V-PVUHT           PIC ------9.V9999.                        
           02  FILLER                 PIC X(01) VALUE ','.                      
JB1812*    02  FGK15V-PVMHT           PIC ------9.V9999.                        
JB1812     02  FILLER                 PIC X(01) VALUE ','.                      
           02  FGK15V-QVENDUE         PIC -------9.                             
           02  FILLER                 PIC X(01) VALUE ','.                      
           02  FGK15V-STOCK           PIC -------9.                             
           02  FILLER                 PIC X(01) VALUE ','.                      
           02  FGK15V-VALOPRMP        PIC ---------9.V9999.                     
           02  FILLER                 PIC X(01) VALUE ','.                      
JB1812*    02  FGK15V-VALOPV          PIC ---------9.V9999.                     
           02  FILLER                 PIC X(01) VALUE ','.                      
JB1812*    02  FGK15V-PSTDTTC         PIC ------9.V9999.                        
           02  FGK15V-PX-VTE-HT       PIC ------9.V9999.                        
AP04       02  FGK15V-PX-VTE-PLUS     REDEFINES  FGK15V-PX-VTE-HT               
AP04                                  PIC ZZZZZZ9.V9999.                        
           02  FILLER                 PIC X(01) VALUE ','.                      
JB1812     02  FGK15V-MARGE-NET       PIC ------9.V9999.                        
JB1812*    02  FGK15V-STOCKMAG        PIC -------9.                             
JB1812*    02  FILLER                 PIC X(01) VALUE ','.                      
JB1812*    02  FGK15V-STOCKDEP        PIC -------9.                             
       01  CPT-FGK15V  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
      ******************************************************************        
      * FGK15A: ACHATS (17 CHAMPS)                                              
      ******************************************************************        
       01  FGK15A-ENR.                                                          
           02  FILLER                 PIC X(01) VALUE '"'.                      
           02  FGK15A-NCODIC          PIC X(07).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
           02  FGK15A-DCREATION       PIC X(10).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
           02  FGK15A-DATE            PIC X(08).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
           02  FGK15A-NENTCDE         PIC X(06).                                
           02  FILLER                 PIC X(02) VALUE '",'.                     
JB1812*    02  FGK15A-CDEVISE         PIC X(05) VALUE SPACES.                   
JB1812*    02  FILLER                 PIC X(04) VALUE '",,,'.                   
JB1812     02  FILLER                 PIC X(09) VALUE ',,,,,,,'.                
    12     02  FGK15A-PRA             PIC ------9.V9999.                        
           02  FILLER                 PIC X(03) VALUE ',,,'.                    
JB1812*    02  FILLER                 PIC X(05) VALUE ',,,,,'.                  
JB1812*    02  FGK15A-PRAM            PIC ------9.V9999 BLANK WHEN ZERO.        
JB1812*    02  FILLER                 PIC X(01) VALUE ','.                      
JB1812*    02  FGK15A-PRMP            PIC ------9.V9999.                        
JB1812*    02  FILLER                 PIC X(01) VALUE ','.                      
           02  FGK15A-QREC            PIC -------9.                             
           02  FILLER                 PIC X(02) VALUE ',,'.                     
JB1812*    02  FGK15A-QCDE            PIC -------9.                             
           02  FGK15A-QREC-VALO       PIC -(12)9.V9999.                         
       01  CPT-FGK15A  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
      ******************************************************************        
      * FGK15F: FOURNISSEURS (10 CHAMPS)                                        
      ******************************************************************        
       01  FGK15F-ENR.                                                          
           02  FILLER                 PIC X(01) VALUE '"'.                      
           02  FGK15F-NENTCDE         PIC X(05).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
           02  FGK15F-LENTCDE         PIC X(20).                                
JB1812*    02  FILLER                 PIC X(03) VALUE '","'.                    
JB1812*    02  FGK15F-ADDRESS         PIC X(98).                                
JB1812*    02  FILLER                 PIC X(03) VALUE '","'.                    
JB1812*    02  FGK15F-NENTCDETEL      PIC X(15).                                
JB1812*    02  FILLER                 PIC X(03) VALUE '","'.                    
JB1812*    02  FGK15F-CENTCDETELX     PIC X(15).                                
JB1812*    02  FILLER                 PIC X(07) VALUE '",,,,,,'.                
JB1812     02  FILLER                 PIC X(11) VALUE '",,,,,,,,, '.            
AP0904 01  FGK15F-ENR-DACEM.                                                    
AP0904     02  FILLER                 PIC X(01) VALUE '"'.                      
AP0904     02  FGK15F-NENTCDE-DACEM   PIC X(06).                                
AP0904     02  FILLER                 PIC X(03) VALUE '","'.                    
AP0904     02  FGK15F-LENTCDE-DACEM   PIC X(20).                                
JB1812*    02  FILLER                 PIC X(03) VALUE '","'.                    
JB1812*    02  FGK15F-ADDRESS         PIC X(98).                                
JB1812*    02  FILLER                 PIC X(03) VALUE '","'.                    
JB1812*    02  FGK15F-NENTCDETEL      PIC X(15).                                
JB1812*    02  FILLER                 PIC X(03) VALUE '","'.                    
JB1812*    02  FGK15F-CENTCDETELX     PIC X(15).                                
JB1812*    02  FILLER                 PIC X(07) VALUE '",,,,,,'.                
JB1812     02  FILLER                 PIC X(10) VALUE '",,,,,,,,,'.             
       01  CPT-FGK15F  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
      ******************************************************************        
      * ABEND ET BETDATC                                                        
      ******************************************************************        
           COPY ABENDCOP.                                                       
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *01  ABEND   PIC X(8) VALUE 'ABEND  '.                                    
      *--                                                                       
       01  MW-ABEND   PIC X(8) VALUE 'ABEND  '.                                 
      *}                                                                        
           COPY WORKDATC.                                                       
       01  BETDATC PIC X(8) VALUE 'BETDATC'.                                    
      ******************************************************************        
      * PROC: D�BUT, MILIEU, FIN.                                               
      ******************************************************************        
       PROCEDURE DIVISION.                                                      
           PERFORM  DEBUT-BGK150.                                               
           PERFORM  TRAIT-BGK150.                                               
           PERFORM  FIN-BGK150.                                                 
      ******************************************************************        
      * OUVERTURE DE FICHIERS, PARAM�TRES: FDATE.                               
      ******************************************************************        
       DEBUT-BGK150 SECTION.                                                    
           DISPLAY 'BGK150: PRODUITS POUR KRISP'                                
           OPEN INPUT FDATE FMOIS FGK020 FGK050 FGK131 FGK140                   
               OUTPUT FGK15C FGK15T FGK15P FGK15V FGK15A FGK15F.                
           READ FDATE INTO GFJJMMSSAA AT END                            01090024
                MOVE 'FICHIER FDATE VIDE' TO ABEND-MESS                 01100024
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           CLOSE FDATE.                                                 01130024
           DISPLAY 'BGK150: DATE PARAM�TRE LUE SUR FDATE: '                     
                    GFJJMMSSAA.                                                 
           READ FMOIS INTO ENR-FMOIS AT END                             01090024
                MOVE 'FICHIER FMOIS VIDE' TO ABEND-MESS                 01100024
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           CLOSE FMOIS.                                                 01130024
           DISPLAY '        DATE PARAM�TRE LUE SUR FMOIS: '                     
                    W-MOIS.                                                     
           EVALUATE W-MOIS-MM                                                   
               WHEN  '01'                                                       
                           MOVE 'JAN'        TO  W-LIBELLE-MOIS                 
               WHEN  '02'                                                       
                           MOVE 'FEB'        TO  W-LIBELLE-MOIS                 
               WHEN  '03'                                                       
                           MOVE 'MAR'        TO  W-LIBELLE-MOIS                 
               WHEN  '04'                                                       
                           MOVE 'APR'        TO  W-LIBELLE-MOIS                 
               WHEN  '05'                                                       
                           MOVE 'MAY'        TO  W-LIBELLE-MOIS                 
               WHEN  '06'                                                       
                           MOVE 'JUN'        TO  W-LIBELLE-MOIS                 
               WHEN  '07'                                                       
                           MOVE 'JUL'        TO  W-LIBELLE-MOIS                 
               WHEN  '08'                                                       
                           MOVE 'AUG'        TO  W-LIBELLE-MOIS                 
               WHEN  '09'                                                       
                           MOVE 'SEP'        TO  W-LIBELLE-MOIS                 
               WHEN  '10'                                                       
                           MOVE 'OCT'        TO  W-LIBELLE-MOIS                 
               WHEN  '11'                                                       
                           MOVE 'NOV'        TO  W-LIBELLE-MOIS                 
               WHEN  '12'                                                       
                           MOVE 'DEC'        TO  W-LIBELLE-MOIS                 
               WHEN  OTHER                                                      
                    STRING 'FICHIER FMOIS MMSSAA INCOHERENT '                   
                        ENR-FMOIS DELIMITED BY SIZE INTO ABEND-MESS             
                     PERFORM ABEND-PROGRAMME                                    
           END-EVALUATE.                                                        
           MOVE SPACES TO FGK020-NCODIC.                                        
      ******************************************************************        
      * PROGRAMME:                                                              
      *   LECTURE DU FICHIER PRODUIT FGK020 PUIS ON REMPLI SI POSSIBLE          
      *   LES FICHIERS FORMAT KRISP                                             
      ******************************************************************        
       TRAIT-BGK150 SECTION.                                                    
           PERFORM LECTURE-FGK020.                                              
           PERFORM LECTURE-FGK131.                                              
           PERFORM LECTURE-FGK140.                                              
1110       PERFORM UNTIL FIN-FGK020                                             
                 IF FGK131-NCODIC < FGK020-NCODIC                               
      *    PRODUIT NON TRAITE, ON NE L'ENVOI PAS � KESA, ON PASSE               
      *    PROCHAIN PRODUIT TRAITE                                              
                    PERFORM UNTIL FGK131-NCODIC >= FGK020-NCODIC                
                       DISPLAY 'PRODUIT FGK131 NON TRAITE '                     
                               'DANS FICHIER PRINCIPAL '                        
                               FGK131-NCODIC                                    
                       PERFORM LECTURE-FGK131                                   
                    END-PERFORM                                                 
                 END-IF                                                         
                 IF FGK131-NCODIC = FGK020-NCODIC                               
                    PERFORM ECRITURE-FGK15V                                     
                    PERFORM LECTURE-FGK131                                      
                    PERFORM ECRITURE-FGK15T                                     
                    PERFORM ECRITURE-FGK15P                                     
                 END-IF                                                         
                 IF FGK140-NCODIC < FGK020-NCODIC                               
      *          PRODUIT NON TRAITE, ON NE L'ENVOI PAS � KESA, ON PASSE         
      *          PROCHAIN PRODUIT TRAITE                                        
                    PERFORM UNTIL FGK140-NCODIC >= FGK020-NCODIC                
                       DISPLAY 'PRODUIT FGK140 NON TRAITE '                     
                               'DANS FICHIER PRINCIPAL '                        
                               FGK140-NCODIC                                    
                       PERFORM LECTURE-FGK140                                   
                    END-PERFORM                                                 
                 END-IF                                                         
                 IF FGK140-NCODIC = FGK020-NCODIC                               
                    PERFORM UNTIL FGK140-NCODIC > FGK020-NCODIC                 
                       PERFORM ECRITURE-FGK15A                                  
                       PERFORM LECTURE-FGK140                                   
                    END-PERFORM                                                 
1110             END-IF                                                         
                 PERFORM LECTURE-FGK020                                         
           END-PERFORM.                                                         
           PERFORM FICHIER-FOURNISSEURS.                                        
      ******************************************************************        
      * LECTURE FGK020                                                          
      ******************************************************************        
       LECTURE-FGK020 SECTION.                                                  
           READ FGK020 INTO FGK020-ENR                                          
             AT END                                                             
                SET FIN-FGK020 TO TRUE                                          
                MOVE HIGH-VALUE TO FGK020-NCODIC                                
             NOT AT END                                                         
                ADD 1 TO CPT-FGK020                                             
           END-READ.                                                            
      ******************************************************************        
      * LECTURE FGK131                                                          
      ******************************************************************        
       LECTURE-FGK131 SECTION.                                                  
           READ FGK131 INTO FGK131-ENR                                          
             AT END                                                             
                SET FIN-FGK131 TO TRUE                                          
                MOVE HIGH-VALUE TO FGK131-NCODIC                                
             NOT AT END                                                         
                ADD 1 TO CPT-FGK131                                             
           END-READ.                                                            
      ******************************************************************        
      * LECTURE FGK140                                                          
      ******************************************************************        
       LECTURE-FGK140 SECTION.                                                  
           READ FGK140 INTO FGK140-ENR                                          
             AT END                                                             
                SET FIN-FGK140 TO TRUE                                          
                MOVE HIGH-VALUE TO FGK140-NCODIC                                
             NOT AT END                                                         
                ADD 1 TO CPT-FGK140                                             
           END-READ.                                                            
      ******************************************************************        
      * LECTURE FGK050                                                          
      ******************************************************************        
       LECTURE-FGK050 SECTION.                                                  
           READ FGK050 INTO FGK050-ENR                                          
             AT END                                                             
                SET FIN-FGK050 TO TRUE                                          
             NOT AT END                                                         
                ADD 1 TO CPT-FGK050                                             
           END-READ.                                                            
      ******************************************************************        
      * ECRITURE SUR FGK15T: TYPOLOGIE                                          
      ******************************************************************        
       ECRITURE-FGK15T SECTION.                                                 
           INITIALIZE FGK15T-ENR.                                               
           MOVE FGK020-NCODIC    TO FGK15T-NCODIC.                              
           MOVE FGK020-DATE-CRE  TO FGK15T-DCREATION.                           
      *    STRING FGK020-DATE-CRE (7:2) '/'                                     
      *           FGK020-DATE-CRE (5:2) '/'                                     
      *           FGK020-DATE-CRE (1:4)                                         
      *    DELIMITED BY SIZE INTO FGK15T-DCREATION.                             
           MOVE FGK020-CODE-MARK TO FGK15T-CMKT.                                
           MOVE FGK020-LIB-MARK  TO FGK15T-LMKT.                                
           MOVE FGK020-CODE-FAM  TO FGK15T-CFAM.                                
           MOVE FGK020-LIB-FAM   TO FGK15T-LFAM.                                
           WRITE ENR-FGK15T FROM FGK15T-ENR.                                    
           ADD 1 TO CPT-FGK15T.                                                 
      ******************************************************************        
      * ECRITURE SUR FGK15P: PRODUITS                                           
      ******************************************************************        
       ECRITURE-FGK15P SECTION.                                                 
           INITIALIZE FGK15P-ENR.                                               
           MOVE FGK020-NCODIC    TO FGK15P-NCODIC.                              
           MOVE FGK020-DATE-CRE  TO FGK15P-DCREATION.                           
      *    STRING FGK020-DATE-CRE (7:2) '/'                                     
      *           FGK020-DATE-CRE (5:2) '/'                                     
      *           FGK020-DATE-CRE (1:4)                                         
      *    DELIMITED BY SIZE INTO FGK15P-DCREATION.                             
           INSPECT FGK020-REF-COMM  REPLACING ALL '"' BY ' '.                   
           MOVE FGK020-REF-COMM  TO FGK15P-LREFFOURN.                           
ap0904     MOVE FGK020-CODE-MARQ TO FGK15P-CMARQ.                               
ap0904     MOVE FGK020-LIB-MARQ  TO FGK15P-LMARQ.                               
JB1812*    MOVE 'N' TO FGK15P-DELETED.                                          
           WRITE ENR-FGK15P FROM FGK15P-ENR.                                    
           ADD 1 TO CPT-FGK15P.                                                 
      ******************************************************************        
      * ECRITURE SUR FGK15V: VENTES                                             
      ******************************************************************        
       ECRITURE-FGK15V SECTION.                                                 
           INITIALIZE FGK15V-ENR.                                               
      *    NUMERO DE CODIC.                                                     
           MOVE FGK131-NCODIC   TO FGK15V-NCODIC.                               
      *    DATE DE CREATION ARTICLE.                                            
           MOVE FGK020-DATE-CRE  TO FGK15V-DCREATION.                           
      *    STRING FGK020-DATE-CRE (7:2) '/'                                     
      *           FGK020-DATE-CRE (5:2) '/'                                     
      *           FGK020-DATE-CRE (1:4)                                         
      *    DELIMITED BY SIZE INTO FGK15V-DCREATION.                             
      *    MOIS TRAITE MMM-YYYY.                                                
           STRING                                                               
                  W-LIBELLE-MOIS '-' W-MOIS-SS  W-MOIS-AA                       
                   DELIMITED BY SIZE INTO FGK15V-DATE.                          
      *    PRIX DE VENTE FIC UNITAIRE.                                          
           MOVE FGK131-PVTE-F-MHT    TO FGK15V-PVUHT.                           
           ADD  FGK131-PVTE-F-MHT    TO W-TOTAL-PVUHT.                          
JB1812*    MOVE FGK131-PVTE-N-MHT    TO FGK15V-PVMHT.                           
JB1812*    ADD  FGK131-PVTE-N-MHT    TO W-TOTAL-PVMHT.                          
      *    QUANTITE VENDUE.                                                     
           MOVE FGK131-VOL-VTE       TO FGK15V-QVENDUE.                         
           ADD  FGK131-VOL-VTE       TO W-TOTAL-QVENDUE.                        
      *    QUANTITE EN STOCK.                                                   
           MOVE FGK131-STOCKTOT      TO FGK15V-STOCK.                           
           ADD  FGK131-STOCKTOT      TO W-TOTAL-STOCK.                          
      *    STOCK VALORISE AU PRMP.                                              
           MOVE FGK131-VALOPRMP      TO FGK15V-VALOPRMP.                        
           ADD  FGK131-VALOPRMP      TO W-TOTAL-VALOPRMP.                       
      *    TOTAL PRIX DE VENTE HT.                                              
AP04       IF FGK131-TOT-VTE-N-HT < 0                                           
              MOVE FGK131-TOT-VTE-N-HT  TO FGK15V-PX-VTE-HT                     
AP04       ELSE                                                                 
AP04          MOVE FGK131-TOT-VTE-N-HT  TO FGK15V-PX-VTE-PLUS                   
AP04       END-IF.                                                              
           ADD  FGK131-TOT-VTE-N-HT  TO W-TOTAL-PX-VTE-HT.                      
      *    TOTAL DE MARGE NET.                                                  
           MOVE FGK131-TOT-MARGE-NET TO FGK15V-MARGE-NET.                       
           ADD  FGK131-TOT-MARGE-NET TO W-TOTAL-MARGE-NET.                      
JB1812*    MOVE FGK131-VALOPV        TO FGK15V-VALOPV.                          
JB1812*    MOVE FGK131-PSTDTTC       TO FGK15V-PSTDTTC.                         
JB1812*    ADD  FGK131-PSTDTTC       TO W-TOTAL-PSTDTTC.                        
JB1812*    MOVE FGK131-STOCKMAG      TO FGK15V-STOCKMAG.                        
JB1812*    ADD  FGK131-STOCKMAG      TO W-TOTAL-STOCKMAG.                       
JB1812*    MOVE FGK131-STOCKDEP      TO FGK15V-STOCKDEP.                        
JB1812*    ADD  FGK131-STOCKDEP      TO W-TOTAL-STOCKDEP.                       
1110       IF FGK131-VOL-VTE NOT = 0                                            
              WRITE ENR-FGK15V FROM FGK15V-ENR                                  
              ADD 1 TO CPT-FGK15V                                               
1110       END-IF.                                                              
      ******************************************************************        
      * ECRITURE SUR FGK15A: ACHATS                                             
      ******************************************************************        
       ECRITURE-FGK15A SECTION.                                                 
           INITIALIZE FGK15A-ENR.                                               
      *    NUMERO DE CODIC.                                                     
           MOVE FGK140-NCODIC   TO FGK15A-NCODIC.                               
      *    DATE DE CREATION DU CODIC.                                           
           MOVE FGK020-DATE-CRE  TO FGK15A-DCREATION.                           
      *    STRING FGK020-DATE-CRE (7:2) '/'                                     
      *           FGK020-DATE-CRE (5:2) '/'                                     
      *           FGK020-DATE-CRE (1:4)                                         
      *    DELIMITED BY SIZE INTO FGK15A-DCREATION.                             
      *    MOIS TRAITE MMM-YYYY.                                                
           STRING                                                               
                  W-LIBELLE-MOIS '-' W-MOIS-SS  W-MOIS-AA                       
                   DELIMITED BY SIZE INTO FGK15A-DATE.                          
      *    CODE ENTITE DE COMMANDE                                              
           MOVE FGK140-CODE-ENTCDE     TO FGK15A-NENTCDE.                       
      *    CODE PRIX DE REVIENT ACHAT                                           
JB1812*    MOVE FGK140-PRMP            TO FGK15A-PRMP.                          
           MOVE FGK140-PRA             TO FGK15A-PRA                            
                                          W-PRA.                                
           ADD  W-PRA                  TO W-TOTAL-PRA.                          
JB1812*    MOVE FGK140-PRA-M-MOIS      TO FGK15A-PRAM.                          
JB1812*    ADD  FGK140-PRA-M-MOIS      TO W-TOTAL-PRAM.                         
      *    QUANTITE COMMANDE RECUE                                              
           MOVE FGK140-VOL-ACHAT       TO FGK15A-QREC                           
                                          W-QREC.                               
           ADD  W-QREC                 TO W-TOTAL-QREC.                         
JB1812*    MOVE FGK140-QCDE            TO FGK15A-QCDE.                          
JB1812*    ADD  FGK140-QCDE            TO W-TOTAL-QCDE.                         
      *    QUANTITE COMMANDE RECUE * PRA MOYEN                                  
           MOVE FGK140-PRA-M-MOIS      TO W-PRAM.                               
           COMPUTE W-QREC-VALO         = W-PRAM                                 
                                       * W-QREC.                                
           MOVE W-QREC-VALO            TO FGK15A-QREC-VALO.                     
           ADD  W-QREC-VALO            TO W-TOTAL-QREC-VALO.                    
           WRITE ENR-FGK15A FROM FGK15A-ENR.                                    
           ADD 1 TO CPT-FGK15A.                                                 
      ******************************************************************        
      * FICHIER FOURNISSEURS                                                    
      ******************************************************************        
       FICHIER-FOURNISSEURS   SECTION.                                          
           PERFORM LECTURE-FGK050.                                              
           PERFORM UNTIL FIN-FGK050                                             
              PERFORM ECRITURE-FGK15F                                           
              PERFORM LECTURE-FGK050                                            
           END-PERFORM.                                                         
      ******************************************************************        
      * ECRITURE SUR FGK15F: FOURNISSEUR                                        
      ******************************************************************        
       ECRITURE-FGK15F SECTION.                                                 
           INITIALIZE FGK15F-ENR.                                               
AP0904     IF FGK050-CODE-ENTCDE(1:1) = 'D'                                     
AP0904        MOVE FGK050-ENR  TO  W-FGK050-DACEM                               
AP0904        MOVE W-FGK050-NENTCDE    TO FGK15F-NENTCDE-DACEM                  
AP0904        MOVE W-FGK050-LENTCDE    TO FGK15F-LENTCDE-DACEM                  
AP0904        WRITE ENR-FGK15F FROM FGK15F-ENR-DACEM                            
AP0904     ELSE                                                                 
              MOVE FGK050-CODE-ENTCDE  TO FGK15F-NENTCDE                        
              MOVE FGK050-LIB-ENTCDE   TO FGK15F-LENTCDE                        
AP0904        WRITE ENR-FGK15F FROM FGK15F-ENR                                  
AP0904     END-IF.                                                              
JB1812*    MOVE FGK050-ADRESSE      TO FGK15F-ADDRESS       .                   
JB1812*    MOVE FGK050-TEL          TO FGK15F-NENTCDETEL    .                   
JB1812*    MOVE FGK050-TELX         TO FGK15F-CENTCDETELX   .                   
           ADD 1 TO CPT-FGK15F.                                                 
      ******************************************************************        
      * C'EST LA FIN PUISQU'IL EN FAUT UNE                                      
      * ECRITURE DU FICHIER DE CONTR�LE.                                        
      ******************************************************************        
       FIN-BGK150 SECTION.                                                      
    *** TYPO...                                                                 
           MOVE SPACES TO FGK15C-TPCE-NOM.                                      
           STRING 'MN_ProductHierarchy_004_'                                    
                   W-MOIS-MM W-MOIS-SS W-MOIS-AA                                
                   '_01.txt'                                                    
                   DELIMITED BY SIZE INTO  FGK15C-TPCE-NOM.                     
           MOVE CPT-FGK15T TO FGK15C-TPCE-NBENR.                                
           WRITE ENR-FGK15C FROM FGK15C-TPCE-ENR.                               
           ADD 1 TO CPT-FGK15C.                                                 
    *** ...PRODUITS...                                                          
           MOVE SPACES TO FGK15C-TPCE-NOM.                                      
           STRING 'MN_Product_004_'                                             
                   W-MOIS-MM W-MOIS-SS W-MOIS-AA                                
                   '_01.txt'                                                    
                   DELIMITED BY SIZE INTO  FGK15C-TPCE-NOM.                     
           MOVE CPT-FGK15P TO FGK15C-TPCE-NBENR.                                
           WRITE ENR-FGK15C FROM FGK15C-TPCE-ENR.                               
           ADD 1 TO CPT-FGK15C.                                                 
    *** ...CONTR�LE...                                                          
           MOVE SPACES TO FGK15C-TPCE-NOM.                                      
           STRING 'MN_ExtractValidation_004_'                                   
                   W-MOIS-MM W-MOIS-SS W-MOIS-AA                                
                   '_01.txt'                                                    
                   DELIMITED BY SIZE INTO  FGK15C-TPCE-NOM.                     
           MOVE 7         TO FGK15C-TPCE-NBENR.                                 
           WRITE ENR-FGK15C FROM FGK15C-TPCE-ENR.                               
           ADD 1 TO CPT-FGK15C.                                                 
    *** ...ENT�TE...                                                            
           MOVE SPACES TO FGK15C-TPCE-NOM.                                      
           STRING 'MN_ExtractHeader_004_'                                       
                   W-MOIS-MM W-MOIS-SS W-MOIS-AA                                
                   '_01.txt'                                                    
                   DELIMITED BY SIZE INTO  FGK15C-TPCE-NOM.                     
           MOVE 1         TO FGK15C-TPCE-NBENR.                                 
           WRITE ENR-FGK15C FROM FGK15C-TPCE-ENR.                               
           ADD 1 TO CPT-FGK15C.                                                 
    *** ...VENTES...                                                            
           MOVE SPACES TO FGK15C-V-NOM.                                         
           STRING 'MN_SalesStatistics_004_'                                     
                   W-MOIS-MM W-MOIS-SS W-MOIS-AA                                
                   '_01.txt'                                                    
                   DELIMITED BY SIZE INTO  FGK15C-V-NOM.                        
           MOVE CPT-FGK15V         TO FGK15C-V-NBENR.                           
           MOVE W-TOTAL-PVUHT      TO FGK15C-V-PVUHT.                           
JB1812*    MOVE W-TOTAL-PVMHT      TO FGK15C-V-PVMHT.                           
           MOVE W-TOTAL-QVENDUE    TO FGK15C-V-QVENDUE.                         
           MOVE W-TOTAL-STOCK      TO FGK15C-V-STOCK.                           
           MOVE W-TOTAL-PX-VTE-HT  TO FGK15C-V-PX-VTE-HT.                       
           MOVE W-TOTAL-MARGE-NET  TO FGK15C-V-MARGE-NET.                       
           MOVE W-TOTAL-VALOPRMP   TO FGK15C-V-VALOPRMP.                        
JB1812*    MOVE W-TOTAL-PSTDTTC    TO FGK15C-V-PSTDTTC.                         
JB1812*    MOVE W-TOTAL-STOCKMAG   TO FGK15C-V-STOCKMAG.                        
JB1812*    MOVE W-TOTAL-STOCKDEP   TO FGK15C-V-STOCKDEP.                        
           WRITE ENR-FGK15C FROM FGK15C-V-ENR.                                  
           ADD 1 TO CPT-FGK15C.                                                 
    *** ...ACHATS...                                                            
           MOVE SPACES TO FGK15C-A-NOM.                                         
           STRING 'MN_PurchaseStatistics_004_'                                  
                   W-MOIS-MM W-MOIS-SS W-MOIS-AA                                
                   '_01.txt'                                                    
                   DELIMITED BY SIZE INTO  FGK15C-A-NOM.                        
           MOVE CPT-FGK15A        TO FGK15C-A-NBENR.                            
           MOVE W-TOTAL-PRA       TO FGK15C-A-PRA.                              
JB1812*    MOVE W-TOTAL-PRAM      TO FGK15C-A-PRAM.                             
           MOVE W-TOTAL-QREC      TO FGK15C-A-QREC.                             
           MOVE W-TOTAL-QREC-VALO TO FGK15C-A-QREC-VALO.                        
JB1812*    MOVE W-TOTAL-QCDE      TO FGK15C-A-QCDE.                             
           WRITE ENR-FGK15C FROM FGK15C-A-ENR.                                  
           ADD 1 TO CPT-FGK15C.                                                 
    *** ...FOURNISSEUR ...                                                      
           MOVE SPACES TO FGK15C-F-NOM.                                         
           STRING 'MN_Supplier_004_'                                            
                   W-MOIS-MM W-MOIS-SS W-MOIS-AA                                
                   '_01.txt'                                                    
                   DELIMITED BY SIZE INTO  FGK15C-F-NOM.                        
           MOVE CPT-FGK15F       TO FGK15C-F-NBENR.                             
           WRITE ENR-FGK15C FROM FGK15C-F-ENR.                                  
           ADD 1 TO CPT-FGK15C.                                                 
    *** ...ET C'EST FINI.                                                       
           CLOSE FGK020 FGK050 FGK131 FGK140.                                   
           CLOSE FGK15C FGK15T FGK15P FGK15V FGK15A FGK15F.                     
           DISPLAY 'BGK150: FIN NORMALE DE PROGRAMME'.                          
           MOVE CPT-FGK020 TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS LUS SUR FGK020: ' PIC-EDITZ9.            
           MOVE CPT-FGK050 TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS LUS SUR FGK050: ' PIC-EDITZ9.            
           MOVE CPT-FGK131 TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS LUS SUR FGK131: ' PIC-EDITZ9.            
           MOVE CPT-FGK140 TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS LUS SUR FGK140: ' PIC-EDITZ9.            
           MOVE CPT-FGK15C TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS �CRITS SUR FGK15C: ' PIC-EDITZ9.         
           MOVE CPT-FGK15T TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS �CRITS SUR FGK15T: ' PIC-EDITZ9.         
           MOVE CPT-FGK15P TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS �CRITS SUR FGK15P: ' PIC-EDITZ9.         
           MOVE CPT-FGK15V TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS �CRITS SUR FGK15V: ' PIC-EDITZ9.         
           MOVE CPT-FGK15A TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS �CRITS SUR FGK15A: ' PIC-EDITZ9.         
           MOVE CPT-FGK15F TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS �CRITS SUR FGK15F: ' PIC-EDITZ9.         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      ******************************************************************        
      ******************************************************************        
      * BADABOUM                                                                
      ******************************************************************        
       ABEND-PROGRAMME SECTION.                                                 
           DISPLAY ABEND-MESS                                                   
           MOVE 'BGK150' TO ABEND-PROG.                                         
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL ABEND USING ABEND-PROG ABEND-MESS.                              
      *--                                                                       
           CALL MW-ABEND USING ABEND-PROG ABEND-MESS.                           
      *}                                                                        
