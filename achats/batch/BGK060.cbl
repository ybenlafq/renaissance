      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BGK060.                                                     
       AUTHOR. JG.                                                              
       ENVIRONMENT DIVISION.                                                    
      ******************************************************************        
      *                                                                         
      *  VENTES KRISP:                                                          
      *  G�N�RATION DU SYSIN POUR FAST UNLOAD DE GS30                           
      *                                                                         
      ******************************************************************        
      *  MODIF JB LE 22/12/03 : CREATION BGK060 A PARTIR BGK210                 
      ******************************************************************        
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FSYSIN  ASSIGN TO FSYSIN.                                    
      *--                                                                       
            SELECT FSYSIN  ASSIGN TO FSYSIN                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FGK060  ASSIGN TO FGK060.                                    
      *--                                                                       
            SELECT FGK060  ASSIGN TO FGK060                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FSYSIN RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
      *MW DAR-2                                                                 
       01  MW-FILLER PIC X(80).                                                 
       FD  FGK060 RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  ENR-FGK060 PIC X(80).                                                
       WORKING-STORAGE SECTION.                                                 
      ***************************************************************           
      * ZONE OUT POUR FGK060, FLAG POUR FSYSIN                                  
      ***************************************************************           
       01  W-FGK060 PIC X(80).                                                  
       01  FILLER PIC X VALUE SPACE.                                            
           88  FIN-FSYSIN VALUE 'F'.                                            
      ***************************************************************           
      * COMMS AVEC LES SOUS PROGS                                               
      ***************************************************************           
           COPY ABENDCOP.                                                       
      ***************************************************************           
      * PROC:                                                                   
      * - GENERATION DE SYSIN                                                   
      * - C'EST TOUT                                                            
      ***************************************************************           
       PROCEDURE DIVISION.                                                      
           DISPLAY 'BGK060: VENTES MENSUELLES POUR KRISP'                       
           OPEN INPUT FSYSIN                                                    
           OUTPUT FGK060.                                                       
      ******************************************************************        
      * RECOPIE DE FSYSIN                                                       
      ******************************************************************        
           DISPLAY 'BGK060: SYSIN G�N�R� � PARTIR DE FSYSIN:'                   
           PERFORM UNTIL FIN-FSYSIN                                             
              READ FSYSIN INTO W-FGK060                                         
                 AT END                                                         
                    SET FIN-FSYSIN TO TRUE                                      
                 NOT AT END                                                     
                    WRITE ENR-FGK060 FROM W-FGK060                              
                    DISPLAY W-FGK060                                            
              END-READ                                                          
           END-PERFORM.                                                         
           CLOSE FSYSIN.                                                        
      ******************************************************************        
      * GENERATION DE L'ORDRE SQL GS30                                          
      ******************************************************************        
           DISPLAY 'BGK060: SYSIN G�N�R� POUR RTGS30:'                          
           MOVE '   SELECT NCODIC, QSTOCK' TO W-FGK060.                         
           WRITE ENR-FGK060 FROM W-FGK060.                                      
           DISPLAY W-FGK060.                                                    
           MOVE '     FROM RTGS30;' TO W-FGK060.                                
           WRITE ENR-FGK060 FROM W-FGK060.                                      
           DISPLAY W-FGK060.                                                    
      ******************************************************************        
      * GENERATION DE L'ORDRE SQL GS31                                          
      ******************************************************************        
           DISPLAY 'BGK060: SYSIN G�N�R� POUR RTGS31:'                          
           MOVE '   SELECT NCODIC, QSTOCK' TO W-FGK060.                         
           WRITE ENR-FGK060 FROM W-FGK060.                                      
           DISPLAY W-FGK060.                                                    
           MOVE '     FROM RTGS31;' TO W-FGK060.                                
           WRITE ENR-FGK060 FROM W-FGK060.                                      
           DISPLAY W-FGK060.                                                    
      ******************************************************************        
      * ZIENDE                                                                  
      ******************************************************************        
           CLOSE FGK060.                                                        
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      ******************************************************************        
      * SCHPLOUF                                                                
      ******************************************************************        
       ABEND-PROGRAMME                 SECTION.                                 
           MOVE  'BGK060'     TO   ABEND-PROG.                                  
           CALL  'ABEND'   USING   ABEND-PROG  ABEND-MESS.                      
