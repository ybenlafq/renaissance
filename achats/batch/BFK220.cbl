      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. BFK220.                                                      
       AUTHOR. MM. SEPTEMBRE 2002                                               
      ******************************************************************        
      *  ENRICHISSEMENT DU FICHIER PRODUITS AVEC LES DONNEES VENTE              
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                                   
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  RTGS40  ASSIGN TO  RTGS40.                                  
      *--                                                                       
            SELECT  RTGS40  ASSIGN TO  RTGS40                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FFK200  ASSIGN TO  FFK200.                                  
      *--                                                                       
            SELECT  FFK200  ASSIGN TO  FFK200                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FFK220  ASSIGN TO  FFK220.                                  
      *--                                                                       
            SELECT  FFK220  ASSIGN TO  FFK220                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      ******************************************************************        
       DATA DIVISION.                                                           
      ******************************************************************        
       FILE SECTION.                                                            
       FD  RTGS40 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  RVGS4000.                                                            
           03  GS40-NCODIC    PIC X(7).                                         
           03  GS40-NLIEUORIG PIC X(3).                                         
           03  GS40-NLIEUDEST PIC X(3).                                         
           03  GS40-PVTOTALSR PIC S9(7)V9(0002) COMP-3.                         
           03  GS40-SANSDEC-SUP REDEFINES GS40-PVTOTALSR.                       
               05  GS40-PVTOTALSR-SANSDEC PIC S9(9) COMP-3.                     
           03  GS40-QMVT      PIC S9(5) COMP-3.                                 
           03  GS40-NSOCVTE   PIC X(3).                                         
           03  GS40-NLIEUVTE  PIC X(3).                                         
       FD  FFK200 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
      *MW DAR-2                                                                 
       01  MW-FILLER        PIC X(100).                                         
       FD  FFK220 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  ENR-FFK220    PIC X(150).                                            
      ******************************************************************        
       WORKING-STORAGE SECTION.                                                 
      ******************************************************************        
      ******************************************************************        
       01  PIC-EDITZ9    PIC ----.---.--9.                                      
       01  PIC-EDITV9    PIC ----.---.--9,9999.                                 
      *                                                                         
       01  W-TIME  PIC 9(8).                                                    
      **********************************************************                
      *                                                                         
       01  CPT-RTGS40  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
      **********************************************************                
      * CLES DE RTGS40                                                          
      **********************************************************                
       01  GS40-CLE-LUE-SUP.                                                    
           03  GS40-LUE-NCODIC        PIC X(7).                                 
           03  GS40-LUE-NSOCVTE   PIC X(3).                                     
           03  GS40-LUE-NLIEUVTE  PIC X(3).                                     
       01  GS40-CLE-STOCKEE-SUP.                                                
           03  GS40-STO-NCODIC        PIC X(7).                                 
           03  GS40-STO-NSOCVTE   PIC X(3).                                     
           03  GS40-STO-NLIEUVTE  PIC X(3).                                     
       01  FFK200-CLE-LUE.                                                      
           03  FFK200-LUE-NCODIC      PIC X(7).                                 
      ******************************************************************        
      * FFK200                                                                  
      ******************************************************************        
           COPY FFK200.                                                         
       01  CPT-FFK200  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  FILLER      PIC X VALUE SPACES.                                      
           88 FIN-FFK200     VALUE '0'.                                         
      **********************************************************                
      ******************************************************************        
      * FFK220                                                                  
      ******************************************************************        
           COPY FFK220.                                                         
       01  CPT-FFK220  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
      ******************************************************************        
      * POUR TOTALISER LES VALEURS DE FFK220                                    
      ******************************************************************        
       01  W-GS40-PVTOTALSR-NSOCLIEU  PIC S9(15) COMP-3 VALUE 0.                
       01  W-GS40-QMVT-NSOCLIEU       PIC S9(11) COMP-3 VALUE 0.                
      *01  W-PVTOTALSR               PIC  9(16)  VALUE 0.                       
      *01  W-QVENDUE                 PIC  9(12)  VALUE 0.                       
       01  W-EGALITE                 PIC  9      VALUE 0.                       
      ******************************************************************        
      *                                                                         
       01  FILLER        PIC X.                                                 
           88 TROUVE     VALUE '0'.                                             
           88 NON-TROUVE VALUE '1'.                                             
      *                                                                         
      ******************************************************************        
      *                                                                         
       PROCEDURE DIVISION.                                                      
      ***************************                                               
           PERFORM  DEBUT-BFK220.                                               
           PERFORM  TRAIT-BFK220.                                               
           PERFORM  FIN-BFK220.                                                 
      *                                                                         
      ******************************************************************        
      *                                                                         
       DEBUT-BFK220 SECTION.                                                    
           DISPLAY 'BFK220: VENTES HEBDO POUR GFK '                             
           OPEN INPUT RTGS40                                                    
                      FFK200                                                    
               OUTPUT FFK220.                                                   
      *                                                                         
      ******************************************************************        
      * PROGRAMME:                                                              
      * - BOUCLE DE LECTURE DE RTGS40:                                          
      *   - ACCUMULATION DES DONNEES PAR CODIC, NSOCVTE, NLIEUVTE               
      *   - ECRITURE SUR FFK220 D'UN ENREGISTREMENT EN RUPTURE GS40             
      ******************************************************************        
      *------------------------------------                                     
       TRAIT-BFK220 SECTION.                                                    
      *-----------------------------------------------                          
      *                                                                         
           PERFORM LECTURE-RTGS40.                                              
      *                                                                         
           MOVE GS40-LUE-NCODIC   TO GS40-STO-NCODIC                            
           MOVE GS40-LUE-NSOCVTE  TO GS40-STO-NSOCVTE                           
           MOVE GS40-LUE-NLIEUVTE TO GS40-STO-NLIEUVTE                          
      *                                                                         
           PERFORM LECTURE-FFK200.                                              
      *                                                                         
           PERFORM UNTIL GS40-CLE-LUE-SUP = HIGH-VALUE                          
             IF  GS40-LUE-NCODIC  =  FFK200-LUE-NCODIC                          
                 PERFORM CUMUL-MONT                                             
                     UNTIL GS40-CLE-LUE-SUP NOT =                               
                                 GS40-CLE-STOCKEE-SUP                           
      *                                                                         
                     PERFORM ECRITURE-FFK220                                    
      *                                                                         
                     MOVE GS40-LUE-NCODIC   TO GS40-STO-NCODIC                  
                     MOVE GS40-LUE-NSOCVTE  TO GS40-STO-NSOCVTE                 
                     MOVE GS40-LUE-NLIEUVTE TO GS40-STO-NLIEUVTE                
             ELSE                                                               
             IF  GS40-LUE-NCODIC  <  FFK200-LUE-NCODIC                          
                 PERFORM LECTURE-RTGS40                                         
                 MOVE GS40-LUE-NCODIC   TO GS40-STO-NCODIC                      
                 MOVE GS40-LUE-NSOCVTE  TO GS40-STO-NSOCVTE                     
                 MOVE GS40-LUE-NLIEUVTE TO GS40-STO-NLIEUVTE                    
             ELSE                                                               
                 PERFORM LECTURE-FFK200                                         
             END-IF                                                             
             END-IF                                                             
           END-PERFORM.                                                         
      *                                                                         
      *------------------------------*                                          
       CUMUL-MONT   SECTION.                                                    
      *------------------------------*                                          
      *                                                                         
           ADD GS40-PVTOTALSR-SANSDEC TO  W-GS40-PVTOTALSR-NSOCLIEU             
           ADD GS40-QMVT              TO  W-GS40-QMVT-NSOCLIEU                  
      *                                                                         
           PERFORM LECTURE-RTGS40.                                              
      *                                                                         
      ******************************************************************        
      ******************************************************************        
      * LECTURE FFK200                                                          
      ******************************************************************        
       LECTURE-FFK200 SECTION.                                                  
           READ FFK200 INTO FFK200-ENR                                          
             AT END                                                             
                MOVE HIGH-VALUE TO FFK200-CLE-LUE                               
             NOT AT END                                                         
                MOVE  FFK200-NCODIC   TO  FFK200-LUE-NCODIC                     
                ADD 1 TO CPT-FFK200                                             
                IF CPT-FFK200 = 1 OR 5000 OR 10000 OR 15000                     
                   OR 20000 OR 25000 OR 30000 OR 35000                          
                   OR 40000 OR 45000 OR 50000 OR 55000                          
                   OR 60000 OR 65000 OR 70000 OR 75000                          
                   OR 80000 OR 85000 OR 90000 OR 95000                          
                   MOVE CPT-FFK200 TO PIC-EDITZ9                                
                   ACCEPT W-TIME FROM TIME                                      
                   IF CPT-FFK200 = 1                                            
                      DISPLAY '         PREMIER'                                
                       ' ENRG. LU SUR FFK200, HEURE: ' W-TIME(1:2)              
                       ':' W-TIME(3:2) ':' W-TIME(5:2) ':' W-TIME(7:2)          
                   ELSE                                                         
                      DISPLAY '  ' PIC-EDITZ9                                   
                       ' ENRGS. LUS SUR FFK200, HEURE: ' W-TIME(1:2)            
                       ':' W-TIME(3:2) ':' W-TIME(5:2) ':' W-TIME(7:2)          
                   END-IF                                                       
                END-IF                                                          
           END-READ.                                                            
      ******************************************************************        
      * LECTURE RTGS40: TRANSFO DES SIGNES.                                     
      ******************************************************************        
       LECTURE-RTGS40 SECTION.                                                  
           READ RTGS40                                                          
             AT END                                                             
                MOVE HIGH-VALUE TO GS40-CLE-LUE-SUP                             
             NOT AT END                                                         
                IF (GS40-NLIEUORIG = 'VEN')                                     
                  AND (GS40-QMVT > 0)                                           
                   COMPUTE GS40-QMVT = GS40-QMVT * -1                           
                END-IF                                                          
                IF (GS40-NLIEUORIG = 'VEN')                                     
                  AND (GS40-PVTOTALSR > 0)                                      
                   COMPUTE GS40-PVTOTALSR = GS40-PVTOTALSR * -1                 
                END-IF                                                          
                MOVE GS40-NCODIC   TO GS40-LUE-NCODIC                           
                MOVE GS40-NSOCVTE  TO GS40-LUE-NSOCVTE                          
                MOVE GS40-NLIEUVTE TO GS40-LUE-NLIEUVTE                         
                ADD 1 TO CPT-RTGS40                                             
           END-READ.                                                            
      ******************************************************************        
      ******************************************************************        
      * ECRITURE SUR FFK220.                                                    
      ******************************************************************        
       ECRITURE-FFK220        SECTION.                                          
      *-------------------------------*                                         
      *                                                                         
           MOVE SPACE                   TO FFK220-ENR                           
           INITIALIZE  FFK220-ENR                                               
           MOVE FFK200-NCODIC           TO FFK220-NCODIC                        
           MOVE FFK200-CFAM             TO FFK220-CFAM                          
           MOVE FFK200-LFAM             TO FFK220-LFAM                          
           MOVE FFK200-LREFFOURN        TO FFK220-LREFFOURN                     
           MOVE FFK200-LMARQ            TO FFK220-LMARQ                         
           MOVE FFK200-NEAN             TO FFK220-NEAN                          
           MOVE GS40-STO-NSOCVTE        TO FFK220-NSOCVTE                       
           MOVE GS40-STO-NLIEUVTE       TO FFK220-NLIEUVTE                      
           MOVE FFK200-PERIODE          TO FFK220-PERIODE                       
           MOVE W-GS40-PVTOTALSR-NSOCLIEU  TO  FFK220-PVTOTALSR                 
           MOVE W-GS40-QMVT-NSOCLIEU       TO  FFK220-QVENDUE                   
      *                                                                         
           WRITE ENR-FFK220 FROM FFK220-ENR.                                    
           MOVE  0   TO   W-GS40-PVTOTALSR-NSOCLIEU                             
                          W-GS40-QMVT-NSOCLIEU.                                 
           ADD 1                 TO CPT-FFK220.                                 
      *-----------------------------------------*                               
      *                                                                         
      *-------------------------------------------------                        
       FIN-BFK220 SECTION.                                                      
           CLOSE RTGS40  FFK200 FFK220.                                         
           DISPLAY 'BFK220: FIN NORMALE DE PROGRAMME'.                          
           MOVE CPT-RTGS40 TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS LUS SUR RTGS40: ' PIC-EDITZ9.            
           MOVE CPT-FFK200 TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS LUS SUR FFK200: ' PIC-EDITZ9.            
           MOVE CPT-FFK220 TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS ECRITS SUR FFK220: ' PIC-EDITZ9.         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *****************************************************************         
