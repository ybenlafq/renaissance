      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BFK210.                                                     
       AUTHOR. MM. SEPTEMBRE 2002.                                              
       ENVIRONMENT DIVISION.                                                    
      ******************************************************************        
      *  VENTES GFK:                                                            
      *  GENERATION DU SYSIN POUR FAST UNLOAD DE GS40 et GS42                   
      ******************************************************************        
DA    *  DATE       : 17/01/2013   - AUTEUR : DSA008 (DA)                       
      *  OBJET      : ON NE PREND PLUS QUE LES VENTES PLUS LES REPRISES         
      ******************************************************************        
DA01  *  DATE       : 22/04/2013   - AUTEUR : DSA008 (DA01)                     
      *  OBJET      : ON PREND LES VENTES EN COMMERCIAL                         
      *               ( DCREATION ET NON PLUS DMVT )                            
      ******************************************************************        
DA02  *  DATE       : 02/07/2013   - AUTEUR : DSA008 (DA02)                     
      *  OBJET      : ON NE PREND PLUS LE CODIC 3756653                         
      *               ET RETOUR ARRIERE SUR LES VENTES EN COMMERCIAL            
      *               ON REPART EN TOPE                                         
      ******************************************************************        
      *                                                                         
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FDATE   ASSIGN TO FDATE.                                     
      *--                                                                       
            SELECT FDATE   ASSIGN TO FDATE                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FMGI    ASSIGN TO FMGI.                                      
      *--                                                                       
            SELECT FMGI    ASSIGN TO FMGI                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FSYSIN  ASSIGN TO FSYSIN.                                    
      *--                                                                       
            SELECT FSYSIN  ASSIGN TO FSYSIN                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FFK210  ASSIGN TO FFK210.                                    
      *--                                                                       
            SELECT FFK210  ASSIGN TO FFK210                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FDATE RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.             
       01  MW-FILLER PIC X(80).                                                 
       FD  FMGI   RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  MW-FILLER PIC X(80).                                                 
       FD  FSYSIN RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
      *MW DAR-2                                                                 
       01  MW-FILLER PIC X(80).                                                 
       FD  FFK210 RECORDING MODE IS F BLOCK 0 LABEL RECORD STANDARD.            
       01  ENR-FFK210 PIC X(80).                                                
       WORKING-STORAGE SECTION.                                                 
      *                                                                         
      ***************************************************************           
      * FMGI=POUR ACCES A RTGS42 SUR LES FILIALES MGI                           
      * FMGI=961945991907                                                       
      * FMGI=961945  SUPPRIMEES(fevrier 2003),PASSEES NMD                       
      * FMGI=991907                                                             
      ***************************************************************           
       01  W-FMGI-SUP.                                                          
           03 W-FMGI PIC X(03) OCCURS 4.                                        
           03 FILLER PIC X(68).                                                 
      *                                                                         
      ***************************************************************           
      * ZONE OUT POUR FFK210, FLAG POUR FSYSIN                                  
      ***************************************************************           
       01  W-FFK210 PIC X(80).                                                  
       01  FILLER PIC X VALUE SPACE.                                            
           88  FIN-FSYSIN VALUE 'F'.                                            
      ***************************************************************           
      * COMMS AVEC LES SOUS PROGS                                               
      ***************************************************************           
           COPY ABENDCOP.                                                       
       01  BETDATC PIC X(8) VALUE 'BETDATC'.                                    
           COPY WORKDATC.                                                       
      ***************************************************************           
      * DATES CALCULEES                                                         
      ***************************************************************           
       01  W-DDEBUT PIC X(8).                                                   
       01  W-DFIN   PIC X(8).                                                   
      ***************************************************************           
      * PROC:                                                                   
      * - LECTURE DATE                                                          
      * - CALCUL DES BORNES DE DATES                                            
      * - GENERATION DE SYSIN                                                   
      ***************************************************************           
       PROCEDURE DIVISION.                                                      
      *                                                                         
           DISPLAY 'BFK210: VENTES HEBDO POUR GFK  '                            
           OPEN INPUT FDATE FSYSIN FMGI                                         
           OUTPUT FFK210.                                                       
           READ FDATE INTO GFJJMMSSAA AT END                            01090024
                MOVE 'FICHIER FDATE VIDE' TO ABEND-MESS                 01100024
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           CLOSE FDATE.                                                 01130024
           MOVE SPACE  TO   W-FMGI-SUP                                          
           READ FMGI   INTO W-FMGI-SUP                                          
           END-READ.                                                            
           CLOSE FMGI.                                                          
           MOVE '1'            TO GFDATA.                               00004640
           CALL BETDATC      USING WORK-BETDATC                         00004670
           IF GFVDAT NOT = '1'                                          00004680
              STRING 'BETDATC : ' GF-MESS-ERR                           00004700
              DELIMITED BY SIZE INTO ABEND-MESS                         00004710
              PERFORM ABEND-PROGRAMME                                           
           END-IF.                                                      00004730
           DISPLAY 'BFK210: DATE PARAMETRE LUE SUR FDATE: '                     
                    GFSMN-LIB-L ' ' GFJOUR ' '                                  
                    GFMOI-LIB-L ' ' GFSIECLE GFANNEE.                           
           MOVE GFSAMJ-0 TO W-DFIN.                                             
      ***************************************************************           
      * CALCUL DES BORNES HEBDO                                                 
      ***************************************************************           
           SUBTRACT 6 FROM GFQNT0                                               
           MOVE '3'            TO GFDATA                                00004640
           CALL BETDATC      USING WORK-BETDATC                         00004670
           IF GFVDAT NOT = '1'                                          00004680
              STRING 'BETDATC : ' GF-MESS-ERR                           00004700
              DELIMITED BY SIZE INTO ABEND-MESS                         00004710
              PERFORM ABEND-PROGRAMME                                           
           END-IF.                                                      00004730
           DISPLAY 'BFK210: DATE DEBUT DE RECHERCHE: '                          
                    GFSMN-LIB-L ' ' GFJOUR ' '                                  
                    GFMOI-LIB-L ' ' GFSIECLE GFANNEE.                           
           MOVE GFSAMJ-0 TO W-DDEBUT.                                           
      ******************************************************************        
      * RECOPIE DE FSYSIN                                                       
      ******************************************************************        
      *                                                                         
           DISPLAY 'BFK210: SYSIN GENERE A PARTIR DE FSYSIN:'                   
           PERFORM UNTIL FIN-FSYSIN                                             
              READ FSYSIN INTO W-FFK210                                         
                 AT END                                                         
                    SET FIN-FSYSIN TO TRUE                                      
                 NOT AT END                                                     
                    WRITE ENR-FFK210 FROM W-FFK210                              
                    DISPLAY W-FFK210                                            
              END-READ                                                          
           END-PERFORM.                                                         
           CLOSE FSYSIN.                                                        
      *                                                                         
      ******************************************************************        
      * GENERATION DE L'ORDRE SQL GS40                                          
      ******************************************************************        
      *                                                                         
             DISPLAY 'BFK210: SYSIN GENERE POUR RTGS40:'                        
             MOVE '   SELECT NCODIC, NLIEUORIG, NLIEUDEST, ' TO W-FFK210.       
             WRITE ENR-FFK210 FROM W-FFK210                                     
             DISPLAY W-FFK210                                                   
             MOVE '   PVTOTALSR, QMVT, NSOCVTE, NLIEUVTE '   TO W-FFK210        
             WRITE ENR-FFK210 FROM W-FFK210                                     
             DISPLAY W-FFK210                                                   
             MOVE '     FROM PDARTY.RTGS40 ' TO W-FFK210                        
             WRITE ENR-FFK210 FROM W-FFK210                                     
             DISPLAY W-FFK210                                                   
 DA   * ON NE PREND PLUS LES RETOURS                                            
 DA   *      MOVE '    WHERE (NLIEUORIG = ''VEN''' TO W-FFK210                  
 DA   *      WRITE ENR-FFK210 FROM W-FFK210                                     
 DA   *      DISPLAY W-FFK210                                                   
 DA   *      MOVE '            OR NLIEUDEST = ''VEN'')' TO W-FFK210             
 DA   *      WRITE ENR-FFK210 FROM W-FFK210                                     
 DA   *      DISPLAY W-FFK210                                                   
 DA          MOVE '    WHERE  NLIEUDEST = ''VEN''' TO W-FFK210                  
 DA          WRITE ENR-FFK210 FROM W-FFK210                                     
 DA          DISPLAY W-FFK210                                                   
 DA02        MOVE '      AND DMVT  >= ''' TO W-FFK210                           
 DA02 *      MOVE '      AND DCREATION  >= ''' TO W-FFK210                      
 DA02        MOVE W-DDEBUT TO W-FFK210(21:)                                     
 DA02 *      MOVE W-DDEBUT TO W-FFK210(26:)                                     
 DA02        MOVE '''' TO W-FFK210(29:)                                         
 DA02 *      MOVE '''' TO W-FFK210(34:)                                         
             WRITE ENR-FFK210 FROM W-FFK210                                     
             DISPLAY W-FFK210                                                   
 DA02        MOVE '      AND DMVT  <= ''' TO W-FFK210                           
 DA02 *      MOVE '      AND DCREATION  <= ''' TO W-FFK210                      
 DA02        MOVE W-DFIN TO W-FFK210(21:)                                       
 DA02 *      MOVE W-DFIN TO W-FFK210(26:)                                       
 DA02        MOVE '''  ' TO W-FFK210(29:)                                       
 DA02 *      MOVE '''  ' TO W-FFK210(34:)                                       
             WRITE ENR-FFK210 FROM W-FFK210                                     
             DISPLAY W-FFK210                                                   
      *{Post-translation Correct-Sign-not
      *      MOVE '      AND NCODIC ^= ''3756653'' ;' TO W-FFK210               
 DA02        MOVE '      AND NCODIC <> ''3756653'' ;' TO W-FFK210               
      *}
 DA02        WRITE ENR-FFK210 FROM W-FFK210                                     
 DA02        DISPLAY W-FFK210.                                                  
      ****************************************************************          
      * GENERATION DE L'ORDRE SQL GS42 pour les filales encore MGI   *          
      ****************************************************************          
      * W-FMGI = 961  ET W-FMGI  SUPPRIMEES                                     
      * W-FMGI (1) = 961                                                        
      *      IF W-FMGI (1)  = '961'                                             
      *                                                                         
      *          DISPLAY 'BFK210: SYSIN GENERE POUR RTGS42 '                    
      *          DISPLAY 'POUR LA FILIALE P961  :'                              
      *          MOVE '   SELECT NCODIC, NLIEUORIG, NLIEUDEST, '        .       
      *                                       TO W-FFK210               .       
      *          WRITE ENR-FFK210 FROM W-FFK210                                 
      *          DISPLAY W-FFK210                                               
      *          MOVE '   PVTOTALSR, QMVT, NSOCVTE, NLIEUVTE '                  
      *                                       TO W-FFK210                       
      *          WRITE ENR-FFK210 FROM W-FFK210                                 
      *          DISPLAY W-FFK210                                               
      *          MOVE '     FROM P961.RTGS42 ' TO W-FFK210                      
      *          WRITE ENR-FFK210 FROM W-FFK210                                 
      *          DISPLAY W-FFK210                                               
      *          MOVE '    WHERE (NLIEUORIG = ''VEN''' TO W-FFK210              
      *          WRITE ENR-FFK210 FROM W-FFK210                                 
      *          DISPLAY W-FFK210                                               
      *          MOVE '        OR NLIEUDEST = ''VEN'')' TO W-FFK210             
      *          WRITE ENR-FFK210 FROM W-FFK210                                 
      *          DISPLAY W-FFK210                                               
      *          MOVE '      AND DMVT  >= ''' TO W-FFK210                       
      *          MOVE W-DDEBUT TO W-FFK210(21:)                                 
      *          MOVE '''' TO W-FFK210(29:)                                     
      *          WRITE ENR-FFK210 FROM W-FFK210                                 
      *          DISPLAY W-FFK210                                               
      *          MOVE '      AND DMVT  <= ''' TO W-FFK210                       
      *          MOVE W-DFIN TO W-FFK210(21:)                                   
      *          MOVE ''' ;' TO W-FFK210(29:)                                   
      *          WRITE ENR-FFK210 FROM W-FFK210                                 
      *          DISPLAY W-FFK210                                               
      *      END-IF.                                                            
      * W-FMGI (2) = 945                                                        
      *      IF W-FMGI (2)  = '945'                                             
      *                                                                         
      *          DISPLAY 'BFK210: SYSIN GENERE POUR RTGS42 '                    
      *          DISPLAY 'POUR LA FILIALE P945  :'                              
      *          MOVE '   SELECT NCODIC, NLIEUORIG, NLIEUDEST, '        .       
      *                                       TO W-FFK210               .       
      *          WRITE ENR-FFK210 FROM W-FFK210                                 
      *          DISPLAY W-FFK210                                               
      *          MOVE '   PVTOTALSR, QMVT, NSOCVTE, NLIEUVTE '                  
      *                                       TO W-FFK210                       
      *          WRITE ENR-FFK210 FROM W-FFK210                                 
      *          DISPLAY W-FFK210                                               
      *          MOVE '     FROM P945.RTGS42 ' TO W-FFK210                      
      *          WRITE ENR-FFK210 FROM W-FFK210                                 
      *          DISPLAY W-FFK210                                               
      *          MOVE '    WHERE (NLIEUORIG = ''VEN''' TO W-FFK210              
      *          WRITE ENR-FFK210 FROM W-FFK210                                 
      *          DISPLAY W-FFK210                                               
      *          MOVE '        OR NLIEUDEST = ''VEN'')' TO W-FFK210             
      *          WRITE ENR-FFK210 FROM W-FFK210                                 
      *          DISPLAY W-FFK210                                               
      *          MOVE '      AND DMVT  >= ''' TO W-FFK210                       
      *          MOVE W-DDEBUT TO W-FFK210(21:)                                 
      *          MOVE '''' TO W-FFK210(29:)                                     
      *          WRITE ENR-FFK210 FROM W-FFK210                                 
      *          DISPLAY W-FFK210                                               
      *          MOVE '      AND DMVT  <= ''' TO W-FFK210                       
      *          MOVE W-DFIN TO W-FFK210(21:)                                   
      *          MOVE ''' ;' TO W-FFK210(29:)                                   
      *          WRITE ENR-FFK210 FROM W-FFK210                                 
      *          DISPLAY W-FFK210                                               
      *     END-IF.                                                             
      * W-FMGI     = 991                                                        
             IF (W-FMGI (1)          = '991'                                    
                OR W-FMGI (2)        = '991'                                    
                   OR W-FMGI (3)     = '991'                                    
                      OR W-FMGI (4)  = '991')                                   
                 DISPLAY 'BFK210: SYSIN GENERE POUR RTGS42 '                    
                 DISPLAY 'POUR LA FILIALE P991  :'                              
                 MOVE '   SELECT NCODIC, NLIEUORIG, NLIEUDEST, '        .       
                                              TO W-FFK210               .       
                 WRITE ENR-FFK210 FROM W-FFK210                                 
                 DISPLAY W-FFK210                                               
                 MOVE '   PVTOTALSR, QMVT, NSOCVTE, NLIEUVTE '                  
                                              TO W-FFK210                       
                 WRITE ENR-FFK210 FROM W-FFK210                                 
                 DISPLAY W-FFK210                                               
                 MOVE '     FROM P991.RTGS42 ' TO W-FFK210                      
                 WRITE ENR-FFK210 FROM W-FFK210                                 
                 DISPLAY W-FFK210                                               
 DA   * ON NE PREND PLUS LES RETOURS                                            
 DA   *          MOVE '    WHERE (NLIEUORIG = ''VEN''' TO W-FFK210              
 DA   *          WRITE ENR-FFK210 FROM W-FFK210                                 
 DA   *          DISPLAY W-FFK210                                               
 DA   *          MOVE '        OR NLIEUDEST = ''VEN'')' TO W-FFK210             
 DA   *          WRITE ENR-FFK210 FROM W-FFK210                                 
 DA   *          DISPLAY W-FFK210                                               
 DA              MOVE '    WHERE  NLIEUDEST = ''VEN''' TO W-FFK210              
 DA              WRITE ENR-FFK210 FROM W-FFK210                                 
 DA              DISPLAY W-FFK210                                               
 DA02            MOVE '      AND DMVT  >= ''' TO W-FFK210                       
 DA02 *          MOVE '      AND DCREATION  >= ''' TO W-FFK210                  
 DA02            MOVE W-DDEBUT TO W-FFK210(21:)                                 
 DA02 *          MOVE W-DDEBUT TO W-FFK210(26:)                                 
 DA02            MOVE '''' TO W-FFK210(29:)                                     
 DA02 *          MOVE '''' TO W-FFK210(34:)                                     
                 WRITE ENR-FFK210 FROM W-FFK210                                 
                 DISPLAY W-FFK210                                               
 DA02            MOVE '      AND DMVT  <= ''' TO W-FFK210                       
 DA02 *          MOVE '      AND DCREATION  <= ''' TO W-FFK210                  
 DA02            MOVE W-DFIN TO W-FFK210(21:)                                   
 DA02 *          MOVE W-DFIN TO W-FFK210(26:)                                   
 DA02            MOVE '''  ' TO W-FFK210(29:)                                   
 DA02 *          MOVE '''  ' TO W-FFK210(34:)                                   
                 WRITE ENR-FFK210 FROM W-FFK210                                 
                 DISPLAY W-FFK210                                               
      *{Post-translation Correct-Sign-not
      *          MOVE '      AND NCODIC ^= ''3756653'' ;' TO W-FFK210           
 DA02            MOVE '      AND NCODIC <> ''3756653'' ;' TO W-FFK210           
      *}
 DA02            WRITE ENR-FFK210 FROM W-FFK210                                 
 DA02            DISPLAY W-FFK210                                               
            END-IF.                                                             
      * W-FMGI   = 907                                                          
             IF (W-FMGI (1)         = '907'                                     
                OR W-FMGI (2)       = '907'                                     
                   OR W-FMGI (3)    = '907'                                     
                      OR W-FMGI (4) = '907')                                    
                 DISPLAY 'BFK210: SYSIN GENERE POUR RTGS42 '                    
                 DISPLAY 'POUR LA FILIALE P907  :'                              
                 MOVE '   SELECT NCODIC, NLIEUORIG, NLIEUDEST, '        .       
                                              TO W-FFK210               .       
                 WRITE ENR-FFK210 FROM W-FFK210                                 
                 DISPLAY W-FFK210                                               
                 MOVE '   PVTOTALSR, QMVT, NSOCVTE, NLIEUVTE '                  
                                              TO W-FFK210                       
                 WRITE ENR-FFK210 FROM W-FFK210                                 
                 DISPLAY W-FFK210                                               
                 MOVE '     FROM PDARTY.RTGS42 ' TO W-FFK210                    
                 WRITE ENR-FFK210 FROM W-FFK210                                 
                 DISPLAY W-FFK210                                               
 DA   * ON NE PREND PLUS LES RETOURS                                            
 DA   *          MOVE '    WHERE (NLIEUORIG = ''VEN''' TO W-FFK210              
 DA   *          WRITE ENR-FFK210 FROM W-FFK210                                 
 DA   *          DISPLAY W-FFK210                                               
 DA   *          MOVE '        OR NLIEUDEST = ''VEN'')' TO W-FFK210             
 DA   *          WRITE ENR-FFK210 FROM W-FFK210                                 
 DA   *          DISPLAY W-FFK210                                               
 DA              MOVE '    WHERE  NLIEUDEST = ''VEN''' TO W-FFK210              
 DA              WRITE ENR-FFK210 FROM W-FFK210                                 
 DA              DISPLAY W-FFK210                                               
 DA02            MOVE '      AND DMVT  >= ''' TO W-FFK210                       
 DA02 *          MOVE '      AND DCREATION  >= ''' TO W-FFK210                  
 DA02            MOVE W-DDEBUT TO W-FFK210(21:)                                 
 DA02 *          MOVE W-DDEBUT TO W-FFK210(26:)                                 
 DA02            MOVE '''' TO W-FFK210(29:)                                     
 DA02 *          MOVE '''' TO W-FFK210(34:)                                     
                 WRITE ENR-FFK210 FROM W-FFK210                                 
                 DISPLAY W-FFK210                                               
 DA02            MOVE '      AND DMVT  <= ''' TO W-FFK210                       
 DA02 *          MOVE '      AND DCREATION  <= ''' TO W-FFK210                  
 DA02            MOVE W-DFIN TO W-FFK210(21:)                                   
 DA02 *          MOVE W-DFIN TO W-FFK210(26:)                                   
 DA02            MOVE '''  ' TO W-FFK210(29:)                                   
 DA02 *          MOVE '''  ' TO W-FFK210(34:)                                   
                 WRITE ENR-FFK210 FROM W-FFK210                                 
                 DISPLAY W-FFK210                                               
      *{Post-translation Correct-Sign-not
      *          MOVE '      AND NCODIC ^= ''3756653'' ;' TO W-FFK210           
 DA02            MOVE '      AND NCODIC <> ''3756653'' ;' TO W-FFK210           
      *}
 DA02            WRITE ENR-FFK210 FROM W-FFK210                                 
 DA02            DISPLAY W-FFK210                                               
            END-IF.                                                             
      *                                                                         
      ******************************************************************        
      ******************************************************************        
           CLOSE FFK210.                                                        
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      ******************************************************************        
       ABEND-PROGRAMME                 SECTION.                                 
           MOVE  'BFK210'     TO   ABEND-PROG.                                  
           CALL  'ABEND'   USING   ABEND-PROG  ABEND-MESS.                      
