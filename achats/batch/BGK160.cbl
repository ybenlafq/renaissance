      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. BGK160.                                                      
       AUTHOR. JG.                                                              
      ******************************************************************        
      *                                                                         
      *  MISE EN FORME DE TOUS LES FICHIERS � ENVOYER � KRISP ET                
      *  G�N�RATION DU FICHIER ENT�TE FGK16E.                                   
      *  VERSION MENSUELLE (FICHIER D'ENTETE DIFFERENT)                         
      *                                                                         
      *  - CONTR�LE (FGK15C+FGK15C) -> FGK16C                                   
      *  - TYPO (FGK15T) -> FGK16T                                              
      *  - PRODUITS (FGK15P) -> FGK16P                                          
      *  - FOURNISSEURS (FGK15F) -> FGK16F                                      
      *  - VENTES (FGK15V) -> FGK16V                                            
      *  - ACHATS (FGK15A) -> FGK16A                                            
      *                                                                         
      ******************************************************************        
      * DSA015 - 13/02/12 : ajout de mois et ann�e dans nom de l'archive        
      *                     envoy�e par CFT => creation d'une sysin             
      *                     utilis�e dans le step de send cft                   
      ******************************************************************        
      * DSA015 - 03/07/13 : arr�t de cft et passage en ftp                      
      *                           => modification de la sysin                   
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                                   
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FDATE   ASSIGN TO  FDATE.                                   
      *--                                                                       
            SELECT  FDATE   ASSIGN TO  FDATE                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FMOIS   ASSIGN TO  FMOIS.                                   
      *--                                                                       
            SELECT  FMOIS   ASSIGN TO  FMOIS                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FPARAM  ASSIGN TO  FPARAM.                                  
      *--                                                                       
            SELECT  FPARAM  ASSIGN TO  FPARAM                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK15C  ASSIGN TO  FGK15C.                                  
      *--                                                                       
            SELECT  FGK15C  ASSIGN TO  FGK15C                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK15T  ASSIGN TO  FGK15T.                                  
      *--                                                                       
            SELECT  FGK15T  ASSIGN TO  FGK15T                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK15P  ASSIGN TO  FGK15P.                                  
      *--                                                                       
            SELECT  FGK15P  ASSIGN TO  FGK15P                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK15F  ASSIGN TO  FGK15F.                                  
      *--                                                                       
            SELECT  FGK15F  ASSIGN TO  FGK15F                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK15V  ASSIGN TO  FGK15V.                                  
      *--                                                                       
            SELECT  FGK15V  ASSIGN TO  FGK15V                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK15A  ASSIGN TO  FGK15A.                                  
      *--                                                                       
            SELECT  FGK15A  ASSIGN TO  FGK15A                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK160  ASSIGN TO  FGK160.                                  
      *--                                                                       
            SELECT  FGK160  ASSIGN TO  FGK160                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK161  ASSIGN TO  FGK161.                                  
      *--                                                                       
            SELECT  FGK161  ASSIGN TO  FGK161                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK16E  ASSIGN TO  FGK16E.                                  
      *--                                                                       
            SELECT  FGK16E  ASSIGN TO  FGK16E                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK16C  ASSIGN TO  FGK16C.                                  
      *--                                                                       
            SELECT  FGK16C  ASSIGN TO  FGK16C                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK16T  ASSIGN TO  FGK16T.                                  
      *--                                                                       
            SELECT  FGK16T  ASSIGN TO  FGK16T                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK16P  ASSIGN TO  FGK16P.                                  
      *--                                                                       
            SELECT  FGK16P  ASSIGN TO  FGK16P                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK16F  ASSIGN TO  FGK16F.                                  
      *--                                                                       
            SELECT  FGK16F  ASSIGN TO  FGK16F                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK16V  ASSIGN TO  FGK16V.                                  
      *--                                                                       
            SELECT  FGK16V  ASSIGN TO  FGK16V                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK16A  ASSIGN TO  FGK16A.                                  
      *--                                                                       
            SELECT  FGK16A  ASSIGN TO  FGK16A                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
0212  *     SELECT  FDCFT   ASSIGN TO  FDCFT.                                   
      *--                                                                       
            SELECT  FDCFT   ASSIGN TO  FDCFT                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      ******************************************************************        
       DATA DIVISION.                                                           
      ******************************************************************        
       FILE SECTION.                                                            
      * PARMS...                                                                
       FD  FDATE RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.             
       01  MW-FILLER        PIC X(80).                                          
      *                                                                         
       FD  FMOIS                                                                
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01 MW-FILLER        PIC X(80).                                           
      *                                                                         
       FD  FPARAM RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER        PIC X(80).                                          
      * INPUT...                                                                
       FD  FGK15C RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01 MW-FILLER        PIC X(200).                                          
       FD  FGK15T RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER        PIC X(200).                                         
       FD  FGK15P RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER        PIC X(200).                                         
       FD  FGK15F RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01 MW-FILLER        PIC X(200).                                          
       FD  FGK15V RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER        PIC X(200).                                         
       FD  FGK15A RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER        PIC X(200).                                         
       FD  FGK160 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
      *MW DAR-2                                                                 
       01  MW-FILLER        PIC X(10).                                          
      * OUTPUT...                                                               
       FD  FGK161 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  ENR-FGK161    PIC X(10).                                             
       FD  FGK16E RECORDING V BLOCK 0 RECORDS                                   
           RECORD VARYING FROM 10 TO 196 DEPENDING ON FGK16O-LENGTH             
           LABEL RECORD STANDARD.                                               
       01  ENR-FGK16E    PIC X(196).                                            
       FD  FGK16C RECORDING V BLOCK 0 RECORDS                                   
           RECORD VARYING FROM 10 TO 196 DEPENDING ON FGK16O-LENGTH             
           LABEL RECORD STANDARD.                                               
       01  ENR-FGK16C    PIC X(196).                                            
       FD  FGK16T RECORDING V BLOCK 0 RECORDS                                   
           RECORD VARYING FROM 10 TO 196 DEPENDING ON FGK16O-LENGTH             
           LABEL RECORD STANDARD.                                               
       01  ENR-FGK16T    PIC X(196).                                            
       FD  FGK16P RECORDING V BLOCK 0 RECORDS                                   
           RECORD VARYING FROM 10 TO 196 DEPENDING ON FGK16O-LENGTH             
           LABEL RECORD STANDARD.                                               
       01  ENR-FGK16P    PIC X(196).                                            
       FD  FGK16V RECORDING V BLOCK 0 RECORDS                                   
           RECORD VARYING FROM 10 TO 196 DEPENDING ON FGK16O-LENGTH             
           LABEL RECORD STANDARD.                                               
       01  ENR-FGK16V    PIC X(196).                                            
       FD  FGK16F RECORDING V BLOCK 0 RECORDS                                   
           RECORD VARYING FROM 10 TO 196 DEPENDING ON FGK16O-LENGTH             
           LABEL RECORD STANDARD.                                               
       01  ENR-FGK16F    PIC X(196).                                            
       FD  FGK16A RECORDING V BLOCK 0 RECORDS                                   
           RECORD VARYING FROM 10 TO 196 DEPENDING ON FGK16O-LENGTH             
           LABEL RECORD STANDARD.                                               
       01  ENR-FGK16A    PIC X(196).                                            
0212   FD  FDCFT  RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
0212   01  ENR-FDCFT     PIC X(80).                                             
      ******************************************************************        
       WORKING-STORAGE SECTION.                                                 
      ******************************************************************        
      * QUELQUES ZONES... QUI SERVENT - ON S'EN DOUTE - � QUELQUE CHOSE         
      ******************************************************************        
       01  W-MOISANNEE    PIC X(8)     VALUE SPACES.                            
       01  PIC-EDITZ9     PIC ----.---.--9.                                     
       01  PIC-EDITV9     PIC ----.---.--9,99.                                  
       01  FPARAM-ENR.                                                          
           02  FPARAM-CDEVISE PIC X(05).                                        
           02  FPARAM-LDEVISE PIC X(50).                                        
      *                                                                         
       01  ENR-FMOIS.                                                           
           03  W-MOIS.                                                          
               05 W-MOIS-MM  PIC X(02).                                         
               05 W-MOIS-SS  PIC X(02).                                         
               05 W-MOIS-AA  PIC X(02).                                         
           03  FILLER    PIC X(74).                                             
      ******************************************************************        
      * FGK160/341: DERNIER ENVOI                                               
      ******************************************************************        
       01  FGK160-ENR.                                                          
           02  FGK160-MOISANNEE       PIC X(08).                                
   7       02  FGK160-VERSION         PIC 9(02).                                
      ******************************************************************        
      * FGK16E: ENT�TE (6 CHAMPS)                                               
      ******************************************************************        
       01  FGK16E-ENR.                                                          
   1       02  FGK16E-NSOC            PIC X(03) VALUE '004'.                    
           02  FILLER                 PIC X(02) VALUE ',"'.                     
   2       02  FGK16E-LSOC            PIC X(20) VALUE 'DARTY'.                  
           02  FILLER                 PIC X(03) VALUE '","'.                    
   3       02  FGK16E-DATETIME        PIC X(20).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
   4       02  FGK16E-DATE            PIC X(08).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
   5       02  FGK16E-CDEVISE         PIC X(05).                                
           02  FILLER                 PIC X(03) VALUE '","'.                    
   6       02  FGK16E-LDEVISE         PIC X(50).                                
           02  FILLER                 PIC X(01) VALUE '"'.                      
       01  CPT-FGK16E  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  W-DATE      PIC 9(6).                                                
       01  W-TIME      PIC 9(8).                                                
       01  W-LIBELLE-MOIS        PIC X(3).                                      
0212  ******************************************************************        
"     * FDCFT                                                                   
"     ******************************************************************        
0212   01  FDCFT-ENR         PIC X(80).                                         
      ******************************************************************        
      * TOUS FICHIERS IO.                                                       
      ******************************************************************        
       01  FGK16I-ENR   PIC X(200).                                             
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FGK15C VALUE 'F'.                                             
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FGK15T VALUE 'F'.                                             
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FGK15P VALUE 'F'.                                             
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FGK15F VALUE 'F'.                                             
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FGK15V VALUE 'F'.                                             
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FGK15A VALUE 'F'.                                             
       01  CPT-FGK15C  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK15T  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK15P  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK15F  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK15V  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK15A  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  FGK16O-ENR   PIC X(200).                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  FGK16O-LENGTH PIC 9(4) COMP.                                         
      *--                                                                       
       01  FGK16O-LENGTH PIC 9(4) COMP-5.                                       
      *}                                                                        
       01  CPT-FGK16C  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK16T  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK16P  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK16F  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK16V  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FGK16A  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-MOV-CHAMP PIC 9(4) COMP.                                           
      *--                                                                       
       01  I-MOV-CHAMP PIC 9(4) COMP-5.                                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-DEB-CHAMP PIC 9(4) COMP.                                           
      *--                                                                       
       01  I-DEB-CHAMP PIC 9(4) COMP-5.                                         
      *}                                                                        
      ******************************************************************        
      * ABEND ET BETDATC                                                        
      ******************************************************************        
           COPY ABENDCOP.                                                       
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *01  ABEND   PIC X(8) VALUE 'ABEND  '.                                    
      *--                                                                       
       01  MW-ABEND   PIC X(8) VALUE 'ABEND  '.                                 
      *}                                                                        
      ******************************************************************        
      * PROC: D�BUT, MILIEU, FIN.                                               
      ******************************************************************        
       PROCEDURE DIVISION.                                                      
           PERFORM  DEBUT-BGK160.                                               
           PERFORM  TRAIT-BGK160.                                               
           PERFORM  FIN-BGK160.                                                 
      ******************************************************************        
      * OUVERTURE DE FICHIERS, PARAM�TRES: FDATE.                               
      ******************************************************************        
       DEBUT-BGK160 SECTION.                                                    
           DISPLAY 'BGK160: FICHIERS POUR KRISP'                                
           OPEN INPUT FMOIS FPARAM FGK160                                       
                             FGK15C FGK15T FGK15P FGK15F FGK15V FGK15A          
               OUTPUT FGK161                                                    
                      FGK16E FGK16C FGK16T FGK16P FGK16F FGK16V FGK16A          
0212                  FDCFT.                                                    
      ******************************************************************        
      * FPARAM: DEVISE                                                          
      ******************************************************************        
           READ FPARAM INTO FPARAM-ENR AT END                           01090024
                MOVE 'FICHIER FPARAM VIDE' TO ABEND-MESS                01100024
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           CLOSE FPARAM.                                                01130024
           DISPLAY 'BGK160: CODE DEVISE LU SUR FPARAM: '                        
                    FPARAM-CDEVISE ' ' FPARAM-LDEVISE.                          
      ******************************************************************        
      * FMOIS : DATE DE FIN DE MOIS                                             
      ******************************************************************        
           READ FMOIS  INTO ENR-FMOIS AT END                            01090024
                MOVE 'FICHIER FMOIS VIDE' TO ABEND-MESS                 01100024
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           CLOSE FMOIS.                                                 01130024
           EVALUATE W-MOIS-MM                                                   
               WHEN  '01'                                                       
                           MOVE 'JAN'        TO  W-LIBELLE-MOIS                 
               WHEN  '02'                                                       
                           MOVE 'FEB'        TO  W-LIBELLE-MOIS                 
               WHEN  '03'                                                       
                           MOVE 'MAR'        TO  W-LIBELLE-MOIS                 
               WHEN  '04'                                                       
                           MOVE 'APR'        TO  W-LIBELLE-MOIS                 
               WHEN  '05'                                                       
                           MOVE 'MAY'        TO  W-LIBELLE-MOIS                 
               WHEN  '06'                                                       
                           MOVE 'JUN'        TO  W-LIBELLE-MOIS                 
               WHEN  '07'                                                       
                           MOVE 'JUL'        TO  W-LIBELLE-MOIS                 
               WHEN  '08'                                                       
                           MOVE 'AUG'        TO  W-LIBELLE-MOIS                 
               WHEN  '09'                                                       
                           MOVE 'SEP'        TO  W-LIBELLE-MOIS                 
               WHEN  '10'                                                       
                           MOVE 'OCT'        TO  W-LIBELLE-MOIS                 
               WHEN  '11'                                                       
                           MOVE 'NOV'        TO  W-LIBELLE-MOIS                 
               WHEN  '12'                                                       
                           MOVE 'DEC'        TO  W-LIBELLE-MOIS                 
               WHEN  OTHER                                                      
                    STRING 'FICHIER FMOIS MMSSAA INCOHERENT '                   
                        W-MOIS DELIMITED BY SIZE INTO ABEND-MESS                
                     PERFORM ABEND-PROGRAMME                                    
           END-EVALUATE.                                                        
           STRING                                                               
                  W-LIBELLE-MOIS '-' W-MOIS-SS  W-MOIS-AA                       
                   DELIMITED BY SIZE INTO W-MOISANNEE.                          
           DISPLAY 'BGK160: LIBELLE MOIS LU SUR FMOIS: '                        
                    W-LIBELLE-MOIS.                                             
0212  ******************************************************************        
"     * Cr�ation de la sysin pour CFT                                           
"     ******************************************************************        
"     *    MOVE 'SEND PART=XFBPRO,' TO FDCFT-ENR                                
"     *    WRITE ENR-FDCFT FROM FDCFT-ENR                                       
"     *    MOVE SPACES TO FDCFT-ENR                                             
"     *    MOVE '  IDF=FKRISPK,' TO FDCFT-ENR                                   
"     *    WRITE ENR-FDCFT FROM FDCFT-ENR                                       
"     *    MOVE SPACES TO FDCFT-ENR                                             
"     *    MOVE '  FNAME=PXX0.F99.FKRISPK,' TO FDCFT-ENR                        
"     *    WRITE ENR-FDCFT FROM FDCFT-ENR                                       
"     *    MOVE SPACES TO FDCFT-ENR                                             
"     *    STRING '  NFNAME=MN_004_'                                            
"     *           W-MOIS                                                        
"     *           '.ZIP'                                                        
"     *    DELIMITED BY SIZE INTO FDCFT-ENR                                     
0212  *    WRITE ENR-FDCFT FROM FDCFT-ENR                                       
0713       MOVE '10.135.2.114'     TO FDCFT-ENR                                 
"          WRITE ENR-FDCFT FROM FDCFT-ENR                                       
"          MOVE SPACES TO FDCFT-ENR                                             
"          MOVE 'IPO1GTW'          TO FDCFT-ENR                                 
"          WRITE ENR-FDCFT FROM FDCFT-ENR                                       
"          MOVE SPACES TO FDCFT-ENR                                             
"          MOVE 'IPO1GTW'          TO FDCFT-ENR                                 
"          WRITE ENR-FDCFT FROM FDCFT-ENR                                       
"          MOVE SPACES TO FDCFT-ENR                                             
"          MOVE 'LOCSITE SBD=TCPIP1.XLATE2.TCPXLBIN' TO FDCFT-ENR               
"          WRITE ENR-FDCFT FROM FDCFT-ENR                                       
"          MOVE SPACES TO FDCFT-ENR                                             
"          MOVE 'BIN'              TO FDCFT-ENR                                 
"          WRITE ENR-FDCFT FROM FDCFT-ENR                                       
"          MOVE SPACES TO FDCFT-ENR                                             
"          STRING 'PUT ''PXX0.F99.FKRISPK'''                                    
"                 ' FKRISPK_MN_004_'                                            
"                 W-MOIS                                                        
"                 '.ZIP'                                                        
"          DELIMITED BY SIZE INTO FDCFT-ENR                                     
"          WRITE ENR-FDCFT FROM FDCFT-ENR                                       
"          MOVE SPACES TO FDCFT-ENR                                             
"          MOVE 'QUIT'         TO FDCFT-ENR                                     
0713       WRITE ENR-FDCFT FROM FDCFT-ENR                                       
      ******************************************************************        
      * NUM�RO DE VERSION.                                                      
      ******************************************************************        
           READ FGK160 INTO FGK160-ENR AT END                           01090024
               MOVE W-MOISANNEE  TO FGK160-MOISANNEE                            
               MOVE ZERO      TO FGK160-VERSION                                 
           END-READ.                                                            
           IF W-MOISANNEE  = FGK160-MOISANNEE                                   
              ADD 1 TO FGK160-VERSION                                           
           ELSE                                                                 
              MOVE 1 TO FGK160-VERSION                                          
              MOVE W-MOISANNEE   TO FGK160-MOISANNEE                            
           END-IF.                                                              
           WRITE ENR-FGK161 FROM FGK160-ENR.                            01090024
           CLOSE FGK160 FGK161.                                                 
      ******************************************************************        
      * PROGRAMME:                                                              
      * - G�N�RATION DU FICHIER ENT�TE                                          
      * - RECOPIE DE TOUS LES FICHIERS                                          
      ******************************************************************        
       TRAIT-BGK160 SECTION.                                                    
           PERFORM ECRITURE-FGK16E.                                             
           PERFORM LECTURE-FGK15C.                                              
           PERFORM UNTIL FIN-FGK15C                                             
              PERFORM ECRITURE-FGK16C                                           
              PERFORM LECTURE-FGK15C                                            
           END-PERFORM.                                                         
           PERFORM LECTURE-FGK15T.                                              
           PERFORM UNTIL FIN-FGK15T                                             
              PERFORM ECRITURE-FGK16T                                           
              PERFORM LECTURE-FGK15T                                            
           END-PERFORM.                                                         
           PERFORM LECTURE-FGK15P.                                              
           PERFORM UNTIL FIN-FGK15P                                             
              PERFORM ECRITURE-FGK16P                                           
              PERFORM LECTURE-FGK15P                                            
           END-PERFORM.                                                         
           PERFORM LECTURE-FGK15F.                                              
           PERFORM UNTIL FIN-FGK15F                                             
              PERFORM ECRITURE-FGK16F                                           
              PERFORM LECTURE-FGK15F                                            
           END-PERFORM.                                                         
           PERFORM LECTURE-FGK15V.                                              
           PERFORM UNTIL FIN-FGK15V                                             
              PERFORM ECRITURE-FGK16V                                           
              PERFORM LECTURE-FGK15V                                            
           END-PERFORM.                                                         
           PERFORM LECTURE-FGK15A.                                              
           PERFORM UNTIL FIN-FGK15A                                             
              PERFORM ECRITURE-FGK16A                                           
              PERFORM LECTURE-FGK15A                                            
           END-PERFORM.                                                         
      ******************************************************************        
      * ECRITURE SUR FGK16E: ENTETE                                             
      ******************************************************************        
       ECRITURE-FGK16E SECTION.                                                 
           ACCEPT W-DATE FROM DATE.                                             
           ACCEPT W-TIME FROM TIME.                                             
           MOVE SPACES TO FGK16E-DATETIME.                                      
           STRING W-DATE(5:2) '/' W-DATE(3:2) '/20' W-DATE(1:2)                 
                  ' ' W-TIME(1:2) ':' W-TIME(3:2) ':' W-TIME(5:2)               
                 DELIMITED BY SIZE INTO FGK16E-DATETIME.                        
           STRING                                                               
                  W-LIBELLE-MOIS '-' W-MOIS-SS  W-MOIS-AA                       
                   DELIMITED BY SIZE INTO FGK16E-DATE.                          
           MOVE FPARAM-CDEVISE TO FGK16E-CDEVISE.                               
           MOVE FPARAM-LDEVISE TO FGK16E-LDEVISE.                               
           MOVE FGK16E-ENR     TO FGK16I-ENR.                                   
           PERFORM CONDENSATION-FGK16O.                                         
           WRITE ENR-FGK16E FROM FGK16O-ENR.                                    
           ADD 1 TO CPT-FGK16E.                                                 
      ******************************************************************        
      * LECTURE FGK15C: CONTR�LE.                                               
      ******************************************************************        
       LECTURE-FGK15C SECTION.                                                  
           READ FGK15C INTO FGK16I-ENR                                          
             AT END                                                             
                SET FIN-FGK15C TO TRUE                                          
             NOT AT END                                                         
                ADD 1 TO CPT-FGK15C                                             
           END-READ.                                                            
      ******************************************************************        
      * LECTURE FGK15T: TYPOLOGIE.                                              
      ******************************************************************        
       LECTURE-FGK15T SECTION.                                                  
           READ FGK15T INTO FGK16I-ENR                                          
             AT END                                                             
                SET FIN-FGK15T TO TRUE                                          
             NOT AT END                                                         
                ADD 1 TO CPT-FGK15T                                             
           END-READ.                                                            
      ******************************************************************        
      * LECTURE FGK15P: PRODUITS.                                               
      ******************************************************************        
       LECTURE-FGK15P SECTION.                                                  
           READ FGK15P INTO FGK16I-ENR                                          
             AT END                                                             
                SET FIN-FGK15P TO TRUE                                          
             NOT AT END                                                         
                ADD 1 TO CPT-FGK15P                                             
           END-READ.                                                            
      ******************************************************************        
      * LECTURE FGK15F: FOURNISSEURS.                                           
      ******************************************************************        
       LECTURE-FGK15F SECTION.                                                  
           READ FGK15F INTO FGK16I-ENR                                          
             AT END                                                             
                SET FIN-FGK15F TO TRUE                                          
             NOT AT END                                                         
                ADD 1 TO CPT-FGK15F                                             
           END-READ.                                                            
      ******************************************************************        
      * LECTURE FGK15V: VENTES.                                                 
      ******************************************************************        
       LECTURE-FGK15V SECTION.                                                  
           READ FGK15V INTO FGK16I-ENR                                          
             AT END                                                             
                SET FIN-FGK15V TO TRUE                                          
             NOT AT END                                                         
                ADD 1 TO CPT-FGK15V                                             
           END-READ.                                                            
      ******************************************************************        
      * LECTURE FGK15A: ACHATS.                                                 
      ******************************************************************        
       LECTURE-FGK15A SECTION.                                                  
           READ FGK15A INTO FGK16I-ENR                                          
             AT END                                                             
                SET FIN-FGK15A TO TRUE                                          
             NOT AT END                                                         
                ADD 1 TO CPT-FGK15A                                             
           END-READ.                                                            
      ******************************************************************        
      * ECRITURE SUR FGK16C: CONTR�LE                                           
      ******************************************************************        
       ECRITURE-FGK16C SECTION.                                                 
           PERFORM VARYING I-DEB-CHAMP FROM 1 BY 1                              
             UNTIL I-DEB-CHAMP > 50                                             
               IF FGK16I-ENR(I-DEB-CHAMP:7) = '_01.txt'                         
                  ADD 1 TO I-DEB-CHAMP                                          
                  MOVE FGK160-VERSION TO FGK16I-ENR(I-DEB-CHAMP:2)              
               END-IF                                                           
           END-PERFORM.                                                         
           PERFORM CONDENSATION-FGK16O.                                         
           WRITE ENR-FGK16C FROM FGK16O-ENR.                                    
           ADD 1 TO CPT-FGK16C.                                                 
      ******************************************************************        
      * ECRITURE SUR FGK16T: TYPOLOGIE                                          
      ******************************************************************        
       ECRITURE-FGK16T SECTION.                                                 
           PERFORM CONDENSATION-FGK16O.                                         
           WRITE ENR-FGK16T FROM FGK16O-ENR.                                    
           ADD 1 TO CPT-FGK16T.                                                 
      ******************************************************************        
      * ECRITURE SUR FGK16P: PRODUITS                                           
      ******************************************************************        
       ECRITURE-FGK16P SECTION.                                                 
           PERFORM CONDENSATION-FGK16O.                                         
           WRITE ENR-FGK16P FROM FGK16O-ENR.                                    
           ADD 1 TO CPT-FGK16P.                                                 
      ******************************************************************        
      * ECRITURE SUR FGK16F: FOURNISSEURS                                       
      ******************************************************************        
       ECRITURE-FGK16F SECTION.                                                 
           PERFORM CONDENSATION-FGK16O.                                         
           WRITE ENR-FGK16F FROM FGK16O-ENR.                                    
           ADD 1 TO CPT-FGK16F.                                                 
      ******************************************************************        
      * ECRITURE SUR FGK16V: VENTES                                             
      ******************************************************************        
       ECRITURE-FGK16V SECTION.                                                 
           PERFORM CONDENSATION-FGK16O.                                         
           WRITE ENR-FGK16V FROM FGK16O-ENR.                                    
           ADD 1 TO CPT-FGK16V.                                                 
      ******************************************************************        
      * ECRITURE SUR FGK16A: ACHATS                                             
      ******************************************************************        
       ECRITURE-FGK16A SECTION.                                                 
           PERFORM CONDENSATION-FGK16O.                                         
           WRITE ENR-FGK16A FROM FGK16O-ENR.                                    
           ADD 1 TO CPT-FGK16A.                                                 
      ******************************************************************        
      * CONDENSATION DE FGK16O-ENR (A PARTIR DE FGK16I-ENR).                    
      ******************************************************************        
       CONDENSATION-FGK16O SECTION.                                             
           MOVE FGK16I-ENR TO FGK16O-ENR.                                       
           MOVE 1 TO I-DEB-CHAMP.                                               
           PERFORM UNTIL FGK16O-ENR(I-DEB-CHAMP:) = ' '                         
              IF FGK16O-ENR(I-DEB-CHAMP:1) = ','                                
                 ADD 1 TO I-DEB-CHAMP                                           
              ELSE                                                              
                 IF FGK16O-ENR(I-DEB-CHAMP:1) = '"'                             
                    PERFORM CONDENSATION-FGK16O-ALPHA                           
                 ELSE                                                           
                    PERFORM CONDENSATION-FGK16O-NUM                             
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
           COMPUTE FGK16O-LENGTH = I-DEB-CHAMP - 1.                             
       CONDENSATION-FGK16O-ALPHA SECTION.                                       
     *** ON VIRE LES ESPACES � GAUCHE...                                        
           ADD 1 TO I-DEB-CHAMP.                                                
           PERFORM UNTIL FGK16O-ENR(I-DEB-CHAMP:1) > ' '                        
              COMPUTE I-MOV-CHAMP = I-DEB-CHAMP + 1                             
              MOVE FGK16O-ENR(I-MOV-CHAMP:) TO FGK16I-ENR(I-DEB-CHAMP:)         
              MOVE FGK16I-ENR TO FGK16O-ENR                                     
           END-PERFORM.                                                         
     *** ...ON CHERCHE LA FIN DU CHAMP...                                       
           PERFORM VARYING I-DEB-CHAMP FROM I-DEB-CHAMP BY 1                    
             UNTIL FGK16O-ENR(I-DEB-CHAMP:1) = '"'                              
           END-PERFORM.                                                         
     *** ...ON CHERCHE LE DERNIER CARACT�RE � DROITE...                         
           SUBTRACT 1 FROM I-DEB-CHAMP.                                         
           PERFORM VARYING I-DEB-CHAMP FROM I-DEB-CHAMP BY -1                   
             UNTIL FGK16O-ENR(I-DEB-CHAMP:1) > ' '                              
           END-PERFORM.                                                         
           ADD 1 TO I-DEB-CHAMP.                                                
     *** ...ON VIRE LES ESPACES � DROITE...                                     
           PERFORM UNTIL FGK16O-ENR(I-DEB-CHAMP:1) > ' '                        
              COMPUTE I-MOV-CHAMP = I-DEB-CHAMP + 1                             
              MOVE FGK16O-ENR(I-MOV-CHAMP:) TO FGK16I-ENR(I-DEB-CHAMP:)         
              MOVE FGK16I-ENR TO FGK16O-ENR                                     
           END-PERFORM.                                                         
     *** ...ET ON SE POSITIONNE SUR LE PROCHAIN CHAMP                           
           ADD 1 TO I-DEB-CHAMP.                                                
       CONDENSATION-FGK16O-NUM SECTION.                                         
     *** ON VIRE LES ESPACES � GAUCHE...                                        
           PERFORM UNTIL FGK16O-ENR(I-DEB-CHAMP:1) > ' '                        
              COMPUTE I-MOV-CHAMP = I-DEB-CHAMP + 1                             
              MOVE FGK16O-ENR(I-MOV-CHAMP:) TO FGK16I-ENR(I-DEB-CHAMP:)         
              MOVE FGK16I-ENR TO FGK16O-ENR                                     
           END-PERFORM.                                                         
     *** ...ON CHERCHE LA FIN DU CHAMP...                                       
           PERFORM VARYING I-DEB-CHAMP FROM I-DEB-CHAMP BY 1                    
             UNTIL FGK16O-ENR(I-DEB-CHAMP:1) = ','                              
                OR FGK16O-ENR(I-DEB-CHAMP:) = ' '                               
           END-PERFORM.                                                         
     *** ...ON CHERCHE LE DERNIER CARACT�RE � DROITE...                         
           SUBTRACT 1 FROM I-DEB-CHAMP.                                         
           PERFORM VARYING I-DEB-CHAMP FROM I-DEB-CHAMP BY -1                   
             UNTIL FGK16O-ENR(I-DEB-CHAMP:1) > ' '                              
           END-PERFORM.                                                         
     *** ...ET ON VIRE LES ESPACES � DROITE...                                  
           ADD 1 TO I-DEB-CHAMP.                                                
           PERFORM UNTIL FGK16O-ENR(I-DEB-CHAMP:1) > ' '                        
                      OR FGK16O-ENR(I-DEB-CHAMP:) = ' '                         
              COMPUTE I-MOV-CHAMP = I-DEB-CHAMP + 1                             
              MOVE FGK16O-ENR(I-MOV-CHAMP:) TO FGK16I-ENR(I-DEB-CHAMP:)         
              MOVE FGK16I-ENR TO FGK16O-ENR                                     
           END-PERFORM.                                                         
     *** (ON EST POSITIONN� SUR LA VIRGULE DE FIN DE CHAMP)                     
      ******************************************************************        
      * C'EST LA FIN DES HARICOTS.                                              
      ******************************************************************        
       FIN-BGK160 SECTION.                                                      
           CLOSE        FGK15C FGK15T FGK15P FGK15F FGK15V FGK15A               
                 FGK16E FGK16C FGK16T FGK16P FGK16F FGK16V FGK16A.              
           DISPLAY 'BGK160: FIN NORMALE DE PROGRAMME'.                          
           MOVE CPT-FGK15C TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS LUS SUR FGK15C: ' PIC-EDITZ9.            
           MOVE CPT-FGK15T TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS LUS SUR FGK15T: ' PIC-EDITZ9.            
           MOVE CPT-FGK15T TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS LUS SUR FGK15P: ' PIC-EDITZ9.            
           MOVE CPT-FGK15F TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS LUS SUR FGK15F: ' PIC-EDITZ9.            
           MOVE CPT-FGK15V TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS LUS SUR FGK15V: ' PIC-EDITZ9.            
           MOVE CPT-FGK15A TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS LUS SUR FGK15A: ' PIC-EDITZ9.            
           MOVE CPT-FGK16E TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS �CRITS SUR FGK16E: ' PIC-EDITZ9.         
           MOVE CPT-FGK16C TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS �CRITS SUR FGK16C: ' PIC-EDITZ9.         
           MOVE CPT-FGK16T TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS �CRITS SUR FGK16T: ' PIC-EDITZ9.         
           MOVE CPT-FGK16P TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS �CRITS SUR FGK16P: ' PIC-EDITZ9.         
           MOVE CPT-FGK16F TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS �CRITS SUR FGK16F: ' PIC-EDITZ9.         
           MOVE CPT-FGK16V TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS �CRITS SUR FGK16V: ' PIC-EDITZ9.         
           MOVE CPT-FGK16A TO PIC-EDITZ9.                                       
           DISPLAY '        NB. ENREGS �CRITS SUR FGK16A: ' PIC-EDITZ9.         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      ******************************************************************        
      * BADABOUM                                                                
      ******************************************************************        
       ABEND-PROGRAMME SECTION.                                                 
           DISPLAY ABEND-MESS                                                   
           MOVE 'BGK160' TO ABEND-PROG.                                         
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL ABEND USING ABEND-PROG ABEND-MESS.                              
      *--                                                                       
           CALL MW-ABEND USING ABEND-PROG ABEND-MESS.                           
      *}                                                                        
