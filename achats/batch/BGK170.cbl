      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BGK170.                                                     
       AUTHOR. JG.                                                              
      ******************************************************************        
      *                                                                         
      * Mise dans l'archive - d�fini par FKRISP des sept fichiers KRISP         
      *                                                                         
      *=================================================================        
      * dsa005 ap - 27/01/03 : changement de version de pkzip                   
      * --> tous les param�tres invariants de PKZIP sont d�sormais              
      *     d�finis dans FARCH                                                  
      ******************************************************************        
      * DSA053 changement du nom des fichiers KRISP                             
      *     le 22/01/2004                                                       
      ******************************************************************        
      * dsa015 modification de pkzip : on ne fait plus un call du pgm,          
      *                                on construit une sysin                   
      *     le 21/05/2013                                                       
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FARCH   ASSIGN TO  FARCH .                                  
      *--                                                                       
            SELECT  FARCH   ASSIGN TO  FARCH                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FDDDSN  ASSIGN TO  FDDDSN.                                  
      *--                                                                       
            SELECT  FDDDSN  ASSIGN TO  FDDDSN                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK16C  ASSIGN TO  FGK16C.                                  
      *--                                                                       
            SELECT  FGK16C  ASSIGN TO  FGK16C                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FPKZIP  ASSIGN TO  FPKZIP.                                  
      *--                                                                       
            SELECT  FPKZIP  ASSIGN TO  FPKZIP                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FARCH  RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER    PIC X(80).                                              
       FD  FDDDSN RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  MW-FILLER    PIC X(80).                                              
       FD  FGK16C RECORDING V BLOCK 0 RECORDS LABEL RECORD STANDARD.            
      *MW DAR-2                                                                 
       01  MW-FILLER    PIC X(196).                                             
0513   FD  FPKZIP RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
0513   01  FPKZIP-ENR PIC X(80).                                                
      ******************************************************************        
      * WSS                                                                     
      ******************************************************************        
       WORKING-STORAGE SECTION.                                                 
      ******************************************************************        
      * FARCH nom de l'Archive ZIP � cr�er                                      
      ******************************************************************        
       01  FARCH-ENR          PIC X(196).                                       
ap    *                                                                         
ap     01  FILLER             PIC X VALUE ' '.                                  
ap         88 FIN-FARCH             VALUE 'F'.                                  
      ******************************************************************        
      * FDDDSN correspondance DDName DSName                                     
      ******************************************************************        
       01  FDDDSN-ENR.                                                          
           02  FDDDSN-DD      PIC X(06).                                        
           02  FILLER         PIC X(01).                                        
           02  FDDDSN-DSN     PIC X(50).                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-FDDDSN           PIC S9(4) BINARY.                                 
      *--                                                                       
       01  I-FDDDSN           PIC S9(4) COMP-5.                                 
      *}                                                                        
      ******************************************************************        
      * FGK16C                                                                  
      ******************************************************************        
       01  FGK16C-ENR         PIC X(196).                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-FGK16C           PIC S9(4) BINARY.                                 
      *--                                                                       
       01  I-FGK16C           PIC S9(4) COMP-5.                                 
      *}                                                                        
      ******************************************************************        
      * Tableau pour garder FDDDSN et FGK16C                                    
      ******************************************************************        
       01  TAB-FICHIERS.                                                        
           02  TAB-FDDDSN-DD  PIC X(06) OCCURS 7.                               
           02  TAB-FDDDSN-DSN PIC X(50) OCCURS 7.                               
           02  TAB-FGK16C-PC  PIC X(41) OCCURS 7.                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-TAB              PIC S9(4) BINARY.                                 
      *--                                                                       
       01  I-TAB              PIC S9(4) COMP-5.                                 
      *}                                                                        
       01  W-DD               PIC X(06).                                        
      ******************************************************************        
      * Param�tres pour PKZIP.                                                  
      ******************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
ap    *01  FARCH-LONG              PIC S9(4)  COMP.                             
      *--                                                                       
       01  FARCH-LONG              PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
ap    *01  FARCH-TOTAL             PIC S9(4)  COMP  VALUE 1.                    
      *--                                                                       
       01  FARCH-TOTAL             PIC S9(4) COMP-5  VALUE 1.                   
      *}                                                                        
       01  PKZIP              PIC X(8) VALUE 'PKZIP'.                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
ap    *01  PKZIP-PARM-LEN-01       PIC S9(3)  COMP  VALUE +320.                 
      *--                                                                       
       01  PKZIP-PARM-LEN-01       PIC S9(3) COMP-5  VALUE +320.                
      *}                                                                        
ap     01  PKZIP-PARM.                                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
ap    *    05  PKZIP-PARM-LEN      PIC S9(3)  COMP  VALUE +451.                 
      *--                                                                       
           05  PKZIP-PARM-LEN      PIC S9(3) COMP-5  VALUE +451.                
      *}                                                                        
ap         05  PKZIP-PARM-DATA-01  PIC X(320).                                  
ap         05  PKZIP-PARM-DATA-02  PIC X(091).                                  
ap         05  PKZIP-PARM-DATA-03  PIC X(040).                                  
      ******************************************************************        
      * ABEND                                                                   
      ******************************************************************        
           COPY ABENDCOP.                                                       
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *01  ABEND   PIC X(8) VALUE 'ABEND  '.                                    
      *--                                                                       
       01  MW-ABEND   PIC X(8) VALUE 'ABEND  '.                                 
      *}                                                                        
      ******************************************************************        
      * PROC                                                                    
      ******************************************************************        
       PROCEDURE DIVISION.                                                      
           DISPLAY 'BGK170: Cr�ation de l''archive pour KRISP'.                 
1305       OPEN OUTPUT FPKZIP                                                   
      ******************************************************************        
      * Chargement du param�tre FARCH: Archive ZIP                              
      ******************************************************************        
ap    ***  OPEN INPUT FARCH.                                                    
ap    ***  READ FARCH INTO FARCH-ENR                                            
ap    ***     AT END                                                            
ap    ***        MOVE 'Fichier FARCH vide' TO ABEND-MESS                00024   
ap    ***        PERFORM ABEND-PROGRAMME                                        
ap    ***     NOT AT END                                                        
ap    ***        DISPLAY '        Archive: ' FARCH-ENR                          
ap    ***        MOVE SPACES TO PKZIP-PARM-DATA-01                              
ap    ***        STRING '-ECHO -NOSYSIN -ARCHIVE(' DELIMITED BY SIZE            
ap    ***               FARCH-ENR                  DELIMITED BY SPACE           
ap    ***               ')'                        DELIMITED BY SIZE            
ap    ***          INTO PKZIP-PARM-DATA-01                                      
ap    ***  END-READ.                                                            
ap    ***  CLOSE FARCH.                                                         
ap                                                                              
ap         DISPLAY 'Param�tres de l''archive : '                                
ap         MOVE SPACES TO PKZIP-PARM-DATA-01                                    
ap         OPEN INPUT FARCH.                                                    
ap         PERFORM UNTIL FIN-FARCH                                              
ap            READ FARCH INTO FARCH-ENR                                         
ap                AT END                                                        
ap                   SET FIN-FARCH  TO TRUE                                     
ap            NOT AT END                                                        
ap                   MOVE 0 TO FARCH-LONG                                       
ap                   INSPECT FARCH-ENR TALLYING FARCH-LONG                      
ap                                     FOR CHARACTERS BEFORE ','                
ap                   DISPLAY FARCH-ENR(1:FARCH-LONG)                            
ap                   MOVE FARCH-ENR TO                                          
ap                        PKZIP-PARM-DATA-01(FARCH-TOTAL:FARCH-LONG)            
0513                 WRITE FPKZIP-ENR FROM FARCH-ENR(1:FARCH-LONG)              
ap                   COMPUTE FARCH-TOTAL =                                      
ap                           FARCH-TOTAL + 1 + FARCH-LONG                       
ap                   IF  FARCH-TOTAL > PKZIP-PARM-LEN-01                        
ap                      MOVE 'Trop de parametres' TO ABEND-MESS                 
ap                      PERFORM ABEND-PROGRAMME                                 
ap                   END-IF                                                     
ap            END-READ                                                          
ap         END-PERFORM                                                          
ap         CLOSE FARCH                                                          
ap         DISPLAY PKZIP-PARM-DATA-01                                           
ap         IF PKZIP-PARM-DATA-01 = SPACES                                       
ap            MOVE 'Aucun parametres' TO ABEND-MESS                             
ap            PERFORM ABEND-PROGRAMME                                           
ap         END-IF                                                               
ap         DISPLAY ' '.                                                         
      ******************************************************************        
      * Chargement des param�tres FDDDSN: DD et DSN                             
      ******************************************************************        
           OPEN INPUT FDDDSN.                                                   
           DISPLAY '        DDname DSName (de FDDDSN)'.                         
           DISPLAY '        ------ -----------------------------------'.        
           PERFORM VARYING I-FDDDSN FROM 1 BY 1 UNTIL I-FDDDSN > 7              
              READ FDDDSN INTO FDDDSN-ENR                                       
                 AT END                                                         
                    MOVE 'Fichier FDDDSN incomplet' TO ABEND-MESS       01100024
                    PERFORM ABEND-PROGRAMME                                     
                 NOT AT END                                                     
                    DISPLAY '        ' FDDDSN-DD ' ' FDDDSN-DSN                 
                    MOVE FDDDSN-DD  TO TAB-FDDDSN-DD(I-FDDDSN)                  
                    MOVE FDDDSN-DSN TO TAB-FDDDSN-DSN(I-FDDDSN)                 
                 END-READ                                                       
           END-PERFORM.                                                         
           CLOSE FDDDSN.                                                        
      ******************************************************************        
      * Chargement des param�tres FGK16C: Fichier PC                            
      ******************************************************************        
           OPEN INPUT FGK16C.                                                   
           PERFORM VARYING I-FGK16C FROM 1 BY 1 UNTIL I-FGK16C > 7              
              READ FGK16C INTO FGK16C-ENR                                       
                 AT END                                                         
                    MOVE 'Fichier FGK16C incomplet' TO ABEND-MESS       01100024
                    PERFORM ABEND-PROGRAMME                                     
                 NOT AT END                                                     
                    PERFORM VARYING I-TAB FROM 1 BY 1                           
                      UNTIL (I-TAB > 41) OR (FGK16C-ENR(I-TAB:) = ' ')          
                       IF FGK16C-ENR(I-TAB:1) = ','                             
                          MOVE ' ' TO FGK16C-ENR(I-TAB:)                        
                       END-IF                                                   
                    END-PERFORM                                                 
                    EVALUATE FGK16C-ENR(1:11)                                   
                       WHEN ('MN_ProductH') MOVE 'FGK16T' TO W-DD               
                       WHEN ('MN_Product_') MOVE 'FGK16P' TO W-DD               
                       WHEN ('MN_ExtractV') MOVE 'FGK16C' TO W-DD               
                       WHEN ('MN_ExtractH') MOVE 'FGK16E' TO W-DD               
                       WHEN ('MN_SalesSta') MOVE 'FGK16V' TO W-DD               
                       WHEN ('MN_Purchase') MOVE 'FGK16A' TO W-DD               
                       WHEN ('MN_Supplier') MOVE 'FGK16F' TO W-DD               
                    END-EVALUATE                                                
                    PERFORM VARYING I-FDDDSN FROM 1 BY 1                        
                      UNTIL I-FDDDSN > 7                                        
                       IF W-DD = TAB-FDDDSN-DD(I-FDDDSN)                        
                          MOVE FGK16C-ENR TO TAB-FGK16C-PC(I-FDDDSN)            
                       END-IF                                                   
                    END-PERFORM                                                 
                 END-READ                                                       
           END-PERFORM.                                                         
           CLOSE FGK16C.                                                        
      ******************************************************************        
      * Appel de l'archivage PKZIP                                              
      ******************************************************************        
           PERFORM VARYING I-FDDDSN FROM 1 BY 1 UNTIL I-FDDDSN > 7              
              MOVE SPACES TO PKZIP-PARM-DATA-02                                 
              STRING TAB-FDDDSN-DSN(I-FDDDSN) DELIMITED BY SPACE                
                     ' -n '                   DELIMITED BY SIZE                 
                     TAB-FGK16C-PC(I-FDDDSN)  DELIMITED BY SPACE                
                     INTO PKZIP-PARM-DATA-02                                    
0513          WRITE FPKZIP-ENR FROM PKZIP-PARM-DATA-02                          
0513       END-PERFORM.                                                         
      ******************************************************************        
      **** FIN                                                                  
      ******************************************************************        
0513       CLOSE FPKZIP                                                         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      ******************************************************************        
      * BadaBoum                                                                
      ******************************************************************        
       ABEND-PROGRAMME SECTION.                                                 
           DISPLAY ABEND-MESS                                                   
           MOVE 'BGK170' TO ABEND-PROG.                                         
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL ABEND USING ABEND-PROG ABEND-MESS.                              
      *--                                                                       
           CALL MW-ABEND USING ABEND-PROG ABEND-MESS.                           
      *}                                                                        
