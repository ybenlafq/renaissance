      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. BGK141.                                                      
       AUTHOR. FT.                                                              
      ***************************************************************           
      * F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E *           
      ***************************************************************           
      *                                                                         
      *  PROJET     : KESA                                                      
      *  PROGRAMME  : BGK141                                                    
      *  CREATION   : 07/1999                                                   
      *  FONCTION   :                                                           
      *  - MISE A JOUR DU FICHIER ACHAT EN PROVENANCE DE L'AS400                
      *    AVEC DES DONNEES EN PROVENANCE DU HOST (CODE FAMILLE,                
      *    CODE MARKETING, CODE MARQUE)                                         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *           
      * AJUSTEMENT LONGUEUR CHAMPS + AJOUT <TAB> ENTRE CHAQUE CHAMP   * 00101100
      ***************************************************************** 00101300
       ENVIRONMENT DIVISION.                                                    
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *           
       CONFIGURATION SECTION.                                                   
      *                                                                         
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                                   
      *                                                                         
       INPUT-OUTPUT SECTION.                                                    
      *                                                                         
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK020  ASSIGN TO  FGK020.                                  
      *--                                                                       
            SELECT  FGK020  ASSIGN TO  FGK020                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK140  ASSIGN TO  FGK140.                                  
      *--                                                                       
            SELECT  FGK140  ASSIGN TO  FGK140                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FGK141  ASSIGN TO  FGK141.                                  
      *--                                                                       
            SELECT  FGK141  ASSIGN TO  FGK141                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *           
       DATA DIVISION.                                                           
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *           
       FILE SECTION.                                                            
      *                                                                         
       FD  FGK020 RECORDING F                                                   
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  ENR-FGK020     PIC  X(113).                                          
      *                                                                         
       FD  FGK140 RECORDING F                                                   
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  ENR-FGK140     PIC  X(071).                                          
      *                                                                         
       FD  FGK141 RECORDING F                                                   
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  ENR-FGK141     PIC  X(071).                                          
      *                                                                         
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
      *                                                                         
           COPY SYBWDIV0.                                                       
      *                                                                         
           COPY FGK020.                                                         
           COPY FGK140.                                                         
                                                                        00100000
      *                                                                         
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * DESCRIPTION  DES ZONES D'INTERFACE DB2                                  
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
           COPY SYKWSQ10.                                                       
      *                                                                         
           COPY WORKDATC.                                                       
      *                                                                         
      *01  W-NOMSPROG              PIC X(08)          VALUE 'SPDATDB2'.         
      *                                                                         
      * ZONES DIVERSES                                                          
      *                                                                         
      * COMPTEURS                                                               
      *                                                                         
       01  VALEURS-NUM.                                                         
           05  W-COMPTEURS.                                                     
               10  WCPT-FGK020     PIC 9(06)          VALUE ZEROES.             
               10  WCPT-FGK140     PIC 9(06)          VALUE ZEROES.             
               10  WCPT-FGK141     PIC 9(06)          VALUE ZEROES.             
               10  WCPT-FGKPB      PIC 9(06)          VALUE ZEROES.             
      *                                                                         
       01  VALEURS-CONDITION.                                                   
      *                                                                 00001950
           05  ETAT-FGK020         PIC 9              VALUE 0.          00012040
               88  NON-FIN-FGK020                     VALUE 0.          00012060
               88  FIN-FGK020                         VALUE 1.          00012050
      *                                                                         
           05  ETAT-FGK140         PIC 9              VALUE 0.          00012040
               88  NON-FIN-FGK140                     VALUE 0.          00012060
               88  FIN-FGK140                         VALUE 1.          00012050
      *                                                                         
      ** AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *               C O P Y   D E   C O N T R O L E                 *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
           COPY  SYBWDATH.                                                      
           COPY  SYBWDATE.                                                      
           COPY  ABENDCOP.                                                      
      *                                                                         
      ** AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00005220
      *         ZONES     BUFFER      D'ENTREE   /  SORTIE            * 00005230
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00005240
      *                                                                 00005250
       01  FILLER    PIC X(16)    VALUE   '*** Z-INOUT ****'.           00005260
       01  Z-INOUT   PIC X(4096)  VALUE  SPACES.                        00005270
      *                                                                 00005280
      ** AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00005290
      *       ZONES     DE      GESTION     DES     ERREURS           * 00005300
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00005310
      *                                                                 00005320
           COPY SYBWERRO.                                               00005330
      ***************************************************************** 00005530
       LINKAGE SECTION.                                                 00005520
      ***************************************************************** 00005530
       PROCEDURE DIVISION.                                              00005550
      ***************************************************************** 00005530
       MODULE-BGK131  SECTION.                                          00006120
      *----------------------*                                          00006050
           PERFORM MODULE-ENTREE.                                               
           PERFORM MODULE-TRAITEMENT.                                           
           PERFORM MODULE-SORTIE.                                               
      *----------------------*                                          00006050
       MODULE-ENTREE SECTION.                                           00006120
      *----------------------*                                          00006050
           DISPLAY '*** DEBUT DU PROGRAMME BGK141 ***'                  00006070
                                                                        00006050
           INITIALIZE VALEURS-NUM                                               
                      VALEURS-CONDITION.                                        
      *--- OUVERTURE DES FICHIERS                                               
           OPEN INPUT  FGK140                                                   
           OPEN INPUT  FGK020                                                   
           OPEN OUTPUT FGK141                                           00024690
      *--- LECTURE INITIALE                                                     
           PERFORM READ-FGK140                                                  
           IF FIN-FGK140                                                        
              DISPLAY 'FICHIER FGK140 VIDE '                                    
              PERFORM ABANDON-PROGRAMME                                         
           END-IF.                                                      00006380
                                                                        00024690
      *-------------------------*                                       00006520
       MODULE-TRAITEMENT SECTION.                                       00006530
      *-------------------------*                                       00006520
                                                                        00006050
           PERFORM UNTIL FIN-FGK140                                             
              PERFORM READ-FGK020 UNTIL                                         
                      FIN-FGK020 OR FGK020-NCODIC >= FGK140-NCODIC              
                 IF FGK020-NCODIC = FGK140-NCODIC                               
                    MOVE FGK020-CODE-MARK TO FGK140-CODE-MARK                   
                    MOVE FGK020-CODE-MARQ TO FGK140-CODE-MARQ                   
                    MOVE FGK020-CODE-FAM  TO FGK140-CODE-FAM                    
                    MOVE FGK140-ENR TO ENR-FGK141                               
                    WRITE ENR-FGK141                                            
                    ADD 1 TO WCPT-FGK141                                        
                 ELSE                                                           
                    DISPLAY 'PAS D''ENREG PRODUIT POUR ' FGK140-NCODIC          
                    ADD 1 TO WCPT-FGKPB                                         
                 END-IF                                                         
                 PERFORM READ-FGK140                                            
           END-PERFORM.                                                         
       FIN-MODULE-TRAITEMENT. EXIT.                                             
      *---------------------*                                           00025840
       MODULE-SORTIE SECTION.                                           00025850
      *---------------------*                                           00025840
           DISPLAY '*** BGK141 EXECUTION TERMINEE NORMALEMENT **'       00026480
                                                                        00025840
           CLOSE  FGK020 FGK140 FGK141.                                 00025870
                                                                        00025840
           DISPLAY 'NB ENREGISTREMENTS FGK020 LUS .. : ' WCPT-FGK020.   00025930
           DISPLAY 'NB ENREGISTREMENTS FGK140 LUS .. : ' WCPT-FGK140.   00025930
           DISPLAY 'NB ENREGISTREMENTS FGK141 ECRITS : ' WCPT-FGK141.   00025930
           DISPLAY 'NB ENREGISTREMENTS FGK140 NON T. : ' WCPT-FGKPB.    00025930
           DISPLAY '---------------------------------------------'.     00025930
                                                                        00025840
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        00025840
      *-----                                                                    
       READ-FGK020      SECTION.                                                
           READ FGK020 INTO FGK020-ENR                                          
                       END SET FIN-FGK020      TO TRUE                          
                   NOT END ADD 1 TO WCPT-FGK020                                 
           END-READ.                                                    00006380
                                                                        00024690
       FIN-READ-FGK020. EXIT.                                                   
      *                                                                         
      *-----                                                                    
       READ-FGK140      SECTION.                                                
           READ FGK140 INTO FGK140-ENR                                          
                       END SET FIN-FGK140      TO TRUE                          
                   NOT END ADD 1 TO WCPT-FGK140                                 
           END-READ.                                                    00006380
                                                                        00024690
       FIN-READ-FGK140. EXIT.                                                   
      *                                                                         
      ** AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *     M O D U L E S     D E     C O N T R O L E                 *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       01-COPY-CONTROLE SECTION. CONTINUE. COPY SYBPDATE.                       
      *                                                                         
      ** AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *     M O D U L E     D ' A B A N D O N                         *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       99-COPY SECTION. CONTINUE. COPY SYBCERRO.                                
      * ------------------------ FIN-ANORMALE ---------------------- *  03850005
                                                                        03860005
       ABANDON-PROGRAMME SECTION.                                       03870005
                                                                        03860005
           CLOSE  FGK020 FGK140 FGK141.                                         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    03900005
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-ABANDON-PROG. EXIT.                                          03920005
