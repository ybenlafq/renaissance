      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : MAJ SAISIE DE COMMANDE FOURNISSEUR                     *         
      *            ARTICLES-QUANTITES (TRX:GF16)                      *         
      *****************************************************************         
      *****************************************************************         
       01  TS-GF16-DESCR.                                                       
      *-----------------------------------------------------LONGUEUR TS         
           02 TS-GF16-LONG PIC S9(3) COMP-3  VALUE +080.                        
      *                                                                         
           02 TS-GF16.                                                          
      *-----------------------------------------INFOS ISSU DE GF15---29         
              03 TS-GF16-NCDE              PIC X(07).                           
              03 TS-GF16-NCODIC            PIC X(07).                           
              03 TS-GF16-NSOCIETE          PIC X(03).                           
              03 TS-GF16-NLIEU             PIC X(03).                           
              03 TS-GF16-QCDE-X.                                                
              04 TS-GF16-QCDE              PIC S9(05) COMP-3.                   
              03 TS-GF16-QCDE-MAP-X.                                            
              04 TS-GF16-QCDE-MAP          PIC S9(05) COMP-3.                   
              03 TS-GF16-QREC-X.                                                
              04 TS-GF16-QREC              PIC S9(05) COMP-3.                   
              03 TS-GF16-QSOLDE-X.                                              
              04 TS-GF16-QSOLDE            PIC S9(05) COMP-3.                   
      *-----------------------------------------INFOS ISSU DE GA00---38         
              03 TS-GF16-LREFFOURN         PIC X(20).                           
              03 TS-GF16-CFAM              PIC X(05).                           
              03 TS-GF16-CMARQ             PIC X(05).                           
              03 TS-GF16-QCOLIRECEPT       PIC S9(05) COMP-3.                   
              03 TS-GF16-CAPPRO            PIC X(05).                           
      *-----------------------------------------INFOS DE TRAVAIL-----13         
              03 TS-GF16-CMAJ              PIC  X(01).                          
                 88 CODIC-SANS-MODIF       VALUE ' '.                           
                 88 NOUVEAU-CODIC          VALUE 'C'.                           
                 88 ANCIEN-CODIC           VALUE 'M'.                           
                 88 CODIC-SUPPRIME         VALUE 'X'.                           
              03 TS-GF16-CREASON-FLAG      PIC  X(01).                          
                 88 TS-GF16-CREASON        VALUE 'O'.                           
                 88 TS-GF16-NO-CREASON     VALUE 'N'.                           
                 88 TS-GF16-CREASON-RENS   VALUE 'R'.                           
              03 TS-GF16-CODE-REASON       PIC X(05).                           
              03 TS-GF16-FILLER            PIC X(03).                           
      *       03 TS-GF16-FILLER            PIC X(12).                           
                                                                                
