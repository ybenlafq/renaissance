      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GRCRD ACTIV. BAR. CR�DIT NIV. GROUPE   *        
      *----------------------------------------------------------------*        
       01  RVGRCRD.                                                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  GRCRD-CTABLEG2    PIC X(15).                                     
           05  GRCRD-CTABLEG2-REDEF REDEFINES GRCRD-CTABLEG2.                   
               10  GRCRD-CBAREME         PIC X(07).                             
               10  GRCRD-NSEQ            PIC X(02).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  GRCRD-NSEQ-N         REDEFINES GRCRD-NSEQ                    
                                         PIC 9(02).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  GRCRD-WTABLEG     PIC X(80).                                     
           05  GRCRD-WTABLEG-REDEF  REDEFINES GRCRD-WTABLEG.                    
               10  GRCRD-DEFFET          PIC X(08).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  GRCRD-DEFFET-N       REDEFINES GRCRD-DEFFET                  
                                         PIC 9(08).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGRCRD-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GRCRD-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GRCRD-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GRCRD-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GRCRD-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
