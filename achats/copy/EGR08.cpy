      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR08   EGR08                                              00000020
      ***************************************************************** 00000030
       01   EGR08I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPAGESI      PIC X(4).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPOTL    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCDEPOTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSOCDEPOTF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCDEPOTI    PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNDEPOTI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEPOTL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLDEPOTF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLDEPOTI  PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNCODICL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MSNCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSNCODICF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSNCODICI      PIC X(7).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSBLOCAGEL     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MSBLOCAGEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSBLOCAGEF     PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MSBLOCAGEI     PIC X(3).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDEFFETL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MSDEFFETL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSDEFFETF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSDEFFETI      PIC X(8).                                  00000450
           02 MLIGNEI OCCURS   13 TIMES .                               00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNNCODICL    COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNNCODICL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNNCODICF    PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNNCODICI    PIC X(7).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBLOCAGEL   COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNBLOCAGEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNBLOCAGEF   PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNBLOCAGEI   PIC X(3).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNZBLOCAGEL  COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNZBLOCAGEL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNZBLOCAGEF  PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNZBLOCAGEI  PIC X(3).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIBELLEL   COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNLIBELLEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNLIBELLEF   PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNLIBELLEI   PIC X(20).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNQPILEL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MNQPILEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNQPILEF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNQPILEI     PIC X(9).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDEFFETL    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNDEFFETL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNDEFFETF    PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNDEFFETI    PIC X(8).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBLOCAGEIL  COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MNBLOCAGEIL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNBLOCAGEIF  PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MNBLOCAGEII  PIC X(3).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNZBLOCAGEIL      COMP PIC S9(4).                       00000750
      *--                                                                       
             03 MNZBLOCAGEIL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 MNZBLOCAGEIF      PIC X.                                00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNZBLOCAGEII      PIC X(3).                             00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLIBERRI  PIC X(73).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MNETNAMI  PIC X(8).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MSCREENI  PIC X(4).                                       00000980
      ***************************************************************** 00000990
      * SDF: EGR08   EGR08                                              00001000
      ***************************************************************** 00001010
       01   EGR08O REDEFINES EGR08I.                                    00001020
           02 FILLER    PIC X(12).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTIMJOUA  PIC X.                                          00001120
           02 MTIMJOUC  PIC X.                                          00001130
           02 MTIMJOUP  PIC X.                                          00001140
           02 MTIMJOUH  PIC X.                                          00001150
           02 MTIMJOUV  PIC X.                                          00001160
           02 MTIMJOUO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MPAGEA    PIC X.                                          00001190
           02 MPAGEC    PIC X.                                          00001200
           02 MPAGEP    PIC X.                                          00001210
           02 MPAGEH    PIC X.                                          00001220
           02 MPAGEV    PIC X.                                          00001230
           02 MPAGEO    PIC X(4).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MNBPAGESA      PIC X.                                     00001260
           02 MNBPAGESC PIC X.                                          00001270
           02 MNBPAGESP PIC X.                                          00001280
           02 MNBPAGESH PIC X.                                          00001290
           02 MNBPAGESV PIC X.                                          00001300
           02 MNBPAGESO      PIC X(4).                                  00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MNSOCDEPOTA    PIC X.                                     00001330
           02 MNSOCDEPOTC    PIC X.                                     00001340
           02 MNSOCDEPOTP    PIC X.                                     00001350
           02 MNSOCDEPOTH    PIC X.                                     00001360
           02 MNSOCDEPOTV    PIC X.                                     00001370
           02 MNSOCDEPOTO    PIC X(3).                                  00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MNDEPOTA  PIC X.                                          00001400
           02 MNDEPOTC  PIC X.                                          00001410
           02 MNDEPOTP  PIC X.                                          00001420
           02 MNDEPOTH  PIC X.                                          00001430
           02 MNDEPOTV  PIC X.                                          00001440
           02 MNDEPOTO  PIC X(3).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MLDEPOTA  PIC X.                                          00001470
           02 MLDEPOTC  PIC X.                                          00001480
           02 MLDEPOTP  PIC X.                                          00001490
           02 MLDEPOTH  PIC X.                                          00001500
           02 MLDEPOTV  PIC X.                                          00001510
           02 MLDEPOTO  PIC X(20).                                      00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MSNCODICA      PIC X.                                     00001540
           02 MSNCODICC PIC X.                                          00001550
           02 MSNCODICP PIC X.                                          00001560
           02 MSNCODICH PIC X.                                          00001570
           02 MSNCODICV PIC X.                                          00001580
           02 MSNCODICO      PIC X(7).                                  00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MSBLOCAGEA     PIC X.                                     00001610
           02 MSBLOCAGEC     PIC X.                                     00001620
           02 MSBLOCAGEP     PIC X.                                     00001630
           02 MSBLOCAGEH     PIC X.                                     00001640
           02 MSBLOCAGEV     PIC X.                                     00001650
           02 MSBLOCAGEO     PIC X(3).                                  00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MSDEFFETA      PIC X.                                     00001680
           02 MSDEFFETC PIC X.                                          00001690
           02 MSDEFFETP PIC X.                                          00001700
           02 MSDEFFETH PIC X.                                          00001710
           02 MSDEFFETV PIC X.                                          00001720
           02 MSDEFFETO      PIC X(8).                                  00001730
           02 MLIGNEO OCCURS   13 TIMES .                               00001740
             03 FILLER       PIC X(2).                                  00001750
             03 MNNCODICA    PIC X.                                     00001760
             03 MNNCODICC    PIC X.                                     00001770
             03 MNNCODICP    PIC X.                                     00001780
             03 MNNCODICH    PIC X.                                     00001790
             03 MNNCODICV    PIC X.                                     00001800
             03 MNNCODICO    PIC X(7).                                  00001810
             03 FILLER       PIC X(2).                                  00001820
             03 MNBLOCAGEA   PIC X.                                     00001830
             03 MNBLOCAGEC   PIC X.                                     00001840
             03 MNBLOCAGEP   PIC X.                                     00001850
             03 MNBLOCAGEH   PIC X.                                     00001860
             03 MNBLOCAGEV   PIC X.                                     00001870
             03 MNBLOCAGEO   PIC X(3).                                  00001880
             03 FILLER       PIC X(2).                                  00001890
             03 MNZBLOCAGEA  PIC X.                                     00001900
             03 MNZBLOCAGEC  PIC X.                                     00001910
             03 MNZBLOCAGEP  PIC X.                                     00001920
             03 MNZBLOCAGEH  PIC X.                                     00001930
             03 MNZBLOCAGEV  PIC X.                                     00001940
             03 MNZBLOCAGEO  PIC X(3).                                  00001950
             03 FILLER       PIC X(2).                                  00001960
             03 MNLIBELLEA   PIC X.                                     00001970
             03 MNLIBELLEC   PIC X.                                     00001980
             03 MNLIBELLEP   PIC X.                                     00001990
             03 MNLIBELLEH   PIC X.                                     00002000
             03 MNLIBELLEV   PIC X.                                     00002010
             03 MNLIBELLEO   PIC X(20).                                 00002020
             03 FILLER       PIC X(2).                                  00002030
             03 MNQPILEA     PIC X.                                     00002040
             03 MNQPILEC     PIC X.                                     00002050
             03 MNQPILEP     PIC X.                                     00002060
             03 MNQPILEH     PIC X.                                     00002070
             03 MNQPILEV     PIC X.                                     00002080
             03 MNQPILEO     PIC X(9).                                  00002090
             03 FILLER       PIC X(2).                                  00002100
             03 MNDEFFETA    PIC X.                                     00002110
             03 MNDEFFETC    PIC X.                                     00002120
             03 MNDEFFETP    PIC X.                                     00002130
             03 MNDEFFETH    PIC X.                                     00002140
             03 MNDEFFETV    PIC X.                                     00002150
             03 MNDEFFETO    PIC X(8).                                  00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MNBLOCAGEIA  PIC X.                                     00002180
             03 MNBLOCAGEIC  PIC X.                                     00002190
             03 MNBLOCAGEIP  PIC X.                                     00002200
             03 MNBLOCAGEIH  PIC X.                                     00002210
             03 MNBLOCAGEIV  PIC X.                                     00002220
             03 MNBLOCAGEIO  PIC X(3).                                  00002230
             03 FILLER       PIC X(2).                                  00002240
             03 MNZBLOCAGEIA      PIC X.                                00002250
             03 MNZBLOCAGEIC PIC X.                                     00002260
             03 MNZBLOCAGEIP PIC X.                                     00002270
             03 MNZBLOCAGEIH PIC X.                                     00002280
             03 MNZBLOCAGEIV PIC X.                                     00002290
             03 MNZBLOCAGEIO      PIC X(3).                             00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLIBERRA  PIC X.                                          00002320
           02 MLIBERRC  PIC X.                                          00002330
           02 MLIBERRP  PIC X.                                          00002340
           02 MLIBERRH  PIC X.                                          00002350
           02 MLIBERRV  PIC X.                                          00002360
           02 MLIBERRO  PIC X(73).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCODTRAA  PIC X.                                          00002390
           02 MCODTRAC  PIC X.                                          00002400
           02 MCODTRAP  PIC X.                                          00002410
           02 MCODTRAH  PIC X.                                          00002420
           02 MCODTRAV  PIC X.                                          00002430
           02 MCODTRAO  PIC X(4).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MCICSA    PIC X.                                          00002460
           02 MCICSC    PIC X.                                          00002470
           02 MCICSP    PIC X.                                          00002480
           02 MCICSH    PIC X.                                          00002490
           02 MCICSV    PIC X.                                          00002500
           02 MCICSO    PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNETNAMA  PIC X.                                          00002530
           02 MNETNAMC  PIC X.                                          00002540
           02 MNETNAMP  PIC X.                                          00002550
           02 MNETNAMH  PIC X.                                          00002560
           02 MNETNAMV  PIC X.                                          00002570
           02 MNETNAMO  PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MSCREENA  PIC X.                                          00002600
           02 MSCREENC  PIC X.                                          00002610
           02 MSCREENP  PIC X.                                          00002620
           02 MSCREENH  PIC X.                                          00002630
           02 MSCREENV  PIC X.                                          00002640
           02 MSCREENO  PIC X(4).                                       00002650
                                                                                
