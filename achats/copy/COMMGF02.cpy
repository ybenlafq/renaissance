      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGF02                    TR: GF02  *    00020001
      * SAISIE DES COMMANDES FOURNISSEUR ARTICLES-QUANTITES        *    00030000
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 7421          
      *                                                                 00060000
          02 COMM-GF02-APPLI REDEFINES COMM-GF00-APPLI.                 00070001
      *                                                                 00080000
      * ZONES PROPRES AU PROGRAMME         TGF02    ------------- 312           
      *                                                                 00100000
      *---------------------------ENTETE ECRAN                          00110002
             03 COMM-GF02-CHEFPROD          PIC X(5).                   00120001
             03 COMM-GF02-NSOCIETE          PIC X(3).                   00120202
             03 COMM-GF02-NDEPOT            PIC X(3).                   00120302
      *                                                                 00121001
             03 COMM-GF02-PROPRE.                                       00130001
      *------------------------------ PAGINATION                        00160001
                04 COMM-GF02-ZONE-PAGINATION.                           00170001
      *------------------------------ NUMERO DE PAGE                    00180000
                   05 COMM-GF02-NPAGE       PIC 9(3) COMP-3.            00190001
      *------------------------------ NUMERO ITEM DE LA NIEME PAGE      00220000
                   05 COMM-GF02-TAB-ITEM    OCCURS 100.                 00230001
                      06 COMM-GF02-NITEM    PIC 9(3) COMP-3.            00240001
      *------------------------------ DERNIER ITEM LU PAGE EN COURS     00250000
                   05 COMM-GF02-NITEM-PAGE  PIC 9(3) COMP-3.            00260001
      *------------------------------ DERNIER ITEM DE LA TS GF02        00270001
                   05 COMM-GF02-NITEM-MAX   PIC 9(3) COMP-3.            00280001
      *------------------------------ TOP EXISTENCE TS GF02             00290001
                   05 COMM-GF02-EXIST-TS    PIC X(1).                   00300001
      *------------------------------ NOMBRE DE CODICS SAISIS           00330001
                   05 COMM-GF02-NOMBRE-CODIC PIC 9(3) COMP-3.           00340001
      *------------------------------ NUMERO ITEM MAP                           
                   05 COMM-GF02-NITEM-MAP   PIC 9(3) COMP-3.                    
      *------------------------------ NUMERO ITEM IGNORES ('S')                 
                   05 COMM-GF02-NITEM-IGNO  PIC 9(3) COMP-3.                    
      *------------------------------ MAP AFFICHEE                      00350001
                04 COMM-GF02-ZONE-MAP       OCCURS 10.                  00360001
      *------------------------------ NUMERO CODIC                      00390001
                   05 COMM-GF02-NCODIC      PIC X(7).                   00400001
      *------------------------------ NUMERO ITEM DE LA LIGNE           00560001
                   05 COMM-GF02-NITEM-CODIC PIC 9(3) COMP-3.            00570001
      ***************************************************************** 00600000
                                                                                
