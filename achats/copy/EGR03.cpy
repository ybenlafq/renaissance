      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR03   EGR03                                              00000020
      ***************************************************************** 00000030
       01   EGR03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETOTL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGETOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGETOTF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGETOTI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSEML     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSEML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSEMF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSEMI     PIC X(6).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOURL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MJOURL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MJOURF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MJOURI    PIC X(10).                                      00000290
           02 MREP1I OCCURS   10 TIMES .                                00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTAL      COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MQUOTAL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQUOTAF      PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MQUOTAI      PIC X(5).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLQUOTAL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MLQUOTAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLQUOTAF     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MLQUOTAI     PIC X(20).                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTOTL  COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MQTOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTOTF  PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MQTOTI  PIC X(8).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQDISPL      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MQDISPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQDISPF      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MQDISPI      PIC X(8).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPRISL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MQPRISL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPRISF      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MQPRISI      PIC X(8).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQRECL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MQRECL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQRECF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MQRECI  PIC X(8).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTTOTL    COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MTTOTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTTOTF    PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MTTOTI    PIC X(8).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTDISPL   COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MTDISPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTDISPF   PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MTDISPI   PIC X(8).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTPRISL   COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MTPRISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTPRISF   PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MTPRISI   PIC X(8).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRECL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MTRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTRECF    PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MTRECI    PIC X(8).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPPRISL   COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MPPRISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPPRISF   PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MPPRISI   PIC X(8).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRECL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MPRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPRECF    PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MPRECI    PIC X(8).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MZONCMDI  PIC X(15).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(58).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCICSI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNETNAMI  PIC X(8).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MSCREENI  PIC X(4).                                       00001020
      ***************************************************************** 00001030
      * SDF: EGR03   EGR03                                              00001040
      ***************************************************************** 00001050
       01   EGR03O REDEFINES EGR03I.                                    00001060
           02 FILLER    PIC X(12).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MDATJOUA  PIC X.                                          00001090
           02 MDATJOUC  PIC X.                                          00001100
           02 MDATJOUP  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUV  PIC X.                                          00001130
           02 MDATJOUO  PIC X(10).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MPAGEA    PIC X.                                          00001230
           02 MPAGEC    PIC X.                                          00001240
           02 MPAGEP    PIC X.                                          00001250
           02 MPAGEH    PIC X.                                          00001260
           02 MPAGEV    PIC X.                                          00001270
           02 MPAGEO    PIC ZZ.                                         00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MPAGETOTA      PIC X.                                     00001300
           02 MPAGETOTC PIC X.                                          00001310
           02 MPAGETOTP PIC X.                                          00001320
           02 MPAGETOTH PIC X.                                          00001330
           02 MPAGETOTV PIC X.                                          00001340
           02 MPAGETOTO      PIC ZZ.                                    00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MSEMA     PIC X.                                          00001370
           02 MSEMC     PIC X.                                          00001380
           02 MSEMP     PIC X.                                          00001390
           02 MSEMH     PIC X.                                          00001400
           02 MSEMV     PIC X.                                          00001410
           02 MSEMO     PIC X(6).                                       00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MJOURA    PIC X.                                          00001440
           02 MJOURC    PIC X.                                          00001450
           02 MJOURP    PIC X.                                          00001460
           02 MJOURH    PIC X.                                          00001470
           02 MJOURV    PIC X.                                          00001480
           02 MJOURO    PIC X(10).                                      00001490
           02 MREP1O OCCURS   10 TIMES .                                00001500
             03 FILLER       PIC X(2).                                  00001510
             03 MQUOTAA      PIC X.                                     00001520
             03 MQUOTAC PIC X.                                          00001530
             03 MQUOTAP PIC X.                                          00001540
             03 MQUOTAH PIC X.                                          00001550
             03 MQUOTAV PIC X.                                          00001560
             03 MQUOTAO      PIC X(5).                                  00001570
             03 FILLER       PIC X(2).                                  00001580
             03 MLQUOTAA     PIC X.                                     00001590
             03 MLQUOTAC     PIC X.                                     00001600
             03 MLQUOTAP     PIC X.                                     00001610
             03 MLQUOTAH     PIC X.                                     00001620
             03 MLQUOTAV     PIC X.                                     00001630
             03 MLQUOTAO     PIC X(20).                                 00001640
             03 FILLER       PIC X(2).                                  00001650
             03 MQTOTA  PIC X.                                          00001660
             03 MQTOTC  PIC X.                                          00001670
             03 MQTOTP  PIC X.                                          00001680
             03 MQTOTH  PIC X.                                          00001690
             03 MQTOTV  PIC X.                                          00001700
             03 MQTOTO  PIC Z(8).                                       00001710
             03 FILLER       PIC X(2).                                  00001720
             03 MQDISPA      PIC X.                                     00001730
             03 MQDISPC PIC X.                                          00001740
             03 MQDISPP PIC X.                                          00001750
             03 MQDISPH PIC X.                                          00001760
             03 MQDISPV PIC X.                                          00001770
             03 MQDISPO      PIC -(8).                                  00001780
             03 FILLER       PIC X(2).                                  00001790
             03 MQPRISA      PIC X.                                     00001800
             03 MQPRISC PIC X.                                          00001810
             03 MQPRISP PIC X.                                          00001820
             03 MQPRISH PIC X.                                          00001830
             03 MQPRISV PIC X.                                          00001840
             03 MQPRISO      PIC Z(8).                                  00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MQRECA  PIC X.                                          00001870
             03 MQRECC  PIC X.                                          00001880
             03 MQRECP  PIC X.                                          00001890
             03 MQRECH  PIC X.                                          00001900
             03 MQRECV  PIC X.                                          00001910
             03 MQRECO  PIC Z(8).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MTTOTA    PIC X.                                          00001940
           02 MTTOTC    PIC X.                                          00001950
           02 MTTOTP    PIC X.                                          00001960
           02 MTTOTH    PIC X.                                          00001970
           02 MTTOTV    PIC X.                                          00001980
           02 MTTOTO    PIC Z(8).                                       00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MTDISPA   PIC X.                                          00002010
           02 MTDISPC   PIC X.                                          00002020
           02 MTDISPP   PIC X.                                          00002030
           02 MTDISPH   PIC X.                                          00002040
           02 MTDISPV   PIC X.                                          00002050
           02 MTDISPO   PIC -(8).                                       00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MTPRISA   PIC X.                                          00002080
           02 MTPRISC   PIC X.                                          00002090
           02 MTPRISP   PIC X.                                          00002100
           02 MTPRISH   PIC X.                                          00002110
           02 MTPRISV   PIC X.                                          00002120
           02 MTPRISO   PIC Z(8).                                       00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MTRECA    PIC X.                                          00002150
           02 MTRECC    PIC X.                                          00002160
           02 MTRECP    PIC X.                                          00002170
           02 MTRECH    PIC X.                                          00002180
           02 MTRECV    PIC X.                                          00002190
           02 MTRECO    PIC Z(8).                                       00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MPPRISA   PIC X.                                          00002220
           02 MPPRISC   PIC X.                                          00002230
           02 MPPRISP   PIC X.                                          00002240
           02 MPPRISH   PIC X.                                          00002250
           02 MPPRISV   PIC X.                                          00002260
           02 MPPRISO   PIC Z(8).                                       00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MPRECA    PIC X.                                          00002290
           02 MPRECC    PIC X.                                          00002300
           02 MPRECP    PIC X.                                          00002310
           02 MPRECH    PIC X.                                          00002320
           02 MPRECV    PIC X.                                          00002330
           02 MPRECO    PIC Z(8).                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MZONCMDA  PIC X.                                          00002360
           02 MZONCMDC  PIC X.                                          00002370
           02 MZONCMDP  PIC X.                                          00002380
           02 MZONCMDH  PIC X.                                          00002390
           02 MZONCMDV  PIC X.                                          00002400
           02 MZONCMDO  PIC X(15).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLIBERRA  PIC X.                                          00002430
           02 MLIBERRC  PIC X.                                          00002440
           02 MLIBERRP  PIC X.                                          00002450
           02 MLIBERRH  PIC X.                                          00002460
           02 MLIBERRV  PIC X.                                          00002470
           02 MLIBERRO  PIC X(58).                                      00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCODTRAA  PIC X.                                          00002500
           02 MCODTRAC  PIC X.                                          00002510
           02 MCODTRAP  PIC X.                                          00002520
           02 MCODTRAH  PIC X.                                          00002530
           02 MCODTRAV  PIC X.                                          00002540
           02 MCODTRAO  PIC X(4).                                       00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MCICSA    PIC X.                                          00002570
           02 MCICSC    PIC X.                                          00002580
           02 MCICSP    PIC X.                                          00002590
           02 MCICSH    PIC X.                                          00002600
           02 MCICSV    PIC X.                                          00002610
           02 MCICSO    PIC X(5).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MNETNAMA  PIC X.                                          00002640
           02 MNETNAMC  PIC X.                                          00002650
           02 MNETNAMP  PIC X.                                          00002660
           02 MNETNAMH  PIC X.                                          00002670
           02 MNETNAMV  PIC X.                                          00002680
           02 MNETNAMO  PIC X(8).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MSCREENA  PIC X.                                          00002710
           02 MSCREENC  PIC X.                                          00002720
           02 MSCREENP  PIC X.                                          00002730
           02 MSCREENH  PIC X.                                          00002740
           02 MSCREENV  PIC X.                                          00002750
           02 MSCREENO  PIC X(4).                                       00002760
                                                                                
