      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: TGf55   TGf55                                              00000020
      ***************************************************************** 00000030
       01   EGF55I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLEL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLIBELLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBELLEF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLIBELLEI      PIC X(12).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEBOOKL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNCDEBOOKL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNCDEBOOKF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCDEBOOKI     PIC X(7).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNCODICI  PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLREFFOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLREFFOF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLREFFOI  PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNSOCI    PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNDEPOTI  PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTCDEL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MNENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTCDEF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNENTCDEI      PIC X(5).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTCDEL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MLENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTCDEF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLENTCDEI      PIC X(20).                                 00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDUEDATEL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MDUEDATEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDUEDATEF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDUEDATEI      PIC X(8).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDLIVRAISONL   COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MDLIVRAISONL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MDLIVRAISONF   PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MDLIVRAISONI   PIC X(8).                                  00000610
           02 MTABI OCCURS   10 TIMES .                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MSELI   PIC X.                                          00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCREASL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCREASL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCREASF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCREASI      PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREASL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MLREASL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLREASF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MLREASI      PIC X(60).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MZONCMDI  PIC X(15).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLIBERRI  PIC X(58).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MNETNAMI  PIC X(8).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MSCREENI  PIC X(4).                                       00000980
      ***************************************************************** 00000990
      * SDF: TGf55   TGf55                                              00001000
      ***************************************************************** 00001010
       01   EGF55O REDEFINES EGF55I.                                    00001020
           02 FILLER    PIC X(12).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTIMJOUA  PIC X.                                          00001120
           02 MTIMJOUC  PIC X.                                          00001130
           02 MTIMJOUP  PIC X.                                          00001140
           02 MTIMJOUH  PIC X.                                          00001150
           02 MTIMJOUV  PIC X.                                          00001160
           02 MTIMJOUO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MPAGEA    PIC X.                                          00001190
           02 MPAGEC    PIC X.                                          00001200
           02 MPAGEP    PIC X.                                          00001210
           02 MPAGEH    PIC X.                                          00001220
           02 MPAGEV    PIC X.                                          00001230
           02 MPAGEO    PIC X(3).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MPAGEMAXA      PIC X.                                     00001260
           02 MPAGEMAXC PIC X.                                          00001270
           02 MPAGEMAXP PIC X.                                          00001280
           02 MPAGEMAXH PIC X.                                          00001290
           02 MPAGEMAXV PIC X.                                          00001300
           02 MPAGEMAXO      PIC X(3).                                  00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MLIBELLEA      PIC X.                                     00001330
           02 MLIBELLEC PIC X.                                          00001340
           02 MLIBELLEP PIC X.                                          00001350
           02 MLIBELLEH PIC X.                                          00001360
           02 MLIBELLEV PIC X.                                          00001370
           02 MLIBELLEO      PIC X(12).                                 00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MNCDEBOOKA     PIC X.                                     00001400
           02 MNCDEBOOKC     PIC X.                                     00001410
           02 MNCDEBOOKP     PIC X.                                     00001420
           02 MNCDEBOOKH     PIC X.                                     00001430
           02 MNCDEBOOKV     PIC X.                                     00001440
           02 MNCDEBOOKO     PIC X(7).                                  00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MNCODICA  PIC X.                                          00001470
           02 MNCODICC  PIC X.                                          00001480
           02 MNCODICP  PIC X.                                          00001490
           02 MNCODICH  PIC X.                                          00001500
           02 MNCODICV  PIC X.                                          00001510
           02 MNCODICO  PIC X(7).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MLREFFOA  PIC X.                                          00001540
           02 MLREFFOC  PIC X.                                          00001550
           02 MLREFFOP  PIC X.                                          00001560
           02 MLREFFOH  PIC X.                                          00001570
           02 MLREFFOV  PIC X.                                          00001580
           02 MLREFFOO  PIC X(20).                                      00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MNSOCA    PIC X.                                          00001610
           02 MNSOCC    PIC X.                                          00001620
           02 MNSOCP    PIC X.                                          00001630
           02 MNSOCH    PIC X.                                          00001640
           02 MNSOCV    PIC X.                                          00001650
           02 MNSOCO    PIC X(3).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MNDEPOTA  PIC X.                                          00001680
           02 MNDEPOTC  PIC X.                                          00001690
           02 MNDEPOTP  PIC X.                                          00001700
           02 MNDEPOTH  PIC X.                                          00001710
           02 MNDEPOTV  PIC X.                                          00001720
           02 MNDEPOTO  PIC X(3).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MNENTCDEA      PIC X.                                     00001750
           02 MNENTCDEC PIC X.                                          00001760
           02 MNENTCDEP PIC X.                                          00001770
           02 MNENTCDEH PIC X.                                          00001780
           02 MNENTCDEV PIC X.                                          00001790
           02 MNENTCDEO      PIC X(5).                                  00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MLENTCDEA      PIC X.                                     00001820
           02 MLENTCDEC PIC X.                                          00001830
           02 MLENTCDEP PIC X.                                          00001840
           02 MLENTCDEH PIC X.                                          00001850
           02 MLENTCDEV PIC X.                                          00001860
           02 MLENTCDEO      PIC X(20).                                 00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MDUEDATEA      PIC X.                                     00001890
           02 MDUEDATEC PIC X.                                          00001900
           02 MDUEDATEP PIC X.                                          00001910
           02 MDUEDATEH PIC X.                                          00001920
           02 MDUEDATEV PIC X.                                          00001930
           02 MDUEDATEO      PIC X(8).                                  00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MDLIVRAISONA   PIC X.                                     00001960
           02 MDLIVRAISONC   PIC X.                                     00001970
           02 MDLIVRAISONP   PIC X.                                     00001980
           02 MDLIVRAISONH   PIC X.                                     00001990
           02 MDLIVRAISONV   PIC X.                                     00002000
           02 MDLIVRAISONO   PIC X(8).                                  00002010
           02 MTABO OCCURS   10 TIMES .                                 00002020
             03 FILLER       PIC X(2).                                  00002030
             03 MSELA   PIC X.                                          00002040
             03 MSELC   PIC X.                                          00002050
             03 MSELP   PIC X.                                          00002060
             03 MSELH   PIC X.                                          00002070
             03 MSELV   PIC X.                                          00002080
             03 MSELO   PIC X.                                          00002090
             03 FILLER       PIC X(2).                                  00002100
             03 MCREASA      PIC X.                                     00002110
             03 MCREASC PIC X.                                          00002120
             03 MCREASP PIC X.                                          00002130
             03 MCREASH PIC X.                                          00002140
             03 MCREASV PIC X.                                          00002150
             03 MCREASO      PIC X(5).                                  00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MLREASA      PIC X.                                     00002180
             03 MLREASC PIC X.                                          00002190
             03 MLREASP PIC X.                                          00002200
             03 MLREASH PIC X.                                          00002210
             03 MLREASV PIC X.                                          00002220
             03 MLREASO      PIC X(60).                                 00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MZONCMDA  PIC X.                                          00002250
           02 MZONCMDC  PIC X.                                          00002260
           02 MZONCMDP  PIC X.                                          00002270
           02 MZONCMDH  PIC X.                                          00002280
           02 MZONCMDV  PIC X.                                          00002290
           02 MZONCMDO  PIC X(15).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLIBERRA  PIC X.                                          00002320
           02 MLIBERRC  PIC X.                                          00002330
           02 MLIBERRP  PIC X.                                          00002340
           02 MLIBERRH  PIC X.                                          00002350
           02 MLIBERRV  PIC X.                                          00002360
           02 MLIBERRO  PIC X(58).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCODTRAA  PIC X.                                          00002390
           02 MCODTRAC  PIC X.                                          00002400
           02 MCODTRAP  PIC X.                                          00002410
           02 MCODTRAH  PIC X.                                          00002420
           02 MCODTRAV  PIC X.                                          00002430
           02 MCODTRAO  PIC X(4).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MCICSA    PIC X.                                          00002460
           02 MCICSC    PIC X.                                          00002470
           02 MCICSP    PIC X.                                          00002480
           02 MCICSH    PIC X.                                          00002490
           02 MCICSV    PIC X.                                          00002500
           02 MCICSO    PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNETNAMA  PIC X.                                          00002530
           02 MNETNAMC  PIC X.                                          00002540
           02 MNETNAMP  PIC X.                                          00002550
           02 MNETNAMH  PIC X.                                          00002560
           02 MNETNAMV  PIC X.                                          00002570
           02 MNETNAMO  PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MSCREENA  PIC X.                                          00002600
           02 MSCREENC  PIC X.                                          00002610
           02 MSCREENP  PIC X.                                          00002620
           02 MSCREENH  PIC X.                                          00002630
           02 MSCREENV  PIC X.                                          00002640
           02 MSCREENO  PIC X(4).                                       00002650
                                                                                
