      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
       01 TS-GF43S-DONNEES.                                                     
          05 TS-GF43S-PAGE.                                                     
             10 TS-GF43S-LIGNE OCCURS 12.                                       
                15 TS-GF43S-NCODIC        PIC X(07).                            
                15 TS-GF43S-QTOT          PIC S9(6) COMP-3.                     
                15 TS-GF43S-QTE-LIGNE OCCURS 3.                                 
                   20 TS-GF43S-QCDE       PIC S9(5) COMP-3.                     
                   20 TS-GF43S-QUOLIVR    PIC S9(5) COMP-3.                     
                                                                                
