      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TGR20 (TGR00 -> MENU)    TR: GR00  *    00002201
      *               GESTION DES RECEPTIONS                       *    00002301
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3710  00009001
      *                                                                 00410100
      *        TRANSACTION GR20 : INTERROGATION RECEPTIONS A QUAI  *    00411001
      *                                                                 00412000
          02 COMM-GR20-APPLI REDEFINES COMM-GR00-APPLI.                 00420001
      *------------------------------ ZONE DONNEES TGR20                00510001
             03 COMM-GR20-DONNEES-TGR20.                                00520001
      *------------------------------ CODE SOCIETE                      00550001
                04 COMM-GR20-NSOCIETE          PIC XXX.                 00560001
      *------------------------------ LIBELLE SOCIETE                   00570001
                04 COMM-GR20-LSOCIETE          PIC X(20).               00580001
      *------------------------------ CODE DEPOT                        00590001
                04 COMM-GR20-NDEPOT            PIC XXX.                 00600001
      *------------------------------ LIBELLE DEPOT                     00610001
                04 COMM-GR20-LDEPOT            PIC X(20).               00620001
      *------------------------------ CODE LIVREUR                      00630001
                04 COMM-GR20-CLIVR             PIC X(5).                00640001
      *------------------------------ LIBELLE LIVREUR                   00650001
                04 COMM-GR20-LLIVR             PIC X(20).               00660001
      *------------------------------ CODE TRANSPORT                    00670001
                04 COMM-GR20-CMODETRANS        PIC X(5).                00680001
      *------------------------------ LIBELLE TRANSPORT                 00690001
                04 COMM-GR20-LMODETRANS        PIC X(20).               00700001
      *------------------------------ N�DE RECEPTION                    00710001
                04 COMM-GR20-NRECQUAI          PIC X(07).               00720001
      *------------------------------ DATE RECEPTION                    00731901
                04 COMM-GR20-DJRECQUAI.                                 00732001
      *------------------------------ DATE ANNEE                        00732501
                   06 COMM-GR20-DANNEE.                                 00732601
      *------------------------------ DATE SIECLE                       00732701
                       08 COMM-GR20-DANNEE1         PIC X(2).           00732801
      *------------------------------ DATE ANNEE                        00732901
                       08 COMM-GR20-DANNEE2         PIC X(2).           00733001
      *------------------------------ DATE MOIS                         00734001
                   06 COMM-GR20-DMOIS               PIC X(2).           00735001
      *------------------------------ DATE JOUR                         00736001
                   06 COMM-GR20-DJOUR               PIC X(2).           00736101
      *------------------------------ DATE HEURE                        00736201
                04 COMM-GR20-DHRECQUAI        PIC X(2).                 00736301
      *------------------------------ DATE MINUTE                       00736401
                04 COMM-GR20-DMRECQUAI        PIC X(2).                 00736501
      *------------------------------ COMMENTAIRE                       00736601
                04 COMM-GR20-LCOMMENT         PIC X(20).                00736701
      *------------------------------ TOTAL PIECES                      00736801
                04 COMM-GR20-QPIECES          PIC X(5).                 00736901
      *-------------------------- ZONE QUODICS PAGE                     00737001
                04 COMM-GR20-LIGCODICS         OCCURS 11.               00737101
                   06 COMM-GR20-NCODIC         PIC X(07).               00737201
                   06 COMM-GR20-LEMBAL         PIC X(51).               00737301
                   06 COMM-GR20-QTE            PIC S9(05) COMP-3.       00737401
      *------------------------------ TABLE N� TS PR PAGINATION         00739901
                04 COMM-GR20-TABART         OCCURS 100.                 00740001
      *------------------------------ 1ERS IND TS DES PAGES <=> CODIC   00740101
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            05 COMM-GR20-NUMTS          PIC S9(4) COMP.          00740201
      *--                                                                       
                   05 COMM-GR20-NUMTS          PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ INDICATEUR FIN DE TABLE           00740301
                04 COMM-GR20-INDPAG            PIC 9.                   00740401
      *------------------------------ DERNIER IND-TS                    00740501
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GR20-PAGSUI            PIC S9(4) COMP.          00740601
      *--                                                                       
                04 COMM-GR20-PAGSUI            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ ANCIEN  IND-TS                    00740701
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GR20-PAGENC            PIC S9(4) COMP.          00740801
      *--                                                                       
                04 COMM-GR20-PAGENC            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ NUMERO PAGE ECRAN                 00740901
                04 COMM-GR20-NUMPAG            PIC 9(3).                00741001
      *------------------------------ IND POUR RECH DS TS               00741101
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GR20-IND-RECH-TS    PIC S9(4) COMP.             00741201
      *--                                                                       
                04 COMM-GR20-IND-RECH-TS    PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ IND TS MAX                        00741301
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GR20-IND-TS-MAX     PIC S9(4) COMP.             00741401
      *--                                                                       
                04 COMM-GR20-IND-TS-MAX     PIC S9(4) COMP-5.                   
      *}                                                                        
      *------------------------------ ZONES SWAP                        00741501
                04 COMM-GR20-SWAP           PIC 9 OCCURS 200.           00741601
      *------------------------------ ZONE LIBRE                        00741701
             03 COMM-GR20-LIBRE          PIC X(2467).                   00742001
      ***************************************************************** 00750000
                                                                                
