      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF42   EGF42                                              00000020
      ***************************************************************** 00000030
       01   EGF42I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTCDEL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTCDEF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNENTCDEI      PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTCDEL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTCDEF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLENTCDEI      PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERLOCUTL  COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCINTERLOCUTL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MCINTERLOCUTF  PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCINTERLOCUTI  PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERLOCUTL  COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLINTERLOCUTL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MLINTERLOCUTF  PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLINTERLOCUTI  PIC X(20).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRAYONL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCRAYONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCRAYONF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCRAYONI  PIC X(5).                                       00000410
           02 MSTRUCTURI OCCURS   10 TIMES .                            00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCFAMI  PIC X(5).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLFAML  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MLFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLFAMF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MLFAMI  PIC X(20).                                      00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MZONCMDI  PIC X(15).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLIBERRI  PIC X(58).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCODTRAI  PIC X(4).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCICSI    PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MNETNAMI  PIC X(8).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MSCREENI  PIC X(4).                                       00000740
      ***************************************************************** 00000750
      * SDF: EGF42   EGF42                                              00000760
      ***************************************************************** 00000770
       01   EGF42O REDEFINES EGF42I.                                    00000780
           02 FILLER    PIC X(12).                                      00000790
           02 FILLER    PIC X(2).                                       00000800
           02 MDATJOUA  PIC X.                                          00000810
           02 MDATJOUC  PIC X.                                          00000820
           02 MDATJOUP  PIC X.                                          00000830
           02 MDATJOUH  PIC X.                                          00000840
           02 MDATJOUV  PIC X.                                          00000850
           02 MDATJOUO  PIC X(10).                                      00000860
           02 FILLER    PIC X(2).                                       00000870
           02 MTIMJOUA  PIC X.                                          00000880
           02 MTIMJOUC  PIC X.                                          00000890
           02 MTIMJOUP  PIC X.                                          00000900
           02 MTIMJOUH  PIC X.                                          00000910
           02 MTIMJOUV  PIC X.                                          00000920
           02 MTIMJOUO  PIC X(5).                                       00000930
           02 FILLER    PIC X(2).                                       00000940
           02 MWPAGEA   PIC X.                                          00000950
           02 MWPAGEC   PIC X.                                          00000960
           02 MWPAGEP   PIC X.                                          00000970
           02 MWPAGEH   PIC X.                                          00000980
           02 MWPAGEV   PIC X.                                          00000990
           02 MWPAGEO   PIC X(3).                                       00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MWFONCA   PIC X.                                          00001020
           02 MWFONCC   PIC X.                                          00001030
           02 MWFONCP   PIC X.                                          00001040
           02 MWFONCH   PIC X.                                          00001050
           02 MWFONCV   PIC X.                                          00001060
           02 MWFONCO   PIC X(3).                                       00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MNENTCDEA      PIC X.                                     00001090
           02 MNENTCDEC PIC X.                                          00001100
           02 MNENTCDEP PIC X.                                          00001110
           02 MNENTCDEH PIC X.                                          00001120
           02 MNENTCDEV PIC X.                                          00001130
           02 MNENTCDEO      PIC X(5).                                  00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MLENTCDEA      PIC X.                                     00001160
           02 MLENTCDEC PIC X.                                          00001170
           02 MLENTCDEP PIC X.                                          00001180
           02 MLENTCDEH PIC X.                                          00001190
           02 MLENTCDEV PIC X.                                          00001200
           02 MLENTCDEO      PIC X(20).                                 00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MCINTERLOCUTA  PIC X.                                     00001230
           02 MCINTERLOCUTC  PIC X.                                     00001240
           02 MCINTERLOCUTP  PIC X.                                     00001250
           02 MCINTERLOCUTH  PIC X.                                     00001260
           02 MCINTERLOCUTV  PIC X.                                     00001270
           02 MCINTERLOCUTO  PIC X(5).                                  00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MLINTERLOCUTA  PIC X.                                     00001300
           02 MLINTERLOCUTC  PIC X.                                     00001310
           02 MLINTERLOCUTP  PIC X.                                     00001320
           02 MLINTERLOCUTH  PIC X.                                     00001330
           02 MLINTERLOCUTV  PIC X.                                     00001340
           02 MLINTERLOCUTO  PIC X(20).                                 00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MCRAYONA  PIC X.                                          00001370
           02 MCRAYONC  PIC X.                                          00001380
           02 MCRAYONP  PIC X.                                          00001390
           02 MCRAYONH  PIC X.                                          00001400
           02 MCRAYONV  PIC X.                                          00001410
           02 MCRAYONO  PIC X(5).                                       00001420
           02 MSTRUCTURO OCCURS   10 TIMES .                            00001430
             03 FILLER       PIC X(2).                                  00001440
             03 MCFAMA  PIC X.                                          00001450
             03 MCFAMC  PIC X.                                          00001460
             03 MCFAMP  PIC X.                                          00001470
             03 MCFAMH  PIC X.                                          00001480
             03 MCFAMV  PIC X.                                          00001490
             03 MCFAMO  PIC X(5).                                       00001500
             03 FILLER       PIC X(2).                                  00001510
             03 MLFAMA  PIC X.                                          00001520
             03 MLFAMC  PIC X.                                          00001530
             03 MLFAMP  PIC X.                                          00001540
             03 MLFAMH  PIC X.                                          00001550
             03 MLFAMV  PIC X.                                          00001560
             03 MLFAMO  PIC X(20).                                      00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MZONCMDA  PIC X.                                          00001590
           02 MZONCMDC  PIC X.                                          00001600
           02 MZONCMDP  PIC X.                                          00001610
           02 MZONCMDH  PIC X.                                          00001620
           02 MZONCMDV  PIC X.                                          00001630
           02 MZONCMDO  PIC X(15).                                      00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MLIBERRA  PIC X.                                          00001660
           02 MLIBERRC  PIC X.                                          00001670
           02 MLIBERRP  PIC X.                                          00001680
           02 MLIBERRH  PIC X.                                          00001690
           02 MLIBERRV  PIC X.                                          00001700
           02 MLIBERRO  PIC X(58).                                      00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MCODTRAA  PIC X.                                          00001730
           02 MCODTRAC  PIC X.                                          00001740
           02 MCODTRAP  PIC X.                                          00001750
           02 MCODTRAH  PIC X.                                          00001760
           02 MCODTRAV  PIC X.                                          00001770
           02 MCODTRAO  PIC X(4).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MCICSA    PIC X.                                          00001800
           02 MCICSC    PIC X.                                          00001810
           02 MCICSP    PIC X.                                          00001820
           02 MCICSH    PIC X.                                          00001830
           02 MCICSV    PIC X.                                          00001840
           02 MCICSO    PIC X(5).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MNETNAMA  PIC X.                                          00001870
           02 MNETNAMC  PIC X.                                          00001880
           02 MNETNAMP  PIC X.                                          00001890
           02 MNETNAMH  PIC X.                                          00001900
           02 MNETNAMV  PIC X.                                          00001910
           02 MNETNAMO  PIC X(8).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MSCREENA  PIC X.                                          00001940
           02 MSCREENC  PIC X.                                          00001950
           02 MSCREENP  PIC X.                                          00001960
           02 MSCREENH  PIC X.                                          00001970
           02 MSCREENV  PIC X.                                          00001980
           02 MSCREENO  PIC X(4).                                       00001990
                                                                                
