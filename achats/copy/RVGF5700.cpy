      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGF5700 = GF55 POUR DACEM                           
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF5700                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGF5700.                                                            
           02  GF57-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  GF57-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GF57-NCODIC                                                      
               PIC X(0007).                                                     
           02  GF57-DCDE                                                        
               PIC X(0008).                                                     
           02  GF57-QCDE                                                        
               PIC S9(5) COMP-3.                                                
           02  GF57-QCDERES                                                     
               PIC S9(5) COMP-3.                                                
           02  GF57-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGF5700                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGF5700-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF57-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF57-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF57-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF57-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF57-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF57-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF57-DCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF57-DCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF57-QCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF57-QCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF57-QCDERES-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF57-QCDERES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF57-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF57-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
