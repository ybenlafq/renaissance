      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGF3102                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF3102                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF3102.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF3102.                                                            
      *}                                                                        
           02  GF31-NSEQ                                                        
               PIC X(0002).                                                     
           02  GF31-NCDE                                                        
               PIC X(0007).                                                     
           02  GF31-NCODIC                                                      
               PIC X(0007).                                                     
           02  GF31-NLIVRAISON                                                  
               PIC X(0007).                                                     
           02  GF31-DLIVRAISON                                                  
               PIC X(0008).                                                     
           02  GF31-DSAISIE                                                     
               PIC X(0008).                                                     
           02  GF31-QCDE                                                        
               PIC S9(5) COMP-3.                                                
           02  GF31-QUOLIVR                                                     
               PIC S9(5) COMP-3.                                                
           02  GF31-QUOPALETTIS                                                 
               PIC S9(5) COMP-3.                                                
           02  GF31-CMODSTOCK                                                   
               PIC X(0005).                                                     
           02  GF31-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GF31-WCLT                                                        
               PIC X(0001).                                                     
           02  GF31-CREASON                                                     
               PIC X(0005).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGF3102                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF3102-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF3102-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-NLIVRAISON-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-NLIVRAISON-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-DLIVRAISON-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-DLIVRAISON-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-DSAISIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-QCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-QCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-QUOLIVR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-QUOLIVR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-QUOPALETTIS-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-QUOPALETTIS-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-CMODSTOCK-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-CMODSTOCK-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-WCLT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-WCLT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-CREASON-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-CREASON-F                                                   
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
