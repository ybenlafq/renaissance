      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGF2201                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF2201                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF2201.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF2201.                                                            
      *}                                                                        
           10 GF22-NCDE                 PIC X(7).                               
           10 GF22-NSEQUENCE            PIC S9(03)V USAGE COMP-3.               
           10 GF22-NSOCLIVR             PIC X(3).                               
           10 GF22-NDEPOT               PIC X(3).                               
           10 GF22-NCODIC               PIC X(7).                               
           10 GF22-QCDE                 PIC S9(05)V USAGE COMP-3.               
           10 GF22-WTYPCDE              PIC X(1).                               
           10 GF22-NSOCIETE             PIC X(3).                               
           10 GF22-NLIEU                PIC X(3).                               
           10 GF22-NVENTE               PIC X(7).                               
           10 GF22-NSEQ                 PIC X(2).                               
           10 GF22-DCREACDE             PIC X(8).                               
           10 GF22-DDESTOCKFOUR         PIC X(8).                               
           10 GF22-DLIVFOUR             PIC X(8).                               
           10 GF22-DDISPO               PIC X(8).                               
           10 GF22-WENVOIEDI            PIC X(1).                               
           10 GF22-DDELIV               PIC X(8).                               
           10 GF22-DMODIF               PIC X(8).                               
           10 GF22-NSOCDEPLIV           PIC X(3).                               
           10 GF22-NLIEUDEPLIV          PIC X(3).                               
           10 GF22-DANNUL               PIC X(8).                               
           10 GF22-DRECEPT              PIC X(8).                               
           10 GF22-DARCHIVAGE           PIC X(8).                               
           10 GF22-DSYST                PIC S9(13)V USAGE COMP-3.               
           10 GF22-DBASCUZAUTO          PIC X(8).                               
           10 GF22-LCOMMENT             PIC X(20).                              
           10 GF22-NMUTATION            PIC X(07).                              
           10 GF22-IDUSER               PIC X(08).                              
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGF2200                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF2201-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF2201-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-NCDE-F               PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-NCDE-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-NSEQUENCE-F          PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-NSEQUENCE-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-NSOCLIVR-F           PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-NSOCLIVR-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-NDEPOT-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-NDEPOT-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-NCODIC-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-NCODIC-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-QCDE-F               PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-QCDE-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-WTYPCDE-F            PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-WTYPCDE-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-NSOCIETE-F           PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-NSOCIETE-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-NLIEU-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-NLIEU-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-NVENTE-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-NVENTE-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-NSEQ-F               PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-NSEQ-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-DCREACDE-F           PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-DCREACDE-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-DDESTOCKFOUR-F       PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-DDESTOCKFOUR-F       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-DLIVFOUR-F           PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-DLIVFOUR-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-DDISPO-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-DDISPO-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-WENVOIEDI-F          PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-WENVOIEDI-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-DDELIV-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-DDELIV-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-DMODIF-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-DMODIF-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-NSOCDEPLIV-F         PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-NSOCDEPLIV-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-NLIEUDEPLIV-F        PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-NLIEUDEPLIV-F        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-DANNUL-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-DANNUL-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-DRECEPT-F            PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-DRECEPT-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-DARCHIVAGE-F         PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-DARCHIVAGE-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-DSYST-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-DSYST-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-DBASCUZAUTO-F        PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-DBASCUZAUTO-F        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-LCOMMENT-F           PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-LCOMMENT-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-NMUTATION-F          PIC S9(4) COMP.                         
      *--                                                                       
           10 GF22-NMUTATION-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF22-IDUSER-F             PIC S9(4) COMP.                         
      *                                                                         
      *--                                                                       
           10 GF22-IDUSER-F             PIC S9(4) COMP-5.                       
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
