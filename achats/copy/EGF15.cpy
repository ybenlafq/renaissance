      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF15   EGF15                                              00000020
      ***************************************************************** 00000030
       01   EGF15I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCDEI    PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCDEKL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLCDEKL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLCDEKF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLCDEKI   PIC X(18).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFCDEKL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MFCDEKL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFCDEKF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MFCDEKI   PIC X.                                          00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEK1L  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNCDEK1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCDEK1F  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNCDEK1I  PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEK2L  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNCDEK2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCDEK2F  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNCDEK2I  PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEK3L  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNCDEK3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCDEK3F  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNCDEK3I  PIC X.                                          00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEK4L  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNCDEK4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCDEK4F  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNCDEK4I  PIC X(2).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPCOL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCTYPCOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTYPCOF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCTYPCOI  PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTRL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNENTRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNENTRF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNENTRI   PIC X(6).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFOURNL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNFOURNF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNFOURNI  PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFOURNL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLFOURNF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLFOURNI  PIC X(20).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATSAIL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MDATSAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATSAIF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDATSAII  PIC X(8).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCINTERL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCINTERF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCINTERI  PIC X(5).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLINTERL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLINTERF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLINTERI  PIC X(20).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATVALL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MDATVALL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATVALF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MDATVALI  PIC X(8).                                       00000810
           02 M117I OCCURS   10 TIMES .                                 00000820
      * NUMERO ITEM                                                     00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNITEML      COMP PIC S9(4).                            00000840
      *--                                                                       
             03 MNITEML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNITEMF      PIC X.                                     00000850
             03 FILLER  PIC X(4).                                       00000860
             03 MNITEMI      PIC X(3).                                  00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000880
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000890
             03 FILLER  PIC X(4).                                       00000900
             03 MNSOCI  PIC X(3).                                       00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000920
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000930
             03 FILLER  PIC X(4).                                       00000940
             03 MNLIEUI      PIC X(3).                                  00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000960
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000970
             03 FILLER  PIC X(4).                                       00000980
             03 MNCODICI     PIC X(7).                                  00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00001000
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00001010
             03 FILLER  PIC X(4).                                       00001020
             03 MCFAMI  PIC X(5).                                       00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00001040
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00001050
             03 FILLER  PIC X(4).                                       00001060
             03 MCMARQI      PIC X(5).                                  00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00001080
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00001090
             03 FILLER  PIC X(4).                                       00001100
             03 MLREFI  PIC X(20).                                      00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTECOML     COMP PIC S9(4).                            00001120
      *--                                                                       
             03 MQTECOML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTECOMF     PIC X.                                     00001130
             03 FILLER  PIC X(4).                                       00001140
             03 MQTECOMI     PIC X(5).                                  00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTERECL     COMP PIC S9(4).                            00001160
      *--                                                                       
             03 MQTERECL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTERECF     PIC X.                                     00001170
             03 FILLER  PIC X(4).                                       00001180
             03 MQTERECI     PIC X(5).                                  00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTESOLL     COMP PIC S9(4).                            00001200
      *--                                                                       
             03 MQTESOLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTESOLF     PIC X.                                     00001210
             03 FILLER  PIC X(4).                                       00001220
             03 MQTESOLI     PIC X(5).                                  00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001240
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001250
           02 FILLER    PIC X(4).                                       00001260
           02 MZONCMDI  PIC X(15).                                      00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001280
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001290
           02 FILLER    PIC X(4).                                       00001300
           02 MLIBERRI  PIC X(58).                                      00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001320
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001330
           02 FILLER    PIC X(4).                                       00001340
           02 MCODTRAI  PIC X(4).                                       00001350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001360
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001370
           02 FILLER    PIC X(4).                                       00001380
           02 MCICSI    PIC X(5).                                       00001390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001400
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001410
           02 FILLER    PIC X(4).                                       00001420
           02 MNETNAMI  PIC X(8).                                       00001430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001440
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001450
           02 FILLER    PIC X(4).                                       00001460
           02 MSCREENI  PIC X(4).                                       00001470
      ***************************************************************** 00001480
      * SDF: EGF15   EGF15                                              00001490
      ***************************************************************** 00001500
       01   EGF15O REDEFINES EGF15I.                                    00001510
           02 FILLER    PIC X(12).                                      00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MDATJOUA  PIC X.                                          00001540
           02 MDATJOUC  PIC X.                                          00001550
           02 MDATJOUP  PIC X.                                          00001560
           02 MDATJOUH  PIC X.                                          00001570
           02 MDATJOUV  PIC X.                                          00001580
           02 MDATJOUO  PIC X(10).                                      00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MTIMJOUA  PIC X.                                          00001610
           02 MTIMJOUC  PIC X.                                          00001620
           02 MTIMJOUP  PIC X.                                          00001630
           02 MTIMJOUH  PIC X.                                          00001640
           02 MTIMJOUV  PIC X.                                          00001650
           02 MTIMJOUO  PIC X(5).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MWPAGEA   PIC X.                                          00001680
           02 MWPAGEC   PIC X.                                          00001690
           02 MWPAGEP   PIC X.                                          00001700
           02 MWPAGEH   PIC X.                                          00001710
           02 MWPAGEV   PIC X.                                          00001720
           02 MWPAGEO   PIC X(3).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MWFONCA   PIC X.                                          00001750
           02 MWFONCC   PIC X.                                          00001760
           02 MWFONCP   PIC X.                                          00001770
           02 MWFONCH   PIC X.                                          00001780
           02 MWFONCV   PIC X.                                          00001790
           02 MWFONCO   PIC X(3).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MNCDEA    PIC X.                                          00001820
           02 MNCDEC    PIC X.                                          00001830
           02 MNCDEP    PIC X.                                          00001840
           02 MNCDEH    PIC X.                                          00001850
           02 MNCDEV    PIC X.                                          00001860
           02 MNCDEO    PIC X(7).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MLCDEKA   PIC X.                                          00001890
           02 MLCDEKC   PIC X.                                          00001900
           02 MLCDEKP   PIC X.                                          00001910
           02 MLCDEKH   PIC X.                                          00001920
           02 MLCDEKV   PIC X.                                          00001930
           02 MLCDEKO   PIC X(18).                                      00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MFCDEKA   PIC X.                                          00001960
           02 MFCDEKC   PIC X.                                          00001970
           02 MFCDEKP   PIC X.                                          00001980
           02 MFCDEKH   PIC X.                                          00001990
           02 MFCDEKV   PIC X.                                          00002000
           02 MFCDEKO   PIC X.                                          00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MNCDEK1A  PIC X.                                          00002030
           02 MNCDEK1C  PIC X.                                          00002040
           02 MNCDEK1P  PIC X.                                          00002050
           02 MNCDEK1H  PIC X.                                          00002060
           02 MNCDEK1V  PIC X.                                          00002070
           02 MNCDEK1O  PIC X(3).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MNCDEK2A  PIC X.                                          00002100
           02 MNCDEK2C  PIC X.                                          00002110
           02 MNCDEK2P  PIC X.                                          00002120
           02 MNCDEK2H  PIC X.                                          00002130
           02 MNCDEK2V  PIC X.                                          00002140
           02 MNCDEK2O  PIC X(5).                                       00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MNCDEK3A  PIC X.                                          00002170
           02 MNCDEK3C  PIC X.                                          00002180
           02 MNCDEK3P  PIC X.                                          00002190
           02 MNCDEK3H  PIC X.                                          00002200
           02 MNCDEK3V  PIC X.                                          00002210
           02 MNCDEK3O  PIC X.                                          00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MNCDEK4A  PIC X.                                          00002240
           02 MNCDEK4C  PIC X.                                          00002250
           02 MNCDEK4P  PIC X.                                          00002260
           02 MNCDEK4H  PIC X.                                          00002270
           02 MNCDEK4V  PIC X.                                          00002280
           02 MNCDEK4O  PIC X(2).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MCTYPCOA  PIC X.                                          00002310
           02 MCTYPCOC  PIC X.                                          00002320
           02 MCTYPCOP  PIC X.                                          00002330
           02 MCTYPCOH  PIC X.                                          00002340
           02 MCTYPCOV  PIC X.                                          00002350
           02 MCTYPCOO  PIC X(5).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MNENTRA   PIC X.                                          00002380
           02 MNENTRC   PIC X.                                          00002390
           02 MNENTRP   PIC X.                                          00002400
           02 MNENTRH   PIC X.                                          00002410
           02 MNENTRV   PIC X.                                          00002420
           02 MNENTRO   PIC X(6).                                       00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MNFOURNA  PIC X.                                          00002450
           02 MNFOURNC  PIC X.                                          00002460
           02 MNFOURNP  PIC X.                                          00002470
           02 MNFOURNH  PIC X.                                          00002480
           02 MNFOURNV  PIC X.                                          00002490
           02 MNFOURNO  PIC X(5).                                       00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MLFOURNA  PIC X.                                          00002520
           02 MLFOURNC  PIC X.                                          00002530
           02 MLFOURNP  PIC X.                                          00002540
           02 MLFOURNH  PIC X.                                          00002550
           02 MLFOURNV  PIC X.                                          00002560
           02 MLFOURNO  PIC X(20).                                      00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MDATSAIA  PIC X.                                          00002590
           02 MDATSAIC  PIC X.                                          00002600
           02 MDATSAIP  PIC X.                                          00002610
           02 MDATSAIH  PIC X.                                          00002620
           02 MDATSAIV  PIC X.                                          00002630
           02 MDATSAIO  PIC X(8).                                       00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MCINTERA  PIC X.                                          00002660
           02 MCINTERC  PIC X.                                          00002670
           02 MCINTERP  PIC X.                                          00002680
           02 MCINTERH  PIC X.                                          00002690
           02 MCINTERV  PIC X.                                          00002700
           02 MCINTERO  PIC X(5).                                       00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MLINTERA  PIC X.                                          00002730
           02 MLINTERC  PIC X.                                          00002740
           02 MLINTERP  PIC X.                                          00002750
           02 MLINTERH  PIC X.                                          00002760
           02 MLINTERV  PIC X.                                          00002770
           02 MLINTERO  PIC X(20).                                      00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MDATVALA  PIC X.                                          00002800
           02 MDATVALC  PIC X.                                          00002810
           02 MDATVALP  PIC X.                                          00002820
           02 MDATVALH  PIC X.                                          00002830
           02 MDATVALV  PIC X.                                          00002840
           02 MDATVALO  PIC X(8).                                       00002850
           02 M117O OCCURS   10 TIMES .                                 00002860
      * NUMERO ITEM                                                     00002870
             03 FILLER       PIC X(2).                                  00002880
             03 MNITEMA      PIC X.                                     00002890
             03 MNITEMC PIC X.                                          00002900
             03 MNITEMP PIC X.                                          00002910
             03 MNITEMH PIC X.                                          00002920
             03 MNITEMV PIC X.                                          00002930
             03 MNITEMO      PIC X(3).                                  00002940
             03 FILLER       PIC X(2).                                  00002950
             03 MNSOCA  PIC X.                                          00002960
             03 MNSOCC  PIC X.                                          00002970
             03 MNSOCP  PIC X.                                          00002980
             03 MNSOCH  PIC X.                                          00002990
             03 MNSOCV  PIC X.                                          00003000
             03 MNSOCO  PIC X(3).                                       00003010
             03 FILLER       PIC X(2).                                  00003020
             03 MNLIEUA      PIC X.                                     00003030
             03 MNLIEUC PIC X.                                          00003040
             03 MNLIEUP PIC X.                                          00003050
             03 MNLIEUH PIC X.                                          00003060
             03 MNLIEUV PIC X.                                          00003070
             03 MNLIEUO      PIC X(3).                                  00003080
             03 FILLER       PIC X(2).                                  00003090
             03 MNCODICA     PIC X.                                     00003100
             03 MNCODICC     PIC X.                                     00003110
             03 MNCODICP     PIC X.                                     00003120
             03 MNCODICH     PIC X.                                     00003130
             03 MNCODICV     PIC X.                                     00003140
             03 MNCODICO     PIC X(7).                                  00003150
             03 FILLER       PIC X(2).                                  00003160
             03 MCFAMA  PIC X.                                          00003170
             03 MCFAMC  PIC X.                                          00003180
             03 MCFAMP  PIC X.                                          00003190
             03 MCFAMH  PIC X.                                          00003200
             03 MCFAMV  PIC X.                                          00003210
             03 MCFAMO  PIC X(5).                                       00003220
             03 FILLER       PIC X(2).                                  00003230
             03 MCMARQA      PIC X.                                     00003240
             03 MCMARQC PIC X.                                          00003250
             03 MCMARQP PIC X.                                          00003260
             03 MCMARQH PIC X.                                          00003270
             03 MCMARQV PIC X.                                          00003280
             03 MCMARQO      PIC X(5).                                  00003290
             03 FILLER       PIC X(2).                                  00003300
             03 MLREFA  PIC X.                                          00003310
             03 MLREFC  PIC X.                                          00003320
             03 MLREFP  PIC X.                                          00003330
             03 MLREFH  PIC X.                                          00003340
             03 MLREFV  PIC X.                                          00003350
             03 MLREFO  PIC X(20).                                      00003360
             03 FILLER       PIC X(2).                                  00003370
             03 MQTECOMA     PIC X.                                     00003380
             03 MQTECOMC     PIC X.                                     00003390
             03 MQTECOMP     PIC X.                                     00003400
             03 MQTECOMH     PIC X.                                     00003410
             03 MQTECOMV     PIC X.                                     00003420
             03 MQTECOMO     PIC X(5).                                  00003430
             03 FILLER       PIC X(2).                                  00003440
             03 MQTERECA     PIC X.                                     00003450
             03 MQTERECC     PIC X.                                     00003460
             03 MQTERECP     PIC X.                                     00003470
             03 MQTERECH     PIC X.                                     00003480
             03 MQTERECV     PIC X.                                     00003490
             03 MQTERECO     PIC X(5).                                  00003500
             03 FILLER       PIC X(2).                                  00003510
             03 MQTESOLA     PIC X.                                     00003520
             03 MQTESOLC     PIC X.                                     00003530
             03 MQTESOLP     PIC X.                                     00003540
             03 MQTESOLH     PIC X.                                     00003550
             03 MQTESOLV     PIC X.                                     00003560
             03 MQTESOLO     PIC X(5).                                  00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MZONCMDA  PIC X.                                          00003590
           02 MZONCMDC  PIC X.                                          00003600
           02 MZONCMDP  PIC X.                                          00003610
           02 MZONCMDH  PIC X.                                          00003620
           02 MZONCMDV  PIC X.                                          00003630
           02 MZONCMDO  PIC X(15).                                      00003640
           02 FILLER    PIC X(2).                                       00003650
           02 MLIBERRA  PIC X.                                          00003660
           02 MLIBERRC  PIC X.                                          00003670
           02 MLIBERRP  PIC X.                                          00003680
           02 MLIBERRH  PIC X.                                          00003690
           02 MLIBERRV  PIC X.                                          00003700
           02 MLIBERRO  PIC X(58).                                      00003710
           02 FILLER    PIC X(2).                                       00003720
           02 MCODTRAA  PIC X.                                          00003730
           02 MCODTRAC  PIC X.                                          00003740
           02 MCODTRAP  PIC X.                                          00003750
           02 MCODTRAH  PIC X.                                          00003760
           02 MCODTRAV  PIC X.                                          00003770
           02 MCODTRAO  PIC X(4).                                       00003780
           02 FILLER    PIC X(2).                                       00003790
           02 MCICSA    PIC X.                                          00003800
           02 MCICSC    PIC X.                                          00003810
           02 MCICSP    PIC X.                                          00003820
           02 MCICSH    PIC X.                                          00003830
           02 MCICSV    PIC X.                                          00003840
           02 MCICSO    PIC X(5).                                       00003850
           02 FILLER    PIC X(2).                                       00003860
           02 MNETNAMA  PIC X.                                          00003870
           02 MNETNAMC  PIC X.                                          00003880
           02 MNETNAMP  PIC X.                                          00003890
           02 MNETNAMH  PIC X.                                          00003900
           02 MNETNAMV  PIC X.                                          00003910
           02 MNETNAMO  PIC X(8).                                       00003920
           02 FILLER    PIC X(2).                                       00003930
           02 MSCREENA  PIC X.                                          00003940
           02 MSCREENC  PIC X.                                          00003950
           02 MSCREENP  PIC X.                                          00003960
           02 MSCREENH  PIC X.                                          00003970
           02 MSCREENV  PIC X.                                          00003980
           02 MSCREENO  PIC X(4).                                       00003990
                                                                                
