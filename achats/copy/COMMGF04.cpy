      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGF04                    TR: GF04  *    00020000
      *        VALIDATION DES COMMANDES EN INSTANCE                *    00030001
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 7421  00050000
      *                                                                 00060000
          02 COMM-GF04-APPLI REDEFINES COMM-GF00-APPLI.                 00070000
      *------------------------------ NUMERO DE PAGE                    00100000
             03 NUMERO-DE-PAGE PIC 9(3).                                00100100
      *------------------------------ SAVE CODIC                        00100403
             03 SAVE-NCODIC     PIC X(7).                               00100508
      *------------------------------ SAVE SOCIETE                      00100607
             03 SAVE-SOC       PIC X(3).                                00100707
      *------------------------------ SAVE NLIEU                        00100807
             03 SAVE-NLIEU     PIC X(3).                                00100907
      *------------------------------ SAVE NCDECLIENT                   00101007
             03 SAVE-NCDE      PIC X(7).                                00101107
      *------------------------------ SAVE DATE                         00101207
             03 SAVE-DATE      PIC X(8).                                00101307
      *------------------------------ SAVE CODIC                        00101404
             03 NUM-SEQ        PIC 9(7).                                00102005
      *------------------------------ NUMERO DE PAGE MAXIMUM            00102006
             03 PAGE-MAX       PIC 9(3).                                00102007
      *------------------------------ SAVE SOCIETE LIVRAISON            00262100
             03 SAVE-NSOCLIVR  PIC X(3).                                00262200
      *------------------------------ SAVE DEPOT LIVRAISON              00262300
             03 SAVE-NDEPOT    PIC X(3).                                00262400
      *------------------------------ TABLEAU DES GROUPES AUTORISES     00262501
             03 COMM-GF04-CGROUPS.                                      00262502
                  05 COMM-GF04-CGROUP    PIC X(5) OCCURS 20.            00262503
E0592 *------------------------------ NOMBRE DE COMMANDE EN INSTANCE    00262300
E0592        03 COMM-GF04-NBMAJ-I        PIC 9(5) COMP-3.               00262400
YS1645       03 COMM-GF04-DATE3          PIC X(10).                     00262400
      *------------------------------ FILLER                            00262600
CD    *      03 COMM-GF04-FILLER         PIC X(7274).                   00270007
      ***************************************************************** 00280000
                                                                                
