      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      * COMMAREA FEAD                                                           
       01 Z-COMMAREA-MAI53.                                                     
           05 COMM-MAI53-ENTREE.                                                
               10 COMM-MAI53-CODLANG PIC X(2).                                  
               10 COMM-MAI53-CODPIC PIC X(2).                                   
               10 COMM-MAI53-CFONC PIC X(4).                                    
               10 COMM-MAI53-TSIDENT PIC X(8).                                  
               10 COMM-MAI53-TSENR PIC S9(5).                                   
               10 COMM-MAI53-NCODIC PIC X(7).                                   
               10 COMM-MAI53-CLASS PIC S9(3).                                   
           05 COMM-MAI53-SORTIE.                                                
               10 COMM-MAI53-CRET PIC X(4).                                     
               10 COMM-MAI53-LLIBRE PIC X(80).                                  
                                                                                
