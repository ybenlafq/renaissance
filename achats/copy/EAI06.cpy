      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAI06   EAI06                                              00000020
      ***************************************************************** 00000030
       01   EAI06I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MFONCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMEROL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNUMEROL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUMEROF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNUMEROI  PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOML     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNOML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNOMF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNOMI     PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRES1L  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MADRES1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MADRES1F  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MADRES1I  PIC X(32).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRES2L  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MADRES2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MADRES2F  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MADRES2I  PIC X(32).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRES3L  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MADRES3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MADRES3F  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MADRES3I  PIC X(32).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRES4L  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MADRES4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MADRES4F  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MADRES4I  PIC X(32).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRES5L  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MADRES5L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MADRES5F  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MADRES5I  PIC X(32).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODPOSL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCODPOSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODPOSF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCODPOSI  PIC X(8).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVILLEL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MVILLEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MVILLEF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MVILLEI   PIC X(23).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRES6L  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MADRES6L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MADRES6F  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MADRES6I  PIC X(32).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELEPHL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MTELEPHL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELEPHF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MTELEPHI  PIC X(15).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELEXL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MTELEXL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTELEXF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MTELEXI   PIC X(15).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MZONCMDI  PIC X(15).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLIBERRI  PIC X(56).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCODTRAI  PIC X(4).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCICSI    PIC X(5).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MNETNAMI  PIC X(8).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MSCREENI  PIC X(4).                                       00000890
      ***************************************************************** 00000900
      * SDF: EAI06   EAI06                                              00000910
      ***************************************************************** 00000920
       01   EAI06O REDEFINES EAI06I.                                    00000930
           02 FILLER    PIC X(12).                                      00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MDATJOUA  PIC X.                                          00000960
           02 MDATJOUC  PIC X.                                          00000970
           02 MDATJOUP  PIC X.                                          00000980
           02 MDATJOUH  PIC X.                                          00000990
           02 MDATJOUV  PIC X.                                          00001000
           02 MDATJOUO  PIC X(10).                                      00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MTIMJOUA  PIC X.                                          00001030
           02 MTIMJOUC  PIC X.                                          00001040
           02 MTIMJOUP  PIC X.                                          00001050
           02 MTIMJOUH  PIC X.                                          00001060
           02 MTIMJOUV  PIC X.                                          00001070
           02 MTIMJOUO  PIC X(5).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MFONCA    PIC X.                                          00001100
           02 MFONCC    PIC X.                                          00001110
           02 MFONCP    PIC X.                                          00001120
           02 MFONCH    PIC X.                                          00001130
           02 MFONCV    PIC X.                                          00001140
           02 MFONCO    PIC X(3).                                       00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MNUMEROA  PIC X.                                          00001170
           02 MNUMEROC  PIC X.                                          00001180
           02 MNUMEROP  PIC X.                                          00001190
           02 MNUMEROH  PIC X.                                          00001200
           02 MNUMEROV  PIC X.                                          00001210
           02 MNUMEROO  PIC X(5).                                       00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MNOMA     PIC X.                                          00001240
           02 MNOMC     PIC X.                                          00001250
           02 MNOMP     PIC X.                                          00001260
           02 MNOMH     PIC X.                                          00001270
           02 MNOMV     PIC X.                                          00001280
           02 MNOMO     PIC X(20).                                      00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MADRES1A  PIC X.                                          00001310
           02 MADRES1C  PIC X.                                          00001320
           02 MADRES1P  PIC X.                                          00001330
           02 MADRES1H  PIC X.                                          00001340
           02 MADRES1V  PIC X.                                          00001350
           02 MADRES1O  PIC X(32).                                      00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MADRES2A  PIC X.                                          00001380
           02 MADRES2C  PIC X.                                          00001390
           02 MADRES2P  PIC X.                                          00001400
           02 MADRES2H  PIC X.                                          00001410
           02 MADRES2V  PIC X.                                          00001420
           02 MADRES2O  PIC X(32).                                      00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MADRES3A  PIC X.                                          00001450
           02 MADRES3C  PIC X.                                          00001460
           02 MADRES3P  PIC X.                                          00001470
           02 MADRES3H  PIC X.                                          00001480
           02 MADRES3V  PIC X.                                          00001490
           02 MADRES3O  PIC X(32).                                      00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MADRES4A  PIC X.                                          00001520
           02 MADRES4C  PIC X.                                          00001530
           02 MADRES4P  PIC X.                                          00001540
           02 MADRES4H  PIC X.                                          00001550
           02 MADRES4V  PIC X.                                          00001560
           02 MADRES4O  PIC X(32).                                      00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MADRES5A  PIC X.                                          00001590
           02 MADRES5C  PIC X.                                          00001600
           02 MADRES5P  PIC X.                                          00001610
           02 MADRES5H  PIC X.                                          00001620
           02 MADRES5V  PIC X.                                          00001630
           02 MADRES5O  PIC X(32).                                      00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MCODPOSA  PIC X.                                          00001660
           02 MCODPOSC  PIC X.                                          00001670
           02 MCODPOSP  PIC X.                                          00001680
           02 MCODPOSH  PIC X.                                          00001690
           02 MCODPOSV  PIC X.                                          00001700
           02 MCODPOSO  PIC X(8).                                       00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MVILLEA   PIC X.                                          00001730
           02 MVILLEC   PIC X.                                          00001740
           02 MVILLEP   PIC X.                                          00001750
           02 MVILLEH   PIC X.                                          00001760
           02 MVILLEV   PIC X.                                          00001770
           02 MVILLEO   PIC X(23).                                      00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MADRES6A  PIC X.                                          00001800
           02 MADRES6C  PIC X.                                          00001810
           02 MADRES6P  PIC X.                                          00001820
           02 MADRES6H  PIC X.                                          00001830
           02 MADRES6V  PIC X.                                          00001840
           02 MADRES6O  PIC X(32).                                      00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MTELEPHA  PIC X.                                          00001870
           02 MTELEPHC  PIC X.                                          00001880
           02 MTELEPHP  PIC X.                                          00001890
           02 MTELEPHH  PIC X.                                          00001900
           02 MTELEPHV  PIC X.                                          00001910
           02 MTELEPHO  PIC X(15).                                      00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MTELEXA   PIC X.                                          00001940
           02 MTELEXC   PIC X.                                          00001950
           02 MTELEXP   PIC X.                                          00001960
           02 MTELEXH   PIC X.                                          00001970
           02 MTELEXV   PIC X.                                          00001980
           02 MTELEXO   PIC X(15).                                      00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MZONCMDA  PIC X.                                          00002010
           02 MZONCMDC  PIC X.                                          00002020
           02 MZONCMDP  PIC X.                                          00002030
           02 MZONCMDH  PIC X.                                          00002040
           02 MZONCMDV  PIC X.                                          00002050
           02 MZONCMDO  PIC X(15).                                      00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MLIBERRA  PIC X.                                          00002080
           02 MLIBERRC  PIC X.                                          00002090
           02 MLIBERRP  PIC X.                                          00002100
           02 MLIBERRH  PIC X.                                          00002110
           02 MLIBERRV  PIC X.                                          00002120
           02 MLIBERRO  PIC X(56).                                      00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MCODTRAA  PIC X.                                          00002150
           02 MCODTRAC  PIC X.                                          00002160
           02 MCODTRAP  PIC X.                                          00002170
           02 MCODTRAH  PIC X.                                          00002180
           02 MCODTRAV  PIC X.                                          00002190
           02 MCODTRAO  PIC X(4).                                       00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MCICSA    PIC X.                                          00002220
           02 MCICSC    PIC X.                                          00002230
           02 MCICSP    PIC X.                                          00002240
           02 MCICSH    PIC X.                                          00002250
           02 MCICSV    PIC X.                                          00002260
           02 MCICSO    PIC X(5).                                       00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MNETNAMA  PIC X.                                          00002290
           02 MNETNAMC  PIC X.                                          00002300
           02 MNETNAMP  PIC X.                                          00002310
           02 MNETNAMH  PIC X.                                          00002320
           02 MNETNAMV  PIC X.                                          00002330
           02 MNETNAMO  PIC X(8).                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MSCREENA  PIC X.                                          00002360
           02 MSCREENC  PIC X.                                          00002370
           02 MSCREENP  PIC X.                                          00002380
           02 MSCREENH  PIC X.                                          00002390
           02 MSCREENV  PIC X.                                          00002400
           02 MSCREENO  PIC X(4).                                       00002410
                                                                                
