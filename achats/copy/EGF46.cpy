      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF46   EGF46                                              00000020
      ***************************************************************** 00000030
       01   EGF46I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFOURNL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNFOURNF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNFOURNI  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFOURNL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLFOURNF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLFOURNI  PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTLOL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCINTLOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCINTLOF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCINTLOI  PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLINTERL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLINTERF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLINTERI  PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODIC0L      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNCODIC0L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODIC0F      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNCODIC0I      PIC X(7).                                  00000410
           02 M100I OCCURS   13 TIMES .                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCINTERL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MCINTERL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCINTERF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCINTERI     PIC X(5).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNLIEUI      PIC X(6).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTRL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNENTRL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNENTRF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNENTRI      PIC X(6).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNCODICI     PIC X(7).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCFAMI  PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MLREFI  PIC X(20).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MQTEI   PIC X(6).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEL  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MDATEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDATEF  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MDATEI  PIC X(8).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCMARQI      PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MZONCMDI  PIC X(15).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(58).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCICSI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNETNAMI  PIC X(8).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MSCREENI  PIC X(4).                                       00001020
      ***************************************************************** 00001030
      * SDF: EGF46   EGF46                                              00001040
      ***************************************************************** 00001050
       01   EGF46O REDEFINES EGF46I.                                    00001060
           02 FILLER    PIC X(12).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MDATJOUA  PIC X.                                          00001090
           02 MDATJOUC  PIC X.                                          00001100
           02 MDATJOUP  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUV  PIC X.                                          00001130
           02 MDATJOUO  PIC X(10).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MWPAGEA   PIC X.                                          00001230
           02 MWPAGEC   PIC X.                                          00001240
           02 MWPAGEP   PIC X.                                          00001250
           02 MWPAGEH   PIC X.                                          00001260
           02 MWPAGEV   PIC X.                                          00001270
           02 MWPAGEO   PIC X(3).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MWFONCA   PIC X.                                          00001300
           02 MWFONCC   PIC X.                                          00001310
           02 MWFONCP   PIC X.                                          00001320
           02 MWFONCH   PIC X.                                          00001330
           02 MWFONCV   PIC X.                                          00001340
           02 MWFONCO   PIC X(3).                                       00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MNFOURNA  PIC X.                                          00001370
           02 MNFOURNC  PIC X.                                          00001380
           02 MNFOURNP  PIC X.                                          00001390
           02 MNFOURNH  PIC X.                                          00001400
           02 MNFOURNV  PIC X.                                          00001410
           02 MNFOURNO  PIC X(5).                                       00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MLFOURNA  PIC X.                                          00001440
           02 MLFOURNC  PIC X.                                          00001450
           02 MLFOURNP  PIC X.                                          00001460
           02 MLFOURNH  PIC X.                                          00001470
           02 MLFOURNV  PIC X.                                          00001480
           02 MLFOURNO  PIC X(20).                                      00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MCINTLOA  PIC X.                                          00001510
           02 MCINTLOC  PIC X.                                          00001520
           02 MCINTLOP  PIC X.                                          00001530
           02 MCINTLOH  PIC X.                                          00001540
           02 MCINTLOV  PIC X.                                          00001550
           02 MCINTLOO  PIC X(5).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MLINTERA  PIC X.                                          00001580
           02 MLINTERC  PIC X.                                          00001590
           02 MLINTERP  PIC X.                                          00001600
           02 MLINTERH  PIC X.                                          00001610
           02 MLINTERV  PIC X.                                          00001620
           02 MLINTERO  PIC X(20).                                      00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MNCODIC0A      PIC X.                                     00001650
           02 MNCODIC0C PIC X.                                          00001660
           02 MNCODIC0P PIC X.                                          00001670
           02 MNCODIC0H PIC X.                                          00001680
           02 MNCODIC0V PIC X.                                          00001690
           02 MNCODIC0O      PIC X(7).                                  00001700
           02 M100O OCCURS   13 TIMES .                                 00001710
             03 FILLER       PIC X(2).                                  00001720
             03 MCINTERA     PIC X.                                     00001730
             03 MCINTERC     PIC X.                                     00001740
             03 MCINTERP     PIC X.                                     00001750
             03 MCINTERH     PIC X.                                     00001760
             03 MCINTERV     PIC X.                                     00001770
             03 MCINTERO     PIC X(5).                                  00001780
             03 FILLER       PIC X(2).                                  00001790
             03 MNLIEUA      PIC X.                                     00001800
             03 MNLIEUC PIC X.                                          00001810
             03 MNLIEUP PIC X.                                          00001820
             03 MNLIEUH PIC X.                                          00001830
             03 MNLIEUV PIC X.                                          00001840
             03 MNLIEUO      PIC X(6).                                  00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MNENTRA      PIC X.                                     00001870
             03 MNENTRC PIC X.                                          00001880
             03 MNENTRP PIC X.                                          00001890
             03 MNENTRH PIC X.                                          00001900
             03 MNENTRV PIC X.                                          00001910
             03 MNENTRO      PIC X(6).                                  00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MNCODICA     PIC X.                                     00001940
             03 MNCODICC     PIC X.                                     00001950
             03 MNCODICP     PIC X.                                     00001960
             03 MNCODICH     PIC X.                                     00001970
             03 MNCODICV     PIC X.                                     00001980
             03 MNCODICO     PIC X(7).                                  00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MCFAMA  PIC X.                                          00002010
             03 MCFAMC  PIC X.                                          00002020
             03 MCFAMP  PIC X.                                          00002030
             03 MCFAMH  PIC X.                                          00002040
             03 MCFAMV  PIC X.                                          00002050
             03 MCFAMO  PIC X(5).                                       00002060
             03 FILLER       PIC X(2).                                  00002070
             03 MLREFA  PIC X.                                          00002080
             03 MLREFC  PIC X.                                          00002090
             03 MLREFP  PIC X.                                          00002100
             03 MLREFH  PIC X.                                          00002110
             03 MLREFV  PIC X.                                          00002120
             03 MLREFO  PIC X(20).                                      00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MQTEA   PIC X.                                          00002150
             03 MQTEC   PIC X.                                          00002160
             03 MQTEP   PIC X.                                          00002170
             03 MQTEH   PIC X.                                          00002180
             03 MQTEV   PIC X.                                          00002190
             03 MQTEO   PIC X(6).                                       00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MDATEA  PIC X.                                          00002220
             03 MDATEC  PIC X.                                          00002230
             03 MDATEP  PIC X.                                          00002240
             03 MDATEH  PIC X.                                          00002250
             03 MDATEV  PIC X.                                          00002260
             03 MDATEO  PIC X(8).                                       00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MCMARQA      PIC X.                                     00002290
             03 MCMARQC PIC X.                                          00002300
             03 MCMARQP PIC X.                                          00002310
             03 MCMARQH PIC X.                                          00002320
             03 MCMARQV PIC X.                                          00002330
             03 MCMARQO      PIC X(5).                                  00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MZONCMDA  PIC X.                                          00002360
           02 MZONCMDC  PIC X.                                          00002370
           02 MZONCMDP  PIC X.                                          00002380
           02 MZONCMDH  PIC X.                                          00002390
           02 MZONCMDV  PIC X.                                          00002400
           02 MZONCMDO  PIC X(15).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLIBERRA  PIC X.                                          00002430
           02 MLIBERRC  PIC X.                                          00002440
           02 MLIBERRP  PIC X.                                          00002450
           02 MLIBERRH  PIC X.                                          00002460
           02 MLIBERRV  PIC X.                                          00002470
           02 MLIBERRO  PIC X(58).                                      00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCODTRAA  PIC X.                                          00002500
           02 MCODTRAC  PIC X.                                          00002510
           02 MCODTRAP  PIC X.                                          00002520
           02 MCODTRAH  PIC X.                                          00002530
           02 MCODTRAV  PIC X.                                          00002540
           02 MCODTRAO  PIC X(4).                                       00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MCICSA    PIC X.                                          00002570
           02 MCICSC    PIC X.                                          00002580
           02 MCICSP    PIC X.                                          00002590
           02 MCICSH    PIC X.                                          00002600
           02 MCICSV    PIC X.                                          00002610
           02 MCICSO    PIC X(5).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MNETNAMA  PIC X.                                          00002640
           02 MNETNAMC  PIC X.                                          00002650
           02 MNETNAMP  PIC X.                                          00002660
           02 MNETNAMH  PIC X.                                          00002670
           02 MNETNAMV  PIC X.                                          00002680
           02 MNETNAMO  PIC X(8).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MSCREENA  PIC X.                                          00002710
           02 MSCREENC  PIC X.                                          00002720
           02 MSCREENP  PIC X.                                          00002730
           02 MSCREENH  PIC X.                                          00002740
           02 MSCREENV  PIC X.                                          00002750
           02 MSCREENO  PIC X(4).                                       00002760
                                                                                
