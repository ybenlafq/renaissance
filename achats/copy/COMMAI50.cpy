      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      * COMMAREA SPECIFIQUE MODULE D'ENVOI DONN�ES DE NCG VERS JDA *            
      **************************************************************            
      *                                                                         
      *   PROGRAMME MAI50                                                       
      *                                                                         
E0008 ******************************************************************        
      * DSA057 07/08/03 SUPPORT EVOLUTION                                       
      *                 PHASE 2 : CALCUL ET ENVOI LEADTIME SYSTEMATIQUE         
      *                 NECESSITE CFAM ET NENTCDE REGULIER EN COM               
E0009 ******************************************************************        
      * DSA057 22/08/03 SUPPORT EVOLUTION                                       
      *                 FLAG WEB EXTENDED RANGE                                 
E0008 *01 COMM-MAI50-LONG-COMMAREA        PIC S9(4) COMP VALUE +751.            
E0009 *01 COMM-MAI50-LONG-COMMAREA PIC S9(4) COMP VALUE +649.                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
E0009 *01 COMM-MAI50-LONG-COMMAREA PIC S9(4) COMP VALUE +650.                   
      *--                                                                       
       01 COMM-MAI50-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +650.                 
      *}                                                                        
       01 COMM-MAI50-APPLI.                                                     
      *     ENTREE --------------------------------------------------587        
            02 COMM-MAI50-ENTREE.                                               
      *            ENTETE ---------------------------------------------2        
                   05 COMM-MAI50-ACTION   PIC X.                                
                      88 COMM-MAI50-CONTROLE  VALUE ' '.                        
                      88 COMM-MAI50-CONFIRMATION VALUE '1'.                     
                      88 COMM-MAI50-EUK         VALUE '2'.                      
                      88 COMM-MAI50-GI20        VALUE '3'.                      
                      88 COMM-MAI50-AI07        VALUE '4'.                      
                   05 COMM-MAI50-MESS     PIC X.                                
                      88 COMM-MAI50-CONFIRMATION-OK VALUE ' '.                  
                      88 COMM-MAI50-CONFIRMATION-NOK VALUE '1'.                 
      *            DONNEES GENERALES ---------------------------------31        
                   05 COMM-MAI50-DJOUR        PIC X(8).                         
                   05 COMM-MAI50-CFONC-FLAG   PIC X.                            
                      88 COMM-MAI50-CRE       VALUE ' '.                        
                      88 COMM-MAI50-MAJ       VALUE '1'.                        
                   05 COMM-MAI50-CFONC        PIC X(3).                         
                   05 COMM-MAI50-NSOCIETE     PIC X(3).                         
                   05 COMM-MAI50-NCODIC       PIC X(7).                         
                   05 COMM-MAI50-WTYPE        PIC 9(1).                         
                   05 COMM-MAI50-BATCHNUMBER  PIC X(8).                         
      *            TERMINAL -------------------------------------------4        
                   05 COMM-MAI50-NTERMID      PIC X(4).                         
      *            PARAMETRES D'AIGUILLAGE --------------------------180        
                   05 COMM-MAI50-INFO-PARAM.                                    
                      10 COMM-MAI50-PARAM   OCCURS 30.                          
                         15 COMM-MAI50-CTRANS PIC X(5).                         
      *                  15 COMM-MAI50-NOPT   PIC 9(2).                         
      *                  15 COMM-MAI50-NSOPT  PIC 9.                            
      *                  15 COMM-MAI50-WOBLIG PIC X.                            
                         15 COMM-MAI50-WPASS  PIC X.                            
      *            ECRANS EGA74 EGA51 EGA76 -------------------------226        
                   05 COMM-MAI50-OPTION1.                                       
                      10 COMM-MAI50-INFO.                                       
                         15 COMM-MAI50-CMARQ     PIC X(5).                      
E0008 *                  APELLATION INCORRECTE                                  
E0008 *                  15 COMM-MAI50-LREFO     PIC X(20).                     
E0008                    15 COMM-MAI50-LREFFOURN PIC X(20).                     
                         15 COMM-MAI50-NEAN      PIC X(13).                     
                         15 COMM-MAI50-CCOUL     PIC X(5).                      
E0008                    15 COMM-MAI50-CFAM      PIC X(5).                      
E0008                    15 COMM-MAI50-NENTCDER  PIC X(5).                      
                      10 COMM-MAI50-GA51.                                       
                         15 COMM-MAI50-CTAUXTVA  PIC X(5).                      
                         15 COMM-MAI50-DISTM     PIC X(1).                      
                         15 COMM-MAI50-CGARCONST PIC X(5).                      
                         15 COMM-MAI50-CCOULMARK PIC X(5).                      
                      10 COMM-MAI50-GA76.                                       
                         15 COMM-MAI50-IDEPT     PIC X(3).                      
                         15 COMM-MAI50-ISDEPT    PIC X(3).                      
                         15 COMM-MAI50-ICLAS     PIC X(3).                      
                         15 COMM-MAI50-ISCLAS    PIC X(3).                      
                         15 COMM-MAI50-IATRB4    PIC X(2).                      
                         15 COMM-MAI50-IATRB2    PIC X(2).                      
                         15 COMM-MAI50-ISTYPE    PIC X(2).                      
                         15 COMM-MAI50-I2TKTD    PIC X(60).                     
                         15 COMM-MAI50-I2ADVD    PIC X(60).                     
                         15 COMM-MAI50-I3MKDS    PIC X(15).                     
                         15 COMM-MAI50-IWARGC    PIC X(2).                      
                         15 COMM-MAI50-NBCOMPO   PIC 9(2).                      
      *            ECRAN EGA77 ---------------------------------------61        
                   05 COMM-MAI50-OPTION3.                                       
                      10 COMM-MAI50-IRPLCD       PIC X(1).                      
                      10 COMM-MAI50-IATRB3       PIC X(2).                      
                      10 COMM-MAI50-I3WKMX       PIC X(2).                      
                      10 COMM-MAI50-LCOMMENT     PIC X(50).                     
                      10 COMM-MAI50-CAPPRO       PIC X(5).                      
                      10 COMM-MAI50-WSENSAPPRO   PIC X(1).                      
E0009                 10 COMM-MAI50-WWER         PIC X(1) VALUE 'N'.            
      *            ECRAN EGA55 ----------------------------------------5        
                   05 COMM-MAI50-OPTION4.                                       
                      10 COMM-MAI50-CHEFPROD     PIC X(5).                      
E0008                 10 COMM-MAI50-QNDELAPP     PIC X(3).                      
      *            ECRANS EGA56 EGA78 --------------------------------78        
                   05 COMM-MAI50-OPTION5.                                       
                      10 COMM-MAI50-GA56.                                       
                         15 COMM-MAI50-CCOTEHOLD     PIC X(1).                  
                         15 COMM-MAI50-I3ABC         PIC X(1).                  
                         15 COMM-MAI50-CUNITRECEPT   PIC X(3).                  
                         15 COMM-MAI50-CUNITVTE      PIC X(3).                  
                         15 COMM-MAI50-WCROSSDOCK    PIC X(1).                  
                      10 COMM-MAI50-GA78.                                       
      *                  15 COMM-MAI50-LEMBALLAGE    PIC X(50).                 
                         15 COMM-MAI50-QCOLIRECEPT   PIC 9(5).                  
                         15 COMM-MAI50-QCOLIVTE      PIC 9(5).                  
                         15 COMM-MAI50-QPOIDS        PIC 9(7).                  
                         15 COMM-MAI50-QLARGEUR      PIC 9(3).                  
                         15 COMM-MAI50-QPROFONDEUR   PIC 9(3).                  
                         15 COMM-MAI50-QHAUTEUR      PIC 9(3).                  
                         15 COMM-MAI50-QPOIDSDE      PIC 9(7).                  
                         15 COMM-MAI50-QLARGDE       PIC 9(5).                  
                         15 COMM-MAI50-QPROFDE       PIC 9(5).                  
                         15 COMM-MAI50-QHAUTDE       PIC 9(5).                  
                         15 COMM-MAI50-QPRODBOX      PIC 9(4).                  
                         15 COMM-MAI50-QBOXCART      PIC 9(4).                  
                         15 COMM-MAI50-QCARTCOUCH    PIC 9(2).                  
                         15 COMM-MAI50-QCOUCHPAL     PIC 9(2).                  
                   05  COMM-MAI50-OPTION7.                                      
                        10 COMM-MAI50-PBF          PIC 9(7)V99.                 
      *      SORTIE --------------------------------------------------59        
             02 COMM-MAI50-SORTIE.                                              
                05 COMM-MAI50-MESSAGE.                                          
                   10 COMM-MAI50-CODRET        PIC X(1).                        
                      88 COMM-MAI50-OK  VALUE ' '.                              
                      88 COMM-MAI50-ERR VALUE '1'.                              
                      88 COMM-MAI50-JDA-NOK VALUE '2'.                          
                   10 COMM-MAI50-LIBERR        PIC X(58).                       
      *            10 COMM-MAI50-NSEQERR       PIC X(4).                        
                                                                                
