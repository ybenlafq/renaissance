      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGF20                    TR: GF20  *    00020000
      * SAISIE DES COMMANDES FOURNISSEUR ARTICLES-QUANTITES        *    00030000
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 7421  00050000
      *                                                                 00060000
          02 COMM-GF20-APPLI REDEFINES COMM-GF00-APPLI.                 00070000
      *                                                                 00080000
      * ZONES PROPRES AU PROGRAMME         TGF20    ------------- 312   00090000
      *                                                                 00100000
      *---------------------------ENTETE ECRAN                          00110000
             03 COMM-GF20-ZONE-MAP.                                     00111002
                05 COMM-GF20-CHEFPROD          PIC X(5).                00120002
                05 COMM-GF20-NSOCIETE          PIC X(3).                00120202
                05 COMM-GF20-NDEPOT            PIC X(3).                00120302
                05 COMM-GF20-LCHEFPROD         PIC X(20).               00120402
                05 COMM-GF20-TABENTCDE.                                 00120604
                   10 COMM-GF20-TAB               OCCURS 12.            00120704
                      15 COMM-GF20-NCODIC         PIC X(7).             00120804
                      15 COMM-GF20-ENTVALID       PIC X(1).             00120904
      *                                                                 00121004
      *------------------------------ PAGINATION                        00121104
             03 COMM-GF20-ZONE-PAGINATION.                              00121204
                05 COMM-GF20-NPAGE       PIC 9(3) COMP-3.               00121304
      *                                                                 00121404
      *------------------------------ GESTION TS                        00122002
             03 COMM-GF20-TS.                                           00123002
                05 CS-GF20-IDENT         PIC X(8).                      00124002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 CS-GF20-NB            PIC S9(4) COMP.                00125002
      *--                                                                       
                05 CS-GF20-NB            PIC S9(4) COMP-5.                      
      *}                                                                        
LA1505*------------------------------ GESTION PARAMETRES                00126005
LA1505       03 COMM-GF20-PARAM.                                        00127005
LA1505          05 COMM-GF20-CTRL-COLIS  PIC X(1).                      00128005
LA1505             88 COMM-GF20-CTRL-COLIS-OK     VALUE 'O'.            00129005
LA1505             88 COMM-GF20-CTRL-COLIS-KO     VALUE 'N'.            00130005
                                                                                
