      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF00   EGF00                                              00000020
      ***************************************************************** 00000030
       01   EGF00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(15).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MWFONCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFOURNL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNFOURNF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNFOURNI  PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCINTERL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCINTERF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCINTERI  PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE1L   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNCDE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE1F   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNCDE1I   PIC X(7).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE2L   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNCDE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE2F   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNCDE2I   PIC X(7).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIT1L    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MTIT1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTIT1F    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MTIT1I    PIC X(60).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIT2L    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MTIT2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTIT2F    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MTIT2I    PIC X(61).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE9L   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNCDE9L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE9F   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNCDE9I   PIC X(7).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIT3L    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MTIT3L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTIT3F    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MTIT3I    PIC X(61).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODIC0L      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MNCODIC0L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODIC0F      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNCODIC0I      PIC X(7).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIT4L    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MTIT4L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTIT4F    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MTIT4I    PIC X(19).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRIT1L   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCRIT1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRIT1F   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCRIT1I   PIC X(6).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNDEPOTI  PIC X(3).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRIT2L   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCRIT2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRIT2F   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCRIT2I   PIC X(8).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEL    COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MDATEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDATEF    PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MDATEI    PIC X(6).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRIT3L   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCRIT3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRIT3F   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCRIT3I   PIC X(9).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMODSTOCKL    COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MCMODSTOCKL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCMODSTOCKF    PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCMODSTOCKI    PIC X(5).                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRIT4L   COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCRIT4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCRIT4F   PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCRIT4I   PIC X(6).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCQUOTAL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MCQUOTAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCQUOTAF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MCQUOTAI  PIC X(5).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIT5L    COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MTIT5L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTIT5F    PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MTIT5I    PIC X(61).                                      00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MLIBERRI  PIC X(78).                                      00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MCODTRAI  PIC X(4).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MCICSI    PIC X(5).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MNETNAMI  PIC X(8).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MSCREENI  PIC X(4).                                       00001210
      ***************************************************************** 00001220
      * SDF: EGF00   EGF00                                              00001230
      ***************************************************************** 00001240
       01   EGF00O REDEFINES EGF00I.                                    00001250
           02 FILLER    PIC X(12).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MDATJOUA  PIC X.                                          00001280
           02 MDATJOUC  PIC X.                                          00001290
           02 MDATJOUP  PIC X.                                          00001300
           02 MDATJOUH  PIC X.                                          00001310
           02 MDATJOUV  PIC X.                                          00001320
           02 MDATJOUO  PIC X(10).                                      00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MTIMJOUA  PIC X.                                          00001350
           02 MTIMJOUC  PIC X.                                          00001360
           02 MTIMJOUP  PIC X.                                          00001370
           02 MTIMJOUH  PIC X.                                          00001380
           02 MTIMJOUV  PIC X.                                          00001390
           02 MTIMJOUO  PIC X(5).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MZONCMDA  PIC X.                                          00001420
           02 MZONCMDC  PIC X.                                          00001430
           02 MZONCMDP  PIC X.                                          00001440
           02 MZONCMDH  PIC X.                                          00001450
           02 MZONCMDV  PIC X.                                          00001460
           02 MZONCMDO  PIC X(15).                                      00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MNSOCA    PIC X.                                          00001490
           02 MNSOCC    PIC X.                                          00001500
           02 MNSOCP    PIC X.                                          00001510
           02 MNSOCH    PIC X.                                          00001520
           02 MNSOCV    PIC X.                                          00001530
           02 MNSOCO    PIC X(3).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MWFONCA   PIC X.                                          00001560
           02 MWFONCC   PIC X.                                          00001570
           02 MWFONCP   PIC X.                                          00001580
           02 MWFONCH   PIC X.                                          00001590
           02 MWFONCV   PIC X.                                          00001600
           02 MWFONCO   PIC X(3).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MNFOURNA  PIC X.                                          00001630
           02 MNFOURNC  PIC X.                                          00001640
           02 MNFOURNP  PIC X.                                          00001650
           02 MNFOURNH  PIC X.                                          00001660
           02 MNFOURNV  PIC X.                                          00001670
           02 MNFOURNO  PIC X(5).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MCINTERA  PIC X.                                          00001700
           02 MCINTERC  PIC X.                                          00001710
           02 MCINTERP  PIC X.                                          00001720
           02 MCINTERH  PIC X.                                          00001730
           02 MCINTERV  PIC X.                                          00001740
           02 MCINTERO  PIC X(5).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MNCDE1A   PIC X.                                          00001770
           02 MNCDE1C   PIC X.                                          00001780
           02 MNCDE1P   PIC X.                                          00001790
           02 MNCDE1H   PIC X.                                          00001800
           02 MNCDE1V   PIC X.                                          00001810
           02 MNCDE1O   PIC X(7).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MNCDE2A   PIC X.                                          00001840
           02 MNCDE2C   PIC X.                                          00001850
           02 MNCDE2P   PIC X.                                          00001860
           02 MNCDE2H   PIC X.                                          00001870
           02 MNCDE2V   PIC X.                                          00001880
           02 MNCDE2O   PIC X(7).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MTIT1A    PIC X.                                          00001910
           02 MTIT1C    PIC X.                                          00001920
           02 MTIT1P    PIC X.                                          00001930
           02 MTIT1H    PIC X.                                          00001940
           02 MTIT1V    PIC X.                                          00001950
           02 MTIT1O    PIC X(60).                                      00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MTIT2A    PIC X.                                          00001980
           02 MTIT2C    PIC X.                                          00001990
           02 MTIT2P    PIC X.                                          00002000
           02 MTIT2H    PIC X.                                          00002010
           02 MTIT2V    PIC X.                                          00002020
           02 MTIT2O    PIC X(61).                                      00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MNCDE9A   PIC X.                                          00002050
           02 MNCDE9C   PIC X.                                          00002060
           02 MNCDE9P   PIC X.                                          00002070
           02 MNCDE9H   PIC X.                                          00002080
           02 MNCDE9V   PIC X.                                          00002090
           02 MNCDE9O   PIC X(7).                                       00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MTIT3A    PIC X.                                          00002120
           02 MTIT3C    PIC X.                                          00002130
           02 MTIT3P    PIC X.                                          00002140
           02 MTIT3H    PIC X.                                          00002150
           02 MTIT3V    PIC X.                                          00002160
           02 MTIT3O    PIC X(61).                                      00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MNCODIC0A      PIC X.                                     00002190
           02 MNCODIC0C PIC X.                                          00002200
           02 MNCODIC0P PIC X.                                          00002210
           02 MNCODIC0H PIC X.                                          00002220
           02 MNCODIC0V PIC X.                                          00002230
           02 MNCODIC0O      PIC X(7).                                  00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MTIT4A    PIC X.                                          00002260
           02 MTIT4C    PIC X.                                          00002270
           02 MTIT4P    PIC X.                                          00002280
           02 MTIT4H    PIC X.                                          00002290
           02 MTIT4V    PIC X.                                          00002300
           02 MTIT4O    PIC X(19).                                      00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MCRIT1A   PIC X.                                          00002330
           02 MCRIT1C   PIC X.                                          00002340
           02 MCRIT1P   PIC X.                                          00002350
           02 MCRIT1H   PIC X.                                          00002360
           02 MCRIT1V   PIC X.                                          00002370
           02 MCRIT1O   PIC X(6).                                       00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MNDEPOTA  PIC X.                                          00002400
           02 MNDEPOTC  PIC X.                                          00002410
           02 MNDEPOTP  PIC X.                                          00002420
           02 MNDEPOTH  PIC X.                                          00002430
           02 MNDEPOTV  PIC X.                                          00002440
           02 MNDEPOTO  PIC X(3).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MCRIT2A   PIC X.                                          00002470
           02 MCRIT2C   PIC X.                                          00002480
           02 MCRIT2P   PIC X.                                          00002490
           02 MCRIT2H   PIC X.                                          00002500
           02 MCRIT2V   PIC X.                                          00002510
           02 MCRIT2O   PIC X(8).                                       00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MDATEA    PIC X.                                          00002540
           02 MDATEC    PIC X.                                          00002550
           02 MDATEP    PIC X.                                          00002560
           02 MDATEH    PIC X.                                          00002570
           02 MDATEV    PIC X.                                          00002580
           02 MDATEO    PIC X(6).                                       00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCRIT3A   PIC X.                                          00002610
           02 MCRIT3C   PIC X.                                          00002620
           02 MCRIT3P   PIC X.                                          00002630
           02 MCRIT3H   PIC X.                                          00002640
           02 MCRIT3V   PIC X.                                          00002650
           02 MCRIT3O   PIC X(9).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCMODSTOCKA    PIC X.                                     00002680
           02 MCMODSTOCKC    PIC X.                                     00002690
           02 MCMODSTOCKP    PIC X.                                     00002700
           02 MCMODSTOCKH    PIC X.                                     00002710
           02 MCMODSTOCKV    PIC X.                                     00002720
           02 MCMODSTOCKO    PIC X(5).                                  00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MCRIT4A   PIC X.                                          00002750
           02 MCRIT4C   PIC X.                                          00002760
           02 MCRIT4P   PIC X.                                          00002770
           02 MCRIT4H   PIC X.                                          00002780
           02 MCRIT4V   PIC X.                                          00002790
           02 MCRIT4O   PIC X(6).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MCQUOTAA  PIC X.                                          00002820
           02 MCQUOTAC  PIC X.                                          00002830
           02 MCQUOTAP  PIC X.                                          00002840
           02 MCQUOTAH  PIC X.                                          00002850
           02 MCQUOTAV  PIC X.                                          00002860
           02 MCQUOTAO  PIC X(5).                                       00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MTIT5A    PIC X.                                          00002890
           02 MTIT5C    PIC X.                                          00002900
           02 MTIT5P    PIC X.                                          00002910
           02 MTIT5H    PIC X.                                          00002920
           02 MTIT5V    PIC X.                                          00002930
           02 MTIT5O    PIC X(61).                                      00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MLIBERRA  PIC X.                                          00002960
           02 MLIBERRC  PIC X.                                          00002970
           02 MLIBERRP  PIC X.                                          00002980
           02 MLIBERRH  PIC X.                                          00002990
           02 MLIBERRV  PIC X.                                          00003000
           02 MLIBERRO  PIC X(78).                                      00003010
           02 FILLER    PIC X(2).                                       00003020
           02 MCODTRAA  PIC X.                                          00003030
           02 MCODTRAC  PIC X.                                          00003040
           02 MCODTRAP  PIC X.                                          00003050
           02 MCODTRAH  PIC X.                                          00003060
           02 MCODTRAV  PIC X.                                          00003070
           02 MCODTRAO  PIC X(4).                                       00003080
           02 FILLER    PIC X(2).                                       00003090
           02 MCICSA    PIC X.                                          00003100
           02 MCICSC    PIC X.                                          00003110
           02 MCICSP    PIC X.                                          00003120
           02 MCICSH    PIC X.                                          00003130
           02 MCICSV    PIC X.                                          00003140
           02 MCICSO    PIC X(5).                                       00003150
           02 FILLER    PIC X(2).                                       00003160
           02 MNETNAMA  PIC X.                                          00003170
           02 MNETNAMC  PIC X.                                          00003180
           02 MNETNAMP  PIC X.                                          00003190
           02 MNETNAMH  PIC X.                                          00003200
           02 MNETNAMV  PIC X.                                          00003210
           02 MNETNAMO  PIC X(8).                                       00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MSCREENA  PIC X.                                          00003240
           02 MSCREENC  PIC X.                                          00003250
           02 MSCREENP  PIC X.                                          00003260
           02 MSCREENH  PIC X.                                          00003270
           02 MSCREENV  PIC X.                                          00003280
           02 MSCREENO  PIC X(4).                                       00003290
                                                                                
