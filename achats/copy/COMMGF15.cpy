      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGF15                    TR: GF15  *    00020001
      * SAISIE DES COMMANDES FOURNISSEUR ARTICLES-QUANTITES        *    00030000
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 7421  00050001
      *                                                                 00060000
          02 COMM-GF15-APPLI REDEFINES COMM-GF00-APPLI.                 00070001
      *                                                                 00080000
      * ZONES COMMUNES PROGRAMMES          TGF14    ------------- 7421  00090001
      *                                    TGF15                        00100001
      *                                                                 00110000
             03 COMM-GF15-COMMUNE           PIC X(84).                  00120001
      *                                                                 00130000
      * ZONES PROPRES AU PROGRAMME         TGF15    ------------- 7337  00140002
      *                                                                 00150000
             03 COMM-GF15-PROPRE.                                       00160001
      *------------------------------ PAGINATION                        00170000
                04 COMM-GF15-ZONE-PAGINATION.                           00180001
      *------------------------------ NUMERO DE PAGE                    00190000
                   05 COMM-GF15-NPAGE       PIC 9(3) COMP-3.            00200001
      *------------------------------ NUMERO ITEM DE LA NIEME PAGE      00210000
                   05 COMM-GF15-TAB-ITEM    OCCURS 100.                 00220001
                      06 COMM-GF15-NITEM    PIC 9(3) COMP-3.            00230001
      *------------------------------ DERNIER ITEM LU PAGE EN COURS     00240000
                   05 COMM-GF15-NITEM-PAGE  PIC 9(3) COMP-3.            00250001
      *------------------------------ DERNIER ITEM DE LA TS GF15        00260001
                   05 COMM-GF15-NITEM-MAX   PIC 9(3) COMP-3.            00270001
      *------------------------------ TOP EXISTENCE TS GF15             00280001
                   05 COMM-GF15-EXIST-TS    PIC X(1).                   00290001
      *------------------------------ NOMBRE DE CODICS SAISIS           00300001
                   05 COMM-GF15-NOMBRE-CODIC PIC 9(3) COMP-3.           00310001
      *------------------------------ MAP AFFICHEE                      00320001
                04 COMM-GF15-ZONE-MAP       OCCURS 10.                  00330001
      *------------------------------ DEMANDEUR                         00340001
                   05 COMM-GF15-NSOC        PIC X(3).                   00341001
                   05 COMM-GF15-NLIEU       PIC X(3).                   00350001
      *------------------------------ NUMERO CODIC                      00360001
                   05 COMM-GF15-NCODIC      PIC X(7).                   00370001
      *------------------------------ FAMILLE                           00380001
                   05 COMM-GF15-CFAM        PIC X(5).                   00390001
      *------------------------------ MARQUE                            00400001
                   05 COMM-GF15-CMARQ       PIC X(5).                   00410001
      *------------------------------ REFERENCE CODIC                   00420001
                   05 COMM-GF15-LREFFOURN   PIC X(20).                  00430001
      *------------------------------ MODE STOCKAGE                     00440001
                   05 COMM-GF15-CMODSTOCK   PIC X(5).                   00450001
      *------------------------------ TYPE CONDITIONNEMENT              00460001
                   05 COMM-GF15-CTYPCONDT   PIC X(5).                   00470001
      *------------------------------ COLISAGE DE RECEPTION             00480001
                   05 COMM-GF15-QCOLIRECEPT PIC 9(5) COMP-3.            00490001
      *------------------------------ QUANTITE                          00500001
                   05 COMM-GF15-QTETOT      PIC 9(5) COMP-3.            00510001
      *------------------------------ NUMERO ITEM DE LA LIGNE           00520001
                   05 COMM-GF15-NITEM-CODIC PIC 9(3) COMP-3.            00530001
      *--- FLAG CODIC GROUPE                                                    
E0191              05 COMM-GF15-FLAG-GRPE           PIC X(1).                   
      *------------------------------ TABLE DES CODICS CARTON           00540001
      *                               AVEC LES CODICS ELEMENTAIRES      00550001
      *                               'LIBRES' ASSOCIES                 00560001
      *                               ET    DES CODICS POUVANT AVOIR    00570001
      *                               DES ACCESSOIRES                   00580001
                04 COMM-GF15-CODIC-CARTON   OCCURS 100.                 00590001
      *------------------------------ CODIC CARTON                      00600001
                   05 COMM-GF15-NCODIC-CARTON PIC X(7).                 00610001
      *------------------------------ CODIC LIE                         00620001
                   05 COMM-GF15-NCODIC-LIE    PIC X(7).                 00630001
      *------------------------------ TYPE DE LIEN                      00640001
                   05 COMM-GF15-NCODIC-LIEN   PIC X(5).                 00650001
      *------------------------------ LIEU                              00660001
                   05 COMM-GF15-NCODIC-NLIEU  PIC X(3).                 00670001
      *------------------------------ TOP DE CONTROLE SI                00680001
      *                               COMMENTAIRE RTGA00 AFFICHE        00690001
                   05 COMM-GF15-CONTROLE      PIC X(1).                 00700001
      *------------------------------ INDICE DU DERNIER CODIC CARTON    00710001
      *                               STOCKE                            00720001
                04 COMM-GF15-IND-CARTON     PIC 9(3) COMP-3.            00730001
AS    *------ FLAG BLOCAGE DES EAN NON AUTHENTIFIEE                             
AS           03 COMM-GF15-CDEAN              PIC X(1).                          
      *------ FLAG BLOCAGE DES LIEUX DEMANDEUTS                                 
JB           03 COMM-GF15-LDEMAND            PIC X(1).                          
C0411 *------ NBRE DE LIGNE DE COMMANDES MAXIMUM                                
C0411        03 COMM-GF15-NLIGMAX            PIC 9(5) COMP-3.                   
E0469 *------ FLAG SAISIE DE LA CDE OEM ASIE                                    
E0469        03 COMM-GF15-F-NCDEK            PIC X(1).                          
      *------------------------------ FILLER                            00740001
CD    *         04 COMM-GF15-FILLER         PIC X(4216).                00750003
      ***************************************************************** 00760000
                                                                                
