      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : ARIANE                                           *        
      *  TRANSACTION: GA??                                             *        
      *  TITRE      : COMMAREA DE PASSAGE VERS LE MODULE MAI14         *        
      *  LONGUEUR   : 74                                               *        
      *                                                                *        
      ******************************************************************        
      *                                                                 00530000
      *                                                                 00230000
       01  COMM-AI14-APPLI.                                             00260000
         02 COMM-AI14-ENTREE.                                           00260000
           05 COMM-AI14-NSOCIETE        PIC X(03).                      00320000
           05 COMM-AI14-CFAM            PIC X(05).                      00340000
           05 COMM-AI14-FLAGMOD         PIC X(01).                      00340000
         02 COMM-AI14-SORTIE.                                           00260000
           05 COMM-AI14-CODE-RETOUR     PIC X(01).                      00340000
           05 COMM-AI14-MESSAGE.                                                
                10 COMM-AI14-CODRET          PIC X.                             
                   88  COMM-AI14-OK          VALUE ' '.                         
                   88  COMM-AI14-ERR         VALUE '1'.                         
                10 COMM-AI14-LIBERR          PIC X(58).                         
         02 FILLER                    PIC X(005).                       00340000
                                                                                
