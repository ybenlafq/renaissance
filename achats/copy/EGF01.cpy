      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF01   EGF01                                              00000020
      ***************************************************************** 00000030
       01   EGF01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(15).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFP1L  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCHEFP1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCHEFP1F  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCHEFP1I  PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDEPL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSOCDEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCDEPF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSOCDEPI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDEPOTI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFP2L  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCHEFP2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCHEFP2F  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCHEFP2I  PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOC1L   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNSOC1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOC1F   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNSOC1I   PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEU1L   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLIEU1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEU1F   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLIEU1I   PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOC2L   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNSOC2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOC2F   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNSOC2I   PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEU2L   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLIEU2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEU2F   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLIEU2I   PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNCDEI    PIC X(7).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCODICL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODICF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCODICI   PIC X(7).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIT1L    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MTIT1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTIT1F    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MTIT1I    PIC X(47).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIT2L    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MTIT2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTIT2F    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MTIT2I    PIC X(25).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE6L   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MNCDE6L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE6F   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNCDE6I   PIC X(5).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIT3L    COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MTIT3L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTIT3F    PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MTIT3I    PIC X(15).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNINTER6L      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MNINTER6L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNINTER6F      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNINTER6I      PIC X(5).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIT4L    COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MTIT4L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTIT4F    PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MTIT4I    PIC X(18).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPE6L  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCTYPE6L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTYPE6F  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCTYPE6I  PIC X(5).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MLIBERRI  PIC X(78).                                      00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCODTRAI  PIC X(4).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MCICSI    PIC X(5).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MNETNAMI  PIC X(8).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MSCREENI  PIC X(4).                                       00001050
      ***************************************************************** 00001060
      * SDF: EGF01   EGF01                                              00001070
      ***************************************************************** 00001080
       01   EGF01O REDEFINES EGF01I.                                    00001090
           02 FILLER    PIC X(12).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MDATJOUA  PIC X.                                          00001120
           02 MDATJOUC  PIC X.                                          00001130
           02 MDATJOUP  PIC X.                                          00001140
           02 MDATJOUH  PIC X.                                          00001150
           02 MDATJOUV  PIC X.                                          00001160
           02 MDATJOUO  PIC X(10).                                      00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MTIMJOUA  PIC X.                                          00001190
           02 MTIMJOUC  PIC X.                                          00001200
           02 MTIMJOUP  PIC X.                                          00001210
           02 MTIMJOUH  PIC X.                                          00001220
           02 MTIMJOUV  PIC X.                                          00001230
           02 MTIMJOUO  PIC X(5).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MZONCMDA  PIC X.                                          00001260
           02 MZONCMDC  PIC X.                                          00001270
           02 MZONCMDP  PIC X.                                          00001280
           02 MZONCMDH  PIC X.                                          00001290
           02 MZONCMDV  PIC X.                                          00001300
           02 MZONCMDO  PIC X(15).                                      00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MCHEFP1A  PIC X.                                          00001330
           02 MCHEFP1C  PIC X.                                          00001340
           02 MCHEFP1P  PIC X.                                          00001350
           02 MCHEFP1H  PIC X.                                          00001360
           02 MCHEFP1V  PIC X.                                          00001370
           02 MCHEFP1O  PIC X(5).                                       00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MSOCDEPA  PIC X.                                          00001400
           02 MSOCDEPC  PIC X.                                          00001410
           02 MSOCDEPP  PIC X.                                          00001420
           02 MSOCDEPH  PIC X.                                          00001430
           02 MSOCDEPV  PIC X.                                          00001440
           02 MSOCDEPO  PIC X(3).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MDEPOTA   PIC X.                                          00001470
           02 MDEPOTC   PIC X.                                          00001480
           02 MDEPOTP   PIC X.                                          00001490
           02 MDEPOTH   PIC X.                                          00001500
           02 MDEPOTV   PIC X.                                          00001510
           02 MDEPOTO   PIC X(3).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MCHEFP2A  PIC X.                                          00001540
           02 MCHEFP2C  PIC X.                                          00001550
           02 MCHEFP2P  PIC X.                                          00001560
           02 MCHEFP2H  PIC X.                                          00001570
           02 MCHEFP2V  PIC X.                                          00001580
           02 MCHEFP2O  PIC X(5).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MNSOC1A   PIC X.                                          00001610
           02 MNSOC1C   PIC X.                                          00001620
           02 MNSOC1P   PIC X.                                          00001630
           02 MNSOC1H   PIC X.                                          00001640
           02 MNSOC1V   PIC X.                                          00001650
           02 MNSOC1O   PIC X(3).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MLIEU1A   PIC X.                                          00001680
           02 MLIEU1C   PIC X.                                          00001690
           02 MLIEU1P   PIC X.                                          00001700
           02 MLIEU1H   PIC X.                                          00001710
           02 MLIEU1V   PIC X.                                          00001720
           02 MLIEU1O   PIC X(3).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MNSOC2A   PIC X.                                          00001750
           02 MNSOC2C   PIC X.                                          00001760
           02 MNSOC2P   PIC X.                                          00001770
           02 MNSOC2H   PIC X.                                          00001780
           02 MNSOC2V   PIC X.                                          00001790
           02 MNSOC2O   PIC X(3).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MLIEU2A   PIC X.                                          00001820
           02 MLIEU2C   PIC X.                                          00001830
           02 MLIEU2P   PIC X.                                          00001840
           02 MLIEU2H   PIC X.                                          00001850
           02 MLIEU2V   PIC X.                                          00001860
           02 MLIEU2O   PIC X(3).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MNCDEA    PIC X.                                          00001890
           02 MNCDEC    PIC X.                                          00001900
           02 MNCDEP    PIC X.                                          00001910
           02 MNCDEH    PIC X.                                          00001920
           02 MNCDEV    PIC X.                                          00001930
           02 MNCDEO    PIC X(7).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MCODICA   PIC X.                                          00001960
           02 MCODICC   PIC X.                                          00001970
           02 MCODICP   PIC X.                                          00001980
           02 MCODICH   PIC X.                                          00001990
           02 MCODICV   PIC X.                                          00002000
           02 MCODICO   PIC X(7).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MTIT1A    PIC X.                                          00002030
           02 MTIT1C    PIC X.                                          00002040
           02 MTIT1P    PIC X.                                          00002050
           02 MTIT1H    PIC X.                                          00002060
           02 MTIT1V    PIC X.                                          00002070
           02 MTIT1O    PIC X(47).                                      00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MTIT2A    PIC X.                                          00002100
           02 MTIT2C    PIC X.                                          00002110
           02 MTIT2P    PIC X.                                          00002120
           02 MTIT2H    PIC X.                                          00002130
           02 MTIT2V    PIC X.                                          00002140
           02 MTIT2O    PIC X(25).                                      00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MNCDE6A   PIC X.                                          00002170
           02 MNCDE6C   PIC X.                                          00002180
           02 MNCDE6P   PIC X.                                          00002190
           02 MNCDE6H   PIC X.                                          00002200
           02 MNCDE6V   PIC X.                                          00002210
           02 MNCDE6O   PIC X(5).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MTIT3A    PIC X.                                          00002240
           02 MTIT3C    PIC X.                                          00002250
           02 MTIT3P    PIC X.                                          00002260
           02 MTIT3H    PIC X.                                          00002270
           02 MTIT3V    PIC X.                                          00002280
           02 MTIT3O    PIC X(15).                                      00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNINTER6A      PIC X.                                     00002310
           02 MNINTER6C PIC X.                                          00002320
           02 MNINTER6P PIC X.                                          00002330
           02 MNINTER6H PIC X.                                          00002340
           02 MNINTER6V PIC X.                                          00002350
           02 MNINTER6O      PIC X(5).                                  00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MTIT4A    PIC X.                                          00002380
           02 MTIT4C    PIC X.                                          00002390
           02 MTIT4P    PIC X.                                          00002400
           02 MTIT4H    PIC X.                                          00002410
           02 MTIT4V    PIC X.                                          00002420
           02 MTIT4O    PIC X(18).                                      00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MCTYPE6A  PIC X.                                          00002450
           02 MCTYPE6C  PIC X.                                          00002460
           02 MCTYPE6P  PIC X.                                          00002470
           02 MCTYPE6H  PIC X.                                          00002480
           02 MCTYPE6V  PIC X.                                          00002490
           02 MCTYPE6O  PIC X(5).                                       00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MLIBERRA  PIC X.                                          00002520
           02 MLIBERRC  PIC X.                                          00002530
           02 MLIBERRP  PIC X.                                          00002540
           02 MLIBERRH  PIC X.                                          00002550
           02 MLIBERRV  PIC X.                                          00002560
           02 MLIBERRO  PIC X(78).                                      00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MCODTRAA  PIC X.                                          00002590
           02 MCODTRAC  PIC X.                                          00002600
           02 MCODTRAP  PIC X.                                          00002610
           02 MCODTRAH  PIC X.                                          00002620
           02 MCODTRAV  PIC X.                                          00002630
           02 MCODTRAO  PIC X(4).                                       00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MCICSA    PIC X.                                          00002660
           02 MCICSC    PIC X.                                          00002670
           02 MCICSP    PIC X.                                          00002680
           02 MCICSH    PIC X.                                          00002690
           02 MCICSV    PIC X.                                          00002700
           02 MCICSO    PIC X(5).                                       00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MNETNAMA  PIC X.                                          00002730
           02 MNETNAMC  PIC X.                                          00002740
           02 MNETNAMP  PIC X.                                          00002750
           02 MNETNAMH  PIC X.                                          00002760
           02 MNETNAMV  PIC X.                                          00002770
           02 MNETNAMO  PIC X(8).                                       00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MSCREENA  PIC X.                                          00002800
           02 MSCREENC  PIC X.                                          00002810
           02 MSCREENP  PIC X.                                          00002820
           02 MSCREENH  PIC X.                                          00002830
           02 MSCREENV  PIC X.                                          00002840
           02 MSCREENO  PIC X(4).                                       00002850
                                                                                
