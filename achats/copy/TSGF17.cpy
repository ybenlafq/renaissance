      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : MAJ SAISIE DE COMMANDE FOURNISSEUR                     *         
      *            ARTICLES-QUANTITES (TRX:GF17)                      *         
      *****************************************************************         
      *   LL  1100C.                                                            
      *****************************************************************         
       01  TS-GF17-DESCR.                                                       
      *-----------------------------------------------------LONGUEUR TS         
      *    02 TS-GF17-LONG PIC S9(5) COMP-3  VALUE +1100.                       
      *    02 TS-GF17-LONG PIC S9(5) COMP-3  VALUE +1225.                       
      *    02 TS-GF17-LONG PIC S9(5) COMP-3  VALUE +1157.                       
      *    02 TS-GF17-LONG PIC S9(5) COMP-3  VALUE +1163.                       
      *                                                                         
           02 TS-GF17.                                                          
      *-----------------------------------------INFOS ISSU DE GF..---           
              03 TS-GF17-NCDE              PIC X(07).                           
              03 TS-GF17-NCODIC            PIC X(07).                           
      *-----------------------------------------INFOS ISSU DE GF20---..         
              03 TS-GF17-QSOLDE            PIC S9(05) COMP-3.                   
      *-----------------------------------------VARIABLE TRT---------..         
              03 TS-GF17-CMAJ              PIC  X(01).                          
      *{ remove-comma-in-dde 1.5                                                
      *          88 AFFICHABLE        VALUE 'B' , 'M'.                          
      *--                                                                       
                 88 AFFICHABLE        VALUE 'B'   'M'.                          
      *}                                                                        
                 88 LIGNE-SANS-MODIF  VALUE 'B'.                                
                 88 LIGNE-MODIFIEE    VALUE 'M'.                                
                 88 LIGNE-SUPPRIMEE   VALUE 'S'.                                
      *-----------------------------------------VARIABLE TRT---------..         
              03   TS-GF17-COMMENTAIRE    PIC  X(1).                            
                 88   TS-GF17-COMMENT     VALUE 'O'.                            
                 88   TS-GF17-NO-COMMENT  VALUE 'N'.                            
      *-----------------------------------------VARIBLE TRT----------..         
              03 TS-GF17-NSOCIETE          PIC X(03).                           
              03 TS-GF17-NLIEU             PIC X(03).                           
      *-----------------------------------------INFOS ISSU DE GA00---33         
              03 TS-GF17-LREFFOURN         PIC X(20).                           
              03 TS-GF17-LREFO             PIC X(20).                           
              03 TS-GF17-NEAN              PIC X(13).                           
              03 TS-GF17-CFAM              PIC X(05).                           
              03 TS-GF17-CMARQ             PIC X(05).                           
              03 TS-GF17-QCOLIRECEPT       PIC S9(05) COMP-3.                   
              03 TS-GF17-QCONDT            PIC S9(05) COMP-3.                   
              03 TS-GF17-CMODSTOCK-CODIC   PIC  X(05).                          
              03 TS-GF17-CMODSTOCK-CDE     PIC  X(05).                          
              03 TS-GF17-CONTROL-COLIRECEP PIC  X(01).                          
              03 TS-GF17-CQUOTA            PIC  X(05).                          
      *-----LL = 24 * 20 = 480------------------TABLEAU 4 X 5 ---------         
              03 TS-GF17-INFOS.                                                 
               04 TS-GF17-POSTE OCCURS 20 TIMES.                                
                05 TS-GF17-QCDE              PIC S9(05) COMP-3.                 
                05 TS-GF17-QREC              PIC S9(05) COMP-3.                 
                05 TS-GF17-QUOLIVR           PIC S9(05) COMP-3.                 
                05 TS-GF17-QUOPAL            PIC S9(05) COMP-3.                 
                05 TS-GF17-DLIVRAISON        PIC  X(08).                        
                05 TS-GF17-NLIVRAISON        PIC  X(07).                        
                05 TS-GF17-QCDERES           PIC S9(05) COMP-3.                 
      *-----LL = 24 * 20 = 480--ORIGINE---------TABLEAU 4 X 5 ---------         
              03 TS-GF17-0-INFOS.                                               
               04 TS-GF17-0-POSTE OCCURS 20 TIMES.                              
                05 TS-GF17-0-QCDE              PIC S9(05) COMP-3.               
                05 TS-GF17-0-QREC              PIC S9(05) COMP-3.               
                05 TS-GF17-0-QUOLIVR           PIC S9(05) COMP-3.               
                05 TS-GF17-0-QUOPAL            PIC S9(05) COMP-3.               
                05 TS-GF17-0-DLIVRAISON        PIC  X(08).                      
                05 TS-GF17-0-NLIVRAISON        PIC  X(07).                      
                05 TS-GF17-0-QCDERES           PIC S9(05) COMP-3.               
      *-----------------------------------------FILLER-----------------         
      *       03 TS-GF17-FILLER              PIC X(56).                         
                                                                                
