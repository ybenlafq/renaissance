      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      **************************************************************    00010000
      * DCLGEN DE LA TABLE FEADTL NON ETENDUE                           00020006
      **************************************************************    00030000
          01 RVAI5300.                                                  00040008
              05 AI53-FSSKU  PIC X(9).                                  00050009
              05 AI53-FSCTL  PIC S9(5) COMP-3.                          00060008
              05 AI53-FSCLAS PIC X(3).                                  00070009
              05 AI53-FSFNUM PIC X(5).                                  00080009
              05 AI53-FSSTCN PIC S9(1) COMP-3.                          00090008
              05 AI53-FSSTDT PIC S9(6) COMP-3.                          00100008
              05 AI53-FSENCN PIC S9(1) COMP-3.                          00110008
              05 AI53-FSENDT PIC S9(6) COMP-3.                          00120008
              05 AI53-FSLWLM PIC X(12).                                 00130008
              05 AI53-FSUPLM PIC X(12).                                 00140008
              05 AI53-FSUOMN PIC S9(5) COMP-3.                          00150008
              05 AI53-FSWEBP PIC S9(1) COMP-3.                          00160008
              05 AI53-FSVAL  PIC S9(8) COMP-3.                          00170008
      * INDICATEURSTNULLITE                                             00180006
          01 RVAI5300-FLAGS.                                            00190008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI53-FSSKU-F PIC S9(4) COMP.                           00200008
      *--                                                                       
              05 AI53-FSSKU-F PIC S9(4) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI53-FSCTL-F PIC S9(4) COMP.                           00210008
      *--                                                                       
              05 AI53-FSCTL-F PIC S9(4) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI53-FSCLAS-F PIC S9(4) COMP.                          00220008
      *--                                                                       
              05 AI53-FSCLAS-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI53-FSFNUM-F PIC S9(4) COMP.                          00230008
      *--                                                                       
              05 AI53-FSFNUM-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI53-FSSTCN-F PIC S9(4) COMP.                          00240008
      *--                                                                       
              05 AI53-FSSTCN-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI53-FSSTDT-F PIC S9(4) COMP.                          00250008
      *--                                                                       
              05 AI53-FSSTDT-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI53-FSENCN-F PIC S9(4) COMP.                          00260008
      *--                                                                       
              05 AI53-FSENCN-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI53-FSENDT-F PIC S9(4) COMP.                          00270008
      *--                                                                       
              05 AI53-FSENDT-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI53-FSLWLM-F PIC S9(4) COMP.                          00280008
      *--                                                                       
              05 AI53-FSLWLM-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI53-FSUPLM-F PIC S9(4) COMP.                          00290008
      *--                                                                       
              05 AI53-FSUPLM-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI53-FSUOMN-F PIC S9(4) COMP.                          00300008
      *--                                                                       
              05 AI53-FSUOMN-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI53-FSWEBP-F PIC S9(4) COMP.                          00310008
      *--                                                                       
              05 AI53-FSWEBP-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI53-FSVAL-F PIC S9(4) COMP.                           00320008
      *                                                                         
      *--                                                                       
              05 AI53-FSVAL-F PIC S9(4) COMP-5.                                 
                                                                                
      *}                                                                        
