      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      *                                                                         
      * TS TGF47R : TOUS LES RAYONS DU QUOTA PLANNING / RAYON                   
      *                                                                         
       01  TS-GF47R.                                                            
      *                                                                         
           05  TS-GF47R-DONNEES.                                                
      *                                                                         
               10 TS-GF47R-TSGF47A.                                             
                    15 TS-GF47R-CRAYON       PIC X(5).                          
                    15 TS-GF47R-LRAYON       PIC X(15).                         
                    15 TS-GF47R-FLAG         PIC X(1).                          
      *                                                                         
               10 TS-GF47R-COMPLEMENT.                                          
                    15 TS-GF47R-CMODSTOCK    PIC X(5) OCCURS 3.                 
      *                                                                         
                                                                                
