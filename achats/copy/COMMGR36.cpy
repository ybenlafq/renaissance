      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TGR33 (TGR00 -> MENU)    TR: GR00  *    00002222
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3710  00009000
      *                                                                 00410100
      *        TRANSACTION GR36 : EDITION DU BON A QUAI            *    00411022
      *                                                                 00412000
          02 COMM-GR36-APPLI REDEFINES COMM-GR00-APPLI.                 00420022
             03 COMM-GR36-DONNEES-TGR36.                                00520224
                04 COMM-GR36-ENTITE         PIC X(05).                  00642026
                04 COMM-GR36-INTER          PIC X(05).                  00642026
                04 COMM-GR36-NSOCIETE       PIC XXX.                    00560026
                04 COMM-GR36-NDEPOT         PIC XXX.                    00600026
                04 COMM-GR36-ITEM           PIC 9(03).                  00644026
                04 COMM-GR36-NBRCOMM        PIC 9(03).                  00644026
                04 COMM-GR36-POS-MAX        PIC 9(03).                  00644026
                04 COMM-GR36-ETAT-PASSAGE   PIC X(01).                  00644026
                   88 COMM-GR36-1ER-PASSAGE     VALUE '1'.              00644026
                   88 COMM-GR36-1ER-MESSAGE     VALUE '2'.              00644026
                   88 COMM-GR36-XEME-MESSAGE    VALUE '3'.              00644026
                04 COMM-GR36-MESS           PIC X(80).                  00600026
             03 COMM-GR36-LIBRE             PIC X(3604).                00740029
      ***************************************************************** 00750000
                                                                                
