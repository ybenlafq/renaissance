      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TAI06                    TR: AI06  *    00020000
      *                  GESTION DES ENTITES DE COMMANDE           *    00030000
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00050000
      *                                                                 00060000
          02 COMM-AI06-APPLI REDEFINES COMM-GA00-APPLI.                 00070000
      *------------------------------ CODE FONCTION                     00080000
             03 COMM-AI06-CFONC          PIC X(3).                      00090001
      *------------------------------ CODE ENTITE DE COMMANDE           00100000
             03 COMM-AI06-NENTCDE        PIC X(5).                      00110000
      *------------------------------ LIBELLE ENTITE DE COMMANDE        00120000
             03 COMM-AI06-LENTCDE        PIC X(20).                     00130000
      *------------------------------ ADRESSE 1                         00140000
             03 COMM-AI06-LENTCDEADR1    PIC X(32).                     00150000
      *------------------------------ ADRESSE 2 --> 5                   00160002
             03 COMM-AI06-LENTCDEADR2    PIC X(32).                     00170000
             03 COMM-AI06-LENTCDEADR3    PIC X(32).                     00171001
             03 COMM-AI06-LENTCDEADR4    PIC X(32).                     00172001
             03 COMM-AI06-LENTCDEADR5    PIC X(32).                     00173001
      *------------------------------ CODE POSTAL                       00180000
             03 COMM-AI06-CODPOS         PIC 9(8).                      00190001
      *------------------------------ VILLE                             00200000
             03 COMM-AI06-VILLE          PIC X(23).                     00210002
      *------------------------------ ADRESSE 6 (PAYS)                  00210102
             03 COMM-AI06-LENTCDEADR6    PIC X(32).                     00211002
      *------------------------------ TELEPHONE                         00220000
             03 COMM-AI06-NENTCDETEL     PIC X(15).                     00230000
      *------------------------------ TELEX                             00240000
             03 COMM-AI06-CENTCDETELX    PIC X(15).                     00250000
      *------------------------------ FILLER                            00260000
             03 COMM-AI06-FILLER         PIC X(3443).                   00270004
                                                                                
                                                                        00280002
