      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGF21                    TR: GF21  *    00020000
      * VALIDATION DES COMMANDES FOURNISSEUR ARTICLES-QUANTITES    *    00030000
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 7421  00050000
      *                                                                 00060000
          02 COMM-GF21-APPLI REDEFINES COMM-GF00-APPLI.                 00070000
      *                                                                 00080000
      * ZONES PROPRES AU PROGRAMME         TGF21    ------------- 312   00090000
      *                                                                 00100000
      *---------------------------ENTETE ECRAN                          00110000
             03 COMM-GF21-ZONE-MAP.                                     00111000
                05 COMM-GF21-NSOCIETE          PIC X(3).                00111100
                05 COMM-GF21-NDEPOT            PIC X(3).                00111200
                05 COMM-GF21-CHEFPROD          PIC X(5).                00112000
                05 COMM-GF21-LCHEFPROD         PIC X(20).               00115000
NSKEP           05 COMM-GF21-CFEXT             PIC X(1).                00116005
      *                                                                 00120000
      *------------------------------ GESTION DES CONTROLES             00121000
             03 COMM-GF21-ZONE-CONTROLE.                                00121100
                05 COMM-GF21-CONTROLE    PIC 9(1).                      00121200
                05 COMM-GF21-VALIDATION  PIC X(1).                      00121300
      *                                                                 00121400
      *------------------------------ PAGINATION                        00121500
             03 COMM-GF21-ZONE-PAGINATION.                              00121600
                05 COMM-GF21-NPAGE       PIC 9(3) COMP-3.               00121700
                05 COMM-GF21-PAGE-MAX    PIC 9(3) COMP-3.               00121801
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 COMM-GF21-NB-COMM     PIC S9(4) COMP.                00121902
      *--                                                                       
                05 COMM-GF21-NB-COMM     PIC S9(4) COMP-5.                      
      *}                                                                        
      *                                                                 00122002
      *------------------------------ GESTION TS                        00122102
             03 COMM-GF21-TS.                                           00122202
                05 CS-GF21-IDENT         PIC X(8).                      00122302
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         05 CS-GF21-NB            PIC S9(4) COMP.                00123000
      *                                                                         
      *--                                                                       
                05 CS-GF21-NB            PIC S9(4) COMP-5.                      
                                                                                
      *}                                                                        
