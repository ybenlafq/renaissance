      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGR02 (TGR00 -> MENU)    TR: GR00  *    00020000
      *               GESTION DES RECEPTIONS                       *    00030000
      *                                                                 00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3596  00050001
      *                                                                 00060000
      *        TRANSACTION GR02 :                                       00070000
      *                                                                 00080000
          03 COMM-GR02-APPLI REDEFINES COMM-GR01-APPLI.                 00090000
      *------------------------------ ZONE DONNEES TGR02                00100000
             04 COMM-GR02-DONNEES-TGR02.                                00110000
      *------------------------------ TYPE DE RECEPTION                 00120000
                05 COMM-GR02-CREC              PIC X(05).               00130000
                05 COMM-GR02-CREC2             PIC X(05).               00140000
                05 COMM-GR02-LREC              PIC X(20).               00150000
                05 COMM-GR02-PAGE              PIC 9(2).                00160000
                05 COMM-GR02-SEM               PIC X(6).                00170000
                05 COMM-GR02-SEMDEB            PIC X(6).                00180000
                05 COMM-GR02-QUANT-JOUR1       PIC 9(5).                00190000
                05 COMM-GR02-SEM-PREC          PIC X(6).                00200000
                05 COMM-GR02-SEM-SUIV          PIC X(6).                00210000
                05 COMM-GR02-INFOS-TRAIT-NORMAL OCCURS 12.              00220000
                   06 COMM-GR02-NATURE-MAJ     PIC  X.                  00230000
                   06 COMM-GR02-NBRE-PRO       PIC  9.                  00240000
                05 COMM-GR02-LIGNE-TESTEE      PIC 9(2).                00250000
      *------------------------------ ZONE LIBRE                        00260000
      *      04 COMM-GR02-LIBRE          PIC X(3509).                   00270001
             04 COMM-GR02-LIBRE          PIC X(2369).                           
      ***************************************************************** 00280000
                                                                                
