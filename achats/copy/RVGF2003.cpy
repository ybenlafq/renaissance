      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGF2003                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF2003                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF2003.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF2003.                                                            
      *}                                                                        
           02  GF20-NCDE               PIC X(007).                              
           02  GF20-NCODIC             PIC X(007).                              
           02  GF20-DSAISIE            PIC X(008).                              
           02  GF20-DVALIDITE          PIC X(008).                              
           02  GF20-QCDE               PIC S9(5)         COMP-3.                
           02  GF20-QREC               PIC S9(5)         COMP-3.                
           02  GF20-QSOLDE             PIC S9(5)         COMP-3.                
           02  GF20-PABASEFACT         PIC S9(7)V9(0002) COMP-3.                
           02  GF20-PROMOFACT          PIC S9(7)V9(0002) COMP-3.                
           02  GF20-QTAUXESCPT         PIC S9(3)V9(0002) COMP-3.                
           02  GF20-PTAUXESCPT         PIC S9(7)V9(0002) COMP-3.                
           02  GF20-PAHORSPROMO        PIC S9(7)V9(0002) COMP-3.                
           02  GF20-PRISTPROMO         PIC S9(7)V9(0002) COMP-3.                
           02  GF20-PRISTCONDI         PIC S9(7)V9(0002) COMP-3.                
           02  GF20-PRA                PIC S9(7)V9(0002) COMP-3.                
           02  GF20-DSYST              PIC S9(13)        COMP-3.                
           02  GF20-PCF                PIC S9(7)V9(0006) COMP-3.                
           02  GF20-QCDEORIG           PIC S9(05)        COMP-3.                
           02  GF20-CREASON            PIC X(005).                              
           02  GF20-FFACTURE           PIC X(001).                              
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGF2003                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF2003-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF2003-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-NCDE-F             PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-NCDE-F             PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-NCODIC-F           PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-NCODIC-F           PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-DSAISIE-F          PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-DSAISIE-F          PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-DVALIDITE-F        PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-DVALIDITE-F        PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-QCDE-F             PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-QCDE-F             PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-QREC-F             PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-QREC-F             PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-QSOLDE-F           PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-QSOLDE-F           PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-PABASEFACT-F       PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-PABASEFACT-F       PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-PROMOFACT-F        PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-PROMOFACT-F        PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-QTAUXESCPT-F       PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-QTAUXESCPT-F       PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-PTAUXESCPT-F       PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-PTAUXESCPT-F       PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-PAHORSPROMO-F      PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-PAHORSPROMO-F      PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-PRISTPROMO-F       PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-PRISTPROMO-F       PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-PRISTCONDI-F       PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-PRISTCONDI-F       PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-PRA-F              PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-PRA-F              PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-DSYST-F            PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-DSYST-F            PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-PCF-F              PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-PCF-F              PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-QCDEORIG-F         PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-QCDEORIG-F         PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-CREASON-F          PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-CREASON-F          PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF20-FFACTURE-F         PIC S9(4)         COMP.                  
      *--                                                                       
           02  GF20-FFACTURE-F         PIC S9(4) COMP-5.                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
