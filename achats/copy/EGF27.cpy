      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF27   EGF27                                              00000020
      ***************************************************************** 00000030
       01   EGF27I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCDEI    PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVALIDITEL    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MDVALIDITEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDVALIDITEF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDVALIDITEI    PIC X(8).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCODICI  PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLREFFOURNI    PIC X(20).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCDEVISEI      PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEVISEL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDEVISEF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLDEVISEI      PIC X(24).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCFAMI    PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLFAMI    PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCMARQI   PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLMARQI   PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPABASEFACTDERL     COMP PIC S9(4).                       00000580
      *--                                                                       
           02 MPABASEFACTDERL COMP-5 PIC S9(4).                                 
      *}                                                                        
           02 MPABASEFACTDERF     PIC X.                                00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MPABASEFACTDERI     PIC X(9).                             00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPABASEFACTL   COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MPABASEFACTL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MPABASEFACTF   PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MPABASEFACTI   PIC X(9).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPROMOFACTDERL      COMP PIC S9(4).                       00000660
      *--                                                                       
           02 MPROMOFACTDERL COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 MPROMOFACTDERF      PIC X.                                00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MPROMOFACTDERI      PIC X(9).                             00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPROMOFACTL    COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MPROMOFACTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MPROMOFACTF    PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MPROMOFACTI    PIC X(9).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPTAUXESCPTDERL     COMP PIC S9(4).                       00000740
      *--                                                                       
           02 MPTAUXESCPTDERL COMP-5 PIC S9(4).                                 
      *}                                                                        
           02 MPTAUXESCPTDERF     PIC X.                                00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MPTAUXESCPTDERI     PIC X(9).                             00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPTAUXESCPTL   COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MPTAUXESCPTL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MPTAUXESCPTF   PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MPTAUXESCPTI   PIC X(9).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRISTCONDIDERL     COMP PIC S9(4).                       00000820
      *--                                                                       
           02 MPRISTCONDIDERL COMP-5 PIC S9(4).                                 
      *}                                                                        
           02 MPRISTCONDIDERF     PIC X.                                00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MPRISTCONDIDERI     PIC X(9).                             00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRISTCONDIL   COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MPRISTCONDIL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MPRISTCONDIF   PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MPRISTCONDII   PIC X(9).                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRISTPROMODERL     COMP PIC S9(4).                       00000900
      *--                                                                       
           02 MPRISTPROMODERL COMP-5 PIC S9(4).                                 
      *}                                                                        
           02 MPRISTPROMODERF     PIC X.                                00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MPRISTPROMODERI     PIC X(9).                             00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRISTPROMOL   COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MPRISTPROMOL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MPRISTPROMOF   PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MPRISTPROMOI   PIC X(9).                                  00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRADERL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MPRADERL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPRADERF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MPRADERI  PIC X(9).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRAL     COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MPRAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPRAF     PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MPRAI     PIC X(9).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPCFDERL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MPCFDERL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPCFDERF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MPCFDERI  PIC X(9).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPCFL     COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MPCFL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPCFF     PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MPCFI     PIC X(9).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAHORSPROMODERL    COMP PIC S9(4).                       00001140
      *--                                                                       
           02 MPAHORSPROMODERL COMP-5 PIC S9(4).                                
      *}                                                                        
           02 MPAHORSPROMODERF    PIC X.                                00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MPAHORSPROMODERI    PIC X(9).                             00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAHORSPROMOL  COMP PIC S9(4).                            00001180
      *--                                                                       
           02 MPAHORSPROMOL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MPAHORSPROMOF  PIC X.                                     00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MPAHORSPROMOI  PIC X(9).                                  00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTAUXESCPTDERL     COMP PIC S9(4).                       00001220
      *--                                                                       
           02 MQTAUXESCPTDERL COMP-5 PIC S9(4).                                 
      *}                                                                        
           02 MQTAUXESCPTDERF     PIC X.                                00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MQTAUXESCPTDERI     PIC X(5).                             00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTAUXESCPTL   COMP PIC S9(4).                            00001260
      *--                                                                       
           02 MQTAUXESCPTL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MQTAUXESCPTF   PIC X.                                     00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MQTAUXESCPTI   PIC X(5).                                  00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MZONCMDI  PIC X(15).                                      00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MLIBERRI  PIC X(58).                                      00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MCODTRAI  PIC X(4).                                       00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MCICSI    PIC X(5).                                       00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MNETNAMI  PIC X(8).                                       00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001500
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MSCREENI  PIC X(4).                                       00001530
      ***************************************************************** 00001540
      * SDF: EGF27   EGF27                                              00001550
      ***************************************************************** 00001560
       01   EGF27O REDEFINES EGF27I.                                    00001570
           02 FILLER    PIC X(12).                                      00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MDATJOUA  PIC X.                                          00001600
           02 MDATJOUC  PIC X.                                          00001610
           02 MDATJOUP  PIC X.                                          00001620
           02 MDATJOUH  PIC X.                                          00001630
           02 MDATJOUV  PIC X.                                          00001640
           02 MDATJOUO  PIC X(10).                                      00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MTIMJOUA  PIC X.                                          00001670
           02 MTIMJOUC  PIC X.                                          00001680
           02 MTIMJOUP  PIC X.                                          00001690
           02 MTIMJOUH  PIC X.                                          00001700
           02 MTIMJOUV  PIC X.                                          00001710
           02 MTIMJOUO  PIC X(5).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MCFONCA   PIC X.                                          00001740
           02 MCFONCC   PIC X.                                          00001750
           02 MCFONCP   PIC X.                                          00001760
           02 MCFONCH   PIC X.                                          00001770
           02 MCFONCV   PIC X.                                          00001780
           02 MCFONCO   PIC X(3).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MNCDEA    PIC X.                                          00001810
           02 MNCDEC    PIC X.                                          00001820
           02 MNCDEP    PIC X.                                          00001830
           02 MNCDEH    PIC X.                                          00001840
           02 MNCDEV    PIC X.                                          00001850
           02 MNCDEO    PIC X(7).                                       00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MDVALIDITEA    PIC X.                                     00001880
           02 MDVALIDITEC    PIC X.                                     00001890
           02 MDVALIDITEP    PIC X.                                     00001900
           02 MDVALIDITEH    PIC X.                                     00001910
           02 MDVALIDITEV    PIC X.                                     00001920
           02 MDVALIDITEO    PIC X(8).                                  00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MNCODICA  PIC X.                                          00001950
           02 MNCODICC  PIC X.                                          00001960
           02 MNCODICP  PIC X.                                          00001970
           02 MNCODICH  PIC X.                                          00001980
           02 MNCODICV  PIC X.                                          00001990
           02 MNCODICO  PIC X(7).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MLREFFOURNA    PIC X.                                     00002020
           02 MLREFFOURNC    PIC X.                                     00002030
           02 MLREFFOURNP    PIC X.                                     00002040
           02 MLREFFOURNH    PIC X.                                     00002050
           02 MLREFFOURNV    PIC X.                                     00002060
           02 MLREFFOURNO    PIC X(20).                                 00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MCDEVISEA      PIC X.                                     00002090
           02 MCDEVISEC PIC X.                                          00002100
           02 MCDEVISEP PIC X.                                          00002110
           02 MCDEVISEH PIC X.                                          00002120
           02 MCDEVISEV PIC X.                                          00002130
           02 MCDEVISEO      PIC X(3).                                  00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MLDEVISEA      PIC X.                                     00002160
           02 MLDEVISEC PIC X.                                          00002170
           02 MLDEVISEP PIC X.                                          00002180
           02 MLDEVISEH PIC X.                                          00002190
           02 MLDEVISEV PIC X.                                          00002200
           02 MLDEVISEO      PIC X(24).                                 00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MCFAMA    PIC X.                                          00002230
           02 MCFAMC    PIC X.                                          00002240
           02 MCFAMP    PIC X.                                          00002250
           02 MCFAMH    PIC X.                                          00002260
           02 MCFAMV    PIC X.                                          00002270
           02 MCFAMO    PIC X(5).                                       00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MLFAMA    PIC X.                                          00002300
           02 MLFAMC    PIC X.                                          00002310
           02 MLFAMP    PIC X.                                          00002320
           02 MLFAMH    PIC X.                                          00002330
           02 MLFAMV    PIC X.                                          00002340
           02 MLFAMO    PIC X(20).                                      00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MCMARQA   PIC X.                                          00002370
           02 MCMARQC   PIC X.                                          00002380
           02 MCMARQP   PIC X.                                          00002390
           02 MCMARQH   PIC X.                                          00002400
           02 MCMARQV   PIC X.                                          00002410
           02 MCMARQO   PIC X(5).                                       00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MLMARQA   PIC X.                                          00002440
           02 MLMARQC   PIC X.                                          00002450
           02 MLMARQP   PIC X.                                          00002460
           02 MLMARQH   PIC X.                                          00002470
           02 MLMARQV   PIC X.                                          00002480
           02 MLMARQO   PIC X(20).                                      00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MPABASEFACTDERA     PIC X.                                00002510
           02 MPABASEFACTDERC     PIC X.                                00002520
           02 MPABASEFACTDERP     PIC X.                                00002530
           02 MPABASEFACTDERH     PIC X.                                00002540
           02 MPABASEFACTDERV     PIC X.                                00002550
           02 MPABASEFACTDERO     PIC X(9).                             00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MPABASEFACTA   PIC X.                                     00002580
           02 MPABASEFACTC   PIC X.                                     00002590
           02 MPABASEFACTP   PIC X.                                     00002600
           02 MPABASEFACTH   PIC X.                                     00002610
           02 MPABASEFACTV   PIC X.                                     00002620
           02 MPABASEFACTO   PIC X(9).                                  00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MPROMOFACTDERA      PIC X.                                00002650
           02 MPROMOFACTDERC PIC X.                                     00002660
           02 MPROMOFACTDERP PIC X.                                     00002670
           02 MPROMOFACTDERH PIC X.                                     00002680
           02 MPROMOFACTDERV PIC X.                                     00002690
           02 MPROMOFACTDERO      PIC X(9).                             00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MPROMOFACTA    PIC X.                                     00002720
           02 MPROMOFACTC    PIC X.                                     00002730
           02 MPROMOFACTP    PIC X.                                     00002740
           02 MPROMOFACTH    PIC X.                                     00002750
           02 MPROMOFACTV    PIC X.                                     00002760
           02 MPROMOFACTO    PIC X(9).                                  00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MPTAUXESCPTDERA     PIC X.                                00002790
           02 MPTAUXESCPTDERC     PIC X.                                00002800
           02 MPTAUXESCPTDERP     PIC X.                                00002810
           02 MPTAUXESCPTDERH     PIC X.                                00002820
           02 MPTAUXESCPTDERV     PIC X.                                00002830
           02 MPTAUXESCPTDERO     PIC X(9).                             00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MPTAUXESCPTA   PIC X.                                     00002860
           02 MPTAUXESCPTC   PIC X.                                     00002870
           02 MPTAUXESCPTP   PIC X.                                     00002880
           02 MPTAUXESCPTH   PIC X.                                     00002890
           02 MPTAUXESCPTV   PIC X.                                     00002900
           02 MPTAUXESCPTO   PIC X(9).                                  00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MPRISTCONDIDERA     PIC X.                                00002930
           02 MPRISTCONDIDERC     PIC X.                                00002940
           02 MPRISTCONDIDERP     PIC X.                                00002950
           02 MPRISTCONDIDERH     PIC X.                                00002960
           02 MPRISTCONDIDERV     PIC X.                                00002970
           02 MPRISTCONDIDERO     PIC X(9).                             00002980
           02 FILLER    PIC X(2).                                       00002990
           02 MPRISTCONDIA   PIC X.                                     00003000
           02 MPRISTCONDIC   PIC X.                                     00003010
           02 MPRISTCONDIP   PIC X.                                     00003020
           02 MPRISTCONDIH   PIC X.                                     00003030
           02 MPRISTCONDIV   PIC X.                                     00003040
           02 MPRISTCONDIO   PIC X(9).                                  00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MPRISTPROMODERA     PIC X.                                00003070
           02 MPRISTPROMODERC     PIC X.                                00003080
           02 MPRISTPROMODERP     PIC X.                                00003090
           02 MPRISTPROMODERH     PIC X.                                00003100
           02 MPRISTPROMODERV     PIC X.                                00003110
           02 MPRISTPROMODERO     PIC X(9).                             00003120
           02 FILLER    PIC X(2).                                       00003130
           02 MPRISTPROMOA   PIC X.                                     00003140
           02 MPRISTPROMOC   PIC X.                                     00003150
           02 MPRISTPROMOP   PIC X.                                     00003160
           02 MPRISTPROMOH   PIC X.                                     00003170
           02 MPRISTPROMOV   PIC X.                                     00003180
           02 MPRISTPROMOO   PIC X(9).                                  00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MPRADERA  PIC X.                                          00003210
           02 MPRADERC  PIC X.                                          00003220
           02 MPRADERP  PIC X.                                          00003230
           02 MPRADERH  PIC X.                                          00003240
           02 MPRADERV  PIC X.                                          00003250
           02 MPRADERO  PIC X(9).                                       00003260
           02 FILLER    PIC X(2).                                       00003270
           02 MPRAA     PIC X.                                          00003280
           02 MPRAC     PIC X.                                          00003290
           02 MPRAP     PIC X.                                          00003300
           02 MPRAH     PIC X.                                          00003310
           02 MPRAV     PIC X.                                          00003320
           02 MPRAO     PIC X(9).                                       00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MPCFDERA  PIC X.                                          00003350
           02 MPCFDERC  PIC X.                                          00003360
           02 MPCFDERP  PIC X.                                          00003370
           02 MPCFDERH  PIC X.                                          00003380
           02 MPCFDERV  PIC X.                                          00003390
           02 MPCFDERO  PIC X(9).                                       00003400
           02 FILLER    PIC X(2).                                       00003410
           02 MPCFA     PIC X.                                          00003420
           02 MPCFC     PIC X.                                          00003430
           02 MPCFP     PIC X.                                          00003440
           02 MPCFH     PIC X.                                          00003450
           02 MPCFV     PIC X.                                          00003460
           02 MPCFO     PIC X(9).                                       00003470
           02 FILLER    PIC X(2).                                       00003480
           02 MPAHORSPROMODERA    PIC X.                                00003490
           02 MPAHORSPROMODERC    PIC X.                                00003500
           02 MPAHORSPROMODERP    PIC X.                                00003510
           02 MPAHORSPROMODERH    PIC X.                                00003520
           02 MPAHORSPROMODERV    PIC X.                                00003530
           02 MPAHORSPROMODERO    PIC X(9).                             00003540
           02 FILLER    PIC X(2).                                       00003550
           02 MPAHORSPROMOA  PIC X.                                     00003560
           02 MPAHORSPROMOC  PIC X.                                     00003570
           02 MPAHORSPROMOP  PIC X.                                     00003580
           02 MPAHORSPROMOH  PIC X.                                     00003590
           02 MPAHORSPROMOV  PIC X.                                     00003600
           02 MPAHORSPROMOO  PIC X(9).                                  00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MQTAUXESCPTDERA     PIC X.                                00003630
           02 MQTAUXESCPTDERC     PIC X.                                00003640
           02 MQTAUXESCPTDERP     PIC X.                                00003650
           02 MQTAUXESCPTDERH     PIC X.                                00003660
           02 MQTAUXESCPTDERV     PIC X.                                00003670
           02 MQTAUXESCPTDERO     PIC X(5).                             00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MQTAUXESCPTA   PIC X.                                     00003700
           02 MQTAUXESCPTC   PIC X.                                     00003710
           02 MQTAUXESCPTP   PIC X.                                     00003720
           02 MQTAUXESCPTH   PIC X.                                     00003730
           02 MQTAUXESCPTV   PIC X.                                     00003740
           02 MQTAUXESCPTO   PIC X(5).                                  00003750
           02 FILLER    PIC X(2).                                       00003760
           02 MZONCMDA  PIC X.                                          00003770
           02 MZONCMDC  PIC X.                                          00003780
           02 MZONCMDP  PIC X.                                          00003790
           02 MZONCMDH  PIC X.                                          00003800
           02 MZONCMDV  PIC X.                                          00003810
           02 MZONCMDO  PIC X(15).                                      00003820
           02 FILLER    PIC X(2).                                       00003830
           02 MLIBERRA  PIC X.                                          00003840
           02 MLIBERRC  PIC X.                                          00003850
           02 MLIBERRP  PIC X.                                          00003860
           02 MLIBERRH  PIC X.                                          00003870
           02 MLIBERRV  PIC X.                                          00003880
           02 MLIBERRO  PIC X(58).                                      00003890
           02 FILLER    PIC X(2).                                       00003900
           02 MCODTRAA  PIC X.                                          00003910
           02 MCODTRAC  PIC X.                                          00003920
           02 MCODTRAP  PIC X.                                          00003930
           02 MCODTRAH  PIC X.                                          00003940
           02 MCODTRAV  PIC X.                                          00003950
           02 MCODTRAO  PIC X(4).                                       00003960
           02 FILLER    PIC X(2).                                       00003970
           02 MCICSA    PIC X.                                          00003980
           02 MCICSC    PIC X.                                          00003990
           02 MCICSP    PIC X.                                          00004000
           02 MCICSH    PIC X.                                          00004010
           02 MCICSV    PIC X.                                          00004020
           02 MCICSO    PIC X(5).                                       00004030
           02 FILLER    PIC X(2).                                       00004040
           02 MNETNAMA  PIC X.                                          00004050
           02 MNETNAMC  PIC X.                                          00004060
           02 MNETNAMP  PIC X.                                          00004070
           02 MNETNAMH  PIC X.                                          00004080
           02 MNETNAMV  PIC X.                                          00004090
           02 MNETNAMO  PIC X(8).                                       00004100
           02 FILLER    PIC X(2).                                       00004110
           02 MSCREENA  PIC X.                                          00004120
           02 MSCREENC  PIC X.                                          00004130
           02 MSCREENP  PIC X.                                          00004140
           02 MSCREENH  PIC X.                                          00004150
           02 MSCREENV  PIC X.                                          00004160
           02 MSCREENO  PIC X(4).                                       00004170
                                                                                
