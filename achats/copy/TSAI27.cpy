      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ******************************************************************        
      * TS AI27 ASSOCIATION CODE DESCRIPTIF -> FEATURES                         
       01 TS-AI27-APPLI.                                                        
           05 TS-AI27-LONG PIC S9(5) COMP-3 VALUE +66.                          
           05 TS-AI27-DATA.                                                     
               10 TS-AI27-TOPMAJ PIC X.                                         
               10 TS-AI27-ENR.                                                  
                   15 TS-AI27-CDESCRIPTIF PIC X(5).                             
      *            15 TS-AI27-FDFNUM PIC S9(5) COMP-3.                          
                   15 TS-AI27-FDFNUM PIC X(5).                                  
                   15 TS-AI27-DSYST PIC S9(13) COMP-3.                          
               10 TS-AI27-FDTKDS PIC X(50).                                     
                                                                                
