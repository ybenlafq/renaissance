      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00100000
      * COMMAREA SPECIFIQUE PRG  TGF37                   TR: GF37  *    00110000
      *           SUIVI DES COMMANDES PAR STATUT/QUALIFIANT        *    00120000
      **************************************************************    00130000
      * ZONES RESERVEES APPLICATIVES (DISPO 6814)-------------------    00140021
          02 COMM-GF37-APPLI REDEFINES COMM-GF00-APPLI.                 00150000
      *--- ZONE DE RECHERCHE                                            00160000
             05 COMM-GF37-NDEPOT1              PIC X(3).                00171004
             05 COMM-GF37-CHEFPROD             PIC X(5).                00180000
             05 COMM-GF37-DDEBUT-D             PIC X(10).               00181012
             05 COMM-GF37-DDEBUT               PIC X(8).                00190000
             05 COMM-GF37-DFIN-D               PIC X(10).               00191012
             05 COMM-GF37-DFIN                 PIC X(8).                00200000
             05 COMM-GF37-STATUT               PIC X(6).                00230008
             05 COMM-GF37-APPLI-BIS.                                    00240010
      *--- ZONE POUR GESTION DE LA PAGE                                 00250000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         10 COMM-GF37-IND-TS-MAX        PIC S9(4) COMP.          00251010
      *--                                                                       
                10 COMM-GF37-IND-TS-MAX        PIC S9(4) COMP-5.                
      *}                                                                        
                10 COMM-GF37-PAGE              PIC 9(03).               00260010
                10 COMM-GF37-PAGE-MAX          PIC 9(03).               00270010
                10 COMM-GF37-TAB               PIC 9(04).               00280022
                10 COMM-GF37-TAB-MAX           PIC 9(04).               00281022
      *--- ZONE AFFICHE                                                 00290001
                10 COMM-GF37-DLIVR1-D          PIC X(8).                00300019
                10 COMM-GF37-DLIVR1            PIC X(8).                00300112
                10 COMM-GF37-DLIVR2-D          PIC X(8).                00301019
                10 COMM-GF37-DLIVR2            PIC X(8).                00301112
                10 COMM-GF37-DLIVR3-D          PIC X(8).                00302019
                10 COMM-GF37-DLIVR3            PIC X(8).                00303012
                10 COMM-GF37-TAB-1.                                     00310011
                   15 COMM-GF37-TAB-DETAIL OCCURS 14.                   00320011
                      20 COMM-GF37-CCOLOR      PIC X.                   00330010
                      20 COMM-GF37-CBASE       PIC X.                   00340010
                      20 COMM-GF37-NCDE        PIC X(7).                00340110
                      20 COMM-GF37-NDEPOT      PIC X(3).                00340210
                      20 COMM-GF37-NCODIC      PIC X(7).                00344010
                      20 COMM-GF37-LREF        PIC X(14).               00350023
                      20 COMM-GF37-LMARQ       PIC X(5).                00351023
                      20 COMM-GF37-QTOT        PIC ZZZZZ.               00360016
                      20 COMM-GF37-QREC        PIC ZZZZZ.               00370016
                      20 COMM-GF37-QSOLDE      PIC ZZZZZ.               00380016
                      20 COMM-GF37-QTE1        PIC ZZZZZ.               00390016
                      20 COMM-GF37-QTE2        PIC ZZZZZ.               00400016
                      20 COMM-GF37-QTE3        PIC ZZZZZ.               00410016
                10 COMM-GF37-TAB-2.                                     00420011
                   15 COMM-GF37-TAB-LIVRAISON OCCURS 99.                00430011
                      20 COMM-GF37-TAB-DLIVR   PIC X(8).                00440011
                      20 COMM-GF37-TAB-DLIVR-D PIC X(8).                00450020
             05 FILLER                         PIC X(4164).             00460023
                                                                                
