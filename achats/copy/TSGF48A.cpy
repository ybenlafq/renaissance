      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      * TS TGF48A : QUOTA PLANNING RAYON                                        
      *                                                                         
       01  TS-GF48A.                                                            
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03  TS-GF48A-LONG             PIC  S9(4) COMP VALUE +500.            
      *--                                                                       
           03  TS-GF48A-LONG             PIC  S9(4) COMP-5 VALUE +500.          
      *}                                                                        
      *                                                                         
           03  TS-GF48A-DONNEES.                                                
             05 TS-GF48A-FAM          OCCURS 100.                               
      *                                                                         
               10 TS-GF48A-CFAM         PIC X(5).                               
      *                                                                         
                                                                                
