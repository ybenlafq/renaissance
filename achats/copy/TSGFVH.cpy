      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE POUR LA SAUVEGARDE DES UO PAR CODIC DE RTGF30  *  00020001
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
       01  TS-LONG              PIC S9(2) COMP-3 VALUE 31.              00080001
       01  TS-DONNEES.                                                  00090000
              10 TS-NCDE        PIC X(07).                              00100001
              10 TS-NCODIC      PIC X(07).                              00120001
              10 TS-DLIVRAISON  PIC X(08).                              00121001
              10 TS-QUOLIVR     PIC S9(5) COMP-3.                       00122001
              10 TS-QUOPALETTIS PIC S9(5) COMP-3.                               
              10 TS-QCDE        PIC S9(5) COMP-3.                               
                                                                                
