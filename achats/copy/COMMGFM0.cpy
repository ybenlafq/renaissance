      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      **************************************************************            
      *  COMMAREA SPECIFIQUE MODULE DE CALCUL DU NOMBRE D' UO      *            
      *        TR  :  GF00   EMISSION DE LA COMMANDE               *            
      *        MD  :  MGF00  MODULE DE CALCUL DU NOMBRE D' UO      *            
      **************************************************************            
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ----------------------------- 200          
      *                                                                         
      * PROGRAMME  MGF00  :  MODULE DE CALCUL DU NOMBRE D' UO      *            
      *                                                                         
           02 COMM-GFM0-APPLI.                                                  
      *---------------------------------------ZONES EN ENTREE DU MODULE         
      *                                       50 CARACTERES                     
              03 COMM-GFM0-ENTREE.                                              
      *---------------------------------------NUMERO DE CODIC                   
                 04 COMM-GFM0-NCODIC          PIC    X(7).                      
      *---------------------------------------DATE SOUS FORME SSAAMMJJ          
                 04 COMM-GFM0-DATE            PIC    X(8).                      
      *---------------------------------------QUANTITE POUR CETTE DATE          
                 04 COMM-GFM0-QTE             PIC    9(5) COMP-3.               
      *---------------------------------------NUMERO DE SEMAINE                 
                 04 COMM-GFM0-NSEMAINE        PIC    X(2).                      
      *---------------------------------------DATE SOUS FORM (SSAA)             
                 04 COMM-GFM0-DANNEE          PIC    X(4).                      
      *---------------------------------------NUMERO DE DEPOT                   
                04 COMM-GFM0-NSOCDEP.                                           
                   05 COMM-GFM0-NSOCLIVR      PIC    X(3).                      
                   05 COMM-GFM0-NDEPOT        PIC    X(3).                      
      *---------------------------------------MODE DE STOCKAGE ARTICLE          
                 04 COMM-GFM0-CMODSTOCK       PIC    X(5).                      
      *---------------------------------------QUOTA RESTANT DISPONIBLE          
      *                                       EN ENTREE                         
                 04 COMM-GFM0-QUOTA-ENTREE    PIC    S9(7) COMP-3.              
      *---------------------------------------NUMERO DE LIVRAISON               
                 04 COMM-GFM0-NLIVRAISON      PIC    X(7).                      
      *---------------------------------------CODE RETOUR                       
                 04 COMM-GFM0-CODRET          PIC    X.                         
      *---------------------------------------CODE RETOUR SQL                   
                 04 COMM-GFM0-SQLCODE         PIC    S9(9).                     
      *---------------------------------------RESERVE                           
                 04 COMM-GFM0-RESERVE         PIC    X(04).                     
      *---------------------------------------ZONES EN SORTIE DU MODULE         
      *                                       50 CARACTERES                     
              03 COMM-GFM0-SORTIE.                                              
      *---------------------------------------NOMBRE D' UO NECESSAIRES          
                 04 COMM-GFM0-NBRUO           PIC    S9(5) COMP-3.              
      *---------------------------------------QUOTA RESTANT DISPONIBLE          
      *                                       EN SORTIE                         
                 04 COMM-GFM0-QUOTA-SORTIE    PIC    S9(7) COMP-3.              
      *---------------------------------------QUOTA INITIAL (REMPLI SI          
      *                                       COMM-GFM0-QUOTA-ENTREE            
      *                                       = '99999')                        
                 04 COMM-GFM0-QUOTA-INIT      PIC    S9(7) COMP-3.              
      *---------------------------------------TEMPS DE PALETISATION             
                 04 COMM-GFM0-QPALETTE        PIC    S9(5) COMP-3.              
      *---------------------------------------TYPE DE QUOTA                     
                 04 COMM-GFM0-CQUOTA          PIC    X(5).                      
      *---------------------------------------DISPO INIT JOUR                   
                 04 COMM-GFM0-QUOTA-JOURS.                                      
                    05 COMM-GFM0-QUOTA-INITJ  PIC S9(7) COMP-3 OCCURS 7.        
      *---------------------------------------RESERVE                           
                 04 COMM-GFM0-RESERVE1        PIC    X(03).                     
      *---------------------------------------FILLER                            
              03  COMM-GFM0-QCOLIRECEPT       PIC S9(5)  COMP-3.                
              03  COMM-GFM0-QNBPVSOL          PIC S9(3)  COMP-3.                
              03  COMM-GFM0-QNBPRACK          PIC S9(3)  COMP-3.                
              03  COMM-GFM0-FILLER            PIC X(93).                        
                                                                                
