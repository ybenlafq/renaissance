      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00000010
      * COMMAREA SPECIFIQUE PRG TGF08                    TR: GF08  *    00000020
      *     COMMENTAIRES DES BONS DE COMMANDE                           00000030
      **************************************************************    00000040
      * ZONES RESERVEES APPLICATIVES ---------------------------- 7421          
      *                                                                 00000060
          02 COMM-GF08-APPLI REDEFINES COMM-GF00-APPLI.                 00000070
             03 COMM-GF08-NSOC         PIC X(3).                        00000080
             03 COMM-GF08-LSOC         PIC X(20).                       00000090
      ***************************************************************** 00000095
                                                                                
