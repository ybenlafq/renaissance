      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGR9500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGR9500                         
      **********************************************************                
       01  RVGR9500.                                                            
           02  GR95-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  GR95-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GR95-NRECQUAI                                                    
               PIC X(0007).                                                     
           02  GR95-NCDE                                                        
               PIC X(0007).                                                     
           02  GR95-NCODIC                                                      
               PIC X(0007).                                                     
           02  GR95-NREC                                                        
               PIC X(0007).                                                     
           02  GR95-DJRECQUAI                                                   
               PIC X(0008).                                                     
           02  GR95-QRECQUAI                                                    
               PIC S9(5) COMP-3.                                                
           02  GR95-NDOSLM                                                      
               PIC X(0015).                                                     
           02  GR95-NBL                                                         
               PIC X(0010).                                                     
           02  GR95-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GR95-CSTATUT                                                     
               PIC X(0002).                                                     
           02  GR95-LCOMMENT                                                    
               PIC X(0020).                                                     
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGR9500                                  
      **********************************************************                
       01  RVGR9500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR95-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR95-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR95-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR95-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR95-NRECQUAI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR95-NRECQUAI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR95-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR95-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR95-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR95-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR95-NREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR95-NREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR95-DJRECQUAI-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR95-DJRECQUAI-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR95-QRECQUAI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR95-QRECQUAI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR95-NDOSLM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR95-NDOSLM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR95-NBL-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR95-NBL-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR95-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR95-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR95-CSTATUT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR95-CSTATUT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR95-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GR95-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
