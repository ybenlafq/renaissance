      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00001000
      * COMMAREA SPECIFIQUE PRG TGR21 (TGR00 -> MENU)    TR: GR00  *    00002000
      *               GESTION DES RECEPTIONS                       *    00003000
      *                                                                 00004000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3710  00005000
      *                                                                 00006000
      *        TRANSACTION GR21 : INTERROGATION RECEPTIONS A QUAI  *    00007000
      *                                                                 00008000
          03 COMM-GR21-APPLI REDEFINES COMM-GR01-APPLI.                 00009017
      *------------------------------ ZONE DONNEES TGR21                00010000
             04 COMM-GR21-DONNEES-TGR21.                                00020017
                05 COMM-GR21-NSOCIETE          PIC XXX.                 00040017
                05 COMM-GR21-LSOCIETE          PIC X(20).               00060017
                05 COMM-GR21-NDEPOT            PIC XXX.                 00080017
                05 COMM-GR21-LDEPOT            PIC X(20).               00100017
                05 COMM-GR21-CETAT             PIC X(20).               00120017
                05 COMM-GR21-CTYPAGE           PIC X(2).                00140017
                05 COMM-GR21-LQREC             PIC X(16).               00200017
                05 COMM-GR21-NRECQUAI          PIC X(7).                00202017
                05 COMM-GR21-DJRECQUAI         PIC X(10).               00220017
                05 COMM-GR21-NREC              PIC X(7).                00222017
                05 COMM-GR21-DSAISREC          PIC X(10).               00224017
                05 COMM-GR21-LCOMMENT          PIC X(34).               00380017
                05 COMM-GR21-NBLIGNES          PIC 9(5).                00390317
                05 COMM-GR21-NBPRODUITS        PIC 9(5).                00390517
                05 COMM-GR21-NBPIECES          PIC 9(5).                00400017
                05 COMM-GR21-PAGE              PIC 9(4).                00402017
                05 COMM-GR21-NBPAGES           PIC 9(4).                00404017
E0723           05 COMM-GR21-NRECQORIG         PIC X(7).                00650119
MAXDEP*      04 COMM-GR21-LIBRE                PIC X(3424).             00650219
MAXDEP*      04 COMM-GR21-LIBRE                PIC X(2284).             00651019
E0721        04 COMM-GR21-LIBRE                PIC X(2277).             00652019
      ***************************************************************** 00660000
                                                                                
