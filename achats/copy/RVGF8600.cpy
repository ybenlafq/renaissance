      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGF8600                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF8600                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF8600.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF8600.                                                            
      *}                                                                        
           10 GF86-NSOCDEPOT            PIC X(3).                               
           10 GF86-NDEPOT               PIC X(3).                               
           10 GF86-NCODIC               PIC X(7).                               
           10 GF86-WDISPO               PIC X(1).                               
           10 GF86-DDISPO               PIC X(8).                               
           10 GF86-DSYST                PIC S9(13)V USAGE COMP-3.               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGF8600                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF8600-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF8600-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF86-NSOCDEPOT-F          PIC S9(4) COMP.                         
      *--                                                                       
           10 GF86-NSOCDEPOT-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF86-NDEPOT-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF86-NDEPOT-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF86-NCODIC-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF86-NCODIC-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF86-WDISPO-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF86-WDISPO-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF86-DDISPO-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF86-DDISPO-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF86-DSYST-F              PIC S9(4) COMP.                         
      *                                                                         
      *--                                                                       
           10 GF86-DSYST-F              PIC S9(4) COMP-5.                       
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
