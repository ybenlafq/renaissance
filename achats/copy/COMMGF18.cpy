      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************    00010003
      * COMMAREA SPECIFIQUE PRG TGF18                     TR: GF18      00020003
      *           TR: GF00                                              00030003
      **************************************************************    00040003
          02 COMM-GF18-APPLI REDEFINES COMM-GF00-APPLI.                 00050003
      *------------------------------ PARTIE COMMUNE 7421C- 1517C       00060003
             03 COMM-ZONE-EVOLUTION      PIC X(1517).                   00070003
      *------                                                           00080003
             03 COMM-GF18-MAITRE         PIC X(01).                     00090003
             03 COMM-GF18-DVALIDITE      PIC X(08).                     00100003
             03 COMM-GF18-IP             PIC 9(03).                     00110003
             03 COMM-GF18-IP-MAX         PIC 9(03).                     00120003
             03 COMM-GF18-IL-MAX         PIC 9(05).                     00130003
E0191.*--- ZONE D'AFFICHAGE VARIABLE                                    00140003
             03 COMM-GF18-LIGNES.                                       00150003
                04 COMM-GF18-LIGNE11         PIC X(24).                 00160003
                04 COMM-GF18-LIGNE21         PIC X(24).                 00170003
      *------                                                           00180003
      *------------------------------ LIBRE 5904 - 20                   00190003
CD    *      03 COMM-GF18-FILLER         PIC X(5884).                   00200003
      *                                                                 00210003
      ***************************************************************** 00220003
                                                                                
