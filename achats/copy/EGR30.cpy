      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR30   EGR30                                              00000020
      ***************************************************************** 00000030
       01   EGR30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * N� PAGE                                                         00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MWPAGEI   PIC X(3).                                       00000200
      * SOCIETE                                                         00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
      * LIBELLE SOCIETE                                                 00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MLSOCI    PIC X(20).                                      00000300
      * ENTREPOT                                                        00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENPOTL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MNENPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENPOTF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MNENPOTI  PIC X(3).                                       00000350
      * LIB. ENTREPOT                                                   00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENPOTL  COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MLENPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLENPOTF  PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MLENPOTI  PIC X(20).                                      00000400
           02 MLIGNEI OCCURS   12 TIMES .                               00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSELL  COMP PIC S9(4).                                 00000420
      *--                                                                       
             03 MWSELL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MWSELF  PIC X.                                          00000430
             03 FILLER  PIC X(4).                                       00000440
             03 MWSELI  PIC X.                                          00000450
      * DATE RECEPTION                                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDRECEPL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MDRECEPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDRECEPF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MDRECEPI     PIC X(8).                                  00000500
      * NUM. RECEP. QUAI                                                00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNRECEPL     COMP PIC S9(4).                            00000520
      *--                                                                       
             03 MNRECEPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNRECEPF     PIC X.                                     00000530
             03 FILLER  PIC X(4).                                       00000540
             03 MNRECEPI     PIC X(7).                                  00000550
      * CODE LIVREUR                                                    00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCLIVREL     COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MCLIVREL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCLIVREF     PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MCLIVREI     PIC X(5).                                  00000600
      * LIBELLE LIVREUR                                                 00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLLIVREL     COMP PIC S9(4).                            00000620
      *--                                                                       
             03 MLLIVREL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLLIVREF     PIC X.                                     00000630
             03 FILLER  PIC X(4).                                       00000640
             03 MLLIVREI     PIC X(20).                                 00000650
      * COMMENTAIRE                                                     00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMMEL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MLCOMMEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLCOMMEF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLCOMMEI     PIC X(20).                                 00000700
      * ZONE CMD AIDA                                                   00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MZONCMDI  PIC X(15).                                      00000750
      * MESSAGE ERREUR                                                  00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MLIBERRI  PIC X(58).                                      00000800
      * CODE TRANSACTION                                                00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCODTRAI  PIC X(4).                                       00000850
      * CICS DE TRAVAIL                                                 00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      * NETNAME                                                         00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MNETNAMI  PIC X(8).                                       00000950
      * CODE TERMINAL                                                   00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MSCREENI  PIC X(5).                                       00001000
      ***************************************************************** 00001010
      * SDF: EGR30   EGR30                                              00001020
      ***************************************************************** 00001030
       01   EGR30O REDEFINES EGR30I.                                    00001040
           02 FILLER    PIC X(12).                                      00001050
      * DATE DU JOUR                                                    00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MDATJOUA  PIC X.                                          00001080
           02 MDATJOUC  PIC X.                                          00001090
           02 MDATJOUP  PIC X.                                          00001100
           02 MDATJOUH  PIC X.                                          00001110
           02 MDATJOUV  PIC X.                                          00001120
           02 MDATJOUO  PIC X(10).                                      00001130
      * HEURE                                                           00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
      * N� PAGE                                                         00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MWPAGEA   PIC X.                                          00001240
           02 MWPAGEC   PIC X.                                          00001250
           02 MWPAGEP   PIC X.                                          00001260
           02 MWPAGEH   PIC X.                                          00001270
           02 MWPAGEV   PIC X.                                          00001280
           02 MWPAGEO   PIC X(3).                                       00001290
      * SOCIETE                                                         00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MNSOCA    PIC X.                                          00001320
           02 MNSOCC    PIC X.                                          00001330
           02 MNSOCP    PIC X.                                          00001340
           02 MNSOCH    PIC X.                                          00001350
           02 MNSOCV    PIC X.                                          00001360
           02 MNSOCO    PIC X(3).                                       00001370
      * LIBELLE SOCIETE                                                 00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MLSOCA    PIC X.                                          00001400
           02 MLSOCC    PIC X.                                          00001410
           02 MLSOCP    PIC X.                                          00001420
           02 MLSOCH    PIC X.                                          00001430
           02 MLSOCV    PIC X.                                          00001440
           02 MLSOCO    PIC X(20).                                      00001450
      * ENTREPOT                                                        00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MNENPOTA  PIC X.                                          00001480
           02 MNENPOTC  PIC X.                                          00001490
           02 MNENPOTP  PIC X.                                          00001500
           02 MNENPOTH  PIC X.                                          00001510
           02 MNENPOTV  PIC X.                                          00001520
           02 MNENPOTO  PIC X(3).                                       00001530
      * LIB. ENTREPOT                                                   00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MLENPOTA  PIC X.                                          00001560
           02 MLENPOTC  PIC X.                                          00001570
           02 MLENPOTP  PIC X.                                          00001580
           02 MLENPOTH  PIC X.                                          00001590
           02 MLENPOTV  PIC X.                                          00001600
           02 MLENPOTO  PIC X(20).                                      00001610
           02 MLIGNEO OCCURS   12 TIMES .                               00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MWSELA  PIC X.                                          00001640
             03 MWSELC  PIC X.                                          00001650
             03 MWSELP  PIC X.                                          00001660
             03 MWSELH  PIC X.                                          00001670
             03 MWSELV  PIC X.                                          00001680
             03 MWSELO  PIC X.                                          00001690
      * DATE RECEPTION                                                  00001700
             03 FILLER       PIC X(2).                                  00001710
             03 MDRECEPA     PIC X.                                     00001720
             03 MDRECEPC     PIC X.                                     00001730
             03 MDRECEPP     PIC X.                                     00001740
             03 MDRECEPH     PIC X.                                     00001750
             03 MDRECEPV     PIC X.                                     00001760
             03 MDRECEPO     PIC X(8).                                  00001770
      * NUM. RECEP. QUAI                                                00001780
             03 FILLER       PIC X(2).                                  00001790
             03 MNRECEPA     PIC X.                                     00001800
             03 MNRECEPC     PIC X.                                     00001810
             03 MNRECEPP     PIC X.                                     00001820
             03 MNRECEPH     PIC X.                                     00001830
             03 MNRECEPV     PIC X.                                     00001840
             03 MNRECEPO     PIC X(7).                                  00001850
      * CODE LIVREUR                                                    00001860
             03 FILLER       PIC X(2).                                  00001870
             03 MCLIVREA     PIC X.                                     00001880
             03 MCLIVREC     PIC X.                                     00001890
             03 MCLIVREP     PIC X.                                     00001900
             03 MCLIVREH     PIC X.                                     00001910
             03 MCLIVREV     PIC X.                                     00001920
             03 MCLIVREO     PIC X(5).                                  00001930
      * LIBELLE LIVREUR                                                 00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MLLIVREA     PIC X.                                     00001960
             03 MLLIVREC     PIC X.                                     00001970
             03 MLLIVREP     PIC X.                                     00001980
             03 MLLIVREH     PIC X.                                     00001990
             03 MLLIVREV     PIC X.                                     00002000
             03 MLLIVREO     PIC X(20).                                 00002010
      * COMMENTAIRE                                                     00002020
             03 FILLER       PIC X(2).                                  00002030
             03 MLCOMMEA     PIC X.                                     00002040
             03 MLCOMMEC     PIC X.                                     00002050
             03 MLCOMMEP     PIC X.                                     00002060
             03 MLCOMMEH     PIC X.                                     00002070
             03 MLCOMMEV     PIC X.                                     00002080
             03 MLCOMMEO     PIC X(20).                                 00002090
      * ZONE CMD AIDA                                                   00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MZONCMDA  PIC X.                                          00002120
           02 MZONCMDC  PIC X.                                          00002130
           02 MZONCMDP  PIC X.                                          00002140
           02 MZONCMDH  PIC X.                                          00002150
           02 MZONCMDV  PIC X.                                          00002160
           02 MZONCMDO  PIC X(15).                                      00002170
      * MESSAGE ERREUR                                                  00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MLIBERRA  PIC X.                                          00002200
           02 MLIBERRC  PIC X.                                          00002210
           02 MLIBERRP  PIC X.                                          00002220
           02 MLIBERRH  PIC X.                                          00002230
           02 MLIBERRV  PIC X.                                          00002240
           02 MLIBERRO  PIC X(58).                                      00002250
      * CODE TRANSACTION                                                00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
      * CICS DE TRAVAIL                                                 00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MCICSA    PIC X.                                          00002360
           02 MCICSC    PIC X.                                          00002370
           02 MCICSP    PIC X.                                          00002380
           02 MCICSH    PIC X.                                          00002390
           02 MCICSV    PIC X.                                          00002400
           02 MCICSO    PIC X(5).                                       00002410
      * NETNAME                                                         00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MNETNAMA  PIC X.                                          00002440
           02 MNETNAMC  PIC X.                                          00002450
           02 MNETNAMP  PIC X.                                          00002460
           02 MNETNAMH  PIC X.                                          00002470
           02 MNETNAMV  PIC X.                                          00002480
           02 MNETNAMO  PIC X(8).                                       00002490
      * CODE TERMINAL                                                   00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MSCREENA  PIC X.                                          00002520
           02 MSCREENC  PIC X.                                          00002530
           02 MSCREENP  PIC X.                                          00002540
           02 MSCREENH  PIC X.                                          00002550
           02 MSCREENV  PIC X.                                          00002560
           02 MSCREENO  PIC X(5).                                       00002570
                                                                                
