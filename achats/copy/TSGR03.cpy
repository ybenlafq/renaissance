      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * TS SPECIFIQUE TGR03                                        *            
      *       TR : GR00  QUOTA DE RECEPTION*                                    
      *       PG : TGR03 SYNTHESE DES QUOTAS                       *            
      **************************************************************            
      *                                                                         
      * ZONES RESERVEES APPLICATIVES -----------------------------              
      *                                                                         
       01  TS-LONG            PIC S9(4) COMP-3 VALUE 450.                       
       01  TS-DONNEES.                                                          
           05 TS-LIGNE   OCCURS 10.                                             
              10 TS-QUOTA           PIC X(5).                                   
              10 TS-LQUOTA          PIC X(20).                                  
              10 TS-QTOT            PIC S9(8) COMP-3.                           
              10 TS-QDISP           PIC S9(8) COMP-3.                           
              10 TS-QPRIS           PIC S9(8) COMP-3.                           
              10 TS-QREC            PIC S9(8) COMP-3.                           
                                                                                
