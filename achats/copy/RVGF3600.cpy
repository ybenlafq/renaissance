      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGF3600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF3600                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF3600.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF3600.                                                            
      *}                                                                        
           02  GF36-DANNEE                                                      
               PIC X(0004).                                                     
           02  GF36-DSEMAINE                                                    
               PIC X(0002).                                                     
           02  GF36-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  GF36-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GF36-CQUOTA                                                      
               PIC X(0005).                                                     
           02  GF36-QUOPREV1                                                    
               PIC S9(7) COMP-3.                                                
           02  GF36-QCDE1                                                       
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOPREV2                                                    
               PIC S9(7) COMP-3.                                                
           02  GF36-QCDE2                                                       
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOPREV3                                                    
               PIC S9(7) COMP-3.                                                
           02  GF36-QCDE3                                                       
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOPREV4                                                    
               PIC S9(7) COMP-3.                                                
           02  GF36-QCDE4                                                       
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOPREV5                                                    
               PIC S9(7) COMP-3.                                                
           02  GF36-QCDE5                                                       
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOPREV6                                                    
               PIC S9(7) COMP-3.                                                
           02  GF36-QCDE6                                                       
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOPREV7                                                    
               PIC S9(7) COMP-3.                                                
           02  GF36-QCDE7                                                       
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOREC1                                                     
               PIC S9(7) COMP-3.                                                
           02  GF36-QREC1                                                       
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOREC2                                                     
               PIC S9(7) COMP-3.                                                
           02  GF36-QREC2                                                       
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOREC3                                                     
               PIC S9(7) COMP-3.                                                
           02  GF36-QREC3                                                       
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOREC4                                                     
               PIC S9(7) COMP-3.                                                
           02  GF36-QREC4                                                       
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOREC5                                                     
               PIC S9(7) COMP-3.                                                
           02  GF36-QREC5                                                       
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOREC6                                                     
               PIC S9(7) COMP-3.                                                
           02  GF36-QREC6                                                       
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOREC7                                                     
               PIC S9(7) COMP-3.                                                
           02  GF36-QREC7                                                       
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOTA                                                       
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOTA1                                                      
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOTA2                                                      
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOTA3                                                      
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOTA4                                                      
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOTA5                                                      
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOTA6                                                      
               PIC S9(7) COMP-3.                                                
           02  GF36-QUOTA7                                                      
               PIC S9(7) COMP-3.                                                
           02  GF36-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGF3600                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF3600-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF3600-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-DANNEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-DANNEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-DSEMAINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-DSEMAINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-CQUOTA-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-CQUOTA-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOPREV1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOPREV1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QCDE1-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QCDE1-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOPREV2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOPREV2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QCDE2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QCDE2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOPREV3-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOPREV3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QCDE3-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QCDE3-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOPREV4-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOPREV4-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QCDE4-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QCDE4-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOPREV5-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOPREV5-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QCDE5-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QCDE5-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOPREV6-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOPREV6-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QCDE6-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QCDE6-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOPREV7-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOPREV7-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QCDE7-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QCDE7-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOREC1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOREC1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QREC1-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QREC1-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOREC2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOREC2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QREC2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QREC2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOREC3-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOREC3-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QREC3-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QREC3-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOREC4-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOREC4-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QREC4-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QREC4-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOREC5-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOREC5-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QREC5-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QREC5-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOREC6-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOREC6-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QREC6-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QREC6-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOREC7-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOREC7-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QREC7-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QREC7-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOTA-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOTA-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOTA1-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOTA1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOTA2-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOTA2-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOTA3-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOTA3-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOTA4-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOTA4-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOTA5-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOTA5-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOTA6-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOTA6-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-QUOTA7-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-QUOTA7-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF36-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF36-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
