      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGR07 (TGR00 -> MENU)    TR: GR00  *    00020000
      *               GESTION DES RECEPTIONS                       *    00030000
      *                                                            *    00040004
      * ZONES RESERVEES APPLICATIVES ---------------------------- 2456  00050005
      *                                                                 00060000
      *        TRANSACTION GR07 :                                       00070000
      *                                                                 00080000
      *                                                                 00081003
          03 COMM-GR07-APPLI REDEFINES COMM-GR01-APPLI.                 00090000
      *------------------------------ ZONE DONNEES TGR07                00100000
             04 COMM-GR07-DONNEES-TGR07.                                00110000
                05 COMM-GR07-NSOCIETE          PIC X(3).                00120000
                05 COMM-GR07-LSOCIETE          PIC X(20).               00121000
                05 COMM-GR07-NDEPOT            PIC X(3).                00122000
                05 COMM-GR07-LDEPOT            PIC X(20).               00123000
                05 COMM-GR07-MIN-DATE          PIC X(8).                00124000
                05 COMM-GR07-LET-SITE          PIC X(1).                00125000
                05 COMM-GR07-NRECQUAI          PIC X(7).                00126001
                05 COMM-GR07-DJRECQUAI         PIC X(7).                00127001
                05 COMM-GR07-PAGE              PIC 9(4).                00128000
                05 COMM-GR07-NBPAGES           PIC 9(4).                00129000
                05 COMM-GR07-NBPAGES-X         PIC 9(4).                00130000
                05 COMM-GR07-NBLIGNES          PIC 9(5).                00160000
                05 COMM-GR07-TSLONG            PIC 9(1).                00161000
                   88 TS07-PAS-TROP-LONGUE           VALUE 0.           00162000
                   88 TS07-TROP-LONGUE               VALUE 1.           00163000
                05 COMM-GR07-MODIF             PIC 9(1).                00164000
                   88 SELECTION-MODIFIEE             VALUE 0.           00165000
                   88 SELECTION-NON-MODIFIEE         VALUE 1.           00166000
                05 COMM-GR07-LIGNE-SELECT      PIC 9(1).                00167000
                   88 LIGNE-SELECT                   VALUE 0.           00168000
                   88 AUCUNE-LIGNE-SELECT            VALUE 1.           00169000
                05 COMM-GR07-PF5               PIC X.                   00212200
                   88 COMM-GR07-PF5-1X               VALUE '1'.         00212300
                   88 COMM-GR07-PF5-2X               VALUE '2'.         00212400
                   88 COMM-GR07-VALID-OK             VALUE '3'.         00212500
                05 COMM-GR07-RECQUAI-TROUVE    PIC 9(1).                00212602
                   88 RECQUAI-PARTIEL-TROUVE         VALUE 0.           00212702
                   88 RECQUAI-PARTIEL-NON-TROUVE     VALUE 1.           00212802
                05 COMM-GR07-MESSAGE           PIC X(70).               00212900
      *------------------------------ ZONE LIBRE                        00213000
MAXDEP*      04 COMM-GR07-LIBRE          PIC X(3435).                   00213105
MAXDEP       04 COMM-GR07-LIBRE          PIC X(2295).                   00213205
      ***************************************************************** 00214000
                                                                                
