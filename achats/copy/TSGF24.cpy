      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : TS LIGNES DE COMMANDE PAR DEMANDEUR CODIC              *         
      *        CREE PAR MGF24                                         *         
      *****************************************************************         
      *                                                               *         
       01  TS-GF24.                                                             
           02 TS-GF24-GESTION.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TS-GF24-POS                  PIC S9(4) COMP.                  
      *--                                                                       
               05 TS-GF24-POS                  PIC S9(4) COMP-5.                
      *}                                                                        
      *--------------------------------  STRUCTURE TS                           
           02 TS-GF24-DONNEES.                                                  
             03 TS-GF24-ENR.                                                    
               05 TS-GF24-NCODIC               PIC X(7).                        
               05 TS-GF24-NSOCIETE             PIC X(3).                        
               05 TS-GF24-NSOCLIVR             PIC X(3).                        
               05 TS-GF24-NDEPOT               PIC X(3).                        
               05 TS-GF24-QTE                  PIC 9(5) COMP-3.                 
               05 TS-GF24-DATE                 PIC X(8).                        
               05 TS-GF24-NENTCDE              PIC X(5).                        
               05 TS-GF24-LENTCDE              PIC X(20).                       
               05 TS-GF24-NLIEU                PIC X(3).                        
               05 TS-GF24-CHEFPROD             PIC X(5).                        
               05 TS-GF24-CTYPCDE              PIC X(05).                       
               05 TS-GF24-CMODSTOCK            PIC X(5).                        
               05 TS-GF24-NLIGNE               PIC X(2).                        
               05 TS-GF24-NPAGE                PIC X(3).                        
               05 TS-GF24-CINTERLOCUT          PIC X(5).                        
               05 TS-GF24-LINTERLOCUT          PIC X(20).                       
               05 TS-GF24-CQUALIF              PIC X(6).                        
               05 TS-GF24-NCDE                 PIC X(7).                        
               05 TS-GF24-DANNEE               PIC X(4).                        
               05 TS-GF24-SEMAINE              PIC X(2).                        
               05 TS-GF24-DETAT                PIC X(8).                        
               05 TS-GF24-NSEQ                 PIC X(7).                        
               05 TS-GF24-NBRUO                PIC S9(5) COMP-3.                
               05 TS-GF24-QPALETTE             PIC S9(5) COMP-3.                
NSKEP          05 TS-GF24-CFEXT                PIC X(1).                        
                                                                                
