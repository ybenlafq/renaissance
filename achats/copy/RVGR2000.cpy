      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGR2000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGR2000                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGR2000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGR2000.                                                            
      *}                                                                        
           02  GR20-DMVTREC                                                     
               PIC X(0008).                                                     
           02  GR20-NREC                                                        
               PIC X(0007).                                                     
           02  GR20-NCDE                                                        
               PIC X(0007).                                                     
           02  GR20-NCODIC                                                      
               PIC X(0007).                                                     
           02  GR20-CTYPMVT                                                     
               PIC X(0003).                                                     
           02  GR20-QREC                                                        
               PIC S9(5) COMP-3.                                                
           02  GR20-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GR20-NBL                                                         
               PIC X(0010).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGR2000                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGR2000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGR2000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR20-DMVTREC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR20-DMVTREC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR20-NREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR20-NREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR20-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR20-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR20-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR20-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR20-CTYPMVT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR20-CTYPMVT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR20-QREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR20-QREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR20-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR20-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR20-NBL-F                                                       
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GR20-NBL-F                                                       
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
