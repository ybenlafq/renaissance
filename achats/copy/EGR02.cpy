      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: TGR02   TGR02                                              00000020
      ***************************************************************** 00000030
       01   EGR02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCIETEI     PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBLIEUL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLIBLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBLIEUF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLIBLIEUI      PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPRECL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MTYPRECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPRECF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MTYPRECI  PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBRECL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLIBRECL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBRECF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLIBRECI  PIC X(20).                                      00000370
           02 M2I OCCURS   12 TIMES .                                   00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSEMAINEL   COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MNSEMAINEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNSEMAINEF   PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNSEMAINEI   PIC X(6).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJOUR1L     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MDJOUR1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDJOUR1F     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MDJOUR1I     PIC X(4).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFERM1L     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MCFERM1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCFERM1F     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MCFERM1I     PIC X.                                     00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJOUR2L     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MDJOUR2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDJOUR2F     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MDJOUR2I     PIC X(4).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFERM2L     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MCFERM2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCFERM2F     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCFERM2I     PIC X.                                     00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJOUR3L     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MDJOUR3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDJOUR3F     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MDJOUR3I     PIC X(4).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFERM3L     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCFERM3L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCFERM3F     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCFERM3I     PIC X.                                     00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJOUR4L     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MDJOUR4L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDJOUR4F     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MDJOUR4I     PIC X(4).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFERM4L     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MCFERM4L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCFERM4F     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MCFERM4I     PIC X.                                     00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJOUR5L     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MDJOUR5L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDJOUR5F     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MDJOUR5I     PIC X(4).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFERM5L     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MCFERM5L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCFERM5F     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCFERM5I     PIC X.                                     00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJOUR6L     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MDJOUR6L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDJOUR6F     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MDJOUR6I     PIC X(4).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFERM6L     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MCFERM6L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCFERM6F     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MCFERM6I     PIC X.                                     00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJOUR7L     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MDJOUR7L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDJOUR7F     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MDJOUR7I     PIC X(4).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFERM7L     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MCFERM7L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCFERM7F     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MCFERM7I     PIC X.                                     00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MZONCMDI  PIC X(15).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MLIBERRI  PIC X(58).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCODTRAI  PIC X(4).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCICSI    PIC X(5).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MNETNAMI  PIC X(8).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MSCREENI  PIC X(4).                                       00001220
      ***************************************************************** 00001230
      * SDF: TGR02   TGR02                                              00001240
      ***************************************************************** 00001250
       01   EGR02O REDEFINES EGR02I.                                    00001260
           02 FILLER    PIC X(12).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MDATJOUA  PIC X.                                          00001290
           02 MDATJOUC  PIC X.                                          00001300
           02 MDATJOUP  PIC X.                                          00001310
           02 MDATJOUH  PIC X.                                          00001320
           02 MDATJOUV  PIC X.                                          00001330
           02 MDATJOUO  PIC X(10).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MTIMJOUA  PIC X.                                          00001360
           02 MTIMJOUC  PIC X.                                          00001370
           02 MTIMJOUP  PIC X.                                          00001380
           02 MTIMJOUH  PIC X.                                          00001390
           02 MTIMJOUV  PIC X.                                          00001400
           02 MTIMJOUO  PIC X(5).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNPAGEA   PIC X.                                          00001430
           02 MNPAGEC   PIC X.                                          00001440
           02 MNPAGEP   PIC X.                                          00001450
           02 MNPAGEH   PIC X.                                          00001460
           02 MNPAGEV   PIC X.                                          00001470
           02 MNPAGEO   PIC X(2).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNSOCIETEA     PIC X.                                     00001500
           02 MNSOCIETEC     PIC X.                                     00001510
           02 MNSOCIETEP     PIC X.                                     00001520
           02 MNSOCIETEH     PIC X.                                     00001530
           02 MNSOCIETEV     PIC X.                                     00001540
           02 MNSOCIETEO     PIC X(3).                                  00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNLIEUA   PIC X.                                          00001570
           02 MNLIEUC   PIC X.                                          00001580
           02 MNLIEUP   PIC X.                                          00001590
           02 MNLIEUH   PIC X.                                          00001600
           02 MNLIEUV   PIC X.                                          00001610
           02 MNLIEUO   PIC X(3).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MLIBLIEUA      PIC X.                                     00001640
           02 MLIBLIEUC PIC X.                                          00001650
           02 MLIBLIEUP PIC X.                                          00001660
           02 MLIBLIEUH PIC X.                                          00001670
           02 MLIBLIEUV PIC X.                                          00001680
           02 MLIBLIEUO      PIC X(20).                                 00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MTYPRECA  PIC X.                                          00001710
           02 MTYPRECC  PIC X.                                          00001720
           02 MTYPRECP  PIC X.                                          00001730
           02 MTYPRECH  PIC X.                                          00001740
           02 MTYPRECV  PIC X.                                          00001750
           02 MTYPRECO  PIC X(5).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MLIBRECA  PIC X.                                          00001780
           02 MLIBRECC  PIC X.                                          00001790
           02 MLIBRECP  PIC X.                                          00001800
           02 MLIBRECH  PIC X.                                          00001810
           02 MLIBRECV  PIC X.                                          00001820
           02 MLIBRECO  PIC X(20).                                      00001830
           02 M2O OCCURS   12 TIMES .                                   00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MNSEMAINEA   PIC X.                                     00001860
             03 MNSEMAINEC   PIC X.                                     00001870
             03 MNSEMAINEP   PIC X.                                     00001880
             03 MNSEMAINEH   PIC X.                                     00001890
             03 MNSEMAINEV   PIC X.                                     00001900
             03 MNSEMAINEO   PIC X(6).                                  00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MDJOUR1A     PIC X.                                     00001930
             03 MDJOUR1C     PIC X.                                     00001940
             03 MDJOUR1P     PIC X.                                     00001950
             03 MDJOUR1H     PIC X.                                     00001960
             03 MDJOUR1V     PIC X.                                     00001970
             03 MDJOUR1O     PIC X(4).                                  00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MCFERM1A     PIC X.                                     00002000
             03 MCFERM1C     PIC X.                                     00002010
             03 MCFERM1P     PIC X.                                     00002020
             03 MCFERM1H     PIC X.                                     00002030
             03 MCFERM1V     PIC X.                                     00002040
             03 MCFERM1O     PIC X.                                     00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MDJOUR2A     PIC X.                                     00002070
             03 MDJOUR2C     PIC X.                                     00002080
             03 MDJOUR2P     PIC X.                                     00002090
             03 MDJOUR2H     PIC X.                                     00002100
             03 MDJOUR2V     PIC X.                                     00002110
             03 MDJOUR2O     PIC X(4).                                  00002120
             03 FILLER       PIC X(2).                                  00002130
             03 MCFERM2A     PIC X.                                     00002140
             03 MCFERM2C     PIC X.                                     00002150
             03 MCFERM2P     PIC X.                                     00002160
             03 MCFERM2H     PIC X.                                     00002170
             03 MCFERM2V     PIC X.                                     00002180
             03 MCFERM2O     PIC X.                                     00002190
             03 FILLER       PIC X(2).                                  00002200
             03 MDJOUR3A     PIC X.                                     00002210
             03 MDJOUR3C     PIC X.                                     00002220
             03 MDJOUR3P     PIC X.                                     00002230
             03 MDJOUR3H     PIC X.                                     00002240
             03 MDJOUR3V     PIC X.                                     00002250
             03 MDJOUR3O     PIC X(4).                                  00002260
             03 FILLER       PIC X(2).                                  00002270
             03 MCFERM3A     PIC X.                                     00002280
             03 MCFERM3C     PIC X.                                     00002290
             03 MCFERM3P     PIC X.                                     00002300
             03 MCFERM3H     PIC X.                                     00002310
             03 MCFERM3V     PIC X.                                     00002320
             03 MCFERM3O     PIC X.                                     00002330
             03 FILLER       PIC X(2).                                  00002340
             03 MDJOUR4A     PIC X.                                     00002350
             03 MDJOUR4C     PIC X.                                     00002360
             03 MDJOUR4P     PIC X.                                     00002370
             03 MDJOUR4H     PIC X.                                     00002380
             03 MDJOUR4V     PIC X.                                     00002390
             03 MDJOUR4O     PIC X(4).                                  00002400
             03 FILLER       PIC X(2).                                  00002410
             03 MCFERM4A     PIC X.                                     00002420
             03 MCFERM4C     PIC X.                                     00002430
             03 MCFERM4P     PIC X.                                     00002440
             03 MCFERM4H     PIC X.                                     00002450
             03 MCFERM4V     PIC X.                                     00002460
             03 MCFERM4O     PIC X.                                     00002470
             03 FILLER       PIC X(2).                                  00002480
             03 MDJOUR5A     PIC X.                                     00002490
             03 MDJOUR5C     PIC X.                                     00002500
             03 MDJOUR5P     PIC X.                                     00002510
             03 MDJOUR5H     PIC X.                                     00002520
             03 MDJOUR5V     PIC X.                                     00002530
             03 MDJOUR5O     PIC X(4).                                  00002540
             03 FILLER       PIC X(2).                                  00002550
             03 MCFERM5A     PIC X.                                     00002560
             03 MCFERM5C     PIC X.                                     00002570
             03 MCFERM5P     PIC X.                                     00002580
             03 MCFERM5H     PIC X.                                     00002590
             03 MCFERM5V     PIC X.                                     00002600
             03 MCFERM5O     PIC X.                                     00002610
             03 FILLER       PIC X(2).                                  00002620
             03 MDJOUR6A     PIC X.                                     00002630
             03 MDJOUR6C     PIC X.                                     00002640
             03 MDJOUR6P     PIC X.                                     00002650
             03 MDJOUR6H     PIC X.                                     00002660
             03 MDJOUR6V     PIC X.                                     00002670
             03 MDJOUR6O     PIC X(4).                                  00002680
             03 FILLER       PIC X(2).                                  00002690
             03 MCFERM6A     PIC X.                                     00002700
             03 MCFERM6C     PIC X.                                     00002710
             03 MCFERM6P     PIC X.                                     00002720
             03 MCFERM6H     PIC X.                                     00002730
             03 MCFERM6V     PIC X.                                     00002740
             03 MCFERM6O     PIC X.                                     00002750
             03 FILLER       PIC X(2).                                  00002760
             03 MDJOUR7A     PIC X.                                     00002770
             03 MDJOUR7C     PIC X.                                     00002780
             03 MDJOUR7P     PIC X.                                     00002790
             03 MDJOUR7H     PIC X.                                     00002800
             03 MDJOUR7V     PIC X.                                     00002810
             03 MDJOUR7O     PIC X(4).                                  00002820
             03 FILLER       PIC X(2).                                  00002830
             03 MCFERM7A     PIC X.                                     00002840
             03 MCFERM7C     PIC X.                                     00002850
             03 MCFERM7P     PIC X.                                     00002860
             03 MCFERM7H     PIC X.                                     00002870
             03 MCFERM7V     PIC X.                                     00002880
             03 MCFERM7O     PIC X.                                     00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MZONCMDA  PIC X.                                          00002910
           02 MZONCMDC  PIC X.                                          00002920
           02 MZONCMDP  PIC X.                                          00002930
           02 MZONCMDH  PIC X.                                          00002940
           02 MZONCMDV  PIC X.                                          00002950
           02 MZONCMDO  PIC X(15).                                      00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MLIBERRA  PIC X.                                          00002980
           02 MLIBERRC  PIC X.                                          00002990
           02 MLIBERRP  PIC X.                                          00003000
           02 MLIBERRH  PIC X.                                          00003010
           02 MLIBERRV  PIC X.                                          00003020
           02 MLIBERRO  PIC X(58).                                      00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MCODTRAA  PIC X.                                          00003050
           02 MCODTRAC  PIC X.                                          00003060
           02 MCODTRAP  PIC X.                                          00003070
           02 MCODTRAH  PIC X.                                          00003080
           02 MCODTRAV  PIC X.                                          00003090
           02 MCODTRAO  PIC X(4).                                       00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MCICSA    PIC X.                                          00003120
           02 MCICSC    PIC X.                                          00003130
           02 MCICSP    PIC X.                                          00003140
           02 MCICSH    PIC X.                                          00003150
           02 MCICSV    PIC X.                                          00003160
           02 MCICSO    PIC X(5).                                       00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MNETNAMA  PIC X.                                          00003190
           02 MNETNAMC  PIC X.                                          00003200
           02 MNETNAMP  PIC X.                                          00003210
           02 MNETNAMH  PIC X.                                          00003220
           02 MNETNAMV  PIC X.                                          00003230
           02 MNETNAMO  PIC X(8).                                       00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MSCREENA  PIC X.                                          00003260
           02 MSCREENC  PIC X.                                          00003270
           02 MSCREENP  PIC X.                                          00003280
           02 MSCREENH  PIC X.                                          00003290
           02 MSCREENV  PIC X.                                          00003300
           02 MSCREENO  PIC X(4).                                       00003310
                                                                                
