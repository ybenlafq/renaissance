      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF13   EGF13                                              00000020
      ***************************************************************** 00000030
       01   EGF13I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCDEI    PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPCOL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCTYPCOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTYPCOF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCTYPCOI  PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTRL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNENTRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNENTRF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNENTRI   PIC X(6).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFOURNL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNFOURNF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNFOURNI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFOURNL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLFOURNF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLFOURNI  PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATSAIL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDATSAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATSAIF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDATSAII  PIC X(8).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCINTERL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCINTERF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCINTERI  PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLINTERL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLINTERF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLINTERI  PIC X(20).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATVALL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDATVALL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATVALF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDATVALI  PIC X(8).                                       00000570
           02 M117I OCCURS   10 TIMES .                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNSOCI  PIC X(3).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNLIEUI      PIC X(3).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNCODICI     PIC X(7).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MCFAMI  PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCMARQI      PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MLREFI  PIC X(20).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTECOML     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MQTECOML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTECOMF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQTECOMI     PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTERECL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MQTERECL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTERECF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQTERECI     PIC X(5).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTESOLL     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MQTESOLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTESOLF     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQTESOLI     PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MZONCMDI  PIC X(15).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MLIBERRI  PIC X(58).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCODTRAI  PIC X(4).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCICSI    PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MNETNAMI  PIC X(8).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MSCREENI  PIC X(4).                                       00001180
      ***************************************************************** 00001190
      * SDF: EGF13   EGF13                                              00001200
      ***************************************************************** 00001210
       01   EGF13O REDEFINES EGF13I.                                    00001220
           02 FILLER    PIC X(12).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MDATJOUA  PIC X.                                          00001250
           02 MDATJOUC  PIC X.                                          00001260
           02 MDATJOUP  PIC X.                                          00001270
           02 MDATJOUH  PIC X.                                          00001280
           02 MDATJOUV  PIC X.                                          00001290
           02 MDATJOUO  PIC X(10).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MTIMJOUA  PIC X.                                          00001320
           02 MTIMJOUC  PIC X.                                          00001330
           02 MTIMJOUP  PIC X.                                          00001340
           02 MTIMJOUH  PIC X.                                          00001350
           02 MTIMJOUV  PIC X.                                          00001360
           02 MTIMJOUO  PIC X(5).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MWPAGEA   PIC X.                                          00001390
           02 MWPAGEC   PIC X.                                          00001400
           02 MWPAGEP   PIC X.                                          00001410
           02 MWPAGEH   PIC X.                                          00001420
           02 MWPAGEV   PIC X.                                          00001430
           02 MWPAGEO   PIC X(3).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MWFONCA   PIC X.                                          00001460
           02 MWFONCC   PIC X.                                          00001470
           02 MWFONCP   PIC X.                                          00001480
           02 MWFONCH   PIC X.                                          00001490
           02 MWFONCV   PIC X.                                          00001500
           02 MWFONCO   PIC X(3).                                       00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MNCDEA    PIC X.                                          00001530
           02 MNCDEC    PIC X.                                          00001540
           02 MNCDEP    PIC X.                                          00001550
           02 MNCDEH    PIC X.                                          00001560
           02 MNCDEV    PIC X.                                          00001570
           02 MNCDEO    PIC X(7).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MCTYPCOA  PIC X.                                          00001600
           02 MCTYPCOC  PIC X.                                          00001610
           02 MCTYPCOP  PIC X.                                          00001620
           02 MCTYPCOH  PIC X.                                          00001630
           02 MCTYPCOV  PIC X.                                          00001640
           02 MCTYPCOO  PIC X(5).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MNENTRA   PIC X.                                          00001670
           02 MNENTRC   PIC X.                                          00001680
           02 MNENTRP   PIC X.                                          00001690
           02 MNENTRH   PIC X.                                          00001700
           02 MNENTRV   PIC X.                                          00001710
           02 MNENTRO   PIC X(6).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MNFOURNA  PIC X.                                          00001740
           02 MNFOURNC  PIC X.                                          00001750
           02 MNFOURNP  PIC X.                                          00001760
           02 MNFOURNH  PIC X.                                          00001770
           02 MNFOURNV  PIC X.                                          00001780
           02 MNFOURNO  PIC X(5).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MLFOURNA  PIC X.                                          00001810
           02 MLFOURNC  PIC X.                                          00001820
           02 MLFOURNP  PIC X.                                          00001830
           02 MLFOURNH  PIC X.                                          00001840
           02 MLFOURNV  PIC X.                                          00001850
           02 MLFOURNO  PIC X(20).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MDATSAIA  PIC X.                                          00001880
           02 MDATSAIC  PIC X.                                          00001890
           02 MDATSAIP  PIC X.                                          00001900
           02 MDATSAIH  PIC X.                                          00001910
           02 MDATSAIV  PIC X.                                          00001920
           02 MDATSAIO  PIC X(8).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCINTERA  PIC X.                                          00001950
           02 MCINTERC  PIC X.                                          00001960
           02 MCINTERP  PIC X.                                          00001970
           02 MCINTERH  PIC X.                                          00001980
           02 MCINTERV  PIC X.                                          00001990
           02 MCINTERO  PIC X(5).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MLINTERA  PIC X.                                          00002020
           02 MLINTERC  PIC X.                                          00002030
           02 MLINTERP  PIC X.                                          00002040
           02 MLINTERH  PIC X.                                          00002050
           02 MLINTERV  PIC X.                                          00002060
           02 MLINTERO  PIC X(20).                                      00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MDATVALA  PIC X.                                          00002090
           02 MDATVALC  PIC X.                                          00002100
           02 MDATVALP  PIC X.                                          00002110
           02 MDATVALH  PIC X.                                          00002120
           02 MDATVALV  PIC X.                                          00002130
           02 MDATVALO  PIC X(8).                                       00002140
           02 M117O OCCURS   10 TIMES .                                 00002150
             03 FILLER       PIC X(2).                                  00002160
             03 MNSOCA  PIC X.                                          00002170
             03 MNSOCC  PIC X.                                          00002180
             03 MNSOCP  PIC X.                                          00002190
             03 MNSOCH  PIC X.                                          00002200
             03 MNSOCV  PIC X.                                          00002210
             03 MNSOCO  PIC X(3).                                       00002220
             03 FILLER       PIC X(2).                                  00002230
             03 MNLIEUA      PIC X.                                     00002240
             03 MNLIEUC PIC X.                                          00002250
             03 MNLIEUP PIC X.                                          00002260
             03 MNLIEUH PIC X.                                          00002270
             03 MNLIEUV PIC X.                                          00002280
             03 MNLIEUO      PIC X(3).                                  00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MNCODICA     PIC X.                                     00002310
             03 MNCODICC     PIC X.                                     00002320
             03 MNCODICP     PIC X.                                     00002330
             03 MNCODICH     PIC X.                                     00002340
             03 MNCODICV     PIC X.                                     00002350
             03 MNCODICO     PIC X(7).                                  00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MCFAMA  PIC X.                                          00002380
             03 MCFAMC  PIC X.                                          00002390
             03 MCFAMP  PIC X.                                          00002400
             03 MCFAMH  PIC X.                                          00002410
             03 MCFAMV  PIC X.                                          00002420
             03 MCFAMO  PIC X(5).                                       00002430
             03 FILLER       PIC X(2).                                  00002440
             03 MCMARQA      PIC X.                                     00002450
             03 MCMARQC PIC X.                                          00002460
             03 MCMARQP PIC X.                                          00002470
             03 MCMARQH PIC X.                                          00002480
             03 MCMARQV PIC X.                                          00002490
             03 MCMARQO      PIC X(5).                                  00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MLREFA  PIC X.                                          00002520
             03 MLREFC  PIC X.                                          00002530
             03 MLREFP  PIC X.                                          00002540
             03 MLREFH  PIC X.                                          00002550
             03 MLREFV  PIC X.                                          00002560
             03 MLREFO  PIC X(20).                                      00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MQTECOMA     PIC X.                                     00002590
             03 MQTECOMC     PIC X.                                     00002600
             03 MQTECOMP     PIC X.                                     00002610
             03 MQTECOMH     PIC X.                                     00002620
             03 MQTECOMV     PIC X.                                     00002630
             03 MQTECOMO     PIC X(5).                                  00002640
             03 FILLER       PIC X(2).                                  00002650
             03 MQTERECA     PIC X.                                     00002660
             03 MQTERECC     PIC X.                                     00002670
             03 MQTERECP     PIC X.                                     00002680
             03 MQTERECH     PIC X.                                     00002690
             03 MQTERECV     PIC X.                                     00002700
             03 MQTERECO     PIC X(5).                                  00002710
             03 FILLER       PIC X(2).                                  00002720
             03 MQTESOLA     PIC X.                                     00002730
             03 MQTESOLC     PIC X.                                     00002740
             03 MQTESOLP     PIC X.                                     00002750
             03 MQTESOLH     PIC X.                                     00002760
             03 MQTESOLV     PIC X.                                     00002770
             03 MQTESOLO     PIC X(5).                                  00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MZONCMDA  PIC X.                                          00002800
           02 MZONCMDC  PIC X.                                          00002810
           02 MZONCMDP  PIC X.                                          00002820
           02 MZONCMDH  PIC X.                                          00002830
           02 MZONCMDV  PIC X.                                          00002840
           02 MZONCMDO  PIC X(15).                                      00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLIBERRA  PIC X.                                          00002870
           02 MLIBERRC  PIC X.                                          00002880
           02 MLIBERRP  PIC X.                                          00002890
           02 MLIBERRH  PIC X.                                          00002900
           02 MLIBERRV  PIC X.                                          00002910
           02 MLIBERRO  PIC X(58).                                      00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MCODTRAA  PIC X.                                          00002940
           02 MCODTRAC  PIC X.                                          00002950
           02 MCODTRAP  PIC X.                                          00002960
           02 MCODTRAH  PIC X.                                          00002970
           02 MCODTRAV  PIC X.                                          00002980
           02 MCODTRAO  PIC X(4).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCICSA    PIC X.                                          00003010
           02 MCICSC    PIC X.                                          00003020
           02 MCICSP    PIC X.                                          00003030
           02 MCICSH    PIC X.                                          00003040
           02 MCICSV    PIC X.                                          00003050
           02 MCICSO    PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MNETNAMA  PIC X.                                          00003080
           02 MNETNAMC  PIC X.                                          00003090
           02 MNETNAMP  PIC X.                                          00003100
           02 MNETNAMH  PIC X.                                          00003110
           02 MNETNAMV  PIC X.                                          00003120
           02 MNETNAMO  PIC X(8).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MSCREENA  PIC X.                                          00003150
           02 MSCREENC  PIC X.                                          00003160
           02 MSCREENP  PIC X.                                          00003170
           02 MSCREENH  PIC X.                                          00003180
           02 MSCREENV  PIC X.                                          00003190
           02 MSCREENO  PIC X(4).                                       00003200
                                                                                
