      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      ***************************************************************** 00020000
      *  TS TRANSACTION GF25 : VALORISATIONS DETAILS RISTOURNES       * 00030000
      ***************************************************************** 00040000
      *                                                                 00050000
       01  TS-GF25.                                                     00060000
      *                                                                 00061000
           02  TS-GF25-LONG  PIC S9(3) COMP-3      VALUE +126.          00062001
      *                                                                 00063000
           02  TS-GF25-DONNEES.                                         00064000
      *                                                                 00160000
      *  DONNEES GENERALE LIGNE VALORISATION RISTOURNE                  00170001
      *                                                                 00180000
             03  TS-GF25-DONNEES-GENERALE.                              00190001
                 04  TS-GF25-NCDE             PIC X(7).                 00210000
                 04  TS-GF25-NCODIC           PIC X(7).                 00220000
                 04  TS-GF25-LREFFOURN        PIC X(25).                00220000
                 04  TS-GF25-DVALIDITE        PIC X(8).                 00230000
      *                                                                 00160000
      *  DONNEES LIGNE VALORISATION RISTOURNE                           00170001
      *                                                                 00180000
             03  TS-GF25-DONNEES-VAL.                                   00190001
                 04  TS-GF25-CRIST            PIC X(7).                 00250000
                 04  TS-GF25-WRIST            PIC X(1).                 00260001
                 04  TS-GF25-PRIST            PIC S9(5)V99 COMP-3.      00270000
                 04  TS-GF25-CPERRCVRT        PIC X(2).                 00280000
                 04  TS-GF25-CMODRCVRT        PIC X(2).                 00290000
                 04  TS-GF25-DRELANCE         PIC X(2).                 00300000
      *                                                                 00160000
      *  DONNEES LIGNE DERNIERE VALORISATION RISTOURNE                  00170001
      *                                                                 00180000
             03  TS-GF25-DONNEES-DER-VAL.                               00350401
                 04  TS-GF25-NCDEDER          PIC X(7).                 00210000
                 04  TS-GF25-CRISTDER         PIC X(7).                 00250000
                 04  TS-GF25-WRISTDER         PIC X.                    00260001
                 04  TS-GF25-PRISTDER         PIC S9(5)V99  COMP-3.     00270000
                 04  TS-GF25-CPERRCVRTDER     PIC X(2).                 00280000
                 04  TS-GF25-CMODRCVRTDER     PIC X(2).                 00290000
                 04  TS-GF25-DRELANCEDER      PIC X(2).                 00300000
      *                                                                 00160000
      *  DONNEES LIGNE INIT VALORISATION RISTOURNE                      00170001
      *                                                                 00180000
                                                                        00351001
             03  TS-GF25-DONNEES-INI-VAL.                               00450002
                 04  TS-GF25-CRISTINI         PIC X(7).                 00250000
                 04  TS-GF25-WRISTINI         PIC X.                    00260001
                 04  TS-GF25-PRISTINI         PIC S9(5)V99 COMP-3.      00270000
                 04  TS-GF25-CPERRCVRTINI     PIC X(2).                 00280000
                 04  TS-GF25-CMODRCVRTINI     PIC X(2).                 00290000
                 04  TS-GF25-DRELANCEINI      PIC X(2).                 00300000
      *                                                                 00160000
      *  DONNEES LIGNE INIT DERNIERE VALORISATION RISTOURNE             00170001
      *                                                                 00180000
                                                                        00450003
             03  TS-GF25-DONNEES-IND-VAL.                               00450030
                 04  TS-GF25-CRISTIND         PIC X(7).                 00250000
                 04  TS-GF25-WRISTIND         PIC X.                    00260001
                 04  TS-GF25-PRISTIND         PIC S9(5)V99 COMP-3.      00270000
                 04  TS-GF25-CPERRCVRTIND     PIC X(2).                 00280000
                 04  TS-GF25-CMODRCVRTIND     PIC X(2).                 00290000
                 04  TS-GF25-DRELANCEIND      PIC X(2).                 00300000
      *                                                                 00710000
                                                                                
