      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      ***************************************************************** 00020000
      *  TS TRANSACTION GF22 : LISTE DES COMMANDES NON-VALORISEES     * 00030000
      ***************************************************************** 00040000
      *                                                                 00050000
       01  TS-GF22.                                                     00060000
      *                                                                 00061000
           02  TS-GF22-LONG  PIC S9(3) COMP-3      VALUE +121.          00062001
      *                                                                 00063000
           02  TS-GF22-DONNEES.                                         00064000
      *                                                                 00160000
      *  DONNEES COMMANDE NON VALORISEE (LONG : 81)                     00170001
      *                                                                 00180000
             03  TS-GF22-DONNEES-CDE.                                   00190000
                 04  TS-GF22-WTOPVALO         PIC X(01).                00210001
AB               04  TS-GF22-MULTI            PIC X(01).                00210101
                 04  TS-GF22-NCDE             PIC X(07).                00211001
                 04  TS-GF22-CTYPCDE          PIC X(05).                00220000
                 04  TS-GF22-NENTCDE          PIC X(05).                00221000
                 04  TS-GF22-MODTRAIT         PIC X(06).                00222002
                 04  TS-GF22-NSOCLIVR         PIC X(03).                00223001
                 04  TS-GF22-NDEPOT           PIC X(03).                00224001
                 04  TS-GF22-DSAISIE          PIC X(08).                00230000
                 04  TS-GF22-DLIVRAISON       PIC X(08).                00231001
                 04  TS-GF22-DVALIDITE        PIC X(08).                00240000
                 04  TS-GF22-CVALORISAT       PIC X(01).                00250000
                 04  TS-GF22-CMODRGLT         PIC X(05).                00251000
                 04  TS-GF22-QDELRGLT         PIC S9(3) COMP-3.         00252000
                 04  TS-GF22-CTYPJOUR         PIC X(05).                00253000
                 04  TS-GF22-CDEPRGLT         PIC X(05).                00254000
                 04  TS-GF22-DECHEANCE1       PIC X(02).                00255000
                 04  TS-GF22-DECHEANCE2       PIC X(02).                00255100
                 04  TS-GF22-DECHEANCE3       PIC X(02).                00255200
                 04  TS-GF22-QTAUXESCPT       PIC S9(3)V99 COMP-3.      00258000
      *                                                                 00258100
      *  DONNEES SPECIALES ECRAN (LONG : 40)                            00258200
      *                                                                 00258300
             03  TS-GF22-DONNEES-ECR.                                   00258400
                 04  TS-GF22-LENTCDE          PIC X(20).                00260000
                 04  TS-GF22-TIMESTPE         PIC X(20).                00262002
      *                                                                 00710000
                                                                                
