      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGF3101                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF3101                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGF3101.                                                            
           02  GF31-NSEQ                                                        
               PIC X(0002).                                                     
           02  GF31-NCDE                                                        
               PIC X(0007).                                                     
           02  GF31-NCODIC                                                      
               PIC X(0007).                                                     
           02  GF31-NLIVRAISON                                                  
               PIC X(0007).                                                     
           02  GF31-DLIVRAISON                                                  
               PIC X(0008).                                                     
           02  GF31-DSAISIE                                                     
               PIC X(0008).                                                     
           02  GF31-QCDE                                                        
               PIC S9(5) COMP-3.                                                
           02  GF31-QUOLIVR                                                     
               PIC S9(5) COMP-3.                                                
           02  GF31-QUOPALETTIS                                                 
               PIC S9(5) COMP-3.                                                
           02  GF31-CMODSTOCK                                                   
               PIC X(0005).                                                     
           02  GF31-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GF31-WCLT                                                        
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGF3101                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGF3101-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-NLIVRAISON-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-NLIVRAISON-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-DLIVRAISON-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-DLIVRAISON-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-DSAISIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-QCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-QCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-QUOLIVR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-QUOLIVR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-QUOPALETTIS-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-QUOPALETTIS-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-CMODSTOCK-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-CMODSTOCK-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF31-WCLT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF31-WCLT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
