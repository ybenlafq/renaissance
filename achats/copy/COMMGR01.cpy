      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGR01 (TGR00 -> MENU)    TR: GR00  *    00020000
      *               GESTION DES RECEPTIONS                       *    00030000
      *                                                                 00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3710  00050001
      *                                                                 00060000
      *        TRANSACTION GR01 : SOUS-MENU GESTION DES QUOTAS     *    00070000
      *                                                                 00080000
          02 COMM-GR01-APPLIC REDEFINES COMM-GR00-APPLI.                00090001
      *------------------------------ ZONE DONNEES TGR01                00100000
             03 COMM-GR01-DONNEES-TGR01.                                00110000
      *------------------------------ CODE SOCIETE                      00120000
                04 COMM-GR01-NSOCIETE          PIC X(3).                00130001
      *------------------------------ LIBELLE SOCIETE                   00140000
                04 COMM-GR01-LSOCIETE          PIC X(20).               00150000
      *------------------------------ CODE DEPOT                        00160000
                04 COMM-GR01-NDEPOT            PIC X(3).                00170001
      *------------------------------ LIBELLE DEPOT                     00180000
                04 COMM-GR01-LDEPOT            PIC X(20).               00190000
      *------------------------------ MULTI SOC/DEP                     00200001
                04 COMM-GR01-TYPUSER           PIC X(8).                00210001
      *------------------------------ SOC/DEP AUTORISE                  00220001
JA              04 COMM-GR01-AUTOR-DIXDEPOT.                            00230001
JA    *            05 COMM-GR01-AUTOR-DEPOT OCCURS 10.                  00240001
                   05 COMM-GR01-AUTOR-DEPOT OCCURS 200.                         
JA                    07 COMM-GR01-AUTOR-NSOCENTRE PIC X(3).            00250001
JA                    07 COMM-GR01-AUTOR-NDEPOT    PIC X(3).            00260001
      *------------------------------ ZONES APPLICATIVES                00270001
             03 COMM-GR01-APPLI.                                        00280002
      *         04 COMM-GR01-LIBRE          PIC X(3596).                00290002
                04 COMM-GR01-LIBRE          PIC X(2456).                        
      ***************************************************************** 00300000
                                                                                
