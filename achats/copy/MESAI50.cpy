      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ                                                      
      *                                                                         
      *                                                                         
      *****************************************************************         
      *  POUR MAI50 AVEC PUT                                                    
      *                                                                         
           10  WS-MESSAGE.                                                      
            15  MES-ENTETE-STANDARD.                                            
              20   MES-TYPE          PIC X(3).                                  
              20   MES-NSOCMSG       PIC X(3).                                  
              20   MES-NLIEUMSG      PIC X(3).                                  
              20   MES-NSOCDST       PIC X(3).                                  
              20   MES-NLIEUDST      PIC X(3).                                  
              20   MES-NORD          PIC 9(8).                                  
              20   MES-LPROG         PIC X(10).                                 
              20   MES-DJOUR         PIC X(8).                                  
              20   MES-WSID          PIC X(10).                                 
              20   MES-USER          PIC X(10).                                 
              20   MES-CHRONO        PIC 9(7).                                  
              20   MES-NBRMSG        PIC 9(7).                                  
              20   MES-NBRENR        PIC 9(5).                                  
              20   MES-TAILLE        PIC 9(5).                                  
              20   MES-FILLER        PIC X(20).                                 
            15  MES-ENTETE.                                                     
              20   MES-WCFONC        PIC X(3).                                  
              20   MES-NCODIC        PIC X(7).                                  
              20   MES-NBEAN         PIC 9(2) VALUE 0.                          
              20   MES-NBFOURN       PIC 9(2) VALUE 0.                          
              20   MES-NBPRODLIE     PIC 9(2) VALUE 0.                          
              20   MES-NBPXCOMM      PIC 9(2) VALUE 0.                          
              20   MES-NBPREST       PIC 9(2) VALUE 0.                          
              20   MES-NBNEAN        PIC 9(2) VALUE 0.                          
              20   MES-WOPT1         PIC X(1).                                  
              20   MES-WOPT3         PIC X(1).                                  
              20   MES-WOPT4         PIC X(1).                                  
              20   MES-WOPT5         PIC X(1).                                  
              20   MES-WTYPE         PIC 9(1) VALUE 0.                          
            15  MES-OPTION1.                                                    
      *                                                                         
              20   MES-CMARQ           PIC X(5).                                
E0008         20   MES-LREFO           PIC X(20).                               
              20   MES-OPT1-NEAN       PIC X(13).                               
              20   MES-CCOUL           PIC X(5).                                
              20   MES-CTAUXTVA        PIC X(5).                                
              20   MES-WDACEM          PIC X(1).                                
              20   MES-CGARCONST       PIC X(5).                                
              20   MES-CCOULMARK       PIC X(5).                                
              20   MES-IDEPT           PIC X(3).                                
              20   MES-ISDEPT          PIC X(3).                                
              20   MES-ICLAS           PIC X(3).                                
              20   MES-ISCLAS          PIC X(3).                                
              20   MES-IATRB4          PIC X(2).                                
              20   MES-IATRB2          PIC X(2).                                
              20   MES-ISTYPE          PIC X(2).                                
              20   MES-I2TKTD          PIC X(60).                               
              20   MES-I2ADVD          PIC X(60).                               
              20   MES-I3MKDS          PIC X(15).                               
              20   MES-IWARGC          PIC X(2).                                
              20   MES-NBCOMPO         PIC 9(2).                                
            15   MES-OPTION3.                                                   
      *                                                                         
              20   MES-IRPLCD          PIC X(1).                                
              20   MES-IATRB3          PIC X(2).                                
              20   MES-I3WKMX          PIC X(2).                                
              20   MES-LCOMMENT        PIC X(50).                               
              20   MES-CAPPRO          PIC X(5).                                
              20   MES-WSENSAPPRO      PIC X(1).                                
            15   MES-OPTION4.                                                   
      *                                                                         
              20   MES-CHEFPROD        PIC X(5).                                
            15   MES-OPTION5.                                                   
      *                                                                         
              20   MES-CCOTEHOLD       PIC X(1).                                
              20   MES-I3ABC           PIC X(1).                                
              20   MES-CUNITRECEPT     PIC X(3).                                
              20   MES-CUNITVTE        PIC X(3).                                
              20   MES-WCROSSDOCK      PIC X(1).                                
      *       20   MES-LEMBALLAGE      PIC X(50).                               
              20   MES-QCOLIRECEPT     PIC 9(5).                                
              20   MES-QCOLIVTE        PIC 9(5).                                
              20   MES-QPOIDS          PIC 9(7).                                
              20   MES-QLARGEUR        PIC 9(3).                                
              20   MES-QPROFONDEUR     PIC 9(3).                                
              20   MES-QHAUTEUR        PIC 9(3).                                
              20   MES-QPOIDSDE        PIC 9(7).                                
              20   MES-QLARGEURDE      PIC 9(5).                                
              20   MES-QPROFONDEURDE   PIC 9(5).                                
              20   MES-QHAUTEURDE      PIC 9(5).                                
              20   MES-QPRODBOX        PIC 9(4).                                
              20   MES-QBOXCART        PIC 9(4).                                
              20   MES-QCARTCOUCH      PIC 9(2).                                
              20   MES-QCOUCHPAL       PIC 9(2).                                
      *                                                                         
            15   MES-FOURN OCCURS 0 TO 99                                       
                   DEPENDING ON MES-NBFOURN.                                    
              20   MES-FOURN-TOPMAJ       PIC X.                                
              20   MES-NENTCDE            PIC 9(5).                             
              20   MES-LIBCDE             PIC X(20).                            
              20   MES-WENTCDE            PIC X(1).                             
            15   MES-PRODLIE OCCURS 0 TO 99                                     
                   DEPENDING ON MES-NBPRODLIE.                                  
              20   MES-PRODLIE-TOPMAJ   PIC X.                                  
              20   MES-CTYPLIEN         PIC X(5).                               
              20   MES-WDGRELIB         PIC X(5).                               
              20   MES-NCODICLIE        PIC X(7).                               
              20   MES-QTYPLIEN         PIC 9(2).                               
            15   MES-PXCOMM OCCURS 0 TO 99                                      
                   DEPENDING ON MES-NBPXCOMM.                                   
              20   MES-PXCOMM-TOPMAJ     PIC X.                                 
              20   MES-NZONPRIX          PIC X(2).                              
              20   MES-DEFFET            PIC X(8).                              
              20   MES-PSTDTTC           PIC 9(7)V99.                           
              20   MES-PCOMM             PIC 9(5)V99.                           
              20   MES-PBF               PIC 9(7)V99.                           
      *                                                                         
            15   MES-EAN       OCCURS 0 TO 99                                   
                   DEPENDING ON MES-NBEAN.                                      
              20   MES-EAN-TOPMAJ               PIC X.                          
              20   MES-NEAN                     PIC X(15).                      
              20   MES-WAUTHENT                 PIC X(1).                       
              20   MES-LNEAN                    PIC X(15).                      
              20   MES-WACTIF                   PIC X(1).                       
              20   MES-DACTIF                   PIC X(8).                       
              20   MES-NSEQUENCE                PIC 9(2).                       
            15   MES-NEAN-EAN       OCCURS 0 TO 99                              
                   DEPENDING ON MES-NBNEAN.                                     
              20   MES-NEAN-TOPMAJ               PIC X.                         
              20   MES-NEAN-NEAN                     PIC X(15).                 
              20   MES-NEAN-WAUTHENT                 PIC X(1).                  
              20   MES-NEAN-LNEAN                    PIC X(15).                 
              20   MES-NEAN-WACTIF                   PIC X(1).                  
              20   MES-NEAN-DACTIF                   PIC X(8).                  
              20   MES-NEAN-NSEQUENCE                PIC 9(2).                  
                                                                                
