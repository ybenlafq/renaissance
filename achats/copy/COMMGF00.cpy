      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGF00 (MENU)             TR: GF00  *    00020000
      *                          TGF01 TGF23                       *    00030000
      *                           TGF11 TGF26                      *    00040000
      *                            TGF12 TGF30                     *    00050000
      *                             TGF15 TGF08                    *    00060000
      *                              TGF16 TGF09                   *    00070000
      *                               TGF18 TGF43                  *            
      *                                TGF44 TGF47                 *            
      *                                 TGF48 TGF50                *            
      *                                                            *    00090000
      *           POUR L'ADMINISTATION DES DONNEES                 *    00100000
      **************************************************************    00110000
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00120000
      **************************************************************    00130000
      *                                                                 00140000
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00150000
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00160000
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00170000
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00180000
      *                                                                 00190000
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00200000
      * COMPRENANT :                                                    00210000
      * 1 - LES ZONES RESERVEES A AIDA                                  00220000
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00230000
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00240000
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00250000
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00260000
      *                                                                 00270000
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00280000
      * PAR AIDA                                                        00290000
      *                                                                 00300000
      *-------------------------------------------------------------    00310000
      *                                                                 00320000
      *01  COM-GF00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00330000
      *01  COM-GF00-LONG-COMMAREA PIC S9(4) COMP VALUE +8192.                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-GF00-LONG-COMMAREA PIC S9(4) COMP VALUE +9304.                   
      *--                                                                       
       01  COM-GF00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +9304.                 
      *}                                                                        
      *                                                                 00340000
       01  Z-COMMAREA.                                                  00350000
      *                                                                 00360000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00370000
          02 FILLER-COM-AIDA      PIC X(100).                           00380000
      *                                                                 00390000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00400000
          02 COMM-CICS-APPLID     PIC X(8).                             00410000
          02 COMM-CICS-NETNAM     PIC X(8).                             00420000
          02 COMM-CICS-TRANSA     PIC X(4).                             00430000
      *                                                                 00440000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00450000
          02 COMM-DATE-SIECLE     PIC XX.                               00460000
          02 COMM-DATE-ANNEE      PIC XX.                               00470000
          02 COMM-DATE-MOIS       PIC XX.                               00480000
          02 COMM-DATE-JOUR       PIC XX.                               00490000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00500000
          02 COMM-DATE-QNTA       PIC 999.                              00510000
          02 COMM-DATE-QNT0       PIC 99999.                            00520000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00530000
          02 COMM-DATE-BISX       PIC 9.                                00540000
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00550000
          02 COMM-DATE-JSM        PIC 9.                                00560000
      *   LIBELLES DU JOUR COURT - LONG                                 00570000
          02 COMM-DATE-JSM-LC     PIC XXX.                              00580000
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                         00590000
      *   LIBELLES DU MOIS COURT - LONG                                 00600000
          02 COMM-DATE-MOIS-LC    PIC XXX.                              00610000
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                         00620000
      *   DIFFERENTES FORMES DE DATE                                    00630000
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                             00640000
          02 COMM-DATE-AAMMJJ     PIC X(6).                             00650000
          02 COMM-DATE-JJMMSSAA   PIC X(8).                             00660000
          02 COMM-DATE-JJMMAA     PIC X(6).                             00670000
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                             00680000
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                            00690000
      * DIFFERENTES FORMES DE DATE                                      00700000
          02 COMM-DATE-FILLER     PIC X(14).                            00710000
      *                                                                 00720000
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00730000
      *                                                                 00740000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.              00750000
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                        00760000
      *                                                                 00770000
      * ZONES MESSAGE CREATION COMMANDE    OU EDITION CDE---------- 131 00780004
      *                                                                 00790000
          02 COMM-MESSAGE                PIC X(78).                     00800004
          02 COMM-GF00-NSOC              PIC X(3).                      00810004
      *   02 COMM-GF00-TAB-SOC.                                         00820004
      *      04 COMM-GF00-OCC-SOC        PIC X(3) OCCURS 5.             00830004
      *      04 COMM-GF00-OCC-DEP        PIC X(3) OCCURS 5.             00830005
          02 COMM-GF00-TAB-SOC.                                                 
      *      04 COMM-GF00-TAB-DEPOTS  OCCURS 100.                               
      *      04 COMM-GF00-TAB-DEPOTS  OCCURS 15.                                
             04 COMM-GF00-TAB-DEPOTS  OCCURS 200.                               
                05 COMM-GF00-OCC-SOC    PIC X(3).                               
                05 COMM-GF00-OCC-DEP    PIC X(3).                               
          02 COMM-GF00-NSOCCICS          PIC X(3).                              
          02 COMM-GF00-CFLGMAG           PIC X(5).                              
          02 COMM-GF00-NBSOC             PIC S9(3) COMP-3.              00840004
          02 COMM-GF00-NBDEP             PIC S9(3) COMP-3.              00840004
          02 COMM-GF00-NDEPOT            PIC X(3).                      00810004
          02 COMM-GF00-LIBRE             PIC X(5).                      00850004
      *                                                                 00860000
      * ZONES DONNEES SPECIFIQUES COMMANDES FOURNISEURS --------- 268           
      *                                                                 00880000
          02 COMM-GF00-MENU.                                            00890000
      *                                                                 00900000
              03 COMM-GF00-FONC              PIC X(3).                  00910000
              03 COMM-GF00-NTERMID           PIC X(4).                          
      *                                                                 00920000
      *------------------------ ENTITE COMMANDE (25)                    00930000
      *                                                                 00940000
              03 COMM-GA06.                                             00950000
                 04 COMM-GA06-NENTCDE        PIC X(5).                  00960000
                 04 COMM-GA06-LENTCDE        PIC X(20).                 00970000
      *                                                                 00980000
      *------------------------ INTERLOCUTEUR (30)                      00990000
      *                                                                 01000000
              03 COMM-GA08.                                             01010000
                 04 COMM-GA08-NENTCDE        PIC X(5).                  01020000
                 04 COMM-GA08-CINTERLOCUT    PIC X(5).                  01030000
                 04 COMM-GA08-LINTERLOCUT    PIC X(20).                 01040000
      *                                                                 01050000
      *------------------------ COMMANDE (206)                                  
      *                                                                 01070000
              03 COMM-GF10.                                             01080000
                 04 COMM-GF10-MULTI          PIC X(1).                          
                 04 COMM-GF10-NSURCDE        PIC X(7).                          
                 04 COMM-GF10-CVALORISAT-NSURCDE PIC X(1).                      
                 04 COMM-GF10-LIGNES OCCURS 10.                                 
                    05 COMM-GF10-NCDE        PIC X(7).                          
                    05 COMM-GF10-NSOCLIVR    PIC X(3).                          
                    05 COMM-GF10-NDEPOT      PIC X(3).                          
                    05 COMM-GF10-CVALORISAT  PIC X(1).                          
      *          04 COMM-GF10-NCDE           PIC X(7).                  01090000
                 04 COMM-GF10-CTYPCDE        PIC X(5).                  01100000
                 04 COMM-GF10-NENTCDE        PIC X(5).                  01110000
                 04 COMM-GF10-CINTERLOCUT    PIC X(5).                  01120000
      *          04 COMM-GF10-NSOCLIVR       PIC X(3).                  01130000
      *          04 COMM-GF10-NDEPOT         PIC X(3).                  01140000
                 04 COMM-GF10-DSAISIE        PIC X(8).                  01150000
                 04 COMM-GF10-DVALIDITE      PIC X(8).                  01160000
      *          04 COMM-GF10-CVALORISAT     PIC X(1).                  01170000
                 04 COMM-GF10-CMODRGLT       PIC X(5).                  01180000
                 04 COMM-GF10-QDELRGLT       PIC S9(3) COMP-3.          01190000
                 04 COMM-GF10-CTYPJOUR       PIC X(5).                  01200000
                 04 COMM-GF10-CDEPRGLT       PIC X(5).                  01210000
                 04 COMM-GF10-DECHEANCE1     PIC X(2).                  01220000
                 04 COMM-GF10-DECHEANCE2     PIC X(2).                  01230000
                 04 COMM-GF10-DECHEANCE3     PIC X(2).                  01240000
                 04 COMM-GF10-QTAUXESCPT     PIC S9(3)V9(2) COMP-3.     01250000
                 04 COMM-GF10-ORIGDATE.                                         
                    05 COMM-GF10-ORIGDATE-SS      PIC X(02).                    
                    05 COMM-GF10-ORIGDATE-AA      PIC X(02).                    
                    05 COMM-GF10-ORIGDATE-MM      PIC X(02).                    
                    05 COMM-GF10-ORIGDATE-JJ      PIC X(02).                    
                 04 COMM-GF10-CSTATUT           PIC X(3).                       
                 04 COMM-GF10-CQUALIF           PIC X(6).                       
                 04 COMM-GF10-CENVOYEE          PIC X(1).                       
              03 COMM-GF43.                                                     
                 04 COMM-GF43-P-ORIGINE.                                        
                    05 COMM-GF43-P-1            PIC X(04).                      
                    05 COMM-GF43-P-2            PIC X(04).                      
      *       03 FILLER                      PIC X(0992).                       
      *       03 FILLER                      PIC X(0422).                       
      *       03 FILLER                      PIC X(0575).                       
      *       03 FILLER                      PIC X(0566).                       
      *       03 FILLER                      PIC X(0256).                       
      *       03 FILLER                      PIC X(0246).                       
      *       03 FILLER                      PIC X(0244).                       
      *       03 FILLER                      PIC X(0243).                       
              03 COMM-GF05.                                                     
                 04 COMM-GF05-NSKEP          PIC X(1).                          
              03 FILLER                      PIC X(0244).                       
          02 COMM-GF00-WSAP              PIC X(01).                             
          02 COMM-GF00-VALOMANO          PIC X(01).                             
          02  COMM-GF00-DATACAPTURE.                                            
              03 COMM-GF00-CREASON-XCTRL     PIC X.                             
                88 COMM-GF00-CREASON         VALUE 'O'.                         
                88 COMM-GF00-NO-CREASON      VALUE 'N'.                         
          02  COMM-GF00-RTPT01.                                                 
              03 COMM-GF00-CODLANG           PIC X(2).                          
              03 COMM-GF00-CODPIC            PIC X(2).                          
          02 COMM-GF00-PARAM.                                                   
              03 COMM-GF00-CTRL-COLISAGE     PIC X.                             
                88 COMM-GF00-CTRL-QTE-OK    VALUE 'O'.                          
                88 COMM-GF00-CTRL-QTE-KO    VALUE 'N'.                          
      *                                                                 01260000
      *                                                                 01270000
      * ZONES RESERVEES APPLICATIVES TGF50  --------------------- 147           
          02 COMM-GF50-APPLI.                                                   
              03 COMM-GF50-NCDE              PIC X(07).                         
              03 COMM-GF50-CDE-FLAG          PIC X(01).                         
                88 COMM-GF50-CDE-MULTI       VALUE 'O'.                         
                88 COMM-GF50-CDE-SIMPLE      VALUE 'N'.                         
              03 COMM-GF50-NENTREPOT.                                           
                 05 COMM-GF50-NSOCLIVR       PIC X(03).                         
                 05 COMM-GF50-NDEPOT         PIC X(03).                         
              03 COMM-GF50-LENTREPOT         PIC X(22).                         
              03 COMM-GF50-NENTCDE           PIC X(05).                         
              03 COMM-GF50-LENTCDE           PIC X(20).                         
              03 COMM-GF50-CINTER            PIC X(05).                         
              03 COMM-GF50-NCODIC            PIC X(07).                         
              03 COMM-GF50-LREFFOURN         PIC X(20).                         
              03 COMM-GF50-QCDE              PIC 9(05).                         
              03 COMM-GF50-QREC              PIC 9(05).                         
              03 COMM-GF50-SORTIE-FLAG       PIC X(01).                         
                88 COMM-GF50-CONFIRM-OK      VALUE 'O'.                         
                88 COMM-GF50-CONFIRM-KO      VALUE 'N'.                         
              03 COMM-GF50-MODIF-FLAG        PIC X(01).                         
                88 COMM-GF50-MODIF           VALUE 'O'.                         
                88 COMM-GF50-NO-MODIF        VALUE 'N'.                         
              03 COMM-GF50-CODESFONCTION.                                       
                04  COMM-GF50-CODES-FONC01   OCCURS 2.                          
                 05  COMM-GF50-FONC01        PIC X(03).                         
              03 COMM-GF50-CFONC             PIC X(03).                         
              03 COMM-GF50-PAGE              PIC 9(03).                         
              03 COMM-GF50-PAGEMAX           PIC 9(03).                         
              03 COMM-GF50-DJOUR             PIC X(08).                         
              03 COMM-GF50-PGMPRC            PIC X(05).                         
              03 COMM-GF50-FILLER-LIBRE      PIC X(10).                         
          02 COMM-GF00-PROG-SUIVANT          PIC X(5).                          
          02 COMM-GF00-TITRE5                PIC X(55).                         
          02 COMM-GF00-WEDI                  PIC X(1).                          
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 7421          
          02 COMM-GF00-APPLI.                                           01290002
      *------------------------------ ZONE COMMUNE                      01300000
      *      03 COMM-GF00-FILLER         PIC X(3454).                   01310002
      *      03 COMM-GF00-FILLER         PIC X(7411).                           
      *      03 COMM-GF00-FILLER         PIC X(7406).                           
      *      03 COMM-GF00-FILLER         PIC X(6406).                           
      *      03 COMM-GF00-FILLER         PIC X(6405).                           
      *      03 COMM-GF00-FILLER         PIC X(6315).                           
      *      03 COMM-GF00-FILLER         PIC X(6625).                           
      *      03 COMM-GF00-FILLER         PIC X(6605).                           
             03 COMM-GF00-FILLER         PIC X(6544).                           
             COPY WORKGL06.                                                     
             COPY WORKGF55.                                                     
             03 COMM-GF00-AUTOR-FLAG     PIC X(1).                              
             03 COMM-GF00-CACID          PIC X(4).                      01320002
             03 COMM-GF00-PRG            PIC X(5).                      01330000
             03 COMM-GF00-TOP            PIC X(1).                      01340000
             03 COMM-GF09-INDEX          PIC 9(2).                              
             03 COMM-GF09-NPAGE          PIC 9(3).                              
      ***************************************************************** 01350000
                                                                                
