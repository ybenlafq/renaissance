      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TGF16                     TR: GF16      00002200
      *           TR: GF00                                              00002300
      **************************************************************    00002500
          02 COMM-GF16-APPLI REDEFINES COMM-GF00-APPLI.                 00420002
             03 COMM-ZONE-EVOLUTION          PIC X(217).                00520002
      *------                                                           00521001
      *                                                                 00523002
      *------ AJOUTER LE 06/07/88                                               
             03 COMM-GF16-ANC-NDEPOT         PIC X(03).                 00521001
             03 COMM-GF16-EXIST              PIC X(01).                 00521001
             03 COMM-GF16-INDIC              PIC X(01).                 00521001
             03 COMM-GF16-IL                 PIC 9(03).                 00522402
      *                                                                 00523002
             03 COMM-GF16-DVALIDITE          PIC X(08).                 00522202
             03 COMM-GF16-DVALIDITE-O        PIC X(08).                 00522307
             03 COMM-GF16-IP                 PIC 9(03).                 00522402
             03 COMM-GF16-IP-MAX             PIC 9(03).                 00522501
             03 COMM-GF16-IL-MAX             PIC 9(05).                 00522603
             03 COMM-GF16-10-L.                                         00522704
                04 COMM-GF16-1X              OCCURS 10 TIMES.           00522804
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            05 COMM-GF16-L            PIC S9(4) COMP.            00522904
      *--                                                                       
                   05 COMM-GF16-L            PIC S9(4) COMP-5.                  
      *}                                                                        
             03 COMM-TABLE-CODICS-PAGE.                                 00522905
                04 COMM-CODICS-DISTINCTS     OCCURS 10.                 00522906
                   05 COMM-NCODIC-EN-COURS   PIC X(07).                 00522907
                   05 COMM-DIFF-EN-COURS     PIC S9(5) COMP-3.          00522908
                   05 COMM-DIFF-GF55         PIC S9(5) COMP-3.          00522908
                   05 COMM-QCOLIRECEPT       PIC S9(5) COMP-3.          00522909
                   05 COMM-ITEMS-CONCERNES   OCCURS 10.                 00522910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *               06 COMM-ITEMS-CODIC    PIC S9(4) COMP.            00522911
      *--                                                                       
                      06 COMM-ITEMS-CODIC    PIC S9(4) COMP-5.                  
      *}                                                                        
                      06 COMM-ITEMS-MAJ      PIC X.                     00522920
      *                                                                 00523002
      *------ PASSE A 200 POSTES LE 28/10/93                                    
      *                                                                 00523002
             03 COMM-TABLE-DEM-CODIC.                                           
                04 COMM-DEM-CODIC            OCCURS 200 TIMES.                  
                   05 COMM-NCODIC PIC X(07).                                    
                   05 COMM-DEM.                                                 
                      06 COMM-NSOCIETE       PIC X(03).                         
                      06 COMM-NLIEU          PIC X(03).                         
AB    *------ FLAG DE VALORISATION AUTOMATIQUE                                  
AB           03 COMM-GF16-VALO               PIC X(1).                          
AS    *------ FLAG INTERFACAGE NCG / JDA                                        
AS           03 COMM-GF16-INTRFC             PIC X(1).                          
AS    *------ FLAG BLOCAGE DES EAN NON AUTHENTIFIEE                             
AS           03 COMM-GF16-CDEAN              PIC X(1).                          
AS    *------ FLAG BLOCAGE DES LIEUX DEMANDEURS INTERDITS                       
AS           03 COMM-GF16-LDEMAND            PIC X(1).                          
E0105.*------ FLAG DATA CAPTURE, INDIC SI ON DOIT SAISIR UN REASON CODE         
             03 COMM-GF16-CREASON-FLAG       PIC X(1).                          
               88 COMM-GF16-CREASON-O        VALUE 'O'.                         
               88 COMM-GF16-CREASON-N        VALUE 'N'.                         
      *------ FLAG DATA CAPTURE, INDIC SI UN RC A ETE SAISI                     
             03 COMM-GF16-CREASON-MAJ        PIC X(1).                          
               88 COMM-GF16-MAJ-O            VALUE 'O'.                         
               88 COMM-GF16-MAJ-N            VALUE 'N'.                         
      *------ ZONE DE SAUVEGARDE DE LA DATE DE VALIDITE DE LA MAP               
.E0105       03 COMM-GF16-MAP-DVALID         PIC X(8).                          
E0191.*--- ZONE D'AFFICHAGE VARIABLE                                            
             03 COMM-GF16-LIGNES.                                               
               04 COMM-GF16-LIGNE11          PIC X(24).                         
               04 COMM-GF16-LIGNE21          PIC X(24).                         
E0311          04 COMM-GF16-LIGNE20          PIC X(11).                         
      *------52000 TGF31  1007 O N          AFFICHAGE ZONE MLIGNE32 GB  00523002
E0105 *      03 COMM-GF16-FILLER         PIC X(3075).                   00525007
      *                                                                 00711000
      ***************************************************************** 00740000
                                                                                
