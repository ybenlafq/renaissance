      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF32   EGF32                                              00000020
      ***************************************************************** 00000030
       01   EGF32I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTEDEPOL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MSTEDEPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSTEDEPOF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSTEDEPOI      PIC X(6).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCDEI    PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFOURNL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNFOURNF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNFOURNI  PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFOURNL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLFOURNF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFOURNI  PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE11L      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLIGNE11L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIGNE11F      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLIGNE11I      PIC X(13).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE12L      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLIGNE12L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIGNE12F      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLIGNE12I      PIC X(3).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE13L      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MLIGNE13L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIGNE13F      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLIGNE13I      PIC X(10).                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE14L      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MLIGNE14L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIGNE14F      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLIGNE14I      PIC X(6).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATE1L   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDATE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATE1F   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDATE1I   PIC X(8).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATE3L   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MDATE3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATE3F   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MDATE3I   PIC X(8).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATE2L   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MDATE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATE2F   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDATE2I   PIC X(8).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATE4L   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MDATE4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATE4F   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDATE4I   PIC X(8).                                       00000690
           02 M219I OCCURS   14 TIMES .                                 00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MNCODICI     PIC X(7).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCMARQI      PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MLREFI  PIC X(20).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTECOML     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MQTECOML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTECOMF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQTECOMI     PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTERECL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MQTERECL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTERECF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQTERECI     PIC X(5).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTESOLL     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MQTESOLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTESOLF     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQTESOLI     PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE1L  COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MQTE1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE1F  PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQTE1I  PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE2L  COMP PIC S9(4).                                 00000990
      *--                                                                       
             03 MQTE2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE2F  PIC X.                                          00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MQTE2I  PIC X(5).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE3L  COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MQTE3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE3F  PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MQTE3I  PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE4L  COMP PIC S9(4).                                 00001070
      *--                                                                       
             03 MQTE4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE4F  PIC X.                                          00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MQTE4I  PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MZONCMDI  PIC X(15).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MLIBERRI  PIC X(58).                                      00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCODTRAI  PIC X(4).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MCICSI    PIC X(5).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MNETNAMI  PIC X(8).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MSCREENI  PIC X(4).                                       00001340
      ***************************************************************** 00001350
      * SDF: EGF32   EGF32                                              00001360
      ***************************************************************** 00001370
       01   EGF32O REDEFINES EGF32I.                                    00001380
           02 FILLER    PIC X(12).                                      00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MDATJOUA  PIC X.                                          00001410
           02 MDATJOUC  PIC X.                                          00001420
           02 MDATJOUP  PIC X.                                          00001430
           02 MDATJOUH  PIC X.                                          00001440
           02 MDATJOUV  PIC X.                                          00001450
           02 MDATJOUO  PIC X(10).                                      00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MTIMJOUA  PIC X.                                          00001480
           02 MTIMJOUC  PIC X.                                          00001490
           02 MTIMJOUP  PIC X.                                          00001500
           02 MTIMJOUH  PIC X.                                          00001510
           02 MTIMJOUV  PIC X.                                          00001520
           02 MTIMJOUO  PIC X(5).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MWPAGEA   PIC X.                                          00001550
           02 MWPAGEC   PIC X.                                          00001560
           02 MWPAGEP   PIC X.                                          00001570
           02 MWPAGEH   PIC X.                                          00001580
           02 MWPAGEV   PIC X.                                          00001590
           02 MWPAGEO   PIC X(3).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MWFONCA   PIC X.                                          00001620
           02 MWFONCC   PIC X.                                          00001630
           02 MWFONCP   PIC X.                                          00001640
           02 MWFONCH   PIC X.                                          00001650
           02 MWFONCV   PIC X.                                          00001660
           02 MWFONCO   PIC X(3).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MSTEDEPOA      PIC X.                                     00001690
           02 MSTEDEPOC PIC X.                                          00001700
           02 MSTEDEPOP PIC X.                                          00001710
           02 MSTEDEPOH PIC X.                                          00001720
           02 MSTEDEPOV PIC X.                                          00001730
           02 MSTEDEPOO      PIC X(6).                                  00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MNCDEA    PIC X.                                          00001760
           02 MNCDEC    PIC X.                                          00001770
           02 MNCDEP    PIC X.                                          00001780
           02 MNCDEH    PIC X.                                          00001790
           02 MNCDEV    PIC X.                                          00001800
           02 MNCDEO    PIC X(7).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MNFOURNA  PIC X.                                          00001830
           02 MNFOURNC  PIC X.                                          00001840
           02 MNFOURNP  PIC X.                                          00001850
           02 MNFOURNH  PIC X.                                          00001860
           02 MNFOURNV  PIC X.                                          00001870
           02 MNFOURNO  PIC X(5).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MLFOURNA  PIC X.                                          00001900
           02 MLFOURNC  PIC X.                                          00001910
           02 MLFOURNP  PIC X.                                          00001920
           02 MLFOURNH  PIC X.                                          00001930
           02 MLFOURNV  PIC X.                                          00001940
           02 MLFOURNO  PIC X(20).                                      00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MLIGNE11A      PIC X.                                     00001970
           02 MLIGNE11C PIC X.                                          00001980
           02 MLIGNE11P PIC X.                                          00001990
           02 MLIGNE11H PIC X.                                          00002000
           02 MLIGNE11V PIC X.                                          00002010
           02 MLIGNE11O      PIC X(13).                                 00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MLIGNE12A      PIC X.                                     00002040
           02 MLIGNE12C PIC X.                                          00002050
           02 MLIGNE12P PIC X.                                          00002060
           02 MLIGNE12H PIC X.                                          00002070
           02 MLIGNE12V PIC X.                                          00002080
           02 MLIGNE12O      PIC X(3).                                  00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MLIGNE13A      PIC X.                                     00002110
           02 MLIGNE13C PIC X.                                          00002120
           02 MLIGNE13P PIC X.                                          00002130
           02 MLIGNE13H PIC X.                                          00002140
           02 MLIGNE13V PIC X.                                          00002150
           02 MLIGNE13O      PIC X(10).                                 00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MLIGNE14A      PIC X.                                     00002180
           02 MLIGNE14C PIC X.                                          00002190
           02 MLIGNE14P PIC X.                                          00002200
           02 MLIGNE14H PIC X.                                          00002210
           02 MLIGNE14V PIC X.                                          00002220
           02 MLIGNE14O      PIC X(6).                                  00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MDATE1A   PIC X.                                          00002250
           02 MDATE1C   PIC X.                                          00002260
           02 MDATE1P   PIC X.                                          00002270
           02 MDATE1H   PIC X.                                          00002280
           02 MDATE1V   PIC X.                                          00002290
           02 MDATE1O   PIC X(8).                                       00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MDATE3A   PIC X.                                          00002320
           02 MDATE3C   PIC X.                                          00002330
           02 MDATE3P   PIC X.                                          00002340
           02 MDATE3H   PIC X.                                          00002350
           02 MDATE3V   PIC X.                                          00002360
           02 MDATE3O   PIC X(8).                                       00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MDATE2A   PIC X.                                          00002390
           02 MDATE2C   PIC X.                                          00002400
           02 MDATE2P   PIC X.                                          00002410
           02 MDATE2H   PIC X.                                          00002420
           02 MDATE2V   PIC X.                                          00002430
           02 MDATE2O   PIC X(8).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MDATE4A   PIC X.                                          00002460
           02 MDATE4C   PIC X.                                          00002470
           02 MDATE4P   PIC X.                                          00002480
           02 MDATE4H   PIC X.                                          00002490
           02 MDATE4V   PIC X.                                          00002500
           02 MDATE4O   PIC X(8).                                       00002510
           02 M219O OCCURS   14 TIMES .                                 00002520
             03 FILLER       PIC X(2).                                  00002530
             03 MNCODICA     PIC X.                                     00002540
             03 MNCODICC     PIC X.                                     00002550
             03 MNCODICP     PIC X.                                     00002560
             03 MNCODICH     PIC X.                                     00002570
             03 MNCODICV     PIC X.                                     00002580
             03 MNCODICO     PIC X(7).                                  00002590
             03 FILLER       PIC X(2).                                  00002600
             03 MCMARQA      PIC X.                                     00002610
             03 MCMARQC PIC X.                                          00002620
             03 MCMARQP PIC X.                                          00002630
             03 MCMARQH PIC X.                                          00002640
             03 MCMARQV PIC X.                                          00002650
             03 MCMARQO      PIC X(5).                                  00002660
             03 FILLER       PIC X(2).                                  00002670
             03 MLREFA  PIC X.                                          00002680
             03 MLREFC  PIC X.                                          00002690
             03 MLREFP  PIC X.                                          00002700
             03 MLREFH  PIC X.                                          00002710
             03 MLREFV  PIC X.                                          00002720
             03 MLREFO  PIC X(20).                                      00002730
             03 FILLER       PIC X(2).                                  00002740
             03 MQTECOMA     PIC X.                                     00002750
             03 MQTECOMC     PIC X.                                     00002760
             03 MQTECOMP     PIC X.                                     00002770
             03 MQTECOMH     PIC X.                                     00002780
             03 MQTECOMV     PIC X.                                     00002790
             03 MQTECOMO     PIC X(5).                                  00002800
             03 FILLER       PIC X(2).                                  00002810
             03 MQTERECA     PIC X.                                     00002820
             03 MQTERECC     PIC X.                                     00002830
             03 MQTERECP     PIC X.                                     00002840
             03 MQTERECH     PIC X.                                     00002850
             03 MQTERECV     PIC X.                                     00002860
             03 MQTERECO     PIC X(5).                                  00002870
             03 FILLER       PIC X(2).                                  00002880
             03 MQTESOLA     PIC X.                                     00002890
             03 MQTESOLC     PIC X.                                     00002900
             03 MQTESOLP     PIC X.                                     00002910
             03 MQTESOLH     PIC X.                                     00002920
             03 MQTESOLV     PIC X.                                     00002930
             03 MQTESOLO     PIC X(5).                                  00002940
             03 FILLER       PIC X(2).                                  00002950
             03 MQTE1A  PIC X.                                          00002960
             03 MQTE1C  PIC X.                                          00002970
             03 MQTE1P  PIC X.                                          00002980
             03 MQTE1H  PIC X.                                          00002990
             03 MQTE1V  PIC X.                                          00003000
             03 MQTE1O  PIC X(5).                                       00003010
             03 FILLER       PIC X(2).                                  00003020
             03 MQTE2A  PIC X.                                          00003030
             03 MQTE2C  PIC X.                                          00003040
             03 MQTE2P  PIC X.                                          00003050
             03 MQTE2H  PIC X.                                          00003060
             03 MQTE2V  PIC X.                                          00003070
             03 MQTE2O  PIC X(5).                                       00003080
             03 FILLER       PIC X(2).                                  00003090
             03 MQTE3A  PIC X.                                          00003100
             03 MQTE3C  PIC X.                                          00003110
             03 MQTE3P  PIC X.                                          00003120
             03 MQTE3H  PIC X.                                          00003130
             03 MQTE3V  PIC X.                                          00003140
             03 MQTE3O  PIC X(5).                                       00003150
             03 FILLER       PIC X(2).                                  00003160
             03 MQTE4A  PIC X.                                          00003170
             03 MQTE4C  PIC X.                                          00003180
             03 MQTE4P  PIC X.                                          00003190
             03 MQTE4H  PIC X.                                          00003200
             03 MQTE4V  PIC X.                                          00003210
             03 MQTE4O  PIC X(5).                                       00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MZONCMDA  PIC X.                                          00003240
           02 MZONCMDC  PIC X.                                          00003250
           02 MZONCMDP  PIC X.                                          00003260
           02 MZONCMDH  PIC X.                                          00003270
           02 MZONCMDV  PIC X.                                          00003280
           02 MZONCMDO  PIC X(15).                                      00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MLIBERRA  PIC X.                                          00003310
           02 MLIBERRC  PIC X.                                          00003320
           02 MLIBERRP  PIC X.                                          00003330
           02 MLIBERRH  PIC X.                                          00003340
           02 MLIBERRV  PIC X.                                          00003350
           02 MLIBERRO  PIC X(58).                                      00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MCODTRAA  PIC X.                                          00003380
           02 MCODTRAC  PIC X.                                          00003390
           02 MCODTRAP  PIC X.                                          00003400
           02 MCODTRAH  PIC X.                                          00003410
           02 MCODTRAV  PIC X.                                          00003420
           02 MCODTRAO  PIC X(4).                                       00003430
           02 FILLER    PIC X(2).                                       00003440
           02 MCICSA    PIC X.                                          00003450
           02 MCICSC    PIC X.                                          00003460
           02 MCICSP    PIC X.                                          00003470
           02 MCICSH    PIC X.                                          00003480
           02 MCICSV    PIC X.                                          00003490
           02 MCICSO    PIC X(5).                                       00003500
           02 FILLER    PIC X(2).                                       00003510
           02 MNETNAMA  PIC X.                                          00003520
           02 MNETNAMC  PIC X.                                          00003530
           02 MNETNAMP  PIC X.                                          00003540
           02 MNETNAMH  PIC X.                                          00003550
           02 MNETNAMV  PIC X.                                          00003560
           02 MNETNAMO  PIC X(8).                                       00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MSCREENA  PIC X.                                          00003590
           02 MSCREENC  PIC X.                                          00003600
           02 MSCREENP  PIC X.                                          00003610
           02 MSCREENH  PIC X.                                          00003620
           02 MSCREENV  PIC X.                                          00003630
           02 MSCREENO  PIC X(4).                                       00003640
                                                                                
