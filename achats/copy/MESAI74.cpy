      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
MAINT * MODIF 09/12/02 DSA057 INTERFACE CHANGE                                  
      *   COPY MESSAGES MQ INTERROGATION BDD KESA                               
      *****************************************************************         
      *  POUR MGAXX AVEC GET ET PUT EXTERNES                                    
      *                                                                         
      *01 WS-MESSAGE.                                                           
           10  WS-MESSAGE REDEFINES COMM-MQ21-MESSAGE.                          
      *---                                                                      
           15  MES-ENTETE.                                                      
               20   MES-TYPE      PIC    X(3).                                  
               20   MES-NSOCMSG   PIC    X(3).                                  
               20   MES-NLIEUMSG  PIC    X(3).                                  
               20   MES-NSOCDST   PIC    X(3).                                  
               20   MES-NLIEUDST  PIC    X(3).                                  
               20   MES-NORD      PIC    9(8).                                  
               20   MES-LPROG     PIC    X(10).                                 
               20   MES-DJOUR     PIC    X(8).                                  
               20   MES-WSID      PIC    X(10).                                 
               20   MES-USER      PIC    X(10).                                 
               20   MES-CHRONO    PIC    9(7).                                  
               20   MES-NBRMSG    PIC    9(7).                                  
CRO   *        20   MES-FILLER    PIC    X(30).                                 
               20   MES-NBENR      PIC 9(05).                                   
               20   MES-TAILLE     PIC 9(05).                                   
               20   MES-VERSION2   PIC X(02).                                   
               20   MES-DSYST      PIC S9(13).                                  
               20   MES-FILLER     PIC X(05).                                   
MAINT *    LONG 88                                                              
           15  MES-KESA.                                                        
MAINT-*        20  MES-ACTION     PIC X.                                        
      *        20  MES-FLAGOPCO   PIC X.                                        
-MAINT*        20  MES-INTERFACE  PIC X.                                        
               20  MES-NCODICK    PIC X(7).                                     
MAINT *        20  MES-NSKUK       PIC X(7).                                    
               20  MES-CMARQ      PIC X(5).                                     
               20  MES-LREFO      PIC X(20).                                    
               20  MES-NEAN       PIC X(13).                                    
               20  MES-NPROD      PIC X(15).                                    
               20  MES-CCOLOR     PIC X(5).                                     
MAINT-         20 MES-CFAM PIC X(5).                                            
               20 FILLER PIC X(9).                                              
           15 MES-GESTION.                                                      
               20 MES-RREF PIC 9.                                               
                   88 MES-SREF VALUE 0.                                         
                   88 MES-GREF VALUE 1.                                         
               20 MES-RFAM PIC 9.                                               
                   88 MES-SFAM VALUE 0.                                         
                   88 MES-GFAM VALUE 1.                                         
-MAINT         20 MES-POSPROD PIC 9(7).                                         
                                                                                
