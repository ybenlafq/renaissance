      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGF03                    TR: GF03  *    00020000
      *     SUIVI DES DEMANDES DE COMMANDE SPECIFIQUE              *    00030000
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 7421          
      *                                                                 00060000
          02 COMM-GF03-APPLI REDEFINES COMM-GF00-APPLI.                 00070000
      *------------------------------ CODE CHEFPROD                     00080000
             03 COMM-GF03-CHEFPROD       PIC X(5).                      00090000
      *------------------------------ MESSAGE                           00100001
             03 COMM-GF03-MESS           PIC X(20).                     00110001
      *------------------------------ BAS DU TABLEAU                    00120001
             03 COMM-GF03-BAS-DU-TABLEAU    OCCURS 12.                  00130001
      *------------------------------ CODE SOCIETE                      00140001
                04 COMM-GF03-NSOC          PIC X(3).                    00150002
      *------------------------------ CODE LIEU                         00160001
                04 COMM-GF03-NLIEU         PIC X(3).                    00170001
      *------------------------------ NUMERO DE LIGNE                   00180001
                04 COMM-GF03-NLIGNE        PIC X(2).                    00190001
      *------------------------------ NUMERO DE PAGE                    00200001
                04 COMM-GF03-NPAGE         PIC X(3).                    00210001
      *------------------------------ NUMERO DE COMMANDE (NSEQ)         00220001
                04 COMM-GF03-NSEQ          PIC X(7).                    00230001
      *------------------------------ NUMERO DE DEPOT                   00240001
                04 COMM-GF03-NDEP          PIC X(3).                    00250002
      *------------------------------ CODE INTERLOCUTEUR                00260001
                04 COMM-GF03-CINTERLOCUT    PIC  X(5).                  00270001
      *------------------------------ DATE D'ETAT                       00280001
             03 COMM-GF03-DETAT          PIC X(8).                      00290001
      *------------------------------ CODE INTERLOCUTEUR                00300001
             03 COMM-GF03-CODE-NON-RENSEIGNE  PIC  X(5).                00310001
      *------------------------------ NOMBRE TOTAL DE CODES             00320001
             03 NOMBRE-TOTAL-CODE        PIC  9(5).                     00330001
      *------------------------------ NOMBRE TOTAL DE PAGES             00340001
             03 NOMBRE-DE-PAGE           PIC  9(5).                     00350001
      *------------------------------ NUMERO-DE-PAGE                    00360001
             03 NUMERO-DE-PAGE           PIC  9(3).                     00370001
      *------------------------------ TOP TOUCHE ENTER.                 00380001
             03 COMM-GF03-TOP-ENTER      PIC  X.                        00390001
      *------------------------------ ENTREPOT                          00400001
             03 COMM-GF03-NSOCIETE       PIC X(3).                      00410001
             03 COMM-GF03-NDEPOT         PIC X(3).                      00410101
      *------------------------------ FILLER                            00411001
      *      03 COMM-GF03-FILLER         PIC X(7051).                           
      ***************************************************************** 00420000
                                                                                
