      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : MAJ SAISIE DE COMMANDE FOURNISSEUR                     *         
      *            ARTICLES-QUANTITES (TRX:GF18)                      *         
      *****************************************************************         
      *   LL  62 + 18 = 80                                                      
      *****************************************************************         
       01  TS-GF18-DESCR.                                                       
      *-----------------------------------------------------LONGUEUR TS         
           02 TS-GF18-LONG PIC S9(3) COMP-3  VALUE +080.                        
      *                                                                         
           02 TS-GF18.                                                          
      *-----------------------------------------INFOS ISSU DE GF15---29         
              03 TS-GF18-NCDE              PIC X(07).                           
              03 TS-GF18-NCODIC            PIC X(07).                           
              03 TS-GF18-NSOCIETE          PIC X(03).                           
              03 TS-GF18-NLIEU             PIC X(03).                           
              03 TS-GF18-QCDE-X.                                                
              04 TS-GF18-QCDE              PIC S9(05) COMP-3.                   
              03 TS-GF18-QREC-X.                                                
              04 TS-GF18-QREC              PIC S9(05) COMP-3.                   
              03 TS-GF18-QSOLDE-X.                                              
              04 TS-GF18-QSOLDE            PIC S9(05) COMP-3.                   
      *-----------------------------------------INFOS ISSU DE GA00---33         
              03 TS-GF18-LREFFOURN         PIC X(20).                           
              03 TS-GF18-CFAM              PIC X(05).                           
              03 TS-GF18-CMARQ             PIC X(05).                           
              03 TS-GF18-QCOLIRECEPT       PIC S9(05) COMP-3.                   
              03 TS-GF18-CMAJ              PIC  X(01).                          
                 88 CODIC-SANS-MODIF  VALUE ' '.                                
                 88 NOUVEAU-CODIC   VALUE 'C'.                                  
                 88 ANCIEN-CODIC    VALUE 'M'.                                  
                 88 CODIC-SUPPRIME  VALUE 'X'.                                  
              03 TS-GF18-FILLER            PIC X(17).                           
                                                                                
