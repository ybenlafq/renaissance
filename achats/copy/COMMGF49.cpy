      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG  TGF49                   TR: GF49  *    00020000
      *           GESTION DES DATES PHARES                         *    00030000
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES (DISPO 6814)-------------------    00041034
          02 COMM-GF49-APPLI REDEFINES COMM-GF00-APPLI.                 00050006
      *--- TABLEAU DES CODES FONCTION                                   00063514
             05 COMM-GF49-CODESFONCTION.                                00064000
                10 COMM-GF49-CODES-FONC01  OCCURS 3.                    00065000
                   15 COMM-GF49-FONC01         PIC X(03).               00066009
                   15 FILLER                   PIC X(01).               00067009
      *--- FLAG DE VALIDATION                                           00067223
             05 COMM-GF49-FLAG-TRAITER         PIC X.                   00067325
                88 COMM-GF49-NE-PAS-TRAITER          VALUE 'N'.         00067423
                88 COMM-GF49-A-TRAITER               VALUE 'O'.         00067523
      *--- ZONE POUR GESTION DE LA PAGE                                 00068014
             05 COMM-GF49-PAGE                 PIC 9(03).               00230000
             05 COMM-GF49-PAGE-MAX             PIC 9(03).               00240000
             05 COMM-GF49-CFONCTION            PIC 9(03).               00241010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05 COMM-GF49-IND-TS-MAX           PIC S9(04) COMP.         00250000
      *--                                                                       
             05 COMM-GF49-IND-TS-MAX           PIC S9(04) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05 COMM-GF49-ROW-DISP             PIC S9(04) COMP.         00250122
      *--                                                                       
             05 COMM-GF49-ROW-DISP             PIC S9(04) COMP-5.               
      *}                                                                        
             05 COMM-GF49-TAB.                                          00251008
                10 COMM-GF49-DETAIL-TAB OCCURS 12.                      00251108
                   15 COMM-GF49-IND-MODIF      PIC X.                   00251230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            15 COMM-GF49-IND-TS         PIC S9(04) COMP.         00251312
      *--                                                                       
                   15 COMM-GF49-IND-TS         PIC S9(04) COMP-5.               
      *}                                                                        
      *--- ZONE NEW                                                     00272011
                   15 COMM-GF49-WFLAG          PIC X.                   00273029
                   15 COMM-GF49-CPHARE         PIC X(3).                00280008
                   15 COMM-GF49-WACTIF         PIC X.                   00290008
                   15 COMM-GF49-DACTIF         PIC X(8).                00310008
                   15 COMM-GF49-DACTIF-JMA     PIC X(8).                00311019
                   15 COMM-GF49-DPHARE         PIC X(8).                00320008
                   15 COMM-GF49-LPHARE         PIC X(20).               00321008
      *--- ZONE OLD                                                     00321128
                   15 COMM-GF49-CPHARE-OLD     PIC X(3).                00321228
                   15 COMM-GF49-DACTIF-OLD     PIC X(8).                00321428
             05 FILLER                         PIC X(6032).             00322034
                                                                                
