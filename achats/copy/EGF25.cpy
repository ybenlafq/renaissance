      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF25   EGF25                                              00000020
      ***************************************************************** 00000030
       01   EGF25I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCDEVISEI      PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEVISEL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDEVISEF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLDEVISEI      PIC X(24).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCDEI    PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNCODICI  PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLREFFOURNI    PIC X(20).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVALIDITEL    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MDVALIDITEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDVALIDITEF    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDVALIDITEI    PIC X(8).                                  00000410
           02 M172I OCCURS   14 TIMES .                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCRISTDERL   COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MCRISTDERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCRISTDERF   PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCRISTDERI   PIC X(7).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWRISTDERL   COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MWRISTDERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWRISTDERF   PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MWRISTDERI   PIC X.                                     00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRISTDERL   COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MPRISTDERL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MPRISTDERF   PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MPRISTDERI   PIC X(7).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPERRCVRTDERL    COMP PIC S9(4).                       00000550
      *--                                                                       
             03 MCPERRCVRTDERL COMP-5 PIC S9(4).                                
      *}                                                                        
             03 MCPERRCVRTDERF    PIC X.                                00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCPERRCVRTDERI    PIC X(2).                             00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMODRCVRTDERL    COMP PIC S9(4).                       00000590
      *--                                                                       
             03 MCMODRCVRTDERL COMP-5 PIC S9(4).                                
      *}                                                                        
             03 MCMODRCVRTDERF    PIC X.                                00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCMODRCVRTDERI    PIC X(2).                             00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDRELANCEDERL     COMP PIC S9(4).                       00000630
      *--                                                                       
             03 MDRELANCEDERL COMP-5 PIC S9(4).                                 
      *}                                                                        
             03 MDRELANCEDERF     PIC X.                                00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MDRELANCEDERI     PIC X(2).                             00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCRISTL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCRISTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCRISTF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCRISTI      PIC X(7).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWRISTL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MWRISTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWRISTF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MWRISTI      PIC X.                                     00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRISTL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MPRISTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MPRISTF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MPRISTI      PIC X(7).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPERRCVRTL  COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MCPERRCVRTL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MCPERRCVRTF  PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCPERRCVRTI  PIC X(2).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMODRCVRTL  COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MCMODRCVRTL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MCMODRCVRTF  PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MCMODRCVRTI  PIC X(2).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDRELANCEL   COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MDRELANCEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MDRELANCEF   PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MDRELANCEI   PIC X(2).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(15).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(58).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * SDF: EGF25   EGF25                                              00001160
      ***************************************************************** 00001170
       01   EGF25O REDEFINES EGF25I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MCDEVISEA      PIC X.                                     00001350
           02 MCDEVISEC PIC X.                                          00001360
           02 MCDEVISEP PIC X.                                          00001370
           02 MCDEVISEH PIC X.                                          00001380
           02 MCDEVISEV PIC X.                                          00001390
           02 MCDEVISEO      PIC X(3).                                  00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MCFONCA   PIC X.                                          00001420
           02 MCFONCC   PIC X.                                          00001430
           02 MCFONCP   PIC X.                                          00001440
           02 MCFONCH   PIC X.                                          00001450
           02 MCFONCV   PIC X.                                          00001460
           02 MCFONCO   PIC X(3).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MLDEVISEA      PIC X.                                     00001490
           02 MLDEVISEC PIC X.                                          00001500
           02 MLDEVISEP PIC X.                                          00001510
           02 MLDEVISEH PIC X.                                          00001520
           02 MLDEVISEV PIC X.                                          00001530
           02 MLDEVISEO      PIC X(24).                                 00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MNCDEA    PIC X.                                          00001560
           02 MNCDEC    PIC X.                                          00001570
           02 MNCDEP    PIC X.                                          00001580
           02 MNCDEH    PIC X.                                          00001590
           02 MNCDEV    PIC X.                                          00001600
           02 MNCDEO    PIC X(7).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MNCODICA  PIC X.                                          00001630
           02 MNCODICC  PIC X.                                          00001640
           02 MNCODICP  PIC X.                                          00001650
           02 MNCODICH  PIC X.                                          00001660
           02 MNCODICV  PIC X.                                          00001670
           02 MNCODICO  PIC X(7).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MLREFFOURNA    PIC X.                                     00001700
           02 MLREFFOURNC    PIC X.                                     00001710
           02 MLREFFOURNP    PIC X.                                     00001720
           02 MLREFFOURNH    PIC X.                                     00001730
           02 MLREFFOURNV    PIC X.                                     00001740
           02 MLREFFOURNO    PIC X(20).                                 00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MDVALIDITEA    PIC X.                                     00001770
           02 MDVALIDITEC    PIC X.                                     00001780
           02 MDVALIDITEP    PIC X.                                     00001790
           02 MDVALIDITEH    PIC X.                                     00001800
           02 MDVALIDITEV    PIC X.                                     00001810
           02 MDVALIDITEO    PIC X(8).                                  00001820
           02 M172O OCCURS   14 TIMES .                                 00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MCRISTDERA   PIC X.                                     00001850
             03 MCRISTDERC   PIC X.                                     00001860
             03 MCRISTDERP   PIC X.                                     00001870
             03 MCRISTDERH   PIC X.                                     00001880
             03 MCRISTDERV   PIC X.                                     00001890
             03 MCRISTDERO   PIC X(7).                                  00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MWRISTDERA   PIC X.                                     00001920
             03 MWRISTDERC   PIC X.                                     00001930
             03 MWRISTDERP   PIC X.                                     00001940
             03 MWRISTDERH   PIC X.                                     00001950
             03 MWRISTDERV   PIC X.                                     00001960
             03 MWRISTDERO   PIC X.                                     00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MPRISTDERA   PIC X.                                     00001990
             03 MPRISTDERC   PIC X.                                     00002000
             03 MPRISTDERP   PIC X.                                     00002010
             03 MPRISTDERH   PIC X.                                     00002020
             03 MPRISTDERV   PIC X.                                     00002030
             03 MPRISTDERO   PIC X(7).                                  00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MCPERRCVRTDERA    PIC X.                                00002060
             03 MCPERRCVRTDERC    PIC X.                                00002070
             03 MCPERRCVRTDERP    PIC X.                                00002080
             03 MCPERRCVRTDERH    PIC X.                                00002090
             03 MCPERRCVRTDERV    PIC X.                                00002100
             03 MCPERRCVRTDERO    PIC X(2).                             00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MCMODRCVRTDERA    PIC X.                                00002130
             03 MCMODRCVRTDERC    PIC X.                                00002140
             03 MCMODRCVRTDERP    PIC X.                                00002150
             03 MCMODRCVRTDERH    PIC X.                                00002160
             03 MCMODRCVRTDERV    PIC X.                                00002170
             03 MCMODRCVRTDERO    PIC X(2).                             00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MDRELANCEDERA     PIC X.                                00002200
             03 MDRELANCEDERC     PIC X.                                00002210
             03 MDRELANCEDERP     PIC X.                                00002220
             03 MDRELANCEDERH     PIC X.                                00002230
             03 MDRELANCEDERV     PIC X.                                00002240
             03 MDRELANCEDERO     PIC X(2).                             00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MCRISTA      PIC X.                                     00002270
             03 MCRISTC PIC X.                                          00002280
             03 MCRISTP PIC X.                                          00002290
             03 MCRISTH PIC X.                                          00002300
             03 MCRISTV PIC X.                                          00002310
             03 MCRISTO      PIC X(7).                                  00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MWRISTA      PIC X.                                     00002340
             03 MWRISTC PIC X.                                          00002350
             03 MWRISTP PIC X.                                          00002360
             03 MWRISTH PIC X.                                          00002370
             03 MWRISTV PIC X.                                          00002380
             03 MWRISTO      PIC X.                                     00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MPRISTA      PIC X.                                     00002410
             03 MPRISTC PIC X.                                          00002420
             03 MPRISTP PIC X.                                          00002430
             03 MPRISTH PIC X.                                          00002440
             03 MPRISTV PIC X.                                          00002450
             03 MPRISTO      PIC X(7).                                  00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MCPERRCVRTA  PIC X.                                     00002480
             03 MCPERRCVRTC  PIC X.                                     00002490
             03 MCPERRCVRTP  PIC X.                                     00002500
             03 MCPERRCVRTH  PIC X.                                     00002510
             03 MCPERRCVRTV  PIC X.                                     00002520
             03 MCPERRCVRTO  PIC X(2).                                  00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MCMODRCVRTA  PIC X.                                     00002550
             03 MCMODRCVRTC  PIC X.                                     00002560
             03 MCMODRCVRTP  PIC X.                                     00002570
             03 MCMODRCVRTH  PIC X.                                     00002580
             03 MCMODRCVRTV  PIC X.                                     00002590
             03 MCMODRCVRTO  PIC X(2).                                  00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MDRELANCEA   PIC X.                                     00002620
             03 MDRELANCEC   PIC X.                                     00002630
             03 MDRELANCEP   PIC X.                                     00002640
             03 MDRELANCEH   PIC X.                                     00002650
             03 MDRELANCEV   PIC X.                                     00002660
             03 MDRELANCEO   PIC X(2).                                  00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MZONCMDA  PIC X.                                          00002690
           02 MZONCMDC  PIC X.                                          00002700
           02 MZONCMDP  PIC X.                                          00002710
           02 MZONCMDH  PIC X.                                          00002720
           02 MZONCMDV  PIC X.                                          00002730
           02 MZONCMDO  PIC X(15).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(58).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(4).                                       00003090
                                                                                
