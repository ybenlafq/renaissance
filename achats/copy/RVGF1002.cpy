      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGF1002                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF1002                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF1002.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF1002.                                                            
      *}                                                                        
           02  GF10-NCDE                                                        
               PIC X(0007).                                                     
           02  GF10-CTYPCDE                                                     
               PIC X(0005).                                                     
           02  GF10-NENTCDE                                                     
               PIC X(0005).                                                     
           02  GF10-CINTERLOCUT                                                 
               PIC X(0005).                                                     
           02  GF10-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  GF10-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GF10-DSAISIE                                                     
               PIC X(0008).                                                     
           02  GF10-DVALIDITE                                                   
               PIC X(0008).                                                     
           02  GF10-CVALORISAT                                                  
               PIC X(0001).                                                     
           02  GF10-CMODRGLT                                                    
               PIC X(0005).                                                     
           02  GF10-QDELRGLT                                                    
               PIC S9(3) COMP-3.                                                
           02  GF10-CTYPJOUR                                                    
               PIC X(0005).                                                     
           02  GF10-CDEPRGLT                                                    
               PIC X(0005).                                                     
           02  GF10-DECHEANCE1                                                  
               PIC X(0002).                                                     
           02  GF10-DECHEANCE2                                                  
               PIC X(0002).                                                     
           02  GF10-DECHEANCE3                                                  
               PIC X(0002).                                                     
           02  GF10-QTAUXESCPT                                                  
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GF10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GF10-NAVENANT                                                    
               PIC X(0003).                                                     
           02  GF10-ORIGDATE                                                    
               PIC X(0008).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGF1002                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF1002-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF1002-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-CTYPCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-CTYPCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-NENTCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-NENTCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-CINTERLOCUT-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-CINTERLOCUT-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-DSAISIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-DVALIDITE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-DVALIDITE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-CVALORISAT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-CVALORISAT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-CMODRGLT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-CMODRGLT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-QDELRGLT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-QDELRGLT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-CTYPJOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-CTYPJOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-CDEPRGLT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-CDEPRGLT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-DECHEANCE1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-DECHEANCE1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-DECHEANCE2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-DECHEANCE2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-DECHEANCE3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-DECHEANCE3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-QTAUXESCPT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-QTAUXESCPT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-NAVENANT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-NAVENANT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF10-ORIGDATE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF10-ORIGDATE-F                                                  
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
