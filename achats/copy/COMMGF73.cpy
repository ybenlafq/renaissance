      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      * COMMAREA SPECIFIQUE MODULE D'ENVOI DONNEES DE NCG VERS EDI *            
      * BLS A CALER                                                *            
      **************************************************************            
      *                                                                         
      *   PROGRAMME MGF73                                                       
      *                                                                         
       01 COMM-MGF73-APPLI.                                                     
          02 COMM-MGF73-ENTREE.                                                 
             05 COMM-MGF73-PGRM          PIC X(5).                              
             05 COMM-MGF73-CODLANG       PIC X(2).                              
             05 COMM-MGF73-CODPIC        PIC X(2).                              
             05 COMM-MGF73-DJOUR         PIC X(8).                              
             05 COMM-MGF73-NCDE          PIC X(7).                              
             05 COMM-MGF73-NENTCDE       PIC X(5).                              
             05 COMM-MGF73-NCODIC        PIC X(07).                             
             05 COMM-MGF73-NSOCDEPOT     PIC X(06).                             
             05 COMM-MGF73-LREFFOURN     PIC X(20).                             
             05 COMM-MGF73-QCDE          PIC 9(05).                             
             05 COMM-MGF73-DLIVRAISONI   PIC X(08).                             
             05 COMM-MGF73-DLIVRAISONM   PIC X(08).                             
             05 COMM-MGF73-TERMINAL      PIC X(4).                              
          02 COMM-MGF73-SORTIE.                                                 
             05 COMM-MGF73-MESSAGE.                                             
                10 COMM-MGF73-CODRET     PIC X(1).                              
                   88 COMM-MGF73-OK          VALUE ' '.                         
                   88 COMM-MGF73-ERR-NBLOC   VALUE '0'.                         
                   88 COMM-MGF73-ERR         VALUE '1'.                         
                10 COMM-MGF73-LIBERR     PIC X(58).                             
          02  FILLER                     PIC X(50).                             
                                                                                
