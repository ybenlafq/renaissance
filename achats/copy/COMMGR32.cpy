      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGR32 (TGR00 -> MENU)    TR: GR00  *    00020000
      *               GESTION DES RECEPTIONS FOURNISSEUR           *    00030000
      *                                                                 00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3710  00050001
      *                                                                 00060000
      *        TRANSACTION GR32 : CREATION RECEPTION FOURNISSEUR   *    00070000
      *                                                                 00080000
          02 COMM-GR32-APPLI REDEFINES COMM-GR00-APPLI.                 00090000
      *------------------------------ ZONE DONNEES PASSEES PAR GR30     00100000
             03 COMM-GR32-DONNEES-GR30     PIC X(1744).                 00110003
      *------------------------------ DONNEES PROPRES A TGR32           00120000
             03 COMM-GR32-DONNEES.                                      00130000
      *------------------------------ PAGINATION HAUT-BAS               00140001
                04 COMM-GR32-PAGE-LATERALE.                             00150001
      *------------------------------ EXISTENCE TS PAGINATION           00160001
                   05 COMM-GR32-EXISTS         PIC X(1).                00170001
      *------------------------------ NUMERO ITEM PAGE EN COURS         00180000
                   05 COMM-GR32-LATE-COURS     PIC S9(3) COMP-3.        00190000
      *------------------------------ DERNIER ITEM ECRIT                00200000
                   05 COMM-GR32-LATE-DERNIER   PIC S9(3) COMP-3.        00210000
      *------------------------------ PAGINATION DROITE-GAUCHE          00220000
                04 COMM-GR32-PAGE-HORIZONTALE.                          00230001
      *------------------------------ NUMERO ITEM PAGE EN COURS         00240000
                   05 COMM-GR32-HORI-COURS     PIC S9(3) COMP-3.        00250000
      *------------------------------ NUMERO ITEM PAGE PRECEDENTE       00260000
                   05 COMM-GR32-HORI-PREC      PIC S9(3) COMP-3.        00270000
      *------------------------------ NUMERO ITEM PAGE SUIVANTE         00280000
                   05 COMM-GR32-HORI-SUIV      PIC S9(3) COMP-3.        00290000
      *------------------------------ DERNIER ITEM ECRIT                00300000
                   05 COMM-GR32-HORI-DERNIER   PIC S9(3) COMP-3.        00310001
      *------------------------------ NUMERO PAGE EN COURS              00320000
                   05 COMM-GR32-HORI-PAGE      PIC S9(3) COMP-3.        00330001
      *------------------------------ SAVE DES SWAP                     00340001
                04 COMM-GR32-SWAP-ATTR         PIC X OCCURS 15.         00350001
      *------------------------------ ZONE LIBRE                        00360000
             03 COMM-GR32-LIBRE             PIC X(1930).                00370002
      ***************************************************************** 00380000
                                                                                
