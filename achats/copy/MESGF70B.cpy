      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ                                                       
      *                                                                         
      *                                                                         
      *****************************************************************         
      *                                                                         
      *                                                                         
       01  WS70-MQ10.                                                           
         02 WS70-QUEUE.                                                         
            10 MQ10-CORRELID-B             PIC X(24).                           
         02 WS70-CODRET                    PIC X(02).                           
         02 WS70-MESSAGE.                                                       
            05 MESS-ENTETE.                                                     
               10 MES-TYPE                 PIC X(03).                           
               10 MES-NSOCMSG              PIC X(03).                           
               10 MES-NLIEUMSG             PIC X(03).                           
               10 MES-NSOCDST              PIC X(03).                           
               10 MES-NLIEUDST             PIC X(03).                           
               10 MES-NORD                 PIC 9(08).                           
               10 MES-LPROG                PIC X(10).                           
               10 MES-DJOUR                PIC X(08).                           
               10 MES-WSID                 PIC X(10).                           
               10 MES-USER                 PIC X(10).                           
               10 MES-CHRONO               PIC 9(07).                           
               10 MES-NBRMSG               PIC 9(07).                           
               10 MES-NBRENR               PIC 9(05).                           
               10 MES-TAILLE               PIC 9(05).                           
               10 MES-FILLER               PIC X(20).                           
      *     MESSAGE ENTETE   ...................... 364                         
            05 MES-ENTETE.                                                      
               10  MES-RETOUR1             PIC X(1).                            
               10  MES-TAG1                PIC X(3).                            
               10  MES-CMODETRAIT          PIC X(6).                            
               10  MES-NCDE                PIC X(7).                            
               10  MES-DSAISIE             PIC X(8).                            
               10  MES-NSOCBYR             PIC X(3).                            
               10  MES-NSOCFAC             PIC X(3).                            
               10  MES-CQUALIF             PIC X(6).                            
               10  MES-NENTCDE             PIC X(5).                            
               10  MES-LENTCDE             PIC X(20).                           
               10  MES-LENTCDEADR1         PIC X(38).                           
               10  MES-LENTCDEADR2         PIC X(38).                           
               10  MES-LENTCDEADR3         PIC X(38).                           
               10  MES-LENTCDEADR4         PIC X(38).                           
               10  MES-LENTCDEADR5         PIC X(38).                           
               10  MES-LENTCDEADR6         PIC X(38).                           
               10  MES-LENTCDEADR7         PIC X(38).                           
               10  MES-NENTCDETEL          PIC X(15).                           
               10  MES-CENTCDETELX         PIC X(15).                           
               10  MES-CTYPCDE             PIC X(5).                            
               10  MES-RETOUR2             PIC X(1).                            
      * MES-DETAIL ................................. 262                        
            05 MES-DETAIL           OCCURS 99.                                  
               10  MES-TAG2                PIC  X(3).                           
               10  MES-NSOCDEP             PIC  X(6).                           
               10  MES-NEAN                PIC  X(13).                          
               10  MES-NCODIC              PIC  X(7).                           
               10  MES-CHEFPROD            PIC  X(5).                           
               10  MES-LCHEFPROD           PIC  X(20).                          
               10  MES-LREFFOURN           PIC  X(20).                          
               10  MES-CFAM                PIC  X(5).                           
               10  MES-LFAM                PIC  X(20).                          
               10  MES-CMARQ               PIC  X(5).                           
               10  MES-LMARQ               PIC  X(20).                          
               10  MES-PBF                 PIC 9(7)V9(2).                       
               10  MES-PRA                 PIC 9(7)V9(2).                       
               10  MES-QCOLIRECEPT         PIC 9(5).                            
               10  MES-DTYPE               PIC X(1).                            
               10  MES-DSEMAINE            PIC X(6).                            
               10  MES-DLIVRAISON          PIC X(8).                            
               10  MES-QCDE                PIC 9(5).                            
               10  MES-QUOLIVR             PIC 9(5).                            
               10  MES-RETOUR3             PIC X(1).                            
                                                                                
