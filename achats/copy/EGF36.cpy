      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF : EGF36                                                     00000020
      ***************************************************************** 00000030
       01   EGF36I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFPRODL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCHEFPRODL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCHEFPRODF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCHEFPRODI     PIC X(5).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHEFPRODL    COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLCHEFPRODL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLCHEFPRODF    PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLCHEFPRODI    PIC X(20).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNCDEI    PIC X(7).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTCDEL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTCDEF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNENTCDEI      PIC X(5).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTCDEL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTCDEF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLENTCDEI      PIC X(20).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDLIVORIGL     COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MDLIVORIGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDLIVORIGF     PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDLIVORIGI     PIC X(8).                                  00000490
           02 TABLEI OCCURS   10 TIMES .                                00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNLIEUI      PIC X(3).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNCODICI     PIC X(7).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFFOURNL  COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MLREFFOURNL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLREFFOURNF  PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MLREFFOURNI  PIC X(20).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCDEORIGL   COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MQCDEORIGL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQCDEORIGF   PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MQCDEORIGI   PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCDEL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MQCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQCDEF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MQCDEI  PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDLIVRAISONL      COMP PIC S9(4).                       00000710
      *--                                                                       
             03 MDLIVRAISONL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 MDLIVRAISONF      PIC X.                                00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MDLIVRAISONI      PIC X(8).                             00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWCLTL  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MWCLTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MWCLTF  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MWCLTI  PIC X.                                          00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MZONCMDI  PIC X(15).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(58).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCICSI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNETNAMI  PIC X(8).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MSCREENI  PIC X(4).                                       00001020
      ***************************************************************** 00001030
      * SDF : EGF36                                                     00001040
      ***************************************************************** 00001050
       01   EGF36O REDEFINES EGF36I.                                    00001060
           02 FILLER    PIC X(12).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MDATJOUA  PIC X.                                          00001090
           02 MDATJOUC  PIC X.                                          00001100
           02 MDATJOUP  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUV  PIC X.                                          00001130
           02 MDATJOUO  PIC X(10).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MPAGEA    PIC X.                                          00001230
           02 MPAGEC    PIC X.                                          00001240
           02 MPAGEP    PIC X.                                          00001250
           02 MPAGEH    PIC X.                                          00001260
           02 MPAGEV    PIC X.                                          00001270
           02 MPAGEO    PIC X(3).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MPAGEMAXA      PIC X.                                     00001300
           02 MPAGEMAXC PIC X.                                          00001310
           02 MPAGEMAXP PIC X.                                          00001320
           02 MPAGEMAXH PIC X.                                          00001330
           02 MPAGEMAXV PIC X.                                          00001340
           02 MPAGEMAXO      PIC X(3).                                  00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MNSOCA    PIC X.                                          00001370
           02 MNSOCC    PIC X.                                          00001380
           02 MNSOCP    PIC X.                                          00001390
           02 MNSOCH    PIC X.                                          00001400
           02 MNSOCV    PIC X.                                          00001410
           02 MNSOCO    PIC X(3).                                       00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MCHEFPRODA     PIC X.                                     00001440
           02 MCHEFPRODC     PIC X.                                     00001450
           02 MCHEFPRODP     PIC X.                                     00001460
           02 MCHEFPRODH     PIC X.                                     00001470
           02 MCHEFPRODV     PIC X.                                     00001480
           02 MCHEFPRODO     PIC X(5).                                  00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MLCHEFPRODA    PIC X.                                     00001510
           02 MLCHEFPRODC    PIC X.                                     00001520
           02 MLCHEFPRODP    PIC X.                                     00001530
           02 MLCHEFPRODH    PIC X.                                     00001540
           02 MLCHEFPRODV    PIC X.                                     00001550
           02 MLCHEFPRODO    PIC X(20).                                 00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MNCDEA    PIC X.                                          00001580
           02 MNCDEC    PIC X.                                          00001590
           02 MNCDEP    PIC X.                                          00001600
           02 MNCDEH    PIC X.                                          00001610
           02 MNCDEV    PIC X.                                          00001620
           02 MNCDEO    PIC X(7).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MNENTCDEA      PIC X.                                     00001650
           02 MNENTCDEC PIC X.                                          00001660
           02 MNENTCDEP PIC X.                                          00001670
           02 MNENTCDEH PIC X.                                          00001680
           02 MNENTCDEV PIC X.                                          00001690
           02 MNENTCDEO      PIC X(5).                                  00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MLENTCDEA      PIC X.                                     00001720
           02 MLENTCDEC PIC X.                                          00001730
           02 MLENTCDEP PIC X.                                          00001740
           02 MLENTCDEH PIC X.                                          00001750
           02 MLENTCDEV PIC X.                                          00001760
           02 MLENTCDEO      PIC X(20).                                 00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MDLIVORIGA     PIC X.                                     00001790
           02 MDLIVORIGC     PIC X.                                     00001800
           02 MDLIVORIGP     PIC X.                                     00001810
           02 MDLIVORIGH     PIC X.                                     00001820
           02 MDLIVORIGV     PIC X.                                     00001830
           02 MDLIVORIGO     PIC X(8).                                  00001840
           02 TABLEO OCCURS   10 TIMES .                                00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MNLIEUA      PIC X.                                     00001870
             03 MNLIEUC PIC X.                                          00001880
             03 MNLIEUP PIC X.                                          00001890
             03 MNLIEUH PIC X.                                          00001900
             03 MNLIEUV PIC X.                                          00001910
             03 MNLIEUO      PIC X(3).                                  00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MNCODICA     PIC X.                                     00001940
             03 MNCODICC     PIC X.                                     00001950
             03 MNCODICP     PIC X.                                     00001960
             03 MNCODICH     PIC X.                                     00001970
             03 MNCODICV     PIC X.                                     00001980
             03 MNCODICO     PIC X(7).                                  00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MLREFFOURNA  PIC X.                                     00002010
             03 MLREFFOURNC  PIC X.                                     00002020
             03 MLREFFOURNP  PIC X.                                     00002030
             03 MLREFFOURNH  PIC X.                                     00002040
             03 MLREFFOURNV  PIC X.                                     00002050
             03 MLREFFOURNO  PIC X(20).                                 00002060
             03 FILLER       PIC X(2).                                  00002070
             03 MQCDEORIGA   PIC X.                                     00002080
             03 MQCDEORIGC   PIC X.                                     00002090
             03 MQCDEORIGP   PIC X.                                     00002100
             03 MQCDEORIGH   PIC X.                                     00002110
             03 MQCDEORIGV   PIC X.                                     00002120
             03 MQCDEORIGO   PIC X(5).                                  00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MQCDEA  PIC X.                                          00002150
             03 MQCDEC  PIC X.                                          00002160
             03 MQCDEP  PIC X.                                          00002170
             03 MQCDEH  PIC X.                                          00002180
             03 MQCDEV  PIC X.                                          00002190
             03 MQCDEO  PIC X(5).                                       00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MDLIVRAISONA      PIC X.                                00002220
             03 MDLIVRAISONC PIC X.                                     00002230
             03 MDLIVRAISONP PIC X.                                     00002240
             03 MDLIVRAISONH PIC X.                                     00002250
             03 MDLIVRAISONV PIC X.                                     00002260
             03 MDLIVRAISONO      PIC X(8).                             00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MWCLTA  PIC X.                                          00002290
             03 MWCLTC  PIC X.                                          00002300
             03 MWCLTP  PIC X.                                          00002310
             03 MWCLTH  PIC X.                                          00002320
             03 MWCLTV  PIC X.                                          00002330
             03 MWCLTO  PIC X.                                          00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MZONCMDA  PIC X.                                          00002360
           02 MZONCMDC  PIC X.                                          00002370
           02 MZONCMDP  PIC X.                                          00002380
           02 MZONCMDH  PIC X.                                          00002390
           02 MZONCMDV  PIC X.                                          00002400
           02 MZONCMDO  PIC X(15).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLIBERRA  PIC X.                                          00002430
           02 MLIBERRC  PIC X.                                          00002440
           02 MLIBERRP  PIC X.                                          00002450
           02 MLIBERRH  PIC X.                                          00002460
           02 MLIBERRV  PIC X.                                          00002470
           02 MLIBERRO  PIC X(58).                                      00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCODTRAA  PIC X.                                          00002500
           02 MCODTRAC  PIC X.                                          00002510
           02 MCODTRAP  PIC X.                                          00002520
           02 MCODTRAH  PIC X.                                          00002530
           02 MCODTRAV  PIC X.                                          00002540
           02 MCODTRAO  PIC X(4).                                       00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MCICSA    PIC X.                                          00002570
           02 MCICSC    PIC X.                                          00002580
           02 MCICSP    PIC X.                                          00002590
           02 MCICSH    PIC X.                                          00002600
           02 MCICSV    PIC X.                                          00002610
           02 MCICSO    PIC X(5).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MNETNAMA  PIC X.                                          00002640
           02 MNETNAMC  PIC X.                                          00002650
           02 MNETNAMP  PIC X.                                          00002660
           02 MNETNAMH  PIC X.                                          00002670
           02 MNETNAMV  PIC X.                                          00002680
           02 MNETNAMO  PIC X(8).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MSCREENA  PIC X.                                          00002710
           02 MSCREENC  PIC X.                                          00002720
           02 MSCREENP  PIC X.                                          00002730
           02 MSCREENH  PIC X.                                          00002740
           02 MSCREENV  PIC X.                                          00002750
           02 MSCREENO  PIC X(4).                                       00002760
                                                                                
