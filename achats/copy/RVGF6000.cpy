      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGF6000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF6000                         
      **********************************************************                
       01  RVGF6000.                                                            
           02  GF60-NBONCDE                                                     
               PIC X(0005).                                                     
           02  GF60-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGF6000                                  
      **********************************************************                
       01  RVGF6000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF60-NBONCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF60-NBONCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF60-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GF60-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
