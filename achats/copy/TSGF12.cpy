      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : LIGNES DE COMMANDES TRAITEE PAR TGF12                  *         
      *****************************************************************         
      *                                                               *         
       01  TS-GF12.                                                             
      *--------------------------------  LONGUEUR TS                            
           02 TS-GF12-LONG                    PIC S9(5) COMP-3                  
      *                                       VALUE +87.                        
                                              VALUE +88.                        
           02 TS-GF12-DONNEES.                                                  
             03 TS-GF12-GF05.                                                   
      *--------------------------------  TABLE LIGNE COMMANDE                   
               04 TS-GF12-NCODIC              PIC X(7).                         
      *        04 TS-GF12-QSOUHAITEE          PIC 9(5) COMP-3.                  
               04 TS-GF12-QSOUHAITEE          PIC S9(5) COMP-3.                 
               04 TS-GF12-DSOUHAITEE          PIC X(8).                         
               04 TS-GF12-NSOCIETE            PIC X(3).                         
               04 TS-GF12-NLIEU               PIC X(3).                         
               04 TS-GF12-CHEFPROD            PIC X(5).                         
               04 TS-GF12-NPAGE               PIC X(3).                         
               04 TS-GF12-NLIGNE              PIC X(2).                         
               04 TS-GF12-DETAT               PIC X(8).                         
               04 TS-GF12-NSEQ                PIC X(7).                         
             03 TS-GF12-GA00.                                                   
      *--------------------------------  TABLE ARTICLE                          
               04 TS-GF12-CFAM                PIC X(5).                         
               04 TS-GF12-CMARQ               PIC X(5).                         
               04 TS-GF12-LREFFOURN           PIC X(20).                        
               04 TS-GF12-CMODSTOCK           PIC X(5).                         
               04 TS-GF12-QCOLIRECEPT         PIC 9(5) COMP-3.                  
             03 TS-GF12-GF05.                                                   
      *--------------------------------  TABLE CF                               
               04 TS-GF12-CFEXT               PIC X.                            
                                                                                
