      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00010011
      *  COMMAREA SPECIFIQUE PROGRAMME TGF14                       *    00020011
      *        TR  :  GF00   EMISSION DE LA COMMANDE               *    00030011
      *        PG  :  TGA14  CREATION D' UNE COMMANDE              *    00040011
      **************************************************************    00050011
      *                                                                 00060011
      * ZONES RESERVEES APPLICATIVES ---------------------------- 5720  00070011
      *                                                                 00080011
      * PROGRAMME  TGF14  :  CREATION D' UNE COMMANDE              *    00090011
      *                                                                 00100011
           03 COMM-GF14-APPLI REDEFINES COMM-GF12-PROPRE.               00110011
      *---------------------------------------NOMBRE DE PAGES DE LA CDE 00120011
              04 COMM-GF14-NBRPAGE            PIC    9(5) COMP-3.       00130011
      *---------------------------------------NUMERO DE PAGE AFFICHE    00140011
              04 COMM-GF14-NUMPAGE            PIC    9(5) COMP-3.       00150011
      *---------------------------------------NOMBRE D' ITEMS DE LA CDE 00160011
              04 COMM-GF14-NBRITEM            PIC    9(5) COMP-3.       00170011
      *---------------------------------------NUMERO D' ITEM AFFICHE    00180011
              04 COMM-GF14-NUMITEM            PIC    9(5) COMP-3.       00190011
      *---------------------------------------CODE RETOUR               00200011
      *                                       '0' -> OK                 00210011
      *                                       '1' -> ERREUR             00220011
              04 COMM-GF14-CODRET             PIC    X.                 00230011
                 88 COMM-GF14-OK              VALUE  '0'.               00240011
                 88 COMM-GF14-ERREUR          VALUE  '1'.               00250011
      *---------------------------------------PROGRAMME RETOUR          00260011
              04 COMM-GF14-PROGRET            PIC    X(5).              00270011
      *---------------------------------------NUMERO PAGE VERTICAL      00280011
              04 COMM-GF14-PAGEQUOTA          PIC    9(2).              00290011
      *---------------------------------------NUMERO PAGE VERTICAL MAX  00300011
              04 COMM-GF14-PQMAX              PIC    9(2).              00310011
      *---------------------------------------RESERVE                   00320011
              04 COMM-GF14-RESERVE            PIC    X(06).             00330011
      *---------------------------------------4 ZONES => 1 POSTE PAR    00340011
      *                                                  SEMAINE        00350011
              04 COMM-GF14-SEMAINE            OCCURS 4.                 00360011
      *---------------------------------------NUMERO DE SEMAINE         00370011
                 05 COMM-GF14-NSEMAINE        PIC    X(2).              00380011
      *---------------------------------------ANNEE SOUS FORME SSAA     00390011
                 05 COMM-GF14-DANNEE          PIC    X(4).              00400011
      *          05 COMM-GF14-NJOUR           PIC    X(1).              00410011
                 05 COMM-GF14-POSTE-QUOTA     OCCURS 6.                 00420011
      *---------------------------------------NOMBRE D' UO DISPONIBLES  00430011
      *                                       INITIALEMENT              00440011
                    06 COMM-GF14-QUOTA-INIT      PIC    S9(7) COMP-3.   00450011
      *---------------------------------------NOMBRE D' UO NECCESSAIRES 00460011
      *                                       A CETTE CDE PAR SEMAINE   00470011
                    06 COMM-GF14-NBRUO-SEM       PIC    S9(7) COMP-3.   00480011
      *---------------------------------------7 JOURS DE LA SEMAINE     00490011
      *                                       ORDRE : LMMJVSD           00500011
                    06 COMM-GF14-JOURS           OCCURS 7.              00510011
      *---------------------------------------NOMBRE D' UO NECCESSAIRES 00520011
      *                                       A CETTE CDE PAR JOUR      00530011
                       07 COMM-GF14-NBRUO-JOUR   PIC    S9(7) COMP-3.   00540011
      *---------------------------------------QUANTITE COMMANDEE        00550011
      *                                                                 00560011
                       07 COMM-GF14-QCDE         PIC    S9(7) COMP-3.   00570011
      *---------------------------------------TYPE DE QUOTA             00580011
              04 COMM-GF14-TABCQUOTA.                                   00590011
                 05 COMM-GF14-TABQUOTA           OCCURS 6.              00600011
                    06 COMM-GF14-CQUOTA          PIC    X(5).           00610011
AB    *--------------------------------------FLAG VALO AUTOMATIQUE      00620011
AB            04 COMM-GF14-VALO                  PIC X(1).              00630011
AS1   *--------------------------------------FLAG INTERFACE NCG-JDA     00640011
AS1           04 COMM-GF14-INTRFC                PIC X(1).              00650011
E0191.*--- ZONE D'AFFICHAGE VARIABLE                                    00660011
              04 COMM-GF14-LIGNES.                                      00670011
                 05 COMM-GF14-LIGNE11         PIC X(24).                00680011
E0311            05 COMM-GF14-LIGNE10         PIC X(22).                00690011
E0191         04 COMM-GF14-FLAG-SAM           PIC X(01).                00700011
E0278.*--- FLAG DU PARAMETRAGE DS XCTRL DECOUPAGE CDE MONOCODIC         00710011
              04 COMM-GF14-P-CDEMCD           PIC X(1).                 00720011
      *--- FLAG DE LA SELCTION DU DECOUPAGE CDE MONOCODIC               00730011
              04 COMM-GF14-CDEMONOCD          PIC X(1).                 00740011
.E0278        04 COMM-GF14-LMONOCD            PIC X(21).                00750011
      *---------------------------------------FILLER                    00760011
      *       04 COMM-GF14-FILLER             PIC    X(2500).           00770011
                                                                                
