      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      * TS TGF48   : QUOTA PLANNING RAYON                                       
      *                                                                         
       01  TS-GF48B.                                                            
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03  TS-GF48B-LONG             PIC  S9(4) COMP  VALUE +726.           
      *--                                                                       
           03  TS-GF48B-LONG             PIC  S9(4) COMP-5  VALUE +726.         
      *}                                                                        
      *                                                                         
           03  TS-GF48B-DONNEES.                                                
      *                                                                         
             05 TS-GF48B-TABLEAU           OCCURS 11.                           
                    10 TS-GF48B-NENTCDE      PIC X(5).                          
                    10 TS-GF48B-LENTCDE      PIC X(15).                         
                    10 TS-GF48B-NCDE         PIC X(7).                          
                    10 TS-GF48B-NCODIC       PIC X(7).                          
                    10 TS-GF48B-CFAM         PIC X(5).                          
                    10 TS-GF48B-CMARQ        PIC X(5).                          
                    10 TS-GF48B-LREF         PIC X(15).                         
                    10 TS-GF48B-QTE          PIC S9(5) COMP-3.                  
                    10 TS-GF48B-QUO          PIC S9(5) COMP-3.                  
                    10 TS-GF48B-FLAG         PIC X(1).                          
      *                                                                         
                                                                                
