      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : LIGNES DE COMMANDES TRAITEE PAR TGF46                  *         
      *****************************************************************         
      *                                                               *         
       01  TS-GF46.                                                             
      *--------------------------------  LONGUEUR TS = 1443 + 3=LG              
           02 TS-GF46-LONG                    PIC S9(5) COMP-3                  
                                              VALUE +1446.                      
           02 TS-GF46-DONNEES.                                                  
             03 TS-GF46-POSTE          OCCURS 13.                               
               04 TS-GF46-GF05.                                                 
      *--------------------------------  TABLE LIGNE COMMANDE                   
                 05 TS-GF46-NCODIC            PIC X(7).                         
                 05 TS-GF46-QSOUHAITEE        PIC S9(5) COMP-3.                 
                 05 TS-GF46-DSOUHAITEE        PIC X(8).                         
                 05 TS-GF46-NSOCIETE          PIC X(3).                         
                 05 TS-GF46-NLIEU             PIC X(3).                         
                 05 TS-GF46-NSOCLIVR          PIC X(3).                         
                 05 TS-GF46-NDEPOT            PIC X(3).                         
                 05 TS-GF46-CHEFPROD          PIC X(5).                         
                 05 TS-GF46-CINTERLOCUT       PIC X(5).                         
                 05 TS-GF46-NPAGE             PIC X(3).                         
                 05 TS-GF46-NLIGNE            PIC X(2).                         
                 05 TS-GF46-DETAT             PIC X(8).                         
                 05 TS-GF46-NSEQ              PIC X(7).                         
                 05 TS-GF46-ANNEE             PIC X(4).                         
                 05 TS-GF46-SEMAINE           PIC X(2).                         
                 05 TS-GF46-NJOUR             PIC X.                            
                 05 TS-GF46-CQUOTA            PIC X(5).                         
               04 TS-GF46-GA00.                                                 
      *--------------------------------  TABLE ARTICLE                          
                 05 TS-GF46-CFAM              PIC X(5).                         
                 05 TS-GF46-CMARQ             PIC X(5).                         
                 05 TS-GF46-LREFFOURN         PIC X(20).                        
                 05 TS-GF46-CMODSTOCK         PIC X(5).                         
                 05 TS-GF46-QCOLIRECEPT       PIC 9(5) COMP-3.                  
      *--------------------------------  FLAG DIVERS ET AVARIES                 
               04 TS-GF46-MODIF               PIC X.                            
               04 TS-GF46-GRP                 PIC X.                            
                                                                                
