      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : ETAT DES LIEUX INITIAL                                 *         
      *        POUR LES CALAGES DES COMMANDES                         *         
      *****************************************************************         
       01 TS-GFCAL1-DESCR.                                                      
          02 TS-GFCAL1.                                                         
             03 TS-GFCAL1-NSURCDE         PIC X(07).                            
             03 TS-GFCAL1-NCDE            PIC X(07).                            
             03 TS-GFCAL1-NCODIC          PIC X(07).                            
             03 TS-GFCAL1-NDEPOT          PIC X(06).                            
             03 TS-GFCAL1-QCDETOT         PIC S9(05) COMP-3.                    
             03 TS-GFCAL1-POSTE OCCURS 20 TIMES.                                
                04 TS-GFCAL1-DLIVRAISON   PIC X(08).                            
                04 TS-GFCAL1-QCDE         PIC S9(05) COMP-3.                    
                                                                                
