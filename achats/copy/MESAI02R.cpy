      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ VALIDATION BDD KESA                                  
      *                                                                         
      *****************************************************************         
      * VERSION POUR MAI02 AVEC PUT ET GET EXTERNES                             
      *                                                                         
         10  WS-MESSAGE-RECU REDEFINES COMM-MQ13-MESSAGE.                       
           17  MESR-ENTETE.                                                     
               20   MESR-TYPE     PIC    X(3).                                  
               20   MESR-NSOCMSG  PIC    X(3).                                  
               20   MESR-NLIEUMSG PIC    X(3).                                  
               20   MESR-NSOCDST  PIC    X(3).                                  
               20   MESR-NLIEUDST PIC    X(3).                                  
               20   MESR-NORD     PIC    9(8).                                  
               20   MESR-LPROG    PIC    X(10).                                 
               20   MESR-DJOUR    PIC    X(8).                                  
               20   MESR-WSID     PIC    X(10).                                 
               20   MESR-USER     PIC    X(10).                                 
               20   MESR-CHRONO   PIC    9(7).                                  
               20   MESR-NBRMSG   PIC    9(7).                                  
 CRO  *        20   MESR-FILLER   PIC    X(30).                                 
               20   MESR-NBENR     PIC 9(05).                                   
               20   MESR-TAILLE    PIC 9(05).                                   
               20   MESR-VERSION2  PIC X(02).                                   
               20   MESR-DSYST     PIC S9(13).                                  
               20   MESR-FILLER    PIC X(05).                                   
           17  MESR-MAI02.                                                      
             18  MESR-MESSAGE.                                                  
               20  MESR-W-PERTDSEC PIC X.                                       
               20  MESR-W-NCODICK  PIC X(7).                                    
               20  MESR-W-NCODIC   PIC X(7).                                    
               20  MESR-W-MULTISOC PIC X(1).                                    
             18  MESR-SORTIE.                                                   
               20 MESR-CODRET          PIC X.                                   
                       88 MESR-OK      VALUE '1'.                               
                       88 MESR-NOK VALUE '0'.                                   
CT12           20 MESR-LIBLIBRE        PIC X(30).                               
               20 MESR-LIBELLE.                                                 
                       25 MESR-NSEQERR PIC X(4).                                
CT12                   25 MESR-NOMPGRM PIC X(6).                                
CT12  *                25 MESR-LIBERR      PIC X(54).                           
                                                                                
