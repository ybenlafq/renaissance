      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF44   EGF44                                              00000020
      ***************************************************************** 00000030
       01   EGF44I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATSAIL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MDATSAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATSAIF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MDATSAII  PIC X(8).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATVALL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDATVALL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATVALF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDATVALI  PIC X(8).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNSOCI    PIC X(3).                                       00000290
           02 MNDEPD OCCURS   4 TIMES .                                 00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDEPL  COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 MNDEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNDEPF  PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MNDEPI  PIC X(3).                                       00000340
           02 MTABI OCCURS   12 TIMES .                                 00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCSPECL   COMP PIC S9(4).                            00000360
      *--                                                                       
             03 MNSOCSPECL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNSOCSPECF   PIC X.                                     00000370
             03 FILLER  PIC X(4).                                       00000380
             03 MNSOCSPECI   PIC X(3).                                  00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUSPECL  COMP PIC S9(4).                            00000400
      *--                                                                       
             03 MNLIEUSPECL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNLIEUSPECF  PIC X.                                     00000410
             03 FILLER  PIC X(4).                                       00000420
             03 MNLIEUSPECI  PIC X(3).                                  00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000440
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000450
             03 FILLER  PIC X(4).                                       00000460
             03 MNCODICI     PIC X(7).                                  00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFFOURNL  COMP PIC S9(4).                            00000480
      *--                                                                       
             03 MLREFFOURNL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLREFFOURNF  PIC X.                                     00000490
             03 FILLER  PIC X(4).                                       00000500
             03 MLREFFOURNI  PIC X(20).                                 00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTOTL  COMP PIC S9(4).                                 00000520
      *--                                                                       
             03 MQTOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTOTF  PIC X.                                          00000530
             03 FILLER  PIC X(4).                                       00000540
             03 MQTOTI  PIC X(6).                                       00000550
             03 MQCDED OCCURS   4 TIMES .                               00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MQCDEL     COMP PIC S9(4).                            00000570
      *--                                                                       
               04 MQCDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MQCDEF     PIC X.                                     00000580
               04 FILLER     PIC X(4).                                  00000590
               04 MQCDEI     PIC X(5).                                  00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MZONCMDI  PIC X(15).                                      00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MLIBERRI  PIC X(58).                                      00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MCODTRAI  PIC X(4).                                       00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MCICSI    PIC X(5).                                       00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MNETNAMI  PIC X(8).                                       00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MSCREENI  PIC X(4).                                       00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWLISTL   COMP PIC S9(4).                                 00000850
      *--                                                                       
           02 MWLISTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWLISTF   PIC X.                                          00000860
           02 FILLER    PIC X(4).                                       00000870
           02 MWLISTI   PIC X(3).                                       00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSURCDEL      COMP PIC S9(4).                            00000890
      *--                                                                       
           02 MNSURCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSURCDEF      PIC X.                                     00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MNSURCDEI      PIC X(7).                                  00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPCOL  COMP PIC S9(4).                                 00000930
      *--                                                                       
           02 MCTYPCOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTYPCOF  PIC X.                                          00000940
           02 FILLER    PIC X(4).                                       00000950
           02 MCTYPCOI  PIC X(5).                                       00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTCDEL      COMP PIC S9(4).                            00000970
      *--                                                                       
           02 MNENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTCDEF      PIC X.                                     00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MNENTCDEI      PIC X(5).                                  00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTCDEL      COMP PIC S9(4).                            00001010
      *--                                                                       
           02 MLENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTCDEF      PIC X.                                     00001020
           02 FILLER    PIC X(4).                                       00001030
           02 MLENTCDEI      PIC X(20).                                 00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERL  COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MCINTERL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCINTERF  PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MCINTERI  PIC X(5).                                       00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERL  COMP PIC S9(4).                                 00001090
      *--                                                                       
           02 MLINTERL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLINTERF  PIC X.                                          00001100
           02 FILLER    PIC X(4).                                       00001110
           02 MLINTERI  PIC X(20).                                      00001120
      ***************************************************************** 00001130
      * SDF: EGF44   EGF44                                              00001140
      ***************************************************************** 00001150
       01   EGF44O REDEFINES EGF44I.                                    00001160
           02 FILLER    PIC X(12).                                      00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MDATJOUA  PIC X.                                          00001190
           02 MDATJOUC  PIC X.                                          00001200
           02 MDATJOUP  PIC X.                                          00001210
           02 MDATJOUH  PIC X.                                          00001220
           02 MDATJOUV  PIC X.                                          00001230
           02 MDATJOUO  PIC X(10).                                      00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MTIMJOUA  PIC X.                                          00001260
           02 MTIMJOUC  PIC X.                                          00001270
           02 MTIMJOUP  PIC X.                                          00001280
           02 MTIMJOUH  PIC X.                                          00001290
           02 MTIMJOUV  PIC X.                                          00001300
           02 MTIMJOUO  PIC X(5).                                       00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MWPAGEA   PIC X.                                          00001330
           02 MWPAGEC   PIC X.                                          00001340
           02 MWPAGEP   PIC X.                                          00001350
           02 MWPAGEH   PIC X.                                          00001360
           02 MWPAGEV   PIC X.                                          00001370
           02 MWPAGEO   PIC X(3).                                       00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MDATSAIA  PIC X.                                          00001400
           02 MDATSAIC  PIC X.                                          00001410
           02 MDATSAIP  PIC X.                                          00001420
           02 MDATSAIH  PIC X.                                          00001430
           02 MDATSAIV  PIC X.                                          00001440
           02 MDATSAIO  PIC X(8).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MDATVALA  PIC X.                                          00001470
           02 MDATVALC  PIC X.                                          00001480
           02 MDATVALP  PIC X.                                          00001490
           02 MDATVALH  PIC X.                                          00001500
           02 MDATVALV  PIC X.                                          00001510
           02 MDATVALO  PIC X(8).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNSOCA    PIC X.                                          00001540
           02 MNSOCC    PIC X.                                          00001550
           02 MNSOCP    PIC X.                                          00001560
           02 MNSOCH    PIC X.                                          00001570
           02 MNSOCV    PIC X.                                          00001580
           02 MNSOCO    PIC X(3).                                       00001590
           02 DFHMS1 OCCURS   4 TIMES .                                 00001600
             03 FILLER       PIC X(2).                                  00001610
             03 MNDEPA  PIC X.                                          00001620
             03 MNDEPC  PIC X.                                          00001630
             03 MNDEPP  PIC X.                                          00001640
             03 MNDEPH  PIC X.                                          00001650
             03 MNDEPV  PIC X.                                          00001660
             03 MNDEPO  PIC X(3).                                       00001670
           02 MTABO OCCURS   12 TIMES .                                 00001680
             03 FILLER       PIC X(2).                                  00001690
             03 MNSOCSPECA   PIC X.                                     00001700
             03 MNSOCSPECC   PIC X.                                     00001710
             03 MNSOCSPECP   PIC X.                                     00001720
             03 MNSOCSPECH   PIC X.                                     00001730
             03 MNSOCSPECV   PIC X.                                     00001740
             03 MNSOCSPECO   PIC X(3).                                  00001750
             03 FILLER       PIC X(2).                                  00001760
             03 MNLIEUSPECA  PIC X.                                     00001770
             03 MNLIEUSPECC  PIC X.                                     00001780
             03 MNLIEUSPECP  PIC X.                                     00001790
             03 MNLIEUSPECH  PIC X.                                     00001800
             03 MNLIEUSPECV  PIC X.                                     00001810
             03 MNLIEUSPECO  PIC X(3).                                  00001820
             03 FILLER       PIC X(2).                                  00001830
             03 MNCODICA     PIC X.                                     00001840
             03 MNCODICC     PIC X.                                     00001850
             03 MNCODICP     PIC X.                                     00001860
             03 MNCODICH     PIC X.                                     00001870
             03 MNCODICV     PIC X.                                     00001880
             03 MNCODICO     PIC X(7).                                  00001890
             03 FILLER       PIC X(2).                                  00001900
             03 MLREFFOURNA  PIC X.                                     00001910
             03 MLREFFOURNC  PIC X.                                     00001920
             03 MLREFFOURNP  PIC X.                                     00001930
             03 MLREFFOURNH  PIC X.                                     00001940
             03 MLREFFOURNV  PIC X.                                     00001950
             03 MLREFFOURNO  PIC X(20).                                 00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MQTOTA  PIC X.                                          00001980
             03 MQTOTC  PIC X.                                          00001990
             03 MQTOTP  PIC X.                                          00002000
             03 MQTOTH  PIC X.                                          00002010
             03 MQTOTV  PIC X.                                          00002020
             03 MQTOTO  PIC X(6).                                       00002030
             03 DFHMS2 OCCURS   4 TIMES .                               00002040
               04 FILLER     PIC X(2).                                  00002050
               04 MQCDEA     PIC X.                                     00002060
               04 MQCDEC     PIC X.                                     00002070
               04 MQCDEP     PIC X.                                     00002080
               04 MQCDEH     PIC X.                                     00002090
               04 MQCDEV     PIC X.                                     00002100
               04 MQCDEO     PIC X(5).                                  00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MZONCMDA  PIC X.                                          00002130
           02 MZONCMDC  PIC X.                                          00002140
           02 MZONCMDP  PIC X.                                          00002150
           02 MZONCMDH  PIC X.                                          00002160
           02 MZONCMDV  PIC X.                                          00002170
           02 MZONCMDO  PIC X(15).                                      00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MLIBERRA  PIC X.                                          00002200
           02 MLIBERRC  PIC X.                                          00002210
           02 MLIBERRP  PIC X.                                          00002220
           02 MLIBERRH  PIC X.                                          00002230
           02 MLIBERRV  PIC X.                                          00002240
           02 MLIBERRO  PIC X(58).                                      00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MCODTRAA  PIC X.                                          00002270
           02 MCODTRAC  PIC X.                                          00002280
           02 MCODTRAP  PIC X.                                          00002290
           02 MCODTRAH  PIC X.                                          00002300
           02 MCODTRAV  PIC X.                                          00002310
           02 MCODTRAO  PIC X(4).                                       00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MCICSA    PIC X.                                          00002340
           02 MCICSC    PIC X.                                          00002350
           02 MCICSP    PIC X.                                          00002360
           02 MCICSH    PIC X.                                          00002370
           02 MCICSV    PIC X.                                          00002380
           02 MCICSO    PIC X(5).                                       00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MNETNAMA  PIC X.                                          00002410
           02 MNETNAMC  PIC X.                                          00002420
           02 MNETNAMP  PIC X.                                          00002430
           02 MNETNAMH  PIC X.                                          00002440
           02 MNETNAMV  PIC X.                                          00002450
           02 MNETNAMO  PIC X(8).                                       00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MSCREENA  PIC X.                                          00002480
           02 MSCREENC  PIC X.                                          00002490
           02 MSCREENP  PIC X.                                          00002500
           02 MSCREENH  PIC X.                                          00002510
           02 MSCREENV  PIC X.                                          00002520
           02 MSCREENO  PIC X(4).                                       00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MWLISTA   PIC X.                                          00002550
           02 MWLISTC   PIC X.                                          00002560
           02 MWLISTP   PIC X.                                          00002570
           02 MWLISTH   PIC X.                                          00002580
           02 MWLISTV   PIC X.                                          00002590
           02 MWLISTO   PIC X(3).                                       00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MNSURCDEA      PIC X.                                     00002620
           02 MNSURCDEC PIC X.                                          00002630
           02 MNSURCDEP PIC X.                                          00002640
           02 MNSURCDEH PIC X.                                          00002650
           02 MNSURCDEV PIC X.                                          00002660
           02 MNSURCDEO      PIC X(7).                                  00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MCTYPCOA  PIC X.                                          00002690
           02 MCTYPCOC  PIC X.                                          00002700
           02 MCTYPCOP  PIC X.                                          00002710
           02 MCTYPCOH  PIC X.                                          00002720
           02 MCTYPCOV  PIC X.                                          00002730
           02 MCTYPCOO  PIC X(5).                                       00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MNENTCDEA      PIC X.                                     00002760
           02 MNENTCDEC PIC X.                                          00002770
           02 MNENTCDEP PIC X.                                          00002780
           02 MNENTCDEH PIC X.                                          00002790
           02 MNENTCDEV PIC X.                                          00002800
           02 MNENTCDEO      PIC X(5).                                  00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MLENTCDEA      PIC X.                                     00002830
           02 MLENTCDEC PIC X.                                          00002840
           02 MLENTCDEP PIC X.                                          00002850
           02 MLENTCDEH PIC X.                                          00002860
           02 MLENTCDEV PIC X.                                          00002870
           02 MLENTCDEO      PIC X(20).                                 00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCINTERA  PIC X.                                          00002900
           02 MCINTERC  PIC X.                                          00002910
           02 MCINTERP  PIC X.                                          00002920
           02 MCINTERH  PIC X.                                          00002930
           02 MCINTERV  PIC X.                                          00002940
           02 MCINTERO  PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MLINTERA  PIC X.                                          00002970
           02 MLINTERC  PIC X.                                          00002980
           02 MLINTERP  PIC X.                                          00002990
           02 MLINTERH  PIC X.                                          00003000
           02 MLINTERV  PIC X.                                          00003010
           02 MLINTERO  PIC X(20).                                      00003020
                                                                                
