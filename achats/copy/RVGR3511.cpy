      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGR3511                                             
      **********************************************************                
      *   AVEC LES ZONES DE LA D3E                                              
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGR3511                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGR3511.                                                            
           02  GR35-NREC                                                        
               PIC X(0007).                                                     
           02  GR35-WTLMELA                                                     
               PIC X(0001).                                                     
           02  GR35-NRECCPT                                                     
               PIC X(0007).                                                     
           02  GR35-NCDE                                                        
               PIC X(0007).                                                     
           02  GR35-NBL                                                         
               PIC X(0010).                                                     
           02  GR35-NCODIC                                                      
               PIC X(0007).                                                     
           02  GR35-NAVENANT                                                    
               PIC X(0002).                                                     
           02  GR35-DVALIDITE                                                   
               PIC X(0008).                                                     
           02  GR35-QDELRGLT                                                    
               PIC S9(3) COMP-3.                                                
           02  GR35-CFAM                                                        
               PIC X(0005).                                                     
           02  GR35-CMARQ                                                       
               PIC X(0005).                                                     
           02  GR35-LREFFOURN                                                   
               PIC X(0020).                                                     
           02  GR35-CTAUXTVA                                                    
               PIC X(0005).                                                     
           02  GR35-DMVTREC                                                     
               PIC X(0008).                                                     
           02  GR35-CTYPMVT                                                     
               PIC X(0003).                                                     
           02  GR35-CMVT                                                        
               PIC X(0001).                                                     
           02  GR35-QREC                                                        
               PIC S9(5) COMP-3.                                                
           02  GR35-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GR35-PABASEFACT                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GR35-PROMOFACT                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GR35-PTAUXESCPT                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GR35-PRA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GR35-QPRISE                                                      
               PIC S9(5) COMP-3.                                                
           02  GR35-NENTCDEPT                                                   
               PIC X(0007).                                                     
           02  GR35-PCONTRFACT                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GR35-PCONTRIMPO                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGR3511                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGR3511-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-NREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-NREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-WTLMELA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-WTLMELA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-NRECCPT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-NRECCPT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-NBL-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-NBL-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-NAVENANT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-NAVENANT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-DVALIDITE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-DVALIDITE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-QDELRGLT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-QDELRGLT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-LREFFOURN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-LREFFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-CTAUXTVA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-CTAUXTVA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-DMVTREC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-DMVTREC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-CTYPMVT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-CTYPMVT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-CMVT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-CMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-QREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-QREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-PABASEFACT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-PABASEFACT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-PROMOFACT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-PROMOFACT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-PTAUXESCPT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-PTAUXESCPT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-PRA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-PRA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-QPRISE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-QPRISE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-NENTCDEPT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-NENTCDEPT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-PCONTRFACT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-PCONTRFACT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR35-PCONTRIMPO-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR35-PCONTRIMPO-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EXEC SQL END DECLARE SECTION END-EXEC.      
       EJECT                                                                    
                                                                                
