      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF20   EGF20                                              00000020
      ***************************************************************** 00000030
       01   EGF20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNDEPOTI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFPL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCHEFPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHEFPF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCHEFPI   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHEFPL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLCHEFPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCHEFPF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLCHEFPI  PIC X(20).                                      00000370
           02 LIGNEI OCCURS   12 TIMES .                                00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNCODICI     PIC X(7).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENTCDEL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MENTCDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MENTCDEF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MENTCDEI     PIC X(5).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MLREFI  PIC X(20).                                      00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCOM1L      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MQCOM1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQCOM1F      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MQCOM1I      PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCOM1L      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MDCOM1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDCOM1F      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MDCOM1I      PIC X(6).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCOM2L      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MQCOM2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQCOM2F      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MQCOM2I      PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCOM2L      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MDCOM2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDCOM2F      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MDCOM2I      PIC X(6).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCOM3L      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MQCOM3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQCOM3F      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MQCOM3I      PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCOM3L      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MDCOM3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDCOM3F      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MDCOM3I      PIC X(6).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MZONCMDI  PIC X(15).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLIBERRI  PIC X(54).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MNETNAMI  PIC X(8).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MSCREENI  PIC X(4).                                       00000980
      ***************************************************************** 00000990
      * SDF: EGF20   EGF20                                              00001000
      ***************************************************************** 00001010
       01   EGF20O REDEFINES EGF20I.                                    00001020
           02 FILLER    PIC X(12).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTIMJOUA  PIC X.                                          00001120
           02 MTIMJOUC  PIC X.                                          00001130
           02 MTIMJOUP  PIC X.                                          00001140
           02 MTIMJOUH  PIC X.                                          00001150
           02 MTIMJOUV  PIC X.                                          00001160
           02 MTIMJOUO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MWPAGEA   PIC X.                                          00001190
           02 MWPAGEC   PIC X.                                          00001200
           02 MWPAGEP   PIC X.                                          00001210
           02 MWPAGEH   PIC X.                                          00001220
           02 MWPAGEV   PIC X.                                          00001230
           02 MWPAGEO   PIC X(3).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MWFONCA   PIC X.                                          00001260
           02 MWFONCC   PIC X.                                          00001270
           02 MWFONCP   PIC X.                                          00001280
           02 MWFONCH   PIC X.                                          00001290
           02 MWFONCV   PIC X.                                          00001300
           02 MWFONCO   PIC X(3).                                       00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MNSOCA    PIC X.                                          00001330
           02 MNSOCC    PIC X.                                          00001340
           02 MNSOCP    PIC X.                                          00001350
           02 MNSOCH    PIC X.                                          00001360
           02 MNSOCV    PIC X.                                          00001370
           02 MNSOCO    PIC X(3).                                       00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MNDEPOTA  PIC X.                                          00001400
           02 MNDEPOTC  PIC X.                                          00001410
           02 MNDEPOTP  PIC X.                                          00001420
           02 MNDEPOTH  PIC X.                                          00001430
           02 MNDEPOTV  PIC X.                                          00001440
           02 MNDEPOTO  PIC X(3).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MCHEFPA   PIC X.                                          00001470
           02 MCHEFPC   PIC X.                                          00001480
           02 MCHEFPP   PIC X.                                          00001490
           02 MCHEFPH   PIC X.                                          00001500
           02 MCHEFPV   PIC X.                                          00001510
           02 MCHEFPO   PIC X(5).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MLCHEFPA  PIC X.                                          00001540
           02 MLCHEFPC  PIC X.                                          00001550
           02 MLCHEFPP  PIC X.                                          00001560
           02 MLCHEFPH  PIC X.                                          00001570
           02 MLCHEFPV  PIC X.                                          00001580
           02 MLCHEFPO  PIC X(20).                                      00001590
           02 LIGNEO OCCURS   12 TIMES .                                00001600
             03 FILLER       PIC X(2).                                  00001610
             03 MNCODICA     PIC X.                                     00001620
             03 MNCODICC     PIC X.                                     00001630
             03 MNCODICP     PIC X.                                     00001640
             03 MNCODICH     PIC X.                                     00001650
             03 MNCODICV     PIC X.                                     00001660
             03 MNCODICO     PIC X(7).                                  00001670
             03 FILLER       PIC X(2).                                  00001680
             03 MENTCDEA     PIC X.                                     00001690
             03 MENTCDEC     PIC X.                                     00001700
             03 MENTCDEP     PIC X.                                     00001710
             03 MENTCDEH     PIC X.                                     00001720
             03 MENTCDEV     PIC X.                                     00001730
             03 MENTCDEO     PIC X(5).                                  00001740
             03 FILLER       PIC X(2).                                  00001750
             03 MLREFA  PIC X.                                          00001760
             03 MLREFC  PIC X.                                          00001770
             03 MLREFP  PIC X.                                          00001780
             03 MLREFH  PIC X.                                          00001790
             03 MLREFV  PIC X.                                          00001800
             03 MLREFO  PIC X(20).                                      00001810
             03 FILLER       PIC X(2).                                  00001820
             03 MQCOM1A      PIC X.                                     00001830
             03 MQCOM1C PIC X.                                          00001840
             03 MQCOM1P PIC X.                                          00001850
             03 MQCOM1H PIC X.                                          00001860
             03 MQCOM1V PIC X.                                          00001870
             03 MQCOM1O      PIC X(5).                                  00001880
             03 FILLER       PIC X(2).                                  00001890
             03 MDCOM1A      PIC X.                                     00001900
             03 MDCOM1C PIC X.                                          00001910
             03 MDCOM1P PIC X.                                          00001920
             03 MDCOM1H PIC X.                                          00001930
             03 MDCOM1V PIC X.                                          00001940
             03 MDCOM1O      PIC X(6).                                  00001950
             03 FILLER       PIC X(2).                                  00001960
             03 MQCOM2A      PIC X.                                     00001970
             03 MQCOM2C PIC X.                                          00001980
             03 MQCOM2P PIC X.                                          00001990
             03 MQCOM2H PIC X.                                          00002000
             03 MQCOM2V PIC X.                                          00002010
             03 MQCOM2O      PIC X(5).                                  00002020
             03 FILLER       PIC X(2).                                  00002030
             03 MDCOM2A      PIC X.                                     00002040
             03 MDCOM2C PIC X.                                          00002050
             03 MDCOM2P PIC X.                                          00002060
             03 MDCOM2H PIC X.                                          00002070
             03 MDCOM2V PIC X.                                          00002080
             03 MDCOM2O      PIC X(6).                                  00002090
             03 FILLER       PIC X(2).                                  00002100
             03 MQCOM3A      PIC X.                                     00002110
             03 MQCOM3C PIC X.                                          00002120
             03 MQCOM3P PIC X.                                          00002130
             03 MQCOM3H PIC X.                                          00002140
             03 MQCOM3V PIC X.                                          00002150
             03 MQCOM3O      PIC X(5).                                  00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MDCOM3A      PIC X.                                     00002180
             03 MDCOM3C PIC X.                                          00002190
             03 MDCOM3P PIC X.                                          00002200
             03 MDCOM3H PIC X.                                          00002210
             03 MDCOM3V PIC X.                                          00002220
             03 MDCOM3O      PIC X(6).                                  00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MZONCMDA  PIC X.                                          00002250
           02 MZONCMDC  PIC X.                                          00002260
           02 MZONCMDP  PIC X.                                          00002270
           02 MZONCMDH  PIC X.                                          00002280
           02 MZONCMDV  PIC X.                                          00002290
           02 MZONCMDO  PIC X(15).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLIBERRA  PIC X.                                          00002320
           02 MLIBERRC  PIC X.                                          00002330
           02 MLIBERRP  PIC X.                                          00002340
           02 MLIBERRH  PIC X.                                          00002350
           02 MLIBERRV  PIC X.                                          00002360
           02 MLIBERRO  PIC X(54).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCODTRAA  PIC X.                                          00002390
           02 MCODTRAC  PIC X.                                          00002400
           02 MCODTRAP  PIC X.                                          00002410
           02 MCODTRAH  PIC X.                                          00002420
           02 MCODTRAV  PIC X.                                          00002430
           02 MCODTRAO  PIC X(4).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MCICSA    PIC X.                                          00002460
           02 MCICSC    PIC X.                                          00002470
           02 MCICSP    PIC X.                                          00002480
           02 MCICSH    PIC X.                                          00002490
           02 MCICSV    PIC X.                                          00002500
           02 MCICSO    PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNETNAMA  PIC X.                                          00002530
           02 MNETNAMC  PIC X.                                          00002540
           02 MNETNAMP  PIC X.                                          00002550
           02 MNETNAMH  PIC X.                                          00002560
           02 MNETNAMV  PIC X.                                          00002570
           02 MNETNAMO  PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MSCREENA  PIC X.                                          00002600
           02 MSCREENC  PIC X.                                          00002610
           02 MSCREENP  PIC X.                                          00002620
           02 MSCREENH  PIC X.                                          00002630
           02 MSCREENV  PIC X.                                          00002640
           02 MSCREENO  PIC X(4).                                       00002650
                                                                                
