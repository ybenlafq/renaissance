      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGR10 (TGR00 -> MENU)    TR: GR00  *    00020001
      *               GESTION DES RECEPTIONS                       *    00030001
      *                                                                 00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3596  00050001
      *                                                                 00060000
      *        TRANSACTION GR10 : INTERROGATION DES QUOTAS         *    00070001
      *                                                                 00080000
             03 COMM-GR10-APPLI REDEFINES COMM-GR01-APPLI.              00090001
      *------------------------------ ZONE DONNEES TGR10                00100001
                04 COMM-GR10-DONNEES-TGR10.                             00110001
      *------------------------------ TYPE DE RECEPTION                 00120001
                   05 COMM-GR10-CREC              PIC X(05).            00130001
                   05 COMM-GR10-LREC              PIC X(20).            00140001
      *------------------------------ TYPE DE QUOTA                     00150001
                   05 COMM-GR10-LQUOTA            PIC X(20).            00160001
                   05 COMM-GR10-CQUOTA            PIC X(05).            00170001
                   05 COMM-GR10-CQUOTA2           PIC X(05).            00180001
      *------------------------------ DATE DE LA SEMAINE                00190001
                   05 COMM-GR10-SEMAINE.                                00200001
                      06 COMM-GR10-DANNEE         PIC X(4).             00210001
                      06 COMM-GR10-DSEMAINE       PIC X(2).             00220001
                   05 COMM-GR10-SEMAINE2          PIC X(6).             00230001
      *------------------------------ TABLE CODE SEMAINE PR PAGINATION  00240001
                   05 COMM-GR10-TABSEM         OCCURS 100.              00250001
      *------------------------------ 1ERS NUMEROS SEMAINE DES PAGES    00260001
                      06 COMM-GR10-NSEM           PIC X(6).             00270001
      *------------------------------ INDICATEUR ETAT FICHIER           00280001
                   05 COMM-GR10-INDPAG            PIC 9.                00290001
      *------------------------------ DERNIERE DATE SEMAINE AFFICHEE    00300001
                   05 COMM-GR10-PAGSUI            PIC X(6).             00310001
      *------------------------------ ANCIENNE DATE SEMAINE AFFICHEE    00320001
                   05 COMM-GR10-PAGENC            PIC X(6).             00330001
      *------------------------------ NUMERO PAGE ECRAN                 00340001
                   05 COMM-GR10-NUMPAG            PIC 9(3).             00350001
      *------------------------------ ZONES SWAP                        00360001
                   05 COMM-GR10-SWAP              PIC 9 OCCURS 396.             
      *------------------------------ PARAM DEPOT                               
                   05 COMM-GR10-DEPOTPAL          PIC X(1).                     
      *------------------------------ PARAM QUOTA                               
                   05 COMM-GR10-QUOTAPAL          PIC X(1).                     
                                                                                
