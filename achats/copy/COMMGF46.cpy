      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGF46                    TR: GF46  *    00020001
      * GESTION DES LIGNES DE COMMANDES EN INSTANCE                *    00030001
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 6421  00050000
      *                                                                 00060000
          02 COMM-GF46-APPLI REDEFINES COMM-GF00-APPLI.                 00070001
      *------------------------------ CODE FONCTION                     00150000
             03 COMM-GF46-WFONC             PIC X(3).                   00160000
      *------------------------------ CODE ENTITE DE COMMANDE           00150000
             03 COMM-GF46-NENTCDE           PIC X(5).                   00160000
      *------------------------------ LIBELLE ENTITE DE COMMANDE        00170000
             03 COMM-GF46-LENTCDE           PIC X(20).                  00180000
      *------------------------------ CODE INTERLOCUTEUR COMMERCIAL     00190000
             03 COMM-GF46-CINTERLOCUT       PIC X(5).                   00200000
      *------------------------------ LIBELLE INTERLO. COMMERCIAL       00210000
             03 COMM-GF46-LINTERLOCUT       PIC X(20).                  00220000
      *------------------------------ CODIC A RECHERCHER                00230000
             03 COMM-GF46-NCODICREF         PIC X(7).                   00240001
      *------------------------------ CODIC                             00330000
             03 COMM-GF46-NCODIC-GF00       PIC X(7).                   00340001
             03 COMM-GF46-PROPRE.                                       00380001
                04 COMM-GF46-ZONE-PAGINATION.                           00470001
      *------------------------------ PAGINATION -------------- 13      00480001
                   05 COMM-GF46-NPAGE       PIC 9(5) COMP-3.            00490001
                   05 COMM-GF46-NITEM       PIC 9(5) COMP-3.            00500001
                   05 COMM-GF46-NOMBRE-PAGE PIC 9(5) COMP-3.            00510001
                   05 COMM-GF46-LCURSEUR    PIC 9(5) COMP-3.            00520001
                   05 COMM-GF46-EXIST-TS    PIC X(1).                   00530001
      *------------------------------ TABLEAU      --------1440         00560001
                04 COMM-GF46-TAB            OCCURS 13.                  00570001
                   05 COMM-GF46-CINTER      PIC X(5).                   00580001
                   05 COMM-GF46-NLIEU       PIC X(6).                   00590001
                   05 COMM-GF46-NENTR       PIC X(6).                   00600001
                   05 COMM-GF46-NCODIC      PIC X(7).                   00610001
                   05 COMM-GF46-CFAM        PIC X(5).                   00610001
                   05 COMM-GF46-LREF        PIC X(20).                  00610001
                   05 COMM-GF46-CMARQ       PIC X(5).                   00610001
                   05 COMM-GF46-QTE         PIC X(6).                   00610001
                   05 COMM-GF46-DATE        PIC X(8).                   00610001
      *------------------------------ CODIC GROUPE --------1440                 
                04 COMM-GF46-TAB-GROUPE     OCCURS 80.                          
                   05 COMM-GF46-NCODIC-GRP  PIC X(7).                           
                   05 COMM-GF46-NCODIC-LIE  PIC X(7).                           
                   05 COMM-GF46-NPAGE-GRP   PIC 9(2).                           
                   05 COMM-GF46-NLIGNE-GRP  PIC 9(2).                           
      *------------------------------ INDICE GROUPE --------2                   
                04 COMM-GF46-IND-GROUPE     PIC S9(3) COMP-3.                   
      *------------------------------ CODIC CARTON ------- 1520                 
                04 COMM-GF46-TAB-CARTON     OCCURS 80.                          
                   05 COMM-GF46-NCODIC-CAR  PIC X(7).                           
                   05 COMM-GF46-NCODIC-ELT  PIC X(7).                           
                   05 COMM-GF46-NPAGE-CAR   PIC 9(2).                           
                   05 COMM-GF46-NLIGNE-CAR  PIC 9(2).                           
                   05 COMM-GF46-CONTROLE    PIC X(1).                           
      *------------------------------ INDICE CARTON --------2                   
                04 COMM-GF46-IND-CARTON     PIC S9(3) COMP-3.                   
      *------------------------------ SAUVEGARDE DES ATTRIBUTS ---200   00730001
                04 COMM-GF46-SWAP-ATTR OCCURS 200 PIC X(1).             00740001
      *--------------------NOUVELLES DONNEES PIC X(14).                         
                04  COMM-GF46-NOUVELLES-DONNEES.                                
                   05 COMM-GF46-NSOCLIVR-MAP      PIC X(3).                     
                   05 COMM-GF46-NDEPOT-MAP        PIC X(3).                     
                   05 COMM-GF46-DSOUHAITEE        PIC X(8).                     
      *------------------------------ STATUS='RECH', 'MAJ ', 'CONF'     00750001
C019  *      03 COMM-GF46-STATUS                  PIC X(4).             00760002
      *------------------------------ FILLER                            00750001
C019         03 COMM-GF46-ERR-MEMO                PIC X(1).             00760002
C019            88 COMM-GF46-MEMO-OK   VALUE '0'.                               
C019            88 COMM-GF46-MEMO-KO   VALUE '1'.                               
C019         03 COMM-GF46-NCODIC-MEMO             PIC X(7).             00760002
C019         03 COMM-GF46-TOUCHE-MEMO             PIC X(2).             00760002
C019  *                      ENTER-MEMO VALUE  0.                               
C019  *                      PF3-MEMO   VALUE  3.                               
C019  *                      PF4-MEMO   VALUE  4.                               
C019  *                      PF5-MEMO   VALUE  5.                               
C019  *                      PF12-MEMO  VALUE  12.                              
E0105**      03 COMM-GF46-FILLER                  PIC X(2269).          00760002
E0105**      03 COMM-GF46-FILLER                  PIC X(1969).          00760002
      ***************************************************************** 00770000
                                                                                
