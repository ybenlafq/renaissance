      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : TS LIGNES DE COMMANDE PAR DEMANDEUR CODIC              *         
      *        CREE PAR TGF20                                         *         
      *****************************************************************         
      *                                                               *         
       01  TS-GF20.                                                             
           02 TS-GF20-GESTION.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TS-GF20-POS                 PIC S9(4) COMP.                   
      *--                                                                       
               05 TS-GF20-POS                 PIC S9(4) COMP-5.                 
      *}                                                                        
      *--------------------------------  STRUCTURE TS                           
           02 TS-GF20-DONNEES.                                                  
             03 TS-GF20-ENR.                                                    
               05 TS-GF20-NCODIC              PIC X(7).                         
               05 TS-GF20-NENTCDE             PIC X(7).                         
               05 TS-GF20-NSOCIETE            PIC X(3).                         
               05 TS-GF20-NSOCLIVR            PIC X(3).                         
               05 TS-GF20-NDEPOT              PIC X(3).                         
               05 TS-GF20-CHEFPROD            PIC X(5).                         
               05 TS-GF20-CMODSTOCK           PIC X(5).                         
               05 TS-GF20-COMMANDE       OCCURS 09.                             
                  10 TS-GF20-QTE                 PIC 9(5) COMP-3.               
                  10 TS-GF20-DATE                PIC X(6).                      
                  10 TS-GF20-ANNEE               PIC X(4).                      
                  10 TS-GF20-DSEM                PIC X(2).                      
                  10 TS-GF20-JSEM                PIC 9(1).                      
             03 TS-GF20-MAP.                                                    
                05 TS-GF20-LREFFOURN             PIC X(20).                     
                05 TS-GF20-TOPMAJ                PIC X.                         
                                                                                
