      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : LIGNES DE COMMANDES VUE RVGF0500 POUR PAGINATION       *         
      *        PROGRAMME TGF12                                        *         
      *****************************************************************         
      *                                                               *         
       01  TS-GF10.                                                             
      *--------------------------------  LONGUEUR TS                            
           02 TS-GF10-LONG                      PIC S9(5) COMP-3                
      *                                         VALUE +1797.                    
                                                VALUE +1872.                    
           02 TS-GF10-DONNEES.                                                  
             03 TS-TAB-DONNEES                  OCCURS 13.                      
               04 TS-GF10-GF05.                                                 
      *--------------------------------  TABLE LIGNE COMMANDE                   
                 05 TS-GF10-NCODIC              PIC X(7).                       
      *          05 TS-GF10-QSOUHAITEE          PIC 9(5) COMP-3.                
                 05 TS-GF10-QSOUHAITEE          PIC S9(5) COMP-3.               
                 05 TS-GF10-DSOUHAITEE          PIC X(8).                       
                 05 TS-GF10-CINTERLOCUT         PIC X(5).                       
                 05 TS-GF10-LINTERLOCUT         PIC X(20).                      
                 05 TS-GF10-NSOCIETE            PIC X(3).                       
                 05 TS-GF10-NLIEU               PIC X(3).                       
                 05 TS-GF10-NSOCDEP.                                            
                    06 TS-GF10-NSOCLIVR         PIC X(3).                       
                    06 TS-GF10-NDEPOT           PIC X(3).                       
                 05 TS-GF10-CSTATUT             PIC X(1).                       
                 05 TS-GF10-CHEFPROD            PIC X(5).                       
                 05 TS-GF10-NPAGE               PIC X(3).                       
                 05 TS-GF10-NLIGNE              PIC X(2).                       
                 05 TS-GF10-DETAT               PIC X(8).                       
                 05 TS-GF10-NSEQ                PIC X(7).                       
                 05 TS-GF10-QUOTA               PIC 9(5).                       
      *--------------------------------  TABLE ARTICLE                          
               04 TS-GF10-GA00.                                                 
                 05 TS-GF10-CFAM                PIC X(5).                       
                 05 TS-GF10-CMARQ               PIC X(5).                       
                 05 TS-GF10-LREFFOURN           PIC X(20).                      
                 05 TS-GF10-CMODSTOCK           PIC X(5).                       
                 05 TS-GF10-QCOLIRECEPT         PIC 9(5) COMP-3.                
                 05 TS-GF10-CQUOTA              PIC X(5).                       
                 05 TS-GF10-FLAG-GRPE           PIC X(1).                       
               04  TS-W-DONNEES.                                                
      *--------------------------------  DONNEES PROPRES TRANSACTION            
                 05 TS-W-TOP-PROT               PIC X(1).                       
                 05 TS-W-STATUT-INITIAL         PIC X(1).                       
                 05 TS-W-DATE-INITIAL           PIC X(8).                       
                 05 TS-W-TOP-MAJ                PIC X(1).                       
                 05 TS-W-TOP-SUP                PIC X(1).                       
                 05 TS-W-TOP-GRP                PIC X(1).                       
                 05 TS-W-TOP-MAJ-STATUT         PIC X(1).                       
      *--------------------------------  ZONE DE SAUVEGARDE DE LA TS            
      *01  W-SAVE-TSGF10                        PIC X(1797).                    
       01  W-SAVE-TSGF10                        PIC X(1872).                    
                                                                                
