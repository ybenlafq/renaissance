      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      * ASSOCIATION CODE DESCRIPTIF -> FEATURES LONG 22                         
          02 COMM-AI27-APPLI REDEFINES COMM-GA00-APPLI.                         
              05 COMM-AI27-GESTION.                                             
                  10 COMM-AI27-WFONC PIC X(3).                                  
                  10 COMM-AI27-ZONCMD PIC X(15).                                
              05 COMM-AI27-DATA.                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           10 COMM-AI27-PAGE PIC S9(3) COMP.                             
      *--                                                                       
                  10 COMM-AI27-PAGE PIC S9(3) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           10 COMM-AI27-PAGEMAX PIC S9(3) COMP.                          
      *--                                                                       
                  10 COMM-AI27-PAGEMAX PIC S9(3) COMP-5.                        
      *}                                                                        
                  10 COMM-AI27-CDESCRIPTIF PIC X(5).                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           10 COMM-AI27-CDIND PIC S9(3) COMP.                            
      *--                                                                       
                  10 COMM-AI27-CDIND PIC S9(3) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           10 COMM-AI27-CDENR PIC S9(3) COMP.                            
      *--                                                                       
                  10 COMM-AI27-CDENR PIC S9(3) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           10 COMM-AI27-FTIND PIC S9(3) COMP.                            
      *--                                                                       
                  10 COMM-AI27-FTIND PIC S9(3) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           10 COMM-AI27-FTDEB PIC S9(3) COMP.                            
      *--                                                                       
                  10 COMM-AI27-FTDEB PIC S9(3) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           10 COMM-AI27-FTFIN PIC S9(3) COMP.                            
      *--                                                                       
                  10 COMM-AI27-FTFIN PIC S9(3) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           10 COMM-AI27-FTENR PIC S9(3) COMP.                            
      *--                                                                       
                  10 COMM-AI27-FTENR PIC S9(3) COMP-5.                          
      *}                                                                        
                  10 COMM-AI27-MAJ PIC 9.                                       
                      88 COMM-AI27-INITIAL VALUE 0.                             
                      88 COMM-AI27-CHANGE VALUE 1.                              
                                                                                
