      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGF8700                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF8700                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF8700.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF8700.                                                            
      *}                                                                        
           10 GF87-NENTCDE              PIC X(5).                               
           10 GF87-NSOCDEPOT            PIC X(3).                               
           10 GF87-NDEPOT               PIC X(3).                               
           10 GF87-DENVOICDE            PIC X(8).                               
           10 GF87-DDESTOCKFOUR         PIC X(8).                               
           10 GF87-DLIVFOUR             PIC X(8).                               
           10 GF87-DDISPO               PIC X(8).                               
           10 GF87-DSYST                PIC S9(13)V USAGE COMP-3.               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGF8700                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF8700-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF8700-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF87-NENTCDE-F            PIC S9(4) COMP.                         
      *--                                                                       
           10 GF87-NENTCDE-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF87-NSOCDEPOT-F          PIC S9(4) COMP.                         
      *--                                                                       
           10 GF87-NSOCDEPOT-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF87-NDEPOT-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF87-NDEPOT-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF87-DENVOICDE-F          PIC S9(4) COMP.                         
      *--                                                                       
           10 GF87-DENVOICDE-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF87-DDESTOCKFOUR-F       PIC S9(4) COMP.                         
      *--                                                                       
           10 GF87-DDESTOCKFOUR-F       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF87-DLIVFOUR-F           PIC S9(4) COMP.                         
      *--                                                                       
           10 GF87-DLIVFOUR-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF87-DDISPO-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF87-DDISPO-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF87-DSYST-F              PIC S9(4) COMP.                         
      *                                                                         
      *--                                                                       
           10 GF87-DSYST-F              PIC S9(4) COMP-5.                       
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
