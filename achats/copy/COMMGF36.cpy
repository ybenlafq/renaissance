      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010004
      * COMMAREA SPECIFIQUE PRG  TGF36                   TR: GF36  *    00020004
      *           SUIVI DES COMMANDES FOURNISSEURS                 *    00030004
      **************************************************************    00040004
      *                                                                 00060004
          03 COMM-GF36 REDEFINES COMM-GF30-FILLER.                      00070004
             05 COMM-GF36-CHAMPS.                                       00080004
                10 COMM-GF36-NENTCDE          PIC X(05).                00090004
                10 COMM-GF36-LENTCDE          PIC X(20).                00100004
                10 COMM-GF36-CHEFPROD         PIC X(05).                00120004
                10 COMM-GF36-LCHEFPROD        PIC X(20).                00130004
                10 COMM-GF36-TAB-LIG.                                   00140004
                   15 COMM-GF36-LIGPAG        OCCURS 10.                00150004
                      20 COMM-GF36-NCDE       PIC X(07).                00151006
                      20 COMM-GF36-NLIEU      PIC X(03).                00160004
                      20 COMM-GF36-NCODIC     PIC X(07).                00170004
                      20 COMM-GF36-LREFFOURN  PIC X(20).                00180004
                      20 COMM-GF36-QCDEORIG   PIC X(05).                00190004
                      20 COMM-GF36-QCDE       PIC X(05).                00200004
                      20 COMM-GF36-DLIVRAISON PIC X(08).                00210004
                      20 COMM-GF36-WCLT       PIC X(01).                00220004
             05 COMM-GF36-APPLI.                                        00230004
                10 COMM-GF36-PAGE             PIC 9(03).                00240004
                10 COMM-GF36-PAGEMAX          PIC 9(03).                00250004
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         10 COMM-GF36-IND-TS-MAX       PIC S9(04) COMP.          00260004
      *--                                                                       
                10 COMM-GF36-IND-TS-MAX       PIC S9(04) COMP-5.                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         10 COMM-GF36-IND-RECH-TS      PIC S9(04) COMP.          00270004
      *--                                                                       
                10 COMM-GF36-IND-RECH-TS      PIC S9(04) COMP-5.                
      *}                                                                        
                10 COMM-GF36-MODIF-FLAG       PIC X(01).                00280004
                   88 COMM-GF36-MODIF             VALUE 'O'.            00290004
                   88 COMM-GF36-NO-MODIF          VALUE 'N'.            00300004
                10 COMM-GF36-AFFICHAGE-NON    PIC X(01).                00310004
                   88 PAS-AFFICHAGE-NON           VALUE 'N'.            00320004
                   88 AFFICHAGE-NON               VALUE 'O'.            00330004
E0240.       05 COMM-GF36-ORIGDATE            PIC X(08).                00331009
      *      05 FILLER                        PIC X(148).               00340009
.E0240       05 FILLER                        PIC X(140).               00341009
      *                                                                 00350004
                                                                                
