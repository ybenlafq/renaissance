      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : TS LIGNES DE COMMANDE PAR DEMANDEUR CODIC              *         
      *        CREE PAR TGF15                                         *         
      *****************************************************************         
      *                                                               *         
       01  TS-GF15.                                                             
      *--------------------------------  LONGUEUR TS                            
           02 TS-GF15-LONG                    PIC S9(5) COMP-3                  
                                              VALUE +162.                       
           02 TS-GF15-DONNEES.                                                  
             03 TS-GF15-GF05.                                                   
      *--------------------------------  TABLE LIGNE COMMANDE                   
      *--------------------------------  DEMANDEUR                              
               04 TS-GF15-NSOCIETE            PIC X(3).                         
               04 TS-GF15-NLIEU               PIC X(3).                         
      *--------------------------------  CODIC                                  
               04 TS-GF15-NCODIC              PIC X(7).                         
      *--------------------------------  QTE GLOBALE PAR                        
      *                                  DEMANDEUR CODIC                        
               04 TS-GF15-QTETOT              PIC 9(5) COMP-3.                  
      *--------------------------------  QTE PAR DEMANDEUR CODIC                
      *                                  ET PAR DATE                            
               04 TS-GF15-TAB-QTE             OCCURS 4.                         
                 05 TS-GF15-QTE               PIC 9(5) COMP-3.                  
                 05 TS-GF15-DATE              PIC X(8).                         
             03 TS-GF15-GA00.                                                   
      *--------------------------------  TABLE ARTICLE                          
               04 TS-GF15-CFAM                PIC X(5).                         
               04 TS-GF15-CMARQ               PIC X(5).                         
               04 TS-GF15-LREFFOURN           PIC X(20).                        
               04 TS-GF15-LCOMMENT            PIC X(50).                        
               04 TS-GF15-CMODSTOCK           PIC X(5).                         
               04 TS-GF15-CTYPCONDT           PIC X(5).                         
               04 TS-GF15-QCOLIRECEPT         PIC 9(5) COMP-3.                  
             03 TS-GF15-TOPMAJ                PIC X.                            
                                                                                
