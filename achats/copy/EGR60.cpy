      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR60   EGR60                                              00000020
      ***************************************************************** 00000030
       01   EGR60I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * PAGE                                                            00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MPAGEI    PIC X(3).                                       00000200
      * CODIC                                                           00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICI  PIC X(7).                                       00000250
      * LIBELLE CODIC                                                   00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODICL  COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MLCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCODICF  PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MLCODICI  PIC X(20).                                      00000300
      * DATE DEBUT                                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEBUTL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MDDEBUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDEBUTF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MDDEBUTI  PIC X(8).                                       00000350
      * FAMILLE                                                         00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFAML    COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MNFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNFAMF    PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MNFAMI    PIC X(5).                                       00000400
      * LIBELLE FAMILLE                                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLFAMI    PIC X(20).                                      00000450
      * DATE FIN                                                        00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINL    COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MDFINL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDFINF    PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MDFINI    PIC X(8).                                       00000500
      * MARQUE                                                          00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMARQUL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MNMARQUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNMARQUF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MNMARQUI  PIC X(5).                                       00000550
      * LIBELLE MARQUE                                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQUL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MLMARQUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMARQUF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MLMARQUI  PIC X(20).                                      00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB1L    COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MLIB1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB1F    PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MLIB1I    PIC X(9).                                       00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB2L    COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MLIB2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB2F    PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MLIB2I    PIC X(6).                                       00000680
           02 M61I OCCURS   11 TIMES .                                  00000690
      * DATE RECEPTION                                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDRECEPL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MDRECEPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDRECEPF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MDRECEPI     PIC X(8).                                  00000740
      * NO RECEP QUAI                                                   00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNRECQUL     COMP PIC S9(4).                            00000760
      *--                                                                       
             03 MNRECQUL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNRECQUF     PIC X.                                     00000770
             03 FILLER  PIC X(4).                                       00000780
             03 MNRECQUI     PIC X(7).                                  00000790
      * DATE SAISIE                                                     00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDSAIREL     COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MDSAIREL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDSAIREF     PIC X.                                     00000820
             03 FILLER  PIC X(4).                                       00000830
             03 MDSAIREI     PIC X(8).                                  00000840
      * NO RECEPTION                                                    00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNRECEPL     COMP PIC S9(4).                            00000860
      *--                                                                       
             03 MNRECEPL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNRECEPF     PIC X.                                     00000870
             03 FILLER  PIC X(4).                                       00000880
             03 MNRECEPI     PIC X(7).                                  00000890
      * NUM COMMANDE                                                    00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCOMMAL     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MNCOMMAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCOMMAF     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MNCOMMAI     PIC X(7).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBLL   COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MNBLL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNBLF   PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MNBLI   PIC X(10).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTERECL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MQTERECL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTERECF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MQTERECI     PIC X(5).                                  00001020
      * ZONE CMD AIDA                                                   00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MZONCMDI  PIC X(15).                                      00001070
      * MESSAGE ERREUR                                                  00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001090
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001100
           02 FILLER    PIC X(4).                                       00001110
           02 MLIBERRI  PIC X(58).                                      00001120
      * CODE TRANSACTION                                                00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MCODTRAI  PIC X(4).                                       00001170
      * CICS DE TRAVAIL                                                 00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCICSI    PIC X(5).                                       00001220
      * NETNAME                                                         00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001240
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001250
           02 FILLER    PIC X(4).                                       00001260
           02 MNETNAMI  PIC X(8).                                       00001270
      * CODE TERMINAL                                                   00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001290
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001300
           02 FILLER    PIC X(4).                                       00001310
           02 MSCREENI  PIC X(5).                                       00001320
      ***************************************************************** 00001330
      * SDF: EGR60   EGR60                                              00001340
      ***************************************************************** 00001350
       01   EGR60O REDEFINES EGR60I.                                    00001360
           02 FILLER    PIC X(12).                                      00001370
      * DATE DU JOUR                                                    00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MDATJOUA  PIC X.                                          00001400
           02 MDATJOUC  PIC X.                                          00001410
           02 MDATJOUP  PIC X.                                          00001420
           02 MDATJOUH  PIC X.                                          00001430
           02 MDATJOUV  PIC X.                                          00001440
           02 MDATJOUO  PIC X(10).                                      00001450
      * HEURE                                                           00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MTIMJOUA  PIC X.                                          00001480
           02 MTIMJOUC  PIC X.                                          00001490
           02 MTIMJOUP  PIC X.                                          00001500
           02 MTIMJOUH  PIC X.                                          00001510
           02 MTIMJOUV  PIC X.                                          00001520
           02 MTIMJOUO  PIC X(5).                                       00001530
      * PAGE                                                            00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MPAGEA    PIC X.                                          00001560
           02 MPAGEC    PIC X.                                          00001570
           02 MPAGEP    PIC X.                                          00001580
           02 MPAGEH    PIC X.                                          00001590
           02 MPAGEV    PIC X.                                          00001600
           02 MPAGEO    PIC X(3).                                       00001610
      * CODIC                                                           00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MNCODICA  PIC X.                                          00001640
           02 MNCODICC  PIC X.                                          00001650
           02 MNCODICP  PIC X.                                          00001660
           02 MNCODICH  PIC X.                                          00001670
           02 MNCODICV  PIC X.                                          00001680
           02 MNCODICO  PIC X(7).                                       00001690
      * LIBELLE CODIC                                                   00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MLCODICA  PIC X.                                          00001720
           02 MLCODICC  PIC X.                                          00001730
           02 MLCODICP  PIC X.                                          00001740
           02 MLCODICH  PIC X.                                          00001750
           02 MLCODICV  PIC X.                                          00001760
           02 MLCODICO  PIC X(20).                                      00001770
      * DATE DEBUT                                                      00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MDDEBUTA  PIC X.                                          00001800
           02 MDDEBUTC  PIC X.                                          00001810
           02 MDDEBUTP  PIC X.                                          00001820
           02 MDDEBUTH  PIC X.                                          00001830
           02 MDDEBUTV  PIC X.                                          00001840
           02 MDDEBUTO  PIC X(8).                                       00001850
      * FAMILLE                                                         00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MNFAMA    PIC X.                                          00001880
           02 MNFAMC    PIC X.                                          00001890
           02 MNFAMP    PIC X.                                          00001900
           02 MNFAMH    PIC X.                                          00001910
           02 MNFAMV    PIC X.                                          00001920
           02 MNFAMO    PIC X(5).                                       00001930
      * LIBELLE FAMILLE                                                 00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MLFAMA    PIC X.                                          00001960
           02 MLFAMC    PIC X.                                          00001970
           02 MLFAMP    PIC X.                                          00001980
           02 MLFAMH    PIC X.                                          00001990
           02 MLFAMV    PIC X.                                          00002000
           02 MLFAMO    PIC X(20).                                      00002010
      * DATE FIN                                                        00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MDFINA    PIC X.                                          00002040
           02 MDFINC    PIC X.                                          00002050
           02 MDFINP    PIC X.                                          00002060
           02 MDFINH    PIC X.                                          00002070
           02 MDFINV    PIC X.                                          00002080
           02 MDFINO    PIC X(8).                                       00002090
      * MARQUE                                                          00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MNMARQUA  PIC X.                                          00002120
           02 MNMARQUC  PIC X.                                          00002130
           02 MNMARQUP  PIC X.                                          00002140
           02 MNMARQUH  PIC X.                                          00002150
           02 MNMARQUV  PIC X.                                          00002160
           02 MNMARQUO  PIC X(5).                                       00002170
      * LIBELLE MARQUE                                                  00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MLMARQUA  PIC X.                                          00002200
           02 MLMARQUC  PIC X.                                          00002210
           02 MLMARQUP  PIC X.                                          00002220
           02 MLMARQUH  PIC X.                                          00002230
           02 MLMARQUV  PIC X.                                          00002240
           02 MLMARQUO  PIC X(20).                                      00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MLIB1A    PIC X.                                          00002270
           02 MLIB1C    PIC X.                                          00002280
           02 MLIB1P    PIC X.                                          00002290
           02 MLIB1H    PIC X.                                          00002300
           02 MLIB1V    PIC X.                                          00002310
           02 MLIB1O    PIC X(9).                                       00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MLIB2A    PIC X.                                          00002340
           02 MLIB2C    PIC X.                                          00002350
           02 MLIB2P    PIC X.                                          00002360
           02 MLIB2H    PIC X.                                          00002370
           02 MLIB2V    PIC X.                                          00002380
           02 MLIB2O    PIC X(6).                                       00002390
           02 M61O OCCURS   11 TIMES .                                  00002400
      * DATE RECEPTION                                                  00002410
             03 FILLER       PIC X(2).                                  00002420
             03 MDRECEPA     PIC X.                                     00002430
             03 MDRECEPC     PIC X.                                     00002440
             03 MDRECEPP     PIC X.                                     00002450
             03 MDRECEPH     PIC X.                                     00002460
             03 MDRECEPV     PIC X.                                     00002470
             03 MDRECEPO     PIC X(8).                                  00002480
      * NO RECEP QUAI                                                   00002490
             03 FILLER       PIC X(2).                                  00002500
             03 MNRECQUA     PIC X.                                     00002510
             03 MNRECQUC     PIC X.                                     00002520
             03 MNRECQUP     PIC X.                                     00002530
             03 MNRECQUH     PIC X.                                     00002540
             03 MNRECQUV     PIC X.                                     00002550
             03 MNRECQUO     PIC X(7).                                  00002560
      * DATE SAISIE                                                     00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MDSAIREA     PIC X.                                     00002590
             03 MDSAIREC     PIC X.                                     00002600
             03 MDSAIREP     PIC X.                                     00002610
             03 MDSAIREH     PIC X.                                     00002620
             03 MDSAIREV     PIC X.                                     00002630
             03 MDSAIREO     PIC X(8).                                  00002640
      * NO RECEPTION                                                    00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MNRECEPA     PIC X.                                     00002670
             03 MNRECEPC     PIC X.                                     00002680
             03 MNRECEPP     PIC X.                                     00002690
             03 MNRECEPH     PIC X.                                     00002700
             03 MNRECEPV     PIC X.                                     00002710
             03 MNRECEPO     PIC X(7).                                  00002720
      * NUM COMMANDE                                                    00002730
             03 FILLER       PIC X(2).                                  00002740
             03 MNCOMMAA     PIC X.                                     00002750
             03 MNCOMMAC     PIC X.                                     00002760
             03 MNCOMMAP     PIC X.                                     00002770
             03 MNCOMMAH     PIC X.                                     00002780
             03 MNCOMMAV     PIC X.                                     00002790
             03 MNCOMMAO     PIC X(7).                                  00002800
             03 FILLER       PIC X(2).                                  00002810
             03 MNBLA   PIC X.                                          00002820
             03 MNBLC   PIC X.                                          00002830
             03 MNBLP   PIC X.                                          00002840
             03 MNBLH   PIC X.                                          00002850
             03 MNBLV   PIC X.                                          00002860
             03 MNBLO   PIC X(10).                                      00002870
             03 FILLER       PIC X(2).                                  00002880
             03 MQTERECA     PIC X.                                     00002890
             03 MQTERECC     PIC X.                                     00002900
             03 MQTERECP     PIC X.                                     00002910
             03 MQTERECH     PIC X.                                     00002920
             03 MQTERECV     PIC X.                                     00002930
             03 MQTERECO     PIC X(5).                                  00002940
      * ZONE CMD AIDA                                                   00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MZONCMDA  PIC X.                                          00002970
           02 MZONCMDC  PIC X.                                          00002980
           02 MZONCMDP  PIC X.                                          00002990
           02 MZONCMDH  PIC X.                                          00003000
           02 MZONCMDV  PIC X.                                          00003010
           02 MZONCMDO  PIC X(15).                                      00003020
      * MESSAGE ERREUR                                                  00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MLIBERRA  PIC X.                                          00003050
           02 MLIBERRC  PIC X.                                          00003060
           02 MLIBERRP  PIC X.                                          00003070
           02 MLIBERRH  PIC X.                                          00003080
           02 MLIBERRV  PIC X.                                          00003090
           02 MLIBERRO  PIC X(58).                                      00003100
      * CODE TRANSACTION                                                00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MCODTRAA  PIC X.                                          00003130
           02 MCODTRAC  PIC X.                                          00003140
           02 MCODTRAP  PIC X.                                          00003150
           02 MCODTRAH  PIC X.                                          00003160
           02 MCODTRAV  PIC X.                                          00003170
           02 MCODTRAO  PIC X(4).                                       00003180
      * CICS DE TRAVAIL                                                 00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MCICSA    PIC X.                                          00003210
           02 MCICSC    PIC X.                                          00003220
           02 MCICSP    PIC X.                                          00003230
           02 MCICSH    PIC X.                                          00003240
           02 MCICSV    PIC X.                                          00003250
           02 MCICSO    PIC X(5).                                       00003260
      * NETNAME                                                         00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MNETNAMA  PIC X.                                          00003290
           02 MNETNAMC  PIC X.                                          00003300
           02 MNETNAMP  PIC X.                                          00003310
           02 MNETNAMH  PIC X.                                          00003320
           02 MNETNAMV  PIC X.                                          00003330
           02 MNETNAMO  PIC X(8).                                       00003340
      * CODE TERMINAL                                                   00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MSCREENA  PIC X.                                          00003370
           02 MSCREENC  PIC X.                                          00003380
           02 MSCREENP  PIC X.                                          00003390
           02 MSCREENH  PIC X.                                          00003400
           02 MSCREENV  PIC X.                                          00003410
           02 MSCREENO  PIC X(5).                                       00003420
                                                                                
