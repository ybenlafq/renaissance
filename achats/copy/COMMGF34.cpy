      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************    00010005
      * COMMAREA SPECIFIQUE PRG  TGF34                   TR: GF34  *    00020005
      *           SUIVI DES COMMANDES FOURNISSEURS                 *    00030005
      **************************************************************    00040005
      * ZONES RESERVEES APPLICATIVES ------------------------- 7354 MAX         
      *                                                                 00060005
          03 COMM-GF34-APPLI REDEFINES COMM-GF30-FILLER.                00070005
      *------------------------------ LIBELLE FAMILLE & MARQUE          00200005
              04  COMM-GF34-LFAM               PIC  X(20).              00210006
              04  COMM-GF34-LMARQ              PIC  X(20).              00220006
      *------------------------------ INDICES GESTION PAGINATION        00230005
              04  COMM-GF34-WPAGE              PIC  9(03).              00240002
              04  COMM-GF34-GPAGE              PIC  9(03).              00250002
              04  COMM-GF34-LPAGE-MAX          PIC  9(03).              00260002
              04  COMM-GF34-LPAGE              PIC  9(03).              00270002
      *------------------------------ TABLE GESTION PAGINATION          00280005
              04  COMM-GF34-TABLE-PAGINATION.                           00290002
                  05  POSTE-PAGINATION  OCCURS  100.                    00300002
                      07  COMM-GF34-COMMANDE                            00310002
                                               PIC  X(07).              00320002
                      07  COMM-GF34-DATE-SAISIE                         00330002
                                               PIC  X(08).              00340002
      *--- LIBELLES + ATTRIBUTS                                                 
              04 COMM-GF34-LIGNES.                                              
                 05 COMM-GF34-LIGNE1           PIC X(3).                        
                 05 COMM-GF34-LIGNE2           PIC X(3).                        
      *------------------------------ TABLE IMAGE ECRAN                 00350005
              04  COMM-GF34-STRUCTURE.                                  00360002
                  05  POSTE-LIGNE  OCCURS  12.                          00370002
                      07  COMM-GF34-STATUT     PIC  X(03).                      
                      07  COMM-GF34-NSURCDE    PIC  X(07).                      
                      07  COMM-GF34-NCDE       PIC  X(07).              00380002
                      07  COMM-GF34-DSAISIE    PIC  X(08).              00390002
                      07  COMM-GF34-DVALIDITE  PIC  X(08).              00400002
NT                    07  COMM-GF34-NDEPOT     PIC  X(03).              00430008
                      07  COMM-GF34-QCDE       PIC  X(05).              00410007
                      07  COMM-GF34-QREC       PIC  X(05).              00420007
                      07  COMM-GF34-QSOLDE     PIC  X(05).              00430007
                      07  COMM-GF34-WGENGRO    PIC  X(03).              00430008
                      07  COMM-GF34-CCOLOR     PIC  X(01).                      
                      07  COMM-GF34-CBASE      PIC  X(01).                      
      *                                                                 00460002
           EJECT                                                        00470002
      *                                                                 00480002
                                                                                
