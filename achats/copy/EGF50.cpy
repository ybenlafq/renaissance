      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: TGF50   TGF50                                              00000020
      ***************************************************************** 00000030
       01   EGF50I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCDEI    PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCODICI  PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLREFFOURNI    PIC X(20).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTREPOTL    COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLENTREPOTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLENTREPOTF    PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLENTREPOTI    PIC X(22).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTREPOTL    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNENTREPOTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNENTREPOTF    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNENTREPOTI    PIC X(6).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTCDEL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTCDEF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNENTCDEI      PIC X(5).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTCDEL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MLENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTCDEF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLENTCDEI      PIC X(20).                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCINTERL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCINTERF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCINTERI  PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQCDEL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MQCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQCDEF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MQCDEI    PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQRECL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MQRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQRECF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MQRECI    PIC X(5).                                       00000610
           02 MLIGNEI OCCURS   12 TIMES .                               00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCOMMENTL   COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MDCOMMENTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MDCOMMENTF   PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MDCOMMENTI   PIC X(8).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMMENTL   COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MLCOMMENTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLCOMMENTF   PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLCOMMENTI   PIC X(62).                                 00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MZONCMDI  PIC X(15).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLIBERRI  PIC X(58).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNETNAMI  PIC X(8).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MSCREENI  PIC X(4).                                       00000940
      ***************************************************************** 00000950
      * SDF: TGF50   TGF50                                              00000960
      ***************************************************************** 00000970
       01   EGF50O REDEFINES EGF50I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MPAGEA    PIC X.                                          00001150
           02 MPAGEC    PIC X.                                          00001160
           02 MPAGEP    PIC X.                                          00001170
           02 MPAGEH    PIC X.                                          00001180
           02 MPAGEV    PIC X.                                          00001190
           02 MPAGEO    PIC X(3).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MCFONCA   PIC X.                                          00001220
           02 MCFONCC   PIC X.                                          00001230
           02 MCFONCP   PIC X.                                          00001240
           02 MCFONCH   PIC X.                                          00001250
           02 MCFONCV   PIC X.                                          00001260
           02 MCFONCO   PIC X(3).                                       00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MNCDEA    PIC X.                                          00001290
           02 MNCDEC    PIC X.                                          00001300
           02 MNCDEP    PIC X.                                          00001310
           02 MNCDEH    PIC X.                                          00001320
           02 MNCDEV    PIC X.                                          00001330
           02 MNCDEO    PIC X(7).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MNCODICA  PIC X.                                          00001360
           02 MNCODICC  PIC X.                                          00001370
           02 MNCODICP  PIC X.                                          00001380
           02 MNCODICH  PIC X.                                          00001390
           02 MNCODICV  PIC X.                                          00001400
           02 MNCODICO  PIC X(7).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MLREFFOURNA    PIC X.                                     00001430
           02 MLREFFOURNC    PIC X.                                     00001440
           02 MLREFFOURNP    PIC X.                                     00001450
           02 MLREFFOURNH    PIC X.                                     00001460
           02 MLREFFOURNV    PIC X.                                     00001470
           02 MLREFFOURNO    PIC X(20).                                 00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MLENTREPOTA    PIC X.                                     00001500
           02 MLENTREPOTC    PIC X.                                     00001510
           02 MLENTREPOTP    PIC X.                                     00001520
           02 MLENTREPOTH    PIC X.                                     00001530
           02 MLENTREPOTV    PIC X.                                     00001540
           02 MLENTREPOTO    PIC X(22).                                 00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNENTREPOTA    PIC X.                                     00001570
           02 MNENTREPOTC    PIC X.                                     00001580
           02 MNENTREPOTP    PIC X.                                     00001590
           02 MNENTREPOTH    PIC X.                                     00001600
           02 MNENTREPOTV    PIC X.                                     00001610
           02 MNENTREPOTO    PIC X(6).                                  00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MNENTCDEA      PIC X.                                     00001640
           02 MNENTCDEC PIC X.                                          00001650
           02 MNENTCDEP PIC X.                                          00001660
           02 MNENTCDEH PIC X.                                          00001670
           02 MNENTCDEV PIC X.                                          00001680
           02 MNENTCDEO      PIC X(5).                                  00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MLENTCDEA      PIC X.                                     00001710
           02 MLENTCDEC PIC X.                                          00001720
           02 MLENTCDEP PIC X.                                          00001730
           02 MLENTCDEH PIC X.                                          00001740
           02 MLENTCDEV PIC X.                                          00001750
           02 MLENTCDEO      PIC X(20).                                 00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MCINTERA  PIC X.                                          00001780
           02 MCINTERC  PIC X.                                          00001790
           02 MCINTERP  PIC X.                                          00001800
           02 MCINTERH  PIC X.                                          00001810
           02 MCINTERV  PIC X.                                          00001820
           02 MCINTERO  PIC X(5).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MQCDEA    PIC X.                                          00001850
           02 MQCDEC    PIC X.                                          00001860
           02 MQCDEP    PIC X.                                          00001870
           02 MQCDEH    PIC X.                                          00001880
           02 MQCDEV    PIC X.                                          00001890
           02 MQCDEO    PIC X(5).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MQRECA    PIC X.                                          00001920
           02 MQRECC    PIC X.                                          00001930
           02 MQRECP    PIC X.                                          00001940
           02 MQRECH    PIC X.                                          00001950
           02 MQRECV    PIC X.                                          00001960
           02 MQRECO    PIC X(5).                                       00001970
           02 MLIGNEO OCCURS   12 TIMES .                               00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MDCOMMENTA   PIC X.                                     00002000
             03 MDCOMMENTC   PIC X.                                     00002010
             03 MDCOMMENTP   PIC X.                                     00002020
             03 MDCOMMENTH   PIC X.                                     00002030
             03 MDCOMMENTV   PIC X.                                     00002040
             03 MDCOMMENTO   PIC X(8).                                  00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MLCOMMENTA   PIC X.                                     00002070
             03 MLCOMMENTC   PIC X.                                     00002080
             03 MLCOMMENTP   PIC X.                                     00002090
             03 MLCOMMENTH   PIC X.                                     00002100
             03 MLCOMMENTV   PIC X.                                     00002110
             03 MLCOMMENTO   PIC X(62).                                 00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MZONCMDA  PIC X.                                          00002140
           02 MZONCMDC  PIC X.                                          00002150
           02 MZONCMDP  PIC X.                                          00002160
           02 MZONCMDH  PIC X.                                          00002170
           02 MZONCMDV  PIC X.                                          00002180
           02 MZONCMDO  PIC X(15).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLIBERRA  PIC X.                                          00002210
           02 MLIBERRC  PIC X.                                          00002220
           02 MLIBERRP  PIC X.                                          00002230
           02 MLIBERRH  PIC X.                                          00002240
           02 MLIBERRV  PIC X.                                          00002250
           02 MLIBERRO  PIC X(58).                                      00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCICSA    PIC X.                                          00002350
           02 MCICSC    PIC X.                                          00002360
           02 MCICSP    PIC X.                                          00002370
           02 MCICSH    PIC X.                                          00002380
           02 MCICSV    PIC X.                                          00002390
           02 MCICSO    PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNETNAMA  PIC X.                                          00002420
           02 MNETNAMC  PIC X.                                          00002430
           02 MNETNAMP  PIC X.                                          00002440
           02 MNETNAMH  PIC X.                                          00002450
           02 MNETNAMV  PIC X.                                          00002460
           02 MNETNAMO  PIC X(8).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSCREENA  PIC X.                                          00002490
           02 MSCREENC  PIC X.                                          00002500
           02 MSCREENP  PIC X.                                          00002510
           02 MSCREENH  PIC X.                                          00002520
           02 MSCREENV  PIC X.                                          00002530
           02 MSCREENO  PIC X(4).                                       00002540
                                                                                
