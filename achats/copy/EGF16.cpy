      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF16   EGF16                                              00000020
      ***************************************************************** 00000030
       01   EGF16I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCDEI    PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE20L      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLIGNE20L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIGNE20F      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLIGNE20I      PIC X(11).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVALMANOL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MVALMANOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MVALMANOF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MVALMANOI      PIC X.                                     00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPCOL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCTYPCOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTYPCOF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCTYPCOI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTRL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNENTRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNENTRF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNENTRI   PIC X(6).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFOURNL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNFOURNF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNFOURNI  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFOURNL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLFOURNF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLFOURNI  PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATSAIL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDATSAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATSAIF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDATSAII  PIC X(8).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCINTERL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCINTERF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCINTERI  PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLINTERL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLINTERF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLINTERI  PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATVALL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MDATVALL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATVALF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDATVALI  PIC X(8).                                       00000650
           02 M117I OCCURS   10 TIMES .                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNSOCI  PIC X(3).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MNLIEUI      PIC X(3).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNCODICI     PIC X(7).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCFAMI  PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MCMARQI      PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MLREFI  PIC X(20).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTECOML     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MQTECOML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTECOMF     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQTECOMI     PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTERECL     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MQTERECL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTERECF     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQTERECI     PIC X(5).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTESOLL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MQTESOLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTESOLF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MQTESOLI     PIC X(5).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MZONCMDI  PIC X(15).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MLIBERRI  PIC X(58).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCODTRAI  PIC X(4).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MNETNAMI  PIC X(8).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MSCREENI  PIC X(4).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE11L      COMP PIC S9(4).                            00001270
      *--                                                                       
           02 MLIGNE11L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIGNE11F      PIC X.                                     00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MLIGNE11I      PIC X(24).                                 00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE12L      COMP PIC S9(4).                            00001310
      *--                                                                       
           02 MLIGNE12L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIGNE12F      PIC X.                                     00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MLIGNE12I      PIC X(3).                                  00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE21L      COMP PIC S9(4).                            00001350
      *--                                                                       
           02 MLIGNE21L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIGNE21F      PIC X.                                     00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MLIGNE21I      PIC X(24).                                 00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE22L      COMP PIC S9(4).                            00001390
      *--                                                                       
           02 MLIGNE22L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIGNE22F      PIC X.                                     00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MLIGNE22I      PIC X(6).                                  00001420
      ***************************************************************** 00001430
      * SDF: EGF16   EGF16                                              00001440
      ***************************************************************** 00001450
       01   EGF16O REDEFINES EGF16I.                                    00001460
           02 FILLER    PIC X(12).                                      00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MDATJOUA  PIC X.                                          00001490
           02 MDATJOUC  PIC X.                                          00001500
           02 MDATJOUP  PIC X.                                          00001510
           02 MDATJOUH  PIC X.                                          00001520
           02 MDATJOUV  PIC X.                                          00001530
           02 MDATJOUO  PIC X(10).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MTIMJOUA  PIC X.                                          00001560
           02 MTIMJOUC  PIC X.                                          00001570
           02 MTIMJOUP  PIC X.                                          00001580
           02 MTIMJOUH  PIC X.                                          00001590
           02 MTIMJOUV  PIC X.                                          00001600
           02 MTIMJOUO  PIC X(5).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MWPAGEA   PIC X.                                          00001630
           02 MWPAGEC   PIC X.                                          00001640
           02 MWPAGEP   PIC X.                                          00001650
           02 MWPAGEH   PIC X.                                          00001660
           02 MWPAGEV   PIC X.                                          00001670
           02 MWPAGEO   PIC X(3).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MWFONCA   PIC X.                                          00001700
           02 MWFONCC   PIC X.                                          00001710
           02 MWFONCP   PIC X.                                          00001720
           02 MWFONCH   PIC X.                                          00001730
           02 MWFONCV   PIC X.                                          00001740
           02 MWFONCO   PIC X(3).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MNCDEA    PIC X.                                          00001770
           02 MNCDEC    PIC X.                                          00001780
           02 MNCDEP    PIC X.                                          00001790
           02 MNCDEH    PIC X.                                          00001800
           02 MNCDEV    PIC X.                                          00001810
           02 MNCDEO    PIC X(7).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MLIGNE20A      PIC X.                                     00001840
           02 MLIGNE20C PIC X.                                          00001850
           02 MLIGNE20P PIC X.                                          00001860
           02 MLIGNE20H PIC X.                                          00001870
           02 MLIGNE20V PIC X.                                          00001880
           02 MLIGNE20O      PIC X(11).                                 00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MVALMANOA      PIC X.                                     00001910
           02 MVALMANOC PIC X.                                          00001920
           02 MVALMANOP PIC X.                                          00001930
           02 MVALMANOH PIC X.                                          00001940
           02 MVALMANOV PIC X.                                          00001950
           02 MVALMANOO      PIC X.                                     00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MCTYPCOA  PIC X.                                          00001980
           02 MCTYPCOC  PIC X.                                          00001990
           02 MCTYPCOP  PIC X.                                          00002000
           02 MCTYPCOH  PIC X.                                          00002010
           02 MCTYPCOV  PIC X.                                          00002020
           02 MCTYPCOO  PIC X(5).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MNENTRA   PIC X.                                          00002050
           02 MNENTRC   PIC X.                                          00002060
           02 MNENTRP   PIC X.                                          00002070
           02 MNENTRH   PIC X.                                          00002080
           02 MNENTRV   PIC X.                                          00002090
           02 MNENTRO   PIC X(6).                                       00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MNFOURNA  PIC X.                                          00002120
           02 MNFOURNC  PIC X.                                          00002130
           02 MNFOURNP  PIC X.                                          00002140
           02 MNFOURNH  PIC X.                                          00002150
           02 MNFOURNV  PIC X.                                          00002160
           02 MNFOURNO  PIC X(5).                                       00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MLFOURNA  PIC X.                                          00002190
           02 MLFOURNC  PIC X.                                          00002200
           02 MLFOURNP  PIC X.                                          00002210
           02 MLFOURNH  PIC X.                                          00002220
           02 MLFOURNV  PIC X.                                          00002230
           02 MLFOURNO  PIC X(20).                                      00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MDATSAIA  PIC X.                                          00002260
           02 MDATSAIC  PIC X.                                          00002270
           02 MDATSAIP  PIC X.                                          00002280
           02 MDATSAIH  PIC X.                                          00002290
           02 MDATSAIV  PIC X.                                          00002300
           02 MDATSAIO  PIC X(8).                                       00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MCINTERA  PIC X.                                          00002330
           02 MCINTERC  PIC X.                                          00002340
           02 MCINTERP  PIC X.                                          00002350
           02 MCINTERH  PIC X.                                          00002360
           02 MCINTERV  PIC X.                                          00002370
           02 MCINTERO  PIC X(5).                                       00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MLINTERA  PIC X.                                          00002400
           02 MLINTERC  PIC X.                                          00002410
           02 MLINTERP  PIC X.                                          00002420
           02 MLINTERH  PIC X.                                          00002430
           02 MLINTERV  PIC X.                                          00002440
           02 MLINTERO  PIC X(20).                                      00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MDATVALA  PIC X.                                          00002470
           02 MDATVALC  PIC X.                                          00002480
           02 MDATVALP  PIC X.                                          00002490
           02 MDATVALH  PIC X.                                          00002500
           02 MDATVALV  PIC X.                                          00002510
           02 MDATVALO  PIC X(8).                                       00002520
           02 M117O OCCURS   10 TIMES .                                 00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MNSOCA  PIC X.                                          00002550
             03 MNSOCC  PIC X.                                          00002560
             03 MNSOCP  PIC X.                                          00002570
             03 MNSOCH  PIC X.                                          00002580
             03 MNSOCV  PIC X.                                          00002590
             03 MNSOCO  PIC X(3).                                       00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MNLIEUA      PIC X.                                     00002620
             03 MNLIEUC PIC X.                                          00002630
             03 MNLIEUP PIC X.                                          00002640
             03 MNLIEUH PIC X.                                          00002650
             03 MNLIEUV PIC X.                                          00002660
             03 MNLIEUO      PIC X(3).                                  00002670
             03 FILLER       PIC X(2).                                  00002680
             03 MNCODICA     PIC X.                                     00002690
             03 MNCODICC     PIC X.                                     00002700
             03 MNCODICP     PIC X.                                     00002710
             03 MNCODICH     PIC X.                                     00002720
             03 MNCODICV     PIC X.                                     00002730
             03 MNCODICO     PIC X(7).                                  00002740
             03 FILLER       PIC X(2).                                  00002750
             03 MCFAMA  PIC X.                                          00002760
             03 MCFAMC  PIC X.                                          00002770
             03 MCFAMP  PIC X.                                          00002780
             03 MCFAMH  PIC X.                                          00002790
             03 MCFAMV  PIC X.                                          00002800
             03 MCFAMO  PIC X(5).                                       00002810
             03 FILLER       PIC X(2).                                  00002820
             03 MCMARQA      PIC X.                                     00002830
             03 MCMARQC PIC X.                                          00002840
             03 MCMARQP PIC X.                                          00002850
             03 MCMARQH PIC X.                                          00002860
             03 MCMARQV PIC X.                                          00002870
             03 MCMARQO      PIC X(5).                                  00002880
             03 FILLER       PIC X(2).                                  00002890
             03 MLREFA  PIC X.                                          00002900
             03 MLREFC  PIC X.                                          00002910
             03 MLREFP  PIC X.                                          00002920
             03 MLREFH  PIC X.                                          00002930
             03 MLREFV  PIC X.                                          00002940
             03 MLREFO  PIC X(20).                                      00002950
             03 FILLER       PIC X(2).                                  00002960
             03 MQTECOMA     PIC X.                                     00002970
             03 MQTECOMC     PIC X.                                     00002980
             03 MQTECOMP     PIC X.                                     00002990
             03 MQTECOMH     PIC X.                                     00003000
             03 MQTECOMV     PIC X.                                     00003010
             03 MQTECOMO     PIC X(5).                                  00003020
             03 FILLER       PIC X(2).                                  00003030
             03 MQTERECA     PIC X.                                     00003040
             03 MQTERECC     PIC X.                                     00003050
             03 MQTERECP     PIC X.                                     00003060
             03 MQTERECH     PIC X.                                     00003070
             03 MQTERECV     PIC X.                                     00003080
             03 MQTERECO     PIC X(5).                                  00003090
             03 FILLER       PIC X(2).                                  00003100
             03 MQTESOLA     PIC X.                                     00003110
             03 MQTESOLC     PIC X.                                     00003120
             03 MQTESOLP     PIC X.                                     00003130
             03 MQTESOLH     PIC X.                                     00003140
             03 MQTESOLV     PIC X.                                     00003150
             03 MQTESOLO     PIC X(5).                                  00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MZONCMDA  PIC X.                                          00003180
           02 MZONCMDC  PIC X.                                          00003190
           02 MZONCMDP  PIC X.                                          00003200
           02 MZONCMDH  PIC X.                                          00003210
           02 MZONCMDV  PIC X.                                          00003220
           02 MZONCMDO  PIC X(15).                                      00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MLIBERRA  PIC X.                                          00003250
           02 MLIBERRC  PIC X.                                          00003260
           02 MLIBERRP  PIC X.                                          00003270
           02 MLIBERRH  PIC X.                                          00003280
           02 MLIBERRV  PIC X.                                          00003290
           02 MLIBERRO  PIC X(58).                                      00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MCODTRAA  PIC X.                                          00003320
           02 MCODTRAC  PIC X.                                          00003330
           02 MCODTRAP  PIC X.                                          00003340
           02 MCODTRAH  PIC X.                                          00003350
           02 MCODTRAV  PIC X.                                          00003360
           02 MCODTRAO  PIC X(4).                                       00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MCICSA    PIC X.                                          00003390
           02 MCICSC    PIC X.                                          00003400
           02 MCICSP    PIC X.                                          00003410
           02 MCICSH    PIC X.                                          00003420
           02 MCICSV    PIC X.                                          00003430
           02 MCICSO    PIC X(5).                                       00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MNETNAMA  PIC X.                                          00003460
           02 MNETNAMC  PIC X.                                          00003470
           02 MNETNAMP  PIC X.                                          00003480
           02 MNETNAMH  PIC X.                                          00003490
           02 MNETNAMV  PIC X.                                          00003500
           02 MNETNAMO  PIC X(8).                                       00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MSCREENA  PIC X.                                          00003530
           02 MSCREENC  PIC X.                                          00003540
           02 MSCREENP  PIC X.                                          00003550
           02 MSCREENH  PIC X.                                          00003560
           02 MSCREENV  PIC X.                                          00003570
           02 MSCREENO  PIC X(4).                                       00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MLIGNE11A      PIC X.                                     00003600
           02 MLIGNE11C PIC X.                                          00003610
           02 MLIGNE11P PIC X.                                          00003620
           02 MLIGNE11H PIC X.                                          00003630
           02 MLIGNE11V PIC X.                                          00003640
           02 MLIGNE11O      PIC X(24).                                 00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MLIGNE12A      PIC X.                                     00003670
           02 MLIGNE12C PIC X.                                          00003680
           02 MLIGNE12P PIC X.                                          00003690
           02 MLIGNE12H PIC X.                                          00003700
           02 MLIGNE12V PIC X.                                          00003710
           02 MLIGNE12O      PIC X(3).                                  00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MLIGNE21A      PIC X.                                     00003740
           02 MLIGNE21C PIC X.                                          00003750
           02 MLIGNE21P PIC X.                                          00003760
           02 MLIGNE21H PIC X.                                          00003770
           02 MLIGNE21V PIC X.                                          00003780
           02 MLIGNE21O      PIC X(24).                                 00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MLIGNE22A      PIC X.                                     00003810
           02 MLIGNE22C PIC X.                                          00003820
           02 MLIGNE22P PIC X.                                          00003830
           02 MLIGNE22H PIC X.                                          00003840
           02 MLIGNE22V PIC X.                                          00003850
           02 MLIGNE22O      PIC X(6).                                  00003860
                                                                                
