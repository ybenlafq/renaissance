      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGF2100                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF2100                         
      **********************************************************                
       01  RVGF2100.                                                            
           10 GF21-NCDE                 PIC X(7).                               
           10 GF21-NSOCLIVR             PIC X(3).                               
           10 GF21-NDEPOT               PIC X(3).                               
           10 GF21-NCODIC               PIC X(7).                               
           10 GF21-QCDE                 PIC S9(05)V USAGE COMP-3.               
           10 GF21-WTYPCDE              PIC X(1).                               
           10 GF21-NSOCIETE             PIC X(3).                               
           10 GF21-NLIEU                PIC X(3).                               
           10 GF21-NVENTE               PIC X(7).                               
           10 GF21-NSEQ                 PIC X(2).                               
           10 GF21-DCREACDE             PIC X(8).                               
           10 GF21-DDESTOCKFOUR         PIC X(8).                               
           10 GF21-DLIVFOUR             PIC X(8).                               
           10 GF21-DDISPO               PIC X(8).                               
           10 GF21-WENVOIEDI            PIC X(1).                               
           10 GF21-DDELIV               PIC X(8).                               
           10 GF21-DMODIF               PIC X(8).                               
           10 GF21-NSOCDEPLIV           PIC X(3).                               
           10 GF21-NLIEUDEPLIV          PIC X(3).                               
           10 GF21-DANNUL               PIC X(8).                               
           10 GF21-DRECEPT              PIC X(8).                               
           10 GF21-DSYST                PIC S9(13)V USAGE COMP-3.               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGF2100                                  
      **********************************************************                
       01  RVGF2100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-NCDE-F               PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-NCDE-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-NSOCLIVR-F           PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-NSOCLIVR-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-NDEPOT-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-NDEPOT-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-NCODIC-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-NCODIC-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-QCDE-F               PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-QCDE-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-WTYPCDE-F            PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-WTYPCDE-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-NSOCIETE-F           PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-NSOCIETE-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-NLIEU-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-NLIEU-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-NVENTE-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-NVENTE-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-NSEQ-F               PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-NSEQ-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-DCREACDE-F           PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-DCREACDE-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-DDESTOCKFOUR-F       PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-DDESTOCKFOUR-F       PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-DLIVFOUR-F           PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-DLIVFOUR-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-DDISPO-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-DDISPO-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-WENVOIEDI-F          PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-WENVOIEDI-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-DDELIV-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-DDELIV-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-DMODIF-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-DMODIF-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-NSOCDEPLIV-F         PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-NSOCDEPLIV-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-NLIEUDEPLIV-F        PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-NLIEUDEPLIV-F        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-DANNUL-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-DANNUL-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-DRECEPT-F            PIC S9(4) COMP.                         
      *--                                                                       
           10 GF21-DRECEPT-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF21-DSYST-F              PIC S9(4) COMP.                         
      *                                                                         
      *--                                                                       
           10 GF21-DSYST-F              PIC S9(4) COMP-5.                       
                                                                                
      *}                                                                        
