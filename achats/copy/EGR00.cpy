      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR00   EGR00                                              00000020
      ***************************************************************** 00000030
       01   EGR00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(15).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDEPL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MSOCDEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCDEPF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MSOCDEPI  PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDEPOTI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFONCL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLFONCF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLFONCI   PIC X(10).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBFONCL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLIBFONCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBFONCF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLIBFONCI      PIC X(20).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MFONCI    PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE0L   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNCDE0L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE0F   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNCDE0I   PIC X(7).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODIC0L      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNCODIC0L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODIC0F      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNCODIC0I      PIC X(7).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOPTIONL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MOPTIONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MOPTIONF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MOPTIONI  PIC X(31).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB1L    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLIB1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB1F    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLIB1I    PIC X(9).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRECQL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNRECQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNRECQF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNRECQI   PIC X(7).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNREC4L   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNREC4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNREC4F   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNREC4I   PIC X(7).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODIC4L  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCODIC4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODIC4F  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCODIC4I  PIC X(7).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNREC5L   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MNREC5L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNREC5F   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNREC5I   PIC X(7).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODIC6L  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCODIC6L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODIC6F  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCODIC6I  PIC X(7).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEDEBL      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MDATEDEBL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEDEBF      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MDATEDEBI      PIC X(10).                                 00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEFINL      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MDATEFINL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEFINF      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MDATEFINI      PIC X(10).                                 00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLIBERRI  PIC X(78).                                      00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCODTRAI  PIC X(4).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCICSI    PIC X(5).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MNETNAMI  PIC X(8).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MSCREENI  PIC X(4).                                       00001010
      ***************************************************************** 00001020
      * SDF: EGR00   EGR00                                              00001030
      ***************************************************************** 00001040
       01   EGR00O REDEFINES EGR00I.                                    00001050
           02 FILLER    PIC X(12).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MDATJOUA  PIC X.                                          00001080
           02 MDATJOUC  PIC X.                                          00001090
           02 MDATJOUP  PIC X.                                          00001100
           02 MDATJOUH  PIC X.                                          00001110
           02 MDATJOUV  PIC X.                                          00001120
           02 MDATJOUO  PIC X(10).                                      00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MTIMJOUA  PIC X.                                          00001150
           02 MTIMJOUC  PIC X.                                          00001160
           02 MTIMJOUP  PIC X.                                          00001170
           02 MTIMJOUH  PIC X.                                          00001180
           02 MTIMJOUV  PIC X.                                          00001190
           02 MTIMJOUO  PIC X(5).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MZONCMDA  PIC X.                                          00001220
           02 MZONCMDC  PIC X.                                          00001230
           02 MZONCMDP  PIC X.                                          00001240
           02 MZONCMDH  PIC X.                                          00001250
           02 MZONCMDV  PIC X.                                          00001260
           02 MZONCMDO  PIC X(15).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MSOCDEPA  PIC X.                                          00001290
           02 MSOCDEPC  PIC X.                                          00001300
           02 MSOCDEPP  PIC X.                                          00001310
           02 MSOCDEPH  PIC X.                                          00001320
           02 MSOCDEPV  PIC X.                                          00001330
           02 MSOCDEPO  PIC X(3).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MDEPOTA   PIC X.                                          00001360
           02 MDEPOTC   PIC X.                                          00001370
           02 MDEPOTP   PIC X.                                          00001380
           02 MDEPOTH   PIC X.                                          00001390
           02 MDEPOTV   PIC X.                                          00001400
           02 MDEPOTO   PIC X(3).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MLFONCA   PIC X.                                          00001430
           02 MLFONCC   PIC X.                                          00001440
           02 MLFONCP   PIC X.                                          00001450
           02 MLFONCH   PIC X.                                          00001460
           02 MLFONCV   PIC X.                                          00001470
           02 MLFONCO   PIC X(10).                                      00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MLIBFONCA      PIC X.                                     00001500
           02 MLIBFONCC PIC X.                                          00001510
           02 MLIBFONCP PIC X.                                          00001520
           02 MLIBFONCH PIC X.                                          00001530
           02 MLIBFONCV PIC X.                                          00001540
           02 MLIBFONCO      PIC X(20).                                 00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MFONCA    PIC X.                                          00001570
           02 MFONCC    PIC X.                                          00001580
           02 MFONCP    PIC X.                                          00001590
           02 MFONCH    PIC X.                                          00001600
           02 MFONCV    PIC X.                                          00001610
           02 MFONCO    PIC X(3).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MNCDE0A   PIC X.                                          00001640
           02 MNCDE0C   PIC X.                                          00001650
           02 MNCDE0P   PIC X.                                          00001660
           02 MNCDE0H   PIC X.                                          00001670
           02 MNCDE0V   PIC X.                                          00001680
           02 MNCDE0O   PIC X(7).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MNCODIC0A      PIC X.                                     00001710
           02 MNCODIC0C PIC X.                                          00001720
           02 MNCODIC0P PIC X.                                          00001730
           02 MNCODIC0H PIC X.                                          00001740
           02 MNCODIC0V PIC X.                                          00001750
           02 MNCODIC0O      PIC X(7).                                  00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MOPTIONA  PIC X.                                          00001780
           02 MOPTIONC  PIC X.                                          00001790
           02 MOPTIONP  PIC X.                                          00001800
           02 MOPTIONH  PIC X.                                          00001810
           02 MOPTIONV  PIC X.                                          00001820
           02 MOPTIONO  PIC X(31).                                      00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MLIB1A    PIC X.                                          00001850
           02 MLIB1C    PIC X.                                          00001860
           02 MLIB1P    PIC X.                                          00001870
           02 MLIB1H    PIC X.                                          00001880
           02 MLIB1V    PIC X.                                          00001890
           02 MLIB1O    PIC X(9).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MNRECQA   PIC X.                                          00001920
           02 MNRECQC   PIC X.                                          00001930
           02 MNRECQP   PIC X.                                          00001940
           02 MNRECQH   PIC X.                                          00001950
           02 MNRECQV   PIC X.                                          00001960
           02 MNRECQO   PIC X(7).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MNREC4A   PIC X.                                          00001990
           02 MNREC4C   PIC X.                                          00002000
           02 MNREC4P   PIC X.                                          00002010
           02 MNREC4H   PIC X.                                          00002020
           02 MNREC4V   PIC X.                                          00002030
           02 MNREC4O   PIC X(7).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCODIC4A  PIC X.                                          00002060
           02 MCODIC4C  PIC X.                                          00002070
           02 MCODIC4P  PIC X.                                          00002080
           02 MCODIC4H  PIC X.                                          00002090
           02 MCODIC4V  PIC X.                                          00002100
           02 MCODIC4O  PIC X(7).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MNREC5A   PIC X.                                          00002130
           02 MNREC5C   PIC X.                                          00002140
           02 MNREC5P   PIC X.                                          00002150
           02 MNREC5H   PIC X.                                          00002160
           02 MNREC5V   PIC X.                                          00002170
           02 MNREC5O   PIC X(7).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MCODIC6A  PIC X.                                          00002200
           02 MCODIC6C  PIC X.                                          00002210
           02 MCODIC6P  PIC X.                                          00002220
           02 MCODIC6H  PIC X.                                          00002230
           02 MCODIC6V  PIC X.                                          00002240
           02 MCODIC6O  PIC X(7).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MDATEDEBA      PIC X.                                     00002270
           02 MDATEDEBC PIC X.                                          00002280
           02 MDATEDEBP PIC X.                                          00002290
           02 MDATEDEBH PIC X.                                          00002300
           02 MDATEDEBV PIC X.                                          00002310
           02 MDATEDEBO      PIC X(10).                                 00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MDATEFINA      PIC X.                                     00002340
           02 MDATEFINC PIC X.                                          00002350
           02 MDATEFINP PIC X.                                          00002360
           02 MDATEFINH PIC X.                                          00002370
           02 MDATEFINV PIC X.                                          00002380
           02 MDATEFINO      PIC X(10).                                 00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MLIBERRA  PIC X.                                          00002410
           02 MLIBERRC  PIC X.                                          00002420
           02 MLIBERRP  PIC X.                                          00002430
           02 MLIBERRH  PIC X.                                          00002440
           02 MLIBERRV  PIC X.                                          00002450
           02 MLIBERRO  PIC X(78).                                      00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MCODTRAA  PIC X.                                          00002480
           02 MCODTRAC  PIC X.                                          00002490
           02 MCODTRAP  PIC X.                                          00002500
           02 MCODTRAH  PIC X.                                          00002510
           02 MCODTRAV  PIC X.                                          00002520
           02 MCODTRAO  PIC X(4).                                       00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MCICSA    PIC X.                                          00002550
           02 MCICSC    PIC X.                                          00002560
           02 MCICSP    PIC X.                                          00002570
           02 MCICSH    PIC X.                                          00002580
           02 MCICSV    PIC X.                                          00002590
           02 MCICSO    PIC X(5).                                       00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MNETNAMA  PIC X.                                          00002620
           02 MNETNAMC  PIC X.                                          00002630
           02 MNETNAMP  PIC X.                                          00002640
           02 MNETNAMH  PIC X.                                          00002650
           02 MNETNAMV  PIC X.                                          00002660
           02 MNETNAMO  PIC X(8).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MSCREENA  PIC X.                                          00002690
           02 MSCREENC  PIC X.                                          00002700
           02 MSCREENP  PIC X.                                          00002710
           02 MSCREENH  PIC X.                                          00002720
           02 MSCREENV  PIC X.                                          00002730
           02 MSCREENO  PIC X(4).                                       00002740
                                                                                
