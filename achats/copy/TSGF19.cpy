      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : MAJ SAISIE DE COMMANDE FOURNISSEUR                     *         
      *            ARTICLES-QUANTITES (TRX:GF19)                      *         
      *****************************************************************         
      *   LL  1142C.                                                            
      *****************************************************************         
       01  TS-GF19-DESCR.                                                       
      *-----------------------------------------------------LONGUEUR TS         
      *    02 TS-GF19-LONG PIC S9(5) COMP-3  VALUE +1147.                       
           02 TS-GF19-LONG PIC S9(5) COMP-3  VALUE +1148.                       
      *                                                                         
           02 TS-GF19.                                                          
      *-----------------------------------------INFOS ISSU DE GF..---           
              03 TS-GF19-NCDE              PIC X(07).                           
              03 TS-GF19-NCODIC            PIC X(07).                           
      *-----------------------------------------INFOS ISSU DE GF20---..         
              03 TS-GF19-QSOLDE            PIC S9(05) COMP-3.                   
      *-----------------------------------------VARIABLE TRT---------..         
              03 TS-GF19-CMAJ              PIC  X(01).                          
      *{ remove-comma-in-dde 1.5                                                
      *          88 AFFICHABLE        VALUE 'B' , 'M'.                          
      *--                                                                       
                 88 AFFICHABLE        VALUE 'B'   'M'.                          
      *}                                                                        
                 88 LIGNE-SANS-MODIF  VALUE 'B'.                                
                 88 LIGNE-MODIFIEE    VALUE 'M'.                                
                 88 LIGNE-SUPPRIMEE   VALUE 'S'.                                
      *-----------------------------------------VARIABLE TRT---------..         
              03   TS-GF19-COMMENTAIRE    PIC  X(1).                            
                 88   TS-GF19-COMMENT     VALUE 'O'.                            
                 88   TS-GF19-NO-COMMENT  VALUE 'N'.                            
      *-----------------------------------------VARIBLE TRT----------..         
              03 TS-GF19-NSOCIETE          PIC X(03).                           
              03 TS-GF19-NLIEU             PIC X(03).                           
      *-----------------------------------------INFOS ISSU DE GA00---33         
              03 TS-GF19-LREFFOURN         PIC X(20).                           
              03 TS-GF19-CFAM              PIC X(05).                           
              03 TS-GF19-CMARQ             PIC X(05).                           
              03 TS-GF19-QCOLIRECEPT       PIC S9(05) COMP-3.                   
              03 TS-GF19-CMODSTOCK         PIC  X(05).                          
              03 TS-GF19-CQUOTA            PIC  X(05).                          
      *-----LL = 24 * 20 = 480------------------TABLEAU 4 X 5 ---------         
              03 TS-GF19-INFOS.                                                 
              04 TS-GF19-POSTE OCCURS 20 TIMES.                                 
              05 TS-GF19-QCDE              PIC S9(05) COMP-3.                   
              05 TS-GF19-QREC              PIC S9(05) COMP-3.                   
              05 TS-GF19-QUOLIVR           PIC S9(05) COMP-3.                   
              05 TS-GF19-QUOPAL            PIC S9(05) COMP-3.                   
              05 TS-GF19-DLIVRAISON        PIC  X(08).                          
              05 TS-GF19-NLIVRAISON        PIC  X(07).                          
      *-----LL = 24 * 20 = 480--ORIGINE---------TABLEAU 4 X 5 ---------         
              03 TS-GF19-0-INFOS.                                               
              04 TS-GF19-0-POSTE OCCURS 20 TIMES.                               
              05 TS-GF19-0-QCDE              PIC S9(05) COMP-3.                 
              05 TS-GF19-0-QREC              PIC S9(05) COMP-3.                 
              05 TS-GF19-0-QUOLIVR           PIC S9(05) COMP-3.                 
              05 TS-GF19-0-QUOPAL            PIC S9(05) COMP-3.                 
              05 TS-GF19-0-DLIVRAISON        PIC  X(08).                        
              05 TS-GF19-0-NLIVRAISON        PIC  X(07).                        
      *-----------------------------------------FILLER-----------------         
                                                                                
