      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGR04 (TGR00 -> MENU)    TR: GR00  *    00020000
      *               GESTION DES RECEPTIONS                       *    00030000
      *                                                                 00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3596  00050001
      *                                                                 00060000
      *        TRANSACTION GR04 :                                       00070000
      *                                                                 00080000
             03 COMM-GR04-APPLI REDEFINES COMM-GR01-APPLI.              00090001
      *------------------------------ ZONE DONNEES TGR04                00100000
                04 COMM-GR04-DONNEES.                                   00110001
                   05 COMM-GR04-PAGE           PIC 99.                  00120001
                   05 COMM-GR04-PAGETOT        PIC 99.                  00130001
                   05 COMM-GR04-GA28        OCCURS 20.                  00140001
                      06 COMM-GA28-DVALIDITE   PIC X(8).                00150001
                      06 COMM-GA28-QSOLA1      PIC S9(3)V9(2) COMP-3.   00160001
                      06 COMM-GA28-QSOLA2      PIC S9(3)V9(2) COMP-3.   00170001
                      06 COMM-GA28-QRACKB1     PIC S9(3)V9(2) COMP-3.   00180001
                      06 COMM-GA28-QRACKB2     PIC S9(3)V9(2) COMP-3.   00190001
                      06 COMM-GA28-QRACKB3     PIC S9(3)V9(2) COMP-3.   00200001
                      06 COMM-GA28-QRACKB4     PIC S9(3)V9(2) COMP-3.   00210001
                      06 COMM-GA28-QVRACC1     PIC S9(3)V9(2) COMP-3.   00220001
      *------------------------------ ZONE LIBRE                        00230000
      *         04 COMM-GR04-LIBRE          PIC X(3012).                00240001
                04 COMM-GR04-LIBRE          PIC X(1872).                        
      ***************************************************************** 00250000
                                                                                
