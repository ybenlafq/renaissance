      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF22   EGF22                                              00000020
      ***************************************************************** 00000030
       01   EGF22I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRIL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCTRIL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTRIF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCTRII    PIC X.                                          00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTCDEIL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNENTCDEIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNENTCDEIF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNENTCDEII     PIC X(5).                                  00000290
           02 M108I OCCURS   15 TIMES .                                 00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSELL  COMP PIC S9(4).                                 00000310
      *--                                                                       
             03 MWSELL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MWSELF  PIC X.                                          00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MWSELI  PIC X.                                          00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCDEL  COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MNCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNCDEF  PIC X.                                          00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MNCDEI  PIC X(7).                                       00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDLIVRL      COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MDLIVRL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDLIVRF      PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MDLIVRI      PIC X(8).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTCDEL    COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNENTCDEF    PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNENTCDEI    PIC X(5).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENTCDEL    COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MLENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLENTCDEF    PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MLENTCDEI    PIC X(20).                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMODTRAITL   COMP PIC S9(4).                                    
      *--                                                                       
             03 MMODTRAITL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MMODTRAITF   PIC X.                                             
             03 FILLER  PIC X(4).                                       00000530
             03 MMODTRAITI   PIC X(6).                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTIMESTPEL   COMP PIC S9(4).                                    
      *--                                                                       
             03 MTIMESTPEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MTIMESTPEF   PIC X.                                             
             03 FILLER  PIC X(4).                                       00000570
             03 MTIMESTPEI   PIC X(20).                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MZONCMDI  PIC X(15).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MLIBERRI  PIC X(58).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCODTRAI  PIC X(4).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCICSI    PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MNETNAMI  PIC X(8).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MSCREENI  PIC X(4).                                       00000820
      ***************************************************************** 00000830
      * SDF: EGF22   EGF22                                              00000840
      ***************************************************************** 00000850
       01   EGF22O REDEFINES EGF22I.                                    00000860
           02 FILLER    PIC X(12).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MDATJOUA  PIC X.                                          00000890
           02 MDATJOUC  PIC X.                                          00000900
           02 MDATJOUP  PIC X.                                          00000910
           02 MDATJOUH  PIC X.                                          00000920
           02 MDATJOUV  PIC X.                                          00000930
           02 MDATJOUO  PIC X(10).                                      00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MTIMJOUA  PIC X.                                          00000960
           02 MTIMJOUC  PIC X.                                          00000970
           02 MTIMJOUP  PIC X.                                          00000980
           02 MTIMJOUH  PIC X.                                          00000990
           02 MTIMJOUV  PIC X.                                          00001000
           02 MTIMJOUO  PIC X(5).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MNPAGEA   PIC X.                                          00001030
           02 MNPAGEC   PIC X.                                          00001040
           02 MNPAGEP   PIC X.                                          00001050
           02 MNPAGEH   PIC X.                                          00001060
           02 MNPAGEV   PIC X.                                          00001070
           02 MNPAGEO   PIC X(3).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MCFONCA   PIC X.                                          00001100
           02 MCFONCC   PIC X.                                          00001110
           02 MCFONCP   PIC X.                                          00001120
           02 MCFONCH   PIC X.                                          00001130
           02 MCFONCV   PIC X.                                          00001140
           02 MCFONCO   PIC X(3).                                       00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MCTRIA    PIC X.                                          00001170
           02 MCTRIC    PIC X.                                          00001180
           02 MCTRIP    PIC X.                                          00001190
           02 MCTRIH    PIC X.                                          00001200
           02 MCTRIV    PIC X.                                          00001210
           02 MCTRIO    PIC X.                                          00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MNENTCDEIA     PIC X.                                     00001240
           02 MNENTCDEIC     PIC X.                                     00001250
           02 MNENTCDEIP     PIC X.                                     00001260
           02 MNENTCDEIH     PIC X.                                     00001270
           02 MNENTCDEIV     PIC X.                                     00001280
           02 MNENTCDEIO     PIC X(5).                                  00001290
           02 M108O OCCURS   15 TIMES .                                 00001300
             03 FILLER       PIC X(2).                                  00001310
             03 MWSELA  PIC X.                                          00001320
             03 MWSELC  PIC X.                                          00001330
             03 MWSELP  PIC X.                                          00001340
             03 MWSELH  PIC X.                                          00001350
             03 MWSELV  PIC X.                                          00001360
             03 MWSELO  PIC X.                                          00001370
             03 FILLER       PIC X(2).                                  00001380
             03 MNCDEA  PIC X.                                          00001390
             03 MNCDEC  PIC X.                                          00001400
             03 MNCDEP  PIC X.                                          00001410
             03 MNCDEH  PIC X.                                          00001420
             03 MNCDEV  PIC X.                                          00001430
             03 MNCDEO  PIC X(7).                                       00001440
             03 FILLER       PIC X(2).                                  00001450
             03 MDLIVRA      PIC X.                                     00001460
             03 MDLIVRC PIC X.                                          00001470
             03 MDLIVRP PIC X.                                          00001480
             03 MDLIVRH PIC X.                                          00001490
             03 MDLIVRV PIC X.                                          00001500
             03 MDLIVRO      PIC X(8).                                  00001510
             03 FILLER       PIC X(2).                                  00001520
             03 MNENTCDEA    PIC X.                                     00001530
             03 MNENTCDEC    PIC X.                                     00001540
             03 MNENTCDEP    PIC X.                                     00001550
             03 MNENTCDEH    PIC X.                                     00001560
             03 MNENTCDEV    PIC X.                                     00001570
             03 MNENTCDEO    PIC X(5).                                  00001580
             03 FILLER       PIC X(2).                                  00001590
             03 MLENTCDEA    PIC X.                                     00001600
             03 MLENTCDEC    PIC X.                                     00001610
             03 MLENTCDEP    PIC X.                                     00001620
             03 MLENTCDEH    PIC X.                                     00001630
             03 MLENTCDEV    PIC X.                                     00001640
             03 MLENTCDEO    PIC X(20).                                 00001650
             03 FILLER       PIC X(2).                                  00001660
             03 MMODTRAITA   PIC X.                                             
             03 MMODTRAITC   PIC X.                                             
             03 MMODTRAITP   PIC X.                                             
             03 MMODTRAITH   PIC X.                                             
             03 MMODTRAITV   PIC X.                                             
             03 MMODTRAITO   PIC X(6).                                          
             03 FILLER       PIC X(2).                                  00001730
             03 MTIMESTPEA   PIC X.                                             
             03 MTIMESTPEC   PIC X.                                             
             03 MTIMESTPEP   PIC X.                                             
             03 MTIMESTPEH   PIC X.                                             
             03 MTIMESTPEV   PIC X.                                             
             03 MTIMESTPEO   PIC X(20).                                         
           02 FILLER    PIC X(2).                                       00001800
           02 MZONCMDA  PIC X.                                          00001810
           02 MZONCMDC  PIC X.                                          00001820
           02 MZONCMDP  PIC X.                                          00001830
           02 MZONCMDH  PIC X.                                          00001840
           02 MZONCMDV  PIC X.                                          00001850
           02 MZONCMDO  PIC X(15).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLIBERRA  PIC X.                                          00001880
           02 MLIBERRC  PIC X.                                          00001890
           02 MLIBERRP  PIC X.                                          00001900
           02 MLIBERRH  PIC X.                                          00001910
           02 MLIBERRV  PIC X.                                          00001920
           02 MLIBERRO  PIC X(58).                                      00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCODTRAA  PIC X.                                          00001950
           02 MCODTRAC  PIC X.                                          00001960
           02 MCODTRAP  PIC X.                                          00001970
           02 MCODTRAH  PIC X.                                          00001980
           02 MCODTRAV  PIC X.                                          00001990
           02 MCODTRAO  PIC X(4).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCICSA    PIC X.                                          00002020
           02 MCICSC    PIC X.                                          00002030
           02 MCICSP    PIC X.                                          00002040
           02 MCICSH    PIC X.                                          00002050
           02 MCICSV    PIC X.                                          00002060
           02 MCICSO    PIC X(5).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNETNAMA  PIC X.                                          00002090
           02 MNETNAMC  PIC X.                                          00002100
           02 MNETNAMP  PIC X.                                          00002110
           02 MNETNAMH  PIC X.                                          00002120
           02 MNETNAMV  PIC X.                                          00002130
           02 MNETNAMO  PIC X(8).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MSCREENA  PIC X.                                          00002160
           02 MSCREENC  PIC X.                                          00002170
           02 MSCREENP  PIC X.                                          00002180
           02 MSCREENH  PIC X.                                          00002190
           02 MSCREENV  PIC X.                                          00002200
           02 MSCREENO  PIC X(4).                                       00002210
                                                                                
