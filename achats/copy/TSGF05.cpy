      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : TS LIGNES DE COMMANDE ISSUE DE RESERVATION VENTE       *         
      *****************************************************************         
      *                                                               *         
       01  TS-GF05.                                                             
           02 TS-GF05-GESTION.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TS-GF05-POS                  PIC S9(4) COMP.                  
      *--                                                                       
               05 TS-GF05-POS                  PIC S9(4) COMP-5.                
      *}                                                                        
      *--------------------------------  STRUCTURE TS                           
           02 TS-GF05-DONNEES.                                                  
             03 TS-GF05-ENR.                                                    
               05 TS-GF05-NCODIC               PIC X(7).                        
               05 TS-GF05-NSOCIETE             PIC X(3).                        
               05 TS-GF05-NSOCLIVR             PIC X(3).                        
               05 TS-GF05-NDEPOT               PIC X(3).                        
               05 TS-GF05-QTE                  PIC 9(5) COMP-3.                 
               05 TS-GF05-DATE                 PIC X(8).                        
               05 TS-GF05-DATECALC             PIC X(8).                        
               05 TS-GF05-NENTCDE              PIC X(5).                        
               05 TS-GF05-NCDE                 PIC X(7).                        
               05 TS-GF05-NLIEU                PIC X(3).                        
               05 TS-GF05-CHEFPROD             PIC X(5).                        
               05 TS-GF05-CTYPCDE              PIC X(05).                       
               05 TS-GF05-CMODSTOCK            PIC X(5).                        
               05 TS-GF05-CQUOTA               PIC X(5).                        
               05 TS-GF05-NLIGNE               PIC X(2).                        
               05 TS-GF05-NPAGE                PIC X(3).                        
               05 TS-GF05-CINTERLOCUT          PIC X(5).                        
               05 TS-GF05-CQUALIF              PIC X(6).                        
               05 TS-GF05-DANNEE               PIC X(4).                        
               05 TS-GF05-SEMAINE              PIC X(2).                        
               05 TS-GF05-JSEM                 PIC 9(1).                        
               05 TS-GF05-DETAT                PIC X(8).                        
               05 TS-GF05-NSEQ                 PIC X(7).                        
               05 TS-GF05-NBRUO                PIC S9(5) COMP-3.                
               05 TS-GF05-QPALETTE             PIC S9(5) COMP-3.                
               05 TS-GF05-TRAITE               PIC X(1).                        
NSKEP          05 TS-GF05-CFEXT                PIC X(1).                        
                                                                                
