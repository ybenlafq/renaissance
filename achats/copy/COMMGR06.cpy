      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGR06 (TGR00 -> MENU)    TR: GR00  *    00020000
      *               GESTION DES RECEPTIONS                       *    00030000
      *                                                                 00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 2456  00050029
      *                                                                 00060000
      *        TRANSACTION GR06 :                                       00070000
      *                                                                 00080000
          03 COMM-GR06-APPLI REDEFINES COMM-GR01-APPLI.                 00090026
      *------------------------------ ZONE DONNEES TGR06                00100000
             04 COMM-GR06-DONNEES-TGR06.                                00110026
                05 COMM-GR06-NSOCIETE          PIC X(3).                00121026
                05 COMM-GR06-LSOCIETE          PIC X(20).               00125026
                05 COMM-GR06-NDEPOT            PIC X(3).                00126026
                05 COMM-GR06-LDEPOT            PIC X(20).               00127027
                05 COMM-GR06-MIN-DATE          PIC X(8).                00130026
                05 COMM-GR06-LET-SITE          PIC X(1).                00130226
                05 COMM-GR06-NCDE              PIC X(7).                00131026
                05 COMM-GR06-NCODIC            PIC X(7).                00140026
                05 COMM-GR06-PAGE              PIC 9(4).                00160026
                05 COMM-GR06-NBPAGES           PIC 9(4).                00161026
                05 COMM-GR06-NBPAGES-X         PIC 9(4).                00161126
                05 COMM-GR06-NBPIECES          PIC 9(5).                00161226
                05 COMM-GR06-NBPRODUITS        PIC 9(5).                00161326
                05 COMM-GR06-NBLIGNES          PIC 9(5).                00161426
                05 COMM-GR06-TSLONG            PIC 9(1).                00162026
                   88 TS06-PAS-TROP-LONGUE           VALUE 0.           00163006
                   88 TS06-TROP-LONGUE               VALUE 1.           00164006
                05 COMM-GR06-MODIF             PIC 9(1).                00165026
                   88 SELECTION-MODIFIEE             VALUE 0.           00166008
                   88 SELECTION-NON-MODIFIEE         VALUE 1.           00167008
E0715           05 COMM-GR06-FOURN-MODIF       PIC 9(1).                00167131
E0715              88 FOURNISSEUR-MODIFIE            VALUE 0.           00167231
E0715              88 FOURNISSEUR-NON-MODIFIE        VALUE 1.           00167331
                05 COMM-GR06-LIGNE-SELECT      PIC 9(1).                00168026
                   88 LIGNE-SELECT                   VALUE 0.           00169114
                   88 AUCUNE-LIGNE-SELECT            VALUE 1.           00169214
                05 COMM-GR06-STYPAGE           PIC X(2).                00170026
                05 COMM-GR06-SRESUME           PIC X(1).                00180026
                05 COMM-GR06-SNENTCDE          PIC X(5).                00190026
                05 COMM-GR06-SNCDE             PIC X(7).                00200026
                05 COMM-GR06-SNEAN             PIC X(13).               00210026
                05 COMM-GR06-SNCODIC           PIC X(7).                00211026
                05 COMM-GR06-SLREFO            PIC X(20).               00212026
E0715           05 COMM-GR06-LENTCDE           PIC X(20).               00212130
                05 COMM-GR06-SW0-NCODIC        PIC X(91).               00212226
                05 COMM-GR06-PF5               PIC X.                   00213026
                   88 COMM-GR06-PF5-1X               VALUE '1'.         00214006
                   88 COMM-GR06-PF5-2X               VALUE '2'.         00215006
                   88 COMM-GR06-VALID-OK             VALUE '3'.         00216017
                05 COMM-GR06-MESSAGE           PIC X(70).               00217026
      *------------------------------ ZONE LIBRE                        00260000
MAXDEP*      04 COMM-GR06-LIBRE          PIC X(3280).                   00270028
MAXDEP       04 COMM-GR06-LIBRE          PIC X(2140).                   00271032
      ***************************************************************** 00280000
                                                                                
