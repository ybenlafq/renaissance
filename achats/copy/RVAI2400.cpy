      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **************************************************************            
      * DCLGEN DE LA TABLE FEADET NON ETENDUE                                   
      **************************************************************            
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *   01 RVAI2400.                                                          
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
          01 RVAI2400.                                                          
      *}                                                                        
              05 AI24-FDCLAS PIC X(3).                                          
              05 AI24-FDFNUM PIC X(5).                                          
              05 AI24-FDPRIO PIC S9(5) COMP-3.                                  
              05 AI24-FDFTYP PIC S9(3) COMP-3.                                  
              05 AI24-FDTKDS PIC X(50).                                         
              05 AI24-FDADDS PIC X(50).                                         
              05 AI24-FDVLFL PIC X.                                             
              05 AI24-FDDSFL PIC X.                                             
              05 AI24-FDSTAT PIC X.                                             
              05 AI24-FDMULT PIC X.                                             
              05 AI24-FDBN01 PIC X(60).                                         
              05 AI24-FDBN02 PIC X(60).                                         
              05 AI24-FDBN03 PIC X(60).                                         
              05 AI24-FDBN04 PIC X(60).                                         
              05 AI24-FDBN05 PIC X(60).                                         
              05 AI24-FDBN06 PIC X(60).                                         
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      * INDICATEURS NULLITE                                             00005050
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *   01 RVAI2400-FLAGS.                                            00690000
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
          01 RVAI2400-FLAGS.                                                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI24-FDCLAS-F PIC S9(4) COMP.                                  
      *--                                                                       
              05 AI24-FDCLAS-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI24-FDFNUM-F PIC S9(4) COMP.                                  
      *--                                                                       
              05 AI24-FDFNUM-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI24-FDPRIO-F PIC S9(4) COMP.                                  
      *--                                                                       
              05 AI24-FDPRIO-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI24-FDFTYP-F PIC S9(4) COMP.                                  
      *--                                                                       
              05 AI24-FDFTYP-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI24-FDTKDS-F PIC S9(4) COMP.                                  
      *--                                                                       
              05 AI24-FDTKDS-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI24-FDADDS-F PIC S9(4) COMP.                                  
      *--                                                                       
              05 AI24-FDADDS-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI24-FDVLFL-F PIC S9(4) COMP.                                  
      *--                                                                       
              05 AI24-FDVLFL-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI24-FDDSFL-F PIC S9(4) COMP.                                  
      *--                                                                       
              05 AI24-FDDSFL-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI24-FDSTAT-F PIC S9(4) COMP.                                  
      *--                                                                       
              05 AI24-FDSTAT-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI24-FDMULT-F PIC S9(4) COMP.                                  
      *--                                                                       
              05 AI24-FDMULT-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI24-FDBN01-F PIC S9(4) COMP.                                  
      *--                                                                       
              05 AI24-FDBN01-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI24-FDBN02-F PIC S9(4) COMP.                                  
      *--                                                                       
              05 AI24-FDBN02-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI24-FDBN03-F PIC S9(4) COMP.                                  
      *--                                                                       
              05 AI24-FDBN03-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI24-FDBN04-F PIC S9(4) COMP.                                  
      *--                                                                       
              05 AI24-FDBN04-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI24-FDBN05-F PIC S9(4) COMP.                                  
      *--                                                                       
              05 AI24-FDBN05-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 AI24-FDBN06-F PIC S9(4) COMP.                                  
      *                                                                         
      *--                                                                       
              05 AI24-FDBN06-F PIC S9(4) COMP-5.                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
