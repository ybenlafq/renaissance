      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
              EJECT                                                             
      ***************************************************************           
      * DSECT DU FICHIER CD0001A                                    *           
      ***************************************************************           
      *                                                                         
       01  FGF01.                                                               
           05 FGF01-CLE.                                                        
              10 FGF01-NSOC            PIC 9(3).                                
              10 FGF01-CODFICH         PIC X(2).                                
              10 FGF01-NTYPCAR         PIC 9.                                   
              10 FGF01-CSOUSCLE        PIC X(8).                                
              10 FGF01-NSEQ            PIC 99.                                  
           05 FGF01-CODMAJ             PIC X.                                   
           05 FGF01-DATA               PIC X(83).                               
      *                                                                         
      *                                                                         
      * REDEFINITION DE LA CLE DU CHEMIN FGF01K                                 
      *                                                                         
       01  FILLER REDEFINES FGF01.                                              
           05 FGF01-FGF01K.                                                     
              10 FGF01-FGF01K-GFNSOC    PIC 9(3).                               
              10 FGF01-FGF01K-GFCODFIC  PIC X(2).                               
              10 FGF01-FGF01K-GFTYPCAR  PIC 9.                                  
              10 FGF01-FGF01K-GFSOUCLE  PIC X(8).                               
              10 FGF01-FGF01K-GFSEQUEN  PIC 99.                                 
      *                                                                         
                                                                                
