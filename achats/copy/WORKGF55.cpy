      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      * LONGUEUR 300                                                            
             03 COMM-GF55-APPLI.                                                
                05 COMM-GF55-SPECIF.                                            
                   10 COMM-GF55-TOP           PIC X(1).                         
                05 COMM-GF55-DONNEES.                                           
                   10 COMM-GF55-NCDEBOOK      PIC X(07).                        
                   10 COMM-GF55-NCODIC        PIC X(07).                        
                   10 COMM-GF55-LREFF0        PIC X(20).                        
                   10 COMM-GF55-NSOC          PIC X(3).                         
                   10 COMM-GF55-NDEPOT        PIC X(3).                         
                   10 COMM-GF55-NENTCDE       PIC X(5).                         
                   10 COMM-GF55-LENTCDE       PIC X(20).                        
                   10 COMM-GF55-DUEDATE       PIC X(8).                         
                   10 COMM-GF55-DLIVRAISON    PIC X(8).                         
                   10 COMM-GF55-NPAGE         PIC 9(3).                         
                   10 COMM-GF55-NPAGE-OLD     PIC 9(3).                         
                   10 COMM-GF55-NPAGEMAX      PIC 9(3).                         
                   10 COMM-GF55-SELECTION     PIC X(1).                         
                   10 COMM-GF55-CREAS-SEL     PIC X(5).                         
                   10 COMM-GF55-MLIBCOMMANDE  PIC X(12).                        
                   10 COMM-GF55-MLIBBOOKING   PIC X(12).                        
                   10 COMM-GF55-IND           PIC 9(2).                         
             03 COMM-GF55-FILLER              PIC X(177).                       
                                                                                
