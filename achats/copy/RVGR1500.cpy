      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGR1500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGR1500                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGR1500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGR1500.                                                            
      *}                                                                        
           02  GR15-NREC                                                        
               PIC X(0007).                                                     
           02  GR15-NCODIC                                                      
               PIC X(0007).                                                     
           02  GR15-NCDE                                                        
               PIC X(0007).                                                     
           02  GR15-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GR15-NLIEU                                                       
               PIC X(0003).                                                     
           02  GR15-QREC                                                        
               PIC S9(5) COMP-3.                                                
           02  GR15-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GR15-NBL                                                         
               PIC X(0010).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGR1500                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGR1500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGR1500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR15-NREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR15-NREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR15-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR15-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR15-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR15-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR15-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR15-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR15-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR15-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR15-QREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR15-QREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR15-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR15-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR15-NBL-F                                                       
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GR15-NBL-F                                                       
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
