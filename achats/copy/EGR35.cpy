      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR35   EGR35                                              00000020
      ***************************************************************** 00000030
       01   EGR35I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLSOCI    PIC X(19).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSAISIEL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MDSAISIEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDSAISIEF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDSAISIEI      PIC X(8).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDEPOTI   PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECEPL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDRECEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRECEPF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDRECEPI  PIC X(8).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIVRL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLIVRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIVRF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLIVRI    PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENTITEL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MENTITEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENTITEF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MENTITEI  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMODEL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MMODEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MMODEF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MMODEI    PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINTERL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MINTERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MINTERF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MINTERI   PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMML    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCOMML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCOMMF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCOMMI    PIC X(20).                                      00000570
           02 M150I OCCURS   8 TIMES .                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMANDEL   COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCOMMANDEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCOMMANDEF   PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCOMMANDEI   PIC X(7).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBONL  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MNBONL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNBONF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNBONI  PIC X(10).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODICL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODICF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCODICI      PIC X(7).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMARQUEL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MMARQUEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMARQUEF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MMARQUEI     PIC X(5).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFFL  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MREFFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MREFFF  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MREFFI  PIC X(19).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCOMML      COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MQCOMML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQCOMMF      PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQCOMMI      PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQRECUEL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MQRECUEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQRECUEF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQRECUEI     PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLITIGEL    COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MQLITIGEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQLITIGEF    PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQLITIGEI    PIC X(5).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(15).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(58).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * SDF: EGR35   EGR35                                              00001160
      ***************************************************************** 00001170
       01   EGR35O REDEFINES EGR35I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MPAGEA    PIC X.                                          00001350
           02 MPAGEC    PIC X.                                          00001360
           02 MPAGEP    PIC X.                                          00001370
           02 MPAGEH    PIC X.                                          00001380
           02 MPAGEV    PIC X.                                          00001390
           02 MPAGEO    PIC X(3).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MNSOCA    PIC X.                                          00001420
           02 MNSOCC    PIC X.                                          00001430
           02 MNSOCP    PIC X.                                          00001440
           02 MNSOCH    PIC X.                                          00001450
           02 MNSOCV    PIC X.                                          00001460
           02 MNSOCO    PIC X(3).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MLSOCA    PIC X.                                          00001490
           02 MLSOCC    PIC X.                                          00001500
           02 MLSOCP    PIC X.                                          00001510
           02 MLSOCH    PIC X.                                          00001520
           02 MLSOCV    PIC X.                                          00001530
           02 MLSOCO    PIC X(19).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MDSAISIEA      PIC X.                                     00001560
           02 MDSAISIEC PIC X.                                          00001570
           02 MDSAISIEP PIC X.                                          00001580
           02 MDSAISIEH PIC X.                                          00001590
           02 MDSAISIEV PIC X.                                          00001600
           02 MDSAISIEO      PIC X(8).                                  00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MDEPOTA   PIC X.                                          00001630
           02 MDEPOTC   PIC X.                                          00001640
           02 MDEPOTP   PIC X.                                          00001650
           02 MDEPOTH   PIC X.                                          00001660
           02 MDEPOTV   PIC X.                                          00001670
           02 MDEPOTO   PIC X(3).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MDRECEPA  PIC X.                                          00001700
           02 MDRECEPC  PIC X.                                          00001710
           02 MDRECEPP  PIC X.                                          00001720
           02 MDRECEPH  PIC X.                                          00001730
           02 MDRECEPV  PIC X.                                          00001740
           02 MDRECEPO  PIC X(8).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MLIVRA    PIC X.                                          00001770
           02 MLIVRC    PIC X.                                          00001780
           02 MLIVRP    PIC X.                                          00001790
           02 MLIVRH    PIC X.                                          00001800
           02 MLIVRV    PIC X.                                          00001810
           02 MLIVRO    PIC X(5).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MENTITEA  PIC X.                                          00001840
           02 MENTITEC  PIC X.                                          00001850
           02 MENTITEP  PIC X.                                          00001860
           02 MENTITEH  PIC X.                                          00001870
           02 MENTITEV  PIC X.                                          00001880
           02 MENTITEO  PIC X(5).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MMODEA    PIC X.                                          00001910
           02 MMODEC    PIC X.                                          00001920
           02 MMODEP    PIC X.                                          00001930
           02 MMODEH    PIC X.                                          00001940
           02 MMODEV    PIC X.                                          00001950
           02 MMODEO    PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MINTERA   PIC X.                                          00001980
           02 MINTERC   PIC X.                                          00001990
           02 MINTERP   PIC X.                                          00002000
           02 MINTERH   PIC X.                                          00002010
           02 MINTERV   PIC X.                                          00002020
           02 MINTERO   PIC X(5).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MCOMMA    PIC X.                                          00002050
           02 MCOMMC    PIC X.                                          00002060
           02 MCOMMP    PIC X.                                          00002070
           02 MCOMMH    PIC X.                                          00002080
           02 MCOMMV    PIC X.                                          00002090
           02 MCOMMO    PIC X(20).                                      00002100
           02 M150O OCCURS   8 TIMES .                                  00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MCOMMANDEA   PIC X.                                     00002130
             03 MCOMMANDEC   PIC X.                                     00002140
             03 MCOMMANDEP   PIC X.                                     00002150
             03 MCOMMANDEH   PIC X.                                     00002160
             03 MCOMMANDEV   PIC X.                                     00002170
             03 MCOMMANDEO   PIC X(7).                                  00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MNBONA  PIC X.                                          00002200
             03 MNBONC  PIC X.                                          00002210
             03 MNBONP  PIC X.                                          00002220
             03 MNBONH  PIC X.                                          00002230
             03 MNBONV  PIC X.                                          00002240
             03 MNBONO  PIC X(10).                                      00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MCODICA      PIC X.                                     00002270
             03 MCODICC PIC X.                                          00002280
             03 MCODICP PIC X.                                          00002290
             03 MCODICH PIC X.                                          00002300
             03 MCODICV PIC X.                                          00002310
             03 MCODICO      PIC X(7).                                  00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MMARQUEA     PIC X.                                     00002340
             03 MMARQUEC     PIC X.                                     00002350
             03 MMARQUEP     PIC X.                                     00002360
             03 MMARQUEH     PIC X.                                     00002370
             03 MMARQUEV     PIC X.                                     00002380
             03 MMARQUEO     PIC X(5).                                  00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MREFFA  PIC X.                                          00002410
             03 MREFFC  PIC X.                                          00002420
             03 MREFFP  PIC X.                                          00002430
             03 MREFFH  PIC X.                                          00002440
             03 MREFFV  PIC X.                                          00002450
             03 MREFFO  PIC X(19).                                      00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MQCOMMA      PIC X.                                     00002480
             03 MQCOMMC PIC X.                                          00002490
             03 MQCOMMP PIC X.                                          00002500
             03 MQCOMMH PIC X.                                          00002510
             03 MQCOMMV PIC X.                                          00002520
             03 MQCOMMO      PIC X(5).                                  00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MQRECUEA     PIC X.                                     00002550
             03 MQRECUEC     PIC X.                                     00002560
             03 MQRECUEP     PIC X.                                     00002570
             03 MQRECUEH     PIC X.                                     00002580
             03 MQRECUEV     PIC X.                                     00002590
             03 MQRECUEO     PIC X(5).                                  00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MQLITIGEA    PIC X.                                     00002620
             03 MQLITIGEC    PIC X.                                     00002630
             03 MQLITIGEP    PIC X.                                     00002640
             03 MQLITIGEH    PIC X.                                     00002650
             03 MQLITIGEV    PIC X.                                     00002660
             03 MQLITIGEO    PIC X(5).                                  00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MZONCMDA  PIC X.                                          00002690
           02 MZONCMDC  PIC X.                                          00002700
           02 MZONCMDP  PIC X.                                          00002710
           02 MZONCMDH  PIC X.                                          00002720
           02 MZONCMDV  PIC X.                                          00002730
           02 MZONCMDO  PIC X(15).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(58).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(4).                                       00003090
                                                                                
