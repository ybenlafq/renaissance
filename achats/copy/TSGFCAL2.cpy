      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : ETAT DES LIEUX FINAL                                   *         
      *        POUR LES CALAGES DES COMMANDES                         *         
      *****************************************************************         
       01 TS-GFCAL2-DESCR.                                                      
          02 TS-GFCAL2.                                                         
             03 TS-GFCAL2-NSURCDE         PIC X(07).                            
             03 TS-GFCAL2-NCDE            PIC X(07).                            
             03 TS-GFCAL2-NCODIC          PIC X(07).                            
             03 TS-GFCAL2-LREFFOURN       PIC X(20).                            
             03 TS-GFCAL2-NDEPOT          PIC X(06).                            
             03 TS-GFCAL2-POSTE OCCURS 20 TIMES.                                
                04 TS-GFCAL2-DLIVRAISON   PIC X(08).                            
                04 TS-GFCAL2-QCDE         PIC S9(05) COMP-3.                    
                04 TS-GFCAL2-CALEE        PIC X.                                
                                                                                
