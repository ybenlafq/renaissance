      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                         
      * TS MGF40 : LISTE CDE FILLE/CODIC SUPPRIMES DE LA COMMANDE               
      *                                                                         
       01  TS-GF40.                                                             
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03  TS-GF40-LONG             PIC  S9(4) COMP  VALUE +075.            
      *--                                                                       
           03  TS-GF40-LONG             PIC  S9(4) COMP-5  VALUE +075.          
      *}                                                                        
      *                                                                         
           03  TS-GF40-DONNEES.                                                 
      *                                                                         
             05 TS-GF40-NSURCDE            PIC X(7).                            
             05 TS-GF40-NCDE               PIC X(7).                            
             05 TS-GF40-NSOCLIVR           PIC X(3).                            
             05 TS-GF40-NDEPOT             PIC X(3).                            
             05 TS-GF40-DSAISIE            PIC X(8).                            
             05 TS-GF40-CVALORISAT         PIC X(1).                            
             05 TS-GF40-NAVENANT           PIC X(2).                            
             05 TS-GF40-NENTCDE            PIC X(5).                            
             05 TS-GF40-CTYPCDE            PIC X(5).                            
             05 TS-GF40-ORIGDATE           PIC X(8).                            
             05 TS-GF40-NCODIC             PIC X(7).                            
             05 TS-GF40-PBF                PIC 9(7)V9(2).                       
             05 TS-GF40-QCDEORIG           PIC 9(5).                            
             05 TS-GF40-CREASON            PIC X(5).                            
      *                                                                         
                                                                                
