      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGR12 (TGR00 -> MENU)    TR: GR00  *    00020001
      *               GESTION DES RECEPTIONS                       *    00030001
      *                                                                 00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3596  00050001
      *                                                                 00060000
      *        TRANSACTION GR12 : MAJ / CREATION DES QUOTAS         *   00070001
      *                                                                 00080000
             03 COMM-GR12-APPLI REDEFINES COMM-GR01-APPLI.              00090001
      *------------------------------ ZONE DONNEES TGR12                00100001
                04 COMM-GR12-DONNEES-TGR12.                             00110001
      *------------------------------ CODE FONCTION                     00120001
                   06 COMM-GR12-FONCTION          PIC X(3).             00130001
                   06 COMM-GR12-FONCTION2         PIC X(3).             00140001
      *------------------------------ TYPE DE QUOTA                     00150001
                   06 COMM-GR12-CQUOTA            PIC X(05).            00160001
                   06 COMM-GR12-CQUOTA2           PIC X(05).            00170001
      *------------------------------ TYPE DE RECEPTION                 00180001
                   06 COMM-GR12-CREC              PIC X(05).            00190001
      *------------------------------ DATE DE LA SEMAINE                00200001
                   06 COMM-GR12-SEMAINE.                                00210001
                      08 COMM-GR12-DANNEE         PIC X(4).             00220001
                      08 COMM-GR12-DSEMAINE       PIC X(2).             00230001
      *------------------------------ DATE SEMAINE N� 2                 00240001
                   06 COMM-GR12-SEMAINE2          PIC X(6).             00250001
      *------------------------------ SEMAINE DEBUT                     00260001
                   06 COMM-GR12-SEMDEB            PIC X(6).             00270001
                   06 COMM-GR12-SEMDEB2           PIC X(6).             00280001
      *------------------------------ SEMAINE FIN                       00290001
                   06 COMM-GR12-SEMFIN            PIC X(6).             00300001
                   06 COMM-GR12-SEMFIN2           PIC X(6).             00310001
      *-------------------------- QUANTITES D'UO PREVISIONNELLES TOTALES00320001
                   06 COMM-GR12-QUOPREV1          PIC S9(05) COMP-3.    00330001
                   06 COMM-GR12-QUOPREV2          PIC S9(05) COMP-3.    00340001
                   06 COMM-GR12-QUOPREV3          PIC S9(05) COMP-3.    00350001
                   06 COMM-GR12-QUOPREV4          PIC S9(05) COMP-3.    00360001
                   06 COMM-GR12-QUOPREV5          PIC S9(05) COMP-3.    00370001
                   06 COMM-GR12-QUOPREV6          PIC S9(05) COMP-3.    00380001
                   06 COMM-GR12-QUOPREV7          PIC S9(05) COMP-3.    00390001
      *-------------------------- ZONES QUOTAS                          00400001
      *-------------------------- QUOTAS D'ESTIMATION TOTALE            00410001
                   06 COMM-GR12-QUOTAS OCCURS 12.                       00420001
      *-------------------------- NUMERO DE SEMAINE                     00430001
                      08 COMM-GR12-NUMSEM         PIC 9(06).            00440001
      *-------------------------- QUOTAS D ESTIMATION TOTALE TTE SEMAINE00450001
                      08 COMM-GR12-QUOTA          PIC 9(05).            00460001
      *-------------------------- QUOTAS D ESTIMATION PAR JOUR          00470001
                      08 COMM-GR12-QUOTA1         PIC 9(05).            00480001
                      08 COMM-GR12-QUOTA2         PIC 9(05).            00490001
                      08 COMM-GR12-QUOTA3         PIC 9(05).            00500001
                      08 COMM-GR12-QUOTA4         PIC 9(05).            00510001
                      08 COMM-GR12-QUOTA5         PIC 9(05).            00520001
                      08 COMM-GR12-QUOTA6         PIC 9(05).            00530001
                      08 COMM-GR12-QUOTA7         PIC 9(05).            00540001
      *------------------------------ TABLE CODE SEMAINE PR PAGINATION  00550001
                   06 COMM-GR12-TABSEM         OCCURS 100.              00560001
      *------------------------------ 1ERS NUMEROS SEMAINE DES PAGES    00570001
                      08 COMM-GR12-NSEM           PIC X(6).             00580001
      *------------------------------ INDICATEUR ETAT FICHIER           00590001
                   06 COMM-GR12-INDPAG            PIC 9.                00600001
      *------------------------------ DERNIERE DATE SEMAINE AFFICHEE    00610001
                   06 COMM-GR12-PAGSUI            PIC X(6).             00620001
      *------------------------------ ANCIENNE DATE SEMAINE AFFICHEE    00630001
                   06 COMM-GR12-PAGENC            PIC X(6).             00640001
      *------------------------------ NUMERO PAGE ECRAN                 00650001
                   06 COMM-GR12-NUMPAG            PIC 9(3).             00660001
      *------------------------------ ZONE ATTRIBUTS                    00670001
                   06 COMM-GR12-ATTR              PIC 9 OCCURS 400.     00680001
      *------------------------------ VALIDATION CREATION PAR PAGE      00690001
                   06 COMM-GR12-VALID             PIC 9 OCCURS 100.     00700001
      *------------------------------ POURCENTAGE                       00710001
                   06 COMM-GR12-POURC.                                  00720001
                      08 COMM-GR12-ZONEPOUR       OCCURS 7.             00730001
                         10 COMM-GR12-POUR        PIC 9(2).             00740001
      *------------------------------ TYPE DE FERMETURE                 00750001
                   06 COMM-GR12-FERM.                                   00760001
                      08  COMM-GR12-TAB           OCCURS 12.            00770001
                          10  COMM-GR12-TYPE      OCCURS 7.             00780001
                              12  COMM-GR12-TYPFERM PIC X.              00790001
      *-------------------------------------PARAM QUOTA PALETTE                 
                   06 COMM-GR12-QUOTAPAL            PIC X.                      
                   06 COMM-GR12-DEPOTPAL            PIC X.                      
      *------------------------------ ZONE LIBRE                        00800001
      *         04 COMM-GR12-LIBRE          PIC X(1715).                00810001
      *         04 COMM-GR12-LIBRE          PIC X(1713).                        
      ***************************************************************** 00820000
                                                                                
