      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TGR22 (TGR00 -> MENU)    TR: GR00  *    00002201
      *               GESTION DES RECEPTIONS                       *    00002301
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3710  00009000
      *                                                                 00410100
      *        TRANSACTION GR22 : GESTION DES RECEPTIONS A QUAI    *    00411001
      *                           OPTION :  CREATION MODIFICATION  *    00411101
      *                                                                 00412000
          02 COMM-GR22-APPLI REDEFINES COMM-GR00-APPLI.                 00420001
      *------------------------------ ZONE DONNEES TGR22                00510001
             03 COMM-GR22-DONNEES-TGR22.                                00520001
      *------------------------------ CODE FONCTION                     00530001
                04 COMM-GR22-FONCTION          PIC X(03).               00540001
      *------------------------------ ZONE DONNEES 1 TGR22              00540101
                04 COMM-GR22-DONNEES-1-TGR22.                           00540201
      *------------------------------ CODE RECEPTION A QUAI             00540301
                   05 COMM-GR22-NRECQUAI          PIC X(07).            00540401
      *------------------------------ DATE JOUR DE RECEPTION A QUAI     00540501
                   05 COMM-GR22-DJRECQUAI         PIC X(08).            00540601
      *------------------------------ HEURE DE RECEPTION A QUAI         00540701
                   05 COMM-GR22-DHRECQUAI         PIC X(02).            00540801
      *------------------------------ MINUTE DE RECEPTION A QUAI        00540901
                   05 COMM-GR22-DMRECQUAI         PIC X(02).            00541001
      *------------------------------ LIBELLE COMMENTAIRE               00541101
                   05 COMM-GR22-LIBCOMMENT        PIC X(20).            00541201
      *------------------------------ CODE SOCIETE                      00541301
                   05 COMM-GR22-NSOCIETE          PIC X(03).            00541401
      *------------------------------ LIBELLE SOCIETE                   00541501
                   05 COMM-GR22-LSOCIETE          PIC X(20).            00542001
      *------------------------------ CODE TYPE DE RECEPTION A QUAI     00542002
                   05 COMM-GR22-CTRQ              PIC X(05).            00542003
      *------------------------------ CODE ENTREPOT                     00543001
                   05 COMM-GR22-NDEPOT            PIC X(03).            00544001
      *------------------------------ LIBELLE ENTREPOT                  00545001
                   05 COMM-GR22-LDEPOT            PIC X(20).            00546001
      *------------------------------ CODE LIVREUR                      00547001
                   05 COMM-GR22-CLIVR             PIC X(05).            00548001
      *------------------------------ LIBELLE LIVREUR                   00549001
                   05 COMM-GR22-LLIVR             PIC X(20).            00549101
      *------------------------------ CODE MODE DE TRANSPORT            00549201
                   05 COMM-GR22-CMODETRANS        PIC X(05).            00549301
      *------------------------------ LIBELLE MODE DE TRANSPORT         00549401
                   05 COMM-GR22-LMODETRANS        PIC X(20).            00549501
      *------------------------------ INDICATEUR FIN DE TABLE           00640101
                   05 COMM-GR22-INDPAG            PIC 9.                00640201
      *------------------------------ DERNIER IND-TS                    00640301
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            05 COMM-GR22-PAGSUI            PIC S9(4) COMP.       00640401
      *--                                                                       
                   05 COMM-GR22-PAGSUI            PIC S9(4) COMP-5.             
      *}                                                                        
      *------------------------------ ANCIEN  IND-TS                    00640501
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            05 COMM-GR22-PAGENC            PIC S9(4) COMP.       00640601
      *--                                                                       
                   05 COMM-GR22-PAGENC            PIC S9(4) COMP-5.             
      *}                                                                        
      *------------------------------ NUMERO PAGE ECRAN                 00640701
                   05 COMM-GR22-NUMPAG            PIC 9(3).             00640801
      *------------------------------ ZONE CONFIRMATION PF3             00640901
                   05 COMM-GR22-CONF-PF3          PIC 9(1).             00641001
      *------------------------------ ZONE CONFIRMATION PF4             00641101
                   05 COMM-GR22-CONF-PF4          PIC 9(1).             00641201
      *------------------------------ MAJ A EFFECTUEE RTGR00 => = '1'   00641301
                   05 COMM-GR22-MODIF-RTGR00   PIC  9.                  00641401
      *------------------------------ MAJ OK SUR RTGR00 => VALID = '1'  00641501
                   05 COMM-GR22-VALID-RTGR00   PIC  9.                  00641601
JA                 05 COMM-GR22-TYPUSER        PIC X(8).                00010000
      * MISE EN COMMENTAIRE                                                     
      *------------------------------ SOC/DEP AUTORISE                  00020000
JA    *            05 COMM-GR22-AUTOR-DIXDEPOT.                         00030000
JA    *               06 COMM-GR22-AUTOR-DEPOT OCCURS 10.               00040000
JA    *                  07 COMM-GR22-AUTOR-NSOCENTRE PIC X(3).         00050000
JA    *                  07 COMM-GR22-AUTOR-NDEPOT    PIC X(3).         00060000
      * FIN MISE EN COMMENTAIRE                                                 
      *------------------------------ ZONE DONNEES 2 TGR22              00641701
                04 COMM-GR22-DONNEES-2-TGR22.                           00642001
      *------------------------------ NBRE DE PIECES                    00650001
                   05 COMM-GR22-QPIECES           PIC X(06).            00660001
      *-------------------------- ZONE QUODICS PAGE                     00715001
                   05 COMM-GR22-LIGPAG.                                 00716001
      *-------------------------- ZONE LIGNES                           00716101
                     06 COMM-GR22-LIGCODICS   OCCURS 12.                00716201
                        07 COMM-GR22-NCODIC      PIC X(07).             00717001
                        07 COMM-GR22-CMARQ       PIC X(05).             00718001
                        07 COMM-GR22-CFAM        PIC X(05).             00718101
                        07 COMM-GR22-LEMBAL      PIC X(50).             00719001
                        07 COMM-GR22-QTE         PIC S9(05) COMP-3.     00719101
                        07 COMM-GR22-SELECT      PIC X.                 00719201
                        07 COMM-GR22-TYPCONDT    PIC X(05).             00719301
                        07 COMM-GR22-QCONDT      PIC S9(05) COMP-3.     00719401
                        07 COMM-GR22-QCOLIRECEPT PIC S9(05) COMP-3.     00719501
                        07 COMM-GR22-LCOMMENT    PIC X(45).             00719601
                        07 COMM-GR22-EXIST-GR05  PIC 9.                 00719701
      *------------------------------ TABLE N� TS PR PAGINATION         00720001
                   05 COMM-GR22-TABART         OCCURS 100.              00730001
      *------------------------------ 1ERS IND TS DES PAGES <=> CODIC   00731001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *               06 COMM-GR22-NUMTS          PIC S9(4) COMP.       00732001
      *--                                                                       
                      06 COMM-GR22-NUMTS          PIC S9(4) COMP-5.             
      *}                                                                        
      *------------------------------ IND POUR RECH DS TS               00739201
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            05 COMM-GR22-IND-RECH-TS    PIC S9(4) COMP.          00739301
      *--                                                                       
                   05 COMM-GR22-IND-RECH-TS    PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ IND TS MAX                        00739401
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            05 COMM-GR22-IND-TS-MAX     PIC S9(4) COMP.          00739501
      *--                                                                       
                   05 COMM-GR22-IND-TS-MAX     PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ MAJ OK SUR RTGR05 => VALID = '1'  00740001
                   05 COMM-GR22-VALID-RTGR05   PIC  9.                  00740101
      *------------------------------ MAJ EFFECTUEES    => VALID = '1'  00740201
                   05 COMM-GR22-VALID-OK       PIC  9.                  00740301
      *------------------------------ ZONE DONNEES 3 TGR22              00740401
                04 COMM-GR22-DONNEES-3-TGR22.                           00740501
      *------------------------------ ZONES SWAP                        00740601
                   05 COMM-GR22-ATTR           PIC X OCCURS 250.        00740701
      *------------------------------ ZONE DONNEES TGR24                00740801
             03 COMM-GR22-DONNEES-TGR24.                                00740901
      *------------------------------ CODE MARQUE                       00741001
                04 COMM-GR22-MARQUE-TGR24      PIC X(05).               00741101
      *------------------------------ CODE FAMILLE                      00741201
                04 COMM-GR22-FAMILLE-TGR24     PIC X(05).               00741301
      *------------------------------ CODE RAYON                        00741401
                04 COMM-GR22-RAYON-TGR24       PIC X(05).               00741501
      *------------------------------ LIBELLE MARQUE                    00741601
                04 COMM-GR22-LMARQUE-TGR24      PIC X(20).              00741701
      *------------------------------ LIBELLE FAMILLE                   00741801
                04 COMM-GR22-LFAMILLE-TGR24     PIC X(20).              00741901
      *------------------------------ LIBELLE RAYON                     00742001
                04 COMM-GR22-LRAYON-TGR24       PIC X(20).              00742101
      *------------------------------ SELECTION MARQUE                  00742201
                04 COMM-GR22-SELMAR-TGR24      PIC X(1).                00742301
      *------------------------------ INDICE NB FAMILLES RAYON          00742401
                04 COMM-GR22-INDFAM-TGR24      PIC 9(3).                00742501
      *-------------------------- TABLE DES FAMILLES PAR RAYON          00742601
                04 COMM-GR22-TABFAM-TGR24.                              00742701
                   06 COMM-GR22-CODFAM-TGR24   PIC X(5) OCCURS 150.     00742801
      *------------------------------ ZONE DONNEES TGR26                00742901
             03 COMM-GR22-DONNEES-TGR26.                                00743001
      *------------------------------ TABLE N� ART PR PAGINATION        00744301
                04 COMM-GR22-TABART-TGR26.                              00744401
                   05 COMM-GR22-TABARTFAM-TGR26 OCCURS 50.              00744501
      *------------------------------ 1ERS ARTICLES DES PAGES <=> CODIC 00744601
                     06 COMM-GR22-NUMART-TGR26   PIC X(07).             00744701
      *------------------------------ FAMILLE CORRESPONDANTES           00744801
                     06 COMM-GR22-FAMART-TGR26   PIC X(05).             00744901
      *------------------------------ INDICATEUR FIN DE TABLE           00745001
                04 COMM-GR22-INDPAG-TGR26      PIC 9.                   00745101
      *------------------------------ INDICE FAMILLES TRAITEES          00745201
                04 COMM-GR22-INDFAM-TGR26      PIC 9(3).                00745301
      *------------------------------ NUMERO ARTICLE POUR PAGE SUIVANTE 00745401
                04 COMM-GR22-PAGSUI-TGR26      PIC X(07).               00745501
      *------------------------------ NUMERO ARTICLE POUR PAGE PRECEDENT00745601
                04 COMM-GR22-PAGENC-TGR26      PIC X(07).               00745701
      *------------------------------ CODE FAMILLE  POUR PAGE SUIVANTE  00745801
                04 COMM-GR22-FAMSUI-TGR26      PIC X(05).               00745901
      *------------------------------ CODE FAMILLE POUR PAGE PRECEDENT  00746001
                04 COMM-GR22-FAMENC-TGR26      PIC X(05).               00746101
      *------------------------------ NUMERO PAGE ECRAN                 00746201
                04 COMM-GR22-NUMPAG-TGR26      PIC 9(3).                00746301
      *-------------------------- COLISAGE ARTICLE SELECTIONNE          00746401
                04 COMM-GR22-QCOLISAGE-TGR26   PIC 9(5).                00746501
      *-------------------------- NOMBRE DE COLIS ARTICLE SELECTIONNE   00746601
                04 COMM-GR22-QCOLIS-TGR26      PIC 9(3).                00746701
      *------------------------------ ZONE LIBRE                        00748001
      *      03 COMM-GR22-LIBRE          PIC X(21).                     00749001
             03 COMM-GR22-LIBRE          PIC X(81).                             
      ***************************************************************** 00750000
                                                                                
