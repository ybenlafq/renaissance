      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR06   EGR06                                              00000020
      ***************************************************************** 00000030
       01   EGR06I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPAGESI      PIC X(4).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPOTL    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCDEPOTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSOCDEPOTF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCDEPOTI    PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNDEPOTI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTYPAGEL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MSTYPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSTYPAGEF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MSTYPAGEI      PIC X(2).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNENTCDEL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MSNENTCDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSNENTCDEF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSNENTCDEI     PIC X(5).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTCDEL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTCDEF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLENTCDEI      PIC X(20).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSRESUMEL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MSRESUMEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSRESUMEF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSRESUMEI      PIC X.                                     00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNCDEL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MSNCDEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSNCDEF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MSNCDEI   PIC X(7).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNEANL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MSNEANL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSNEANF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSNEANI   PIC X(13).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNCODICL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MSNCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSNCODICF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSNCODICI      PIC X(7).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLREFOL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MSLREFOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSLREFOF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MSLREFOI  PIC X(20).                                      00000610
           02 MLIGNEI OCCURS   13 TIMES .                               00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNNCDEL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MNNCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNNCDEF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNNCDEI      PIC X(7).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNNEANL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNNEANL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNNEANF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNNEANI      PIC X(13).                                 00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNNCODICL    COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MNNCODICL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNNCODICF    PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MNNCODICI    PIC X(7).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLREFOL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MNLREFOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLREFOF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNLREFOI     PIC X(20).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNQRECL      COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MNQRECL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNQRECF      PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNQRECI      PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNNBLL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MNNBLL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNNBLF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MNNBLI  PIC X(10).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMENTL     COMP PIC S9(4).                            00000870
      *--                                                                       
           02 MLCOMMENTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCOMMENTF     PIC X.                                     00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLCOMMENTI     PIC X(31).                                 00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MLIBERRI  PIC X(73).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCODTRAI  PIC X(4).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCICSI    PIC X(5).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MNETNAMI  PIC X(8).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MSCREENI  PIC X(4).                                       00001100
      ***************************************************************** 00001110
      * SDF: EGR06   EGR06                                              00001120
      ***************************************************************** 00001130
       01   EGR06O REDEFINES EGR06I.                                    00001140
           02 FILLER    PIC X(12).                                      00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MDATJOUA  PIC X.                                          00001170
           02 MDATJOUC  PIC X.                                          00001180
           02 MDATJOUP  PIC X.                                          00001190
           02 MDATJOUH  PIC X.                                          00001200
           02 MDATJOUV  PIC X.                                          00001210
           02 MDATJOUO  PIC X(10).                                      00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MTIMJOUA  PIC X.                                          00001240
           02 MTIMJOUC  PIC X.                                          00001250
           02 MTIMJOUP  PIC X.                                          00001260
           02 MTIMJOUH  PIC X.                                          00001270
           02 MTIMJOUV  PIC X.                                          00001280
           02 MTIMJOUO  PIC X(5).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MPAGEA    PIC X.                                          00001310
           02 MPAGEC    PIC X.                                          00001320
           02 MPAGEP    PIC X.                                          00001330
           02 MPAGEH    PIC X.                                          00001340
           02 MPAGEV    PIC X.                                          00001350
           02 MPAGEO    PIC X(4).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MNBPAGESA      PIC X.                                     00001380
           02 MNBPAGESC PIC X.                                          00001390
           02 MNBPAGESP PIC X.                                          00001400
           02 MNBPAGESH PIC X.                                          00001410
           02 MNBPAGESV PIC X.                                          00001420
           02 MNBPAGESO      PIC X(4).                                  00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MNSOCDEPOTA    PIC X.                                     00001450
           02 MNSOCDEPOTC    PIC X.                                     00001460
           02 MNSOCDEPOTP    PIC X.                                     00001470
           02 MNSOCDEPOTH    PIC X.                                     00001480
           02 MNSOCDEPOTV    PIC X.                                     00001490
           02 MNSOCDEPOTO    PIC X(3).                                  00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MNDEPOTA  PIC X.                                          00001520
           02 MNDEPOTC  PIC X.                                          00001530
           02 MNDEPOTP  PIC X.                                          00001540
           02 MNDEPOTH  PIC X.                                          00001550
           02 MNDEPOTV  PIC X.                                          00001560
           02 MNDEPOTO  PIC X(3).                                       00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MSTYPAGEA      PIC X.                                     00001590
           02 MSTYPAGEC PIC X.                                          00001600
           02 MSTYPAGEP PIC X.                                          00001610
           02 MSTYPAGEH PIC X.                                          00001620
           02 MSTYPAGEV PIC X.                                          00001630
           02 MSTYPAGEO      PIC X(2).                                  00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MSNENTCDEA     PIC X.                                     00001660
           02 MSNENTCDEC     PIC X.                                     00001670
           02 MSNENTCDEP     PIC X.                                     00001680
           02 MSNENTCDEH     PIC X.                                     00001690
           02 MSNENTCDEV     PIC X.                                     00001700
           02 MSNENTCDEO     PIC X(5).                                  00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MLENTCDEA      PIC X.                                     00001730
           02 MLENTCDEC PIC X.                                          00001740
           02 MLENTCDEP PIC X.                                          00001750
           02 MLENTCDEH PIC X.                                          00001760
           02 MLENTCDEV PIC X.                                          00001770
           02 MLENTCDEO      PIC X(20).                                 00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MSRESUMEA      PIC X.                                     00001800
           02 MSRESUMEC PIC X.                                          00001810
           02 MSRESUMEP PIC X.                                          00001820
           02 MSRESUMEH PIC X.                                          00001830
           02 MSRESUMEV PIC X.                                          00001840
           02 MSRESUMEO      PIC X.                                     00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MSNCDEA   PIC X.                                          00001870
           02 MSNCDEC   PIC X.                                          00001880
           02 MSNCDEP   PIC X.                                          00001890
           02 MSNCDEH   PIC X.                                          00001900
           02 MSNCDEV   PIC X.                                          00001910
           02 MSNCDEO   PIC X(7).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MSNEANA   PIC X.                                          00001940
           02 MSNEANC   PIC X.                                          00001950
           02 MSNEANP   PIC X.                                          00001960
           02 MSNEANH   PIC X.                                          00001970
           02 MSNEANV   PIC X.                                          00001980
           02 MSNEANO   PIC X(13).                                      00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MSNCODICA      PIC X.                                     00002010
           02 MSNCODICC PIC X.                                          00002020
           02 MSNCODICP PIC X.                                          00002030
           02 MSNCODICH PIC X.                                          00002040
           02 MSNCODICV PIC X.                                          00002050
           02 MSNCODICO      PIC X(7).                                  00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MSLREFOA  PIC X.                                          00002080
           02 MSLREFOC  PIC X.                                          00002090
           02 MSLREFOP  PIC X.                                          00002100
           02 MSLREFOH  PIC X.                                          00002110
           02 MSLREFOV  PIC X.                                          00002120
           02 MSLREFOO  PIC X(20).                                      00002130
           02 MLIGNEO OCCURS   13 TIMES .                               00002140
             03 FILLER       PIC X(2).                                  00002150
             03 MNNCDEA      PIC X.                                     00002160
             03 MNNCDEC PIC X.                                          00002170
             03 MNNCDEP PIC X.                                          00002180
             03 MNNCDEH PIC X.                                          00002190
             03 MNNCDEV PIC X.                                          00002200
             03 MNNCDEO      PIC X(7).                                  00002210
             03 FILLER       PIC X(2).                                  00002220
             03 MNNEANA      PIC X.                                     00002230
             03 MNNEANC PIC X.                                          00002240
             03 MNNEANP PIC X.                                          00002250
             03 MNNEANH PIC X.                                          00002260
             03 MNNEANV PIC X.                                          00002270
             03 MNNEANO      PIC X(13).                                 00002280
             03 FILLER       PIC X(2).                                  00002290
             03 MNNCODICA    PIC X.                                     00002300
             03 MNNCODICC    PIC X.                                     00002310
             03 MNNCODICP    PIC X.                                     00002320
             03 MNNCODICH    PIC X.                                     00002330
             03 MNNCODICV    PIC X.                                     00002340
             03 MNNCODICO    PIC X(7).                                  00002350
             03 FILLER       PIC X(2).                                  00002360
             03 MNLREFOA     PIC X.                                     00002370
             03 MNLREFOC     PIC X.                                     00002380
             03 MNLREFOP     PIC X.                                     00002390
             03 MNLREFOH     PIC X.                                     00002400
             03 MNLREFOV     PIC X.                                     00002410
             03 MNLREFOO     PIC X(20).                                 00002420
             03 FILLER       PIC X(2).                                  00002430
             03 MNQRECA      PIC X.                                     00002440
             03 MNQRECC PIC X.                                          00002450
             03 MNQRECP PIC X.                                          00002460
             03 MNQRECH PIC X.                                          00002470
             03 MNQRECV PIC X.                                          00002480
             03 MNQRECO      PIC X(5).                                  00002490
             03 FILLER       PIC X(2).                                  00002500
             03 MNNBLA  PIC X.                                          00002510
             03 MNNBLC  PIC X.                                          00002520
             03 MNNBLP  PIC X.                                          00002530
             03 MNNBLH  PIC X.                                          00002540
             03 MNNBLV  PIC X.                                          00002550
             03 MNNBLO  PIC X(10).                                      00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MLCOMMENTA     PIC X.                                     00002580
           02 MLCOMMENTC     PIC X.                                     00002590
           02 MLCOMMENTP     PIC X.                                     00002600
           02 MLCOMMENTH     PIC X.                                     00002610
           02 MLCOMMENTV     PIC X.                                     00002620
           02 MLCOMMENTO     PIC X(31).                                 00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MLIBERRA  PIC X.                                          00002650
           02 MLIBERRC  PIC X.                                          00002660
           02 MLIBERRP  PIC X.                                          00002670
           02 MLIBERRH  PIC X.                                          00002680
           02 MLIBERRV  PIC X.                                          00002690
           02 MLIBERRO  PIC X(73).                                      00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MCODTRAA  PIC X.                                          00002720
           02 MCODTRAC  PIC X.                                          00002730
           02 MCODTRAP  PIC X.                                          00002740
           02 MCODTRAH  PIC X.                                          00002750
           02 MCODTRAV  PIC X.                                          00002760
           02 MCODTRAO  PIC X(4).                                       00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MCICSA    PIC X.                                          00002790
           02 MCICSC    PIC X.                                          00002800
           02 MCICSP    PIC X.                                          00002810
           02 MCICSH    PIC X.                                          00002820
           02 MCICSV    PIC X.                                          00002830
           02 MCICSO    PIC X(5).                                       00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MNETNAMA  PIC X.                                          00002860
           02 MNETNAMC  PIC X.                                          00002870
           02 MNETNAMP  PIC X.                                          00002880
           02 MNETNAMH  PIC X.                                          00002890
           02 MNETNAMV  PIC X.                                          00002900
           02 MNETNAMO  PIC X(8).                                       00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MSCREENA  PIC X.                                          00002930
           02 MSCREENC  PIC X.                                          00002940
           02 MSCREENP  PIC X.                                          00002950
           02 MSCREENH  PIC X.                                          00002960
           02 MSCREENV  PIC X.                                          00002970
           02 MSCREENO  PIC X(4).                                       00002980
                                                                                
