      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * FGK220: PRODUITS POUR KRISP                                             
      ******************************************************************        
       01  FGK220-ENR.                                                          
           02  FGK220-NCODIC         PIC X(07).                                 
           02  FGK220-NSOC           PIC X(03).                                 
           02  FGK220-WDACEM         PIC X(01).                                 
           02  FGK220-DCREATION      PIC X(08).                                 
           02  FGK220-CASSORT        PIC X(05).                                 
           02  FGK220-CAPPRO         PIC X(05).                                 
           02  FGK220-DATE           PIC X(06).                                 
           02  FGK220-CMKT           PIC X(05).                                 
           02  FGK220-LMKT           PIC X(20).                                 
           02  FGK220-CFAM           PIC X(05).                                 
           02  FGK220-LFAM           PIC X(20).                                 
           02  FGK220-LREFFOURN      PIC X(20).                                 
           02  FGK220-CMARQ          PIC X(05).                                 
           02  FGK220-LMARQ          PIC X(20).                                 
           02  FGK220-NEAN           PIC X(13).                                 
           02  FGK220-TAUXTVA        PIC S9(3)V999   PACKED-DECIMAL.            
           02  FGK220-PVUHT          PIC S9(7)V9999  PACKED-DECIMAL.            
           02  FGK220-PCAHT          PIC S9(11)V9999 PACKED-DECIMAL.            
           02  FGK220-QVENDUE        PIC S9(7)       PACKED-DECIMAL.            
           02  FGK220-STOCK          PIC S9(7)       PACKED-DECIMAL.            
           02  FGK220-VALOPRMP       PIC S9(11)V9999 PACKED-DECIMAL.            
           02  FGK220-VALOPV         PIC S9(11)V9999 PACKED-DECIMAL.            
           02  FGK220-PSTDTTC        PIC S9(7)V9999  PACKED-DECIMAL.            
           02  FGK220-STOCKMAG       PIC S9(7)       PACKED-DECIMAL.            
           02  FGK220-STOCKDEP       PIC S9(7)       PACKED-DECIMAL.            
           02  FGK220-PRMP           PIC S9(7)V9(6)  PACKED-DECIMAL.            
     *** ACHATS PAR ENTITE DE COMMANDE                                          
           02  FGK220-ACHATS        OCCURS 25.                                  
               03  FGK220-NENTCDE    PIC X(06).                                 
               03  FGK220-PRA        PIC S9(7)V9999  PACKED-DECIMAL.            
               03  FGK220-PACHATS    PIC S9(11)V9999 PACKED-DECIMAL.            
               03  FGK220-QREC       PIC S9(7)       PACKED-DECIMAL.            
               03  FGK220-QCDE       PIC S9(7)       PACKED-DECIMAL.            
     *** CHAQUE POSTE A 30 DE LONG -> 25 POSTES = 750                           
 957      02  FILLER                  PIC X(44).                                
1001 *** LRECL=1000                                                             
                                                                                
