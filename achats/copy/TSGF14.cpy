      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : TS LIGNES DE COMMANDE PAR CODIC ET PAR DATE            *         
      *        CREE PAR TGF13                                         *         
      *****************************************************************         
      *                                                               *         
       01  TS-GF14.                                                             
      *--------------------------------  LONGUEUR TS                            
           02 TS-GF14-LONG                    PIC S9(5) COMP-3                  
                                              VALUE +116.                       
           02 TS-GF14-DONNEES.                                                  
             03 TS-GF14-GF05.                                                   
      *--------------------------------  TABLE LIGNE COMMANDE                   
      *--------------------------------  CODIC                                  
               04 TS-GF14-NCODIC              PIC X(7).                         
      *--------------------------------  QTE GLOBALE PAR                        
      *                                  DEMANDEUR CODIC                        
               04 TS-GF14-QTETOT              PIC 9(5) COMP-3.                  
      *--------------------------------  QTE PAR DEMANDEUR CODIC                
      *                                  ET PAR DATE                            
               04 TS-GF14-TAB-QTE             OCCURS 4.                         
                 05 TS-GF14-QTE               PIC 9(5) COMP-3.                  
                 05 TS-GF14-DATE              PIC X(8).                         
                 05 TS-GF14-NBRUO             PIC S9(5) COMP-3.                 
                 05 TS-GF14-QPALETTE          PIC S9(5) COMP-3.                 
             03 TS-GF14-GA00.                                                   
      *--------------------------------  TABLE ARTICLE                          
               04 TS-GF14-CFAM                PIC X(5).                         
               04 TS-GF14-CMARQ               PIC X(5).                         
               04 TS-GF14-LREFFOURN           PIC X(20).                        
               04 TS-GF14-CMODSTOCK           PIC X(5).                         
               04 TS-GF14-QCOLIRECEPT         PIC 9(5) COMP-3.                  
                                                                                
