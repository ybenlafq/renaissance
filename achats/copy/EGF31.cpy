      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF31   EGF31                                              00000020
      ***************************************************************** 00000030
       01   EGF31I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFOURNL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNFOURNF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNFOURNI  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFOURNL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLFOURNF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLFOURNI  PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFPL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCHEFPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHEFPF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCHEFPI   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEBL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDATDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDEBF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDATDEBI  PIC X(8).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFINL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDATFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFINF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDATFINI  PIC X(8).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWSOLDEL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MWSOLDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWSOLDEF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MWSOLDEI  PIC X.                                          00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE1L  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLIGNE1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIGNE1F  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLIGNE1I  PIC X(57).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE21L      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MLIGNE21L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIGNE21F      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLIGNE21I      PIC X.                                     00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE22L      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MLIGNE22L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIGNE22F      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLIGNE22I      PIC X(3).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE31L      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MLIGNE31L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIGNE31F      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLIGNE31I      PIC X.                                     00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE32L      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MLIGNE32L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIGNE32F      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLIGNE32I      PIC X(3).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE4L  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLIGNE4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIGNE4F  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLIGNE4I  PIC X(57).                                      00000730
           02 M197I OCCURS   13 TIMES .                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTATUTL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MSTATUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTATUTF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MSTATUTI     PIC X(3).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCDEL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MNCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNCDEF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNCDEI  PIC X(7).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATSAIL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MDATSAIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATSAIF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MDATSAII     PIC X(8).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATVALL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MDATVALL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATVALF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MDATVALI     PIC X(8).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDEPOTL     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MNDEPOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNDEPOTF     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MNDEPOTI     PIC X(3).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTECOML     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MQTECOML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTECOMF     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQTECOMI     PIC X(5).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTERECL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MQTERECL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTERECF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MQTERECI     PIC X(5).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTESOLL     COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MQTESOLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTESOLF     PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MQTESOLI     PIC X(5).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MZONCMDI  PIC X(15).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MLIBERRI  PIC X(58).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCODTRAI  PIC X(4).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCICSI    PIC X(5).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MNETNAMI  PIC X(8).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MSCREENI  PIC X(4).                                       00001300
      ***************************************************************** 00001310
      * SDF: EGF31   EGF31                                              00001320
      ***************************************************************** 00001330
       01   EGF31O REDEFINES EGF31I.                                    00001340
           02 FILLER    PIC X(12).                                      00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MDATJOUA  PIC X.                                          00001370
           02 MDATJOUC  PIC X.                                          00001380
           02 MDATJOUP  PIC X.                                          00001390
           02 MDATJOUH  PIC X.                                          00001400
           02 MDATJOUV  PIC X.                                          00001410
           02 MDATJOUO  PIC X(10).                                      00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MTIMJOUA  PIC X.                                          00001440
           02 MTIMJOUC  PIC X.                                          00001450
           02 MTIMJOUP  PIC X.                                          00001460
           02 MTIMJOUH  PIC X.                                          00001470
           02 MTIMJOUV  PIC X.                                          00001480
           02 MTIMJOUO  PIC X(5).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MWPAGEA   PIC X.                                          00001510
           02 MWPAGEC   PIC X.                                          00001520
           02 MWPAGEP   PIC X.                                          00001530
           02 MWPAGEH   PIC X.                                          00001540
           02 MWPAGEV   PIC X.                                          00001550
           02 MWPAGEO   PIC X(3).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MWFONCA   PIC X.                                          00001580
           02 MWFONCC   PIC X.                                          00001590
           02 MWFONCP   PIC X.                                          00001600
           02 MWFONCH   PIC X.                                          00001610
           02 MWFONCV   PIC X.                                          00001620
           02 MWFONCO   PIC X(3).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MNFOURNA  PIC X.                                          00001650
           02 MNFOURNC  PIC X.                                          00001660
           02 MNFOURNP  PIC X.                                          00001670
           02 MNFOURNH  PIC X.                                          00001680
           02 MNFOURNV  PIC X.                                          00001690
           02 MNFOURNO  PIC X(5).                                       00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MLFOURNA  PIC X.                                          00001720
           02 MLFOURNC  PIC X.                                          00001730
           02 MLFOURNP  PIC X.                                          00001740
           02 MLFOURNH  PIC X.                                          00001750
           02 MLFOURNV  PIC X.                                          00001760
           02 MLFOURNO  PIC X(20).                                      00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCHEFPA   PIC X.                                          00001790
           02 MCHEFPC   PIC X.                                          00001800
           02 MCHEFPP   PIC X.                                          00001810
           02 MCHEFPH   PIC X.                                          00001820
           02 MCHEFPV   PIC X.                                          00001830
           02 MCHEFPO   PIC X(5).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MCFAMA    PIC X.                                          00001860
           02 MCFAMC    PIC X.                                          00001870
           02 MCFAMP    PIC X.                                          00001880
           02 MCFAMH    PIC X.                                          00001890
           02 MCFAMV    PIC X.                                          00001900
           02 MCFAMO    PIC X(5).                                       00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MDATDEBA  PIC X.                                          00001930
           02 MDATDEBC  PIC X.                                          00001940
           02 MDATDEBP  PIC X.                                          00001950
           02 MDATDEBH  PIC X.                                          00001960
           02 MDATDEBV  PIC X.                                          00001970
           02 MDATDEBO  PIC X(8).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MDATFINA  PIC X.                                          00002000
           02 MDATFINC  PIC X.                                          00002010
           02 MDATFINP  PIC X.                                          00002020
           02 MDATFINH  PIC X.                                          00002030
           02 MDATFINV  PIC X.                                          00002040
           02 MDATFINO  PIC X(8).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MWSOLDEA  PIC X.                                          00002070
           02 MWSOLDEC  PIC X.                                          00002080
           02 MWSOLDEP  PIC X.                                          00002090
           02 MWSOLDEH  PIC X.                                          00002100
           02 MWSOLDEV  PIC X.                                          00002110
           02 MWSOLDEO  PIC X.                                          00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MLIGNE1A  PIC X.                                          00002140
           02 MLIGNE1C  PIC X.                                          00002150
           02 MLIGNE1P  PIC X.                                          00002160
           02 MLIGNE1H  PIC X.                                          00002170
           02 MLIGNE1V  PIC X.                                          00002180
           02 MLIGNE1O  PIC X(57).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLIGNE21A      PIC X.                                     00002210
           02 MLIGNE21C PIC X.                                          00002220
           02 MLIGNE21P PIC X.                                          00002230
           02 MLIGNE21H PIC X.                                          00002240
           02 MLIGNE21V PIC X.                                          00002250
           02 MLIGNE21O      PIC X.                                     00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MLIGNE22A      PIC X.                                     00002280
           02 MLIGNE22C PIC X.                                          00002290
           02 MLIGNE22P PIC X.                                          00002300
           02 MLIGNE22H PIC X.                                          00002310
           02 MLIGNE22V PIC X.                                          00002320
           02 MLIGNE22O      PIC X(3).                                  00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MLIGNE31A      PIC X.                                     00002350
           02 MLIGNE31C PIC X.                                          00002360
           02 MLIGNE31P PIC X.                                          00002370
           02 MLIGNE31H PIC X.                                          00002380
           02 MLIGNE31V PIC X.                                          00002390
           02 MLIGNE31O      PIC X.                                     00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MLIGNE32A      PIC X.                                     00002420
           02 MLIGNE32C PIC X.                                          00002430
           02 MLIGNE32P PIC X.                                          00002440
           02 MLIGNE32H PIC X.                                          00002450
           02 MLIGNE32V PIC X.                                          00002460
           02 MLIGNE32O      PIC X(3).                                  00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MLIGNE4A  PIC X.                                          00002490
           02 MLIGNE4C  PIC X.                                          00002500
           02 MLIGNE4P  PIC X.                                          00002510
           02 MLIGNE4H  PIC X.                                          00002520
           02 MLIGNE4V  PIC X.                                          00002530
           02 MLIGNE4O  PIC X(57).                                      00002540
           02 M197O OCCURS   13 TIMES .                                 00002550
             03 FILLER       PIC X(2).                                  00002560
             03 MSTATUTA     PIC X.                                     00002570
             03 MSTATUTC     PIC X.                                     00002580
             03 MSTATUTP     PIC X.                                     00002590
             03 MSTATUTH     PIC X.                                     00002600
             03 MSTATUTV     PIC X.                                     00002610
             03 MSTATUTO     PIC X(3).                                  00002620
             03 FILLER       PIC X(2).                                  00002630
             03 MNCDEA  PIC X.                                          00002640
             03 MNCDEC  PIC X.                                          00002650
             03 MNCDEP  PIC X.                                          00002660
             03 MNCDEH  PIC X.                                          00002670
             03 MNCDEV  PIC X.                                          00002680
             03 MNCDEO  PIC X(7).                                       00002690
             03 FILLER       PIC X(2).                                  00002700
             03 MDATSAIA     PIC X.                                     00002710
             03 MDATSAIC     PIC X.                                     00002720
             03 MDATSAIP     PIC X.                                     00002730
             03 MDATSAIH     PIC X.                                     00002740
             03 MDATSAIV     PIC X.                                     00002750
             03 MDATSAIO     PIC X(8).                                  00002760
             03 FILLER       PIC X(2).                                  00002770
             03 MDATVALA     PIC X.                                     00002780
             03 MDATVALC     PIC X.                                     00002790
             03 MDATVALP     PIC X.                                     00002800
             03 MDATVALH     PIC X.                                     00002810
             03 MDATVALV     PIC X.                                     00002820
             03 MDATVALO     PIC X(8).                                  00002830
             03 FILLER       PIC X(2).                                  00002840
             03 MNDEPOTA     PIC X.                                     00002850
             03 MNDEPOTC     PIC X.                                     00002860
             03 MNDEPOTP     PIC X.                                     00002870
             03 MNDEPOTH     PIC X.                                     00002880
             03 MNDEPOTV     PIC X.                                     00002890
             03 MNDEPOTO     PIC X(3).                                  00002900
             03 FILLER       PIC X(2).                                  00002910
             03 MQTECOMA     PIC X.                                     00002920
             03 MQTECOMC     PIC X.                                     00002930
             03 MQTECOMP     PIC X.                                     00002940
             03 MQTECOMH     PIC X.                                     00002950
             03 MQTECOMV     PIC X.                                     00002960
             03 MQTECOMO     PIC X(5).                                  00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MQTERECA     PIC X.                                     00002990
             03 MQTERECC     PIC X.                                     00003000
             03 MQTERECP     PIC X.                                     00003010
             03 MQTERECH     PIC X.                                     00003020
             03 MQTERECV     PIC X.                                     00003030
             03 MQTERECO     PIC X(5).                                  00003040
             03 FILLER       PIC X(2).                                  00003050
             03 MQTESOLA     PIC X.                                     00003060
             03 MQTESOLC     PIC X.                                     00003070
             03 MQTESOLP     PIC X.                                     00003080
             03 MQTESOLH     PIC X.                                     00003090
             03 MQTESOLV     PIC X.                                     00003100
             03 MQTESOLO     PIC X(5).                                  00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MZONCMDA  PIC X.                                          00003130
           02 MZONCMDC  PIC X.                                          00003140
           02 MZONCMDP  PIC X.                                          00003150
           02 MZONCMDH  PIC X.                                          00003160
           02 MZONCMDV  PIC X.                                          00003170
           02 MZONCMDO  PIC X(15).                                      00003180
           02 FILLER    PIC X(2).                                       00003190
           02 MLIBERRA  PIC X.                                          00003200
           02 MLIBERRC  PIC X.                                          00003210
           02 MLIBERRP  PIC X.                                          00003220
           02 MLIBERRH  PIC X.                                          00003230
           02 MLIBERRV  PIC X.                                          00003240
           02 MLIBERRO  PIC X(58).                                      00003250
           02 FILLER    PIC X(2).                                       00003260
           02 MCODTRAA  PIC X.                                          00003270
           02 MCODTRAC  PIC X.                                          00003280
           02 MCODTRAP  PIC X.                                          00003290
           02 MCODTRAH  PIC X.                                          00003300
           02 MCODTRAV  PIC X.                                          00003310
           02 MCODTRAO  PIC X(4).                                       00003320
           02 FILLER    PIC X(2).                                       00003330
           02 MCICSA    PIC X.                                          00003340
           02 MCICSC    PIC X.                                          00003350
           02 MCICSP    PIC X.                                          00003360
           02 MCICSH    PIC X.                                          00003370
           02 MCICSV    PIC X.                                          00003380
           02 MCICSO    PIC X(5).                                       00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MNETNAMA  PIC X.                                          00003410
           02 MNETNAMC  PIC X.                                          00003420
           02 MNETNAMP  PIC X.                                          00003430
           02 MNETNAMH  PIC X.                                          00003440
           02 MNETNAMV  PIC X.                                          00003450
           02 MNETNAMO  PIC X(8).                                       00003460
           02 FILLER    PIC X(2).                                       00003470
           02 MSCREENA  PIC X.                                          00003480
           02 MSCREENC  PIC X.                                          00003490
           02 MSCREENP  PIC X.                                          00003500
           02 MSCREENH  PIC X.                                          00003510
           02 MSCREENV  PIC X.                                          00003520
           02 MSCREENO  PIC X(4).                                       00003530
                                                                                
