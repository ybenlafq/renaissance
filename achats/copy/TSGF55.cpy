      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      * TS TGF55 : LISTE CODE RAISON                                            
      *                                                                         
       01  TS-GF55.                                                             
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03  TS-GF55-LONG             PIC  S9(4) COMP  VALUE +660.            
      *--                                                                       
           03  TS-GF55-LONG             PIC  S9(4) COMP-5  VALUE +660.          
      *}                                                                        
      *                                                                         
           03  TS-GF55-DONNEES.                                                 
      *                                                                         
             05 TS-GF55-TABLEAU           OCCURS 10.                            
                   10 TS-GF55-CREAS        PIC X(5).                            
                   10 TS-GF55-LREAS        PIC X(60).                           
                   10 TS-GF55-SELECT       PIC X(1).                            
      *                                                                         
                                                                                
