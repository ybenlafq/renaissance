      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR40   EGR40                                              00000020
      ***************************************************************** 00000030
       01   EGR40I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * PAGE                                                            00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MNPAGEI   PIC X(3).                                       00000200
      * SOCIETE                                                         00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCLIVRL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCLIVRL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCLIVRF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCLIVRI     PIC X(3).                                  00000250
      * ENTREPOT                                                        00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MNDEPOTI  PIC X(3).                                       00000300
      * NO RECEPTION                                                    00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRECL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MNRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNRECF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MNRECI    PIC X(7).                                       00000350
      * DATE RECEPTION A QUAI                                           00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDJRECQUAIL    COMP PIC S9(4).                            00000370
      *--                                                                       
           02 MDJRECQUAIL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDJRECQUAIF    PIC X.                                     00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MDJRECQUAII    PIC X(8).                                  00000400
      * CODIC                                                           00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNCODICI  PIC X(7).                                       00000450
      * LIBELLE CODIC                                                   00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODICL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MLCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCODICF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MLCODICI  PIC X(20).                                      00000500
      * DATE SAISIE RECEPTION                                           00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSAISRECL     COMP PIC S9(4).                            00000520
      *--                                                                       
           02 MDSAISRECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDSAISRECF     PIC X.                                     00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MDSAISRECI     PIC X(8).                                  00000550
      * FAMILLE                                                         00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MCFAMI    PIC X(5).                                       00000600
      * LIBELLE FAMILLE                                                 00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLFAMI    PIC X(20).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBL     COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLIBL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MLIBF     PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLIBI     PIC X(16).                                      00000690
      * NO RECEPTION QUAI                                               00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRECQUAIL     COMP PIC S9(4).                            00000710
      *--                                                                       
           02 MNRECQUAIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNRECQUAIF     PIC X.                                     00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MNRECQUAII     PIC X(8).                                  00000740
      * MARQUE                                                          00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MCMARQI   PIC X(5).                                       00000790
      * LIBELLE MARQUE                                                  00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MLMARQI   PIC X(20).                                      00000840
      * QTE SAISIE RECEPTIONNEE ARTICLE                                 00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQRECSAIL      COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MQRECSAIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQRECSAIF      PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MQRECSAII      PIC X(5).                                  00000890
      * QTE MODIF RECEPTIONNEE ARTICLE                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQRECMODL      COMP PIC S9(4).                            00000910
      *--                                                                       
           02 MQRECMODL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQRECMODF      PIC X.                                     00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MQRECMODI      PIC X(5).                                  00000940
           02 M93I OCCURS   6 TIMES .                                   00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMSAIL    COMP PIC S9(4).                            00000960
      *--                                                                       
             03 MCOMMSAIL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCOMMSAIF    PIC X.                                     00000970
             03 FILLER  PIC X(4).                                       00000980
             03 MCOMMSAII    PIC X(7).                                  00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBLSAIL     COMP PIC S9(4).                            00001000
      *--                                                                       
             03 MNBLSAIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNBLSAIF     PIC X.                                     00001010
             03 FILLER  PIC X(4).                                       00001020
             03 MNBLSAII     PIC X(10).                                 00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCDESAIL    COMP PIC S9(4).                            00001040
      *--                                                                       
             03 MQCDESAIL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQCDESAIF    PIC X.                                     00001050
             03 FILLER  PIC X(4).                                       00001060
             03 MQCDESAII    PIC X(5).                                  00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMMODL    COMP PIC S9(4).                            00001080
      *--                                                                       
             03 MCOMMMODL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCOMMMODF    PIC X.                                     00001090
             03 FILLER  PIC X(4).                                       00001100
             03 MCOMMMODI    PIC X(7).                                  00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBLMODL     COMP PIC S9(4).                            00001120
      *--                                                                       
             03 MNBLMODL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNBLMODF     PIC X.                                     00001130
             03 FILLER  PIC X(4).                                       00001140
             03 MNBLMODI     PIC X(10).                                 00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCDEMODL    COMP PIC S9(4).                            00001160
      *--                                                                       
             03 MQCDEMODL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQCDEMODF    PIC X.                                     00001170
             03 FILLER  PIC X(4).                                       00001180
             03 MQCDEMODI    PIC X(5).                                  00001190
      * QTE SAISIE LITIG                                                00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQLITSAIL      COMP PIC S9(4).                            00001210
      *--                                                                       
           02 MQLITSAIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQLITSAIF      PIC X.                                     00001220
           02 FILLER    PIC X(4).                                       00001230
           02 MQLITSAII      PIC X(5).                                  00001240
      * QTE MODIF LITIGE                                                00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQLITMODL      COMP PIC S9(4).                            00001260
      *--                                                                       
           02 MQLITMODL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQLITMODF      PIC X.                                     00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MQLITMODI      PIC X(5).                                  00001290
      * ZONE CMD AIDA                                                   00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MZONCMDI  PIC X(15).                                      00001340
      * MESSAGE ERREUR                                                  00001350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001360
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001370
           02 FILLER    PIC X(4).                                       00001380
           02 MLIBERRI  PIC X(58).                                      00001390
      * CODE TRANSACTION                                                00001400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001410
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001420
           02 FILLER    PIC X(4).                                       00001430
           02 MCODTRAI  PIC X(4).                                       00001440
      * CICS DE TRAVAIL                                                 00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MCICSI    PIC X(5).                                       00001490
      * NETNAME                                                         00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MNETNAMI  PIC X(8).                                       00001540
      * CODE TERMINAL                                                   00001550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001560
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001570
           02 FILLER    PIC X(4).                                       00001580
           02 MSCREENI  PIC X(5).                                       00001590
      ***************************************************************** 00001600
      * SDF: EGR40   EGR40                                              00001610
      ***************************************************************** 00001620
       01   EGR40O REDEFINES EGR40I.                                    00001630
           02 FILLER    PIC X(12).                                      00001640
      * DATE DU JOUR                                                    00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MDATJOUA  PIC X.                                          00001670
           02 MDATJOUC  PIC X.                                          00001680
           02 MDATJOUP  PIC X.                                          00001690
           02 MDATJOUH  PIC X.                                          00001700
           02 MDATJOUV  PIC X.                                          00001710
           02 MDATJOUO  PIC X(10).                                      00001720
      * HEURE                                                           00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MTIMJOUA  PIC X.                                          00001750
           02 MTIMJOUC  PIC X.                                          00001760
           02 MTIMJOUP  PIC X.                                          00001770
           02 MTIMJOUH  PIC X.                                          00001780
           02 MTIMJOUV  PIC X.                                          00001790
           02 MTIMJOUO  PIC X(5).                                       00001800
      * PAGE                                                            00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MNPAGEA   PIC X.                                          00001830
           02 MNPAGEC   PIC X.                                          00001840
           02 MNPAGEP   PIC X.                                          00001850
           02 MNPAGEH   PIC X.                                          00001860
           02 MNPAGEV   PIC X.                                          00001870
           02 MNPAGEO   PIC X(3).                                       00001880
      * SOCIETE                                                         00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MNSOCLIVRA     PIC X.                                     00001910
           02 MNSOCLIVRC     PIC X.                                     00001920
           02 MNSOCLIVRP     PIC X.                                     00001930
           02 MNSOCLIVRH     PIC X.                                     00001940
           02 MNSOCLIVRV     PIC X.                                     00001950
           02 MNSOCLIVRO     PIC X(3).                                  00001960
      * ENTREPOT                                                        00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MNDEPOTA  PIC X.                                          00001990
           02 MNDEPOTC  PIC X.                                          00002000
           02 MNDEPOTP  PIC X.                                          00002010
           02 MNDEPOTH  PIC X.                                          00002020
           02 MNDEPOTV  PIC X.                                          00002030
           02 MNDEPOTO  PIC X(3).                                       00002040
      * NO RECEPTION                                                    00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MNRECA    PIC X.                                          00002070
           02 MNRECC    PIC X.                                          00002080
           02 MNRECP    PIC X.                                          00002090
           02 MNRECH    PIC X.                                          00002100
           02 MNRECV    PIC X.                                          00002110
           02 MNRECO    PIC X(7).                                       00002120
      * DATE RECEPTION A QUAI                                           00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MDJRECQUAIA    PIC X.                                     00002150
           02 MDJRECQUAIC    PIC X.                                     00002160
           02 MDJRECQUAIP    PIC X.                                     00002170
           02 MDJRECQUAIH    PIC X.                                     00002180
           02 MDJRECQUAIV    PIC X.                                     00002190
           02 MDJRECQUAIO    PIC X(8).                                  00002200
      * CODIC                                                           00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MNCODICA  PIC X.                                          00002230
           02 MNCODICC  PIC X.                                          00002240
           02 MNCODICP  PIC X.                                          00002250
           02 MNCODICH  PIC X.                                          00002260
           02 MNCODICV  PIC X.                                          00002270
           02 MNCODICO  PIC X(7).                                       00002280
      * LIBELLE CODIC                                                   00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MLCODICA  PIC X.                                          00002310
           02 MLCODICC  PIC X.                                          00002320
           02 MLCODICP  PIC X.                                          00002330
           02 MLCODICH  PIC X.                                          00002340
           02 MLCODICV  PIC X.                                          00002350
           02 MLCODICO  PIC X(20).                                      00002360
      * DATE SAISIE RECEPTION                                           00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MDSAISRECA     PIC X.                                     00002390
           02 MDSAISRECC     PIC X.                                     00002400
           02 MDSAISRECP     PIC X.                                     00002410
           02 MDSAISRECH     PIC X.                                     00002420
           02 MDSAISRECV     PIC X.                                     00002430
           02 MDSAISRECO     PIC X(8).                                  00002440
      * FAMILLE                                                         00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MCFAMA    PIC X.                                          00002470
           02 MCFAMC    PIC X.                                          00002480
           02 MCFAMP    PIC X.                                          00002490
           02 MCFAMH    PIC X.                                          00002500
           02 MCFAMV    PIC X.                                          00002510
           02 MCFAMO    PIC X(5).                                       00002520
      * LIBELLE FAMILLE                                                 00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MLFAMA    PIC X.                                          00002550
           02 MLFAMC    PIC X.                                          00002560
           02 MLFAMP    PIC X.                                          00002570
           02 MLFAMH    PIC X.                                          00002580
           02 MLFAMV    PIC X.                                          00002590
           02 MLFAMO    PIC X(20).                                      00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MLIBA     PIC X.                                          00002620
           02 MLIBC     PIC X.                                          00002630
           02 MLIBP     PIC X.                                          00002640
           02 MLIBH     PIC X.                                          00002650
           02 MLIBV     PIC X.                                          00002660
           02 MLIBO     PIC X(16).                                      00002670
      * NO RECEPTION QUAI                                               00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MNRECQUAIA     PIC X.                                     00002700
           02 MNRECQUAIC     PIC X.                                     00002710
           02 MNRECQUAIP     PIC X.                                     00002720
           02 MNRECQUAIH     PIC X.                                     00002730
           02 MNRECQUAIV     PIC X.                                     00002740
           02 MNRECQUAIO     PIC X(8).                                  00002750
      * MARQUE                                                          00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MCMARQA   PIC X.                                          00002780
           02 MCMARQC   PIC X.                                          00002790
           02 MCMARQP   PIC X.                                          00002800
           02 MCMARQH   PIC X.                                          00002810
           02 MCMARQV   PIC X.                                          00002820
           02 MCMARQO   PIC X(5).                                       00002830
      * LIBELLE MARQUE                                                  00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MLMARQA   PIC X.                                          00002860
           02 MLMARQC   PIC X.                                          00002870
           02 MLMARQP   PIC X.                                          00002880
           02 MLMARQH   PIC X.                                          00002890
           02 MLMARQV   PIC X.                                          00002900
           02 MLMARQO   PIC X(20).                                      00002910
      * QTE SAISIE RECEPTIONNEE ARTICLE                                 00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MQRECSAIA      PIC X.                                     00002940
           02 MQRECSAIC PIC X.                                          00002950
           02 MQRECSAIP PIC X.                                          00002960
           02 MQRECSAIH PIC X.                                          00002970
           02 MQRECSAIV PIC X.                                          00002980
           02 MQRECSAIO      PIC X(5).                                  00002990
      * QTE MODIF RECEPTIONNEE ARTICLE                                  00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MQRECMODA      PIC X.                                     00003020
           02 MQRECMODC PIC X.                                          00003030
           02 MQRECMODP PIC X.                                          00003040
           02 MQRECMODH PIC X.                                          00003050
           02 MQRECMODV PIC X.                                          00003060
           02 MQRECMODO      PIC X(5).                                  00003070
           02 M93O OCCURS   6 TIMES .                                   00003080
             03 FILLER       PIC X(2).                                  00003090
             03 MCOMMSAIA    PIC X.                                     00003100
             03 MCOMMSAIC    PIC X.                                     00003110
             03 MCOMMSAIP    PIC X.                                     00003120
             03 MCOMMSAIH    PIC X.                                     00003130
             03 MCOMMSAIV    PIC X.                                     00003140
             03 MCOMMSAIO    PIC X(7).                                  00003150
             03 FILLER       PIC X(2).                                  00003160
             03 MNBLSAIA     PIC X.                                     00003170
             03 MNBLSAIC     PIC X.                                     00003180
             03 MNBLSAIP     PIC X.                                     00003190
             03 MNBLSAIH     PIC X.                                     00003200
             03 MNBLSAIV     PIC X.                                     00003210
             03 MNBLSAIO     PIC X(10).                                 00003220
             03 FILLER       PIC X(2).                                  00003230
             03 MQCDESAIA    PIC X.                                     00003240
             03 MQCDESAIC    PIC X.                                     00003250
             03 MQCDESAIP    PIC X.                                     00003260
             03 MQCDESAIH    PIC X.                                     00003270
             03 MQCDESAIV    PIC X.                                     00003280
             03 MQCDESAIO    PIC X(5).                                  00003290
             03 FILLER       PIC X(2).                                  00003300
             03 MCOMMMODA    PIC X.                                     00003310
             03 MCOMMMODC    PIC X.                                     00003320
             03 MCOMMMODP    PIC X.                                     00003330
             03 MCOMMMODH    PIC X.                                     00003340
             03 MCOMMMODV    PIC X.                                     00003350
             03 MCOMMMODO    PIC X(7).                                  00003360
             03 FILLER       PIC X(2).                                  00003370
             03 MNBLMODA     PIC X.                                     00003380
             03 MNBLMODC     PIC X.                                     00003390
             03 MNBLMODP     PIC X.                                     00003400
             03 MNBLMODH     PIC X.                                     00003410
             03 MNBLMODV     PIC X.                                     00003420
             03 MNBLMODO     PIC X(10).                                 00003430
             03 FILLER       PIC X(2).                                  00003440
             03 MQCDEMODA    PIC X.                                     00003450
             03 MQCDEMODC    PIC X.                                     00003460
             03 MQCDEMODP    PIC X.                                     00003470
             03 MQCDEMODH    PIC X.                                     00003480
             03 MQCDEMODV    PIC X.                                     00003490
             03 MQCDEMODO    PIC X(5).                                  00003500
      * QTE SAISIE LITIG                                                00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MQLITSAIA      PIC X.                                     00003530
           02 MQLITSAIC PIC X.                                          00003540
           02 MQLITSAIP PIC X.                                          00003550
           02 MQLITSAIH PIC X.                                          00003560
           02 MQLITSAIV PIC X.                                          00003570
           02 MQLITSAIO      PIC X(5).                                  00003580
      * QTE MODIF LITIGE                                                00003590
           02 FILLER    PIC X(2).                                       00003600
           02 MQLITMODA      PIC X.                                     00003610
           02 MQLITMODC PIC X.                                          00003620
           02 MQLITMODP PIC X.                                          00003630
           02 MQLITMODH PIC X.                                          00003640
           02 MQLITMODV PIC X.                                          00003650
           02 MQLITMODO      PIC X(5).                                  00003660
      * ZONE CMD AIDA                                                   00003670
           02 FILLER    PIC X(2).                                       00003680
           02 MZONCMDA  PIC X.                                          00003690
           02 MZONCMDC  PIC X.                                          00003700
           02 MZONCMDP  PIC X.                                          00003710
           02 MZONCMDH  PIC X.                                          00003720
           02 MZONCMDV  PIC X.                                          00003730
           02 MZONCMDO  PIC X(15).                                      00003740
      * MESSAGE ERREUR                                                  00003750
           02 FILLER    PIC X(2).                                       00003760
           02 MLIBERRA  PIC X.                                          00003770
           02 MLIBERRC  PIC X.                                          00003780
           02 MLIBERRP  PIC X.                                          00003790
           02 MLIBERRH  PIC X.                                          00003800
           02 MLIBERRV  PIC X.                                          00003810
           02 MLIBERRO  PIC X(58).                                      00003820
      * CODE TRANSACTION                                                00003830
           02 FILLER    PIC X(2).                                       00003840
           02 MCODTRAA  PIC X.                                          00003850
           02 MCODTRAC  PIC X.                                          00003860
           02 MCODTRAP  PIC X.                                          00003870
           02 MCODTRAH  PIC X.                                          00003880
           02 MCODTRAV  PIC X.                                          00003890
           02 MCODTRAO  PIC X(4).                                       00003900
      * CICS DE TRAVAIL                                                 00003910
           02 FILLER    PIC X(2).                                       00003920
           02 MCICSA    PIC X.                                          00003930
           02 MCICSC    PIC X.                                          00003940
           02 MCICSP    PIC X.                                          00003950
           02 MCICSH    PIC X.                                          00003960
           02 MCICSV    PIC X.                                          00003970
           02 MCICSO    PIC X(5).                                       00003980
      * NETNAME                                                         00003990
           02 FILLER    PIC X(2).                                       00004000
           02 MNETNAMA  PIC X.                                          00004010
           02 MNETNAMC  PIC X.                                          00004020
           02 MNETNAMP  PIC X.                                          00004030
           02 MNETNAMH  PIC X.                                          00004040
           02 MNETNAMV  PIC X.                                          00004050
           02 MNETNAMO  PIC X(8).                                       00004060
      * CODE TERMINAL                                                   00004070
           02 FILLER    PIC X(2).                                       00004080
           02 MSCREENA  PIC X.                                          00004090
           02 MSCREENC  PIC X.                                          00004100
           02 MSCREENP  PIC X.                                          00004110
           02 MSCREENH  PIC X.                                          00004120
           02 MSCREENV  PIC X.                                          00004130
           02 MSCREENO  PIC X(5).                                       00004140
                                                                                
