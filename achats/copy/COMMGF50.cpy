      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * ------------------------------------------------------------ *          
      * ZONE DE COMMUNICATION POUR APPEL MODULE CROSS DOCK           *          
      *                                                              *          
      * ------------------------------------------------------------ *          
       01 COMMAREA-GF50.                                                        
      * - PROGRAMME APPELANT.                                                   
          02 CO-GF50-CPROG             PIC X(06).                               
      * - DATE DU JOUR.                                                         
          02 CO-GF50-SSAAMMJJ          PIC X(08).                               
      * - SOC LIEU MODIF                                                        
          02 CO-GF50-NSOCMODIF         PIC X(03).                               
          02 CO-GF50-NLIEUMODIF        PIC X(03).                               
          02 CO-GF50-ORIGINE           PIC X(01) VALUE ' '.                     
             88 CO-GF50-VENTE                    VALUE 'V'.                     
             88 CO-GF50-AUTRE                    VALUE ' '.                     
      * - SOC LIEU VENTE                                                        
          02 CO-GF50-NSOCIETE          PIC X(03).                               
          02 CO-GF50-NLIEU             PIC X(03).                               
          02 CO-GF50-NVENTE            PIC X(07).                               
          02 CO-GF50-IDUSER            PIC X(08).                               
      * - CODE FONCTION PRINCIPAL INTERROGATION / COMMANDE                      
          02 CO-GF50-CFONCTION         PIC X(01).                               
             88 CO-GF50-INTERRO                    VALUE 'I'.                   
             88 CO-GF50-COMMANDE                   VALUE 'C'.                   
      * - RETOUR / ARRET TRAITEMENT D�S PREMI�RE ERREUR.                        
          02 CO-GF50-OUT.                                                       
      * - NATURE ERREUR BLOQUANTE, NON BLOQUANTE                                
             03 CO-GF50-CRETOUR.                                                
                04 CO-GF50-CRET           PIC X(04).                            
                04 CO-GF50-LRET           PIC X(58).                            
             03 CO-GF50-NATURE-ERREUR PIC 9(01) VALUE 2.                        
                88 CO-GF50-ERR-BLOQUANTE           VALUE 0.                     
                88 CO-GF50-ERR-NON-BLOQUANTE       VALUE 1.                     
                88 CO-GF50-PAS-D-ERREUR            VALUE 2.                     
      * ------------------------------------------------------------- *         
      * ZONE DE COMMUNICATION COMMANDE                                *         
      * ------------------------------------------------------------- *         
      * - INDEX MAX POUR TABLEAU COMMANDE                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 CO-GF50-IDXMAX            PIC S9(02) COMP VALUE 40.                
      *--                                                                       
          02 CO-GF50-IDXMAX            PIC S9(02) COMP-5 VALUE 40.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 CO-GF50-IDX               PIC S9(02) COMP VALUE 0.                 
      *--                                                                       
          02 CO-GF50-IDX               PIC S9(02) COMP-5 VALUE 0.               
      *}                                                                        
          02 CO-GF50-COMMANDES.                                                 
             03 FILLER OCCURS 40.                                               
      * -                                      ACTION SUR LIGNE DE VENTE.       
                04 CO-GF50-ACTION              PIC X(02).                       
      *{ remove-comma-in-dde 1.5                                                
      *            88 CO-GF50-CREATION             VALUE 'AJ' , ' '.            
      *--                                                                       
                   88 CO-GF50-CREATION             VALUE 'AJ'   ' '.            
      *}                                                                        
                   88 CO-GF50-ANNULATION           VALUE 'AN'.                  
                   88 CO-GF50-MODIFICATION         VALUE 'MO'.                  
      *                                        LIEU ENTREP�T.                   
                04 CO-GF50-NSOCDEP             PIC X(03).                       
                04 CO-GF50-NLIEUDEP            PIC X(03).                       
                04 CO-GF50-NMUTATION           PIC X(07).                       
      * - VALEURS COURANTES DONN�ES PAR LA VENTE.                               
                04 CO-GF50-VALEURS-ACTUELLES.                                   
      * -                                      LIEU DESTINATAIRE.               
                   05 CO-GF50-NSOCD               PIC X(03).                    
                   05 CO-GF50-NLIEUD              PIC X(03).                    
      *   -                               CODIC � COMMANDER OU ANNULER.         
                   05 CO-GF50-NCODIC           PIC X(07).                       
      *   -                               ID LIGNE DE VENTEE OU ANNULER.        
                   05 CO-GF50-NSEQ             PIC X(2).                        
      *   -                               QTE � COMMANDER.                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            05 CO-GF50-QVENDUE          PIC S9(06) COMP.                 
      *--                                                                       
                   05 CO-GF50-QVENDUE          PIC S9(06) COMP-5.               
      *}                                                                        
      *   -                               DATE DELIVRANCE.                      
                   05 CO-GF50-DDELIV           PIC X(08).                       
      *   -                               DATE DE D�STOCKAGE                    
                   05 CO-GF50-DDESTOCK         PIC X(08).                       
      * - VALEURS PRECEDENTES DONN�ES PAR LA VENTE.                             
                04 CO-GF50-VALEURS-PRECEDENTES.                                 
      *   -                               QTE � COMMANDER INITIALE.             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            05 CO-GF50-QVENDUE-I        PIC S9(06) COMP.                 
      *--                                                                       
                   05 CO-GF50-QVENDUE-I        PIC S9(06) COMP-5.               
      *}                                                                        
      *   -                               DATE DELIVRANCE INITIALE.             
                   05 CO-GF50-DDELIV-I         PIC X(08).                       
      *   -                               LIEU DESTINATAIRE INITIALE.           
                   05 CO-GF50-NSOCD-I          PIC X(03).                       
                   05 CO-GF50-NLIEUD-I         PIC X(03).                       
      *   -                                NUMERO DE COMMANDE INITIAL.          
                   05 CO-GF50-CDE-CROSS-DOCK-I PIC X(07).                       
      * ------------------------------------------------------------- *         
      * ZONE RETOUR MODULE DE COMMANDE                                *         
      * ------------------------------------------------------------- *         
      * - DATE DE DISPONIBILIT� ENTREPOT DONN�E PAR LE MODULE                   
                04 CO-GF50-DDISPO              PIC X(08).                       
      * - NUM�RO DE LA COMMANDE D�TERMIN� PAR LE MODULE.                        
                04 CO-GF50-CROSS-DOCK          PIC X(07).                       
      * - CODE RETOUR AU NIVEAU DE LA LIGNE                                     
                04 CO-GF50-CRETOURD.                                            
                   05 CO-GF50-CRETD            PIC X(04).                       
                   05 CO-GF50-LRETD            PIC X(58).                       
                04 CO-GF50-ITEM                PIC 9(02).                       
                                                                                
