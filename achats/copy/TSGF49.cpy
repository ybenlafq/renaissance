      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00010000
      * TS SPECIFIQUE PRG TGF49                                         00020000
      **************************************************************    00040000
          01 TS-GF49-DONNEES.                                           00063000
             05 TS-GF49-WFLAG                  PIC X.                   00132006
             05 TS-GF49-CPHARE                 PIC X(3).                00140002
             05 TS-GF49-WACTIF                 PIC X.                   00150000
             05 TS-GF49-DACTIF                 PIC X(8).                00170000
             05 TS-GF49-DACTIF-JMA             PIC X(8).                00171007
             05 TS-GF49-DPHARE                 PIC X(8).                00180000
             05 TS-GF49-LPHARE                 PIC X(20).               00190001
             05 TS-GF49-CPHARE-OLD             PIC X(3).                00191005
             05 TS-GF49-DACTIF-OLD             PIC X(8).                00193005
                                                                                
