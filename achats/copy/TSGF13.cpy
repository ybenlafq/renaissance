      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : TS LIGNES DE COMMANDE PAR DEMANDEUR CODIC              *         
      *        CREE PAR TGF13                                         *         
      *****************************************************************         
      *                                                               *         
       01  TS-GF13.                                                             
      *--------------------------------  LONGUEUR TS                            
           02 TS-GF13-LONG                    PIC S9(5) COMP-3                  
                                              VALUE +108.                       
           02 TS-GF13-DONNEES.                                                  
             03 TS-GF13-GF05.                                                   
      *--------------------------------  TABLE LIGNE COMMANDE                   
      *--------------------------------  DEMANDEUR                              
               04 TS-GF13-NSOCIETE            PIC X(3).                         
               04 TS-GF13-NLIEU               PIC X(3).                         
               04 TS-GF13-ORIG-DEMANDE        PIC X(1).                         
      *--------------------------------  CODIC                                  
               04 TS-GF13-NCODIC              PIC X(7).                         
      *--------------------------------  QTE GLOBALE PAR                        
      *                                  DEMANDEUR CODIC                        
               04 TS-GF13-QTETOT              PIC 9(5) COMP-3.                  
      *--------------------------------  QTE PAR DEMANDEUR CODIC                
      *                                  ET PAR DATE                            
               04 TS-GF13-TAB-QTE             OCCURS 4.                         
                 05 TS-GF13-QTE               PIC 9(5) COMP-3.                  
                 05 TS-GF13-DATE              PIC X(8).                         
             03 TS-GF13-GA00.                                                   
      *--------------------------------  TABLE ARTICLE                          
               04 TS-GF13-CFAM                PIC X(5).                         
               04 TS-GF13-CMARQ               PIC X(5).                         
               04 TS-GF13-LREFFOURN           PIC X(20).                        
               04 TS-GF13-CMODSTOCK           PIC X(5).                         
               04 TS-GF13-QCOLIRECEPT         PIC 9(5) COMP-3.                  
               04 TS-GF13-QDELAIAPPRO         PIC 9(3).                         
                                                                                
