      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ                                                       
      *****************************************************************         
      *                                                                         
       01 WS72-MQ10.                                                            
          02 WS72-QUEUE.                                                        
             10 MQ10-CORRELID-B            PIC X(24).                           
          02 WS72-CODRET                     PIC X(02).                         
          02 WS72-MESSAGE.                                                      
             05 MESS72-ENTETE.                                                  
                10 MES72-TYPE                PIC X(03).                         
                10 MES72-NSOCMSG             PIC X(03).                         
                10 MES72-NLIEUMSG            PIC X(03).                         
                10 MES72-NSOCDST             PIC X(03).                         
                10 MES72-NLIEUDST            PIC X(03).                         
                10 MES72-NORD                PIC 9(08).                         
                10 MES72-LPROG               PIC X(10).                         
                10 MES72-DJOUR               PIC X(08).                         
                10 MES72-WSID                PIC X(10).                         
                10 MES72-USER                PIC X(10).                         
                10 MES72-CHRONO              PIC 9(07).                         
                10 MES72-NBRMSG              PIC 9(07).                         
                10 MES72-NBRENR              PIC 9(05).                         
                10 MES72-TAILLE              PIC 9(05).                         
                10 MES72-FILLER              PIC X(20).                         
      * MES-ENTETE .................................                            
             05 MES72-ENTETE.                                                   
                10 MES72-RETOUR1             PIC X(01).                         
                10 MES72-TAG1                PIC X(03).                         
                10 MES72-NCDE                PIC X(07).                         
                10 MES72-NENTCDE             PIC X(05).                         
                10 MES72-RETOUR2             PIC X(01).                         
      * MES-DETAIL .................................                            
             05 MES72-DETAIL           OCCURS 99.                               
                10 MES72-TAG2                PIC X(03).                         
                10 MES72-NCODIC              PIC X(07).                         
                10 MES72-NSOCDEPOT           PIC X(06).                         
                10 MES72-LREFFOURN           PIC X(20).                         
                10 MES72-NEAN                PIC X(13).                         
                10 MES72-QCDETOT             PIC 9(05).                         
                10 MES72-QRECTOT             PIC 9(05).                         
                10 MES72-QCDENCAL            PIC 9(05).                         
                10 MES72-QCDECAL             PIC 9(05).                         
                10 MES72-DLIVRAISON          PIC X(08).                         
                10 MES72-PBF                 PIC 9(7)V9(2).                     
                10 MES72-PRA                 PIC 9(7)V9(2).                     
                10 MES72-RETOUR3             PIC X(01).                         
                                                                                
