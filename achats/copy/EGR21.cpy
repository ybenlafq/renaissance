      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR21   EGR21                                              00000020
      ***************************************************************** 00000030
       01   EGR21I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(4).                                       00000190
      * PAGE                                                            00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000210
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000220
           02 FILLER    PIC X(4).                                       00000230
           02 MNBPAGESI      PIC X(4).                                  00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEPOTL  COMP PIC S9(4).                                 00000250
      *--                                                                       
           02 MLDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLDEPOTF  PIC X.                                          00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MLDEPOTI  PIC X(20).                                      00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCETATL   COMP PIC S9(4).                                 00000290
      *--                                                                       
           02 MCETATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCETATF   PIC X.                                          00000300
           02 FILLER    PIC X(4).                                       00000310
           02 MCETATI   PIC X(20).                                      00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRECQUAIL     COMP PIC S9(4).                            00000330
      *--                                                                       
           02 MNRECQUAIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNRECQUAIF     PIC X.                                     00000340
           02 FILLER    PIC X(4).                                       00000350
           02 MNRECQUAII     PIC X(7).                                  00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNRECQORIL    COMP PIC S9(4).                            00000370
      *--                                                                       
           02 MLNRECQORIL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLNRECQORIF    PIC X.                                     00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MLNRECQORII    PIC X(17).                                 00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRECL    COMP PIC S9(4).                                 00000410
      *--                                                                       
           02 MNRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNRECF    PIC X.                                          00000420
           02 FILLER    PIC X(4).                                       00000430
           02 MNRECI    PIC X(7).                                       00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLQRECL   COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MLQRECL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLQRECF   PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MLQRECI   PIC X(16).                                      00000480
           02 MLIGNEI OCCURS   12 TIMES .                               00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNNCODICL    COMP PIC S9(4).                            00000500
      *--                                                                       
             03 MNNCODICL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNNCODICF    PIC X.                                     00000510
             03 FILLER  PIC X(4).                                       00000520
             03 MNNCODICI    PIC X(7).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLREFOL     COMP PIC S9(4).                            00000540
      *--                                                                       
             03 MNLREFOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLREFOF     PIC X.                                     00000550
             03 FILLER  PIC X(4).                                       00000560
             03 MNLREFOI     PIC X(19).                                 00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNNCDEL      COMP PIC S9(4).                            00000580
      *--                                                                       
             03 MNNCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNNCDEF      PIC X.                                     00000590
             03 FILLER  PIC X(4).                                       00000600
             03 MNNCDEI      PIC X(7).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNNENTCDEL   COMP PIC S9(4).                            00000620
      *--                                                                       
             03 MNNENTCDEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNNENTCDEF   PIC X.                                     00000630
             03 FILLER  PIC X(4).                                       00000640
             03 MNNENTCDEI   PIC X(5).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLENTCDEL   COMP PIC S9(4).                            00000660
      *--                                                                       
             03 MNLENTCDEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNLENTCDEF   PIC X.                                     00000670
             03 FILLER  PIC X(4).                                       00000680
             03 MNLENTCDEI   PIC X(20).                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNNBLL  COMP PIC S9(4).                                 00000700
      *--                                                                       
             03 MNNBLL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNNBLF  PIC X.                                          00000710
             03 FILLER  PIC X(4).                                       00000720
             03 MNNBLI  PIC X(10).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNQRECL      COMP PIC S9(4).                            00000740
      *--                                                                       
             03 MNQRECL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNQRECF      PIC X.                                     00000750
             03 FILLER  PIC X(4).                                       00000760
             03 MNQRECI      PIC X(5).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MZONCMDI  PIC X(7).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLIBERRI  PIC X(66).                                      00000850
      * CODE TRANSACTION                                                00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      * CICS DE TRAVAIL                                                 00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MCICSI    PIC X(5).                                       00000950
      * NETNAME                                                         00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MNETNAMI  PIC X(8).                                       00001000
      * CODE TERMINAL                                                   00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MSCREENI  PIC X(5).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPAGEL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MTYPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPAGEF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MTYPAGEI  PIC X(2).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPOTL    COMP PIC S9(4).                            00001100
      *--                                                                       
           02 MNSOCDEPOTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSOCDEPOTF    PIC X.                                     00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MNSOCDEPOTI    PIC X(3).                                  00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MNDEPOTI  PIC X(3).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDJRECQUAIL    COMP PIC S9(4).                            00001180
      *--                                                                       
           02 MDJRECQUAIL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDJRECQUAIF    PIC X.                                     00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MDJRECQUAII    PIC X(10).                                 00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSAISRECL     COMP PIC S9(4).                            00001220
      *--                                                                       
           02 MDSAISRECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDSAISRECF     PIC X.                                     00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MDSAISRECI     PIC X(10).                                 00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMENTL     COMP PIC S9(4).                            00001260
      *--                                                                       
           02 MLCOMMENTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCOMMENTF     PIC X.                                     00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MLCOMMENTI     PIC X(34).                                 00001290
      ***************************************************************** 00001300
      * SDF: EGR21   EGR21                                              00001310
      ***************************************************************** 00001320
       01   EGR21O REDEFINES EGR21I.                                    00001330
           02 FILLER    PIC X(12).                                      00001340
      * DATE DU JOUR                                                    00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MDATJOUA  PIC X.                                          00001370
           02 MDATJOUC  PIC X.                                          00001380
           02 MDATJOUP  PIC X.                                          00001390
           02 MDATJOUH  PIC X.                                          00001400
           02 MDATJOUV  PIC X.                                          00001410
           02 MDATJOUO  PIC X(10).                                      00001420
      * HEURE                                                           00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MTIMJOUA  PIC X.                                          00001450
           02 MTIMJOUC  PIC X.                                          00001460
           02 MTIMJOUP  PIC X.                                          00001470
           02 MTIMJOUH  PIC X.                                          00001480
           02 MTIMJOUV  PIC X.                                          00001490
           02 MTIMJOUO  PIC X(5).                                       00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MPAGEA    PIC X.                                          00001520
           02 MPAGEC    PIC X.                                          00001530
           02 MPAGEP    PIC X.                                          00001540
           02 MPAGEH    PIC X.                                          00001550
           02 MPAGEV    PIC X.                                          00001560
           02 MPAGEO    PIC X(4).                                       00001570
      * PAGE                                                            00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MNBPAGESA      PIC X.                                     00001600
           02 MNBPAGESC PIC X.                                          00001610
           02 MNBPAGESP PIC X.                                          00001620
           02 MNBPAGESH PIC X.                                          00001630
           02 MNBPAGESV PIC X.                                          00001640
           02 MNBPAGESO      PIC X(4).                                  00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MLDEPOTA  PIC X.                                          00001670
           02 MLDEPOTC  PIC X.                                          00001680
           02 MLDEPOTP  PIC X.                                          00001690
           02 MLDEPOTH  PIC X.                                          00001700
           02 MLDEPOTV  PIC X.                                          00001710
           02 MLDEPOTO  PIC X(20).                                      00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MCETATA   PIC X.                                          00001740
           02 MCETATC   PIC X.                                          00001750
           02 MCETATP   PIC X.                                          00001760
           02 MCETATH   PIC X.                                          00001770
           02 MCETATV   PIC X.                                          00001780
           02 MCETATO   PIC X(20).                                      00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MNRECQUAIA     PIC X.                                     00001810
           02 MNRECQUAIC     PIC X.                                     00001820
           02 MNRECQUAIP     PIC X.                                     00001830
           02 MNRECQUAIH     PIC X.                                     00001840
           02 MNRECQUAIV     PIC X.                                     00001850
           02 MNRECQUAIO     PIC X(7).                                  00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLNRECQORIA    PIC X.                                     00001880
           02 MLNRECQORIC    PIC X.                                     00001890
           02 MLNRECQORIP    PIC X.                                     00001900
           02 MLNRECQORIH    PIC X.                                     00001910
           02 MLNRECQORIV    PIC X.                                     00001920
           02 MLNRECQORIO    PIC X(17).                                 00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MNRECA    PIC X.                                          00001950
           02 MNRECC    PIC X.                                          00001960
           02 MNRECP    PIC X.                                          00001970
           02 MNRECH    PIC X.                                          00001980
           02 MNRECV    PIC X.                                          00001990
           02 MNRECO    PIC X(7).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MLQRECA   PIC X.                                          00002020
           02 MLQRECC   PIC X.                                          00002030
           02 MLQRECP   PIC X.                                          00002040
           02 MLQRECH   PIC X.                                          00002050
           02 MLQRECV   PIC X.                                          00002060
           02 MLQRECO   PIC X(16).                                      00002070
           02 MLIGNEO OCCURS   12 TIMES .                               00002080
             03 FILLER       PIC X(2).                                  00002090
             03 MNNCODICA    PIC X.                                     00002100
             03 MNNCODICC    PIC X.                                     00002110
             03 MNNCODICP    PIC X.                                     00002120
             03 MNNCODICH    PIC X.                                     00002130
             03 MNNCODICV    PIC X.                                     00002140
             03 MNNCODICO    PIC X(7).                                  00002150
             03 FILLER       PIC X(2).                                  00002160
             03 MNLREFOA     PIC X.                                     00002170
             03 MNLREFOC     PIC X.                                     00002180
             03 MNLREFOP     PIC X.                                     00002190
             03 MNLREFOH     PIC X.                                     00002200
             03 MNLREFOV     PIC X.                                     00002210
             03 MNLREFOO     PIC X(19).                                 00002220
             03 FILLER       PIC X(2).                                  00002230
             03 MNNCDEA      PIC X.                                     00002240
             03 MNNCDEC PIC X.                                          00002250
             03 MNNCDEP PIC X.                                          00002260
             03 MNNCDEH PIC X.                                          00002270
             03 MNNCDEV PIC X.                                          00002280
             03 MNNCDEO      PIC X(7).                                  00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MNNENTCDEA   PIC X.                                     00002310
             03 MNNENTCDEC   PIC X.                                     00002320
             03 MNNENTCDEP   PIC X.                                     00002330
             03 MNNENTCDEH   PIC X.                                     00002340
             03 MNNENTCDEV   PIC X.                                     00002350
             03 MNNENTCDEO   PIC X(5).                                  00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MNLENTCDEA   PIC X.                                     00002380
             03 MNLENTCDEC   PIC X.                                     00002390
             03 MNLENTCDEP   PIC X.                                     00002400
             03 MNLENTCDEH   PIC X.                                     00002410
             03 MNLENTCDEV   PIC X.                                     00002420
             03 MNLENTCDEO   PIC X(20).                                 00002430
             03 FILLER       PIC X(2).                                  00002440
             03 MNNBLA  PIC X.                                          00002450
             03 MNNBLC  PIC X.                                          00002460
             03 MNNBLP  PIC X.                                          00002470
             03 MNNBLH  PIC X.                                          00002480
             03 MNNBLV  PIC X.                                          00002490
             03 MNNBLO  PIC X(10).                                      00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MNQRECA      PIC X.                                     00002520
             03 MNQRECC PIC X.                                          00002530
             03 MNQRECP PIC X.                                          00002540
             03 MNQRECH PIC X.                                          00002550
             03 MNQRECV PIC X.                                          00002560
             03 MNQRECO      PIC X(5).                                  00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MZONCMDA  PIC X.                                          00002590
           02 MZONCMDC  PIC X.                                          00002600
           02 MZONCMDP  PIC X.                                          00002610
           02 MZONCMDH  PIC X.                                          00002620
           02 MZONCMDV  PIC X.                                          00002630
           02 MZONCMDO  PIC X(7).                                       00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MLIBERRA  PIC X.                                          00002660
           02 MLIBERRC  PIC X.                                          00002670
           02 MLIBERRP  PIC X.                                          00002680
           02 MLIBERRH  PIC X.                                          00002690
           02 MLIBERRV  PIC X.                                          00002700
           02 MLIBERRO  PIC X(66).                                      00002710
      * CODE TRANSACTION                                                00002720
           02 FILLER    PIC X(2).                                       00002730
           02 MCODTRAA  PIC X.                                          00002740
           02 MCODTRAC  PIC X.                                          00002750
           02 MCODTRAP  PIC X.                                          00002760
           02 MCODTRAH  PIC X.                                          00002770
           02 MCODTRAV  PIC X.                                          00002780
           02 MCODTRAO  PIC X(4).                                       00002790
      * CICS DE TRAVAIL                                                 00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MCICSA    PIC X.                                          00002820
           02 MCICSC    PIC X.                                          00002830
           02 MCICSP    PIC X.                                          00002840
           02 MCICSH    PIC X.                                          00002850
           02 MCICSV    PIC X.                                          00002860
           02 MCICSO    PIC X(5).                                       00002870
      * NETNAME                                                         00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MNETNAMA  PIC X.                                          00002900
           02 MNETNAMC  PIC X.                                          00002910
           02 MNETNAMP  PIC X.                                          00002920
           02 MNETNAMH  PIC X.                                          00002930
           02 MNETNAMV  PIC X.                                          00002940
           02 MNETNAMO  PIC X(8).                                       00002950
      * CODE TERMINAL                                                   00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MSCREENA  PIC X.                                          00002980
           02 MSCREENC  PIC X.                                          00002990
           02 MSCREENP  PIC X.                                          00003000
           02 MSCREENH  PIC X.                                          00003010
           02 MSCREENV  PIC X.                                          00003020
           02 MSCREENO  PIC X(5).                                       00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MTYPAGEA  PIC X.                                          00003050
           02 MTYPAGEC  PIC X.                                          00003060
           02 MTYPAGEP  PIC X.                                          00003070
           02 MTYPAGEH  PIC X.                                          00003080
           02 MTYPAGEV  PIC X.                                          00003090
           02 MTYPAGEO  PIC X(2).                                       00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MNSOCDEPOTA    PIC X.                                     00003120
           02 MNSOCDEPOTC    PIC X.                                     00003130
           02 MNSOCDEPOTP    PIC X.                                     00003140
           02 MNSOCDEPOTH    PIC X.                                     00003150
           02 MNSOCDEPOTV    PIC X.                                     00003160
           02 MNSOCDEPOTO    PIC X(3).                                  00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MNDEPOTA  PIC X.                                          00003190
           02 MNDEPOTC  PIC X.                                          00003200
           02 MNDEPOTP  PIC X.                                          00003210
           02 MNDEPOTH  PIC X.                                          00003220
           02 MNDEPOTV  PIC X.                                          00003230
           02 MNDEPOTO  PIC X(3).                                       00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MDJRECQUAIA    PIC X.                                     00003260
           02 MDJRECQUAIC    PIC X.                                     00003270
           02 MDJRECQUAIP    PIC X.                                     00003280
           02 MDJRECQUAIH    PIC X.                                     00003290
           02 MDJRECQUAIV    PIC X.                                     00003300
           02 MDJRECQUAIO    PIC X(10).                                 00003310
           02 FILLER    PIC X(2).                                       00003320
           02 MDSAISRECA     PIC X.                                     00003330
           02 MDSAISRECC     PIC X.                                     00003340
           02 MDSAISRECP     PIC X.                                     00003350
           02 MDSAISRECH     PIC X.                                     00003360
           02 MDSAISRECV     PIC X.                                     00003370
           02 MDSAISRECO     PIC X(10).                                 00003380
           02 FILLER    PIC X(2).                                       00003390
           02 MLCOMMENTA     PIC X.                                     00003400
           02 MLCOMMENTC     PIC X.                                     00003410
           02 MLCOMMENTP     PIC X.                                     00003420
           02 MLCOMMENTH     PIC X.                                     00003430
           02 MLCOMMENTV     PIC X.                                     00003440
           02 MLCOMMENTO     PIC X(34).                                 00003450
                                                                                
