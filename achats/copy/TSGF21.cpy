      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : TS LIGNES DE COMMANDE PAR DEMANDEUR CODIC              *         
      *        CREE PAR TGF21                                         *         
      *****************************************************************         
      *                                                               *         
       01  TS-GF21.                                                             
           02 TS-GF21-GESTION.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 TS-GF21-POS                 PIC S9(4) COMP.                   
      *--                                                                       
               05 TS-GF21-POS                 PIC S9(4) COMP-5.                 
      *}                                                                        
      *--------------------------------  STRUCTURE TS                           
           02 TS-GF21-DONNEES.                                                  
             03 TS-GF21-ENR.                                                    
               05 TS-GF21-NCODIC              PIC X(7).                         
               05 TS-GF21-NENTCDE             PIC X(7).                         
               05 TS-GF21-NSOCIETE            PIC X(3).                         
               05 TS-GF21-NLIEU               PIC X(3).                         
               05 TS-GF21-NDEPOT              PIC X(3).                         
               05 TS-GF21-CHEFPROD            PIC X(5).                         
               05 TS-GF21-CMODSTOCK           PIC X(5).                         
               05 TS-GF21-QSOUHAITEE          PIC 9(5) COMP-3.                  
               05 TS-GF21-DSOUHAITEE          PIC X(6).                         
               05 TS-GF21-CFAM                PIC X(5).                         
               05 TS-GF21-CMARQ               PIC X(5).                         
               05 TS-GF21-CQUOTA              PIC X(5).                         
               05 TS-GF21-DANNEE              PIC X(4).                         
               05 TS-GF21-SEMAINE             PIC X(2).                         
               05 TS-GF21-JSEM                PIC X(1).                         
               05 TS-GF21-LREFFOURN           PIC X(20).                        
               05 TS-GF21-CSTATUT             PIC X(1).                         
               05 TS-GF21-DSAMEDI             PIC X(1).                         
               05 TS-GF21-NLIGNE              PIC X(2).                         
               05 TS-GF21-NPAGE               PIC X(3).                         
               05 TS-GF21-NBRUO               PIC S9(5) COMP-3.                 
               05 TS-GF21-QPALETTE            PIC S9(5) COMP-3.                 
                                                                                
