      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RTGF89                             *        
      ******************************************************************        
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF8900                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGF8900.                                                            
           10 GF89-NCODIC               PIC X(7).                               
           10 GF89-NENTCDE              PIC X(5).                               
           10 GF89-NEAN                 PIC X(13).                              
           10 GF89-LREFFOURN            PIC X(20).                              
           10 GF89-WDISPO               PIC X(1).                               
           10 GF89-DPROCHDISPO          PIC X(8).                               
           10 GF89-DTRTFIC              PIC X(8).                               
           10 GF89-LCOMM                PIC X(80).                              
           10 GF89-DSYST                PIC S9(13)V USAGE COMP-3.               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGF8900                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGF8900-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF89-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GF89-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF89-NENTCDE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GF89-NENTCDE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF89-NEAN-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 GF89-NEAN-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF89-LREFFOURN-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 GF89-LREFFOURN-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF89-WDISPO-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 GF89-WDISPO-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF89-DPROCHDISPO-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 GF89-DPROCHDISPO-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF89-DTRTFIC-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 GF89-DTRTFIC-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF89-LCOMM-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 GF89-LCOMM-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF89-DSYST-F         PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 GF89-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
      *}                                                                        
