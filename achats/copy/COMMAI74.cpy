      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  TRANSACTION: GA50                                             *        
      *  TITRE      : COMMAREA DE L'APPLICATION INT  APELLEE PAR TGA74 *        
      *                        INTERROGATION DONNEES KESA              *        
      *  LONGUEUR   : 984                                              *        
      *                                                                *        
      ******************************************************************        
CT10  * MODIF 10/10/02 DSA056 CUSINE X(1) + CUSINEVRAI X(5)             00530000
MAINT * MODIF 09/12/02 DSA057 REFONTE INTERFACE                         00530000
FT    * QLARGDE ETC                                                     00530000
      *                                                                 00230000
      *01  COMM-AI74-LONG-COMMAREA     PIC S9(4) COMP VALUE +984.               
CT10  *01  COMM-AI74-LONG-COMMAREA     PIC S9(4) COMP VALUE +1141 .             
MAINT *01  COMM-AI74-LONG-COMMAREA     PIC S9(4) COMP VALUE +1142 .             
MAINT *01  COMM-AI74-LONG-COMMAREA PIC S9(4) COMP VALUE +1169 .                 
       01  COMM-AI74-APPLI.                                             00260000
         02 COMM-AI74-ENTREE.                                           00260000
           05 COMM-AI74-NOM-TS-55.                                              
               10  COMM-AI74-DEB-NOM-TS-55   PIC X(4).                          
               10  COMM-AI74-FIN-NOM-TS-55   PIC X(4).                          
           05 COMM-AI74-NOM-TS-58.                                              
               10  COMM-AI74-DEB-NOM-TS-58   PIC X(4).                          
               10  COMM-AI74-FIN-NOM-TS-58   PIC X(4).                          
           05 COMM-AI74-NOM-TS-75.                                              
               10  COMM-AI74-DEB-NOM-TS-75   PIC X(4).                          
               10  COMM-AI74-FIN-NOM-TS-75   PIC X(4).                          
           05 COMM-AI74-NOM-TS-63.                                              
               10  COMM-AI74-DEB-NOM-TS-63   PIC X(4).                          
               10  COMM-AI74-FIN-NOM-TS-63   PIC X(4).                          
           05 COMM-AI74-NOM-TS-76.                                              
               10  COMM-AI74-DEB-NOM-TS-76   PIC X(4).                          
               10  COMM-AI74-FIN-NOM-TS-76   PIC X(4).                          
           05 COMM-AI74-NOM-TS-98.                                              
               10  COMM-AI74-DEB-NOM-TS-98   PIC X(4).                          
               10  COMM-AI74-FIN-NOM-TS-98   PIC X(4).                          
           05 COMM-AI74-NSOCIETE        PIC X(3).                               
MAINT-*    05 COMM-AI74-ACTION          PIC X.                                  
      *    05 COMM-AI74-FLAGOPCO        PIC X.                                  
-MAINT*    05 COMM-AI74-INTERFACE       PIC X.                                  
           05 COMM-AI74-NCODICK             PIC X(07).                  00330000
MAINT *    05 COMM-AI74-NSKUK               PIC X(07).                  00340000
           05 COMM-AI74-CMARQ               PIC X(5).                           
           05 COMM-AI74-LREFO               PIC X(20).                          
           05 COMM-AI74-NEAN                PIC X(13).                          
           05 COMM-AI74-NPROD               PIC X(15).                          
           05 COMM-AI74-CCOLOR              PIC X(5).                           
MAINT-     05 COMM-AI74-CFAM PIC X(5).                                          
           05 FILLER PIC X(9).                                                  
         02 COMM-AI74-GESTION.                                                  
           05 COMM-AI74-RREF PIC 9 VALUE 0.                                     
               88 COMM-AI74-SREF VALUE 0.                                       
               88 COMM-AI74-GREF VALUE 1.                                       
           05 COMM-AI74-RFAM PIC 9 VALUE 0.                                     
               88 COMM-AI74-SFAM VALUE 0.                                       
               88 COMM-AI74-GFAM VALUE 1.                                       
-MAINT     05 COMM-AI74-POSPROD PIC 9(7).                                       
         02 COMM-AI74-SORTIE.                                                   
           05 COMM-AI74-OPCO.                                                   
               10  COMM-AI74-DG1.                                               
                 15 COMM-AI74-CGESTVTE PIC X(3).                                
                 15 COMM-AI74-CASSORT PIC X(5).                                 
                 15 COMM-AI74-WDACEM PIC X.                                     
                 15 COMM-AI74-WTLMELA PIC X.                                    
                 15 COMM-AI74-CGARANTIE PIC X(5).                               
                 15 COMM-AI74-CGARCONST PIC X(5).                               
CT10             15 COMM-AI74-CUSINE PIC X(1).                                  
      *        IDENTIFICATION ARTICLE -  DONNEES GENERALES 2                    
               10  COMM-AI74-DG2.                                               
                 15 COMM-AI74-DEPT PIC X(3).                                    
                 15 COMM-AI74-SUBDEPT PIC X(3).                                 
                 15 COMM-AI74-CLASS PIC X(3).                                   
                 15 COMM-AI74-SUBCLASS PIC X(3).                                
                 15 COMM-AI74-IATRB4 PIC X(2).                                  
                 15 COMM-AI74-IATRB2 PIC X(2).                                  
                 15 COMM-AI74-ISTYPE PIC X(2).                                  
                 15 COMM-AI74-IWARGC PIC X(2).                                  
               10  COMM-AI74-NBCOMPO PIC 9(2).                                  
      *        MISE � JOUR DES STATUTS ARTICLES                                 
               10  COMM-AI74-MAJ-STATUT.                                        
                 15 COMM-AI74-LREFDARTY PIC X(20).                              
                 15 COMM-AI74-LCOMMENT PIC X(50).                               
                 15 COMM-AI74-IATRB3 PIC X(2).                                  
                 15 COMM-AI74-CAPPRO PIC X(3).                                  
                 15 COMM-AI74-LSTATCOMP PIC X(3).                               
                 15 COMM-AI74-IRPLCD PIC X.                                     
                 15 COMM-AI74-CEXPO PIC X.                                      
                 15 COMM-AI74-WSENSVTE PIC X.                                   
                 15 COMM-AI74-I3WKMX PIC 9(2).                                  
                 15 COMM-AI74-WSENSAPPRO PIC X.                                 
      *        DEFINITION D'UN ARTICLE -  APPROVISIONNEMENT 1                   
               10  COMM-AI74-APPRO1.                                            
                 15 COMM-AI74-CHEFPROD PIC X(5).                                
                 15 COMM-AI74-QDELAIAPPRO PIC 9(3).                             
                 15 COMM-AI74-QCOLICDE PIC 9(3).                                
                 15 COMM-AI74-WSTOCKAVANCE PIC X.                               
      *        DEFINITION D'UN ARTICLE -  DONNEES ENTREPOT 1                    
               10  COMM-AI74-ENTREPOT1.                                         
                 15 COMM-AI74-NSOCDEPOT1 PIC X(3).                              
                 15 COMM-AI74-NDEPOT1 PIC X(3).                                 
                 15 COMM-AI74-CCOTEHOLD PIC X.                                  
                 15 COMM-AI74-CMODSTOCK1 PIC X(5).                              
                 15 COMM-AI74-CMODSTOCK2 PIC X(5).                              
                 15 COMM-AI74-CMODSTOCK3 PIC X(5).                              
                 15 COMM-AI74-WMODSTOCK1 PIC X.                                 
                 15 COMM-AI74-WMODSTOCK2 PIC X.                                 
                 15 COMM-AI74-WMODSTOCK3 PIC X.                                 
                 15 COMM-AI74-CZONEACCES PIC X.                                 
                 15 COMM-AI74-SYSMEC PIC X.                                     
                 15 COMM-AI74-CLASSE PIC X.                                     
                 15 COMM-AI74-QNBPRACK PIC 9(5).                                
                 15 COMM-AI74-QNBPVSOL PIC 9(3).                                
                 15 COMM-AI74-CCONTENEUR PIC X.                                 
                 15 COMM-AI74-QNBRANMAIL PIC 9(2).                              
                 15 COMM-AI74-CSPECIFSTK PIC X.                                 
                 15 COMM-AI74-QNBNIVGERB PIC 9(2).                              
                 15 COMM-AI74-WCROSSDOCK PIC X.                                 
                 15 COMM-AI74-CUNITRECEPT PIC X(3).                             
                 15 COMM-AI74-CUNITVTE     PIC X(3).                            
                 15 COMM-AI74-CQUOTA PIC X(5).                                  
      *       DEFINITION D'UN ARTICLE -  DONNEES ENTREPOT 2                     
              10  COMM-AI74-ENTREPOT2.                                          
                  15 COMM-AI74-LEMBALLAGE PIC X(50).                            
                  15 COMM-AI74-CTYPCONDT PIC X(5).                              
                  15 COMM-AI74-QCONDT PIC 9(5).                                 
                  15 COMM-AI74-QCOLIRECEPT PIC 9(5).                            
                  15 COMM-AI74-QCOLIDESTOCK PIC 9(5).                           
                  15 COMM-AI74-CFETEMPL PIC X.                                  
                  15 COMM-AI74-QCOLIVTE PIC 9(5).                               
                  15 COMM-AI74-QPOIDS PIC 9(7).                                 
                  15 COMM-AI74-QLARGEUR PIC 9(3).                               
                  15 COMM-AI74-QPROFONDEUR PIC 9(3).                            
                  15 COMM-AI74-QHAUTEUR PIC 9(3).                               
                  15 COMM-AI74-QPOIDSDE PIC 9(7).                               
                  15 COMM-AI74-QLARGEURDE PIC 9(3).                             
                  15 COMM-AI74-QPROFONDEURDE PIC 9(3).                          
                  15 COMM-AI74-QHAUTEURDE PIC 9(3).                             
                  15 COMM-AI74-QCOUCHPAL PIC 9(2).                              
                  15 COMM-AI74-QCARTCOUCH PIC 9(2).                             
                  15 COMM-AI74-QBOXCART PIC 9(4).                               
                  15 COMM-AI74-QPRODBOX PIC 9(4).                               
FT                15 COMM-AI74-QLARGDE PIC 9(4)V9.                              
                  15 COMM-AI74-QPROFDE PIC 9(4)V9.                              
                  15 COMM-AI74-QHAUTDE PIC 9(4)V9.                              
      *         DEFINITION D'UN ARTICLE -  DONNEES DE VENTE                     
                10  COMM-AI74-VENTE.                                            
                  15 COMM-AI74-QCONTENU PIC S9(5).                              
                  15 COMM-AI74-QGRATUITE PIC S9(5).                             
                  15 COMM-AI74-WLCONF PIC X.                                    
                  15 COMM-AI74-CFETIQINFO PIC X.                                
                  15 COMM-AI74-CFETIQPRIX PIC X.                                
      *                                                                         
          05  COMM-AI74-MESSAGE.                                                
              10  COMM-AI74-SORTIE-CODRET PIC X.                                
      *              88 COMM-AI74-SORTIE-OK VALUE ' '.                          
      *              88 COMM-AI74-SORTIE-ERR VALUE '1'.                         
              10  COMM-AI74-SORTIE-LIBELLE.                                     
                  15 COMM-AI74-SORTIE-NOMPGRM PIC X(6).                         
                  15 COMM-AI74-SORTIE-LIBERR PIC X(52).                         
MAINT *       10  COMM-AI74-CODRET PIC X.                                       
MAINT *       10  COMM-AI74-DEPHEAN PIC X.                                      
PH1R  *       10  COMM-AI74-NBPROD PIC 9(3).                                    
MAINT *       10  COMM-AI74-NBPROD PIC 9(6).                                    
MAINT         10 COMM-AI74-COUNTPROD PIC 9(7).                                  
MAINT         10 COMM-AI74-NBPROD PIC 9(7).                                     
MAINT         10 COMM-AI74-MATCH PIC 9 VALUE 0.                                 
MAINT             88 COMM-AI74-MULTI VALUE 0.                                   
MAINT             88 COMM-AI74-UNIQUE VALUE 1.                                  
MAINT         10 FILLER PIC X(4).                                               
      *                                                                         
              10  COMM-AI74-SECTID.                                             
                  15 COMM-AI74-SECTID-NCODICK PIC X(7).                         
                  15  COMM-AI74-SECTID-NCODIC PIC X(7).                         
                  15  COMM-AI74-SECTID-CMARQ PIC X(5).                          
                  15  COMM-AI74-SECTID-LREFO PIC X(20).                         
                  15  COMM-AI74-SECTID-NEAN PIC X(13).                          
                  15  COMM-AI74-SECTID-NPROD PIC X(15).                         
                  15  COMM-AI74-SECTID-CCOLOR PIC X(5).                         
CT10              15  COMM-AI74-CUSINEVRAI PIC X(5).                            
                  15  COMM-AI74-CORIGPROD PIC X(5).                             
                  15  COMM-AI74-CLIGPROD PIC X(5).                              
                  15  COMM-AI74-LMODBASE PIC X(20).                             
MAINT *           15  COMM-AI74-CFAM PIC X(5).                                  
MAINT             15  COMM-AI74-SECTID-CFAM PIC X(5).                           
                  15  COMM-AI74-LREFFOURN PIC X(20).                            
                  15  COMM-AI74-COPCO PIC X(3).                                 
                  15  COMM-AI74-DCREATION PIC X(8).                             
                  15  COMM-AI74-WMULTISOC PIC X.                                
                  15  COMM-AI74-VERSION    PIC X(20).                           
                  15  COMM-AI74-NBDESCR PIC 9(3).                               
                  15  COMM-AI74-NBDEST  PIC 9(3).                               
                  15  COMM-AI74-DESCRIPTION OCCURS 30.                          
                      20  COMM-AI74-CDESCRIPTIF PIC X(5).                       
                      20  COMM-AI74-CVDESCRIPT  PIC X(5).                       
                  15  COMM-AI74-CDESTINATION OCCURS 30.                         
                      20  COMM-AI74-CDEST       PIC X(5).                       
                                                                                
