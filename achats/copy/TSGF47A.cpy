      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      *                                                                         
      * TS TGF47A : QUOTA PLANNING RAYON                                        
      *                                                                         
       01  TS-GF47A.                                                            
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03  TS-GF47A-LONG             PIC  S9(4) COMP  VALUE +105.           
      *--                                                                       
           03  TS-GF47A-LONG             PIC  S9(4) COMP-5  VALUE +105.         
      *}                                                                        
      *                                                                         
           03  TS-GF47A-DONNEES.                                                
      *                                                                         
             05 TS-GF47A-TABLEAU           OCCURS 5.                            
                    10 TS-GF47A-CRAYON       PIC X(5).                          
                    10 TS-GF47A-LRAYON       PIC X(15).                         
                    10 TS-GF47A-FLAG         PIC X(1).                          
      *                                                                         
                                                                                
