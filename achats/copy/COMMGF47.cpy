      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00001030
      *     COMMAREA LISTE DES QUOTA PAR RAYON                        * 00002030
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00003030
      * ***************************************** LONG = 736            00004030
E0157 * ***************************************** LONG = 752            00004130
            02 COMM-GF47-APPLI REDEFINES COMM-GF00-APPLI.               00004230
      ****************************************************** 113        00004330
               03  COMM-GF47GF48-COMMUN.                                00004430
                 05  COMM-GF47GF48-WFONC         PIC X(3).              00004530
                 05  COMM-GF47GF48-AUTOR-FLAG    PIC X(1).              00004630
                 05  COMM-GF47GF48-DATJOUR       PIC X(8).              00004730
                 05  COMM-GF47GF48-NSEMAINE.                            00004830
                     10 COMM-GF47GF48-DANNEE.                           00004930
                        15 COMM-GF47GF48-SS      PIC X(2).              00005030
                        15 COMM-GF47GF48-AA      PIC X(2).              00006030
                     10 COMM-GF47GF48-NUMSEM     PIC X(2).              00006130
                 05  COMM-GF47GF48-DATE-COMET    PIC X(6).              00006230
                 05  COMM-GF47GF48-DATES-SEMAINE OCCURS 7.              00006330
                     10 COMM-GF47GF48-J          PIC X(8).              00006430
                 05  COMM-GF47GF48-SOCDEP.                              00006530
                     10 COMM-GF47GF48-NSOCLIVR   PIC X(3).              00006630
                     10 COMM-GF47GF48-NDEPOT     PIC X(3).              00006730
E0157            05  COMM-GF47GF48-CMODSTOCK     PIC X(5).              00006830
                 05  COMM-GF47GF48-CRAYON        PIC X(5).              00006930
LA2803           05  COMM-GF47GF48-CQUOTA        PIC X(5).              00007032
                 05  COMM-GF47GF48-CHGT-RAYON    PIC X(1).              00007130
                 05  COMM-GF47GF48-CHGT-PAGE     PIC X(1).              00007230
                 05  COMM-GF47GF48-CHGT-DATE-DEPOT   PIC X(1).          00007330
E0157.             88 COMM-GF47GF48-AUCUN-CHGT       VALUE '0'.         00007430
                   88 COMM-GF47GF48-CHGT-DATE        VALUE 'D'.         00007530
                   88 COMM-GF47GF48-CHGT-ENTREPOT    VALUE 'E'.         00007630
.E0157             88 COMM-GF47GF48-CHGT-CMODSTOCK   VALUE 'C'.         00007730
                 05  COMM-GF47GF48-DATE-SELECT   PIC X(8).              00007830
                 05  COMM-GF47GF48-RAYON-SELECT  PIC X(5).              00007930
                 05  COMM-GF47GF48-FLAG-SELECT   PIC X(1).              00008030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
E0157 *          05  COMM-GF47GF48-NBRAYON       PIC S9(4) COMP.        00008130
      *--                                                                       
                 05  COMM-GF47GF48-NBRAYON       PIC S9(4) COMP-5.              
      *}                                                                        
      *************************************************** 536           00009030
               03  COMM-GF47-SPECIF.                                    00009130
                 05  COMM-GF47-TOP           PIC X(1).                  00009230
                 05  COMM-GF47-PF6           PIC X(1).                  00009330
                 05  COMM-GF47-SWAP          PIC X(1).                  00009430
                 05  COMM-GF47-LABEL-UO      PIC X(40).                 00009530
                 05  COMM-GF47-LABEL-PIECE   PIC X(40).                 00009630
                 05  COMM-GF47-LPREV         PIC X(4).                  00009730
                 05  COMM-GF47-LPRIS         PIC X(4).                  00009830
                 05  COMM-GF47-LSOLDE        PIC X(5).                  00009930
E0157            05  COMM-GF47-LSOLDETOT     PIC X(7).                  00010030
                 05  COMM-GF47-SELECTION     PIC X(1).                  00011030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
E0157 *          05  COMM-GF47-RANG-TSGF47B  PIC S9(4) COMP.            00012030
      *--                                                                       
                 05  COMM-GF47-RANG-TSGF47B  PIC S9(4) COMP-5.                  
      *}                                                                        
                 05  COMM-GF47-NPAGE         PIC 9(3).                  00013030
                 05  COMM-GF47-NPAGE-OLD     PIC 9(3).                  00014030
                 05  COMM-GF47-NPAGEMAX      PIC 9(3).                  00015030
                 05  COMM-GF47-TABLEAU       OCCURS 5.                  00016030
                     10  COMM-GF47-CRAYON    PIC X(5).                  00017030
                     10  COMM-GF47-LRAYON    PIC X(15).                 00018030
                     10  COMM-GF47-FLAG      PIC X(1).                  00019030
                     10  COMM-GF47-CHGT-LIGNE PIC X(1).                 00020030
                     10  COMM-GF47-DATA      OCCURS 7.                  00030030
                         15 COMM-GF47-QPREV  PIC S9(5) COMP-3.          00030130
                         15 COMM-GF47-QPRIS  PIC S9(5) COMP-3.          00030230
                         15 COMM-GF47-QSOLDE PIC S9(5) COMP-3.          00030330
      ************************************************* 101             00030530
               03  COMM-GF48-SPECIF.                                    00030630
                 05  COMM-GF48-TOP           PIC X(1).                  00030730
                 05  COMM-GF48-NUMJOUR       PIC 9(1).                  00030830
                 05  COMM-GF48-DEB-SEMAINE   PIC X(8).                  00030930
                 05  COMM-GF48-FIN-SEMAINE   PIC X(8).                  00031030
                 05  COMM-GF48-NPAGE         PIC 9(3).                  00031130
                 05  COMM-GF48-NPAGE-OLD     PIC 9(3).                  00032030
                 05  COMM-GF48-NPAGEMAX      PIC 9(3).                  00033030
                 05  COMM-GF48-NBRCFAM       PIC 9(3).                  00034030
                 05  COMM-GF48-QUOPREVSEM    PIC S9(6) COMP-3.          00035030
                 05  COMM-GF48-QUOPRISSEM    PIC S9(6) COMP-3.          00036030
                 05  COMM-GF48-QUOSOLDSEM    PIC S9(6) COMP-3.          00037030
                 05  COMM-GF48-QUORECSEM     PIC S9(6) COMP-3.          00038030
                 05  COMM-GF48-QTEPREVSEM    PIC S9(6) COMP-3.          00039030
                 05  COMM-GF48-QTEPRISSEM    PIC S9(6) COMP-3.          00040030
                 05  COMM-GF48-QTESOLDSEM    PIC S9(6) COMP-3.          00050030
                 05  COMM-GF48-QTERECSEM     PIC S9(6) COMP-3.          00051030
                 05  COMM-GF48-QUOPREVJ      PIC S9(6) COMP-3.          00052030
                 05  COMM-GF48-QUOPRISJ      PIC S9(6) COMP-3.          00053030
                 05  COMM-GF48-QUOSOLDJ      PIC S9(6) COMP-3.          00054030
                 05  COMM-GF48-QUORECJ       PIC S9(6) COMP-3.          00055030
                 05  COMM-GF48-QTEPREVJ      PIC S9(6) COMP-3.          00056030
                 05  COMM-GF48-QTEPRISJ      PIC S9(6) COMP-3.          00057030
                 05  COMM-GF48-QTESOLDJ      PIC S9(6) COMP-3.          00058030
                 05  COMM-GF48-QTERECJ       PIC S9(6) COMP-3.          00059030
      *                                                                 00060030
                                                                                
