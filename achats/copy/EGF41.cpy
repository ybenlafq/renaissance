      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF41   EGF41                                              00000020
      ***************************************************************** 00000030
       01   EGF41I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTCDEL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTCDEF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNENTCDEI      PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTCDEL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTCDEF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLENTCDEI      PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINTERLOCUTL  COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCINTERLOCUTL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MCINTERLOCUTF  PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCINTERLOCUTI  PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINTERLOCUTL  COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLINTERLOCUTL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MLINTERLOCUTF  PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLINTERLOCUTI  PIC X(20).                                 00000370
           02 MSTRUCTURI OCCURS   10 TIMES .                            00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCMARQI      PIC X(5).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMARQL      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MLMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLMARQF      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MLMARQI      PIC X(20).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MZONCMDI  PIC X(15).                                      00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MLIBERRI  PIC X(57).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MCODTRAI  PIC X(4).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCICSI    PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MNETNAMI  PIC X(8).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MSCREENI  PIC X(4).                                       00000700
      ***************************************************************** 00000710
      * SDF: EGF41   EGF41                                              00000720
      ***************************************************************** 00000730
       01   EGF41O REDEFINES EGF41I.                                    00000740
           02 FILLER    PIC X(12).                                      00000750
           02 FILLER    PIC X(2).                                       00000760
           02 MDATJOUA  PIC X.                                          00000770
           02 MDATJOUC  PIC X.                                          00000780
           02 MDATJOUP  PIC X.                                          00000790
           02 MDATJOUH  PIC X.                                          00000800
           02 MDATJOUV  PIC X.                                          00000810
           02 MDATJOUO  PIC X(10).                                      00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MTIMJOUA  PIC X.                                          00000840
           02 MTIMJOUC  PIC X.                                          00000850
           02 MTIMJOUP  PIC X.                                          00000860
           02 MTIMJOUH  PIC X.                                          00000870
           02 MTIMJOUV  PIC X.                                          00000880
           02 MTIMJOUO  PIC X(5).                                       00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MWPAGEA   PIC X.                                          00000910
           02 MWPAGEC   PIC X.                                          00000920
           02 MWPAGEP   PIC X.                                          00000930
           02 MWPAGEH   PIC X.                                          00000940
           02 MWPAGEV   PIC X.                                          00000950
           02 MWPAGEO   PIC X(3).                                       00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MWFONCA   PIC X.                                          00000980
           02 MWFONCC   PIC X.                                          00000990
           02 MWFONCP   PIC X.                                          00001000
           02 MWFONCH   PIC X.                                          00001010
           02 MWFONCV   PIC X.                                          00001020
           02 MWFONCO   PIC X(3).                                       00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MNENTCDEA      PIC X.                                     00001050
           02 MNENTCDEC PIC X.                                          00001060
           02 MNENTCDEP PIC X.                                          00001070
           02 MNENTCDEH PIC X.                                          00001080
           02 MNENTCDEV PIC X.                                          00001090
           02 MNENTCDEO      PIC X(5).                                  00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MLENTCDEA      PIC X.                                     00001120
           02 MLENTCDEC PIC X.                                          00001130
           02 MLENTCDEP PIC X.                                          00001140
           02 MLENTCDEH PIC X.                                          00001150
           02 MLENTCDEV PIC X.                                          00001160
           02 MLENTCDEO      PIC X(20).                                 00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MCINTERLOCUTA  PIC X.                                     00001190
           02 MCINTERLOCUTC  PIC X.                                     00001200
           02 MCINTERLOCUTP  PIC X.                                     00001210
           02 MCINTERLOCUTH  PIC X.                                     00001220
           02 MCINTERLOCUTV  PIC X.                                     00001230
           02 MCINTERLOCUTO  PIC X(5).                                  00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MLINTERLOCUTA  PIC X.                                     00001260
           02 MLINTERLOCUTC  PIC X.                                     00001270
           02 MLINTERLOCUTP  PIC X.                                     00001280
           02 MLINTERLOCUTH  PIC X.                                     00001290
           02 MLINTERLOCUTV  PIC X.                                     00001300
           02 MLINTERLOCUTO  PIC X(20).                                 00001310
           02 MSTRUCTURO OCCURS   10 TIMES .                            00001320
             03 FILLER       PIC X(2).                                  00001330
             03 MCMARQA      PIC X.                                     00001340
             03 MCMARQC PIC X.                                          00001350
             03 MCMARQP PIC X.                                          00001360
             03 MCMARQH PIC X.                                          00001370
             03 MCMARQV PIC X.                                          00001380
             03 MCMARQO      PIC X(5).                                  00001390
             03 FILLER       PIC X(2).                                  00001400
             03 MLMARQA      PIC X.                                     00001410
             03 MLMARQC PIC X.                                          00001420
             03 MLMARQP PIC X.                                          00001430
             03 MLMARQH PIC X.                                          00001440
             03 MLMARQV PIC X.                                          00001450
             03 MLMARQO      PIC X(20).                                 00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MZONCMDA  PIC X.                                          00001480
           02 MZONCMDC  PIC X.                                          00001490
           02 MZONCMDP  PIC X.                                          00001500
           02 MZONCMDH  PIC X.                                          00001510
           02 MZONCMDV  PIC X.                                          00001520
           02 MZONCMDO  PIC X(15).                                      00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MLIBERRA  PIC X.                                          00001550
           02 MLIBERRC  PIC X.                                          00001560
           02 MLIBERRP  PIC X.                                          00001570
           02 MLIBERRH  PIC X.                                          00001580
           02 MLIBERRV  PIC X.                                          00001590
           02 MLIBERRO  PIC X(57).                                      00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MCODTRAA  PIC X.                                          00001620
           02 MCODTRAC  PIC X.                                          00001630
           02 MCODTRAP  PIC X.                                          00001640
           02 MCODTRAH  PIC X.                                          00001650
           02 MCODTRAV  PIC X.                                          00001660
           02 MCODTRAO  PIC X(4).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MCICSA    PIC X.                                          00001690
           02 MCICSC    PIC X.                                          00001700
           02 MCICSP    PIC X.                                          00001710
           02 MCICSH    PIC X.                                          00001720
           02 MCICSV    PIC X.                                          00001730
           02 MCICSO    PIC X(5).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MNETNAMA  PIC X.                                          00001760
           02 MNETNAMC  PIC X.                                          00001770
           02 MNETNAMP  PIC X.                                          00001780
           02 MNETNAMH  PIC X.                                          00001790
           02 MNETNAMV  PIC X.                                          00001800
           02 MNETNAMO  PIC X(8).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MSCREENA  PIC X.                                          00001830
           02 MSCREENC  PIC X.                                          00001840
           02 MSCREENP  PIC X.                                          00001850
           02 MSCREENH  PIC X.                                          00001860
           02 MSCREENV  PIC X.                                          00001870
           02 MSCREENO  PIC X(4).                                       00001880
                                                                                
