      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ****************************************************************  00000010
      * TS SPECIFIQUE TGF50                                          *  00000020
      ****************************************************************  00000030
      *------------------------------ LONGUEUR                          00000040
      *01  TS-GF50-LONG          PIC S9(3) COMP-3 VALUE 840.            00000050
      *------------------------------ DONNEES, 1 LIGNE PAR ITEM         00000060
       01  TS-GF50-DONNEES.                                             00000070
         05 TS-GF50-PAGE-CODIC    OCCURS 12.                            00000090
           10 TS-GF50-DCOMMENT    PIC X(08).                            00000140
           10 TS-GF50-LCOMMENT    PIC X(62).                            00000140
                                                                                
