      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                 00010000
      ***************************************************************** 00020000
      *  COMMAREA SPECIFIQUE AUX VALORISATIONS COMMANDES FOURNISSEURS * 00030000
      ***************************************************************** 00040000
      * AJOUT MONTANT CONTRIBUTION ECOLOGIQUE                                   
      *                                                                 00050000
           02  COMM-GF22-APPLI REDEFINES COMM-GF00-APPLI.               00060000
      *                                                                 00130001
      *  DONNEES ARTICLE                                                00140001
      *                                                                 00150001
             03  COMM-GA00-DONNEES.                                     00160001
                 04  COMM-GA00-NCODIC         PIC  X(07).               00170001
                 04  COMM-GA00-CFAM           PIC  X(05).               00180001
                 04  COMM-GA00-CMARQ          PIC  X(05).               00190001
                 04  COMM-GA00-LREFFOURN      PIC  X(20).               00200001
                 04  COMM-GA00-CTAUXTVA       PIC  X(05).               00210001
      *                                                                 00220000
      *  DONNEES LIGNE COMMANDE A VALORISER                             00230000
      *                                                                 00240000
             03  COMM-GF20-DONNEES.                                     00250000
      *          04  COMM-GF20-NCDE           PIC  X(07).               00260001
                 04  COMM-GF20-NCDE-FILLE OCCURS 10.                            
                     05 COMM-GF20-NCDE        PIC X(7).                         
                 04  COMM-GF20-NCODIC         PIC  X(07).               00270001
                 04  COMM-GF20-DSAISIE        PIC  X(08).               00280001
                 04  COMM-GF20-DVALIDITE      PIC  X(08).               00290001
                 04  COMM-GF20-QCDE           PIC S9(05) COMP-3.        00300001
                 04  COMM-GF20-QREC           PIC S9(05) COMP-3.        00310001
                 04  COMM-GF20-QSOLDE         PIC S9(05) COMP-3.        00320001
                 04  COMM-GF20-PABASEFACT     PIC S9(07)V99 COMP-3.     00330001
                 04  COMM-GF20-PROMOFACT      PIC S9(07)V99 COMP-3.     00340001
                 04  COMM-GF20-QTAUXESCPT     PIC S9(03)V99 COMP-3.     00350001
                 04  COMM-GF20-PTAUXESCPT     PIC S9(07)V99 COMP-3.     00360001
                 04  COMM-GF20-PAHORSPROMO    PIC S9(07)V99 COMP-3.     00370001
                 04  COMM-GF20-PRISTPROMO     PIC S9(07)V99 COMP-3.     00380001
                 04  COMM-GF20-PRISTCONDI     PIC S9(07)V99 COMP-3.     00390001
                 04  COMM-GF20-PRA            PIC S9(07)V99 COMP-3.     00400001
                 04  COMM-GF20-PCF            PIC S9(07)V99 COMP-3.     00401001
      *                                                                 00410001
      *  DONNEES LIGNE ANCIENNE COMMANDE VALORISEE                      00420001
      *                                                                 00430001
                 04  COMM-GF20-NCDEDER        PIC  X(07).               00440001
                 04  COMM-GF20-DVALIDITEDER   PIC  X(08).               00450001
                 04  COMM-GF20-QCDEDER        PIC S9(05)    COMP-3.     00460001
                 04  COMM-GF20-PABASEFACTDER  PIC S9(07)V99 COMP-3.     00470001
                 04  COMM-GF20-PROMOFACTDER   PIC S9(07)V99 COMP-3.     00480001
                 04  COMM-GF20-QTAUXESCPTDER  PIC S9(03)V99 COMP-3.     00490001
                 04  COMM-GF20-PTAUXESCPTDER  PIC S9(07)V99 COMP-3.     00500001
                 04  COMM-GF20-PAHORSPROMODER PIC S9(07)V99 COMP-3.     00510001
                 04  COMM-GF20-PRISTPROMODER  PIC S9(07)V99 COMP-3.     00520001
                 04  COMM-GF20-PRISTCONDIDER  PIC S9(07)V99 COMP-3.     00530001
                 04  COMM-GF20-PRADER         PIC S9(07)V99 COMP-3.     00540001
                 04  COMM-GF20-PCFDER         PIC S9(07)V99 COMP-3.     00541001
      *                                                                         
      * SAUVEGARDE INIT                                                         
      *                                                                         
             03  COMM-GF20-SAUV.                                                
                 04  COMM-SAUV-PABASEFACT     PIC S9(07)V99 COMP-3.     00330001
                 04  COMM-SAUV-PROMOFACT      PIC S9(07)V99 COMP-3.     00340001
                 04  COMM-SAUV-PTAUXESCPT     PIC S9(07)V99 COMP-3.     00360001
                 04  COMM-SAUV-PAHORSPROMO    PIC S9(07)V99 COMP-3.     00370001
                 04  COMM-SAUV-PRISTPROMO     PIC S9(07)V99 COMP-3.     00380001
                 04  COMM-SAUV-PRISTCONDI     PIC S9(07)V99 COMP-3.     00390001
                 04  COMM-SAUV-PRA            PIC S9(07)V99 COMP-3.     00400001
                 04  COMM-SAUV-PCF            PIC S9(07)V99 COMP-3.     00401001
      *                                                                         
                 04  COMM-SAUV-PABASEFACTDER  PIC S9(07)V99 COMP-3.     00470001
                 04  COMM-SAUV-PROMOFACTDER   PIC S9(07)V99 COMP-3.     00480001
                 04  COMM-SAUV-PTAUXESCPTDER  PIC S9(07)V99 COMP-3.     00500001
                 04  COMM-SAUV-PAHORSPROMODER PIC S9(07)V99 COMP-3.     00510001
                 04  COMM-SAUV-PRISTPROMODER  PIC S9(07)V99 COMP-3.     00520001
                 04  COMM-SAUV-PRISTCONDIDER  PIC S9(07)V99 COMP-3.     00530001
                 04  COMM-SAUV-PRADER         PIC S9(07)V99 COMP-3.     00540001
                 04  COMM-SAUV-PCFDER         PIC S9(07)V99 COMP-3.     00541001
      *                                                                 00550000
      *  DONNEES EN VRAC                                                00560000
      *                                                                 00570000
             03  COMM-VRAC-DONNEES.                                     00580000
                 04  COMM-VRAC-LMARQ           PIC  X(20).              00590001
                 04  COMM-VRAC-LFAM            PIC  X(20).              00600001
                 04  COMM-VRAC-LMODRGLT        PIC  X(20).              00610001
                 04  COMM-VRAC-LDEPRGLT        PIC  X(20).              00620001
      *                                                                 00630001
      *  DONNEES DE LA TRANSACTION GF22                                 00640001
      *                                                                 00650001
             03  COMM-GF22-DONNEES.                                     00660001
                 04  COMM-GF22-TOP             PIC  X.                  00670001
                 04  COMM-GF22-TOPVALO         PIC  X(15).              00680001
                 04  COMM-GF22-NITEM-1         PIC S9(04) COMP-3.       00690001
                 04  COMM-GF22-NPAGE           PIC  999.                00700001
                 04  COMM-GF22-NPAG-MAX        PIC S9(03) COMP-3.       00710001
                 04  COMM-GF22-NLIG-PAG        PIC S9(04) COMP-3.       00720001
                 04  COMM-GF22-NLIG-MAX        PIC S9(04) COMP-3.       00730001
                 04  COMM-GF22-MCTRI           PIC X.                           
                 04  COMM-GF22-MNENTCDEI       PIC X(5).                        
      *                                                                 00740001
      *  DONNEES DE LA TRANSACTION GF23                                 00750001
      *                                                                 00760001
             03  COMM-GF23-DONNEES.                                     00770001
                 04  COMM-GF23-TOP             PIC  X.                  00780001
                 04  COMM-GF23-NITEM-1         PIC S9(03) COMP-3.       00790001
                 04  COMM-GF23-NPAGE           PIC  999.                00800001
                 04  COMM-GF23-NPAG-MAX        PIC S9(03) COMP-3.       00810001
                 04  COMM-GF23-NLIG-MAX        PIC S9(03) COMP-3.       00820001
                 04  COMM-GF23-NLIG-PAG        PIC S9(03) COMP-3.       00830001
                 04  COMM-GF23-NLIG-SEL        PIC S9(03) COMP-3.       00840001
                 04  COMM-GF23-POURCENT        PIC S9(03)V99 COMP-3.    00850001
                 04  COMM-GF23-JOUR            PIC S9(03) COMP-3.       00860001
                 04  COMM-GF23-TOUCHE-FONCT    PIC X(03).               00862001
                 04  COMM-GF23-CDEVISE-INIT    PIC X(03).               00863001
                 04  COMM-GF23-CDEVISE-CONV    PIC X(03).               00863001
                 04  COMM-GF23-CDEVISE-DEPART  PIC X(03).               00863101
                 04  COMM-GF23-CDEVISE-ARRIVEE PIC X(03).               00863201
                 04  COMM-GF23-CDEVISE-TOP     PIC X.                   00863202
                 04  COMM-GF23-CDEVISE-GF23    PIC X(03).               00863301
                 04  COMM-GF23-LDEVISE         PIC X(25).               00864001
      *                                                                 00870001
      *  DONNEES DE LA TRANSACTION GF25                                 00880001
      *                                                                 00890001
             03  COMM-GF25-DONNEES.                                     00900001
                 04  COMM-GF25-CRIST OCCURS 14 PIC  X(07).              00910001
                 04  COMM-GF25-PRIST OCCURS 14 PIC  S9(5)V99 COMP-3.    00910001
      *          04  COMM-GF25-NPAGE           PIC 999.                 00910002
      *          04  COMM-GF25-NPAG-MAX        PIC S9(3) COMP-3.        00910003
      *          04  COMM-GF25-NITEM-1         PIC S9(03) COMP-3.       00910004
      *          04  COMM-GF25-NLIG-MAX        PIC S9(03) COMP-3.       00910007
      *          04  COMM-GF25-NLIG-PAG        PIC S9(03) COMP-3.       00910008
                                                                        00911001
      *  DONNEES DE LA TRANSACTION GF24                                 00912001
      *                                                                 00913001
             03  COMM-GF24-DONNEES.                                     00914001
                 04  COMM-GF24-CONFIRM PIC  X.                          00915001
                 04  COMM-GF24-PMONTANT  PIC  S9(7)V99 COMP-3.                  
                 04  COMM-GF24-MLCONTRIB PIC  X(20).                            
      *                                                                 00920001
                                                                                
