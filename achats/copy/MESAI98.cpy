      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ VALIDATION BDD KESA                                  
      *                                                                         
      *****************************************************************         
      * VERSION POUR MGAXX AVEC PUT ET GET EXTERNES                             
      *                                                                         
           10  WS-MESSAGE.                                                      
              15 MES-ENTETE.                                                    
               20 MES-TYPE PIC X(3).                                            
               20 MES-NSOCMSG PIC X(3).                                         
               20 MES-NLIEUMSG PIC X(3).                                        
               20 MES-NSOCDST PIC X(3).                                         
               20 MES-NLIEUDST PIC X(3).                                        
               20 MES-NORD PIC 9(8).                                            
               20 MES-LPROG PIC X(10).                                          
               20 MES-DJOUR PIC X(8).                                           
               20 MES-WSID PIC X(10).                                           
               20 MES-USER PIC X(10).                                           
               20 MES-CHRONO PIC 9(7).                                          
               20 MES-NBRMSG PIC 9(7).                                          
CRO   *        20   MES-FILLER    PIC    X(30).                                 
               20   MES-NBENR      PIC 9(05).                                   
               20   MES-TAILLE     PIC 9(05).                                   
               20   MES-VERSION2   PIC X(02).                                   
               20   MES-DSYST      PIC S9(13).                                  
               20   MES-FILLER     PIC X(05).                                   
               20 MES-INTERFACE PIC X.                                          
      *        20 MES-SECTID.                                                   
               20 MES-NCODICK        PIC X(7).                                  
               20 MES-NCODIC         PIC X(7).                                  
               20 MES-NEAN           PIC X(13).                                 
      *            25  MES-CMARQ          PIC X(5).                             
      *            25  MES-LREFO          PIC X(20).                            
      *            25  MES-SECTID-NEAN    PIC X(13).                            
      *            25  MES-NPROD          PIC X(15).                            
      *            25  MES-CCOLOR         PIC X(5).                             
      *        20 MES-NBLIGNE  PIC 9(3).                                        
      *        20 MES-CODE-BARRE OCCURS 30 DEPENDING ON MES-NBLIGNE.            
      *            25  MES-NEAN PIC X(13).                                      
                                                                                
