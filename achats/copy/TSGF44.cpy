      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                         
       01 TS-GF44.                                                              
CRO   *   05 TS-GF44-LONG PIC S9(5) COMP-3 VALUE +440.                          
E0037 *   05 TS-GF44-LONG PIC S9(5) COMP-3 VALUE +401.                          
      *                                                                         
          05 TS-GF44-DATA.                                                      
             10 TS-GF44-ENR.                                                    
                15 TS-GF44-ENTETE.                                              
                   20 TS-GF44-TOPMAJ-ENTETE  PIC X.                             
                      88 CODIC-SANS-MODIF VALUE ' '.                            
                      88 NOUVEAU-CODIC    VALUE 'C'.                            
                      88 ANCIEN-CODIC     VALUE 'M'.                            
                      88 CODIC-SUPPRIME   VALUE 'X'.                            
                      88 CODIC-MOD-43     VALUE 'N'.                            
      *                                                                         
                   20 TS-GF44-GRP-ENTETE.                                       
                      25 TS-GF44-NCODIC         PIC X(7).                       
E0037                 25 TS-GF44-LREFFOURN      PIC X(20).                      
                      25 TS-GF44-NSOCDEM        PIC X(3).                       
                      25 TS-GF44-NLIEUDEM       PIC X(3).                       
CRO                   25 TS-GF44-ORIG-DEMANDE   PIC X(1).                       
E0037              20 TS-GF44-QCDE-TOT       PIC S9(6) COMP-3.                  
                   20 TS-GF44-NSURCDE        PIC X(7).                          
                15 TS-GF44-W-PROTEGE         PIC 9.                             
                   88 OUI                    VALUE 1.                           
                   88 NON                    VALUE 0.                           
                15 TS-GF44-TAB-CORPS.                                           
                   20 TS-GF44-CORPS  OCCURS 10 TIMES.                           
                      25 TS-GF44-TOPMAJ-CORPS   PIC X.                          
                         88 TRIPLET-SANS-MODIF  VALUE ' '.                      
                         88 TRIPLET-NOUVEAU     VALUE 'C'.                      
                         88 TRIPLET-ANCIEN      VALUE 'M'.                      
                         88 TRIPLET-SUPPRIME    VALUE 'X'.                      
                         88 TRIPLET-BLOQUE      VALUE 'N'.                      
                      25 TS-GF44-NSOC-NDEPOT.                                   
E0037**                  30 TS-GF44-NSOC          PIC X(3).                     
                         30 TS-GF44-NDEPOT        PIC X(3).                     
                         30 TS-GF44-NCDE-FILLE    PIC X(7).                     
                      25 TS-GF44-QCDE-ENT       PIC S9(5) COMP-3.               
                      25 TS-GF44-QREC-ENT       PIC S9(5) COMP-3.               
                      25 TS-GF44-QSOLDE-ENT     PIC S9(5) COMP-3.               
CD                    25 TS-GF44-QCOLIRECEPT    PIC S9(05) COMP-3.              
                15 TS-GF44-0-GRP-ENTETE.                                        
                   20 TS-GF44-0-NCODIC      PIC X(7).                           
                   20 TS-GF44-0-NSOCDEM     PIC X(3).                           
                   20 TS-GF44-0-NLIEUDEM    PIC X(3).                           
                15 TS-GF44-0-TAB-CORPS.                                         
                   20 TS-GF44-0-CORPS  OCCURS 10 TIMES.                         
                      25 TS-GF44-0-TOPMAJ-CORPS   PIC X.                        
                         88 TRIPLET-SANS-MODIF  VALUE ' '.                      
                         88 TRIPLET-NOUVEAU     VALUE 'C'.                      
                         88 TRIPLET-ANCIEN      VALUE 'M'.                      
                         88 TRIPLET-SUPPRIME    VALUE 'X'.                      
                      25 TS-GF44-0-NSOC-NDEPOT.                                 
E0037**                  30 TS-GF44-0-NSOC         PIC X(3).                    
                         30 TS-GF44-0-NDEPOT       PIC X(3).                    
                         30 TS-GF44-0-NCDE-FILLE   PIC X(7).                    
                      25 TS-GF44-0-QCDE-ENT     PIC S9(5) COMP-3.               
                      25 TS-GF44-0-QREC-ENT     PIC S9(5) COMP-3.               
                      25 TS-GF44-0-QSOLDE-ENT   PIC S9(5) COMP-3.               
CD                    25 TS-GF44-0-QCOLIRECEPT  PIC S9(5) COMP-3.               
                                                                                
