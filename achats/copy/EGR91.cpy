      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR91   EGR91                                              00000020
      ***************************************************************** 00000030
       01   EGR91I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPAGESI      PIC X(4).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPOTL    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCDEPOTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSOCDEPOTF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCDEPOTI    PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNDEPOTI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEPOTL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLDEPOTF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLDEPOTI  PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRECQORIGL    COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNRECQORIGL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNRECQORIGF    PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNRECQORIGI    PIC X(7).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTCDEL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTCDEF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNENTCDEI      PIC X(5).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTCDEL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTCDEF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLENTCDEI      PIC X(20).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDJRECQUAIL    COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MDJRECQUAIL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDJRECQUAIF    PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDJRECQUAII    PIC X(10).                                 00000490
           02 MLIGNEI OCCURS   13 TIMES .                               00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCDEL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MNCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNCDEF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNCDEI  PIC X(7).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMMENTL   COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MLCOMMENTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLCOMMENTF   PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MLCOMMENTI   PIC X(20).                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLTRAITL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MLTRAITL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLTRAITF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MLTRAITI     PIC X(20).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNRECQUAIL   COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MNRECQUAIL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNRECQUAIF   PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNRECQUAII   PIC X(7).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSTATUTL    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCSTATUTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCSTATUTF    PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCSTATUTI    PIC X(2).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MNCODICI     PIC X(7).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLIBERRI  PIC X(73).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MNETNAMI  PIC X(8).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MSCREENI  PIC X(4).                                       00000940
      ***************************************************************** 00000950
      * SDF: EGR91   EGR91                                              00000960
      ***************************************************************** 00000970
       01   EGR91O REDEFINES EGR91I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUP  PIC X.                                          00001030
           02 MDATJOUH  PIC X.                                          00001040
           02 MDATJOUV  PIC X.                                          00001050
           02 MDATJOUO  PIC X(10).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MTIMJOUA  PIC X.                                          00001080
           02 MTIMJOUC  PIC X.                                          00001090
           02 MTIMJOUP  PIC X.                                          00001100
           02 MTIMJOUH  PIC X.                                          00001110
           02 MTIMJOUV  PIC X.                                          00001120
           02 MTIMJOUO  PIC X(5).                                       00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MPAGEA    PIC X.                                          00001150
           02 MPAGEC    PIC X.                                          00001160
           02 MPAGEP    PIC X.                                          00001170
           02 MPAGEH    PIC X.                                          00001180
           02 MPAGEV    PIC X.                                          00001190
           02 MPAGEO    PIC X(4).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MNBPAGESA      PIC X.                                     00001220
           02 MNBPAGESC PIC X.                                          00001230
           02 MNBPAGESP PIC X.                                          00001240
           02 MNBPAGESH PIC X.                                          00001250
           02 MNBPAGESV PIC X.                                          00001260
           02 MNBPAGESO      PIC X(4).                                  00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MNSOCDEPOTA    PIC X.                                     00001290
           02 MNSOCDEPOTC    PIC X.                                     00001300
           02 MNSOCDEPOTP    PIC X.                                     00001310
           02 MNSOCDEPOTH    PIC X.                                     00001320
           02 MNSOCDEPOTV    PIC X.                                     00001330
           02 MNSOCDEPOTO    PIC X(3).                                  00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MNDEPOTA  PIC X.                                          00001360
           02 MNDEPOTC  PIC X.                                          00001370
           02 MNDEPOTP  PIC X.                                          00001380
           02 MNDEPOTH  PIC X.                                          00001390
           02 MNDEPOTV  PIC X.                                          00001400
           02 MNDEPOTO  PIC X(3).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MLDEPOTA  PIC X.                                          00001430
           02 MLDEPOTC  PIC X.                                          00001440
           02 MLDEPOTP  PIC X.                                          00001450
           02 MLDEPOTH  PIC X.                                          00001460
           02 MLDEPOTV  PIC X.                                          00001470
           02 MLDEPOTO  PIC X(20).                                      00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNRECQORIGA    PIC X.                                     00001500
           02 MNRECQORIGC    PIC X.                                     00001510
           02 MNRECQORIGP    PIC X.                                     00001520
           02 MNRECQORIGH    PIC X.                                     00001530
           02 MNRECQORIGV    PIC X.                                     00001540
           02 MNRECQORIGO    PIC X(7).                                  00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNENTCDEA      PIC X.                                     00001570
           02 MNENTCDEC PIC X.                                          00001580
           02 MNENTCDEP PIC X.                                          00001590
           02 MNENTCDEH PIC X.                                          00001600
           02 MNENTCDEV PIC X.                                          00001610
           02 MNENTCDEO      PIC X(5).                                  00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MLENTCDEA      PIC X.                                     00001640
           02 MLENTCDEC PIC X.                                          00001650
           02 MLENTCDEP PIC X.                                          00001660
           02 MLENTCDEH PIC X.                                          00001670
           02 MLENTCDEV PIC X.                                          00001680
           02 MLENTCDEO      PIC X(20).                                 00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MDJRECQUAIA    PIC X.                                     00001710
           02 MDJRECQUAIC    PIC X.                                     00001720
           02 MDJRECQUAIP    PIC X.                                     00001730
           02 MDJRECQUAIH    PIC X.                                     00001740
           02 MDJRECQUAIV    PIC X.                                     00001750
           02 MDJRECQUAIO    PIC X(10).                                 00001760
           02 MLIGNEO OCCURS   13 TIMES .                               00001770
             03 FILLER       PIC X(2).                                  00001780
             03 MNCDEA  PIC X.                                          00001790
             03 MNCDEC  PIC X.                                          00001800
             03 MNCDEP  PIC X.                                          00001810
             03 MNCDEH  PIC X.                                          00001820
             03 MNCDEV  PIC X.                                          00001830
             03 MNCDEO  PIC X(7).                                       00001840
             03 FILLER       PIC X(2).                                  00001850
             03 MLCOMMENTA   PIC X.                                     00001860
             03 MLCOMMENTC   PIC X.                                     00001870
             03 MLCOMMENTP   PIC X.                                     00001880
             03 MLCOMMENTH   PIC X.                                     00001890
             03 MLCOMMENTV   PIC X.                                     00001900
             03 MLCOMMENTO   PIC X(20).                                 00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MLTRAITA     PIC X.                                     00001930
             03 MLTRAITC     PIC X.                                     00001940
             03 MLTRAITP     PIC X.                                     00001950
             03 MLTRAITH     PIC X.                                     00001960
             03 MLTRAITV     PIC X.                                     00001970
             03 MLTRAITO     PIC X(20).                                 00001980
             03 FILLER       PIC X(2).                                  00001990
             03 MNRECQUAIA   PIC X.                                     00002000
             03 MNRECQUAIC   PIC X.                                     00002010
             03 MNRECQUAIP   PIC X.                                     00002020
             03 MNRECQUAIH   PIC X.                                     00002030
             03 MNRECQUAIV   PIC X.                                     00002040
             03 MNRECQUAIO   PIC X(7).                                  00002050
             03 FILLER       PIC X(2).                                  00002060
             03 MCSTATUTA    PIC X.                                     00002070
             03 MCSTATUTC    PIC X.                                     00002080
             03 MCSTATUTP    PIC X.                                     00002090
             03 MCSTATUTH    PIC X.                                     00002100
             03 MCSTATUTV    PIC X.                                     00002110
             03 MCSTATUTO    PIC X(2).                                  00002120
             03 FILLER       PIC X(2).                                  00002130
             03 MNCODICA     PIC X.                                     00002140
             03 MNCODICC     PIC X.                                     00002150
             03 MNCODICP     PIC X.                                     00002160
             03 MNCODICH     PIC X.                                     00002170
             03 MNCODICV     PIC X.                                     00002180
             03 MNCODICO     PIC X(7).                                  00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLIBERRA  PIC X.                                          00002210
           02 MLIBERRC  PIC X.                                          00002220
           02 MLIBERRP  PIC X.                                          00002230
           02 MLIBERRH  PIC X.                                          00002240
           02 MLIBERRV  PIC X.                                          00002250
           02 MLIBERRO  PIC X(73).                                      00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODTRAA  PIC X.                                          00002280
           02 MCODTRAC  PIC X.                                          00002290
           02 MCODTRAP  PIC X.                                          00002300
           02 MCODTRAH  PIC X.                                          00002310
           02 MCODTRAV  PIC X.                                          00002320
           02 MCODTRAO  PIC X(4).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCICSA    PIC X.                                          00002350
           02 MCICSC    PIC X.                                          00002360
           02 MCICSP    PIC X.                                          00002370
           02 MCICSH    PIC X.                                          00002380
           02 MCICSV    PIC X.                                          00002390
           02 MCICSO    PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNETNAMA  PIC X.                                          00002420
           02 MNETNAMC  PIC X.                                          00002430
           02 MNETNAMP  PIC X.                                          00002440
           02 MNETNAMH  PIC X.                                          00002450
           02 MNETNAMV  PIC X.                                          00002460
           02 MNETNAMO  PIC X(8).                                       00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSCREENA  PIC X.                                          00002490
           02 MSCREENC  PIC X.                                          00002500
           02 MSCREENP  PIC X.                                          00002510
           02 MSCREENH  PIC X.                                          00002520
           02 MSCREENV  PIC X.                                          00002530
           02 MSCREENO  PIC X(4).                                       00002540
                                                                                
