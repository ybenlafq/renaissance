      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : IDENTIFICATION ARTICLE - CODES DESCRIPTIFS             *         
      *****************************************************************         
      *  AVRIL 2011  EVOL D8635                                                 
      *              SAISIE NON OBLIGATOIRE DU CODE CHEF PRODUIT                
      *****************************************************************         
       01  TS-GF03.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GF03-LONG                 PIC S9(5) COMP-3                     
      *                                    VALUE +102.                          
                                           VALUE +107.                          
           02 TS-GF03-DONNEES.                                                  
      *------------------------------TABLE TYPE DE DECLARATION                  
              03 TS-GF03-NCODIC           PIC X(7).                             
      *       03 TS-GF03-CHEFPROD         PIC X(5).                             
              03 TS-GF03-CHEFPROD         PIC X(5).                             
              03 TS-GF03-CINTERLOCUT      PIC X(5).                             
              03 TS-GF03-CMARQ            PIC X(5).                             
              03 TS-GF03-LREFFOURN        PIC X(20).                            
              03 TS-GF03-NENTCDE          PIC X(5).                             
              03 TS-GF03-QSOUHAITEE       PIC X(5).                             
              03 TS-GF03-DSOUHAITEE       PIC X(8).                             
              03 TS-GF03-LENTCDE          PIC X(20).                            
              03 TS-GF03-CFAM             PIC X(05).                            
              03 TS-GF03-NLIGNE           PIC X(02).                            
              03 TS-GF03-NPAGE            PIC X(03).                            
              03 TS-GF03-NSOCIETE         PIC X(03).                            
              03 TS-GF03-NLIEU            PIC X(03).                            
      *       03 TS-GF03-NSOCLIVR         PIC X(03).                            
      *       03 TS-GF03-NDEPOT           PIC X(03).                            
              03 TS-GF03-NENTCDE-ANC      PIC X(5).                             
      *       03 TS-GF03-NSOCLIVR-ANC     PIC X(03).                            
      *       03 TS-GF03-NDEPOT-ANC       PIC X(03).                            
      *                                                                         
                                                                                
