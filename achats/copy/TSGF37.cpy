      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * TS SPECIFIQUE PRG  TGF37                                        00020000
      **************************************************************    00040000
         01 TS-GF37-DONNEES.                                            00041006
            05 TS-GF37-CCOLOR                  PIC X.                   00260004
            05 TS-GF37-CBASE                   PIC X.                   00270004
            05 TS-GF37-NCDE                    PIC X(7).                00280004
            05 TS-GF37-NDEPOT                  PIC X(3).                00290004
            05 TS-GF37-NCODIC                  PIC X(7).                00300004
            05 TS-GF37-LREF                    PIC X(14).               00310011
            05 TS-GF37-LMARQ                   PIC X(5).                00311011
            05 TS-GF37-QTOT                    PIC X(5).                00320009
            05 TS-GF37-QREC                    PIC X(5).                00331009
            05 TS-GF37-QSOLDE                  PIC X(5).                00340009
            05 TS-GF37-TAB.                                             00341005
               10 TS-GF37-DETAIL OCCURS 20.                             00341105
                  15 TS-GF37-DETAIL-DLIVR      PIC X(8).                00342005
                  15 TS-GF37-DETAIL-QCDE       PIC X(5).                00343009
                                                                                
