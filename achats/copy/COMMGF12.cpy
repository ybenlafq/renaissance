      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGF12                    TR: GF12  *    00020001
      * GESTION DES LIGNES DE COMMANDES EN INSTANCE                *    00030001
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 7421  00050000
      *                                                                 00060000
          02 COMM-GF12-APPLI REDEFINES COMM-GF00-APPLI.                 00070001
      *                                                                 00080001
      * ZONES COMMUNES PROGRAMMES          TGF12    ------------- 7421  00090000
      *                                    TGF13                        00100000
      *                                    TGF14                        00110000
      *                                    TGF15                        00120000
      *                                                                 00130000
      *----------------------------------------------------------- 84           
             03 COMM-GF12-COMMUNE.                                      00140000
      *------------------------------ CODE ENTITE DE COMMANDE           00150000
                04 COMM-GF12-NENTCDE        PIC X(5).                   00160000
      *------------------------------ LIBELLE ENTITE DE COMMANDE        00170000
                04 COMM-GF12-LENTCDE        PIC X(20).                  00180000
      *------------------------------ CODE INTERLOCUTEUR COMMERCIAL     00190000
                04 COMM-GF12-CINTERLOCUT    PIC X(5).                   00200000
      *------------------------------ LIBELLE INTERLO. COMMERCIAL       00210000
                04 COMM-GF12-LINTERLOCUT    PIC X(20).                  00220000
      *------------------------------ NUMERO DEPOT                      00230000
                04 COMM-GF12-NSOCDEP.                                   00240001
                   05 COMM-GF12-NSOCLIVR    PIC X(3).                   00250001
                   05 COMM-GF12-NDEPOT      PIC X(3).                   00260001
      *------------------------------ TYPE COMMANDE                     00270000
                04 COMM-GF12-TYPCO          PIC X(5).                   00280000
      *------------------------------ NUMERO DE COMMANDE                00290000
                04 COMM-GF12-NCOMMANDE      PIC X(7).                   00300001
      *------------------------------ DATE SAISIE COMMANDE              00310000
                04 COMM-GF12-DSAISIE        PIC X(8).                   00320001
      *------------------------------ DATE VALIDITE COMMANDE            00330000
                04 COMM-GF12-DVALIDITE      PIC X(8).                   00340001
      *                                                                 00350001
      * ZONES PROPRES AU PROGRAMME         TGF12    ------------- 7337  00360001
      *                                                                 00370001
AS           03 COMM-GF12-CDEAN              PIC X(1).                          
NSKEP2*      03 COMM-GF12-CFEXT-MAP          PIC X(1).                  00550001
             03 COMM-GF12-PROPRE.                                       00380001
      *------------------------------ TABLE DATE -------------- 80      00390001
                04 COMM-GF12-TAB-DATE       OCCURS 4.                   00400001
                  05 COMM-GF12-DATE         PIC X(8).                   00410001
                04 COMM-GF12-TAB-SEM        OCCURS 4.                   00430001
                  05 COMM-GF12-SEMAINE      PIC X(2).                   00440001
                  05 COMM-GF12-ANNEE        PIC X(4).                   00450001
                  05 COMM-GF12-NJOUR        PIC X(1).                   00450002
                  05 COMM-GF12-CQUOTA       PIC X(5).                   00460001
                04 COMM-GF12-ZONE-PAGINATION.                           00470001
      *------------------------------ PAGINATION -------------- 13      00480001
                   05 COMM-GF12-NPAGE       PIC 9(5) COMP-3.            00490001
                   05 COMM-GF12-NITEM       PIC 9(5) COMP-3.            00500001
                   05 COMM-GF12-NOMBRE-PAGE PIC 9(5) COMP-3.            00510001
                   05 COMM-GF12-LCURSEUR    PIC 9(5) COMP-3.            00520001
                   05 COMM-GF12-EXIST-TS    PIC X(1).                   00530001
      *------------------------------ COMPTEUR NOMBRE DE LIGNE A 'C'--3 00540001
                   05 COMM-GF12-COMPTEUR-C  PIC 9(5) COMP-3.            00550001
      *------------------------------ CODIC GROUPE --------1440         00560001
                04 COMM-GF12-TAB-GROUPE     OCCURS 80.                  00570001
                   05 COMM-GF12-NCODIC-GRP  PIC X(7).                   00580001
                   05 COMM-GF12-NCODIC-LIE  PIC X(7).                   00590001
                   05 COMM-GF12-NPAGE-GRP   PIC 9(2).                   00600001
                   05 COMM-GF12-NLIGNE-GRP  PIC 9(2).                   00610001
      *------------------------------ INDICE GROUPE --------2           00620001
                04 COMM-GF12-IND-GROUPE     PIC S9(3) COMP-3.           00630001
      *------------------------------ CODIC CARTON ------- 1520         00640001
                04 COMM-GF12-TAB-CARTON     OCCURS 80.                  00650001
                   05 COMM-GF12-NCODIC-CAR  PIC X(7).                   00660001
                   05 COMM-GF12-NCODIC-ELT  PIC X(7).                   00670001
                   05 COMM-GF12-NPAGE-CAR   PIC 9(2).                   00680001
                   05 COMM-GF12-NLIGNE-CAR  PIC 9(2).                   00690001
                   05 COMM-GF12-CONTROLE    PIC X(1).                   00700001
      *------------------------------ INDICE CARTON --------2           00710001
                04 COMM-GF12-IND-CARTON     PIC S9(3) COMP-3.           00720001
      *------------------------------ SAUVEGARDE DES ATTRIBUTS ---200   00730001
                04 COMM-GF12-SWAP-ATTR OCCURS 200 PIC X(1).             00740001
      *------------------------------ FILLER                            00750001
AB    *         04 COMM-GF12-FILLER         PIC X(118).                 00760002
      *--------------------NOUVELLES DONNEES PIC X(18).                         
                04  COMM-GF12-NOUVELLES-DONNEES.                                
                   05 COMM-GF12-NSOCLIVR-MAP  PIC X(3).                         
                   05 COMM-GF12-NDEPOT-MAP    PIC X(3).                         
                   05 COMM-GF12-DSOUHAITEE    PIC X(8).                         
                   05 COMM-GF12-FLAGPF9       PIC X(1).                         
C0411              05 COMM-GF12-NLIGMAX       PIC 9(5) COMP-3.          00550001
AS    *------ FLAG BLOCAGE DES EAN NON AUTHENTIFIEE                             
AB    *         04 COMM-GF12-FILLER         PIC X(4062).                00760002
CD    *         04 COMM-GF12-FILLER         PIC X(4062).                00760002
      ***************************************************************** 00770000
                                                                                
