      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *********************************************************         00010000
      * MESSAGE CONTENANT LES STATUTS DES RECEPTIONS COTE MORPHEUS      00020001
       01 W-MESSAGE-CMU.                                                00110001
           10 W-DATA-CMU     OCCURS  100.                               00170001
             15 W-MES-CMU-NMUTATION          PIC X(07).                 00180001
             15 W-MES-CMU-WMULTI             PIC X(01).                 00190001
             15 W-MES-CMU-WTYPENVOI          PIC X(01).                 00191001
             15 W-MES-CMU-WBTOB              PIC X(01).                 00192001
             15 W-MES-CMU-DEXPED.                                       00193002
               20 W-MES-CMU-DEXPED-SS        PIC X(02).                 00193102
               20 W-MES-CMU-DEXPED-AA        PIC X(02).                 00193202
               20 W-MES-CMU-DEXPED-MM        PIC X(02).                 00193302
               20 W-MES-CMU-DEXPED-JJ        PIC X(02).                 00193402
             15 W-MES-CMU-CFILIERE           PIC X(02).                 00194001
             15 W-MES-CMU-DFILIERE           PIC X(08).                 00195001
             15 W-MES-CMU-NBLIGNES           PIC 9(05).                 00196001
             15 W-MES-CMU-QTE                PIC 9(05).                 00197001
             15 W-MES-CMU-QTEREC             PIC 9(05).                 00198001
             15 W-MES-CMU-DRECUEMO           PIC X(08).                 00199001
             15 W-MES-CMU-CETAT              PIC X(10).                 00199101
             15 W-MES-CMU-DCLOTURE           PIC X(08).                 00199201
             15 W-MES-CMU-HCLOTURE           PIC X(04).                 00199301
                                                                                
