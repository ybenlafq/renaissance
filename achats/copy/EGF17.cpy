      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF17   EGF17                                              00000020
      ***************************************************************** 00000030
       01   EGF17I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCDEI    PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFOURNL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNFOURNF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNFOURNI  PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLZONEL   COMP PIC S9(4).                                         
      *--                                                                       
           02 MLZONEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLZONEF   PIC X.                                                  
           02 FILLER    PIC X(4).                                               
           02 MLZONEI   PIC X(9).                                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATE1L   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDATE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATE1F   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDATE1I   PIC X(8).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATE3L   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDATE3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATE3F   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDATE3I   PIC X(8).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATE2L   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDATE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATE2F   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDATE2I   PIC X(8).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATE4L   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDATE4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATE4F   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDATE4I   PIC X(8).                                       00000450
           02 M126I OCCURS   10 TIMES .                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNCODICI     PIC X(7).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLREFI  PIC X(20).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTETOTL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MQTETOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTETOTF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MQTETOTI     PIC X(5).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE1L  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MQTE1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE1F  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MQTE1I  PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE2L  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MQTE2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE2F  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MQTE2I  PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE3L  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MQTE3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE3F  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MQTE3I  PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE4L  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MQTE4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE4F  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MQTE4I  PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUO1L  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MQUO1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQUO1F  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MQUO1I  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUO2L  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MQUO2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQUO2F  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQUO2I  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUO3L  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MQUO3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQUO3F  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQUO3I  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUO4L  COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MQUO4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQUO4F  PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQUO4I  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCQUOTA1L      COMP PIC S9(4).                            00000910
      *--                                                                       
           02 MCQUOTA1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCQUOTA1F      PIC X.                                     00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCQUOTA1I      PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLQUOTA1L      COMP PIC S9(4).                            00000950
      *--                                                                       
           02 MLQUOTA1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLQUOTA1F      PIC X.                                     00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLQUOTA1I      PIC X(20).                                 00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCQUOTA2L      COMP PIC S9(4).                            00000990
      *--                                                                       
           02 MCQUOTA2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCQUOTA2F      PIC X.                                     00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCQUOTA2I      PIC X(5).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLQUOTA2L      COMP PIC S9(4).                            00001030
      *--                                                                       
           02 MLQUOTA2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLQUOTA2F      PIC X.                                     00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MLQUOTA2I      PIC X(20).                                 00001060
           02 MQPRIS1D OCCURS   4 TIMES .                               00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPRIS1L     COMP PIC S9(4).                            00001080
      *--                                                                       
             03 MQPRIS1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQPRIS1F     PIC X.                                     00001090
             03 FILLER  PIC X(4).                                       00001100
             03 MQPRIS1I     PIC X(6).                                  00001110
           02 MQPRIS2D OCCURS   4 TIMES .                               00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPRIS2L     COMP PIC S9(4).                            00001130
      *--                                                                       
             03 MQPRIS2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQPRIS2F     PIC X.                                     00001140
             03 FILLER  PIC X(4).                                       00001150
             03 MQPRIS2I     PIC X(6).                                  00001160
           02 MQDISP1D OCCURS   4 TIMES .                               00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQDISP1L     COMP PIC S9(4).                            00001180
      *--                                                                       
             03 MQDISP1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQDISP1F     PIC X.                                     00001190
             03 FILLER  PIC X(4).                                       00001200
             03 MQDISP1I     PIC X(6).                                  00001210
           02 MQDISP2D OCCURS   4 TIMES .                               00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQDISP2L     COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MQDISP2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQDISP2F     PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MQDISP2I     PIC X(6).                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MZONCMDI  PIC X(15).                                      00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MLIBERRI  PIC X(58).                                      00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MCODTRAI  PIC X(4).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MCICSI    PIC X(5).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MNETNAMI  PIC X(8).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MSCREENI  PIC X(4).                                       00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLEL      COMP PIC S9(4).                                    
      *--                                                                       
           02 MLIBELLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBELLEF      PIC X.                                             
           02 FILLER    PIC X(4).                                               
           02 MLIBELLEI      PIC X(19).                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MORIGDATEL     COMP PIC S9(4).                                    
      *--                                                                       
           02 MORIGDATEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MORIGDATEF     PIC X.                                             
           02 FILLER    PIC X(4).                                               
           02 MORIGDATEI     PIC X(8).                                          
      ***************************************************************** 00001510
      * SDF: EGF17   EGF17                                              00001520
      ***************************************************************** 00001530
       01   EGF17O REDEFINES EGF17I.                                    00001540
           02 FILLER    PIC X(12).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MDATJOUA  PIC X.                                          00001570
           02 MDATJOUC  PIC X.                                          00001580
           02 MDATJOUP  PIC X.                                          00001590
           02 MDATJOUH  PIC X.                                          00001600
           02 MDATJOUV  PIC X.                                          00001610
           02 MDATJOUO  PIC X(10).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MTIMJOUA  PIC X.                                          00001640
           02 MTIMJOUC  PIC X.                                          00001650
           02 MTIMJOUP  PIC X.                                          00001660
           02 MTIMJOUH  PIC X.                                          00001670
           02 MTIMJOUV  PIC X.                                          00001680
           02 MTIMJOUO  PIC X(5).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MWPAGEA   PIC X.                                          00001710
           02 MWPAGEC   PIC X.                                          00001720
           02 MWPAGEP   PIC X.                                          00001730
           02 MWPAGEH   PIC X.                                          00001740
           02 MWPAGEV   PIC X.                                          00001750
           02 MWPAGEO   PIC X(3).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MWFONCA   PIC X.                                          00001780
           02 MWFONCC   PIC X.                                          00001790
           02 MWFONCP   PIC X.                                          00001800
           02 MWFONCH   PIC X.                                          00001810
           02 MWFONCV   PIC X.                                          00001820
           02 MWFONCO   PIC X(3).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MNCDEA    PIC X.                                          00001850
           02 MNCDEC    PIC X.                                          00001860
           02 MNCDEP    PIC X.                                          00001870
           02 MNCDEH    PIC X.                                          00001880
           02 MNCDEV    PIC X.                                          00001890
           02 MNCDEO    PIC X(7).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MNFOURNA  PIC X.                                          00001920
           02 MNFOURNC  PIC X.                                          00001930
           02 MNFOURNP  PIC X.                                          00001940
           02 MNFOURNH  PIC X.                                          00001950
           02 MNFOURNV  PIC X.                                          00001960
           02 MNFOURNO  PIC X(5).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLZONEA   PIC X.                                                  
           02 MLZONEC   PIC X.                                                  
           02 MLZONEP   PIC X.                                                  
           02 MLZONEH   PIC X.                                                  
           02 MLZONEV   PIC X.                                                  
           02 MLZONEO   PIC X(9).                                               
           02 FILLER    PIC X(2).                                               
           02 MDATE1A   PIC X.                                          00001990
           02 MDATE1C   PIC X.                                          00002000
           02 MDATE1P   PIC X.                                          00002010
           02 MDATE1H   PIC X.                                          00002020
           02 MDATE1V   PIC X.                                          00002030
           02 MDATE1O   PIC X(8).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MDATE3A   PIC X.                                          00002060
           02 MDATE3C   PIC X.                                          00002070
           02 MDATE3P   PIC X.                                          00002080
           02 MDATE3H   PIC X.                                          00002090
           02 MDATE3V   PIC X.                                          00002100
           02 MDATE3O   PIC X(8).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MDATE2A   PIC X.                                          00002130
           02 MDATE2C   PIC X.                                          00002140
           02 MDATE2P   PIC X.                                          00002150
           02 MDATE2H   PIC X.                                          00002160
           02 MDATE2V   PIC X.                                          00002170
           02 MDATE2O   PIC X(8).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MDATE4A   PIC X.                                          00002200
           02 MDATE4C   PIC X.                                          00002210
           02 MDATE4P   PIC X.                                          00002220
           02 MDATE4H   PIC X.                                          00002230
           02 MDATE4V   PIC X.                                          00002240
           02 MDATE4O   PIC X(8).                                       00002250
           02 M126O OCCURS   10 TIMES .                                 00002260
             03 FILLER       PIC X(2).                                  00002270
             03 MNCODICA     PIC X.                                     00002280
             03 MNCODICC     PIC X.                                     00002290
             03 MNCODICP     PIC X.                                     00002300
             03 MNCODICH     PIC X.                                     00002310
             03 MNCODICV     PIC X.                                     00002320
             03 MNCODICO     PIC X(7).                                  00002330
             03 FILLER       PIC X(2).                                  00002340
             03 MLREFA  PIC X.                                          00002350
             03 MLREFC  PIC X.                                          00002360
             03 MLREFP  PIC X.                                          00002370
             03 MLREFH  PIC X.                                          00002380
             03 MLREFV  PIC X.                                          00002390
             03 MLREFO  PIC X(20).                                      00002400
             03 FILLER       PIC X(2).                                  00002410
             03 MQTETOTA     PIC X.                                     00002420
             03 MQTETOTC     PIC X.                                     00002430
             03 MQTETOTP     PIC X.                                     00002440
             03 MQTETOTH     PIC X.                                     00002450
             03 MQTETOTV     PIC X.                                     00002460
             03 MQTETOTO     PIC X(5).                                  00002470
             03 FILLER       PIC X(2).                                  00002480
             03 MQTE1A  PIC X.                                          00002490
             03 MQTE1C  PIC X.                                          00002500
             03 MQTE1P  PIC X.                                          00002510
             03 MQTE1H  PIC X.                                          00002520
             03 MQTE1V  PIC X.                                          00002530
             03 MQTE1O  PIC X(5).                                       00002540
             03 FILLER       PIC X(2).                                  00002550
             03 MQTE2A  PIC X.                                          00002560
             03 MQTE2C  PIC X.                                          00002570
             03 MQTE2P  PIC X.                                          00002580
             03 MQTE2H  PIC X.                                          00002590
             03 MQTE2V  PIC X.                                          00002600
             03 MQTE2O  PIC X(5).                                       00002610
             03 FILLER       PIC X(2).                                  00002620
             03 MQTE3A  PIC X.                                          00002630
             03 MQTE3C  PIC X.                                          00002640
             03 MQTE3P  PIC X.                                          00002650
             03 MQTE3H  PIC X.                                          00002660
             03 MQTE3V  PIC X.                                          00002670
             03 MQTE3O  PIC X(5).                                       00002680
             03 FILLER       PIC X(2).                                  00002690
             03 MQTE4A  PIC X.                                          00002700
             03 MQTE4C  PIC X.                                          00002710
             03 MQTE4P  PIC X.                                          00002720
             03 MQTE4H  PIC X.                                          00002730
             03 MQTE4V  PIC X.                                          00002740
             03 MQTE4O  PIC X(5).                                       00002750
             03 FILLER       PIC X(2).                                  00002760
             03 MQUO1A  PIC X.                                          00002770
             03 MQUO1C  PIC X.                                          00002780
             03 MQUO1P  PIC X.                                          00002790
             03 MQUO1H  PIC X.                                          00002800
             03 MQUO1V  PIC X.                                          00002810
             03 MQUO1O  PIC X(4).                                       00002820
             03 FILLER       PIC X(2).                                  00002830
             03 MQUO2A  PIC X.                                          00002840
             03 MQUO2C  PIC X.                                          00002850
             03 MQUO2P  PIC X.                                          00002860
             03 MQUO2H  PIC X.                                          00002870
             03 MQUO2V  PIC X.                                          00002880
             03 MQUO2O  PIC X(4).                                       00002890
             03 FILLER       PIC X(2).                                  00002900
             03 MQUO3A  PIC X.                                          00002910
             03 MQUO3C  PIC X.                                          00002920
             03 MQUO3P  PIC X.                                          00002930
             03 MQUO3H  PIC X.                                          00002940
             03 MQUO3V  PIC X.                                          00002950
             03 MQUO3O  PIC X(4).                                       00002960
             03 FILLER       PIC X(2).                                  00002970
             03 MQUO4A  PIC X.                                          00002980
             03 MQUO4C  PIC X.                                          00002990
             03 MQUO4P  PIC X.                                          00003000
             03 MQUO4H  PIC X.                                          00003010
             03 MQUO4V  PIC X.                                          00003020
             03 MQUO4O  PIC X(4).                                       00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MCQUOTA1A      PIC X.                                     00003050
           02 MCQUOTA1C PIC X.                                          00003060
           02 MCQUOTA1P PIC X.                                          00003070
           02 MCQUOTA1H PIC X.                                          00003080
           02 MCQUOTA1V PIC X.                                          00003090
           02 MCQUOTA1O      PIC X(5).                                  00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MLQUOTA1A      PIC X.                                     00003120
           02 MLQUOTA1C PIC X.                                          00003130
           02 MLQUOTA1P PIC X.                                          00003140
           02 MLQUOTA1H PIC X.                                          00003150
           02 MLQUOTA1V PIC X.                                          00003160
           02 MLQUOTA1O      PIC X(20).                                 00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MCQUOTA2A      PIC X.                                     00003190
           02 MCQUOTA2C PIC X.                                          00003200
           02 MCQUOTA2P PIC X.                                          00003210
           02 MCQUOTA2H PIC X.                                          00003220
           02 MCQUOTA2V PIC X.                                          00003230
           02 MCQUOTA2O      PIC X(5).                                  00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MLQUOTA2A      PIC X.                                     00003260
           02 MLQUOTA2C PIC X.                                          00003270
           02 MLQUOTA2P PIC X.                                          00003280
           02 MLQUOTA2H PIC X.                                          00003290
           02 MLQUOTA2V PIC X.                                          00003300
           02 MLQUOTA2O      PIC X(20).                                 00003310
           02 DFHMS1 OCCURS   4 TIMES .                                 00003320
             03 FILLER       PIC X(2).                                  00003330
             03 MQPRIS1A     PIC X.                                     00003340
             03 MQPRIS1C     PIC X.                                     00003350
             03 MQPRIS1P     PIC X.                                     00003360
             03 MQPRIS1H     PIC X.                                     00003370
             03 MQPRIS1V     PIC X.                                     00003380
             03 MQPRIS1O     PIC ZZZZZZ.                                00003390
           02 DFHMS2 OCCURS   4 TIMES .                                 00003400
             03 FILLER       PIC X(2).                                  00003410
             03 MQPRIS2A     PIC X.                                     00003420
             03 MQPRIS2C     PIC X.                                     00003430
             03 MQPRIS2P     PIC X.                                     00003440
             03 MQPRIS2H     PIC X.                                     00003450
             03 MQPRIS2V     PIC X.                                     00003460
             03 MQPRIS2O     PIC ZZZZZZ.                                00003470
           02 DFHMS3 OCCURS   4 TIMES .                                 00003480
             03 FILLER       PIC X(2).                                  00003490
             03 MQDISP1A     PIC X.                                     00003500
             03 MQDISP1C     PIC X.                                     00003510
             03 MQDISP1P     PIC X.                                     00003520
             03 MQDISP1H     PIC X.                                     00003530
             03 MQDISP1V     PIC X.                                     00003540
             03 MQDISP1O     PIC ZZZZZZ.                                00003550
           02 DFHMS4 OCCURS   4 TIMES .                                 00003560
             03 FILLER       PIC X(2).                                  00003570
             03 MQDISP2A     PIC X.                                     00003580
             03 MQDISP2C     PIC X.                                     00003590
             03 MQDISP2P     PIC X.                                     00003600
             03 MQDISP2H     PIC X.                                     00003610
             03 MQDISP2V     PIC X.                                     00003620
             03 MQDISP2O     PIC ZZZZZZ.                                00003630
           02 FILLER    PIC X(2).                                       00003640
           02 MZONCMDA  PIC X.                                          00003650
           02 MZONCMDC  PIC X.                                          00003660
           02 MZONCMDP  PIC X.                                          00003670
           02 MZONCMDH  PIC X.                                          00003680
           02 MZONCMDV  PIC X.                                          00003690
           02 MZONCMDO  PIC X(15).                                      00003700
           02 FILLER    PIC X(2).                                       00003710
           02 MLIBERRA  PIC X.                                          00003720
           02 MLIBERRC  PIC X.                                          00003730
           02 MLIBERRP  PIC X.                                          00003740
           02 MLIBERRH  PIC X.                                          00003750
           02 MLIBERRV  PIC X.                                          00003760
           02 MLIBERRO  PIC X(58).                                      00003770
           02 FILLER    PIC X(2).                                       00003780
           02 MCODTRAA  PIC X.                                          00003790
           02 MCODTRAC  PIC X.                                          00003800
           02 MCODTRAP  PIC X.                                          00003810
           02 MCODTRAH  PIC X.                                          00003820
           02 MCODTRAV  PIC X.                                          00003830
           02 MCODTRAO  PIC X(4).                                       00003840
           02 FILLER    PIC X(2).                                       00003850
           02 MCICSA    PIC X.                                          00003860
           02 MCICSC    PIC X.                                          00003870
           02 MCICSP    PIC X.                                          00003880
           02 MCICSH    PIC X.                                          00003890
           02 MCICSV    PIC X.                                          00003900
           02 MCICSO    PIC X(5).                                       00003910
           02 FILLER    PIC X(2).                                       00003920
           02 MNETNAMA  PIC X.                                          00003930
           02 MNETNAMC  PIC X.                                          00003940
           02 MNETNAMP  PIC X.                                          00003950
           02 MNETNAMH  PIC X.                                          00003960
           02 MNETNAMV  PIC X.                                          00003970
           02 MNETNAMO  PIC X(8).                                       00003980
           02 FILLER    PIC X(2).                                       00003990
           02 MSCREENA  PIC X.                                          00004000
           02 MSCREENC  PIC X.                                          00004010
           02 MSCREENP  PIC X.                                          00004020
           02 MSCREENH  PIC X.                                          00004030
           02 MSCREENV  PIC X.                                          00004040
           02 MSCREENO  PIC X(4).                                       00004050
           02 FILLER    PIC X(2).                                               
           02 MLIBELLEA      PIC X.                                             
           02 MLIBELLEC PIC X.                                                  
           02 MLIBELLEP PIC X.                                                  
           02 MLIBELLEH PIC X.                                                  
           02 MLIBELLEV PIC X.                                                  
           02 MLIBELLEO      PIC X(19).                                         
           02 FILLER    PIC X(2).                                               
           02 MORIGDATEA     PIC X.                                             
           02 MORIGDATEC     PIC X.                                             
           02 MORIGDATEP     PIC X.                                             
           02 MORIGDATEH     PIC X.                                             
           02 MORIGDATEV     PIC X.                                             
           02 MORIGDATEO     PIC X(8).                                          
                                                                                
