      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ VALIDATION BDD KESA                                  
      *                                                                         
      *****************************************************************         
      * VERSION POUR MGAXX AVEC PUT ET GET EXTERNES                             
      *                                                                         
           10  WS-MESSAGE-RECU REDEFINES COMM-MQ13-MESSAGE.                     
      *---                                                                      
           17  MESR-ENTETE.                                                     
               20   MESR-TYPE     PIC    X(3).                                  
               20   MESR-NSOCMSG  PIC    X(3).                                  
               20   MESR-NLIEUMSG PIC    X(3).                                  
               20   MESR-NSOCDST  PIC    X(3).                                  
               20   MESR-NLIEUDST PIC    X(3).                                  
               20   MESR-NORD     PIC    9(8).                                  
               20   MESR-LPROG    PIC    X(10).                                 
               20   MESR-DJOUR    PIC    X(8).                                  
               20   MESR-WSID     PIC    X(10).                                 
               20   MESR-USER     PIC    X(10).                                 
               20   MESR-CHRONO   PIC    9(7).                                  
               20   MESR-NBRMSG   PIC    9(7).                                  
CRO   *        20   MESR-FILLER    PIC    X(30).                                
               20   MESR-NBENR      PIC 9(05).                                  
               20   MESR-TAILLE     PIC 9(05).                                  
               20   MESR-VERSION2   PIC X(02).                                  
               20   MESR-DSYST      PIC S9(13).                                 
               20   MESR-FILLER     PIC X(05).                                  
           17  MESR-SORTIE.                                                     
               20 MESR-SORTIE-CODRET PIC X.                                     
               20 MESR-SORTIE-LIBLIBRE PIC X(20).                               
               20 MESR-SORTIE-LIBELLE.                                          
                   25 MESR-SORTIE-NSEQERR  PIC X(4).                            
                   25 MESR-SORTIE-NOMPGRM  PIC X(6).                            
                   25 MESR-SORTIE-LIBERR   PIC X(52).                           
               20  MESR-MESSAGE.                                                
                   25  MESR-FLAGEAN   PIC X.                                    
      *            25  MESR-FLAGDEPH  PIC X.                                    
                   25  MESR-WMULTISOC   PIC X.                                  
                   25  MESR-NCODICK     PIC X(7).                               
                                                                                
