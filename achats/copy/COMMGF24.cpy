      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      * COMMAREA SPECIFIQUE MODULE CREATION COMMANDE EN MONOCODIC  *            
      **************************************************************            
      *                                                                         
      *   PROGRAMME MGF24                                                       
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 COMM-MGF24-LONG-COMMAREA          PIC S9(4) COMP VALUE +220.          
      *--                                                                       
       01 COMM-MGF24-LONG-COMMAREA          PIC S9(4) COMP-5 VALUE +220.        
      *}                                                                        
       01 COMM-MGF24-APPLI.                                                     
      *                                                                         
          02 COMM-MGF24-ENTREE.                                                 
             05 COMM-MGF24-NTERMID          PIC X(04).                          
             05 COMM-MGF24-NCDE             PIC X(07).                          
             05 COMM-MGF24-NENTCDE          PIC X(05).                          
             05 COMM-MGF24-LENTCDE          PIC X(20).                          
             05 COMM-MGF24-CINTERLOCUT      PIC X(05).                          
             05 COMM-MGF24-LINTERLOCUT      PIC X(20).                          
             05 COMM-MGF24-DSAISIE          PIC X(08).                          
             05 COMM-MGF24-DVALIDITE        PIC X(08).                          
             05 COMM-MGF24-CSTATUT          PIC X(03).                          
             05 COMM-MGF24-CQUALIF          PIC X(06).                          
             05 COMM-MGF24-PROGRET          PIC X(05).                          
             05 COMM-MGF24-CREASON          PIC X(01).                          
             05 COMM-MGF24-ORIGDATE         PIC X(08).                          
             05 COMM-MGF24-DANNEE           PIC X(04).                          
             05 COMM-MGF24-NSEMAINE         PIC X(02).                          
             05 COMM-MGF24-WEDI             PIC X(01).                          
             05 COMM-MGF24-WSAP             PIC X(01).                          
             05 COMM-MGF24-VALOMANO         PIC X(01).                          
      *                                                                         
          02 COMM-MGF24-SORTIE.                                                 
             05 COMM-MGF24-NCDE1            PIC X(07).                          
             05 COMM-MGF24-NCDE2            PIC X(07).                          
             05 COMM-MGF24-CODRET           PIC X(01).                          
                88 COMM-MGF24-OK                  VALUE ' '.                    
                88 COMM-MGF24-ERR                 VALUE '1'.                    
             05 COMM-MGF24-LIBERR           PIC X(58).                          
      *                                                                         
          02 COMM-GF24-TS.                                                      
             05 CS-GF24-IDENT               PIC X(8).                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05 CS-GF24-NB                  PIC S9(4) COMP.                     
      *--                                                                       
             05 CS-GF24-NB                  PIC S9(4) COMP-5.                   
      *}                                                                        
          02 FILLER                         PIC X(28).                          
      *                                                                         
                                                                                
