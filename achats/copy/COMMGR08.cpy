      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00001000
      * COMMAREA SPECIFIQUE PRG TGR08 (TGR00 -> MENU)    TR: GR00  *    00002000
      *               GESTION DES RECEPTIONS                       *    00003000
      *                                                                 00004000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3710  00005000
      *                                                                 00006000
      *        TRANSACTION GR08 : TABLEAU D'AVANCEMENT             *    00007000
      *                           DES "RANGEMENTS" EN ATTENTE      *    00007100
      *                                                                 00007200
          03 COMM-GR08-APPLI REDEFINES COMM-GR01-APPLI.                 00007300
      *------------------------------ ZONE DONNEES TGR08                00007400
             04 COMM-GR08-DONNEES-TGR08.                                00007500
                05 COMM-GR08-NSOCIETE          PIC XXX.                 00007600
                05 COMM-GR08-LSOCIETE          PIC X(20).               00007700
                05 COMM-GR08-NDEPOT            PIC XXX.                 00007800
                05 COMM-GR08-LDEPOT            PIC X(20).               00007900
                05 COMM-GR08-SNCODIC           PIC X(7).                00008000
                05 COMM-GR08-SBLOCAGE          PIC X(3).                00009000
                05 COMM-GR08-SDEFFET           PIC X(8).                00010000
                05 COMM-GR08-TSLONG            PIC 9(1).                00011001
                   88 TS08-PAS-TROP-LONGUE           VALUE 1.           00012002
                   88 TS08-TROP-LONGUE               VALUE 2.           00013002
                05 COMM-GR08-DEPOT-LM          PIC 9(1).                00020002
                   88 COMM-GR08-DEPOT-LM6            VALUE 1.           00030000
                   88 COMM-GR08-DEPOT-LM7            VALUE 2.           00040000
                05 COMM-GR08-NBLIGNES          PIC 9(5).                00050000
                05 COMM-GR08-NBPRODUITS        PIC 9(5).                00060000
                05 COMM-GR08-NBPIECES          PIC 9(5).                00070000
                05 COMM-GR08-PAGE              PIC 9(4).                00080000
                05 COMM-GR08-NBPAGES           PIC 9(4).                00090000
             04 COMM-GR08-LIBRE                PIC X(2277).             00100000
      ***************************************************************** 00110000
                                                                                
