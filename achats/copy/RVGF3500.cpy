      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGF3500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF3500                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF3500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF3500.                                                            
      *}                                                                        
           02  GF35-DANNEE                                                      
               PIC X(0004).                                                     
           02  GF35-DSEMAINE                                                    
               PIC X(0002).                                                     
           02  GF35-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  GF35-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GF35-QUOPREVSOL1                                                 
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOPREVSOL2                                                 
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOPREVSOL3                                                 
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOPREVSOL4                                                 
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOPREVSOL5                                                 
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOPREVSOL6                                                 
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOPREVSOL7                                                 
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOPREV1                                                    
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOPREV2                                                    
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOPREV3                                                    
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOPREV4                                                    
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOPREV5                                                    
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOPREV6                                                    
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOPREV7                                                    
               PIC S9(5) COMP-3.                                                
           02  GF35-QUORECSOL1                                                  
               PIC S9(5) COMP-3.                                                
           02  GF35-QUORECSOL2                                                  
               PIC S9(5) COMP-3.                                                
           02  GF35-QUORECSOL3                                                  
               PIC S9(5) COMP-3.                                                
           02  GF35-QUORECSOL4                                                  
               PIC S9(5) COMP-3.                                                
           02  GF35-QUORECSOL5                                                  
               PIC S9(5) COMP-3.                                                
           02  GF35-QUORECSOL6                                                  
               PIC S9(5) COMP-3.                                                
           02  GF35-QUORECSOL7                                                  
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOREC1                                                     
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOREC2                                                     
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOREC3                                                     
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOREC4                                                     
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOREC5                                                     
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOREC6                                                     
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOREC7                                                     
               PIC S9(5) COMP-3.                                                
           02  GF35-QUOTA                                                       
               PIC S9(7) COMP-3.                                                
           02  GF35-QUOTA1                                                      
               PIC S9(7) COMP-3.                                                
           02  GF35-QUOTA2                                                      
               PIC S9(7) COMP-3.                                                
           02  GF35-QUOTA3                                                      
               PIC S9(7) COMP-3.                                                
           02  GF35-QUOTA4                                                      
               PIC S9(7) COMP-3.                                                
           02  GF35-QUOTA5                                                      
               PIC S9(7) COMP-3.                                                
           02  GF35-QUOTA6                                                      
               PIC S9(7) COMP-3.                                                
           02  GF35-QUOTA7                                                      
               PIC S9(7) COMP-3.                                                
           02  GF35-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGF3500                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF3500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF3500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-DANNEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-DANNEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-DSEMAINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-DSEMAINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOPREVSOL1-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOPREVSOL1-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOPREVSOL2-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOPREVSOL2-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOPREVSOL3-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOPREVSOL3-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOPREVSOL4-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOPREVSOL4-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOPREVSOL5-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOPREVSOL5-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOPREVSOL6-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOPREVSOL6-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOPREVSOL7-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOPREVSOL7-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOPREV1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOPREV1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOPREV2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOPREV2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOPREV3-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOPREV3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOPREV4-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOPREV4-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOPREV5-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOPREV5-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOPREV6-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOPREV6-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOPREV7-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOPREV7-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUORECSOL1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUORECSOL1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUORECSOL2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUORECSOL2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUORECSOL3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUORECSOL3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUORECSOL4-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUORECSOL4-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUORECSOL5-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUORECSOL5-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUORECSOL6-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUORECSOL6-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUORECSOL7-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUORECSOL7-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOREC1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOREC1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOREC2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOREC2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOREC3-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOREC3-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOREC4-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOREC4-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOREC5-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOREC5-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOREC6-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOREC6-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOREC7-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOREC7-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOTA-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOTA-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOTA1-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOTA1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOTA2-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOTA2-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOTA3-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOTA3-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOTA4-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOTA4-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOTA5-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOTA5-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOTA6-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOTA6-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-QUOTA7-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF35-QUOTA7-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF35-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GF35-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
