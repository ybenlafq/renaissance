      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGF2500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF2500                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF2500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF2500.                                                            
      *}                                                                        
           02  GF25-NCDE                                                        
               PIC X(0007).                                                     
           02  GF25-NCODIC                                                      
               PIC X(0007).                                                     
           02  GF25-CRIST                                                       
               PIC X(0007).                                                     
           02  GF25-WRIST                                                       
               PIC X(0001).                                                     
           02  GF25-PRIST                                                       
               PIC S9(5)V9(0002) COMP-3.                                        
           02  GF25-CPERRCVRT                                                   
               PIC X(0002).                                                     
           02  GF25-CMODRCVRT                                                   
               PIC X(0002).                                                     
           02  GF25-DRELANCE                                                    
               PIC X(0002).                                                     
           02  GF25-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGF2500                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF2500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF2500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF25-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF25-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF25-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF25-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF25-CRIST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF25-CRIST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF25-WRIST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF25-WRIST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF25-PRIST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF25-PRIST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF25-CPERRCVRT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF25-CPERRCVRT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF25-CMODRCVRT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF25-CMODRCVRT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF25-DRELANCE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF25-DRELANCE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF25-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GF25-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
