      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *--------------------------------------------------------                 
      * VERIFICATION E-MAIL                                                     
      * COMM DE PROGRAMME EMAIL                                                 
      * APPELLE PAR LINK DANS TBC60 TBC50                                       
      *                                                                         
      *--------------------------------------------------------                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 COMM-EMAIL-LONG-COMMAREA PIC S9(4) COMP VALUE +200.                   
      *--                                                                       
       01 COMM-EMAIL-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +200.                 
      *}                                                                        
      *----------------------------------LONGUEUR 200                           
       01 Z-COMMAREA-EMAIL.                                                     
      *DONNES COMPLEMENTAIRES EN ENTREE DE LA MODULE                            
      *                               NOM DE E-MAIL                             
             10 COMM-EMAIL-MAIL           PIC X(100).                           
      *DONNES COMPLEMENTAIRES EN SORTIE DE LA MODULE                            
      *                               CODE DE RETOUR                    00550009
             10 COMM-EMAIL-RETOUR         PIC 9(01).                            
                88 COMM-EMAIL-RETOUR-OK      VALUE 0.                           
                88 COMM-EMAIL-RETOUR-KO      VALUE 1.                           
      *                               NUMER MESSAGE ERREUR                      
             10 COMM-EMAIL-MSGERR         PIC X(80).                            
             10 FILLER                    PIC X(19).                            
      *                                                                 00550009
      ***************************************************************** 00740000
                                                                                
