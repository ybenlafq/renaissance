      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ                                                       
      *****************************************************************         
      *                                                                         
       01 WS73-MQ10.                                                            
          02 WS73-QUEUE.                                                        
             10 MQ10-CORRELID-73           PIC X(24).                           
          02 WS73-CODRET                     PIC X(02).                         
          02 WS73-MESSAGE.                                                      
             05 MESS73-ENTETE.                                                  
                10 MES73-TYPE              PIC X(03).                           
                10 MES73-NSOCMSG           PIC X(03).                           
                10 MES73-NLIEUMSG          PIC X(03).                           
                10 MES73-NSOCDST           PIC X(03).                           
                10 MES73-NLIEUDST          PIC X(03).                           
                10 MES73-NORD              PIC 9(08).                           
                10 MES73-LPROG             PIC X(10).                           
                10 MES73-DJOUR             PIC X(08).                           
                10 MES73-WSID              PIC X(10).                           
                10 MES73-USER              PIC X(10).                           
                10 MES73-CHRONO            PIC 9(07).                           
                10 MES73-NBRMSG            PIC 9(07).                           
                10 MES73-NBRENR            PIC 9(05).                           
                10 MES73-TAILLE            PIC 9(05).                           
                10 MES73-FILLER            PIC X(20).                           
      * MES73-ENTETE .................................                          
             05 MES73-ENTETE.                                                   
                10 MES73-RETOUR1           PIC X(01).                           
                10 MES73-TAG1              PIC X(03).                           
                10 MES73-NCDE              PIC X(07).                           
                10 MES73-NENTCDE           PIC X(05).                           
                10 MES73-RETOUR2           PIC X(01).                           
      * MES73-DETAIL .................................                          
             05 MES73-DETAIL.                                                   
                10 MES73-TAG2              PIC X(03).                           
                10 MES73-NCODIC            PIC X(07).                           
                10 MES73-NSOCDEPOT         PIC X(06).                           
                10 MES73-LREFFOURN         PIC X(20).                           
                10 MES73-NEAN              PIC X(13).                           
                10 MES73-QCDE              PIC 9(05).                           
                10 MES73-DLIVRAISONI       PIC X(08).                           
                10 MES73-DLIVRAISONM       PIC X(08).                           
                10 MES73-PBF               PIC 9(7)V9(2).                       
                10 MES73-PRA               PIC 9(7)V9(2).                       
                10 MES73-RETOUR3           PIC X(01).                           
                                                                                
