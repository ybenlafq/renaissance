      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF07   EGF07                                              00000020
      ***************************************************************** 00000030
       01   EGF07I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOC1L   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOC1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOC1F   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOC1I   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEU1L   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLIEU1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEU1F   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLIEU1I   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE1L   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCDE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE1F   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCDE1I   PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQCDEL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MQCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQCDEF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MQCDEI    PIC X(6).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODIC1L  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCODIC1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODIC1F  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCODIC1I  PIC X(7).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODICL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCODICF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLCODICI  PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQCLIL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MQCLIL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQCLIF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MQCLII    PIC X(6).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAM1L   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCFAM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAM1F   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCFAM1I   PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLFAMI    PIC X(20).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQMAGL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MQMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MQMAGF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MQMAGI    PIC X(6).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQ1L  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCMARQ1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQ1F  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCMARQ1I  PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLMARQI   PIC X(20).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQDISPOL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MQDISPOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQDISPOF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MQDISPOI  PIC X(6).                                       00000690
           02 MLIGNEI OCCURS   13 TIMES .                               00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODIC2L     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MCODIC2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCODIC2F     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MCODIC2I     PIC X(7).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAM2L      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCFAM2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCFAM2F      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCFAM2I      PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQ2L     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MCMARQ2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCMARQ2F     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCMARQ2I     PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFFOURNL  COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MLREFFOURNL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLREFFOURNF  PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MLREFFOURNI  PIC X(20).                                 00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENTREPOTL   COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MENTREPOTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MENTREPOTF   PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MENTREPOTI   PIC X(6).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCDE2L      COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MNCDE2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNCDE2F      PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MNCDE2I      PIC X(7).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSERVL      COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MQSERVL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQSERVF      PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQSERVI      PIC X(5).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDLIVRAISONL      COMP PIC S9(4).                       00000990
      *--                                                                       
             03 MDLIVRAISONL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 MDLIVRAISONF      PIC X.                                00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MDLIVRAISONI      PIC X(8).                             00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MZONCMDI  PIC X(15).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MLIBERRI  PIC X(58).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCODTRAI  PIC X(4).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCICSI    PIC X(5).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MNETNAMI  PIC X(8).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MSCREENI  PIC X(4).                                       00001260
      ***************************************************************** 00001270
      * SDF: EGF07   EGF07                                              00001280
      ***************************************************************** 00001290
       01   EGF07O REDEFINES EGF07I.                                    00001300
           02 FILLER    PIC X(12).                                      00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MDATJOUA  PIC X.                                          00001330
           02 MDATJOUC  PIC X.                                          00001340
           02 MDATJOUP  PIC X.                                          00001350
           02 MDATJOUH  PIC X.                                          00001360
           02 MDATJOUV  PIC X.                                          00001370
           02 MDATJOUO  PIC X(10).                                      00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MTIMJOUA  PIC X.                                          00001400
           02 MTIMJOUC  PIC X.                                          00001410
           02 MTIMJOUP  PIC X.                                          00001420
           02 MTIMJOUH  PIC X.                                          00001430
           02 MTIMJOUV  PIC X.                                          00001440
           02 MTIMJOUO  PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MWPAGEA   PIC X.                                          00001470
           02 MWPAGEC   PIC X.                                          00001480
           02 MWPAGEP   PIC X.                                          00001490
           02 MWPAGEH   PIC X.                                          00001500
           02 MWPAGEV   PIC X.                                          00001510
           02 MWPAGEO   PIC X(3).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MNSOC1A   PIC X.                                          00001540
           02 MNSOC1C   PIC X.                                          00001550
           02 MNSOC1P   PIC X.                                          00001560
           02 MNSOC1H   PIC X.                                          00001570
           02 MNSOC1V   PIC X.                                          00001580
           02 MNSOC1O   PIC X(3).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MLIEU1A   PIC X.                                          00001610
           02 MLIEU1C   PIC X.                                          00001620
           02 MLIEU1P   PIC X.                                          00001630
           02 MLIEU1H   PIC X.                                          00001640
           02 MLIEU1V   PIC X.                                          00001650
           02 MLIEU1O   PIC X(3).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MNCDE1A   PIC X.                                          00001680
           02 MNCDE1C   PIC X.                                          00001690
           02 MNCDE1P   PIC X.                                          00001700
           02 MNCDE1H   PIC X.                                          00001710
           02 MNCDE1V   PIC X.                                          00001720
           02 MNCDE1O   PIC X(7).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MQCDEA    PIC X.                                          00001750
           02 MQCDEC    PIC X.                                          00001760
           02 MQCDEP    PIC X.                                          00001770
           02 MQCDEH    PIC X.                                          00001780
           02 MQCDEV    PIC X.                                          00001790
           02 MQCDEO    PIC X(6).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MCODIC1A  PIC X.                                          00001820
           02 MCODIC1C  PIC X.                                          00001830
           02 MCODIC1P  PIC X.                                          00001840
           02 MCODIC1H  PIC X.                                          00001850
           02 MCODIC1V  PIC X.                                          00001860
           02 MCODIC1O  PIC X(7).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MLCODICA  PIC X.                                          00001890
           02 MLCODICC  PIC X.                                          00001900
           02 MLCODICP  PIC X.                                          00001910
           02 MLCODICH  PIC X.                                          00001920
           02 MLCODICV  PIC X.                                          00001930
           02 MLCODICO  PIC X(20).                                      00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MQCLIA    PIC X.                                          00001960
           02 MQCLIC    PIC X.                                          00001970
           02 MQCLIP    PIC X.                                          00001980
           02 MQCLIH    PIC X.                                          00001990
           02 MQCLIV    PIC X.                                          00002000
           02 MQCLIO    PIC X(6).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MCFAM1A   PIC X.                                          00002030
           02 MCFAM1C   PIC X.                                          00002040
           02 MCFAM1P   PIC X.                                          00002050
           02 MCFAM1H   PIC X.                                          00002060
           02 MCFAM1V   PIC X.                                          00002070
           02 MCFAM1O   PIC X(5).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLFAMA    PIC X.                                          00002100
           02 MLFAMC    PIC X.                                          00002110
           02 MLFAMP    PIC X.                                          00002120
           02 MLFAMH    PIC X.                                          00002130
           02 MLFAMV    PIC X.                                          00002140
           02 MLFAMO    PIC X(20).                                      00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MQMAGA    PIC X.                                          00002170
           02 MQMAGC    PIC X.                                          00002180
           02 MQMAGP    PIC X.                                          00002190
           02 MQMAGH    PIC X.                                          00002200
           02 MQMAGV    PIC X.                                          00002210
           02 MQMAGO    PIC X(6).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCMARQ1A  PIC X.                                          00002240
           02 MCMARQ1C  PIC X.                                          00002250
           02 MCMARQ1P  PIC X.                                          00002260
           02 MCMARQ1H  PIC X.                                          00002270
           02 MCMARQ1V  PIC X.                                          00002280
           02 MCMARQ1O  PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MLMARQA   PIC X.                                          00002310
           02 MLMARQC   PIC X.                                          00002320
           02 MLMARQP   PIC X.                                          00002330
           02 MLMARQH   PIC X.                                          00002340
           02 MLMARQV   PIC X.                                          00002350
           02 MLMARQO   PIC X(20).                                      00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MQDISPOA  PIC X.                                          00002380
           02 MQDISPOC  PIC X.                                          00002390
           02 MQDISPOP  PIC X.                                          00002400
           02 MQDISPOH  PIC X.                                          00002410
           02 MQDISPOV  PIC X.                                          00002420
           02 MQDISPOO  PIC X(6).                                       00002430
           02 MLIGNEO OCCURS   13 TIMES .                               00002440
             03 FILLER       PIC X(2).                                  00002450
             03 MCODIC2A     PIC X.                                     00002460
             03 MCODIC2C     PIC X.                                     00002470
             03 MCODIC2P     PIC X.                                     00002480
             03 MCODIC2H     PIC X.                                     00002490
             03 MCODIC2V     PIC X.                                     00002500
             03 MCODIC2O     PIC X(7).                                  00002510
             03 FILLER       PIC X(2).                                  00002520
             03 MCFAM2A      PIC X.                                     00002530
             03 MCFAM2C PIC X.                                          00002540
             03 MCFAM2P PIC X.                                          00002550
             03 MCFAM2H PIC X.                                          00002560
             03 MCFAM2V PIC X.                                          00002570
             03 MCFAM2O      PIC X(5).                                  00002580
             03 FILLER       PIC X(2).                                  00002590
             03 MCMARQ2A     PIC X.                                     00002600
             03 MCMARQ2C     PIC X.                                     00002610
             03 MCMARQ2P     PIC X.                                     00002620
             03 MCMARQ2H     PIC X.                                     00002630
             03 MCMARQ2V     PIC X.                                     00002640
             03 MCMARQ2O     PIC X(5).                                  00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MLREFFOURNA  PIC X.                                     00002670
             03 MLREFFOURNC  PIC X.                                     00002680
             03 MLREFFOURNP  PIC X.                                     00002690
             03 MLREFFOURNH  PIC X.                                     00002700
             03 MLREFFOURNV  PIC X.                                     00002710
             03 MLREFFOURNO  PIC X(20).                                 00002720
             03 FILLER       PIC X(2).                                  00002730
             03 MENTREPOTA   PIC X.                                     00002740
             03 MENTREPOTC   PIC X.                                     00002750
             03 MENTREPOTP   PIC X.                                     00002760
             03 MENTREPOTH   PIC X.                                     00002770
             03 MENTREPOTV   PIC X.                                     00002780
             03 MENTREPOTO   PIC X(6).                                  00002790
             03 FILLER       PIC X(2).                                  00002800
             03 MNCDE2A      PIC X.                                     00002810
             03 MNCDE2C PIC X.                                          00002820
             03 MNCDE2P PIC X.                                          00002830
             03 MNCDE2H PIC X.                                          00002840
             03 MNCDE2V PIC X.                                          00002850
             03 MNCDE2O      PIC X(7).                                  00002860
             03 FILLER       PIC X(2).                                  00002870
             03 MQSERVA      PIC X.                                     00002880
             03 MQSERVC PIC X.                                          00002890
             03 MQSERVP PIC X.                                          00002900
             03 MQSERVH PIC X.                                          00002910
             03 MQSERVV PIC X.                                          00002920
             03 MQSERVO      PIC X(5).                                  00002930
             03 FILLER       PIC X(2).                                  00002940
             03 MDLIVRAISONA      PIC X.                                00002950
             03 MDLIVRAISONC PIC X.                                     00002960
             03 MDLIVRAISONP PIC X.                                     00002970
             03 MDLIVRAISONH PIC X.                                     00002980
             03 MDLIVRAISONV PIC X.                                     00002990
             03 MDLIVRAISONO      PIC X(8).                             00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MZONCMDA  PIC X.                                          00003020
           02 MZONCMDC  PIC X.                                          00003030
           02 MZONCMDP  PIC X.                                          00003040
           02 MZONCMDH  PIC X.                                          00003050
           02 MZONCMDV  PIC X.                                          00003060
           02 MZONCMDO  PIC X(15).                                      00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MLIBERRA  PIC X.                                          00003090
           02 MLIBERRC  PIC X.                                          00003100
           02 MLIBERRP  PIC X.                                          00003110
           02 MLIBERRH  PIC X.                                          00003120
           02 MLIBERRV  PIC X.                                          00003130
           02 MLIBERRO  PIC X(58).                                      00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MCODTRAA  PIC X.                                          00003160
           02 MCODTRAC  PIC X.                                          00003170
           02 MCODTRAP  PIC X.                                          00003180
           02 MCODTRAH  PIC X.                                          00003190
           02 MCODTRAV  PIC X.                                          00003200
           02 MCODTRAO  PIC X(4).                                       00003210
           02 FILLER    PIC X(2).                                       00003220
           02 MCICSA    PIC X.                                          00003230
           02 MCICSC    PIC X.                                          00003240
           02 MCICSP    PIC X.                                          00003250
           02 MCICSH    PIC X.                                          00003260
           02 MCICSV    PIC X.                                          00003270
           02 MCICSO    PIC X(5).                                       00003280
           02 FILLER    PIC X(2).                                       00003290
           02 MNETNAMA  PIC X.                                          00003300
           02 MNETNAMC  PIC X.                                          00003310
           02 MNETNAMP  PIC X.                                          00003320
           02 MNETNAMH  PIC X.                                          00003330
           02 MNETNAMV  PIC X.                                          00003340
           02 MNETNAMO  PIC X(8).                                       00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MSCREENA  PIC X.                                          00003370
           02 MSCREENC  PIC X.                                          00003380
           02 MSCREENP  PIC X.                                          00003390
           02 MSCREENH  PIC X.                                          00003400
           02 MSCREENV  PIC X.                                          00003410
           02 MSCREENO  PIC X(4).                                       00003420
                                                                                
