      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF34   EGF34                                              00000020
      ***************************************************************** 00000030
       01   EGF34I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLREFI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLFAMI    PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEBL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDATDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDEBF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDATDEBI  PIC X(8).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFINL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDATFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFINF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDATFINI  PIC X(8).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCMARQI   PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLMARQI   PIC X(20).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETEL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MSOCIETEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCIETEF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSOCIETEI      PIC X(3).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE1L  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLIGNE1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIGNE1F  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLIGNE1I  PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATE1L   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MDATE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATE1F   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDATE1I   PIC X(8).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATE3L   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MDATE3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATE3F   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDATE3I   PIC X(8).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATE5L   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MDATE5L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATE5F   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MDATE5I   PIC X(8).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE2L  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLIGNE2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIGNE2F  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLIGNE2I  PIC X(3).                                       00000770
           02 M244I OCCURS   12 TIMES .                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTATUTL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MSTATUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTATUTF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MSTATUTI     PIC X(3).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCDEL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MNCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNCDEF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MNCDEI  PIC X(7).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATSAIL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MDATSAIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATSAIF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MDATSAII     PIC X(6).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEPOTL      COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MDEPOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDEPOTF      PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MDEPOTI      PIC X(3).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTECOML     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MQTECOML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTECOMF     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQTECOMI     PIC X(5).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTERECL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MQTERECL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTERECF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MQTERECI     PIC X(5).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTESOLL     COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MQTESOLL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTESOLF     PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MQTESOLI     PIC X(5).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE1L  COMP PIC S9(4).                                 00001070
      *--                                                                       
             03 MQTE1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE1F  PIC X.                                          00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MQTE1I  PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE2L  COMP PIC S9(4).                                 00001110
      *--                                                                       
             03 MQTE2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE2F  PIC X.                                          00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MQTE2I  PIC X(5).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE3L  COMP PIC S9(4).                                 00001150
      *--                                                                       
             03 MQTE3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE3F  PIC X.                                          00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MQTE3I  PIC X(5).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE4L  COMP PIC S9(4).                                 00001190
      *--                                                                       
             03 MQTE4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE4F  PIC X.                                          00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MQTE4I  PIC X(5).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE5L  COMP PIC S9(4).                                 00001230
      *--                                                                       
             03 MQTE5L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE5F  PIC X.                                          00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MQTE5I  PIC X(5).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MZONCMDI  PIC X(15).                                      00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MLIBERRI  PIC X(58).                                      00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MCODTRAI  PIC X(4).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MCICSI    PIC X(5).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MNETNAMI  PIC X(8).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MSCREENI  PIC X(4).                                       00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATE2L   COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MDATE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATE2F   PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MDATE2I   PIC X(8).                                       00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATE4L   COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MDATE4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATE4F   PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MDATE4I   PIC X(8).                                       00001580
      ***************************************************************** 00001590
      * SDF: EGF34   EGF34                                              00001600
      ***************************************************************** 00001610
       01   EGF34O REDEFINES EGF34I.                                    00001620
           02 FILLER    PIC X(12).                                      00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MDATJOUA  PIC X.                                          00001650
           02 MDATJOUC  PIC X.                                          00001660
           02 MDATJOUP  PIC X.                                          00001670
           02 MDATJOUH  PIC X.                                          00001680
           02 MDATJOUV  PIC X.                                          00001690
           02 MDATJOUO  PIC X(10).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MTIMJOUA  PIC X.                                          00001720
           02 MTIMJOUC  PIC X.                                          00001730
           02 MTIMJOUP  PIC X.                                          00001740
           02 MTIMJOUH  PIC X.                                          00001750
           02 MTIMJOUV  PIC X.                                          00001760
           02 MTIMJOUO  PIC X(5).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MWPAGEA   PIC X.                                          00001790
           02 MWPAGEC   PIC X.                                          00001800
           02 MWPAGEP   PIC X.                                          00001810
           02 MWPAGEH   PIC X.                                          00001820
           02 MWPAGEV   PIC X.                                          00001830
           02 MWPAGEO   PIC X(3).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MWFONCA   PIC X.                                          00001860
           02 MWFONCC   PIC X.                                          00001870
           02 MWFONCP   PIC X.                                          00001880
           02 MWFONCH   PIC X.                                          00001890
           02 MWFONCV   PIC X.                                          00001900
           02 MWFONCO   PIC X(3).                                       00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MNCODICA  PIC X.                                          00001930
           02 MNCODICC  PIC X.                                          00001940
           02 MNCODICP  PIC X.                                          00001950
           02 MNCODICH  PIC X.                                          00001960
           02 MNCODICV  PIC X.                                          00001970
           02 MNCODICO  PIC X(7).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MLREFA    PIC X.                                          00002000
           02 MLREFC    PIC X.                                          00002010
           02 MLREFP    PIC X.                                          00002020
           02 MLREFH    PIC X.                                          00002030
           02 MLREFV    PIC X.                                          00002040
           02 MLREFO    PIC X(20).                                      00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MCFAMA    PIC X.                                          00002070
           02 MCFAMC    PIC X.                                          00002080
           02 MCFAMP    PIC X.                                          00002090
           02 MCFAMH    PIC X.                                          00002100
           02 MCFAMV    PIC X.                                          00002110
           02 MCFAMO    PIC X(5).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MLFAMA    PIC X.                                          00002140
           02 MLFAMC    PIC X.                                          00002150
           02 MLFAMP    PIC X.                                          00002160
           02 MLFAMH    PIC X.                                          00002170
           02 MLFAMV    PIC X.                                          00002180
           02 MLFAMO    PIC X(20).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MDATDEBA  PIC X.                                          00002210
           02 MDATDEBC  PIC X.                                          00002220
           02 MDATDEBP  PIC X.                                          00002230
           02 MDATDEBH  PIC X.                                          00002240
           02 MDATDEBV  PIC X.                                          00002250
           02 MDATDEBO  PIC X(8).                                       00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MDATFINA  PIC X.                                          00002280
           02 MDATFINC  PIC X.                                          00002290
           02 MDATFINP  PIC X.                                          00002300
           02 MDATFINH  PIC X.                                          00002310
           02 MDATFINV  PIC X.                                          00002320
           02 MDATFINO  PIC X(8).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCMARQA   PIC X.                                          00002350
           02 MCMARQC   PIC X.                                          00002360
           02 MCMARQP   PIC X.                                          00002370
           02 MCMARQH   PIC X.                                          00002380
           02 MCMARQV   PIC X.                                          00002390
           02 MCMARQO   PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MLMARQA   PIC X.                                          00002420
           02 MLMARQC   PIC X.                                          00002430
           02 MLMARQP   PIC X.                                          00002440
           02 MLMARQH   PIC X.                                          00002450
           02 MLMARQV   PIC X.                                          00002460
           02 MLMARQO   PIC X(20).                                      00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSOCIETEA      PIC X.                                     00002490
           02 MSOCIETEC PIC X.                                          00002500
           02 MSOCIETEP PIC X.                                          00002510
           02 MSOCIETEH PIC X.                                          00002520
           02 MSOCIETEV PIC X.                                          00002530
           02 MSOCIETEO      PIC X(3).                                  00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MLIGNE1A  PIC X.                                          00002560
           02 MLIGNE1C  PIC X.                                          00002570
           02 MLIGNE1P  PIC X.                                          00002580
           02 MLIGNE1H  PIC X.                                          00002590
           02 MLIGNE1V  PIC X.                                          00002600
           02 MLIGNE1O  PIC X(3).                                       00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MDATE1A   PIC X.                                          00002630
           02 MDATE1C   PIC X.                                          00002640
           02 MDATE1P   PIC X.                                          00002650
           02 MDATE1H   PIC X.                                          00002660
           02 MDATE1V   PIC X.                                          00002670
           02 MDATE1O   PIC X(8).                                       00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MDATE3A   PIC X.                                          00002700
           02 MDATE3C   PIC X.                                          00002710
           02 MDATE3P   PIC X.                                          00002720
           02 MDATE3H   PIC X.                                          00002730
           02 MDATE3V   PIC X.                                          00002740
           02 MDATE3O   PIC X(8).                                       00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MDATE5A   PIC X.                                          00002770
           02 MDATE5C   PIC X.                                          00002780
           02 MDATE5P   PIC X.                                          00002790
           02 MDATE5H   PIC X.                                          00002800
           02 MDATE5V   PIC X.                                          00002810
           02 MDATE5O   PIC X(8).                                       00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MLIGNE2A  PIC X.                                          00002840
           02 MLIGNE2C  PIC X.                                          00002850
           02 MLIGNE2P  PIC X.                                          00002860
           02 MLIGNE2H  PIC X.                                          00002870
           02 MLIGNE2V  PIC X.                                          00002880
           02 MLIGNE2O  PIC X(3).                                       00002890
           02 M244O OCCURS   12 TIMES .                                 00002900
             03 FILLER       PIC X(2).                                  00002910
             03 MSTATUTA     PIC X.                                     00002920
             03 MSTATUTC     PIC X.                                     00002930
             03 MSTATUTP     PIC X.                                     00002940
             03 MSTATUTH     PIC X.                                     00002950
             03 MSTATUTV     PIC X.                                     00002960
             03 MSTATUTO     PIC X(3).                                  00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MNCDEA  PIC X.                                          00002990
             03 MNCDEC  PIC X.                                          00003000
             03 MNCDEP  PIC X.                                          00003010
             03 MNCDEH  PIC X.                                          00003020
             03 MNCDEV  PIC X.                                          00003030
             03 MNCDEO  PIC X(7).                                       00003040
             03 FILLER       PIC X(2).                                  00003050
             03 MDATSAIA     PIC X.                                     00003060
             03 MDATSAIC     PIC X.                                     00003070
             03 MDATSAIP     PIC X.                                     00003080
             03 MDATSAIH     PIC X.                                     00003090
             03 MDATSAIV     PIC X.                                     00003100
             03 MDATSAIO     PIC X(6).                                  00003110
             03 FILLER       PIC X(2).                                  00003120
             03 MDEPOTA      PIC X.                                     00003130
             03 MDEPOTC PIC X.                                          00003140
             03 MDEPOTP PIC X.                                          00003150
             03 MDEPOTH PIC X.                                          00003160
             03 MDEPOTV PIC X.                                          00003170
             03 MDEPOTO      PIC X(3).                                  00003180
             03 FILLER       PIC X(2).                                  00003190
             03 MQTECOMA     PIC X.                                     00003200
             03 MQTECOMC     PIC X.                                     00003210
             03 MQTECOMP     PIC X.                                     00003220
             03 MQTECOMH     PIC X.                                     00003230
             03 MQTECOMV     PIC X.                                     00003240
             03 MQTECOMO     PIC X(5).                                  00003250
             03 FILLER       PIC X(2).                                  00003260
             03 MQTERECA     PIC X.                                     00003270
             03 MQTERECC     PIC X.                                     00003280
             03 MQTERECP     PIC X.                                     00003290
             03 MQTERECH     PIC X.                                     00003300
             03 MQTERECV     PIC X.                                     00003310
             03 MQTERECO     PIC X(5).                                  00003320
             03 FILLER       PIC X(2).                                  00003330
             03 MQTESOLA     PIC X.                                     00003340
             03 MQTESOLC     PIC X.                                     00003350
             03 MQTESOLP     PIC X.                                     00003360
             03 MQTESOLH     PIC X.                                     00003370
             03 MQTESOLV     PIC X.                                     00003380
             03 MQTESOLO     PIC X(5).                                  00003390
             03 FILLER       PIC X(2).                                  00003400
             03 MQTE1A  PIC X.                                          00003410
             03 MQTE1C  PIC X.                                          00003420
             03 MQTE1P  PIC X.                                          00003430
             03 MQTE1H  PIC X.                                          00003440
             03 MQTE1V  PIC X.                                          00003450
             03 MQTE1O  PIC X(5).                                       00003460
             03 FILLER       PIC X(2).                                  00003470
             03 MQTE2A  PIC X.                                          00003480
             03 MQTE2C  PIC X.                                          00003490
             03 MQTE2P  PIC X.                                          00003500
             03 MQTE2H  PIC X.                                          00003510
             03 MQTE2V  PIC X.                                          00003520
             03 MQTE2O  PIC X(5).                                       00003530
             03 FILLER       PIC X(2).                                  00003540
             03 MQTE3A  PIC X.                                          00003550
             03 MQTE3C  PIC X.                                          00003560
             03 MQTE3P  PIC X.                                          00003570
             03 MQTE3H  PIC X.                                          00003580
             03 MQTE3V  PIC X.                                          00003590
             03 MQTE3O  PIC X(5).                                       00003600
             03 FILLER       PIC X(2).                                  00003610
             03 MQTE4A  PIC X.                                          00003620
             03 MQTE4C  PIC X.                                          00003630
             03 MQTE4P  PIC X.                                          00003640
             03 MQTE4H  PIC X.                                          00003650
             03 MQTE4V  PIC X.                                          00003660
             03 MQTE4O  PIC X(5).                                       00003670
             03 FILLER       PIC X(2).                                  00003680
             03 MQTE5A  PIC X.                                          00003690
             03 MQTE5C  PIC X.                                          00003700
             03 MQTE5P  PIC X.                                          00003710
             03 MQTE5H  PIC X.                                          00003720
             03 MQTE5V  PIC X.                                          00003730
             03 MQTE5O  PIC X(5).                                       00003740
           02 FILLER    PIC X(2).                                       00003750
           02 MZONCMDA  PIC X.                                          00003760
           02 MZONCMDC  PIC X.                                          00003770
           02 MZONCMDP  PIC X.                                          00003780
           02 MZONCMDH  PIC X.                                          00003790
           02 MZONCMDV  PIC X.                                          00003800
           02 MZONCMDO  PIC X(15).                                      00003810
           02 FILLER    PIC X(2).                                       00003820
           02 MLIBERRA  PIC X.                                          00003830
           02 MLIBERRC  PIC X.                                          00003840
           02 MLIBERRP  PIC X.                                          00003850
           02 MLIBERRH  PIC X.                                          00003860
           02 MLIBERRV  PIC X.                                          00003870
           02 MLIBERRO  PIC X(58).                                      00003880
           02 FILLER    PIC X(2).                                       00003890
           02 MCODTRAA  PIC X.                                          00003900
           02 MCODTRAC  PIC X.                                          00003910
           02 MCODTRAP  PIC X.                                          00003920
           02 MCODTRAH  PIC X.                                          00003930
           02 MCODTRAV  PIC X.                                          00003940
           02 MCODTRAO  PIC X(4).                                       00003950
           02 FILLER    PIC X(2).                                       00003960
           02 MCICSA    PIC X.                                          00003970
           02 MCICSC    PIC X.                                          00003980
           02 MCICSP    PIC X.                                          00003990
           02 MCICSH    PIC X.                                          00004000
           02 MCICSV    PIC X.                                          00004010
           02 MCICSO    PIC X(5).                                       00004020
           02 FILLER    PIC X(2).                                       00004030
           02 MNETNAMA  PIC X.                                          00004040
           02 MNETNAMC  PIC X.                                          00004050
           02 MNETNAMP  PIC X.                                          00004060
           02 MNETNAMH  PIC X.                                          00004070
           02 MNETNAMV  PIC X.                                          00004080
           02 MNETNAMO  PIC X(8).                                       00004090
           02 FILLER    PIC X(2).                                       00004100
           02 MSCREENA  PIC X.                                          00004110
           02 MSCREENC  PIC X.                                          00004120
           02 MSCREENP  PIC X.                                          00004130
           02 MSCREENH  PIC X.                                          00004140
           02 MSCREENV  PIC X.                                          00004150
           02 MSCREENO  PIC X(4).                                       00004160
           02 FILLER    PIC X(2).                                       00004170
           02 MDATE2A   PIC X.                                          00004180
           02 MDATE2C   PIC X.                                          00004190
           02 MDATE2P   PIC X.                                          00004200
           02 MDATE2H   PIC X.                                          00004210
           02 MDATE2V   PIC X.                                          00004220
           02 MDATE2O   PIC X(8).                                       00004230
           02 FILLER    PIC X(2).                                       00004240
           02 MDATE4A   PIC X.                                          00004250
           02 MDATE4C   PIC X.                                          00004260
           02 MDATE4P   PIC X.                                          00004270
           02 MDATE4H   PIC X.                                          00004280
           02 MDATE4V   PIC X.                                          00004290
           02 MDATE4O   PIC X(8).                                       00004300
                                                                                
