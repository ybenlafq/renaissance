      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      *                                                                         
      * TS TGF47B : QUOTA PLANNING RAYON                                        
      *                                                                         
       01  TS-GF47B.                                                            
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03  TS-GF47B-LONG             PIC  S9(4) COMP  VALUE +63.            
      *--                                                                       
           03  TS-GF47B-LONG             PIC  S9(4) COMP-5  VALUE +63.          
      *}                                                                        
      *                                                                         
           03  TS-GF47B-DONNEES.                                                
      *                                                                         
      *      05 TS-GF47B-TABLEAU           OCCURS 5.                            
                10 TS-GF47B-DATA           OCCURS 7.                            
                   15 TS-GF47B-QPREV     PIC S9(5) COMP-3.                      
                   15 TS-GF47B-QPRIS     PIC S9(5) COMP-3.                      
                   15 TS-GF47B-QSOLDE    PIC S9(5) COMP-3.                      
      *                                                                         
                                                                                
