      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                         
      * TS TGF11 : LISTE DES COMMANDES FOURNISSEURS EN INSTANCE                 
      *                                                                         
       01  TS-GF11.                                                             
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    03  TS-GF11-LONG             PIC  S9(4) COMP  VALUE +53.             
      *--                                                                       
           03  TS-GF11-LONG             PIC  S9(4) COMP-5  VALUE +53.           
      *}                                                                        
      *                                                                         
           03  TS-GF11-DONNEES.                                                 
      *                                                                         
               05   TS-GF11-NENTCDE     PIC  X(5).                              
               05   TS-GF11-CINTERLOCUT PIC  X(5).                              
               05   TS-GF11-LENTCDE     PIC  X(20).                             
               05   TS-GF11-LINTERLOCUT PIC  X(20).                             
               05   TS-GF11-COUNT       PIC  S9(5) COMP-3.                      
      *                                                                         
                                                                                
