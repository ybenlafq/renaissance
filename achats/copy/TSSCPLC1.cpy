      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 04/09/2016 1        
                                                                                
      ****************************************************************          
      *                                                              *          
      *TOP SECRET SECURITY CICS PARAMETER LIST.       V4L4           *          
      *                                                              *          
      *NOTE FIELD TSSHEAD MUST HAVE THE VALUE 'TCPLV4L4'             *          
      *04/29/94 - UPDATED BY QA, SEE CONTACT 2402388 AND 2435039.    *          
      ****************************************************************          
       01  TSSCPL.                                                              
           02  TSSHEAD        PICTURE X(8).                                     
           02  TSSCLASS       PICTURE X(8).                                     
           02  TSSRNAME       PICTURE X(44).                                    
           02  TSSPPGM        PICTURE X(8).                                     
           02  TSSACC         PICTURE X(8).                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TSSRC          PICTURE S9(4) COMPUTATIONAL.                      
      *--                                                                       
           02  TSSRC          PICTURE S9(4) COMP-5.                             
      *}                                                                        
               88  TSSROK     VALUE +00.                                        
               88  TSSRND     VALUE +04.                                        
               88  TSSRNA     VALUE +08.                                        
               88  TSSRIPL    VALUE +12.                                        
               88  TSSRENV    VALUE +16.                                        
               88  TSSRINAC   VALUE +20.                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TSSSTAT        PICTURE S9(4) COMPUTATIONAL.                      
      *--                                                                       
           02  TSSSTAT        PICTURE S9(4) COMP-5.                             
      *}                                                                        
               88  TSSSDEF    VALUE +00.                                        
               88  TSSSUND    VALUE +04.                                        
               88  TSSSNSO    VALUE +08.                                        
               88  TSSSIDT    VALUE +12.                                        
           02  TSSCRC         PICTURE X(2).                                     
           02  TSSCSTAT       PICTURE X(2).                                     
           02  TSSCACEE       PICTURE X(4).                                     
           02  TSSVOL         PICTURE X(6).                                     
           02  TSSLOG         PICTURE X.                                        
           02  FILLER         PICTURE X(19).                                    
           02  TSSRTN.                                                          
               05  TSSFACX    OCCURS 128 TIMES                                  
                              PICTURE X(8).                                     
           02  FILLER         REDEFINES TSSRTN.                                 
               05  TSSACIDA   PICTURE X(8).                                     
               05  TSSFAC     PICTURE X(8).                                     
               05  TSSMODE    PICTURE X(8).                                     
               05  TSSTYPE    PICTURE X(8).                                     
               05  TSSTERM    PICTURE X(8).                                     
               05  TSSSYS     PICTURE X(8).                                     
               05  TSSACIDF   PICTURE X(32).                                    
               05  TSSDEPTA   PICTURE X(8).                                     
               05  TSSDEPTF   PICTURE X(32).                                    
               05  TSSDIVA    PICTURE X(8).                                     
               05  TSSDIVF    PICTURE X(32).                                    
               05  TSSZONEA   PICTURE X(8).                                     
               05  TSSZONEF   PICTURE X(32).                                    
               05  FILLER     PICTURE X(824).                                   
           02  FILLER         REDEFINES TSSRTN.                                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05  TSSRESFLDS PICTURE S9(9) COMPUTATIONAL.                      
      *--                                                                       
               05  TSSRESFLDS PICTURE S9(9) COMP-5.                             
      *}                                                                        
               05  TSSRESDATA PICTURE X(1020).                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TSSPLEN            PICTURE S9(4) COMPUTATIONAL VALUE +370.           
      *--                                                                       
       01  TSSPLEN            PICTURE S9(4) COMP-5 VALUE +370.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TSSPLEN2           PICTURE S9(4) COMPUTATIONAL VALUE +1138.          
      *--                                                                       
       01  TSSPLEN2           PICTURE S9(4) COMP-5 VALUE +1138.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TSSRES-INDEX       PICTURE S9(4) COMPUTATIONAL.                      
      *--                                                                       
       01  TSSRES-INDEX       PICTURE S9(4) COMP-5.                             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TSSRES-PTR         PICTURE S9(4) COMPUTATIONAL.                      
      *--                                                                       
       01  TSSRES-PTR         PICTURE S9(4) COMP-5.                             
      *}                                                                        
       01  TSSRES-ENTRY.                                                        
           02  TSSRES-LNTH-X   PICTURE X(2).                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TSSRES-LNTH     REDEFINES TSSRES-LNTH-X                          
      *                        PICTURE S9(4) COMPUTATIONAL.                     
      *--                                                                       
           02  TSSRES-LNTH     REDEFINES TSSRES-LNTH-X                          
                               PICTURE S9(4) COMP-5.                            
      *}                                                                        
           02  TSSRES-FLAG     PICTURE X.                                       
               88  TSSRES-NGNRC             VALUE LOW-VALUE.                    
           02  TSSRES-RSRC.                                                     
               05  FILLER     PICTURE X(1)                                      
                              OCCURS 1 TO 44 TIMES                              
                              DEPENDING ON TSSRES-LNTH.                         
      *                                                                         
      * DESCRIPTION USER-AREA-FOR-INSTALLATION-DATA   - DARTY NCP               
      *                                                                         
       01  TSS-AREA-INST.                                                       
           02  TSS-PRT1-COD       PIC X(004).                                   
           02  TSS-PRT1-TYP       PIC X(001).                                   
           02  TSS-AREA-FIL       PIC X(251).                                   
      *------------------------------------------------------*                  
      * ===> ATTENTION <===                                  *                  
      * CETTE DESCRIPTION COBOL-TSSCPLC A UNE CORRESPONDANCE *                  
      * PL1-TSSCPLP                                          *                  
      * TOUTES LES MODIFICATIONS DOIVENT ETRE RECIPROQUES.   *                  
      *------------------------------------------------------*                  
                                                                                
