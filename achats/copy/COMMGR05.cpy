      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************00000010
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *00000020
      ******************************************************************00000030
      *                                                                *00000040
      *  PROJET     :                                                   00000050
      *  PROGRAMME  : MGR05                                            *00000060
      *  TITRE      : COMMAREA DES VALIDATIONS DE TRANSIT RECEPTION    *00000070
      *  LONGUEUR   : 4096C                                            *00000100
      *                                                                *00000110
      ******************************************************************00000120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-GR05-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                  
      *--                                                                       
       01  COMM-GR05-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                
      *}                                                                        
       01  COMM-GR05-APPLI.                                             00000130
                05 COMM-GR05-ZONES-ENTREE.                              00000140
                  10 COMM-GR05-OPTION        PIC X.                             
                  10 COMM-GR05-CACID         PIC X(08).                         
                  10 COMM-GR05-SSAAMMJJ      PIC X(08).                         
                  10 COMM-GR05-NMUTATION     PIC X(07).                 00000200
                  10 COMM-GR05-TRANSIT       PIC X.                     00000200
                  10 COMM-GR05-TYPPRET       PIC X.                             
                05 COMM-GR05-ZONES-SORTIE.                              00000220
                  10 COMM-GR05-CODRET        PIC X(02).                 00000230
                  10 COMM-GR05-MESSAGE       PIC X(80).                 00000231
                05 COMM-GR05-FILLER          PIC X(3988).               00000240
                                                                                
