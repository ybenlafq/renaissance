      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      * COMMAREA SPECIFIQUE MODULE D'ENVOI DONN�ES DE NCG VERS JDA *            
      **************************************************************            
      *                                                                         
      *   PROGRAMME MGF70                                                       
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 COMM-MGF70-LONG-COMMAREA        PIC S9(4) COMP VALUE +900.            
      *--                                                                       
       01 COMM-MGF70-LONG-COMMAREA        PIC S9(4) COMP-5 VALUE +900.          
      *}                                                                        
       01 COMM-MGF70-APPLI.                                                     
      *                                                                         
          02 COMM-MGF70-ENTREE.                                                 
      *------------------------------------------------------------17           
             05 COMM-MGF70-PGRM          PIC X(5).                              
             05 COMM-MGF70-WFONC         PIC X(3).                              
             05 COMM-MGF70-NSURCDE       PIC X(7).                              
             05 COMM-MGF70-DSAISIE       PIC X(8).                              
             05 COMM-MGF70-NENTCDE       PIC X(5).                              
             05 COMM-MGF70-MULTI         PIC X(1).                              
             05 COMM-MGF70-NCDE1         PIC X(7).                              
             05 COMM-MGF70-CQUALIF       PIC X(6).                              
             05 COMM-MGF70-CTYPCDE       PIC X(5).                              
             05 COMM-MGF70-CENVOYEE      PIC X(1).                              
             05 COMM-MGF70-CPT-SAMEDI    PIC 9(1).                              
      *                                                                         
          02 COMM-MGF70-SORTIE.                                                 
             05 COMM-MGF70-FNREDI                   PIC X(1).                   
             05 COMM-MGF70-FLAG-SAM                 PIC X(1).                   
      *            MESSAGE DE SORTIE ---------------------------- 62            
             05 COMM-MGF70-MESSAGE.                                             
                  10 COMM-MGF70-CODRET              PIC X(1).                   
                     88 COMM-MGF70-OK               VALUE ' '.                  
                     88 COMM-MGF70-ERR-NBLOC        VALUE '0'.                  
                     88 COMM-MGF70-ERR              VALUE '1'.                  
                     88 COMM-MGF70-EDI-NOK          VALUE '2'.                  
      *           10 COMM-MGF70-NSEQERR             PIC X(4).                   
                  10 COMM-MGF70-LIBERR              PIC X(58).                  
                  10 FILLER                         PIC X(3).                   
E0247.    02 COMM-MGF70-FLAG-ORDERS.                                            
             05 COMM-MGF70-ORDERS                   PIC X.                      
             05 COMM-MGF70-ORDCHG                   PIC X.                      
E0278     02 COMM-MGF70-CDEMONOCD                   PIC X.                      
E0278     02  FILLER                                PIC X(21).                  
E0278 *   02  FILLER                                PIC X(22).                  
.E0247*   02  FILLER                                PIC X(24).                  
      *                                                                         
                                                                                
