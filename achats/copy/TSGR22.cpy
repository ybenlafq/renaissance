      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   TS : RECEPTIONS A QUAI PAR ARTICLE                          *         
      *        POUR MISE A JOUR VUE RVGR0500                          *         
      *****************************************************************         
      *                                                               *         
       01  TS-GR22.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-GR22-LONG                 PIC S9(3) COMP-3                     
                                           VALUE +91.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-GR22-DONNEES.                                                  
      *----------------------------------  MODE DE MAJ                          
      *                                         ' ' : PAS DE MAJ                
      *                                         'C' : CREATION                  
      *                                         'M' : MODIFICATION              
              03 TS-GR22-CMAJ              PIC X(1).                            
      *----------------------------------  N� RECEPTION A QUAI                  
              03 TS-GR22-NRECQUAI          PIC X(7).                            
      *----------------------------------  CODIC                                
              03 TS-GR22-NCODIC            PIC X(7).                            
      *----------------------------------  DATE RECEPTION A QUAI                
              03 TS-GR22-DJRECQUAI         PIC X(8).                            
      *----------------------------------  QUANTITE RECEPTION A QUAI            
              03 TS-GR22-QRECQUAI          PIC S9(5) COMP-3.                    
      *----------------------------------  QUANTIT� COLISAGE                    
              03 TS-GR22-QCONDT            PIC S9(5) COMP-3.                    
      *----------------------------------  LIBELLE EMBALLAGE CODIC              
              03 TS-GR22-LEMBAL            PIC X(50).                           
      *----------------------------------  CODE MARQUE CODIC                    
              03 TS-GR22-CMARQ             PIC X(05).                           
      *----------------------------------  CODE FAMILLECODIC                    
              03 TS-GR22-CFAM              PIC X(05).                           
                                                                                
