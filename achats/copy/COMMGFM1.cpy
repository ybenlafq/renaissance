      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************            
      *  COMMAREA SPECIFIQUE MODULE DE MAJ DE LA TABLE RTGF36      *            
      **************************************************************            
      *                                                                         
      * PROGRAMME  MGF10                                                        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-GFM1-LONG-COMMAREA PIC S9(4) COMP VALUE +118.                   
      *--                                                                       
       01  COMM-GFM1-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +118.                 
      *}                                                                        
       01  Z-COMMAREA-GFM1.                                                     
      *---------------------------------------ZONES EN ENTREE DU MODULE         
           02 COMM-GFM1-I.                                                      
      *                                                                         
              03 COMM-GFM1-NSOCDEPOT       PIC    X(3).                         
              03 COMM-GFM1-NDEPOT          PIC    X(3).                         
              03 COMM-GFM1-NCODIC          PIC    X(7).                         
              03 COMM-GFM1-CQUOTA1         PIC    X(5).                         
              03 COMM-GFM1-CQUOTA2         PIC    X(5).                         
              03 COMM-GFM1-CMODSTOCK-ORIG  PIC    X(5).                         
              03 COMM-GFM1-CMODSTOCK       PIC    X(5).                         
              03 COMM-GFM1-QNBPVSOL        PIC    9(3) COMP-3.                  
              03 COMM-GFM1-QCOLIRECEPT     PIC    9(5) COMP-3.                  
              03 COMM-GFM1-QNBPRACK        PIC    9(5) COMP-3.                  
              03 COMM-GFM1-QNBPVSOL-ORIG   PIC    9(3) COMP-3.                  
              03 COMM-GFM1-QCOLIRECEPT-ORIG PIC    9(5) COMP-3.                 
              03 COMM-GFM1-QNBPRACK-ORIG   PIC    9(5) COMP-3.                  
      *-------------------------------------- M : MODIF, C : CONTROLE           
              03 COMM-GFM1-CODE            PIC    X(1).                         
      *-------------------------------------- DATE SOUS FORME SSAAMMJJ          
              03 COMM-GFM1-DATE            PIC    X(8).                         
      *---------------------------------------ZONES EN SORTIE DU MODULE         
           02 COMM-GFM1-O.                                                      
      *                                                                         
      *---------------------------------------CODE RETOUR                       
      * MAJ IMPOSSIBLE : QUOTA MANQUANT SEMAINE ET ANNEE                        
              03 COMM-GFM1-DANNEE          PIC    X(4).                         
              03 COMM-GFM1-DSEMAINE        PIC    X(2).                         
              03 COMM-GFM1-QUOTAM          PIC    S9(7) COMP-3.                 
      * MAJ IMPOSSIBLE : QUOTA MANQUANT JOUR                                    
              03 COMM-GFM1-DLIVRAISON      PIC    X(8).                         
      *---------------------------------------CODE RETOUR                       
      * ERREUR --> ABANDON PROGRAMME                                            
              03 COMM-GFM1-CODRET          PIC    X VALUE '0'.                  
                 88 COMM-GFM1-OK           VALUE '0'.                           
                 88 COMM-GFM1-KO           VALUE '1'.                           
      *---------------------------------------CODE RETOUR                       
      * LE CODIC NE PEUT PAS ETRE MIS A JOUR                                    
              03 COMM-GFM1-CODRET-CODIC    PIC    X VALUE '0'.                  
                 88 COMM-GFM1-CODIC-OK     VALUE '0'.                           
                 88 COMM-GFM1-CODIC-KO     VALUE '1'.                           
              03 COMM-GFM1-MSG             PIC    X(40).                        
                                                                                
