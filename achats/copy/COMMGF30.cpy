      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG  TGF30                   TR: GF30  *    00020000
      *           SUIVI DES COMMANDES FOURNISSEURS                 *    00030000
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ------------------------- 6814 MAX         
      *                                                                 00060000
          02 COMM-GF30-APPLI REDEFINES COMM-GF00-APPLI.                 00070000
      *------------------------------ PERIODE DEBUT                     00080000
             03 COMM-GF30-PERIODE-DEB    PIC X(8).                      00090004
      *------------------------------ PERIODE FIN                       00100000
             03 COMM-GF30-PERIODE-FIN    PIC X(8).                      00110004
      *------------------------------ SOLDE                             00120000
             03 COMM-GF30-SOLDE          PIC X(1).                      00130000
      *------------------------------ DONNEES ARTICLE                   00140002
             03 COMM-GA00-DONNEES.                                      00150002
                04 COMM-GA00-NCODIC         PIC X(7).                   00160002
                04 COMM-GA00-CFAM           PIC X(5).                   00170002
                04 COMM-GA00-CMARQ          PIC X(5).                   00180002
                04 COMM-GA00-LREFFOURN      PIC X(20).                  00190002
      *------------------------------ DONNEES ENTREPOT                  00190003
             03 COMM-FL90-DONNEES.                                      00190004
                04 COMM-FL90-WGENGRO        PIC X(3).                   00190005
             03 COMM-GA14-CFAM              PIC X(5).                   00190004
             03 COMM-GA02-CHEFP             PIC X(5).                   00190004
             03 COMM-GF30-TITRE6            PIC X(77).                          
             03 COMM-GF30-TITRE61           PIC X(35).                          
             03 COMM-GF30-STATUT            PIC X(3).                           
      *------------------------------ FILLER                            00200001
      *      03 COMM-GF30-FILLER         PIC X(7354).                           
      *      03 COMM-GF30-FILLER         PIC X(2164).                           
      *      03 COMM-GF30-FILLER         PIC X(2188).                           
             03 COMM-GF30-FILLER         PIC X(6632).                           
      ***************************************************************** 00220000
                                                                                
