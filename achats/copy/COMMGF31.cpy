      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG  TGF31                   TR: GF31  *    00020000
      *           LISTE DES COMMANDES POUR UNE ENTITE DONNEE       *    00030001
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 7354          
      *                                                                 00060000
          03 COMM-GF31-APPLI REDEFINES COMM-GF30-FILLER.                00070000
      *------------------------------ DONNEES PAGINATION                00080001
             04 COMM-GF31-WPAGIN.                                       00090001
                05 COMM-GF31-NPAGE       PIC S9(3) COMP-3.              00100001
                05 COMM-GF31-NPAGE-MAX   PIC S9(3) COMP-3.              00110001
      *--- LIBELLES + ATTRIBUTS                                                 
             04 COMM-GF31-LIGNES.                                               
                05 COMM-GF31-LIGNE1            PIC X(80).                       
                05 COMM-GF31-LIGNE21           PIC X.                           
                05 COMM-GF31-LIGNE22           PIC X(3).                        
                05 COMM-GF31-LIGNE31           PIC X.                           
                05 COMM-GF31-LIGNE32           PIC X(3).                        
                05 COMM-GF31-LIGNE4            PIC X(80).                       
                05 COMM-GF31-STATUT-A          PIC X.                           
      *------------------------------ FILLER                            00120000
      *      04 COMM-GF31-FILLER         PIC X(7350).                           
      *                                                                 00140001
                                                                                
