      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR32   EGR32                                              00000020
      ***************************************************************** 00000030
       01   EGR32I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * PAGE                                                            00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MPAGEI    PIC X(3).                                       00000200
      * NUMERO RECEPTION                                                00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRECEPL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNRECEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNRECEPF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNRECEPI  PIC X(7).                                       00000250
      * SOCIETE                                                         00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MNSOCI    PIC X(3).                                       00000300
      * LIBELLE SOCIETE                                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLSOCI    PIC X(20).                                      00000350
      * DATE SAISIE REC.                                                00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSAIREL  COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MDSAIREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDSAIREF  PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MDSAIREI  PIC X(8).                                       00000400
      * ENTREPOT                                                        00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTRL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNENTRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNENTRF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNENTRI   PIC X(3).                                       00000450
      * LIB. ENTREPOT                                                   00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTRL   COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MLENTRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLENTRF   PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MLENTRI   PIC X(20).                                      00000500
      * NUMERO REC. QUAI                                                00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRECQUL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MNRECQUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNRECQUF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MNRECQUI  PIC X(7).                                       00000550
      * LIVREUR                                                         00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIVREL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MCLIVREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLIVREF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MCLIVREI  PIC X(5).                                       00000600
      * LIB. LIVREUR                                                    00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIVREL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLLIVREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIVREF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLLIVREI  PIC X(20).                                      00000650
      * DATE RECEPTION                                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECEPL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MDRECEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRECEPF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MDRECEPI  PIC X(8).                                       00000700
      * COMMENTAIRE                                                     00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMEL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MLCOMMEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCOMMEF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MLCOMMEI  PIC X(20).                                      00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE1L   COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MNCDE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE1F   PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MNCDE1I   PIC X(7).                                       00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE3L   COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MNCDE3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE3F   PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MNCDE3I   PIC X(7).                                       00000830
      * NUM CDE 1                                                       00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBL1L    COMP PIC S9(4).                                 00000850
      *--                                                                       
           02 MNBL1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNBL1F    PIC X.                                          00000860
           02 FILLER    PIC X(4).                                       00000870
           02 MNBL1I    PIC X(10).                                      00000880
      * NUM CDE 3                                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBL3L    COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MNBL3L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNBL3F    PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MNBL3I    PIC X(10).                                      00000930
      * NUM CDE 2                                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE2L   COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MNCDE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE2F   PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNCDE2I   PIC X(7).                                       00000980
      * NUM CDE 4                                                       00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE4L   COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MNCDE4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE4F   PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MNCDE4I   PIC X(7).                                       00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBL2L    COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MNBL2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNBL2F    PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MNBL2I    PIC X(10).                                      00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBL4L    COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MNBL4L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNBL4F    PIC X.                                          00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MNBL4I    PIC X(10).                                      00001110
           02 M99I OCCURS   10 TIMES .                                  00001120
      * CODIC                                                           00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00001140
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00001150
             03 FILLER  PIC X(4).                                       00001160
             03 MNCODICI     PIC X(7).                                  00001170
      * MARQUE                                                          00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MCMARQI      PIC X(5).                                  00001220
      * REFERENCE                                                       00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFERL     COMP PIC S9(4).                            00001240
      *--                                                                       
             03 MLREFERL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLREFERF     PIC X.                                     00001250
             03 FILLER  PIC X(4).                                       00001260
             03 MLREFERI     PIC X(19).                                 00001270
      * QUANTITE RECUE                                                  00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTERECL     COMP PIC S9(4).                            00001290
      *--                                                                       
             03 MQTERECL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTERECF     PIC X.                                     00001300
             03 FILLER  PIC X(4).                                       00001310
             03 MQTERECI     PIC X(5).                                  00001320
      * QUANTITE 1                                                      00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE1L  COMP PIC S9(4).                                 00001340
      *--                                                                       
             03 MQTE1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE1F  PIC X.                                          00001350
             03 FILLER  PIC X(4).                                       00001360
             03 MQTE1I  PIC X(5).                                       00001370
      * QUANTITE 2                                                      00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE2L  COMP PIC S9(4).                                 00001390
      *--                                                                       
             03 MQTE2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE2F  PIC X.                                          00001400
             03 FILLER  PIC X(4).                                       00001410
             03 MQTE2I  PIC X(5).                                       00001420
      * QUANTITE 3                                                      00001430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE3L  COMP PIC S9(4).                                 00001440
      *--                                                                       
             03 MQTE3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE3F  PIC X.                                          00001450
             03 FILLER  PIC X(4).                                       00001460
             03 MQTE3I  PIC X(5).                                       00001470
      * QUANTITE 4                                                      00001480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE4L  COMP PIC S9(4).                                 00001490
      *--                                                                       
             03 MQTE4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE4F  PIC X.                                          00001500
             03 FILLER  PIC X(4).                                       00001510
             03 MQTE4I  PIC X(5).                                       00001520
      * QUANTITE LITIGES                                                00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTELITL     COMP PIC S9(4).                            00001540
      *--                                                                       
             03 MQTELITL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTELITF     PIC X.                                     00001550
             03 FILLER  PIC X(4).                                       00001560
             03 MQTELITI     PIC X(5).                                  00001570
      * ZONE CMD AIDA                                                   00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MZONCMDI  PIC X(15).                                      00001620
      * MESSAGE ERREUR                                                  00001630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001640
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001650
           02 FILLER    PIC X(4).                                       00001660
           02 MLIBERRI  PIC X(58).                                      00001670
      * CODE TRANSACTION                                                00001680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001690
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001700
           02 FILLER    PIC X(4).                                       00001710
           02 MCODTRAI  PIC X(4).                                       00001720
      * CICS DE TRAVAIL                                                 00001730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001740
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001750
           02 FILLER    PIC X(4).                                       00001760
           02 MCICSI    PIC X(5).                                       00001770
      * NETNAME                                                         00001780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001790
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001800
           02 FILLER    PIC X(4).                                       00001810
           02 MNETNAMI  PIC X(8).                                       00001820
      * CODE TERMINAL                                                   00001830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001840
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001850
           02 FILLER    PIC X(4).                                       00001860
           02 MSCREENI  PIC X(5).                                       00001870
      ***************************************************************** 00001880
      * SDF: EGR32   EGR32                                              00001890
      ***************************************************************** 00001900
       01   EGR32O REDEFINES EGR32I.                                    00001910
           02 FILLER    PIC X(12).                                      00001920
      * DATE DU JOUR                                                    00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MDATJOUA  PIC X.                                          00001950
           02 MDATJOUC  PIC X.                                          00001960
           02 MDATJOUP  PIC X.                                          00001970
           02 MDATJOUH  PIC X.                                          00001980
           02 MDATJOUV  PIC X.                                          00001990
           02 MDATJOUO  PIC X(10).                                      00002000
      * HEURE                                                           00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MTIMJOUA  PIC X.                                          00002030
           02 MTIMJOUC  PIC X.                                          00002040
           02 MTIMJOUP  PIC X.                                          00002050
           02 MTIMJOUH  PIC X.                                          00002060
           02 MTIMJOUV  PIC X.                                          00002070
           02 MTIMJOUO  PIC X(5).                                       00002080
      * PAGE                                                            00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MPAGEA    PIC X.                                          00002110
           02 MPAGEC    PIC X.                                          00002120
           02 MPAGEP    PIC X.                                          00002130
           02 MPAGEH    PIC X.                                          00002140
           02 MPAGEV    PIC X.                                          00002150
           02 MPAGEO    PIC X(3).                                       00002160
      * NUMERO RECEPTION                                                00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MNRECEPA  PIC X.                                          00002190
           02 MNRECEPC  PIC X.                                          00002200
           02 MNRECEPP  PIC X.                                          00002210
           02 MNRECEPH  PIC X.                                          00002220
           02 MNRECEPV  PIC X.                                          00002230
           02 MNRECEPO  PIC X(7).                                       00002240
      * SOCIETE                                                         00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MNSOCA    PIC X.                                          00002270
           02 MNSOCC    PIC X.                                          00002280
           02 MNSOCP    PIC X.                                          00002290
           02 MNSOCH    PIC X.                                          00002300
           02 MNSOCV    PIC X.                                          00002310
           02 MNSOCO    PIC X(3).                                       00002320
      * LIBELLE SOCIETE                                                 00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MLSOCA    PIC X.                                          00002350
           02 MLSOCC    PIC X.                                          00002360
           02 MLSOCP    PIC X.                                          00002370
           02 MLSOCH    PIC X.                                          00002380
           02 MLSOCV    PIC X.                                          00002390
           02 MLSOCO    PIC X(20).                                      00002400
      * DATE SAISIE REC.                                                00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MDSAIREA  PIC X.                                          00002430
           02 MDSAIREC  PIC X.                                          00002440
           02 MDSAIREP  PIC X.                                          00002450
           02 MDSAIREH  PIC X.                                          00002460
           02 MDSAIREV  PIC X.                                          00002470
           02 MDSAIREO  PIC X(8).                                       00002480
      * ENTREPOT                                                        00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MNENTRA   PIC X.                                          00002510
           02 MNENTRC   PIC X.                                          00002520
           02 MNENTRP   PIC X.                                          00002530
           02 MNENTRH   PIC X.                                          00002540
           02 MNENTRV   PIC X.                                          00002550
           02 MNENTRO   PIC X(3).                                       00002560
      * LIB. ENTREPOT                                                   00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MLENTRA   PIC X.                                          00002590
           02 MLENTRC   PIC X.                                          00002600
           02 MLENTRP   PIC X.                                          00002610
           02 MLENTRH   PIC X.                                          00002620
           02 MLENTRV   PIC X.                                          00002630
           02 MLENTRO   PIC X(20).                                      00002640
      * NUMERO REC. QUAI                                                00002650
           02 FILLER    PIC X(2).                                       00002660
           02 MNRECQUA  PIC X.                                          00002670
           02 MNRECQUC  PIC X.                                          00002680
           02 MNRECQUP  PIC X.                                          00002690
           02 MNRECQUH  PIC X.                                          00002700
           02 MNRECQUV  PIC X.                                          00002710
           02 MNRECQUO  PIC X(7).                                       00002720
      * LIVREUR                                                         00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MCLIVREA  PIC X.                                          00002750
           02 MCLIVREC  PIC X.                                          00002760
           02 MCLIVREP  PIC X.                                          00002770
           02 MCLIVREH  PIC X.                                          00002780
           02 MCLIVREV  PIC X.                                          00002790
           02 MCLIVREO  PIC X(5).                                       00002800
      * LIB. LIVREUR                                                    00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MLLIVREA  PIC X.                                          00002830
           02 MLLIVREC  PIC X.                                          00002840
           02 MLLIVREP  PIC X.                                          00002850
           02 MLLIVREH  PIC X.                                          00002860
           02 MLLIVREV  PIC X.                                          00002870
           02 MLLIVREO  PIC X(20).                                      00002880
      * DATE RECEPTION                                                  00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MDRECEPA  PIC X.                                          00002910
           02 MDRECEPC  PIC X.                                          00002920
           02 MDRECEPP  PIC X.                                          00002930
           02 MDRECEPH  PIC X.                                          00002940
           02 MDRECEPV  PIC X.                                          00002950
           02 MDRECEPO  PIC X(8).                                       00002960
      * COMMENTAIRE                                                     00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MLCOMMEA  PIC X.                                          00002990
           02 MLCOMMEC  PIC X.                                          00003000
           02 MLCOMMEP  PIC X.                                          00003010
           02 MLCOMMEH  PIC X.                                          00003020
           02 MLCOMMEV  PIC X.                                          00003030
           02 MLCOMMEO  PIC X(20).                                      00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MNCDE1A   PIC X.                                          00003060
           02 MNCDE1C   PIC X.                                          00003070
           02 MNCDE1P   PIC X.                                          00003080
           02 MNCDE1H   PIC X.                                          00003090
           02 MNCDE1V   PIC X.                                          00003100
           02 MNCDE1O   PIC X(7).                                       00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MNCDE3A   PIC X.                                          00003130
           02 MNCDE3C   PIC X.                                          00003140
           02 MNCDE3P   PIC X.                                          00003150
           02 MNCDE3H   PIC X.                                          00003160
           02 MNCDE3V   PIC X.                                          00003170
           02 MNCDE3O   PIC X(7).                                       00003180
      * NUM CDE 1                                                       00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MNBL1A    PIC X.                                          00003210
           02 MNBL1C    PIC X.                                          00003220
           02 MNBL1P    PIC X.                                          00003230
           02 MNBL1H    PIC X.                                          00003240
           02 MNBL1V    PIC X.                                          00003250
           02 MNBL1O    PIC X(10).                                      00003260
      * NUM CDE 3                                                       00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MNBL3A    PIC X.                                          00003290
           02 MNBL3C    PIC X.                                          00003300
           02 MNBL3P    PIC X.                                          00003310
           02 MNBL3H    PIC X.                                          00003320
           02 MNBL3V    PIC X.                                          00003330
           02 MNBL3O    PIC X(10).                                      00003340
      * NUM CDE 2                                                       00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MNCDE2A   PIC X.                                          00003370
           02 MNCDE2C   PIC X.                                          00003380
           02 MNCDE2P   PIC X.                                          00003390
           02 MNCDE2H   PIC X.                                          00003400
           02 MNCDE2V   PIC X.                                          00003410
           02 MNCDE2O   PIC X(7).                                       00003420
      * NUM CDE 4                                                       00003430
           02 FILLER    PIC X(2).                                       00003440
           02 MNCDE4A   PIC X.                                          00003450
           02 MNCDE4C   PIC X.                                          00003460
           02 MNCDE4P   PIC X.                                          00003470
           02 MNCDE4H   PIC X.                                          00003480
           02 MNCDE4V   PIC X.                                          00003490
           02 MNCDE4O   PIC X(7).                                       00003500
           02 FILLER    PIC X(2).                                       00003510
           02 MNBL2A    PIC X.                                          00003520
           02 MNBL2C    PIC X.                                          00003530
           02 MNBL2P    PIC X.                                          00003540
           02 MNBL2H    PIC X.                                          00003550
           02 MNBL2V    PIC X.                                          00003560
           02 MNBL2O    PIC X(10).                                      00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MNBL4A    PIC X.                                          00003590
           02 MNBL4C    PIC X.                                          00003600
           02 MNBL4P    PIC X.                                          00003610
           02 MNBL4H    PIC X.                                          00003620
           02 MNBL4V    PIC X.                                          00003630
           02 MNBL4O    PIC X(10).                                      00003640
           02 M99O OCCURS   10 TIMES .                                  00003650
      * CODIC                                                           00003660
             03 FILLER       PIC X(2).                                  00003670
             03 MNCODICA     PIC X.                                     00003680
             03 MNCODICC     PIC X.                                     00003690
             03 MNCODICP     PIC X.                                     00003700
             03 MNCODICH     PIC X.                                     00003710
             03 MNCODICV     PIC X.                                     00003720
             03 MNCODICO     PIC X(7).                                  00003730
      * MARQUE                                                          00003740
             03 FILLER       PIC X(2).                                  00003750
             03 MCMARQA      PIC X.                                     00003760
             03 MCMARQC PIC X.                                          00003770
             03 MCMARQP PIC X.                                          00003780
             03 MCMARQH PIC X.                                          00003790
             03 MCMARQV PIC X.                                          00003800
             03 MCMARQO      PIC X(5).                                  00003810
      * REFERENCE                                                       00003820
             03 FILLER       PIC X(2).                                  00003830
             03 MLREFERA     PIC X.                                     00003840
             03 MLREFERC     PIC X.                                     00003850
             03 MLREFERP     PIC X.                                     00003860
             03 MLREFERH     PIC X.                                     00003870
             03 MLREFERV     PIC X.                                     00003880
             03 MLREFERO     PIC X(19).                                 00003890
      * QUANTITE RECUE                                                  00003900
             03 FILLER       PIC X(2).                                  00003910
             03 MQTERECA     PIC X.                                     00003920
             03 MQTERECC     PIC X.                                     00003930
             03 MQTERECP     PIC X.                                     00003940
             03 MQTERECH     PIC X.                                     00003950
             03 MQTERECV     PIC X.                                     00003960
             03 MQTERECO     PIC X(5).                                  00003970
      * QUANTITE 1                                                      00003980
             03 FILLER       PIC X(2).                                  00003990
             03 MQTE1A  PIC X.                                          00004000
             03 MQTE1C  PIC X.                                          00004010
             03 MQTE1P  PIC X.                                          00004020
             03 MQTE1H  PIC X.                                          00004030
             03 MQTE1V  PIC X.                                          00004040
             03 MQTE1O  PIC X(5).                                       00004050
      * QUANTITE 2                                                      00004060
             03 FILLER       PIC X(2).                                  00004070
             03 MQTE2A  PIC X.                                          00004080
             03 MQTE2C  PIC X.                                          00004090
             03 MQTE2P  PIC X.                                          00004100
             03 MQTE2H  PIC X.                                          00004110
             03 MQTE2V  PIC X.                                          00004120
             03 MQTE2O  PIC X(5).                                       00004130
      * QUANTITE 3                                                      00004140
             03 FILLER       PIC X(2).                                  00004150
             03 MQTE3A  PIC X.                                          00004160
             03 MQTE3C  PIC X.                                          00004170
             03 MQTE3P  PIC X.                                          00004180
             03 MQTE3H  PIC X.                                          00004190
             03 MQTE3V  PIC X.                                          00004200
             03 MQTE3O  PIC X(5).                                       00004210
      * QUANTITE 4                                                      00004220
             03 FILLER       PIC X(2).                                  00004230
             03 MQTE4A  PIC X.                                          00004240
             03 MQTE4C  PIC X.                                          00004250
             03 MQTE4P  PIC X.                                          00004260
             03 MQTE4H  PIC X.                                          00004270
             03 MQTE4V  PIC X.                                          00004280
             03 MQTE4O  PIC X(5).                                       00004290
      * QUANTITE LITIGES                                                00004300
             03 FILLER       PIC X(2).                                  00004310
             03 MQTELITA     PIC X.                                     00004320
             03 MQTELITC     PIC X.                                     00004330
             03 MQTELITP     PIC X.                                     00004340
             03 MQTELITH     PIC X.                                     00004350
             03 MQTELITV     PIC X.                                     00004360
             03 MQTELITO     PIC X(5).                                  00004370
      * ZONE CMD AIDA                                                   00004380
           02 FILLER    PIC X(2).                                       00004390
           02 MZONCMDA  PIC X.                                          00004400
           02 MZONCMDC  PIC X.                                          00004410
           02 MZONCMDP  PIC X.                                          00004420
           02 MZONCMDH  PIC X.                                          00004430
           02 MZONCMDV  PIC X.                                          00004440
           02 MZONCMDO  PIC X(15).                                      00004450
      * MESSAGE ERREUR                                                  00004460
           02 FILLER    PIC X(2).                                       00004470
           02 MLIBERRA  PIC X.                                          00004480
           02 MLIBERRC  PIC X.                                          00004490
           02 MLIBERRP  PIC X.                                          00004500
           02 MLIBERRH  PIC X.                                          00004510
           02 MLIBERRV  PIC X.                                          00004520
           02 MLIBERRO  PIC X(58).                                      00004530
      * CODE TRANSACTION                                                00004540
           02 FILLER    PIC X(2).                                       00004550
           02 MCODTRAA  PIC X.                                          00004560
           02 MCODTRAC  PIC X.                                          00004570
           02 MCODTRAP  PIC X.                                          00004580
           02 MCODTRAH  PIC X.                                          00004590
           02 MCODTRAV  PIC X.                                          00004600
           02 MCODTRAO  PIC X(4).                                       00004610
      * CICS DE TRAVAIL                                                 00004620
           02 FILLER    PIC X(2).                                       00004630
           02 MCICSA    PIC X.                                          00004640
           02 MCICSC    PIC X.                                          00004650
           02 MCICSP    PIC X.                                          00004660
           02 MCICSH    PIC X.                                          00004670
           02 MCICSV    PIC X.                                          00004680
           02 MCICSO    PIC X(5).                                       00004690
      * NETNAME                                                         00004700
           02 FILLER    PIC X(2).                                       00004710
           02 MNETNAMA  PIC X.                                          00004720
           02 MNETNAMC  PIC X.                                          00004730
           02 MNETNAMP  PIC X.                                          00004740
           02 MNETNAMH  PIC X.                                          00004750
           02 MNETNAMV  PIC X.                                          00004760
           02 MNETNAMO  PIC X(8).                                       00004770
      * CODE TERMINAL                                                   00004780
           02 FILLER    PIC X(2).                                       00004790
           02 MSCREENA  PIC X.                                          00004800
           02 MSCREENC  PIC X.                                          00004810
           02 MSCREENP  PIC X.                                          00004820
           02 MSCREENH  PIC X.                                          00004830
           02 MSCREENV  PIC X.                                          00004840
           02 MSCREENO  PIC X(5).                                       00004850
                                                                                
