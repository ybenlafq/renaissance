      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ INTERROGATION CODES EAN CHEZ KESA                    
      *                                                                         
      *****************************************************************         
      * VERSION POUR MAI98 AVEC PUT ET GET EXTERNES                             
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-AI98-LONG-COMMAREA    PIC S9(4) COMP VALUE +99.                 
      *--                                                                       
       01  COMM-AI98-LONG-COMMAREA    PIC S9(4) COMP-5 VALUE +99.               
      *}                                                                        
       01  COMM-AI98-APPLI.                                                     
              02 COMM-AI98-ENTREE.                                              
               20 COMM-AI98-NCODICK   PIC X(7).                                 
               20 COMM-AI98-NCODIC    PIC X(7).                                 
               20 COMM-AI98-NEAN      PIC X(13).                                
               20 COMM-AI98-INTERFACE PIC X.                                    
               20 COMM-AI98-NSOCIETE   PIC X(3).                                
              02  COMM-AI98-SORTIE.                                             
                 20 COMM-AI98-SORTIE-CODRET PIC X.                              
                 20 COMM-AI98-SORTIE-LIBELLE.                                   
                      25 COMM-AI98-SORTIE-NOMPGRM PIC X(6).                     
                      25 COMM-AI98-SORTIE-LIBERR   PIC X(52).                   
                 20 COMM-AI98-MESSAGE.                                          
                     25 COMM-AI98-SORTIE-FLAGRECHEAN PIC X.                     
                     25 COMM-AI98-SORTIE-WMULTISOC PIC X.                       
                     25 COMM-AI98-SORTIE-NCODICK PIC X(7).                      
                                                                                
