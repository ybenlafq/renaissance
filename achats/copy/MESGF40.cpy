      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ                                                       
      *                                                                         
      *                                                                         
      *****************************************************************         
      *                                                                         
      *                                                                         
       01  WS-MQ10.                                                             
         02 WS-QUEUE.                                                           
            10 MQ10-CORRELID               PIC X(24).                           
         02 WS-CODRET                      PIC X(02).                           
         02 WS-MESSAGE.                                                         
            05 MESS-ENTETE.                                                     
               10 MES-TYPE                 PIC X(03).                           
               10 MES-NSOCMSG              PIC X(03).                           
               10 MES-NLIEUMSG             PIC X(03).                           
               10 MES-NSOCDST              PIC X(03).                           
               10 MES-NLIEUDST             PIC X(03).                           
               10 MES-NORD                 PIC 9(08).                           
               10 MES-LPROG                PIC X(10).                           
               10 MES-DJOUR                PIC X(08).                           
               10 MES-WSID                 PIC X(10).                           
               10 MES-USER                 PIC X(10).                           
               10 MES-CHRONO               PIC 9(07).                           
               10 MES-NBRMSG               PIC 9(07).                           
               10 MES-NBRENR               PIC 9(05).                           
               10 MES-TAILLE               PIC 9(05).                           
               10 MES-FILLER               PIC X(20).                           
      *     05 MES-MESSAGE                 PIC X(1925).                         
E0105       05 MES-MESSAGE                 PIC X(2450).                         
      *     MESSAGE DETAIL                                                      
            05 MES-MESSAGE-Q4 REDEFINES MES-MESSAGE.                            
               10  MES-WFONC               PIC X(3).                            
               10  MES-NSURCDE             PIC X(7).                            
               10  MES-NCDE                PIC X(7).                            
               10  MES-NCODIC              PIC X(7).                            
               10  MES-LCOMMENT            PIC X(50).                           
               10  MES-NPROD               PIC X(15).                           
               10  MES-NEAN                PIC X(13).                           
               10  MES-LREFFOURN           PIC X(20).                           
               10  MES-QCDE                PIC  9(5).                           
               10  MES-QREC                PIC  9(5).                           
               10  MES-QSOLDE              PIC  9(5).                           
               10  MES-PBF                 PIC  9(7)V99.                        
               10  MES-VALCDE              PIC  9(12)V99.                       
E0105          10  MES-SCREAS              PIC X(5).                            
E0105          10  MES-QCDEORIG            PIC  9(5).                           
               10  MES-DETPARDATE        OCCURS 20.                             
                   15  MES-DLIVRAISON      PIC X(8).                            
                   15  MES-QCDE-GF30       PIC  9(5).                           
                   15  MES-DETPARBOOK    OCCURS 5.                              
                       20  MES-NSEQUENCE   PIC 9(3).                            
                       20  MES-NLIVRAISON  PIC X(7).                            
                       20  MES-QTECDE      PIC  9(5).                           
E0105                  20  MES-DCREAS      PIC X(5).                            
      *        10  FILLER                  PIC X(005).                          
E0105          10  FILLER                  PIC X(020).                          
      *     MESSAGE CONTROLE                                                    
            05 MES-MESSAGE-Q2 REDEFINES MES-MESSAGE.                            
               10  MES-WFONC2               PIC X(3).                           
               10  MES-NSURCDE2             PIC X(7).                           
               10  MES-CTYPCDE              PIC X(5).                           
               10  MES-NENTCDE              PIC X(5).                           
               10  MES-LENTCDE              PIC X(20).                          
               10  MES-LENTCDEADR1          PIC X(32).                          
               10  MES-LENTCDEADR2          PIC X(32).                          
               10  MES-NENTCDEADR3          PIC X(5).                           
               10  MES-LENTCDEADR4          PIC X(26).                          
               10  MES-DSAISIE              PIC X(8).                           
               10  MES-NBRDEPOT             PIC 9(2).                           
               10  MES-NBRMESS              PIC 9(4).                           
               10  MES-COMM-FILLE   OCCURS 20.                                  
                   15  MES-NCDE2            PIC X(7).                           
                   15  MES-NSOCLIVR         PIC X(3).                           
                   15  MES-NDEPOT           PIC X(3).                           
                   15  MES-CVALORISAT       PIC X(1).                           
                   15  MES-NAVENANT         PIC X(3).                           
                   15  MES-NBRCODIC         PIC 9(5).                           
E0105              15  MES-ORIGDATE         PIC X(8).                           
      *        10  FILLER                   PIC X(1336).                        
E0105          10  FILLER                   PIC X(1701).                        
                                                                                
