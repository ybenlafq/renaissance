      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                         
      *04 COMMGF44-APPLI REDEFINES COMM-GF43-GESTION-AFFICHAGE.                 
CRO    04 COMMGF44-APPLI REDEFINES COMM-GF43-FILLER-LONG.                       
      *   04 COMM-GF44-DATA.                                                    
      *      05 COMM-GF44-TGF44 PIC X VALUE '1'.                                
      *         88 COMM-GF44-TGF44-OK VALUE '1'.                                
      *         88 COMM-GF44-TGF44-KO VALUE '0'.                                
      *                                                                         
          05 COMM-GF44-GESTION-AFFICHAGE.                                       
             06 COMM-GF44-NBRPAGE          PIC 9(03).                           
             06 COMM-GF44-NUMPAGE          PIC 9(03).                           
             06 COMM-GF44-NBRITEMPAGE      PIC 9(03).                           
             06 COMM-GF44-NUMITEMPAGE      PIC 9(03).                           
             06 COMM-GF44-NBRLIST          PIC 9(03).                           
             06 COMM-GF44-NUMLIST          PIC 9(03).                           
             06 COMM-GF44-NBRITEMLIST      PIC 9(03).                           
             06 COMM-GF44-NUMITEMLIST      PIC 9(03).                           
             06 COMM-GF44-1ERE-LIGNE       PIC 9(03).                           
             06 COMM-GF44-RANG-TS          PIC 9(03).                           
             06 COMM-GF44-COLONNE          PIC 9(03).                           
       04 COMM-GF44-VERTICAL PIC X.                                             
          88 PAS-VERTICAL44     VALUE '0'.                                      
          88 VERTICAL44         VALUE '1'.                                      
       04 COMM-GF44-HORIZONTAL PIC X.                                           
          88 PAS-HORIZONTAL44   VALUE '0'.                                      
          88 HORIZONTAL44       VALUE '1'.                                      
       04 COMM-GF44-CMAJ       PIC X.                                           
      *{ remove-comma-in-dde 1.5                                                
      *   88 AFFICHABLE        VALUE 'B' , 'M'.                                 
      *--                                                                       
          88 AFFICHABLE        VALUE 'B'   'M'.                                 
      *}                                                                        
          88 LIGNE-SANS-MODIF  VALUE 'B'.                                       
          88 LIGNE-MODIFIEE    VALUE 'M'.                                       
          88 LIGNE-SUPPRIMEE   VALUE 'S'.                                       
       04 COMM-GF44-PF12                  PIC 9(1).                             
          88 COMM-GF44-PF12-CONFIRM-KO       VALUE 0.                           
          88 COMM-GF44-PF12-CONFIRM-OK       VALUE 3 THRU 4.                    
          88 COMM-GF44-PF12-CONFIRM-F3       VALUE 3.                           
          88 COMM-GF44-PF12-CONFIRM-F4       VALUE 4.                           
       04 COMM-GF44-FLAG-MAJ              PIC 9(1).                             
          88 COMM-GF44-NO-MODIF              VALUE 0.                           
          88 COMM-GF44-MODIF                 VALUE 1.                           
       04 COMM-GF44-TAB-CODIC.                                                  
          05 COMM-GF44-AJOUTS OCCURS 35   INDEXED BY IX-TAB.                    
             06 COMM-GF44-AJ-CODIC                 PIC X(07).                   
             06 COMM-GF44-AJ-FLAG                  PIC 9(01).                   
             06 COMM-GF44-AJ-QCDE OCCURS 10.                                    
E0037**         07 COMM-GF44-AJ-NSOC               PIC X(03).                   
CD              07 COMM-GF44-AJ-NDEPOT             PIC X(03).                   
                07 COMM-GF44-AJ-QCDE-ENT           PIC S9(06) COMP-3.           
       04 COMM-GF44-CPT-TOUCHE.                                                 
          05 COMM-GF44-CPT11    PIC 9(1).                                       
       04 COMM-GF44-FL-PAGE     PIC X.                                          
          88 COMM-GF44-OLD-PAGE VALUE '0'.                                      
          88 COMM-GF44-NEW-PAGE VALUE '1'.                                      
       04 COMM-GF44-AID         PIC X.                                          
          88 COMM-GF44-AID-PERMIS     VALUE '0'.                                
          88 COMM-GF44-AID-PAS-PERMIS VALUE '1'.                                
       04 COMM-GF44-TAB-LIEU.                                                   
E0037     05 COMM-GF44-SV-LIEU  OCCURS 12.                                      
            06 COMM-GF44-SV-NSOCSPEC      PIC X(03).                            
            06 COMM-GF44-SV-NLIEUSPEC     PIC X(03).                            
E0037     05 COMM-GF44-SV-CODIC OCCURS 12 PIC X(07).                            
       04 COMM-GF44-CTRL  PIC X.                                                
          88 COMM-GF44-CTRL-OK VALUE '0'.                                       
          88 COMM-GF44-CTRL-KO VALUE '1'.                                       
       04 COMM-GF44-CHARGE-TS  PIC S9(04).                                      
       04 COMM-GF44-NBITEM-TS  PIC S9(04).                                      
       04 COMM-GF44-TB-ENT.                                                     
E0037**   05 COMM-GF44-SOCDEPOT OCCURS 5 PIC X(03).                             
E0037     05 COMM-GF44-SOCDEPOT          PIC X(03).                             
E0037     05 COMM-GF44-DEPOT    OCCURS 4 PIC X(03).                             
      *                                                                         
E0037**04 COMM-GF44-XCTRL-LDEMAND                  PIC X(01).                   
       04 COMM-GF44-XCTRL-WQCOLIRECEPT       PIC X(01).                         
E0191  04 COMM-GF44-EDI-TOUCHE               PIC X.                             
E0191     88 COMM-GF44-EDI-AUTRE             VALUE '0'.                         
E0191     88 COMM-GF44-EDI-PF5               VALUE '1'.                         
E0191     88 COMM-GF44-EDI-PF17              VALUE '2'.                         
      *                                                                         
      *04 COMM-GF44-FILLER              PIC X(1101).                            
      *                                                                         
                                                                                
