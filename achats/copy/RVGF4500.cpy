      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGF4500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF4500                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF4500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF4500.                                                            
      *}                                                                        
           02  GF45-CTYPCDE                                                     
               PIC X(0005).                                                     
           02  GF45-LENTCDE                                                     
               PIC X(0020).                                                     
           02  GF45-LINTERLOCUT                                                 
               PIC X(0020).                                                     
           02  GF45-CTRANSACTION                                                
               PIC X(0003).                                                     
           02  GF45-NCDE                                                        
               PIC X(0007).                                                     
           02  GF45-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GF45-NLIEU                                                       
               PIC X(0003).                                                     
           02  GF45-CTYPMVT                                                     
               PIC X(0003).                                                     
           02  GF45-DVALIDITEANC                                                
               PIC X(0008).                                                     
           02  GF45-NCODIC                                                      
               PIC X(0007).                                                     
           02  GF45-QCDEANC                                                     
               PIC S9(5) COMP-3.                                                
           02  GF45-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGF4500                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF4500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF4500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF45-CTYPCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF45-CTYPCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF45-LENTCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF45-LENTCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF45-LINTERLOCUT-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF45-LINTERLOCUT-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF45-CTRANSACTION-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF45-CTRANSACTION-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF45-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF45-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF45-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF45-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF45-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF45-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF45-CTYPMVT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF45-CTYPMVT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF45-DVALIDITEANC-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF45-DVALIDITEANC-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF45-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF45-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF45-QCDEANC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF45-QCDEANC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF45-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GF45-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
