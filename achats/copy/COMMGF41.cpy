      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      * COMMAREA SPECIFIQUE MODULE DECOUPAGE COMMANDE EN MONOCODIC *            
      **************************************************************            
      *                                                                         
      *   PROGRAMME MGF41                                                       
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 COMM-MGF41-LONG-COMMAREA          PIC S9(4) COMP VALUE +220.          
      *--                                                                       
       01 COMM-MGF41-LONG-COMMAREA          PIC S9(4) COMP-5 VALUE +220.        
      *}                                                                        
       01 COMM-MGF41-APPLI.                                                     
     *                                                                          
          02 COMM-MGF41-ENTREE.                                                 
             05 COMM-MGF41-NTERMID          PIC X(04).                          
             05 COMM-MGF41-NCDE             PIC X(07).                          
             05 COMM-MGF41-CTYPCDE          PIC X(05).                          
             05 COMM-MGF41-NENTCDE          PIC X(05).                          
             05 COMM-MGF41-LENTCDE          PIC X(20).                          
             05 COMM-MGF41-CINTERLOCUT      PIC X(05).                          
             05 COMM-MGF41-LINTERLOCUT      PIC X(20).                          
             05 COMM-MGF41-NSOCLIVR         PIC X(03).                          
             05 COMM-MGF41-NDEPOT           PIC X(03).                          
             05 COMM-MGF41-DSAISIE          PIC X(08).                          
             05 COMM-MGF41-DVALIDITE        PIC X(08).                          
             05 COMM-MGF41-CSTATUT          PIC X(03).                          
             05 COMM-MGF41-CQUALIF          PIC X(06).                          
             05 COMM-MGF41-PROGRET          PIC X(05).                          
             05 COMM-MGF41-CREASON          PIC X(01).                          
             05 COMM-MGF41-ORIGDATE         PIC X(08).                          
             05 COMM-MGF41-DANNEE           PIC X(04).                          
             05 COMM-MGF41-NSEMAINE         PIC X(02).                          
GD           05 COMM-MGF41-WEDI             PIC X(01).                          
E0311.       05 COMM-MGF41-WSAP             PIC X(01).                          
.E0311       05 COMM-MGF41-VALOMANO         PIC X(01).                          
      *                                                                         
          02 COMM-MGF41-SORTIE.                                                 
             05 COMM-MGF41-NCDE1            PIC X(07).                          
             05 COMM-MGF41-NCDE2            PIC X(07).                          
             05 COMM-MGF41-CODRET           PIC X(01).                          
                88 COMM-MGF41-OK                  VALUE ' '.                    
                88 COMM-MGF41-ERR                 VALUE '1'.                    
             05 COMM-MGF41-LIBERR           PIC X(58).                          
      *                                                                         
GD    *   02 FILLER                         PIC X(30).                          
GD        02 FILLER                         PIC X(29).                          
      *                                                                         
                                                                                
