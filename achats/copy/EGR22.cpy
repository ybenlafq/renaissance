      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR22   EGR22                                              00000020
      ***************************************************************** 00000030
       01   EGR22I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * PAGE                                                            00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MPAGEI    PIC X(3).                                       00000200
      * RECEP. A QUAI                                                   00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRECEPL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNRECEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNRECEPF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNRECEPI  PIC X(7).                                       00000250
      * SOCIETE                                                         00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MNSOCI    PIC X(3).                                       00000300
      * LIB. SOCIETE                                                    00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLSOCI    PIC X(20).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRQL    COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MCTRQL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTRQF    PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MCTRQI    PIC X(5).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRQL    COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MLTRQL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLTRQF    PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MLTRQI    PIC X(15).                                      00000430
      * NUM. ENTREPOT                                                   00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTREL  COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MNENTREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENTREF  PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MNENTREI  PIC X(3).                                       00000480
      * LIB. ENTREPOT                                                   00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTREL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLENTREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLENTREF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLENTREI  PIC X(20).                                      00000530
      * DATE JOUR REC                                                   00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDJOURRL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MDJOURRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDJOURRF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MDJOURRI  PIC X(10).                                      00000580
      * HEURE                                                           00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDHEURRL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MDHEURRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDHEURRF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MDHEURRI  PIC X(2).                                       00000630
      * MINUTE                                                          00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMINURL  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MDMINURL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDMINURF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MDMINURI  PIC X(2).                                       00000680
      * CODE LIVREUR                                                    00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIVREL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MNLIVREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIVREF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNLIVREI  PIC X(5).                                       00000730
      * LIB. LIVREUR                                                    00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIVREL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLLIVREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIVREF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLLIVREI  PIC X(20).                                      00000780
      * COMMENTAIRE                                                     00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMEL  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MLCOMMEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCOMMEF  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MLCOMMEI  PIC X(20).                                      00000830
      * MODE TRANSPORT                                                  00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRANSL  COMP PIC S9(4).                                 00000850
      *--                                                                       
           02 MCTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRANSF  PIC X.                                          00000860
           02 FILLER    PIC X(4).                                       00000870
           02 MCTRANSI  PIC X(5).                                       00000880
      * LIB. TRANSPORT                                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRANSL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MLTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTRANSF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLTRANSI  PIC X(20).                                      00000930
      * NOMBRE PIECE                                                    00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPIECEL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MQPIECEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQPIECEF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MQPIECEI  PIC X(6).                                       00000980
           02 M70I OCCURS   10 TIMES .                                  00000990
      * CODIC                                                           00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00001010
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00001020
             03 FILLER  PIC X(4).                                       00001030
             03 MNCODICI     PIC X(7).                                  00001040
      * CODE MARQUE                                                     00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00001060
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00001070
             03 FILLER  PIC X(4).                                       00001080
             03 MCMARQI      PIC X(5).                                  00001090
      * CODE FAMILLE                                                    00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00001110
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MCFAMI  PIC X(5).                                       00001140
      * LIB. EMBALLA                                                    00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLEMBALL     COMP PIC S9(4).                            00001160
      *--                                                                       
             03 MLEMBALL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLEMBALF     PIC X.                                     00001170
             03 FILLER  PIC X(4).                                       00001180
             03 MLEMBALI     PIC X(50).                                 00001190
      * QUANTITE                                                        00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00001210
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00001220
             03 FILLER  PIC X(4).                                       00001230
             03 MQTEI   PIC X(5).                                       00001240
      * ZONE CMD AIDA                                                   00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MZONCMDI  PIC X(15).                                      00001290
      * MESSAGE ERREUR                                                  00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MLIBERRI  PIC X(58).                                      00001340
      * CODE TRANSACTION                                                00001350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001360
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001370
           02 FILLER    PIC X(4).                                       00001380
           02 MCODTRAI  PIC X(4).                                       00001390
      * CICS DE TRAVAIL                                                 00001400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001410
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001420
           02 FILLER    PIC X(4).                                       00001430
           02 MCICSI    PIC X(5).                                       00001440
      * NETNAME                                                         00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MNETNAMI  PIC X(8).                                       00001490
      * CODE TERMINAL                                                   00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MSCREENI  PIC X(5).                                       00001540
      ***************************************************************** 00001550
      * SDF: EGR22   EGR22                                              00001560
      ***************************************************************** 00001570
       01   EGR22O REDEFINES EGR22I.                                    00001580
           02 FILLER    PIC X(12).                                      00001590
      * DATE DU JOUR                                                    00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MDATJOUA  PIC X.                                          00001620
           02 MDATJOUC  PIC X.                                          00001630
           02 MDATJOUP  PIC X.                                          00001640
           02 MDATJOUH  PIC X.                                          00001650
           02 MDATJOUV  PIC X.                                          00001660
           02 MDATJOUO  PIC X(10).                                      00001670
      * HEURE                                                           00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MTIMJOUA  PIC X.                                          00001700
           02 MTIMJOUC  PIC X.                                          00001710
           02 MTIMJOUP  PIC X.                                          00001720
           02 MTIMJOUH  PIC X.                                          00001730
           02 MTIMJOUV  PIC X.                                          00001740
           02 MTIMJOUO  PIC X(5).                                       00001750
      * PAGE                                                            00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MPAGEA    PIC X.                                          00001780
           02 MPAGEC    PIC X.                                          00001790
           02 MPAGEP    PIC X.                                          00001800
           02 MPAGEH    PIC X.                                          00001810
           02 MPAGEV    PIC X.                                          00001820
           02 MPAGEO    PIC X(3).                                       00001830
      * RECEP. A QUAI                                                   00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MNRECEPA  PIC X.                                          00001860
           02 MNRECEPC  PIC X.                                          00001870
           02 MNRECEPP  PIC X.                                          00001880
           02 MNRECEPH  PIC X.                                          00001890
           02 MNRECEPV  PIC X.                                          00001900
           02 MNRECEPO  PIC X(7).                                       00001910
      * SOCIETE                                                         00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MNSOCA    PIC X.                                          00001940
           02 MNSOCC    PIC X.                                          00001950
           02 MNSOCP    PIC X.                                          00001960
           02 MNSOCH    PIC X.                                          00001970
           02 MNSOCV    PIC X.                                          00001980
           02 MNSOCO    PIC X(3).                                       00001990
      * LIB. SOCIETE                                                    00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MLSOCA    PIC X.                                          00002020
           02 MLSOCC    PIC X.                                          00002030
           02 MLSOCP    PIC X.                                          00002040
           02 MLSOCH    PIC X.                                          00002050
           02 MLSOCV    PIC X.                                          00002060
           02 MLSOCO    PIC X(20).                                      00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MCTRQA    PIC X.                                          00002090
           02 MCTRQC    PIC X.                                          00002100
           02 MCTRQP    PIC X.                                          00002110
           02 MCTRQH    PIC X.                                          00002120
           02 MCTRQV    PIC X.                                          00002130
           02 MCTRQO    PIC X(5).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MLTRQA    PIC X.                                          00002160
           02 MLTRQC    PIC X.                                          00002170
           02 MLTRQP    PIC X.                                          00002180
           02 MLTRQH    PIC X.                                          00002190
           02 MLTRQV    PIC X.                                          00002200
           02 MLTRQO    PIC X(15).                                      00002210
      * NUM. ENTREPOT                                                   00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MNENTREA  PIC X.                                          00002240
           02 MNENTREC  PIC X.                                          00002250
           02 MNENTREP  PIC X.                                          00002260
           02 MNENTREH  PIC X.                                          00002270
           02 MNENTREV  PIC X.                                          00002280
           02 MNENTREO  PIC X(3).                                       00002290
      * LIB. ENTREPOT                                                   00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLENTREA  PIC X.                                          00002320
           02 MLENTREC  PIC X.                                          00002330
           02 MLENTREP  PIC X.                                          00002340
           02 MLENTREH  PIC X.                                          00002350
           02 MLENTREV  PIC X.                                          00002360
           02 MLENTREO  PIC X(20).                                      00002370
      * DATE JOUR REC                                                   00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MDJOURRA  PIC X.                                          00002400
           02 MDJOURRC  PIC X.                                          00002410
           02 MDJOURRP  PIC X.                                          00002420
           02 MDJOURRH  PIC X.                                          00002430
           02 MDJOURRV  PIC X.                                          00002440
           02 MDJOURRO  PIC X(10).                                      00002450
      * HEURE                                                           00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MDHEURRA  PIC X.                                          00002480
           02 MDHEURRC  PIC X.                                          00002490
           02 MDHEURRP  PIC X.                                          00002500
           02 MDHEURRH  PIC X.                                          00002510
           02 MDHEURRV  PIC X.                                          00002520
           02 MDHEURRO  PIC X(2).                                       00002530
      * MINUTE                                                          00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MDMINURA  PIC X.                                          00002560
           02 MDMINURC  PIC X.                                          00002570
           02 MDMINURP  PIC X.                                          00002580
           02 MDMINURH  PIC X.                                          00002590
           02 MDMINURV  PIC X.                                          00002600
           02 MDMINURO  PIC X(2).                                       00002610
      * CODE LIVREUR                                                    00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MNLIVREA  PIC X.                                          00002640
           02 MNLIVREC  PIC X.                                          00002650
           02 MNLIVREP  PIC X.                                          00002660
           02 MNLIVREH  PIC X.                                          00002670
           02 MNLIVREV  PIC X.                                          00002680
           02 MNLIVREO  PIC X(5).                                       00002690
      * LIB. LIVREUR                                                    00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MLLIVREA  PIC X.                                          00002720
           02 MLLIVREC  PIC X.                                          00002730
           02 MLLIVREP  PIC X.                                          00002740
           02 MLLIVREH  PIC X.                                          00002750
           02 MLLIVREV  PIC X.                                          00002760
           02 MLLIVREO  PIC X(20).                                      00002770
      * COMMENTAIRE                                                     00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MLCOMMEA  PIC X.                                          00002800
           02 MLCOMMEC  PIC X.                                          00002810
           02 MLCOMMEP  PIC X.                                          00002820
           02 MLCOMMEH  PIC X.                                          00002830
           02 MLCOMMEV  PIC X.                                          00002840
           02 MLCOMMEO  PIC X(20).                                      00002850
      * MODE TRANSPORT                                                  00002860
           02 FILLER    PIC X(2).                                       00002870
           02 MCTRANSA  PIC X.                                          00002880
           02 MCTRANSC  PIC X.                                          00002890
           02 MCTRANSP  PIC X.                                          00002900
           02 MCTRANSH  PIC X.                                          00002910
           02 MCTRANSV  PIC X.                                          00002920
           02 MCTRANSO  PIC X(5).                                       00002930
      * LIB. TRANSPORT                                                  00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MLTRANSA  PIC X.                                          00002960
           02 MLTRANSC  PIC X.                                          00002970
           02 MLTRANSP  PIC X.                                          00002980
           02 MLTRANSH  PIC X.                                          00002990
           02 MLTRANSV  PIC X.                                          00003000
           02 MLTRANSO  PIC X(20).                                      00003010
      * NOMBRE PIECE                                                    00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MQPIECEA  PIC X.                                          00003040
           02 MQPIECEC  PIC X.                                          00003050
           02 MQPIECEP  PIC X.                                          00003060
           02 MQPIECEH  PIC X.                                          00003070
           02 MQPIECEV  PIC X.                                          00003080
           02 MQPIECEO  PIC X(6).                                       00003090
           02 M70O OCCURS   10 TIMES .                                  00003100
      * CODIC                                                           00003110
             03 FILLER       PIC X(2).                                  00003120
             03 MNCODICA     PIC X.                                     00003130
             03 MNCODICC     PIC X.                                     00003140
             03 MNCODICP     PIC X.                                     00003150
             03 MNCODICH     PIC X.                                     00003160
             03 MNCODICV     PIC X.                                     00003170
             03 MNCODICO     PIC X(7).                                  00003180
      * CODE MARQUE                                                     00003190
             03 FILLER       PIC X(2).                                  00003200
             03 MCMARQA      PIC X.                                     00003210
             03 MCMARQC PIC X.                                          00003220
             03 MCMARQP PIC X.                                          00003230
             03 MCMARQH PIC X.                                          00003240
             03 MCMARQV PIC X.                                          00003250
             03 MCMARQO      PIC X(5).                                  00003260
      * CODE FAMILLE                                                    00003270
             03 FILLER       PIC X(2).                                  00003280
             03 MCFAMA  PIC X.                                          00003290
             03 MCFAMC  PIC X.                                          00003300
             03 MCFAMP  PIC X.                                          00003310
             03 MCFAMH  PIC X.                                          00003320
             03 MCFAMV  PIC X.                                          00003330
             03 MCFAMO  PIC X(5).                                       00003340
      * LIB. EMBALLA                                                    00003350
             03 FILLER       PIC X(2).                                  00003360
             03 MLEMBALA     PIC X.                                     00003370
             03 MLEMBALC     PIC X.                                     00003380
             03 MLEMBALP     PIC X.                                     00003390
             03 MLEMBALH     PIC X.                                     00003400
             03 MLEMBALV     PIC X.                                     00003410
             03 MLEMBALO     PIC X(50).                                 00003420
      * QUANTITE                                                        00003430
             03 FILLER       PIC X(2).                                  00003440
             03 MQTEA   PIC X.                                          00003450
             03 MQTEC   PIC X.                                          00003460
             03 MQTEP   PIC X.                                          00003470
             03 MQTEH   PIC X.                                          00003480
             03 MQTEV   PIC X.                                          00003490
             03 MQTEO   PIC X(5).                                       00003500
      * ZONE CMD AIDA                                                   00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MZONCMDA  PIC X.                                          00003530
           02 MZONCMDC  PIC X.                                          00003540
           02 MZONCMDP  PIC X.                                          00003550
           02 MZONCMDH  PIC X.                                          00003560
           02 MZONCMDV  PIC X.                                          00003570
           02 MZONCMDO  PIC X(15).                                      00003580
      * MESSAGE ERREUR                                                  00003590
           02 FILLER    PIC X(2).                                       00003600
           02 MLIBERRA  PIC X.                                          00003610
           02 MLIBERRC  PIC X.                                          00003620
           02 MLIBERRP  PIC X.                                          00003630
           02 MLIBERRH  PIC X.                                          00003640
           02 MLIBERRV  PIC X.                                          00003650
           02 MLIBERRO  PIC X(58).                                      00003660
      * CODE TRANSACTION                                                00003670
           02 FILLER    PIC X(2).                                       00003680
           02 MCODTRAA  PIC X.                                          00003690
           02 MCODTRAC  PIC X.                                          00003700
           02 MCODTRAP  PIC X.                                          00003710
           02 MCODTRAH  PIC X.                                          00003720
           02 MCODTRAV  PIC X.                                          00003730
           02 MCODTRAO  PIC X(4).                                       00003740
      * CICS DE TRAVAIL                                                 00003750
           02 FILLER    PIC X(2).                                       00003760
           02 MCICSA    PIC X.                                          00003770
           02 MCICSC    PIC X.                                          00003780
           02 MCICSP    PIC X.                                          00003790
           02 MCICSH    PIC X.                                          00003800
           02 MCICSV    PIC X.                                          00003810
           02 MCICSO    PIC X(5).                                       00003820
      * NETNAME                                                         00003830
           02 FILLER    PIC X(2).                                       00003840
           02 MNETNAMA  PIC X.                                          00003850
           02 MNETNAMC  PIC X.                                          00003860
           02 MNETNAMP  PIC X.                                          00003870
           02 MNETNAMH  PIC X.                                          00003880
           02 MNETNAMV  PIC X.                                          00003890
           02 MNETNAMO  PIC X(8).                                       00003900
      * CODE TERMINAL                                                   00003910
           02 FILLER    PIC X(2).                                       00003920
           02 MSCREENA  PIC X.                                          00003930
           02 MSCREENC  PIC X.                                          00003940
           02 MSCREENP  PIC X.                                          00003950
           02 MSCREENH  PIC X.                                          00003960
           02 MSCREENV  PIC X.                                          00003970
           02 MSCREENO  PIC X(5).                                       00003980
                                                                                
