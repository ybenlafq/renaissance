      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR20   EGR20                                              00000020
      ***************************************************************** 00000030
       01   EGR20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * PAGE                                                            00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MPAGEI    PIC X(3).                                       00000200
      * RECEP. A QUAI                                                   00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRECEPL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNRECEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNRECEPF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNRECEPI  PIC X(7).                                       00000250
      * SOCIETE                                                         00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MNSOCI    PIC X(3).                                       00000300
      * LIB. SOCIETE                                                    00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLSOCI    PIC X(20).                                      00000350
      * NUM. ENTREPOT                                                   00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTREL  COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MNENTREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENTREF  PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MNENTREI  PIC X(3).                                       00000400
      * LIB. ENTREPOT                                                   00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTREL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLENTREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLENTREF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLENTREI  PIC X(20).                                      00000450
      * DATE JOUR REC                                                   00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDJOURRL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MDJOURRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDJOURRF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MDJOURRI  PIC X(8).                                       00000500
      * HEURE                                                           00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDHEURRL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MDHEURRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDHEURRF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MDHEURRI  PIC X(2).                                       00000550
      * MINUTE                                                          00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMINURL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MDMINURL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDMINURF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MDMINURI  PIC X(2).                                       00000600
      * CODE LIVREUR                                                    00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIVREL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNLIVREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIVREF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNLIVREI  PIC X(5).                                       00000650
      * LIB. LIVREUR                                                    00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIVREL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLLIVREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIVREF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLLIVREI  PIC X(20).                                      00000700
      * COMMENTAIRE                                                     00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMEL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MLCOMMEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCOMMEF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MLCOMMEI  PIC X(20).                                      00000750
      * MODE TRANSPORT                                                  00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRANSL  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MCTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRANSF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MCTRANSI  PIC X(5).                                       00000800
      * LIB. TRANSPORT                                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRANSL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTRANSF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLTRANSI  PIC X(20).                                      00000850
      * NOMBRE PIECE                                                    00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPIECEL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MQPIECEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQPIECEF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MQPIECEI  PIC X(5).                                       00000900
           02 M67I OCCURS   10 TIMES .                                  00000910
      * CODIC                                                           00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000930
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000940
             03 FILLER  PIC X(4).                                       00000950
             03 MNCODICI     PIC X(7).                                  00000960
      * CODE MARQUE                                                     00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000980
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000990
             03 FILLER  PIC X(4).                                       00001000
             03 MCMARQI      PIC X(5).                                  00001010
      * CODE FAMILLE                                                    00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MCFAMI  PIC X(5).                                       00001060
      * LIB. EMBALLA                                                    00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLEMBALL     COMP PIC S9(4).                            00001080
      *--                                                                       
             03 MLEMBALL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLEMBALF     PIC X.                                     00001090
             03 FILLER  PIC X(4).                                       00001100
             03 MLEMBALI     PIC X(51).                                 00001110
      * QUANTITE                                                        00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00001130
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00001140
             03 FILLER  PIC X(4).                                       00001150
             03 MQTEI   PIC X(5).                                       00001160
      * ZONE CMD AIDA                                                   00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MZONCMDI  PIC X(15).                                      00001210
      * MESSAGE ERREUR                                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MLIBERRI  PIC X(58).                                      00001260
      * CODE TRANSACTION                                                00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001280
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001290
           02 FILLER    PIC X(4).                                       00001300
           02 MCODTRAI  PIC X(4).                                       00001310
      * CICS DE TRAVAIL                                                 00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001330
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001340
           02 FILLER    PIC X(4).                                       00001350
           02 MCICSI    PIC X(5).                                       00001360
      * NETNAME                                                         00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MNETNAMI  PIC X(8).                                       00001410
      * CODE TERMINAL                                                   00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MSCREENI  PIC X(5).                                       00001460
      ***************************************************************** 00001470
      * SDF: EGR20   EGR20                                              00001480
      ***************************************************************** 00001490
       01   EGR20O REDEFINES EGR20I.                                    00001500
           02 FILLER    PIC X(12).                                      00001510
      * DATE DU JOUR                                                    00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MDATJOUA  PIC X.                                          00001540
           02 MDATJOUC  PIC X.                                          00001550
           02 MDATJOUP  PIC X.                                          00001560
           02 MDATJOUH  PIC X.                                          00001570
           02 MDATJOUV  PIC X.                                          00001580
           02 MDATJOUO  PIC X(10).                                      00001590
      * HEURE                                                           00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MTIMJOUA  PIC X.                                          00001620
           02 MTIMJOUC  PIC X.                                          00001630
           02 MTIMJOUP  PIC X.                                          00001640
           02 MTIMJOUH  PIC X.                                          00001650
           02 MTIMJOUV  PIC X.                                          00001660
           02 MTIMJOUO  PIC X(5).                                       00001670
      * PAGE                                                            00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MPAGEA    PIC X.                                          00001700
           02 MPAGEC    PIC X.                                          00001710
           02 MPAGEP    PIC X.                                          00001720
           02 MPAGEH    PIC X.                                          00001730
           02 MPAGEV    PIC X.                                          00001740
           02 MPAGEO    PIC X(3).                                       00001750
      * RECEP. A QUAI                                                   00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNRECEPA  PIC X.                                          00001780
           02 MNRECEPC  PIC X.                                          00001790
           02 MNRECEPP  PIC X.                                          00001800
           02 MNRECEPH  PIC X.                                          00001810
           02 MNRECEPV  PIC X.                                          00001820
           02 MNRECEPO  PIC X(7).                                       00001830
      * SOCIETE                                                         00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MNSOCA    PIC X.                                          00001860
           02 MNSOCC    PIC X.                                          00001870
           02 MNSOCP    PIC X.                                          00001880
           02 MNSOCH    PIC X.                                          00001890
           02 MNSOCV    PIC X.                                          00001900
           02 MNSOCO    PIC X(3).                                       00001910
      * LIB. SOCIETE                                                    00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MLSOCA    PIC X.                                          00001940
           02 MLSOCC    PIC X.                                          00001950
           02 MLSOCP    PIC X.                                          00001960
           02 MLSOCH    PIC X.                                          00001970
           02 MLSOCV    PIC X.                                          00001980
           02 MLSOCO    PIC X(20).                                      00001990
      * NUM. ENTREPOT                                                   00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MNENTREA  PIC X.                                          00002020
           02 MNENTREC  PIC X.                                          00002030
           02 MNENTREP  PIC X.                                          00002040
           02 MNENTREH  PIC X.                                          00002050
           02 MNENTREV  PIC X.                                          00002060
           02 MNENTREO  PIC X(3).                                       00002070
      * LIB. ENTREPOT                                                   00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLENTREA  PIC X.                                          00002100
           02 MLENTREC  PIC X.                                          00002110
           02 MLENTREP  PIC X.                                          00002120
           02 MLENTREH  PIC X.                                          00002130
           02 MLENTREV  PIC X.                                          00002140
           02 MLENTREO  PIC X(20).                                      00002150
      * DATE JOUR REC                                                   00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MDJOURRA  PIC X.                                          00002180
           02 MDJOURRC  PIC X.                                          00002190
           02 MDJOURRP  PIC X.                                          00002200
           02 MDJOURRH  PIC X.                                          00002210
           02 MDJOURRV  PIC X.                                          00002220
           02 MDJOURRO  PIC X(8).                                       00002230
      * HEURE                                                           00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MDHEURRA  PIC X.                                          00002260
           02 MDHEURRC  PIC X.                                          00002270
           02 MDHEURRP  PIC X.                                          00002280
           02 MDHEURRH  PIC X.                                          00002290
           02 MDHEURRV  PIC X.                                          00002300
           02 MDHEURRO  PIC X(2).                                       00002310
      * MINUTE                                                          00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MDMINURA  PIC X.                                          00002340
           02 MDMINURC  PIC X.                                          00002350
           02 MDMINURP  PIC X.                                          00002360
           02 MDMINURH  PIC X.                                          00002370
           02 MDMINURV  PIC X.                                          00002380
           02 MDMINURO  PIC X(2).                                       00002390
      * CODE LIVREUR                                                    00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNLIVREA  PIC X.                                          00002420
           02 MNLIVREC  PIC X.                                          00002430
           02 MNLIVREP  PIC X.                                          00002440
           02 MNLIVREH  PIC X.                                          00002450
           02 MNLIVREV  PIC X.                                          00002460
           02 MNLIVREO  PIC X(5).                                       00002470
      * LIB. LIVREUR                                                    00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MLLIVREA  PIC X.                                          00002500
           02 MLLIVREC  PIC X.                                          00002510
           02 MLLIVREP  PIC X.                                          00002520
           02 MLLIVREH  PIC X.                                          00002530
           02 MLLIVREV  PIC X.                                          00002540
           02 MLLIVREO  PIC X(20).                                      00002550
      * COMMENTAIRE                                                     00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MLCOMMEA  PIC X.                                          00002580
           02 MLCOMMEC  PIC X.                                          00002590
           02 MLCOMMEP  PIC X.                                          00002600
           02 MLCOMMEH  PIC X.                                          00002610
           02 MLCOMMEV  PIC X.                                          00002620
           02 MLCOMMEO  PIC X(20).                                      00002630
      * MODE TRANSPORT                                                  00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MCTRANSA  PIC X.                                          00002660
           02 MCTRANSC  PIC X.                                          00002670
           02 MCTRANSP  PIC X.                                          00002680
           02 MCTRANSH  PIC X.                                          00002690
           02 MCTRANSV  PIC X.                                          00002700
           02 MCTRANSO  PIC X(5).                                       00002710
      * LIB. TRANSPORT                                                  00002720
           02 FILLER    PIC X(2).                                       00002730
           02 MLTRANSA  PIC X.                                          00002740
           02 MLTRANSC  PIC X.                                          00002750
           02 MLTRANSP  PIC X.                                          00002760
           02 MLTRANSH  PIC X.                                          00002770
           02 MLTRANSV  PIC X.                                          00002780
           02 MLTRANSO  PIC X(20).                                      00002790
      * NOMBRE PIECE                                                    00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MQPIECEA  PIC X.                                          00002820
           02 MQPIECEC  PIC X.                                          00002830
           02 MQPIECEP  PIC X.                                          00002840
           02 MQPIECEH  PIC X.                                          00002850
           02 MQPIECEV  PIC X.                                          00002860
           02 MQPIECEO  PIC X(5).                                       00002870
           02 M67O OCCURS   10 TIMES .                                  00002880
      * CODIC                                                           00002890
             03 FILLER       PIC X(2).                                  00002900
             03 MNCODICA     PIC X.                                     00002910
             03 MNCODICC     PIC X.                                     00002920
             03 MNCODICP     PIC X.                                     00002930
             03 MNCODICH     PIC X.                                     00002940
             03 MNCODICV     PIC X.                                     00002950
             03 MNCODICO     PIC X(7).                                  00002960
      * CODE MARQUE                                                     00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MCMARQA      PIC X.                                     00002990
             03 MCMARQC PIC X.                                          00003000
             03 MCMARQP PIC X.                                          00003010
             03 MCMARQH PIC X.                                          00003020
             03 MCMARQV PIC X.                                          00003030
             03 MCMARQO      PIC X(5).                                  00003040
      * CODE FAMILLE                                                    00003050
             03 FILLER       PIC X(2).                                  00003060
             03 MCFAMA  PIC X.                                          00003070
             03 MCFAMC  PIC X.                                          00003080
             03 MCFAMP  PIC X.                                          00003090
             03 MCFAMH  PIC X.                                          00003100
             03 MCFAMV  PIC X.                                          00003110
             03 MCFAMO  PIC X(5).                                       00003120
      * LIB. EMBALLA                                                    00003130
             03 FILLER       PIC X(2).                                  00003140
             03 MLEMBALA     PIC X.                                     00003150
             03 MLEMBALC     PIC X.                                     00003160
             03 MLEMBALP     PIC X.                                     00003170
             03 MLEMBALH     PIC X.                                     00003180
             03 MLEMBALV     PIC X.                                     00003190
             03 MLEMBALO     PIC X(51).                                 00003200
      * QUANTITE                                                        00003210
             03 FILLER       PIC X(2).                                  00003220
             03 MQTEA   PIC X.                                          00003230
             03 MQTEC   PIC X.                                          00003240
             03 MQTEP   PIC X.                                          00003250
             03 MQTEH   PIC X.                                          00003260
             03 MQTEV   PIC X.                                          00003270
             03 MQTEO   PIC X(5).                                       00003280
      * ZONE CMD AIDA                                                   00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MZONCMDA  PIC X.                                          00003310
           02 MZONCMDC  PIC X.                                          00003320
           02 MZONCMDP  PIC X.                                          00003330
           02 MZONCMDH  PIC X.                                          00003340
           02 MZONCMDV  PIC X.                                          00003350
           02 MZONCMDO  PIC X(15).                                      00003360
      * MESSAGE ERREUR                                                  00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MLIBERRA  PIC X.                                          00003390
           02 MLIBERRC  PIC X.                                          00003400
           02 MLIBERRP  PIC X.                                          00003410
           02 MLIBERRH  PIC X.                                          00003420
           02 MLIBERRV  PIC X.                                          00003430
           02 MLIBERRO  PIC X(58).                                      00003440
      * CODE TRANSACTION                                                00003450
           02 FILLER    PIC X(2).                                       00003460
           02 MCODTRAA  PIC X.                                          00003470
           02 MCODTRAC  PIC X.                                          00003480
           02 MCODTRAP  PIC X.                                          00003490
           02 MCODTRAH  PIC X.                                          00003500
           02 MCODTRAV  PIC X.                                          00003510
           02 MCODTRAO  PIC X(4).                                       00003520
      * CICS DE TRAVAIL                                                 00003530
           02 FILLER    PIC X(2).                                       00003540
           02 MCICSA    PIC X.                                          00003550
           02 MCICSC    PIC X.                                          00003560
           02 MCICSP    PIC X.                                          00003570
           02 MCICSH    PIC X.                                          00003580
           02 MCICSV    PIC X.                                          00003590
           02 MCICSO    PIC X(5).                                       00003600
      * NETNAME                                                         00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MNETNAMA  PIC X.                                          00003630
           02 MNETNAMC  PIC X.                                          00003640
           02 MNETNAMP  PIC X.                                          00003650
           02 MNETNAMH  PIC X.                                          00003660
           02 MNETNAMV  PIC X.                                          00003670
           02 MNETNAMO  PIC X(8).                                       00003680
      * CODE TERMINAL                                                   00003690
           02 FILLER    PIC X(2).                                       00003700
           02 MSCREENA  PIC X.                                          00003710
           02 MSCREENC  PIC X.                                          00003720
           02 MSCREENP  PIC X.                                          00003730
           02 MSCREENH  PIC X.                                          00003740
           02 MSCREENV  PIC X.                                          00003750
           02 MSCREENO  PIC X(5).                                       00003760
                                                                                
