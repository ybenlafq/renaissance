      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
      **************************************************************    00000020
      ****     MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00000030
      **************************************************************    00000040
      *                                                                 00000050
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00000060
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00000070
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00000080
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00000090
      *                                                                 00000100
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00000110
      * COMPRENANT :                                                    00000120
      * 1 - LES ZONES RESERVEES A AIDA                                  00000130
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00000140
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00000150
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00000160
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00000170
      *                                                                 00000180
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00000190
      * PAR AIDA                                                        00000200
      *                                                                 00000210
      *-------------------------------------------------------------    00000220
      *                                                                 00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-GR80-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00000240
      *--                                                                       
       01  COM-GR80-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00000250
       01  Z-COMMAREA.                                                  00000260
      *                                                                 00000270
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00000280
           02 FILLER-COM-AIDA      PIC X(100).                          00000290
      *                                                                 00000300
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00000310
           02 COMM-CICS-APPLID     PIC X(8).                            00000320
           02 COMM-CICS-NETNAM     PIC X(8).                            00000330
           02 COMM-CICS-TRANSA     PIC X(4).                            00000340
      *                                                                 00000350
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00000360
           02 COMM-DATE-SIECLE     PIC XX.                              00000370
           02 COMM-DATE-ANNEE      PIC XX.                              00000380
           02 COMM-DATE-MOIS       PIC XX.                              00000390
           02 COMM-DATE-JOUR       PIC 99.                              00000400
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000410
           02 COMM-DATE-QNTA       PIC 999.                             00000420
           02 COMM-DATE-QNT0       PIC 99999.                           00000430
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000440
           02 COMM-DATE-BISX       PIC 9.                               00000450
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00000460
           02 COMM-DATE-JSM        PIC 9.                               00000470
      *   LIBELLES DU JOUR COURT - LONG                                 00000480
           02 COMM-DATE-JSM-LC     PIC XXX.                             00000490
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00000500
      *   LIBELLES DU MOIS COURT - LONG                                 00000510
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00000520
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00000530
      *   DIFFERENTES FORMES DE DATE                                    00000540
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00000550
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00000560
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00000570
           02 COMM-DATE-JJMMAA     PIC X(6).                            00000580
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00000590
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00000600
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00000610
           02 COMM-DATE-WEEK.                                           00000620
              05 COMM-DATE-SEMSS   PIC 99.                              00000630
              05 COMM-DATE-SEMAA   PIC 99.                              00000640
              05 COMM-DATE-SEMNU   PIC 99.                              00000650
      *                                                                 00000660
           02 COMM-DATE-FILLER     PIC X(08).                           00000670
      *                                                                 00000680
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00000700
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                       00000710
      *                                                                 00000720
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00000730
      *                                                                 00000740
           02 COMM-GR80-MENU.                                           00000750
              03 COMM-GR80-OPTION               PIC X(1).               00000760
              03 COMM-GR80-CACID                PIC X(8).               00000760
              03 COMM-GR80-NSOCIETE             PIC X(3).               00000760
              03 COMM-GR80-NLIEU                PIC X(3).               00000770
              03 COMM-GR80-LLIEU                PIC X(20).              00000780
              03 COMM-GR80-MONODEP              PIC X(1).               00000780
              03 COMM-GR80-DMUTATION1           PIC X(8).               00000790
              03 COMM-GR80-DMUTATION2           PIC X(8).               00000800
              03 COMM-GR80-NMUTATION            PIC X(7).               00000810
      *                                                                 00000820
      * ZONES APPLICATIVES (PAR REDEFINES) ----------------             00000830
      *                                                                 00000840
          02 COMM-GR80-APPLI.                                           00000850
             03 COMM-GR80-FILLER                PIC X(3665).            00000860
      *                                                                 00000870
      * ZONES ECRAN RC05 (LISTE DES MUTS   ) --------------             00000880
      *                                                                 00000890
          02 COMM-GR81-APPLI REDEFINES COMM-GR80-APPLI.                 00000900
             03 COMM-GR81-PAGE               PIC S9(3) COMP-3.          00000910
             03 COMM-GR81-PAGEMAX            PIC S9(3) COMP-3.          00000920
             03 COMM-GR81-NBMUT              PIC S9(7) COMP-3.          00000930
             03 COMM-GR81-FILLER             PIC X(3657).               00000940
      *                                                                 00000870
                                                                                
