      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00010004
      * COMMAREA SPECIFIQUE PRG TGF43                     TR: GF43      00020004
      *************************************************SOIT 6405C.**    00040004
        02 COMMGF43-APPLI REDEFINES COMM-GF00-APPLI.                            
E0191.*--- ZONE D'AFFICHAGE VARIABLE                                            
          03 COMM-GF43-LIGNES.                                                  
           04 COMM-GF43-LIGNE11             PIC X(15).                          
.E0191     04 COMM-GF43-LIGNE13             PIC X(25).                          
E0311      04 COMM-GF43-LIGNE20             PIC X(11).                          
          03 COMM-GF43-TP.                                                      
           04 COMM-GF43-DATA.                                                   
              05 COMM-GF43-NSOC             PIC X(03).                          
              05 COMM-GF43-CTYPCDE          PIC X(05).                          
              05 COMM-GF43-CINTERLOCUT      PIC X(05).                          
              05 COMM-GF43-DVALIDITE        PIC X(08).                          
              05 COMM-GF43-O-DVALIDITE      PIC X(08).                          
              05 COMM-GF43-LINTERLOCUT      PIC X(20).                          
              05 COMM-GF43-CODRET           PIC X.                              
                 88 COMM-GF43-OK                  VALUE '0'.                    
                 88 COMM-GF43-ERREUR              VALUE '1'.                    
              05 COMM-GF43-MULTI-A-SIMPLE   PIC X.                              
                 88 COMM-GF43-MULTI-OK            VALUE '0'.                    
                 88 COMM-GF43-SIMPLE-OK           VALUE '1'.                    
              05 COMM-GF43-DSAISIE          PIC X(08).                          
              05 COMM-GF43-NSURCDE          PIC X(07).                          
              05 COMM-GF43-ZONE-MAP OCCURS 3.                                   
                 06 COMM-GF43-NCDE-MAP      PIC X(07).                          
              05 COMM-GF43-ENTREPOT OCCURS 10.                                  
                 06 COMM-GF43-NCDE          PIC X(07).                          
      *          06 COMM-GF43-NSOC          PIC X(03).                          
                 06 COMM-GF43-NDEPOT        PIC X(03).                          
                 06 COMM-GF43-TOPMAJ-ENT    PIC X.                              
                 06 COMM-GF43-FL90-WQJOUR   PIC X(01).                          
                 06 COMM-GF43-FL90-WRESFOUR PIC X(01).                          
                 06 COMM-GF43-FL90-WFM      PIC X(01).                          
                 06 COMM-GF43-FL90-WGENGRO  PIC X(03).                          
DAR8             06 COMM-GF43-REVALORISER   PIC X(1).                           
E0012            06 COMM-GF43-REOUVERTURE   PIC X(1).                           
                 06 COMM-GF43-SEMAINE OCCURS 20.                                
                    07 COMM-GF43-DLIVRAISON.                                    
                       08 SS    PIC X(02).                                      
                       08 AA    PIC X(02).                                      
                       08 MM    PIC X(02).                                      
                       08 JJ    PIC X(02).                                      
E0105.*------ FLAG DATA CAPTURE, INDIC SI ON DOIT SAISIR UN REASON CODE         
                    07 COMM-GF43-DAT-CREAS-F   PIC X(1).                        
                       88 COMM-GF43-DAT-CREAS-O VALUE 'O'.                      
                       88 COMM-GF43-DAT-CREAS-N VALUE 'N'.                      
                       88 COMM-GF43-DAT-CREAS-R VALUE 'R'.                      
E0105.*------ CODE RAISON  SAISI                                                
                    07 COMM-GF43-CODE-REASON   PIC X(5).                        
              05 COMM-GF43-TGF44        PIC X(1).                               
                 88 COMM-GF43-TGF44-OK           VALUE '1'.                     
                 88 COMM-GF43-TGF44-KO           VALUE '0'.                     
              05 COMM-GF43-MULTILIEU    PIC X(1).                               
                 88 COMM-GF43-MULTILIEU-OK       VALUE '1'.                     
                 88 COMM-GF43-MULTILIEU-KO       VALUE '0'.                     
           04 COMM-GF43-TAB-NB-DATES    OCCURS 10.                              
              05 COMM-GF43-NB-DATES       PIC 9(2).                             
      *                                                                         
           04 COMM-GF43-RANG-TS         PIC 9(3).                               
           04 COMM-GF43-XCTRL-CDEAN           PIC X(1).                         
           04 COMM-GF43-XCTRL-VALO            PIC X(1).                         
           04 COMM-GF43-XCTRL-INTRFC          PIC X(1).                         
GD         04 COMM-GF43-XCTRL-FLGDAT          PIC X(01).                        
E0037**    04 COMM-GF43-XCTRL-NCDEFILLE       PIC X(1).                         
E0037**    04 COMM-GF43-XCTRL-LCDE            PIC X(10).                        
CRO183     04 COMM-GF43-XCTRL-WQCOLIRECEPT    PIC X.                            
           04 COMM-GF43-INTRDIT-TGF44         PIC X(1).                         
           04 COMM-GF43-ENTETE                PIC 9(1).                         
              88 COMM-GF43-ENTETE-NO-MODIF       VALUE 0.                       
              88 COMM-GF43-ENTETE-MODIF          VALUE 1.                       
           04 COMM-GF43-DVAL                  PIC 9(1).                         
              88 COMM-GF43-DVAL-NO-MODIF         VALUE 0.                       
              88 COMM-GF43-DVAL-MODIF            VALUE 1.                       
E0037      04 COMM-GF43-PF12                  PIC 9(1).                         
              88 COMM-GF43-PF12-CONFIRM-KO       VALUE 0.                       
              88 COMM-GF43-PF12-CONFIRM-OK       VALUE 3 THRU 4.                
              88 COMM-GF43-PF12-CONFIRM-F3       VALUE 3.                       
              88 COMM-GF43-PF12-CONFIRM-F4       VALUE 4.                       
E0037      04 COMM-GF43-FLAG-MAJ              PIC 9(1).                         
              88 COMM-GF43-NO-MODIF              VALUE 0.                       
              88 COMM-GF43-MODIF                 VALUE 1.                       
E0105.*------ FLAG DATA CAPTURE, INDIC SI ON DOIT SAISIR UN REASON CODE         
           04 COMM-GF43-CREASON-FLAG          PIC X(1).                         
              88 COMM-GF43-CREASON-N             VALUE 'N'.                     
      *{ remove-comma-in-dde 1.5                                                
      *       88 COMM-GF43-CREASON-O             VALUE 'C', 'L'.                
      *--                                                                       
              88 COMM-GF43-CREASON-O             VALUE 'C'  'L'.                
      *}                                                                        
              88 COMM-GF43-CREASON-C             VALUE 'C'.                     
              88 COMM-GF43-CREASON-L             VALUE 'L'.                     
              88 COMM-GF43-CREASON-R             VALUE 'R'.                     
E0105.*------ DUE DATE + 6 JOURS                                                
           04 COMM-GF43-DUEDATE6              PIC X(8).                         
E0105.*------ DONNEES DATA CAPTURE DE LA MAP EN COURS                           
           04 COMM-GF43-DATAC.                                                  
             05 COMM-GF43-DATAC-MAP-C        OCCURS 3.                          
              06 COMM-GF43-CR-NDEPOT            PIC X(03).                      
              06 COMM-GF43-CR-DLIVR             PIC X(08).                      
              06 COMM-GF43-CREAF-C              PIC X(01).                      
              06 COMM-GF43-CREAS-C              PIC X(05).                      
             05 COMM-GF43-DATAC-MAP-L        OCCURS 12.                         
              06 COMM-GF43-CR-NCODIC            PIC X(07).                      
              06 COMM-GF43-CREAF-L              PIC X(01).                      
              06 COMM-GF43-CREAS-L              PIC X(05).                      
           04 COMM-GF43-FILLER      PIC X(100).                                 
           04 COMM-GF43-GESTION-AFFICHAGE.                                      
              05 COMM-GF43-NBPAGE       PIC 9(03).                              
              05 COMM-GF43-NUMPAGE      PIC 9(03).                              
              05 COMM-GF43-NBCODICMAX   PIC 9(03).                              
              05 COMM-GF43-NUMCODIC     PIC 9(03).                              
              05 COMM-GF43-NBLIST       PIC 9(03).                              
              05 COMM-GF43-NUMLIST      PIC 9(03).                              
              05 COMM-GF43-NBCOLONMAX   PIC 9(03).                              
              05 COMM-GF43-NUMCOLONNE   PIC 9(03).                              
              05 COMM-GF43-1ERE-LIGNE   PIC 9(03).                              
              05 COMM-GF43-1ERE-LISTE.                                          
                 06 COMM-GF43-1-LISTE-E   PIC 9(03).                            
                 06 COMM-GF43-1-LISTE-D   PIC 9(03).                            
              05 COMM-GF43-VERTICAL   PIC X.                                    
                 88 PAS-VERTICAL   VALUE '0'.                                   
                 88 VERTICAL       VALUE '1'.                                   
              05 COMM-GF43-HORIZONTAL PIC X.                                    
                 88 PAS-HORIZONTAL VALUE '0'.                                   
                 88 HORIZONTAL     VALUE '1'.                                   
              05 COMM-GF43-PF9              PIC X.                              
                 88 COMM-GF43-PF9-SOLDE      VALUE '0'.                         
                 88 COMM-GF43-PF9-RECEP      VALUE '1'.                         
              05 COMM-GF43-PF9-LIB.                                             
                 06 COMM-GF43-PF9-R-LIB1   PIC X(12).                           
                 06 COMM-GF43-PF9-R-LIB2   PIC X(12).                           
                 06 COMM-GF43-PF9-S-LIB1   PIC X(12).                           
                 06 COMM-GF43-PF9-S-LIB2   PIC X(12).                           
E0105            06 COMM-GF43-PF9-LORIGDAT PIC X(25).                           
E0105            06 COMM-GF43-PF9-LDATSAI  PIC X(25).                           
GD         04 COMM-GF43-PARMDAT     PIC 9(10).                                  
GD         04 COMM-GF43-DATEPARAM   PIC X(08).                                  
E0191      04 COMM-GF43-FLAG-SAM    PIC X(1).                                   
CRO   *    04 COMM-GF43-MEMO-TOUCHE PIC X(2).                                   
CRO211*    04 COMM-GF43-FILLER-LONG PIC X(151).                                 
E0105 *    ZONE REDEFINIE PAR COMMGF44                                          
CRO211*    04 COMM-GF43-FILLER-LONG PIC X(91).                                  
E0105 *    04 COMM-GF43-FILLER-LONG PIC X(40).                                  
E0191 *    04 COMM-GF43-FILLER-LONG PIC X(42).                                  
E0191      04 COMM-GF43-FILLER-LONG PIC X(41).                                  
      ***************************************************************** 00920004
                                                                                
