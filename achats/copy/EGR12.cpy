      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR12   EGR12                                              00000020
      ***************************************************************** 00000030
       01   EGR12I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * PAGE                                                            00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MPAGEI    PIC X(3).                                       00000200
      * FONCTION                                                        00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFONCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MFONCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MFONCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MFONCI    PIC X(3).                                       00000250
      * semaine                                                         00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATESEML      COMP PIC S9(4).                            00000270
      *--                                                                       
           02 MDATESEML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATESEMF      PIC X.                                     00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MDATESEMI      PIC X(6).                                  00000300
      * encadre                                                         00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE1L  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MLIGNE1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIGNE1F  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLIGNE1I  PIC X(38).                                      00000350
      * NUMERO SOCIETE                                                  00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MNSOCI    PIC X(3).                                       00000400
      * LIBELLE SOCIETE                                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLSOCI    PIC X(20).                                      00000450
      * libelle                                                         00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB1L    COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MLIB1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB1F    PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MLIB1I    PIC X(12).                                      00000500
      * semaine de debut                                                00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSEMDEBL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MSEMDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSEMDEBF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MSEMDEBI  PIC X(6).                                       00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB2L    COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MLIB2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB2F    PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MLIB2I    PIC X.                                          00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSEMFINL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MSEMFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSEMFINF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MSEMFINI  PIC X(6).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB3L    COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MLIB3L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB3F    PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MLIB3I    PIC X.                                          00000670
      * NUM. ENTREPOT                                                   00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENPOTL  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MNENPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENPOTF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MNENPOTI  PIC X(3).                                       00000720
      * LIB. ENTREPOT                                                   00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENPOTL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLENPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLENPOTF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLENPOTI  PIC X(20).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB4L    COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLIB4L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB4F    PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLIB4I    PIC X.                                          00000810
           02 MPOURD OCCURS   7 TIMES .                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPOURL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MPOURL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPOURF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MPOURI  PIC X(2).                                       00000860
           02 MPD OCCURS   7 TIMES .                                    00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPL     COMP PIC S9(4).                                 00000880
      *--                                                                       
             03 MPL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MPF     PIC X.                                          00000890
             03 FILLER  PIC X(4).                                       00000900
             03 MPI     PIC X.                                          00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIB5L    COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MLIB5L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIB5F    PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MLIB5I    PIC X.                                          00000950
      * type de quota                                                   00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUOTAL   COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MQUOTAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQUOTAF   PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MQUOTAI   PIC X(5).                                       00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIGNE2L  COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MLIGNE2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIGNE2F  PIC X.                                          00001020
           02 FILLER    PIC X(4).                                       00001030
           02 MLIGNE2I  PIC X(38).                                      00001040
           02 M336I OCCURS   11 TIMES .                                 00001050
      * NUM. SEMAINE                                                    00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSEML  COMP PIC S9(4).                                 00001070
      *--                                                                       
             03 MNSEML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSEMF  PIC X.                                          00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MNSEMI  PIC X(6).                                       00001100
      * DATE DEBUT                                                      00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDEBL  COMP PIC S9(4).                                 00001120
      *--                                                                       
             03 MDDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDDEBF  PIC X.                                          00001130
             03 FILLER  PIC X(4).                                       00001140
             03 MDDEBI  PIC X(5).                                       00001150
      * DATE FIN                                                        00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFINL  COMP PIC S9(4).                                 00001170
      *--                                                                       
             03 MDFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDFINF  PIC X.                                          00001180
             03 FILLER  PIC X(4).                                       00001190
             03 MDFINI  PIC X(5).                                       00001200
      * QUOTA SEMAINE                                                   00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQQUOTAL     COMP PIC S9(4).                            00001220
      *--                                                                       
             03 MQQUOTAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQQUOTAF     PIC X.                                     00001230
             03 FILLER  PIC X(4).                                       00001240
             03 MQQUOTAI     PIC X(5).                                  00001250
      * QTE PRISE SEMAIN                                                00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPRISL      COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MQPRISL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPRISF      PIC X.                                     00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MQPRISI      PIC X(7).                                  00001300
      * QTE LUNDI                                                       00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLUNDIL     COMP PIC S9(4).                            00001320
      *--                                                                       
             03 MQLUNDIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQLUNDIF     PIC X.                                     00001330
             03 FILLER  PIC X(4).                                       00001340
             03 MQLUNDII     PIC X(5).                                  00001350
      * QTE MARDI                                                       00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQMARDIL     COMP PIC S9(4).                            00001370
      *--                                                                       
             03 MQMARDIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQMARDIF     PIC X.                                     00001380
             03 FILLER  PIC X(4).                                       00001390
             03 MQMARDII     PIC X(5).                                  00001400
      * QTE MERCREDI                                                    00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQMERCRL     COMP PIC S9(4).                            00001420
      *--                                                                       
             03 MQMERCRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQMERCRF     PIC X.                                     00001430
             03 FILLER  PIC X(4).                                       00001440
             03 MQMERCRI     PIC X(5).                                  00001450
      * QTE JEUDI                                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQJEUDIL     COMP PIC S9(4).                            00001470
      *--                                                                       
             03 MQJEUDIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQJEUDIF     PIC X.                                     00001480
             03 FILLER  PIC X(4).                                       00001490
             03 MQJEUDII     PIC X(5).                                  00001500
      * QTE VENDREDI                                                    00001510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQVENDRL     COMP PIC S9(4).                            00001520
      *--                                                                       
             03 MQVENDRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQVENDRF     PIC X.                                     00001530
             03 FILLER  PIC X(4).                                       00001540
             03 MQVENDRI     PIC X(5).                                  00001550
      * QTE SAMEDI                                                      00001560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSAMEDL     COMP PIC S9(4).                            00001570
      *--                                                                       
             03 MQSAMEDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQSAMEDF     PIC X.                                     00001580
             03 FILLER  PIC X(4).                                       00001590
             03 MQSAMEDI     PIC X(5).                                  00001600
      * QTE DIMANCHE                                                    00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQDIMANL     COMP PIC S9(4).                            00001620
      *--                                                                       
             03 MQDIMANL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQDIMANF     PIC X.                                     00001630
             03 FILLER  PIC X(4).                                       00001640
             03 MQDIMANI     PIC X(5).                                  00001650
      * ZONE CMD AIDA                                                   00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MZONCMDI  PIC X(15).                                      00001700
      * MESSAGE ERREUR                                                  00001710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001720
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001730
           02 FILLER    PIC X(4).                                       00001740
           02 MLIBERRI  PIC X(58).                                      00001750
      * CODE TRANSACTION                                                00001760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001770
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001780
           02 FILLER    PIC X(4).                                       00001790
           02 MCODTRAI  PIC X(4).                                       00001800
      * CICS DE TRAVAIL                                                 00001810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001820
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001830
           02 FILLER    PIC X(4).                                       00001840
           02 MCICSI    PIC X(5).                                       00001850
      * NETNAME                                                         00001860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001880
           02 FILLER    PIC X(4).                                       00001890
           02 MNETNAMI  PIC X(8).                                       00001900
      * CODE TERMINAL                                                   00001910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001920
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001930
           02 FILLER    PIC X(4).                                       00001940
           02 MSCREENI  PIC X(5).                                       00001950
      ***************************************************************** 00001960
      * SDF: EGR12   EGR12                                              00001970
      ***************************************************************** 00001980
       01   EGR12O REDEFINES EGR12I.                                    00001990
           02 FILLER    PIC X(12).                                      00002000
      * DATE DU JOUR                                                    00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MDATJOUA  PIC X.                                          00002030
           02 MDATJOUC  PIC X.                                          00002040
           02 MDATJOUP  PIC X.                                          00002050
           02 MDATJOUH  PIC X.                                          00002060
           02 MDATJOUV  PIC X.                                          00002070
           02 MDATJOUO  PIC X(10).                                      00002080
      * HEURE                                                           00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MTIMJOUA  PIC X.                                          00002110
           02 MTIMJOUC  PIC X.                                          00002120
           02 MTIMJOUP  PIC X.                                          00002130
           02 MTIMJOUH  PIC X.                                          00002140
           02 MTIMJOUV  PIC X.                                          00002150
           02 MTIMJOUO  PIC X(5).                                       00002160
      * PAGE                                                            00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MPAGEA    PIC X.                                          00002190
           02 MPAGEC    PIC X.                                          00002200
           02 MPAGEP    PIC X.                                          00002210
           02 MPAGEH    PIC X.                                          00002220
           02 MPAGEV    PIC X.                                          00002230
           02 MPAGEO    PIC X(3).                                       00002240
      * FONCTION                                                        00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MFONCA    PIC X.                                          00002270
           02 MFONCC    PIC X.                                          00002280
           02 MFONCP    PIC X.                                          00002290
           02 MFONCH    PIC X.                                          00002300
           02 MFONCV    PIC X.                                          00002310
           02 MFONCO    PIC X(3).                                       00002320
      * semaine                                                         00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MDATESEMA      PIC X.                                     00002350
           02 MDATESEMC PIC X.                                          00002360
           02 MDATESEMP PIC X.                                          00002370
           02 MDATESEMH PIC X.                                          00002380
           02 MDATESEMV PIC X.                                          00002390
           02 MDATESEMO      PIC X(6).                                  00002400
      * encadre                                                         00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLIGNE1A  PIC X.                                          00002430
           02 MLIGNE1C  PIC X.                                          00002440
           02 MLIGNE1P  PIC X.                                          00002450
           02 MLIGNE1H  PIC X.                                          00002460
           02 MLIGNE1V  PIC X.                                          00002470
           02 MLIGNE1O  PIC X(38).                                      00002480
      * NUMERO SOCIETE                                                  00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MNSOCA    PIC X.                                          00002510
           02 MNSOCC    PIC X.                                          00002520
           02 MNSOCP    PIC X.                                          00002530
           02 MNSOCH    PIC X.                                          00002540
           02 MNSOCV    PIC X.                                          00002550
           02 MNSOCO    PIC X(3).                                       00002560
      * LIBELLE SOCIETE                                                 00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MLSOCA    PIC X.                                          00002590
           02 MLSOCC    PIC X.                                          00002600
           02 MLSOCP    PIC X.                                          00002610
           02 MLSOCH    PIC X.                                          00002620
           02 MLSOCV    PIC X.                                          00002630
           02 MLSOCO    PIC X(20).                                      00002640
      * libelle                                                         00002650
           02 FILLER    PIC X(2).                                       00002660
           02 MLIB1A    PIC X.                                          00002670
           02 MLIB1C    PIC X.                                          00002680
           02 MLIB1P    PIC X.                                          00002690
           02 MLIB1H    PIC X.                                          00002700
           02 MLIB1V    PIC X.                                          00002710
           02 MLIB1O    PIC X(12).                                      00002720
      * semaine de debut                                                00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MSEMDEBA  PIC X.                                          00002750
           02 MSEMDEBC  PIC X.                                          00002760
           02 MSEMDEBP  PIC X.                                          00002770
           02 MSEMDEBH  PIC X.                                          00002780
           02 MSEMDEBV  PIC X.                                          00002790
           02 MSEMDEBO  PIC X(6).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MLIB2A    PIC X.                                          00002820
           02 MLIB2C    PIC X.                                          00002830
           02 MLIB2P    PIC X.                                          00002840
           02 MLIB2H    PIC X.                                          00002850
           02 MLIB2V    PIC X.                                          00002860
           02 MLIB2O    PIC X.                                          00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MSEMFINA  PIC X.                                          00002890
           02 MSEMFINC  PIC X.                                          00002900
           02 MSEMFINP  PIC X.                                          00002910
           02 MSEMFINH  PIC X.                                          00002920
           02 MSEMFINV  PIC X.                                          00002930
           02 MSEMFINO  PIC X(6).                                       00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MLIB3A    PIC X.                                          00002960
           02 MLIB3C    PIC X.                                          00002970
           02 MLIB3P    PIC X.                                          00002980
           02 MLIB3H    PIC X.                                          00002990
           02 MLIB3V    PIC X.                                          00003000
           02 MLIB3O    PIC X.                                          00003010
      * NUM. ENTREPOT                                                   00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MNENPOTA  PIC X.                                          00003040
           02 MNENPOTC  PIC X.                                          00003050
           02 MNENPOTP  PIC X.                                          00003060
           02 MNENPOTH  PIC X.                                          00003070
           02 MNENPOTV  PIC X.                                          00003080
           02 MNENPOTO  PIC X(3).                                       00003090
      * LIB. ENTREPOT                                                   00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MLENPOTA  PIC X.                                          00003120
           02 MLENPOTC  PIC X.                                          00003130
           02 MLENPOTP  PIC X.                                          00003140
           02 MLENPOTH  PIC X.                                          00003150
           02 MLENPOTV  PIC X.                                          00003160
           02 MLENPOTO  PIC X(20).                                      00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MLIB4A    PIC X.                                          00003190
           02 MLIB4C    PIC X.                                          00003200
           02 MLIB4P    PIC X.                                          00003210
           02 MLIB4H    PIC X.                                          00003220
           02 MLIB4V    PIC X.                                          00003230
           02 MLIB4O    PIC X.                                          00003240
           02 DFHMS1 OCCURS   7 TIMES .                                 00003250
             03 FILLER       PIC X(2).                                  00003260
             03 MPOURA  PIC X.                                          00003270
             03 MPOURC  PIC X.                                          00003280
             03 MPOURP  PIC X.                                          00003290
             03 MPOURH  PIC X.                                          00003300
             03 MPOURV  PIC X.                                          00003310
             03 MPOURO  PIC ZZ.                                         00003320
           02 DFHMS2 OCCURS   7 TIMES .                                 00003330
             03 FILLER       PIC X(2).                                  00003340
             03 MPA     PIC X.                                          00003350
             03 MPC     PIC X.                                          00003360
             03 MPP     PIC X.                                          00003370
             03 MPH     PIC X.                                          00003380
             03 MPV     PIC X.                                          00003390
             03 MPO     PIC X.                                          00003400
           02 FILLER    PIC X(2).                                       00003410
           02 MLIB5A    PIC X.                                          00003420
           02 MLIB5C    PIC X.                                          00003430
           02 MLIB5P    PIC X.                                          00003440
           02 MLIB5H    PIC X.                                          00003450
           02 MLIB5V    PIC X.                                          00003460
           02 MLIB5O    PIC X.                                          00003470
      * type de quota                                                   00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MQUOTAA   PIC X.                                          00003500
           02 MQUOTAC   PIC X.                                          00003510
           02 MQUOTAP   PIC X.                                          00003520
           02 MQUOTAH   PIC X.                                          00003530
           02 MQUOTAV   PIC X.                                          00003540
           02 MQUOTAO   PIC X(5).                                       00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MLIGNE2A  PIC X.                                          00003570
           02 MLIGNE2C  PIC X.                                          00003580
           02 MLIGNE2P  PIC X.                                          00003590
           02 MLIGNE2H  PIC X.                                          00003600
           02 MLIGNE2V  PIC X.                                          00003610
           02 MLIGNE2O  PIC X(38).                                      00003620
           02 M336O OCCURS   11 TIMES .                                 00003630
      * NUM. SEMAINE                                                    00003640
             03 FILLER       PIC X(2).                                  00003650
             03 MNSEMA  PIC X.                                          00003660
             03 MNSEMC  PIC X.                                          00003670
             03 MNSEMP  PIC X.                                          00003680
             03 MNSEMH  PIC X.                                          00003690
             03 MNSEMV  PIC X.                                          00003700
             03 MNSEMO  PIC X(6).                                       00003710
      * DATE DEBUT                                                      00003720
             03 FILLER       PIC X(2).                                  00003730
             03 MDDEBA  PIC X.                                          00003740
             03 MDDEBC  PIC X.                                          00003750
             03 MDDEBP  PIC X.                                          00003760
             03 MDDEBH  PIC X.                                          00003770
             03 MDDEBV  PIC X.                                          00003780
             03 MDDEBO  PIC X(5).                                       00003790
      * DATE FIN                                                        00003800
             03 FILLER       PIC X(2).                                  00003810
             03 MDFINA  PIC X.                                          00003820
             03 MDFINC  PIC X.                                          00003830
             03 MDFINP  PIC X.                                          00003840
             03 MDFINH  PIC X.                                          00003850
             03 MDFINV  PIC X.                                          00003860
             03 MDFINO  PIC X(5).                                       00003870
      * QUOTA SEMAINE                                                   00003880
             03 FILLER       PIC X(2).                                  00003890
             03 MQQUOTAA     PIC X.                                     00003900
             03 MQQUOTAC     PIC X.                                     00003910
             03 MQQUOTAP     PIC X.                                     00003920
             03 MQQUOTAH     PIC X.                                     00003930
             03 MQQUOTAV     PIC X.                                     00003940
             03 MQQUOTAO     PIC Z(5).                                  00003950
      * QTE PRISE SEMAIN                                                00003960
             03 FILLER       PIC X(2).                                  00003970
             03 MQPRISA      PIC X.                                     00003980
             03 MQPRISC PIC X.                                          00003990
             03 MQPRISP PIC X.                                          00004000
             03 MQPRISH PIC X.                                          00004010
             03 MQPRISV PIC X.                                          00004020
             03 MQPRISO      PIC Z(4)9,9.                               00004030
      * QTE LUNDI                                                       00004040
             03 FILLER       PIC X(2).                                  00004050
             03 MQLUNDIA     PIC X.                                     00004060
             03 MQLUNDIC     PIC X.                                     00004070
             03 MQLUNDIP     PIC X.                                     00004080
             03 MQLUNDIH     PIC X.                                     00004090
             03 MQLUNDIV     PIC X.                                     00004100
             03 MQLUNDIO     PIC Z(5).                                  00004110
      * QTE MARDI                                                       00004120
             03 FILLER       PIC X(2).                                  00004130
             03 MQMARDIA     PIC X.                                     00004140
             03 MQMARDIC     PIC X.                                     00004150
             03 MQMARDIP     PIC X.                                     00004160
             03 MQMARDIH     PIC X.                                     00004170
             03 MQMARDIV     PIC X.                                     00004180
             03 MQMARDIO     PIC Z(5).                                  00004190
      * QTE MERCREDI                                                    00004200
             03 FILLER       PIC X(2).                                  00004210
             03 MQMERCRA     PIC X.                                     00004220
             03 MQMERCRC     PIC X.                                     00004230
             03 MQMERCRP     PIC X.                                     00004240
             03 MQMERCRH     PIC X.                                     00004250
             03 MQMERCRV     PIC X.                                     00004260
             03 MQMERCRO     PIC Z(5).                                  00004270
      * QTE JEUDI                                                       00004280
             03 FILLER       PIC X(2).                                  00004290
             03 MQJEUDIA     PIC X.                                     00004300
             03 MQJEUDIC     PIC X.                                     00004310
             03 MQJEUDIP     PIC X.                                     00004320
             03 MQJEUDIH     PIC X.                                     00004330
             03 MQJEUDIV     PIC X.                                     00004340
             03 MQJEUDIO     PIC Z(5).                                  00004350
      * QTE VENDREDI                                                    00004360
             03 FILLER       PIC X(2).                                  00004370
             03 MQVENDRA     PIC X.                                     00004380
             03 MQVENDRC     PIC X.                                     00004390
             03 MQVENDRP     PIC X.                                     00004400
             03 MQVENDRH     PIC X.                                     00004410
             03 MQVENDRV     PIC X.                                     00004420
             03 MQVENDRO     PIC Z(5).                                  00004430
      * QTE SAMEDI                                                      00004440
             03 FILLER       PIC X(2).                                  00004450
             03 MQSAMEDA     PIC X.                                     00004460
             03 MQSAMEDC     PIC X.                                     00004470
             03 MQSAMEDP     PIC X.                                     00004480
             03 MQSAMEDH     PIC X.                                     00004490
             03 MQSAMEDV     PIC X.                                     00004500
             03 MQSAMEDO     PIC Z(5).                                  00004510
      * QTE DIMANCHE                                                    00004520
             03 FILLER       PIC X(2).                                  00004530
             03 MQDIMANA     PIC X.                                     00004540
             03 MQDIMANC     PIC X.                                     00004550
             03 MQDIMANP     PIC X.                                     00004560
             03 MQDIMANH     PIC X.                                     00004570
             03 MQDIMANV     PIC X.                                     00004580
             03 MQDIMANO     PIC Z(5).                                  00004590
      * ZONE CMD AIDA                                                   00004600
           02 FILLER    PIC X(2).                                       00004610
           02 MZONCMDA  PIC X.                                          00004620
           02 MZONCMDC  PIC X.                                          00004630
           02 MZONCMDP  PIC X.                                          00004640
           02 MZONCMDH  PIC X.                                          00004650
           02 MZONCMDV  PIC X.                                          00004660
           02 MZONCMDO  PIC X(15).                                      00004670
      * MESSAGE ERREUR                                                  00004680
           02 FILLER    PIC X(2).                                       00004690
           02 MLIBERRA  PIC X.                                          00004700
           02 MLIBERRC  PIC X.                                          00004710
           02 MLIBERRP  PIC X.                                          00004720
           02 MLIBERRH  PIC X.                                          00004730
           02 MLIBERRV  PIC X.                                          00004740
           02 MLIBERRO  PIC X(58).                                      00004750
      * CODE TRANSACTION                                                00004760
           02 FILLER    PIC X(2).                                       00004770
           02 MCODTRAA  PIC X.                                          00004780
           02 MCODTRAC  PIC X.                                          00004790
           02 MCODTRAP  PIC X.                                          00004800
           02 MCODTRAH  PIC X.                                          00004810
           02 MCODTRAV  PIC X.                                          00004820
           02 MCODTRAO  PIC X(4).                                       00004830
      * CICS DE TRAVAIL                                                 00004840
           02 FILLER    PIC X(2).                                       00004850
           02 MCICSA    PIC X.                                          00004860
           02 MCICSC    PIC X.                                          00004870
           02 MCICSP    PIC X.                                          00004880
           02 MCICSH    PIC X.                                          00004890
           02 MCICSV    PIC X.                                          00004900
           02 MCICSO    PIC X(5).                                       00004910
      * NETNAME                                                         00004920
           02 FILLER    PIC X(2).                                       00004930
           02 MNETNAMA  PIC X.                                          00004940
           02 MNETNAMC  PIC X.                                          00004950
           02 MNETNAMP  PIC X.                                          00004960
           02 MNETNAMH  PIC X.                                          00004970
           02 MNETNAMV  PIC X.                                          00004980
           02 MNETNAMO  PIC X(8).                                       00004990
      * CODE TERMINAL                                                   00005000
           02 FILLER    PIC X(2).                                       00005010
           02 MSCREENA  PIC X.                                          00005020
           02 MSCREENC  PIC X.                                          00005030
           02 MSCREENP  PIC X.                                          00005040
           02 MSCREENH  PIC X.                                          00005050
           02 MSCREENV  PIC X.                                          00005060
           02 MSCREENO  PIC X(5).                                       00005070
                                                                                
