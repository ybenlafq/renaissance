      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF37   EGF37                                              00000020
      ***************************************************************** 00000030
       01   EGF37I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFPL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCHEFPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHEFPF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCHEFPI   PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCLIVRL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MSOCLIVRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCLIVRF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MSOCLIVRI      PIC X(3).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOT1L      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNDEPOT1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNDEPOT1F      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNDEPOT1I      PIC X(3).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEBL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDATDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDEBF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDATDEBI  PIC X(10).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFINL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDATFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFINF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDATFINI  PIC X(10).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSTATUTI  PIC X(6).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDLIVR1L  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MDLIVR1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDLIVR1F  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDLIVR1I  PIC X(8).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDLIVR3L  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDLIVR3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDLIVR3F  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDLIVR3I  PIC X(8).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDLIVR2L  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDLIVR2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDLIVR2F  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDLIVR2I  PIC X(8).                                       00000570
           02 MNCDED OCCURS   14 TIMES .                                00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCDEL  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MNCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNCDEF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNCDEI  PIC X(7).                                       00000620
           02 MNCODICD OCCURS   14 TIMES .                              00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000640
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000650
             03 FILLER  PIC X(4).                                       00000660
             03 MNCODICI     PIC X(7).                                  00000670
           02 MNDEPOTD OCCURS   14 TIMES .                              00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDEPOTL     COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MNDEPOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNDEPOTF     PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MNDEPOTI     PIC X(3).                                  00000720
           02 MLREFD OCCURS   14 TIMES .                                00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000740
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000750
             03 FILLER  PIC X(4).                                       00000760
             03 MLREFI  PIC X(14).                                      00000770
           02 MLMARQD OCCURS   14 TIMES .                               00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMARQL      COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MLMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLMARQF      PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MLMARQI      PIC X(5).                                  00000820
           02 MQTOTD OCCURS   14 TIMES .                                00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTOTL  COMP PIC S9(4).                                 00000840
      *--                                                                       
             03 MQTOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTOTF  PIC X.                                          00000850
             03 FILLER  PIC X(4).                                       00000860
             03 MQTOTI  PIC X(5).                                       00000870
           02 MQRECD OCCURS   14 TIMES .                                00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQRECL  COMP PIC S9(4).                                 00000890
      *--                                                                       
             03 MQRECL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQRECF  PIC X.                                          00000900
             03 FILLER  PIC X(4).                                       00000910
             03 MQRECI  PIC X(5).                                       00000920
           02 MQSOLDED OCCURS   14 TIMES .                              00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSOLDEL     COMP PIC S9(4).                            00000940
      *--                                                                       
             03 MQSOLDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQSOLDEF     PIC X.                                     00000950
             03 FILLER  PIC X(4).                                       00000960
             03 MQSOLDEI     PIC X(5).                                  00000970
           02 MQTE1D OCCURS   14 TIMES .                                00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE1L  COMP PIC S9(4).                                 00000990
      *--                                                                       
             03 MQTE1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE1F  PIC X.                                          00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MQTE1I  PIC X(5).                                       00001020
           02 MQTE2D OCCURS   14 TIMES .                                00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE2L  COMP PIC S9(4).                                 00001040
      *--                                                                       
             03 MQTE2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE2F  PIC X.                                          00001050
             03 FILLER  PIC X(4).                                       00001060
             03 MQTE2I  PIC X(5).                                       00001070
           02 MQTE3D OCCURS   14 TIMES .                                00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE3L  COMP PIC S9(4).                                 00001090
      *--                                                                       
             03 MQTE3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE3F  PIC X.                                          00001100
             03 FILLER  PIC X(4).                                       00001110
             03 MQTE3I  PIC X(5).                                       00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001130
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001140
           02 FILLER    PIC X(4).                                       00001150
           02 MZONCMDI  PIC X(15).                                      00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001170
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001180
           02 FILLER    PIC X(4).                                       00001190
           02 MLIBERRI  PIC X(44).                                      00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001210
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001220
           02 FILLER    PIC X(4).                                       00001230
           02 MCODTRAI  PIC X(4).                                       00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001250
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001260
           02 FILLER    PIC X(4).                                       00001270
           02 MCICSI    PIC X(5).                                       00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001290
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001300
           02 FILLER    PIC X(4).                                       00001310
           02 MNETNAMI  PIC X(8).                                       00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001330
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001340
           02 FILLER    PIC X(4).                                       00001350
           02 MSCREENI  PIC X(4).                                       00001360
      ***************************************************************** 00001370
      * SDF: EGF37   EGF37                                              00001380
      ***************************************************************** 00001390
       01   EGF37O REDEFINES EGF37I.                                    00001400
           02 FILLER    PIC X(12).                                      00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MDATJOUA  PIC X.                                          00001430
           02 MDATJOUC  PIC X.                                          00001440
           02 MDATJOUP  PIC X.                                          00001450
           02 MDATJOUH  PIC X.                                          00001460
           02 MDATJOUV  PIC X.                                          00001470
           02 MDATJOUO  PIC X(10).                                      00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MTIMJOUA  PIC X.                                          00001500
           02 MTIMJOUC  PIC X.                                          00001510
           02 MTIMJOUP  PIC X.                                          00001520
           02 MTIMJOUH  PIC X.                                          00001530
           02 MTIMJOUV  PIC X.                                          00001540
           02 MTIMJOUO  PIC X(5).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MPAGEA    PIC X.                                          00001570
           02 MPAGEC    PIC X.                                          00001580
           02 MPAGEP    PIC X.                                          00001590
           02 MPAGEH    PIC X.                                          00001600
           02 MPAGEV    PIC X.                                          00001610
           02 MPAGEO    PIC X(3).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MPAGEMAXA      PIC X.                                     00001640
           02 MPAGEMAXC PIC X.                                          00001650
           02 MPAGEMAXP PIC X.                                          00001660
           02 MPAGEMAXH PIC X.                                          00001670
           02 MPAGEMAXV PIC X.                                          00001680
           02 MPAGEMAXO      PIC X(3).                                  00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCHEFPA   PIC X.                                          00001710
           02 MCHEFPC   PIC X.                                          00001720
           02 MCHEFPP   PIC X.                                          00001730
           02 MCHEFPH   PIC X.                                          00001740
           02 MCHEFPV   PIC X.                                          00001750
           02 MCHEFPO   PIC X(5).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MSOCLIVRA      PIC X.                                     00001780
           02 MSOCLIVRC PIC X.                                          00001790
           02 MSOCLIVRP PIC X.                                          00001800
           02 MSOCLIVRH PIC X.                                          00001810
           02 MSOCLIVRV PIC X.                                          00001820
           02 MSOCLIVRO      PIC X(3).                                  00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MNDEPOT1A      PIC X.                                     00001850
           02 MNDEPOT1C PIC X.                                          00001860
           02 MNDEPOT1P PIC X.                                          00001870
           02 MNDEPOT1H PIC X.                                          00001880
           02 MNDEPOT1V PIC X.                                          00001890
           02 MNDEPOT1O      PIC X(3).                                  00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MDATDEBA  PIC X.                                          00001920
           02 MDATDEBC  PIC X.                                          00001930
           02 MDATDEBP  PIC X.                                          00001940
           02 MDATDEBH  PIC X.                                          00001950
           02 MDATDEBV  PIC X.                                          00001960
           02 MDATDEBO  PIC X(10).                                      00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MDATFINA  PIC X.                                          00001990
           02 MDATFINC  PIC X.                                          00002000
           02 MDATFINP  PIC X.                                          00002010
           02 MDATFINH  PIC X.                                          00002020
           02 MDATFINV  PIC X.                                          00002030
           02 MDATFINO  PIC X(10).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MSTATUTA  PIC X.                                          00002060
           02 MSTATUTC  PIC X.                                          00002070
           02 MSTATUTP  PIC X.                                          00002080
           02 MSTATUTH  PIC X.                                          00002090
           02 MSTATUTV  PIC X.                                          00002100
           02 MSTATUTO  PIC X(6).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MDLIVR1A  PIC X.                                          00002130
           02 MDLIVR1C  PIC X.                                          00002140
           02 MDLIVR1P  PIC X.                                          00002150
           02 MDLIVR1H  PIC X.                                          00002160
           02 MDLIVR1V  PIC X.                                          00002170
           02 MDLIVR1O  PIC X(8).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MDLIVR3A  PIC X.                                          00002200
           02 MDLIVR3C  PIC X.                                          00002210
           02 MDLIVR3P  PIC X.                                          00002220
           02 MDLIVR3H  PIC X.                                          00002230
           02 MDLIVR3V  PIC X.                                          00002240
           02 MDLIVR3O  PIC X(8).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MDLIVR2A  PIC X.                                          00002270
           02 MDLIVR2C  PIC X.                                          00002280
           02 MDLIVR2P  PIC X.                                          00002290
           02 MDLIVR2H  PIC X.                                          00002300
           02 MDLIVR2V  PIC X.                                          00002310
           02 MDLIVR2O  PIC X(8).                                       00002320
           02 DFHMS1 OCCURS   14 TIMES .                                00002330
             03 FILLER       PIC X(2).                                  00002340
             03 MNCDEA  PIC X.                                          00002350
             03 MNCDEC  PIC X.                                          00002360
             03 MNCDEP  PIC X.                                          00002370
             03 MNCDEH  PIC X.                                          00002380
             03 MNCDEV  PIC X.                                          00002390
             03 MNCDEO  PIC X(7).                                       00002400
           02 DFHMS2 OCCURS   14 TIMES .                                00002410
             03 FILLER       PIC X(2).                                  00002420
             03 MNCODICA     PIC X.                                     00002430
             03 MNCODICC     PIC X.                                     00002440
             03 MNCODICP     PIC X.                                     00002450
             03 MNCODICH     PIC X.                                     00002460
             03 MNCODICV     PIC X.                                     00002470
             03 MNCODICO     PIC X(7).                                  00002480
           02 DFHMS3 OCCURS   14 TIMES .                                00002490
             03 FILLER       PIC X(2).                                  00002500
             03 MNDEPOTA     PIC X.                                     00002510
             03 MNDEPOTC     PIC X.                                     00002520
             03 MNDEPOTP     PIC X.                                     00002530
             03 MNDEPOTH     PIC X.                                     00002540
             03 MNDEPOTV     PIC X.                                     00002550
             03 MNDEPOTO     PIC X(3).                                  00002560
           02 DFHMS4 OCCURS   14 TIMES .                                00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MLREFA  PIC X.                                          00002590
             03 MLREFC  PIC X.                                          00002600
             03 MLREFP  PIC X.                                          00002610
             03 MLREFH  PIC X.                                          00002620
             03 MLREFV  PIC X.                                          00002630
             03 MLREFO  PIC X(14).                                      00002640
           02 DFHMS5 OCCURS   14 TIMES .                                00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MLMARQA      PIC X.                                     00002670
             03 MLMARQC PIC X.                                          00002680
             03 MLMARQP PIC X.                                          00002690
             03 MLMARQH PIC X.                                          00002700
             03 MLMARQV PIC X.                                          00002710
             03 MLMARQO      PIC X(5).                                  00002720
           02 DFHMS6 OCCURS   14 TIMES .                                00002730
             03 FILLER       PIC X(2).                                  00002740
             03 MQTOTA  PIC X.                                          00002750
             03 MQTOTC  PIC X.                                          00002760
             03 MQTOTP  PIC X.                                          00002770
             03 MQTOTH  PIC X.                                          00002780
             03 MQTOTV  PIC X.                                          00002790
             03 MQTOTO  PIC X(5).                                       00002800
           02 DFHMS7 OCCURS   14 TIMES .                                00002810
             03 FILLER       PIC X(2).                                  00002820
             03 MQRECA  PIC X.                                          00002830
             03 MQRECC  PIC X.                                          00002840
             03 MQRECP  PIC X.                                          00002850
             03 MQRECH  PIC X.                                          00002860
             03 MQRECV  PIC X.                                          00002870
             03 MQRECO  PIC X(5).                                       00002880
           02 DFHMS8 OCCURS   14 TIMES .                                00002890
             03 FILLER       PIC X(2).                                  00002900
             03 MQSOLDEA     PIC X.                                     00002910
             03 MQSOLDEC     PIC X.                                     00002920
             03 MQSOLDEP     PIC X.                                     00002930
             03 MQSOLDEH     PIC X.                                     00002940
             03 MQSOLDEV     PIC X.                                     00002950
             03 MQSOLDEO     PIC X(5).                                  00002960
           02 DFHMS9 OCCURS   14 TIMES .                                00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MQTE1A  PIC X.                                          00002990
             03 MQTE1C  PIC X.                                          00003000
             03 MQTE1P  PIC X.                                          00003010
             03 MQTE1H  PIC X.                                          00003020
             03 MQTE1V  PIC X.                                          00003030
             03 MQTE1O  PIC X(5).                                       00003040
           02 DFHMS10 OCCURS   14 TIMES .                               00003050
             03 FILLER       PIC X(2).                                  00003060
             03 MQTE2A  PIC X.                                          00003070
             03 MQTE2C  PIC X.                                          00003080
             03 MQTE2P  PIC X.                                          00003090
             03 MQTE2H  PIC X.                                          00003100
             03 MQTE2V  PIC X.                                          00003110
             03 MQTE2O  PIC X(5).                                       00003120
           02 DFHMS11 OCCURS   14 TIMES .                               00003130
             03 FILLER       PIC X(2).                                  00003140
             03 MQTE3A  PIC X.                                          00003150
             03 MQTE3C  PIC X.                                          00003160
             03 MQTE3P  PIC X.                                          00003170
             03 MQTE3H  PIC X.                                          00003180
             03 MQTE3V  PIC X.                                          00003190
             03 MQTE3O  PIC X(5).                                       00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MZONCMDA  PIC X.                                          00003220
           02 MZONCMDC  PIC X.                                          00003230
           02 MZONCMDP  PIC X.                                          00003240
           02 MZONCMDH  PIC X.                                          00003250
           02 MZONCMDV  PIC X.                                          00003260
           02 MZONCMDO  PIC X(15).                                      00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MLIBERRA  PIC X.                                          00003290
           02 MLIBERRC  PIC X.                                          00003300
           02 MLIBERRP  PIC X.                                          00003310
           02 MLIBERRH  PIC X.                                          00003320
           02 MLIBERRV  PIC X.                                          00003330
           02 MLIBERRO  PIC X(44).                                      00003340
           02 FILLER    PIC X(2).                                       00003350
           02 MCODTRAA  PIC X.                                          00003360
           02 MCODTRAC  PIC X.                                          00003370
           02 MCODTRAP  PIC X.                                          00003380
           02 MCODTRAH  PIC X.                                          00003390
           02 MCODTRAV  PIC X.                                          00003400
           02 MCODTRAO  PIC X(4).                                       00003410
           02 FILLER    PIC X(2).                                       00003420
           02 MCICSA    PIC X.                                          00003430
           02 MCICSC    PIC X.                                          00003440
           02 MCICSP    PIC X.                                          00003450
           02 MCICSH    PIC X.                                          00003460
           02 MCICSV    PIC X.                                          00003470
           02 MCICSO    PIC X(5).                                       00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MNETNAMA  PIC X.                                          00003500
           02 MNETNAMC  PIC X.                                          00003510
           02 MNETNAMP  PIC X.                                          00003520
           02 MNETNAMH  PIC X.                                          00003530
           02 MNETNAMV  PIC X.                                          00003540
           02 MNETNAMO  PIC X(8).                                       00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MSCREENA  PIC X.                                          00003570
           02 MSCREENC  PIC X.                                          00003580
           02 MSCREENP  PIC X.                                          00003590
           02 MSCREENH  PIC X.                                          00003600
           02 MSCREENV  PIC X.                                          00003610
           02 MSCREENO  PIC X(4).                                       00003620
                                                                                
