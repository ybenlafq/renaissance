      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF43   EGF43                                              00000020
      ***************************************************************** 00000030
       01   EGF43I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
           02 MZONE1I .                                                 00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATJOUL     COMP PIC S9(4).                            00000070
      *--                                                                       
             03 MDATJOUL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATJOUF     PIC X.                                     00000080
             03 FILLER  PIC X(4).                                       00000090
             03 MDATJOUI     PIC X(10).                                 00000100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTIMJOUL     COMP PIC S9(4).                            00000110
      *--                                                                       
             03 MTIMJOUL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTIMJOUF     PIC X.                                     00000120
             03 FILLER  PIC X(4).                                       00000130
             03 MTIMJOUI     PIC X(5).                                  00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWPAGEL      COMP PIC S9(4).                            00000150
      *--                                                                       
             03 MWPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWPAGEF      PIC X.                                     00000160
             03 FILLER  PIC X(4).                                       00000170
             03 MWPAGEI      PIC X(3).                                  00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWLISTEL     COMP PIC S9(4).                            00000190
      *--                                                                       
             03 MWLISTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWLISTEF     PIC X.                                     00000200
             03 FILLER  PIC X(4).                                       00000210
             03 MWLISTEI     PIC X(3).                                  00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSURCDEL    COMP PIC S9(4).                            00000230
      *--                                                                       
             03 MNSURCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNSURCDEF    PIC X.                                     00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MNSURCDEI    PIC X(7).                                  00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPCOL     COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MCTYPCOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCTYPCOF     PIC X.                                     00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MCTYPCOI     PIC X(5).                                  00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTCDEL    COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MNENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNENTCDEF    PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MNENTCDEI    PIC X(5).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENTCDEL    COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MLENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLENTCDEF    PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MLENTCDEI    PIC X(7).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATSAIL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MDATSAIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATSAIF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MDATSAII     PIC X(8).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCINTERL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MCINTERL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCINTERF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCINTERI     PIC X(5).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLINTERL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MLINTERL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLINTERF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MLINTERI     PIC X(20).                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLDATSAIL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MLDATSAIL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLDATSAIF    PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLDATSAII    PIC X(25).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATVALL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MDATVALL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDATVALF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MDATVALI     PIC X(8).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNE11L    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MLIGNE11L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIGNE11F    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MLIGNE11I    PIC X(15).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNE12L    COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MLIGNE12L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIGNE12F    PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MLIGNE12I    PIC X(3).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNE20L    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MLIGNE20L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIGNE20F    PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MLIGNE20I    PIC X(11).                                 00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVALMANOL    COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MVALMANOL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MVALMANOF    PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MVALMANOI    PIC X.                                     00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNE13L    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MLIGNE13L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIGNE13F    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MLIGNE13I    PIC X(25).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNE14L    COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MLIGNE14L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIGNE14F    PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MLIGNE14I    PIC X(6).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MNSOCI  PIC X(3).                                       00000860
             03 MNDEPD OCCURS   3 TIMES .                               00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNDEPL     COMP PIC S9(4).                            00000880
      *--                                                                       
               04 MNDEPL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MNDEPF     PIC X.                                     00000890
               04 FILLER     PIC X(4).                                  00000900
               04 MNDEPI     PIC X(3).                                  00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLIB1L      COMP PIC S9(4).                            00000920
      *--                                                                       
             03 MQLIB1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQLIB1F      PIC X.                                     00000930
             03 FILLER  PIC X(4).                                       00000940
             03 MQLIB1I      PIC X(12).                                 00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLIB2L      COMP PIC S9(4).                            00000960
      *--                                                                       
             03 MQLIB2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQLIB2F      PIC X.                                     00000970
             03 FILLER  PIC X(4).                                       00000980
             03 MQLIB2I      PIC X(12).                                 00000990
             03 MTAB1I OCCURS   12 TIMES .                              00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNCODICL   COMP PIC S9(4).                            00001010
      *--                                                                       
               04 MNCODICL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MNCODICF   PIC X.                                     00001020
               04 FILLER     PIC X(4).                                  00001030
               04 MNCODICI   PIC X(7).                                  00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MLREFFOURNL     COMP PIC S9(4).                       00001050
      *--                                                                       
               04 MLREFFOURNL COMP-5 PIC S9(4).                                 
      *}                                                                        
               04 MLREFFOURNF     PIC X.                                00001060
               04 FILLER     PIC X(4).                                  00001070
               04 MLREFFOURNI     PIC X(20).                            00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MQTOTL     COMP PIC S9(4).                            00001090
      *--                                                                       
               04 MQTOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MQTOTF     PIC X.                                     00001100
               04 FILLER     PIC X(4).                                  00001110
               04 MQTOTI     PIC X(6).                                  00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MQRECSOLL  COMP PIC S9(4).                            00001130
      *--                                                                       
               04 MQRECSOLL COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MQRECSOLF  PIC X.                                     00001140
               04 FILLER     PIC X(4).                                  00001150
               04 MQRECSOLI  PIC X(6).                                  00001160
               04 MQCDED OCCURS   3 TIMES .                             00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 MQCDEL   COMP PIC S9(4).                            00001180
      *--                                                                       
                 05 MQCDEL COMP-5 PIC S9(4).                                    
      *}                                                                        
                 05 MQCDEF   PIC X.                                     00001190
                 05 FILLER   PIC X(4).                                  00001200
                 05 MQCDEI   PIC X(5).                                  00001210
               04 MQUOD OCCURS   3 TIMES .                              00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          05 MQUOL    COMP PIC S9(4).                            00001230
      *--                                                                       
                 05 MQUOL COMP-5 PIC S9(4).                                     
      *}                                                                        
                 05 MQUOF    PIC X.                                     00001240
                 05 FILLER   PIC X(4).                                  00001250
                 05 MQUOI    PIC X(5).                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZONCMDL     COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MZONCMDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MZONCMDF     PIC X.                                     00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MZONCMDI     PIC X(15).                                 00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBERRL     COMP PIC S9(4).                            00001310
      *--                                                                       
             03 MLIBERRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLIBERRF     PIC X.                                     00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MLIBERRI     PIC X(58).                                 00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODTRAL     COMP PIC S9(4).                            00001350
      *--                                                                       
             03 MCODTRAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCODTRAF     PIC X.                                     00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MCODTRAI     PIC X(4).                                  00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCICSL  COMP PIC S9(4).                                 00001390
      *--                                                                       
             03 MCICSL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCICSF  PIC X.                                          00001400
             03 FILLER  PIC X(4).                                       00001410
             03 MCICSI  PIC X(5).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNETNAML     COMP PIC S9(4).                            00001430
      *--                                                                       
             03 MNETNAML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNETNAMF     PIC X.                                     00001440
             03 FILLER  PIC X(4).                                       00001450
             03 MNETNAMI     PIC X(8).                                  00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSCREENL     COMP PIC S9(4).                            00001470
      *--                                                                       
             03 MSCREENL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSCREENF     PIC X.                                     00001480
             03 FILLER  PIC X(4).                                       00001490
             03 MSCREENI     PIC X(4).                                  00001500
           02 MZONE2I .                                                 00001510
             03 MDATLIVD1I OCCURS   1 TIMES .                           00001520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MDATLIV1L  COMP PIC S9(4).                            00001530
      *--                                                                       
               04 MDATLIV1L COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MDATLIV1F  PIC X.                                     00001540
               04 FILLER     PIC X(4).                                  00001550
               04 MDATLIV1I  PIC X(8).                                  00001560
             03 MDATLIVD2I OCCURS   1 TIMES .                           00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MDATLIV2L  COMP PIC S9(4).                            00001580
      *--                                                                       
               04 MDATLIV2L COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MDATLIV2F  PIC X.                                     00001590
               04 FILLER     PIC X(4).                                  00001600
               04 MDATLIV2I  PIC X(8).                                  00001610
             03 MDATLIVD3I OCCURS   1 TIMES .                           00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MDATLIV3L  COMP PIC S9(4).                            00001630
      *--                                                                       
               04 MDATLIV3L COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MDATLIV3F  PIC X.                                     00001640
               04 FILLER     PIC X(4).                                  00001650
               04 MDATLIV3I  PIC X(8).                                  00001660
      ***************************************************************** 00001670
      * SDF: EGF43   EGF43                                              00001680
      ***************************************************************** 00001690
       01   EGF43O REDEFINES EGF43I.                                    00001700
           02 FILLER    PIC X(12).                                      00001710
           02 MZONE1O .                                                 00001720
             03 FILLER       PIC X(2).                                  00001730
             03 MDATJOUA     PIC X.                                     00001740
             03 MDATJOUC     PIC X.                                     00001750
             03 MDATJOUP     PIC X.                                     00001760
             03 MDATJOUH     PIC X.                                     00001770
             03 MDATJOUV     PIC X.                                     00001780
             03 MDATJOUO     PIC X(10).                                 00001790
             03 FILLER       PIC X(2).                                  00001800
             03 MTIMJOUA     PIC X.                                     00001810
             03 MTIMJOUC     PIC X.                                     00001820
             03 MTIMJOUP     PIC X.                                     00001830
             03 MTIMJOUH     PIC X.                                     00001840
             03 MTIMJOUV     PIC X.                                     00001850
             03 MTIMJOUO     PIC X(5).                                  00001860
             03 FILLER       PIC X(2).                                  00001870
             03 MWPAGEA      PIC X.                                     00001880
             03 MWPAGEC PIC X.                                          00001890
             03 MWPAGEP PIC X.                                          00001900
             03 MWPAGEH PIC X.                                          00001910
             03 MWPAGEV PIC X.                                          00001920
             03 MWPAGEO      PIC X(3).                                  00001930
             03 FILLER       PIC X(2).                                  00001940
             03 MWLISTEA     PIC X.                                     00001950
             03 MWLISTEC     PIC X.                                     00001960
             03 MWLISTEP     PIC X.                                     00001970
             03 MWLISTEH     PIC X.                                     00001980
             03 MWLISTEV     PIC X.                                     00001990
             03 MWLISTEO     PIC X(3).                                  00002000
             03 FILLER       PIC X(2).                                  00002010
             03 MNSURCDEA    PIC X.                                     00002020
             03 MNSURCDEC    PIC X.                                     00002030
             03 MNSURCDEP    PIC X.                                     00002040
             03 MNSURCDEH    PIC X.                                     00002050
             03 MNSURCDEV    PIC X.                                     00002060
             03 MNSURCDEO    PIC X(7).                                  00002070
             03 FILLER       PIC X(2).                                  00002080
             03 MCTYPCOA     PIC X.                                     00002090
             03 MCTYPCOC     PIC X.                                     00002100
             03 MCTYPCOP     PIC X.                                     00002110
             03 MCTYPCOH     PIC X.                                     00002120
             03 MCTYPCOV     PIC X.                                     00002130
             03 MCTYPCOO     PIC X(5).                                  00002140
             03 FILLER       PIC X(2).                                  00002150
             03 MNENTCDEA    PIC X.                                     00002160
             03 MNENTCDEC    PIC X.                                     00002170
             03 MNENTCDEP    PIC X.                                     00002180
             03 MNENTCDEH    PIC X.                                     00002190
             03 MNENTCDEV    PIC X.                                     00002200
             03 MNENTCDEO    PIC X(5).                                  00002210
             03 FILLER       PIC X(2).                                  00002220
             03 MLENTCDEA    PIC X.                                     00002230
             03 MLENTCDEC    PIC X.                                     00002240
             03 MLENTCDEP    PIC X.                                     00002250
             03 MLENTCDEH    PIC X.                                     00002260
             03 MLENTCDEV    PIC X.                                     00002270
             03 MLENTCDEO    PIC X(7).                                  00002280
             03 FILLER       PIC X(2).                                  00002290
             03 MDATSAIA     PIC X.                                     00002300
             03 MDATSAIC     PIC X.                                     00002310
             03 MDATSAIP     PIC X.                                     00002320
             03 MDATSAIH     PIC X.                                     00002330
             03 MDATSAIV     PIC X.                                     00002340
             03 MDATSAIO     PIC X(8).                                  00002350
             03 FILLER       PIC X(2).                                  00002360
             03 MCINTERA     PIC X.                                     00002370
             03 MCINTERC     PIC X.                                     00002380
             03 MCINTERP     PIC X.                                     00002390
             03 MCINTERH     PIC X.                                     00002400
             03 MCINTERV     PIC X.                                     00002410
             03 MCINTERO     PIC X(5).                                  00002420
             03 FILLER       PIC X(2).                                  00002430
             03 MLINTERA     PIC X.                                     00002440
             03 MLINTERC     PIC X.                                     00002450
             03 MLINTERP     PIC X.                                     00002460
             03 MLINTERH     PIC X.                                     00002470
             03 MLINTERV     PIC X.                                     00002480
             03 MLINTERO     PIC X(20).                                 00002490
             03 FILLER       PIC X(2).                                  00002500
             03 MLDATSAIA    PIC X.                                     00002510
             03 MLDATSAIC    PIC X.                                     00002520
             03 MLDATSAIP    PIC X.                                     00002530
             03 MLDATSAIH    PIC X.                                     00002540
             03 MLDATSAIV    PIC X.                                     00002550
             03 MLDATSAIO    PIC X(25).                                 00002560
             03 FILLER       PIC X(2).                                  00002570
             03 MDATVALA     PIC X.                                     00002580
             03 MDATVALC     PIC X.                                     00002590
             03 MDATVALP     PIC X.                                     00002600
             03 MDATVALH     PIC X.                                     00002610
             03 MDATVALV     PIC X.                                     00002620
             03 MDATVALO     PIC X(8).                                  00002630
             03 FILLER       PIC X(2).                                  00002640
             03 MLIGNE11A    PIC X.                                     00002650
             03 MLIGNE11C    PIC X.                                     00002660
             03 MLIGNE11P    PIC X.                                     00002670
             03 MLIGNE11H    PIC X.                                     00002680
             03 MLIGNE11V    PIC X.                                     00002690
             03 MLIGNE11O    PIC X(15).                                 00002700
             03 FILLER       PIC X(2).                                  00002710
             03 MLIGNE12A    PIC X.                                     00002720
             03 MLIGNE12C    PIC X.                                     00002730
             03 MLIGNE12P    PIC X.                                     00002740
             03 MLIGNE12H    PIC X.                                     00002750
             03 MLIGNE12V    PIC X.                                     00002760
             03 MLIGNE12O    PIC X(3).                                  00002770
             03 FILLER       PIC X(2).                                  00002780
             03 MLIGNE20A    PIC X.                                     00002790
             03 MLIGNE20C    PIC X.                                     00002800
             03 MLIGNE20P    PIC X.                                     00002810
             03 MLIGNE20H    PIC X.                                     00002820
             03 MLIGNE20V    PIC X.                                     00002830
             03 MLIGNE20O    PIC X(11).                                 00002840
             03 FILLER       PIC X(2).                                  00002850
             03 MVALMANOA    PIC X.                                     00002860
             03 MVALMANOC    PIC X.                                     00002870
             03 MVALMANOP    PIC X.                                     00002880
             03 MVALMANOH    PIC X.                                     00002890
             03 MVALMANOV    PIC X.                                     00002900
             03 MVALMANOO    PIC X.                                     00002910
             03 FILLER       PIC X(2).                                  00002920
             03 MLIGNE13A    PIC X.                                     00002930
             03 MLIGNE13C    PIC X.                                     00002940
             03 MLIGNE13P    PIC X.                                     00002950
             03 MLIGNE13H    PIC X.                                     00002960
             03 MLIGNE13V    PIC X.                                     00002970
             03 MLIGNE13O    PIC X(25).                                 00002980
             03 FILLER       PIC X(2).                                  00002990
             03 MLIGNE14A    PIC X.                                     00003000
             03 MLIGNE14C    PIC X.                                     00003010
             03 MLIGNE14P    PIC X.                                     00003020
             03 MLIGNE14H    PIC X.                                     00003030
             03 MLIGNE14V    PIC X.                                     00003040
             03 MLIGNE14O    PIC X(6).                                  00003050
             03 FILLER       PIC X(2).                                  00003060
             03 MNSOCA  PIC X.                                          00003070
             03 MNSOCC  PIC X.                                          00003080
             03 MNSOCP  PIC X.                                          00003090
             03 MNSOCH  PIC X.                                          00003100
             03 MNSOCV  PIC X.                                          00003110
             03 MNSOCO  PIC X(3).                                       00003120
             03 DFHMS1 OCCURS   3 TIMES .                               00003130
               04 FILLER     PIC X(2).                                  00003140
               04 MNDEPA     PIC X.                                     00003150
               04 MNDEPC     PIC X.                                     00003160
               04 MNDEPP     PIC X.                                     00003170
               04 MNDEPH     PIC X.                                     00003180
               04 MNDEPV     PIC X.                                     00003190
               04 MNDEPO     PIC X(3).                                  00003200
             03 FILLER       PIC X(2).                                  00003210
             03 MQLIB1A      PIC X.                                     00003220
             03 MQLIB1C PIC X.                                          00003230
             03 MQLIB1P PIC X.                                          00003240
             03 MQLIB1H PIC X.                                          00003250
             03 MQLIB1V PIC X.                                          00003260
             03 MQLIB1O      PIC X(12).                                 00003270
             03 FILLER       PIC X(2).                                  00003280
             03 MQLIB2A      PIC X.                                     00003290
             03 MQLIB2C PIC X.                                          00003300
             03 MQLIB2P PIC X.                                          00003310
             03 MQLIB2H PIC X.                                          00003320
             03 MQLIB2V PIC X.                                          00003330
             03 MQLIB2O      PIC X(12).                                 00003340
             03 MTAB1O OCCURS   12 TIMES .                              00003350
               04 FILLER     PIC X(2).                                  00003360
               04 MNCODICA   PIC X.                                     00003370
               04 MNCODICC   PIC X.                                     00003380
               04 MNCODICP   PIC X.                                     00003390
               04 MNCODICH   PIC X.                                     00003400
               04 MNCODICV   PIC X.                                     00003410
               04 MNCODICO   PIC X(7).                                  00003420
               04 FILLER     PIC X(2).                                  00003430
               04 MLREFFOURNA     PIC X.                                00003440
               04 MLREFFOURNC     PIC X.                                00003450
               04 MLREFFOURNP     PIC X.                                00003460
               04 MLREFFOURNH     PIC X.                                00003470
               04 MLREFFOURNV     PIC X.                                00003480
               04 MLREFFOURNO     PIC X(20).                            00003490
               04 FILLER     PIC X(2).                                  00003500
               04 MQTOTA     PIC X.                                     00003510
               04 MQTOTC     PIC X.                                     00003520
               04 MQTOTP     PIC X.                                     00003530
               04 MQTOTH     PIC X.                                     00003540
               04 MQTOTV     PIC X.                                     00003550
               04 MQTOTO     PIC X(6).                                  00003560
               04 FILLER     PIC X(2).                                  00003570
               04 MQRECSOLA  PIC X.                                     00003580
               04 MQRECSOLC  PIC X.                                     00003590
               04 MQRECSOLP  PIC X.                                     00003600
               04 MQRECSOLH  PIC X.                                     00003610
               04 MQRECSOLV  PIC X.                                     00003620
               04 MQRECSOLO  PIC X(6).                                  00003630
               04 DFHMS2 OCCURS   3 TIMES .                             00003640
                 05 FILLER   PIC X(2).                                  00003650
                 05 MQCDEA   PIC X.                                     00003660
                 05 MQCDEC   PIC X.                                     00003670
                 05 MQCDEP   PIC X.                                     00003680
                 05 MQCDEH   PIC X.                                     00003690
                 05 MQCDEV   PIC X.                                     00003700
                 05 MQCDEO   PIC X(5).                                  00003710
               04 DFHMS3 OCCURS   3 TIMES .                             00003720
                 05 FILLER   PIC X(2).                                  00003730
                 05 MQUOA    PIC X.                                     00003740
                 05 MQUOC    PIC X.                                     00003750
                 05 MQUOP    PIC X.                                     00003760
                 05 MQUOH    PIC X.                                     00003770
                 05 MQUOV    PIC X.                                     00003780
                 05 MQUOO    PIC X(5).                                  00003790
             03 FILLER       PIC X(2).                                  00003800
             03 MZONCMDA     PIC X.                                     00003810
             03 MZONCMDC     PIC X.                                     00003820
             03 MZONCMDP     PIC X.                                     00003830
             03 MZONCMDH     PIC X.                                     00003840
             03 MZONCMDV     PIC X.                                     00003850
             03 MZONCMDO     PIC X(15).                                 00003860
             03 FILLER       PIC X(2).                                  00003870
             03 MLIBERRA     PIC X.                                     00003880
             03 MLIBERRC     PIC X.                                     00003890
             03 MLIBERRP     PIC X.                                     00003900
             03 MLIBERRH     PIC X.                                     00003910
             03 MLIBERRV     PIC X.                                     00003920
             03 MLIBERRO     PIC X(58).                                 00003930
             03 FILLER       PIC X(2).                                  00003940
             03 MCODTRAA     PIC X.                                     00003950
             03 MCODTRAC     PIC X.                                     00003960
             03 MCODTRAP     PIC X.                                     00003970
             03 MCODTRAH     PIC X.                                     00003980
             03 MCODTRAV     PIC X.                                     00003990
             03 MCODTRAO     PIC X(4).                                  00004000
             03 FILLER       PIC X(2).                                  00004010
             03 MCICSA  PIC X.                                          00004020
             03 MCICSC  PIC X.                                          00004030
             03 MCICSP  PIC X.                                          00004040
             03 MCICSH  PIC X.                                          00004050
             03 MCICSV  PIC X.                                          00004060
             03 MCICSO  PIC X(5).                                       00004070
             03 FILLER       PIC X(2).                                  00004080
             03 MNETNAMA     PIC X.                                     00004090
             03 MNETNAMC     PIC X.                                     00004100
             03 MNETNAMP     PIC X.                                     00004110
             03 MNETNAMH     PIC X.                                     00004120
             03 MNETNAMV     PIC X.                                     00004130
             03 MNETNAMO     PIC X(8).                                  00004140
             03 FILLER       PIC X(2).                                  00004150
             03 MSCREENA     PIC X.                                     00004160
             03 MSCREENC     PIC X.                                     00004170
             03 MSCREENP     PIC X.                                     00004180
             03 MSCREENH     PIC X.                                     00004190
             03 MSCREENV     PIC X.                                     00004200
             03 MSCREENO     PIC X(4).                                  00004210
           02 MZONE2O .                                                 00004220
             03 MDATLIVD1O OCCURS   1 TIMES .                           00004230
               04 FILLER     PIC X(2).                                  00004240
               04 MDATLIV1A  PIC X.                                     00004250
               04 MDATLIV1C  PIC X.                                     00004260
               04 MDATLIV1P  PIC X.                                     00004270
               04 MDATLIV1H  PIC X.                                     00004280
               04 MDATLIV1V  PIC X.                                     00004290
               04 MDATLIV1O  PIC X(8).                                  00004300
             03 MDATLIVD2O OCCURS   1 TIMES .                           00004310
               04 FILLER     PIC X(2).                                  00004320
               04 MDATLIV2A  PIC X.                                     00004330
               04 MDATLIV2C  PIC X.                                     00004340
               04 MDATLIV2P  PIC X.                                     00004350
               04 MDATLIV2H  PIC X.                                     00004360
               04 MDATLIV2V  PIC X.                                     00004370
               04 MDATLIV2O  PIC X(8).                                  00004380
             03 MDATLIVD3O OCCURS   1 TIMES .                           00004390
               04 FILLER     PIC X(2).                                  00004400
               04 MDATLIV3A  PIC X.                                     00004410
               04 MDATLIV3C  PIC X.                                     00004420
               04 MDATLIV3P  PIC X.                                     00004430
               04 MDATLIV3H  PIC X.                                     00004440
               04 MDATLIV3V  PIC X.                                     00004450
               04 MDATLIV3O  PIC X(8).                                  00004460
                                                                                
