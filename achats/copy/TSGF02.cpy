      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : TS LIGNES DE COMMANDE PAR DEMANDEUR CODIC              *         
      *        CREE PAR TGF02                                         *         
      *****************************************************************         
      * DATE : 31/03/2011 EVOL D8635                                            
      * RAJOUT DU CODE CHEF PRODUIT                                             
      *****************************************************************         
      *                                                               *         
       01  TS-GF02.                                                             
      *--------------------------------  LONGUEUR TS                            
           02 TS-GF02-LONG                    PIC S9(5) COMP-3                  
      *                                       VALUE +239.                       
                                              VALUE +244.                       
           02 TS-GF02-DONNEES.                                                  
             03 TS-GF02-GF05.                                                   
      *--------------------------------  TABLE LIGNE COMMANDE                   
      *--------------------------------  DEMANDEUR                              
               04 TS-GF02-NSOCIETE            PIC X(3).                         
               04 TS-GF02-NLIEU               PIC X(3).                         
      *--------------------------------  ENTREPOT                               
               04 TS-GF02-NSOCLIVR            PIC X(3).                         
               04 TS-GF02-NDEPOT              PIC X(3).                         
      *--------------------------------  CODIC                                  
               04 TS-GF02-NCODIC              PIC X(7).                         
      *--------------------------------  QTE GLOBALE PAR                        
      *                                  DEMANDEUR CODIC                        
               04 TS-GF02-FRACTIONNEMENT OCCURS 03.                             
                  05 TS-GF02-QTE                 PIC 9(5) COMP-3.               
                  05 TS-GF02-DATE                PIC X(8).                      
             03 TS-GF02-GA00.                                                   
      *--------------------------------  TABLE ARTICLE                          
               04 TS-GF02-CFAM                PIC X(5).                         
               04 TS-GF02-CMARQ               PIC X(5).                         
               04 TS-GF02-LREFFOURN           PIC X(20).                        
               04 TS-GF02-CMODSTOCK           PIC X(5).                         
               04 TS-GF02-QCOLIRECEPT         PIC 9(5) COMP-3.                  
               04 TS-GF02-CHEFPROD            PIC X(5).                         
             03 TS-GF02-TOPMAJ                PIC X.                            
                                                                                
