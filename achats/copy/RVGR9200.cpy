      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGR9200                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGR9200                         
      **********************************************************                
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
       01  RVGR9200.                                                            
           02  GR92-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  GR92-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GR92-NRECQUAI                                                    
               PIC X(0007).                                                     
           02  GR92-NREC                                                        
               PIC X(0007).                                                     
           02  GR92-DJRECQUAI                                                   
               PIC X(0008).                                                     
           02  GR92-DSAISREC                                                    
               PIC X(0008).                                                     
           02  GR92-WTRAIT                                                      
               PIC X(0002).                                                     
           02  GR92-LTRAIT                                                      
               PIC X(0020).                                                     
           02  GR92-DTRAIT                                                      
               PIC X(0008).                                                     
           02  GR92-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGR9200                                  
      **********************************************************                
       01  RVGR9200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR92-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR92-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR92-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR92-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR92-NRECQUAI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR92-NRECQUAI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR92-NREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR92-NREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR92-DJRECQUAI-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR92-DJRECQUAI-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR92-DSAISREC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR92-DSAISREC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR92-WTRAIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR92-WTRAIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR92-LTRAIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR92-LTRAIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR92-DTRAIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR92-DTRAIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR92-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GR92-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.
                                                                                
      *}                                                                        
