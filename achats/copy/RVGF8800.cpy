      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGF8800                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF8800                         
      **********************************************************                
       01  RVGF8800.                                                            
           10 GF88-NCODIC               PIC X(7).                               
           10 GF88-NENTCDE              PIC X(5).                               
           10 GF88-NEAN                 PIC X(13).                              
           10 GF88-LREFFOURN            PIC X(20).                              
           10 GF88-WDISPO               PIC X(1).                               
           10 GF88-DPROCHDISPO          PIC X(8).                               
           10 GF88-DTRTFIC              PIC X(8).                               
           10 GF88-DSYST                PIC S9(13)V USAGE COMP-3.               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGF8800                                  
      **********************************************************                
       01  RVGF8800-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF88-NCODIC-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF88-NCODIC-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF88-NENTCDE-F            PIC S9(4) COMP.                         
      *--                                                                       
           10 GF88-NENTCDE-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF88-NEAN-F               PIC S9(4) COMP.                         
      *--                                                                       
           10 GF88-NEAN-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF88-LREFFOURN-F          PIC S9(4) COMP.                         
      *--                                                                       
           10 GF88-LREFFOURN-F          PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF88-WDISPO-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF88-WDISPO-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF88-DPROCHDISPO-F        PIC S9(4) COMP.                         
      *--                                                                       
           10 GF88-DPROCHDISPO-F        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF88-DTRTFIC-F            PIC S9(4) COMP.                         
      *--                                                                       
           10 GF88-DTRTFIC-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF88-DSYST-F              PIC S9(4) COMP.                         
      *                                                                         
      *--                                                                       
           10 GF88-DSYST-F              PIC S9(4) COMP-5.                       
                                                                                
      *}                                                                        
