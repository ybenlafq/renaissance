      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR37   EGR37                                              00000020
      ***************************************************************** 00000030
       01   EGR37I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENTITEL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MENTITEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENTITEF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MENTITEI  PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINTERL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MINTERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MINTERF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MINTERI   PIC X(5).                                       00000250
           02 M223I OCCURS   8 TIMES .                                  00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000270
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MSELI   PIC X.                                          00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMANDEL   COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MCOMMANDEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCOMMANDEF   PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MCOMMANDEI   PIC X(7).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDSAISIEL    COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MDSAISIEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDSAISIEF    PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MDSAISIEI    PIC X(8).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDVALIDL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MDVALIDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDVALIDF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MDVALIDI     PIC X(8).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCOMML      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MQCOMML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQCOMMF      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MQCOMMI      PIC X(5).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQRECUEL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MQRECUEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQRECUEF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MQRECUEI     PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSOLDEL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MQSOLDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQSOLDEF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MQSOLDEI     PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MZONCMDI  PIC X(15).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MLIBERRI  PIC X(58).                                      00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCODTRAI  PIC X(4).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MCICSI    PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MNETNAMI  PIC X(8).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MSCREENI  PIC X(4).                                       00000780
      ***************************************************************** 00000790
      * SDF: EGR37   EGR37                                              00000800
      ***************************************************************** 00000810
       01   EGR37O REDEFINES EGR37I.                                    00000820
           02 FILLER    PIC X(12).                                      00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MDATJOUA  PIC X.                                          00000850
           02 MDATJOUC  PIC X.                                          00000860
           02 MDATJOUP  PIC X.                                          00000870
           02 MDATJOUH  PIC X.                                          00000880
           02 MDATJOUV  PIC X.                                          00000890
           02 MDATJOUO  PIC X(10).                                      00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MTIMJOUA  PIC X.                                          00000920
           02 MTIMJOUC  PIC X.                                          00000930
           02 MTIMJOUP  PIC X.                                          00000940
           02 MTIMJOUH  PIC X.                                          00000950
           02 MTIMJOUV  PIC X.                                          00000960
           02 MTIMJOUO  PIC X(5).                                       00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MPAGEA    PIC X.                                          00000990
           02 MPAGEC    PIC X.                                          00001000
           02 MPAGEP    PIC X.                                          00001010
           02 MPAGEH    PIC X.                                          00001020
           02 MPAGEV    PIC X.                                          00001030
           02 MPAGEO    PIC X(3).                                       00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MENTITEA  PIC X.                                          00001060
           02 MENTITEC  PIC X.                                          00001070
           02 MENTITEP  PIC X.                                          00001080
           02 MENTITEH  PIC X.                                          00001090
           02 MENTITEV  PIC X.                                          00001100
           02 MENTITEO  PIC X(5).                                       00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MINTERA   PIC X.                                          00001130
           02 MINTERC   PIC X.                                          00001140
           02 MINTERP   PIC X.                                          00001150
           02 MINTERH   PIC X.                                          00001160
           02 MINTERV   PIC X.                                          00001170
           02 MINTERO   PIC X(5).                                       00001180
           02 M223O OCCURS   8 TIMES .                                  00001190
             03 FILLER       PIC X(2).                                  00001200
             03 MSELA   PIC X.                                          00001210
             03 MSELC   PIC X.                                          00001220
             03 MSELP   PIC X.                                          00001230
             03 MSELH   PIC X.                                          00001240
             03 MSELV   PIC X.                                          00001250
             03 MSELO   PIC X.                                          00001260
             03 FILLER       PIC X(2).                                  00001270
             03 MCOMMANDEA   PIC X.                                     00001280
             03 MCOMMANDEC   PIC X.                                     00001290
             03 MCOMMANDEP   PIC X.                                     00001300
             03 MCOMMANDEH   PIC X.                                     00001310
             03 MCOMMANDEV   PIC X.                                     00001320
             03 MCOMMANDEO   PIC X(7).                                  00001330
             03 FILLER       PIC X(2).                                  00001340
             03 MDSAISIEA    PIC X.                                     00001350
             03 MDSAISIEC    PIC X.                                     00001360
             03 MDSAISIEP    PIC X.                                     00001370
             03 MDSAISIEH    PIC X.                                     00001380
             03 MDSAISIEV    PIC X.                                     00001390
             03 MDSAISIEO    PIC X(8).                                  00001400
             03 FILLER       PIC X(2).                                  00001410
             03 MDVALIDA     PIC X.                                     00001420
             03 MDVALIDC     PIC X.                                     00001430
             03 MDVALIDP     PIC X.                                     00001440
             03 MDVALIDH     PIC X.                                     00001450
             03 MDVALIDV     PIC X.                                     00001460
             03 MDVALIDO     PIC X(8).                                  00001470
             03 FILLER       PIC X(2).                                  00001480
             03 MQCOMMA      PIC X.                                     00001490
             03 MQCOMMC PIC X.                                          00001500
             03 MQCOMMP PIC X.                                          00001510
             03 MQCOMMH PIC X.                                          00001520
             03 MQCOMMV PIC X.                                          00001530
             03 MQCOMMO      PIC X(5).                                  00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MQRECUEA     PIC X.                                     00001560
             03 MQRECUEC     PIC X.                                     00001570
             03 MQRECUEP     PIC X.                                     00001580
             03 MQRECUEH     PIC X.                                     00001590
             03 MQRECUEV     PIC X.                                     00001600
             03 MQRECUEO     PIC X(5).                                  00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MQSOLDEA     PIC X.                                     00001630
             03 MQSOLDEC     PIC X.                                     00001640
             03 MQSOLDEP     PIC X.                                     00001650
             03 MQSOLDEH     PIC X.                                     00001660
             03 MQSOLDEV     PIC X.                                     00001670
             03 MQSOLDEO     PIC X(5).                                  00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MZONCMDA  PIC X.                                          00001700
           02 MZONCMDC  PIC X.                                          00001710
           02 MZONCMDP  PIC X.                                          00001720
           02 MZONCMDH  PIC X.                                          00001730
           02 MZONCMDV  PIC X.                                          00001740
           02 MZONCMDO  PIC X(15).                                      00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MLIBERRA  PIC X.                                          00001770
           02 MLIBERRC  PIC X.                                          00001780
           02 MLIBERRP  PIC X.                                          00001790
           02 MLIBERRH  PIC X.                                          00001800
           02 MLIBERRV  PIC X.                                          00001810
           02 MLIBERRO  PIC X(58).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCODTRAA  PIC X.                                          00001840
           02 MCODTRAC  PIC X.                                          00001850
           02 MCODTRAP  PIC X.                                          00001860
           02 MCODTRAH  PIC X.                                          00001870
           02 MCODTRAV  PIC X.                                          00001880
           02 MCODTRAO  PIC X(4).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCICSA    PIC X.                                          00001910
           02 MCICSC    PIC X.                                          00001920
           02 MCICSP    PIC X.                                          00001930
           02 MCICSH    PIC X.                                          00001940
           02 MCICSV    PIC X.                                          00001950
           02 MCICSO    PIC X(5).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNETNAMA  PIC X.                                          00001980
           02 MNETNAMC  PIC X.                                          00001990
           02 MNETNAMP  PIC X.                                          00002000
           02 MNETNAMH  PIC X.                                          00002010
           02 MNETNAMV  PIC X.                                          00002020
           02 MNETNAMO  PIC X(8).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MSCREENA  PIC X.                                          00002050
           02 MSCREENC  PIC X.                                          00002060
           02 MSCREENP  PIC X.                                          00002070
           02 MSCREENH  PIC X.                                          00002080
           02 MSCREENV  PIC X.                                          00002090
           02 MSCREENO  PIC X(4).                                       00002100
                                                                                
