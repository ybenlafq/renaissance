      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ                                                       
      *****************************************************************         
      *                                                                         
       01 WS-MQ10.                                                              
          02 WS-QUEUE.                                                          
             10 MQ10-CORRELID              PIC X(24).                           
          02 WS-CODRET                     PIC X(02).                           
          02 WS-MESSAGE.                                                        
             05 MESS-ENTETE.                                                    
                10 MES-TYPE                PIC X(03).                           
                10 MES-NSOCMSG             PIC X(03).                           
                10 MES-NLIEUMSG            PIC X(03).                           
                10 MES-NSOCDST             PIC X(03).                           
                10 MES-NLIEUDST            PIC X(03).                           
                10 MES-NORD                PIC 9(08).                           
                10 MES-LPROG               PIC X(10).                           
                10 MES-DJOUR               PIC X(08).                           
                10 MES-WSID                PIC X(10).                           
                10 MES-USER                PIC X(10).                           
                10 MES-CHRONO              PIC 9(07).                           
                10 MES-NBRMSG              PIC 9(07).                           
                10 MES-NBRENR              PIC 9(05).                           
                10 MES-TAILLE              PIC 9(05).                           
                10 MES-FILLER              PIC X(20).                           
      * MES-ENTETE .................................                            
             05 MES-ENTETE.                                                     
                10 MES-RETOUR1             PIC X(01).                           
                10 MES-TAG1                PIC X(03).                           
                10 MES-NCDEK               PIC X(11).                           
                10 MES-NCDE                PIC X(07).                           
                10 MES-DSAISIE             PIC X(08).                           
                10 MES-CSOC                PIC X(03).                           
                10 MES-RETOUR2             PIC X(01).                           
      * MES-DETAIL .................................                            
             05 MES-DETAIL           OCCURS 99.                                 
                10 MES-TAG2                PIC X(03).                           
                10 MES-NCODIC              PIC X(07).                           
                10 MES-QCDE                PIC 9(07).                           
                10 MES-RETOUR3             PIC X(01).                           
                                                                                
