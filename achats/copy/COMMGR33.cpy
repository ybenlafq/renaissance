      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TGR33 (TGR00 -> MENU)    TR: GR00  *    00002222
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3710  00009000
      *                                                                 00410100
      *        TRANSACTION GR33 : CREATION RECEPTION FOURNISSEUR   *    00411022
      *                                                                 00412000
          02 COMM-GR33-APPLI REDEFINES COMM-GR00-APPLI.                 00420022
             03 COMM-GR33-DONNEES-TGR33.                                00520224
                04 COMM-GR33-NRECEPTION     PIC X(07).                  00640023
                04 COMM-GR33-DSAISIE        PIC X(08).                  00642026
                04 COMM-GR33-DRECEPTION     PIC X(08).                  00642026
                04 COMM-GR33-ENTITE         PIC X(05).                  00642026
                04 COMM-GR33-INTER          PIC X(05).                  00642026
                04 COMM-GR33-NSOCIETE       PIC XXX.                    00560026
                04 COMM-GR33-LSOCIETE       PIC X(19).                  00580026
                04 COMM-GR33-NDEPOT         PIC XXX.                    00600026
                04 COMM-GR33-CLIVR          PIC X(05).                  00646026
                04 COMM-GR33-MODE           PIC X(05).                  00646026
                04 COMM-GR33-LCOMMENT       PIC X(20).                  00644026
                04 COMM-GR33-ITEM           PIC 9(03).                  00644026
                04 COMM-GR33-NBRCOMM        PIC 9(03).                  00644026
                04 COMM-GR33-POS-MAX        PIC 9(03).                  00644026
                04 COMM-GR33-ETAT-PASSAGE   PIC X(01).                  00644026
                   88 COMM-GR33-1ER-PASSAGE     VALUE '1'.              00644026
                   88 COMM-GR33-1ER-MESSAGE     VALUE '2'.              00644026
                   88 COMM-GR33-XEME-MESSAGE    VALUE '3'.              00644026
             03 COMM-GR33-LIBRE             PIC X(2041).                00740029
      ***************************************************************** 00750000
                                                                                
