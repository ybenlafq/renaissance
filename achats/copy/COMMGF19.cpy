      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGF19                     TR: GF19      00020000
      *           TR: GF00 "ECHEANCEMENT LIVRAISONS"                    00030000
      *************************************************SOIT 7421C.**            
          02 COMM-GF19-APPLI REDEFINES COMM-GF00-APPLI.                 00050000
      *------------------------------ PARTIE COMMUNE 7421C- 625C.               
             03 COMM-GF19-PAGE                     PIC S9(003).         00070000
             03 COMM-GF19-PAGE-MAX                 PIC S9(003).         00080000
             03 COMM-GF19-COLS                     PIC S9(03).          00090000
             03 COMM-GF19-IND-Q                    PIC S9(03).          00100001
             03 COMM-GF19-IND-MAX                  PIC S9(03).          00110001
      *------                                                           00120000
             03 COMM-GF19-RANG-TS-HV.                                   00130000
               04 COMM-GF19-RANG-TS PIC S9(4).                          00140000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GF19-I-RELIQUAT               PIC S9(4) COMP.      00150000
      *--                                                                       
             03 COMM-GF19-I-RELIQUAT               PIC S9(4) COMP-5.            
      *}                                                                        
             03 COMM-GF19-1ERE-LIGNE               PIC S9(05).          00160000
             03 COMM-GF19-LORIGDATE                PIC X(19).                   
      *------LL = 700C.                                                 00170000
             03 COMM-GF19-DLIVRAISONS.                                  00180000
      *------                                                           00190000
                04 COMM-GF19-POSTE OCCURS 20 TIMES.                     00200000
      *------                                                           00210000
                  05 COMM-GF19-DLIVRAISON.                              00220000
                     06 SS                         PIC X(02).           00230000
                     06 AA                         PIC X(02).           00240000
                     06 MM                         PIC X(02).           00250000
                     06 JJ                         PIC X(02).           00260000
                  05 COMM-GF19-SSAAR  REDEFINES   COMM-GF19-DLIVRAISON. 00270000
                     06 COMM-GF19-SSAA             PIC X(04).           00280000
                     06 COMM-GF19-MMAA             PIC X(02).           00290000
                     06 COMM-GF19-JJAA             PIC X(02).           00300000
      *------                                                           00310000
                  05 COMM-GF19-ANNEE-SEMAINE.                           00320000
                    06 COMM-GF19-SSANNEE          PIC X(04).            00330000
                    06 COMM-GF19-DSEMAINE         PIC X(02).            00340000
                    06 COMM-GF19-JOUR             PIC X(01).            00350000
      *                                                                 00360001
             03 COMM-GF19-QUOTA.                                        00370001
                04 COMM-GF19-POSTE-QUOTA OCCURS 20 TIMES.               00380001
                   05 COMM-GF19-CQUOTA PIC X(5).                        00390001
      *      03 COMM-GF19-FILLER         PIC X(1981).                           
      *                                                                 00410000
      ***************************************************************** 00420000
                                                                                
