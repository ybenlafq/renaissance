      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00001000
      *     COMMAREA LISTE DES CODES RAISON                             00002000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00003000
      * ***************************************** LONG = 114            00004005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01   COMM-GF55-LONG-COMMAREA    PIC S9(4) COMP VALUE +8192.     00004112
      *--                                                                       
        01   COMM-GF55-LONG-COMMAREA    PIC S9(4) COMP-5 VALUE +8192.           
      *}                                                                        
        01   Z-COMMAREA.                                                00005011
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00006011
          02 FILLER-COM-AIDA            PIC X(100).                     00006111
      *                                                                 00006211
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00006311
          02    COMM-CICS-APPLID        PIC X(8).                       00006411
          02    COMM-CICS-NETNAM        PIC X(8).                       00006511
          02    COMM-CICS-TRANSA        PIC X(4).                       00006611
      *                                                                 00006711
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00006811
          02    COMM-DATE-SIECLE        PIC XX.                         00006911
          02    COMM-DATE-ANNEE         PIC XX.                         00007011
          02    COMM-DATE-MOIS          PIC XX.                         00007111
          02    COMM-DATE-JOUR          PIC XX.                         00007211
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00007311
          02    COMM-DATE-QNTA          PIC 999.                        00007411
          02    COMM-DATE-QNT0          PIC 99999.                      00007511
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00007611
          02    COMM-DATE-BISX            PIC 9.                        00007711
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                           00007811
          02    COMM-DATE-JSM             PIC 9.                        00007911
      *   LIBELLES DU JOUR COURT - LONG                                 00008011
          02    COMM-DATE-JSM-LC          PIC XXX.                      00008111
          02    COMM-DATE-JSM-LL          PIC XXXXXXXX.                 00008211
      *   LIBELLES DU MOIS COURT - LONG                                 00008311
          02    COMM-DATE-MOIS-LC         PIC XXX.                      00008411
          02    COMM-DATE-MOIS-LL         PIC XXXXXXXX.                 00008511
      *   DIFFERENTES FORMES DE DATE                                    00008611
          02    COMM-DATE-SSAAMMJJ        PIC X(8).                     00008711
          02    COMM-DATE-AAMMJJ          PIC X(6).                     00008811
          02    COMM-DATE-JJMMSSAA        PIC X(8).                     00008911
          02    COMM-DATE-JJMMAA          PIC X(6).                     00009011
          02    COMM-DATE-JJ-MM-AA        PIC X(8).                     00009111
          02    COMM-DATE-JJ-MM-SSAA      PIC X(10).                    00009211
      *   DIFFERENTES FORMES DE DATE                                    00009311
          02    COMM-DATE-SEMSS           PIC X(02).                    00009411
          02    COMM-DATE-SEMAA           PIC X(02).                    00009511
          02    COMM-DATE-SEMNU           PIC X(02).                    00009611
          02    COMM-DATE-FILLER          PIC X(08).                    00009711
      *                                                                 00009811
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00009911
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02    COMM-SWAP-CURS          PIC S9(4) COMP VALUE -1.        00010011
      *--                                                                       
          02    COMM-SWAP-CURS          PIC S9(4) COMP-5 VALUE -1.              
      *}                                                                        
          02    COMM-SWAP-ATTR OCCURS 150       PIC X(1).               00010111
                                                                        00010211
          02    COMM-GF55-GF00-GL00-FILLER.                             00010317
             03 FILLER                      PIC X(7504).                00010416
                COPY WORKGF55.                                          00010514
      * FILLER A NE PAS TOUCHER                                         00060612
             03 FILLER                      PIC X(16).                  00060718
                                                                        00061000
      *                                                                 00070000
                                                                                
