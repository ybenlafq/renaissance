      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGR0000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGR0000                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGR0001.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGR0001.                                                            
      *}                                                                        
           02  GR00-NRECQUAI                                                    
               PIC X(0007).                                                     
           02  GR00-NREC                                                        
               PIC X(0007).                                                     
           02  GR00-DJRECQUAI                                                   
               PIC X(0008).                                                     
           02  GR00-DHRECQUAI                                                   
               PIC X(0002).                                                     
           02  GR00-DMRECQUAI                                                   
               PIC X(0002).                                                     
           02  GR00-DSAISREC                                                    
               PIC X(0008).                                                     
           02  GR00-LCOMMENT                                                    
               PIC X(0020).                                                     
           02  GR00-CLIVR                                                       
               PIC X(0005).                                                     
           02  GR00-CMODETRANS                                                  
               PIC X(0005).                                                     
           02  GR00-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  GR00-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GR00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GR00-CRTRQ                                                       
               PIC X(0005).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGR0000                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGR0000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGR0000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR00-NRECQUAI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR00-NRECQUAI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR00-NREC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR00-NREC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR00-DJRECQUAI-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR00-DJRECQUAI-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR00-DHRECQUAI-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR00-DHRECQUAI-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR00-DMRECQUAI-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR00-DMRECQUAI-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR00-DSAISREC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR00-DSAISREC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR00-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR00-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR00-CLIVR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR00-CLIVR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR00-CMODETRANS-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR00-CMODETRANS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR00-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR00-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR00-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR00-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GR00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GR00-CRTRQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GR00-CRTRQ-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
