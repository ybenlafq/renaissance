      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGR03 (TGR00 -> MENU)    TR: GR00  *    00020000
      *               GESTION DES RECEPTIONS                       *    00030000
      *                                                                 00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3596  00050001
      *                                                                 00060000
      *        TRANSACTION GR03 :                                       00070000
      *                                                                 00080000
             03 COMM-GR03-APPLI REDEFINES COMM-GR01-APPLI.              00090001
      *------------------------------ ZONE DONNEES TGR03                00100000
                04 COMM-GR03-DONNEES-TGR03.                             00110001
      *------------------------------ DONNEES JOUR                      00120001
                   05 COMM-GR03-JOUR              PIC X(8).             00130001
                   05 COMM-GR03-MJOUR             PIC X(1).             00140001
                   05 COMM-GR03-NJOUR             PIC X(1).             00150001
                   05 COMM-GR03-NSEM              PIC X(2).             00160001
                   05 COMM-GR03-NANNEE            PIC X(4).             00170001
      *------------------------------ DONNEES SEMAINE                   00180001
                   05 COMM-GR03-SEM               PIC X(6).             00190001
                   05 COMM-GR03-MSEM              PIC X(1).             00200001
      *------------------------------                                   00210001
                   05 COMM-GR03-PAGE              PIC 99.               00220001
                   05 COMM-GR03-PAGETOT           PIC 99.               00230001
                   05 COMM-GR03-CUMUL.                                  00240001
                      06 COMM-GR03-TOT            PIC S9(8) COMP-3.     00250001
                      06 COMM-GR03-DISP           PIC S9(8) COMP-3.     00260001
                      06 COMM-GR03-PRIS           PIC S9(8) COMP-3.     00270001
                      06 COMM-GR03-REC            PIC S9(8) COMP-3.     00280001
                      06 COMM-GR03-PPRIS          PIC S9(8) COMP-3.     00290001
                      06 COMM-GR03-PREC           PIC S9(8) COMP-3.     00300001
      *         04 COMM-GR03-LIBRE          PIC X(3539).                00310001
      ***************************************************************** 00320000
                                                                                
