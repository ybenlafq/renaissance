      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR10   EGR10                                              00000020
      ***************************************************************** 00000030
       01   EGR10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * PAGE                                                            00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MPAGEI    PIC X(3).                                       00000200
      * NUMERO SOCIETE                                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
      * LIBELLE SOCIETE                                                 00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MLSOCI    PIC X(20).                                      00000300
      * TYPE DE RECEPTION                                               00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRECL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MCRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCRECF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MCRECI    PIC X(5).                                       00000350
      * LIBELLE DU TYPE                                                 00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRECL    COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MLRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLRECF    PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MLRECI    PIC X(20).                                      00000400
      * NUM. ENTREPOT                                                   00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENPOTL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNENPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENPOTF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNENPOTI  PIC X(3).                                       00000450
      * LIB. ENTREPOT                                                   00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENPOTL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MLENPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLENPOTF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MLENPOTI  PIC X(20).                                      00000500
      * NUM. SEMAINE                                                    00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSEMAIL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MNSEMAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSEMAIF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MNSEMAII  PIC X(6).                                       00000550
      * TYPE DE QUOTA                                                   00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCQUOTAL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MCQUOTAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCQUOTAF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MCQUOTAI  PIC X(5).                                       00000600
      * LIBELLE DU TYPE QUOTA                                           00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLQUOTAL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLQUOTAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLQUOTAF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLQUOTAI  PIC X(20).                                      00000650
           02 M456I OCCURS   11 TIMES .                                 00000660
      * NUM. SEMAINE                                                    00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSEML  COMP PIC S9(4).                                 00000680
      *--                                                                       
             03 MNSEML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSEMF  PIC X.                                          00000690
             03 FILLER  PIC X(4).                                       00000700
             03 MNSEMI  PIC X(6).                                       00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDEBL  COMP PIC S9(4).                                 00000720
      *--                                                                       
             03 MDDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDDEBF  PIC X.                                          00000730
             03 FILLER  PIC X(4).                                       00000740
             03 MDDEBI  PIC X(5).                                       00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFINL  COMP PIC S9(4).                                 00000760
      *--                                                                       
             03 MDFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDFINF  PIC X.                                          00000770
             03 FILLER  PIC X(4).                                       00000780
             03 MDFINI  PIC X(5).                                       00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQQUOTAL     COMP PIC S9(4).                            00000800
      *--                                                                       
             03 MQQUOTAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQQUOTAF     PIC X.                                     00000810
             03 FILLER  PIC X(4).                                       00000820
             03 MQQUOTAI     PIC X(5).                                  00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPRISL      COMP PIC S9(4).                            00000840
      *--                                                                       
             03 MQPRISL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPRISF      PIC X.                                     00000850
             03 FILLER  PIC X(4).                                       00000860
             03 MQPRISI      PIC X(7).                                  00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLUNDIL     COMP PIC S9(4).                            00000880
      *--                                                                       
             03 MQLUNDIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQLUNDIF     PIC X.                                     00000890
             03 FILLER  PIC X(4).                                       00000900
             03 MQLUNDII     PIC X(5).                                  00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQMARDIL     COMP PIC S9(4).                            00000920
      *--                                                                       
             03 MQMARDIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQMARDIF     PIC X.                                     00000930
             03 FILLER  PIC X(4).                                       00000940
             03 MQMARDII     PIC X(5).                                  00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQMERCRL     COMP PIC S9(4).                            00000960
      *--                                                                       
             03 MQMERCRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQMERCRF     PIC X.                                     00000970
             03 FILLER  PIC X(4).                                       00000980
             03 MQMERCRI     PIC X(5).                                  00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQJEUDIL     COMP PIC S9(4).                            00001000
      *--                                                                       
             03 MQJEUDIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQJEUDIF     PIC X.                                     00001010
             03 FILLER  PIC X(4).                                       00001020
             03 MQJEUDII     PIC X(5).                                  00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQVENDRL     COMP PIC S9(4).                            00001040
      *--                                                                       
             03 MQVENDRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQVENDRF     PIC X.                                     00001050
             03 FILLER  PIC X(4).                                       00001060
             03 MQVENDRI     PIC X(5).                                  00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSAMEDL     COMP PIC S9(4).                            00001080
      *--                                                                       
             03 MQSAMEDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQSAMEDF     PIC X.                                     00001090
             03 FILLER  PIC X(4).                                       00001100
             03 MQSAMEDI     PIC X(5).                                  00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQDIMANL     COMP PIC S9(4).                            00001120
      *--                                                                       
             03 MQDIMANL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQDIMANF     PIC X.                                     00001130
             03 FILLER  PIC X(4).                                       00001140
             03 MQDIMANI     PIC X(5).                                  00001150
      * ZONE CMD AIDA                                                   00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001170
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001180
           02 FILLER    PIC X(4).                                       00001190
           02 MZONCMDI  PIC X(15).                                      00001200
      * MESSAGE ERREUR                                                  00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MLIBERRI  PIC X(58).                                      00001250
      * CODE TRANSACTION                                                00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MCODTRAI  PIC X(4).                                       00001300
      * CICS DE TRAVAIL                                                 00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001320
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001330
           02 FILLER    PIC X(4).                                       00001340
           02 MCICSI    PIC X(5).                                       00001350
      * NETNAME                                                         00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001370
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001380
           02 FILLER    PIC X(4).                                       00001390
           02 MNETNAMI  PIC X(8).                                       00001400
      * CODE TERMINAL                                                   00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MSCREENI  PIC X(5).                                       00001450
      ***************************************************************** 00001460
      * SDF: EGR10   EGR10                                              00001470
      ***************************************************************** 00001480
       01   EGR10O REDEFINES EGR10I.                                    00001490
           02 FILLER    PIC X(12).                                      00001500
      * DATE DU JOUR                                                    00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MDATJOUA  PIC X.                                          00001530
           02 MDATJOUC  PIC X.                                          00001540
           02 MDATJOUP  PIC X.                                          00001550
           02 MDATJOUH  PIC X.                                          00001560
           02 MDATJOUV  PIC X.                                          00001570
           02 MDATJOUO  PIC X(10).                                      00001580
      * HEURE                                                           00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MTIMJOUA  PIC X.                                          00001610
           02 MTIMJOUC  PIC X.                                          00001620
           02 MTIMJOUP  PIC X.                                          00001630
           02 MTIMJOUH  PIC X.                                          00001640
           02 MTIMJOUV  PIC X.                                          00001650
           02 MTIMJOUO  PIC X(5).                                       00001660
      * PAGE                                                            00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MPAGEA    PIC X.                                          00001690
           02 MPAGEC    PIC X.                                          00001700
           02 MPAGEP    PIC X.                                          00001710
           02 MPAGEH    PIC X.                                          00001720
           02 MPAGEV    PIC X.                                          00001730
           02 MPAGEO    PIC X(3).                                       00001740
      * NUMERO SOCIETE                                                  00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MNSOCA    PIC X.                                          00001770
           02 MNSOCC    PIC X.                                          00001780
           02 MNSOCP    PIC X.                                          00001790
           02 MNSOCH    PIC X.                                          00001800
           02 MNSOCV    PIC X.                                          00001810
           02 MNSOCO    PIC X(3).                                       00001820
      * LIBELLE SOCIETE                                                 00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MLSOCA    PIC X.                                          00001850
           02 MLSOCC    PIC X.                                          00001860
           02 MLSOCP    PIC X.                                          00001870
           02 MLSOCH    PIC X.                                          00001880
           02 MLSOCV    PIC X.                                          00001890
           02 MLSOCO    PIC X(20).                                      00001900
      * TYPE DE RECEPTION                                               00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MCRECA    PIC X.                                          00001930
           02 MCRECC    PIC X.                                          00001940
           02 MCRECP    PIC X.                                          00001950
           02 MCRECH    PIC X.                                          00001960
           02 MCRECV    PIC X.                                          00001970
           02 MCRECO    PIC X(5).                                       00001980
      * LIBELLE DU TYPE                                                 00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MLRECA    PIC X.                                          00002010
           02 MLRECC    PIC X.                                          00002020
           02 MLRECP    PIC X.                                          00002030
           02 MLRECH    PIC X.                                          00002040
           02 MLRECV    PIC X.                                          00002050
           02 MLRECO    PIC X(20).                                      00002060
      * NUM. ENTREPOT                                                   00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MNENPOTA  PIC X.                                          00002090
           02 MNENPOTC  PIC X.                                          00002100
           02 MNENPOTP  PIC X.                                          00002110
           02 MNENPOTH  PIC X.                                          00002120
           02 MNENPOTV  PIC X.                                          00002130
           02 MNENPOTO  PIC X(3).                                       00002140
      * LIB. ENTREPOT                                                   00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MLENPOTA  PIC X.                                          00002170
           02 MLENPOTC  PIC X.                                          00002180
           02 MLENPOTP  PIC X.                                          00002190
           02 MLENPOTH  PIC X.                                          00002200
           02 MLENPOTV  PIC X.                                          00002210
           02 MLENPOTO  PIC X(20).                                      00002220
      * NUM. SEMAINE                                                    00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MNSEMAIA  PIC X.                                          00002250
           02 MNSEMAIC  PIC X.                                          00002260
           02 MNSEMAIP  PIC X.                                          00002270
           02 MNSEMAIH  PIC X.                                          00002280
           02 MNSEMAIV  PIC X.                                          00002290
           02 MNSEMAIO  PIC X(6).                                       00002300
      * TYPE DE QUOTA                                                   00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MCQUOTAA  PIC X.                                          00002330
           02 MCQUOTAC  PIC X.                                          00002340
           02 MCQUOTAP  PIC X.                                          00002350
           02 MCQUOTAH  PIC X.                                          00002360
           02 MCQUOTAV  PIC X.                                          00002370
           02 MCQUOTAO  PIC X(5).                                       00002380
      * LIBELLE DU TYPE QUOTA                                           00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MLQUOTAA  PIC X.                                          00002410
           02 MLQUOTAC  PIC X.                                          00002420
           02 MLQUOTAP  PIC X.                                          00002430
           02 MLQUOTAH  PIC X.                                          00002440
           02 MLQUOTAV  PIC X.                                          00002450
           02 MLQUOTAO  PIC X(20).                                      00002460
           02 M456O OCCURS   11 TIMES .                                 00002470
      * NUM. SEMAINE                                                    00002480
             03 FILLER       PIC X(2).                                  00002490
             03 MNSEMA  PIC X.                                          00002500
             03 MNSEMC  PIC X.                                          00002510
             03 MNSEMP  PIC X.                                          00002520
             03 MNSEMH  PIC X.                                          00002530
             03 MNSEMV  PIC X.                                          00002540
             03 MNSEMO  PIC X(6).                                       00002550
             03 FILLER       PIC X(2).                                  00002560
             03 MDDEBA  PIC X.                                          00002570
             03 MDDEBC  PIC X.                                          00002580
             03 MDDEBP  PIC X.                                          00002590
             03 MDDEBH  PIC X.                                          00002600
             03 MDDEBV  PIC X.                                          00002610
             03 MDDEBO  PIC X(5).                                       00002620
             03 FILLER       PIC X(2).                                  00002630
             03 MDFINA  PIC X.                                          00002640
             03 MDFINC  PIC X.                                          00002650
             03 MDFINP  PIC X.                                          00002660
             03 MDFINH  PIC X.                                          00002670
             03 MDFINV  PIC X.                                          00002680
             03 MDFINO  PIC X(5).                                       00002690
             03 FILLER       PIC X(2).                                  00002700
             03 MQQUOTAA     PIC X.                                     00002710
             03 MQQUOTAC     PIC X.                                     00002720
             03 MQQUOTAP     PIC X.                                     00002730
             03 MQQUOTAH     PIC X.                                     00002740
             03 MQQUOTAV     PIC X.                                     00002750
             03 MQQUOTAO     PIC Z(5).                                  00002760
             03 FILLER       PIC X(2).                                  00002770
             03 MQPRISA      PIC X.                                     00002780
             03 MQPRISC PIC X.                                          00002790
             03 MQPRISP PIC X.                                          00002800
             03 MQPRISH PIC X.                                          00002810
             03 MQPRISV PIC X.                                          00002820
             03 MQPRISO      PIC Z(4)9,9.                               00002830
             03 FILLER       PIC X(2).                                  00002840
             03 MQLUNDIA     PIC X.                                     00002850
             03 MQLUNDIC     PIC X.                                     00002860
             03 MQLUNDIP     PIC X.                                     00002870
             03 MQLUNDIH     PIC X.                                     00002880
             03 MQLUNDIV     PIC X.                                     00002890
             03 MQLUNDIO     PIC Z(5).                                  00002900
             03 FILLER       PIC X(2).                                  00002910
             03 MQMARDIA     PIC X.                                     00002920
             03 MQMARDIC     PIC X.                                     00002930
             03 MQMARDIP     PIC X.                                     00002940
             03 MQMARDIH     PIC X.                                     00002950
             03 MQMARDIV     PIC X.                                     00002960
             03 MQMARDIO     PIC Z(5).                                  00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MQMERCRA     PIC X.                                     00002990
             03 MQMERCRC     PIC X.                                     00003000
             03 MQMERCRP     PIC X.                                     00003010
             03 MQMERCRH     PIC X.                                     00003020
             03 MQMERCRV     PIC X.                                     00003030
             03 MQMERCRO     PIC Z(5).                                  00003040
             03 FILLER       PIC X(2).                                  00003050
             03 MQJEUDIA     PIC X.                                     00003060
             03 MQJEUDIC     PIC X.                                     00003070
             03 MQJEUDIP     PIC X.                                     00003080
             03 MQJEUDIH     PIC X.                                     00003090
             03 MQJEUDIV     PIC X.                                     00003100
             03 MQJEUDIO     PIC Z(5).                                  00003110
             03 FILLER       PIC X(2).                                  00003120
             03 MQVENDRA     PIC X.                                     00003130
             03 MQVENDRC     PIC X.                                     00003140
             03 MQVENDRP     PIC X.                                     00003150
             03 MQVENDRH     PIC X.                                     00003160
             03 MQVENDRV     PIC X.                                     00003170
             03 MQVENDRO     PIC Z(5).                                  00003180
             03 FILLER       PIC X(2).                                  00003190
             03 MQSAMEDA     PIC X.                                     00003200
             03 MQSAMEDC     PIC X.                                     00003210
             03 MQSAMEDP     PIC X.                                     00003220
             03 MQSAMEDH     PIC X.                                     00003230
             03 MQSAMEDV     PIC X.                                     00003240
             03 MQSAMEDO     PIC Z(5).                                  00003250
             03 FILLER       PIC X(2).                                  00003260
             03 MQDIMANA     PIC X.                                     00003270
             03 MQDIMANC     PIC X.                                     00003280
             03 MQDIMANP     PIC X.                                     00003290
             03 MQDIMANH     PIC X.                                     00003300
             03 MQDIMANV     PIC X.                                     00003310
             03 MQDIMANO     PIC Z(5).                                  00003320
      * ZONE CMD AIDA                                                   00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MZONCMDA  PIC X.                                          00003350
           02 MZONCMDC  PIC X.                                          00003360
           02 MZONCMDP  PIC X.                                          00003370
           02 MZONCMDH  PIC X.                                          00003380
           02 MZONCMDV  PIC X.                                          00003390
           02 MZONCMDO  PIC X(15).                                      00003400
      * MESSAGE ERREUR                                                  00003410
           02 FILLER    PIC X(2).                                       00003420
           02 MLIBERRA  PIC X.                                          00003430
           02 MLIBERRC  PIC X.                                          00003440
           02 MLIBERRP  PIC X.                                          00003450
           02 MLIBERRH  PIC X.                                          00003460
           02 MLIBERRV  PIC X.                                          00003470
           02 MLIBERRO  PIC X(58).                                      00003480
      * CODE TRANSACTION                                                00003490
           02 FILLER    PIC X(2).                                       00003500
           02 MCODTRAA  PIC X.                                          00003510
           02 MCODTRAC  PIC X.                                          00003520
           02 MCODTRAP  PIC X.                                          00003530
           02 MCODTRAH  PIC X.                                          00003540
           02 MCODTRAV  PIC X.                                          00003550
           02 MCODTRAO  PIC X(4).                                       00003560
      * CICS DE TRAVAIL                                                 00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MCICSA    PIC X.                                          00003590
           02 MCICSC    PIC X.                                          00003600
           02 MCICSP    PIC X.                                          00003610
           02 MCICSH    PIC X.                                          00003620
           02 MCICSV    PIC X.                                          00003630
           02 MCICSO    PIC X(5).                                       00003640
      * NETNAME                                                         00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MNETNAMA  PIC X.                                          00003670
           02 MNETNAMC  PIC X.                                          00003680
           02 MNETNAMP  PIC X.                                          00003690
           02 MNETNAMH  PIC X.                                          00003700
           02 MNETNAMV  PIC X.                                          00003710
           02 MNETNAMO  PIC X(8).                                       00003720
      * CODE TERMINAL                                                   00003730
           02 FILLER    PIC X(2).                                       00003740
           02 MSCREENA  PIC X.                                          00003750
           02 MSCREENC  PIC X.                                          00003760
           02 MSCREENP  PIC X.                                          00003770
           02 MSCREENH  PIC X.                                          00003780
           02 MSCREENV  PIC X.                                          00003790
           02 MSCREENO  PIC X(5).                                       00003800
                                                                                
