      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU69   EMU69                                              00000020
      ***************************************************************** 00000030
       01   ESC10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEI   PIC X(4).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSSOCI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSMAGL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MSMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSMAGF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MSMAGI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLLIEUI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSSTATUL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSSTATUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSSTATUF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSSTATUI  PIC X.                                          00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSSOCPFL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MSSOCPFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSSOCPFF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MSSOCPFI  PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSPTFL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MSPTFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSPTFF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSPTFI    PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPTFL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLPTFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLPTFF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLPTFI    PIC X(20).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSVENTEL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MSVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSVENTEF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSVENTEI  PIC X(7).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSHDELL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MSHDELL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSHDELF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSHDELI   PIC X.                                          00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCODICL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MSCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCODICF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MSCODICI  PIC X(7).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCUISL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MSCUISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSCUISF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSCUISI   PIC X.                                          00000650
           02 MNCODICD OCCURS   6 TIMES .                               00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNCODICI     PIC X(7).                                  00000700
           02 MCFAMD OCCURS   6 TIMES .                                 00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000720
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000730
             03 FILLER  PIC X(4).                                       00000740
             03 MCFAMI  PIC X(5).                                       00000750
           02 MQTED OCCURS   6 TIMES .                                  00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000770
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MQTEI   PIC X(6).                                       00000800
           02 MSTATUTD OCCURS   6 TIMES .                               00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTATUTL     COMP PIC S9(4).                            00000820
      *--                                                                       
             03 MSTATUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTATUTF     PIC X.                                     00000830
             03 FILLER  PIC X(4).                                       00000840
             03 MSTATUTI     PIC X(3).                                  00000850
           02 MSTVTED OCCURS   6 TIMES .                                00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTVTEL      COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MSTVTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MSTVTEF      PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MSTVTEI      PIC X.                                     00000900
           02 MNVENTED OCCURS   6 TIMES .                               00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNVENTEL     COMP PIC S9(4).                            00000920
      *--                                                                       
             03 MNVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNVENTEF     PIC X.                                     00000930
             03 FILLER  PIC X(4).                                       00000940
             03 MNVENTEI     PIC X(7).                                  00000950
           02 MDRECD OCCURS   6 TIMES .                                 00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDRECL  COMP PIC S9(4).                                 00000970
      *--                                                                       
             03 MDRECL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDRECF  PIC X.                                          00000980
             03 FILLER  PIC X(4).                                       00000990
             03 MDRECI  PIC X(8).                                       00001000
           02 MDDELIVD OCCURS   6 TIMES .                               00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDELIVL     COMP PIC S9(4).                            00001020
      *--                                                                       
             03 MDDELIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDDELIVF     PIC X.                                     00001030
             03 FILLER  PIC X(4).                                       00001040
             03 MDDELIVI     PIC X(8).                                  00001050
           02 MHDELAID OCCURS   6 TIMES .                               00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHDELAIL     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MHDELAIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MHDELAIF     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MHDELAII     PIC X.                                     00001100
           02 MCUISD OCCURS   6 TIMES .                                 00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCUISL  COMP PIC S9(4).                                 00001120
      *--                                                                       
             03 MCUISL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCUISF  PIC X.                                          00001130
             03 FILLER  PIC X(4).                                       00001140
             03 MCUISI  PIC X.                                          00001150
           02 MMARQD OCCURS   6 TIMES .                                 00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMARQL  COMP PIC S9(4).                                 00001170
      *--                                                                       
             03 MMARQL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MMARQF  PIC X.                                          00001180
             03 FILLER  PIC X(4).                                       00001190
             03 MMARQI  PIC X(5).                                       00001200
           02 MREFD OCCURS   6 TIMES .                                  00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFL   COMP PIC S9(4).                                 00001220
      *--                                                                       
             03 MREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MREFF   PIC X.                                          00001230
             03 FILLER  PIC X(4).                                       00001240
             03 MREFI   PIC X(20).                                      00001250
           02 MSOCD OCCURS   6 TIMES .                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCL   COMP PIC S9(4).                                 00001270
      *--                                                                       
             03 MSOCL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSOCF   PIC X.                                          00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MSOCI   PIC X(3).                                       00001300
           02 MMAGD OCCURS   6 TIMES .                                  00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMAGL   COMP PIC S9(4).                                 00001320
      *--                                                                       
             03 MMAGL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMAGF   PIC X.                                          00001330
             03 FILLER  PIC X(4).                                       00001340
             03 MMAGI   PIC X(3).                                       00001350
           02 MNMUTD OCCURS   6 TIMES .                                 00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMUTL  COMP PIC S9(4).                                 00001370
      *--                                                                       
             03 MNMUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMUTF  PIC X.                                          00001380
             03 FILLER  PIC X(4).                                       00001390
             03 MNMUTI  PIC X(7).                                       00001400
           02 MMODDELD OCCURS   6 TIMES .                               00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMODDELL     COMP PIC S9(4).                            00001420
      *--                                                                       
             03 MMODDELL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMODDELF     PIC X.                                     00001430
             03 FILLER  PIC X(4).                                       00001440
             03 MMODDELI     PIC X(3).                                  00001450
           02 MSOCPLTD OCCURS   6 TIMES .                               00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCPLTL     COMP PIC S9(4).                            00001470
      *--                                                                       
             03 MSOCPLTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSOCPLTF     PIC X.                                     00001480
             03 FILLER  PIC X(4).                                       00001490
             03 MSOCPLTI     PIC X(3).                                  00001500
           02 MMAGPLTD OCCURS   6 TIMES .                               00001510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMAGPLTL     COMP PIC S9(4).                            00001520
      *--                                                                       
             03 MMAGPLTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMAGPLTF     PIC X.                                     00001530
             03 FILLER  PIC X(4).                                       00001540
             03 MMAGPLTI     PIC X(3).                                  00001550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001560
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001570
           02 FILLER    PIC X(4).                                       00001580
           02 MLIBERRI  PIC X(78).                                      00001590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001600
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001610
           02 FILLER    PIC X(4).                                       00001620
           02 MCODTRAI  PIC X(4).                                       00001630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001640
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001650
           02 FILLER    PIC X(4).                                       00001660
           02 MCICSI    PIC X(5).                                       00001670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001680
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001690
           02 FILLER    PIC X(4).                                       00001700
           02 MNETNAMI  PIC X(8).                                       00001710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001720
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001730
           02 FILLER    PIC X(4).                                       00001740
           02 MSCREENI  PIC X(4).                                       00001750
      ***************************************************************** 00001760
      * SDF: EMU69   EMU69                                              00001770
      ***************************************************************** 00001780
       01   ESC10O REDEFINES ESC10I.                                    00001790
           02 FILLER    PIC X(12).                                      00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MDATJOUA  PIC X.                                          00001820
           02 MDATJOUC  PIC X.                                          00001830
           02 MDATJOUP  PIC X.                                          00001840
           02 MDATJOUH  PIC X.                                          00001850
           02 MDATJOUV  PIC X.                                          00001860
           02 MDATJOUO  PIC X(10).                                      00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MTIMJOUA  PIC X.                                          00001890
           02 MTIMJOUC  PIC X.                                          00001900
           02 MTIMJOUP  PIC X.                                          00001910
           02 MTIMJOUH  PIC X.                                          00001920
           02 MTIMJOUV  PIC X.                                          00001930
           02 MTIMJOUO  PIC X(5).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MPAGEA    PIC X.                                          00001960
           02 MPAGEC    PIC X.                                          00001970
           02 MPAGEP    PIC X.                                          00001980
           02 MPAGEH    PIC X.                                          00001990
           02 MPAGEV    PIC X.                                          00002000
           02 MPAGEO    PIC X(4).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MNPAGEA   PIC X.                                          00002030
           02 MNPAGEC   PIC X.                                          00002040
           02 MNPAGEP   PIC X.                                          00002050
           02 MNPAGEH   PIC X.                                          00002060
           02 MNPAGEV   PIC X.                                          00002070
           02 MNPAGEO   PIC X(4).                                       00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MSSOCA    PIC X.                                          00002100
           02 MSSOCC    PIC X.                                          00002110
           02 MSSOCP    PIC X.                                          00002120
           02 MSSOCH    PIC X.                                          00002130
           02 MSSOCV    PIC X.                                          00002140
           02 MSSOCO    PIC X(3).                                       00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MSMAGA    PIC X.                                          00002170
           02 MSMAGC    PIC X.                                          00002180
           02 MSMAGP    PIC X.                                          00002190
           02 MSMAGH    PIC X.                                          00002200
           02 MSMAGV    PIC X.                                          00002210
           02 MSMAGO    PIC X(3).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MLLIEUA   PIC X.                                          00002240
           02 MLLIEUC   PIC X.                                          00002250
           02 MLLIEUP   PIC X.                                          00002260
           02 MLLIEUH   PIC X.                                          00002270
           02 MLLIEUV   PIC X.                                          00002280
           02 MLLIEUO   PIC X(20).                                      00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MSSTATUA  PIC X.                                          00002310
           02 MSSTATUC  PIC X.                                          00002320
           02 MSSTATUP  PIC X.                                          00002330
           02 MSSTATUH  PIC X.                                          00002340
           02 MSSTATUV  PIC X.                                          00002350
           02 MSSTATUO  PIC X.                                          00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MSSOCPFA  PIC X.                                          00002380
           02 MSSOCPFC  PIC X.                                          00002390
           02 MSSOCPFP  PIC X.                                          00002400
           02 MSSOCPFH  PIC X.                                          00002410
           02 MSSOCPFV  PIC X.                                          00002420
           02 MSSOCPFO  PIC X(3).                                       00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MSPTFA    PIC X.                                          00002450
           02 MSPTFC    PIC X.                                          00002460
           02 MSPTFP    PIC X.                                          00002470
           02 MSPTFH    PIC X.                                          00002480
           02 MSPTFV    PIC X.                                          00002490
           02 MSPTFO    PIC X(3).                                       00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MLPTFA    PIC X.                                          00002520
           02 MLPTFC    PIC X.                                          00002530
           02 MLPTFP    PIC X.                                          00002540
           02 MLPTFH    PIC X.                                          00002550
           02 MLPTFV    PIC X.                                          00002560
           02 MLPTFO    PIC X(20).                                      00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MSVENTEA  PIC X.                                          00002590
           02 MSVENTEC  PIC X.                                          00002600
           02 MSVENTEP  PIC X.                                          00002610
           02 MSVENTEH  PIC X.                                          00002620
           02 MSVENTEV  PIC X.                                          00002630
           02 MSVENTEO  PIC X(7).                                       00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MSHDELA   PIC X.                                          00002660
           02 MSHDELC   PIC X.                                          00002670
           02 MSHDELP   PIC X.                                          00002680
           02 MSHDELH   PIC X.                                          00002690
           02 MSHDELV   PIC X.                                          00002700
           02 MSHDELO   PIC X.                                          00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MSCODICA  PIC X.                                          00002730
           02 MSCODICC  PIC X.                                          00002740
           02 MSCODICP  PIC X.                                          00002750
           02 MSCODICH  PIC X.                                          00002760
           02 MSCODICV  PIC X.                                          00002770
           02 MSCODICO  PIC X(7).                                       00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MSCUISA   PIC X.                                          00002800
           02 MSCUISC   PIC X.                                          00002810
           02 MSCUISP   PIC X.                                          00002820
           02 MSCUISH   PIC X.                                          00002830
           02 MSCUISV   PIC X.                                          00002840
           02 MSCUISO   PIC X.                                          00002850
           02 DFHMS1 OCCURS   6 TIMES .                                 00002860
             03 FILLER       PIC X(2).                                  00002870
             03 MNCODICA     PIC X.                                     00002880
             03 MNCODICC     PIC X.                                     00002890
             03 MNCODICP     PIC X.                                     00002900
             03 MNCODICH     PIC X.                                     00002910
             03 MNCODICV     PIC X.                                     00002920
             03 MNCODICO     PIC X(7).                                  00002930
           02 DFHMS2 OCCURS   6 TIMES .                                 00002940
             03 FILLER       PIC X(2).                                  00002950
             03 MCFAMA  PIC X.                                          00002960
             03 MCFAMC  PIC X.                                          00002970
             03 MCFAMP  PIC X.                                          00002980
             03 MCFAMH  PIC X.                                          00002990
             03 MCFAMV  PIC X.                                          00003000
             03 MCFAMO  PIC X(5).                                       00003010
           02 DFHMS3 OCCURS   6 TIMES .                                 00003020
             03 FILLER       PIC X(2).                                  00003030
             03 MQTEA   PIC X.                                          00003040
             03 MQTEC   PIC X.                                          00003050
             03 MQTEP   PIC X.                                          00003060
             03 MQTEH   PIC X.                                          00003070
             03 MQTEV   PIC X.                                          00003080
             03 MQTEO   PIC X(6).                                       00003090
           02 DFHMS4 OCCURS   6 TIMES .                                 00003100
             03 FILLER       PIC X(2).                                  00003110
             03 MSTATUTA     PIC X.                                     00003120
             03 MSTATUTC     PIC X.                                     00003130
             03 MSTATUTP     PIC X.                                     00003140
             03 MSTATUTH     PIC X.                                     00003150
             03 MSTATUTV     PIC X.                                     00003160
             03 MSTATUTO     PIC X(3).                                  00003170
           02 DFHMS5 OCCURS   6 TIMES .                                 00003180
             03 FILLER       PIC X(2).                                  00003190
             03 MSTVTEA      PIC X.                                     00003200
             03 MSTVTEC PIC X.                                          00003210
             03 MSTVTEP PIC X.                                          00003220
             03 MSTVTEH PIC X.                                          00003230
             03 MSTVTEV PIC X.                                          00003240
             03 MSTVTEO      PIC X.                                     00003250
           02 DFHMS6 OCCURS   6 TIMES .                                 00003260
             03 FILLER       PIC X(2).                                  00003270
             03 MNVENTEA     PIC X.                                     00003280
             03 MNVENTEC     PIC X.                                     00003290
             03 MNVENTEP     PIC X.                                     00003300
             03 MNVENTEH     PIC X.                                     00003310
             03 MNVENTEV     PIC X.                                     00003320
             03 MNVENTEO     PIC X(7).                                  00003330
           02 DFHMS7 OCCURS   6 TIMES .                                 00003340
             03 FILLER       PIC X(2).                                  00003350
             03 MDRECA  PIC X.                                          00003360
             03 MDRECC  PIC X.                                          00003370
             03 MDRECP  PIC X.                                          00003380
             03 MDRECH  PIC X.                                          00003390
             03 MDRECV  PIC X.                                          00003400
             03 MDRECO  PIC X(8).                                       00003410
           02 DFHMS8 OCCURS   6 TIMES .                                 00003420
             03 FILLER       PIC X(2).                                  00003430
             03 MDDELIVA     PIC X.                                     00003440
             03 MDDELIVC     PIC X.                                     00003450
             03 MDDELIVP     PIC X.                                     00003460
             03 MDDELIVH     PIC X.                                     00003470
             03 MDDELIVV     PIC X.                                     00003480
             03 MDDELIVO     PIC X(8).                                  00003490
           02 DFHMS9 OCCURS   6 TIMES .                                 00003500
             03 FILLER       PIC X(2).                                  00003510
             03 MHDELAIA     PIC X.                                     00003520
             03 MHDELAIC     PIC X.                                     00003530
             03 MHDELAIP     PIC X.                                     00003540
             03 MHDELAIH     PIC X.                                     00003550
             03 MHDELAIV     PIC X.                                     00003560
             03 MHDELAIO     PIC X.                                     00003570
           02 DFHMS10 OCCURS   6 TIMES .                                00003580
             03 FILLER       PIC X(2).                                  00003590
             03 MCUISA  PIC X.                                          00003600
             03 MCUISC  PIC X.                                          00003610
             03 MCUISP  PIC X.                                          00003620
             03 MCUISH  PIC X.                                          00003630
             03 MCUISV  PIC X.                                          00003640
             03 MCUISO  PIC X.                                          00003650
           02 DFHMS11 OCCURS   6 TIMES .                                00003660
             03 FILLER       PIC X(2).                                  00003670
             03 MMARQA  PIC X.                                          00003680
             03 MMARQC  PIC X.                                          00003690
             03 MMARQP  PIC X.                                          00003700
             03 MMARQH  PIC X.                                          00003710
             03 MMARQV  PIC X.                                          00003720
             03 MMARQO  PIC X(5).                                       00003730
           02 DFHMS12 OCCURS   6 TIMES .                                00003740
             03 FILLER       PIC X(2).                                  00003750
             03 MREFA   PIC X.                                          00003760
             03 MREFC   PIC X.                                          00003770
             03 MREFP   PIC X.                                          00003780
             03 MREFH   PIC X.                                          00003790
             03 MREFV   PIC X.                                          00003800
             03 MREFO   PIC X(20).                                      00003810
           02 DFHMS13 OCCURS   6 TIMES .                                00003820
             03 FILLER       PIC X(2).                                  00003830
             03 MSOCA   PIC X.                                          00003840
             03 MSOCC   PIC X.                                          00003850
             03 MSOCP   PIC X.                                          00003860
             03 MSOCH   PIC X.                                          00003870
             03 MSOCV   PIC X.                                          00003880
             03 MSOCO   PIC X(3).                                       00003890
           02 DFHMS14 OCCURS   6 TIMES .                                00003900
             03 FILLER       PIC X(2).                                  00003910
             03 MMAGA   PIC X.                                          00003920
             03 MMAGC   PIC X.                                          00003930
             03 MMAGP   PIC X.                                          00003940
             03 MMAGH   PIC X.                                          00003950
             03 MMAGV   PIC X.                                          00003960
             03 MMAGO   PIC X(3).                                       00003970
           02 DFHMS15 OCCURS   6 TIMES .                                00003980
             03 FILLER       PIC X(2).                                  00003990
             03 MNMUTA  PIC X.                                          00004000
             03 MNMUTC  PIC X.                                          00004010
             03 MNMUTP  PIC X.                                          00004020
             03 MNMUTH  PIC X.                                          00004030
             03 MNMUTV  PIC X.                                          00004040
             03 MNMUTO  PIC X(7).                                       00004050
           02 DFHMS16 OCCURS   6 TIMES .                                00004060
             03 FILLER       PIC X(2).                                  00004070
             03 MMODDELA     PIC X.                                     00004080
             03 MMODDELC     PIC X.                                     00004090
             03 MMODDELP     PIC X.                                     00004100
             03 MMODDELH     PIC X.                                     00004110
             03 MMODDELV     PIC X.                                     00004120
             03 MMODDELO     PIC X(3).                                  00004130
           02 DFHMS17 OCCURS   6 TIMES .                                00004140
             03 FILLER       PIC X(2).                                  00004150
             03 MSOCPLTA     PIC X.                                     00004160
             03 MSOCPLTC     PIC X.                                     00004170
             03 MSOCPLTP     PIC X.                                     00004180
             03 MSOCPLTH     PIC X.                                     00004190
             03 MSOCPLTV     PIC X.                                     00004200
             03 MSOCPLTO     PIC X(3).                                  00004210
           02 DFHMS18 OCCURS   6 TIMES .                                00004220
             03 FILLER       PIC X(2).                                  00004230
             03 MMAGPLTA     PIC X.                                     00004240
             03 MMAGPLTC     PIC X.                                     00004250
             03 MMAGPLTP     PIC X.                                     00004260
             03 MMAGPLTH     PIC X.                                     00004270
             03 MMAGPLTV     PIC X.                                     00004280
             03 MMAGPLTO     PIC X(3).                                  00004290
           02 FILLER    PIC X(2).                                       00004300
           02 MLIBERRA  PIC X.                                          00004310
           02 MLIBERRC  PIC X.                                          00004320
           02 MLIBERRP  PIC X.                                          00004330
           02 MLIBERRH  PIC X.                                          00004340
           02 MLIBERRV  PIC X.                                          00004350
           02 MLIBERRO  PIC X(78).                                      00004360
           02 FILLER    PIC X(2).                                       00004370
           02 MCODTRAA  PIC X.                                          00004380
           02 MCODTRAC  PIC X.                                          00004390
           02 MCODTRAP  PIC X.                                          00004400
           02 MCODTRAH  PIC X.                                          00004410
           02 MCODTRAV  PIC X.                                          00004420
           02 MCODTRAO  PIC X(4).                                       00004430
           02 FILLER    PIC X(2).                                       00004440
           02 MCICSA    PIC X.                                          00004450
           02 MCICSC    PIC X.                                          00004460
           02 MCICSP    PIC X.                                          00004470
           02 MCICSH    PIC X.                                          00004480
           02 MCICSV    PIC X.                                          00004490
           02 MCICSO    PIC X(5).                                       00004500
           02 FILLER    PIC X(2).                                       00004510
           02 MNETNAMA  PIC X.                                          00004520
           02 MNETNAMC  PIC X.                                          00004530
           02 MNETNAMP  PIC X.                                          00004540
           02 MNETNAMH  PIC X.                                          00004550
           02 MNETNAMV  PIC X.                                          00004560
           02 MNETNAMO  PIC X(8).                                       00004570
           02 FILLER    PIC X(2).                                       00004580
           02 MSCREENA  PIC X.                                          00004590
           02 MSCREENC  PIC X.                                          00004600
           02 MSCREENP  PIC X.                                          00004610
           02 MSCREENH  PIC X.                                          00004620
           02 MSCREENV  PIC X.                                          00004630
           02 MSCREENO  PIC X(4).                                       00004640
                                                                                
