      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR24   EGR24                                              00000020
      ***************************************************************** 00000030
       01   EGR24I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * PAGE                                                            00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MPAGEI    PIC X(3).                                       00000200
      * CODE MARQUE                                                     00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQUL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCMARQUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQUF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCMARQUI  PIC X(5).                                       00000250
      * LIBELLE MARQUE                                                  00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQUL  COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MLMARQUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMARQUF  PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MLMARQUI  PIC X(20).                                      00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQCOLISAGEL    COMP PIC S9(4).                            00000310
      *--                                                                       
           02 MQCOLISAGEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQCOLISAGEF    PIC X.                                     00000320
           02 FILLER    PIC X(4).                                       00000330
           02 MQCOLISAGEI    PIC X(5).                                  00000340
      * CODE FAMILLE                                                    00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MCFAMI    PIC X(5).                                       00000390
      * LIBELLE FAMILLE                                                 00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000410
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000420
           02 FILLER    PIC X(4).                                       00000430
           02 MLFAMI    PIC X(20).                                      00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQCOLISL  COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MQCOLISL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQCOLISF  PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MQCOLISI  PIC X(3).                                       00000480
      * CODE RAYON                                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRAYONL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCRAYONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCRAYONF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCRAYONI  PIC X(5).                                       00000530
      * LIBELLE RAYON                                                   00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRAYONL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLRAYONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLRAYONF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLRAYONI  PIC X(20).                                      00000580
           02 M91I OCCURS   12 TIMES .                                  00000590
      * CODIC                                                           00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MNCODICI     PIC X(7).                                  00000640
      * CODE FAMILLE                                                    00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAMILL     COMP PIC S9(4).                            00000660
      *--                                                                       
             03 MCFAMILL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCFAMILF     PIC X.                                     00000670
             03 FILLER  PIC X(4).                                       00000680
             03 MCFAMILI     PIC X(5).                                  00000690
      * LIB.EMBALLAG                                                    00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLEMBALL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MLEMBALL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLEMBALF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MLEMBALI     PIC X(50).                                 00000740
      * QUANTITE                                                        00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000760
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000770
             03 FILLER  PIC X(4).                                       00000780
             03 MQTEI   PIC X(5).                                       00000790
      * SELECTION                                                       00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELECTL     COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MSELECTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELECTF     PIC X.                                     00000820
             03 FILLER  PIC X(4).                                       00000830
             03 MSELECTI     PIC X.                                     00000840
      * ZONE CMD AIDA                                                   00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MZONCMDI  PIC X(15).                                      00000890
      * MESSAGE ERREUR                                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MLIBERRI  PIC X(58).                                      00000940
      * CODE TRANSACTION                                                00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MCODTRAI  PIC X(4).                                       00000990
      * CICS DE TRAVAIL                                                 00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001020
           02 FILLER    PIC X(4).                                       00001030
           02 MCICSI    PIC X(5).                                       00001040
      * NETNAME                                                         00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MNETNAMI  PIC X(8).                                       00001090
      * CODE TERMINAL                                                   00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(5).                                       00001140
      ***************************************************************** 00001150
      * SDF: EGR24   EGR24                                              00001160
      ***************************************************************** 00001170
       01   EGR24O REDEFINES EGR24I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
      * DATE DU JOUR                                                    00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MDATJOUA  PIC X.                                          00001220
           02 MDATJOUC  PIC X.                                          00001230
           02 MDATJOUP  PIC X.                                          00001240
           02 MDATJOUH  PIC X.                                          00001250
           02 MDATJOUV  PIC X.                                          00001260
           02 MDATJOUO  PIC X(10).                                      00001270
      * HEURE                                                           00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MTIMJOUA  PIC X.                                          00001300
           02 MTIMJOUC  PIC X.                                          00001310
           02 MTIMJOUP  PIC X.                                          00001320
           02 MTIMJOUH  PIC X.                                          00001330
           02 MTIMJOUV  PIC X.                                          00001340
           02 MTIMJOUO  PIC X(5).                                       00001350
      * PAGE                                                            00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MPAGEA    PIC X.                                          00001380
           02 MPAGEC    PIC X.                                          00001390
           02 MPAGEP    PIC X.                                          00001400
           02 MPAGEH    PIC X.                                          00001410
           02 MPAGEV    PIC X.                                          00001420
           02 MPAGEO    PIC X(3).                                       00001430
      * CODE MARQUE                                                     00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MCMARQUA  PIC X.                                          00001460
           02 MCMARQUC  PIC X.                                          00001470
           02 MCMARQUP  PIC X.                                          00001480
           02 MCMARQUH  PIC X.                                          00001490
           02 MCMARQUV  PIC X.                                          00001500
           02 MCMARQUO  PIC X(5).                                       00001510
      * LIBELLE MARQUE                                                  00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MLMARQUA  PIC X.                                          00001540
           02 MLMARQUC  PIC X.                                          00001550
           02 MLMARQUP  PIC X.                                          00001560
           02 MLMARQUH  PIC X.                                          00001570
           02 MLMARQUV  PIC X.                                          00001580
           02 MLMARQUO  PIC X(20).                                      00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MQCOLISAGEA    PIC X.                                     00001610
           02 MQCOLISAGEC    PIC X.                                     00001620
           02 MQCOLISAGEP    PIC X.                                     00001630
           02 MQCOLISAGEH    PIC X.                                     00001640
           02 MQCOLISAGEV    PIC X.                                     00001650
           02 MQCOLISAGEO    PIC X(5).                                  00001660
      * CODE FAMILLE                                                    00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MCFAMA    PIC X.                                          00001690
           02 MCFAMC    PIC X.                                          00001700
           02 MCFAMP    PIC X.                                          00001710
           02 MCFAMH    PIC X.                                          00001720
           02 MCFAMV    PIC X.                                          00001730
           02 MCFAMO    PIC X(5).                                       00001740
      * LIBELLE FAMILLE                                                 00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MLFAMA    PIC X.                                          00001770
           02 MLFAMC    PIC X.                                          00001780
           02 MLFAMP    PIC X.                                          00001790
           02 MLFAMH    PIC X.                                          00001800
           02 MLFAMV    PIC X.                                          00001810
           02 MLFAMO    PIC X(20).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MQCOLISA  PIC X.                                          00001840
           02 MQCOLISC  PIC X.                                          00001850
           02 MQCOLISP  PIC X.                                          00001860
           02 MQCOLISH  PIC X.                                          00001870
           02 MQCOLISV  PIC X.                                          00001880
           02 MQCOLISO  PIC X(3).                                       00001890
      * CODE RAYON                                                      00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MCRAYONA  PIC X.                                          00001920
           02 MCRAYONC  PIC X.                                          00001930
           02 MCRAYONP  PIC X.                                          00001940
           02 MCRAYONH  PIC X.                                          00001950
           02 MCRAYONV  PIC X.                                          00001960
           02 MCRAYONO  PIC X(5).                                       00001970
      * LIBELLE RAYON                                                   00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MLRAYONA  PIC X.                                          00002000
           02 MLRAYONC  PIC X.                                          00002010
           02 MLRAYONP  PIC X.                                          00002020
           02 MLRAYONH  PIC X.                                          00002030
           02 MLRAYONV  PIC X.                                          00002040
           02 MLRAYONO  PIC X(20).                                      00002050
           02 M91O OCCURS   12 TIMES .                                  00002060
      * CODIC                                                           00002070
             03 FILLER       PIC X(2).                                  00002080
             03 MNCODICA     PIC X.                                     00002090
             03 MNCODICC     PIC X.                                     00002100
             03 MNCODICP     PIC X.                                     00002110
             03 MNCODICH     PIC X.                                     00002120
             03 MNCODICV     PIC X.                                     00002130
             03 MNCODICO     PIC X(7).                                  00002140
      * CODE FAMILLE                                                    00002150
             03 FILLER       PIC X(2).                                  00002160
             03 MCFAMILA     PIC X.                                     00002170
             03 MCFAMILC     PIC X.                                     00002180
             03 MCFAMILP     PIC X.                                     00002190
             03 MCFAMILH     PIC X.                                     00002200
             03 MCFAMILV     PIC X.                                     00002210
             03 MCFAMILO     PIC X(5).                                  00002220
      * LIB.EMBALLAG                                                    00002230
             03 FILLER       PIC X(2).                                  00002240
             03 MLEMBALA     PIC X.                                     00002250
             03 MLEMBALC     PIC X.                                     00002260
             03 MLEMBALP     PIC X.                                     00002270
             03 MLEMBALH     PIC X.                                     00002280
             03 MLEMBALV     PIC X.                                     00002290
             03 MLEMBALO     PIC X(50).                                 00002300
      * QUANTITE                                                        00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MQTEA   PIC X.                                          00002330
             03 MQTEC   PIC X.                                          00002340
             03 MQTEP   PIC X.                                          00002350
             03 MQTEH   PIC X.                                          00002360
             03 MQTEV   PIC X.                                          00002370
             03 MQTEO   PIC X(5).                                       00002380
      * SELECTION                                                       00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MSELECTA     PIC X.                                     00002410
             03 MSELECTC     PIC X.                                     00002420
             03 MSELECTP     PIC X.                                     00002430
             03 MSELECTH     PIC X.                                     00002440
             03 MSELECTV     PIC X.                                     00002450
             03 MSELECTO     PIC X.                                     00002460
      * ZONE CMD AIDA                                                   00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MZONCMDA  PIC X.                                          00002490
           02 MZONCMDC  PIC X.                                          00002500
           02 MZONCMDP  PIC X.                                          00002510
           02 MZONCMDH  PIC X.                                          00002520
           02 MZONCMDV  PIC X.                                          00002530
           02 MZONCMDO  PIC X(15).                                      00002540
      * MESSAGE ERREUR                                                  00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MLIBERRA  PIC X.                                          00002570
           02 MLIBERRC  PIC X.                                          00002580
           02 MLIBERRP  PIC X.                                          00002590
           02 MLIBERRH  PIC X.                                          00002600
           02 MLIBERRV  PIC X.                                          00002610
           02 MLIBERRO  PIC X(58).                                      00002620
      * CODE TRANSACTION                                                00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MCODTRAA  PIC X.                                          00002650
           02 MCODTRAC  PIC X.                                          00002660
           02 MCODTRAP  PIC X.                                          00002670
           02 MCODTRAH  PIC X.                                          00002680
           02 MCODTRAV  PIC X.                                          00002690
           02 MCODTRAO  PIC X(4).                                       00002700
      * CICS DE TRAVAIL                                                 00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MCICSA    PIC X.                                          00002730
           02 MCICSC    PIC X.                                          00002740
           02 MCICSP    PIC X.                                          00002750
           02 MCICSH    PIC X.                                          00002760
           02 MCICSV    PIC X.                                          00002770
           02 MCICSO    PIC X(5).                                       00002780
      * NETNAME                                                         00002790
           02 FILLER    PIC X(2).                                       00002800
           02 MNETNAMA  PIC X.                                          00002810
           02 MNETNAMC  PIC X.                                          00002820
           02 MNETNAMP  PIC X.                                          00002830
           02 MNETNAMH  PIC X.                                          00002840
           02 MNETNAMV  PIC X.                                          00002850
           02 MNETNAMO  PIC X(8).                                       00002860
      * CODE TERMINAL                                                   00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MSCREENA  PIC X.                                          00002890
           02 MSCREENC  PIC X.                                          00002900
           02 MSCREENP  PIC X.                                          00002910
           02 MSCREENH  PIC X.                                          00002920
           02 MSCREENV  PIC X.                                          00002930
           02 MSCREENO  PIC X(5).                                       00002940
                                                                                
