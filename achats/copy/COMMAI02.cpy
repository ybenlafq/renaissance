      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   COMM PASSEE PAR LE  TGA50                                             
      *   COMAREA COMMAI02 SE DECOMPOSE EN 3 PARTIES:                           
      *    - 1 LES ECRANS MIS A JOURS                                           
      *    - 2 LES DONNEES MODIFIEES                                            
      *    - 3 LES TS MODIFIEES                                                 
      *                                                                         
      *****************************************************************         
PH1TI * MODIF 09/08/02 DSA057 CORRECTION ANOMALIE TEST INTEGRATION      00010001
PH1TI * MODIF 27/08/02 DSA056 AUTRE FONCTION DU MAD02 : SUPPRESSION DU  00010001
PH1TI * LIEN DANS LA RTGA03 CHEZ KESA POUR JDA                                  
CT10  * 10/10/02 DSA056 CUSINE X(1) + NOUVEAU CHAMP CUSINEVRAI X(5)             
CT04  * 04/12/02 DSA056 AJOUT FLAG-GA31 POUR VIDER LA RTGA31 SI ON PASSE        
      * D'UN PRODUIT SIMPLE � UN MULTICOMPOSANT ( '1' ==> DELETE                
      * '  ' ==> ON NE FAIT RIEN)                                               
FT    * QLAGRDE ETC                                                             
E0005 ******************************************************************        
      * DSA057 08/09/03 SUPPORT EVOLUTION                                       
      *                 GESTION EAN MIXTE                                       
      *                 PASSAGE MONO A MULTI COMPOSANT                          
       01  COMM-AI02-GA00-LONG-COMMAREA   PIC S9(4) VALUE +4096.                
       01  COMM-AI02-GA00-APPLI.                                                
PH1TI-* FLAG DE SUPPRESSION OU DE CRE/MAJ                                       
           02 COMM-AI02-ACTION            PIC X.                                
              88 COMM-AI02-SUPPRESSION    VALUE ' '.                            
-PH1TI        88 COMM-AI02-CRE-MAJ        VALUE '1'.                            
      *    SOCIETE - 3                                                          
           02 COMM-AI02-NSOCIETE           PIC X(3).                            
           02 COMM-AI02-MAI02-ENTREE.                                           
              03 COMM-AI02-INFO-PARAM.                                          
      *       ZONE INFO PARAM - 184                                             
                 10 COMM-AI02-PARAM    OCCURS 30.                               
                    15 COMM-AI02-CTRANS    PIC X(5).                            
                    15 COMM-AI02-WPASS     PIC X.                               
                 10 COMM-AI02-NTERMID      PIC X(4).                            
              03 COMM-AI02-GA50-GESTION.                                        
      *       ZONE GESTION - 13                                                 
               04 COMM-AI02-CFONC         PIC X.                                
               04 COMM-AI02-CODE-CFONC    PIC X(3).                             
               04 COMM-AI02-DATE-SSAAMMJJ PIC 9(8).                             
               04 COMM-AI02-MULTISOC-R    PIC X.                                
              03 COMM-AI02-GA50-DATA.                                           
      *       ZONE DONNEES COMUNES - 216                                        
                 05 COMM-AI02-INFO-ARTICLE.                                     
                 10 COMM-AI02-GA00-NCODIC PIC X(07).                            
                 10 COMM-AI02-GA00-LREFFOURN PIC X(20).                         
                 10 COMM-AI02-GA00-CFAM   PIC X(05).                            
                 10 COMM-AI02-GA00-CMARQ  PIC X(05).                            
                 10 COMM-AI02-GA00-DCREATION PIC X(08).                         
                 10 COMM-AI02-GA00-D1RECEPT PIC X(08).                          
                 10 COMM-AI02-GA00-DEFSTATUT PIC X(08).                         
                 10 COMM-AI02-GA00-DMAJ   PIC X(08).                            
                 10 COMM-AI02-GA00-LSTATCOMP PIC X(03).                         
                 10 COMM-AI02-GA00-LREFO  PIC X(20).                            
                 10 COMM-AI02-GA00-NEAN   PIC X(13).                            
                 10 COMM-AI02-GA00-CCOLOR PIC X(05).                            
                 10 COMM-AI02-GA00-CUSINE PIC X(01).                            
CT10             10 COMM-AI02-GA00-CUSINEVRAI PIC X(05).                        
                 10 COMM-AI02-GA00-CORIGPROD PIC X(05).                         
                 10 COMM-AI02-GA49-NBCOMPO PIC S9(2).                           
                 10 COMM-AI02-GA00-CASSORT PIC X(05).                           
                 10 COMM-AI02-GA00-CAPPRO PIC X(05).                            
                 10 COMM-AI02-GA00-CEXPO  PIC X(05).                            
                 10 COMM-AI02-GA00-LREFDARTY PIC X(20).                         
                 10 COMM-AI02-GA00-LCOMMENT PIC X(50).                          
                 10 COMM-AI02-GA00-WSENSVTE PIC X(01).                          
                 10 COMM-AI02-GA00-WSENSAPPRO PIC X(01).                        
                 10 COMM-AI02-GA00-NSOCDEPOT1 PIC X(03).                        
                 10 COMM-AI02-GA00-NDEPOT1 PIC X(03).                           
                 10 COMM-AI02-GA00-MULTISOC PIC X(01).                          
      *         DONNEES SECTEURS - 075                                          
                05 COMM-AI02-EGA74-DATA.                                        
                 10 COMM-AI02-GA00-COPCO  PIC X(03).                            
                 10 COMM-AI02-GA00-NCODICK PIC X(07).                           
                 10 COMM-AI02-GA00-NPROD  PIC X(15).                            
                 10 COMM-AI02-GA00-CLIGPROD PIC X(05).                          
                 10 COMM-AI02-GA00-LMODBASE PIC X(20).                          
                 10 COMM-AI02-GA00-VERSION PIC X(20).                           
      *         DONNEES GENERALES I - 25                                        
                05 COMM-AI02-EGA51-DATA.                                        
                 10 COMM-AI02-GA00-CGESTVTE PIC X(03).                          
                 10 COMM-AI02-GA00-WDACEM PIC X(01).                            
                 10 COMM-AI02-GA00-CTAUXTVA PIC X(05).                          
                 10 COMM-AI02-GA00-WTLMELA PIC X(01).                           
                 10 COMM-AI02-GA00-CGARANTIE PIC X(05).                         
                 10 COMM-AI02-GA00-CGARCONST PIC X(05).                         
                 10 FILLER                PIC X(05).                            
      *         DONNEES GENERALES II - 160                                      
                05 COMM-AI02-EGA76-DATA.                                        
                 10 COMM-AI02-GA49-IDEPT  PIC X(03).                            
                 10 COMM-AI02-GA49-ISDEPT PIC X(03).                            
                 10 COMM-AI02-GA49-ICLASS PIC X(03).                            
                 10 COMM-AI02-GA49-ISCLASS PIC X(03).                           
                 10 COMM-AI02-GA49-IATRB4 PIC X(02).                            
                 10 COMM-AI02-GA49-IATRB2 PIC X(02).                            
                 10 COMM-AI02-GA49-ISTYPE PIC X(02).                            
                 10 COMM-AI02-GA49-IWARGC PIC X(02).                            
                 10 COMM-AI02-GA49-I2TKTD PIC X(60).                            
                 10 COMM-AI02-GA49-I2ADVD PIC X(60).                            
                 10 COMM-AI02-GA49-I3MKDS PIC X(15).                            
E0005 *          10 COMM-AI02-FLAG-GA31   PIC X.                                
E0005 *          10 FILLER                PIC X(04).                            
E0005            10 FILLER PIC X(5).                                            
      *         STATUTS ARTICLE DARTY - 230                                     
                05 COMM-AI02-EGA53-DATA.                                        
                 10 COMM-AI02-GA00-MATRICE.                                     
                    15 COMM-AI02-GA00-FILIALE PIC X(03) OCCURS 10.              
                    15 COMM-AI02-GA00-FILSOC PIC X(03) OCCURS 10.               
                    15 COMM-AI02-GA00-FILDEP PIC X(03) OCCURS 10.               
                    15 COMM-AI02-GA00-NBFIL PIC 9(02).                          
                 10 COMM-AI02-GA00-NSOC2  PIC X(03).                            
                 10 COMM-AI02-GA00-NDEP2  PIC X(03).                            
                 10 COMM-AI02-GA00-NSOC3  PIC X(03).                            
                 10 COMM-AI02-GA00-NDEP3  PIC X(03).                            
JAC              10 COMM-AI02-GA00-WSENSAPPRO2 PIC X(01).                       
JAC              10 COMM-AI02-GA00-WSENSAPPRO3 PIC X(01).                       
JAC              10 COMM-AI02-WSENSAPPRO-ORIG2 PIC X(01).                       
JAC              10 COMM-AI02-WSENSAPPRO-ORIG3 PIC X(01).                       
                 10 COMM-AI02-GA00-CAPPRO2 PIC X(05).                           
                 10 COMM-AI02-GA00-CAP2-ORIG PIC X(05).                         
                 10 COMM-AI02-GA00-CAPPRO3 PIC X(05).                           
                 10 COMM-AI02-GA00-CAP3-ORIG PIC X(05).                         
                 10 COMM-AI02-GA00-LAPPRO2     PIC X(20).                       
                 10 COMM-AI02-GA00-LAPPRO3     PIC X(20).                       
                 10 COMM-AI02-GA00-CEXPO2 PIC X(05).                            
                 10 COMM-AI02-GA00-CEXPO3 PIC X(05).                            
                 10 COMM-AI02-GA00-LEXPO2 PIC X(20).                            
                 10 COMM-AI02-GA00-LEXPO3 PIC X(20).                            
                 10 COMM-AI02-LSTATCOMP2  PIC X(03).                            
                 10 COMM-AI02-LSTATCOMP3  PIC X(03).                            
                 10 COMM-AI02-GA00-DEPSUP3 PIC X(01).                           
                 10 COMM-AI02-GA53-CASSORT PIC X(05).                           
                 10 FILLER                PIC X(29).                            
      *         STATUTS ARTICLE COMET - 110                                     
                05 COMM-AI02-EGA77-DATA.                                        
                 10 COMM-AI02-GA49-IRPLCD   PIC X(01).                          
                 10 COMM-AI02-GA49-IATRB3   PIC X(02).                          
                 10 COMM-AI02-GA49-I3WKMX   PIC S9(02).                         
                 10 COMM-AI02-GA77-EXPO-COMET PIC X(01).                        
                 10 COMM-AI02-GA77-TAB1     OCCURS 30.                          
                    15 COMM-AI02-GA48-SPPRTP PIC X(02).                         
                    15 COMM-AI02-GA48-SPSTAT PIC X(01).                         
                 10 FILLER                  PIC X(13).                          
      *         DONNEES APPRO - 17                                              
                05 COMM-AI02-EGA55-DATA.                                        
                 10 COMM-AI02-GA00-CHEFPROD PIC X(05).                          
                 10 COMM-AI02-GA00-QDELAIAPPRO PIC X(03).                       
                 10 COMM-AI02-GA00-QCOLICDE PIC S9(3).                          
                 10 COMM-AI02-GA00-WSTOCKAVANCE PIC X(01).                      
                 10 FILLER                  PIC X(05).                          
      *         DONNEES ENTREPOT  I - 50                                        
                05 COMM-AI02-EGA56-DATA.                                        
                 10  COMM-AI02-GA00-CCOTEHOLD PIC X(01).                        
                 10  COMM-AI02-GA00-CMODSTOCK1 PIC X(05).                       
                 10  COMM-AI02-GA00-WMODSTOCK1 PIC X(01).                       
                 10  COMM-AI02-GA00-CMODSTOCK2 PIC X(05).                       
                 10  COMM-AI02-GA00-WMODSTOCK2 PIC X(01).                       
                 10  COMM-AI02-GA00-CMODSTOCK3 PIC X(05).                       
                 10  COMM-AI02-GA00-WMODSTOCK3 PIC X(01).                       
                 10  COMM-AI02-GA00-CZONEACCES PIC X(01).                       
                 10  COMM-AI02-GA00-QNBPRACK PIC S9(03).                        
                 10  COMM-AI02-GA00-QNBPVSOL PIC S9(03).                        
                 10  COMM-AI02-GA00-CCONTENEUR PIC X(01).                       
                 10  COMM-AI02-GA00-QNBRANMAIL PIC S9(03).                      
                 10  COMM-AI02-GA00-CSPECIFSTK PIC X(01).                       
                 10  COMM-AI02-GA00-QNBNIVGERB PIC S9(03).                      
                 10  COMM-AI02-GA00-WCROSSDOCK PIC X(01).                       
                 10  COMM-AI02-FL50-CUNITRECEPT PIC X(03).                      
                 10  COMM-AI02-FL40-CUNITVTE PIC X(03).                         
                 10  COMM-AI02-GA00-CQUOTA PIC X(05).                           
                 10  COMM-AI02-GA00-SYSMEC PIC X(01).                           
                 10  COMM-AI02-GA00-CLASSE PIC X(01).                           
                 10  FILLER                PIC X(04).                           
      *         DONNEES ENTREPOT II - 128                                       
                05 COMM-AI02-EGA78-DATA.                                        
                 10  COMM-AI02-GA00-LEMBALLAGE PIC X(50).                       
                 10  COMM-AI02-GA00-CTYPCONDT PIC X(05).                        
                 10  COMM-AI02-GA00-QCONDT    PIC S9(05).                       
                 10  COMM-AI02-GA00-QCOLIRECEPT PIC S9(05).                     
                 10  COMM-AI02-GA00-QCOLIDSTOCK PIC S9(05).                     
                 10  COMM-AI02-GA00-CFETEMPL  PIC X(01).                        
                 10  COMM-AI02-GA00-QCOLIVTE  PIC S9(05).                       
                 10  COMM-AI02-GA00-QPOIDS    PIC S9(07).                       
                 10  COMM-AI02-GA00-QLARGEUR  PIC S9(03).                       
                 10  COMM-AI02-GA00-QPROFONDEUR PIC S9(03).                     
                 10  COMM-AI02-GA00-QHAUTEUR  PIC S9(03).                       
                 10  COMM-AI02-GA00-QPOIDSDE  PIC S9(07).                       
                 10  COMM-AI02-GA00-QLARGEURDE    PIC S9(03).                   
                 10  COMM-AI02-GA00-QPROFONDEURDE PIC S9(03).                   
                 10  COMM-AI02-GA00-QHAUTEURDE    PIC S9(03).                   
                 10  COMM-AI02-FL50-QCOUCHPAL PIC S9(03).                       
                 10  COMM-AI02-FL50-QCARTCOUCH PIC S9(03).                      
                 10  COMM-AI02-FL50-QBOXCART  PIC S9(07).                       
                 10  COMM-AI02-FL50-QPRODBOX  PIC S9(07).                       
FT    *          10  FILLER                   PIC X(15).                        
                 10  COMM-AI02-GA00-QLARGDE PIC S9(04)V9.                       
                 10  COMM-AI02-GA00-QPROFDE PIC S9(04)V9.                       
                 10  COMM-AI02-GA00-QHAUTDE PIC S9(04)V9.                       
      *         DONNEES LIENS ENTRE ARTICLE - 25                                
      *         05 COMM-AI02-EGA57-58-DATA.                                     
      *          10  COMM-AI02-GA57-CODICLIE  PIC X(07).                        
      *          10  COMM-AI02-GA57-WDEGRELIB PIC X(02).                        
      *          10  COMM-AI02-GA57-LIENLIE   PIC X(05).                        
      *          10  COMM-AI02-GA57-CTYPLIEN  PIC X(05).                        
      *          10  COMM-AI02-GA57-GROUPE    PIC X.                            
      *              88 COMM-AI02-GA57-DEJA-RECU VALUE '1'.                     
      *              88 COMM-AI02-GA57-NON-RECU VALUE '0'.                      
      *          10  FILLER                   PIC X(05).                        
      *                                                POSITION -->1696         
      *         PRIX DE VENTE DARTY - 19                                        
                05 COMM-AI02-EGA59-DATA.                                        
                 10 COMM-AI02-GA00-PRAR   PIC S9(7)V9(2).                       
                 10 COMM-AI02-GA00-PBF    PIC S9(7)V9(2).                       
      *          10 FILLER                PIC X(01).                            
      *                                                                         
      *         DONNEES ETIQUETTES INFORMATIVES - 48                            
                05 COMM-AI02-EGA61-DATA.                                        
                 10  COMM-AI02-GA61-FAMCLT    PIC X(30).                        
                 10  COMM-AI02-GA61-DMAJ      PIC X(08).                        
                 10  COMM-AI02-GA61-WPROFIL-COUL PIC X.                         
                     88 COMM-AI02-GA61-PROFIL-COULEUR VALUE 'O'.                
                 10  FILLER                   PIC X(05).                        
      *         DONNEES ETIQUETTES INFORMATIVES COMET - 256                     
                05 COMM-AI02-EGA95-DATA.                                        
      *          10  COMM-AI02-GA05-I2TKTD    PIC X(60).                        
      *          10  COMM-AI02-GA05-I2ADVD    PIC X(60).                        
      *          10  COMM-AI02-GA05-I3MKDS    PIC X(15).                        
                 10  COMM-AI02-GA34-TKTID     PIC X(25).                        
                 10  COMM-AI02-GA34-TKTQTY    PIC 9(03).                        
                 10  COMM-AI02-GA34-RECICN    PIC X(20).                        
                 10  COMM-AI02-GA34-INVFLG    PIC X(01).                        
                 10  COMM-AI02-GA34-FRCFLG    PIC X(01).                        
                 10  COMM-AI02-GA34-STKTKT    PIC X(01).                        
                 10  COMM-AI02-GA34-RSNTXT    PIC X(200).                       
                 10  FILLER                   PIC X(05).                        
      *         DONNEES VENTE - 18                                              
                05 COMM-AI02-EGA63-DATA.                                        
                 10  COMM-AI02-GA00-WLCONF    PIC X(01).                        
                 10  COMM-AI02-GA00-QCONTENU  PIC S9(05).                       
                 10  COMM-AI02-GA00-QGRATUITE PIC S9(05).                       
                 10  COMM-AI02-GA00-CFETIQINFO PIC X(01).                       
                 10  COMM-AI02-GA00-CFETIQPRIX PIC X(01).                       
                 10  FILLER                   PIC X(05).                        
      *         PRESTATIONS LIEES - 23                                          
                05 COMM-AI02-EGA67-DATA.                                        
                 10 COMM-AI02-GA67-CPREST PIC X(05).                            
                 10 COMM-AI02-GA67-CFOURN PIC X(05).                            
                 10 COMM-AI02-GA67-DEFFET PIC X(08).                            
                 10 FILLER                PIC X(05).                            
PH1TI-*         DONNEES OUBLIEES 1                                              
                05 COMM-AI02-GA00-AUTRE.                                        
-PH1TI              10 COMM-AI02-GA00-WRESFOURN PIC X.                          
      *        LONG ENREGITREMENT PAR TS - 60                                   
               03 COMM-AI02-LONG-ENR-TS.                                        
                  05 COMM-AI02-TS-GA51-LONG PIC S9(5).                          
                  05 COMM-AI02-TS-GA52-LONG PIC S9(5).                          
                  05 COMM-AI02-TS-GA53-LONG PIC S9(5).                          
                  05 COMM-AI02-TS-GA74-LONG PIC S9(5).                          
                  05 COMM-AI02-TS-GA54-LONG PIC S9(5).                          
                  05 COMM-AI02-TS-GA55-LONG PIC S9(5).                          
                  05 COMM-AI02-TS-GA58-LONG PIC S9(5).                          
                  05 COMM-AI02-TS-GA60-LONG PIC S9(5).                          
                  05 COMM-AI02-TS-GA61-LONG PIC S9(5).                          
                  05 COMM-AI02-TS-GA63-LONG PIC S9(5).                          
                  05 COMM-AI02-TS-GA64-LONG PIC S9(5).                          
                  05 COMM-AI02-TS-GA76-LONG PIC S9(5).                          
                  05 COMM-AI02-TS-GA98-LONG PIC S9(5).                          
      *   ZONE DE SORTIE - 79                                                   
          02 COMM-AI02-MAI02-SORTIE.                                            
                05  COMM-AI02-MESSAGE.                                          
                  10  COMM-AI02-W-PERTDSEC PIC X.                               
                  10  COMM-AI02-NCODICK PIC X(07).                              
                  10  COMM-AI02-NCODIC PIC X(07).                               
                  10  COMM-AI02-WMULTISOC PIC X(01).                            
                05  COMM-AI02-SORTIE.                                           
                  10 COMM-AI02-CODRET      PIC X.                               
                          88 COMM-AI02-OK  VALUE '1'.                           
                          88 COMM-AI02-NOK VALUE '0'.                           
                  10 COMM-AI02-LIBELLE.                                         
                     15 COMM-AI02-NPGRM PIC X(6).                               
      *              15 COMM-AI02-NSEQERR PIC X(4).                             
                     15 COMM-AI02-LIBERR PIC X(52).                             
                                                                                
