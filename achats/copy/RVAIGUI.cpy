      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE AIGUI AIGUILLAGE PAR OPTION DES PROG   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVAIGUI .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVAIGUI .                                                            
      *}                                                                        
           05  AIGUI-CTABLEG2    PIC X(15).                                     
           05  AIGUI-CTABLEG2-REDEF REDEFINES AIGUI-CTABLEG2.                   
               10  AIGUI-CSOC            PIC X(03).                             
               10  AIGUI-CPROG           PIC X(05).                             
               10  AIGUI-CFONC           PIC X(03).                             
               10  AIGUI-NOPTION         PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  AIGUI-NOPTION-N      REDEFINES AIGUI-NOPTION                 
                                         PIC 9(03).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  AIGUI-WTABLEG     PIC X(80).                                     
           05  AIGUI-WTABLEG-REDEF  REDEFINES AIGUI-WTABLEG.                    
               10  AIGUI-LCOMMENT        PIC X(25).                             
               10  AIGUI-WFLAG           PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVAIGUI-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVAIGUI-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AIGUI-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  AIGUI-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AIGUI-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  AIGUI-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
