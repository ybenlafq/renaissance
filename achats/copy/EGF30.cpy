      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF30   EGF30                                              00000020
      ***************************************************************** 00000030
       01   EGF30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(15).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFPL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCHEFPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHEFPF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCHEFPI   PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCFAMI    PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFOURNL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNFOURNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNFOURNF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNFOURNI  PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEBL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDATDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDEBF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDATDEBI  PIC X(10).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFINL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDATFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFINF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDATFINI  PIC X(10).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWSOLDEL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MWSOLDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWSOLDEF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MWSOLDEI  PIC X.                                          00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNCDEI    PIC X(7).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCLIVRL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MSOCLIVRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCLIVRF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSOCLIVRI      PIC X(3).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNDEPOTI  PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNCODICI  PIC X(7).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITRE61L      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MTITRE61L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTITRE61F      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MTITRE61I      PIC X(33).                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITRE62L      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MTITRE62L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTITRE62F      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MTITRE62I      PIC X(6).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITRE6L  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MTITRE6L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTITRE6F  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MTITRE6I  PIC X(77).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLIBERRI  PIC X(78).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCODTRAI  PIC X(4).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCICSI    PIC X(5).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MNETNAMI  PIC X(8).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MSCREENI  PIC X(4).                                       00000930
      ***************************************************************** 00000940
      * SDF: EGF30   EGF30                                              00000950
      ***************************************************************** 00000960
       01   EGF30O REDEFINES EGF30I.                                    00000970
           02 FILLER    PIC X(12).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MDATJOUA  PIC X.                                          00001000
           02 MDATJOUC  PIC X.                                          00001010
           02 MDATJOUP  PIC X.                                          00001020
           02 MDATJOUH  PIC X.                                          00001030
           02 MDATJOUV  PIC X.                                          00001040
           02 MDATJOUO  PIC X(10).                                      00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MTIMJOUA  PIC X.                                          00001070
           02 MTIMJOUC  PIC X.                                          00001080
           02 MTIMJOUP  PIC X.                                          00001090
           02 MTIMJOUH  PIC X.                                          00001100
           02 MTIMJOUV  PIC X.                                          00001110
           02 MTIMJOUO  PIC X(5).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MZONCMDA  PIC X.                                          00001140
           02 MZONCMDC  PIC X.                                          00001150
           02 MZONCMDP  PIC X.                                          00001160
           02 MZONCMDH  PIC X.                                          00001170
           02 MZONCMDV  PIC X.                                          00001180
           02 MZONCMDO  PIC X(15).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MWFONCA   PIC X.                                          00001210
           02 MWFONCC   PIC X.                                          00001220
           02 MWFONCP   PIC X.                                          00001230
           02 MWFONCH   PIC X.                                          00001240
           02 MWFONCV   PIC X.                                          00001250
           02 MWFONCO   PIC X(3).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MCHEFPA   PIC X.                                          00001280
           02 MCHEFPC   PIC X.                                          00001290
           02 MCHEFPP   PIC X.                                          00001300
           02 MCHEFPH   PIC X.                                          00001310
           02 MCHEFPV   PIC X.                                          00001320
           02 MCHEFPO   PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MCFAMA    PIC X.                                          00001350
           02 MCFAMC    PIC X.                                          00001360
           02 MCFAMP    PIC X.                                          00001370
           02 MCFAMH    PIC X.                                          00001380
           02 MCFAMV    PIC X.                                          00001390
           02 MCFAMO    PIC X(5).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MNFOURNA  PIC X.                                          00001420
           02 MNFOURNC  PIC X.                                          00001430
           02 MNFOURNP  PIC X.                                          00001440
           02 MNFOURNH  PIC X.                                          00001450
           02 MNFOURNV  PIC X.                                          00001460
           02 MNFOURNO  PIC X(5).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MDATDEBA  PIC X.                                          00001490
           02 MDATDEBC  PIC X.                                          00001500
           02 MDATDEBP  PIC X.                                          00001510
           02 MDATDEBH  PIC X.                                          00001520
           02 MDATDEBV  PIC X.                                          00001530
           02 MDATDEBO  PIC X(10).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MDATFINA  PIC X.                                          00001560
           02 MDATFINC  PIC X.                                          00001570
           02 MDATFINP  PIC X.                                          00001580
           02 MDATFINH  PIC X.                                          00001590
           02 MDATFINV  PIC X.                                          00001600
           02 MDATFINO  PIC X(10).                                      00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MWSOLDEA  PIC X.                                          00001630
           02 MWSOLDEC  PIC X.                                          00001640
           02 MWSOLDEP  PIC X.                                          00001650
           02 MWSOLDEH  PIC X.                                          00001660
           02 MWSOLDEV  PIC X.                                          00001670
           02 MWSOLDEO  PIC X.                                          00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MNCDEA    PIC X.                                          00001700
           02 MNCDEC    PIC X.                                          00001710
           02 MNCDEP    PIC X.                                          00001720
           02 MNCDEH    PIC X.                                          00001730
           02 MNCDEV    PIC X.                                          00001740
           02 MNCDEO    PIC X(7).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MSOCLIVRA      PIC X.                                     00001770
           02 MSOCLIVRC PIC X.                                          00001780
           02 MSOCLIVRP PIC X.                                          00001790
           02 MSOCLIVRH PIC X.                                          00001800
           02 MSOCLIVRV PIC X.                                          00001810
           02 MSOCLIVRO      PIC X(3).                                  00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MNDEPOTA  PIC X.                                          00001840
           02 MNDEPOTC  PIC X.                                          00001850
           02 MNDEPOTP  PIC X.                                          00001860
           02 MNDEPOTH  PIC X.                                          00001870
           02 MNDEPOTV  PIC X.                                          00001880
           02 MNDEPOTO  PIC X(3).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MNCODICA  PIC X.                                          00001910
           02 MNCODICC  PIC X.                                          00001920
           02 MNCODICP  PIC X.                                          00001930
           02 MNCODICH  PIC X.                                          00001940
           02 MNCODICV  PIC X.                                          00001950
           02 MNCODICO  PIC X(7).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MTITRE61A      PIC X.                                     00001980
           02 MTITRE61C PIC X.                                          00001990
           02 MTITRE61P PIC X.                                          00002000
           02 MTITRE61H PIC X.                                          00002010
           02 MTITRE61V PIC X.                                          00002020
           02 MTITRE61O      PIC X(33).                                 00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MTITRE62A      PIC X.                                     00002050
           02 MTITRE62C PIC X.                                          00002060
           02 MTITRE62P PIC X.                                          00002070
           02 MTITRE62H PIC X.                                          00002080
           02 MTITRE62V PIC X.                                          00002090
           02 MTITRE62O      PIC X(6).                                  00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MTITRE6A  PIC X.                                          00002120
           02 MTITRE6C  PIC X.                                          00002130
           02 MTITRE6P  PIC X.                                          00002140
           02 MTITRE6H  PIC X.                                          00002150
           02 MTITRE6V  PIC X.                                          00002160
           02 MTITRE6O  PIC X(77).                                      00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MLIBERRA  PIC X.                                          00002190
           02 MLIBERRC  PIC X.                                          00002200
           02 MLIBERRP  PIC X.                                          00002210
           02 MLIBERRH  PIC X.                                          00002220
           02 MLIBERRV  PIC X.                                          00002230
           02 MLIBERRO  PIC X(78).                                      00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MCODTRAA  PIC X.                                          00002260
           02 MCODTRAC  PIC X.                                          00002270
           02 MCODTRAP  PIC X.                                          00002280
           02 MCODTRAH  PIC X.                                          00002290
           02 MCODTRAV  PIC X.                                          00002300
           02 MCODTRAO  PIC X(4).                                       00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MCICSA    PIC X.                                          00002330
           02 MCICSC    PIC X.                                          00002340
           02 MCICSP    PIC X.                                          00002350
           02 MCICSH    PIC X.                                          00002360
           02 MCICSV    PIC X.                                          00002370
           02 MCICSO    PIC X(5).                                       00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MNETNAMA  PIC X.                                          00002400
           02 MNETNAMC  PIC X.                                          00002410
           02 MNETNAMP  PIC X.                                          00002420
           02 MNETNAMH  PIC X.                                          00002430
           02 MNETNAMV  PIC X.                                          00002440
           02 MNETNAMO  PIC X(8).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MSCREENA  PIC X.                                          00002470
           02 MSCREENC  PIC X.                                          00002480
           02 MSCREENP  PIC X.                                          00002490
           02 MSCREENH  PIC X.                                          00002500
           02 MSCREENV  PIC X.                                          00002510
           02 MSCREENO  PIC X(4).                                       00002520
                                                                                
