      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR50   EGR50                                              00000020
      ***************************************************************** 00000030
       01   EGR50I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * PAGE                                                            00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MPAGEI    PIC X(3).                                       00000200
      * NUMERO RECEPTION                                                00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRECEPL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNRECEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNRECEPF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNRECEPI  PIC X(7).                                       00000250
      * SOCIETE                                                         00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MNSOCI    PIC X(3).                                       00000300
      * LIBELLE SOCIETE                                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLSOCI    PIC X(20).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBL     COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MLIBL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MLIBF     PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MLIBI     PIC X(21).                                      00000390
      * NUM. REC. A QUAI                                                00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRECQUL  COMP PIC S9(4).                                 00000410
      *--                                                                       
           02 MNRECQUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNRECQUF  PIC X.                                          00000420
           02 FILLER    PIC X(4).                                       00000430
           02 MNRECQUI  PIC X(7).                                       00000440
      * ENTREPOT                                                        00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTRL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNENTRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNENTRF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNENTRI   PIC X(3).                                       00000490
      * LIB. ENTREPOT                                                   00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTRL   COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MLENTRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLENTRF   PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MLENTRI   PIC X(20).                                      00000540
      * DATE RECEPTION                                                  00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECEPL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MDRECEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRECEPF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MDRECEPI  PIC X(8).                                       00000590
      * LIVREUR                                                         00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIVREL  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MCLIVREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLIVREF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MCLIVREI  PIC X(5).                                       00000640
      * LIB. LIVREUR                                                    00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIVREL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLLIVREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIVREF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLLIVREI  PIC X(20).                                      00000690
      * DATE SAISIE REC.                                                00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSAIREL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MDSAIREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDSAIREF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MDSAIREI  PIC X(8).                                       00000740
      * COMMENTAIRE                                                     00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMEL  COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MLCOMMEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCOMMEF  PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MLCOMMEI  PIC X(20).                                      00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE1L   COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MNCDE1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE1F   PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MNCDE1I   PIC X(7).                                       00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE3L   COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MNCDE3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE3F   PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MNCDE3I   PIC X(7).                                       00000870
      * NUM CDE 1                                                       00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBL1L    COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MNBL1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNBL1F    PIC X.                                          00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MNBL1I    PIC X(10).                                      00000920
      * NUM CDE 3                                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBL3L    COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MNBL3L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNBL3F    PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MNBL3I    PIC X(10).                                      00000970
      * NUM CDE 2                                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE2L   COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MNCDE2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE2F   PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNCDE2I   PIC X(7).                                       00001020
      * NUM CDE 4                                                       00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDE4L   COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MNCDE4L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCDE4F   PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MNCDE4I   PIC X(7).                                       00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBL2L    COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MNBL2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNBL2F    PIC X.                                          00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MNBL2I    PIC X(10).                                      00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBL4L    COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MNBL4L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNBL4F    PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MNBL4I    PIC X(10).                                      00001150
           02 M99I OCCURS   10 TIMES .                                  00001160
      * CODIC                                                           00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00001180
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00001190
             03 FILLER  PIC X(4).                                       00001200
             03 MNCODICI     PIC X(7).                                  00001210
      * MARQUE                                                          00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MCMARQI      PIC X(5).                                  00001260
      * REFERENCE                                                       00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFERL     COMP PIC S9(4).                            00001280
      *--                                                                       
             03 MLREFERL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLREFERF     PIC X.                                     00001290
             03 FILLER  PIC X(4).                                       00001300
             03 MLREFERI     PIC X(19).                                 00001310
      * QUANTITE RECUE                                                  00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTERECL     COMP PIC S9(4).                            00001330
      *--                                                                       
             03 MQTERECL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTERECF     PIC X.                                     00001340
             03 FILLER  PIC X(4).                                       00001350
             03 MQTERECI     PIC X(5).                                  00001360
      * QUANTITE 1                                                      00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE1L  COMP PIC S9(4).                                 00001380
      *--                                                                       
             03 MQTE1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE1F  PIC X.                                          00001390
             03 FILLER  PIC X(4).                                       00001400
             03 MQTE1I  PIC X(5).                                       00001410
      * QUANTITE 2                                                      00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE2L  COMP PIC S9(4).                                 00001430
      *--                                                                       
             03 MQTE2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE2F  PIC X.                                          00001440
             03 FILLER  PIC X(4).                                       00001450
             03 MQTE2I  PIC X(5).                                       00001460
      * QUANTITE 3                                                      00001470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE3L  COMP PIC S9(4).                                 00001480
      *--                                                                       
             03 MQTE3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE3F  PIC X.                                          00001490
             03 FILLER  PIC X(4).                                       00001500
             03 MQTE3I  PIC X(5).                                       00001510
      * QUANTITE 4                                                      00001520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTE4L  COMP PIC S9(4).                                 00001530
      *--                                                                       
             03 MQTE4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQTE4F  PIC X.                                          00001540
             03 FILLER  PIC X(4).                                       00001550
             03 MQTE4I  PIC X(5).                                       00001560
      * QUANTITE LITIGES                                                00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTELITL     COMP PIC S9(4).                            00001580
      *--                                                                       
             03 MQTELITL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTELITF     PIC X.                                     00001590
             03 FILLER  PIC X(4).                                       00001600
             03 MQTELITI     PIC X(5).                                  00001610
      * ZONE CMD AIDA                                                   00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MZONCMDI  PIC X(15).                                      00001660
      * MESSAGE ERREUR                                                  00001670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001680
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001690
           02 FILLER    PIC X(4).                                       00001700
           02 MLIBERRI  PIC X(58).                                      00001710
      * CODE TRANSACTION                                                00001720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001730
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001740
           02 FILLER    PIC X(4).                                       00001750
           02 MCODTRAI  PIC X(4).                                       00001760
      * CICS DE TRAVAIL                                                 00001770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001780
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001790
           02 FILLER    PIC X(4).                                       00001800
           02 MCICSI    PIC X(5).                                       00001810
      * NETNAME                                                         00001820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001840
           02 FILLER    PIC X(4).                                       00001850
           02 MNETNAMI  PIC X(8).                                       00001860
      * CODE TERMINAL                                                   00001870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001880
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001890
           02 FILLER    PIC X(4).                                       00001900
           02 MSCREENI  PIC X(5).                                       00001910
      ***************************************************************** 00001920
      * SDF: EGR50   EGR50                                              00001930
      ***************************************************************** 00001940
       01   EGR50O REDEFINES EGR50I.                                    00001950
           02 FILLER    PIC X(12).                                      00001960
      * DATE DU JOUR                                                    00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MDATJOUA  PIC X.                                          00001990
           02 MDATJOUC  PIC X.                                          00002000
           02 MDATJOUP  PIC X.                                          00002010
           02 MDATJOUH  PIC X.                                          00002020
           02 MDATJOUV  PIC X.                                          00002030
           02 MDATJOUO  PIC X(10).                                      00002040
      * HEURE                                                           00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MTIMJOUA  PIC X.                                          00002070
           02 MTIMJOUC  PIC X.                                          00002080
           02 MTIMJOUP  PIC X.                                          00002090
           02 MTIMJOUH  PIC X.                                          00002100
           02 MTIMJOUV  PIC X.                                          00002110
           02 MTIMJOUO  PIC X(5).                                       00002120
      * PAGE                                                            00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MPAGEA    PIC X.                                          00002150
           02 MPAGEC    PIC X.                                          00002160
           02 MPAGEP    PIC X.                                          00002170
           02 MPAGEH    PIC X.                                          00002180
           02 MPAGEV    PIC X.                                          00002190
           02 MPAGEO    PIC X(3).                                       00002200
      * NUMERO RECEPTION                                                00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MNRECEPA  PIC X.                                          00002230
           02 MNRECEPC  PIC X.                                          00002240
           02 MNRECEPP  PIC X.                                          00002250
           02 MNRECEPH  PIC X.                                          00002260
           02 MNRECEPV  PIC X.                                          00002270
           02 MNRECEPO  PIC X(7).                                       00002280
      * SOCIETE                                                         00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNSOCA    PIC X.                                          00002310
           02 MNSOCC    PIC X.                                          00002320
           02 MNSOCP    PIC X.                                          00002330
           02 MNSOCH    PIC X.                                          00002340
           02 MNSOCV    PIC X.                                          00002350
           02 MNSOCO    PIC X(3).                                       00002360
      * LIBELLE SOCIETE                                                 00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MLSOCA    PIC X.                                          00002390
           02 MLSOCC    PIC X.                                          00002400
           02 MLSOCP    PIC X.                                          00002410
           02 MLSOCH    PIC X.                                          00002420
           02 MLSOCV    PIC X.                                          00002430
           02 MLSOCO    PIC X(20).                                      00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MLIBA     PIC X.                                          00002460
           02 MLIBC     PIC X.                                          00002470
           02 MLIBP     PIC X.                                          00002480
           02 MLIBH     PIC X.                                          00002490
           02 MLIBV     PIC X.                                          00002500
           02 MLIBO     PIC X(21).                                      00002510
      * NUM. REC. A QUAI                                                00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MNRECQUA  PIC X.                                          00002540
           02 MNRECQUC  PIC X.                                          00002550
           02 MNRECQUP  PIC X.                                          00002560
           02 MNRECQUH  PIC X.                                          00002570
           02 MNRECQUV  PIC X.                                          00002580
           02 MNRECQUO  PIC X(7).                                       00002590
      * ENTREPOT                                                        00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MNENTRA   PIC X.                                          00002620
           02 MNENTRC   PIC X.                                          00002630
           02 MNENTRP   PIC X.                                          00002640
           02 MNENTRH   PIC X.                                          00002650
           02 MNENTRV   PIC X.                                          00002660
           02 MNENTRO   PIC X(3).                                       00002670
      * LIB. ENTREPOT                                                   00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MLENTRA   PIC X.                                          00002700
           02 MLENTRC   PIC X.                                          00002710
           02 MLENTRP   PIC X.                                          00002720
           02 MLENTRH   PIC X.                                          00002730
           02 MLENTRV   PIC X.                                          00002740
           02 MLENTRO   PIC X(20).                                      00002750
      * DATE RECEPTION                                                  00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MDRECEPA  PIC X.                                          00002780
           02 MDRECEPC  PIC X.                                          00002790
           02 MDRECEPP  PIC X.                                          00002800
           02 MDRECEPH  PIC X.                                          00002810
           02 MDRECEPV  PIC X.                                          00002820
           02 MDRECEPO  PIC X(8).                                       00002830
      * LIVREUR                                                         00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MCLIVREA  PIC X.                                          00002860
           02 MCLIVREC  PIC X.                                          00002870
           02 MCLIVREP  PIC X.                                          00002880
           02 MCLIVREH  PIC X.                                          00002890
           02 MCLIVREV  PIC X.                                          00002900
           02 MCLIVREO  PIC X(5).                                       00002910
      * LIB. LIVREUR                                                    00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MLLIVREA  PIC X.                                          00002940
           02 MLLIVREC  PIC X.                                          00002950
           02 MLLIVREP  PIC X.                                          00002960
           02 MLLIVREH  PIC X.                                          00002970
           02 MLLIVREV  PIC X.                                          00002980
           02 MLLIVREO  PIC X(20).                                      00002990
      * DATE SAISIE REC.                                                00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MDSAIREA  PIC X.                                          00003020
           02 MDSAIREC  PIC X.                                          00003030
           02 MDSAIREP  PIC X.                                          00003040
           02 MDSAIREH  PIC X.                                          00003050
           02 MDSAIREV  PIC X.                                          00003060
           02 MDSAIREO  PIC X(8).                                       00003070
      * COMMENTAIRE                                                     00003080
           02 FILLER    PIC X(2).                                       00003090
           02 MLCOMMEA  PIC X.                                          00003100
           02 MLCOMMEC  PIC X.                                          00003110
           02 MLCOMMEP  PIC X.                                          00003120
           02 MLCOMMEH  PIC X.                                          00003130
           02 MLCOMMEV  PIC X.                                          00003140
           02 MLCOMMEO  PIC X(20).                                      00003150
           02 FILLER    PIC X(2).                                       00003160
           02 MNCDE1A   PIC X.                                          00003170
           02 MNCDE1C   PIC X.                                          00003180
           02 MNCDE1P   PIC X.                                          00003190
           02 MNCDE1H   PIC X.                                          00003200
           02 MNCDE1V   PIC X.                                          00003210
           02 MNCDE1O   PIC X(7).                                       00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MNCDE3A   PIC X.                                          00003240
           02 MNCDE3C   PIC X.                                          00003250
           02 MNCDE3P   PIC X.                                          00003260
           02 MNCDE3H   PIC X.                                          00003270
           02 MNCDE3V   PIC X.                                          00003280
           02 MNCDE3O   PIC X(7).                                       00003290
      * NUM CDE 1                                                       00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MNBL1A    PIC X.                                          00003320
           02 MNBL1C    PIC X.                                          00003330
           02 MNBL1P    PIC X.                                          00003340
           02 MNBL1H    PIC X.                                          00003350
           02 MNBL1V    PIC X.                                          00003360
           02 MNBL1O    PIC X(10).                                      00003370
      * NUM CDE 3                                                       00003380
           02 FILLER    PIC X(2).                                       00003390
           02 MNBL3A    PIC X.                                          00003400
           02 MNBL3C    PIC X.                                          00003410
           02 MNBL3P    PIC X.                                          00003420
           02 MNBL3H    PIC X.                                          00003430
           02 MNBL3V    PIC X.                                          00003440
           02 MNBL3O    PIC X(10).                                      00003450
      * NUM CDE 2                                                       00003460
           02 FILLER    PIC X(2).                                       00003470
           02 MNCDE2A   PIC X.                                          00003480
           02 MNCDE2C   PIC X.                                          00003490
           02 MNCDE2P   PIC X.                                          00003500
           02 MNCDE2H   PIC X.                                          00003510
           02 MNCDE2V   PIC X.                                          00003520
           02 MNCDE2O   PIC X(7).                                       00003530
      * NUM CDE 4                                                       00003540
           02 FILLER    PIC X(2).                                       00003550
           02 MNCDE4A   PIC X.                                          00003560
           02 MNCDE4C   PIC X.                                          00003570
           02 MNCDE4P   PIC X.                                          00003580
           02 MNCDE4H   PIC X.                                          00003590
           02 MNCDE4V   PIC X.                                          00003600
           02 MNCDE4O   PIC X(7).                                       00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MNBL2A    PIC X.                                          00003630
           02 MNBL2C    PIC X.                                          00003640
           02 MNBL2P    PIC X.                                          00003650
           02 MNBL2H    PIC X.                                          00003660
           02 MNBL2V    PIC X.                                          00003670
           02 MNBL2O    PIC X(10).                                      00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MNBL4A    PIC X.                                          00003700
           02 MNBL4C    PIC X.                                          00003710
           02 MNBL4P    PIC X.                                          00003720
           02 MNBL4H    PIC X.                                          00003730
           02 MNBL4V    PIC X.                                          00003740
           02 MNBL4O    PIC X(10).                                      00003750
           02 M99O OCCURS   10 TIMES .                                  00003760
      * CODIC                                                           00003770
             03 FILLER       PIC X(2).                                  00003780
             03 MNCODICA     PIC X.                                     00003790
             03 MNCODICC     PIC X.                                     00003800
             03 MNCODICP     PIC X.                                     00003810
             03 MNCODICH     PIC X.                                     00003820
             03 MNCODICV     PIC X.                                     00003830
             03 MNCODICO     PIC X(7).                                  00003840
      * MARQUE                                                          00003850
             03 FILLER       PIC X(2).                                  00003860
             03 MCMARQA      PIC X.                                     00003870
             03 MCMARQC PIC X.                                          00003880
             03 MCMARQP PIC X.                                          00003890
             03 MCMARQH PIC X.                                          00003900
             03 MCMARQV PIC X.                                          00003910
             03 MCMARQO      PIC X(5).                                  00003920
      * REFERENCE                                                       00003930
             03 FILLER       PIC X(2).                                  00003940
             03 MLREFERA     PIC X.                                     00003950
             03 MLREFERC     PIC X.                                     00003960
             03 MLREFERP     PIC X.                                     00003970
             03 MLREFERH     PIC X.                                     00003980
             03 MLREFERV     PIC X.                                     00003990
             03 MLREFERO     PIC X(19).                                 00004000
      * QUANTITE RECUE                                                  00004010
             03 FILLER       PIC X(2).                                  00004020
             03 MQTERECA     PIC X.                                     00004030
             03 MQTERECC     PIC X.                                     00004040
             03 MQTERECP     PIC X.                                     00004050
             03 MQTERECH     PIC X.                                     00004060
             03 MQTERECV     PIC X.                                     00004070
             03 MQTERECO     PIC X(5).                                  00004080
      * QUANTITE 1                                                      00004090
             03 FILLER       PIC X(2).                                  00004100
             03 MQTE1A  PIC X.                                          00004110
             03 MQTE1C  PIC X.                                          00004120
             03 MQTE1P  PIC X.                                          00004130
             03 MQTE1H  PIC X.                                          00004140
             03 MQTE1V  PIC X.                                          00004150
             03 MQTE1O  PIC X(5).                                       00004160
      * QUANTITE 2                                                      00004170
             03 FILLER       PIC X(2).                                  00004180
             03 MQTE2A  PIC X.                                          00004190
             03 MQTE2C  PIC X.                                          00004200
             03 MQTE2P  PIC X.                                          00004210
             03 MQTE2H  PIC X.                                          00004220
             03 MQTE2V  PIC X.                                          00004230
             03 MQTE2O  PIC X(5).                                       00004240
      * QUANTITE 3                                                      00004250
             03 FILLER       PIC X(2).                                  00004260
             03 MQTE3A  PIC X.                                          00004270
             03 MQTE3C  PIC X.                                          00004280
             03 MQTE3P  PIC X.                                          00004290
             03 MQTE3H  PIC X.                                          00004300
             03 MQTE3V  PIC X.                                          00004310
             03 MQTE3O  PIC X(5).                                       00004320
      * QUANTITE 4                                                      00004330
             03 FILLER       PIC X(2).                                  00004340
             03 MQTE4A  PIC X.                                          00004350
             03 MQTE4C  PIC X.                                          00004360
             03 MQTE4P  PIC X.                                          00004370
             03 MQTE4H  PIC X.                                          00004380
             03 MQTE4V  PIC X.                                          00004390
             03 MQTE4O  PIC X(5).                                       00004400
      * QUANTITE LITIGES                                                00004410
             03 FILLER       PIC X(2).                                  00004420
             03 MQTELITA     PIC X.                                     00004430
             03 MQTELITC     PIC X.                                     00004440
             03 MQTELITP     PIC X.                                     00004450
             03 MQTELITH     PIC X.                                     00004460
             03 MQTELITV     PIC X.                                     00004470
             03 MQTELITO     PIC X(5).                                  00004480
      * ZONE CMD AIDA                                                   00004490
           02 FILLER    PIC X(2).                                       00004500
           02 MZONCMDA  PIC X.                                          00004510
           02 MZONCMDC  PIC X.                                          00004520
           02 MZONCMDP  PIC X.                                          00004530
           02 MZONCMDH  PIC X.                                          00004540
           02 MZONCMDV  PIC X.                                          00004550
           02 MZONCMDO  PIC X(15).                                      00004560
      * MESSAGE ERREUR                                                  00004570
           02 FILLER    PIC X(2).                                       00004580
           02 MLIBERRA  PIC X.                                          00004590
           02 MLIBERRC  PIC X.                                          00004600
           02 MLIBERRP  PIC X.                                          00004610
           02 MLIBERRH  PIC X.                                          00004620
           02 MLIBERRV  PIC X.                                          00004630
           02 MLIBERRO  PIC X(58).                                      00004640
      * CODE TRANSACTION                                                00004650
           02 FILLER    PIC X(2).                                       00004660
           02 MCODTRAA  PIC X.                                          00004670
           02 MCODTRAC  PIC X.                                          00004680
           02 MCODTRAP  PIC X.                                          00004690
           02 MCODTRAH  PIC X.                                          00004700
           02 MCODTRAV  PIC X.                                          00004710
           02 MCODTRAO  PIC X(4).                                       00004720
      * CICS DE TRAVAIL                                                 00004730
           02 FILLER    PIC X(2).                                       00004740
           02 MCICSA    PIC X.                                          00004750
           02 MCICSC    PIC X.                                          00004760
           02 MCICSP    PIC X.                                          00004770
           02 MCICSH    PIC X.                                          00004780
           02 MCICSV    PIC X.                                          00004790
           02 MCICSO    PIC X(5).                                       00004800
      * NETNAME                                                         00004810
           02 FILLER    PIC X(2).                                       00004820
           02 MNETNAMA  PIC X.                                          00004830
           02 MNETNAMC  PIC X.                                          00004840
           02 MNETNAMP  PIC X.                                          00004850
           02 MNETNAMH  PIC X.                                          00004860
           02 MNETNAMV  PIC X.                                          00004870
           02 MNETNAMO  PIC X(8).                                       00004880
      * CODE TERMINAL                                                   00004890
           02 FILLER    PIC X(2).                                       00004900
           02 MSCREENA  PIC X.                                          00004910
           02 MSCREENC  PIC X.                                          00004920
           02 MSCREENP  PIC X.                                          00004930
           02 MSCREENH  PIC X.                                          00004940
           02 MSCREENV  PIC X.                                          00004950
           02 MSCREENO  PIC X(5).                                       00004960
                                                                                
