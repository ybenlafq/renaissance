      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TGR30 (TGR00 -> MENU)    TR: GR00  *    00002222
      *               GESTION DES QUOTAS DE RECEPTION              *    00002321
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3710  00009000
      *                                                                 00410100
      *        TRANSACTION GR30 : CREATION RECEPTION FOURNISSEUR   *    00411022
      *                                                                 00412000
          02 COMM-GR30-APPLI REDEFINES COMM-GR00-APPLI.                 00420022
      *------------------------------ ZONE DONNEES TGR30                00520124
             03 COMM-GR30-DONNEES-TGR30.                                00520224
      *------------------------------ CODE FONCTION                     00521018
                04 COMM-GR30-FONCT          PIC XXX.                    00522026
      *------------------------------ CODE SOCIETE                      00550009
                04 COMM-GR30-NSOCIETE       PIC XXX.                    00560026
      *------------------------------ LIBELLE SOCIETE                   00570009
                04 COMM-GR30-LSOCIETE       PIC X(20).                  00580026
      *------------------------------ CODE DEPOT                        00590020
                04 COMM-GR30-NDEPOT         PIC XXX.                    00600026
      *------------------------------ LIBELLE DEPOT                     00610020
                04 COMM-GR30-LDEPOT         PIC X(20).                  00620026
      *------------------------------ NO DE RECEPTION A QUAI            00630023
             03 COMM-GR32-NRECQUAI          PIC X(07).                  00640023
      *------------------------------ DATE DE RECEPTION A QUAI          00641026
             03 COMM-GR32-DJRECQUAI         PIC X(08).                  00642026
      *------------------------------ COMMENTAIRE                       00643026
             03 COMM-GR32-LCOMMENT          PIC X(20).                  00644026
      *------------------------------ CODE LIVREUR                      00645026
             03 COMM-GR32-CLIVR             PIC X(05).                  00646026
      *------------------------------ LIBELLE LIVREUR                   00647026
             03 COMM-GR32-LLIVR             PIC X(20).                  00648026
      *------------------------------ TOP PAGE-VIDE                     00648129
             03 COMM-GR30-VIDE              PIC X(01).                  00648229
      *------------------------------ ZONE DE MESSAGE                   00649027
             03 COMM-GR30-MESS              PIC X(50).                  00649127
      *------------------------------ NO DE RECEPTION A QUAI            00650023
             03 COMM-GR30-ZONEPAGINATION OCCURS 100.                    00660023
                04 COMM-GR30-NORECQUAI      PIC X(07).                  00670026
                04 COMM-GR30-DJRECQUAI      PIC X(08).                  00670027
      *------------------------------ NO DE REC ADM DE LA PAGE EN COURS 00650023
             03 COMM-GR30-NRECPAGE       OCCURS 12.                     00660023
                04 COMM-GR30-NREC           PIC X(07).                  00670026
      *------------------------------ ZONE LIBRE                        00739919
             03 COMM-GR30-LIBRE             PIC X(1966).                00740029
      ***************************************************************** 00750000
                                                                                
