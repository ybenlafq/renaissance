      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010001
      * COMMAREA SPECIFIQUE PRG TGR50 (TGR00 -> MENU)    TR: GR00  *    00020001
      *               GESTION DES RECEPTIONS                       *    00030001
      *                                                                 00040001
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3710  00050001
      *                                                                 00060001
      *    TRANSACTION GR50 : GESTION DES RECEPTIONS FOURNISSEUR   *    00070001
      *                              OPTION INTERROGATION          *    00080001
      *                                                                 00090001
          02 COMM-GR50-APPLI REDEFINES COMM-GR00-APPLI.                 00100001
      *------------------------------ DONNEES TGR50 COMMUNES AU MENU    00110001
             03 COMM-GR50-DONNEES-TGR50.                                00120001
      *------------------------------ CODE FONCTION                     00130001
                04 COMM-GR50-FONCT             PIC XXX.                 00140001
      *------------------------------ CODE RECEPTION A QUAI             00150001
                04 COMM-GR50-NRECQUAI          PIC X(07).               00160001
      *------------------------------ DATE JOUR DE RECEPTION A QUAI     00170001
                04 COMM-GR50-DJRECQUAI         PIC X(08).               00180001
      *------------------------------ CODE RECEPTION ADMINISTRATIVE     00190001
                04 COMM-GR50-NREC              PIC X(07).               00200001
      *------------------------------ DATE JOUR DE RECEPTION ADM.       00210001
                04 COMM-GR50-DSAISREC          PIC X(08).               00220001
      *------------------------------ LIBELLE COMMENTAIRE               00230001
                04 COMM-GR50-LCOMMENT          PIC X(20).               00240001
      *------------------------------ CODE LIVREUR                      00250001
                04 COMM-GR50-CLIVR             PIC X(05).               00260001
      *------------------------------ LIBELLE LIVREUR                   00270001
                04 COMM-GR50-LLIVR             PIC X(20).               00280001
      *------------------------------ CODE SOCIETE                      00290001
                04 COMM-GR50-NSOCLIVR          PIC X(03).               00300001
      *------------------------------ CODE ENTREPOT                     00310001
                04 COMM-GR50-NDEPOT            PIC X(03).               00320001
      *------------------------------ LIBELLE SOCIETE                   00330001
                04 COMM-GR50-LSOCLIVR          PIC X(20).               00340001
      *------------------------------ LIBELLE ENTREPOT                  00350001
                04 COMM-GR50-LDEPOT            PIC X(20).               00360001
      *------------------------------ DONNEES PROPRES A TGR50           00370001
             03 COMM-GR50-DONNEES.                                      00380001
      *------------------------------ PAGINATION HAUT-BAS               00390001
                04 COMM-GR50-PAGE-LATERALE.                             00400001
      *------------------------------ EXISTENCE TS PAGINATION           00410001
                   05 COMM-GR50-EXISTS         PIC X(1).                00420001
      *------------------------------ NUMERO ITEM PAGE EN COURS         00430001
                   05 COMM-GR50-LATE-COURS     PIC S9(3) COMP-3.        00440001
      *------------------------------ DERNIER ITEM ECRIT                00450001
                   05 COMM-GR50-LATE-DERNIER   PIC S9(3) COMP-3.        00460001
      *------------------------------ PAGINATION DROITE-GAUCHE          00470001
                04 COMM-GR50-PAGE-HORIZONTALE.                          00480001
      *------------------------------ NUMERO ITEM PAGE EN COURS         00490001
                   05 COMM-GR50-HORI-COURS     PIC S9(3) COMP-3.        00500001
      *------------------------------ NUMERO ITEM PAGE PRECEDENTE       00510001
                   05 COMM-GR50-HORI-PREC      PIC S9(3) COMP-3.        00520001
      *------------------------------ NUMERO ITEM PAGE SUIVANTE         00530001
                   05 COMM-GR50-HORI-SUIV      PIC S9(3) COMP-3.        00540001
      *------------------------------ DERNIER ITEM ECRIT                00550001
                   05 COMM-GR50-HORI-DERNIER   PIC S9(3) COMP-3.        00560001
      *------------------------------ NUMERO PAGE EN COURS              00570001
                   05 COMM-GR50-HORI-PAGE      PIC S9(3) COMP-3.        00580001
      *------------------------------ ZONE LIBRE                        00590001
             03 COMM-GR50-LIBRE          PIC X(3564).                   00600001
      ***************************************************************** 00610001
                                                                                
