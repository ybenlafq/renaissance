      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR09   EGR09                                              00000020
      ***************************************************************** 00000030
       01   EGR09I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPAGESI      PIC X(4).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPOTL    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCDEPOTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSOCDEPOTF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCDEPOTI    PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNDEPOTI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEPOTL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLDEPOTF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLDEPOTI  PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNENTCDEL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MSNENTCDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSNENTCDEF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSNENTCDEI     PIC X(5).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLENTCDEL     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MSLENTCDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSLENTCDEF     PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MSLENTCDEI     PIC X(20).                                 00000410
           02 MLIGNEI OCCURS   13 TIMES .                               00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBATTENTEL  COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNBATTENTEL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNBATTENTEF  PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNBATTENTEI  PIC X(5).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBCTRLESL   COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNBCTRLESL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNBCTRLESF   PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNBCTRLESI   PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBRANGESL   COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNBRANGESL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNBRANGESF   PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNBRANGESI   PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDOSLM6L    COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNDOSLM6L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNDOSLM6F    PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNDOSLM6I    PIC X(8).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENTCDEL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MLENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLENTCDEF    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MLENTCDEI    PIC X(20).                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNRECQUAIL   COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MNRECQUAIL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNRECQUAIF   PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNRECQUAII   PIC X(7).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBSUPL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNBSUPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNBSUPF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNBSUPI      PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBCODICL    COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MNBCODICL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNBCODICF    PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MNBCODICI    PIC X(5).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJRECQUAIL  COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MDJRECQUAIL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MDJRECQUAIF  PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MDJRECQUAII  PIC X(4).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTCDEL    COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MNENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNENTCDEF    PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNENTCDEI    PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(73).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCICSI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNETNAMI  PIC X(8).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MSCREENI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNRECQUAIL    COMP PIC S9(4).                            00001030
      *--                                                                       
           02 MSNRECQUAIL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MSNRECQUAIF    PIC X.                                     00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSNRECQUAII    PIC X(7).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNDOSLM6L     COMP PIC S9(4).                            00001070
      *--                                                                       
           02 MSNDOSLM6L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MSNDOSLM6F     PIC X.                                     00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MSNDOSLM6I     PIC X(8).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDJRECQUAIL   COMP PIC S9(4).                            00001110
      *--                                                                       
           02 MSDJRECQUAIL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MSDJRECQUAIF   PIC X.                                     00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSDJRECQUAII   PIC X(4).                                  00001140
      ***************************************************************** 00001150
      * SDF: EGR09   EGR09                                              00001160
      ***************************************************************** 00001170
       01   EGR09O REDEFINES EGR09I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MPAGEA    PIC X.                                          00001350
           02 MPAGEC    PIC X.                                          00001360
           02 MPAGEP    PIC X.                                          00001370
           02 MPAGEH    PIC X.                                          00001380
           02 MPAGEV    PIC X.                                          00001390
           02 MPAGEO    PIC X(4).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MNBPAGESA      PIC X.                                     00001420
           02 MNBPAGESC PIC X.                                          00001430
           02 MNBPAGESP PIC X.                                          00001440
           02 MNBPAGESH PIC X.                                          00001450
           02 MNBPAGESV PIC X.                                          00001460
           02 MNBPAGESO      PIC X(4).                                  00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MNSOCDEPOTA    PIC X.                                     00001490
           02 MNSOCDEPOTC    PIC X.                                     00001500
           02 MNSOCDEPOTP    PIC X.                                     00001510
           02 MNSOCDEPOTH    PIC X.                                     00001520
           02 MNSOCDEPOTV    PIC X.                                     00001530
           02 MNSOCDEPOTO    PIC X(3).                                  00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MNDEPOTA  PIC X.                                          00001560
           02 MNDEPOTC  PIC X.                                          00001570
           02 MNDEPOTP  PIC X.                                          00001580
           02 MNDEPOTH  PIC X.                                          00001590
           02 MNDEPOTV  PIC X.                                          00001600
           02 MNDEPOTO  PIC X(3).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MLDEPOTA  PIC X.                                          00001630
           02 MLDEPOTC  PIC X.                                          00001640
           02 MLDEPOTP  PIC X.                                          00001650
           02 MLDEPOTH  PIC X.                                          00001660
           02 MLDEPOTV  PIC X.                                          00001670
           02 MLDEPOTO  PIC X(20).                                      00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MSNENTCDEA     PIC X.                                     00001700
           02 MSNENTCDEC     PIC X.                                     00001710
           02 MSNENTCDEP     PIC X.                                     00001720
           02 MSNENTCDEH     PIC X.                                     00001730
           02 MSNENTCDEV     PIC X.                                     00001740
           02 MSNENTCDEO     PIC X(5).                                  00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MSLENTCDEA     PIC X.                                     00001770
           02 MSLENTCDEC     PIC X.                                     00001780
           02 MSLENTCDEP     PIC X.                                     00001790
           02 MSLENTCDEH     PIC X.                                     00001800
           02 MSLENTCDEV     PIC X.                                     00001810
           02 MSLENTCDEO     PIC X(20).                                 00001820
           02 MLIGNEO OCCURS   13 TIMES .                               00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MNBATTENTEA  PIC X.                                     00001850
             03 MNBATTENTEC  PIC X.                                     00001860
             03 MNBATTENTEP  PIC X.                                     00001870
             03 MNBATTENTEH  PIC X.                                     00001880
             03 MNBATTENTEV  PIC X.                                     00001890
             03 MNBATTENTEO  PIC X(5).                                  00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MNBCTRLESA   PIC X.                                     00001920
             03 MNBCTRLESC   PIC X.                                     00001930
             03 MNBCTRLESP   PIC X.                                     00001940
             03 MNBCTRLESH   PIC X.                                     00001950
             03 MNBCTRLESV   PIC X.                                     00001960
             03 MNBCTRLESO   PIC X(5).                                  00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MNBRANGESA   PIC X.                                     00001990
             03 MNBRANGESC   PIC X.                                     00002000
             03 MNBRANGESP   PIC X.                                     00002010
             03 MNBRANGESH   PIC X.                                     00002020
             03 MNBRANGESV   PIC X.                                     00002030
             03 MNBRANGESO   PIC X(5).                                  00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MNDOSLM6A    PIC X.                                     00002060
             03 MNDOSLM6C    PIC X.                                     00002070
             03 MNDOSLM6P    PIC X.                                     00002080
             03 MNDOSLM6H    PIC X.                                     00002090
             03 MNDOSLM6V    PIC X.                                     00002100
             03 MNDOSLM6O    PIC X(8).                                  00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MLENTCDEA    PIC X.                                     00002130
             03 MLENTCDEC    PIC X.                                     00002140
             03 MLENTCDEP    PIC X.                                     00002150
             03 MLENTCDEH    PIC X.                                     00002160
             03 MLENTCDEV    PIC X.                                     00002170
             03 MLENTCDEO    PIC X(20).                                 00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MNRECQUAIA   PIC X.                                     00002200
             03 MNRECQUAIC   PIC X.                                     00002210
             03 MNRECQUAIP   PIC X.                                     00002220
             03 MNRECQUAIH   PIC X.                                     00002230
             03 MNRECQUAIV   PIC X.                                     00002240
             03 MNRECQUAIO   PIC X(7).                                  00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MNBSUPA      PIC X.                                     00002270
             03 MNBSUPC PIC X.                                          00002280
             03 MNBSUPP PIC X.                                          00002290
             03 MNBSUPH PIC X.                                          00002300
             03 MNBSUPV PIC X.                                          00002310
             03 MNBSUPO      PIC X(5).                                  00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MNBCODICA    PIC X.                                     00002340
             03 MNBCODICC    PIC X.                                     00002350
             03 MNBCODICP    PIC X.                                     00002360
             03 MNBCODICH    PIC X.                                     00002370
             03 MNBCODICV    PIC X.                                     00002380
             03 MNBCODICO    PIC X(5).                                  00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MDJRECQUAIA  PIC X.                                     00002410
             03 MDJRECQUAIC  PIC X.                                     00002420
             03 MDJRECQUAIP  PIC X.                                     00002430
             03 MDJRECQUAIH  PIC X.                                     00002440
             03 MDJRECQUAIV  PIC X.                                     00002450
             03 MDJRECQUAIO  PIC X(4).                                  00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MNENTCDEA    PIC X.                                     00002480
             03 MNENTCDEC    PIC X.                                     00002490
             03 MNENTCDEP    PIC X.                                     00002500
             03 MNENTCDEH    PIC X.                                     00002510
             03 MNENTCDEV    PIC X.                                     00002520
             03 MNENTCDEO    PIC X(5).                                  00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MLIBERRA  PIC X.                                          00002550
           02 MLIBERRC  PIC X.                                          00002560
           02 MLIBERRP  PIC X.                                          00002570
           02 MLIBERRH  PIC X.                                          00002580
           02 MLIBERRV  PIC X.                                          00002590
           02 MLIBERRO  PIC X(73).                                      00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MCODTRAA  PIC X.                                          00002620
           02 MCODTRAC  PIC X.                                          00002630
           02 MCODTRAP  PIC X.                                          00002640
           02 MCODTRAH  PIC X.                                          00002650
           02 MCODTRAV  PIC X.                                          00002660
           02 MCODTRAO  PIC X(4).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MCICSA    PIC X.                                          00002690
           02 MCICSC    PIC X.                                          00002700
           02 MCICSP    PIC X.                                          00002710
           02 MCICSH    PIC X.                                          00002720
           02 MCICSV    PIC X.                                          00002730
           02 MCICSO    PIC X(5).                                       00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MNETNAMA  PIC X.                                          00002760
           02 MNETNAMC  PIC X.                                          00002770
           02 MNETNAMP  PIC X.                                          00002780
           02 MNETNAMH  PIC X.                                          00002790
           02 MNETNAMV  PIC X.                                          00002800
           02 MNETNAMO  PIC X(8).                                       00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MSCREENA  PIC X.                                          00002830
           02 MSCREENC  PIC X.                                          00002840
           02 MSCREENP  PIC X.                                          00002850
           02 MSCREENH  PIC X.                                          00002860
           02 MSCREENV  PIC X.                                          00002870
           02 MSCREENO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MSNRECQUAIA    PIC X.                                     00002900
           02 MSNRECQUAIC    PIC X.                                     00002910
           02 MSNRECQUAIP    PIC X.                                     00002920
           02 MSNRECQUAIH    PIC X.                                     00002930
           02 MSNRECQUAIV    PIC X.                                     00002940
           02 MSNRECQUAIO    PIC X(7).                                  00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MSNDOSLM6A     PIC X.                                     00002970
           02 MSNDOSLM6C     PIC X.                                     00002980
           02 MSNDOSLM6P     PIC X.                                     00002990
           02 MSNDOSLM6H     PIC X.                                     00003000
           02 MSNDOSLM6V     PIC X.                                     00003010
           02 MSNDOSLM6O     PIC X(8).                                  00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSDJRECQUAIA   PIC X.                                     00003040
           02 MSDJRECQUAIC   PIC X.                                     00003050
           02 MSDJRECQUAIP   PIC X.                                     00003060
           02 MSDJRECQUAIH   PIC X.                                     00003070
           02 MSDJRECQUAIV   PIC X.                                     00003080
           02 MSDJRECQUAIO   PIC X(4).                                  00003090
                                                                                
