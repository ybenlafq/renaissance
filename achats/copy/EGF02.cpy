      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF02   EGF02                                              00000020
      ***************************************************************** 00000030
       01   EGF02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFPL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCHEFPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHEFPF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCHEFPI   PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDEPL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSOCDEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCDEPF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSOCDEPI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDEPOTI   PIC X(3).                                       00000290
           02 M106I OCCURS   10 TIMES .                                 00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MNCODICI     PIC X(7).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MLREFI  PIC X(20).                                      00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSOU1L      COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MQSOU1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQSOU1F      PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MQSOU1I      PIC X(5).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDSOU1L      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MDSOU1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDSOU1F      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MDSOU1I      PIC X(8).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSOU2L      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MQSOU2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQSOU2F      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MQSOU2I      PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDSOU2L      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MDSOU2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDSOU2F      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MDSOU2I      PIC X(8).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSOU3L      COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MQSOU3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQSOU3F      PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MQSOU3I      PIC X(5).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDSOU3L      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MDSOU3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDSOU3F      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MDSOU3I      PIC X(8).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MZONCMDI  PIC X(15).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLIBERRI  PIC X(58).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCICSI    PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNETNAMI  PIC X(8).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MSCREENI  PIC X(4).                                       00000860
      ***************************************************************** 00000870
      * SDF: EGF02   EGF02                                              00000880
      ***************************************************************** 00000890
       01   EGF02O REDEFINES EGF02I.                                    00000900
           02 FILLER    PIC X(12).                                      00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MDATJOUA  PIC X.                                          00000930
           02 MDATJOUC  PIC X.                                          00000940
           02 MDATJOUP  PIC X.                                          00000950
           02 MDATJOUH  PIC X.                                          00000960
           02 MDATJOUV  PIC X.                                          00000970
           02 MDATJOUO  PIC X(10).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MTIMJOUA  PIC X.                                          00001000
           02 MTIMJOUC  PIC X.                                          00001010
           02 MTIMJOUP  PIC X.                                          00001020
           02 MTIMJOUH  PIC X.                                          00001030
           02 MTIMJOUV  PIC X.                                          00001040
           02 MTIMJOUO  PIC X(5).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MWFONCA   PIC X.                                          00001070
           02 MWFONCC   PIC X.                                          00001080
           02 MWFONCP   PIC X.                                          00001090
           02 MWFONCH   PIC X.                                          00001100
           02 MWFONCV   PIC X.                                          00001110
           02 MWFONCO   PIC X(3).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MCHEFPA   PIC X.                                          00001140
           02 MCHEFPC   PIC X.                                          00001150
           02 MCHEFPP   PIC X.                                          00001160
           02 MCHEFPH   PIC X.                                          00001170
           02 MCHEFPV   PIC X.                                          00001180
           02 MCHEFPO   PIC X(5).                                       00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MSOCDEPA  PIC X.                                          00001210
           02 MSOCDEPC  PIC X.                                          00001220
           02 MSOCDEPP  PIC X.                                          00001230
           02 MSOCDEPH  PIC X.                                          00001240
           02 MSOCDEPV  PIC X.                                          00001250
           02 MSOCDEPO  PIC X(3).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MDEPOTA   PIC X.                                          00001280
           02 MDEPOTC   PIC X.                                          00001290
           02 MDEPOTP   PIC X.                                          00001300
           02 MDEPOTH   PIC X.                                          00001310
           02 MDEPOTV   PIC X.                                          00001320
           02 MDEPOTO   PIC X(3).                                       00001330
           02 M106O OCCURS   10 TIMES .                                 00001340
             03 FILLER       PIC X(2).                                  00001350
             03 MNCODICA     PIC X.                                     00001360
             03 MNCODICC     PIC X.                                     00001370
             03 MNCODICP     PIC X.                                     00001380
             03 MNCODICH     PIC X.                                     00001390
             03 MNCODICV     PIC X.                                     00001400
             03 MNCODICO     PIC X(7).                                  00001410
             03 FILLER       PIC X(2).                                  00001420
             03 MLREFA  PIC X.                                          00001430
             03 MLREFC  PIC X.                                          00001440
             03 MLREFP  PIC X.                                          00001450
             03 MLREFH  PIC X.                                          00001460
             03 MLREFV  PIC X.                                          00001470
             03 MLREFO  PIC X(20).                                      00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MQSOU1A      PIC X.                                     00001500
             03 MQSOU1C PIC X.                                          00001510
             03 MQSOU1P PIC X.                                          00001520
             03 MQSOU1H PIC X.                                          00001530
             03 MQSOU1V PIC X.                                          00001540
             03 MQSOU1O      PIC X(5).                                  00001550
             03 FILLER       PIC X(2).                                  00001560
             03 MDSOU1A      PIC X.                                     00001570
             03 MDSOU1C PIC X.                                          00001580
             03 MDSOU1P PIC X.                                          00001590
             03 MDSOU1H PIC X.                                          00001600
             03 MDSOU1V PIC X.                                          00001610
             03 MDSOU1O      PIC X(8).                                  00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MQSOU2A      PIC X.                                     00001640
             03 MQSOU2C PIC X.                                          00001650
             03 MQSOU2P PIC X.                                          00001660
             03 MQSOU2H PIC X.                                          00001670
             03 MQSOU2V PIC X.                                          00001680
             03 MQSOU2O      PIC X(5).                                  00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MDSOU2A      PIC X.                                     00001710
             03 MDSOU2C PIC X.                                          00001720
             03 MDSOU2P PIC X.                                          00001730
             03 MDSOU2H PIC X.                                          00001740
             03 MDSOU2V PIC X.                                          00001750
             03 MDSOU2O      PIC X(8).                                  00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MQSOU3A      PIC X.                                     00001780
             03 MQSOU3C PIC X.                                          00001790
             03 MQSOU3P PIC X.                                          00001800
             03 MQSOU3H PIC X.                                          00001810
             03 MQSOU3V PIC X.                                          00001820
             03 MQSOU3O      PIC X(5).                                  00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MDSOU3A      PIC X.                                     00001850
             03 MDSOU3C PIC X.                                          00001860
             03 MDSOU3P PIC X.                                          00001870
             03 MDSOU3H PIC X.                                          00001880
             03 MDSOU3V PIC X.                                          00001890
             03 MDSOU3O      PIC X(8).                                  00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MZONCMDA  PIC X.                                          00001920
           02 MZONCMDC  PIC X.                                          00001930
           02 MZONCMDP  PIC X.                                          00001940
           02 MZONCMDH  PIC X.                                          00001950
           02 MZONCMDV  PIC X.                                          00001960
           02 MZONCMDO  PIC X(15).                                      00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLIBERRA  PIC X.                                          00001990
           02 MLIBERRC  PIC X.                                          00002000
           02 MLIBERRP  PIC X.                                          00002010
           02 MLIBERRH  PIC X.                                          00002020
           02 MLIBERRV  PIC X.                                          00002030
           02 MLIBERRO  PIC X(58).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCODTRAA  PIC X.                                          00002060
           02 MCODTRAC  PIC X.                                          00002070
           02 MCODTRAP  PIC X.                                          00002080
           02 MCODTRAH  PIC X.                                          00002090
           02 MCODTRAV  PIC X.                                          00002100
           02 MCODTRAO  PIC X(4).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCICSA    PIC X.                                          00002130
           02 MCICSC    PIC X.                                          00002140
           02 MCICSP    PIC X.                                          00002150
           02 MCICSH    PIC X.                                          00002160
           02 MCICSV    PIC X.                                          00002170
           02 MCICSO    PIC X(5).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNETNAMA  PIC X.                                          00002200
           02 MNETNAMC  PIC X.                                          00002210
           02 MNETNAMP  PIC X.                                          00002220
           02 MNETNAMH  PIC X.                                          00002230
           02 MNETNAMV  PIC X.                                          00002240
           02 MNETNAMO  PIC X(8).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MSCREENA  PIC X.                                          00002270
           02 MSCREENC  PIC X.                                          00002280
           02 MSCREENP  PIC X.                                          00002290
           02 MSCREENH  PIC X.                                          00002300
           02 MSCREENV  PIC X.                                          00002310
           02 MSCREENO  PIC X(4).                                       00002320
                                                                                
