      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00001000
      * COMMAREA SPECIFIQUE PRG TGR09 (TGR00 -> MENU)    TR: GR00  *    00002001
      *               GESTION DES RECEPTIONS                       *    00003000
      *                                                                 00004000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3710  00005000
      *                                                                 00006000
      *        TRANSACTION GR09 : TABLEAU D'AVANCEMENT             *    00007001
      *                           DES "RANGEMENTS" EN ATTENTE      *    00007100
      *                                                                 00007200
          03 COMM-GR09-APPLI REDEFINES COMM-GR01-APPLI.                 00007301
      *------------------------------ ZONE DONNEES TGR09                00007401
             04 COMM-GR09-DONNEES-TGR09.                                00007501
                05 COMM-GR09-NSOCIETE          PIC XXX.                 00007601
                05 COMM-GR09-LSOCIETE          PIC X(20).               00007701
                05 COMM-GR09-NDEPOT            PIC XXX.                 00007801
                05 COMM-GR09-LDEPOT            PIC X(20).               00007901
                05 COMM-GR09-SDJRECQUAI        PIC X(4).                00008002
                05 COMM-GR09-SNRECQUAI         PIC X(7).                00009002
                05 COMM-GR09-SNRECQORIG        PIC X(7).                00009102
                05 COMM-GR09-SNDOSLM6          PIC X(8).                00010005
                05 COMM-GR09-SNENTCDE          PIC X(5).                00010104
                05 COMM-GR09-SLENTCDE          PIC X(20).               00010204
                05 COMM-GR09-DD-SELECTION      PIC X(8).                00010304
                05 COMM-GR09-TSLONG            PIC 9(1).                00011001
                   88 TS09-PAS-TROP-LONGUE           VALUE 1.           00012001
                   88 TS09-TROP-LONGUE               VALUE 2.           00013001
                05 COMM-GR09-DEPOT-LM          PIC 9(1).                00014001
                   88 COMM-GR09-DEPOT-LM6            VALUE 1.           00015001
                   88 COMM-GR09-DEPOT-LM7            VALUE 2.           00016001
                05 COMM-GR09-NBLIGNES          PIC 9(5).                00017001
                05 COMM-GR09-NBPRODUITS        PIC 9(5).                00018001
                05 COMM-GR09-NBPIECES          PIC 9(5).                00019001
                05 COMM-GR09-PAGE              PIC 9(4).                00020001
                05 COMM-GR09-NBPAGES           PIC 9(4).                00030001
AA0415          05 COMM-GR09-PAGEENC           PIC 9(4).                00030108
AA0415          05 FILLER                      PIC X(196).              00031008
             04 COMM-GR09-LIBRE                PIC X(3400).             00040007
      ***************************************************************** 00050000
                                                                                
