      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00010004
      * COMMAREA SPECIFIQUE PRG TGF17                     TR: GF17      00020004
      *           TR: GF00 "ECHEANCEMENT LIVRAISONS"                    00030004
      *************************************************SOIT 7421C.**    00040004
          02 COMM-GF17-APPLI REDEFINES COMM-GF00-APPLI.                 00050004
      *------------------------------ PARTIE COMMUNE                    00060009
             03 COMM-GF17-PAGE           PIC 9(2).                      00070004
             03 COMM-GF17-PAGE-MAX       PIC 9(2).                      00080004
             03 COMM-GF17-COLS           PIC 9(2).                      00090004
      *--                                                      006C     00100009
           03  VERTICAL-88 PIC X.                                       00110009
               88  PAS-VERTICAL VALUE '0'.                              00120004
               88  VERTICAL     VALUE '1'.                              00130004
      *--                                                      007C     00140009
           03  HORIZONTAL-88 PIC X.                                     00150004
               88  PAS-HORIZONTAL VALUE '0'.                            00160004
               88  HORIZONTAL     VALUE '1'.                            00170004
      *------                                                  008C     00180009
             03 COMM-GF17-RANG-TS-HV.                                   00190004
                04 COMM-GF17-RANG-TS      PIC S9(4).                    00200007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COMM-GF17-I-RELIQUAT     PIC S9(4) COMP.                00210004
      *--                                                                       
             03 COMM-GF17-I-RELIQUAT     PIC S9(4) COMP-5.                      
      *}                                                                        
             03 COMM-GF17-1ERE-LIGNE     PIC S9(03) COMP-3.             00220004
             03 COMM-GF17-IL-MAX         PIC S9(03) COMP-3.             00230004
             03 COMM-GF17-IND-Q          PIC S9(03) COMP-3.             00230004
             03 COMM-GF17-IND-MAX        PIC S9(03) COMP-3.             00230004
      *------                                                  023C     00240009
             03 COMM-GF17-DLIVRAISONS.                                  00250004
                04 COMM-GF17-POSTE OCCURS 20 TIMES.                     00270005
                  05 COMM-GF17-DLIVRAISON.                              00290005
                     06 SS              PIC X(02).                      00300005
                     06 AA              PIC X(02).                      00310005
                     06 MM              PIC X(02).                      00320005
                     06 JJ              PIC X(02).                      00330005
                  05 COMM-GF17-SSAAR  REDEFINES   COMM-GF17-DLIVRAISON. 00340005
                     06 COMM-GF17-SSAA        PIC X(04).                     003
                     06 COMM-GF17-MMAA        PIC X(02).                     003
                     06 COMM-GF17-JJAA        PIC X(02).                     003
      *                                                                 00380004
                  05 COMM-GF17-ANNEE-SEMAINE.                              00390
                     06 COMM-GF17-SSANNEE     PIC X(04).                   00400
                     06 COMM-GF17-DSEMAINE    PIC X(02).                   00410
                     06 COMM-GF17-JOUR        PIC X(01).                   00420
      *------                                                                   
E0105        03 COMM-GF17-REASONCODE-FLAG       PIC X(01).                      
      *------                                                   1270    00560004
E0105        03 COMM-GF17-REASONCODE    OCCURS 20.                              
E0105          04 COMM-GF17-AFFECT-CREAS        PIC X(01).                      
E0105          04 COMM-GF17-CREASON             PIC X(5).                       
      *-------                                                 323C     00560009
             03 COMM-GF17-0-DLIVRAISONS.                                00570004
                04 COMM-GF17-0-POSTE OCCURS 20 TIMES.                   00590005
                   05 COMM-GF17-0-DLIVRAISON.                           00610005
                      06 SS                   PIC X(02).                00620005
                      06 AA                   PIC X(02).                      00
                      06 MM                   PIC X(02).                      00
                      06 JJ                   PIC X(02).                      00
                 05 COMM-GF17-0-SSAAR REDEFINES  COMM-GF17-0-DLIVRAISON.00660005
                    06 COMM-GF17-0-SSAA       PIC X(04).                00670005
                    06 COMM-GF17-0-MMAA       PIC X(02).                00680005
                    06 COMM-GF17-0-JJAA       PIC X(02).                00690005
      *                                                                 00700004
                 05 COMM-GF17-0-ANNEE-SEMAINE.                            007   
                    06 COMM-GF17-0-SSANNEE        PIC X(04).                007 
                    06 COMM-GF17-0-DSEMAINE       PIC X(02).                007 
                    06 COMM-GF17-0-JOUR           PIC X(01).                007 
      *------                                                  623C     00750004
             03 CODE-EFFECTUE            PIC X .                        00880005
                88 NON-DEJA-EFFECT       VALUE '1'.                     00880006
                88 MOD-DEJA-EFFECT       VALUE '0'.                     00880007
      *------                                                  624C     00890009
             03 COMM-GF17-QUOTA.                                        00570004
                04 COMM-GF17-POSTE-QUOTA OCCURS 20 TIMES.               00590005
                   05 COMM-GF17-CQUOTA PIC X(5).                        00610005
ASAR  *------ FLAG INTERFACAGE NCG / JDA                                        
ASAR         03 COMM-GF17-INTRFC             PIC X(1).                          
CD           03 COMM-GF17-IDC                PIC 9(1).                          
CD           03 COMM-FL-TRT-SUPP         PIC X .                        00880005
CD              88 COMM-TRT-SUPP         VALUE '1'.                     00880006
CD              88 COMM-PAS-TRT-SUPP     VALUE '0'.                     00880007
CD2404       03 COMM-INTER.                                                     
CD2404          05 CO-GF17-DLIVRAISON.                                          
CD2404             06 SS              PIC X(02).                                
CD2404             06 AA              PIC X(02).                                
CD2404             06 MM              PIC X(02).                                
CD2404             06 JJ              PIC X(02).                                
CD2404          05 CO-GF17-SSAAR  REDEFINES   CO-GF17-DLIVRAISON.               
CD2404             06 CO-GF17-SSAA        PIC X(04).                            
CD2404             06 CO-GF17-MMAA        PIC X(02).                            
CD2404             06 CO-GF17-JJAA        PIC X(02).                            
CD2404                                                                          
CD2404          05 CO-GF17-ANNEE-SEMAINE.                                       
CD2404             06 CO-GF17-SSANNEE     PIC X(04).                            
CD2404             06 CO-GF17-DSEMAINE    PIC X(02).                            
CD2404             06 CO-GF17-JOUR        PIC X(01).                            
      *------                                                  724C     00600004
GD1306       03 COMM-GF17-FLGDAT          PIC X(01).                            
GD1306       03 COMM-GF17-PARMDAT         PIC 9(10).                            
GD1306       03 COMM-GF17-DATEPARAM       PIC X(08).                            
CRO183       03 COMM-GF17-XCTRL-WQCOLIRECEPT PIC X.                             
E0105        03 COMM-GF17-SATDUEDATE      PIC X(8).                             
E0105        03 COMM-GF17-LIBELLE         PIC X(19).                            
LA1014       03 COMM-GF17-REF-EAN         PIC 9.                                
      *      03 COMM-GF17-FILLER         PIC X(1230).                   00900010
      *                                                                 00910004
      ***************************************************************** 00920004
                                                                                
