      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RTAI27                                      
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVAI2700.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVAI2700.                                                             
      *}                                                                        
           05 AI27-CDESCRIPTIF     PIC X(05).                                   
           05 AI27-FDFNUM          PIC X(05).                                   
           05 AI27-DSYST           PIC S9(13) COMP-3.                           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVAI2700-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVAI2700-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 AI27-CDESCRIPTIF-F   PIC S9(4) COMP.                              
      *--                                                                       
           05 AI27-CDESCRIPTIF-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 AI27-FDFNUM-F        PIC S9(4) COMP.                              
      *--                                                                       
           05 AI27-FDFNUM-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 AI27-DSYST-F         PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           05 AI27-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
