      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGF003 AU 11/09/2002  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,05,BI,A,                          *        
      *                           18,03,BI,A,                          *        
      *                           21,03,BI,A,                          *        
      *                           24,05,BI,A,                          *        
      *                           29,20,BI,A,                          *        
      *                           49,07,BI,A,                          *        
      *                           56,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGF003.                                                        
            05 NOMETAT-IGF003           PIC X(6) VALUE 'IGF003'.                
            05 RUPTURES-IGF003.                                                 
           10 IGF003-NSOCDEPOT          PIC X(03).                      007  003
           10 IGF003-NDEPOT             PIC X(03).                      010  003
           10 IGF003-BRUNBLANC          PIC X(05).                      013  005
           10 IGF003-NSOCIETE           PIC X(03).                      018  003
           10 IGF003-NLIEU              PIC X(03).                      021  003
           10 IGF003-CAPPRO             PIC X(05).                      024  005
           10 IGF003-LREFFOURN          PIC X(20).                      029  020
           10 IGF003-NVENTE             PIC X(07).                      049  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGF003-SEQUENCE           PIC S9(04) COMP.                056  002
      *--                                                                       
           10 IGF003-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGF003.                                                   
           10 IGF003-CFAM               PIC X(05).                      058  005
           10 IGF003-CMARQ              PIC X(05).                      063  005
           10 IGF003-CVENDEUR           PIC X(06).                      068  006
           10 IGF003-DANNULATION        PIC X(10).                      074  010
           10 IGF003-NCODIC             PIC X(07).                      084  007
           10 IGF003-WCQERESF           PIC X(01).                      091  001
           10 IGF003-QSTOCK             PIC S9(05)      COMP-3.         092  003
           10 IGF003-QVENDUE            PIC S9(05)      COMP-3.         095  003
            05 FILLER                      PIC X(415).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGF003-LONG           PIC S9(4)   COMP  VALUE +097.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGF003-LONG           PIC S9(4) COMP-5  VALUE +097.           
                                                                                
      *}                                                                        
