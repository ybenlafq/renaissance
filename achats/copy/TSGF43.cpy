      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                         
       01 TS-GF43-APPLI.                                                        
          05 TS-GF43.                                                           
             10 TS-GF43-ENR.                                                    
               14 TS-GF43-ENTETE.                                               
                   20 TS-GF43-TOPMAJ-ENTETE  PIC X.                             
                      88 CODIC-SANS-MODIF VALUE ' '.                            
                      88 NOUVEAU-CODIC    VALUE 'C'.                            
                      88 ANCIEN-CODIC     VALUE 'M'.                            
                      88 CODIC-SUPPRIME   VALUE 'X'.                            
                   20 TS-GF43-NCODIC         PIC X(7).                          
                   20 TS-GF43-LREFFOURN      PIC X(20).                         
                   20 TS-GF43-NSOCDEM        PIC X(3).                          
                   20 TS-GF43-NLIEUDEM       PIC X(3).                          
CRO175             20 TS-GF43-QTOT-MAP       PIC S9(6) COMP-3.                  
                   20 TS-GF43-QCDE-TOT       PIC S9(6) COMP-3.                  
E0100              20 TS-GF43-QCDEORIG       PIC S9(6) COMP-3.                  
                   20 TS-GF43-QREC-TOT       PIC S9(6) COMP-3.                  
                   20 TS-GF43-QSOLDE-TOT     PIC S9(6) COMP-3.                  
                   20 TS-GF43-NSURCDE        PIC X(7).                          
E0092              20 TS-GF43-COMMENTAIRE    PIC X(1).                          
                      88 TS-GF43-COMMENT      VALUE 'O'.                        
                      88 TS-GF43-NO-COMMENT   VALUE 'N'.                        
E0105              20 TS-GF43-CREASON-FLAG   PIC  X(01).                        
                      88 TS-GF43-CREASON      VALUE 'O'.                        
                      88 TS-GF43-NO-CREASON   VALUE 'N'.                        
                      88 TS-GF43-CREASON-RENS VALUE 'R'.                        
E0105              20 TS-GF43-CODE-REASON    PIC X(05).                         
               14 TS-GF43-MATRICE.                                              
                15 TS-GF43-CORPS  OCCURS 10 TIMES.                              
                 16 TS-GF43-ENTREPOT.                                           
                   20 TS-GF43-NSOC-NDEPOT.                                      
 E0037**              25 TS-GF43-NSOC          PIC X(3).                        
                      25 TS-GF43-NDEPOT        PIC X(3).                        
                      25 TS-GF43-NCDE-FILLE    PIC X(7).                        
 CRO               20 TS-GF43-QCDE-ENT       PIC S9(6) COMP-3.                  
 CRO               20 TS-GF43-QREC-ENT       PIC S9(6) COMP-3.                  
 CRO               20 TS-GF43-QSOLDE-ENT     PIC S9(6) COMP-3.                  
                   20 TS-GF43-TOPMAJ-ENTREPOT PIC X.                            
                      88 ENTREPOT-SANS-MODIF VALUE ' '.                         
                      88 ENTREPOT-NOUVEAU    VALUE 'C'.                         
                      88 ENTREPOT-ANCIEN     VALUE 'M'.                         
                      88 ENTREPOT-SUPPRIME   VALUE 'X'.                         
                   20 TS-GF43-DATE-QTE OCCURS 20 TIMES.                         
                      25 TS-GF43-TOPMAJ-CORPS  PIC X.                           
                         88 TRIPLET-SANS-MODIF VALUE ' '.                       
                         88 TRIPLET-NOUVEAU    VALUE 'C'.                       
                         88 TRIPLET-ANCIEN     VALUE 'M'.                       
                         88 TRIPLET-SUPPRIME   VALUE 'X'.                       
                      25 TS-GF43-QCDE          PIC S9(5) COMP-3.                
CRO206*               25 TS-GF43-QREC          PIC S9(5) COMP-3.                
CRO206*               25 TS-GF43-QSOLDE        PIC S9(5) COMP-3.                
                      25 TS-GF43-QUOLIVR       PIC S9(5) COMP-3.                
                      25 TS-GF43-QUOPAL        PIC S9(5) COMP-3.                
                      25 TS-GF43-DLIVRAISON    PIC X(8).                        
                      25 TS-GF43-NLIVRAISON    PIC X(7).                        
                      25 TS-GF43-MULTI-NSEQ    PIC X.                           
                   20 TS-GF43-CQUOTA         PIC X(5).                          
                   20 TS-GF43-CMODSTOCK      PIC X(5).                          
                   20 TS-GF43-QCOLIRECEPT    PIC S9(5) COMP-3.                  
                   20 TS-GF43-CONTROL-COLIRECEP PIC X(1).                       
               14 TS-GF43-O-NCODIC      PIC X(7).                               
               14 TS-GF43-O-MATRICE.                                            
                15 TS-GF43-O-CORPS  OCCURS 10 TIMES.                            
                 16 TS-GF43-O-ENTREPOT.                                         
                   20 TS-GF43-O-NSOC-NDEPOT.                                    
E0037**               25 TS-GF43-O-NSOC         PIC X(3).                       
                      25 TS-GF43-O-NDEPOT       PIC X(3).                       
                      25 TS-GF43-O-NCDE-FILLE   PIC X(7).                       
CRO                20 TS-GF43-O-QCDE-ENT     PIC S9(6) COMP-3.                  
CRO                20 TS-GF43-O-QREC-ENT     PIC S9(6) COMP-3.                  
CRO                20 TS-GF43-O-QSOLDE-ENT   PIC S9(6) COMP-3.                  
                   20 TS-GF43-O-TOPMAJ-ENTREPOT PIC X.                          
                      88 ENTREPOT-O-SANS-MODIF VALUE ' '.                       
                      88 ENTREPOT-O-NOUVEAU    VALUE 'C'.                       
                      88 ENTREPOT-O-ANCIEN     VALUE 'M'.                       
                      88 ENTREPOT-O-SUPPRIME   VALUE 'X'.                       
                   20 TS-GF43-O-DATE-QTE OCCURS 20 TIMES.                       
                      25 TS-GF43-O-TOPMAJ-CORPS  PIC X.                         
                         88 TRIPLET-O-SANS-MODIF    VALUE ' '.                  
                         88 TRIPLET-O-NOUVEAU       VALUE 'C'.                  
                         88 TRIPLET-O-ANCIEN        VALUE 'M'.                  
                         88 TRIPLET-O-SUPPRIME      VALUE 'X'.                  
                      25 TS-GF43-O-QCDE          PIC S9(5) COMP-3.              
CRO206*               25 TS-GF43-O-QREC          PIC S9(5) COMP-3.              
CRO206*               25 TS-GF43-O-QSOLDE        PIC S9(5) COMP-3.              
                      25 TS-GF43-O-QUOLIVR       PIC S9(5) COMP-3.              
                      25 TS-GF43-O-QUOPAL        PIC S9(5) COMP-3.              
                      25 TS-GF43-O-DLIVRAISON    PIC X(8).                      
                      25 TS-GF43-O-NLIVRAISON    PIC X(7).                      
                      25 TS-GF43-O-MULTI-NSEQ    PIC X.                         
                   20 TS-GF43-O-CQUOTA           PIC X(5).                      
                   20 TS-GF43-O-CMODSTOCK        PIC X(5).                      
                   20 TS-GF43-O-QCOLIRECEPT      PIC S9(5) COMP-3.              
                   20 TS-GF43-O-CONTROL-COLIRECEP PIC X(1).                     
E0236          14 TS-GF43-O-QCDE-TOT             PIC S9(6) COMP-3.              
                                                                                
