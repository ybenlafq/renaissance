      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGF0501                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF0501                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF0501.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF0501.                                                            
      *}                                                                        
           10 GF05-NCODIC               PIC X(7).                               
           10 GF05-QSOUHAITEE           PIC S9(5)V USAGE COMP-3.                
           10 GF05-DSOUHAITEE           PIC X(8).                               
           10 GF05-NENTCDE              PIC X(5).                               
           10 GF05-CINTERLOCUT          PIC X(5).                               
           10 GF05-NSOCIETE             PIC X(3).                               
           10 GF05-NLIEU                PIC X(3).                               
           10 GF05-NSOCLIVR             PIC X(3).                               
           10 GF05-NDEPOT               PIC X(3).                               
           10 GF05-CSTATUT              PIC X(1).                               
           10 GF05-CHEFPROD             PIC X(5).                               
           10 GF05-NPAGE                PIC X(3).                               
           10 GF05-NLIGNE               PIC X(2).                               
           10 GF05-DETAT                PIC X(8).                               
           10 GF05-NSEQ                 PIC X(7).                               
           10 GF05-DSYST                PIC S9(13)V USAGE COMP-3.               
           10 GF05-CFEXT                PIC X(1).                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGF0501                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF0501-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF0501-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF05-NCODIC-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF05-NCODIC-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF05-QSOUHAITEE-F         PIC S9(4) COMP.                         
      *--                                                                       
           10 GF05-QSOUHAITEE-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF05-DSOUHAITEE-F         PIC S9(4) COMP.                         
      *--                                                                       
           10 GF05-DSOUHAITEE-F         PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF05-NENTCDE-F            PIC S9(4) COMP.                         
      *--                                                                       
           10 GF05-NENTCDE-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF05-CINTERLOCUT-F        PIC S9(4) COMP.                         
      *--                                                                       
           10 GF05-CINTERLOCUT-F        PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF05-NSOCIETE-F           PIC S9(4) COMP.                         
      *--                                                                       
           10 GF05-NSOCIETE-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF05-NLIEU-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 GF05-NLIEU-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF05-NSOCLIVR-F           PIC S9(4) COMP.                         
      *--                                                                       
           10 GF05-NSOCLIVR-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF05-NDEPOT-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF05-NDEPOT-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF05-CSTATUT-F            PIC S9(4) COMP.                         
      *--                                                                       
           10 GF05-CSTATUT-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF05-CHEFPROD-F           PIC S9(4) COMP.                         
      *--                                                                       
           10 GF05-CHEFPROD-F           PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF05-NPAGE-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 GF05-NPAGE-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF05-NLIGNE-F             PIC S9(4) COMP.                         
      *--                                                                       
           10 GF05-NLIGNE-F             PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF05-DETAT-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 GF05-DETAT-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF05-NSEQ-F               PIC S9(4) COMP.                         
      *--                                                                       
           10 GF05-NSEQ-F               PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF05-DSYST-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 GF05-DSYST-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GF05-CFEXT-F              PIC S9(4) COMP.                         
      *                                                                         
      *--                                                                       
           10 GF05-CFEXT-F              PIC S9(4) COMP-5.                       
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
