      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGF9900                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF9900                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF9900.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF9900.                                                            
      *}                                                                        
           02  GF99-TIMESTPE                                                    
               PIC X(0026).                                                     
           02  GF99-NCDE                                                        
               PIC X(0007).                                                     
           02  GF99-NSEQCDE                                                     
               PIC S9(0009) COMP-3.                                             
           02  GF99-MODTRAIT                                                    
               PIC X(0006).                                                     
           02  GF99-TRAITE                                                      
               PIC X(0001).                                                     
           02  GF99-TIMESTPR                                                    
               PIC X(0026).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
           02  GF99-ENTETE                                                      
               PIC X(0364).                                                     
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           02  GF99-NLIGNE                                                      
               PIC X(0002).                                                     
           02  GF99-DETAIL                                                      
               PIC X(0173).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGF9900                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF9900-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF9900-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF99-TIMESTPE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF99-TIMESTPE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF99-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF99-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF99-NSEQCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF99-NSEQCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF99-MODTRAIT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF99-MODTRAIT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF99-TRAITE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF99-TRAITE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF99-TIMESTPR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF99-TIMESTPR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF99-ENTETE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF99-ENTETE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF99-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF99-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF99-DETAIL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF99-DETAIL-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
