      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGF6500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF6500                         
      **********************************************************                
       01  RVGF6500.                                                            
           02  GF65-NSOC                                                        
               PIC X(0003).                                                     
           02  GF65-LCOMBCDE1                                                   
               PIC X(0040).                                                     
           02  GF65-LCOMBCDE2                                                   
               PIC X(0040).                                                     
           02  GF65-LCOMBCDE3                                                   
               PIC X(0040).                                                     
           02  GF65-LCOMBCDE4                                                   
               PIC X(0040).                                                     
           02  GF65-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGF6500                                  
      **********************************************************                
       01  RVGF6500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF65-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF65-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF65-LCOMBCDE1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF65-LCOMBCDE1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF65-LCOMBCDE2-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF65-LCOMBCDE2-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF65-LCOMBCDE3-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF65-LCOMBCDE3-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF65-LCOMBCDE4-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF65-LCOMBCDE4-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF65-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GF65-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
