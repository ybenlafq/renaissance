      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA VUE RVGF8002                                               
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA VUE RVGF8002                           
      *---------------------------------------------------------                
      *                                                                         
       01  RVGF8002.                                                            
           02  GF80-NCDE                                                        
               PIC X(7).                                                        
           02  GF80-NCODIC                                                      
               PIC X(7).                                                        
           02  GF80-QBL                                                         
               PIC S9(7) COMP-3.                                                
           02  GF80-NVOLUME                                                     
               PIC S9(4)V9(2) COMP-3.                                           
           02  GF80-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GF80-DEPOTEXP                                                    
               PIC X(20).                                                       
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA VUE RVGF8001                                    
      *---------------------------------------------------------                
      *                                                                         
       01  RVGF8002-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF80-NCDE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF80-NCDE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF80-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF80-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF80-QBL-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF80-QBL-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF80-NVOLUME-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF80-NVOLUME-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF80-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF80-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF80-DEPOTEXP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GF80-DEPOTEXP-F                                                  
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
