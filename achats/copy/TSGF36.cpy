      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00000010
       01  TS-GF36-DONNEES.                                             00000070
           05 TS-GF36-FLAG          PIC X(01).                          00000090
           05 TS-GF36-NCDE          PIC X(07).                          00000090
           05 TS-GF36-NLIEU         PIC X(03).                          00000090
           05 TS-GF36-NCODIC        PIC X(07).                          00000090
           05 TS-GF36-LREFFOURN     PIC X(20).                          00000090
           05 TS-GF36-QCDEORIG      PIC X(05).                          00000090
           05 TS-GF36-QCDE          PIC X(05).                          00000090
           05 TS-GF36-DLIVRAISON    PIC X(08).                          00000090
           05 TS-GF36-WCLT          PIC X(01).                          00000090
      ****************************************************************  00000010
                                                                                
