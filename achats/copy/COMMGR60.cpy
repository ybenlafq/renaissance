      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGR60 (TGR00 -> MENU)    TR: GR00  *    00020000
      *               GESTION DES RECEPTIONS                       *    00030000
      *                                                                 00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3710  00050001
      *                                                                 00060000
      *    TRANSACTION GR60 : GESTION DES RECEPTIONS PAR ARTICLE   *    00070000
      *                              OPTION INTERROGATION          *    00080000
      *                                                                 00090000
          02 COMM-GR60-APPLI REDEFINES COMM-GR00-APPLI.                 00100000
      *------------------------------ ZONE DONNEES TGR60                00110000
             03 COMM-GR60-DONNEES-TGR60.                                00120000
      *------------------------------ CODE FONCTION                     00130000
                04 COMM-GR60-FONCT             PIC XXX.                 00140000
      *------------------------------ DATE DEBUT INT REC PAR ARTICLE    00150000
                04 COMM-GR60-DATEDEB           PIC X(08).               00160000
      *------------------------------ DATE FIN INT REC PAR ARTICLE      00170000
                04 COMM-GR60-DATEFIN           PIC X(08).               00180000
      *------------------------------ CODE ARTICLE                      00190000
                04 COMM-GR60-NCODIC            PIC X(07).               00200000
      *------------------------------ LIBELLE REF FOURNISSEUR           00210000
                04 COMM-GR60-LREFFOURN         PIC X(20).               00220000
      *------------------------------ CODE FAMILLE                      00230000
                04 COMM-GR60-CFAM              PIC X(05).               00240000
      *------------------------------ CODE MARQUE                       00250000
                04 COMM-GR60-CMARQ             PIC X(05).               00260000
      *------------------------------ CODE SOCIETE DEPOT                00270001
                04 COMM-GR60-NSOCLIVR          PIC X(03).               00280001
                04 COMM-GR60-NDEPOT            PIC X(03).               00290001
      *------------------------------ NO DE RECEPTION A QUAI            00300001
             03 COMM-GR60-ZONEPAGINATION OCCURS 100.                    00310001
                04 COMM-GR60-NORECQUAI         PIC X(07).               00320001
                04 COMM-GR60-NCDE              PIC X(07).               00330001
      *------------------------------ ZONE LIBRE                        00340001
             03 COMM-GR60-LIBRE          PIC X(2248).                   00350002
      ***************************************************************** 00360000
                                                                                
