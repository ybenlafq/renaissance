      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : TS LIGNES DES NUMEROS DE COMMANDES CREES LORS DU       *         
      *        DECOUPAGE DE LA COMMANDE EN COMMANDE MONOCODIC         *         
      *****************************************************************         
      *                                                               *         
       01  TS-GF41.                                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 TS-GF41-LONG                    PIC S9(4) COMP VALUE +14.         
      *--                                                                       
           02 TS-GF41-LONG                    PIC S9(4) COMP-5 VALUE            
                                                                    +14.        
      *}                                                                        
           02 TS-GF41-DONNEES.                                                  
              03 TS-GF41-NCDE                 PIC X(07).                        
              03 TS-GF41-NCODIC               PIC X(07).                        
                                                                                
