      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: TGf47   TGf47                                              00000020
      ***************************************************************** 00000030
       01   EGF47I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDEPL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSOCDEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCDEPF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSOCDEPI  PIC X(6).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNWEEKL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNWEEKL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNWEEKF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNWEEKI   PIC X(6).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMODSTOCKL    COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCMODSTOCKL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCMODSTOCKF    PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCMODSTOCKI    PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRAYONL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCRAYONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCRAYONF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCRAYONI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCQUOTAL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCQUOTAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCQUOTAF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCQUOTAI  PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MUNITEL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MUNITEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MUNITEF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MUNITEI   PIC X(40).                                      00000450
           02 MTABGI OCCURS   5 TIMES .                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLRAYONL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MLRAYONL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLRAYONF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MLRAYONI     PIC X(15).                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPREVL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MLPREVL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLPREVF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MLPREVI      PIC X(4).                                  00000540
             03 MSELECTD OCCURS   7 TIMES .                             00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSELECTL   COMP PIC S9(4).                            00000560
      *--                                                                       
               04 MSELECTL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MSELECTF   PIC X.                                     00000570
               04 FILLER     PIC X(4).                                  00000580
               04 MSELECTI   PIC X.                                     00000590
             03 MQPREVD OCCURS   7 TIMES .                              00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MQPREVL    COMP PIC S9(4).                            00000610
      *--                                                                       
               04 MQPREVL COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MQPREVF    PIC X.                                     00000620
               04 FILLER     PIC X(4).                                  00000630
               04 MQPREVI    PIC X(5).                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPRISL      COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MLPRISL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLPRISF      PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MLPRISI      PIC X(4).                                  00000680
             03 MQPRISD OCCURS   7 TIMES .                              00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MQPRISL    COMP PIC S9(4).                            00000700
      *--                                                                       
               04 MQPRISL COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MQPRISF    PIC X.                                     00000710
               04 FILLER     PIC X(4).                                  00000720
               04 MQPRISI    PIC X(5).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLSOLDETOTL  COMP PIC S9(4).                            00000740
      *--                                                                       
             03 MLSOLDETOTL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLSOLDETOTF  PIC X.                                     00000750
             03 FILLER  PIC X(4).                                       00000760
             03 MLSOLDETOTI  PIC X(7).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLSOLDEL     COMP PIC S9(4).                            00000780
      *--                                                                       
             03 MLSOLDEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLSOLDEF     PIC X.                                     00000790
             03 FILLER  PIC X(4).                                       00000800
             03 MLSOLDEI     PIC X(5).                                  00000810
             03 MQSOLDED OCCURS   7 TIMES .                             00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MQSOLDEL   COMP PIC S9(4).                            00000830
      *--                                                                       
               04 MQSOLDEL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MQSOLDEF   PIC X.                                     00000840
               04 FILLER     PIC X(4).                                  00000850
               04 MQSOLDEI   PIC X(6).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSOLDETOTL  COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MQSOLDETOTL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MQSOLDETOTF  PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQSOLDETOTI  PIC X(7).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(15).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(58).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * SDF: TGf47   TGf47                                              00001160
      ***************************************************************** 00001170
       01   EGF47O REDEFINES EGF47I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MPAGEA    PIC X.                                          00001350
           02 MPAGEC    PIC X.                                          00001360
           02 MPAGEP    PIC X.                                          00001370
           02 MPAGEH    PIC X.                                          00001380
           02 MPAGEV    PIC X.                                          00001390
           02 MPAGEO    PIC X(3).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MPAGEMAXA      PIC X.                                     00001420
           02 MPAGEMAXC PIC X.                                          00001430
           02 MPAGEMAXP PIC X.                                          00001440
           02 MPAGEMAXH PIC X.                                          00001450
           02 MPAGEMAXV PIC X.                                          00001460
           02 MPAGEMAXO      PIC X(3).                                  00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MSOCDEPA  PIC X.                                          00001490
           02 MSOCDEPC  PIC X.                                          00001500
           02 MSOCDEPP  PIC X.                                          00001510
           02 MSOCDEPH  PIC X.                                          00001520
           02 MSOCDEPV  PIC X.                                          00001530
           02 MSOCDEPO  PIC X(6).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MNWEEKA   PIC X.                                          00001560
           02 MNWEEKC   PIC X.                                          00001570
           02 MNWEEKP   PIC X.                                          00001580
           02 MNWEEKH   PIC X.                                          00001590
           02 MNWEEKV   PIC X.                                          00001600
           02 MNWEEKO   PIC X(6).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MCMODSTOCKA    PIC X.                                     00001630
           02 MCMODSTOCKC    PIC X.                                     00001640
           02 MCMODSTOCKP    PIC X.                                     00001650
           02 MCMODSTOCKH    PIC X.                                     00001660
           02 MCMODSTOCKV    PIC X.                                     00001670
           02 MCMODSTOCKO    PIC X(5).                                  00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MCRAYONA  PIC X.                                          00001700
           02 MCRAYONC  PIC X.                                          00001710
           02 MCRAYONP  PIC X.                                          00001720
           02 MCRAYONH  PIC X.                                          00001730
           02 MCRAYONV  PIC X.                                          00001740
           02 MCRAYONO  PIC X(5).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MCQUOTAA  PIC X.                                          00001770
           02 MCQUOTAC  PIC X.                                          00001780
           02 MCQUOTAP  PIC X.                                          00001790
           02 MCQUOTAH  PIC X.                                          00001800
           02 MCQUOTAV  PIC X.                                          00001810
           02 MCQUOTAO  PIC X(5).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MUNITEA   PIC X.                                          00001840
           02 MUNITEC   PIC X.                                          00001850
           02 MUNITEP   PIC X.                                          00001860
           02 MUNITEH   PIC X.                                          00001870
           02 MUNITEV   PIC X.                                          00001880
           02 MUNITEO   PIC X(40).                                      00001890
           02 MTABGO OCCURS   5 TIMES .                                 00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MLRAYONA     PIC X.                                     00001920
             03 MLRAYONC     PIC X.                                     00001930
             03 MLRAYONP     PIC X.                                     00001940
             03 MLRAYONH     PIC X.                                     00001950
             03 MLRAYONV     PIC X.                                     00001960
             03 MLRAYONO     PIC X(15).                                 00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MLPREVA      PIC X.                                     00001990
             03 MLPREVC PIC X.                                          00002000
             03 MLPREVP PIC X.                                          00002010
             03 MLPREVH PIC X.                                          00002020
             03 MLPREVV PIC X.                                          00002030
             03 MLPREVO      PIC X(4).                                  00002040
             03 DFHMS1 OCCURS   7 TIMES .                               00002050
               04 FILLER     PIC X(2).                                  00002060
               04 MSELECTA   PIC X.                                     00002070
               04 MSELECTC   PIC X.                                     00002080
               04 MSELECTP   PIC X.                                     00002090
               04 MSELECTH   PIC X.                                     00002100
               04 MSELECTV   PIC X.                                     00002110
               04 MSELECTO   PIC X.                                     00002120
             03 DFHMS2 OCCURS   7 TIMES .                               00002130
               04 FILLER     PIC X(2).                                  00002140
               04 MQPREVA    PIC X.                                     00002150
               04 MQPREVC    PIC X.                                     00002160
               04 MQPREVP    PIC X.                                     00002170
               04 MQPREVH    PIC X.                                     00002180
               04 MQPREVV    PIC X.                                     00002190
               04 MQPREVO    PIC X(5).                                  00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MLPRISA      PIC X.                                     00002220
             03 MLPRISC PIC X.                                          00002230
             03 MLPRISP PIC X.                                          00002240
             03 MLPRISH PIC X.                                          00002250
             03 MLPRISV PIC X.                                          00002260
             03 MLPRISO      PIC X(4).                                  00002270
             03 DFHMS3 OCCURS   7 TIMES .                               00002280
               04 FILLER     PIC X(2).                                  00002290
               04 MQPRISA    PIC X.                                     00002300
               04 MQPRISC    PIC X.                                     00002310
               04 MQPRISP    PIC X.                                     00002320
               04 MQPRISH    PIC X.                                     00002330
               04 MQPRISV    PIC X.                                     00002340
               04 MQPRISO    PIC X(5).                                  00002350
             03 FILLER       PIC X(2).                                  00002360
             03 MLSOLDETOTA  PIC X.                                     00002370
             03 MLSOLDETOTC  PIC X.                                     00002380
             03 MLSOLDETOTP  PIC X.                                     00002390
             03 MLSOLDETOTH  PIC X.                                     00002400
             03 MLSOLDETOTV  PIC X.                                     00002410
             03 MLSOLDETOTO  PIC X(7).                                  00002420
             03 FILLER       PIC X(2).                                  00002430
             03 MLSOLDEA     PIC X.                                     00002440
             03 MLSOLDEC     PIC X.                                     00002450
             03 MLSOLDEP     PIC X.                                     00002460
             03 MLSOLDEH     PIC X.                                     00002470
             03 MLSOLDEV     PIC X.                                     00002480
             03 MLSOLDEO     PIC X(5).                                  00002490
             03 DFHMS4 OCCURS   7 TIMES .                               00002500
               04 FILLER     PIC X(2).                                  00002510
               04 MQSOLDEA   PIC X.                                     00002520
               04 MQSOLDEC   PIC X.                                     00002530
               04 MQSOLDEP   PIC X.                                     00002540
               04 MQSOLDEH   PIC X.                                     00002550
               04 MQSOLDEV   PIC X.                                     00002560
               04 MQSOLDEO   PIC X(6).                                  00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MQSOLDETOTA  PIC X.                                     00002590
             03 MQSOLDETOTC  PIC X.                                     00002600
             03 MQSOLDETOTP  PIC X.                                     00002610
             03 MQSOLDETOTH  PIC X.                                     00002620
             03 MQSOLDETOTV  PIC X.                                     00002630
             03 MQSOLDETOTO  PIC X(7).                                  00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MZONCMDA  PIC X.                                          00002660
           02 MZONCMDC  PIC X.                                          00002670
           02 MZONCMDP  PIC X.                                          00002680
           02 MZONCMDH  PIC X.                                          00002690
           02 MZONCMDV  PIC X.                                          00002700
           02 MZONCMDO  PIC X(15).                                      00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MLIBERRA  PIC X.                                          00002730
           02 MLIBERRC  PIC X.                                          00002740
           02 MLIBERRP  PIC X.                                          00002750
           02 MLIBERRH  PIC X.                                          00002760
           02 MLIBERRV  PIC X.                                          00002770
           02 MLIBERRO  PIC X(58).                                      00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MCODTRAA  PIC X.                                          00002800
           02 MCODTRAC  PIC X.                                          00002810
           02 MCODTRAP  PIC X.                                          00002820
           02 MCODTRAH  PIC X.                                          00002830
           02 MCODTRAV  PIC X.                                          00002840
           02 MCODTRAO  PIC X(4).                                       00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MCICSA    PIC X.                                          00002870
           02 MCICSC    PIC X.                                          00002880
           02 MCICSP    PIC X.                                          00002890
           02 MCICSH    PIC X.                                          00002900
           02 MCICSV    PIC X.                                          00002910
           02 MCICSO    PIC X(5).                                       00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MNETNAMA  PIC X.                                          00002940
           02 MNETNAMC  PIC X.                                          00002950
           02 MNETNAMP  PIC X.                                          00002960
           02 MNETNAMH  PIC X.                                          00002970
           02 MNETNAMV  PIC X.                                          00002980
           02 MNETNAMO  PIC X(8).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MSCREENA  PIC X.                                          00003010
           02 MSCREENC  PIC X.                                          00003020
           02 MSCREENP  PIC X.                                          00003030
           02 MSCREENH  PIC X.                                          00003040
           02 MSCREENV  PIC X.                                          00003050
           02 MSCREENO  PIC X(4).                                       00003060
                                                                                
