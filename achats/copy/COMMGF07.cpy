      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE PRG TGF07                    TR: GF07  *            
      *     RESERVATIONS SUR COMMANDES FOURNISSEURS                *            
      **************************************************************            
      * ZONES RESERVEES APPLICATIVES ---------------------------- 7421          
      *                                                                         
          02 COMM-GF07-APPLI REDEFINES COMM-GF00-APPLI.                         
      *------------------------------ NUMERO DE PAGE                            
             03 NUMERO-DE-PAGE         PIC 9(3).                                
      *------------------------------ NUMERO DE PAGE MAXIMALE                   
             03 NUMERO-DE-PAGE-MAX     PIC 9(3).                                
      *------------------------------ PARTIE HAUTE DU TABLEAU                   
             03 HAUT-DU-TABLEAU.                                                
      *------------------------------ CONFIGURATION (SOCIETE/MAGASIN)           
                04 COUPLE-SOCIETE-MAGASIN.                                      
      *------------------------------ NUMERO DE SOCIETE                         
                   05 COMM-GF07-NSOC-E          PIC X(3).                       
      *------------------------------ NUMERO DE LIEU (MAGASIN)                  
                   05 COMM-GF07-NLIEU-E         PIC X(3).                       
      *------------------------------ NUMERO DE COMMANDE                        
                04 COMM-GF07-NCDE-E            PIC  X(7).                       
      *------------------------------ NUMERO DU CODE ARTICLE                    
                04 COMM-GF07-NCODIC-E          PIC  X(7).                       
      *------------------------------ CODE FAMILLE                              
                04 COMM-GF07-CFAM-E            PIC  X(5).                       
      *------------------------------ CODE MARQUE                               
                04 COMM-GF07-CMARQ-E           PIC  X(5).                       
      *------------------------------ LIBELLE DU CODE ARTICLE =LREFFOURN        
                04 COMM-GF07-LCODIC           PIC  X(20).                       
      *------------------------------ LIBELLE DE LA FAMILLE                     
                04 COMM-GF07-LFAM             PIC  X(20).                       
      *------------------------------ LIBELLE DE LA MARQUE                      
                04 COMM-GF07-LMARQ            PIC  X(20).                       
      *------------------------------ PARTIE DROITE DU HAUT DU TABLEAU          
                04 PARTIE-DROITE.                                               
      *------------------------------ SOMME DES QUANTITES COMMANDEES            
                   05 COMM-GF07-QCDE           PIC S9(5)   COMP-3.              
      *------------------------------ SOMME DES RESERVES CLIENTS                
                   05 COMM-GF07-QCLIENTS       PIC S9(5)   COMP-3.              
      *------------------------------ SOMME DES RESERVES MAGASINS               
                   05 COMM-GF07-QMAGASINS      PIC S9(5)   COMP-3.              
      *------------------------------ SOMME RESTANTE DISPONIBLE                 
                   05 COMM-GF07-QDISPONIBLE    PIC S9(5)   COMP-3.              
      *------------------------------ MAJ DEJA EFFECTUE                         
      *      03 COMM-GF07-FILLER         PIC X(7313).                           
      *****************************************************************         
                                                                                
