      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   TS : VALIDATION DES COMMANDES SPECIFIQUES EN INSTANCE       *         
      *****************************************************************         
      *                                                               *         
       01  TS-GF-04.                                                            
      *----------------------------------  LONGUEUR TS                          
           02 TS-GF04-LONG                 PIC S9(5) COMP-3                     
      *                                    VALUE +1320.                         
      *                                    VALUE +1332.                         
      *YS2565                              VALUE +1344.                         
                                           VALUE +1380.                         
           02 TS-GF04-DONNEES.                                                  
      *------------------------------TABLE TYPE DE DECLARATION                  
              03 TS-GF04-TABLEAU  OCCURS 12.                                    
                 04 TS-GF04-NCODIC           PIC X(7).                          
                 04 TS-GF04-CMARQ            PIC X(5).                          
                 04 TS-GF04-CFAM             PIC X(5).                          
                 04 TS-GF04-LREFFOURN        PIC X(20).                         
                 04 TS-GF04-TOPCODIC         PIC X(1).                          
                 04 TS-GF04-NLIEU            PIC X(3).                          
                 04 TS-GF04-NSOC             PIC X(3).                          
                 04 TS-GF04-DSOUHAITEE       PIC X(8).                          
                 04 TS-GF04-DETAT            PIC X(8).                          
                 04 TS-GF04-DCREATION        PIC X(8).                          
                 04 TS-GF04-QSOUHAITEE       PIC 9(5).                          
                 04 TS-GF04-NCDE             PIC X(7).                          
                 04 TS-GF04-NENTCDE          PIC X(5).                          
                 04 TS-GF04-NSEQ             PIC X(7).                          
                 04 TS-GF04-CSTATUT          PIC X(1).                          
                 04 TS-GF04-NDEPOT           PIC X(3).                          
                 04 TS-GF04-NSOCLIVR         PIC X(3).                          
                 04 TS-GF04-CHEFPROD         PIC X(5).                          
                 04 TS-GF04-CINTERLOCUT      PIC X(5).                          
                 04 TS-GF04-CSTATUT-ORIG     PIC X.                             
                 04 TS-GF04-CSTATUT-PREC     PIC X.                             
                 04 TS-GF04-CFEXT            PIC X.                             
                 04 TS-GF04-CAPPRO           PIC X(3).                          
      *                                                                         
                                                                                
