      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ VALIDATION BDD KESA                                  
      *                                                                         
      *   LE MESSAGE MESAI02 SE DECOMPOSE EN 3 PARTIES:                         
      *    - MES-ENTETE    (TAILLE FIXE :  115 ) DEBUT POSITION 0               
      *    - MES-COMGA50   (TAILLE FIXE : 2885 ) DEBUT POSITION 115             
      *    - MES-TS        (TAILLE VARI : XXXX ) DEBUT POSITION 2885            
      *                    (MAX TAILLE TS=25000)                                
      *                                                                         
      *****************************************************************         
PH1TI * MODIF 09/08/02 DSA057 CORRECTION ANOMALIE TEST INTEGRATION      00010001
CT10  * MODIF 10/10/02 DSA056 CUSINE X(1) + NOUVEAU CUSINEVRAI X(5)             
CT04  * MODIF 04/12/02 DSA056 AJOUT FLAG-GA31 POUR VIDER RTGA31 SI ON           
      * PASSE D'UN PRODUIT SIMPLE � UN MULTICOMPOSANT                           
      *                                                                         
      * VERSION POUR MAI02 AVEC PUT ET GET EXTERNES                             
E0005 ******************************************************************        
      * DSA057 08/09/03 SUPPORT EVOLUTION                                       
      *                 GESTION EAN MIXTE                                       
      *                 PASSAGE MONO A MULTI COMPOSANT                          
           10  WS-MESSAGE-ENVOI.                                                
      *       MES ENTETE - 105                                                  
              15 MES-ENTETE.                                                    
               20 MES-TYPE       PIC X(3).                                      
               20 MES-NSOCMSG    PIC X(3).                                      
               20 MES-NLIEUMSG   PIC X(3).                                      
               20 MES-NSOCDST    PIC X(3).                                      
               20 MES-NLIEUDST   PIC X(3).                                      
               20 MES-NORD       PIC 9(8).                                      
               20 MES-LPROG      PIC X(10).                                     
               20 MES-DJOUR      PIC X(8).                                      
               20 MES-WSID       PIC X(10).                                     
               20 MES-USER       PIC X(10).                                     
               20 MES-CHRONO     PIC 9(7).                                      
               20 MES-NBRMSG     PIC 9(7).                                      
 CRO  *        20 MES-FILLER     PIC X(30).                                     
               20 MES-NBENR      PIC 9(05).                                     
               20 MES-TAILLE     PIC 9(05).                                     
               20 MES-VERSION2   PIC X(02).                                     
               20 MES-DSYST      PIC S9(13).                                    
               20 MES-FILLER     PIC X(05).                                     
              15 MES-COMGA50.                                                   
               16 MES-ACTION PIC X(1).                                          
                   88 MES-SUPPRESSION VALUE ' '.                                
                   88 MES-CRE-MAJ     VALUE '1'.                                
      *        ZONE DE DEFINITION - 024                                         
               16 MES-GA50-FLAG.                                                
      *            TOP MISE A JOUR ---------------   24                         
                   17 MES-FLAG-OPTION0.                                         
                      20 MES-FLAG-OPT0-GA74    PIC 9.                           
                      20 MES-FLAG-OPT0-GA75    PIC 9.                           
                   17 COMM-FLAG-OPTION1.                                        
                      20 MES-FLAG-OPT1-GA51    PIC 9.                           
                      20 MES-FLAG-OPT1-GA76    PIC 9.                           
                   17 MES-FLAG-OPTION2.                                         
                      20 MES-FLAG-OPT2-GA52     PIC 9.                          
                   17 MES-FLAG-OPTION3.                                         
                      20 MES-FLAG-OPT3-GA53     PIC 9.                          
                      20 MES-FLAG-OPT3-GA77     PIC 9.                          
                   17 MES-FLAG-OPTION4.                                         
                      20 MES-FLAG-OPT4-GA55     PIC 9.                          
                      20 MES-FLAG-OPT4-GA62     PIC 9.                          
                   17 MES-FLAG-OPTION5.                                         
                      20 MES-FLAG-OPT5-GA56     PIC 9.                          
                      20 MES-FLAG-OPT5-GA78     PIC 9.                          
                   17 MES-FLAG-OPTION6.                                         
                      20 MES-FLAG-OPT6-GA57     PIC 9.                          
                      20 MES-FLAG-OPT6-GA58     PIC 9.                          
                   17 MES-FLAG-OPTION7.                                         
                      20 MES-FLAG-OPT7-GA59     PIC 9.                          
                      20 MES-FLAG-OPT7-GA79     PIC 9.                          
                   17 MES-FLAG-OPTION8.                                         
                      20 MES-FLAG-OPT8-GA60     PIC 9.                          
                   17 MES-FLAG-OPTION9.                                         
                      20 MES-FLAG-OPT9-GA61     PIC 9.                          
                      20 MES-FLAG-OPT9-GA95     PIC 9.                          
                      20 MES-FLAG-OPT9-GA96     PIC 9.                          
                   17 MES-FLAG-OPTION10.                                        
                      20 MES-FLAG-OPT10-GA63    PIC 9.                          
                   17 MES-FLAG-OPTION11.                                        
                      20 MES-FLAG-OPT11-GA67    PIC 9.                          
                   17 MES-FLAG-OPTION12.                                        
                      20 MES-FLAG-OPT12-GA97    PIC 9.                          
                   17 MES-FLAG-OPTION13.                                        
                      20 MES-FLAG-OPT13-GA98    PIC 9.                          
                   17 MES-FLAG-OPTION14.                                        
                      20 MES-FLAG-OPT14-GA68    PIC 9.                          
      *           DONNEES GESTION - 5                                           
               16 MES-GA50-GESTION.                                             
                   20 MES-CFONC             PIC X.                              
                   20 MES-CODE-CFONC        PIC X(03).                          
                   20 MES-DATE-SSAAMMJJ     PIC 9(08).                          
                   20 MES-MULTISOC-R        PIC X(01).                          
               16 MES-GA50-DATA.                                                
      *         ZONE DONNEES COMUNES - 253                                      
                17 MES-INFO-ARTICLE.                                            
                 20 MES-GA00-NCODIC       PIC X(07).                            
                 20 MES-GA00-LREFFOURN    PIC X(20).                            
                 20 MES-GA00-CFAM         PIC X(05).                            
                 20 MES-GA00-CMARQ        PIC X(05).                            
                 20 MES-GA00-DCREATION    PIC X(08).                            
                 20 MES-GA00-D1RECEPT     PIC X(08).                            
                 20 MES-GA00-DEFSTATUT    PIC X(08).                            
                 20 MES-GA00-DMAJ         PIC X(08).                            
                 20 MES-GA00-LSTATCOMP    PIC X(03).                            
                 20 MES-GA00-LREFO        PIC X(20).                            
                 20 MES-GA00-NEAN         PIC X(13).                            
                 20 MES-GA00-CCOLOR       PIC X(05).                            
CT10             20 MES-GA00-CUSINE       PIC X(01).                            
CT10             20 MES-GA00-CUSINEVRAI   PIC X(05).                            
                 20 MES-GA00-CORIGPROD    PIC X(05).                            
                 20 MES-GA49-NBCOMPO      PIC S9(2).                            
                 20 MES-GA00-CASSORT      PIC X(05).                            
                 20 MES-GA00-CAPPRO       PIC X(05).                            
                 20 MES-GA00-CEXPO        PIC X(05).                            
                 20 MES-GA00-LREFDARTY    PIC X(20).                            
                 20 MES-GA00-LCOMMENT     PIC X(50).                            
                 20 MES-GA00-WSENSVTE     PIC X(01).                            
                 20 MES-GA00-WSENSAPPRO   PIC X(01).                            
                 20 MES-GA00-NSOCDEPOT1   PIC X(03).                            
                 20 MES-GA00-NDEPOT1      PIC X(03).                            
                 20 MES-GA00-MULTISOC     PIC X(01).                            
      *         DONNEES SECTEURS - 075                                          
                17 MES-EGA74-DATA.                                              
                 20 MES-GA00-COPCO        PIC X(03).                            
                 20 MES-NCODICK           PIC X(07).                            
                 20 MES-GA00-NPROD        PIC X(15).                            
                 20 MES-GA00-CLIGPROD     PIC X(05).                            
                 20 MES-GA00-LMODBASE     PIC X(20).                            
                 20 MES-GA00-VERSION      PIC X(20).                            
      *         DONNEES GENERALES I - 25                                        
                17 MES-EGA51-DATA.                                              
                 20 MES-GA00-CGESTVTE     PIC X(03).                            
                 20 MES-GA00-WDACEM       PIC X(01).                            
                 20 MES-GA00-CTAUXTVA     PIC X(05).                            
                 20 MES-GA00-WTLMELA      PIC X(01).                            
                 20 MES-GA00-CGARANTIE    PIC X(05).                            
                 20 MES-GA00-CGARCONST    PIC X(05).                            
                 20 FILLER                PIC X(05).                            
      *         DONNEES GENERALES II - 160                                      
                17 MES-EGA76-DATA.                                              
                 20 MES-GA49-IDEPT        PIC X(03).                            
                 20 MES-GA49-ISDEPT       PIC X(03).                            
                 20 MES-GA49-ICLASS       PIC X(03).                            
                 20 MES-GA49-ISCLASS      PIC X(03).                            
                 20 MES-GA49-IATRB4       PIC X(02).                            
                 20 MES-GA49-IATRB2       PIC X(02).                            
                 20 MES-GA49-ISTYPE       PIC X(02).                            
                 20 MES-GA49-IWARGC       PIC X(02).                            
                 20 MES-GA49-I2TKTD       PIC X(60).                            
                 20 MES-GA49-I2ADVD       PIC X(60).                            
                 20 MES-GA49-I3MKDS       PIC X(15).                            
E0005 *          20 MES-FLAG-GA31         PIC X.                                
E0005 *          20 FILLER                PIC X(04).                            
E0005            20 FILLER PIC X(5).                                            
      *         STATUTS ARTICLE DARTY - 145                                     
                17 MES-EGA53-DATA.                                              
                 20 MES-GA00-MATRICE.                                           
                    25 MES-GA00-FILIALE   PIC X(03) OCCURS 10.                  
                    25 MES-GA00-FILSOC    PIC X(03) OCCURS 10.                  
                    25 MES-GA00-FILDEP    PIC X(03) OCCURS 10.                  
                    25 MES-GA00-NBFIL     PIC 9(02).                            
                 20 MES-GA00-NSOC2        PIC X(03).                            
                 20 MES-GA00-NDEP2        PIC X(03).                            
                 20 MES-GA00-NSOC3        PIC X(03).                            
                 20 MES-GA00-NDEP3        PIC X(03).                            
JAC              20 MES-GA00-WSENSAPPRO2  PIC X(01).                            
JAC              20 MES-GA00-WSENSAPPRO3  PIC X(01).                            
JAC              20 MES-WSENSAPPRO-ORIG2  PIC X(01).                            
JAC              20 MES-WSENSAPPRO-ORIG3  PIC X(01).                            
                 20 MES-GA00-CAPPRO2      PIC X(05).                            
                 20 MES-GA00-CAP2-ORIG    PIC X(05).                            
                 20 MES-GA00-CAPPRO3      PIC X(05).                            
                 20 MES-GA00-CAP3-ORIG    PIC X(05).                            
                 20 MES-GA00-LAPPRO2      PIC X(20).                            
                 20 MES-GA00-LAPPRO3      PIC X(20).                            
                 20 MES-GA00-CEXPO2       PIC X(05).                            
                 20 MES-GA00-CEXPO3       PIC X(05).                            
                 20 MES-GA00-LEXPO2       PIC X(20).                            
                 20 MES-GA00-LEXPO3       PIC X(20).                            
                 20 MES-GA00-LSTATCOMP2   PIC X(03).                            
                 20 MES-GA00-LSTATCOMP3   PIC X(03).                            
                 20 MES-GA00-DEPSUP3      PIC X(01).                            
                 20 MES-GA53-CASSORT      PIC X(05).                            
                 20 FILLER                PIC X(29).                            
      *         STATUTS ARTICLE COMET - 110                                     
                17 MES-EGA77-DATA.                                              
                 20 MES-GA49-IRPLCD         PIC X(01).                          
                 20 MES-GA49-IATRB3         PIC X(02).                          
                 20 MES-GA49-I3WKMX         PIC S9(02).                         
                 20 MES-GA77-EXPO-COMET     PIC X(01).                          
                 20 MES-GA77-TAB1         OCCURS 30.                            
                    25 MES-GA48-SPPRTP PIC X(02).                               
                    25 MES-GA48-SPSTAT PIC X(01).                               
                 20 FILLER                  PIC X(13).                          
      *         DONNEES APPRO - 17                                              
                17 MES-EGA55-DATA.                                              
                 20 MES-GA00-CHEFPROD       PIC X(05).                          
                 20 MES-GA00-QDELAIAPPRO    PIC X(03).                          
                 20 MES-GA00-QCOLICDE       PIC S9(3).                          
                 20 MES-GA00-WSTOCKAVANCE   PIC X(01).                          
                 20 FILLER                  PIC X(05).                          
      *         DONNEES ENTREPOT  I - 56                                        
                17 MES-EGA56-DATA.                                              
                 20  MES-GA00-CCOTEHOLD    PIC X(01).                           
                 20  MES-GA00-CMODSTOCK1   PIC X(05).                           
                 20  MES-GA00-WMODSTOCK1   PIC X(01).                           
                 20  MES-GA00-CMODSTOCK2   PIC X(05).                           
                 20  MES-GA00-WMODSTOCK2   PIC X(01).                           
                 20  MES-GA00-CMODSTOCK3   PIC X(05).                           
                 20  MES-GA00-WMODSTOCK3   PIC X(01).                           
                 20  MES-GA00-CZONEACCES   PIC X(01).                           
                 20  MES-GA00-QNBPRACK     PIC S9(03).                          
                 20  MES-GA00-QNBPVSOL     PIC S9(03).                          
                 20  MES-GA00-CCONTENEUR   PIC X(01).                           
                 20  MES-GA00-QNBRANMAIL   PIC S9(03).                          
                 20  MES-GA00-CSPECIFSTK   PIC X(01).                           
                 20  MES-GA00-QNBNIVGERB   PIC S9(03).                          
                 20  MES-GA00-WCROSSDOCK   PIC X(01).                           
                 20  MES-FL50-CUNITRECEPT  PIC X(03).                           
                 20  MES-FL40-CUNITVTE     PIC X(03).                           
                 20  MES-GA00-CQUOTA       PIC X(05).                           
                 20  MES-GA00-SYSMEC       PIC X(01).                           
                 20  MES-GA00-CLASSE       PIC X(01).                           
                 20  FILLER                PIC X(04).                           
      *         DONNEES ENTREPOT II - 128                                       
                17 MES-EGA78-DATA.                                              
                 20  MES-GA00-LEMBALLAGE      PIC X(50).                        
                 20  MES-GA00-CTYPCONDT       PIC X(05).                        
                 20  MES-GA00-QCONDT          PIC S9(05).                       
                 20  MES-GA00-QCOLIRECEPT     PIC S9(05).                       
                 20  MES-GA00-QCOLIDSTOCK     PIC S9(05).                       
                 20  MES-GA00-CFETEMPL        PIC X(01).                        
                 20  MES-GA00-QCOLIVTE        PIC S9(05).                       
                 20  MES-GA00-QPOIDS          PIC S9(07).                       
                 20  MES-GA00-QLARGEUR        PIC S9(03).                       
                 20  MES-GA00-QPROFONDEUR     PIC S9(03).                       
                 20  MES-GA00-QHAUTEUR        PIC S9(03).                       
                 20  MES-GA00-QPOIDSDE        PIC S9(07).                       
                 20  MES-GA00-QLARGEURDE      PIC S9(03).                       
                 20  MES-GA00-QPROFONDEURDE   PIC S9(03).                       
                 20  MES-GA00-QHAUTEURDE      PIC S9(03).                       
                 20  MES-FL50-QCOUCHPAL       PIC S9(03).                       
                 20  MES-FL50-QCARTCOUCH      PIC S9(03).                       
                 20  MES-FL50-QBOXCART        PIC S9(07).                       
                 20  MES-FL50-QPRODBOX        PIC S9(07).                       
MAINT-*          20  FILLER                   PIC X(15).                        
                 20  MESAD02R-GA00-QLARGDE    PIC S9(4)V9.                      
                 20  MESAD02R-GA00-QPROFDE    PIC S9(4)V9.                      
-MAINT           20  MESAD02R-GA00-QHAUTDE    PIC S9(4)V9.                      
      *         DONNEES LIENS ENTRE ARTICLE - 25                                
      *         17 MES-EGA57-58-DATA.                                           
      *          20  MES-GA57-CODICLIE        PIC X(07).                        
      *          20  MES-GA57-WDEGRELIB       PIC X(02).                        
      *          20  MES-GA57-LIENLIE         PIC X(05).                        
      *          20  MES-GA57-CTYPLIEN        PIC X(05).                        
      *          20  MES-GA57-LTYPLIEN        PIC X(20).                        
      *          20  MES-GA57-LZONEPV         PIC X(20).                        
      *          20  MES-GA57-GROUPE          PIC X.                            
      *              88 MES-GA57-DEJA-RECU    VALUE '1'.                        
      *              88 MES-GA57-NON-RECU     VALUE '0'.                        
      *          20  FILLER                   PIC X(05).                        
      *         PRIX DE VENTE DARTY - 19                                        
                17 MES-EGA59-DATA.                                              
                 20 MES-GA00-PRAR       PIC S9(7)V9(2).                         
                 20 MES-GA00-PBF        PIC S9(7)V9(2).                         
      *         DONNEES ETIQUETTES INFORMATIVES - 48                            
                17 MES-EGA61-DATA.                                              
                 20  MES-GA61-FAMCLT          PIC X(30).                        
                 20  MES-GA61-DMAJ            PIC X(08).                        
                 20  MES-GA61-WPROFIL-COUL    PIC X.                            
                     88 MES-GA61-PROFIL-COULEUR    VALUE 'O'.                   
                 20  FILLER                   PIC X(05).                        
      *         DONNEES ETIQUETTES INFORMATIVES COMET - 256                     
                17 MES-EGA95-DATA.                                              
      *          20  MES-GA49-I2TKTD          PIC X(60).                        
      *          20  MES-GA49-I2ADVD          PIC X(60).                        
      *          20  MES-GA49-I3MKDS          PIC X(15).                        
                 20  MES-GA34-TKTID           PIC X(25).                        
                 20  MES-GA34-TKTQTY          PIC 9(03).                        
                 20  MES-GA34-RECICN          PIC X(20).                        
                 20  MES-GA34-INVFLG          PIC X(01).                        
                 20  MES-GA34-FRCFLG          PIC X(01).                        
                 20  MES-GA34-STKTKT          PIC X(01).                        
                 20  MES-GA34-RSNTXT          PIC X(200).                       
                 20  FILLER                   PIC X(05).                        
      *         DONNEES VENTE - 18                                              
                17 MES-EGA63-DATA.                                              
                 20  MES-GA00-WLCONF          PIC X(01).                        
                 20  MES-GA00-QCONTENU        PIC S9(05).                       
                 20  MES-GA00-QGRATUITE       PIC S9(05).                       
                 20  MES-GA00-CFETIQINFO      PIC X(01).                        
                 20  MES-GA00-CFETIQPRIX      PIC X(01).                        
                 20  FILLER                   PIC X(05).                        
      *         PRESTATIONS LIEES - 23                                          
                17 MES-EGA67-DATA.                                              
                 20 MES-GA67-CPREST       PIC X(05).                            
                 20 MES-GA67-CFOURN       PIC X(05).                            
                 20 MES-GA67-DEFFET       PIC X(08).                            
                 20 FILLER                PIC X(05).                            
PH1TI-*         DONNEES OUBLIEES 1                                              
                17 MES-GA00-AUTRE.                                              
-PH1TI              20 MES-GA00-WRESFOURN PIC X.                                
      *       ZONE TS                                                           
              15 MES-INIT-TS.                                                   
      *        NBRE ENREGITREMENT PAR TS                                        
               16 MES-INIT-NBENR-TS.                                            
                  18 MES-TS-GA51-NBENR PIC S9(5).                               
                  18 MES-TS-GA52-NBENR PIC S9(5).                               
                  18 MES-TS-GA53-NBENR PIC S9(5).                               
                  18 MES-TS-GA74-NBENR PIC S9(5).                               
                  18 MES-TS-GA54-NBENR PIC S9(5).                               
                  18 MES-TS-GA55-NBENR PIC S9(5).                               
                  18 MES-TS-GA58-NBENR PIC S9(5).                               
      *           18 MES-TS-GA59-NBENR PIC S9(5).                               
                  18 MES-TS-GA60-NBENR PIC S9(5).                               
                  18 MES-TS-GA61-NBENR PIC S9(5).                               
                  18 MES-TS-GA63-NBENR PIC S9(5).                               
                  18 MES-TS-GA64-NBENR PIC S9(5).                               
                  18 MES-TS-GA76-NBENR PIC S9(5).                               
                  18 MES-TS-GA98-NBENR PIC S9(5).                               
      *        ZONE DE TAILLE VARIABLE (DONNEES DE CHAQUE TS)                   
               15 MES-TS-DATA.                                                  
      *         TS-GA51 - 65                                                    
                17 MES-TSGA51.                                                  
                   18 MES-TS-GA51-DATA OCCURS 0 TO 10                           
                                       DEPENDING  ON MES-TS-GA51-NBENR.         
                      20 MES-TS-GA51-TOPMAJ           PIC X(1).                 
      *               20 TS-GA51-NCODIC               PIC X(7).                 
                      20 MES-TS-GA51-CTYPDCL          PIC X(5).                 
      *         TS-GA52 - 185                                                   
                17 MES-TSGA52.                                                  
                   18 MES-TS-GA52-DATA OCCURS 0 TO 20                           
                                       DEPENDING  ON MES-TS-GA52-NBENR.         
                      20 MES-TS-GA52-TOPMAJ           PIC X(1).                 
      *               20 MES-TS-GA52-NCODIC           PIC X(7).                 
                      20 MES-TS-GA52-CGARANTCPLT      PIC X(5).                 
                      20 MES-TS-GA52-CVGARANCPLT      PIC X(3).                 
      *         TS-GA53 - 852                                                   
                17 MES-TSGA53.                                                  
                   18 MES-TS-GA53-DATA OCCURS 0   TO 77                         
                                       DEPENDING  ON MES-TS-GA53-NBENR.         
      *               20 MES-TS-GA53-NCODIC       PIC X(7).                     
                      20 MES-TS-GA53-CDESCRIPTIF  PIC X(5).                     
                      20 MES-TS-GA53-CVDESCRIPT   PIC X(5).                     
                      20 MES-TS-GA53-CMAJ         PIC X(1).                     
      *               20 MES-TS-GA53-LDESCRIPTIF  PIC X(20).                    
      *               20 MES-TS-GA53-LVDESCRIPT   PIC X(20).                    
      *         TS-GA74 - 150                                                   
                17 MES-TSGA74.                                                  
                   18 MES-TS-GA74-DATA OCCURS 0   TO 25                         
                                       DEPENDING  ON MES-TS-GA74-NBENR.         
                      20 MES-TS-GA74-CDEST        PIC X(5).                     
                      20 MES-TS-GA74-CMAJ         PIC X(1).                     
      *         TS-GA54 - 14405                                                 
                17 MES-TSGA54.                                                  
                   18 MES-TS-GA54-DATA OCCURS 0   TO 600                        
                                       DEPENDING  ON MES-TS-GA54-NBENR.         
                        20 MES-TS-GA54-EXIST         PIC X(1).                  
                        20 MES-TS-GA54-NSOC          PIC X(3).                  
                        20 MES-TS-GA54-NLIEU         PIC X(3).                  
                        20 MES-TS-GA54-CEXPO         PIC X(5).                  
                        20 MES-TS-GA54-WASSOS        PIC X(1).                  
                        20 MES-TS-GA54-WASSOR        PIC X(1).                  
      *                 20 MES-TS-GA54-LLIEU         PIC X(20).                 
                        20 MES-TS-GA54-QEXPO         PIC X(5).                  
                        20 MES-TS-GA54-QLS           PIC X(5).                  
      *                 20 FILLER                    PIC X(4).                  
      *         TS-GA55 - 201                                                   
                17 MES-TSGA55.                                                  
                   18 MES-TS-GA55-DATA OCCURS 0   TO 28                         
                                       DEPENDING  ON MES-TS-GA55-NBENR.         
      *               20 MES-TS-GA55-NCODIC          PIC X(7).                  
                      20 MES-TS-GA55-NENTCDE         PIC X(5).                  
                      20 MES-TS-GA55-WENTCDE         PIC X(1).                  
                      20 MES-TS-GA55-CMAJ            PIC X(1).                  
      *               20 MES-TS-GA55-LENTCDE PIC X(20).                         
      *         TS-GA58 - 2970/11880                                            
                17 MES-TSGA58.                                                  
                   18 MES-TS-GA58-DATA OCCURS 0 TO 110                          
                                       DEPENDING  ON MES-TS-GA58-NBENR.         
MAINT                 20 MES-TS-GA58-NCODIC        PIC X(07).                   
                      20 MES-TS-GA58-NCODICLIE     PIC X(07).                   
                      20 MES-TS-GA58-TYPLIE        PIC X(05).                   
                      20 MES-TS-GA58-WDEGRELIB     PIC X(05).                   
                      20 MES-TS-GA58-QLIEN         PIC 9(02).                   
                      20 MES-TS-GA58-CMAJ          PIC X(1).                    
      *               DONNEES SPECIFIQUE PROG TGA58                             
      *               20 MES-TS-GA58-MCLIENSI      PIC X(05).                   
      *               20 MES-TS-GA58-MCLIBI        PIC X(05).                   
      *               20 MES-TS-GA58-MNCODIQI      PIC X(07).                   
      *               20 MES-TS-GA58-MNFAMSI       PIC X(05).                   
      *               20 MES-TS-GA58-MNMARQSI      PIC X(05).                   
      *               20 MES-TS-GA58-MLREFSI       PIC X(20).                   
      *               20 MES-TS-GA58-MPPVASI       PIC X(10).                   
      *               20 MES-TS-GA58-MPPVNSI       PIC X(10).                   
      *               20 MES-TS-GA58-MDDATEPI      PIC X(06).                   
      *               20 MES-TS-GA58-D1RECEPT      PIC X(08).                   
                      20 MES-TS-GA58-NCODICGRP     PIC X(07).                   
      *         TS-GA60 - 87                                                    
                17 MES-TSGA60.                                                  
                   18 MES-TS-GA60-DATA OCCURS 0   TO 1                          
                                       DEPENDING  ON MES-TS-GA60-NBENR.         
                      20 MES-TS-GA60-CMAJ   PIC  X.                             
      *               20 MES-TS-GA60-NCODIC PIC X(7).                           
                      20 MES-TS-GA60-NMESSDEPOT PIC S9(3).                      
                      20 MES-TS-GA60-LMESSDEPOT PIC X(78).                      
      *         TS-GA61 - 665                                                   
                17 MES-TSGA61.                                                  
                   18 MES-TS-GA61-DATA OCCURS 0   TO 10                         
                                       DEPENDING  ON MES-TS-GA61-NBENR.         
                     19 MES-TS-GA61-PARTIE1.                                    
      *               20 MES-TS-GA61-NCODIC       PIC X(7).                     
                      20 MES-TS-GA61-NTITRE       PIC X(5).                     
                      20 MES-TS-GA61-LTEXTE       PIC X(40).                    
      *               20 MES-TS-GA61-LGMAX        PIC 9(2).                     
      *               20 MES-TS-GA61-LIBELLE.                                   
      *                       25 MES-TS-GA61-LRUBRIQ      PIC X(10).            
      *                       25 MES-TS-GA61-FILLER       PIC X(25).            
                      20 MES-TS-GA61-COULEUR.                                   
                              25 MES-TS-GA61-CCOULTEXTE   PIC X(5).             
                              25 MES-TS-GA61-NDEBTEXTE    PIC X(2).             
                              25 MES-TS-GA61-NFINTEXTE    PIC X(2).             
                              25 MES-TS-GA61-CCOULFOND    PIC X(5).             
                              25 MES-TS-GA61-NDEBFOND     PIC X(2).             
                              25 MES-TS-GA61-LGRFOND      PIC X(4).             
                      20 MES-TS-GA61-CMAJ                 PIC X(1).             
      *         TS-GA63 - 123                                                   
                17 MES-TSGA63.                                                  
                   18 MES-TS-GA63-DATA OCCURS 0   TO 20                         
                                       DEPENDING  ON MES-TS-GA63-NBENR.         
                      20 MES-TS-GA63-TABLE.                                     
      *                  25 MES-TS-GA63-NCODIC PIC X(7).                        
                         25 MES-TS-GA63-CARACTSPE PIC X(5).                     
                         25 MES-TS-GA63-CMAJ  PIC X(1).                         
      *               20 MES-TS-GA63-LCARACTSPE PIC X(20).                      
      *               20 MES-TS-GA63-FILLER   PIC X(7).                         
      *         TS-GA64 - 165                                                   
                17 MES-TSGA64.                                                  
                   18 MES-TS-GA64-DATA OCCURS 0   TO 40                         
                                       DEPENDING  ON MES-TS-GA64-NBENR.         
                      20 MES-TS-GA64-TABLE.                                     
      *                  25 MES-TS-GA64-NCODIC PIC X(7).                        
                         25 MES-TS-GA64-CMODDEL PIC X(3).                       
                         25 MES-TS-GA64-CMAJ  PIC X(1).                         
      *               20 MES-TS-GA64-LMODDEL  PIC X(20).                        
      *               20 MES-TS-GA64-FILLER   PIC X(9).                         
      *         TS-GA76 - 2500                                                  
                17 MES-TSGA76.                                                  
                   18 MES-TS-GA76-DATA OCCURS 0   TO 50                         
                                       DEPENDING  ON MES-TS-GA76-NBENR.         
                      20 MES-TS-GA76-TOPMAJ       PIC X(1).                     
      *               20 MES-TS-GA76-NCODIC       PIC X(7).                     
                      20 MES-TS-GA76-NSEQUENCE    PIC 9(2).                     
                      20 MES-TS-GA76-NEANCOMPO    PIC X(13).                    
                      20 MES-TS-GA76-LNEANCOMPO   PIC X(15).                    
                      20 MES-TS-GA76-WAUTHENT     PIC X(1).                     
                      20 MES-TS-GA76-LIDENTIFIANT PIC X(10).                    
                      20 MES-TS-GA76-WACTIF       PIC X(1).                     
                      20 MES-TS-GA76-DACTIF       PIC X(8).                     
      *         TS-GA98 - 03200                                                 
                17 MES-TSGA98.                                                  
                   18 MES-TS-GA98-DATA OCCURS 0   TO 50                         
                                       DEPENDING  ON MES-TS-GA98-NBENR.         
                      20 MES-TS-GA98-TOPMAJ        PIC X.                       
                      20 MES-TS-GA98-ENR.                                       
      *                  25 MES-TS-GA98-NCODIC PIC X(7).                        
                         25 MES-TS-GA98-NSEQUENCE  PIC S9(2).                   
                         25 MES-TS-GA98-EAN.                                    
                            26 MES-TS-GA98-NEANCOMPO PIC X(13).                 
                            26 MES-TS-GA98-WACTIF  PIC X.                       
                            26 MES-TS-GA98-LNEANCOMPO PIC X(15).                
                            26 MES-TS-GA98-WAUTHENT PIC X.                      
                         25 MES-TS-GA98-LIDENTIFIANT PIC X(10).                 
                         25 MES-TS-GA98-DACTIF     PIC X(8).                    
                         25 MES-TS-GA98-DSYST      PIC S9(13).                  
                                                                                
