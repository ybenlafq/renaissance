      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGF3701                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGF3700                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF3701.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF3701.                                                            
      *}                                                                        
           02  GF37-DANNEE                                                      
               PIC X(0004).                                                     
           02  GF37-DSEMAINE                                                    
               PIC X(0002).                                                     
           02  GF37-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  GF37-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GF37-CRAYON                                                      
               PIC X(0005).                                                     
           02  GF37-CMODSTOCK                                                   
               PIC X(0005).                                                     
           02  GF37-QPREV1                                                      
               PIC S9(7) COMP-3.                                                
           02  GF37-QCDE1                                                       
               PIC S9(7) COMP-3.                                                
           02  GF37-QPREV2                                                      
               PIC S9(7) COMP-3.                                                
           02  GF37-QCDE2                                                       
               PIC S9(7) COMP-3.                                                
           02  GF37-QPREV3                                                      
               PIC S9(7) COMP-3.                                                
           02  GF37-QCDE3                                                       
               PIC S9(7) COMP-3.                                                
           02  GF37-QPREV4                                                      
               PIC S9(7) COMP-3.                                                
           02  GF37-QCDE4                                                       
               PIC S9(7) COMP-3.                                                
           02  GF37-QPREV5                                                      
               PIC S9(7) COMP-3.                                                
           02  GF37-QCDE5                                                       
               PIC S9(7) COMP-3.                                                
           02  GF37-QPREV6                                                      
               PIC S9(7) COMP-3.                                                
           02  GF37-QCDE6                                                       
               PIC S9(7) COMP-3.                                                
           02  GF37-QPREV7                                                      
               PIC S9(7) COMP-3.                                                
           02  GF37-QCDE7                                                       
               PIC S9(7) COMP-3.                                                
           02  GF37-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GF37-CQUOTA                                                      
               PIC X(0005).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGF3700                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGF3701-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGF3701-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-DANNEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-DANNEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-DSEMAINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-DSEMAINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-CRAYON-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-CRAYON-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-CMODSTOCK-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-CMODSTOCK-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-QPREV1-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-QPREV1-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-QCDE1-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-QCDE1-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-QPREV2-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-QPREV2-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-QCDE2-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-QCDE2-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-QPREV3-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-QPREV3-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-QCDE3-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-QCDE3-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-QPREV4-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-QPREV4-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-QCDE4-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-QCDE4-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-QPREV5-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-QPREV5-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-QCDE5-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-QCDE5-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-QPREV6-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-QPREV6-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-QCDE6-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-QCDE6-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-QPREV7-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-QPREV7-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-QCDE7-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-QCDE7-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GF37-CQUOTA-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GF37-CQUOTA-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
