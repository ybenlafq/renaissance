      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      * COMMAREA MODULE MAI04 ENVOI GENERALISE                                  
       01 Z-COMMAREA-MAI04.                                                     
      *    VARIABLE GESTION LONG 9+10                                           
           05 COMM-MAI04-GESTION.                                               
               10 COMM-MAI04-CNOMPGRM   PIC  X(05).                             
               10 COMM-MAI04-CODLANG    PIC  X(02).                             
               10 COMM-MAI04-CODPIC     PIC  X(02).                             
               10 FILLER                PIC  X(10).                             
      *    VARIABLE ENTETE ENVOI MQ LONG 28+10                                  
           05 COMM-MAI04-ENTETE.                                                
               10 COMM-MAI04-NSOC       PIC  X(03).                             
               10 COMM-MAI04-NLIEU      PIC  X(03).                             
               10 COMM-MAI04-CSOCDEST   PIC  X(03).                             
               10 COMM-MAI04-NSOCDEST   PIC  X(03).                             
               10 COMM-MAI04-NLIEUDEST  PIC  X(03).                             
               10 COMM-MAI04-CFONC      PIC  X(03).                             
               10 COMM-MAI04-NBENR      PIC  9(05).                             
               10 COMM-MAI04-TAILLE     PIC  9(05).                             
               10 FILLER                PIC  X(10).                             
      *    VARIABLE MESSAGE ENVOI MQ LONG 3011+10                               
           05 COMM-MAI04-MESS.                                                  
               10 COMM-MAI04-CIBLE.                                             
                   15 COMM-MAI04-NTABLE     PIC  X(10).                         
                   15 COMM-MAI04-ACTION     PIC  X(01).                         
               10 COMM-MAI04-ENR        PIC  X(3000).                           
               10 FILLER                PIC  X(10).                             
      *    VARIABLE RETOUR LONG 84                                              
           05 COMM-MAI04-RETOUR.                                                
               10 COMM-MAI04-CRET       PIC  X(04).                             
               10 COMM-MAI04-LRET       PIC  X(80).                             
      *    FILLER LONG 4096-PREC=934                                            
           05 FILLER                    PIC  X(934).                            
                                                                                
