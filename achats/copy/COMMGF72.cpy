      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      * COMMAREA SPECIFIQUE MODULE D'ENVOI DONNEES DE NCG VERS EDI *            
      * BLS A CALER                                                *            
      **************************************************************            
      *                                                                         
      *   PROGRAMME MGF72                                                       
      *                                                                         
       01 COMM-MGF72-APPLI.                                                     
          02 COMM-MGF72-ENTREE.                                                 
             05 COMM-MGF72-PGRM          PIC X(5).                              
             05 COMM-MGF72-CODLANG       PIC X(2).                              
             05 COMM-MGF72-CODPIC        PIC X(2).                              
             05 COMM-MGF72-DJOUR         PIC X(8).                              
             05 COMM-MGF72-NCDE          PIC X(7).                              
             05 COMM-MGF72-NENTCDE       PIC X(5).                              
             05 COMM-MGF72-TERMINAL      PIC X(4).                              
          02 COMM-MGF72-SORTIE.                                                 
             05 COMM-MGF72-MESSAGE.                                             
                10 COMM-MGF72-CODRET     PIC X(1).                              
                   88 COMM-MGF72-OK          VALUE ' '.                         
                   88 COMM-MGF72-ERR-NBLOC   VALUE '0'.                         
                   88 COMM-MGF72-ERR         VALUE '1'.                         
                10 COMM-MGF72-LIBERR     PIC X(58).                             
          02  FILLER                     PIC X(50).                             
                                                                                
