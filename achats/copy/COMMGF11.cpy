      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00001000
      *     COMMAREA LISTE DES COMMANDES FOURNISSEURS EN INSTANCE     * 00010000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00020000
      *                                                                 00030000
           02  COMM-GF11 REDEFINES COMM-GF00-APPLI.                     00730000
      *                                                                 00791001
               03  COMM-GF11-PAGIN.                                     00792001
                   05  COMM-GF11-TOP       PIC X.                       00793001
                   05  COMM-GF11-PAGPREC   PIC S9(3) COMP-3.            00794001
                   05  COMM-GF11-PAGENC    PIC S9(3) COMP-3.            00795001
                   05  COMM-GF11-PAGSUI    PIC S9(3) COMP-3.            00796001
                   05  COMM-GF11-NLIG-PAG  PIC S9(3) COMP-3.            00797001
                   05  COMM-GF11-NLIG-MAX  PIC S9(3) COMP-3.            00798001
                   05  COMM-GF11-NPAGE     PIC S9(3) COMP-3.            00799002
      *                                                                 00800000
                                                                                
