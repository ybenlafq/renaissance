      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TGR40 (TGR00 -> MENU)    TR: GR00  *    00002200
      *               GESTION DES RECEPTIONS                       *    00002300
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3710  00009000
      *                                                                 00410100
      *    TRANSACTION GR40 : GESTION DES RECEPTIONS FOURNISSEUR   *    00411000
      *                              OPTION MAJ                    *    00411100
      **************************************************************    00411201
      *                                                                 00412000
          02 COMM-GR40-APPLI REDEFINES COMM-GR00-APPLI.                 00420000
      *------------------------------ ZONE DONNEES TGR40                00510000
             03 COMM-GR40-DONNEES-TGR40.                                00520000
      *------------------------------ CODE FONCTION                     00521000
                04 COMM-GR40-FONCT             PIC XXX.                 00522000
      *------------------------------ CODE RECEPTION A QUAI             00550000
                04 COMM-GR40-NRECQUAI          PIC X(07).               00560000
      *------------------------------ DATE JOUR DE RECEPTION A QUAI     00560100
                04 COMM-GR40-DJRECQUAI.                                 00560201
                   05 COMM-GR40-DJRECQUAI-SS   PIC X(02).               00560401
                   05 COMM-GR40-DJRECQUAI-AA   PIC X(02).               00560501
                   05 COMM-GR40-DJRECQUAI-MM   PIC X(02).               00560601
                   05 COMM-GR40-DJRECQUAI-JJ   PIC X(02).               00560701
                04 COMM-GR40-SM-DJRECQUAI REDEFINES COMM-GR40-DJRECQUAI.00560702
                   05 COMM-GR40-DJRECQUAI-SSAA PIC X(4).                00560703
      *------------------------------ NUMERO DE ANNEE DE LA             00560801
      *                               DATE DE RECEPTION A QUAI          00560901
                04 COMM-GR40-DANNEE            PIC X(04).               00561001
      *------------------------------ NUMERO DE SEMAINE DE LA           00561002
      *                               DATE DE RECEPTION A QUAI          00561003
                04 COMM-GR40-NSEMAINE          PIC X(02).               00561004
      *------------------------------ NUMERO DE JOUR DE LA              00561005
      *                               DATE DE RECEPTION A QUAI          00561006
                04 COMM-GR40-NJOUR             PIC X(02).               00561007
      *------------------------------ CODE RECEPTION ADMINISTRATIVE     00561100
                04 COMM-GR40-NREC              PIC X(07).               00562000
      *------------------------------ NUMERO DE LIVRAISON                       
                04 COMM-GR40-NLIVRAISON        PIC X(07).                       
      *------------------------------ DATE JOUR DE RECEPTION ADM.       00581000
                04 COMM-GR40-DSAISREC          PIC X(08).               00582000
      *------------------------------ LIBELLE COMMENTAIRE               00630000
                04 COMM-GR40-LCOMMENT          PIC X(20).               00640000
      *------------------------------ CODE ENTREPOT                     00731900
                04 COMM-GR40-NDEPOT            PIC X(03).               00732000
      *------------------------------ CODE SOCIETE                      00732301
                04 COMM-GR40-NSOCLIVR          PIC X(03).               00732401
      *------------------------------ CODE ARTICLE                      00733000
                04 COMM-GR40-NCODIC            PIC X(07).               00734000
      *------------------------------ LIBELLE REF FOURNISSEUR           00735000
                04 COMM-GR40-LREFFOURN         PIC X(20).               00736000
      *------------------------------ CODE FAMILLE                      00737000
                04 COMM-GR40-CFAM              PIC X(05).               00738000
      *------------------------------ LIBELLE FAMILLE                   00738100
                04 COMM-GR40-LFAM              PIC X(20).               00738200
      *------------------------------ NUMERO DE SEQUENCE DE LA FAMILLE  00738300
                04 COMM-GR40-WSEQFAM           PIC 9(5) COMP-3.         00738400
      *------------------------------ CODE MARQUE                       00739000
                04 COMM-GR40-CMARQ             PIC X(05).               00739100
      *------------------------------ LIBELLE MARQUE                    00739200
                04 COMM-GR40-LMARQ             PIC X(20).               00739300
      *------------------------------ CODE MODE DE STOCKAGE             00739401
                04 COMM-GR40-CMODSTOCK         PIC X(05).               00739501
      *------------------------------ DATE PREMIERE RECEPTION           00739601
                04 COMM-GR40-D1RECEPT          PIC X(08).               00739701
      *------------------------------ CODE APPROVISIONNEMENT            00739702
      *                               'C' : CONTREMARQUE                00739703
                04 COMM-GR40-CAPPRO            PIC X(05).               00739704
      *------------------------------ CONDITIONNEMENT UNITAIRE          00739801
                04 COMM-GR40-QCONDT            PIC 9(5) COMP-3.         00739901
      *------------------------------ POIDS DU CODIC                    00739902
                04 COMM-GR40-QPOIDS            PIC 9(7) COMP-3.         00739903
      *------------------------------ VOLUME DU CODIC                   00739904
                04 COMM-GR40-QVOLUME           PIC 9(13) COMP-3.        00739905
      *------------------------------ QUANTITE RECEPTIONNEE SAISIE      00740001
                04 COMM-GR40-QRECSAI           PIC 9(5) COMP-3.         00740101
      *------------------------------ QUANTITE RECEPTIONNEE MODIFIEE    00740201
                04 COMM-GR40-QRECMOD           PIC 9(5) COMP-3.         00740301
      *------------------------------ QUANTITE EN LITIGE SAISIE         00740401
                04 COMM-GR40-QLITSAI           PIC 9(5) COMP-3.         00740501
      *------------------------------ QUANTITE EN LITIGE MODIFIEE       00740601
                04 COMM-GR40-QLITMOD           PIC 9(5) COMP-3.         00740701
      *------------------------------ NOMBRE DE PAGES DE LA CDE         00740901
                04 COMM-GR40-NBRPAGE           PIC 9(5) COMP-3.         00741001
      *------------------------------ NUMERO DE PAGE AFFICHE            00741101
                04 COMM-GR40-NUMPAGE           PIC 9(5) COMP-3.         00741201
      *------------------------------ NOMBRE D' ITEMS DE LA CDE         00741301
                04 COMM-GR40-NBRITEM           PIC 9(5) COMP-3.         00741401
      *------------------------------ NUMERO D' ITEM AFFICHE            00741501
                04 COMM-GR40-NUMITEM           PIC 9(5) COMP-3.         00741601
      *------------------------------ MESSAGE                           00741701
                04 COMM-GR40-MESSAGE           PIC X(68).               00741801
      *------------------------------ ZONE LIBRE                        00745700
      *      03 COMM-GR40-LIBRE          PIC X(3354).                   00746001
             03 COMM-GR40-LIBRE          PIC X(3347).                           
      ***************************************************************** 00750000
                                                                                
