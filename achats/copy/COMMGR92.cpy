      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *-------------------------------------------------------------    00000040
      *-------------------------------------------------------------    00000040
      *       PROJET WMS - LM7 - ASIS                                   00000040
      *       COPIE POUR LE MODULE MGR92                                00000040
      *       MODULE DE RECEPTION ADMINISTRATIVE AUTOMATISE             00000070
      *  LE 05/09/2012                                                  00000100
      *  ARNAUD ANGER - DSA033                                          00000110
      *-------------------------------------------------------------    00000130
      *-------------------------------------------------------------    00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 COM-GR92-LONG-COMMAREA PIC S9(4) COMP VALUE +2000.                    
      *                                                                         
      *--                                                                       
       01 COM-GR92-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +2000.                  
                                                                        00260000
      *}                                                                        
       01 COMM-GR92.                                                    00260000
          02 COMM-GR92-ALLER.                                           00260000
             03 COMM-GR92-CTRAITEMENT PIC X(02).                                
             03 COMM-GR92-NPROG       PIC X(08).                        00340000
             03 COMM-GR92-TERMID      PIC X(04).                        00340000
             03 COMM-GR92-USERID      PIC X(08).                        00340000
             03 COMM-GR92-DATEJOUR    PIC X(08).                                
             03 COMM-GR92-NSOCDEPOT   PIC X(03).                                
             03 COMM-GR92-NDEPOT      PIC X(03).                                
             03 COMM-GR92-NRECQUAI    PIC X(07).                                
             03 COMM-GR92-NREC        PIC X(07).                                
             03 COMM-GR92-DJRECQUAI   PIC X(08).                                
             03 COMM-GR92-DSAISREC    PIC X(08).                                
          02 COMM-GR92-DETAIL.                                                  
             03 COMM-GR92-LIGNE       OCCURS 40.                                
                05 COMM-GR92-NCODIC         PIC X(07).                          
                05 COMM-GR92-QRECQUAI-CODIC PIC S9(5) COMP-3.                   
                05 COMM-GR92-QREC           PIC S9(5) COMP-3.                   
                05 COMM-GR92-QLITIGE        PIC S9(5) COMP-3.                   
                05 COMM-GR92-NCDE           PIC X(07).                          
                05 COMM-GR92-QRECQUAI-DET   PIC S9(5) COMP-3.                   
                05 COMM-GR92-NBL            PIC X(10).                          
          02 COMM-GR92-RETOUR.                                          00260000
             03 COMM-GR92-CRETOUR     PIC X(02).                        00340000
             03 COMM-GR92-LRETOUR     PIC X(20).                                
             03 COMM-GR92-LIBELLE     PIC X(60).                                
                                                                                
