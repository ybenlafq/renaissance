      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF26   EGF26                                              00000020
      ***************************************************************** 00000030
       01   EGF26I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFONCF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCFONCI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCDEL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCDEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCDEF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCDEI    PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCDEVISEI      PIC X(3).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEVISEL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDEVISEF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLDEVISEI      PIC X(25).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTCDEL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTCDEF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNENTCDEI      PIC X(5).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTCDEL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLENTCDEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLENTCDEF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLENTCDEI      PIC X(20).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMODRGLTL     COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MCMODRGLTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCMODRGLTF     PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCMODRGLTI     PIC X(5).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMODRGLTL     COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MLMODRGLTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLMODRGLTF     PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLMODRGLTI     PIC X(20).                                 00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSAISIEL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MDSAISIEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDSAISIEF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDSAISIEI      PIC X(8).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQDELRGLTL     COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MQDELRGLTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQDELRGLTF     PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MQDELRGLTI     PIC X(3).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPJOURL     COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MCTYPJOURL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPJOURF     PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCTYPJOURI     PIC X(5).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVALIDITEL    COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MDVALIDITEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDVALIDITEF    PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDVALIDITEI    PIC X(8).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEPRGLTL     COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MCDEPRGLTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCDEPRGLTF     PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCDEPRGLTI     PIC X(5).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEPRGLTL     COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MLDEPRGLTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLDEPRGLTF     PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLDEPRGLTI     PIC X(20).                                 00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDECHEANCE1L   COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MDECHEANCE1L COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MDECHEANCE1F   PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MDECHEANCE1I   PIC X(2).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDECHEANCE2L   COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MDECHEANCE2L COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MDECHEANCE2F   PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MDECHEANCE2I   PIC X(2).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDECHEANCE3L   COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MDECHEANCE3L COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MDECHEANCE3F   PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MDECHEANCE3I   PIC X(2).                                  00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTAUXESCPTL   COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MQTAUXESCPTL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MQTAUXESCPTF   PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MQTAUXESCPTI   PIC X(5).                                  00000890
           02 M139I OCCURS   10 TIMES .                                 00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSELL  COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MWSELL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MWSELF  PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MWSELI  PIC X.                                          00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MNCODICI     PIC X(7).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MCFAMI  PIC X(5).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MCMARQI      PIC X(5).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFFOURNL  COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MLREFFOURNL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLREFFOURNF  PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MLREFFOURNI  PIC X(20).                                 00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCDEL  COMP PIC S9(4).                                 00001110
      *--                                                                       
             03 MQCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQCDEF  PIC X.                                          00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MQCDEI  PIC X(5).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRADERL     COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MPRADERL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MPRADERF     PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MPRADERI     PIC X(9).                                  00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRAL   COMP PIC S9(4).                                 00001190
      *--                                                                       
             03 MPRAL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPRAF   PIC X.                                          00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MPRAI   PIC X(9).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MZONCMDI  PIC X(15).                                      00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MLIBERRI  PIC X(58).                                      00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MCODTRAI  PIC X(4).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MCICSI    PIC X(5).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MNETNAMI  PIC X(8).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MSCREENI  PIC X(4).                                       00001460
      ***************************************************************** 00001470
      * SDF: EGF26   EGF26                                              00001480
      ***************************************************************** 00001490
       01   EGF26O REDEFINES EGF26I.                                    00001500
           02 FILLER    PIC X(12).                                      00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MDATJOUA  PIC X.                                          00001530
           02 MDATJOUC  PIC X.                                          00001540
           02 MDATJOUP  PIC X.                                          00001550
           02 MDATJOUH  PIC X.                                          00001560
           02 MDATJOUV  PIC X.                                          00001570
           02 MDATJOUO  PIC X(10).                                      00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MTIMJOUA  PIC X.                                          00001600
           02 MTIMJOUC  PIC X.                                          00001610
           02 MTIMJOUP  PIC X.                                          00001620
           02 MTIMJOUH  PIC X.                                          00001630
           02 MTIMJOUV  PIC X.                                          00001640
           02 MTIMJOUO  PIC X(5).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MNPAGEA   PIC X.                                          00001670
           02 MNPAGEC   PIC X.                                          00001680
           02 MNPAGEP   PIC X.                                          00001690
           02 MNPAGEH   PIC X.                                          00001700
           02 MNPAGEV   PIC X.                                          00001710
           02 MNPAGEO   PIC X(3).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MCFONCA   PIC X.                                          00001740
           02 MCFONCC   PIC X.                                          00001750
           02 MCFONCP   PIC X.                                          00001760
           02 MCFONCH   PIC X.                                          00001770
           02 MCFONCV   PIC X.                                          00001780
           02 MCFONCO   PIC X(3).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MNCDEA    PIC X.                                          00001810
           02 MNCDEC    PIC X.                                          00001820
           02 MNCDEP    PIC X.                                          00001830
           02 MNCDEH    PIC X.                                          00001840
           02 MNCDEV    PIC X.                                          00001850
           02 MNCDEO    PIC X(7).                                       00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MCDEVISEA      PIC X.                                     00001880
           02 MCDEVISEC PIC X.                                          00001890
           02 MCDEVISEP PIC X.                                          00001900
           02 MCDEVISEH PIC X.                                          00001910
           02 MCDEVISEV PIC X.                                          00001920
           02 MCDEVISEO      PIC X(3).                                  00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MLDEVISEA      PIC X.                                     00001950
           02 MLDEVISEC PIC X.                                          00001960
           02 MLDEVISEP PIC X.                                          00001970
           02 MLDEVISEH PIC X.                                          00001980
           02 MLDEVISEV PIC X.                                          00001990
           02 MLDEVISEO      PIC X(25).                                 00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MNENTCDEA      PIC X.                                     00002020
           02 MNENTCDEC PIC X.                                          00002030
           02 MNENTCDEP PIC X.                                          00002040
           02 MNENTCDEH PIC X.                                          00002050
           02 MNENTCDEV PIC X.                                          00002060
           02 MNENTCDEO      PIC X(5).                                  00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MLENTCDEA      PIC X.                                     00002090
           02 MLENTCDEC PIC X.                                          00002100
           02 MLENTCDEP PIC X.                                          00002110
           02 MLENTCDEH PIC X.                                          00002120
           02 MLENTCDEV PIC X.                                          00002130
           02 MLENTCDEO      PIC X(20).                                 00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MCMODRGLTA     PIC X.                                     00002160
           02 MCMODRGLTC     PIC X.                                     00002170
           02 MCMODRGLTP     PIC X.                                     00002180
           02 MCMODRGLTH     PIC X.                                     00002190
           02 MCMODRGLTV     PIC X.                                     00002200
           02 MCMODRGLTO     PIC X(5).                                  00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MLMODRGLTA     PIC X.                                     00002230
           02 MLMODRGLTC     PIC X.                                     00002240
           02 MLMODRGLTP     PIC X.                                     00002250
           02 MLMODRGLTH     PIC X.                                     00002260
           02 MLMODRGLTV     PIC X.                                     00002270
           02 MLMODRGLTO     PIC X(20).                                 00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MDSAISIEA      PIC X.                                     00002300
           02 MDSAISIEC PIC X.                                          00002310
           02 MDSAISIEP PIC X.                                          00002320
           02 MDSAISIEH PIC X.                                          00002330
           02 MDSAISIEV PIC X.                                          00002340
           02 MDSAISIEO      PIC X(8).                                  00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MQDELRGLTA     PIC X.                                     00002370
           02 MQDELRGLTC     PIC X.                                     00002380
           02 MQDELRGLTP     PIC X.                                     00002390
           02 MQDELRGLTH     PIC X.                                     00002400
           02 MQDELRGLTV     PIC X.                                     00002410
           02 MQDELRGLTO     PIC X(3).                                  00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MCTYPJOURA     PIC X.                                     00002440
           02 MCTYPJOURC     PIC X.                                     00002450
           02 MCTYPJOURP     PIC X.                                     00002460
           02 MCTYPJOURH     PIC X.                                     00002470
           02 MCTYPJOURV     PIC X.                                     00002480
           02 MCTYPJOURO     PIC X(5).                                  00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MDVALIDITEA    PIC X.                                     00002510
           02 MDVALIDITEC    PIC X.                                     00002520
           02 MDVALIDITEP    PIC X.                                     00002530
           02 MDVALIDITEH    PIC X.                                     00002540
           02 MDVALIDITEV    PIC X.                                     00002550
           02 MDVALIDITEO    PIC X(8).                                  00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MCDEPRGLTA     PIC X.                                     00002580
           02 MCDEPRGLTC     PIC X.                                     00002590
           02 MCDEPRGLTP     PIC X.                                     00002600
           02 MCDEPRGLTH     PIC X.                                     00002610
           02 MCDEPRGLTV     PIC X.                                     00002620
           02 MCDEPRGLTO     PIC X(5).                                  00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MLDEPRGLTA     PIC X.                                     00002650
           02 MLDEPRGLTC     PIC X.                                     00002660
           02 MLDEPRGLTP     PIC X.                                     00002670
           02 MLDEPRGLTH     PIC X.                                     00002680
           02 MLDEPRGLTV     PIC X.                                     00002690
           02 MLDEPRGLTO     PIC X(20).                                 00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MDECHEANCE1A   PIC X.                                     00002720
           02 MDECHEANCE1C   PIC X.                                     00002730
           02 MDECHEANCE1P   PIC X.                                     00002740
           02 MDECHEANCE1H   PIC X.                                     00002750
           02 MDECHEANCE1V   PIC X.                                     00002760
           02 MDECHEANCE1O   PIC X(2).                                  00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MDECHEANCE2A   PIC X.                                     00002790
           02 MDECHEANCE2C   PIC X.                                     00002800
           02 MDECHEANCE2P   PIC X.                                     00002810
           02 MDECHEANCE2H   PIC X.                                     00002820
           02 MDECHEANCE2V   PIC X.                                     00002830
           02 MDECHEANCE2O   PIC X(2).                                  00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MDECHEANCE3A   PIC X.                                     00002860
           02 MDECHEANCE3C   PIC X.                                     00002870
           02 MDECHEANCE3P   PIC X.                                     00002880
           02 MDECHEANCE3H   PIC X.                                     00002890
           02 MDECHEANCE3V   PIC X.                                     00002900
           02 MDECHEANCE3O   PIC X(2).                                  00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MQTAUXESCPTA   PIC X.                                     00002930
           02 MQTAUXESCPTC   PIC X.                                     00002940
           02 MQTAUXESCPTP   PIC X.                                     00002950
           02 MQTAUXESCPTH   PIC X.                                     00002960
           02 MQTAUXESCPTV   PIC X.                                     00002970
           02 MQTAUXESCPTO   PIC X(5).                                  00002980
           02 M139O OCCURS   10 TIMES .                                 00002990
             03 FILLER       PIC X(2).                                  00003000
             03 MWSELA  PIC X.                                          00003010
             03 MWSELC  PIC X.                                          00003020
             03 MWSELP  PIC X.                                          00003030
             03 MWSELH  PIC X.                                          00003040
             03 MWSELV  PIC X.                                          00003050
             03 MWSELO  PIC X.                                          00003060
             03 FILLER       PIC X(2).                                  00003070
             03 MNCODICA     PIC X.                                     00003080
             03 MNCODICC     PIC X.                                     00003090
             03 MNCODICP     PIC X.                                     00003100
             03 MNCODICH     PIC X.                                     00003110
             03 MNCODICV     PIC X.                                     00003120
             03 MNCODICO     PIC X(7).                                  00003130
             03 FILLER       PIC X(2).                                  00003140
             03 MCFAMA  PIC X.                                          00003150
             03 MCFAMC  PIC X.                                          00003160
             03 MCFAMP  PIC X.                                          00003170
             03 MCFAMH  PIC X.                                          00003180
             03 MCFAMV  PIC X.                                          00003190
             03 MCFAMO  PIC X(5).                                       00003200
             03 FILLER       PIC X(2).                                  00003210
             03 MCMARQA      PIC X.                                     00003220
             03 MCMARQC PIC X.                                          00003230
             03 MCMARQP PIC X.                                          00003240
             03 MCMARQH PIC X.                                          00003250
             03 MCMARQV PIC X.                                          00003260
             03 MCMARQO      PIC X(5).                                  00003270
             03 FILLER       PIC X(2).                                  00003280
             03 MLREFFOURNA  PIC X.                                     00003290
             03 MLREFFOURNC  PIC X.                                     00003300
             03 MLREFFOURNP  PIC X.                                     00003310
             03 MLREFFOURNH  PIC X.                                     00003320
             03 MLREFFOURNV  PIC X.                                     00003330
             03 MLREFFOURNO  PIC X(20).                                 00003340
             03 FILLER       PIC X(2).                                  00003350
             03 MQCDEA  PIC X.                                          00003360
             03 MQCDEC  PIC X.                                          00003370
             03 MQCDEP  PIC X.                                          00003380
             03 MQCDEH  PIC X.                                          00003390
             03 MQCDEV  PIC X.                                          00003400
             03 MQCDEO  PIC X(5).                                       00003410
             03 FILLER       PIC X(2).                                  00003420
             03 MPRADERA     PIC X.                                     00003430
             03 MPRADERC     PIC X.                                     00003440
             03 MPRADERP     PIC X.                                     00003450
             03 MPRADERH     PIC X.                                     00003460
             03 MPRADERV     PIC X.                                     00003470
             03 MPRADERO     PIC X(9).                                  00003480
             03 FILLER       PIC X(2).                                  00003490
             03 MPRAA   PIC X.                                          00003500
             03 MPRAC   PIC X.                                          00003510
             03 MPRAP   PIC X.                                          00003520
             03 MPRAH   PIC X.                                          00003530
             03 MPRAV   PIC X.                                          00003540
             03 MPRAO   PIC X(9).                                       00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MZONCMDA  PIC X.                                          00003570
           02 MZONCMDC  PIC X.                                          00003580
           02 MZONCMDP  PIC X.                                          00003590
           02 MZONCMDH  PIC X.                                          00003600
           02 MZONCMDV  PIC X.                                          00003610
           02 MZONCMDO  PIC X(15).                                      00003620
           02 FILLER    PIC X(2).                                       00003630
           02 MLIBERRA  PIC X.                                          00003640
           02 MLIBERRC  PIC X.                                          00003650
           02 MLIBERRP  PIC X.                                          00003660
           02 MLIBERRH  PIC X.                                          00003670
           02 MLIBERRV  PIC X.                                          00003680
           02 MLIBERRO  PIC X(58).                                      00003690
           02 FILLER    PIC X(2).                                       00003700
           02 MCODTRAA  PIC X.                                          00003710
           02 MCODTRAC  PIC X.                                          00003720
           02 MCODTRAP  PIC X.                                          00003730
           02 MCODTRAH  PIC X.                                          00003740
           02 MCODTRAV  PIC X.                                          00003750
           02 MCODTRAO  PIC X(4).                                       00003760
           02 FILLER    PIC X(2).                                       00003770
           02 MCICSA    PIC X.                                          00003780
           02 MCICSC    PIC X.                                          00003790
           02 MCICSP    PIC X.                                          00003800
           02 MCICSH    PIC X.                                          00003810
           02 MCICSV    PIC X.                                          00003820
           02 MCICSO    PIC X(5).                                       00003830
           02 FILLER    PIC X(2).                                       00003840
           02 MNETNAMA  PIC X.                                          00003850
           02 MNETNAMC  PIC X.                                          00003860
           02 MNETNAMP  PIC X.                                          00003870
           02 MNETNAMH  PIC X.                                          00003880
           02 MNETNAMV  PIC X.                                          00003890
           02 MNETNAMO  PIC X(8).                                       00003900
           02 FILLER    PIC X(2).                                       00003910
           02 MSCREENA  PIC X.                                          00003920
           02 MSCREENC  PIC X.                                          00003930
           02 MSCREENP  PIC X.                                          00003940
           02 MSCREENH  PIC X.                                          00003950
           02 MSCREENV  PIC X.                                          00003960
           02 MSCREENO  PIC X(4).                                       00003970
                                                                                
