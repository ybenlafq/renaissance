      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGF13                    TR: GF13  *    00020000
      * SAISIE DES COMMANDES FOURNISSEUR ARTICLES-QUANTITES        *    00030000
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 7421          
      *                                                                 00060000
          02 COMM-GF13-APPLI REDEFINES COMM-GF00-APPLI.                 00070000
      *                                                                 00080000
      * ZONES COMMUNES PROGRAMMES          TGF13    ------------- 7421          
      *                                    TGF13                        00100000
      *                                    TGF14                        00110000
      *                                    TGF15                        00120000
      *                                                                 00130000
      *      03 COMM-GF13-COMMUNE           PIC X(84).                  00140000
             03 COMM-GF13-COMMUNE           PIC X(85).                          
      *                                                                 00150000
      * ZONES PROPRES AU PROGRAMME         TGF13    ------------- 7337          
      *                                                                 00170000
             03 COMM-GF13-PROPRE.                                       00180000
      *------------------------------ TABLE DATE                        00190000
                04 COMM-GF13-TAB-DATE       OCCURS 4.                   00200000
                  05 COMM-GF13-DATE         PIC X(8).                   00210000
                04 COMM-GF13-ZONE-PAGINATION.                           00220000
      *------------------------------ PAGINATION                        00230000
      *------------------------------ NUMERO DE PAGE                    00240001
                   05 COMM-GF13-NPAGE       PIC 9(5) COMP-3.            00250000
      *------------------------------ DERNIER ITEM LU                   00260001
                   05 COMM-GF13-NITEM       PIC 9(5) COMP-3.            00270000
      *------------------------------ PREMIER ITEM PAGE EN COURS        00280001
                   05 COMM-GF13-NITEM-PAGE  PIC 9(5) COMP-3.            00290001
      *------------------------------ NOMBRE D'ITEM MAXIMUM             00300001
                   05 COMM-GF13-NITEMMAX-TSGF13                         00310000
                                            PIC 9(5) COMP-3.            00320000
      *------------------------------ FILLER                            00330000
      *         04 COMM-GF13-FILLER         PIC X(7293).                        
      ***************************************************************** 00350000
                                                                                
