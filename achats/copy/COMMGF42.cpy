      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE MODULE D'ENVOI DONNEES DE NCG VERS KOTT*            
      **************************************************************            
       01 COMM-MGF42-APPLI.                                                     
          02 COMM-MGF42-ENTREE.                                                 
             03 COMM-MGF42-PGRM          PIC X(5).                              
             03 COMM-MGF42-DJOUR         PIC X(8).                              
             03 COMM-MGF42-NCDE          PIC X(7).                              
             03 COMM-MGF42-NCDEK         PIC X(11).                             
             03 COMM-MGF42-NSOC          PIC X(3).                              
          02 COMM-MGF42-SORTIE.                                                 
             03 COMM-MGF42-MESSAGE.                                             
                04 COMM-MGF42-CODRET     PIC X(1).                              
                   88 COMM-MGF42-OK          VALUE ' '.                         
                   88 COMM-MGF42-ERR-NBLOC   VALUE '0'.                         
                   88 COMM-MGF42-ERR         VALUE '1'.                         
                04 COMM-MGF42-LIBERR     PIC X(58).                             
          02  FILLER                     PIC X(100).                            
                                                                                
