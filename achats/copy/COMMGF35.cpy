      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG  TGF35                   TR: GF35  *    00020000
      *           SUIVI DES COMMANDES FOURNISSEURS                 *    00030000
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ------------------------- 7354 MAX         
      *                                                                 00060000
          03 COMM-GF35-APPLI REDEFINES COMM-GF30-FILLER.                00070000
      *------------------------------ LIBELLE FAMILLE & MARQUE          00200000
              04  COMM-GF35-LFAM               PIC  X(20).              00210000
              04  COMM-GF35-LMARQ              PIC  X(20).              00220000
      *------------------------------ INDICES GESTION PAGINATION        00230000
              04  COMM-GF35-WPAGE              PIC  9(03).              00240000
              04  COMM-GF35-GPAGE              PIC  9(03).              00250000
              04  COMM-GF35-LPAGE-MAX          PIC  9(03).              00260000
              04  COMM-GF35-LPAGE              PIC  9(03).              00270000
      *------------------------------ TABLE GESTION PAGINATION          00280000
              04  COMM-GF35-TABLE-PAGINATION.                           00290000
                  05  POSTE-PAGINATION  OCCURS  100.                    00300000
                      07  COMM-GF35-COMMANDE                            00310000
                                               PIC  X(07).              00320000
                      07  COMM-GF35-DATE-SAISIE                         00330000
                                               PIC  X(08).              00340000
      *--- LIBELLES + ATTRIBUTS                                                 
             04 COMM-GF35-LIGNES.                                               
                05 COMM-GF35-LIGNE1            PIC X(3).                        
                05 COMM-GF35-LIGNE2            PIC X(3).                        
      *------------------------------ TABLE IMAGE ECRAN                 00350000
              04  COMM-GF35-STRUCTURE.                                  00360000
                  05  POSTE-LIGNE  OCCURS  12.                          00370000
                      07  COMM-GF35-STATUT     PIC  X(03).                      
                      07  COMM-GF35-NSURCDE    PIC  X(07).                      
                      07  COMM-GF35-NCDE       PIC  X(07).              00380000
                      07  COMM-GF35-DSAISIE    PIC  X(08).              00390000
                      07  COMM-GF35-DVALIDITE  PIC  X(08).              00400000
NT                    07  COMM-GF35-NDEPOT     PIC  X(03).              00401001
                      07  COMM-GF35-QCDE       PIC  X(05).              00410000
                      07  COMM-GF35-QREC       PIC  X(05).              00420000
                      07  COMM-GF35-QSOLDE     PIC  X(05).              00430000
                      07  COMM-GF35-WGENGRO    PIC  X(03).              00430100
                      07  COMM-GF35-CCOLOR     PIC  X(01).                      
                      07  COMM-GF35-CBASE      PIC  X(01).                      
      *                                                                 00460000
           EJECT                                                        00470000
      *                                                                 00480000
                                                                                
