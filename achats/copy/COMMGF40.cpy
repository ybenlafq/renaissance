      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      * COMMAREA SPECIFIQUE MODULE D'ENVOI DONN�ES DE NCG VERS JDA *            
      **************************************************************            
      *                                                                         
      *   PROGRAMME MGF40                                                       
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 COMM-MGF40-LONG-COMMAREA        PIC S9(4) COMP VALUE +900.            
      *--                                                                       
       01 COMM-MGF40-LONG-COMMAREA        PIC S9(4) COMP-5 VALUE +900.          
      *}                                                                        
       01 COMM-MGF40-APPLI.                                                     
      *                                                                         
          02 COMM-MGF40-ENTREE.                                                 
      *------------------------------------------------------------12           
             05 COMM-MGF40-PGRM          PIC X(5).                              
             05 COMM-MGF40-NTERMID       PIC X(4).                              
             05 COMM-MGF40-NSOCIETE      PIC X(3).                              
      *      DONNEES GENERALES ----------------------------- 802                
             05 COMM-MGF40-WFONC        PIC X(3).                               
             05 COMM-MGF40-NSURCDE      PIC X(7).                               
             05 COMM-MGF40-NBNCDE       PIC 9(2).                               
             05 COMM-MGF40-DETAIL-A  OCCURS 10.                                 
      *                              DEPENDING ON COMM-MGF40-NBNCDE.            
                10  COMM-MGF40-NCDE     PIC X(7).                               
                10  COMM-MGF40-NBCODIC  PIC 9(2).                               
                10  COMM-MGF40-DETAIL-B  OCCURS 10.                             
                    15 COMM-MGF40-NCODIC PIC X(7).                              
      *                                                                         
          02 COMM-MGF40-SORTIE.                                                 
      *            MESSAGE DE SORTIE ---------------------------- 62            
             05 COMM-MGF40-MESSAGE.                                             
                  10 COMM-MGF40-CODRET              PIC X(1).                   
                     88 COMM-MGF40-OK               VALUE ' '.                  
                     88 COMM-MGF40-ERR              VALUE '1'.                  
                     88 COMM-MGF40-JDA-NOK          VALUE '2'.                  
      *           10 COMM-MGF40-NSEQERR             PIC X(4).                   
                  10 COMM-MGF40-LIBERR              PIC X(58).                  
                  10 FILLER                         PIC X(3).                   
          02  FILLER                                PIC X(24).                  
      *                                                                         
                                                                                
