      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGR33   EGR33                                              00000020
      ***************************************************************** 00000030
       01   EGR33I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLSOCI    PIC X(19).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSAISIEL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MDSAISIEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDSAISIEF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDSAISIEI      PIC X(8).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDEPOTI   PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECEPL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDRECEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRECEPF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDRECEPI  PIC X(8).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIVRL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLIVRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIVRF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLIVRI    PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENTITEL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MENTITEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENTITEF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MENTITEI  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMODEL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MMODEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MMODEF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MMODEI    PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINTERL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MINTERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MINTERF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MINTERI   PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMML    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCOMML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCOMMF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCOMMI    PIC X(20).                                      00000570
           02 M137I OCCURS   8 TIMES .                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMANDEL   COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCOMMANDEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCOMMANDEF   PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCOMMANDEI   PIC X(7).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MZONCMDI  PIC X(15).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLIBERRI  PIC X(58).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCODTRAI  PIC X(4).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCICSI    PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNETNAMI  PIC X(8).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MSCREENI  PIC X(4).                                       00000860
      ***************************************************************** 00000870
      * SDF: EGR33   EGR33                                              00000880
      ***************************************************************** 00000890
       01   EGR33O REDEFINES EGR33I.                                    00000900
           02 FILLER    PIC X(12).                                      00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MDATJOUA  PIC X.                                          00000930
           02 MDATJOUC  PIC X.                                          00000940
           02 MDATJOUP  PIC X.                                          00000950
           02 MDATJOUH  PIC X.                                          00000960
           02 MDATJOUV  PIC X.                                          00000970
           02 MDATJOUO  PIC X(10).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MTIMJOUA  PIC X.                                          00001000
           02 MTIMJOUC  PIC X.                                          00001010
           02 MTIMJOUP  PIC X.                                          00001020
           02 MTIMJOUH  PIC X.                                          00001030
           02 MTIMJOUV  PIC X.                                          00001040
           02 MTIMJOUO  PIC X(5).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MPAGEA    PIC X.                                          00001070
           02 MPAGEC    PIC X.                                          00001080
           02 MPAGEP    PIC X.                                          00001090
           02 MPAGEH    PIC X.                                          00001100
           02 MPAGEV    PIC X.                                          00001110
           02 MPAGEO    PIC X(3).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MNSOCA    PIC X.                                          00001140
           02 MNSOCC    PIC X.                                          00001150
           02 MNSOCP    PIC X.                                          00001160
           02 MNSOCH    PIC X.                                          00001170
           02 MNSOCV    PIC X.                                          00001180
           02 MNSOCO    PIC X(3).                                       00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MLSOCA    PIC X.                                          00001210
           02 MLSOCC    PIC X.                                          00001220
           02 MLSOCP    PIC X.                                          00001230
           02 MLSOCH    PIC X.                                          00001240
           02 MLSOCV    PIC X.                                          00001250
           02 MLSOCO    PIC X(19).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MDSAISIEA      PIC X.                                     00001280
           02 MDSAISIEC PIC X.                                          00001290
           02 MDSAISIEP PIC X.                                          00001300
           02 MDSAISIEH PIC X.                                          00001310
           02 MDSAISIEV PIC X.                                          00001320
           02 MDSAISIEO      PIC X(8).                                  00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MDEPOTA   PIC X.                                          00001350
           02 MDEPOTC   PIC X.                                          00001360
           02 MDEPOTP   PIC X.                                          00001370
           02 MDEPOTH   PIC X.                                          00001380
           02 MDEPOTV   PIC X.                                          00001390
           02 MDEPOTO   PIC X(3).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MDRECEPA  PIC X.                                          00001420
           02 MDRECEPC  PIC X.                                          00001430
           02 MDRECEPP  PIC X.                                          00001440
           02 MDRECEPH  PIC X.                                          00001450
           02 MDRECEPV  PIC X.                                          00001460
           02 MDRECEPO  PIC X(8).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MLIVRA    PIC X.                                          00001490
           02 MLIVRC    PIC X.                                          00001500
           02 MLIVRP    PIC X.                                          00001510
           02 MLIVRH    PIC X.                                          00001520
           02 MLIVRV    PIC X.                                          00001530
           02 MLIVRO    PIC X(5).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MENTITEA  PIC X.                                          00001560
           02 MENTITEC  PIC X.                                          00001570
           02 MENTITEP  PIC X.                                          00001580
           02 MENTITEH  PIC X.                                          00001590
           02 MENTITEV  PIC X.                                          00001600
           02 MENTITEO  PIC X(5).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MMODEA    PIC X.                                          00001630
           02 MMODEC    PIC X.                                          00001640
           02 MMODEP    PIC X.                                          00001650
           02 MMODEH    PIC X.                                          00001660
           02 MMODEV    PIC X.                                          00001670
           02 MMODEO    PIC X(5).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MINTERA   PIC X.                                          00001700
           02 MINTERC   PIC X.                                          00001710
           02 MINTERP   PIC X.                                          00001720
           02 MINTERH   PIC X.                                          00001730
           02 MINTERV   PIC X.                                          00001740
           02 MINTERO   PIC X(5).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MCOMMA    PIC X.                                          00001770
           02 MCOMMC    PIC X.                                          00001780
           02 MCOMMP    PIC X.                                          00001790
           02 MCOMMH    PIC X.                                          00001800
           02 MCOMMV    PIC X.                                          00001810
           02 MCOMMO    PIC X(20).                                      00001820
           02 M137O OCCURS   8 TIMES .                                  00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MCOMMANDEA   PIC X.                                     00001850
             03 MCOMMANDEC   PIC X.                                     00001860
             03 MCOMMANDEP   PIC X.                                     00001870
             03 MCOMMANDEH   PIC X.                                     00001880
             03 MCOMMANDEV   PIC X.                                     00001890
             03 MCOMMANDEO   PIC X(7).                                  00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MZONCMDA  PIC X.                                          00001920
           02 MZONCMDC  PIC X.                                          00001930
           02 MZONCMDP  PIC X.                                          00001940
           02 MZONCMDH  PIC X.                                          00001950
           02 MZONCMDV  PIC X.                                          00001960
           02 MZONCMDO  PIC X(15).                                      00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLIBERRA  PIC X.                                          00001990
           02 MLIBERRC  PIC X.                                          00002000
           02 MLIBERRP  PIC X.                                          00002010
           02 MLIBERRH  PIC X.                                          00002020
           02 MLIBERRV  PIC X.                                          00002030
           02 MLIBERRO  PIC X(58).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCODTRAA  PIC X.                                          00002060
           02 MCODTRAC  PIC X.                                          00002070
           02 MCODTRAP  PIC X.                                          00002080
           02 MCODTRAH  PIC X.                                          00002090
           02 MCODTRAV  PIC X.                                          00002100
           02 MCODTRAO  PIC X(4).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCICSA    PIC X.                                          00002130
           02 MCICSC    PIC X.                                          00002140
           02 MCICSP    PIC X.                                          00002150
           02 MCICSH    PIC X.                                          00002160
           02 MCICSV    PIC X.                                          00002170
           02 MCICSO    PIC X(5).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MNETNAMA  PIC X.                                          00002200
           02 MNETNAMC  PIC X.                                          00002210
           02 MNETNAMP  PIC X.                                          00002220
           02 MNETNAMH  PIC X.                                          00002230
           02 MNETNAMV  PIC X.                                          00002240
           02 MNETNAMO  PIC X(8).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MSCREENA  PIC X.                                          00002270
           02 MSCREENC  PIC X.                                          00002280
           02 MSCREENP  PIC X.                                          00002290
           02 MSCREENH  PIC X.                                          00002300
           02 MSCREENV  PIC X.                                          00002310
           02 MSCREENO  PIC X(4).                                       00002320
                                                                                
