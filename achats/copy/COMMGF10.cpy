      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00000010
      * COMMAREA SPECIFIQUE PRG TGF10                    TR: FF06  *    00000020
      *     EDITION DU BON DE PREPARATION DE COMMANDE              *    00000030
      **************************************************************    00000040
      * ZONES RESERVEES APPLICATIVES ---------------------------- 7421          
      *                                                                 00000060
          02 COMM-GF10-APPLI REDEFINES COMM-GF00-APPLI.                 00000070
             03 COMM-GF10-NCDE6        PIC X(5).                        00000080
             03 COMM-GF10-NINTER6      PIC X(5).                        00000090
             03 COMM-GF10-CTYPE6       PIC X(5).                        00000091
             03 COMM-GF10-MESS         PIC X(80).                       00000093
      *      03 COMM-GF10-FILLER       PIC X(7326).                             
      ***************************************************************** 00000095
                                                                                
