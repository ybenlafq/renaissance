      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ INTERROGATION BDD KESA                               
      *                                                                         
      *                                                                         
      *****************************************************************         
      * VERSION POUR MAI74 AVEC PUT ET GET EXTERNES                             
CT10  * MODIF 10/10/02 DSA056 REMPLACEMENT CUSINE PAR CUSINEVRAI                
MAINT * MODIF 09/12/02 DSA057 REFONTE INTERFACE                                 
FT    * QLARGDE ETC                                                             
           10  WS-MESSAGE-RECU.                                                 
      *---                                                                      
           17  MESR-ENTETE.                                                     
               20   MESR-TYPE     PIC    X(3).                                  
               20   MESR-NSOCMSG  PIC    X(3).                                  
               20   MESR-NLIEUMSG PIC    X(3).                                  
               20   MESR-NSOCDST  PIC    X(3).                                  
               20   MESR-NLIEUDST PIC    X(3).                                  
               20   MESR-NORD     PIC    9(8).                                  
               20   MESR-LPROG    PIC    X(10).                                 
               20   MESR-DJOUR    PIC    X(8).                                  
               20   MESR-WSID     PIC    X(10).                                 
               20   MESR-USER     PIC    X(10).                                 
               20   MESR-CHRONO   PIC    9(7).                                  
               20   MESR-NBRMSG   PIC    9(7).                                  
 CRO  *        20   MESR-FILLER   PIC    X(30).                                 
               20   MESR-NBENR    PIC 9(05).                                    
               20   MESR-TAILLE   PIC 9(05).                                    
               20   MESR-VERSION2 PIC X(02).                                    
               20   MESR-DSYST    PIC S9(13).                                   
               20   MESR-FILLER   PIC X(05).                                    
           17  MESR-CPTEURS.                                                    
               20  MESR-NBEAN      PIC 9(3).                                    
               20  MESR-NBCDE      PIC 9(3).                                    
               20  MESR-NBPRODLIE  PIC 9(3).                                    
               20  MESR-NBGA63     PIC 9(3).                                    
      *        20  MESR-NBPRODUIT  PIC 9(3).                                    
           17  MESR-SORTIE.                                                     
      *  DONNEES OPCO                                                           
               20   MESR-OPCO.                                                  
      **    IDENTIFICATION ARTICLE -  DONNEES GENERALES 1                       
                  25  MESR-DG1.                                                 
                       30 MESR-CGESTVTE   PIC X(3).                             
                       30 MESR-CASSORT    PIC X(5).                             
                       30 MESR-WDACEM     PIC X.                                
                       30 MESR-WTLMELA    PIC X.                                
                       30 MESR-CGARANTIE  PIC X(5).                             
                       30 MESR-CGARCONST  PIC X(5).                             
CT10                   30 MESR-CUSINE     PIC X(1).                             
      **    IDENTIFICATION ARTICLE -  DONNEES GENERALES 2                       
                  25  MESR-DG2.                                                 
                       30 MESR-DEPT       PIC X(3).                             
                       30 MESR-SUBDEPT    PIC X(3).                             
                       30 MESR-CLASS      PIC X(3).                             
                       30 MESR-SUBCLASS   PIC X(3).                             
                       30 MESR-IATRB4     PIC X(2).                             
                       30 MESR-IATRB2     PIC X(2).                             
                       30 MESR-ISTYPE     PIC X(2).                             
                       30 MESR-IWARGC     PIC X(2).                             
                  25 MESR-NBCOMPO    PIC 9(2).                                  
      *   MISE � JOUR DES STATUTS ARTICLES                                      
                  25 MESR-MAJ-STATUT.                                           
                       30 MESR-LREFDARTY  PIC X(20).                            
                       30 MESR-LCOMMENT   PIC X(50).                            
                       30 MESR-IATRB3     PIC X(2).                             
                       30 MESR-CAPPRO     PIC X(3).                             
                       30 MESR-LSTATCOMP  PIC X(3).                             
                       30 MESR-IRPLCD     PIC X.                                
                       30 MESR-CEXPO      PIC X.                                
                       30 MESR-WSENSVTE   PIC X.                                
                       30 MESR-I3WKMX     PIC 9(2).                             
                       30 MESR-WSENSAPPRO PIC X.                                
      *  DEFINITION D'UN ARTICLE -  APPROVISIONNEMENT 1                         
                  25  MESR-APPRO1.                                              
                       30 MESR-CHEFPROD   PIC X(5).                             
                       30 MESR-QDELAIAPPRO PIC 9(3).                            
                       30 MESR-QCOLICDE    PIC 9(3).                            
                       30 MESR-WSTOCKAVANCE PIC X.                              
      *  DEFINITION D'UN ARTICLE -  DONNEES ENTREPOT 1                          
                  25  MESR-ENTREPOT1.                                           
                       30 MESR-NSOCDEPOT1  PIC X(3).                            
                       30 MESR-NDEPOT1     PIC X(3).                            
                       30 MESR-CCOTEHOLD   PIC X.                               
                       30 MESR-CMODSTOCK1  PIC X(5).                            
                       30 MESR-CMODSTOCK2  PIC X(5).                            
                       30 MESR-CMODSTOCK3  PIC X(5).                            
                       30 MESR-WMODSTOCK1  PIC X.                               
                       30 MESR-WMODSTOCK2  PIC X.                               
                       30 MESR-WMODSTOCK3  PIC X.                               
                       30 MESR-CZONEACCES  PIC X.                               
                       30 MESR-SYSMEC      PIC X.                               
                       30 MESR-CLASSE      PIC X.                               
                       30 MESR-QNBPRACK    PIC 9(5).                            
                       30 MESR-QNBPVSOL    PIC 9(3).                            
                       30 MESR-CCONTENEUR  PIC X.                               
                       30 MESR-QNBRANMAIL  PIC 9(2).                            
                       30 MESR-CSPECIFSTK  PIC X.                               
                       30 MESR-QNBNIVGERB  PIC 9(2).                            
                       30 MESR-WCROSSDOCK  PIC X.                               
                       30 MESR-CUNITRECEPT PIC X(3).                            
                       30 MESR-CUNITVTE    PIC X(3).                            
                       30 MESR-CQUOTA      PIC X(5).                            
      *  DEFINITION D'UN ARTICLE -  DONNEES ENTREPOT 2                          
                  25  MESR-ENTREPOT2.                                           
                       30 MESR-LEMBALLAGE  PIC X(50).                           
                       30 MESR-CTYPCONDT   PIC X(5).                            
                       30 MESR-QCONDT      PIC 9(5).                            
                       30 MESR-QCOLIRECEPT PIC 9(5).                            
                       30 MESR-QCOLIDESTOCK PIC 9(5).                           
                       30 MESR-CFETEMPL    PIC X.                               
                       30 MESR-QCOLIVTE    PIC 9(5).                            
                       30 MESR-QPOIDS      PIC 9(7).                            
                       30 MESR-QLARGEUR    PIC 9(3).                            
                       30 MESR-QPROFONDEUR PIC 9(3).                            
                       30 MESR-QHAUTEUR    PIC 9(3).                            
                       30 MESR-QPOIDSDE    PIC 9(7).                            
                       30 MESR-QLARGEURDE    PIC 9(3).                          
                       30 MESR-QPROFONDEURDE PIC 9(3).                          
                       30 MESR-QHAUTEURDE    PIC 9(3).                          
                       30 MESR-QCOUCHPAL   PIC 9(2).                            
                       30 MESR-QCARTCOUCH  PIC 9(2).                            
                       30 MESR-QBOXCART    PIC 9(4).                            
                       30 MESR-QPRODBOX    PIC 9(4).                            
                       30 MESR-QLARGDE  PIC 9(4)V9.                             
                       30 MESR-QPROFDE PIC 9(4)V9.                              
                       30 MESR-QHAUTDE  PIC 9(4)V9.                             
      *  DEFINITION D'UN ARTICLE -  DONNEES DE VENTE                            
                  25  MESR-VENTE.                                               
                       30 MESR-QCONTENU   PIC S9(5).                            
                       30 MESR-QGRATUITE  PIC S9(5).                            
                       30 MESR-WLCONF     PIC X.                                
                       30 MESR-CFETIQINFO PIC X.                                
                       30 MESR-CFETIQPRIX PIC X.                                
               20 MESR-SORTIE-MESSAGE.                                          
                 21 MESR-SORTIE-MESS.                                           
MAINT *           22 MESR-SORTIE-CODRET PIC X.                                  
      *              88 MESR-SORTIE-OK VALUE ' '.                               
      *              88 MESR-SORTIE-ERR VALUE '1'.                              
MAINT             22 MESR-SORTIE-CODRET PIC X(4).                               
MAINT *           22 MESR-SORTIE-LIBLIBRE  PIC X(20).                           
MAINT             22 MESR-SORTIE-LIBLIBRE PIC X(30).                            
MAINT-*           22 MESR-SORTIE-LIBELLE.                                       
      *              25 MESR-SORTIE-NSEQERR PIC X(4).                           
-MAINT*              25 MESR-SORTIE-NOMPGRM PIC X(6).                           
      *              25 MESR-SORTIE-LIBERR  PIC X(52).                          
MAINT *           22 MESR-CODRET     PIC X.                                     
MAINT *           22 MESR-DEPHEAN    PIC X.                                     
PH1TI *           22 MESR-NBPROD     PIC 9(3).                                  
MAINT *           22 MESR-NBPROD     PIC 9(6).                                  
MAINT             22 MESR-COUNTPROD PIC 9(7).                                   
MAINT             22 MESR-NBPROD PIC 9(7).                                      
MAINT             22 MESR-MATCH PIC 9 VALUE 0.                                  
MAINT                 88 MESR-MULTI VALUE 0.                                    
MAINT                 88 MESR-UNIQUE VALUE 1.                                   
MAINT             22 FILLER PIC X(4).                                           
MAINT *         21 MESR-SECTID OCCURS 50.                                       
MAINT           21 MESR-SECTID OCCURS 60.                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
MAINT *           22 MESR-STATUS PIC 9(3) COMP.                                 
      *--                                                                       
                  22 MESR-STATUS PIC 9(3) COMP-5.                               
      *}                                                                        
                  22  MESR-NCODICK    PIC X(7).                                 
      *           22  MESR-NCODIC     PIC X(7).                                 
                  22  MESR-CMARQ      PIC X(5).                                 
                  22  MESR-LREFO      PIC X(20).                                
      *  CE CODE BARRE NE SERA PAS TRAIT� DANS LE TGA74/MAI74                   
                  22  MESR-NEAN       PIC X(13).                                
                  22  MESR-NPROD      PIC X(15).                                
                  22  MESR-CCOLOR     PIC X(5).                                 
CT10  *           22  MESR-CUSINE     PIC X(1).                                 
CT10              22  MESR-CUSINEVRAI PIC X(5).                                 
                  22  MESR-CORIGPROD  PIC X(5).                                 
                  22  MESR-CLIGPROD   PIC X(5).                                 
                  22  MESR-LMODBASE   PIC X(20).                                
                  22  MESR-CFAM       PIC X(5).                                 
                  22  MESR-LREFFOURN  PIC X(20).                                
                  22  MESR-COPCO      PIC X(3).                                 
                  22  MESR-DCREATION  PIC X(8).                                 
                  22  MESR-WMULTISOC  PIC X.                                    
                  22  MESR-VERSION    PIC X(20).                                
                  22  MESR-NBDESCR    PIC 9(2).                                 
CRO               22  MESR-NBDEST     PIC 9(2).                                 
                  22  MESR-DESCRIPTION OCCURS 30.                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
MAINT *               25 MESR-CDESTATUS PIC 9(3) COMP.                          
      *--                                                                       
                      25 MESR-CDESTATUS PIC 9(3) COMP-5.                        
      *}                                                                        
                      25  MESR-CDESCRIPTIF   PIC X(5).                          
                      25  MESR-CVDESCRIPT    PIC X(5).                          
                  22  MESR-DESTINATION OCCURS 30.                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
MAINT *               25 MESR-DESSTATUS PIC 9(3) COMP.                          
      *--                                                                       
                      25 MESR-DESSTATUS PIC 9(3) COMP-5.                        
      *}                                                                        
                      25  MESR-CDEST         PIC X(5).                          
           17  MESR-TS.                                                         
CR    * CES DONN�ES NE SERONT PAS TRAIT�ES DANS LE TGA74-MAI74 / TGA76          
               20 MESR-CODE-BARRE OCCURS 0 TO 10 DEPENDING ON                   
                       MESR-NBEAN.                                              
                      25  MESR-GA76-NSEQUENCE   PIC X(2).                       
                      25  MESR-GA76-NEANCOMPO   PIC X(13).                      
                      25  MESR-GA76-LNEANCOMPO  PIC X(15).                      
                      25  MESR-GA76-WAUTHENT    PIC X.                          
                      25  MESR-GA76-LIDENTIFIANT PIC X(10).                     
                      25  MESR-GA76-WACTIF      PIC X.                          
                      25  MESR-GA76-DACTIF      PIC X(8).                       
               20 MESR-APPRO2 OCCURS 0 TO 20 DEPENDING ON MESR-NBCDE.           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
MAINT *               25 MESR-ENTSTATUS PIC 9(3) COMP.                          
      *--                                                                       
                      25 MESR-ENTSTATUS PIC 9(3) COMP-5.                        
      *}                                                                        
                      25 MESR-APPRO2-NCODIC PIC X(7).                           
                      25 MESR-NENTCDE     PIC 9(5).                             
                      25 MESR-WENTCDE     PIC X.                                
               20 MESR-GA63  OCCURS 0 TO 20 DEPENDING ON MESR-NBGA63.           
                      25 MESR-GA63-NCODIC PIC X(7).                             
                      25 MESR-GA63-CARACTSPE  PIC X(5).                         
               20  MESR-PRODLIE OCCURS 0 TO 20 DEPENDING ON                     
                      MESR-NBPRODLIE.                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
MAINT *               25 MESR-LIESTATUS PIC 9(3) COMP.                          
      *--                                                                       
                      25 MESR-LIESTATUS PIC 9(3) COMP-5.                        
      *}                                                                        
                      25 MESR-GA58-NCODIC PIC X(7).                             
                      25 MESR-TYPLIE      PIC X(5).                             
                      25 MESR-WDEGRELIB   PIC X(5).                             
                      25 MESR-NCODICLIE   PIC X(7).                             
                      25 MESR-QLIEN       PIC 9(3).                             
                      25 MESR-CFAMLIE     PIC X(5).                             
                      25 MESR-CMARQLIE    PIC X(5).                             
                      25 MESR-LREFFOURNLIE PIC X(5).                            
                                                                                
