      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: TGf48   TGf48                                              00000020
      ***************************************************************** 00000030
       01   EGF48I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDEPL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MSOCDEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCDEPF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSOCDEPI  PIC X(6).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDATEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDATEF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDATEI    PIC X(10).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNWEEKL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNWEEKL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNWEEKF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNWEEKI   PIC X(6).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRAYONL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCRAYONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCRAYONF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCRAYONI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMODSTOCKL    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MCMODSTOCKL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCMODSTOCKF    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCMODSTOCKI    PIC X(5).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCQUOTAL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCQUOTAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCQUOTAF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCQUOTAI  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MZONCMDI  PIC X(15).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLIBERRI  PIC X(58).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCODTRAI  PIC X(4).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCICSI    PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNETNAMI  PIC X(8).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSCREENI  PIC X(4).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEPREVJL     COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MQTEPREVJL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQTEPREVJF     PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MQTEPREVJI     PIC X(6).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUOPREVJL     COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MQUOPREVJL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQUOPREVJF     PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MQUOPREVJI     PIC X(6).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEPREVSL     COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MQTEPREVSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQTEPREVSF     PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MQTEPREVSI     PIC X(6).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUOPREVSL     COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MQUOPREVSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQUOPREVSF     PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MQUOPREVSI     PIC X(6).                                  00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEPRISJL     COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MQTEPRISJL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQTEPRISJF     PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MQTEPRISJI     PIC X(6).                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUOPRISJL     COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MQUOPRISJL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQUOPRISJF     PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MQUOPRISJI     PIC X(6).                                  00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEPRISSL     COMP PIC S9(4).                            00000940
      *--                                                                       
           02 MQTEPRISSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQTEPRISSF     PIC X.                                     00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MQTEPRISSI     PIC X(6).                                  00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUOPRISSL     COMP PIC S9(4).                            00000980
      *--                                                                       
           02 MQUOPRISSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQUOPRISSF     PIC X.                                     00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MQUOPRISSI     PIC X(6).                                  00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTERECJL      COMP PIC S9(4).                            00001020
      *--                                                                       
           02 MQTERECJL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQTERECJF      PIC X.                                     00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MQTERECJI      PIC X(6).                                  00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUORECJL      COMP PIC S9(4).                            00001060
      *--                                                                       
           02 MQUORECJL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQUORECJF      PIC X.                                     00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MQUORECJI      PIC X(6).                                  00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTERECSL      COMP PIC S9(4).                            00001100
      *--                                                                       
           02 MQTERECSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQTERECSF      PIC X.                                     00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MQTERECSI      PIC X(6).                                  00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUORECSL      COMP PIC S9(4).                            00001140
      *--                                                                       
           02 MQUORECSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQUORECSF      PIC X.                                     00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MQUORECSI      PIC X(6).                                  00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTESOLDJL     COMP PIC S9(4).                            00001180
      *--                                                                       
           02 MQTESOLDJL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQTESOLDJF     PIC X.                                     00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MQTESOLDJI     PIC X(6).                                  00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUOSOLDJL     COMP PIC S9(4).                            00001220
      *--                                                                       
           02 MQUOSOLDJL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQUOSOLDJF     PIC X.                                     00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MQUOSOLDJI     PIC X(6).                                  00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTESOLDSL     COMP PIC S9(4).                            00001260
      *--                                                                       
           02 MQTESOLDSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQTESOLDSF     PIC X.                                     00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MQTESOLDSI     PIC X(6).                                  00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQUOSOLDSL     COMP PIC S9(4).                            00001300
      *--                                                                       
           02 MQUOSOLDSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQUOSOLDSF     PIC X.                                     00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MQUOSOLDSI     PIC X(6).                                  00001330
           02 MTABI OCCURS   11 TIMES .                                 00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTCDEL    COMP PIC S9(4).                            00001350
      *--                                                                       
             03 MNENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNENTCDEF    PIC X.                                     00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MNENTCDEI    PIC X(5).                                  00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENTCDEL    COMP PIC S9(4).                            00001390
      *--                                                                       
             03 MLENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLENTCDEF    PIC X.                                     00001400
             03 FILLER  PIC X(4).                                       00001410
             03 MLENTCDEI    PIC X(15).                                 00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCDEL  COMP PIC S9(4).                                 00001430
      *--                                                                       
             03 MNCDEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNCDEF  PIC X.                                          00001440
             03 FILLER  PIC X(4).                                       00001450
             03 MNCDEI  PIC X(7).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00001470
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00001480
             03 FILLER  PIC X(4).                                       00001490
             03 MNCODICI     PIC X(7).                                  00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00001510
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00001520
             03 FILLER  PIC X(4).                                       00001530
             03 MCFAMI  PIC X(5).                                       00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMARQL  COMP PIC S9(4).                                 00001550
      *--                                                                       
             03 MMARQL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MMARQF  PIC X.                                          00001560
             03 FILLER  PIC X(4).                                       00001570
             03 MMARQI  PIC X(5).                                       00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00001590
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00001600
             03 FILLER  PIC X(4).                                       00001610
             03 MLREFI  PIC X(14).                                      00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOL   COMP PIC S9(4).                                 00001630
      *--                                                                       
             03 MQUOL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQUOF   PIC X.                                          00001640
             03 FILLER  PIC X(4).                                       00001650
             03 MQUOI   PIC X(5).                                       00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00001670
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00001680
             03 FILLER  PIC X(4).                                       00001690
             03 MQTEI   PIC X(5).                                       00001700
      ***************************************************************** 00001710
      * SDF: TGf48   TGf48                                              00001720
      ***************************************************************** 00001730
       01   EGF48O REDEFINES EGF48I.                                    00001740
           02 FILLER    PIC X(12).                                      00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MDATJOUA  PIC X.                                          00001770
           02 MDATJOUC  PIC X.                                          00001780
           02 MDATJOUP  PIC X.                                          00001790
           02 MDATJOUH  PIC X.                                          00001800
           02 MDATJOUV  PIC X.                                          00001810
           02 MDATJOUO  PIC X(10).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MTIMJOUA  PIC X.                                          00001840
           02 MTIMJOUC  PIC X.                                          00001850
           02 MTIMJOUP  PIC X.                                          00001860
           02 MTIMJOUH  PIC X.                                          00001870
           02 MTIMJOUV  PIC X.                                          00001880
           02 MTIMJOUO  PIC X(5).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MPAGEA    PIC X.                                          00001910
           02 MPAGEC    PIC X.                                          00001920
           02 MPAGEP    PIC X.                                          00001930
           02 MPAGEH    PIC X.                                          00001940
           02 MPAGEV    PIC X.                                          00001950
           02 MPAGEO    PIC X(3).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MPAGEMAXA      PIC X.                                     00001980
           02 MPAGEMAXC PIC X.                                          00001990
           02 MPAGEMAXP PIC X.                                          00002000
           02 MPAGEMAXH PIC X.                                          00002010
           02 MPAGEMAXV PIC X.                                          00002020
           02 MPAGEMAXO      PIC X(3).                                  00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MSOCDEPA  PIC X.                                          00002050
           02 MSOCDEPC  PIC X.                                          00002060
           02 MSOCDEPP  PIC X.                                          00002070
           02 MSOCDEPH  PIC X.                                          00002080
           02 MSOCDEPV  PIC X.                                          00002090
           02 MSOCDEPO  PIC X(6).                                       00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MDATEA    PIC X.                                          00002120
           02 MDATEC    PIC X.                                          00002130
           02 MDATEP    PIC X.                                          00002140
           02 MDATEH    PIC X.                                          00002150
           02 MDATEV    PIC X.                                          00002160
           02 MDATEO    PIC X(10).                                      00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MNWEEKA   PIC X.                                          00002190
           02 MNWEEKC   PIC X.                                          00002200
           02 MNWEEKP   PIC X.                                          00002210
           02 MNWEEKH   PIC X.                                          00002220
           02 MNWEEKV   PIC X.                                          00002230
           02 MNWEEKO   PIC X(6).                                       00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MCRAYONA  PIC X.                                          00002260
           02 MCRAYONC  PIC X.                                          00002270
           02 MCRAYONP  PIC X.                                          00002280
           02 MCRAYONH  PIC X.                                          00002290
           02 MCRAYONV  PIC X.                                          00002300
           02 MCRAYONO  PIC X(5).                                       00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MCMODSTOCKA    PIC X.                                     00002330
           02 MCMODSTOCKC    PIC X.                                     00002340
           02 MCMODSTOCKP    PIC X.                                     00002350
           02 MCMODSTOCKH    PIC X.                                     00002360
           02 MCMODSTOCKV    PIC X.                                     00002370
           02 MCMODSTOCKO    PIC X(5).                                  00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MCQUOTAA  PIC X.                                          00002400
           02 MCQUOTAC  PIC X.                                          00002410
           02 MCQUOTAP  PIC X.                                          00002420
           02 MCQUOTAH  PIC X.                                          00002430
           02 MCQUOTAV  PIC X.                                          00002440
           02 MCQUOTAO  PIC X(5).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MZONCMDA  PIC X.                                          00002470
           02 MZONCMDC  PIC X.                                          00002480
           02 MZONCMDP  PIC X.                                          00002490
           02 MZONCMDH  PIC X.                                          00002500
           02 MZONCMDV  PIC X.                                          00002510
           02 MZONCMDO  PIC X(15).                                      00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLIBERRA  PIC X.                                          00002540
           02 MLIBERRC  PIC X.                                          00002550
           02 MLIBERRP  PIC X.                                          00002560
           02 MLIBERRH  PIC X.                                          00002570
           02 MLIBERRV  PIC X.                                          00002580
           02 MLIBERRO  PIC X(58).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCODTRAA  PIC X.                                          00002610
           02 MCODTRAC  PIC X.                                          00002620
           02 MCODTRAP  PIC X.                                          00002630
           02 MCODTRAH  PIC X.                                          00002640
           02 MCODTRAV  PIC X.                                          00002650
           02 MCODTRAO  PIC X(4).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCICSA    PIC X.                                          00002680
           02 MCICSC    PIC X.                                          00002690
           02 MCICSP    PIC X.                                          00002700
           02 MCICSH    PIC X.                                          00002710
           02 MCICSV    PIC X.                                          00002720
           02 MCICSO    PIC X(5).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSCREENA  PIC X.                                          00002820
           02 MSCREENC  PIC X.                                          00002830
           02 MSCREENP  PIC X.                                          00002840
           02 MSCREENH  PIC X.                                          00002850
           02 MSCREENV  PIC X.                                          00002860
           02 MSCREENO  PIC X(4).                                       00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MQTEPREVJA     PIC X.                                     00002890
           02 MQTEPREVJC     PIC X.                                     00002900
           02 MQTEPREVJP     PIC X.                                     00002910
           02 MQTEPREVJH     PIC X.                                     00002920
           02 MQTEPREVJV     PIC X.                                     00002930
           02 MQTEPREVJO     PIC X(6).                                  00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MQUOPREVJA     PIC X.                                     00002960
           02 MQUOPREVJC     PIC X.                                     00002970
           02 MQUOPREVJP     PIC X.                                     00002980
           02 MQUOPREVJH     PIC X.                                     00002990
           02 MQUOPREVJV     PIC X.                                     00003000
           02 MQUOPREVJO     PIC X(6).                                  00003010
           02 FILLER    PIC X(2).                                       00003020
           02 MQTEPREVSA     PIC X.                                     00003030
           02 MQTEPREVSC     PIC X.                                     00003040
           02 MQTEPREVSP     PIC X.                                     00003050
           02 MQTEPREVSH     PIC X.                                     00003060
           02 MQTEPREVSV     PIC X.                                     00003070
           02 MQTEPREVSO     PIC X(6).                                  00003080
           02 FILLER    PIC X(2).                                       00003090
           02 MQUOPREVSA     PIC X.                                     00003100
           02 MQUOPREVSC     PIC X.                                     00003110
           02 MQUOPREVSP     PIC X.                                     00003120
           02 MQUOPREVSH     PIC X.                                     00003130
           02 MQUOPREVSV     PIC X.                                     00003140
           02 MQUOPREVSO     PIC X(6).                                  00003150
           02 FILLER    PIC X(2).                                       00003160
           02 MQTEPRISJA     PIC X.                                     00003170
           02 MQTEPRISJC     PIC X.                                     00003180
           02 MQTEPRISJP     PIC X.                                     00003190
           02 MQTEPRISJH     PIC X.                                     00003200
           02 MQTEPRISJV     PIC X.                                     00003210
           02 MQTEPRISJO     PIC X(6).                                  00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MQUOPRISJA     PIC X.                                     00003240
           02 MQUOPRISJC     PIC X.                                     00003250
           02 MQUOPRISJP     PIC X.                                     00003260
           02 MQUOPRISJH     PIC X.                                     00003270
           02 MQUOPRISJV     PIC X.                                     00003280
           02 MQUOPRISJO     PIC X(6).                                  00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MQTEPRISSA     PIC X.                                     00003310
           02 MQTEPRISSC     PIC X.                                     00003320
           02 MQTEPRISSP     PIC X.                                     00003330
           02 MQTEPRISSH     PIC X.                                     00003340
           02 MQTEPRISSV     PIC X.                                     00003350
           02 MQTEPRISSO     PIC X(6).                                  00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MQUOPRISSA     PIC X.                                     00003380
           02 MQUOPRISSC     PIC X.                                     00003390
           02 MQUOPRISSP     PIC X.                                     00003400
           02 MQUOPRISSH     PIC X.                                     00003410
           02 MQUOPRISSV     PIC X.                                     00003420
           02 MQUOPRISSO     PIC X(6).                                  00003430
           02 FILLER    PIC X(2).                                       00003440
           02 MQTERECJA      PIC X.                                     00003450
           02 MQTERECJC PIC X.                                          00003460
           02 MQTERECJP PIC X.                                          00003470
           02 MQTERECJH PIC X.                                          00003480
           02 MQTERECJV PIC X.                                          00003490
           02 MQTERECJO      PIC X(6).                                  00003500
           02 FILLER    PIC X(2).                                       00003510
           02 MQUORECJA      PIC X.                                     00003520
           02 MQUORECJC PIC X.                                          00003530
           02 MQUORECJP PIC X.                                          00003540
           02 MQUORECJH PIC X.                                          00003550
           02 MQUORECJV PIC X.                                          00003560
           02 MQUORECJO      PIC X(6).                                  00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MQTERECSA      PIC X.                                     00003590
           02 MQTERECSC PIC X.                                          00003600
           02 MQTERECSP PIC X.                                          00003610
           02 MQTERECSH PIC X.                                          00003620
           02 MQTERECSV PIC X.                                          00003630
           02 MQTERECSO      PIC X(6).                                  00003640
           02 FILLER    PIC X(2).                                       00003650
           02 MQUORECSA      PIC X.                                     00003660
           02 MQUORECSC PIC X.                                          00003670
           02 MQUORECSP PIC X.                                          00003680
           02 MQUORECSH PIC X.                                          00003690
           02 MQUORECSV PIC X.                                          00003700
           02 MQUORECSO      PIC X(6).                                  00003710
           02 FILLER    PIC X(2).                                       00003720
           02 MQTESOLDJA     PIC X.                                     00003730
           02 MQTESOLDJC     PIC X.                                     00003740
           02 MQTESOLDJP     PIC X.                                     00003750
           02 MQTESOLDJH     PIC X.                                     00003760
           02 MQTESOLDJV     PIC X.                                     00003770
           02 MQTESOLDJO     PIC X(6).                                  00003780
           02 FILLER    PIC X(2).                                       00003790
           02 MQUOSOLDJA     PIC X.                                     00003800
           02 MQUOSOLDJC     PIC X.                                     00003810
           02 MQUOSOLDJP     PIC X.                                     00003820
           02 MQUOSOLDJH     PIC X.                                     00003830
           02 MQUOSOLDJV     PIC X.                                     00003840
           02 MQUOSOLDJO     PIC X(6).                                  00003850
           02 FILLER    PIC X(2).                                       00003860
           02 MQTESOLDSA     PIC X.                                     00003870
           02 MQTESOLDSC     PIC X.                                     00003880
           02 MQTESOLDSP     PIC X.                                     00003890
           02 MQTESOLDSH     PIC X.                                     00003900
           02 MQTESOLDSV     PIC X.                                     00003910
           02 MQTESOLDSO     PIC X(6).                                  00003920
           02 FILLER    PIC X(2).                                       00003930
           02 MQUOSOLDSA     PIC X.                                     00003940
           02 MQUOSOLDSC     PIC X.                                     00003950
           02 MQUOSOLDSP     PIC X.                                     00003960
           02 MQUOSOLDSH     PIC X.                                     00003970
           02 MQUOSOLDSV     PIC X.                                     00003980
           02 MQUOSOLDSO     PIC X(6).                                  00003990
           02 MTABO OCCURS   11 TIMES .                                 00004000
             03 FILLER       PIC X(2).                                  00004010
             03 MNENTCDEA    PIC X.                                     00004020
             03 MNENTCDEC    PIC X.                                     00004030
             03 MNENTCDEP    PIC X.                                     00004040
             03 MNENTCDEH    PIC X.                                     00004050
             03 MNENTCDEV    PIC X.                                     00004060
             03 MNENTCDEO    PIC X(5).                                  00004070
             03 FILLER       PIC X(2).                                  00004080
             03 MLENTCDEA    PIC X.                                     00004090
             03 MLENTCDEC    PIC X.                                     00004100
             03 MLENTCDEP    PIC X.                                     00004110
             03 MLENTCDEH    PIC X.                                     00004120
             03 MLENTCDEV    PIC X.                                     00004130
             03 MLENTCDEO    PIC X(15).                                 00004140
             03 FILLER       PIC X(2).                                  00004150
             03 MNCDEA  PIC X.                                          00004160
             03 MNCDEC  PIC X.                                          00004170
             03 MNCDEP  PIC X.                                          00004180
             03 MNCDEH  PIC X.                                          00004190
             03 MNCDEV  PIC X.                                          00004200
             03 MNCDEO  PIC X(7).                                       00004210
             03 FILLER       PIC X(2).                                  00004220
             03 MNCODICA     PIC X.                                     00004230
             03 MNCODICC     PIC X.                                     00004240
             03 MNCODICP     PIC X.                                     00004250
             03 MNCODICH     PIC X.                                     00004260
             03 MNCODICV     PIC X.                                     00004270
             03 MNCODICO     PIC X(7).                                  00004280
             03 FILLER       PIC X(2).                                  00004290
             03 MCFAMA  PIC X.                                          00004300
             03 MCFAMC  PIC X.                                          00004310
             03 MCFAMP  PIC X.                                          00004320
             03 MCFAMH  PIC X.                                          00004330
             03 MCFAMV  PIC X.                                          00004340
             03 MCFAMO  PIC X(5).                                       00004350
             03 FILLER       PIC X(2).                                  00004360
             03 MMARQA  PIC X.                                          00004370
             03 MMARQC  PIC X.                                          00004380
             03 MMARQP  PIC X.                                          00004390
             03 MMARQH  PIC X.                                          00004400
             03 MMARQV  PIC X.                                          00004410
             03 MMARQO  PIC X(5).                                       00004420
             03 FILLER       PIC X(2).                                  00004430
             03 MLREFA  PIC X.                                          00004440
             03 MLREFC  PIC X.                                          00004450
             03 MLREFP  PIC X.                                          00004460
             03 MLREFH  PIC X.                                          00004470
             03 MLREFV  PIC X.                                          00004480
             03 MLREFO  PIC X(14).                                      00004490
             03 FILLER       PIC X(2).                                  00004500
             03 MQUOA   PIC X.                                          00004510
             03 MQUOC   PIC X.                                          00004520
             03 MQUOP   PIC X.                                          00004530
             03 MQUOH   PIC X.                                          00004540
             03 MQUOV   PIC X.                                          00004550
             03 MQUOO   PIC X(5).                                       00004560
             03 FILLER       PIC X(2).                                  00004570
             03 MQTEA   PIC X.                                          00004580
             03 MQTEC   PIC X.                                          00004590
             03 MQTEP   PIC X.                                          00004600
             03 MQTEH   PIC X.                                          00004610
             03 MQTEV   PIC X.                                          00004620
             03 MQTEO   PIC X(5).                                       00004630
                                                                                
