      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      **************************************************************    00010000
      * COMMAREA SPECIFIQUE PRG TGF05                    TR: GF05  *    00020013
      *     SUIVI DES DEMANDES DE COMMANDE SPECIFIQUE              *    00030010
      **************************************************************    00040000
      * ZONES RESERVEES APPLICATIVES ---------------------------- 7421          
      *                                                                 00060000
          02 COMM-GF05-APPLI REDEFINES COMM-GF00-APPLI.                 00070013
      *------------------------------ NUMERO DE PAGE                    00100000
             03 NUMERO-DE-PAGE         PIC 9(3).                        00101000
      *------------------------------ CODE SOCIETE                      00121007
             03 COMM-GF05-NSOCIETE       PIC X(3).                      00130013
      *------------------------------ CODE LIEU                         00130108
             03 COMM-GF05-NLIEU          PIC  X(3).                     00130213
      *------------------------------ RENSEIGNEMENT BAS DU TABLEAU      00130310
             03 COMM-GF05-RENSEIGNEMENT.                                00130413
      *------------------------------ BAS DU TABLEAU                    00131010
                04 BAS-DU-TABLEAU   OCCURS 11.                          00132007
      *------------------------------ CODE ARTICLE                      00140007
                   05 COMM-GF05-NCODIC         PIC X(7).                00140213
      *------------------------------ NUMERO MAGAZIN                    00140607
                   05 COMM-GF05-NMAG          PIC X(3).                 00140713
      *------------------------------ DATE DE CREATION                  00140801
                   05 COMM-GF05-DCREATION      PIC  X(8).               00140913
      *------------------------------ CODE MARQUEE                      00201000
                   05 COMM-GF05-CMARQ          PIC  X(5).               00202013
      *----------------------------- REFERENCE DU CODIC                 00203014
                   05 COMM-GF05-LREFFOURN     PIC  X(20).               00204017
      *------------------------------ QUANTITE SOUHAITEE                00205001
                   05 COMM-GF05-QSOUHAITEE     PIC  9(5).               00206013
      *------------------------------ DATE SOUHAITEE                    00207001
                   05 COMM-GF05-DSOUHAITEE     PIC  X(8).               00208013
      *------------------------------ NUMERO DE COMMANDE CLIENT         00209001
                   05 COMM-GF05-NCDECLIENT     PIC  X(7).               00209113
      *------------------------------ CODE STATUT                       00209201
                   05 COMM-GF05-CSTATUT        PIC  X(1).               00209313
      *------------------------------ NUMERO DE COMMANDE (NSEQ)         00209401
                   05 COMM-GF05-NSEQ           PIC  X(7).               00209513
      *------------------------------ DERNIERE  CLE LUE                 00210416
             03 SAVE-CLE-FIN1            PIC X(8).                      00210516
      *------------------------------ DERNIERE  CLE LUE                 00210615
             03 SAVE-CLE-FIN2            PIC X(7).                      00210715
      *------------------------------ DERNIERE  CLE LUE                 00210820
             03 SAVE-CLE-FIN3            PIC X(7).                      00210920
      *------------------------------ DERNIERE  CLE LUE                 00211020
             03 SAVE-CLE-FIN4            PIC X(3).                      00212020
      *------------------------------ DERNIERE  CLE LUE                 00212030
             03 SAVE-CLE-FIN5            PIC X(3).                      00212040
      *------------------------------ DERNIERE  CLE LUE                 00212050
             03 SAVE-CLE-FIN6            PIC X(3).                      00212060
      *------------------------------ TABLE POUR LA PAGINATION          00220000
             03 TABLE-PAGINATION1    .                                  00230015
      *------------------------------ CLE DE LA PAGINATION              00240000
                04 SAVE-PAGINATION1  OCCURS  30.                        00240115
      *------------------------------ PAGINATION                        00241000
                  05 SAVE-CLE-PAGINATION    PIC 9(8).                   00250011
      *------------------------------ TABLE POUR LA PAGINATION          00260015
             03 TABLE-PAGINATION2     .                                 00261015
      *------------------------------ CLE DE LA PAGINATION              00261115
                04 SAVE-PAGINATION2  OCCURS  30.                        00261215
      *------------------------------ PAGINATION                        00261315
                  05 SAVE-CLE-NCDE          PIC X(7).                   00261420
      *------------------------------ DERNIERE  CODIC LUE               00261518
                  05 SAVE-NCODIC              PIC X(7).                 00261618
      *------------------------------ DERNIERE  LIEU  LUE               00261719
                  05 SAVE-NLIEU               PIC X(3).                 00261819
      *------------------------------ DERNIERE  STE LIVRAISON LUE       00261820
                  05 SAVE-NSOCLIVR            PIC X(3).                 00261830
      *------------------------------ DERNIER   DEPOT LU                00261840
                  05 SAVE-NDEPOT              PIC X(3).                 00261850
      *------------------------------ FILLER                            00262000
      *      03 COMM-GF05-FILLER         PIC X(5670).                           
      ***************************************************************** 00280000
                                                                                
