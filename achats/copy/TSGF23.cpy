      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                                 00010000
      ***************************************************************** 00020000
      *  TS TRANSACTION GF23 : VALORISATIONS COMMANDES FOURNISSEURS   * 00030000
      ***************************************************************** 00040000
      *                                                                 00050000
       01  TS-GF23.                                                     00060000
      *                                                                 00061000
           02  TS-GF23-LONG  PIC S9(3) COMP-3      VALUE +340.                  
      *                                                                 00063000
           02  TS-GF23-DONNEES.                                         00064000
      *                                                                 00070000
      *  DONNEES ARTICLE (LONG : 37)                                    00080001
      *                                                                 00090000
             03  TS-GF33-DONNEES-ARTICLE.                               00100001
                 04  TS-GF23-CFAM             PIC X(5).                 00130000
                 04  TS-GF23-CMARQ            PIC X(5).                 00140000
                 04  TS-GF23-LREFFOURN        PIC X(20).                00150001
                 04  TS-GF23-CTYPCONDT        PIC X.                    00151001
                 04  TS-GF23-CTAUXTVA         PIC X(5).                 00151101
                 04  TS-GF23-TOP-ESCL         PIC X.                    00152001
      *                                                                 00160000
      *  DONNEES LIGNE COMMANDE A VALORISER (LONG : 160)                00170001
      *                                                                 00180000
             03  TS-GF23-DONNEES-CDE.                                   00190001
      *          04  TS-GF23-NCDE             PIC X(7).                 00210000
                 04  TS-GF23-NCDE-FILLE OCCURS 10.                              
                     05  TS-GF23-NCDE         PIC X(7).                         
                 04  TS-GF23-NCODIC           PIC X(7).                 00220000
                 04  TS-GF23-NCODICLIE        PIC X(7).                 00221001
                 04  TS-GF23-DSAISIE          PIC X(8).                 00230000
                 04  TS-GF23-DVALIDITE        PIC X(8).                 00240000
                 04  TS-GF23-QCDE             PIC S9(5) COMP-3.         00250000
                 04  TS-GF23-QREC             PIC S9(5) COMP-3.         00260001
                 04  TS-GF23-QSOLDE           PIC S9(5) COMP-3.         00270000
                 04  TS-GF23-PABASEFACT       PIC S9(7)V99 COMP-3.      00280000
                 04  TS-GF23-PROMOFACT        PIC S9(7)V99 COMP-3.      00290000
                 04  TS-GF23-QTAUXESCPT       PIC S9(3)V99 COMP-3.      00300000
                 04  TS-GF23-PTAUXESCPT       PIC S9(7)V99 COMP-3.      00310000
                 04  TS-GF23-PAHORSPROMO      PIC S9(7)V99 COMP-3.      00320000
                 04  TS-GF23-PRISTPROMO       PIC S9(7)V99 COMP-3.      00330000
                 04  TS-GF23-PRISTCONDI       PIC S9(7)V99 COMP-3.      00340000
                 04  TS-GF23-PRA              PIC S9(7)V99 COMP-3.      00350000
                 04  TS-GF23-PCF              PIC S9(7)V99 COMP-3.      00350101
                 04  TS-GF23-PTAUXESCPT-MOD   PIC S9(7)V99 COMP-3.      00350201
                 04  TS-GF23-PRA-MOD          PIC S9(7)V99 COMP-3.      00350301
             03  TS-GF23-DONNEES-ANC-CDE.                               00350401
                 04  TS-GF23-NCDEDER          PIC X(7).                 00351001
                 04  TS-GF23-DVALIDITEDER     PIC X(8).                 00360000
                 04  TS-GF23-QCDEDER          PIC S9(5) COMP-3.         00361000
                 04  TS-GF23-PABASEFACTDER    PIC S9(7)V99 COMP-3.      00370000
                 04  TS-GF23-PROMOFACTDER     PIC S9(7)V99 COMP-3.      00380000
                 04  TS-GF23-QTAUXESCPTDER    PIC S9(3)V99 COMP-3.      00390000
                 04  TS-GF23-PTAUXESCPTDER    PIC S9(7)V99 COMP-3.      00400000
                 04  TS-GF23-PAHORSPROMODER   PIC S9(7)V99 COMP-3.      00410000
                 04  TS-GF23-PRISTPROMODER    PIC S9(7)V99 COMP-3.      00420000
                 04  TS-GF23-PRISTCONDIDER    PIC S9(7)V99 COMP-3.      00430000
                 04  TS-GF23-PRADER           PIC S9(7)V99 COMP-3.      00440000
                 04  TS-GF23-PCFDER           PIC S9(7)V99 COMP-3.      00450001
             03  TS-GF23-DONNEES-INI-CDE.                               00460002
                 04  TS-GF23-PABASEFACTINI    PIC S9(7)V99 COMP-3.      00470002
                 04  TS-GF23-PROMOFACTINI     PIC S9(7)V99 COMP-3.      00480002
                 04  TS-GF23-PTAUXESCPTINI    PIC S9(7)V99 COMP-3.      00490002
                 04  TS-GF23-PAHORSPROMOINI   PIC S9(7)V99 COMP-3.      00500002
                 04  TS-GF23-PRISTPROMOINI    PIC S9(7)V99 COMP-3.      00510002
                 04  TS-GF23-PRISTCONDIINI    PIC S9(7)V99 COMP-3.      00520002
                 04  TS-GF23-PRAINI           PIC S9(7)V99 COMP-3.      00530002
                 04  TS-GF23-PCFINI           PIC S9(7)V99 COMP-3.      00540002
             03  TS-GF23-DONNEES-IND-CDE.                               00550002
                 04  TS-GF23-PABASEFACTIND    PIC S9(7)V99 COMP-3.      00560002
                 04  TS-GF23-PROMOFACTIND     PIC S9(7)V99 COMP-3.      00570002
                 04  TS-GF23-PTAUXESCPTIND    PIC S9(7)V99 COMP-3.      00580002
                 04  TS-GF23-PAHORSPROMOIND   PIC S9(7)V99 COMP-3.      00590002
                 04  TS-GF23-PRISTPROMOIND    PIC S9(7)V99 COMP-3.      00600002
                 04  TS-GF23-PRISTCONDIIND    PIC S9(7)V99 COMP-3.      00610002
                 04  TS-GF23-PRAIND           PIC S9(7)V99 COMP-3.      00620002
                 04  TS-GF23-PCFIND           PIC S9(7)V99 COMP-3.      00630002
      *                                                                 00640002
      *                                                                 00710000
                                                                                
