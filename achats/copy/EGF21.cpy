      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGF21   EGF21                                              00000020
      ***************************************************************** 00000030
       01   EGF21I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWFONCI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFEXTL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCFEXTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFEXTF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCFEXTI   PIC X.                                          00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MWPAGEI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWSEPL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MWSEPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MWSEPF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MWSEPI    PIC X.                                          00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEFL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MWPAGEFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWPAGEFF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MWPAGEFI  PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNSOCI    PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNDEPOTI  PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFPL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCHEFPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHEFPF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCHEFPI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCHEFPL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLCHEFPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCHEFPF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLCHEFPI  PIC X(20).                                      00000490
           02 M108I OCCURS   12 TIMES .                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MSELI   PIC X.                                          00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCLIEUL    COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MSOCLIEUL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MSOCLIEUF    PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MSOCLIEUI    PIC X(6).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCHEFPRDL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCHEFPRDL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCHEFPRDF    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCHEFPRDI    PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNCODICI     PIC X(7).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MARQL   COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MARQF   PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MARQI   PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MCFAMI  PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFL   COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MREFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MREFF   PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MREFI   PIC X(20).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTCDEL    COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MNENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNENTCDEF    PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNENTCDEI    PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQCOML  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MQCOML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQCOMF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQCOMI  PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCOML  COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MDCOML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDCOMF  PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MDCOMI  PIC X(6).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(15).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(58).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * SDF: EGF21   EGF21                                              00001160
      ***************************************************************** 00001170
       01   EGF21O REDEFINES EGF21I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MWFONCA   PIC X.                                          00001350
           02 MWFONCC   PIC X.                                          00001360
           02 MWFONCP   PIC X.                                          00001370
           02 MWFONCH   PIC X.                                          00001380
           02 MWFONCV   PIC X.                                          00001390
           02 MWFONCO   PIC X(3).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MCFEXTA   PIC X.                                          00001420
           02 MCFEXTC   PIC X.                                          00001430
           02 MCFEXTP   PIC X.                                          00001440
           02 MCFEXTH   PIC X.                                          00001450
           02 MCFEXTV   PIC X.                                          00001460
           02 MCFEXTO   PIC X.                                          00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MWPAGEA   PIC X.                                          00001490
           02 MWPAGEC   PIC X.                                          00001500
           02 MWPAGEP   PIC X.                                          00001510
           02 MWPAGEH   PIC X.                                          00001520
           02 MWPAGEV   PIC X.                                          00001530
           02 MWPAGEO   PIC X(3).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MWSEPA    PIC X.                                          00001560
           02 MWSEPC    PIC X.                                          00001570
           02 MWSEPP    PIC X.                                          00001580
           02 MWSEPH    PIC X.                                          00001590
           02 MWSEPV    PIC X.                                          00001600
           02 MWSEPO    PIC X.                                          00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MWPAGEFA  PIC X.                                          00001630
           02 MWPAGEFC  PIC X.                                          00001640
           02 MWPAGEFP  PIC X.                                          00001650
           02 MWPAGEFH  PIC X.                                          00001660
           02 MWPAGEFV  PIC X.                                          00001670
           02 MWPAGEFO  PIC X(3).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MNSOCA    PIC X.                                          00001700
           02 MNSOCC    PIC X.                                          00001710
           02 MNSOCP    PIC X.                                          00001720
           02 MNSOCH    PIC X.                                          00001730
           02 MNSOCV    PIC X.                                          00001740
           02 MNSOCO    PIC X(3).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MNDEPOTA  PIC X.                                          00001770
           02 MNDEPOTC  PIC X.                                          00001780
           02 MNDEPOTP  PIC X.                                          00001790
           02 MNDEPOTH  PIC X.                                          00001800
           02 MNDEPOTV  PIC X.                                          00001810
           02 MNDEPOTO  PIC X(3).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCHEFPA   PIC X.                                          00001840
           02 MCHEFPC   PIC X.                                          00001850
           02 MCHEFPP   PIC X.                                          00001860
           02 MCHEFPH   PIC X.                                          00001870
           02 MCHEFPV   PIC X.                                          00001880
           02 MCHEFPO   PIC X(5).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MLCHEFPA  PIC X.                                          00001910
           02 MLCHEFPC  PIC X.                                          00001920
           02 MLCHEFPP  PIC X.                                          00001930
           02 MLCHEFPH  PIC X.                                          00001940
           02 MLCHEFPV  PIC X.                                          00001950
           02 MLCHEFPO  PIC X(20).                                      00001960
           02 M108O OCCURS   12 TIMES .                                 00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MSELA   PIC X.                                          00001990
             03 MSELC   PIC X.                                          00002000
             03 MSELP   PIC X.                                          00002010
             03 MSELH   PIC X.                                          00002020
             03 MSELV   PIC X.                                          00002030
             03 MSELO   PIC X.                                          00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MSOCLIEUA    PIC X.                                     00002060
             03 MSOCLIEUC    PIC X.                                     00002070
             03 MSOCLIEUP    PIC X.                                     00002080
             03 MSOCLIEUH    PIC X.                                     00002090
             03 MSOCLIEUV    PIC X.                                     00002100
             03 MSOCLIEUO    PIC X(6).                                  00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MCHEFPRDA    PIC X.                                     00002130
             03 MCHEFPRDC    PIC X.                                     00002140
             03 MCHEFPRDP    PIC X.                                     00002150
             03 MCHEFPRDH    PIC X.                                     00002160
             03 MCHEFPRDV    PIC X.                                     00002170
             03 MCHEFPRDO    PIC X(5).                                  00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MNCODICA     PIC X.                                     00002200
             03 MNCODICC     PIC X.                                     00002210
             03 MNCODICP     PIC X.                                     00002220
             03 MNCODICH     PIC X.                                     00002230
             03 MNCODICV     PIC X.                                     00002240
             03 MNCODICO     PIC X(7).                                  00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MARQA   PIC X.                                          00002270
             03 MARQC   PIC X.                                          00002280
             03 MARQP   PIC X.                                          00002290
             03 MARQH   PIC X.                                          00002300
             03 MARQV   PIC X.                                          00002310
             03 MARQO   PIC X(5).                                       00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MCFAMA  PIC X.                                          00002340
             03 MCFAMC  PIC X.                                          00002350
             03 MCFAMP  PIC X.                                          00002360
             03 MCFAMH  PIC X.                                          00002370
             03 MCFAMV  PIC X.                                          00002380
             03 MCFAMO  PIC X(5).                                       00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MREFA   PIC X.                                          00002410
             03 MREFC   PIC X.                                          00002420
             03 MREFP   PIC X.                                          00002430
             03 MREFH   PIC X.                                          00002440
             03 MREFV   PIC X.                                          00002450
             03 MREFO   PIC X(20).                                      00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MNENTCDEA    PIC X.                                     00002480
             03 MNENTCDEC    PIC X.                                     00002490
             03 MNENTCDEP    PIC X.                                     00002500
             03 MNENTCDEH    PIC X.                                     00002510
             03 MNENTCDEV    PIC X.                                     00002520
             03 MNENTCDEO    PIC X(5).                                  00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MQCOMA  PIC X.                                          00002550
             03 MQCOMC  PIC X.                                          00002560
             03 MQCOMP  PIC X.                                          00002570
             03 MQCOMH  PIC X.                                          00002580
             03 MQCOMV  PIC X.                                          00002590
             03 MQCOMO  PIC X(5).                                       00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MDCOMA  PIC X.                                          00002620
             03 MDCOMC  PIC X.                                          00002630
             03 MDCOMP  PIC X.                                          00002640
             03 MDCOMH  PIC X.                                          00002650
             03 MDCOMV  PIC X.                                          00002660
             03 MDCOMO  PIC X(6).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MZONCMDA  PIC X.                                          00002690
           02 MZONCMDC  PIC X.                                          00002700
           02 MZONCMDP  PIC X.                                          00002710
           02 MZONCMDH  PIC X.                                          00002720
           02 MZONCMDV  PIC X.                                          00002730
           02 MZONCMDO  PIC X(15).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(58).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(4).                                       00003090
                                                                                
