      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. SUIVITRX.                                                    
      *                                                                         
      *                                                                         
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
      *{  Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                     
       CONFIGURATION SECTION.                                                   
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           COPY MW-COLSEQ.                                                      
                                                                                
      *}                                                                        
      *}                                                                        
       DATA DIVISION.                                                           
      ***************************************************************           
      *15.42.55 STC02522 +PATROLZOS0INNOVENTE CDPRP   MQTM TRX ARRETEE          
      *15.43.23 STC02522 +PATROLZOS0MQSERIES  CDPRP   MQRT TRX ARRETEE          
      *15.44.25 STC02522 +PATROLZOS0MQSERIES  CDPRP   MQRC TRX ARRETEE          
      ***************************************************************           
      ***************************************************************           
      ***************************************************************           
       WORKING-STORAGE SECTION.                                                 
      ***************************************************************           
       01  MESSLOG1.                                                            
           02  SUIVLOG     PIC X(10)  VALUE 'PATROLZOS2'.                       
           02  SUIVAPPL    PIC X(10)  VALUE 'MQSERIES '.                        
           02  SUIVCICS    PIC X(8)  VALUE  '       '.                          
           02  SUIVTRAN    PIC X(4)  VALUE  '    '.                             
           02  SUIVSTOP    PIC X(20) VALUE  ' TRANSACTION ARRETEE'.             
           02  SUIVMES1    PIC X(4).                                            
           02  SUIVMES2    PIC X(4).                                            
           02  SUIVMES3    PIC X(4).                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  COMPTEUR    PIC S9(4)  COMP VALUE +0.                            
      *--                                                                       
           02  COMPTEUR    PIC S9(4) COMP-5 VALUE +0.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  I                 PIC S9(4)  COMP VALUE 0.                           
      *--                                                                       
       77  I                 PIC S9(4) COMP-5 VALUE 0.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  II                PIC S9(4)  COMP VALUE 0.                           
      *--                                                                       
       77  II                PIC S9(4) COMP-5 VALUE 0.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  X                 PIC S9(4)  COMP VALUE +0.                          
      *--                                                                       
       77  X                 PIC S9(4) COMP-5 VALUE +0.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  Z                 PIC S9(4)  COMP VALUE +0.                          
      *--                                                                       
       77  Z                 PIC S9(4) COMP-5 VALUE +0.                         
      *}                                                                        
       77  W-HEUR                PIC  9(07).                                    
       77  W-CMD                 PIC  X(8).                                     
       77  W-RESP                PIC  S9(8).                                    
       77  W-RESP2               PIC  S9(8).                                    
       77  W-MESS            PIC  X(80)          VALUE SPACES.                  
       77  LOG-MESS          PIC  X(80)          VALUE SPACES.                  
       77  ABSTIME           PIC S9(15) COMP-3.                                 
       77  NUM-AUX           PIC 9(8)   VALUE 0.                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  LISTSIZE1         PIC S9(8)  COMP VALUE +0.                          
      *--                                                                       
       77  LISTSIZE1         PIC S9(8) COMP-5 VALUE +0.                         
      *}                                                                        
       77  LISTPTR           USAGE IS POINTER.                                  
       77  END-MESSAGE       PIC X(3)   VALUE 'END'.                            
       77  TRANS-NAME        PIC X(4)   VALUE 'SUIV'.                           
       COPY DFHAID.                                                             
       COPY WORKCICS.                                                           
      ***************************************************************           
       01 COMMAREA.                                                             
      ************************************************ 1625 BYTES ***           
           02        W-TASKNUMBER    PIC S9(7)  COMP-3.                         
           02        T-TRANSACTION   PIC  X(4).                                 
           02        T-USERID        PIC  X(8).                                 
           02        T-FACILITY      PIC  X(4).                                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02        W-FACILITYTYPE  PIC S9(8)  COMP.                           
      *--                                                                       
           02        W-FACILITYTYPE  PIC S9(8) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02        W-PRIORITY      PIC S9(8)  COMP.                           
      *--                                                                       
           02        W-PRIORITY      PIC S9(8) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02        W-RUNSTATUS     PIC S9(8)  COMP.                           
      *--                                                                       
           02        W-RUNSTATUS     PIC S9(8) COMP-5.                          
      *}                                                                        
           02        T-SUSPENDTYPE   PIC  X(8).                                 
           02        T-SUSPENDVALUE  PIC  X(8).                                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02        W-SUSPENDTIME   PIC S9(8)  COMP.                           
      *--                                                                       
           02        W-SUSPENDTIME   PIC S9(8) COMP-5.                          
      *}                                                                        
           02        T-STARTCODE     PIC  X(2).                                 
           02  FILLER                PIC X(100).                                
      *                                                                         
      ***************************************************************           
       LINKAGE SECTION.                                                         
      ***************************************************************           
       01  DFHCOMMAREA.                                                         
           02  FILLER          PIC X(2000).                                     
       01  TASKLIST.                                                            
           04  TASKL OCCURS  30 PIC S9(7) COMP-3.                               
       01 TAB-LIB.                                                              
           05 LIB-POSTES OCCURS 30 INDEXED BY ILIB.                             
                10 LIB-CLE.                                                     
                    15 LIB-COD-TRX PIC X(4).                                    
                    15 LIB-COD-APL PIC X(10).                                   
                    15 LIB-COD-FI1 PIC X(2).                                    
                    15 LIB-COD-LOG PIC X(10).                                   
                    15 LIB-COD-FI2 PIC X(2).                                    
      ***************************************************************           
       PROCEDURE DIVISION.                                                      
      ***************************************************************           
      *                                                                         
       BEGIN-TIME.                                                              
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS DELAY INTERVAL(1000) END-EXEC.                             
      *--                                                                       
           EXEC CICS DELAY INTERVAL(1000)                                       
           END-EXEC.                                                            
      *}                                                                        
           MOVE 0 TO I.                                                         
           MOVE 0 TO II.                                                        
           MOVE 0 TO X.                                                         
      *{ normalize-exec-xx 1.5                                                  
      *      EXEC CICS ASSIGN APPLID(SUIVCICS) END-EXEC.                        
      *--                                                                       
             EXEC CICS ASSIGN APPLID(SUIVCICS)                                  
             END-EXEC.                                                          
      *}                                                                        
           IF EIBCALEN = 0                                                      
              MOVE LOW-VALUES TO COMMAREA                                       
              MOVE 1625 TO EIBCALEN                                             
              GO TO  ITS-TIME.                                                  
       ITS-TIME.                                                                
      *    EXEC CICS DELAY INTERVAL(0005) END-EXEC.                             
           MOVE 'LOAD' TO W-CMD.                                                
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS LOAD PROGRAM('TABECOUT')                                   
      *       SET(ADDRESS OF TAB-LIB)                                           
      *                    RESP(EIB-RESP)                                       
      *                    END-EXEC.                                            
      *--                                                                       
           EXEC CICS LOAD PROGRAM('TABECOUT')                                   
              SET(ADDRESS OF TAB-LIB)                                           
                           RESP(EIB-RESP)                                       
           END-EXEC.                                                            
      *}                                                                        
           PERFORM TEST-CODE-RETOUR-CICS.                                       
           PERFORM START-MQ   THROUGH MQ-END                                    
           VARYING I FROM 1 BY 1 UNTIL II > 30.                                 
           GO TO BEGIN-TIME.                                                    
       START-MQ SECTION.                                                        
           MOVE LIB-COD-TRX(II)  TO  SUIVTRAN.                                  
           MOVE LIB-COD-APL(II)  TO  SUIVAPPL.                                  
           MOVE LIB-COD-LOG(II)  TO  SUIVLOG.                                   
           IF  LIB-COD-TRX(II) = 'XXXX'                                         
           GO TO BEGIN-TIME                                                     
           ELSE                                                                 
           ADD 1 TO I                                                           
           ADD 1 TO II                                                          
           MOVE 0 TO X                                                          
           EXEC CICS INQUIRE TASK LIST                                          
                SET     (LISTPTR)                                               
                LISTSIZE(LISTSIZE1)                                             
           END-EXEC                                                             
           SET ADDRESS OF TASKLIST TO LISTPTR                                   
           PERFORM INQUIRE-CICS-LOOP THRU                                       
                   INQUIRE-CICS-LOOP-EXIT UNTIL X > 170                         
           GO TO RETURN-END                                                     
           END-IF.                                                              
       MQ-END. EXIT.                                                            
      *                                                                         
      ***************************************************************           
      *    Subroutines                                              *           
      ***************************************************************           
      *                                                                         
       INQUIRE-CICS-LOOP.                                                       
      *=================*                                                       
           MOVE LIB-COD-TRX(II)  TO  SUIVTRAN                                   
           MOVE LIB-COD-APL(II)  TO  SUIVAPPL                                   
           MOVE LIB-COD-LOG(II)  TO  SUIVLOG                                    
      *    IF SUIVTRAN = 'RB50'                                                 
      *    MOVE 'PATROLZOS3' TO SUIVLOG                                         
      *    ELSE                                                                 
      *    MOVE 'PATROLZOS2' TO SUIVLOG                                         
      *    END-IF                                                               
           IF SUIVTRAN = 'XXXX' GO TO BEGIN-TIME                                
           ELSE                                                                 
           EXEC CICS DELAY INTERVAL(0000)                                       
           END-EXEC                                                             
           END-IF                                                               
           ADD 1 TO X                                                           
           IF X > LISTSIZE1                                                     
              MOVE 16 TO COMPTEUR                                               
              IF     SUIVTRAN =  'CKAM'                                         
                GO TO RETURN-END                                                
                END-IF                                                          
      *{ normalize-exec-xx 1.5                                                  
      *       EXEC  CICS ENTER TRACEID(021)  END-EXEC                           
      *--                                                                       
              EXEC  CICS ENTER TRACEID(021)                                     
              END-EXEC                                                          
      *}                                                                        
              EXEC  CICS WRITE OPERATOR TEXT(MESSLOG1)                          
              END-EXEC                                                          
              GO TO ITS-TIME                                                    
           END-IF                                                               
           MOVE TASKL(X) TO W-TASKNUMBER                                        
           EXEC CICS INQUIRE TASK        (W-TASKNUMBER)                         
                             TRANSACTION (T-TRANSACTION)                        
                             USERID      (T-USERID)                             
                             FACILITY    (T-FACILITY)                           
                             FACILITYTYPE(W-FACILITYTYPE)                       
                             PRIORITY    (W-PRIORITY)                           
                             RUNSTATUS   (W-RUNSTATUS)                          
                             SUSPENDTYPE (T-SUSPENDTYPE)                        
                             SUSPENDVALUE(T-SUSPENDVALUE)                       
                             SUSPENDTIME (W-SUSPENDTIME)                        
                             STARTCODE   (T-STARTCODE)                          
                             RESP        (W-RESP)                               
                             RESP2       (W-RESP2)                              
           END-EXEC                                                             
           IF W-RESP2 > 0                                                       
              EXEC  CICS WRITE OPERATOR TEXT(MESSLOG1)                          
              END-EXEC                                                          
      *{ normalize-exec-xx 1.5                                                  
      *       EXEC  CICS ENTER TRACEID(025)  END-EXEC                           
      *--                                                                       
              EXEC  CICS ENTER TRACEID(025)                                     
              END-EXEC                                                          
      *}                                                                        
      *                                                                         
              GO TO  ITS-TIME                                                   
           END-IF                                                               
           IF T-TRANSACTION  = SUIVTRAN                                         
      *{ normalize-exec-xx 1.5                                                  
      *       EXEC  CICS ENTER TRACEID(067)  END-EXEC                           
      *--                                                                       
              EXEC  CICS ENTER TRACEID(067)                                     
              END-EXEC                                                          
      *}                                                                        
              GO TO  ITS-TIME                                                   
           END-IF.                                                              
      *                                                                         
       INQUIRE-CICS-LOOP-EXIT.                                                  
      *======================*                                                  
           EXIT.                                                                
      *                                                                         
       TEST-CODE-RETOUR-CICS           SECTION.                                 
      *---------------------------------------                                  
            IF RESP-NORMAL                                                      
               MOVE '      ' TO W-CMD                                           
            ELSE                                                                
               PERFORM WRITE-LOG                                                
      *        EXEC  CICS ENTER TRACEID(032)  END-EXEC                          
            END-IF.                                                             
       WRITE-LOG  SECTION.                                                      
      *    EXEC  CICS ENTER TRACEID(033)  END-EXEC                              
           MOVE  EIBTIME TO W-HEUR.                                             
           MOVE  EIBRESP TO W-RESP.                                             
           STRING 'HEURE:' W-HEUR ' RC:' W-RESP ' CMD:' W-CMD                   
           DELIMITED SIZE INTO LOG-MESS.                                        
*          IF  W-HEUR > 19 AND SUIVTRAN = 'NV00' GO TO SUITE                    
           IF                  SUIVTRAN = 'NV00' GO TO SUITE                    
           GO TO SUITE-END.                                                     
       SUITE.                                                                   
           EXEC CICS WRITEQ TD                                                  
                     QUEUE('PLOG')                                              
                     FROM(LOG-MESS)                                             
                     NOHANDLE                                                   
           END-EXEC.                                                            
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS RETURN END-EXEC.                                           
      *--                                                                       
           EXEC CICS RETURN                                                     
           END-EXEC.                                                            
      *}                                                                        
       SUITE-END.                                                               
       RETURN-END.                                                              
      *==============*                                                          
      *    EXEC  CICS ENTER TRACEID(034)  END-EXEC                              
           EXEC CICS RETURN                                                     
           END-EXEC.                                                            
      *                                                                         
           GOBACK.                                                              
