      **************** M E T A W A R E    H E A D E R ****************
      * AUTHOR :     MSTIRI
      * COMPANY:     METAWARE
      * PROJECT:     PL1 TO COBOL SPEC FOR DARTY 
      * Description: THIS IS A SUBPROGRAM THAT WAS DEVELOPED AS AN 
      *              ALTERNATIVE TO THE PL/1 BUILT-IN FUNCTION VERIFY.
      *              IT DOES SUPPORT ONLY DARTY CASES. SEE SPECIFICATION
      *              PAGE (WIKI) FOR MORE INFORMATIONS:
      *              https://wiki.metaware.co/display/indus/IF+STATEMENT
      ****************************************************************
        IDENTIFICATION DIVISION.
        PROGRAM-ID.     FUNC-VERIFY.
        DATA DIVISION.
        WORKING-STORAGE SECTION.
        01 CURRENT-CHAR         PIC X .
           88 ONE-THRU-EIGHT    VALUE '1' THRU '8'.
           88 ONE-THRU-THREE    VALUE '1' THRU '3'.
           88 TYPE-DATE         VALUE '0' THRU '9' '/' SPACE.
           88 TYPE-2            VALUE '1' THRU '6' '9'.

        01 I                    PIC 99 .
        01 RET-VERIFY           PIC S9(9) COMP-5 .

        LINKAGE SECTION.
        01 LEN-STR-IN           PIC S9(9) COMP-5.
        01 LEN-MASK             PIC S9(9) COMP-5. 
        01 STR-IN               PIC X(100).
        01 MASK                 PIC X(15).
        01 RETURN-VERIFY        PIC S9(9) COMP-5 .
 
        PROCEDURE DIVISION USING 
                LEN-STR-IN STR-IN MASK RETURN-VERIFY .
       
        EVALUATE MASK
          WHEN '123456789'
                PERFORM NUMERIC-NOT-ZERO-PARA THRU 
                        END-NUMERIC-NOT-ZERO-PARA 
          WHEN ' ' 
                PERFORM SPACE-PARA THRU 
                        END-SPACE-PARA 
          WHEN '12345678'
                PERFORM ONE-THRU-EIGHT-PARA THRU 
                        END-ONE-THRU-EIGHT-PARA
          WHEN '123'
                PERFORM ONE-THRU-THREE-PARA THRU 
                        END-ONE-THRU-THREE-PARA
          WHEN '0123456789'
                PERFORM NUMERIC-PARA THRU 
                        END-NUMERIC-PARA
          WHEN '9876543210'
                PERFORM NUMERIC-PARA THRU 
                        END-NUMERIC-PARA
          WHEN '1234567890'
                PERFORM NUMERIC-PARA THRU 
                        END-NUMERIC-PARA
          WHEN 'ZONE_NUM' 
                PERFORM NUMERIC-PARA THRU 
                        END-NUMERIC-PARA
          WHEN 'NUMERIQUE' 
                PERFORM NUMERIC-PARA THRU 
                        END-NUMERIC-PARA
          WHEN 'ALPHA'
                PERFORM ALPHANUMERIC-PARA THRU 
                        END-ALPHANUMERIC-PARA
          WHEN 'ALPHANUMERIC' 
                PERFORM ALPHANUMERIC-PARA THRU 
                        END-ALPHANUMERIC-PARA
          WHEN 'ALPHABETIQUE' 
                PERFORM ALPHABETIQUE-PARA THRU 
                        END-ALPHABETIQUE-PARA
          WHEN '1234569' 
                PERFORM TYPE-2-PARA THRU 
                        END-TYPE-2-PARA 
          WHEN ' /1234567890'
                PERFORM TYPE-DATE-PARA THRU 
                        END-TYPE-DATE-PARA 
          WHEN OTHER 
                MOVE -1 TO RET-VERIFY
        END-EVALUATE 
        MOVE RET-VERIFY TO RETURN-VERIFY

        GOBACK.
       
      * NUMERIC NOT ZERO
        NUMERIC-NOT-ZERO-PARA.
        MOVE 0 TO RET-VERIFY.
        PERFORM VARYING I FROM 1 BY 1 
                UNTIL I > LEN-STR-IN OR RET-VERIFY > 0
        MOVE STR-IN(I:1) TO CURRENT-CHAR 
        IF CURRENT-CHAR IS NUMERIC AND CURRENT-CHAR NOT = 0 THEN 
          CONTINUE 
        ELSE
          MOVE I TO RET-VERIFY
        END-IF
        END-PERFORM.
        END-NUMERIC-NOT-ZERO-PARA.
      * SPACE 
        SPACE-PARA.
        MOVE 0 TO RET-VERIFY.
        PERFORM VARYING I FROM 1 BY 1 
                UNTIL I > LEN-STR-IN OR RET-VERIFY > 0
        MOVE STR-IN(I:1) TO CURRENT-CHAR
        IF CURRENT-CHAR = SPACE THEN 
          CONTINUE 
        ELSE
          MOVE I TO RET-VERIFY
        END-PERFORM.
        END-SPACE-PARA.
      * ONE-THRU-EIGHT
        ONE-THRU-EIGHT-PARA.
        MOVE 0 TO RET-VERIFY
        PERFORM VARYING I FROM 1 BY 1 
                UNTIL I > LEN-STR-IN OR  RET-VERIFY > 0
        MOVE STR-IN(I:1) TO CURRENT-CHAR
        IF ONE-THRU-EIGHT THEN 
          CONTINUE
        ELSE
          MOVE I TO RET-VERIFY
        END-IF
        END-PERFORM.
        END-ONE-THRU-EIGHT-PARA.

      * ONE-THRU-THREE
        ONE-THRU-THREE-PARA.
        MOVE 0 TO RET-VERIFY
        PERFORM VARYING I FROM 1 BY 1
                UNTIL I > LEN-STR-IN OR  RET-VERIFY > 0
        MOVE STR-IN(I:1) TO CURRENT-CHAR
        IF ONE-THRU-THREE THEN 
          CONTINUE
        ELSE 
          MOVE I TO RET-VERIFY 
        END-IF 
        END-PERFORM.
        END-ONE-THRU-THREE-PARA.    
      * TYPE-DATE  
        TYPE-DATE-PARA.
        MOVE 0 TO RET-VERIFY
        PERFORM VARYING I FROM 1 BY 1 
                UNTIL I > LEN-STR-IN OR RET-VERIFY > 0 
        MOVE STR-IN(I:1) TO CURRENT-CHAR
        IF TYPE-DATE THEN 
          CONTINUE 
        ELSE
          MOVE I TO RET-VERIFY
        END-IF
        END-PERFORM.
        END-TYPE-DATE-PARA.
        
      * NUMERIC
        NUMERIC-PARA. 
        MOVE 0 TO RET-VERIFY.
        PERFORM VARYING I FROM 1 BY 1 
                UNTIL I > LEN-STR-IN OR RET-VERIFY > 0
        MOVE STR-IN(I:1) TO CURRENT-CHAR 
        IF CURRENT-CHAR IS NUMERIC THEN 
          CONTINUE
        ELSE
          MOVE I TO RET-VERIFY
        END-IF
        END-PERFORM.
        END-NUMERIC-PARA.

      * ALPHANUMERIC
        ALPHANUMERIC-PARA.
        MOVE 0 TO RET-VERIFY 
        PERFORM VARYING I FROM 1 BY 1 
                UNTIL I > LEN-STR-IN OR RET-VERIFY > 0
        MOVE STR-IN(I:1) TO CURRENT-CHAR
        IF CURRENT-CHAR IS NUMERIC OR CURRENT-CHAR IS ALPHABETIC THEN 
          CONTINUE
        ELSE
          MOVE I TO RET-VERIFY
        END-IF
        END-PERFORM.
        END-ALPHANUMERIC-PARA.
        
      * ALPHABETIC  
        ALPHABETIQUE-PARA.
        MOVE 0 TO RET-VERIFY.
        PERFORM VARYING I FROM 1 BY 1 
                UNTIL I > LEN-STR-IN OR RET-VERIFY > 0
        MOVE STR-IN(I:1) TO CURRENT-CHAR
        IF CURRENT-CHAR IS ALPHABETIC THEN 
          CONTINUE
        ELSE 
          MOVE I TO RET-VERIFY
        END-IF
        END-PERFORM.
        END-ALPHABETIQUE-PARA.

      * TYPE-2 
        TYPE-2-PARA.
        MOVE 0 TO RET-VERIFY.
        PERFORM VARYING I FROM 1 BY 1
                UNTIL I > LEN-STR-IN OR RET-VERIFY > 0
        MOVE STR-IN(I:1) TO CURRENT-CHAR
        IF TYPE-2 THEN   
          CONTINUE
        ELSE 
          MOVE I TO RET-VERIFY
        END-IF 
        END-PERFORM.
        END-TYPE-2-PARA.

        END PROGRAM FUNC-VERIFY .
