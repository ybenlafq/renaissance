      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. MPTNM.                                                       
       AUTHOR. JG.                                                              
      ******************************************************************        
      *  Contr�le et mise en forme d'une zone de saisie num�rique               
      *  et/ou mise en format d'�dition d'une zone num�rique                    
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       DATA DIVISION.                                                           
      *****************************************************************         
      *  OUORQUE                                                                
      *****************************************************************         
       WORKING-STORAGE SECTION.                                                 
           COPY COMMPTNM.                                                       
      ** Sauvegarde ZEDIT                                                       
       01  W-ZEDIT               PIC X(20) VALUE SPACES.                        
      ** Indices                                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-L                   PIC S9(4) COMP.                                
      *--                                                                       
       01  I-L                   PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-I                   PIC S9(4) COMP.                                
      *--                                                                       
       01  I-I                   PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-POS-DEC             PIC S9(4) COMP.                                
      *--                                                                       
       01  I-POS-DEC             PIC S9(4) COMP-5.                              
      *}                                                                        
      ** Longueur des parties enti�re et d�cimale                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-L-ENT               PIC S9(4) COMP.                                
      *--                                                                       
       01  W-L-ENT               PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-L-DEC               PIC S9(4) COMP.                                
      *--                                                                       
       01  W-L-DEC               PIC S9(4) COMP-5.                              
      *}                                                                        
      ** Sauvegarde parties enti�re et d�cimale                                 
       01  W-ENT                 PIC X(20) VALUE SPACES.                        
       01  W-DEC                 PIC X(20) VALUE SPACES.                        
      ** Valeurs des parties enti�re et d�cimale                                
       01  X-ENT                 PIC X(15).                                     
       01  P-ENT REDEFINES X-ENT PIC 9(15).                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-ENT                 PIC S9(4) COMP.                                
      *--                                                                       
       01  I-ENT                 PIC S9(4) COMP-5.                              
      *}                                                                        
       01  X-DEC                 PIC X(15).                                     
       01  P-DEC REDEFINES X-DEC PIC 9(15).                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-DEC                 PIC S9(4) COMP.                                
      *--                                                                       
       01  I-DEC                 PIC S9(4) COMP-5.                              
      *}                                                                        
      ** Signe                                                                  
       01  W-SIGNE               PIC X.                                         
           88  W-SIGNE-MOINS     VALUE '-'.                                     
           88  W-SIGNE-PLUS      VALUE '+'.                                     
      ** S�parateurs                                                            
       01  W-SEP-DEC             PIC X.                                         
           88  W-SEP-DEC-POINT   VALUE '.'.                                     
           88  W-SEP-DEC-VIRGULE VALUE ','.                                     
       01  W-SEP-MIL             PIC X.                                         
           88  W-SEP-MIL-POINT   VALUE '.'.                                     
           88  W-SEP-MIL-VIRGULE VALUE ','.                                     
           88  W-SEP-MIL-ESPACE  VALUE ' '.                                     
      ** Flags: D�cimales                                                       
       01  F-NBDEC               PIC X.                                         
       01  F-NBDEC-P REDEFINES F-NBDEC                                          
                                 PIC 9.                                         
      ** Flags: Valeurs admises                                                 
       01  F-VALEURS             PIC X.                                         
           88  F-TOUT            VALUE '0'.                                     
           88  F-NON-NUL         VALUE '1'.                                     
           88  F-NEG-OU-NUL      VALUE '2'.                                     
           88  F-NEGATIF         VALUE '3'.                                     
           88  F-POS-OU-NUL      VALUE '4'.                                     
           88  F-POSITIF         VALUE '5'.                                     
           88  F-NUL             VALUE '6'.                                     
      ** Flags: S�parateur des milliers                                         
       01  F-SEPMIL              PIC X.                                         
           88  F-SANS-SEP        VALUE ' '.                                     
           88  F-AVEC-SEP        VALUE 'M'.                                     
      ** Flags: Signe                                                           
       01  F-SIGNE                PIC X.                                        
           88  F-SIGNE-MOINS     VALUE '-'.                                     
           88  F-SIGNE-PLUS      VALUE '+'.                                     
      *****************************************************************         
      *  LINKAGE                                                                
      *****************************************************************         
       LINKAGE SECTION.                                                         
       01  DFHCOMMAREA.                                                         
           05  FILLER PIC X OCCURS 200 DEPENDING ON EIBCALEN.                   
      *****************************************************************         
      *  PROCEDURE DIVISION                                                     
      *  - Contr�le des param�tres d'appel                                      
      *  - D�codage de la zone caract�re (si besoin)                            
      *  - Mise en forme de la valeur num�rique                                 
      *****************************************************************         
       PROCEDURE DIVISION.                                                      
           MOVE DFHCOMMAREA TO Z-COMMAREA-MPTNM.                                
           SET PTNM-OK      TO TRUE.                                            
           SET PTNM-NM      TO TRUE.                                            
           MOVE SPACES      TO PTNM-NSEQERR.                                    
           PERFORM CONTROLE-PARAMETRES.                                         
           IF PTNM-OK                                                           
              IF PTNM-ZEDIT NOT = SPACES                                        
                 PERFORM DECODAGE-NUMERIQUE                                     
              END-IF                                                            
              IF PTNM-OK                                                        
                 PERFORM MISE-EN-FORME-VALEUR                                   
              END-IF                                                            
           END-IF.                                                              
           MOVE Z-COMMAREA-MPTNM   TO DFHCOMMAREA.                              
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS RETURN END-EXEC.                                           
      *--                                                                       
           EXEC CICS RETURN                                                     
           END-EXEC.                                                            
      *}                                                                        
      *****************************************************************         
      *  Contr�le des param�tres d'appel                                        
      *****************************************************************         
       CONTROLE-PARAMETRES SECTION.                                             
      **> Code PIC envoy� par le programme appelant                             
           EVALUATE PTNM-CODPIC                                                 
              WHEN '01'                                                         
                 SET W-SEP-DEC-VIRGULE TO TRUE                                  
                 SET W-SEP-MIL-ESPACE  TO TRUE                                  
              WHEN '02'                                                         
                 SET W-SEP-DEC-POINT   TO TRUE                                  
                 SET W-SEP-MIL-VIRGULE TO TRUE                                  
              WHEN '03'                                                         
                 SET W-SEP-DEC-VIRGULE TO TRUE                                  
                 SET W-SEP-MIL-POINT   TO TRUE                                  
              WHEN OTHER                                                        
                 IF PTNM-OK                                                     
                    MOVE 'NC00' TO PTNM-NSEQERR                                 
                    SET PTNM-KO TO TRUE                                         
                 END-IF                                                         
           END-EVALUATE                                                         
      **> Longueur de la zone                                                   
           IF PTNM-LONG NOT NUMERIC                                             
              IF PTNM-OK                                                        
                 MOVE 'NC01' TO PTNM-NSEQERR                                    
                 SET PTNM-KO TO TRUE                                            
              END-IF                                                            
           ELSE                                                                 
              IF PTNM-LONG > 20 OR < 1                                          
                 IF PTNM-OK                                                     
                    MOVE 'NC01' TO PTNM-NSEQERR                                 
                    SET PTNM-KO TO TRUE                                         
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      **> Eclatement du flag param�tre de Commarea                              
           MOVE PTNM-PARAMETRES(1:1) TO F-NBDEC.                                
           MOVE PTNM-PARAMETRES(2:1) TO F-VALEURS.                              
           MOVE PTNM-PARAMETRES(3:1) TO F-SEPMIL.                               
           MOVE PTNM-PARAMETRES(4:1) TO F-SIGNE.                                
      **> Nombre de d�cimales                                                   
           IF F-NBDEC < '0' OR > '7'                                            
              IF PTNM-OK                                                        
                 MOVE 'NC02' TO PTNM-NSEQERR                                    
                 SET PTNM-KO TO TRUE                                            
              END-IF                                                            
           END-IF.                                                              
      **> Valeurs                                                               
           IF F-VALEURS < '0' OR > '6'                                          
              IF PTNM-OK                                                        
                 MOVE 'NC02' TO PTNM-NSEQERR                                    
                 SET PTNM-KO TO TRUE                                            
              END-IF                                                            
           END-IF.                                                              
      **> S�parateur                                                            
           IF F-SANS-SEP OR F-AVEC-SEP                                          
              NEXT SENTENCE                                                     
           ELSE                                                                 
              IF PTNM-OK                                                        
                 MOVE 'NC02' TO PTNM-NSEQERR                                    
                 SET PTNM-KO TO TRUE                                            
              END-IF                                                            
           END-IF.                                                              
      **> Signe                                                                 
           IF F-SIGNE-MOINS OR F-SIGNE-PLUS                                     
              NEXT SENTENCE                                                     
           ELSE                                                                 
              IF PTNM-OK                                                        
                 MOVE 'NC02' TO PTNM-NSEQERR                                    
                 SET PTNM-KO TO TRUE                                            
              END-IF                                                            
           END-IF.                                                              
      **> Longueur de la zone par rapport aux d�cimales                         
           IF PTNM-OK                                                           
              IF F-NBDEC-P > 0                                                  
                 COMPUTE I-L = F-NBDEC-P + 2                                    
                 IF I-L > PTNM-LONG                                             
                    MOVE 'NC03' TO PTNM-NSEQERR                                 
                    SET PTNM-KO TO TRUE                                         
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *****************************************************************         
      *  D�codage de la zone caract�re                                          
      *****************************************************************         
       DECODAGE-NUMERIQUE SECTION.                                              
      **> Sauvegarde de ZEDIT (on travaille sur la sauvegarde)                  
           MOVE PTNM-ZEDIT TO W-ZEDIT.                                          
      **> On d�tecte la position du s�parateur d�cimal.                         
      **> Il doit y en avoir un seul.                                           
           MOVE 0 TO I-POS-DEC.                                                 
           PERFORM VARYING I-L FROM 1 BY 1 UNTIL I-L > PTNM-LONG                
              IF W-ZEDIT(I-L:1) = W-SEP-DEC                                     
                 IF I-POS-DEC NOT = 0                                           
                    MOVE 'ND00' TO PTNM-NSEQERR                                 
                    SET PTNM-KO TO TRUE                                         
                    GO FIN-DECODAGE-NUMERIQUE                                   
                 END-IF                                                         
                 MOVE I-L TO I-POS-DEC                                          
              END-IF                                                            
           END-PERFORM.                                                         
      **> On d�tecte le signe.                                                  
      **> Il doit y en avoir un seul et �tre le premier caract�re               
           MOVE ' ' TO W-SIGNE.                                                 
           PERFORM VARYING I-L FROM 1 BY 1 UNTIL I-L > PTNM-LONG                
              IF W-SIGNE = ' '                                                  
                 IF W-ZEDIT(I-L:1) NOT = '+' AND '-' AND ' '                    
                    MOVE W-ZEDIT(I-L:1) TO W-SIGNE                              
                 END-IF                                                         
              END-IF                                                            
              IF W-ZEDIT(I-L:1) = '+' OR '-'                                    
                 IF W-SIGNE NOT = ' '                                           
                    MOVE 'ND01' TO PTNM-NSEQERR                                 
                    SET PTNM-KO TO TRUE                                         
                    GO FIN-DECODAGE-NUMERIQUE                                   
                 END-IF                                                         
                 MOVE W-ZEDIT(I-L:1) TO W-SIGNE                                 
                 MOVE ' ' TO W-ZEDIT(I-L:1)                                     
              END-IF                                                            
           END-PERFORM.                                                         
      **> calcul de la longueur des parties enti�re et d�cimale.                
      **> (signe d�cimal doit suivre la partie enti�re, si existante)           
           IF I-POS-DEC = 0                                                     
              COMPUTE W-L-ENT = PTNM-LONG                                       
              COMPUTE W-L-DEC = 0                                               
           ELSE                                                                 
              COMPUTE W-L-ENT = I-POS-DEC - 1                                   
              IF W-L-ENT > 0                                                    
                 IF (W-ZEDIT(1:W-L-ENT) > ' ')                                  
                  AND (W-ZEDIT(W-L-ENT:1) < '0' OR > '9')                       
                    MOVE 'ND01' TO PTNM-NSEQERR                                 
                    SET PTNM-KO TO TRUE                                         
                    GO FIN-DECODAGE-NUMERIQUE                                   
                 END-IF                                                         
              END-IF                                                            
              COMPUTE W-L-DEC = PTNM-LONG - I-POS-DEC                           
           END-IF.                                                              
      **> Sauvegarde des parties enti�re et d�cimale.                           
           MOVE W-ZEDIT(1:W-L-ENT) TO W-ENT.                                    
           IF I-POS-DEC > 0                                                     
              COMPUTE I-L = I-POS-DEC + 1                                       
              MOVE W-ZEDIT(I-L:W-L-DEC) TO W-DEC                                
           END-IF.                                                              
      **> Contr�le partie enti�re                                               
           IF W-ENT > ' '                                                       
      **> position du premier chiffre                                           
              PERFORM VARYING I-L FROM 1 BY 1                                   
                UNTIL (I-L > 20)                                                
                   OR (W-ENT(I-L:1) > ' ')                                      
              END-PERFORM                                                       
      **> deux s�parateurs ne peuvent pas se suivre                             
              PERFORM VARYING I-L FROM I-L BY 1                                 
                UNTIL (I-L > 20)                                                
                   OR (W-ENT(I-L:) = ' ')                                       
                 IF (W-ENT(I-L:1) < '0' OR > '9') AND (I-L < 20)                
                    COMPUTE I-I = I-L + 1                                       
                    IF (W-ENT(I-I:) > ' ')                                      
                      AND (W-ENT(I-I:1) < '0' OR > '9')                         
                       MOVE 'ND01' TO PTNM-NSEQERR                              
                       SET PTNM-KO TO TRUE                                      
                       GO FIN-DECODAGE-NUMERIQUE                                
                    END-IF                                                      
                 END-IF                                                         
              END-PERFORM                                                       
      **> re-position du premier chiffre                                        
              PERFORM VARYING I-L FROM 1 BY 1                                   
                UNTIL (I-L > 20)                                                
                   OR (W-ENT(I-L:1) > ' ')                                      
              END-PERFORM                                                       
      **> le premier groupe peut avoir n'importe quelle longueur                
              PERFORM VARYING I-L FROM I-L BY 1                                 
                UNTIL (I-L > 20)                                                
                   OR (W-ENT(I-L:1) < '0' OR > '9')                             
                   OR (W-ENT(I-L:) = ' ')                                       
              END-PERFORM                                                       
      **> les groupes suivants doivent avoir trois positions                    
              PERFORM VARYING I-L FROM I-L BY 1                                 
                UNTIL (I-L > 20)                                                
                   OR (W-ENT(I-L:) = ' ')                                       
                 IF I-L > 18                                                    
                    MOVE 'ND01' TO PTNM-NSEQERR                                 
                    SET PTNM-KO TO TRUE                                         
                    GO FIN-DECODAGE-NUMERIQUE                                   
                 END-IF                                                         
                 IF W-ENT(I-L:1) NUMERIC                                        
                    IF W-ENT(I-L:3) NOT NUMERIC                                 
                       MOVE 'ND01' TO PTNM-NSEQERR                              
                       SET PTNM-KO TO TRUE                                      
                       GO FIN-DECODAGE-NUMERIQUE                                
                    END-IF                                                      
                    ADD 2 TO I-L                                                
                 END-IF                                                         
              END-PERFORM                                                       
           END-IF.                                                              
      **> Contr�le partie d�cimale: les chiffres doivent se suivre              
           IF W-DEC > ' '                                                       
              PERFORM VARYING I-L FROM 1 BY 1                                   
                UNTIL (I-L > 20)                                                
                   OR (W-DEC(I-L:) = ' ')                                       
                 IF W-DEC(I-L:1) < '0' OR > '9'                                 
                    MOVE 'ND01' TO PTNM-NSEQERR                                 
                    SET PTNM-KO TO TRUE                                         
                    GO FIN-DECODAGE-NUMERIQUE                                   
                 END-IF                                                         
              END-PERFORM                                                       
           END-IF.                                                              
      **> On r�cup�re les chiffres de la partie enti�re.                        
           MOVE 16 TO I-ENT                                                     
           MOVE ZERO TO P-ENT.                                                  
           PERFORM VARYING I-L FROM W-L-ENT BY -1 UNTIL I-L = 0                 
              IF W-ENT(I-L:1) NOT = ' ' AND W-SEP-MIL                           
                 IF W-ENT(I-L:1) < '0' OR > '9'                                 
                    MOVE 'ND02' TO PTNM-NSEQERR                                 
                    SET PTNM-KO TO TRUE                                         
                    GO FIN-DECODAGE-NUMERIQUE                                   
                 ELSE                                                           
                    SUBTRACT 1 FROM I-ENT                                       
                    IF I-ENT = 0                                                
                       MOVE 'ND03' TO PTNM-NSEQERR                              
                       SET PTNM-KO TO TRUE                                      
                       GO FIN-DECODAGE-NUMERIQUE                                
                    END-IF                                                      
                    MOVE W-ENT(I-L:1) TO X-ENT(I-ENT:1)                         
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
      **> Il y a trop d'entiers                                                 
           PERFORM VARYING I-ENT FROM 1 BY 1 UNTIL I-ENT > F-NBDEC-P            
              IF X-ENT(I-ENT:1) NOT = '0'                                       
                 MOVE 'ND03' TO PTNM-NSEQERR                                    
                 SET PTNM-KO TO TRUE                                            
                 GO FIN-DECODAGE-NUMERIQUE                                      
              END-IF                                                            
           END-PERFORM.                                                         
      **> On r�cup�re les chiffres de la partie d�cimale                        
           MOVE 0 TO I-DEC                                                      
           MOVE ZERO TO P-DEC.                                                  
           PERFORM VARYING I-L FROM 1 BY 1 UNTIL I-L > W-L-DEC                  
              IF W-DEC(I-L:1) NOT = ' '                                         
                 IF W-DEC(I-L:1) < '0' OR > '9'                                 
                    MOVE 'ND02' TO PTNM-NSEQERR                                 
                    SET PTNM-KO TO TRUE                                         
                    GO FIN-DECODAGE-NUMERIQUE                                   
                 ELSE                                                           
                    ADD 1 TO I-DEC                                              
                    IF I-DEC > 15                                               
                       MOVE 'ND03' TO PTNM-NSEQERR                              
                       SET PTNM-KO TO TRUE                                      
                       GO FIN-DECODAGE-NUMERIQUE                                
                    END-IF                                                      
                    MOVE W-DEC(I-L:1) TO X-DEC(I-DEC:1)                         
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
      **> Il y a trop de d�cimales                                              
           PERFORM VARYING I-DEC FROM 15 BY -1                                  
             UNTIL (X-DEC(I-DEC:1) NOT = '0') OR (I-DEC = 0)                    
           END-PERFORM.                                                         
           IF I-DEC > F-NBDEC-P                                                 
              MOVE 'ND03' TO PTNM-NSEQERR                                       
              SET PTNM-KO TO TRUE                                               
              GO FIN-DECODAGE-NUMERIQUE                                         
           END-IF.                                                              
      **> On multiplie la valeur enti�re par 10 ** F-NBDEC-P                    
           IF F-NBDEC-P > 0                                                     
              COMPUTE P-ENT = P-ENT * (10 ** F-NBDEC-P)                         
              COMPUTE I-L = 16 - F-NBDEC-P                                      
              MOVE X-DEC(1:F-NBDEC-P) TO X-ENT(I-L:)                            
           END-IF.                                                              
      **> Mise en forme du r�sultat                                             
           COMPUTE PTNM-ZNUM = P-ENT.                                           
           IF W-SIGNE-MOINS                                                     
              COMPUTE PTNM-ZNUM = PTNM-ZNUM * -1                                
           END-IF.                                                              
       FIN-DECODAGE-NUMERIQUE. EXIT.                                            
      *****************************************************************         
      *  Mise en forme de la valeur num�rique                                   
      *****************************************************************         
       MISE-EN-FORME-VALEUR SECTION.                                            
           IF PTNM-ZNUM NOT NUMERIC                                             
              MOVE 'NF00' TO PTNM-NSEQERR                                       
              SET PTNM-KO TO TRUE                                               
              GO FIN-MISE-EN-FORME-VALEUR                                       
           END-IF.                                                              
           COMPUTE P-ENT = PTNM-ZNUM.                                           
           MOVE SPACES TO W-ENT.                                                
           IF F-NBDEC-P > 0                                                     
              COMPUTE I-ENT = 16 - F-NBDEC-P                                    
              COMPUTE W-L-ENT = PTNM-LONG - F-NBDEC-P + 1                       
              MOVE X-ENT(I-ENT:) TO W-ENT(W-L-ENT:)                             
              SUBTRACT 1 FROM I-ENT                                             
              SUBTRACT 1 FROM W-L-ENT                                           
              MOVE W-SEP-DEC TO W-ENT(W-L-ENT:1)                                
              SUBTRACT 1 FROM W-L-ENT                                           
           ELSE                                                                 
              COMPUTE I-ENT = 15                                                
              COMPUTE W-L-ENT = PTNM-LONG                                       
           END-IF.                                                              
           PERFORM UNTIL I-ENT = 0                                              
              IF (I-ENT >= 3) AND (W-L-ENT >= 3)                                
                 SUBTRACT 2 FROM I-ENT                                          
                 SUBTRACT 2 FROM W-L-ENT                                        
                 MOVE X-ENT(I-ENT:3) TO W-ENT(W-L-ENT:3)                        
                 SUBTRACT 1 FROM I-ENT                                          
                 SUBTRACT 1 FROM W-L-ENT                                        
                 IF (W-L-ENT > 0) AND (F-AVEC-SEP)                              
                    MOVE W-SEP-MIL TO W-ENT(W-L-ENT:1)                          
                    SUBTRACT 1 FROM W-L-ENT                                     
                 END-IF                                                         
              ELSE                                                              
                 IF I-ENT >= W-L-ENT                                            
                    IF W-L-ENT > 0                                              
                       COMPUTE I-ENT = I-ENT - W-L-ENT + 1                      
                       MOVE X-ENT(I-ENT:W-L-ENT) TO W-ENT(1:W-L-ENT)            
                       SUBTRACT 1 FROM I-ENT                                    
                    END-IF                                                      
                    PERFORM VARYING I-ENT FROM I-ENT BY -1                      
                      UNTIL I-ENT < 1                                           
                       IF X-ENT(I-ENT:1) NOT = '0'                              
                          MOVE 'NF01' TO PTNM-NSEQERR                           
                          SET PTNM-KO TO TRUE                                   
                          GO FIN-MISE-EN-FORME-VALEUR                           
                       END-IF                                                   
                    END-PERFORM                                                 
                    MOVE 0 TO I-ENT W-L-ENT                                     
                 ELSE                                                           
                    COMPUTE W-L-ENT = W-L-ENT - I-ENT + 1                       
                    MOVE X-ENT(1:I-ENT) TO W-ENT(W-L-ENT:I-ENT)                 
                    MOVE 0 TO I-ENT W-L-ENT                                     
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
           COMPUTE W-L-ENT = PTNM-LONG - 1.                                     
           PERFORM VARYING I-L FROM 1 BY 1 UNTIL I-L > W-L-ENT                  
              COMPUTE I-ENT = I-L + 1                                           
              IF W-ENT(I-ENT:1) = W-SEP-DEC                                     
                 MOVE 99 TO I-L                                                 
              ELSE                                                              
                 IF W-ENT(I-L:1) = ' ' OR '0' OR W-SEP-MIL                      
                    MOVE ' ' TO W-ENT(I-L:1)                                    
                 ELSE                                                           
                    MOVE 99 TO I-L                                              
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
           PERFORM VARYING I-L FROM 1 BY 1 UNTIL I-L > PTNM-LONG                
              IF W-ENT(I-L:1) > ' '                                             
                 IF I-L > 1                                                     
                    SUBTRACT 1 FROM I-L                                         
                 END-IF                                                         
                 IF PTNM-ZNUM < 0                                               
                    IF W-ENT(I-L:1) > ' '                                       
                       MOVE 'NF01' TO PTNM-NSEQERR                              
                       SET PTNM-KO TO TRUE                                      
                       GO FIN-MISE-EN-FORME-VALEUR                              
                    END-IF                                                      
                    MOVE '-' TO W-ENT(I-L:1)                                    
                 ELSE                                                           
                    IF F-SIGNE-PLUS                                             
                       IF W-ENT(I-L:1) > ' '                                    
                          MOVE 'NF01' TO PTNM-NSEQERR                           
                          SET PTNM-KO TO TRUE                                   
                          GO FIN-MISE-EN-FORME-VALEUR                           
                       END-IF                                                   
                       MOVE '+' TO W-ENT(I-L:1)                                 
                    END-IF                                                      
                 END-IF                                                         
                 MOVE 99 TO I-L                                                 
              END-IF                                                            
           END-PERFORM.                                                         
           MOVE W-ENT TO PTNM-ZEDIT PTNM-ZEDIT-GAUCHE.                          
           PERFORM VARYING I-L FROM 1 BY 1                                      
             UNTIL PTNM-ZEDIT-GAUCHE(1:1) > ' '                                 
              MOVE PTNM-ZEDIT-GAUCHE(2:) TO W-ENT                               
              MOVE W-ENT TO PTNM-ZEDIT-GAUCHE                                   
           END-PERFORM.                                                         
           SET PTNM-MV TO TRUE.                                                 
           IF F-NON-NUL AND PTNM-ZNUM = 0                                       
              MOVE 'NF02' TO PTNM-NSEQERR                                       
              SET PTNM-KO TO TRUE                                               
              GO FIN-MISE-EN-FORME-VALEUR                                       
           END-IF.                                                              
           IF F-NEG-OU-NUL AND PTNM-ZNUM > 0                                    
              MOVE 'NF03' TO PTNM-NSEQERR                                       
              SET PTNM-KO TO TRUE                                               
              GO FIN-MISE-EN-FORME-VALEUR                                       
           END-IF.                                                              
           IF F-NEGATIF AND PTNM-ZNUM >= 0                                      
              MOVE 'NF04' TO PTNM-NSEQERR                                       
              SET PTNM-KO TO TRUE                                               
              GO FIN-MISE-EN-FORME-VALEUR                                       
           END-IF.                                                              
           IF F-POS-OU-NUL AND PTNM-ZNUM < 0                                    
              MOVE 'NF05' TO PTNM-NSEQERR                                       
              SET PTNM-KO TO TRUE                                               
              GO FIN-MISE-EN-FORME-VALEUR                                       
           END-IF.                                                              
           IF F-POSITIF AND PTNM-ZNUM <= 0                                      
              MOVE 'NF06' TO PTNM-NSEQERR                                       
              SET PTNM-KO TO TRUE                                               
              GO FIN-MISE-EN-FORME-VALEUR                                       
           END-IF.                                                              
           IF F-NUL    AND PTNM-ZNUM NOT = 0                                    
              MOVE 'NF07' TO PTNM-NSEQERR                                       
              SET PTNM-KO TO TRUE                                               
              GO FIN-MISE-EN-FORME-VALEUR                                       
           END-IF.                                                              
       FIN-MISE-EN-FORME-VALEUR. EXIT.                                          
