      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. BVC003.                                                      
       AUTHOR. ANNE LAROCHE.                                                    
      ******************************************************************        
      *                                                                *        
      *  PROGRAMME  : BVC003                                           *        
      *  CREATION   : 11/2000                                          *        
      *  FONCTION   : EDITION DE LA LISTE DES MAGASINS AYANT DES       *        
      *               ECARTS VOLUMES ET / OU CHIFFRES D'AFFAIRES       *        
      *  PERIODICITE: QUOTIDIENNE                                      *        
      *                                                                *        
      * INPUT:                                                         *        
      * FVC004  FICHIER CONTENANT LES MAGASINS PRESENTANT DES ECARTS   *        
      *           SUR VOLUME ET / OU CHIFFRE D'AFFAIRES                *        
      * FVC005  FICHIER CONTENANT LES TOTAUX                           *        
      * FDATE                                                          *        
      * FPER                                                           *        
      *                                                                *        
      * OUTPUT:                                                        *        
      * EDITION                                                        *        
      ******************************************************************        
      * MODIF PR 04/01/2001 : ARRET DU PGM SI FVC005 EST VIDE          *        
      *  (SCAN : PR1)                                                  *        
      *----------------------------------------------------------------*        
      * MODIF PR 11/01/2001 : PRISE EN COMPTE DES PIECES RECYCLEES CAD *        
      *  (SCAN : PR2)         AJOUT DE 2 COLONNES A L'ETAT             *        
      *----------------------------------------------------------------*        
      * MODIF PR 23/04/2001 : AJOUT DU NOM DE L'ETAT SUR LA 1ERE LIGNE *        
      *  (SCAN : PR3)                                                  *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
            DECIMAL-POINT IS COMMA.                                             
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FVC004  ASSIGN TO FVC004.                                    
      *--                                                                       
            SELECT FVC004  ASSIGN TO FVC004                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FVC005  ASSIGN TO FVC005.                                    
      *--                                                                       
            SELECT FVC005  ASSIGN TO FVC005                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FDATE   ASSIGN TO FDATE.                                     
      *--                                                                       
            SELECT FDATE   ASSIGN TO FDATE                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FPER    ASSIGN TO FPER.                                      
      *--                                                                       
            SELECT FPER    ASSIGN TO FPER                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT EDITION ASSIGN TO EDITION.                                   
      *--                                                                       
            SELECT EDITION ASSIGN TO EDITION                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *    FVC004 : FICHIER CONTENANT LES ECARTS                                
      ******************************************                                
       FD  FVC004 RECORDING F BLOCK 0 RECORDS                                   
           LABEL RECORD STANDARD.                                               
       01  MW-FILLER     PIC X(71).                                             
      *    FVC005 : FICHIER FVC004 CUMULE                                       
      ***********************************                                       
       FD  FVC005 RECORDING F BLOCK 0 RECORDS                                   
           LABEL RECORD STANDARD.                                               
       01  MW-FILLER     PIC X(71).                                             
      *    EDITION: ETAT RECAPITULATIF DES MAGASINS EN ECART                    
      ******************************************************                    
       FD  EDITION RECORDING F BLOCK 0 RECORDS                                  
           LABEL RECORD STANDARD.                                               
       01  ENR-EDITION PIC X(133).                                              
      *    FDATE                                                                
      **********                                                                
       FD  FDATE RECORDING F BLOCK 0 RECORDS                                    
           LABEL RECORD STANDARD.                                               
       01  MW-FILLER PIC X(80).                                                 
      *    FPER                                                                 
      **********                                                                
       FD  FPER  RECORDING F BLOCK 0 RECORDS                                    
           LABEL RECORD STANDARD.                                               
      *MW DAR-2                                                                 
       01  MW-FILLER PIC X(80).                                                 
       WORKING-STORAGE SECTION.                                                 
      *    FDATE                                                                
      **********                                                                
       01  BETDATC PIC X(8) VALUE 'BETDATC'.                                    
           COPY  WORKDATC.                                                      
      *    FPER                                                                 
      **********                                                                
       01  W-FPER  PIC X(4).                                                    
      *    FVC004                                                               
      ***********                                                               
       01  FILLER  PIC X VALUE SPACE.                                           
           88 FIN-FVC004 VALUE 'F'.                                             
       01  CPT-FVC004 PIC S9(7) COMP-3.                                         
       01  ENR-FVC004.                                                          
     1     05 FVC004-NSOC            PIC X(03).                                 
     4     05 FVC004-LIEU            PIC X(03).                                 
     7     05 FVC004-TOTVOLCIAL      PIC S9(9) COMP-3.                          
    12     05 FVC004-TOTVOLLIVR      PIC S9(9) COMP-3.                          
    17     05 FVC004-LIVRSTAT        PIC S9(9) COMP-3.                          
    22     05 FVC004-ECARTLI         PIC S9(9) COMP-3.                          
    27     05 FVC004-DIFFVOL         PIC S9(9) COMP-3.                          
    32     05 FVC004-CASTAT          PIC S9(13)V9(2) COMP-3.                    
    40     05 FVC004-CACPTA          PIC S9(13)V9(2) COMP-3.                    
    48     05 FVC004-DIFFCA          PIC S9(13)V9(2) COMP-3.                    
PR2 56     05 FVC004-CACPTARECY      PIC S9(13)V9(2) COMP-3.                    
PR2 64     05 FVC004-DIFFCATOT       PIC S9(13)V9(2) COMP-3.                    
      *    FVC005                                                               
      ***********                                                               
       01  ENR-FVC005.                                                          
     1     05 FVC005-NSOC            PIC X(03).                                 
     4     05 FVC005-LIEU            PIC X(03).                                 
     7     05 FVC005-TOTVOLCIAL      PIC S9(9) COMP-3.                          
    12     05 FVC005-TOTVOLLIVR      PIC S9(9) COMP-3.                          
    17     05 FVC005-LIVRSTAT        PIC S9(9) COMP-3.                          
    22     05 FVC005-ECARTLI         PIC S9(9) COMP-3.                          
    27     05 FVC005-DIFFVOL         PIC S9(9) COMP-3.                          
    32     05 FVC005-CASTAT          PIC S9(13)V9(2) COMP-3.                    
    40     05 FVC005-CACPTA          PIC S9(13)V9(2) COMP-3.                    
    48     05 FVC005-DIFFCA          PIC S9(13)V9(2) COMP-3.                    
PR2 56     05 FVC005-CACPTARECY      PIC S9(13)V9(2) COMP-3.                    
PR2 64     05 FVC005-DIFFCATOT       PIC S9(13)V9(2) COMP-3.                    
PR1    01  FILLER PIC X.                                                        
PR1        88 OK-FVC005  VALUE '0'.                                             
PR1        88 FIN-FVC005 VALUE '9'.                                             
      *    EDITION                                                              
      ************                                                              
       01  EDITION-ENR    PIC X(133).                                           
      * INTITULES                                                               
       01  INTITULE-JOUR.                                                       
           05 L1-CSAUT     PIC X(01) VALUE ' '.                                 
PR3        05 FILLER       PIC X(09) VALUE '!  BVC003'.                         
PR3        05 FILLER       PIC X(30) VALUE SPACES.                              
           05 FILLER       PIC X(20) VALUE 'Liste des �carts des'.              
           05 FILLER       PIC X(25) VALUE ' magasins � la date du : '.         
           05 L1-DATE.                                                          
              10 L1-JOUR   PIC X(02).                                           
              10 FILLER    PIC X(01) VALUE '/'.                                 
              10 L1-MOIS   PIC X(02).                                           
              10 FILLER    PIC X(01) VALUE '/'.                                 
              10 L1-SIECLE PIC X(02).                                           
              10 L1-ANNEE  PIC X(02).                                           
           05 FILLER       PIC X(27) VALUE SPACE.                               
           05 FILLER       PIC X(07) VALUE 'Page n�'.                           
           05 NO-PAGE      PIC Z9.                                              
           05 FILLER       PIC X(02) VALUE ' !'.                                
       01  INTITULE-MOIS.                                                       
           05 L2-CSAUT      PIC X(01) VALUE ' '.                                
PR3        05 FILLER        PIC X(10) VALUE '!  BVC003M'.                       
PR3        05 FILLER        PIC X(20) VALUE SPACE.                              
PR3        05 FILLER        PIC X(20) VALUE 'Liste des �carts des'.             
           05 FILLER        PIC X(25) VALUE ' magasins - p�riode du : '.        
           05 L2-DATEDEB.                                                       
              10 L2-JOUR1   PIC X(02).                                          
              10 FILLER     PIC X(01) VALUE '/'.                                
              10 L2-MOIS1   PIC X(02).                                          
              10 FILLER     PIC X(01) VALUE '/'.                                
              10 L2-SIECLE1 PIC X(02).                                          
              10 L2-ANNEE1  PIC X(02).                                          
           05 FILLER        PIC X(06) VALUE ' au : '.                           
           05 L2-DATEDEB.                                                       
              10 L2-JOUR2   PIC X(02).                                          
              10 FILLER     PIC X(01) VALUE '/'.                                
              10 L2-MOIS2   PIC X(02).                                          
              10 FILLER     PIC X(01) VALUE '/'.                                
              10 L2-SIECLE2 PIC X(02).                                          
              10 L2-ANNEE2  PIC X(02).                                          
           05 FILLER        PIC X(20) VALUE SPACE.                              
           05 FILLER        PIC X(07) VALUE 'Page n�'.                          
           05 NO-PAGE       PIC Z9.                                             
           05 FILLER        PIC X(02) VALUE ' !'.                               
      * ENTETE                                                                  
       01  LIGNE0.                                                              
           05 CSAUT   PIC X(01) VALUE '1'.                                      
           05 FILLER  PIC X(01) VALUE '+'.                                      
           05 FILLER  PIC X(130) VALUE ALL '-'.                                 
           05 FILLER  PIC X(01) VALUE '+'.                                      
       01  LIGNE1.                                                              
           05 CSAUT   PIC X(01) VALUE ' '.                                      
           05 FILLER  PIC X(15) VALUE '+-----+--------'.                        
           05 FILLER  PIC X(30) VALUE '+---------------------+-------'.         
           05 FILLER  PIC X(01) VALUE '+'.                                      
           05 FILLER  PIC X(85) VALUE ALL '-'.                                  
           05 FILLER  PIC X(01) VALUE '+'.                                      
       01  LIGNE2.                                                              
           05 CSAUT   PIC X(01) VALUE ' '.                                      
           05 FILLER  PIC X(15) VALUE '+-----+--------'.                        
           05 FILLER  PIC X(30) VALUE '+------+------+-------+-------'.         
           05 FILLER  PIC X(17) VALUE '+----------------'.                      
           05 FILLER  PIC X(17) VALUE '+----------------'.                      
           05 FILLER  PIC X(17) VALUE '+----------------'.                      
           05 FILLER  PIC X(18) VALUE '+-----------------'.                     
           05 FILLER  PIC X(18) VALUE '+----------------+'.                     
       02  LIGNE3.                                                              
           05 CSAUT   PIC X(01) VALUE ' '.                                      
           05 FILLER  PIC X(06) VALUE '!     '.                                 
           05 FILLER  PIC X(09) VALUE '! Volume '.                              
           05 FILLER  PIC X(22) VALUE '!    Volume livr�     '.                 
           05 FILLER  PIC X(08) VALUE '!       '.                               
           05 FILLER  PIC X(01) VALUE '!'.                                      
           05 FILLER  PIC X(33) VALUE SPACE.                                    
           05 FILLER  PIC X(18) VALUE 'Chiffre d''affaires'.                    
           05 FILLER  PIC X(34) VALUE SPACE.                                    
           05 FILLER  PIC X(01) VALUE '!'.                                      
       02  LIGNE4.                                                              
           05 CSAUT   PIC X(01) VALUE ' '.                                      
           05 FILLER  PIC X(06) VALUE '!Lieu '.                                 
           05 FILLER  PIC X(09) VALUE '! commer-'.                              
           05 FILLER  PIC X(22) VALUE '+------+------+-------'.                 
           05 FILLER  PIC X(08) VALUE '+ Ecart '.                               
           05 FILLER  PIC X(17) VALUE '+----------------'.                      
           05 FILLER  PIC X(17) VALUE '+----------------'.                      
           05 FILLER  PIC X(17) VALUE '+----------------'.                      
           05 FILLER  PIC X(18) VALUE '+-----------------'.                     
           05 FILLER  PIC X(18) VALUE '+----------------+'.                     
       02  LIGNE5.                                                              
           05 CSAUT   PIC X(01) VALUE ' '.                                      
           05 FILLER  PIC X(06) VALUE '!     '.                                 
           05 FILLER  PIC X(09) VALUE '!  cial  '.                              
           05 FILLER  PIC X(07) VALUE '! HV15 '.                                
           05 FILLER  PIC X(07) VALUE '! GS40 '.                                
           05 FILLER  PIC X(08) VALUE '! �cart '.                               
           05 FILLER  PIC X(08) VALUE '!       '.                               
           05 FILLER  PIC X(17) VALUE '!     Stats      '.                      
           05 FILLER  PIC X(17) VALUE '!     Compta     '.                      
           05 FILLER  PIC X(17) VALUE '!     Ecart      '.                      
           05 FILLER  PIC X(18) VALUE '! Compta recycl�s '.                     
           05 FILLER  PIC X(17) VALUE '!     Ecart      '.                      
           05 FILLER  PIC X(01) VALUE '!'.                                      
      * DETAIL DES DONNEES                                                      
       01  LIGDETAIL.                                                           
           05 FILLER            PIC X(01)  VALUE SPACE.                         
           05 FILLER            PIC X(02)  VALUE '! '.                          
           05 DETAIL-MAG        PIC X(03).                                      
           05 FILLER            PIC X(03)  VALUE ' ! '.                         
           05 DETAIL-VOLCO      PIC Z(5)9.                                      
           05 FILLER            PIC X(02)  VALUE ' !'.                          
           05 DETAIL-VOL15      PIC Z(5)9.                                      
           05 FILLER            PIC X(01)  VALUE '!'.                           
           05 DETAIL-VOL40      PIC Z(5)9.                                      
           05 FILLER            PIC X(02)  VALUE '! '.                          
           05 DETAIL-ECARTLI    PIC Z(4)9.                                      
           05 FILLER            PIC X(02)  VALUE ' !'.                          
           05 DETAIL-DIFVOL     PIC -Z(5)9.                                     
           05 FILLER            PIC X(02)  VALUE '! '.                          
           05 DETAIL-STAT       PIC -Z(09)9,99.                                 
           05 FILLER            PIC X(03)  VALUE ' ! '.                         
           05 DETAIL-CPTA       PIC -Z(09)9,99.                                 
           05 FILLER            PIC X(03)  VALUE ' ! '.                         
           05 DETAIL-DIFCA      PIC -Z(09)9,99.                                 
           05 FILLER            PIC X(04)  VALUE ' !  '.                        
           05 DETAIL-CPTA-RECY  PIC -Z(09)9,99.                                 
           05 FILLER            PIC X(03)  VALUE ' ! '.                         
           05 DETAIL-DIFCA-TOT  PIC -Z(09)9,99.                                 
           05 FILLER            PIC X(02)  VALUE ' !'.                          
      *    ABEND                                                                
      **********                                                                
           COPY  ABENDCOP.                                                      
      * AUTRES ZONES                                                            
      ******************************************************************        
       01  PIC-Z7          PIC ZZZZZZ9.                                         
       01  CPT-LIGNES      PIC S9(3) COMP-3.                                    
       01  NB-PAGES        PIC 99.                                              
       01  MAX-LIGNES      PIC S9(3) COMP-3 VALUE 55.                           
       PROCEDURE DIVISION.                                                      
      ******************************************************************        
      *                                                                *        
      * MODULE PRINCIPAL                                               *        
      *                                                                *        
      ******************************************************************        
       MODULE-BVC003 SECTION.                                                   
           PERFORM MODULE-ENTREE.                                               
PR1        IF OK-FVC005                                                         
             PERFORM MODULE-TRAITEMENT                                          
PR1        END-IF.                                                              
           PERFORM MODULE-SORTIE.                                               
       FIN-MODULE-BVC003.  EXIT.                                                
      ******************************************************************        
      *                                                                *        
      * INITIALISATION DU TRAITEMENT                                   *        
      *                                                                *        
      ******************************************************************        
       MODULE-ENTREE              SECTION.                                      
           DISPLAY 'BVC003: EDITION DES MAGASINS EN ECART    '.                 
           MOVE 'BVC003' TO ABEND-PROG.                                         
      ******************************************************************        
      * INITIALISATION DES COMPTEURS                                   *        
      ******************************************************************        
           MOVE 0 TO CPT-LIGNES                                                 
                     CPT-FVC004.                                                
           MOVE 0 TO NB-PAGES.                                                  
      ******************************************************************        
      * OUVERTURE DES FICHIERS                                         *        
      ******************************************************************        
           OPEN INPUT  FDATE                                                    
                       FVC004                                                   
                       FVC005                                                   
                       FPER                                                     
                OUTPUT EDITION.                                                 
      ******************************************************************        
      * LECTURE DE LA PERIODE                                          *        
      ******************************************************************        
           READ FPER  INTO W-FPER     AT END                                    
                MOVE 'Fichier FPER vide' TO ABEND-MESS                          
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           CLOSE FPER.                                                          
           DISPLAY ' PERIODE D''ANALYSE :'  W-FPER.                             
           IF W-FPER NOT = 'JOUR'                                               
           AND W-FPER NOT = 'MOIS'                                              
             MOVE 'FPER CONTIENT JOUR OU MOIS' TO ABEND-MESS                    
             PERFORM ABEND-PROGRAMME                                            
           END-IF.                                                              
      ******************************************************************        
      * LECTURE DE LA DATE                                             *        
      ******************************************************************        
           READ FDATE INTO GFJJMMSSAA AT END                                    
                MOVE 'Fichier FDATE vide' TO ABEND-MESS                         
                PERFORM ABEND-PROGRAMME                                         
           END-READ.                                                            
           CLOSE FDATE.                                                         
           MOVE '1'            TO GFDATA.                                       
           CALL BETDATC      USING WORK-BETDATC                                 
           IF GFVDAT NOT = '1'                                                  
              STRING 'BETDATC : ' GF-MESS-ERR                                   
              DELIMITED BY SIZE INTO ABEND-MESS                                 
              PERFORM ABEND-PROGRAMME                                           
           END-IF.                                                              
           IF W-FPER = 'JOUR'                                                   
             MOVE GFSIECLE TO L1-SIECLE                                         
             MOVE GFANNEE  TO L1-ANNEE                                          
             MOVE GFMOIS   TO L1-MOIS                                           
             MOVE GFJOUR   TO L1-JOUR                                           
           ELSE                                                                 
             MOVE GFSIECLE TO L2-SIECLE1  L2-SIECLE2                            
             MOVE GFANNEE  TO L2-ANNEE1   L2-ANNEE2                             
             MOVE GFMOIS   TO L2-MOIS1    L2-MOIS2                              
             MOVE '01'     TO L2-JOUR1                                          
             MOVE GFJOUR   TO L2-JOUR2                                          
           END-IF.                                                              
      ******************************************************************        
      * LECTURE DU FICHIER DES TOTAUX                                  *        
      ******************************************************************        
PR1        SET OK-FVC005 TO TRUE.                                               
           READ FVC005 INTO ENR-FVC005                                          
           AT END                                                               
PR1          DISPLAY 'Fichier des totaux vide'                                  
PR1   *      MOVE 'Fichier des totaux vide' TO ABEND-MESS                       
PR1   *      PERFORM ABEND-PROGRAMME                                            
PR1          SET FIN-FVC005 TO TRUE                                             
           END-READ.                                                            
           CLOSE FVC005.                                                        
      ******************************************************************        
      *                                                                *        
      * MODULE DE TRAITEMENT: EDITION DES ECARTS                       *        
      *                                                                *        
      ******************************************************************        
       MODULE-TRAITEMENT SECTION.                                               
           DISPLAY '***  DEBUT DE PHASE TRAITEMENT  ***'.                       
           PERFORM LECTURE-FVC004.                                              
           IF FIN-FVC004                                                        
             PERFORM EDITION-MESSAGE                                            
           ELSE                                                                 
             MOVE ' ' TO CSAUT OF LIGNE0                                        
             PERFORM EDITION-ENTETE                                             
             MOVE '1' TO CSAUT OF LIGNE0                                        
             PERFORM UNTIL FIN-FVC004                                           
               IF CPT-LIGNES > MAX-LIGNES                                       
                 PERFORM EDITION-LIGNE2                                         
                 PERFORM EDITION-ENTETE                                         
                 PERFORM EDITION-DETAIL                                         
               ELSE                                                             
                 PERFORM EDITION-DETAIL                                         
               END-IF                                                           
               PERFORM LECTURE-FVC004                                           
               IF FIN-FVC004                                                    
                 PERFORM EDITION-LIGNE2                                         
                 PERFORM EDITION-TOTAUX                                         
                 PERFORM EDITION-LIGNE2                                         
               END-IF                                                           
             END-PERFORM                                                        
           END-IF.                                                              
      ******************************************************************        
      * LECTURE DE FVC004                                              *        
      ******************************************************************        
       LECTURE-FVC004 SECTION.                                                  
           READ FVC004 INTO ENR-FVC004                                          
           AT END                                                               
             SET FIN-FVC004 TO TRUE                                             
           NOT AT END                                                           
             ADD 1 TO CPT-FVC004                                                
           END-READ.                                                            
      ******************************************************************        
      * MESSAGE LORSQUE AUCUN DES MAGASINS N'EST EN ECART              *        
      ******************************************************************        
       EDITION-MESSAGE SECTION.                                                 
           MOVE ' ' TO CSAUT OF LIGNE0.                                         
           MOVE LIGNE0 TO EDITION-ENR.                                          
           PERFORM ECRITURE-EDITION.                                            
           IF W-FPER = 'JOUR'                                                   
             MOVE INTITULE-JOUR TO EDITION-ENR                                  
           ELSE                                                                 
             MOVE INTITULE-MOIS TO EDITION-ENR                                  
           END-IF.                                                              
           PERFORM ECRITURE-EDITION.                                            
           MOVE LIGNE0 TO EDITION-ENR.                                          
           PERFORM ECRITURE-EDITION.                                            
           MOVE SPACE TO EDITION-ENR.                                           
           PERFORM ECRITURE-EDITION.                                            
           MOVE '** Pas de magasin en �cart  **' TO EDITION-ENR.                
           PERFORM ECRITURE-EDITION.                                            
           MOVE 5 TO CPT-LIGNES.                                                
      ******************************************************************        
      * ENTETE DE PAGE                                                 *        
      ******************************************************************        
       EDITION-ENTETE SECTION.                                                  
           ADD  1 TO NB-PAGES.                                                  
           MOVE LIGNE0 TO EDITION-ENR.                                          
           PERFORM ECRITURE-EDITION.                                            
           MOVE NB-PAGES TO NO-PAGE OF INTITULE-JOUR                            
                            NO-PAGE OF INTITULE-MOIS.                           
           IF W-FPER = 'JOUR'                                                   
             MOVE INTITULE-JOUR TO EDITION-ENR                                  
           ELSE                                                                 
             MOVE INTITULE-MOIS TO EDITION-ENR                                  
           END-IF.                                                              
           PERFORM ECRITURE-EDITION.                                            
           MOVE LIGNE1 TO EDITION-ENR.                                          
           PERFORM ECRITURE-EDITION.                                            
           MOVE LIGNE3 TO EDITION-ENR.                                          
           PERFORM ECRITURE-EDITION.                                            
           MOVE LIGNE4 TO EDITION-ENR.                                          
           PERFORM ECRITURE-EDITION.                                            
           MOVE LIGNE5 TO EDITION-ENR.                                          
           PERFORM ECRITURE-EDITION.                                            
           PERFORM EDITION-LIGNE2.                                              
           MOVE 7 TO CPT-LIGNES.                                                
      ******************************************************************        
      * DETAIL DES DONNEES                                                      
      ******************************************************************        
       EDITION-DETAIL SECTION.                                                  
           MOVE FVC004-LIEU       TO  DETAIL-MAG.                               
           MOVE FVC004-TOTVOLCIAL TO  DETAIL-VOLCO.                             
           MOVE FVC004-TOTVOLLIVR TO  DETAIL-VOL15.                             
           MOVE FVC004-LIVRSTAT   TO  DETAIL-VOL40.                             
           MOVE FVC004-ECARTLI    TO  DETAIL-ECARTLI.                           
           MOVE FVC004-DIFFVOL    TO  DETAIL-DIFVOL.                            
           MOVE FVC004-CASTAT     TO  DETAIL-STAT.                              
           MOVE FVC004-CACPTA     TO  DETAIL-CPTA.                              
           MOVE FVC004-DIFFCA     TO  DETAIL-DIFCA.                             
PR2        MOVE FVC004-CACPTARECY TO  DETAIL-CPTA-RECY                          
PR2        MOVE FVC004-DIFFCATOT  TO  DETAIL-DIFCA-TOT                          
           MOVE LIGDETAIL TO EDITION-ENR.                                       
           PERFORM ECRITURE-EDITION.                                            
      ******************************************************************        
      * EDITION DE LA DERNIERE LIGNE DU TABLEAU                                 
      ******************************************************************        
       EDITION-LIGNE2 SECTION.                                                  
           MOVE LIGNE2 TO EDITION-ENR.                                          
           PERFORM ECRITURE-EDITION.                                            
      ******************************************************************        
      * EDITION DE LA LIGNE DES CUMULS                                          
      ******************************************************************        
       EDITION-TOTAUX SECTION.                                                  
           MOVE SPACE             TO  DETAIL-MAG.                               
           MOVE FVC005-TOTVOLCIAL TO  DETAIL-VOLCO.                             
           MOVE FVC005-TOTVOLLIVR TO  DETAIL-VOL15.                             
           MOVE FVC005-LIVRSTAT   TO  DETAIL-VOL40.                             
           MOVE FVC005-ECARTLI    TO  DETAIL-ECARTLI.                           
           MOVE FVC005-DIFFVOL    TO  DETAIL-DIFVOL.                            
           MOVE FVC005-CASTAT     TO  DETAIL-STAT.                              
           MOVE FVC005-CACPTA     TO  DETAIL-CPTA.                              
           MOVE FVC005-DIFFCA     TO  DETAIL-DIFCA.                             
PR2        MOVE FVC005-CACPTARECY TO  DETAIL-CPTA-RECY                          
PR2        MOVE FVC005-DIFFCATOT  TO  DETAIL-DIFCA-TOT                          
           MOVE LIGDETAIL TO EDITION-ENR.                                       
           PERFORM ECRITURE-EDITION.                                            
      ******************************************************************        
      * ECRITURE DE EDITION                                                     
      ******************************************************************        
       ECRITURE-EDITION SECTION.                                                
           WRITE ENR-EDITION FROM EDITION-ENR.                                  
           ADD 1 TO CPT-LIGNES.                                                 
      ******************************************************************        
      * MODULE DE FIN                                                  *        
      ******************************************************************        
       MODULE-SORTIE        SECTION.                                            
      * MESSAGES DE FIN                                                         
           MOVE CPT-FVC004 TO PIC-Z7.                                           
           DISPLAY '***  NOMBRE DE LIGNES LUES DANS FVC004: '                   
                   PIC-Z7 '  (CAD NB MAGASINS EN ECART)'.                       
           DISPLAY '***  NOMBRE DE PAGES DE L''ETAT        :      '             
                   NB-PAGES .                                                   
           DISPLAY '***  EXECUTION TERMINEE NORMALEMENT  ***'.                  
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-MODULE-SORTIE. EXIT.                                                 
      ******************************************************************        
      *                                                                *        
      * FIN ANORMALE                                                   *        
      *                                                                *        
      ******************************************************************        
      * ARRET DU PROGRAMME                                                      
       ABEND-PROGRAMME SECTION.                                                 
           DISPLAY ABEND-MESS.                                                  
           CALL 'ABEND'  USING  ABEND-PROG  ABEND-MESS.                         
           GOBACK.                                                              
