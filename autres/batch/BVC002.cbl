      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. BVC002.                                                      
       AUTHOR. AL.                                                              
      ******************************************************************        
      *  Programme  : BVC002                                           *        
      *  Cr�ation   : 11/2000                                          *        
      *  Fonction   : Rapprochement des fichiers issus des tables      *        
      *               RTHV15, RTGS40, RTFT12, RTFT21, RTFT34 et RTFT43 *        
      *               pour �dition des magasins pr�sentant des �carts  *        
      *                                                                *        
      *  P�riodicit�: quotidienne                                      *        
      *                                                                *        
      * INPUT:                                                         *        
      * Fichiers FVC001, FVC002, FVC003 tri�s et cumul�s.              *        
      *                                                                *        
      * OUTPUT:                                                        *        
      * FVC004  Fichier contenant les magasins pr�sentant des �carts   *        
      *           sur volume et / ou chiffre d'affaires                *        
      *                                                                *        
      ******************************************************************        
      * MODIF PR 09/01/2001 : prise en compte des pi�ces recycl�es     *        
      *  (SCAN PR0101)        et tests de numericit� avant calculs     *        
      ******************************************************************        
      * MODIF PR 27/02/2001 : refonte de l'assortiment des 3 fichiers  *        
      *  (SCAN PR0201)        en entr�e                                *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                                   
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FVC001 ASSIGN TO FVC001.                                     
      *--                                                                       
            SELECT FVC001 ASSIGN TO FVC001                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FVC002 ASSIGN TO FVC002.                                     
      *--                                                                       
            SELECT FVC002 ASSIGN TO FVC002                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FVC003 ASSIGN TO FVC003.                                     
      *--                                                                       
            SELECT FVC003 ASSIGN TO FVC003                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FVC004 ASSIGN TO FVC004.                                     
      *--                                                                       
            SELECT FVC004 ASSIGN TO FVC004                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *    FVC001                                                               
      ***********                                                               
       FD  FVC001 RECORDING F BLOCK 0 RECORDS                                   
           LABEL RECORD STANDARD.                                               
       01  MW-FILLER PIC X(20).                                                 
      *    FVC002                                                               
      ***********                                                               
       FD  FVC002 RECORDING F BLOCK 0 RECORDS                                   
           LABEL RECORD STANDARD.                                               
       01  MW-FILLER PIC X(41).                                                 
      *    FVC003                                                               
      ***********                                                               
       FD  FVC003 RECORDING F BLOCK 0 RECORDS                                   
           LABEL RECORD STANDARD.                                               
      *MW DAR-2                                                                 
       01  MW-FILLER PIC X(46).                                                 
      *    FVC004 : fichier contenant les �carts                                
      ******************************************                                
       FD  FVC004 RECORDING F BLOCK 0 RECORDS                                   
           LABEL RECORD STANDARD.                                               
PR0101 01  FVC004-ENR PIC X(71).                                                
       WORKING-STORAGE SECTION.                                                 
      *    FVC001                                                               
      ***********                                                               
       01  CPT-FVC001 PIC S9(7) COMP-3 VALUE ZERO.                              
       01  FILLER PIC X  VALUE SPACE.                                           
PR0201     88 FVC001-OK  VALUE 'O'.                                             
           88 FIN-FVC001 VALUE 'F'.                                             
       01  ENR-FVC001.                                                          
     1     05 FVC001-SOC         PIC X(03).                                     
     4     05 FVC001-MAG         PIC X(03).                                     
     7     05 FVC001-DVENTE      PIC X(08).                                     
    15     05 FVC001-TOTVOLCIAL  PIC S9(5) COMP-3.                              
    18     05 FVC001-TOTVOLLIVR  PIC S9(5) COMP-3.                              
      *    FVC002                                                               
      ***********                                                               
       01  CPT-FVC002 PIC S9(7) COMP-3 VALUE ZERO.                              
       01  FILLER PIC X  VALUE SPACE.                                           
PR0201     88 FVC002-OK  VALUE 'O'.                                             
           88 FIN-FVC002 VALUE 'F'.                                             
       01  ENR-FVC002.                                                          
     1     05 FVC002-NSOC        PIC X(03).                                     
     4     05 FVC002-MAG         PIC X(03).                                     
     7     05 FVC002-CMODDEL     PIC X(03).                                     
    10     05 FVC002-DMVT        PIC X(08).                                     
    18     05 FVC002-TOTVOLEMR   PIC S9(7) COMP-3.                              
    22     05 FVC002-TOTVOLRM    PIC S9(7) COMP-3.                              
    26     05 FVC002-TOTCAEMR    PIC S9(13)V9(2) COMP-3.                        
    33     05 FVC002-TOTCARM     PIC S9(13)V9(2) COMP-3.                        
      *    FVC003                                                               
      ***********                                                               
       01  CPT-FVC003               PIC S9(7) COMP-3 VALUE ZERO.                
       01  FILLER        PIC X VALUE SPACE.                                     
PR0201     88 FVC003-OK  VALUE 'O'.                                             
           88 FIN-FVC003       VALUE 'F'.                                       
       01  ENR-FVC003.                                                          
     1     05 FVC003-NSOC           PIC X(03).                                  
     3     05 FVC003-SECTION .                                                  
              10 FVC003-FILLER      PIC X(03).                                  
              10 FVC003-MAG         PIC X(03).                                  
    10     05 FVC003-DSAISIE        PIC X(08).                                  
    18     05 FVC003-WSENS          PIC X(01).                                  
    19     05 FVC003-TOTDEBIT       PIC S9(11)V9(2) COMP-3.                     
    26     05 FVC003-TOTCREDIT      PIC S9(11)V9(2) COMP-3.                     
PR  33     05 FVC003-TOTDEBIT-RECY  PIC S9(11)V9(2) COMP-3.                     
PR  40     05 FVC003-TOTCREDIT-RECY PIC S9(11)V9(2) COMP-3.                     
      *    FVC004                                                               
      ***********                                                               
       01  CPT-FVC004 PIC S9(7) COMP-3 VALUE ZERO.                              
       01  ENR-FVC004.                                                          
     1     05 FVC004-NSOC            PIC X(03).                                 
     4     05 FVC004-LIEU            PIC X(03).                                 
     7     05 FVC004-TOTVOLCIAL      PIC S9(9) COMP-3.                          
    12     05 FVC004-TOTVOLLIVR      PIC S9(9) COMP-3.                          
    17     05 FVC004-LIVRSTAT        PIC S9(9) COMP-3.                          
    22     05 FVC004-ECARTLI         PIC S9(9) COMP-3.                          
    27     05 FVC004-DIFFVOL         PIC S9(9) COMP-3.                          
    32     05 FVC004-CASTAT          PIC S9(13)V9(2) COMP-3.                    
    40     05 FVC004-CACPTA          PIC S9(13)V9(2) COMP-3.                    
    48     05 FVC004-DIFFCA          PIC S9(13)V9(2) COMP-3.                    
PR  56     05 FVC004-CACPTARECY      PIC S9(13)V9(2) COMP-3.                    
PR  64     05 FVC004-DIFFCATOT       PIC S9(13)V9(2) COMP-3.                    
      *    ABEND                                                                
      **********                                                                
           COPY  ABENDCOP.                                                      
      * Autres zones                                                            
      ******************************************************************        
       01  PIC-Z7      PIC ZZZZZZ9.                                             
PR0201 01  W-CLE-REF   PIC X(03).                                               
PR0201 01  W-FVC001-MAG-RES PIC X(03).                                          
PR0201 01  W-FVC002-MAG-RES PIC X(03).                                          
PR0201 01  W-FVC003-MAG-RES PIC X(03).                                          
PR0201 01  MEMO-ASSORT PIC 9.                                                   
       01  FILLER      PIC X VALUE SPACE.                                       
           88 ECART     VALUE 'F'.                                              
           88 PAS-ECART VALUE 'V'.                                              
       PROCEDURE DIVISION.                                                      
      ******************************************************************        
      *                                                                *        
      * MODULE PRINCIPAL                                               *        
      *                                                                *        
      ******************************************************************        
       MODULE-BVC002              SECTION.                                      
           PERFORM MODULE-ENTREE.                                               
           PERFORM MODULE-TRAITEMENT.                                           
           PERFORM MODULE-SORTIE.                                               
       FIN-MODULE-BVC002.                                                       
           EXIT.                                                                
      ******************************************************************        
      *                                                                *        
      * INITIALISATION DU TRAITEMENT                                   *        
      *                                                                *        
      ******************************************************************        
       MODULE-ENTREE              SECTION.                                      
           DISPLAY 'BVC002: Rapprochement des fichiers issus des ' .            
           DISPLAY '        tables RTHV15, RTGS40, RTFT12, RTFT21, RTFT3        
      -    '4 et RTFT43'.                                                       
           DISPLAY '        pour �dition des magasins en �cart     '.           
           MOVE 'BVC002' TO ABEND-PROG.                                         
      ******************************************************************        
      * OUVERTURE DES FICHIERS                                         *        
      ******************************************************************        
           OPEN INPUT  FVC001                                                   
                       FVC002                                                   
                       FVC003                                                   
                OUTPUT FVC004 .                                                 
      * INITIALISATIONS                                                         
           MOVE 9 TO MEMO-ASSORT.                                               
           MOVE LOW-VALUE TO W-FVC001-MAG-RES.                                  
           MOVE LOW-VALUE TO W-FVC002-MAG-RES.                                  
           MOVE LOW-VALUE TO W-FVC003-MAG-RES.                                  
      ******************************************************************        
      *                                                                *        
      * MODULE DE TRAITEMENT: RAPPROCHEMENT DES FICHIERS ISSUS DES     *        
      * EXTRACTIONS DES TABLES RTHV15, RTGS40, RTFT12 ET RTFT21        *        
      *                                                                *        
      ******************************************************************        
       MODULE-TRAITEMENT SECTION.                                               
           DISPLAY '***  DEBUT DE PHASE TRAITEMENT  ***'.                       
           PERFORM LECTURE-FVC001.                                              
           PERFORM LECTURE-FVC002.                                              
           PERFORM LECTURE-FVC003.                                              
           MOVE FVC001-SOC TO FVC004-NSOC.                                      
PR0201     PERFORM UNTIL MEMO-ASSORT = 0                                        
  !!         PERFORM ASSORTIMENT                                                
  !!         EVALUATE MEMO-ASSORT                                               
  !!           WHEN 0                                                           
  !!             CONTINUE                                                       
  !!           WHEN 1                                                           
  !!             PERFORM MAG-ABSENT-DE-FVC002-ET-FVC003                         
  !!             PERFORM ECRITURE-FVC004                                        
  !!             PERFORM LECTURE-FVC001                                         
  !!           WHEN 2                                                           
  !!             PERFORM MAG-ABSENT-DE-FVC001-ET-FVC003                         
  !!             PERFORM ECRITURE-FVC004                                        
  !!             PERFORM LECTURE-FVC002                                         
  !!           WHEN 3                                                           
  !!             PERFORM MAG-ABSENT-DE-FVC003                                   
  !!             PERFORM ECRITURE-FVC004                                        
  !!             PERFORM LECTURE-FVC001                                         
  !!             PERFORM LECTURE-FVC002                                         
  !!           WHEN 4                                                           
  !!             PERFORM MAG-ABSENT-DE-FVC001-ET-FVC002                         
  !!             PERFORM ECRITURE-FVC004                                        
  !!             PERFORM LECTURE-FVC003                                         
  !!           WHEN 5                                                           
  !!             PERFORM MAG-ABSENT-DE-FVC002                                   
  !!             PERFORM ECRITURE-FVC004                                        
  !!             PERFORM LECTURE-FVC001                                         
  !!             PERFORM LECTURE-FVC003                                         
  !!           WHEN 6                                                           
  !!             PERFORM MAG-ABSENT-DE-FVC001                                   
  !!             PERFORM ECRITURE-FVC004                                        
  !!             PERFORM LECTURE-FVC002                                         
  !!             PERFORM LECTURE-FVC003                                         
  !!           WHEN 7                                                           
  !!             PERFORM CONTROLE-DES-DONNEES                                   
  !!             PERFORM TRAITEMENT-ECART                                       
  !!             PERFORM ECRITURE-FVC004                                        
  !!             PERFORM LECTURE-FVC001                                         
  !!             PERFORM LECTURE-FVC002                                         
  !!             PERFORM LECTURE-FVC003                                         
  !!         END-EVALUATE                                                       
PR0201     END-PERFORM.                                                         
PR0201******************************************************************        
  !!  * MODULE D'ASSORTIMENT DE FEXFPC ET FEXHISTO                     *        
  !!  ******************************************************************        
  !!   ASSORTIMENT SECTION.                                                     
  !!       MOVE HIGH-VALUE TO W-CLE-REF.                                        
  !!       MOVE 0 TO MEMO-ASSORT.                                               
  !!                                                                            
  !!       IF FVC001-OK                                                         
  !!         MOVE FVC001-MAG TO W-CLE-REF                                       
  !!         MOVE 1 TO MEMO-ASSORT                                              
  !!       END-IF.                                                              
  !!                                                                            
  !!       IF FVC002-OK                                                         
  !!         EVALUATE TRUE                                                      
  !!           WHEN FVC002-MAG > W-CLE-REF                                      
  !!             CONTINUE                                                       
  !!           WHEN FVC002-MAG = W-CLE-REF                                      
  !!             ADD 2 TO MEMO-ASSORT                                           
  !!           WHEN FVC002-MAG < W-CLE-REF                                      
  !!             MOVE 2 TO MEMO-ASSORT                                          
  !!             MOVE FVC002-MAG TO W-CLE-REF                                   
  !!         END-EVALUATE                                                       
  !!       END-IF.                                                              
  !!                                                                            
  !!       IF FVC003-OK                                                         
  !!         EVALUATE TRUE                                                      
  !!           WHEN FVC003-MAG > W-CLE-REF                                      
  !!             CONTINUE                                                       
  !!           WHEN FVC003-MAG = W-CLE-REF                                      
  !!             ADD 4 TO MEMO-ASSORT                                           
  !!           WHEN FVC003-MAG < W-CLE-REF                                      
  !!             MOVE 4 TO MEMO-ASSORT                                          
  !!             MOVE FVC003-MAG TO W-CLE-REF                                   
  !!         END-EVALUATE                                                       
  !!       END-IF.                                                              
  !!   ASSORTIMENT-FIN.                                                         
PR0201     EXIT.                                                                
      ******************************************************************        
      * Lecture des fichiers en entr�e                                          
      ******************************************************************        
       LECTURE-FVC001 SECTION.                                                  
           SET FVC001-OK TO TRUE                                                
           READ FVC001 INTO ENR-FVC001                                          
           AT END                                                               
             SET FIN-FVC001 TO TRUE                                             
PR0201*      MOVE HIGH-VALUE TO FVC001-MAG                                      
             IF CPT-FVC001 < 1                                                  
               DISPLAY '*** POUR INFO, LE FICHIER FVC001 EST VIDE'              
               MOVE 0 TO FVC001-TOTVOLCIAL                                      
                         FVC001-TOTVOLLIVR                                      
      *        INITIALIZE ENR-FVC001                                            
             END-IF                                                             
           NOT AT END                                                           
             ADD 1 TO CPT-FVC001                                                
PR0201       IF FVC001-MAG < W-FVC001-MAG-RES                                   
PR0201         DISPLAY '!!! TRI DE FVC001 NON COHERENT'                         
PR0201         PERFORM ABEND-PROGRAMME                                          
PR0201       ELSE                                                               
PR0201         MOVE FVC001-MAG TO W-FVC001-MAG-RES                              
PR0201       END-IF                                                             
           END-READ.                                                            
       LECTURE-FVC002 SECTION.                                                  
           SET FVC002-OK TO TRUE                                                
           READ FVC002 INTO ENR-FVC002                                          
           AT END                                                               
             SET FIN-FVC002 TO TRUE                                             
PR0201*      MOVE HIGH-VALUE TO FVC002-MAG                                      
             IF CPT-FVC002 < 1                                                  
               DISPLAY '*** POUR INFO, LE FICHIER FVC002 EST VIDE'              
               MOVE 0 TO FVC002-TOTVOLEMR                                       
                         FVC002-TOTVOLRM                                        
                         FVC002-TOTCAEMR                                        
                         FVC002-TOTCARM                                         
      *        INITIALIZE ENR-FVC002                                            
             END-IF                                                             
           NOT AT END                                                           
             ADD 1 TO CPT-FVC002                                                
PR0201       IF FVC002-MAG < W-FVC002-MAG-RES                                   
PR0201         DISPLAY '!!! TRI DE FVC002 NON COHERENT'                         
PR0201         PERFORM ABEND-PROGRAMME                                          
PR0201       ELSE                                                               
PR0201         MOVE FVC002-MAG TO W-FVC002-MAG-RES                              
PR0201       END-IF                                                             
           END-READ.                                                            
       LECTURE-FVC003 SECTION.                                                  
           SET FVC003-OK TO TRUE                                                
           READ FVC003 INTO ENR-FVC003                                          
           AT END                                                               
             SET FIN-FVC003 TO TRUE                                             
PR0201*      MOVE HIGH-VALUE TO FVC003-MAG                                      
             IF CPT-FVC003 < 1                                                  
               DISPLAY '*** POUR INFO, LE FICHIER FVC003 EST VIDE'              
               MOVE 0 TO FVC003-TOTDEBIT                                        
                         FVC003-TOTCREDIT                                       
                         FVC003-TOTDEBIT-RECY                                   
                         FVC003-TOTCREDIT-RECY                                  
      *        INITIALIZE ENR-FVC003                                            
             END-IF                                                             
           NOT AT END                                                           
             ADD 1 TO CPT-FVC003                                                
PR0201       IF FVC003-MAG < W-FVC003-MAG-RES                                   
PR0201         DISPLAY '!!! TRI DE FVC003 NON COHERENT'                         
PR0201         PERFORM ABEND-PROGRAMME                                          
PR0201       ELSE                                                               
PR0201         MOVE FVC003-MAG TO W-FVC003-MAG-RES                              
PR0201       END-IF                                                             
           END-READ.                                                            
      ******************************************************************        
      * Contr�le des donn�es                                                    
      ******************************************************************        
       CONTROLE-DES-DONNEES SECTION.                                            
           SET PAS-ECART TO TRUE.                                               
           PERFORM CALCUL-LIVRSTAT.                                             
           PERFORM CALCUL-ECARTLI.                                              
           PERFORM CALCUL-CASTAT.                                               
           PERFORM CALCUL-CACPTA.                                               
           PERFORM CALCUL-DIFFVOL.                                              
           PERFORM CALCUL-DIFFCA.                                               
PR0101     PERFORM CALCUL-CACPTARECY.                                           
PR0101     PERFORM CALCUL-DIFFCATOT.                                            
      ******************************************************************        
      * Calcul du volume stat livr�, des chiffres d'affaires stat et            
      * compta                                                                  
      ******************************************************************        
       CALCUL-LIVRSTAT SECTION.                                                 
           EVALUATE TRUE                                                        
             WHEN FVC002-TOTVOLEMR  NUMERIC AND                                 
                  FVC002-TOTVOLRM   NUMERIC                                     
               COMPUTE FVC004-LIVRSTAT     = FVC002-TOTVOLEMR                   
                                           + FVC002-TOTVOLRM                    
             WHEN FVC002-TOTVOLEMR  NOT NUMERIC                                 
               DISPLAY '!!! FVC002-TOTVOLEMR  PAS NUMERIQUE:'                   
                       FVC002-TOTVOLEMR                                         
               PERFORM ABEND-PROGRAMME                                          
             WHEN FVC002-TOTVOLRM   NOT NUMERIC                                 
               DISPLAY '!!! FVC002-TOTVOLRM   PAS NUMERIQUE:'                   
                       FVC002-TOTVOLRM                                          
               PERFORM ABEND-PROGRAMME                                          
           END-EVALUATE.                                                        
       CALCUL-ECARTLI SECTION.                                                  
           EVALUATE TRUE                                                        
             WHEN FVC004-LIVRSTAT   NUMERIC AND                                 
                  FVC001-TOTVOLLIVR NUMERIC                                     
               COMPUTE FVC004-ECARTLI      = FVC004-LIVRSTAT                    
                                           - FVC001-TOTVOLLIVR                  
             WHEN FVC004-LIVRSTAT   NOT NUMERIC                                 
               DISPLAY '!!! FVC004-LIVRSTAT   PAS NUMERIQUE:'                   
                       FVC004-LIVRSTAT                                          
               PERFORM ABEND-PROGRAMME                                          
             WHEN FVC001-TOTVOLLIVR NOT NUMERIC                                 
               DISPLAY '!!! FVC001-TOTVOLLIVR PAS NUMERIQUE:'                   
                       FVC001-TOTVOLLIVR                                        
               PERFORM ABEND-PROGRAMME                                          
           END-EVALUATE.                                                        
       CALCUL-CASTAT  SECTION.                                                  
           EVALUATE TRUE                                                        
             WHEN FVC002-TOTCAEMR   NUMERIC AND                                 
                  FVC002-TOTCARM    NUMERIC                                     
               COMPUTE FVC004-CASTAT       = FVC002-TOTCAEMR                    
                                           + FVC002-TOTCARM                     
             WHEN FVC002-TOTCAEMR   NOT NUMERIC                                 
               DISPLAY '!!! FVC002-TOTCAEMR   PAS NUMERIQUE:'                   
                       FVC002-TOTCAEMR                                          
               PERFORM ABEND-PROGRAMME                                          
             WHEN FVC002-TOTCARM    NOT NUMERIC                                 
               DISPLAY '!!! FVC002-TOTCARM    PAS NUMERIQUE:'                   
                       FVC002-TOTCARM                                           
               PERFORM ABEND-PROGRAMME                                          
           END-EVALUATE.                                                        
       CALCUL-CACPTA  SECTION.                                                  
           EVALUATE TRUE                                                        
             WHEN FVC003-TOTDEBIT   NUMERIC AND                                 
                  FVC003-TOTCREDIT  NUMERIC                                     
               COMPUTE FVC004-CACPTA       = FVC003-TOTDEBIT                    
                                           + FVC003-TOTCREDIT                   
             WHEN FVC003-TOTDEBIT   NOT NUMERIC                                 
               DISPLAY '!!! FVC003-TOTDEBIT   PAS NUMERIQUE:'                   
                       FVC003-TOTDEBIT                                          
               PERFORM ABEND-PROGRAMME                                          
             WHEN FVC003-TOTCREDIT  NOT NUMERIC                                 
               DISPLAY '!!! FVC003-TOTCREDIT  PAS NUMERIQUE:'                   
                       FVC003-TOTCREDIT                                         
               PERFORM ABEND-PROGRAMME                                          
           END-EVALUATE.                                                        
       CALCUL-DIFFVOL SECTION.                                                  
           EVALUATE TRUE                                                        
             WHEN FVC001-TOTVOLCIAL NUMERIC AND                                 
                  FVC001-TOTVOLLIVR NUMERIC                                     
               COMPUTE FVC004-DIFFVOL      = FVC001-TOTVOLCIAL                  
                                           - FVC001-TOTVOLLIVR                  
             WHEN FVC001-TOTVOLCIAL NOT NUMERIC                                 
               DISPLAY '!!! FVC001-TOTVOLCIAL PAS NUMERIQUE:'                   
                       FVC001-TOTVOLCIAL                                        
               PERFORM ABEND-PROGRAMME                                          
             WHEN FVC001-TOTVOLLIVR NOT NUMERIC                                 
               DISPLAY '!!! FVC001-TOTVOLLIVR PAS NUMERIQUE:'                   
                       FVC001-TOTVOLLIVR                                        
               PERFORM ABEND-PROGRAMME                                          
           END-EVALUATE.                                                        
       CALCUL-DIFFCA SECTION.                                                   
           EVALUATE TRUE                                                        
             WHEN FVC004-CASTAT     NUMERIC AND                                 
                  FVC004-CACPTA     NUMERIC                                     
               COMPUTE FVC004-DIFFCA       = FVC004-CASTAT                      
                                           + FVC004-CACPTA                      
             WHEN FVC004-CASTAT     NOT NUMERIC                                 
               DISPLAY '!!! FVC004-CASTAT     PAS NUMERIQUE:'                   
                       FVC004-CASTAT                                            
               PERFORM ABEND-PROGRAMME                                          
             WHEN FVC004-CACPTA     NOT NUMERIC                                 
               DISPLAY '!!! FVC004-CACPTA     PAS NUMERIQUE:'                   
                       FVC004-CACPTA                                            
               PERFORM ABEND-PROGRAMME                                          
           END-EVALUATE.                                                        
PR0101 CALCUL-CACPTARECY  SECTION.                                              
PR0101     EVALUATE TRUE                                                        
PR0101       WHEN FVC003-TOTDEBIT-RECY  NUMERIC AND                             
PR0101            FVC003-TOTCREDIT-RECY NUMERIC                                 
PR0101         COMPUTE FVC004-CACPTARECY   = FVC003-TOTDEBIT-RECY               
PR0101                                     + FVC003-TOTCREDIT-RECY              
PR0101                                                                          
PR0101       WHEN FVC003-TOTDEBIT-RECY  NOT NUMERIC                             
PR0101         DISPLAY '!!! FVC003-TOTDEBIT-RECY  PAS NUMERIQUE:'               
PR0101                 FVC003-TOTDEBIT-RECY                                     
PR0101         PERFORM ABEND-PROGRAMME                                          
PR0101                                                                          
PR0101       WHEN FVC003-TOTCREDIT-RECY NOT NUMERIC                             
PR0101         DISPLAY '!!! FVC003-TOTCREDIT-RECY PAS NUMERIQUE:'               
PR0101                 FVC003-TOTCREDIT-RECY                                    
PR0101         PERFORM ABEND-PROGRAMME                                          
PR0101     END-EVALUATE.                                                        
PR0101 CALCUL-DIFFCATOT SECTION.                                                
PR0101     EVALUATE TRUE                                                        
PR0101       WHEN FVC004-DIFFCA     NUMERIC AND                                 
PR0101            FVC004-CACPTARECY NUMERIC                                     
PR0101         COMPUTE FVC004-DIFFCATOT    = FVC004-DIFFCA                      
PR0101                                     + FVC004-CACPTARECY                  
PR0101                                                                          
PR0101       WHEN FVC004-DIFFCA     NOT NUMERIC                                 
PR0101         DISPLAY '!!! FVC004-DIFFCA     PAS NUMERIQUE:'                   
PR0101                 FVC004-DIFFCA                                            
PR0101         PERFORM ABEND-PROGRAMME                                          
PR0101                                                                          
PR0101       WHEN FVC004-CACPTARECY NOT NUMERIC                                 
PR0101         DISPLAY '!!! FVC004-CACPTARECY PAS NUMERIQUE:'                   
PR0101                 FVC004-CACPTARECY                                        
PR0101         PERFORM ABEND-PROGRAMME                                          
PR0101     END-EVALUATE.                                                        
      ******************************************************************        
      * Traitement des �carts sur volumes et/ou chiffres d'affaires             
      ******************************************************************        
       TRAITEMENT-ECART SECTION.                                                
           MOVE FVC001-MAG        TO FVC004-LIEU.                               
           MOVE FVC001-TOTVOLCIAL TO FVC004-TOTVOLCIAL.                         
           MOVE FVC001-TOTVOLLIVR TO FVC004-TOTVOLLIVR.                         
      ******************************************************************        
      * Traitements des magasins absents des fichiers d'extraction              
      ******************************************************************        
      * Magasin absent de la table HV15                                         
       MAG-ABSENT-DE-FVC001 SECTION.                                            
           MOVE FVC002-MAG        TO FVC004-LIEU.                               
           MOVE 0                 TO FVC004-TOTVOLCIAL.                         
           MOVE 0                 TO FVC004-TOTVOLLIVR.                         
           PERFORM CALCUL-LIVRSTAT.                                             
           MOVE 0                 TO FVC004-ECARTLI.                            
           MOVE 0                 TO FVC004-DIFFVOL.                            
           PERFORM CALCUL-CASTAT.                                               
           PERFORM CALCUL-CACPTA.                                               
           PERFORM CALCUL-DIFFCA.                                               
PR0101     PERFORM CALCUL-CACPTARECY.                                           
PR0101     PERFORM CALCUL-DIFFCATOT.                                            
      * Magasin absent de la table GS40                                         
       MAG-ABSENT-DE-FVC002 SECTION.                                            
           MOVE FVC001-MAG        TO FVC004-LIEU.                               
           MOVE FVC001-TOTVOLCIAL TO FVC004-TOTVOLCIAL.                         
           MOVE FVC001-TOTVOLLIVR TO FVC004-TOTVOLLIVR.                         
           MOVE 0                 TO FVC004-LIVRSTAT.                           
           MOVE 0                 TO FVC004-ECARTLI.                            
           MOVE 0                 TO FVC004-DIFFVOL.                            
           MOVE 0                 TO FVC004-CASTAT.                             
           PERFORM CALCUL-CACPTA.                                               
           PERFORM CALCUL-DIFFCA.                                               
PR0101     PERFORM CALCUL-CACPTARECY.                                           
PR0101     PERFORM CALCUL-DIFFCATOT.                                            
      * Magasin absent des tables RTFT12 et RTFT21                              
       MAG-ABSENT-DE-FVC003 SECTION.                                            
           MOVE FVC001-MAG        TO FVC004-LIEU.                               
           MOVE FVC001-TOTVOLCIAL TO FVC004-TOTVOLCIAL.                         
           MOVE FVC001-TOTVOLLIVR TO FVC004-TOTVOLLIVR.                         
           PERFORM CALCUL-DIFFVOL.                                              
           PERFORM CALCUL-LIVRSTAT.                                             
           PERFORM CALCUL-ECARTLI.                                              
           PERFORM CALCUL-CASTAT.                                               
           MOVE 0                 TO FVC004-CACPTA.                             
           MOVE 0                 TO FVC004-CACPTARECY.                         
PR0101     PERFORM CALCUL-DIFFCA.                                               
PR0101     PERFORM CALCUL-DIFFCATOT.                                            
      * Magasin absent des tables statistiques, pr�sent dans les tables         
      * comptables                                                              
       MAG-ABSENT-DE-FVC001-ET-FVC002 SECTION.                                  
           MOVE FVC003-MAG        TO FVC004-LIEU.                               
           MOVE 0                 TO FVC004-TOTVOLCIAL.                         
           MOVE 0                 TO FVC004-TOTVOLLIVR.                         
           MOVE 0                 TO FVC004-LIVRSTAT.                           
           MOVE 0                 TO FVC004-ECARTLI.                            
           MOVE 0                 TO FVC004-DIFFVOL.                            
           MOVE 0                 TO FVC004-CASTAT.                             
           PERFORM CALCUL-CACPTA.                                               
PR0101     PERFORM CALCUL-DIFFCA.                                               
PR0101     PERFORM CALCUL-CACPTARECY.                                           
PR0101     PERFORM CALCUL-DIFFCATOT.                                            
      * Magasin absent de RTHV15 et des tables comptables                       
      * pr�sent dans RTGS40                                                     
       MAG-ABSENT-DE-FVC001-ET-FVC003 SECTION.                                  
           MOVE FVC002-MAG        TO FVC004-LIEU.                               
           MOVE 0                 TO FVC004-TOTVOLCIAL.                         
           MOVE 0                 TO FVC004-TOTVOLLIVR.                         
           PERFORM CALCUL-LIVRSTAT.                                             
           MOVE 0                 TO FVC004-ECARTLI.                            
           MOVE 0                 TO FVC004-DIFFVOL.                            
           PERFORM CALCUL-CASTAT.                                               
           MOVE 0                 TO FVC004-CACPTA.                             
PR0101     PERFORM CALCUL-DIFFCA.                                               
PR0101     MOVE 0                 TO FVC004-CACPTARECY.                         
PR0101     PERFORM CALCUL-DIFFCATOT.                                            
      * Magasin pr�sent dans RTHV15 , absent des autres tables                  
       MAG-ABSENT-DE-FVC002-ET-FVC003 SECTION.                                  
           MOVE FVC001-MAG        TO FVC004-LIEU.                               
           MOVE FVC001-TOTVOLCIAL TO FVC004-TOTVOLCIAL.                         
           MOVE FVC001-TOTVOLLIVR TO FVC004-TOTVOLLIVR.                         
           MOVE 0                 TO FVC004-LIVRSTAT.                           
           MOVE 0                 TO FVC004-ECARTLI.                            
           MOVE 0                 TO FVC004-DIFFVOL.                            
           MOVE 0                 TO FVC004-CASTAT.                             
           MOVE 0                 TO FVC004-CACPTA.                             
           MOVE 0                 TO FVC004-DIFFCA.                             
PR0101     MOVE 0                 TO FVC004-CACPTARECY.                         
PR0101     MOVE 0                 TO FVC004-DIFFCATOT.                          
      ******************************************************************        
      * Ecriture de FVC004                                                      
      ******************************************************************        
       ECRITURE-FVC004 SECTION.                                                 
           WRITE FVC004-ENR FROM ENR-FVC004.                                    
           ADD 1 TO CPT-FVC004.                                                 
      ******************************************************************        
      * Module de fin                                                           
      ******************************************************************        
       MODULE-SORTIE        SECTION.                                            
      * Messages de fin                                                         
           MOVE CPT-FVC001 TO PIC-Z7.                                           
           DISPLAY '***  Enregistrements lus dans FVC001: '                     
                   PIC-Z7 '  ***'                                               
           MOVE CPT-FVC002 TO PIC-Z7.                                           
           DISPLAY '***  Enregistrements lus dans FVC002: '                     
                   PIC-Z7 '  ***'                                               
           MOVE CPT-FVC003 TO PIC-Z7.                                           
           DISPLAY '***  Enregistrements lus dans FVC003: '                     
                   PIC-Z7 '  ***'                                               
           MOVE CPT-FVC004 TO PIC-Z7.                                           
           DISPLAY '***  Nombre de magasins en �cart : '                        
                   PIC-Z7 '  ***'                                               
           DISPLAY '***  EXECUTION TERMINEE NORMALEMENT  ***'.                  
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-MODULE-SORTIE. EXIT.                                                 
      ******************************************************************        
      *                                                                *        
      * FIN ANORMALE                                                   *        
      *                                                                *        
      ******************************************************************        
      * Arr�t du programme                                                      
       ABEND-PROGRAMME SECTION.                                                 
           DISPLAY ABEND-MESS.                                                  
           CALL 'ABEND'  USING  ABEND-PROG  ABEND-MESS.                         
           GOBACK.                                                              
