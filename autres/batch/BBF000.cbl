      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000020
       PROGRAM-ID.    BBF000.                                           00000040
       AUTHOR.        DSARA3-HV.                                        00000050
       DATE-WRITTEN.  16/05/2011.                                       00000060
      *----------------------------------------------------------------*00000080
      * extraction des guichets pour envoi � des applications          *00000090
      * (redbox , sap , b2b ...                                        *        
      *----------------------------------------------------------------*00000080
      *----------------------------------------------------------------*00000080
       ENVIRONMENT DIVISION.                                            00000150
       CONFIGURATION SECTION.                                           00000160
       SPECIAL-NAMES.                                                   00000170
           DECIMAL-POINT IS COMMA.                                      00000180
      *                                                                 00000190
       INPUT-OUTPUT SECTION.                                            00000200
       FILE-CONTROL.                                                    00000210
      *    SELECT FDATE      ASSIGN TO FDATE.                           00000250
      *                                                                 00000190
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT Fparam     ASSIGN TO Fparam.                          00000250
      *--                                                                       
           SELECT Fparam     ASSIGN TO Fparam                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FBF000I    ASSIGN TO FBF000I                          00000250
      *                      FILE STATUS ST-FBF000I.                            
      *--                                                                       
           SELECT FBF000I    ASSIGN TO FBF000I                                  
                             FILE STATUS ST-FBF000I                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FBF000O    ASSIGN TO FBF000O                          00000250
      *                      FILE STATUS ST-FBF000O.                            
      *--                                                                       
           SELECT FBF000O    ASSIGN TO FBF000O                                  
                             FILE STATUS ST-FBF000O                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
       DATA DIVISION.                                                   00000270
       FILE SECTION.                                                    00000290
      *                                                                 00000600
      *FD   FDATE                                                       00000600
      *     RECORDING F                                                 00000600
      *     BLOCK 0 RECORDS                                             00000600
      *     LABEL RECORD STANDARD.                                      00000600
      *01   ENR-FDATE.                                                  00000600
      *     03  FILLER      PIC X(80).                                  00000600
      *                                                                 00000600
       FD   Fparam                                                              
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01   ENR-Fparam.                                                         
            03  FILLER      PIC X(80).                                          
      *                                                                 00000480
       FD  FBF000I                                                      00000440
           RECORDING f                                                  00000450
           BLOCK 0 RECORDS                                              00000460
           LABEL RECORD STANDARD.                                       00000470
       01  FBF000I-ENR.                                                 00000480
           10  FBF000I-ENREG          PICTURE X(350).                   00000480
      *                                                                 00000480
       FD   FBF000O                                                             
            RECORDING F                                                         
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD.                                              
       01   FBF000O-RECORD.                                                     
            03   FILLER                    PIC X(450).                          
      *                                                                         
      *                                                                 00000430
       WORKING-STORAGE SECTION.                                         00000710
      *    STATUT DES OP�RATIONS SUR FICHIER.                                   
       01  ws-date                    PIC X(008).                               
       01  WW-FBF000I                 PIC X(350).                               
       01  WW-FBF000O.                                                          
         10   WW-FBF000O-1            PIC X(350).                               
         10   WW-FBF000O-cerr         PIC X(002).                               
         10   WW-FBF000O-lerr         PIC X(080).                               
         10   filler                  PIC X(018).                               
       01  ws-GFQNT0                  PIC 9(5).                                 
      *    COPIES D'ABEND ET DE CONTROLE DE DATE.                               
           COPY  SYKWZINO.                                                      
           COPY  SYBWERRO.                                                      
           COPY  ABENDCOP.                                              00001860
           COPY  WORKDATC.                                              00001870
      *    COMMUNICATION AVEC DB2.                                              
           COPY SYKWSQ10.                                                       
           COPY SYBWDIV0.                                                       
      ***   COPY RVBFPAR.                                                       
                                                                        00001910
       01  ST-FBF000I                 PIC  X(02).                               
      *{ remove-comma-in-dde 1.5                                                
      *     88  ST-FBF000i-OK                      VALUE '00' , '  '.           
      *--                                                                       
            88  ST-FBF000i-OK                      VALUE '00'   '  '.           
      *}                                                                        
       01  ST-FBF000O                 PIC  X(02).                               
      *{ remove-comma-in-dde 1.5                                                
      *     88  ST-FBF000O-OK                      VALUE '00' , '  '.           
      *--                                                                       
            88  ST-FBF000O-OK                      VALUE '00'   '  '.           
      *}                                                                        
       01  WS-FIN-BFPAR               PIC 9(01)           VALUE 0.              
           88 FIN-BFPAR                                   VALUE 1.              
       01  WS-ecrire                  PIC 9(01)           VALUE 0.              
           88 KO-ECRIRE                                   VALUE 1.              
           88 OK-ECRIRE                                   VALUE 0.              
      *    NOM DES MODULES APPEL�S.                                             
       01  BETDATC                    PIC X(08) VALUE 'BETDATC'.                
      *    R�CUP�RATION DE LA DSYST.                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WS-RET                     PIC S9(4)  COMP     VALUE 0.              
      *--                                                                       
       01  WS-RET                     PIC S9(4) COMP-5     VALUE 0.             
      *}                                                                        
       01  WS-DSYST                   PIC S9(13) COMP-3   VALUE 0.              
      *    VARIABLES DE TRAVAIL.                                                
       01  ZONE-EDIT              PIC X(500).                           00000480
       01  ZONEX                  PIC X(500).                           00000480
       01  I-ZONE                 PIC S9(3)  COMP-3 VALUE 0.                    
       01  I-ENREG-MAX            PIC S9(3)  COMP-3 VALUE 500.                  
       01  I-TEXTE                PIC S9(3)  COMP-3 VALUE 0.                    
       01  WS-NBR-LUS-FBF000I         PIC S9(07) COMP-3   VALUE 0.              
       01  WS-NBR-ECR-FBF000O         PIC S9(07) COMP-3   VALUE 0.              
       01  WS-SSAAMMJJ                PIC X(08)           VALUE SPACES.         
       01  WS-HHMMSSCC                PIC X(08)           VALUE SPACES.         
       01    WS-WHEN-COMPILED.                                                  
             03 WS-WHEN-COMPILED-MM-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-JJ-1      PIC XX.                              
             03 FILLER                     PIC X.                               
             03 WS-WHEN-COMPILED-AA-1      PIC XX.                              
             03 WS-WHEN-COMPILED-HEURE     PIC X(8)    VALUE SPACE.             
      *                                                                         
        01 DATE-COMPILE.                                                        
             05 COMPILE-JJ            PIC XX.                                   
             05 FILLER                PIC X VALUE '/'.                          
             05 COMPILE-MM            PIC XX.                                   
             05 FILLER                PIC XXX VALUE '/20'.                      
             05 COMPILE-AA            PIC XX.                                   
      *                                                                         
       01  WS-STAT-FBF000I            PIC 9(01)           VALUE 0.              
           88 FIN-FBF000I                                 VALUE 1.              
       01  Fparam-ENREG.                                                        
            02  fparam-appli   pic x(05).                                       
            02  FILLER         PIC X(75).                                       
      *                                                                         
       01  FDATE-ENREG.                                                         
            02  FDAT.                                                           
                04  FDATE-SS   PIC X(02).                                       
                04  FDATE-AA   PIC X(02).                                       
                04  FDATE-MM   PIC X(02).                                       
                04  FDATE-JJ   PIC X(02).                                       
            02  FILLER         PIC X(72).                                       
      *                                                                         
       01  FDATE-TRANS.                                                         
                 04  TRANS-SS   PIC X(02).                                      
                 04  TRANS-AA   PIC X(02).                                      
                 04  TRANS-MM   PIC X(02).                                      
                 04  TRANS-JJ   PIC X(02).                                      
      *                                                                         
       01  ws-ctrl-date.                                                        
                 04  bfpar-signe PIC x(01).                                     
                 04  bfpar-delai PIC 9(03).                                     
                 04  bfpar-unite PIC X(01).                                     
       01  ws-ctableg2          pic x(15).                                      
       01  ws-wtableg           pic x(80).                                      
       01  BFPAR-table.                                                         
              10  BFPAR-T occurs 500.                                           
                15 bfpar-ctableg2.                                              
                 20  bfpar-appl PIC x(05).                                      
                 20  bfpar-deb  PIC 9(03).                                      
                 20  bfpar-lg   PIC 9(03).                                      
                 20  bfpar-cerr PIC x(02).                                      
                15 bfpar-wtableg.                                               
                 20  bfpar-comp PIC X(02).                                      
                 20  bfpar-mod  PIC X(02).                                      
                 20  bfpar-val  PIC X(40).                                      
                15 bfpar-ctrl.                                                  
                 20  bfpar-ecrire pIC X(01).                                    
                15 bfpar-lerr     pic x(80).                                    
       01  ws-lerr             PIC x(80).                                       
       01  ws-cerr             PIC x(04).                                       
       01  IND-bfpar           PIC 9(5).                                        
       01  IND-bfpar-max       PIC 9(5) value 500.                              
       01  I                   PIC 9(5).                                        
      *-----------------------------------------------------------------        
      * PROCEDURE DIVISION.                                                     
      *-----------------------------------------------------------------        
       PROCEDURE  DIVISION.                                             00002160
      *                                                                 00001900
       MODULE-BBF000O      SECTION.                                             
      *---------------                                                          
      *    INITIALISATIONS.                                             00001900
           PERFORM  DEBUT .                                             00002180
      *                                                                 00001900
           PERFORM  READ-FBF000I .                                              
      *    display 'READ=' WW-FBF000I                                           
           move 'OK'   to ww-fBF000o-cerr                                       
           move spaces to ww-fBF000o-lerr                                       
      *                                                                         
           PERFORM UNTIL FIN-FBF000I                                            
                 set ok-ecrire to true                                          
                 if WW-FBF000o(262:8) > spaces and ww-fBF000o-lerr = ' '        
                    move 'KO' to ww-fBF000o-cerr                                
                    move'Coordonn�es du guichet plus valides' to                
                    ww-fBF000o-lerr                                             
                 end-if                                                         
                 if WW-FBF000o(1:1) not = '3'                                   
                    set ko-ecrire to TRUE                                       
                 end-if                                                         
                 if wW-FBF000o (2:5) = '10011' and                              
                  WW-FBF000o (7:5) = '00020'  and ww-fBF000o-lerr = ' '         
                    move 'KO' to ww-fBF000o-cerr                                
                    move'Type de RIB non pr�levable' to                         
                    ww-fBF000o-lerr                                             
                 end-if                                                         
                 if ok-ecrire                                                   
                    PERFORM write-FBF000O                                       
                 end-if                                                         
                 PERFORM READ-FBF000I                                           
           END-PERFORM .                                                        
      *                                                                         
           PERFORM  FIN-BBF000O.                                                
      *                                                                         
       FIN-MODULE-BBF000O. EXIT.                                                
              EJECT                                                     00002620
      *                                                                         
      *-----------------------------------------------------------------        
      *-----------------------------------------------------------------        
      * INITIALISATIONS.                                                        
      *-----------------------------------------------------------------        
       DEBUT SECTION.                                                   00002330
      *    MESSAGE DE D�BUT DE PROGRAMME.                                       
           DISPLAY  '+----------------------------------------------+'          
           DISPLAY  '! EXTRACTION DU FICHIER DES GUICHETS           !'  00002400
           DISPLAY  '! BANQUE DE FRANCE                             !'  00002400
           DISPLAY  '+----------------------------------------------+'  00002350
           DISPLAY  '!    CE PROGRAMME DEBUTE NORMALEMENT           !'.         
           MOVE WHEN-COMPILED TO WS-WHEN-COMPILED.                              
           MOVE WS-WHEN-COMPILED-JJ-1 TO COMPILE-JJ.                            
           MOVE WS-WHEN-COMPILED-AA-1 TO COMPILE-AA.                            
           MOVE WS-WHEN-COMPILED-MM-1 TO COMPILE-MM.                            
           DISPLAY  '! VERSION DU 28/04/2011 COMPILEE LE ' DATE-COMPILE.        
      *    R�CUP�RATION DE DATE ET HEURE DE TRAITEMENT.                         
           ACCEPT WS-SSAAMMJJ FROM  DATE YYYYMMDD.                              
           ACCEPT WS-HHMMSSCC FROM TIME                                         
           DISPLAY  'PASSAGE A :' WS-SSAAMMJJ(7:2) '/' WS-SSAAMMJJ(5:2) 00002400
                                 '/' WS-SSAAMMJJ(1:4) .                 00002400
           DISPLAY  'HEURE     :'  WS-HHMMSSCC                          00002400
           DISPLAY  '+----------------------------------------------+'  00002350
      *    OUVERTURE DU FICHIER EN ENTR�E.                                      
           OPEN  OUTPUT FBF000O                                         00002950
           OPEN  INPUT  FBF000I fparam                                  00002950
           display 'STATUT FBF000I=' ST-FBF000i                                 
           IF NOT ST-FBF000i-OK                                         59350001
              DISPLAY 'FICHIER VIDE        ' ST-FBF000i                 59350001
              PERFORM  FIN-BBF000O                                              
           END-IF                                                               
      *                                                                         
           READ   Fparam INTO   Fparam-ENREG   AT END                           
                MOVE  ' FPARAM ! APPLICATION '                                  
                                       TO   ABEND-MESS                          
                MOVE  03                     TO   ABEND-CODE                    
           PERFORM  FIN-ANORMALE.                                               
           display 'FPARAM=' fparam-appli                                       
      *                                                                         
      *    READ   FDATE  INTO   FDATE-ENREG   AT END                            
      *         MOVE  ' FDATE ! DATE DU JOUR : JJMMSSAA '                       
      *                                TO   ABEND-MESS                          
      *         MOVE  01                     TO   ABEND-CODE                    
      *    PERFORM  FIN-ANORMALE.                                               
           DISPLAY 'ACCEPT=' WS-SSAAMMJJ                                        
           move WS-SSAAMMJJ to FDATE-ENREG                                      
           MOVE '1' TO GFDATA.                                                  
           MOVE FDATE-JJ  TO GFJOUR.                                            
           MOVE FDATE-MM  TO GFMOIS.                                            
           MOVE FDATE-SS  TO GFSIECLE.                                          
           MOVE FDATE-AA  TO GFANNEE.                                           
           CALL 'BETDATC' USING WORK-BETDATC.                                   
           IF GFVDAT = 0                                                        
              MOVE 'DATE INCORRECTE  ' TO ABEND-MESS                            
              MOVE  02                     TO   ABEND-CODE                      
              PERFORM  FIN-ANORMALE                                             
           END-IF.                                                              
           move GFQNT0 to ws-GFQNT0.                                            
           INITIALIZE ST-FBF000i ST-FBF000O                                     
                      BFPAR-table.                                              
           move spaces to WW-FBF000o                                            
           STRING 'Enregistrement' ';'                                          
                  'Etablissement'    ';'                                        
                  'Immatriculation_guichet' ';'                                 
                  'Situation'        ';'                                        
                  'Denomination_particuliere' ';'                               
                  'Nom_du_guichet'         ';'                                  
                  'Etablissement_cible' ';'                                     
                  'Guichet_repreneur'  ';'                                      
                  'Geographique_INSEE'  ';'                                     
                  'Reservee_1'  ';'                                             
                  'Reservee_2'      ';'                                         
                  'Reservee_3'   ';'                                            
                  'Comptoir_Banque_de_France' ';'                               
                  'Adresse_l3' ';'                                              
                  'Adresse_l4'  ';'                                             
                  'Adresse_l5'  ';'                                             
                  'Adresse_l6' ';'                                              
                  'BIC_guichet'  ';'                                            
                  'Ouverture_guichet' ';'                                       
                  'Fermeture_guichet' ';'                                       
                  'Fin_diffusion_guichet' ';'                                   
                  'Routage_guichet' ';'                                         
                  'Libelle_domiciliation' ';'                                   
                  'Nature_guichet'    ';'                                       
                  'Reservee_4'             ';'                                  
                  'Reservee_5'       ';'                                        
                  'Inutilisee'   ';'                                            
                  'Ok_ko'              ';'                                      
                  'Libelle_erreur'                                              
                  DELIMITED BY  SIZE INTO WW-FBF000o.                           
      *    WRITE FBF000O-RECORD FROM WW-FBF000o                                 
                                                                        59340001
      *    IF NOT ST-FBF000O-OK                                         59350001
      *       DISPLAY 'PB ECRITURE FBF000O ' ST-FBF000O                 59350001
      *       PERFORM  FIN-ANORMALE                                             
      *    END-IF                                                               
      *    ADD 1 TO WS-NBR-ECR-FBF000O.                                         
      *                                                                 00002950
       FIN-DEBUT. EXIT.                                                 00002620
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * FIN NORMALE DU PROGRAMME.                                               
      *-----------------------------------------------------------------        
       FIN-BBF000O SECTION.                                             00007910
      *    MESSAGE DE FIN DE PROGRAMME.                                         
           DISPLAY  '+------------------------------------------------+'        
           DISPLAY  'NBRE ENREGISTREMENTS LUS     : ' WS-NBR-LUS-FBF000I00002400
                    '.'                                                 00002400
           DISPLAY  'NBRE ENREGISTREMENTS ECRITS  : ' WS-NBR-ECR-FBF000O00002400
                    '.'                                                 00002400
           DISPLAY  '+------------------------------------------------+'        
           DISPLAY  '! FIN NORMALE DE PROGRAMME.                      !'00002400
           DISPLAY  '+------------------------------------------------+'00002350
      *                                                                         
           CLOSE  FBF000O FBF000I fparam.                                       
      *                                                                         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00008210
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-FIN-BBF000O. EXIT.                                           00008230
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * FIN ANORMALE DU PROGRAMME.                                              
      *-----------------------------------------------------------------        
       FIN-ANORMALE     SECTION.                                        00009190
      *                                                                 00009220
           CLOSE  FBF000I FBF000O fparam.                                       
           DISPLAY '***************************************'            00009230
           DISPLAY '*** PROGRAMME TERMINE ANORMALEMENT  ***'            00009240
           DISPLAY '***************************************'            00009250
      *                                                                 00009260
           MOVE  'BBF000O' TO    ABEND-PROG                             00009270
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                00009280
      *                                                                 00009290
       FIN-FIN-ANORMALE. EXIT.                                          00009300
              EJECT                                                     00002620
      *-----------------------------------------------------------------        
      * LECTURE DU FICHIER EN ENTR�E.                                           
      *-----------------------------------------------------------------        
       READ-FBF000I SECTION.                                                    
           initialize ww-FBF000O                                                
           READ FBF000I INTO   WW-FBF000I                                       
                AT END     SET FIN-FBF000I    TO TRUE                           
                NOT AT END ADD 1  TO WS-NBR-LUS-FBF000I                         
           END-read                                                             
           IF NOT ST-FBF000i-OK                                         59350001
              DISPLAY 'STATUT FBF000I=' ST-FBF000i                      59350001
           END-IF                                                               
           INSPECT WW-FBF000I    REPLACING ALL ';' BY SPACE                     
           move WW-FBF000I to WW-FBF000o                                        
           move 'OK'   to ww-fBF000o-cerr                                       
           .                                                                    
       FIN-READ-FBF000I. EXIT.                                          00009300
      *                                                                         
      ******************************************************************        
      *                     G E S T I O N   D B 2                      *        
      ******************************************************************        
      *                                                                         
      *                                                                         
       COPY SYBCERRO.                                                           
      *                                                                 58680001
      *                                                                 59260001
       WRITE-FBF000O                   SECTION.                         59270001
      *----------------------------------------                         59280001
      *                                                                 59290001
           move spaces to zone-edit                                             
           STRING WW-FBF000o        (1:1) ';'                                   
                  WW-FBF000o        (2:5) ';'                                   
                  WW-FBF000o        (7:5) ';'                                   
                  WW-FBF000o        (12:1) ';'                                  
                  WW-FBF000o        (13:40) ';'                                 
                  WW-FBF000o        (53:20) ';'                                 
                  WW-FBF000o        (73:5) ';'                                  
                  WW-FBF000o        (78:5) ';'                                  
                  WW-FBF000o        (83:5) ';'                                  
                  WW-FBF000o        (88:5) ';'                                  
                  WW-FBF000o        (93:5) ';'                                  
                  WW-FBF000o        (98:5) ';'                                  
                  WW-FBF000o        (103:4) ';'                                 
                  WW-FBF000o        (107:32) ';'                                
                  WW-FBF000o        (139:32) ';'                                
                  WW-FBF000o        (171:32) ';'                                
                  WW-FBF000o        (203:32) ';'                                
                  WW-FBF000o        (235:11)  ';'                               
                  WW-FBF000o        (246:8) ';'                                 
                  WW-FBF000o        (254:8) ';'                                 
                  WW-FBF000o        (262:8) ';'                                 
                  WW-FBF000o        (270:5) ';'                                 
                  WW-FBF000o        (275:24) ';'                                
                  WW-FBF000o        (299:1) ';'                                 
                  WW-FBF000o        (300:4) ';'                                 
                  WW-FBF000o        (304:13) ';'                                
                  WW-FBF000o        (317:33) ';'                                
                  WW-FBF000o        (351:2) ';'                                 
                  WW-FBF000o        (353:80)                                    
                  DELIMITED BY  SIZE INTO zone-edit                             
           PERFORM MISE-EN-FORME-ENREG                                          
           MOVE   ZONE-EDIT         TO  Ww-fBF000o                              
           WRITE FBF000O-RECORD FROM WW-FBF000o                                 
                                                                        59340001
           IF NOT ST-FBF000O-OK                                         59350001
              DISPLAY 'PB ECRITURE FBF000O ' ST-FBF000O                 59350001
              PERFORM  FIN-ANORMALE                                             
           END-IF                                                               
           ADD 1 TO WS-NBR-ECR-FBF000O.                                         
      *                                                                 59420001
      *                                                                 59340001
      *-----------------------------------------------------------------        
       MISE-EN-FORME-ENREG  SECTION.                                    00002330
      *                                                                 00002950
           INSPECT ZONE-EDIT   REPLACING ALL LOW-VALUE BY SPACE                 
           PERFORM VARYING I-TEXTE FROM 1 BY 1                                  
             UNTIL ( I-TEXTE > I-ENREG-MAX )                                    
                OR (ZONE-EDIT(I-TEXTE:) = ' ')                                  
              IF ZONE-EDIT(I-TEXTE:2) = '  '                                    
              OR ZONE-EDIT(I-TEXTE:2) = ' ;'                                    
                 COMPUTE I-ZONE = I-TEXTE + 1                                   
                 MOVE ZONE-EDIT(I-ZONE:) TO ZONEX                               
                 MOVE ZONEX TO ZONE-EDIT(I-TEXTE:)                              
                 IF I-TEXTE > 1                                                 
                    COMPUTE I-TEXTE = I-TEXTE - 1                               
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
      *                                                                 00002950
       FIN-MEF-ENREG         . EXIT.                                    00002620
              EJECT                                                     00002620
      *                                                                         
