      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/09/2016 1        
000100 IDENTIFICATION DIVISION.                                         ZABAUDIT
000200*------------------------                                         ZABAUDIT
000300                                                                  ZABAUDIT
000400 PROGRAM-ID. ZABAUDIT.                                            ZABAUDIT
000500*---------------------                                            ZABAUDIT
000600                                                                  ZABAUDIT
000700******************************************************************ZABAUDIT
000800*                                                                *ZABAUDIT
000900*    CLIENT:     DARTY          CODE:      D001                  *ZABAUDIT
001000*    -------                    -----                            *ZABAUDIT
001100******************************************************************ZABAUDIT
001200*                                                                *ZABAUDIT
001300*    PROGRAM:    ZABAUDIT       ALIAS:     NONE                  *ZABAUDIT
001400*    --------                   ------                           *ZABAUDIT
001500*                                                                *ZABAUDIT
001600*    AUTHOR:     BINOVSKY       LANGUAGE:  COBOL                 *ZABAUDIT
001700*    -------                    ---------                        *ZABAUDIT
001800*                                                                *ZABAUDIT
001900*    MODIFIED:   BRIAN ALLGAR   DATE:      01/08/85              *ZABAUDIT
002000*    ---------                  -----                            *ZABAUDIT
002100*                                                                *ZABAUDIT
002200*    FUNCTION:   THIS PROGRAM CREATES A PDS MEMBER FOR ANY       *ZABAUDIT
002300*    ---------   JOBSET WHICH ENDS ABNORMALLY                    *ZABAUDIT
002400*                                                                *ZABAUDIT
002500******************************************************************ZABAUDIT
002600                                                                  ZABAUDIT
002700 DATE-WRITTEN. 10 MAY 1985.                                       ZABAUDIT
002800*-----------------------------                                    ZABAUDIT
002900                                                                  ZABAUDIT
003000 DATE-COMPILED.                                                   ZABAUDIT
003100*---------------------------                                      ZABAUDIT
003200                                                                  ZABAUDIT
003300 AUTHOR. CORTRANS.                                                ZABAUDIT
003400*-----------------                                                ZABAUDIT
003500                                                                  ZABAUDIT
003600******************************************************************ZABAUDIT
003700                                                                  ZABAUDIT
003800 ENVIRONMENT DIVISION.                                            ZABAUDIT
003900                                                                  ZABAUDIT
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                                   
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           COPY MW-COLSEQ.                                                      
      *}                                                                        
004000 INPUT-OUTPUT SECTION.                                            ZABAUDIT
004100*---------------------                                            ZABAUDIT
004200                                                                  ZABAUDIT
004300 FILE-CONTROL.                                                    ZABAUDIT
      *{ Tr-Select-Sysout 1.7                                                   
004400*    SELECT ZABFILE ASSIGN TO DA-S-ZABFILE.                       ZABAUDIT
      *                                                                         
      *--                                                                       
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT ZABFILE ASSIGN TO DA.                                         
      *                                                                         
      *--                                                                       
           SELECT ZABFILE ASSIGN TO DA                                          
              ORGANIZATION LINE SEQUENTIAL.                                     
004500                                                                  ZABAUDIT
      *}                                                                        
      *}                                                                        
004600 DATA DIVISION.                                                   ZABAUDIT
004700*--------------                                                   ZABAUDIT
004800                                                                  ZABAUDIT
004900 FILE SECTION.                                                    ZABAUDIT
005000 FD  ZABFILE                                                      ZABAUDIT
005100     LABEL RECORD STANDARD                                        ZABAUDIT
005200     BLOCK CONTAINS 0 RECORDS.                                    ZABAUDIT
005300                                                                  ZABAUDIT
005400 01  ZABFILE-RECORD              PIC  X(80).                      ZABAUDIT
005500                                                                  ZABAUDIT
005600******************************************************************ZABAUDIT
005700                                                                  ZABAUDIT
005800 WORKING-STORAGE SECTION.                                         ZABAUDIT
005900*------------------------                                         ZABAUDIT
006000                                                                  ZABAUDIT
006100*-----------------------------------------------------------------ZABAUDIT
006200*    GENERAL PURPOSE WORK AREAS: PREFIX IS WS-                    ZABAUDIT
006300*-----------------------------------------------------------------ZABAUDIT
006400                                                                  ZABAUDIT
006500 01  WS-CURRENT-DATE.                                             ZABAUDIT
006600*--------------------                                             ZABAUDIT
006700                                                                  ZABAUDIT
006800     05  WD-MONTH                PIC  X(02).                      ZABAUDIT
006900     05  FILLER                  PIC  X(01).                      ZABAUDIT
007000     05  WD-DAY                  PIC  X(02).                      ZABAUDIT
007100     05  FILLER                  PIC  X(01).                      ZABAUDIT
007200     05  WD-YEAR                 PIC  X(02).                      ZABAUDIT
007300                                                                  ZABAUDIT
007400 01  WS-TIME-NUMERIC             PIC  9(06).                      ZABAUDIT
007500*-------------------                                              ZABAUDIT
007600                                                                  ZABAUDIT
007700 01  WS-TIME-OF-DAY              REDEFINES WS-TIME-NUMERIC.       ZABAUDIT
007800*------------------                                               ZABAUDIT
007900                                                                  ZABAUDIT
008000     05  WT-HOURS                PIC  X(02).                      ZABAUDIT
008100     05  WT-MINUTES              PIC  X(02).                      ZABAUDIT
008200     05  WT-SECONDS              PIC  X(02).                      ZABAUDIT
008300                                                                  ZABAUDIT
008400*-----------------------------------------------------------------ZABAUDIT
008500*    OUTPUT RECORD DESCRIPTIONS: PREFIX IS WF-                    ZABAUDIT
008600*-----------------------------------------------------------------ZABAUDIT
008700                                                                  ZABAUDIT
008800 01  WF-ZABFILE-REC1.                                             ZABAUDIT
008900*--------------------                                             ZABAUDIT
009000                                                                  ZABAUDIT
009100     05  WF-JOBSET-NAME          PIC  X(08).                      ZABAUDIT
009200     05  FILLER                  PIC  X(19) VALUE                 ZABAUDIT
009300         ' - ABNORMAL END ON '.                                   ZABAUDIT
009400     05  WF-DATE-OF-DAY.                                          ZABAUDIT
009500         10  WD-DAY              PIC  X(02).                      ZABAUDIT
009600         10  FILLER              PIC  X(01) VALUE '/'.            ZABAUDIT
009700         10  WD-MONTH            PIC  X(02).                      ZABAUDIT
009800         10  FILLER              PIC  X(01) VALUE '/'.            ZABAUDIT
009900         10  WD-YEAR             PIC  X(02).                      ZABAUDIT
010000     05  FILLER                  PIC  X(03) VALUE '  ('.          ZABAUDIT
010100     05  WF-TIME-OF-DAY.                                          ZABAUDIT
010200         10  WT-HOURS            PIC  X(02).                      ZABAUDIT
010300         10  FILLER              PIC  X(01) VALUE '.'.            ZABAUDIT
010400         10  WT-MINUTES          PIC  X(02).                      ZABAUDIT
010500         10  FILLER              PIC  X(01) VALUE '.'.            ZABAUDIT
010600         10  WT-SECONDS          PIC  X(02).                      ZABAUDIT
010700     05  FILLER                  PIC  X(34) VALUE ')'.            ZABAUDIT
010800                                                                  ZABAUDIT
010900 01  WF-ZABFILE-REC2             PIC X(80)  VALUE                 ZABAUDIT
011000*-------------------                                              ZABAUDIT
011100     'STEP DE PLANTAGE:               PROGRAMME:'.                ZABAUDIT
011200                                                                  ZABAUDIT
011300 01  WF-ZABFILE-REC3             PIC X(80)  VALUE                 ZABAUDIT
011400*-------------------                                              ZABAUDIT
011500     'DESCRIPTION DU PROBLEME:'.                                  ZABAUDIT
011600                                                                  ZABAUDIT
011700 01  WF-ZABFILE-REC4             PIC X(80)  VALUE                 ZABAUDIT
011800*-------------------                                              ZABAUDIT
011900     'TENTATIVE DE RESOLUTION:'.                                  ZABAUDIT
012000                                                                  ZABAUDIT
012100 01  WF-ZABFILE-REC5             PIC X(80)  VALUE                 ZABAUDIT
012200*-------------------                                              ZABAUDIT
012300     'PROBLEME RESOLU: NON         PAR:'.                         ZABAUDIT
012310 01  WF-ZABFILE-REC6.                                             ZABAUDIT
012320*-------------------                                              ZABAUDIT
012400     05  ZCHAINE      PIC X(6) VALUE SPACES.                      ZABAUDIT
012410     05  FIL1         PIC X(1) VALUE SPACES.                      ZABAUDIT
012411     05  WX-DATE-OF-DAY.                                          ZABAUDIT
012416         10  WD-YEAR             PIC  X(02).                      ZABAUDIT
012417         10  WD-MONTH            PIC  X(02).                      ZABAUDIT
012418         10  WD-DAY              PIC  X(02).                      ZABAUDIT
012419     05  FIL2         PIC X(1) VALUE SPACES.                      ZABAUDIT
012420     05  ESTEP        PIC X(5) VALUE 'STEP:'.                     ZABAUDIT
012430     05  NSTEP        PIC X(3) VALUE SPACES.                      ZABAUDIT
012440     05  FIL3         PIC X(1) VALUE SPACES.                      ZABAUDIT
012441     05  ERESOLU      PIC X(10) VALUE 'REPRISE:N '.                     IT
012450     05  EDESCRIPT    PIC X(12) VALUE 'LIBELLE:'.                 ZABAUDIT
012460     05  NDESCRIPT    PIC X(30) VALUE SPACES.                     ZABAUDIT
012470     05  FIL4         PIC X(05) VALUE SPACES.                           IT
012500 01  WF-HYPHEN-LINE              PIC X(80)  VALUE SPACES.         ZABAUDIT
012600*------------------                                               ZABAUDIT
012700                                                                  ZABAUDIT
012800 01  WF-BLANK-LINE               PIC X(80)  VALUE SPACES.         ZABAUDIT
012900*-----------------                                                ZABAUDIT
013000                                                                  ZABAUDIT
013100******************************************************************ZABAUDIT
013200                                                                  ZABAUDIT
013300 LINKAGE SECTION.                                                 ZABAUDIT
013400*----------------                                                 ZABAUDIT
013500                                                                  ZABAUDIT
013600 01  L-PARM.                                                      ZABAUDIT
013700*-----------                                                      ZABAUDIT
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
013800*    05  FILLER                  PIC S9(04) COMP.                 ZABAUDIT
      *--                                                                       
           05  FILLER                  PIC S9(04) COMP-5.                       
      *}                                                                        
013900     05  L-JOBSET-NAME           PIC  X(06).                      ZABAUDIT
014000                                                                  ZABAUDIT
014100******************************************************************ZABAUDIT
014200                                                                  ZABAUDIT
014300 PROCEDURE DIVISION USING L-PARM.                                 ZABAUDIT
014400*------------------                                               ZABAUDIT
014500                                                                  ZABAUDIT
014600     OPEN OUTPUT ZABFILE.                                         ZABAUDIT
014700                                                                  ZABAUDIT
014800     MOVE ALL '-'       TO WF-HYPHEN-LINE.                        ZABAUDIT
014900     MOVE L-JOBSET-NAME TO WF-JOBSET-NAME.                        ZABAUDIT
014910     MOVE L-JOBSET-NAME TO ZCHAINE.                               ZABAUDIT
      *{ Post-Translation Correct-CURRENT-DATE                                  
015000*     MOVE CURRENT-DATE  TO WS-CURRENT-DATE.                      ZABAUDIT
           MOVE FUNCTION CURRENT-DATE  TO WS-CURRENT-DATE.                      
      *} Post-Translation                                                       
015100     MOVE CORRESPONDING WS-CURRENT-DATE                           ZABAUDIT
015200                        TO WF-DATE-OF-DAY.                        ZABAUDIT
015210     MOVE CORRESPONDING WS-CURRENT-DATE                           ZABAUDIT
015220                        TO WX-DATE-OF-DAY.                        ZABAUDIT
      *{ Post-Translation Correct-TIME-OF-DAY                                   
015300*     MOVE TIME-OF-DAY   TO WS-TIME-NUMERIC.                      ZABAUDIT
           MOVE FUNCTION CURRENT-DATE (9:6) TO WS-TIME-NUMERIC.                 
      *} Post-Translation                                                       
015400     MOVE CORRESPONDING WS-TIME-OF-DAY                            ZABAUDIT
015500                        TO WF-TIME-OF-DAY.                        ZABAUDIT
015600***  DISPLAY '---> ' WF-ZABFILE-AREA.                             ZABAUDIT
015700                                                                  ZABAUDIT
015800     WRITE ZABFILE-RECORD FROM WF-ZABFILE-REC1.                   ZABAUDIT
015900     WRITE ZABFILE-RECORD FROM WF-HYPHEN-LINE.                    ZABAUDIT
016000     WRITE ZABFILE-RECORD FROM WF-ZABFILE-REC6.                   ZABAUDIT
016010     WRITE ZABFILE-RECORD FROM WF-ZABFILE-REC2.                   ZABAUDIT
016100     WRITE ZABFILE-RECORD FROM WF-ZABFILE-REC3.                   ZABAUDIT
016200     WRITE ZABFILE-RECORD FROM WF-BLANK-LINE.                     ZABAUDIT
016300     WRITE ZABFILE-RECORD FROM WF-BLANK-LINE.                     ZABAUDIT
016400     WRITE ZABFILE-RECORD FROM WF-BLANK-LINE.                     ZABAUDIT
016500     WRITE ZABFILE-RECORD FROM WF-HYPHEN-LINE.                    ZABAUDIT
016600     WRITE ZABFILE-RECORD FROM WF-ZABFILE-REC4.                   ZABAUDIT
016700     WRITE ZABFILE-RECORD FROM WF-BLANK-LINE.                     ZABAUDIT
016800     WRITE ZABFILE-RECORD FROM WF-BLANK-LINE.                     ZABAUDIT
016900     WRITE ZABFILE-RECORD FROM WF-BLANK-LINE.                     ZABAUDIT
017000     WRITE ZABFILE-RECORD FROM WF-HYPHEN-LINE.                    ZABAUDIT
017100     WRITE ZABFILE-RECORD FROM WF-ZABFILE-REC5.                   ZABAUDIT
017200                                                                  ZABAUDIT
017300     CLOSE ZABFILE.                                               ZABAUDIT
017400     GOBACK.                                                      ZABAUDIT
017500                                                                  ZABAUDIT
017600*-----------------------------------------------------------------ZABAUDIT
017700*    END OF PROGRAM ZABAUDIT                                      ZABAUDIT
017800*-----------------------------------------------------------------ZABAUDIT
