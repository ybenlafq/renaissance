      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MMFAR RATIO PSE                        *        
      *----------------------------------------------------------------*        
       01  RVMMFAR.                                                             
           05  MMFAR-CTABLEG2    PIC X(15).                                     
           05  MMFAR-CTABLEG2-REDEF REDEFINES MMFAR-CTABLEG2.                   
               10  MMFAR-CFAM            PIC X(05).                             
               10  MMFAR-CTARIF          PIC X(02).                             
           05  MMFAR-WTABLEG     PIC X(80).                                     
           05  MMFAR-WTABLEG-REDEF  REDEFINES MMFAR-WTABLEG.                    
               10  MMFAR-WFLAG           PIC X(01).                             
               10  MMFAR-RATIO           PIC X(03).                             
               10  MMFAR-RATIO-N        REDEFINES MMFAR-RATIO                   
                                         PIC 9(03).                             
               10  MMFAR-COMMENT         PIC X(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVMMFAR-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MMFAR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MMFAR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MMFAR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MMFAR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
