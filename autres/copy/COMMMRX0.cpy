      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
YS12K4*  DATE       : 12/11/2014                                        00000220
      *  AUTEUR     : Y SCOPIN (DSA004)                                 00000230
      *  REPAIRE    : YS12K4                                            00000240
      *  OBJET      : AGRANDISSEMENT TABLEAU DES FAMILLES A 1000        00000250
      *-----------------------------------------------------------------00000260
       01  COMM-RX00-APPLI.                                                     
           05  COMM-RX00-NETAT                 PIC  X(2).                       
           05  COMM-RX00-NCONC                 PIC  X(4).                       
           05  COMM-RX00-NSOC                  PIC  X(3).                       
           05  COMM-RX00-NMAG                  PIC  X(3).                       
           05  COMM-RX00-NRELEVE               PIC  X(7).                       
           05  COMM-RX00-CODRET                PIC  S9(4).                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-RX00-MAXFAM       COMP     PIC  S9(4).                      
      *--                                                                       
           05  COMM-RX00-MAXFAM COMP-5     PIC  S9(4).                          
      *}                                                                        
           05  COMM-RX00-FAMILLE.                                               
YS12K4*        10 COMM-RX00-FAM OCCURS  800    PIC  X(5).                       
YS12K4         10 COMM-RX00-FAM OCCURS 1000    PIC  X(5).                       
           05  COMM-RX00-MESS                  PIC  X(78).                      
           05  COMM-RX00-SSAAMMJJ              PIC  X(8).                       
           05  COMM-RX00-DSUPPORT              PIC  X(8).                       
           05  COMM-RX00-LENSEIGN              PIC  X(15).                      
           05  COMM-RX00-DEBUT                 PIC  X(03).                      
           05  COMM-RX00-FIN                   PIC  X(03).                      
           05  COMM-RX00-MEM-SAUT.                                              
               10  COMM-RX00-MEM-FAM           PIC  X(05).                      
               10  COMM-RX00-MEM-MARK          PIC  X(20).                      
YS12K4*    05  FILLER                          PIC  X(3933).                    
YS12K4     05  FILLER                          PIC  X(2931).                    
                                                                                
