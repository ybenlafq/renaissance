      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      * ZONES DE TRAVAIL UTILISEES POUR LA GESTION DE LA TS TSTNECEN            
       77 WC-ID-TSTNECEN          PIC  X(08)      VALUE 'TSTNECEN'.             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 WP-RANG-TSTNECEN        PIC S9(04) COMP VALUE +0.                     
      *--                                                                       
       77 WP-RANG-TSTNECEN        PIC S9(04) COMP-5 VALUE +0.                   
      *}                                                                        
       77 WS-TSTNECEN-NOMPGM      PIC  X(10)      VALUE SPACE.                  
      *                                                                         
       01 WF-TSTNECEN             PIC X.                                        
          88 WC-TSTNECEN-SUITE           VALUE '0'.                             
          88 WC-TSTNECEN-FIN             VALUE '1'.                             
          88 WC-TSTNECEN-ERREUR          VALUE '2'.                             
       01 WF-XCTRL-NECEN          PIC X.                                        
          88 WC-XCTRL-NECEN-TROUVE       VALUE '0'.                             
          88 WC-XCTRL-NECEN-FIN          VALUE '1'.                             
      * DESCRIPTION DE LA TS.                                                   
       01 TS-TSTNECEN-DONNEES.                                                  
          05 TSTNECEN-NSOCZP                       PIC X(05).                   
          05 TSTNECEN-TRANS                        PIC X(05).                   
          05 TSTNECEN-WFLAG                        PIC X(01).                   
          05 TSTNECEN-WPARAM                       PIC X(10).                   
          05 TSTNECEN-LIBELLE                      PIC X(30).                   
                                                                                
