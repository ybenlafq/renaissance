      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                               *         
       01  TS-IDMQ.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-IDMQ-LONG                PIC S9(03) COMP-3                     
                                          VALUE +183.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-IDMQ-DONNEES.                                                  
              03 TS-IDMQ-LENGTH           PIC 9(03).                            
              03 TS-IDMQ-VALUE            PIC X(180).                           
      *                                                                         
                                                                                
