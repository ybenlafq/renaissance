      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * Transposition Codes Vendeurs                                            
      ******************************************************************        
       01  RVYF0000.                                                            
           10 YF00-NSOCANC         PIC X(3).                                    
           10 YF00-NMAGANC         PIC X(3).                                    
           10 YF00-CVENDANC        PIC X(6).                                    
           10 YF00-NSOCNV          PIC X(3).                                    
           10 YF00-NMAGNV          PIC X(3).                                    
           10 YF00-CVENDNV         PIC X(6).                                    
           10 YF00-LVENDEUR        PIC X(15).                                   
           10 YF00-NMATSIGA        PIC X(7).                                    
           10 YF00-WPRIME          PIC X(1).                                    
           10 YF00-DCREATION       PIC X(8).                                    
           10 YF00-DSYST           PIC S9(13)V USAGE COMP-3.                    
                                                                                
