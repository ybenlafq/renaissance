      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * -------------------------------------------------------------- *        
      * -                                                            - *        
      * -                                                            - *        
      * -                                                            - *        
      * -                                                            - *        
      * -------------------------------------------------------------- *        
       01 CMM-MIQ00-DATA.                                                       
          05 CMM-MIQ00-TYPAPPEL            PIC X(02).                           
          05 CMM-MIQ00-DATEJOUR            PIC X(08).                           
          05 CMM-MIQ00-QNT0                PIC 99999.                           
          05 CMM-MIQ00-DATED               PIC X(08).                           
          05 CMM-MIQ00-NSOCIETE            PIC X(03).                           
          05 CMM-MIQ00-NLIEU               PIC X(03).                           
          05 CMM-MIQ00-NZPREST             PIC X(02).                           
      * -> VISION STOCK AVANC�... O/N                                           
          05 CMM-MIQ00-STKAV               PIC X(01).                           
      * -> INTERROGATION POOUR BE                                               
          05 CMM-MIQ00-LDM                 PIC X(03).                           
          05 FILLER                        PIC X(19).                           
          05 DEBUG                         PIC X(01).                           
          05 CMM-MIQ00-STAGE1.                                                  
              10 CMM-STAGE1-CPOSTAL         PIC X(05).                          
              10 CMM-STAGE1-LCOMMUNE        PIC X(32).                          
              10 CMM-STAGE1-CINSEE          PIC X(05).                          
              10 CMM-STAGE1-CELEMEN         PIC X(05).                          
              10 CMM-STAGE1-NCODIC          PIC X(07).                          
              10 COMM-IQ00-QVOLUME          PIC S9(9)V99 COMP-3                 
                                            VALUE ZEROES.                       
              10 CMM-STAGE1-CFAM            PIC X(05).                          
              10 CMM-STAGE1-CROSS-DOCK      PIC X(01).                          
              10 CMM-IQ00-CPT-DISPO      PIC S9(04) COMP-3.                     
          05 CMM-MIQ00-STAGE2.                                                  
            10 CMM-STAGE2-AFFICHAGE      PIC 9(01)        VALUE 0.              
               88 CMM-STAGE2-FAM-VIDE                     VALUE 0.              
               88 CMM-STAGE2-FAM-OK                       VALUE 1.              
             10 CMM-STAGE2-CFAMSRVCE        PIC X(05).                          
             10 CMM-STAGE2-LFAMSRVCE        PIC X(20).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      10 CMM-STAGE2-TS-DISPO         PIC S9(4) COMP.                     
      *--                                                                       
             10 CMM-STAGE2-TS-DISPO         PIC S9(4) COMP-5.                   
      *}                                                                        
             10 CMM-STAGE2-DDISPO           PIC X(08).                          
             10 CMM-STAGE2-NBSRVCE          PIC S9(3) COMP-3 VALUE 0.           
             10 CMM-STAGE2-TAB1 OCCURS 100.                                     
                15 CMM-STAGE2-SRVCE         PIC X(05).                          
                15 CMM-STAGE2-CMODDEL-S     PIC X(03).                          
                15 CMM-STAGE2-CMODDEL       PIC X(03).                          
                15 CMM-STAGE2-CELEMEN       PIC X(05).                          
                15 CMM-STAGE2-CPERIM        PIC X(05).                          
                15 CMM-STAGE2-CPLAGE        PIC X(02).                          
                15 CMM-STAGE2-CADRE         PIC X(05).                          
                15 CMM-STAGE2-CEQUIPE       PIC X(05).                          
                15 CMM-STAGE2-MINI          PIC 9(02).                          
                15 CMM-STAGE2-MAXI          PIC 9(03).                          
                15 CMM-STAGE2-DDISPO        PIC X(08).                          
                15 CMM-STAGE2-CPLAGES       PIC X(30).                          
      * -> ZONE CONTENANT ZONES DE LIVRAISON DONNEES PAR MIQ00                  
           05 CMM-MIQ00-STAGE3              PIC X(23200).                       
          05 COMM-MIQ00-SORTIE.                                                 
             10 COMM-MIQ00-NOMTS           PIC X(08).                           
             10 COMM-MIQ00-CODRET          PIC X(02).                           
             10 COMM-MIQ00-LIBRET          PIC X(75).                           
                                                                                
