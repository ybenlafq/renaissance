      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: R�CEPTION LIBELL� ENT�TE STANDARD               * 00031000
      * NOM FICHIER.: RLESTD                                          * 00032001
      *---------------------------------------------------------------* 00034000
      * CR   .......: 05/12/2011                                      * 00035000
      * MODIFIE.....:   /  /                                          * 00036000
      * VERSION N�..: 001                                             * 00037000
      * LONGUEUR....: 033                                             * 00037100
      ***************************************************************** 00037300
      *                                                                 00037400
       01  RLESTD.                                                      00037500
           05      RLESTD-TYP-ENREG       PIC  X(0006).                 00037600
           05      RLESTD-CODE            PIC  X(0005).                 00037700
           05      RLESTD-LIBELLE         PIC  X(0020).                 00037800
           05      RLESTD-TYPE            PIC  9(0001).                 00037900
           05      RLESTD-FIN             PIC  X(0001).                 00170600
                                                                                
