      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *==> DARTY *****************************************************          
      *          * ZONE START-RETRIEVE DU PROGRAMME TIGAG            *          
      ************************************************ COPY STRTIGGC *          
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  STRT-IGAG-LONG-AREA PIC S9(4) COMP VALUE +103.                       
      *--                                                                       
       01  STRT-IGAG-LONG-AREA PIC S9(4) COMP-5 VALUE +103.                     
      *}                                                                        
      *                                                                         
       01  Z-STRTAREA-IGAG.                                                     
      *---CONFIGURATION ETAT A IMPRIMER                                         
           02 STRT-IGAG-CLE.                                                    
              05 STRT-IGAG-ETA        PIC X(06).                                
              05 STRT-IGAG-DAT        PIC X(06).                                
              05 STRT-IGAG-DST        PIC X(09).                                
              05 STRT-IGAG-DOC        PIC X(15).                                
      *---CARACTERISTIQUES ETAT                                                 
      *      . NUMERO DE CONFIGURATION                                          
      *      . TYPE IMPRESSION                                                  
           02 STRT-IGAG-CFG           PIC X(01).                                
           02 STRT-IGAG-IMP           PIC X(01).                                
      *---CARACTERISTIQUES IMPRIMANTE                                           
      *      . CODE IMPRIMANTE TCT                                              
      *      . TYPE IMPRIMANTE (SYRD)                                           
      *      . MARQUE IMPRIMANTE                                                
      *      . MODELE IMPRIMANTE                                                
           02 STRT-IGAG-PRT           PIC X(04).                                
           02 STRT-IGAG-TYP           PIC X(01).                                
           02 STRT-IGAG-MRQ           PIC X(20).                                
           02 STRT-IGAG-MDL           PIC X(08).                                
      *---CARACTERISTIQUES PROTOCOLE                                            
      *       . LONGUEUR ZONE COMMANDES                                         
      *       . LONGUEUR ZONE DONNEES                                           
      *       . HAUTEUR PAGE GRAPHIQUE                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 STRT-IGAG-CMD           PIC 9(04) COMP.                           
      *--                                                                       
           02 STRT-IGAG-CMD           PIC 9(04) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 STRT-IGAG-DON           PIC 9(04) COMP.                           
      *--                                                                       
           02 STRT-IGAG-DON           PIC 9(04) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 STRT-IGAG-HTR           PIC 9(04) COMP.                           
      *--                                                                       
           02 STRT-IGAG-HTR           PIC 9(04) COMP-5.                         
      *}                                                                        
      *---PROVENANCE                                                            
      *       . ORIGINE PROGRAMME                                               
      *       . NOM DE LA TS                                                    
      *       . LONGUEUR DE LA TS                                               
      *       . NOMBRE ITEM DE LA TS                                            
           02 STRT-IGAG-PGM           PIC X(08).                                
           02 STRT-IGAG-TSN           PIC X(08).                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 STRT-IGAG-TSL           PIC 9(04) COMP.                           
      *--                                                                       
           02 STRT-IGAG-TSL           PIC 9(04) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 STRT-IGAG-TSI           PIC 9(04) COMP.                           
      *--                                                                       
           02 STRT-IGAG-TSI           PIC 9(04) COMP-5.                         
      *}                                                                        
      *---DATE DU JOUR AAMMJJ           */                                      
           02 STRT-IGAG-AMJ           PIC X(06).                                
      *-------------------------------------------------------------*           
      *  ===> ATTENTION <==                                         *           
      *  CETTE DESCRIPTION PL1-STRTIGGP A UNE CORRESPONDANCE EN     *           
      *  COBOL-STRTIGGC .                                           *           
      *  LES MODIFICATIONS DOIVENT ETRE RECIPROQUES .               *           
      *-------------------------------------------------------------*           
              EJECT                                                             
                                                                                
