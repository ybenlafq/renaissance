      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PAYS  CODES PAYS                       *        
      *----------------------------------------------------------------*        
       01  RVPAYS .                                                             
           05  PAYS-CTABLEG2     PIC X(15).                                     
           05  PAYS-CTABLEG2-REDEF  REDEFINES PAYS-CTABLEG2.                    
               10  PAYS-CPAYS            PIC X(03).                             
           05  PAYS-WTABLEG      PIC X(80).                                     
           05  PAYS-WTABLEG-REDEF   REDEFINES PAYS-WTABLEG.                     
               10  PAYS-LPAYS            PIC X(15).                             
               10  PAYS-INDIC            PIC X(10).                             
               10  PAYS-EXTENS           PIC X(05).                             
               10  PAYS-PLAQUE           PIC X(03).                             
               10  PAYS-PAYS2            PIC X(02).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVPAYS-FLAGS.                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PAYS-CTABLEG2-F   PIC S9(4)  COMP.                               
      *--                                                                       
           05  PAYS-CTABLEG2-F   PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PAYS-WTABLEG-F    PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PAYS-WTABLEG-F    PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
