      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PUMUT TYPE DE MUT LHEURLIMIT           *        
      *----------------------------------------------------------------*        
       01  RVPUMUT.                                                             
           05  PUMUT-CTABLEG2    PIC X(15).                                     
           05  PUMUT-CTABLEG2-REDEF REDEFINES PUMUT-CTABLEG2.                   
               10  PUMUT-TYPMUT          PIC X(10).                             
           05  PUMUT-WTABLEG     PIC X(80).                                     
           05  PUMUT-WTABLEG-REDEF  REDEFINES PUMUT-WTABLEG.                    
               10  PUMUT-WACTIF          PIC X(01).                             
               10  PUMUT-WPURGE          PIC X(01).                             
               10  PUMUT-WDIVERS         PIC X(54).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVPUMUT-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PUMUT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PUMUT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PUMUT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PUMUT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
