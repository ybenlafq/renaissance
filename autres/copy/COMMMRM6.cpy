      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
       01  COMM-MRM6-APPLI.                                                     
           05  COMM-MRM6-CRMGROUP              PIC  X(5).                       
           05  COMM-MRM6-CFAM                  PIC  X(5).                       
           05  COMM-MRM6-LAGREGATED            PIC  X(20).                      
           05  COMM-MRM6-NCODIC                PIC  X(7).                       
           05  COMM-MRM6-DATES OCCURS 8.                                        
               10  COMM-MRM6-SEMAINE           PIC  X(6).                       
           05  COMM-MRM6-DATEJOUR              PIC  X(8).                       
           05  COMM-MRM6-CODRET                PIC  S9(4).                      
           05  COMM-MRM6-MESS                  PIC  X(78).                      
           05  COMM-MRM6-INDTS                 PIC  9(4)  COMP-3.               
           05  COMM-MRM6-GROUPE                PIC  X.                          
           05  COMM-MRM6-TABLEAU.                                               
HR    *        10  COMM-MRM6-TABTS OCCURS  500.                                 
HR             10  COMM-MRM6-TABTS OCCURS 1000.                                 
                   15  COMM-MRM6-TAB-NCODIC    PIC  X(7).                       
           05  COMM-MRM6-NSOCIETE              PIC  X(7).                       
           05  COMM-MRM6-GROUPES.                                               
               06 COMM-MRM6-LIGNE-GROUPE      OCCURS 10.                        
                  10 COMM-MRM6-DETAIL-GROUPE.                                   
                     15 COMM-MRM6-MCGROUP      PIC X(5).                        
                     15 COMM-MRM6-MFILIALE     PIC X(3).                        
                  10 COMM-MRM6-QSEUILSEGMENT   PIC  9(3)  COMP-3.               
                  10 COMM-MRM6-QCSTLISSAGE     PIC  9V99  COMP-3.               
                  10 COMM-MRM6-QNBSEM          PIC  9(3)  COMP-3.               
                  10 COMM-MRM6-QPV             PIC S9(7)  COMP-3.               
                  10 COMM-MRM6-NATURE          PIC  X.                          
                  10 COMM-MRM6-MARCHE          PIC  X.                          
                  10 COMM-MRM6-QPM             PIC S9(7)  COMP-3.               
               06 COMM-MRM6-LIGNE-FILIALE     OCCURS 10.                        
                  10 COMM-MRM6-FILIALE         PIC X(3).                        
               06 COMM-MRM6-LIGNE-DEPOT       OCCURS 10.                        
                  10 COMM-MRM6-NSOCDEPOT       PIC X(3).                        
                  10 COMM-MRM6-NDEPOT          PIC X(3).                        
           05  COMM-MRM6-NBFIL                 PIC S9(3).                       
           05  COMM-MRM6-NBGROUP               PIC S9(3).                       
           05  COMM-MRM6-NBDEP                 PIC S9(3).                       
           05  COMM-MRM6-S-1                   PIC X(06).                       
           05  COMM-MRM6-S-2                   PIC X(06).                       
           05  COMM-MRM6-S-3                   PIC X(06).                       
           05  COMM-MRM6-S-4                   PIC X(06).                       
           05  COMM-MRM6-S-5                   PIC X(06).                       
           05  COMM-MRM6-S-6                   PIC X(06).                       
           05  COMM-MRM6-S-7                   PIC X(06).                       
           05  COMM-MRM6-S-8                   PIC X(06).                       
HR   **    05  FILLER                          PIC  X(023).                     
HR         05  FILLER                          PIC  X(619).                     
                                                                                
