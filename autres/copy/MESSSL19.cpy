      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *                                                                         
      *   COPY MESSSL19 STRICTEMENT EGALE A CCBMDSL19 SUR AS400                 
      *                                                                         
      *   COPY MESSAGES MQ ENVOYE PAR LE HOST                                   
      *   PROGRAMME MSL19 TAILLE MESSAGE 40000                                  
      *                                                                         
      *   EDITION DU SYNTHETIQUE DES VENTES NMD                                 
      *                                                                         
      *****************************************************************         
      *                                                                         
      *                                                                         
           05  MESS-SL19-MESSAGE.                                               
      *                                                                         
      *      ENTETE - 1 / TRAVAIL DEMANDE        (58).                          
             10  MESS-SL19-DEMANDE.                                             
              15 MESS-SL19-CTRT            PIC  X(10).                          
              15 MESS-SL19-NSOCTRT         PIC  X(03).                          
              15 MESS-SL19-NLIEUTRT        PIC  X(03).                          
              15 MESS-SL19-USER            PIC  X(10).                          
              15 MESS-SL19-WSID            PIC  X(10).                          
              15 MESS-SL19-PRINTER         PIC  X(10).                          
              15 MESS-SL19-LGPARAM         PIC S9(09) COMP-3.                   
              15 MESS-SL19-DSYST           PIC S9(13) COMP-3.                   
      *                                                                         
      *      ENTETE - 2 / CRITERES DE SELECTION (136).                          
             10  MESS-SL19-SELECTION.                                           
      *       CRITERES DE SELECTION.                                            
              15 MESS-SL19-CSEL.                                                
               20 MESS-SL19-CZOND          PIC  X(5).                           
               20 MESS-SL19-NSOCPTF        PIC  X(3).                           
               20 MESS-SL19-NLIEUPTF       PIC  X(3).                           
               20 MESS-SL19-NLIEUMAGPTF    PIC  X(3).                           
               20 MESS-SL19-WPTF           PIC  X(1).                           
               20 MESS-SL19-EQUIPAGE       PIC  X(3).                           
               20 MESS-SL19-WEMPLIV        PIC  X(1).                           
               20 MESS-SL19-DDELIV1        PIC  X(8).                           
               20 MESS-SL19-DDELIV2        PIC  X(8).                           
      *       CRITERES DE TRI                                                   
              15 MESS-SL19-CTRI.                                                
               20 MESS-SL19-MSG            PIC  X(7) OCCURS 6.                  
      *       MAGASINS.                                                         
              15 MESS-SL19-NBMAG           PIC S9(3) COMP-3.                    
              15 MESS-SL19-MAGASINS.                                            
               20 MESS-SL19-NLIEUMAG       PIC  X(3) OCCURS 20.                 
      *                                                                         
      *      DONNEES RETOURNEES.                                                
      *      (30000 - 105 - 58 - 139 = 29698)                                   
             10  MESS-SL19-DATA            PIC  X(29698).                       
      *                                                                         
      *                                                                         
      ******************************************************************        
      *          DEFINITIONS PAR TYPE DE MESSAGE                                
      ******************************************************************        
      *                                                                         
      *          ENTETE DE VENTE                                                
       01        MESS-SL19-A.                                                   
              05 MESS-SL19-A-CTYPE         PIC  X(01) VALUE 'A'.                
              05 MESS-SL19-A-NSOCVTE       PIC  X(03).                          
              05 MESS-SL19-A-NLIEUVTE      PIC  X(03).                          
              05 MESS-SL19-A-NVENTE        PIC  X(07).                          
              05 MESS-SL19-A-DDELIV        PIC  X(08).                          
              05 MESS-SL19-A-CPLAGE        PIC  X(02).                          
              05 MESS-SL19-A-CPLOGI        PIC  X(02).                          
              05 MESS-SL19-A-CTITRENOM     PIC  X(05).                          
              05 MESS-SL19-A-LNOM          PIC  X(25).                          
              05 MESS-SL19-A-LPRENOM       PIC  X(15).                          
              05 MESS-SL19-A-LBATIMENT     PIC  X(03).                          
              05 MESS-SL19-A-LESCALIER     PIC  X(03).                          
              05 MESS-SL19-A-LETAGE        PIC  X(03).                          
              05 MESS-SL19-A-LPORTE        PIC  X(03).                          
              05 MESS-SL19-A-LCMPAD1       PIC  X(32).                          
              05 MESS-SL19-A-LCMPAD2       PIC  X(32).                          
              05 MESS-SL19-A-CVOIE         PIC  X(05).                          
              05 MESS-SL19-A-CTVOIE        PIC  X(04).                          
              05 MESS-SL19-A-LNOMVOIE      PIC  X(21).                          
              05 MESS-SL19-A-LCOMMUNE      PIC  X(32).                          
              05 MESS-SL19-A-CPOSTAL       PIC  X(05).                          
              05 MESS-SL19-A-TELDOM        PIC  X(10).                          
              05 MESS-SL19-A-TELBUR        PIC  X(10).                          
DA03          05 MESS-SL19-A-NGSM          PIC  X(10).                          
              05 MESS-SL19-A-DVENTE        PIC  X(08).                          
              05 MESS-SL19-A-DHMVENTE      PIC  X(04).                          
              05 MESS-SL19-A-PTTVENTE      PIC S9(07)V99 COMP-3.                
              05 MESS-SL19-A-PLIVR         PIC S9(07)V99 COMP-3.                
              05 MESS-SL19-A-CZONLIV       PIC  X(05).                          
      *                                                                         
      *        TYPE MESSAGE-B : COMMENTAIRES DE VENTE (GV10-LCOMMVT             
       01        MESS-SL19-B.                                                   
              05 MESS-SL19-B-CTYPE         PIC  X(01) VALUE 'B'.                
              05 MESS-SL19-B-LCOMVTE       PIC  X(30).                          
      *                                                                         
      *        TYPE MESSAGE-C : COMMENTAIRES DE LIVR. (GV02-LCOMMLI             
       01        MESS-SL19-C.                                                   
              05 MESS-SL19-C-CTYPE         PIC  X(01) VALUE 'C'.                
              05 MESS-SL19-C-LCOMLIV       PIC  X(78).                          
      *                                                                         
      *        TYPE MESSAGE-D : LIGNES DE VENTE (GV11)                          
      *          PARTIE IDENTIFIANT                                             
       01        MESS-SL19-D.                                                   
              05 MESS-SL19-D-CTYPE         PIC  X(01) VALUE 'D'.                
              05 MESS-SL19-D-CMODDEL       PIC  X(03).                          
              05 MESS-SL19-D-NSEQNQ        PIC S9(05) COMP-3.                   
              05 MESS-SL19-D-NLIGNE        PIC  X(02).                          
              05 MESS-SL19-D-NCODIC        PIC  X(07).                          
              05 MESS-SL19-D-QVENDUE       PIC S9(05)    COMP-3.                
              05 MESS-SL19-D-PVUNIT        PIC S9(07)V99 COMP-3.                
              05 MESS-SL19-D-CVENDEUR      PIC  X(06).                          
              05 MESS-SL19-D-NSOCGST       PIC  X(03).                          
              05 MESS-SL19-D-NLIEUGST      PIC  X(03).                          
              05 MESS-SL19-D-NSOCDEL       PIC  X(03).                          
              05 MESS-SL19-D-NLIEUDEL      PIC  X(03).                          
              05 MESS-SL19-D-NSOCSTK       PIC  X(03).                          
              05 MESS-SL19-D-NLIEUSTK      PIC  X(03).                          
              05 MESS-SL19-D-NSOCDEP       PIC  X(03).                          
              05 MESS-SL19-D-NLIEUDEP      PIC  X(03).                          
              05 MESS-SL19-D-NBE           PIC  X(07).                          
              05 MESS-SL19-D-WACOMMUTER    PIC  X(01).                          
              05 MESS-SL19-D-WCQERESF      PIC  X(01).                          
              05 MESS-SL19-D-DMUTATION     PIC  X(08).                          
              05 MESS-SL19-D-NMUTATION     PIC  X(07).                          
              05 MESS-SL19-D-CTOURNEE      PIC  X(03).                          
DA03          05 MESS-SL19-D-EQUTY         PIC  X(03).                          
              05 MESS-SL19-D-QSTOCKDEP     PIC S9(05) COMP-3.                   
              05 MESS-SL19-D-QSTOCKPTF     PIC S9(05) COMP-3.                   
      *                                                                         
      *        TYPE MESSAGE-E : COMMENTAIRES DE LIGNE DE VENTE (GV11)           
      *          PARTIE IDENTIFIANT                                             
       01        MESS-SL19-E.                                                   
              05 MESS-SL19-E-CTYPE         PIC  X(01) VALUE 'E'.                
              05 MESS-SL19-E-LCOMMENT      PIC  X(35).                          
      *                                                                         
      *                                                                         
      *                                                                         
      ******************************************************************        
      * DESCRIPTION DE MESSAGE VERSION 1.                                       
      ******************************************************************        
       01  MESS-SL19-MESSAGE-V1.                                                
      *      ENTETE - 1 / TRAVAIL DEMANDE       (58).                           
             15  MESS-SL19-DEMANDE.                                             
              20 MESS-SL19-CTRT            PIC  X(10).                          
              20 MESS-SL19-NSOCTRT         PIC  X(03).                          
              20 MESS-SL19-NLIEUTRT        PIC  X(03).                          
              20 MESS-SL19-USER            PIC  X(10).                          
              20 MESS-SL19-WSID            PIC  X(10).                          
              20 MESS-SL19-PRINTER         PIC  X(10).                          
              20 MESS-SL19-LGPARAM         PIC S9(09) COMP-3.                   
              20 MESS-SL19-DSYST           PIC S9(13) COMP-3.                   
      *      ENTETE - 2 / CRITERES DE SELECTION (94).                           
             15  MESS-SL19-SELECTION.                                           
      *       CRITERES DE SELECTION.                                            
              20 MESS-SL19-CSEL.                                                
               25 MESS-SL19-CZOND          PIC  X(5).                           
               25 MESS-SL19-NSOCPTF        PIC  X(3).                           
               25 MESS-SL19-NLIEUPTF       PIC  X(3).                           
               25 MESS-SL19-NLIEUMAGPTF    PIC  X(3).                           
               25 MESS-SL19-WPTF           PIC  X(1).                           
               25 MESS-SL19-EQUIPAGE       PIC  X(3).                           
               25 MESS-SL19-WEMPLIV        PIC  X(1).                           
               25 MESS-SL19-DDELIV1        PIC  X(8).                           
               25 MESS-SL19-DDELIV2        PIC  X(8).                           
      *       MAGASINS.                                                         
              20 MESS-SL19-NBMAG           PIC S9(3) COMP-3.                    
              20 MESS-SL19-MAGASINS.                                            
               25 MESS-SL19-NLIEUMAG       PIC  X(3) OCCURS 20.                 
      *      DONNEES RETOURNEES.                                                
      *      (30000 - 97 - 58 - 105 = 29740)                                    
             15  MESS-SL19-DATA            PIC  X(29740).                       
      *                                                                         
      ******************************************************************        
      * ENTETE DE VENTE - VERSION 1.                                            
      ******************************************************************        
       01        MESS-SL19-A-V1.                                                
              05 MESS-SL19-A-CTYPE         PIC  X(01) VALUE 'A'.                
              05 MESS-SL19-A-NSOCVTE       PIC  X(03).                          
              05 MESS-SL19-A-NLIEUVTE      PIC  X(03).                          
              05 MESS-SL19-A-NVENTE        PIC  X(07).                          
              05 MESS-SL19-A-DDELIV        PIC  X(08).                          
              05 MESS-SL19-A-CPLAGE        PIC  X(02).                          
              05 MESS-SL19-A-CPLOGI        PIC  X(02).                          
              05 MESS-SL19-A-CTITRENOM     PIC  X(05).                          
              05 MESS-SL19-A-LNOM          PIC  X(25).                          
              05 MESS-SL19-A-LPRENOM       PIC  X(15).                          
              05 MESS-SL19-A-LBATIMENT     PIC  X(03).                          
              05 MESS-SL19-A-LESCALIER     PIC  X(03).                          
              05 MESS-SL19-A-LETAGE        PIC  X(03).                          
              05 MESS-SL19-A-LPORTE        PIC  X(03).                          
              05 MESS-SL19-A-LCMPAD1       PIC  X(32).                          
              05 MESS-SL19-A-LCMPAD2       PIC  X(32).                          
              05 MESS-SL19-A-CVOIE         PIC  X(05).                          
              05 MESS-SL19-A-CTVOIE        PIC  X(04).                          
              05 MESS-SL19-A-LNOMVOIE      PIC  X(21).                          
              05 MESS-SL19-A-LCOMMUNE      PIC  X(32).                          
              05 MESS-SL19-A-CPOSTAL       PIC  X(05).                          
              05 MESS-SL19-A-TELDOM        PIC  X(10).                          
              05 MESS-SL19-A-TELBUR        PIC  X(10).                          
              05 MESS-SL19-A-NGSM          PIC  X(10).                          
              05 MESS-SL19-A-DVENTE        PIC  X(08).                          
              05 MESS-SL19-A-DHMVENTE      PIC  X(04).                          
              05 MESS-SL19-A-PTTVENTE      PIC S9(07)V99 COMP-3.                
              05 MESS-SL19-A-PLIVR         PIC S9(07)V99 COMP-3.                
      *                                                                         
      *                                                                         
----->* FIN COPY MESSSL19                                                       
                                                                                
