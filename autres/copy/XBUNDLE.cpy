      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EXEC SQL                                                             
       declare CG cursor for                                                    
       --codif_bundle                                                           
       with                                                                     
       --Profil d'affectation en cours des soci�t�s                             
       FL05 as (                                                                
       select                                                                   
       FL05.NSOCIETE                                                            
       , FL05.NLIEU                                                             
       , FL05.CPROAFF                                                           
       from RVFL0500 FL05                                                       
       inner join RVLI0000 LI00 on                                              
       LI00.CPARAM = 'FIL'                                                      
       and LI00.LVPARAM = 'O'                                                   
       and LI00.NSOCIETE = FL05.NSOCIETE                                        
       and LI00.NLIEU = FL05.NLIEU                                              
       left join RVFL0500 Z on                                                  
       Z.CTYPTRAIT = 'GA50M'                                                    
       and Z.DEFFET > FL05.DEFFET                                               
       and Z.NSOCIETE = FL05.NSOCIETE                                           
       and Z.NLIEU = FL05.NLIEU                                                 
       where                                                                    
       FL05.CTYPTRAIT = 'GA50M'                                                 
       and Z.NSOCIETE is null                                                   
       )                                                                        
       --Prix Nationaux                                                         
       , GN59 as (                                                              
       select                                                                   
       GN59.NCODIC                                                              
       , GN59.PREFTTC                                                           
       , GN59.DEFFET                                                            
       from RVGN5901 as GN59                                                    
       where                                                                    
       GN59.CREF = '1'                                                          
       and GN59.DEFFET <= :W-DJOUR-SUIV                                         
       )                                                                        
       --Primes Nationales                                                      
       , GN75 as (                                                              
       select                                                                   
       GN75.NCODIC                                                              
       , GN75.PCOMMREF                                                          
       , GN75.DEFFET                                                            
       from RVGN7501 as GN75                                                    
       where                                                                    
       GN75.DEFFET <= :W-DJOUR-SUIV                                             
       )                                                                        
       --Fragments XML                                                          
       --Eans                                                                   
       , eans as (                                                              
       select                                                                   
       GA31.NCODIC                                                              
       , xmlagg(                                                                
       xmlelement(name "barcode"                                                
       , xmlnamespaces(default 'http://codif.darty.fr/bundle')                  
       , xmlelement(name "ean13", GA31.NEAN)                                    
       , xmlelement(name "active",                                              
       case GA31.WACTIF                                                         
       when 'O' then int(1)                                                     
       else int(0)                                                              
       end                                                                      
       )                                                                        
       , case                                                                   
       when GA31.DACTIF <= '' then                                              
       xmlelement(name "activd",                                                
       xmlattributes('true' as "xsi:nil"), '')                                  
       else                                                                     
       xmlelement(name "activd",                                                
       timestamp_format(GA31.DACTIF, 'YYYYMMDD'))                               
       end                                                                      
       )                                                                        
       ) as xbarcode                                                            
       from RVGA3100 as GA31                                                    
       group by                                                                 
       GA31.NCODIC                                                              
       )                                                                        
       --Modes de D�livrance                                                    
       , modes as (                                                             
       select                                                                   
       GA64.NCODIC                                                              
       , xmlagg(                                                                
       xmlelement(name "mode_NCG"                                               
       , xmlnamespaces(default 'http://codif.darty.fr/bundle')                  
       , rtrim(GA64.CMODDEL)                                                    
       )                                                                        
       ) as xmode_NCG                                                           
       from RVGA6400 as GA64                                                    
       group by                                                                 
       GA64.NCODIC                                                              
       )                                                                        
       --Attributes                                                             
       , attrs as (                                                             
       select                                                                   
       GA53.NCODIC                                                              
       , xmlagg(                                                                
       xmlelement(name "attr_NCG"                                               
       , xmlnamespaces(default 'http://codif.darty.fr/bundle')                  
       , xmlelement(name "code", rtrim(GA53.CDESCRIPTIF))                       
       , xmlelement(name "value", rtrim(GA53.CVDESCRIPT))                       
       )                                                                        
       ) as xattr_NCG                                                           
       from RVGA5300 as GA53                                                    
       group by                                                                 
       GA53.NCODIC                                                              
       )                                                                        
       --Destinations                                                           
       , dests as (                                                             
       select                                                                   
       GA33.NCODIC                                                              
       , xmlagg(                                                                
       xmlelement(name "saleabl"                                                
       , xmlnamespaces(default 'http://codif.darty.fr/bundle')                  
       --mapping country vers ISO-3166-1-2                                      
       , rtrim(                                                                 
       case when substr(GA01.WTABLEG, 21, 1) = 'N'                              
       then substr(GA01.WTABLEG, 22, 2)                                         
       else GA01.CTABLEG2 end)                                                  
       )                                                                        
       ) as xsaleabl                                                            
       from RVGA3300 as GA33                                                    
       --mapping country vers ISO-3166-1-2                                      
       inner join RVGA0100 as GA01 on                                           
       GA01.CTABLEG1 = 'ORIGP'                                                  
       and substr(GA01.CTABLEG2, 1, 5) = GA33.CDEST                             
       group by                                                                 
       GA33.NCODIC                                                              
       )                                                                        
       --Liens Group                                                            
       , liens as (                                                             
       select                                                                   
       GA58.NCODIC                                                              
       , xmlagg(                                                                
       xmlelement(name "lnkGrp_NCG"                                             
       , xmlnamespaces(default 'http://codif.darty.fr/bundle')                  
       , xmlelement(name "codicOpc", rtrim(GA58.NCODICLIE))                     
       , xmlelement(name "qty", int(GA58.QTYPLIEN))                             
       --INFO: logique invers�e dans NCG                                        
       , xmlelement(name "saleabl"                                              
       , case GA58.WDEGRELIB when 'LIBRE' then 0 else 1 end                     
       )                                                                        
       --INFO: non g�r�                                                         
       , xmlelement(name "major", 0)                                            
       )                                                                        
       ) as xgroup_NCG                                                          
       from RVGA5800 as GA58                                                    
       where                                                                    
       GA58.CTYPLIEN = 'GRP M'                                                  
       group by                                                                 
       GA58.NCODIC                                                              
       )                                                                        
       --Assortiment futur 1 seul, le premier dans le futur                     
       , GA65 as (                                                              
       select                                                                   
       GA65.NCODIC                                                              
       , GA65.LSTATUT                                                           
       , GA65.DEFFET                                                            
       from RVGA6500 as GA65                                                    
       left join RVGA6500 as Z on                                               
       Z.NCODIC = GA65.NCODIC                                                   
       and Z.CSTATUT = 'ASSOR'                                                  
       and Z.DEFFET < GA65.DEFFET                                               
       and Z.DEFFET > :W-DJOUR-SUIV                                             
       where                                                                    
       GA65.CSTATUT = 'ASSOR'                                                   
       and GA65.DEFFET > :W-DJOUR-SUIV                                          
       and Z.NCODIC IS NULL                                                     
       )                                                                        
       --Select                                                                 
       select                                                                   
       GA00.NCODIC                                                              
      *{ SQL-concatenation-operators 1.4                                        
      *, varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
      *  concat '_BXM101_' concat                                               
      *  lpad(cast(row_number() over() as varchar(6)), 6, '0')                  
      *--                                                                       
      *{ SQL-concatenation-operators 1.4                                        
      *, varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
      *  concat '_BXM101_' ||                                                   
      *--                                                                       
       , varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
         || '_BXM101_' ||                                                       
      *}                                                                        
         lpad(cast(row_number() over() as varchar(6)), 6, '0')                  
      *}                                                                        
       --Ent�te codif                                                           
       , xmlserialize (                                                         
       xmldocument(                                                             
       xmlelement(                                                              
       name "codif"                                                             
       , xmlnamespaces(default 'http://codif.darty.fr',                         
       'http://www.w3.org/2001/XMLSchema-instance' as "xsi")                    
       , xmlattributes(                                                         
       'http://codif.darty.fr http://codif.darty.fr/schemas/codif.xsd'          
       as "xsi:schemaLocation"                                                  
       , 'NCG' as "source"                                                      
       , varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
         concat '_BXM101_' concat                                               
         lpad(cast(row_number() over() as varchar(6)), 6, '0')                  
         as "reference"                                                         
       , current timestamp as "issued"                                          
       )                                                                        
       --                                                                       
       , xmlelement(name "product"                                              
       , xmlnamespaces(default 'http://codif.darty.fr/bundle'                   
       , 'http://www.w3.org/2001/XMLSchema-instance' as "xsi"                   
       )                                                                        
       , xmlattributes('907' as "context")                                      
       , xmlelement(name "codicInt", GA03.NCODICK)                              
       , xmlelement(name "lnkOpc"                                               
       , xmlelement(name "opco", '907')                                         
       , xmlelement(name "codicOpc", GA00.NCODIC)                               
       , (                                                                      
       select                                                                   
       xbarcode                                                                 
       from eans                                                                
       where                                                                    
       eans.NCODIC = GA00.NCODIC                                                
       )                                                                        
       , xmlelement(name "creatd",                                              
       timestamp_format(GA00.DCREATION, 'YYYYMMDD'))                            
       , xmlelement(name "modifd",                                              
       timestamp_format(GA00.DMAJ, 'YYYYMMDD'))                                 
       --Specifique NCG                                                         
       , xmlelement(name "specOpc"                                              
       , xmlelement(name "type_NCG", rtrim(GA14.CTYPENT))                       
       , xmlelement(name "ref_NCG", rtrim(GA00.LREFFOURN))                      
       , xmlelement(name "smmr_NCG", rtrim(GA00.LREFDARTY))                     
       , xmlelement(name "categ_NCG", rtrim(GA00.CFAM))                         
       , (                                                                      
       select                                                                   
       xattr_NCG                                                                
       from attrs                                                               
       where                                                                    
       attrs.NCODIC = GA00.NCODIC                                               
       )                                                                        
       , xmlelement(name "mngr_NCG", rtrim(GA00.CHEFPROD))                      
       , xmlelement(name "assor_NCG", rtrim(GA00.CASSORT))                      
       , (                                                                      
       select                                                                   
       xmlconcat(                                                               
       xmlelement(name "assorNxt_NCG", rtrim(GA65.LSTATUT))                     
       , xmlelement(name "assorNDt_NCG"                                         
       , timestamp_format(GA65.DEFFET, 'YYYYMMDD')                              
       )                                                                        
       )                                                                        
       from GA65                                                                
       where                                                                    
       GA65.NCODIC = GA00.NCODIC                                                
       )                                                                        
       --statDep_NCG pour produit non Dacem                                     
       , (                                                                      
       select                                                                   
       xmlagg(                                                                  
       xmlelement(name "statDep_NCG"                                            
       , xmlelement(name "whs", concat(FL50.NSOCDEPOT, FL50.NDEPOT))            
       , socs.xsocs                                                             
       , xmlelement(name "appro", rtrim(FL50.CAPPRO))                           
       , xmlelement(name "approDt"                                              
       , timestamp_format(value(FL65.DEFFET,'20010101'), 'YYYYMMDD')            
       )                                                                        
       , xmlelement(name "sensapp"                                              
       , case FL50.WSENSAPPRO when 'O' then 1 else 0 end                        
       )                                                                        
       , xmlelement(name "expo", rtrim(FL50.CEXPO))                             
       , xmlelement(name "comp", rtrim(FL50.LSTATCOMP))                         
       )                                                                        
       )                                                                        
       from RVGA0000 Z                                                          
       inner join RVFL5010 FL50 on                                              
       FL50.NCODIC = Z.NCODIC                                                   
       inner join (                                                             
       select                                                                   
       GQ05.CFAM                                                                
       , GQ05.NSOC                                                              
       , GQ05.NDEPOT                                                            
       , xmlagg(xmlelement(name "soc", rtrim(FL05.NSOCIETE))) as xsocs          
       from RVGQ0500 GQ05                                                       
       inner join FL05 on                                                       
       FL05.CPROAFF = GQ05.CPROAFF                                              
       where                                                                    
       GQ05.NPRIORITE = '1'                                                     
       group by                                                                 
       GQ05.CFAM, GQ05.NSOC, GQ05.NDEPOT                                        
       ) socs on                                                                
       socs.CFAM = Z.CFAM                                                       
       and socs.NSOC = FL50.NSOCDEPOT                                           
       and socs.NDEPOT = FL50.NDEPOT                                            
       left join RVFL6500 FL65 on                                               
       FL65.NCODIC = Z.NCODIC                                                   
       and FL65.NSOCDEPOT = FL50.NSOCDEPOT                                      
       and FL65.NDEPOT = FL50.NDEPOT                                            
       and FL65.CSTATUT = 'APPRO'                                               
       and FL65.LSTATUT = FL50.CAPPRO                                           
       left join RVFL6500 FL65Z on                                              
       FL65Z.NCODIC = FL65.NCODIC                                               
       and FL65Z.NSOCDEPOT = FL50.NSOCDEPOT                                     
       and FL65Z.NDEPOT = FL50.NDEPOT                                           
       and FL65Z.CSTATUT = 'APPRO'                                              
       and FL65Z.LSTATUT = FL65.LSTATUT                                         
       and FL65Z.DEFFET > FL65.DEFFET                                           
       where                                                                    
       Z.NCODIC = GA00.NCODIC                                                   
       and FL65Z.NCODIC is null                                                 
       )                                                                        
       --statDep_NCG pour produit Dacem                                         
       , (                                                                      
       select                                                                   
       xmlagg(                                                                  
       xmlelement(name "statDep_NCG"                                            
       , xmlelement(name "whs", concat(LI00.NSOCIETE, LI00.NLIEU))              
       , (                                                                      
       select                                                                   
       xmlagg(xmlelement(name "soc", rtrim(LI00.NSOCIETE)))                     
       from RVLI0000 LI00                                                       
       where                                                                    
       LI00.CPARAM = 'FIL'                                                      
       and LI00.LVPARAM = 'O'                                                   
       )                                                                        
       , xmlelement(name "appro", rtrim(Z.CAPPRO))                              
       , xmlelement(name "approDt"                                              
       , timestamp_format(value(GA65.DEFFET,'20010101'), 'YYYYMMDD')            
       )                                                                        
       , xmlelement(name "sensapp"                                              
       , case Z.WSENSAPPRO when 'O' then 1 else 0 end                           
       )                                                                        
       , xmlelement(name "expo", rtrim(Z.CEXPO))                                
       , xmlelement(name "comp", rtrim(Z.LSTATCOMP))                            
       )                                                                        
       )                                                                        
       from RVGA0000 Z                                                          
       -- inner join RVFL6000 FL60 on                                           
       -- FL60.NCODIC = Z.NCODIC                                                
       -- and FL60.FILIALE = '907'                                              
       inner join RVLI0000 LI00 on                                              
       LI00.CPARAM = 'DEPOT'                                                    
       and LI00.NSOCIETE = '996'                                                
       left join RVGA6500 GA65 on                                               
       GA65.NCODIC = Z.NCODIC                                                   
       and GA65.CSTATUT = 'APPRO'                                               
       and GA65.LSTATUT = Z.CAPPRO                                              
       left join RVGA6500 GA65Z on                                              
       GA65Z.NCODIC = GA65.NCODIC                                               
       and GA65Z.CSTATUT = 'APPRO'                                              
       and GA65Z.LSTATUT = GA65.LSTATUT                                         
       and GA65Z.DEFFET > GA65.DEFFET                                           
       where                                                                    
       Z.NCODIC = GA00.NCODIC                                                   
       and Z.WDACEM = 'O'                                                       
       and GA65Z.NCODIC is null                                                 
       )                                                                        
       , (                                                                      
       select                                                                   
       xmode_NCG                                                                
       from modes                                                               
       where                                                                    
       modes.NCODIC = GA00.NCODIC                                               
       )                                                                        
       --dlvrQuery_NCG: questionnaire de livraison sp�cifique                   
       --� la famille                                                           
       , case when substr(INCRD.LVPARAM, 7, 1) > ' ' then                       
       xmlelement(name "dlvrQuery_NCG",                                         
       rtrim(substr(INCRD.LVPARAM, 7, 1)))                                      
       end                                                                      
       , case when substr(INCRD.LVPARAM, 8, 1) > ' ' then                       
       xmlelement(name "dlvrQuery_NCG",                                         
       rtrim(substr(INCRD.LVPARAM, 8, 1)))                                      
       end                                                                      
       , case when substr(INCRD.LVPARAM, 9, 1) > ' ' then                       
       xmlelement(name "dlvrQuery_NCG",                                         
       rtrim(substr(INCRD.LVPARAM, 9, 1)))                                      
       end                                                                      
       , case when substr(INCRD.LVPARAM, 10, 1) > ' ' then                      
       xmlelement(name "dlvrQuery_NCG",                                         
       rtrim(substr(INCRD.LVPARAM, 10, 1)))                                     
       end                                                                      
       , case when substr(INCRD.LVPARAM, 11, 1) > ' ' then                      
       xmlelement(name "dlvrQuery_NCG",                                         
       rtrim(substr(INCRD.LVPARAM, 11, 1)))                                     
       end                                                                      
       , xmlelement(name "warrOpc_NCG", rtrim(GA00.CGARANTIE))                  
       --add MGD                                                                
       , xmlelement(name "warrOpc_MGD", rtrim(GA00.CGARANMGD))                  
       --fin MGD                                                                
       , xmlelement(name "warrMnf_NCG", rtrim(GA00.CGARCONST))                  
       , xmlelement(name "vat_NCG", rtrim(GA00.CTAUXTVA))                       
       , xmlelement(name "priceNat_NCG"                                         
       , value(                                                                 
       (                                                                        
       select                                                                   
       replace(rtrim(cast(GN59.PREFTTC as char(8))),',','.')                    
       from GN59                                                                
       where                                                                    
       GN59.NCODIC = GA00.NCODIC                                                
       order by                                                                 
       GN59.DEFFET desc                                                         
       fetch first 1 rows only                                                  
       )                                                                        
       , '0.00')                                                                
       )                                                                        
       , xmlelement(name "bonusNat_NCG"                                         
       , value(                                                                 
       (                                                                        
       select                                                                   
       replace(rtrim(cast(GN75.PCOMMREF as char(8))),',','.')                   
       from GN75                                                                
       where                                                                    
       GN75.NCODIC = GA00.NCODIC                                                
       order by                                                                 
       GN75.DEFFET desc                                                         
       fetch first 1 rows only                                                  
       )                                                                        
       , '0.00')                                                                
       )                                                                        
       , xmlelement(name "offerNat_NCG"                                         
       , case when GA00.WSENSVTE = 'O' then 1 else 0 end                        
       )                                                                        
       --INFO:non g�r�                                                          
       , xmlelement(name "valord_NCG", 1)                                       
       , (                                                                      
       select                                                                   
       xgroup_NCG                                                               
       from liens                                                               
       where                                                                    
       liens.NCODIC = GA00.NCODIC                                               
       )                                                                        
       )                                                                        
       )                                                                        
       , xmlelement(name "creatd",                                              
       timestamp_format(GA00.DCREATION, 'YYYYMMDD'))                            
       , xmlelement(name "opco", '907')                                         
       , xmlelement(name "modifd",                                              
       timestamp_format(GA00.DMAJ, 'YYYYMMDD'))                                 
       --INFO:non g�r�                                                          
       , xmlelement(name "userMod",                                             
       xmlattributes('true' as "xsi:nil"), '')                                  
       --INFO:non g�r�                                                          
       , xmlelement(name "nature", 'bundle')                                    
       , xmlelement(name "brand_NCG", rtrim(GA00.CMARQ))                        
       , xmlelement(name "refMnf", rtrim(GA00.LREFO))                           
       , xmlelement(name "lblMrkt"                                              
       , xmlattributes('fr' as "locale")                                        
       , rtrim(GA00.LREFFOURN)                                                  
       )                                                                        
       , xmlelement(name "refSale"                                              
       , xmlattributes('fr' as "locale")                                        
       , rtrim(GA00.LREFFOURN)                                                  
       )                                                                        
       , xmlelement(name "origin"                                               
       , rtrim(                                                                 
       case when substr(GA01.WTABLEG, 21, 1) = 'N'                              
       then substr(GA01.WTABLEG, 22, 2)                                         
       else GA01.CTABLEG2 end                                                   
       )                                                                        
       )                                                                        
       --INFO:non g�r�                                                          
       , xmlelement(name "categInt",                                            
       xmlattributes('true' as "xsi:nil"), '')                                  
       , (                                                                      
       select                                                                   
       xsaleabl                                                                 
       from dests                                                               
       where                                                                    
       dests.NCODIC = GA00.NCODIC                                               
       )                                                                        
       , xmlelement(name "tax"                                                  
       , xmlelement(name "country", 'FR')                                       
       , xmlelement(name "code", 'TVA')                                         
       )                                                                        
       --INFO:composd occurs: inconnu = 0                                       
       --INFO:dangerUN occurs: inconnu = 0                                      
       --INFO:launch occurs: inconnu = 0                                        
       , xmlelement(name "unitQty", max(int(GA00.QCONTENU), int(1)))            
       , xmlelement(name "freeQty", int(GA00.QGRATUITE))                        
       -- ) as XPRODUCT                                                         
       )                                                                        
       )) as clob(50K) including xmldeclaration) as XCODIF                      
       from RVGA0014 as GA00                                                    
       --limit� aux produits                                                    
       inner join RVGA1401 as GA14 on                                           
       GA14.CFAM = GA00.CFAM                                                    
       and GA14.CTYPENT = 'CG'                                                  
       inner join RVGA0300 as GA03 on                                           
       GA03.NCODIC = GA00.NCODIC                                                
       and GA03.CSOC = 'DAR'                                                    
       --mapping country vers ISO-3166-1-2                                      
       inner join RVGA0100 as GA01 on                                           
       GA01.CTABLEG1 = 'ORIGP'                                                  
       and substr(GA01.CTABLEG2, 1, 5) = GA00.CORIGPROD                         
       left join RVGA3000 as INCRD on                                           
       INCRD.CFAM = GA00.CFAM                                                   
       and INCRD.CPARAM = 'INCRD'                                               
       and substr(INCRD.LVPARAM, 7, 5) != ' '                                   
       --                                                                       
       inner join rvad0500 ad05 on                                              
        ga00.ncodic = ad05.ncodic                                               
       and ad05.copco = 'DAR'                                                   
       and ad05.cdata = 'BXM101-CG'                                             
       and ad05.ddata = :W-DJOUR                                                
           END-EXEC                                                             
                                                                                
