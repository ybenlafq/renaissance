      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*00000010
      *         COMMAREA POUR SOUS-PROGRAMME DE DECODAGE               *00000020
      *         ET AFFICHAGE DES ZONES NUMERIQUES                      *00000030
      *----------------------------------------------------------------*00000040
                                                                        00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-FNUM-LONG-COMMAREA PIC S9(4)   COMP VALUE +200.         00000060
      *                                                                         
      *--                                                                       
       01  COMM-FNUM-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +200.                 
                                                                        00000070
      *}                                                                        
       01  Z-COMMAREA-MFFNUM.                                           00000080
         02  FNUM-RECORD.                                               00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FNUM-LGIN           PIC S9(4)       COMP.                00000100
      *--                                                                       
           05  FNUM-LGIN           PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FNUM-LGOUT          PIC S9(4)       COMP.                00000110
      *--                                                                       
           05  FNUM-LGOUT          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FNUM-NBDEC          PIC S9(4)       COMP.                00000120
      *--                                                                       
           05  FNUM-NBDEC          PIC S9(4) COMP-5.                            
      *}                                                                        
           05  FNUM-TYPTRAIT       PIC X.                               00000130
           05  FNUM-CDRET          PIC X.                               00000140
               88  FNUM-OK                         VALUE '0'.           00000150
               88  FNUM-INVCAR                     VALUE '1'.           00000160
               88  FNUM-DBLEDECIM                  VALUE '2'.           00000170
               88  FNUM-TROPLONG                   VALUE '3'.           00000180
               88  FNUM-DBLESIGNE                  VALUE '4'.           00000190
               88  FNUM-TROPDECIM                  VALUE '5'.           00000200
               88  FNUM-PASNEGATIF                 VALUE '6'.           00000210
               88  FNUM-ERRIN                      VALUE 'P'.           00000220
           05  FNUM-TYPDECIMAL     PIC X      VALUE ','.                00000230
           05  FNUM-SIGNE          PIC X      VALUE ' '.                00000240
               88  FNUM-NOTMOINS                   VALUE 'N'.           00000250
           05  FNUM-ZEDIT          PIC X(18).                           00000260
           05  FNUM-ZNUM.                                               00000270
               10  FNUM-ZNUMN          PIC S9(18).                      00000280
           05  FNUM-MESS-ERREUR.                                        00000290
               10  FNUM-CODERREUR      PIC X(4).                        00000300
               10  FILLER              PIC X.                           00000310
               10  FNUM-LIBERREUR      PIC X(55).                       00000320
                                                                                
                                                                        00000330
