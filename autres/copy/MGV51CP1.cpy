      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *    Copie utilis�e pour MGV51, MGV52, MGV53                      00580008
      *                                                                 00590008
      *    Utilisation pour envoi Infos vers PLATF NSM                  00620008
       01  WNSM-TABLE.                                                  00610008
      *       Valeur de recherche                                       00590008
           05 W-NSM-NSOCVTE         PIC X(03).                          00610008
           05 W-NSM-NLIEUVTE        PIC X(03).                          00610008
           05 W-NSM-NSOC            PIC X(03).                          00610008
           05 W-NSM-NLIEU           PIC X(03).                          00610008
      *                                                                 00590008
      *       Zone de stockage                                          00590008
           05 WNSM-MAX              PIC 9(03) VALUE 50.                 00610008
           05 WNSM-ZONE.                                                00610008
            7 WNSM-NBP              PIC 9(03) VALUE 0.                  00610008
            7 WNSM-POSTES           OCCURS 50 INDEXED INSM.             00610008
PV0307        10 WNSM-NSOCD         PIC X(03).                                  
PV0307        10 WNSM-NLIEUD        PIC X(03).                                  
              10 WNSM-NSOC          PIC X(03).                                  
              10 WNSM-NLIEU         PIC X(03).                                  
      *                                                                 00590008
                                                                                
