      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TYPLM LIBELLE DES TYPES D OP           *        
      *----------------------------------------------------------------*        
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVTYPLM .                                                            
           05  TYPLM-CTABLEG2    PIC X(15).                                     
           05  TYPLM-CTABLEG2-REDEF REDEFINES TYPLM-CTABLEG2.                   
               10  TYPLM-TYPOP           PIC X(02).                             
           05  TYPLM-WTABLEG     PIC X(80).                                     
           05  TYPLM-WTABLEG-REDEF  REDEFINES TYPLM-WTABLEG.                    
               10  TYPLM-LTYPOP          PIC X(40).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTYPLM-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TYPLM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TYPLM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TYPLM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TYPLM-WTABLEG-F   PIC S9(4) COMP-5.                              
           EXEC SQL END DECLARE SECTION END-EXEC.
                                                                                
      *}                                                                        
