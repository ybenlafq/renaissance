      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * ZONES PASSES EN PARAMETRE POUR LE MAN000                                
       01 WKMAN000.                                                             
           05 WKMAN000-ENTREE.                                                  
               10 WKMAN000-CNOMPGRM PIC X(6).                                   
               10 WKMAN000-NSEQERR PIC X(4).                                    
               10 WKMAN000-LLIBRE PIC X(50).                                    
           05 WKMAN000-SORTIE.                                                  
               10 WKMAN000-CRET PIC X(4).                                       
               10 WKMAN000-LRET PIC X(41).                                      
           05 WKMAN000-MSG.                                                     
               10 WKMAN000-ENRID PIC X(24).                                     
                                                                                
