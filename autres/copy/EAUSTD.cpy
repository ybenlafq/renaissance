      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 16:58 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREPOT        *
      * FICHIER.....: EMISSION ARTICLE UNITE LOGISTIQUE STANDARD      *
      * NOM FICHIER.: EAUSTD                                          *
      *---------------------------------------------------------------*
      * CR   .......: 29/02/2012                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR0...: 302                                             *
      *****************************************************************
      *
       01  EAUSTD.
      * TYPE ENREG :RECEPTION ARTICLE UNITE LOGISTIQUE STANDARD
           05      EAUSTD-TYP-ENREG       PIC  X(0006).
      * CODE ETAT
           05      EAUSTD-CETAT           PIC  X(0001).
      * CODE SOCIETE
           05      EAUSTD-CSOCIETE        PIC  X(0005).
      * CODE ARTICLE
           05      EAUSTD-CARTICLE        PIC  X(0018).
      * CODE UNITE LOGISTIQUE
           05      EAUSTD-CODE-UL         PIC  X(0005).
      * TYPE UNITE LOGISTIQUE
           05      EAUSTD-TYPE-UL         PIC  9(0003).
      * CODE UNITE LOGISTIQUE FILLE
           05      EAUSTD-CODE-UL-FILLE   PIC  X(0005).
      * LIBELLE
           05      EAUSTD-LIB-UL          PIC  X(0035).
      * QUANTITE
           05      EAUSTD-QTE             PIC  9(0009).
      * COEFFICIENT CONVERSION QUANTITE POUR AFFICHAGE
           05      EAUSTD-COEF-CONV-QTE-A PIC  X(0020).
      * LIBELLE AFFICHAGE QUANTITE
           05      EAUSTD-LAFF-QTE        PIC  X(0010).
      * LONGUEUR
           05      EAUSTD-LONGUEUR        PIC  9(0009).
      * LARGEUR
           05      EAUSTD-LARGEUR         PIC  9(0009).
      * HAUTEUR
           05      EAUSTD-HAUTEUR         PIC  9(0009).
      * POIDS
           05      EAUSTD-POIDS           PIC  9(0009).
      * ROTATION
           05      EAUSTD-ROTATION        PIC  X(0001).
      * CODE-A-BARRES
           05      EAUSTD-CODE-BARRES     PIC  X(0020).
      * DISPONIBLE
           05      EAUSTD-DISPONIBLE      PIC  9(0001).
      * CODE FOURNISSEUR PRINCIPAL
           05      EAUSTD-CFOURNISSEUR-PPAL PIC X(015).
      * NOM FOURNISSEUR PRINCIPAL
           05      EAUSTD-NOM-FOURN-PPAL  PIC  X(0035).
      * COMPACTAGE RECEPTION (AUTORISATION)
           05      EAUSTD-COMPACT-RECEPT  PIC  9(0001).
      * NOMBRE DE COUCHES SUR LE SUPPORT
           05      EAUSTD-NB-COUCHES-SUP  PIC  9(0009).
      * COEFFICIENT DE FOISONNEMENT
           05      EAUSTD-COEF-FOISONNE   PIC  9(0001)V9(2).
      * COEFFICIENT DE VIDE
           05      EAUSTD-COEF-VIDE       PIC  9(0001)V9(2).
      * CARACTERISTIQUE 1
           05      EAUSTD-CARACT1         PIC  X(0020).
      * CARACTERISTIQUE 2
           05      EAUSTD-CARACT2         PIC  X(0020).
      * CARACTERISTIQUE 3
           05      EAUSTD-CARACT3         PIC  X(0020).
      * FILLER
           05      EAUSTD-FILLER          PIC  X(0001).
      
