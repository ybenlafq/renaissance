      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EXEC SQL                                                             
       declare CO cursor for                                                    
       --codif_product                                                          
       with                                                                     
       --Taxes en cours et futures (hors D3E)                                   
       GA82 as (                                                                
       select                                                                   
       GA82.NCODIC                                                              
       , GA82.CTAXE                                                             
       , GA82.NENTCDE                                                           
       , GA82.CPAYS                                                             
       , GA82.WIMPORT                                                           
       , value(GA82.PMONTANT, dec(0, 2, 1)) as TAX                              
       , GA82.CPRODECO                                                          
       , GA82.DEFFET                                                            
       , GA82.DFINEFFET                                                         
       , substr(GA01.CTABLEG2, 12, 3) as collectr                               
       from RVGA8200 as GA82                                                    
       left join RVGA0100 GA01 on                                               
       GA01.CTABLEG1 = 'XCTRL'                                                  
       and GA01.CTABLEG2 like '00000TGA79%'                                     
       and substr(GA01.WTABLEG, 1, 1) = 'O'                                     
       and substr(GA01.WTABLEG, 2, 2) = GA82.CPAYS                              
       and substr(GA01.WTABLEG, 4, 3) = GA82.CTAXE                              
       where                                                                    
       GA82.DFINEFFET > :W-DJOUR-SUIV                                           
       )                                                                        
       --Profil d'affectation en cours des soci�t�s                             
       , FL05 as (                                                              
       select                                                                   
       FL05.NSOCIETE                                                            
       , FL05.NLIEU                                                             
       , FL05.CPROAFF                                                           
       from RVFL0500 FL05                                                       
       inner join RVLI0000 LI00 on                                              
       LI00.CPARAM = 'FIL'                                                      
       and LI00.LVPARAM = 'O'                                                   
       and LI00.NSOCIETE = FL05.NSOCIETE                                        
       and LI00.NLIEU = FL05.NLIEU                                              
       left join RVFL0500 Z on                                                  
       Z.CTYPTRAIT = 'GA50M'                                                    
       and Z.DEFFET > FL05.DEFFET                                               
       and Z.NSOCIETE = FL05.NSOCIETE                                           
       and Z.NLIEU = FL05.NLIEU                                                 
       where                                                                    
       FL05.CTYPTRAIT = 'GA50M'                                                 
       and Z.NSOCIETE is null                                                   
       )                                                                        
       --Prix Nationaux                                                         
       , GN59 as (                                                              
       select                                                                   
       GN59.NCODIC                                                              
       , GN59.PREFTTC                                                           
       , GN59.DEFFET                                                            
       from RVGN5901 as GN59                                                    
       where                                                                    
       GN59.CREF = '1'                                                          
       and GN59.DEFFET <= :W-DJOUR-SUIV                                         
       )                                                                        
       --Primes Nationales                                                      
       , GN75 as (                                                              
       select                                                                   
       GN75.NCODIC                                                              
       , GN75.PCOMMREF                                                          
       , GN75.DEFFET                                                            
       from RVGN7501 as GN75                                                    
       where                                                                    
       GN75.DEFFET <= :W-DJOUR-SUIV                                             
       )                                                                        
       --Assortiments futurs                                                    
       , GA65 as (                                                              
       select                                                                   
       GA65.NCODIC                                                              
       , GA65.LSTATUT                                                           
       , GA65.DEFFET                                                            
       from RVGA6500 as GA65                                                    
       where                                                                    
       GA65.CSTATUT = 'ASSOR'                                                   
       and GA65.DEFFET > :W-DJOUR-SUIV                                          
       )                                                                        
       --Fragments XML                                                          
       --Eans                                                                   
       , eans as (                                                              
       select                                                                   
       GA31.NCODIC                                                              
       , xmlagg(                                                                
       xmlelement(name "barcode"                                                
       , xmlnamespaces(default 'http://codif.darty.fr/product')                 
       , xmlelement(name "ean13", GA31.NEAN)                                    
       , xmlelement(name "active",                                              
       case GA31.WACTIF                                                         
       when 'O' then int(1)                                                     
       else int(0)                                                              
       end                                                                      
       )                                                                        
       , case                                                                   
       when GA31.DACTIF <= '' then                                              
       xmlelement(name "activd",                                                
       xmlattributes('true' as "xsi:nil"), '')                                  
       else                                                                     
       xmlelement(name "activd",                                                
       timestamp_format(GA31.DACTIF, 'YYYYMMDD'))                               
       end                                                                      
       )                                                                        
       ) as xbarcode                                                            
       from RVGA3100 as GA31                                                    
       group by                                                                 
       GA31.NCODIC                                                              
       )                                                                        
       --Caract�ristiques sp�cifiques                                           
       , specs as (                                                             
       select                                                                   
       GA63.NCODIC                                                              
       , xmlagg(                                                                
       xmlelement(name "spec_NCG"                                               
       , xmlnamespaces(default 'http://codif.darty.fr/product')                 
       , rtrim(GA63.CARACTSPE)                                                  
       )                                                                        
       ) as xspec_NCG                                                           
       from RVGA6300 as GA63                                                    
       group by                                                                 
       GA63.NCODIC                                                              
       )                                                                        
       --Liens Substitution (REMPL)                                             
       , lnkSub as (                                                            
       select                                                                   
       GA58.NCODIC                                                              
       , xmlagg(                                                                
       xmlelement(name "lnkSub"                                                 
       , xmlnamespaces(default 'http://codif.darty.fr/product')                 
       , xmlelement(name "codicOpc", rtrim(GA58.NCODICLIE))                     
       --INFO: non g�r�                                                         
       , xmlelement(name "type", 'definitive')                                  
       , xmlelement(name "modus", 'outofstock')                                 
       , xmlelement(name "from",                                                
       timestamp_format('20010101', 'YYYYMMDD'))                                
       --INFO: non g�r� <to> optionnel                                          
       )                                                                        
       ) as xlnkSub                                                             
       from RVGA5800 as GA58                                                    
       where                                                                    
       GA58.CTYPLIEN = 'REMPL'                                                  
       group by                                                                 
       GA58.NCODIC                                                              
       )                                                                        
       --Liens Substitution (REMPL)                                             
       , lnkSim as (                                                            
       select                                                                   
       GA58.NCODIC                                                              
       , xmlagg(                                                                
       xmlelement(name "lnkSim"                                                 
       , xmlnamespaces(default 'http://codif.darty.fr/product')                 
       , xmlelement(name "codicOpc", rtrim(GA58.NCODICLIE))                     
       )                                                                        
       ) as xlnkSim                                                             
       from RVGA5800 as GA58                                                    
       where                                                                    
       GA58.CTYPLIEN = 'S'                                                      
       group by                                                                 
       GA58.NCODIC                                                              
       )                                                                        
       --D�clarations                                                           
       , dclrs as (                                                             
       select                                                                   
       GA51.NCODIC                                                              
       , xmlagg(                                                                
       xmlelement(name "dclr_NCG"                                               
       , xmlnamespaces(default 'http://codif.darty.fr/product')                 
       , rtrim(GA51.CTYPDCL)                                                    
       )                                                                        
       ) as xdclr_NCG                                                           
       from RVGA5100 as GA51                                                    
       group by                                                                 
       GA51.NCODIC                                                              
       )                                                                        
       --Modes de D�livrance                                                    
       , modes as (                                                             
       select                                                                   
       GA64.NCODIC                                                              
       , xmlagg(                                                                
       xmlelement(name "mode_NCG"                                               
       , xmlnamespaces(default 'http://codif.darty.fr/product')                 
       , rtrim(GA64.CMODDEL)                                                    
       )                                                                        
       ) as xmode_NCG                                                           
       from RVGA6400 as GA64                                                    
       group by                                                                 
       GA64.NCODIC                                                              
       )                                                                        
       --Attributes                                                             
       , attrs as (                                                             
       select                                                                   
       GA53.NCODIC                                                              
       , xmlagg(                                                                
       xmlelement(name "attr_NCG"                                               
       , xmlnamespaces(default 'http://codif.darty.fr/product')                 
       , xmlelement(name "code", rtrim(GA53.CDESCRIPTIF))                       
       --TODO: trim pour valeur num                                             
       , xmlelement(name "value", rtrim(GA53.CVDESCRIPT))                       
       )                                                                        
       ) as xattr_NCG                                                           
       from RVGA5300 as GA53                                                    
       group by                                                                 
       GA53.NCODIC                                                              
       )                                                                        
       --Destinations                                                           
       , dests as (                                                             
       select                                                                   
       GA33.NCODIC                                                              
       , xmlagg(                                                                
       xmlelement(name "saleabl"                                                
       , xmlnamespaces(default 'http://codif.darty.fr/product')                 
       --mapping country vers ISO-3166-1-2                                      
       , rtrim(                                                                 
       case when substr(GA01.WTABLEG, 21, 1) = 'N'                              
       then substr(GA01.WTABLEG, 22, 2)                                         
       else GA01.CTABLEG2 end)                                                  
       )                                                                        
       ) as xsaleabl                                                            
       from RVGA3300 as GA33                                                    
       --mapping country vers ISO-3166-1-2                                      
       inner join RVGA0100 as GA01 on                                           
       GA01.CTABLEG1 = 'ORIGP'                                                  
       and substr(GA01.CTABLEG2, 1, 5) = GA33.CDEST                             
       group by                                                                 
       GA33.NCODIC                                                              
       )                                                                        
       --Dossiers                                                               
       , fileNcg as (                                                           
       select                                                                   
       BS25.NENTITE                                                             
       , xmlagg(                                                                
       xmlelement(name "file_NCG"                                               
       , xmlnamespaces(default 'http://codif.darty.fr/product')                 
       , xmlelement(name "code", rtrim(BS25.CDOSSIER))                          
       , xmlelement(name "seq", int(BS25.NSEQDOS))                              
       , xmlelement(name "from",                                                
       timestamp_format(BS25.DEFFET, 'YYYYMMDD'))                               
       , case when BS25.DFINEFFET = '99999999' then null                        
       else xmlelement(name "to",                                               
       timestamp_format(BS25.DFINEFFET, 'YYYYMMDD'))                            
       end                                                                      
       )                                                                        
       ) as xfile_NCG                                                           
       from RVBS2500 as BS25                                                    
       group by                                                                 
       BS25.NENTITE                                                             
       )                                                                        
       --Select                                                                 
       select                                                                   
       GA00.NCODIC                                                              
       --, char(current timestamp) concat '_XM101'                              
      *{ SQL-concatenation-operators 1.4                                        
      *, varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
      *  concat '_BXM101_' concat                                               
      *  lpad(cast(row_number() over() as varchar(6)), 6, '0')                  
      *--                                                                       
      *{ SQL-concatenation-operators 1.4                                        
      *, varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
      *  concat '_BXM101_' ||                                                   
      *--                                                                       
       , varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
         || '_BXM101_' ||                                                       
      *}                                                                        
         lpad(cast(row_number() over() as varchar(6)), 6, '0')                  
      *}                                                                        
       --Ent�te codif                                                           
       , xmlserialize (                                                         
       xmldocument(                                                             
       xmlelement(                                                              
       name "codif"                                                             
       , xmlnamespaces(default 'http://codif.darty.fr',                         
       'http://www.w3.org/2001/XMLSchema-instance' as "xsi")                    
       , xmlattributes(                                                         
       'http://codif.darty.fr http://codif.darty.fr/schemas/codif.xsd'          
       as "xsi:schemaLocation"                                                  
       , 'NCG' as "source"                                                      
       --, char(current timestamp) concat '_XM101' as "reference"               
         , varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')              
           concat '_BXM101_' concat                                             
           lpad(cast(row_number() over() as varchar(6)), 6, '0')                
           as "reference"                                                       
       , current timestamp as "issued"                                          
       )                                                                        
       --                                                                       
       , xmlelement(name "product"                                              
       , xmlnamespaces(default 'http://codif.darty.fr/product'                  
       , 'http://www.w3.org/2001/XMLSchema-instance' as "xsi"                   
       )                                                                        
       , xmlattributes('907' as "context")                                      
       , xmlelement(name "codicInt", GA03.NCODICK)                              
       , xmlelement(name "lnkOpc"                                               
       , xmlelement(name "opco", '907')                                         
       , xmlelement(name "codicOpc", GA00.NCODIC)                               
       , (                                                                      
       select                                                                   
       xbarcode                                                                 
       from eans                                                                
       where                                                                    
       eans.NCODIC = GA00.NCODIC                                                
       )                                                                        
       , xmlelement(name "creatd",                                              
       timestamp_format(GA00.DCREATION, 'YYYYMMDD'))                            
       , xmlelement(name "modifd",                                              
       timestamp_format(GA00.DMAJ, 'YYYYMMDD'))                                 
       --Specifique NCG                                                         
       , xmlelement(name "specOpc"                                              
       , xmlelement(name "type_NCG", rtrim(GA14.CTYPENT))                       
       , xmlelement(name "ref_NCG", rtrim(GA00.LREFFOURN))                      
       , xmlelement(name "smmr_NCG", rtrim(GA00.LREFDARTY))                     
       , xmlelement(name "categ_NCG", rtrim(GA00.CFAM))                         
       , xmlelement(name "replenGrp"                                            
       , case                                                                   
       when GA00.WDACEM = 'O' then 'NRMGR'                                      
       when FISHM.NCODIC is not null then 'FISHM'                               
       else 'LOCAL'                                                             
       end                                                                      
       )                                                                        
       , (                                                                      
       select                                                                   
       xattr_NCG                                                                
       from attrs                                                               
       where                                                                    
       attrs.NCODIC = GA00.NCODIC                                               
       )                                                                        
       , xmlelement(name "mngr_NCG", rtrim(GA00.CHEFPROD))                      
       , (                                                                      
       select                                                                   
       xfile_NCG                                                                
       from fileNcg                                                             
       where                                                                    
       fileNcg.NENTITE = GA00.NCODIC                                            
       )                                                                        
       --supplr_NCG                                                             
       , (                                                                      
       select                                                                   
       xmlagg(supplr.xsupplr)                                                   
       from                                                                     
       (                                                                        
       select                                                                   
       xmlelement(name "supplr_NCG"                                             
       , xmlelement(name "code", rtrim(GA55.NENTCDE))                           
       , xmlelement(name "main",                                                
       case GA55.WENTCDE when 'R' then 1 else 0 end                             
       )                                                                        
       , xmlelement(name "lead"                                                 
       , case when GA00.QDELAIAPPRO <= ''                                       
       then 0                                                                   
       else int(GA00.QDELAIAPPRO)                                               
       end                                                                      
       )                                                                        
       , (                                                                      
       select                                                                   
       xmlagg(                                                                  
       xmlelement(name "lnkTax"                                                 
       , xmlelement(name "country", rtrim(GA38.CPAYS))                          
       , xmlelement(name "tax", 'D3E')                                          
       , xmlelement(name "import", rtrim(GA38.WIMPORTE))                        
       , case                                                                   
       when (GA38.CPAYS = 'FR' and GA06.CECO > '') then                         
       xmlelement(name "collectr", rtrim(GA06.CECO))                            
       else null                                                                
       end                                                                      
       , xmlelement(name "value"                                                
       , replace(rtrim(cast(                                                    
       value(GA38.PMONTANT, dec(0, 2, 1)) as char(10))),',','.')                
       )                                                                        
       , xmlelement(name "from"                                                 
       , timestamp_format(GA38.DEFFET, 'YYYYMMDD')                              
       )                                                                        
       )                                                                        
       )                                                                        
       --D3E actuelle et futures                                                
       from RVGA3800 GA38                                                       
       inner join RVGA0604 GA06 on                                              
       GA06.NENTCDE = GA38.NENTCDE                                              
       left join RVGA3800 Z on                                                  
       Z.NCODIC = GA38.NCODIC                                                   
       and Z.NENTCDE = GA38.NENTCDE                                             
       and Z.WENTCDE in ('R', 'O')                                              
       and Z.WACHVEN = 'A'                                                      
       and Z.CPAYS = GA38.CPAYS                                                 
       and Z.DEFFET <= :W-DJOUR-SUIV                                            
       and Z.DEFFET > GA38.DEFFET                                               
       where                                                                    
       GA38.NCODIC = GA00.NCODIC                                                
       and GA38.NENTCDE = GA55.NENTCDE                                          
       and GA38.WENTCDE in ('R', 'O')                                           
       and GA38.WACHVEN = 'A'                                                   
       and (GA38.DEFFET <= :W-DJOUR-SUIV)                                       
       and Z.NCODIC is null                                                     
       )                                                                        
       , (                                                                      
       select                                                                   
       xmlagg(                                                                  
       xmlelement(name "lnkTax"                                                 
       , xmlelement(name "country", rtrim(GA82.CPAYS))                          
       , xmlelement(name "tax", rtrim(GA82.CTAXE))                              
       , xmlelement(name "import", rtrim(GA82.WIMPORT))                         
       , case                                                                   
       when (GA82.CPRODECO > '' and GA82.collectr is not null) then             
       xmlconcat(                                                               
       xmlelement(name "collectr", rtrim(GA82.collectr))                        
       , xmlelement(name "class", rtrim(GA82.CPRODECO))                         
       --INFO: non g�r�                                                         
       , xmlelement(name "tariff", 'A')                                         
       )                                                                        
       else null                                                                
       end                                                                      
       , xmlelement(name "value"                                                
       , replace(rtrim(cast(GA82.TAX as char(10))),',','.')                     
       )                                                                        
       , xmlelement(name "from",                                                
       timestamp_format(GA82.DEFFET, 'YYYYMMDD'))                               
       , case                                                                   
       when (GA82.DFINEFFET = '99999999') then null                             
       else                                                                     
       xmlelement(name "to",                                                    
       timestamp_format(GA82.DFINEFFET, 'YYYYMMDD'))                            
       end                                                                      
       )                                                                        
       )                                                                        
       from GA82                                                                
       where                                                                    
       GA82.NCODIC = GA00.NCODIC                                                
       and GA82.NENTCDE = GA55.NENTCDE                                          
       )                                                                        
       ) as xsupplr                                                             
       from RVGA5500 GA55                                                       
       where                                                                    
       GA55.NCODIC = GA00.NCODIC                                                
       ) supplr                                                                 
       )                                                                        
       --INFO:Donn�es camion et r�ception d�p�t principal 907                   
       --INFO: sur Fournisseur r�gulier (CR Darty-2730) pour produit            
       --avec FL50 (non Dacem)                                                  
       , (                                                                      
       select                                                                   
       xmlconcat(                                                               
       xmlelement(name "unitPTruck"                                             
       , xmlelement(name "supplr_NCG", rtrim(GA55.NENTCDE))                     
       , xmlelement(name "qty", int(FL50.QPRODCAM))                             
       )                                                                        
       , xmlelement(name "unitPRec"                                             
       , xmlelement(name "supplr_NCG", rtrim(GA55.NENTCDE))                     
       , xmlelement(name "qty", int(FL50.QCOLIRECEPT))                          
       )                                                                        
       --Themis                                                                 
       , xmlelement(name "unitPEco"                                             
       , xmlelement(name "supplr_NCG", rtrim(GA55.NENTCDE))                     
       , xmlelement(name "qty", int(FL50.QECO))                                 
       )                                                                        
       -- deb MGD                                                               
       , xmlelement(name "unitPCol"                                             
       , xmlelement(name "supplr_NCG", rtrim(GA55.NENTCDE))                     
       , xmlelement(name "qty", int(FL50.QNBCOL))                               
        )                                                                       
       -- fin MGD                                                               
       )                                                                        
       from RVFL5013 FL50                                                       
       inner join RVGQ0500 GQ05 on                                              
       GQ05.CFAM = GA00.CFAM                                                    
       and GQ05.NPRIORITE = '1'                                                 
       and GQ05.NSOC = FL50.NSOCDEPOT                                           
       and GQ05.NDEPOT = FL50.NDEPOT                                            
       inner join FL05 on                                                       
       FL05.CPROAFF = GQ05.CPROAFF                                              
       and FL05.NSOCIETE = '907'                                                
       and FL05.NLIEU = '000'                                                   
       inner join RVGA5500 GA55 on                                              
       GA55.NCODIC = GA00.NCODIC                                                
       and GA55.WENTCDE = 'R'                                                   
       where                                                                    
       FL50.NCODIC = GA00.NCODIC                                                
       )                                                                        
       --INFO: sur Fournisseur r�gulier (CR Darty-2730) produit sans            
       --FL50 (Dacem)                                                           
       , (                                                                      
       select                                                                   
       xmlelement(name "unitPRec"                                               
       , xmlelement(name "supplr_NCG", rtrim(GA55.NENTCDE))                     
       , xmlelement(name "qty", int(GA00.QCOLIRECEPT))                          
       )                                                                        
       from RVGA5500 GA55                                                       
       where                                                                    
       GA00.WDACEM = 'O'                                                        
       and GA55.NCODIC = GA00.NCODIC                                            
       and GA55.WENTCDE = 'R'                                                   
       )                                                                        
       , xmlelement(name "assor_NCG", rtrim(GA00.CASSORT))                      
       , (                                                                      
       select                                                                   
       xmlconcat(                                                               
       xmlelement(name "assorNxt_NCG", rtrim(GA65.LSTATUT))                     
       , xmlelement(name "assorNDt_NCG"                                         
       , timestamp_format(GA65.DEFFET, 'YYYYMMDD')                              
       )                                                                        
       )                                                                        
       from GA65                                                                
       where                                                                    
       GA65.NCODIC = GA00.NCODIC                                                
       order by                                                                 
       GA65.DEFFET                                                              
       fetch first 1 rows only                                                  
       )                                                                        
       --statDep_NCG pour produit non Dacem                                     
       , (                                                                      
       select                                                                   
       xmlagg(                                                                  
       xmlelement(name "statDep_NCG"                                            
       , xmlelement(name "whs", concat(FL50.NSOCDEPOT, FL50.NDEPOT))            
       , socs.xsocs                                                             
       , xmlelement(name "appro", rtrim(FL50.CAPPRO))                           
       , xmlelement(name "approDt"                                              
       , timestamp_format(                                                      
       value(FL65.DEFFET,'20010101'), 'YYYYMMDD')                               
       )                                                                        
       , xmlelement(name "sensapp"                                              
       , case FL50.WSENSAPPRO when 'O' then 1 else 0 end                        
       )                                                                        
       , xmlelement(name "expo", rtrim(FL50.CEXPO))                             
       , xmlelement(name "comp", rtrim(FL50.LSTATCOMP))                         
       )                                                                        
       )                                                                        
       from RVGA0000 Z                                                          
       inner join RVFL5012 FL50 on                                              
       FL50.NCODIC = Z.NCODIC                                                   
       inner join (                                                             
       select                                                                   
       GQ05.CFAM                                                                
       , GQ05.NSOC                                                              
       , GQ05.NDEPOT                                                            
       , xmlagg(xmlelement(name "soc", rtrim(FL05.NSOCIETE))) as xsocs          
       from RVGQ0500 GQ05                                                       
       inner join FL05 on                                                       
       FL05.CPROAFF = GQ05.CPROAFF                                              
       where                                                                    
       GQ05.NPRIORITE = '1'                                                     
       group by                                                                 
       GQ05.CFAM, GQ05.NSOC, GQ05.NDEPOT                                        
       ) socs on                                                                
       socs.CFAM = Z.CFAM                                                       
       and socs.NSOC = FL50.NSOCDEPOT                                           
       and socs.NDEPOT = FL50.NDEPOT                                            
       left join RVFL6500 FL65 on                                               
       FL65.NCODIC = Z.NCODIC                                                   
       and FL65.NSOCDEPOT = FL50.NSOCDEPOT                                      
       and FL65.NDEPOT = FL50.NDEPOT                                            
       and FL65.CSTATUT = 'APPRO'                                               
       and FL65.LSTATUT = FL50.CAPPRO                                           
       left join RVFL6500 FL65Z on                                              
       FL65Z.NCODIC = FL65.NCODIC                                               
       and FL65Z.NSOCDEPOT = FL50.NSOCDEPOT                                     
       and FL65Z.NDEPOT = FL50.NDEPOT                                           
       and FL65Z.CSTATUT = 'APPRO'                                              
       and FL65Z.LSTATUT = FL65.LSTATUT                                         
       and FL65Z.DEFFET > FL65.DEFFET                                           
       where                                                                    
       Z.NCODIC = GA00.NCODIC                                                   
       and FL65Z.NCODIC is null                                                 
       )                                                                        
       --statDep_NCG pour produit Dacem                                         
       , (                                                                      
       select                                                                   
       xmlagg(                                                                  
       xmlelement(name "statDep_NCG"                                            
       , xmlelement(name "whs", concat(LI00.NSOCIETE, LI00.NLIEU))              
       , (                                                                      
       select                                                                   
       xmlagg(xmlelement(name "soc", rtrim(LI00.NSOCIETE)))                     
       from RVLI0000 LI00                                                       
       where                                                                    
       LI00.CPARAM = 'FIL'                                                      
       and LI00.LVPARAM = 'O'                                                   
       )                                                                        
       , xmlelement(name "appro", rtrim(Z.CAPPRO))                              
       , xmlelement(name "approDt"                                              
       , timestamp_format(                                                      
       value(GA65.DEFFET,'20010101'), 'YYYYMMDD')                               
       )                                                                        
       , xmlelement(name "sensapp"                                              
       , case Z.WSENSAPPRO when 'O' then 1 else 0 end                           
       )                                                                        
       , xmlelement(name "expo", rtrim(Z.CEXPO))                                
       , xmlelement(name "comp", rtrim(Z.LSTATCOMP))                            
       )                                                                        
       )                                                                        
       from RVGA0000 Z                                                          
       -- inner join RVFL6000 FL60 on                                           
       -- FL60.NCODIC = Z.NCODIC                                                
       -- and FL60.FILIALE = '907'                                              
       inner join RVLI0000 LI00 on                                              
       LI00.CPARAM = 'DEPOT'                                                    
       and LI00.NSOCIETE = '996'                                                
       left join RVGA6500 GA65 on                                               
       GA65.NCODIC = Z.NCODIC                                                   
       and GA65.CSTATUT = 'APPRO'                                               
       and GA65.LSTATUT = Z.CAPPRO                                              
       left join RVGA6500 GA65Z on                                              
       GA65Z.NCODIC = GA65.NCODIC                                               
       and GA65Z.CSTATUT = 'APPRO'                                              
       and GA65Z.LSTATUT = GA65.LSTATUT                                         
       and GA65Z.DEFFET > GA65.DEFFET                                           
       where                                                                    
       Z.NCODIC = GA00.NCODIC                                                   
       and Z.WDACEM = 'O'                                                       
       and GA65Z.NCODIC is null                                                 
       )                                                                        
       --receivd_NCG: nillable                                                  
       , case                                                                   
       when GA00.D1RECEPT = '' then                                             
       xmlelement(name "receivd_NCG"                                            
       , xmlattributes('true' as "xsi:nil"), ''                                 
       )                                                                        
       else                                                                     
       xmlelement(name "receivd_NCG",                                           
       timestamp_format(GA00.D1RECEPT, 'YYYYMMDD'))                             
       end                                                                      
       , (                                                                      
       select                                                                   
       xdclr_NCG                                                                
       from dclrs                                                               
       where                                                                    
       dclrs.NCODIC = GA00.NCODIC                                               
       )                                                                        
       , (                                                                      
       select                                                                   
       xspec_NCG                                                                
       from specs                                                               
       where                                                                    
       specs.NCODIC = GA00.NCODIC                                               
       )                                                                        
       , (                                                                      
       select                                                                   
       xmode_NCG                                                                
       from modes                                                               
       where                                                                    
       modes.NCODIC = GA00.NCODIC                                               
       )                                                                        
       --dlvrQuery_NCG: questionnaire de                                        
       --livraison sp�cifique � la famille                                      
       , case when substr(INCRD.LVPARAM, 7, 1) > ' ' then                       
       xmlelement(name "dlvrQuery_NCG",                                         
       rtrim(substr(INCRD.LVPARAM, 7, 1)))                                      
       end                                                                      
       , case when substr(INCRD.LVPARAM, 8, 1) > ' ' then                       
       xmlelement(name "dlvrQuery_NCG",                                         
       rtrim(substr(INCRD.LVPARAM, 8, 1)))                                      
       end                                                                      
       , case when substr(INCRD.LVPARAM, 9, 1) > ' ' then                       
       xmlelement(name "dlvrQuery_NCG",                                         
       rtrim(substr(INCRD.LVPARAM, 9, 1)))                                      
       end                                                                      
       , case when substr(INCRD.LVPARAM, 10, 1) > ' ' then                      
       xmlelement(name "dlvrQuery_NCG",                                         
       rtrim(substr(INCRD.LVPARAM, 10, 1)))                                     
       end                                                                      
       , case when substr(INCRD.LVPARAM, 11, 1) > ' ' then                      
       xmlelement(name "dlvrQuery_NCG",                                         
       rtrim(substr(INCRD.LVPARAM, 11, 1)))                                     
       end                                                                      
       , xmlelement(name "warrOpc_NCG", rtrim(GA00.CGARANTIE))                  
       --add MGD                                                                
       , xmlelement(name "warrOpc_MGD", rtrim(GA00.CGARANMGD))                  
       --fin MGD                                                                
       , xmlelement(name "warrMnf_NCG", rtrim(GA00.CGARCONST))                  
       , xmlelement(name "vat_NCG", rtrim(GA00.CTAUXTVA))                       
       , xmlelement(name "priceNat_NCG"                                         
       , value(                                                                 
       (                                                                        
       select                                                                   
       replace(rtrim(cast(GN59.PREFTTC as char(8))),',','.')                    
       from GN59                                                                
       where                                                                    
       GN59.NCODIC = GA00.NCODIC                                                
       order by                                                                 
       GN59.DEFFET desc                                                         
       fetch first 1 rows only                                                  
       )                                                                        
       , '0.00')                                                                
       )                                                                        
       , xmlelement(name "bonusNat_NCG"                                         
       , value(                                                                 
       (                                                                        
       select                                                                   
       replace(rtrim(cast(GN75.PCOMMREF as char(8))),',','.')                   
       from GN75                                                                
       where                                                                    
       GN75.NCODIC = GA00.NCODIC                                                
       order by                                                                 
       GN75.DEFFET desc                                                         
       fetch first 1 rows only                                                  
       )                                                                        
       , '0.00')                                                                
       )                                                                        
       , xmlelement(name "offerNat_NCG"                                         
       , case when GA00.WSENSVTE = 'O' then 1 else 0 end                        
       )                                                                        
       --INFO:non g�r�                                                          
       , xmlelement(name "valord_NCG", 1)                                       
       --INFO:Themis (d�but)                                                    
       --prix tarif en cours                                                    
       , (                                                                      
       select                                                                   
       xmlelement(                                                              
       name "priceTrrf_NCG",                                                    
       replace(rtrim(cast(TH11.PTC as char(14))),',','.'))                      
       from RVTH1100 TH11                                                       
       left join RVTH1100 Z on                                                  
       Z.NCODIC = TH11.NCODIC                                                   
       and Z.DEFFET <= :W-DJOUR-SUIV                                            
       and Z.DEFFET > TH11.DEFFET                                               
       where                                                                    
       TH11.NCODIC = GA00.NCODIC                                                
       and TH11.DEFFET <= :W-DJOUR-SUIV                                         
       and Z.NCODIC is null                                                     
       and TH11.PTC >= 0,0                                                      
       )                                                                        
       , xmlelement(name "sourcng_NCG", rtrim(GA00.SOURCNAT))                   
       , case when GA00.COLLECTION > '' then xmlelement(                        
       name "range_NCG", rtrim(GA00.COLLECTION)) else null end                  
       , xmlelement(                                                            
       name "aliment_NCG",                                                      
       case when GA00.ALIMENTAIRE = 'O' then 1 else 0 end)                      
       , xmlelement(                                                            
       name "liquid_NCG",                                                       
       case when GA00.LIQUIDE = 'O' then 1 else 0 end)                          
       , xmlelement(                                                            
       name "perish_NCG",                                                       
       case when GA00.PEREMPT = 'O' then 1 else 0 end)                          
       , xmlelement(                                                            
       name "delay_NCG", int(GA00.DELAIACCEPT))                                 
       --priceClnt_NCG                                                          
       , (                                                                      
       select                                                                   
       xmlagg(                                                                  
       xmlelement(name "priceClnt_NCG"                                          
       , xmlelement(name "opcoClnt_NCG", rtrim(LI00.NSOCIETE))                  
       --prix public conseill� en cours                                         
       , (                                                                      
       select                                                                   
       xmlelement(name "pricePub_NCG"                                           
       , xmlelement(                                                            
       --name "value", replace(rtrim(cast(TH13.PVP as char(8))),',','.'))       
       name "value"                                                             
       , replace(rtrim(cast(min(TH13.PVP, 99999,99)                             
         as char(8))),',','.'))                                                 
       , xmlelement(name "currency", 'EUR')                                     
       )                                                                        
       from RVTH1300 TH13                                                       
       where                                                                    
       TH13.COPCO = LI00.NSOCIETE                                               
       and TH13.NCODIC = GA00.NCODIC                                            
       and TH13.DEFFET <= :W-DJOUR-SUIV                                         
       and (TH13.DFINEFFET = '' or TH13.DFINEFFET >= :W-DJOUR-SUIV)             
       )                                                                        
       --prix cession Opco en cours                                             
       , (                                                                      
       select                                                                   
       xmlelement(name "priceOpc_NCG"                                           
       , xmlelement(                                                            
       name "value", replace(rtrim(cast(TH12.PCF as char(14))),',','.'))        
       , xmlelement(name "currency", 'EUR')                                     
       )                                                                        
       from RVTH1200 TH12                                                       
       left join RVTH1200 Z on                                                  
       Z.COPCO = TH12.COPCO                                                     
       and Z.NCODIC = TH12.NCODIC                                               
       and Z.DEFFET <= :W-DJOUR-SUIV                                            
       and Z.DEFFET > TH12.DEFFET                                               
       where                                                                    
       TH12.COPCO = LI00.NSOCIETE                                               
       and TH12.NCODIC = GA00.NCODIC                                            
       and TH12.DEFFET <= :W-DJOUR-SUIV                                         
       and TH12.PCF >= 0,0                                                      
       and Z.NCODIC is null                                                     
       )                                                                        
       )                                                                        
       )                                                                        
       from RVLI0000 LI00                                                       
       where                                                                    
       LI00.CPARAM = 'OPCLI'                                                    
       and LI00.LVPARAM = 'O'                                                   
       )                                                                        
       --saleClnt_NCG                                                           
       , (                                                                      
       select                                                                   
       xmlagg(                                                                  
       xmlelement(name "saleClnt_NCG"                                           
       , xmlelement(name "opcoClnt_NCG", rtrim(TH00.COPCO))                     
       , case when TH00.CODEOPCO > '' then xmlelement(                          
       name "codicOpcClnt", rtrim(TH00.CODEOPCO))                               
       else null end                                                            
       , case when TH00.LIBOPCO > ''                                            
       then xmlelement(name "label",                                            
       rtrim(TH00.LIBOPCO)) else null end                                       
       , xmlelement(name "sourcd"                                               
       , case when TH00.FLAGREF = 'O' then 1 else 0 end)                        
       --vendabilit�s en cours ou futures                                       
       , (                                                                      
       select xmlagg(                                                           
       xmlelement(name "saleabl"                                                
       , xmlelement(name "from",                                                
       timestamp_format(TH01.DEFFET, 'YYYYMMDD'))                               
       , case                                                                   
       when (TH01.DFINEFFET = '99999999') then null                             
       else                                                                     
       xmlelement(name "to",                                                    
       timestamp_format(TH01.DFINEFFET, 'YYYYMMDD'))                            
       end                                                                      
       )                                                                        
       )                                                                        
       from RVTH0100 TH01                                                       
       where                                                                    
       TH01.NCODIC = TH00.NCODIC                                                
       and TH01.COPCO = TH00.COPCO                                              
       and TH01.DFINEFFET >= :W-DJOUR-SUIV                                      
       )                                                                        
       )                                                                        
       )                                                                        
       from RVTH0000 TH00                                                       
       where                                                                    
       TH00.NCODIC = GA00.NCODIC                                                
       )                                                                        
       --INFO: Themis (fin)                                                     
       --donn�es Innovente � minima                                             
       --non g�r�                                                               
       , xmlelement(name "ref_INO", '')                                         
       , xmlelement(name "short_INO", '')                                       
       , xmlelement(name "srcEngn_INO", '')                                     
       , xmlelement(name "lblMrkt_INO", '')                                     
       --selon param�tre Famille GVB&S(18:1)                                    
       , xmlelement(name "saleabl_INO"                                          
       , case when GVBS.CFAM is not null then int(0) else int(1) end)           
       --non g�r�                                                               
       , xmlelement(name "client_INO", int(0))                                  
       , xmlelement(name "multipl_INO", int(0))                                 
       , xmlelement(name "discount_INO", int(0))                                
       , xmlelement(name "receipt_INO", int(0))                                 
       , xmlelement(name "clntDob_INO", int(0))                                 
       --lnkSub                                                                 
       , (                                                                      
       select                                                                   
       xlnkSub                                                                  
       from lnkSub                                                              
       where                                                                    
       lnkSub.NCODIC = GA00.NCODIC                                              
       )                                                                        
       --lnkSim                                                                 
       , (                                                                      
       select                                                                   
       xlnkSim                                                                  
       from lnkSim                                                              
       where                                                                    
       lnkSim.NCODIC = GA00.NCODIC                                              
       )                                                                        
       )                                                                        
       )                                                                        
       , xmlelement(name "creatd",                                              
       timestamp_format(GA00.DCREATION, 'YYYYMMDD'))                            
       , xmlelement(name "opco", '907')                                         
       , xmlelement(name "modifd",                                              
       timestamp_format(GA00.DMAJ, 'YYYYMMDD'))                                 
       --INFO:non g�r�                                                          
       , xmlelement(name "userMod",                                             
       xmlattributes('true' as "xsi:nil"), '')                                  
       --INFO:non g�r�                                                          
       , xmlelement(name "nature", 'product')                                   
       , xmlelement(name "brand_NCG", rtrim(GA00.CMARQ))                        
       , xmlelement(name "refMnf", rtrim(GA00.LREFO))                           
       , xmlelement(name "colour_NCG", rtrim(GA00.CCOLOR))                      
       , xmlelement(name "line", rtrim(GA00.CLIGPROD))                          
       , xmlelement(name "base", rtrim(GA00.LMODBASE))                          
       , xmlelement(name "version", rtrim(GA00."VERSION"))                      
       , xmlelement(name "origin"                                               
       , rtrim(                                                                 
       case when substr(GA01.WTABLEG, 21, 1) = 'N'                              
       then substr(GA01.WTABLEG, 22, 2)                                         
       else GA01.CTABLEG2 end                                                   
       )                                                                        
       )                                                                        
       , xmlelement(name "label", rtrim(GA00.LEMBALLAGE))                       
       , xmlelement(name "lblMrkt"                                              
       , xmlattributes('fr' as "locale")                                        
       , rtrim(GA00.LREFFOURN)                                                  
       )                                                                        
       , xmlelement(name "refSale"                                              
       , xmlattributes('fr' as "locale")                                        
       , rtrim(GA00.LREFFOURN)                                                  
       )                                                                        
       , xmlelement(name "colSale"                                              
       , xmlattributes('fr' as "locale")                                        
       , ''                                                                     
       )                                                                        
       --INFO:non g�r�                                                          
       , xmlelement(name "categInt",                                            
       xmlattributes('true' as "xsi:nil"), '')                                  
       , xmlelement(name "heightPckd", int(GA00.QHAUTEUR))                      
       , xmlelement(name "widthPckd", int(GA00.QLARGEUR))                       
       , xmlelement(name "depthPckd", int(GA00.QPROFONDEUR))                    
       , xmlelement(name "weightPckd", int(GA00.QPOIDS))                        
       , xmlelement(name "height"                                               
       , replace(rtrim(cast(GA00.QHAUTDE as char(6))),',','.')                  
       )                                                                        
       , xmlelement(name "width"                                                
       , replace(rtrim(cast(GA00.QLARGDE as char(6))),',','.')                  
       )                                                                        
       , xmlelement(name "depth"                                                
       , replace(rtrim(cast(GA00.QPROFDE as char(6))),',','.')                  
       )                                                                        
       , xmlelement(name "weight", int(GA00.QPOIDSDE))                          
       , (                                                                      
       select                                                                   
       xsaleabl                                                                 
       from dests                                                               
       where                                                                    
       dests.NCODIC = GA00.NCODIC                                               
       )                                                                        
       --INFO:non g�r�                                                          
       , xmlelement(name "manual", '1')                                         
       --INFO:non g�r�                                                          
       , xmlelement(name "langage", 'fra')                                      
       , xmlelement(name "custom"                                               
       , xmlelement(name "country", 'FR')                                       
       , xmlelement(name "code", rtrim(GA00.CNOMDOU))                           
       )                                                                        
       --INFO:non g�r�                                                          
       , xmlelement(name "tax"                                                  
       , xmlelement(name "country", 'FR')                                       
       , xmlelement(name "code", 'TVA')                                         
       )                                                                        
       --INFO:composd occurs: inconnu = 0                                       
       --INFO:Themis                                                            
       , xmlelement(name "dangerUN", rtrim(GA00.NATDANGER))                     
       --INFO:launch occurs: inconnu = 0                                        
       , xmlelement(name "unitQty", max(int(GA00.QCONTENU), int(1)))            
       , xmlelement(name "freeQty", int(GA00.QGRATUITE))                        
       --Supplier principal et donn�es FL50 907                                 
       , (                                                                      
       select                                                                   
       xmlconcat(                                                               
       xmlelement(name "unitPBox"                                               
       , xmlelement(name "supplr_NCG", rtrim(GA55.NENTCDE))                     
       , xmlelement(name "qty", int(FL50.QPRODBOX))                             
       )                                                                        
       , xmlelement(name "boxPCart"                                             
       , xmlelement(name "supplr_NCG", rtrim(GA55.NENTCDE))                     
       , xmlelement(name "qty", int(FL50.QBOXCART))                             
       )                                                                        
       , xmlelement(name "cartPRow"                                             
       , xmlelement(name "supplr_NCG", rtrim(GA55.NENTCDE))                     
       , xmlelement(name "qty", int(FL50.QCARTCOUCH))                           
       )                                                                        
       , xmlelement(name "rowPPal"                                              
       , xmlelement(name "supplr_NCG", rtrim(GA55.NENTCDE))                     
       , xmlelement(name "qty", int(FL50.QCOUCHPAL))                            
       )                                                                        
       , xmlelement(name "unitPPal"                                             
       , xmlelement(name "supplr_NCG", rtrim(GA55.NENTCDE))                     
       , xmlelement(name "qty", int(FL50.QNBPRACK))                             
       )                                                                        
       --INFO:Donn�es d�plac�es dans lnkOpc/specOpc (CR Darty-2730)             
       --, xmlelement(name "unitPTruck"                                         
       --, xmlelement(name "supplr_NCG", rtrim(GA55.NENTCDE))                   
       --, xmlelement(name "qty", int(FL50.QPRODCAM))                           
       --)                                                                      
       --, xmlelement(name "unitPRec"                                           
       --, xmlelement(name "supplr_NCG", rtrim(GA55.NENTCDE))                   
       --, xmlelement(name "qty", int(FL50.QCOLIRECEPT))                        
       --)                                                                      
       )                                                                        
       from RVFL5012 FL50                                                       
       inner join RVGQ0500 GQ05 on                                              
       GQ05.CFAM = GA00.CFAM                                                    
       and GQ05.NPRIORITE = '1'                                                 
       and GQ05.NSOC = FL50.NSOCDEPOT                                           
       and GQ05.NDEPOT = FL50.NDEPOT                                            
       inner join FL05 on                                                       
       FL05.CPROAFF = GQ05.CPROAFF                                              
       and FL05.NSOCIETE = '907'                                                
       and FL05.NLIEU = '000'                                                   
       inner join RVGA5500 GA55 on                                              
       GA55.NCODIC = GA00.NCODIC                                                
       and GA55.WENTCDE = 'R'                                                   
       where                                                                    
       FL50.NCODIC = GA00.NCODIC                                                
       )                                                                        
       -- ) as XPRODUCT                                                         
       )                                                                        
       )) as clob(50K) including xmldeclaration) as XCODIF                      
       from RVGA0014 as GA00                                                    
       --limit� aux produits                                                    
       inner join RVGA1401 as GA14 on                                           
       GA14.CFAM = GA00.CFAM                                                    
       and GA14.CTYPENT = 'CO'                                                  
       inner join RVGA0300 as GA03 on                                           
       GA03.NCODIC = GA00.NCODIC                                                
       and GA03.CSOC = 'DAR'                                                    
       --mapping country vers ISO-3166-1-2                                      
       inner join RVGA0100 as GA01 on                                           
       GA01.CTABLEG1 = 'ORIGP'                                                  
       and substr(GA01.CTABLEG2, 1, 5) = GA00.CORIGPROD                         
       left join RVAD0500 as FISHM on                                           
       FISHM.NCODIC = GA00.NCODIC                                               
       and FISHM.COPCO = 'DAR'                                                  
       and FISHM.CDATA = 'FISHM'                                                
       and FISHM.WDATA = 'O'                                                    
       left join RVGA3000 as INCRD on                                           
       INCRD.CFAM = GA00.CFAM                                                   
       and INCRD.CPARAM = 'INCRD'                                               
       and substr(INCRD.LVPARAM, 7, 5) != ' '                                   
       --famille dont les produits sont non vendables seuls                     
       left join RVGA3000 as GVBS on                                            
       GVBS.CFAM = GA00.CFAM                                                    
       and GVBS.CPARAM = 'GVB&S'                                                
       and substr(GVBS.LVPARAM, 18, 1) = 'O'                                    
       --                                                                       
       inner join rvad0500 ad05 on                                              
        ga00.ncodic = ad05.ncodic                                               
       and ad05.copco = 'DAR'                                                   
       and ad05.cdata = :AD05-CDATA                                             
       and ad05.ddata = :W-DJOUR                                                
           END-EXEC                                                             
                                                                                
