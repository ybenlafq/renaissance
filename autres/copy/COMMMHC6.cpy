      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      *  COMMAREA SPECIFIQUE EDITION SUIVI DES PRODUITS AAF        *            
      *  PROJET CHS                                                *            
      **************************************************************            
      *                                                                         
      * PROGRAMME  MHC60                                                        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-MHC6-LONG-COMMAREA PIC S9(4) COMP VALUE +100.                   
      *--                                                                       
       01  COMM-MHC6-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +100.                 
      *}                                                                        
       01  Z-COMMAREA-MHC6.                                                     
      *---------------------------------------ZONES EN ENTREE DU MODULE         
           02 COMM-MHC6-I.                                                      
      *                                                                         
              03 COMM-MHC6-NLIEUHC         PIC    X(03).                        
              03 COMM-MHC6-TEBRB           PIC    X(02).                        
              03 COMM-MHC6-TRI             PIC    X(01).                        
              03 COMM-MHC6-CIMP            PIC    X(04).                        
              03 FILLER                    PIC    X(49).                        
      *---------------------------------------ZONES EN SORTIE DU MODULE         
           02 COMM-MHC6-O.                                                      
      * ERREUR --> ABANDON PROGRAMME                                            
              03 COMM-MHC6-CODRET          PIC    X VALUE '0'.                  
                 88 COMM-MHC6-OK           VALUE '0'.                           
                 88 COMM-MHC6-KO           VALUE '1'.                           
      * MESSAGE D'ERREUR                                                        
              03 COMM-MHC6-MSG             PIC    X(40).                        
                                                                                
