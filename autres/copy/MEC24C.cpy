       *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:03 >
       
            EJECT
       *==> DARTY ******************************************************
       *    ZONE DE COMMUNICATION DU MEC24                             *
       *****************************************************************
       *--> LONGUEUR MAX ADMIS POUR UNE COMMAREA 32 767.
        01  MEC24C-COMMAREA.
       *--> DONNEES FOURNIS PAR LE PROGRAMME APPELANT.
            02  MEC24C-DATA-ENTREE.
              05  MEC24C-TAB-SOC.
                10  MEC24C-SOCIETE-ENT     PIC  X(3).
                10  MEC24C-LIEU-ENT        PIC  X(3).
                10  MEC24C-CZONLIV-ENT     PIC  X(5).
                10  MEC24C-CPLAGE-ENT      PIC  X(02).
                10  MEC24C-DATE-ENT        PIC  X(08).
                10  MEC24C-CMODDEL-ENT     PIC  X(03).
                10  MEC24C-CEQUIP-ENT      PIC  X(05).
              05    FILLER                 PIC  X(100).
       *--> DONNEES RESULTANTES.
            02  MEC24C-DATA-SORTIE.
       * NBR MESSSAGE ENVOY�
                10  MEC24C-NBR-MESSAGE-ENVOYE       PIC  9(09).
                10  MEC24C-CODE-RETOUR              PIC  X(01).
                    88  MEC24C-CDRET-OK                  VALUE '0'.
                    88  MEC24C-CDRET-ERR-NON-BLQ         VALUE '1'.
                    88  MEC24C-CDRET-ERR-DB2-NON-BLQ     VALUE '2'.
                    88  MEC24C-CDRET-ERR-BLQ             VALUE '8'.
                    88  MEC24C-CDRET-ERR-DB2-BLQ         VALUE '9'.
                10  MEC24C-CODE-DB2                 PIC ++++9.
                10  MEC24C-CODE-DB2-DISPLAY         PIC  X(05).
                10  MEC24C-QUOTA.
                    20 MEC24C-RTGQ03-C-SOC  PIC XXX.
                    20 MEC24C-RTGQ03-C-LIEU PIC XXX.
                    20 MEC24C-RTGQ03-C-ZONE PIC X(5).
                    20 MEC24C-RTGQ03-DT     PIC X(8).
                    20 MEC24C-RTGQ03-C-CREN PIC XX.
                    20 MEC24C-RTGQ03-QUOTA  PIC 9(5).
                    20 MEC24C-TIMESTP       PIC 9(13)  VALUE 0.
       ********* FIN DE LA ZONE DE COMMUNICATION      *************
       
