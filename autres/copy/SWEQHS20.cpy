      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *-----------------------------------------------------*                   
      *    COPY DU FICHIER CSV D'EXTRACTION  ETAT "QCHS01B" *                   
      *    LONGUEUR : 73                                    *                   
      *-----------------------------------------------------*                   
       01  HS20-ENREG.                                                          
           05  FILLER                PIC X(08)  VALUE 'QCHS01B;'.               
           05  HS20-NLIEU            PIC X(03).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  HS20-WTLMELA          PIC X(03).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  HS20-CTHS             PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  HS20-CFAM             PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  HS20-LVPARAM          PIC X(03).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  HS20-NBRLIG           PIC 9(09).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  HS20-PRMP             PIC -9(9)V99.                              
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  HS20-DATDEB.                                                     
               10  HS20-DEB-SA       PIC X(04).                                 
               10  HS20-DEB-MM       PIC X(02).                                 
               10  HS20-DEB-JJ       PIC X(02).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  HS20-DATFIN.                                                     
               10  HS20-FIN-SA       PIC X(04).                                 
               10  HS20-FIN-MM       PIC X(02).                                 
               10  HS20-FIN-JJ       PIC X(02).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
      *                                                                         
      *-----------------------------------------------------*                   
                                                                                
