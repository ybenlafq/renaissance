      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TRALM TRANSPORTEUR                     *        
      *----------------------------------------------------------------*        
       01  RVTRALM .                                                            
           05  TRALM-CTABLEG2    PIC X(15).                                     
           05  TRALM-CTABLEG2-REDEF REDEFINES TRALM-CTABLEG2.                   
               10  TRALM-NSOCDEP         PIC X(06).                             
               10  TRALM-NSOCLIE         PIC X(06).                             
               10  TRALM-NSEQ            PIC X(03).                             
           05  TRALM-WTABLEG     PIC X(80).                                     
           05  TRALM-WTABLEG-REDEF  REDEFINES TRALM-WTABLEG.                    
               10  TRALM-CSELART         PIC X(05).                             
               10  TRALM-TRANS           PIC X(44).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTRALM-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRALM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TRALM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRALM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TRALM-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
