      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE INT1.RTAD06                        *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVAD0601.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVAD0601.                                                            
      *}                                                                        
           10 AD06-NENTCDE         PIC X(5).                                    
           10 AD06-LENTCDE         PIC X(20).                                   
           10 AD06-LENTCDEADR1     PIC X(38).                                   
           10 AD06-LENTCDEADR2     PIC X(38).                                   
           10 AD06-LENTCDEADR3     PIC X(38).                                   
           10 AD06-LENTCDEADR4     PIC X(38).                                   
           10 AD06-LENTCDEADR5     PIC X(38).                                   
           10 AD06-LENTCDEADR6     PIC X(38).                                   
           10 AD06-LENTCDEADR7     PIC X(38).                                   
           10 AD06-NENTCDETEL      PIC X(15).                                   
           10 AD06-CENTCDETELX     PIC X(15).                                   
           10 AD06-CDEVISE         PIC X(3).                                    
           10 AD06-DEFFET          PIC X(8).                                    
           10 AD06-CDEVISA         PIC X(3).                                    
           10 AD06-KHKVEN          PIC X(1).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVAD0601-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVAD0601-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD06-NENTCDE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 AD06-NENTCDE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD06-LENTCDE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 AD06-LENTCDE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD06-LENTCDEADR1-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 AD06-LENTCDEADR1-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD06-LENTCDEADR2-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 AD06-LENTCDEADR2-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD06-LENTCDEADR3-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 AD06-LENTCDEADR3-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD06-LENTCDEADR4-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 AD06-LENTCDEADR4-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD06-LENTCDEADR5-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 AD06-LENTCDEADR5-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD06-LENTCDEADR6-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 AD06-LENTCDEADR6-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD06-LENTCDEADR7-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 AD06-LENTCDEADR7-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD06-NENTCDETEL-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 AD06-NENTCDETEL-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD06-CENTCDETELX-F   PIC S9(4) COMP.                              
      *--                                                                       
           10 AD06-CENTCDETELX-F   PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD06-CDEVISE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 AD06-CDEVISE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD06-DEFFET-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 AD06-DEFFET-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD06-CDEVISA-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 AD06-CDEVISA-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD06-KHKVEN-F        PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 AD06-KHKVEN-F        PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
