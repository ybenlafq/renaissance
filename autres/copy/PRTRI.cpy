      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *===========================================================*             
      *                                                           *             
      *            MODULE DE TRI INTERNE D'UNE TABLE              *             
      *            ---------------------------------              *             
      *                                                           *             
      *     ( CE MODULE EST A UTILISER AVEC LE COPY "WSTRI" )     *             
      *                                                           *             
      *===========================================================*             
       PRTRI                   SECTION.                                         
           PERFORM    VARYING WTI FROM  1  BY 1 UNTIL WTI = WTIMAX              
               OR         WCLETRI (WTI)  =   HIGH-VALUE                         
               OR         WCLETRI (WTI)  =    LOW-VALUE                         
              COMPUTE              WTK   =   WTI   +   1                        
              PERFORM VARYING WTJ FROM WTK BY 1 UNTIL WTJ > WTIMAX              
                  OR      WCLETRI (WTJ)  =   HIGH-VALUE                         
                  OR      WCLETRI (WTJ)  =    LOW-VALUE                         
                 IF   (   CROISSANT  AND                                        
                             WCLETRI (WTI)  >   WCLETRI (WTJ) )                 
                 OR   ( DECROISSANT  AND                                        
                             WCLETRI (WTI)  <   WCLETRI (WTJ) )                 
                       MOVE  WCLETRI (WTJ) TO  WTCLETRI                         
                       MOVE  WCLETRI (WTI) TO   WCLETRI (WTJ)                   
                       MOVE WTCLETRI       TO   WCLETRI (WTI)                   
                       MOVE  WNBITEM (WTJ) TO  WTNBITEM                         
                       MOVE  WNBITEM (WTI) TO   WNBITEM (WTJ)                   
                       MOVE WTNBITEM       TO   WNBITEM (WTI)                   
                       MOVE  WNLIGNE (WTJ) TO  WTNLIGNE                         
                       MOVE  WNLIGNE (WTI) TO   WNLIGNE (WTJ)                   
                       MOVE WTNLIGNE       TO   WNLIGNE (WTI)                   
                 END-IF                                                         
              END-PERFORM                                                       
           END-PERFORM.                                                         
       PRTRI-FIN.                 EXIT.                                         
                                                                                
