      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : APPLICATION LOGISTIQUE PARTENAIRE                *        
      *  PROGRAMMES : MLP099                                           *        
      *  TITRE      : COMMAREA DU MODULE D'ALIMENTATION TABLE RTEE99   *        
      *                   TABLE COMPTE-RENDU ACTIVITE DES CHAINES      *        
      *  LONGUEUR   :      C                                           *        
      *                                                                *        
      ******************************************************************        
       01  COMM-LP99-APPLI.                                                     
      *--- DONNEES EN ENTREE DU MODULE ------------------------------ 12        
           05 COMM-LP99-DONNEES-ENTREE.                                         
              10 COMM-LP99-CPROGRAMME       PIC X(06).                          
              10 COMM-LP99-NSOCIETE         PIC X(03).                          
              10 COMM-LP99-NLIEU            PIC X(03).                          
              10 COMM-LP99-DTRAITEMENT      PIC X(08).                          
              10 COMM-LP99-DVENTECIALE      PIC X(08).                          
              10 COMM-LP99-NSEQ             PIC X(03).                          
              10 COMM-LP99-LCOMMENT         PIC X(25).                          
              10 COMM-LP99-QLIGNES          PIC 9(7) COMP-3.                    
              10 COMM-LP99-QPIECES          PIC 9(7) COMP-3.                    
      *--- CODE RETOUR + MESSAGE ------------------------------------ 59        
           05 COMM-LP99-MESSAGE.                                                
              10 COMM-LP99-CODRET           PIC X(01).                          
                 88 COMM-LP99-CODRET-OK                  VALUE ' '.             
                 88 COMM-LP99-CODRET-ERREUR              VALUE '1'.             
              10 COMM-LP99-LIBERR           PIC X(58).                          
                                                                                
