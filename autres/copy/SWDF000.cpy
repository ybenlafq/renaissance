      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *---------------------------------------------------------*               
      *    FICHIER CSV EXTRACTION DE L'ETAT IEF000              *               
      *    LONGUEUR : 209                                       *               
      *---------------------------------------------------------*               
       01  CSV-ENREG.                                                           
           05  CSV-NOMETAT           PIC X(06)  VALUE 'IEF000'.                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  CSV-DENCOURS          PIC X(08).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  CSV-CTRAIT            PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  CSV-CTIERS            PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  CSV-DENVOI            PIC X(08).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  CSV-NENVOI            PIC X(07).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  CSV-NACCORD           PIC X(12).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  CSV-DACCORD           PIC X(08).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  CSV-NHS.                                                         
               10  CSV-NENT          PIC X(04).                                 
               10  CSV-NORIGINE      PIC X(07).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  CSV-QTENV             PIC 9(06).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  CSV-NCODIC            PIC X(07).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  CSV-CMARQ             PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  CSV-CFAM              PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  CSV-LREF              PIC X(20).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  CSV-D1REL             PIC X(08).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  CSV-D2REL             PIC X(08).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  CSV-D3REL             PIC X(08).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  CSV-LCOMM             PIC X(15).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  CSV-PVTTC             PIC -9(7)V99.                              
           05  FILLER                PIC X(01)  VALUE ';'.                   000
           05  CSV-PRMP              PIC -9(7)V99.                              
           05  FILLER                PIC X(01)  VALUE ';'.                   000
           05  CSV-NSERIE            PIC X(16).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                   000
      *                                                                         
      *---------------------------------------------------------*               
                                                                                
