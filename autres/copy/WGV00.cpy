      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
1     *****************************************************************         
      *                                                               *         
      *                -------------------------------                *         
      *                -    DESCRIPTION DES ZONES    -                *         
      *                -   COMMUNES AUX PROGRAMMES   -                *         
      *                -          DE VENTE           -                *         
      *                -------------------------------                *         
      *                                                               *         
      * WGV00 REGROUPE LES DESCRIPTIONS DES ZONES COMMUNES            *         
      * A TOUS LES PROGRAMMES DE VENTE.                               *         
      *                                                               *         
      *                                                               *         
      * MGV00 EST UTILISE PAR TOUS LES PROGRAMMES DE VENTE            *         
      *                                                               *         
      *****************************************************************         
      *                                                                         
      *---------------------------------------------------------------*         
      *                  IMAGES D'EDITION                             *         
      *---------------------------------------------------------------*         
       01  FILLER                          PIC X(10) VALUE 'PICTURE'.           
      *                                                                         
       01  WS-ALPHA                        PIC X(9)     VALUE SPACES.           
      *                                                                         
       01  WS-MONTANT-3Z                   PIC ZZZZZZZZZ.                       
       01  WS-MONTANT-4Z                   PIC ZZZZZZ,ZZ.                       
      *                                                                         
       01  WS-MONTANT-3                    PIC ---------.                       
       01  WS-MONTANT-4                    PIC ------,--.                       
      *                                                                         
       01  WS-QUANTITE                     PIC ZZZ.                             
       01  WS-VENDEUR                      PIC Z.                               
      *                                                                         
      *---------------------------------------------------------------*         
      *                  ZONES DE CALCUL                              *         
      *---------------------------------------------------------------*         
       01  FILLER                          PIC X(10) VALUE 'CALCUL '.           
      *                                                                         
       01  WS-DECIMAL-5S                   PIC S9(5)V99 VALUE ZEROES.           
       01  WS-DECIMAL-6                    PIC  9(6)V99 VALUE ZEROES.           
       01  WS-DECIMAL-7.                                                        
           05 WS-DECIMAL-7S                PIC S9(9)V99 VALUE ZEROES.           
           05 FILLER                       REDEFINES    WS-DECIMAL-7S.          
              10 WS-DECIMAL-7S-E           PIC S9(9).                           
              10 WS-DECIMAL-7S-D           PIC S9(2).                           
      *                                                                         
       01  WS-ENTIER-2                     PIC  9(2)    VALUE ZEROES.           
       01  WS-ENTIER-3                     PIC  9(3)    VALUE ZEROES.           
       01  WS-ENTIER-7                     PIC  9(7)    VALUE ZEROES.           
       01  WS-ENTIER-8S                    PIC S9(8)    VALUE ZEROES.           
       01  WS-ENTIER-9                     PIC  9(9)    VALUE ZEROES.           
       01  WS-PSTDTTC                      PIC S9(9)V99 VALUE ZEROES.           
      *                                                                         
      *---------------------------------------------------------------*         
      *    RENSEIGNEMENT DE LA DATE SYSTEME SPDATDB2                  *         
      *---------------------------------------------------------------*         
       01  FILLER                  PIC X(14)  VALUE '*SPDATDB2    *'.           
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WRET                    PIC S9(4)  COMP.                             
      *--                                                                       
       01  WRET                    PIC S9(4) COMP-5.                            
      *}                                                                        
       01  DATHEUR                 PIC S9(13) COMP-3.                           
       01  W-MESS-SPDATDB2.                                                     
           05 W-MESS-SPDAT         PIC X(32)                                    
                 VALUE 'RETOUR SPDATDB2 ANORMAL, CODE : '.                      
           05 WRET-PIC             PIC 9(4).                                    
       01  WDATHEUR                PIC S9(13).                                  
       01  WDATHEUR1               REDEFINES WDATHEUR.                          
           05 WJOUR                PIC S9(05).                                  
           05 WHEURE.                                                           
              10 WHH               PIC  9(02).                                  
              10 WMM               PIC  9(02).                                  
           05 WSECONDE             PIC  9(04).                                  
                                                                                
