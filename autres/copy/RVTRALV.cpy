      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TRALV CODE TRANSPORTEUR LM6            *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVTRALV .                                                            
           05  TRALV-CTABLEG2    PIC X(15).                                     
           05  TRALV-CTABLEG2-REDEF REDEFINES TRALV-CTABLEG2.                   
               10  TRALV-CTRANS          PIC X(13).                             
           05  TRALV-WTABLEG     PIC X(80).                                     
           05  TRALV-WTABLEG-REDEF  REDEFINES TRALV-WTABLEG.                    
               10  TRALV-LTRANS          PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTRALV-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRALV-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TRALV-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRALV-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TRALV-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
