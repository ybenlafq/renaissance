      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      * COMMAREA SPECIFIQUE MODULE TRANSCO IDOC MTI00              *            
      **************************************************************            
      *                                                                         
      *   PROGRAMME MTI00                                                       
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 COMM-MTI00-LONG-COMMAREA       PIC S9(4) COMP VALUE +303.             
      *--                                                                       
       01 COMM-MTI00-LONG-COMMAREA       PIC S9(4) COMP-5 VALUE +303.           
      *}                                                                        
       01 COMM-MTI00-APPLI.                                                     
      *                                                                         
          02 COMM-MTI00-ENTREE.                                                 
      *----------------------------------------------------- 100                
             05 COMM-MTI00-PGRM         PIC X(5).                               
             05 COMM-MTI00-NTERMID      PIC X(4).                               
             05 FILLER                  PIC X(91).                              
      *                                                                         
          02 COMM-MTI00-SORTIE.                                                 
      *            MESSAGE DE SORTIE ----------------------- 100                
             05 COMM-MTI00-MESSAGE.                                             
                  10 COMM-MTI00-CODRET              PIC X(1).                   
                     88 COMM-MTI00-OK              VALUE ' '.                   
                     88 COMM-MTI00-ERR             VALUE '1'.                   
                     88 COMM-MTI00-IDOC-PT99       VALUE '2'.                   
                  10 COMM-MTI00-FILLER PIC X(1) VALUE SPACES.                   
                  10 COMM-MTI00-LIBERR             PIC X(50).                   
                  10 FILLER                        PIC X(48).                   
          02  FILLER                               PIC X(100).                  
      *                                                                         
                                                                                
