      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BDCLI AUTORISATION BASE CLIENT         *        
      *----------------------------------------------------------------*        
       01  RVBDCLI.                                                             
           05  BDCLI-CTABLEG2    PIC X(15).                                     
           05  BDCLI-CTABLEG2-REDEF REDEFINES BDCLI-CTABLEG2.                   
               10  BDCLI-SOC             PIC X(03).                             
               10  BDCLI-LIEU            PIC X(03).                             
           05  BDCLI-WTABLEG     PIC X(80).                                     
           05  BDCLI-WTABLEG-REDEF  REDEFINES BDCLI-WTABLEG.                    
               10  BDCLI-ACTIF           PIC X(01).                             
               10  BDCLI-CTNVE           PIC X(05).                             
               10  BDCLI-CURSEUR         PIC X(01).                             
               10  BDCLI-COD-PAY         PIC X(17).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVBDCLI-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BDCLI-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BDCLI-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BDCLI-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BDCLI-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
