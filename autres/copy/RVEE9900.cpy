      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVEE9900                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEE9900                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVEE9900.                                                            
           02  EE99-CPROGRAMME                                                  
               PIC X(0006).                                                     
           02  EE99-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  EE99-NLIEU                                                       
               PIC X(0003).                                                     
           02  EE99-DTRAITEMENT                                                 
               PIC X(0008).                                                     
           02  EE99-DVENTECIALE                                                 
               PIC X(0008).                                                     
           02  EE99-NSEQ                                                        
               PIC X(0003).                                                     
           02  EE99-LCOMMENT                                                    
               PIC X(0025).                                                     
           02  EE99-QLIGNES                                                     
               PIC S9(7) COMP-3.                                                
           02  EE99-QPIECES                                                     
               PIC S9(7) COMP-3.                                                
           02  EE99-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVEE9900                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVEE9900-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE99-CPROGRAMME-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE99-CPROGRAMME-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE99-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE99-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE99-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE99-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE99-DTRAITEMENT-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE99-DTRAITEMENT-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE99-DVENTECIALE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE99-DVENTECIALE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE99-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE99-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE99-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE99-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE99-QLIGNES-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE99-QLIGNES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE99-QPIECES-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE99-QPIECES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE99-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE99-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
       EJECT                                                                    

                                                                                
