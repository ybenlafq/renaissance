      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  MESBWI00-MESSAGE.                                                    
              05 MESBWI00-RETOUR.                                               
                 10 MESBWI00-CODE-RETOUR.                                       
                    15  MESBWI00-CODRET  PIC X(1).                              
                         88  MESBWI00-CODRET-OK        VALUE ' '.               
                         88  MESBWI00-CODRET-ERREUR    VALUE '1'.               
                    15  MESBWI00-LIBERR  PIC X(33).                             
              05 MESBWI00-DATA.                                                 
                 10 MESBWI00-DATA-X.                                            
                    15 MESBWI00-NSOCDEPOT      PIC X(3).                        
                    15 MESBWI00-NDEPOT         PIC X(3).                        
                    15 MESBWI00-WJIE311        PIC X(1).                        
                    15 MESBWI00-WFERME         PIC X(1).                        
                    15 MESBWI00-TRANSFERT      PIC X(1).                        
                    15 MESBWI00-LIBRE          PIC X(91).                       
                 10 MESBWI00-DATA-INUTILE     PIC X(29840).                     
           05 MESBWI00-FILLER2 PIC X(26).                               00001670
                                                                                
