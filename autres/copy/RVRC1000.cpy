           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
 
      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 25/04/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVRC1000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRC1000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRC1000.                                                            
           02  RC10-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  RC10-NLIEU                                                       
               PIC X(0003).                                                     
           02  RC10-NOMFIC                                                      
               PIC X(0008).                                                     
           02  RC10-DATEFIC                                                     
               PIC X(0008).                                                     
           02  RC10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRC1000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRC1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RC10-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RC10-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RC10-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RC10-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RC10-NOMFIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RC10-NOMFIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RC10-DATEFIC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RC10-DATEFIC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RC10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RC10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           EXEC SQL END DECLARE SECTION END-EXEC.
       EJECT
 
                                                                                
