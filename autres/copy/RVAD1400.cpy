      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RTAD14                      *        
      ******************************************************************        
       01  RVAD1400.                                                            
           10 AD14-CFAMK           PIC X(5).                                    
           10 AD14-CFAM            PIC X(5).                                    
           10 AD14-LFAM            PIC X(20).                                   
           10 AD14-WSEQFAM         PIC S9(05) COMP-3.                           
           10 AD14-WMULTIFAM       PIC X.                                       
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVAD1400-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD14-CFAMK-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 AD14-CFAMK-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD14-CFAM-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 AD14-CFAM-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD14-LFAM-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 AD14-LFAM-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD14-WSEQFAM-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 AD14-WSEQFAM-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD14-WMULTIFAM-F     PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 AD14-WMULTIFAM-F     PIC S9(4) COMP-5.                            
                                                                                
      *}                                                                        
