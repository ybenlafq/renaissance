      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVPT9903                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPT9903                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPT9903.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPT9903.                                                            
      *}                                                                        
           02  PT9903-CODLANG                                                   
               PIC X(0002).                                                     
           02  PT9903-CNOMPGRM                                                  
               PIC X(0008).                                                     
           02  PT9903-NSEQLIB                                                   
               PIC X(0004).                                                     
           02  PT9903-CLONG                                                     
               PIC X(0003).                                                     
           02  PT9903-LIBEDIT                                                   
               PIC X(0198).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPT9903                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPT9903-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPT9903-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PT9903-CODLANG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PT9903-CODLANG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PT9903-CNOMPGRM-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PT9903-CNOMPGRM-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PT9903-NSEQLIB-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PT9903-NSEQLIB-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PT9903-CLONG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PT9903-CLONG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PT9903-LIBEDIT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PT9903-LIBEDIT-F                                                 
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
