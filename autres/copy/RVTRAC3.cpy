      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TRAC3 REGLES DE CTRLS PAR IDREGLE      *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTRAC3 .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTRAC3 .                                                            
      *}                                                                        
           05  TRAC3-CTABLEG2    PIC X(15).                                     
           05  TRAC3-CTABLEG2-REDEF REDEFINES TRAC3-CTABLEG2.                   
               10  TRAC3-IDREGLE         PIC X(10).                             
           05  TRAC3-WTABLEG     PIC X(80).                                     
           05  TRAC3-WTABLEG-REDEF  REDEFINES TRAC3-WTABLEG.                    
               10  TRAC3-REGLE           PIC X(30).                             
               10  TRAC3-COMM            PIC X(20).                             
               10  TRAC3-PRINT           PIC X(01).                             
               10  TRAC3-CONTEXT         PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTRAC3-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTRAC3-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRAC3-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TRAC3-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRAC3-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TRAC3-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
