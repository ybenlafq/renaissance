      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TVAVE TVA VENTES                       *        
      *----------------------------------------------------------------*        
       01  RVTVAVE .                                                            
           05  TVAVE-CTABLEG2    PIC X(15).                                     
           05  TVAVE-CTABLEG2-REDEF REDEFINES TVAVE-CTABLEG2.                   
               10  TVAVE-CTAUXTVA        PIC X(05).                             
               10  TVAVE-DDEBEFF         PIC X(08).                             
           05  TVAVE-WTABLEG     PIC X(80).                                     
           05  TVAVE-WTABLEG-REDEF  REDEFINES TVAVE-WTABLEG.                    
               10  TVAVE-DFINEFF         PIC X(08).                             
               10  TVAVE-VALTVA          PIC X(05).                             
               10  TVAVE-VALCOEFF        PIC X(08).                             
               10  TVAVE-LIBELLE         PIC X(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTVAVE-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TVAVE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TVAVE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TVAVE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TVAVE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
