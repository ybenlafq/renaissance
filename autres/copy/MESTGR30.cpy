      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * - MESTGR30 COPY GENERALISES  49974 OCTETS                      *      13
      *   = 105 DE DEFINITION D ENTETE                                 *        
      *   + 34  DE DEFINITION DE CODE RETOUR                           *        
      *   + 49835 DE MESSAGE REEL                                      *        
      * COPY POUR LA SERIE DE TRANSACTION TGR3*                        *        
      ******************************************************************        
              05 MESTGR30-ENTETE.                                               
                 10 MESTGR30-TYPE        PIC X(03).                             
                 10 MESTGR30-NSOCMSG     PIC X(03).                             
                 10 MESTGR30-NLIEUMSG    PIC X(03).                             
                 10 MESTGR30-NSOCDST     PIC X(03).                             
                 10 MESTGR30-NLIEUDST    PIC X(03).                             
                 10 MESTGR30-NORD        PIC 9(08).                             
                 10 MESTGR30-LPROG       PIC X(10).                             
                 10 MESTGR30-DJOUR       PIC X(08).                             
                 10 MESTGR30-WSID        PIC X(10).                             
                 10 MESTGR30-USER        PIC X(10).                             
                 10 MESTGR30-CHRONO      PIC 9(07).                             
                 10 MESTGR30-NBRMSG      PIC 9(07).                             
                 10 MESTGR30-NBRENR      PIC 9(5).                              
                 10 MESTGR30-TAILLE      PIC 9(5).                              
                 10 MESTGR30-VERSION     PIC X(2).                              
                 10 MESTGR30-FILLER      PIC X(18).                             
              05 MESTGR30-RETOUR.                                               
                 10 MESTGR30-CODE-RETOUR.                                       
                    15  MESTGR30-CODRET  PIC X(1).                              
                         88  MESTGR30-CODRET-OK        VALUE ' '.               
                         88  MESTGR30-CODRET-ERREUR    VALUE '1'.               
                    15  MESTGR30-LIBERR  PIC X(33).                             
              05 MESTGR30-DATA           PIC X(49835).                          
                                                                                
